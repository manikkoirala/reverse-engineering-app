.class public final Lcom/intsig/camscanner/util/ImageProgressClient;
.super Ljava/lang/Object;
.source "ImageProgressClient.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lcom/intsig/okbinder/AIDL;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/util/ImageProgressClient$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/intsig/camscanner/util/ImageProgressClient;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final Companion:Lcom/intsig/camscanner/util/ImageProgressClient$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final DEFAULT_BRIGHTNESS_INDEX:I = 0x0

.field private static final DEFAULT_CONTRAST_INDEX:I = 0x0

.field private static final DEFAULT_DETAIL_INDEX:I = 0x64

.field private static final TAG:Ljava/lang/String; = "ImageProgressClient"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final UNKNOWN_UUID:Ljava/lang/String; = "UNKNOWN_UUID"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private autoScaleForEnhance:F

.field private border:[I

.field private classificationCallback:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;

.field private cloudFilterImageCallback:Lcom/intsig/camscanner/imagescanner/CloudFilterImageCallback;

.field private final dewarpPostType:I

.field private eeMoireWithTrimmedBackBack:Lcom/intsig/camscanner/imagescanner/DeMoireWithTrimmedBackBack;

.field private encodeImageSMoz:Z

.field private enhanceMode:I

.field private eraseMaskAllServerCallback:Lcom/intsig/camscanner/imagescanner/EraseMaskAllServerCallback;

.field private forbidDeMoire:Z

.field private imageUuid:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private isDecodeOk:Z

.field private isEncodeOk:Z

.field private isUsingNewTrimLib:Z

.field private mAllowLocalSuperFilterEngine:Z

.field private mBrightnessIndex:I

.field private mCallbackWhenFinish:Lcom/intsig/camscanner/image_progress/ImageProcessCallback;

.field private mContrastIndex:I

.field private mDeMoireSuccessful:Z

.field private mDetailIndex:I

.field private mEnableAutoFindRotation:Z

.field private final mEnableCloudServiceWithServerCfg:Z

.field private mEnableTrim:Z

.field private mEngineUseCpuCount:I

.field private mImageProgressListener:Lcom/intsig/camscanner/image_progress/ImageProgressListener;

.field private final mImageQualityHelper:Lcom/intsig/camscanner/demoire/ImageQualityHelper;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private mImageStruct:I

.field private mNeedSmallEnhanceMode:Z

.field private final mOnlyUseLocalSuperFilter:Z

.field private mPadPath:Ljava/lang/String;

.field private mPageIndex:I

.field private mRawImageSize:[I

.field private mRotationBeforeTrim:I

.field private mSaveImagePath:Ljava/lang/String;

.field private mSaveImageQualityForTemp:I

.field private mSaveOnlyTrimImage:Ljava/lang/String;

.field private mSrcImagePath:Ljava/lang/String;

.field private mSuperFilterNeedTrace:Z

.field private mThreadContext:I

.field private mTrimmedPaperPath:Ljava/lang/String;

.field private needAutoDeMoire:Z

.field private needClearTrimImageCache:Z

.field private needCropDewrap:Z

.field private needCropWhenNoBorder:Z

.field private needDeBlurImage:Z

.field private needDetectBorder:Z

.field private onlyNeedTrimmedImage:Z

.field private rotation:I

.field private serverFilterRes:I

.field private serverHandleImageErrorCode:I

.field private superFilterImageCallback:Lcom/intsig/camscanner/imagescanner/SuperFilterImageCallback;

.field private final threadTimeOutChecker:Lcom/intsig/thread/ThreadTimeOutChecker;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private trimImageMaxSide:I

.field private trimResult:I

.field private final usingDewarpNewModel:Z

.field private final usingNewDeShadow:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/util/ImageProgressClient$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/util/ImageProgressClient$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/util/ImageProgressClient;->Companion:Lcom/intsig/camscanner/util/ImageProgressClient$Companion;

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/util/ImageProgressClient$Creator;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/camscanner/util/ImageProgressClient$Creator;-><init>()V

    .line 12
    .line 13
    .line 14
    sput-object v0, Lcom/intsig/camscanner/util/ImageProgressClient;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 44

    .line 1
    move-object/from16 v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, -0x1

    const/16 v42, 0xff

    const/16 v43, 0x0

    invoke-direct/range {v0 .. v43}, Lcom/intsig/camscanner/util/ImageProgressClient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[IZIIIII[IZZLjava/lang/String;Ljava/lang/String;ZIZZIIIZZZZZZZZIZIZZIZIZLjava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[IZIIIII[IZZLjava/lang/String;Ljava/lang/String;ZIZZIIIZZZZZZZZIZIZZIZIZLjava/lang/String;)V
    .locals 5
    .param p40    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object v0, p0

    move-object/from16 v1, p40

    const-string v2, "imageUuid"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v2, p1

    .line 3
    iput-object v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSrcImagePath:Ljava/lang/String;

    move-object v2, p2

    .line 4
    iput-object v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    move-object v2, p3

    .line 5
    iput-object v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    move-object v2, p4

    .line 6
    iput-object v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    move v2, p5

    .line 7
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->needDetectBorder:Z

    move v2, p6

    .line 8
    iput v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->rotation:I

    move v2, p7

    .line 9
    iput v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    move v2, p8

    .line 10
    iput v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mDetailIndex:I

    move v2, p9

    .line 11
    iput v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mContrastIndex:I

    move v2, p10

    .line 12
    iput v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mBrightnessIndex:I

    move-object/from16 v2, p11

    .line 13
    iput-object v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    move/from16 v2, p12

    .line 14
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEnableTrim:Z

    move/from16 v2, p13

    .line 15
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEnableAutoFindRotation:Z

    move-object/from16 v2, p14

    .line 16
    iput-object v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mTrimmedPaperPath:Ljava/lang/String;

    move-object/from16 v2, p15

    .line 17
    iput-object v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mPadPath:Ljava/lang/String;

    move/from16 v2, p16

    .line 18
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->encodeImageSMoz:Z

    move/from16 v2, p17

    .line 19
    iput v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->trimImageMaxSide:I

    move/from16 v2, p18

    .line 20
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->isDecodeOk:Z

    move/from16 v2, p19

    .line 21
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->isEncodeOk:Z

    move/from16 v2, p20

    .line 22
    iput v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mPageIndex:I

    move/from16 v2, p21

    .line 23
    iput v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverFilterRes:I

    move/from16 v2, p22

    .line 24
    iput v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverHandleImageErrorCode:I

    move/from16 v2, p23

    .line 25
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->needDeBlurImage:Z

    move/from16 v2, p24

    .line 26
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->needCropDewrap:Z

    move/from16 v2, p25

    .line 27
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->needAutoDeMoire:Z

    move/from16 v2, p26

    .line 28
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->onlyNeedTrimmedImage:Z

    move/from16 v2, p27

    .line 29
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->needCropWhenNoBorder:Z

    move/from16 v2, p28

    .line 30
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mAllowLocalSuperFilterEngine:Z

    move/from16 v2, p29

    .line 31
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mOnlyUseLocalSuperFilter:Z

    move/from16 v2, p30

    .line 32
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEnableCloudServiceWithServerCfg:Z

    move/from16 v2, p31

    .line 33
    iput v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEngineUseCpuCount:I

    move/from16 v2, p32

    .line 34
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSuperFilterNeedTrace:Z

    move/from16 v2, p33

    .line 35
    iput v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->trimResult:I

    move/from16 v2, p34

    .line 36
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->usingNewDeShadow:Z

    move/from16 v2, p35

    .line 37
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->usingDewarpNewModel:Z

    move/from16 v2, p36

    .line 38
    iput v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->dewarpPostType:I

    move/from16 v2, p37

    .line 39
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->isUsingNewTrimLib:Z

    move/from16 v2, p38

    .line 40
    iput v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRotationBeforeTrim:I

    move/from16 v2, p39

    .line 41
    iput-boolean v2, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mNeedSmallEnhanceMode:Z

    .line 42
    iput-object v1, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->imageUuid:Ljava/lang/String;

    const/high16 v1, 0x3f800000    # 1.0f

    .line 43
    iput v1, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->autoScaleForEnhance:F

    const/16 v1, 0x50

    .line 44
    iput v1, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImageQualityForTemp:I

    .line 45
    new-instance v1, Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v1, v4, v2, v3}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;-><init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v1, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageQualityHelper:Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 46
    new-instance v1, Lcom/intsig/thread/ThreadTimeOutChecker;

    const-wide/16 v2, 0x4e20

    const-string v4, "ImageProgressClient"

    invoke-direct {v1, v2, v3, v4}, Lcom/intsig/thread/ThreadTimeOutChecker;-><init>(JLjava/lang/String;)V

    iput-object v1, v0, Lcom/intsig/camscanner/util/ImageProgressClient;->threadTimeOutChecker:Lcom/intsig/thread/ThreadTimeOutChecker;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[IZIIIII[IZZLjava/lang/String;Ljava/lang/String;ZIZZIIIZZZZZZZZIZIZZIZIZLjava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 40

    move/from16 v0, p41

    move/from16 v1, p42

    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    move-object/from16 v2, p1

    :goto_0
    and-int/lit8 v4, v0, 0x2

    if-eqz v4, :cond_1

    const/4 v4, 0x0

    goto :goto_1

    :cond_1
    move-object/from16 v4, p2

    :goto_1
    and-int/lit8 v5, v0, 0x4

    if-eqz v5, :cond_2

    const/4 v5, 0x0

    goto :goto_2

    :cond_2
    move-object/from16 v5, p3

    :goto_2
    and-int/lit8 v6, v0, 0x8

    if-eqz v6, :cond_3

    const/4 v6, 0x0

    goto :goto_3

    :cond_3
    move-object/from16 v6, p4

    :goto_3
    and-int/lit8 v7, v0, 0x10

    if-eqz v7, :cond_4

    const/4 v7, 0x1

    goto :goto_4

    :cond_4
    move/from16 v7, p5

    :goto_4
    and-int/lit8 v9, v0, 0x20

    if-eqz v9, :cond_5

    const/4 v9, 0x0

    goto :goto_5

    :cond_5
    move/from16 v9, p6

    :goto_5
    and-int/lit8 v11, v0, 0x40

    if-eqz v11, :cond_6

    const/4 v11, -0x1

    goto :goto_6

    :cond_6
    move/from16 v11, p7

    :goto_6
    and-int/lit16 v13, v0, 0x80

    if-eqz v13, :cond_7

    const/16 v13, 0x64

    goto :goto_7

    :cond_7
    move/from16 v13, p8

    :goto_7
    and-int/lit16 v14, v0, 0x100

    if-eqz v14, :cond_8

    const/4 v14, 0x0

    goto :goto_8

    :cond_8
    move/from16 v14, p9

    :goto_8
    and-int/lit16 v15, v0, 0x200

    if-eqz v15, :cond_9

    const/4 v15, 0x0

    goto :goto_9

    :cond_9
    move/from16 v15, p10

    :goto_9
    and-int/lit16 v3, v0, 0x400

    if-eqz v3, :cond_a

    const/4 v3, 0x0

    goto :goto_a

    :cond_a
    move-object/from16 v3, p11

    :goto_a
    and-int/lit16 v8, v0, 0x800

    if-eqz v8, :cond_b

    const/4 v8, 0x1

    goto :goto_b

    :cond_b
    move/from16 v8, p12

    :goto_b
    and-int/lit16 v10, v0, 0x1000

    if-eqz v10, :cond_c

    const/4 v10, 0x0

    goto :goto_c

    :cond_c
    move/from16 v10, p13

    :goto_c
    and-int/lit16 v12, v0, 0x2000

    if-eqz v12, :cond_d

    const/4 v12, 0x0

    goto :goto_d

    :cond_d
    move-object/from16 v12, p14

    :goto_d
    move-object/from16 v16, v12

    and-int/lit16 v12, v0, 0x4000

    if-eqz v12, :cond_e

    const/4 v12, 0x0

    goto :goto_e

    :cond_e
    move-object/from16 v12, p15

    :goto_e
    const v17, 0x8000

    and-int v17, v0, v17

    if-eqz v17, :cond_f

    const/16 v17, 0x1

    goto :goto_f

    :cond_f
    move/from16 v17, p16

    :goto_f
    const/high16 v18, 0x10000

    and-int v18, v0, v18

    if-eqz v18, :cond_10

    const/16 v18, 0x0

    goto :goto_10

    :cond_10
    move/from16 v18, p17

    :goto_10
    const/high16 v19, 0x20000

    and-int v19, v0, v19

    if-eqz v19, :cond_11

    const/16 v19, 0x1

    goto :goto_11

    :cond_11
    move/from16 v19, p18

    :goto_11
    const/high16 v20, 0x40000

    and-int v20, v0, v20

    if-eqz v20, :cond_12

    const/16 v20, 0x1

    goto :goto_12

    :cond_12
    move/from16 v20, p19

    :goto_12
    const/high16 v21, 0x80000

    and-int v21, v0, v21

    if-eqz v21, :cond_13

    const/16 v21, 0x0

    goto :goto_13

    :cond_13
    move/from16 v21, p20

    :goto_13
    const/high16 v22, 0x100000

    and-int v22, v0, v22

    if-eqz v22, :cond_14

    const/16 v22, 0x0

    goto :goto_14

    :cond_14
    move/from16 v22, p21

    :goto_14
    const/high16 v23, 0x200000

    and-int v23, v0, v23

    if-eqz v23, :cond_15

    const/16 v23, 0x0

    goto :goto_15

    :cond_15
    move/from16 v23, p22

    :goto_15
    const/high16 v24, 0x400000

    and-int v24, v0, v24

    if-eqz v24, :cond_16

    const/16 v24, 0x0

    goto :goto_16

    :cond_16
    move/from16 v24, p23

    :goto_16
    const/high16 v25, 0x800000

    and-int v25, v0, v25

    if-eqz v25, :cond_17

    const/16 v25, 0x0

    goto :goto_17

    :cond_17
    move/from16 v25, p24

    :goto_17
    const/high16 v26, 0x1000000

    and-int v26, v0, v26

    if-eqz v26, :cond_18

    const/16 v26, 0x0

    goto :goto_18

    :cond_18
    move/from16 v26, p25

    :goto_18
    const/high16 v27, 0x2000000

    and-int v27, v0, v27

    if-eqz v27, :cond_19

    const/16 v27, 0x0

    goto :goto_19

    :cond_19
    move/from16 v27, p26

    :goto_19
    const/high16 v28, 0x4000000

    and-int v28, v0, v28

    if-eqz v28, :cond_1a

    const/16 v28, 0x0

    goto :goto_1a

    :cond_1a
    move/from16 v28, p27

    :goto_1a
    const/high16 v29, 0x8000000

    and-int v29, v0, v29

    if-eqz v29, :cond_1b

    .line 47
    sget-object v29, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->Companion:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;

    invoke-virtual/range {v29 .. v29}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;->isAllowLocalSuperFilter()Z

    move-result v29

    goto :goto_1b

    :cond_1b
    move/from16 v29, p28

    :goto_1b
    const/high16 v30, 0x10000000

    and-int v30, v0, v30

    if-eqz v30, :cond_1c

    .line 48
    invoke-static {}, Lcom/intsig/camscanner/experiment/SuperFilterStyleExp;->O8()Z

    move-result v30

    goto :goto_1c

    :cond_1c
    move/from16 v30, p29

    :goto_1c
    const/high16 v31, 0x20000000

    and-int v31, v0, v31

    if-eqz v31, :cond_1d

    .line 49
    invoke-static {}, Lcom/intsig/camscanner/ipo/IPOCheck;->o800o8O()Z

    move-result v31

    goto :goto_1d

    :cond_1d
    move/from16 v31, p30

    :goto_1d
    const/high16 v32, 0x40000000    # 2.0f

    and-int v32, v0, v32

    if-eqz v32, :cond_1e

    .line 50
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/intsig/tsapp/sync/AppConfigJson;->getEngineUseCupCount()I

    move-result v32

    goto :goto_1e

    :cond_1e
    move/from16 v32, p31

    :goto_1e
    const/high16 v33, -0x80000000

    and-int v0, v0, v33

    if-eqz v0, :cond_1f

    const/4 v0, 0x0

    goto :goto_1f

    :cond_1f
    move/from16 v0, p32

    :goto_1f
    and-int/lit8 v33, v1, 0x1

    if-eqz v33, :cond_20

    const/16 v33, -0x1

    goto :goto_20

    :cond_20
    move/from16 v33, p33

    :goto_20
    and-int/lit8 v34, v1, 0x2

    if-eqz v34, :cond_21

    .line 51
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->isUsingNewMagicDeShadow()Z

    move-result v34

    goto :goto_21

    :cond_21
    move/from16 v34, p34

    :goto_21
    and-int/lit8 v35, v1, 0x4

    if-eqz v35, :cond_22

    .line 52
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Lcom/intsig/tsapp/sync/AppConfigJson;->isUsingNewModel()Z

    move-result v35

    goto :goto_22

    :cond_22
    move/from16 v35, p35

    :goto_22
    and-int/lit8 v36, v1, 0x8

    if-eqz v36, :cond_23

    .line 53
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Lcom/intsig/tsapp/sync/AppConfigJson;->getEngineDewarpPostType()I

    move-result v36

    goto :goto_23

    :cond_23
    move/from16 v36, p36

    :goto_23
    and-int/lit8 v37, v1, 0x10

    if-eqz v37, :cond_24

    .line 54
    sget-object v37, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;

    invoke-virtual/range {v37 .. v37}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080()Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->isUsingNewTrimLib()Z

    move-result v37

    goto :goto_24

    :cond_24
    move/from16 v37, p37

    :goto_24
    and-int/lit8 v38, v1, 0x20

    if-eqz v38, :cond_25

    const/16 v38, 0x0

    goto :goto_25

    :cond_25
    move/from16 v38, p38

    :goto_25
    and-int/lit8 v39, v1, 0x40

    if-eqz v39, :cond_26

    const/16 v39, 0x0

    goto :goto_26

    :cond_26
    move/from16 v39, p39

    :goto_26
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_27

    const-string v1, "UNKNOWN_UUID"

    goto :goto_27

    :cond_27
    move-object/from16 v1, p40

    :goto_27
    move-object/from16 p1, p0

    move-object/from16 p2, v2

    move-object/from16 p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move/from16 p6, v7

    move/from16 p7, v9

    move/from16 p8, v11

    move/from16 p9, v13

    move/from16 p10, v14

    move/from16 p11, v15

    move-object/from16 p12, v3

    move/from16 p13, v8

    move/from16 p14, v10

    move-object/from16 p15, v16

    move-object/from16 p16, v12

    move/from16 p17, v17

    move/from16 p18, v18

    move/from16 p19, v19

    move/from16 p20, v20

    move/from16 p21, v21

    move/from16 p22, v22

    move/from16 p23, v23

    move/from16 p24, v24

    move/from16 p25, v25

    move/from16 p26, v26

    move/from16 p27, v27

    move/from16 p28, v28

    move/from16 p29, v29

    move/from16 p30, v30

    move/from16 p31, v31

    move/from16 p32, v32

    move/from16 p33, v0

    move/from16 p34, v33

    move/from16 p35, v34

    move/from16 p36, v35

    move/from16 p37, v36

    move/from16 p38, v37

    move/from16 p39, v38

    move/from16 p40, v39

    move-object/from16 p41, v1

    .line 55
    invoke-direct/range {p1 .. p41}, Lcom/intsig/camscanner/util/ImageProgressClient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[IZIIIII[IZZLjava/lang/String;Ljava/lang/String;ZIZZIIIZZZZZZZZIZIZZIZIZLjava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$adjustImage(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->adjustImage()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$autoScaleBeforeEnhance(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->autoScaleBeforeEnhance()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$checkAndDetectImageBorder(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->checkAndDetectImageBorder()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$classifyImage(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->classifyImage()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$deBlurImage(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->deBlurImage()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$deMoireImage(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->deMoireImage()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$decodeImage(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->decodeImage()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$detectRotation(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->detectRotation()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$enhanceImage(Lcom/intsig/camscanner/util/ImageProgressClient;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceImage()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$getDewarpPostType$p(Lcom/intsig/camscanner/util/ImageProgressClient;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->dewarpPostType:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$getEraseMaskAllServerCallback$p(Lcom/intsig/camscanner/util/ImageProgressClient;)Lcom/intsig/camscanner/imagescanner/EraseMaskAllServerCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->eraseMaskAllServerCallback:Lcom/intsig/camscanner/imagescanner/EraseMaskAllServerCallback;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$getMEngineUseCpuCount$p(Lcom/intsig/camscanner/util/ImageProgressClient;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEngineUseCpuCount:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$getMImageStruct$p(Lcom/intsig/camscanner/util/ImageProgressClient;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$getOnlyNeedTrimmedImage$p(Lcom/intsig/camscanner/util/ImageProgressClient;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->onlyNeedTrimmedImage:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$logD(Lcom/intsig/camscanner/util/ImageProgressClient;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic access$prepareParams(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->prepareParams()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$recordWhenFinish(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->recordWhenFinish()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$releaseStruct(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->releaseStruct()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$rotateBeforeTrim(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->rotateBeforeTrim()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$rotateImage(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->rotateImage()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$saveImage(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->saveImage()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$saveOnlyTrimImage(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->saveOnlyTrimImage()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$saveTrimmedPaper(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->saveTrimmedPaper()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic access$setAutoScaleForEnhance$p(Lcom/intsig/camscanner/util/ImageProgressClient;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->autoScaleForEnhance:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic access$setClassificationCallback$p(Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->classificationCallback:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic access$setImageUuid$p(Lcom/intsig/camscanner/util/ImageProgressClient;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->imageUuid:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic access$setMCallbackWhenFinish$p(Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/image_progress/ImageProcessCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mCallbackWhenFinish:Lcom/intsig/camscanner/image_progress/ImageProcessCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic access$setMImageProgressListener$p(Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/image_progress/ImageProgressListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageProgressListener:Lcom/intsig/camscanner/image_progress/ImageProgressListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic access$trimImage(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->trimImage()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final adjustImage()V
    .locals 8

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mBrightnessIndex:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mContrastIndex:I

    .line 6
    .line 7
    if-nez v1, :cond_0

    .line 8
    .line 9
    iget v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mDetailIndex:I

    .line 10
    .line 11
    const/16 v2, 0x64

    .line 12
    .line 13
    if-ne v1, v2, :cond_0

    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageProgressListener:Lcom/intsig/camscanner/image_progress/ImageProgressListener;

    .line 17
    .line 18
    if-eqz v1, :cond_1

    .line 19
    .line 20
    iget v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mContrastIndex:I

    .line 21
    .line 22
    iget v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mDetailIndex:I

    .line 23
    .line 24
    invoke-interface {v1, v0, v2, v3}, Lcom/intsig/camscanner/image_progress/ImageProgressListener;->startAdjustImage(III)V

    .line 25
    .line 26
    .line 27
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 28
    .line 29
    .line 30
    move-result-wide v0

    .line 31
    iget v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mThreadContext:I

    .line 32
    .line 33
    iget v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 34
    .line 35
    iget v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mBrightnessIndex:I

    .line 36
    .line 37
    iget v5, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mContrastIndex:I

    .line 38
    .line 39
    iget v6, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mDetailIndex:I

    .line 40
    .line 41
    invoke-static {v2, v3, v4, v5, v6}, Lcom/intsig/scanner/ScannerEngine;->adjustImageS(IIIII)I

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    iget v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mBrightnessIndex:I

    .line 46
    .line 47
    iget v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mContrastIndex:I

    .line 48
    .line 49
    iget v5, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mDetailIndex:I

    .line 50
    .line 51
    new-instance v6, Ljava/lang/StringBuilder;

    .line 52
    .line 53
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .line 55
    .line 56
    const-string v7, "mBrightnessIndex="

    .line 57
    .line 58
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    const-string v3, " mContrastIndex="

    .line 65
    .line 66
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    const-string v3, " mDetailIndex="

    .line 73
    .line 74
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v3

    .line 84
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 88
    .line 89
    .line 90
    move-result-wide v3

    .line 91
    sub-long/2addr v3, v0

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    .line 93
    .line 94
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    .line 96
    .line 97
    const-string v1, "adjustImageS result="

    .line 98
    .line 99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    const-string v1, " costTime="

    .line 106
    .line 107
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 118
    .line 119
    .line 120
    return-void
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private final autoScaleBeforeEnhance()V
    .locals 7

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->trimImageMaxSide:I

    .line 2
    .line 3
    const-string v1, "ImageProgressClient"

    .line 4
    .line 5
    if-gtz v0, :cond_0

    .line 6
    .line 7
    const-string v0, "autoScaleBeforeEnhance trimImageMaxSide <= 0"

    .line 8
    .line 9
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    const/4 v3, 0x0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mThreadContext:I

    .line 20
    .line 21
    iget-object v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 22
    .line 23
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 24
    .line 25
    .line 26
    aget v4, v4, v3

    .line 27
    .line 28
    iget-object v5, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 29
    .line 30
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 31
    .line 32
    .line 33
    aget v5, v5, v2

    .line 34
    .line 35
    iget-object v6, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 36
    .line 37
    invoke-static {v0, v4, v5, v6}, Lcom/intsig/scanner/ScannerEngine;->nativeDewarpImagePlaneForSize(III[I)[I

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    goto :goto_0

    .line 42
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 43
    .line 44
    :goto_0
    if-nez v0, :cond_2

    .line 45
    .line 46
    const-string v0, "autoScaleBeforeEnhance trimSize is null"

    .line 47
    .line 48
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    return-void

    .line 52
    :cond_2
    aget v4, v0, v3

    .line 53
    .line 54
    aget v0, v0, v2

    .line 55
    .line 56
    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    iget v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->trimImageMaxSide:I

    .line 61
    .line 62
    if-gt v0, v2, :cond_3

    .line 63
    .line 64
    const-string v0, "autoScaleBeforeEnhance maxSide <= trimImageMaxSide"

    .line 65
    .line 66
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    return-void

    .line 70
    :cond_3
    const/high16 v1, 0x3f800000    # 1.0f

    .line 71
    .line 72
    int-to-float v2, v2

    .line 73
    mul-float v2, v2, v1

    .line 74
    .line 75
    int-to-float v0, v0

    .line 76
    div-float/2addr v2, v0

    .line 77
    iput v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->autoScaleForEnhance:F

    .line 78
    .line 79
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 80
    .line 81
    .line 82
    move-result-wide v0

    .line 83
    iget v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 84
    .line 85
    iget v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->autoScaleForEnhance:F

    .line 86
    .line 87
    invoke-static {v2, v3, v4}, Lcom/intsig/scanner/ScannerEngine;->rotateAndScaleImageS(IIF)I

    .line 88
    .line 89
    .line 90
    move-result v2

    .line 91
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 92
    .line 93
    .line 94
    move-result-wide v3

    .line 95
    sub-long/2addr v3, v0

    .line 96
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->autoScaleForEnhance:F

    .line 97
    .line 98
    new-instance v1, Ljava/lang/StringBuilder;

    .line 99
    .line 100
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    .line 102
    .line 103
    const-string v5, "autoScaleBeforeEnhance result="

    .line 104
    .line 105
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    const-string v2, " costTime="

    .line 112
    .line 113
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    const-string v2, " autoScaleForEnhance="

    .line 120
    .line 121
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v0

    .line 131
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    return-void
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private final checkAndDetectImageBorder()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->detectImageBorder()V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 10
    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    return-void

    .line 14
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 17
    .line 18
    .line 19
    const/4 v2, 0x0

    .line 20
    aget v1, v1, v2

    .line 21
    .line 22
    iget-object v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 23
    .line 24
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 25
    .line 26
    .line 27
    const/4 v4, 0x1

    .line 28
    aget v3, v3, v4

    .line 29
    .line 30
    invoke-static {v0, v1, v3}, Lcom/intsig/scanner/ScannerEngine;->isValidRect([III)I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    iget v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mThreadContext:I

    .line 35
    .line 36
    iget-object v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 37
    .line 38
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 39
    .line 40
    .line 41
    aget v3, v3, v2

    .line 42
    .line 43
    iget-object v5, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 44
    .line 45
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 46
    .line 47
    .line 48
    aget v5, v5, v4

    .line 49
    .line 50
    iget-object v6, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 51
    .line 52
    invoke-static {v1, v3, v5, v6}, Lcom/intsig/scanner/ScannerEngine;->nativeDewarpImagePlaneForSize(III[I)[I

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    if-lez v0, :cond_2

    .line 57
    .line 58
    if-eqz v1, :cond_2

    .line 59
    .line 60
    aget v0, v1, v2

    .line 61
    .line 62
    aget v3, v1, v4

    .line 63
    .line 64
    new-instance v5, Ljava/lang/StringBuilder;

    .line 65
    .line 66
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    const-string v6, "Trim image: size[0] = "

    .line 70
    .line 71
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    const-string v0, " size[1] = "

    .line 78
    .line 79
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    aget v0, v1, v2

    .line 93
    .line 94
    aget v1, v1, v4

    .line 95
    .line 96
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    .line 97
    .line 98
    .line 99
    move-result v0

    .line 100
    const/16 v1, 0x1d4c

    .line 101
    .line 102
    if-le v0, v1, :cond_2

    .line 103
    .line 104
    const/4 v0, 0x0

    .line 105
    iput-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 106
    .line 107
    :cond_2
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private final classifyImage()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil;->Companion:Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil$Companion;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->classificationCallback:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;

    .line 6
    .line 7
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil$Companion;->tryClassify(ILcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final deBlurImage()V
    .locals 6

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needDeBlurImage:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 6
    .line 7
    new-instance v1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v2, "deBlurImage NO NEED mImageStruct="

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void

    .line 28
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 29
    .line 30
    .line 31
    move-result-wide v0

    .line 32
    iget v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 33
    .line 34
    invoke-static {v2}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getImagePtr(I)J

    .line 35
    .line 36
    .line 37
    move-result-wide v2

    .line 38
    invoke-static {v2, v3}, Lcom/intsig/camscanner/scanner/ScannerUtils;->deBlurImagePtr(J)Z

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    iget v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 43
    .line 44
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 45
    .line 46
    .line 47
    move-result-wide v4

    .line 48
    sub-long/2addr v4, v0

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    .line 50
    .line 51
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    .line 53
    .line 54
    const-string v1, "deBlurImage mImageStruct="

    .line 55
    .line 56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    const-string v1, "; res="

    .line 63
    .line 64
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    const-string v1, " costTime="

    .line 71
    .line 72
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    return-void
    .line 86
.end method

.method private final deMoireImage()V
    .locals 12

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needAutoDeMoire:Z

    .line 2
    .line 3
    if-eqz v0, :cond_9

    .line 4
    .line 5
    iget-boolean v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->forbidDeMoire:Z

    .line 6
    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    goto/16 :goto_1

    .line 10
    .line 11
    :cond_0
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    const-string v0, "TRY deMoireImage BUT NO network"

    .line 24
    .line 25
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logE(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void

    .line 29
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEnableCloudServiceWithServerCfg:Z

    .line 30
    .line 31
    if-nez v0, :cond_2

    .line 32
    .line 33
    const-string v0, "deMoireImage not enableCloudService"

    .line 34
    .line 35
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logE(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    return-void

    .line 39
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageQualityHelper:Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 40
    .line 41
    sget-object v1, Lcom/intsig/camscanner/demoire/DeMoireManager;->〇080:Lcom/intsig/camscanner/demoire/DeMoireManager;

    .line 42
    .line 43
    iget v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 44
    .line 45
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/demoire/DeMoireManager;->〇O〇(I)I

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->〇00〇8(Ljava/lang/Integer;)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageQualityHelper:Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 57
    .line 58
    invoke-virtual {v0}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->O8ooOoo〇()Z

    .line 59
    .line 60
    .line 61
    move-result v0

    .line 62
    if-nez v0, :cond_3

    .line 63
    .line 64
    const-string v0, "deMoireImage is healthy"

    .line 65
    .line 66
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    return-void

    .line 70
    :cond_3
    const-string v0, "deMoireImage"

    .line 71
    .line 72
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {v1}, Lcom/intsig/camscanner/demoire/DeMoireManager;->o〇〇0〇()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v0

    .line 79
    iget v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 80
    .line 81
    const/4 v3, 0x0

    .line 82
    invoke-static {v2, v0, v3}, Lcom/intsig/camscanner/scanner/ScannerUtils;->encodeImageSWithFlexibleQuality(ILjava/lang/String;Z)I

    .line 83
    .line 84
    .line 85
    move-result v2

    .line 86
    if-gez v2, :cond_4

    .line 87
    .line 88
    new-instance v1, Ljava/lang/StringBuilder;

    .line 89
    .line 90
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    .line 92
    .line 93
    const-string v2, "encodeFailed - deleteTmpFile tmpPath:"

    .line 94
    .line 95
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 109
    .line 110
    .line 111
    return-void

    .line 112
    :cond_4
    invoke-virtual {v1}, Lcom/intsig/camscanner/demoire/DeMoireManager;->o〇O8〇〇o()Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v11

    .line 116
    iget-object v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->eeMoireWithTrimmedBackBack:Lcom/intsig/camscanner/imagescanner/DeMoireWithTrimmedBackBack;

    .line 117
    .line 118
    if-eqz v2, :cond_5

    .line 119
    .line 120
    const/4 v3, 0x0

    .line 121
    iget-object v5, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageQualityHelper:Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 122
    .line 123
    const/4 v6, 0x1

    .line 124
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 125
    .line 126
    .line 127
    move-result-wide v7

    .line 128
    invoke-static {v11}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 129
    .line 130
    .line 131
    const/16 v10, 0x1770

    .line 132
    .line 133
    move-object v4, v0

    .line 134
    move-object v9, v11

    .line 135
    invoke-interface/range {v2 .. v10}, Lcom/intsig/camscanner/imagescanner/DeMoireWithTrimmedBackBack;->requestDeMoireWithTrimmedPath(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/demoire/ImageQualityHelper;ZJLjava/lang/String;I)Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v2

    .line 139
    if-nez v2, :cond_6

    .line 140
    .line 141
    :cond_5
    const/4 v2, 0x0

    .line 142
    iget-object v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageQualityHelper:Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 143
    .line 144
    const/4 v5, 0x1

    .line 145
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 146
    .line 147
    .line 148
    move-result-wide v6

    .line 149
    invoke-static {v11}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 150
    .line 151
    .line 152
    const/16 v9, 0x1770

    .line 153
    .line 154
    move-object v3, v0

    .line 155
    move-object v8, v11

    .line 156
    invoke-virtual/range {v1 .. v9}, Lcom/intsig/camscanner/demoire/DeMoireManager;->〇〇〇0〇〇0(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/demoire/ImageQualityHelper;ZJLjava/lang/String;I)Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object v2

    .line 160
    :cond_6
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 161
    .line 162
    .line 163
    move-result v1

    .line 164
    if-eqz v1, :cond_7

    .line 165
    .line 166
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/util/ImageProgressClient;->reDecodeImage(Ljava/lang/String;)V

    .line 167
    .line 168
    .line 169
    iget v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 170
    .line 171
    int-to-long v1, v1

    .line 172
    invoke-static {v1, v2}, Lcom/intsig/camscanner/scanner/ScannerUtils;->isLegalImageStruct(J)Z

    .line 173
    .line 174
    .line 175
    move-result v1

    .line 176
    if-eqz v1, :cond_8

    .line 177
    .line 178
    const/4 v1, 0x1

    .line 179
    iput-boolean v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mDeMoireSuccessful:Z

    .line 180
    .line 181
    goto :goto_0

    .line 182
    :cond_7
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->reDecodeImage(Ljava/lang/String;)V

    .line 183
    .line 184
    .line 185
    new-instance v1, Ljava/lang/StringBuilder;

    .line 186
    .line 187
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    .line 189
    .line 190
    const-string v3, "deMoireDecodeImage - tmpResPath="

    .line 191
    .line 192
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    const-string v2, ", tmpPath="

    .line 199
    .line 200
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    .line 202
    .line 203
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    .line 205
    .line 206
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 207
    .line 208
    .line 209
    move-result-object v1

    .line 210
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->logE(Ljava/lang/String;)V

    .line 211
    .line 212
    .line 213
    :cond_8
    :goto_0
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 214
    .line 215
    .line 216
    return-void

    .line 217
    :cond_9
    :goto_1
    iget-boolean v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->forbidDeMoire:Z

    .line 218
    .line 219
    new-instance v2, Ljava/lang/StringBuilder;

    .line 220
    .line 221
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 222
    .line 223
    .line 224
    const-string v3, "deMoireImage NO NEED AutoDeMoire need="

    .line 225
    .line 226
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    .line 228
    .line 229
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 230
    .line 231
    .line 232
    const-string v0, ", forbidDeMoire="

    .line 233
    .line 234
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    .line 236
    .line 237
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 238
    .line 239
    .line 240
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 241
    .line 242
    .line 243
    move-result-object v0

    .line 244
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 245
    .line 246
    .line 247
    return-void
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private final decodeImage()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageProgressListener:Lcom/intsig/camscanner/image_progress/ImageProgressListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/image_progress/ImageProgressListener;->startDecodeImage()V

    .line 6
    .line 7
    .line 8
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 9
    .line 10
    .line 11
    move-result-wide v0

    .line 12
    iget-object v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSrcImagePath:Ljava/lang/String;

    .line 13
    .line 14
    invoke-static {v2}, Lcom/intsig/camscanner/scanner/ScannerUtils;->decodeImageS(Ljava/lang/String;)I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    iput v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 19
    .line 20
    iget-object v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSrcImagePath:Ljava/lang/String;

    .line 21
    .line 22
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 23
    .line 24
    .line 25
    move-result-wide v4

    .line 26
    sub-long/2addr v4, v0

    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    .line 28
    .line 29
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 30
    .line 31
    .line 32
    const-string v1, "decodeImage mImageStruct="

    .line 33
    .line 34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    const-string v1, "; mSrcImagePath="

    .line 41
    .line 42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    const-string v1, " costTime="

    .line 49
    .line 50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private final detectImageBorder()V
    .locals 6

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needDetectBorder:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/16 v0, 0x8

    .line 7
    .line 8
    new-array v0, v0, [I

    .line 9
    .line 10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 11
    .line 12
    .line 13
    move-result-wide v1

    .line 14
    iget v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 15
    .line 16
    invoke-static {v3, v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->detectImageS(I[I)I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 21
    .line 22
    .line 23
    move-result-wide v4

    .line 24
    sub-long/2addr v4, v1

    .line 25
    new-instance v1, Ljava/lang/StringBuilder;

    .line 26
    .line 27
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    .line 29
    .line 30
    const-string v2, "detectImageBorder result="

    .line 31
    .line 32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    const-string v2, " costTime="

    .line 39
    .line 40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    if-gez v3, :cond_1

    .line 54
    .line 55
    return-void

    .line 56
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 57
    .line 58
    invoke-static {v1, v0, v3}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getScanBound([I[II)[I

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    iput-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private final detectRotation()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getImagePtr(I)J

    .line 9
    .line 10
    .line 11
    move-result-wide v2

    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 13
    .line 14
    invoke-static {v2, v3, v0, v1}, Lcom/intsig/camscanner/scanner/DocDirectionUtilKt;->detectDirection(J[IZ)I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 20
    .line 21
    invoke-static {v0, v1}, Lcom/intsig/camscanner/scanner/DocDirectionUtilKt;->getEngineRotationWithoutBorder(IZ)I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    :goto_0
    if-ltz v0, :cond_1

    .line 26
    .line 27
    iget v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->rotation:I

    .line 28
    .line 29
    new-instance v2, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v3, "tempRotation = "

    .line 35
    .line 36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    const-string v1, " -> "

    .line 43
    .line 44
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    iput v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->rotation:I

    .line 58
    .line 59
    :cond_1
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private final enhanceImage()Z
    .locals 15

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, -0x1

    .line 5
    if-ne v0, v2, :cond_0

    .line 6
    .line 7
    const-string v0, "ENHANCE_MODE_NO_ENHANCE"

    .line 8
    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return v1

    .line 13
    :cond_0
    iget-object v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageProgressListener:Lcom/intsig/camscanner/image_progress/ImageProgressListener;

    .line 14
    .line 15
    if-eqz v3, :cond_1

    .line 16
    .line 17
    invoke-interface {v3, v0}, Lcom/intsig/camscanner/image_progress/ImageProgressListener;->startEnhanceImage(I)V

    .line 18
    .line 19
    .line 20
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 21
    .line 22
    .line 23
    move-result-wide v3

    .line 24
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 25
    .line 26
    const/16 v5, -0xc

    .line 27
    .line 28
    const/4 v6, 0x1

    .line 29
    if-ne v0, v5, :cond_2

    .line 30
    .line 31
    const/4 v5, 0x1

    .line 32
    goto :goto_0

    .line 33
    :cond_2
    const/4 v5, 0x0

    .line 34
    :goto_0
    const/16 v7, -0xd

    .line 35
    .line 36
    if-ne v0, v7, :cond_3

    .line 37
    .line 38
    const/4 v7, 0x1

    .line 39
    goto :goto_1

    .line 40
    :cond_3
    const/4 v7, 0x0

    .line 41
    :goto_1
    if-eqz v5, :cond_7

    .line 42
    .line 43
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->useServerSuperFilterEngine()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceImageBySuperFilter(Z)Z

    .line 48
    .line 49
    .line 50
    move-result v5

    .line 51
    iput-boolean v5, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isEncodeOk:Z

    .line 52
    .line 53
    if-eqz v5, :cond_4

    .line 54
    .line 55
    const/4 v6, 0x0

    .line 56
    goto :goto_2

    .line 57
    :cond_4
    const/4 v6, -0x1

    .line 58
    :goto_2
    if-nez v5, :cond_6

    .line 59
    .line 60
    if-eqz v0, :cond_6

    .line 61
    .line 62
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mAllowLocalSuperFilterEngine:Z

    .line 63
    .line 64
    if-eqz v0, :cond_5

    .line 65
    .line 66
    const-string v0, "enhanceImage superFilter server error, so use local engine"

    .line 67
    .line 68
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->saveSuperFilterByLocal()Z

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    if-eqz v0, :cond_8

    .line 76
    .line 77
    goto :goto_4

    .line 78
    :cond_5
    sget-object v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->Companion:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;

    .line 79
    .line 80
    iget v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverFilterRes:I

    .line 81
    .line 82
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;->handleGlobalErrorToast(I)V

    .line 83
    .line 84
    .line 85
    const-string v0, "enhanceImage - but server error -> use MAGIC RETRY"

    .line 86
    .line 87
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    const/16 v0, -0xb

    .line 91
    .line 92
    iput v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 93
    .line 94
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceImage()Z

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    return v0

    .line 99
    :cond_6
    :goto_3
    move v1, v0

    .line 100
    goto/16 :goto_9

    .line 101
    .line 102
    :cond_7
    if-eqz v7, :cond_9

    .line 103
    .line 104
    const-string v0, "enhanceImage use local engine"

    .line 105
    .line 106
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 107
    .line 108
    .line 109
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->saveSuperFilterByLocal()Z

    .line 110
    .line 111
    .line 112
    move-result v0

    .line 113
    if-eqz v0, :cond_8

    .line 114
    .line 115
    :goto_4
    const/4 v2, 0x0

    .line 116
    :cond_8
    move v6, v2

    .line 117
    goto/16 :goto_9

    .line 118
    .line 119
    :cond_9
    const/16 v7, 0x8

    .line 120
    .line 121
    if-eq v0, v7, :cond_f

    .line 122
    .line 123
    const/16 v7, 0x9

    .line 124
    .line 125
    if-ne v0, v7, :cond_a

    .line 126
    .line 127
    goto/16 :goto_7

    .line 128
    .line 129
    :cond_a
    const/16 v7, 0x6e

    .line 130
    .line 131
    if-ne v0, v7, :cond_c

    .line 132
    .line 133
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 134
    .line 135
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 136
    .line 137
    .line 138
    move-result v0

    .line 139
    if-nez v0, :cond_b

    .line 140
    .line 141
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 142
    .line 143
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 144
    .line 145
    .line 146
    move-result v0

    .line 147
    if-nez v0, :cond_b

    .line 148
    .line 149
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 150
    .line 151
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 152
    .line 153
    .line 154
    iget-object v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 155
    .line 156
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 157
    .line 158
    .line 159
    invoke-static {v0, v2}, Lcom/intsig/camscanner/capture/writeboard/PadLocalModelEnhance;->〇〇888(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 160
    .line 161
    .line 162
    invoke-static {}, Lcom/intsig/camscanner/capture/writeboard/PadLocalModelEnhance;->Oo08()V

    .line 163
    .line 164
    .line 165
    const/4 v0, 0x1

    .line 166
    goto :goto_5

    .line 167
    :cond_b
    const/4 v0, 0x0

    .line 168
    const/4 v1, -0x1

    .line 169
    :goto_5
    iput-boolean v6, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isEncodeOk:Z

    .line 170
    .line 171
    move v6, v1

    .line 172
    goto :goto_3

    .line 173
    :cond_c
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->isPadModel()Z

    .line 174
    .line 175
    .line 176
    move-result v0

    .line 177
    if-eqz v0, :cond_e

    .line 178
    .line 179
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mPadPath:Ljava/lang/String;

    .line 180
    .line 181
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 182
    .line 183
    .line 184
    move-result v0

    .line 185
    if-nez v0, :cond_d

    .line 186
    .line 187
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 188
    .line 189
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 190
    .line 191
    .line 192
    move-result v0

    .line 193
    if-nez v0, :cond_d

    .line 194
    .line 195
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 196
    .line 197
    iget-object v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mPadPath:Ljava/lang/String;

    .line 198
    .line 199
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 200
    .line 201
    .line 202
    iget-object v5, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 203
    .line 204
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 205
    .line 206
    .line 207
    invoke-static {v0, v2, v5}, Lcom/intsig/camscanner/capture/writeboard/PadLocalModelEnhance;->o〇O8〇〇o(ILjava/lang/String;Ljava/lang/String;)V

    .line 208
    .line 209
    .line 210
    invoke-static {}, Lcom/intsig/camscanner/capture/writeboard/PadLocalModelEnhance;->Oo08()V

    .line 211
    .line 212
    .line 213
    goto :goto_6

    .line 214
    :cond_d
    const/4 v1, -0x1

    .line 215
    const/4 v6, 0x0

    .line 216
    :goto_6
    move v14, v6

    .line 217
    move v6, v1

    .line 218
    move v1, v14

    .line 219
    goto :goto_9

    .line 220
    :cond_e
    iget v7, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mThreadContext:I

    .line 221
    .line 222
    iget v8, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 223
    .line 224
    iget v9, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 225
    .line 226
    const/4 v10, 0x1

    .line 227
    iget-boolean v11, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->usingNewDeShadow:Z

    .line 228
    .line 229
    iget-boolean v12, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mNeedSmallEnhanceMode:Z

    .line 230
    .line 231
    iget v13, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEngineUseCpuCount:I

    .line 232
    .line 233
    invoke-static/range {v7 .. v13}, Lcom/intsig/camscanner/scanner/ScannerUtils;->enhanceImageSMultiProcess(IIIIZZI)I

    .line 234
    .line 235
    .line 236
    move-result v6

    .line 237
    move v1, v5

    .line 238
    goto :goto_9

    .line 239
    :cond_f
    :goto_7
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceImageByServer(I)Z

    .line 240
    .line 241
    .line 242
    move-result v0

    .line 243
    iput-boolean v6, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isEncodeOk:Z

    .line 244
    .line 245
    if-eqz v0, :cond_10

    .line 246
    .line 247
    goto :goto_8

    .line 248
    :cond_10
    const/4 v1, -0x1

    .line 249
    :goto_8
    move v6, v1

    .line 250
    const/4 v1, 0x1

    .line 251
    :goto_9
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->usingNewDeShadow:Z

    .line 252
    .line 253
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 254
    .line 255
    .line 256
    move-result-wide v7

    .line 257
    sub-long/2addr v7, v3

    .line 258
    iget v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 259
    .line 260
    new-instance v3, Ljava/lang/StringBuilder;

    .line 261
    .line 262
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 263
    .line 264
    .line 265
    const-string v4, "enhanceImageS, usingNewDeShadow="

    .line 266
    .line 267
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    .line 269
    .line 270
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 271
    .line 272
    .line 273
    const-string v0, ", result="

    .line 274
    .line 275
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    .line 277
    .line 278
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 279
    .line 280
    .line 281
    const-string v0, " costTime="

    .line 282
    .line 283
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    .line 285
    .line 286
    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 287
    .line 288
    .line 289
    const-string v0, ", mEnhanceMode="

    .line 290
    .line 291
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292
    .line 293
    .line 294
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 295
    .line 296
    .line 297
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 298
    .line 299
    .line 300
    move-result-object v0

    .line 301
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 302
    .line 303
    .line 304
    return v1
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private final enhanceImageByServer(I)Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 11
    .line 12
    new-instance v0, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v2, "enhanceImageByServer but mSaveOnlyTrimImage="

    .line 18
    .line 19
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    return v1

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 34
    .line 35
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    if-eqz v0, :cond_1

    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 42
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    const-string v2, "enhanceImageByServer but mSaveImagePath="

    .line 49
    .line 50
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    return v1

    .line 64
    :cond_1
    const-string v0, ""

    .line 65
    .line 66
    const/16 v2, 0x8

    .line 67
    .line 68
    const/4 v3, 0x1

    .line 69
    if-eq p1, v2, :cond_6

    .line 70
    .line 71
    const/16 v2, 0x9

    .line 72
    .line 73
    if-eq p1, v2, :cond_4

    .line 74
    .line 75
    iget-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSuperFilterNeedTrace:Z

    .line 76
    .line 77
    if-eqz p1, :cond_2

    .line 78
    .line 79
    const-string p1, "server_super_filter_batch"

    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_2
    const/4 p1, 0x0

    .line 83
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->superFilterImageCallback:Lcom/intsig/camscanner/imagescanner/SuperFilterImageCallback;

    .line 84
    .line 85
    if-eqz v0, :cond_3

    .line 86
    .line 87
    iget-object v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 88
    .line 89
    iget-object v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 90
    .line 91
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 92
    .line 93
    .line 94
    invoke-interface {v0, v2, v4, p1}, Lcom/intsig/camscanner/imagescanner/SuperFilterImageCallback;->saveSuperFilterImage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    .line 96
    .line 97
    move-result p1

    .line 98
    goto :goto_1

    .line 99
    :cond_3
    sget-object v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->Companion:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;

    .line 100
    .line 101
    iget-object v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 102
    .line 103
    iget-object v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 104
    .line 105
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 106
    .line 107
    .line 108
    invoke-virtual {v0, v2, v4, p1}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;->saveSuperFilterImage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    .line 110
    .line 111
    move-result p1

    .line 112
    :goto_1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverFilterRes:I

    .line 113
    .line 114
    goto :goto_4

    .line 115
    :cond_4
    const-string p1, "ENHANCE_REMOVE_HANDWRITE_INDEX"

    .line 116
    .line 117
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 118
    .line 119
    .line 120
    iget-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->cloudFilterImageCallback:Lcom/intsig/camscanner/imagescanner/CloudFilterImageCallback;

    .line 121
    .line 122
    if-eqz p1, :cond_5

    .line 123
    .line 124
    iget-object v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 125
    .line 126
    iget-object v5, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 127
    .line 128
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 129
    .line 130
    .line 131
    invoke-interface {p1, v4, v5, v0, v2}, Lcom/intsig/camscanner/imagescanner/CloudFilterImageCallback;->saveFilterImage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    .line 132
    .line 133
    .line 134
    move-result p1

    .line 135
    goto :goto_2

    .line 136
    :cond_5
    sget-object p1, Lcom/intsig/camscanner/imagescanner/CloudEnhanceUtil;->〇080:Lcom/intsig/camscanner/imagescanner/CloudEnhanceUtil;

    .line 137
    .line 138
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 139
    .line 140
    iget-object v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 141
    .line 142
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 143
    .line 144
    .line 145
    invoke-virtual {p1, v0, v2, v1, v3}, Lcom/intsig/camscanner/imagescanner/CloudEnhanceUtil;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;ZZ)I

    .line 146
    .line 147
    .line 148
    move-result p1

    .line 149
    :goto_2
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverFilterRes:I

    .line 150
    .line 151
    goto :goto_4

    .line 152
    :cond_6
    const-string p1, "ENHANCE_REMOVE_WATER_MARK_INDEX"

    .line 153
    .line 154
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    iget-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->cloudFilterImageCallback:Lcom/intsig/camscanner/imagescanner/CloudFilterImageCallback;

    .line 158
    .line 159
    if-eqz p1, :cond_7

    .line 160
    .line 161
    iget-object v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 162
    .line 163
    iget-object v5, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 164
    .line 165
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 166
    .line 167
    .line 168
    invoke-interface {p1, v4, v5, v0, v2}, Lcom/intsig/camscanner/imagescanner/CloudFilterImageCallback;->saveFilterImage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    .line 169
    .line 170
    .line 171
    move-result p1

    .line 172
    goto :goto_3

    .line 173
    :cond_7
    sget-object p1, Lcom/intsig/camscanner/imagescanner/CloudEnhanceUtil;->〇080:Lcom/intsig/camscanner/imagescanner/CloudEnhanceUtil;

    .line 174
    .line 175
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 176
    .line 177
    iget-object v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 178
    .line 179
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 180
    .line 181
    .line 182
    invoke-virtual {p1, v0, v2, v1, v3}, Lcom/intsig/camscanner/imagescanner/CloudEnhanceUtil;->〇o〇(Ljava/lang/String;Ljava/lang/String;ZZ)I

    .line 183
    .line 184
    .line 185
    move-result p1

    .line 186
    :goto_3
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverFilterRes:I

    .line 187
    .line 188
    :goto_4
    iget-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needClearTrimImageCache:Z

    .line 189
    .line 190
    if-eqz p1, :cond_8

    .line 191
    .line 192
    iget-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 193
    .line 194
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 195
    .line 196
    .line 197
    :cond_8
    iget p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverFilterRes:I

    .line 198
    .line 199
    if-nez p1, :cond_9

    .line 200
    .line 201
    const/4 v1, 0x1

    .line 202
    :cond_9
    return v1
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private final enhanceImageBySuperFilter(Z)Z
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "enhanceImageBySuperFilter: useServerEngine: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    if-eqz p1, :cond_0

    .line 22
    .line 23
    iget p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 24
    .line 25
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceImageByServer(I)Z

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    goto :goto_0

    .line 30
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->saveSuperFilterByLocal()Z

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    :goto_0
    return p1
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static synthetic executeProgress$default(Lcom/intsig/camscanner/util/ImageProgressClient;Ljava/lang/String;Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;Lcom/intsig/camscanner/image_progress/ImageProcessCallback;Lcom/intsig/camscanner/image_progress/ImageProgressListener;ILjava/lang/Object;)V
    .locals 1

    .line 1
    and-int/lit8 p6, p5, 0x1

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    if-eqz p6, :cond_0

    .line 5
    .line 6
    move-object p1, v0

    .line 7
    :cond_0
    and-int/lit8 p6, p5, 0x2

    .line 8
    .line 9
    if-eqz p6, :cond_1

    .line 10
    .line 11
    move-object p2, v0

    .line 12
    :cond_1
    and-int/lit8 p5, p5, 0x4

    .line 13
    .line 14
    if-eqz p5, :cond_2

    .line 15
    .line 16
    move-object p3, v0

    .line 17
    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/util/ImageProgressClient;->executeProgress(Ljava/lang/String;Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;Lcom/intsig/camscanner/image_progress/ImageProcessCallback;Lcom/intsig/camscanner/image_progress/ImageProgressListener;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
.end method

.method private static synthetic getAutoScaleForEnhance$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic getClassificationCallback$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic getCloudFilterImageCallback$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic getEeMoireWithTrimmedBackBack$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic getEraseMaskAllServerCallback$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic getForbidDeMoire$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic getMCallbackWhenFinish$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic getMDeMoireSuccessful$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic getMImageProgressListener$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic getMImageQualityHelper$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic getMImageStruct$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic getMSaveImageQualityForTemp$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic getMThreadContext$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic getNeedClearTrimImageCache$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic getSuperFilterImageCallback$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final isCloudEnhance()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 2
    .line 3
    const/16 v1, 0x9

    .line 4
    .line 5
    if-eq v0, v1, :cond_1

    .line 6
    .line 7
    const/16 v1, 0x8

    .line 8
    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    goto :goto_1

    .line 14
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 15
    :goto_1
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final isPadModel()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mPadPath:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 10
    .line 11
    const/16 v1, 0x7a

    .line 12
    .line 13
    if-eq v0, v1, :cond_0

    .line 14
    .line 15
    const/16 v1, 0x77

    .line 16
    .line 17
    if-eq v0, v1, :cond_0

    .line 18
    .line 19
    const/16 v1, 0x78

    .line 20
    .line 21
    if-eq v0, v1, :cond_0

    .line 22
    .line 23
    const/16 v1, 0x7b

    .line 24
    .line 25
    if-ne v0, v1, :cond_1

    .line 26
    .line 27
    :cond_0
    const/4 v0, 0x1

    .line 28
    goto :goto_0

    .line 29
    :cond_1
    const/4 v0, 0x0

    .line 30
    :goto_0
    return v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private final logD(Ljava/lang/String;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->imageUuid:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "imageUuid="

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v0, ", msg="

    .line 17
    .line 18
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const-string v0, "ImageProgressClient"

    .line 29
    .line 30
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private final logE(Ljava/lang/String;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->imageUuid:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "imageUuid="

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v0, ", msg="

    .line 17
    .line 18
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const-string v0, "ImageProgressClient"

    .line 29
    .line 30
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private final prepareParams()V
    .locals 5

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->isSuperFilterMode(I)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-nez v0, :cond_1

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->isCloudEnhance()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_1

    .line 15
    .line 16
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 17
    .line 18
    const/16 v2, 0x6e

    .line 19
    .line 20
    if-ne v0, v2, :cond_0

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/util/ImageProgressClient;->isPadModel()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_2

    .line 28
    .line 29
    iput-boolean v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->forbidDeMoire:Z

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_1
    :goto_0
    iput-boolean v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->forbidDeMoire:Z

    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 35
    .line 36
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    if-eqz v0, :cond_2

    .line 41
    .line 42
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 47
    .line 48
    .line 49
    move-result-wide v2

    .line 50
    new-instance v4, Ljava/lang/StringBuilder;

    .line 51
    .line 52
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    const-string v0, ".jpg"

    .line 62
    .line 63
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    iput-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 71
    .line 72
    iput-boolean v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needClearTrimImageCache:Z

    .line 73
    .line 74
    :cond_2
    :goto_1
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private final reDecodeImage(Ljava/lang/String;)V
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "reDecodeImage: START. newPath="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "ImageProgressClient"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    invoke-static {p1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->decodeImageS(Ljava/lang/String;)I

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 34
    .line 35
    return-void

    .line 36
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 37
    .line 38
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .line 40
    .line 41
    const-string v2, "reDecodeImage: ERROR newPath="

    .line 42
    .line 43
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    const-string p1, ", is ALL NOT EXIST"

    .line 50
    .line 51
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private final recordWhenFinish()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needAutoDeMoire:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mCallbackWhenFinish:Lcom/intsig/camscanner/image_progress/ImageProcessCallback;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-boolean v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mDeMoireSuccessful:Z

    .line 10
    .line 11
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/image_progress/ImageProcessCallback;->finishCallback(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final releaseStruct()V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->releaseStruct(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final rotateBeforeTrim()V
    .locals 6

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRotationBeforeTrim:I

    .line 2
    .line 3
    if-ltz v0, :cond_1

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x168

    .line 6
    .line 7
    if-nez v1, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v2, "rotateBeforeTrim: START! mRotationBeforeTrim="

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 31
    .line 32
    .line 33
    move-result-wide v0

    .line 34
    iget v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 35
    .line 36
    iget v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRotationBeforeTrim:I

    .line 37
    .line 38
    const/high16 v4, 0x3f800000    # 1.0f

    .line 39
    .line 40
    invoke-static {v2, v3, v4}, Lcom/intsig/scanner/ScannerEngine;->rotateAndScaleImageS(IIF)I

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 45
    .line 46
    .line 47
    move-result-wide v3

    .line 48
    sub-long/2addr v3, v0

    .line 49
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRotationBeforeTrim:I

    .line 50
    .line 51
    new-instance v1, Ljava/lang/StringBuilder;

    .line 52
    .line 53
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .line 55
    .line 56
    const-string v5, "rotateAndScaleImageS result="

    .line 57
    .line 58
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    const-string v2, ", costTime="

    .line 65
    .line 66
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    const-string v2, ", rotationBefore="

    .line 73
    .line 74
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    :cond_1
    :goto_0
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private final rotateImage()V
    .locals 6

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->rotation:I

    .line 2
    .line 3
    rem-int/lit16 v1, v0, 0x168

    .line 4
    .line 5
    if-nez v1, :cond_0

    .line 6
    .line 7
    new-instance v1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v2, "rotateAndScaleImage mRotation="

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageProgressListener:Lcom/intsig/camscanner/image_progress/ImageProgressListener;

    .line 29
    .line 30
    if-eqz v0, :cond_1

    .line 31
    .line 32
    invoke-interface {v0}, Lcom/intsig/camscanner/image_progress/ImageProgressListener;->startRotateAndScaleImage()V

    .line 33
    .line 34
    .line 35
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 36
    .line 37
    .line 38
    move-result-wide v0

    .line 39
    iget v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 40
    .line 41
    iget v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->rotation:I

    .line 42
    .line 43
    const/high16 v4, 0x3f800000    # 1.0f

    .line 44
    .line 45
    invoke-static {v2, v3, v4}, Lcom/intsig/scanner/ScannerEngine;->rotateAndScaleImageS(IIF)I

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 50
    .line 51
    .line 52
    move-result-wide v3

    .line 53
    sub-long/2addr v3, v0

    .line 54
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->rotation:I

    .line 55
    .line 56
    new-instance v1, Ljava/lang/StringBuilder;

    .line 57
    .line 58
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    .line 60
    .line 61
    const-string v5, "rotateAndScaleImageS result="

    .line 62
    .line 63
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    const-string v2, " costTime="

    .line 70
    .line 71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    const-string v2, " rotation="

    .line 78
    .line 79
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private final saveImage()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageProgressListener:Lcom/intsig/camscanner/image_progress/ImageProgressListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/image_progress/ImageProgressListener;->startEncodeImage()V

    .line 6
    .line 7
    .line 8
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 9
    .line 10
    .line 11
    move-result-wide v0

    .line 12
    iget v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 13
    .line 14
    iget-object v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 15
    .line 16
    iget-boolean v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->encodeImageSMoz:Z

    .line 17
    .line 18
    invoke-static {v2, v3, v4}, Lcom/intsig/camscanner/scanner/ScannerUtils;->encodeImageSWithFlexibleQuality(ILjava/lang/String;Z)I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-ltz v2, :cond_1

    .line 23
    .line 24
    const/4 v3, 0x1

    .line 25
    goto :goto_0

    .line 26
    :cond_1
    const/4 v3, 0x0

    .line 27
    :goto_0
    iput-boolean v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isEncodeOk:Z

    .line 28
    .line 29
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 30
    .line 31
    .line 32
    move-result-wide v3

    .line 33
    sub-long/2addr v3, v0

    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 35
    .line 36
    new-instance v1, Ljava/lang/StringBuilder;

    .line 37
    .line 38
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .line 40
    .line 41
    const-string v5, "encodeImageS result="

    .line 42
    .line 43
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    const-string v2, " costTime="

    .line 50
    .line 51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    const-string v2, "; mSaveImagePath="

    .line 58
    .line 59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private final saveOnlyTrimImage()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v0, "saveOnlyTrimImage mSaveOnlyTrimImage is empty"

    .line 10
    .line 11
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logE(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 16
    .line 17
    iget-object v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 18
    .line 19
    iget v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImageQualityForTemp:I

    .line 20
    .line 21
    iget-boolean v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->encodeImageSMoz:Z

    .line 22
    .line 23
    const/4 v5, 0x0

    .line 24
    const/4 v6, 0x1

    .line 25
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/scanner/ScannerUtils;->encodeImageS(ILjava/lang/String;IZZZ)I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 30
    .line 31
    new-instance v2, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v3, "saveOnlyTrimImage result="

    .line 37
    .line 38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    const-string v0, ", mSaveOnlyTrimImage="

    .line 45
    .line 46
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private final saveSuperFilterByLocal()Z
    .locals 7

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 2
    .line 3
    int-to-long v0, v0

    .line 4
    invoke-static {v0, v1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->isLegalImageStruct(J)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    const/4 v1, 0x0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 12
    .line 13
    new-instance v2, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    const-string v3, "saveSuperFilterByLocal imageStruct invalid, "

    .line 19
    .line 20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    return v1

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 35
    .line 36
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    if-eqz v0, :cond_1

    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 43
    .line 44
    new-instance v2, Ljava/lang/StringBuilder;

    .line 45
    .line 46
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    .line 48
    .line 49
    const-string v3, "saveSuperFilterByLocal but mSaveImagePath="

    .line 50
    .line 51
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    return v1

    .line 65
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalMulti;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalMulti;

    .line 66
    .line 67
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->prepareForCostLog(Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog;)V

    .line 68
    .line 69
    .line 70
    iget-boolean v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSuperFilterNeedTrace:Z

    .line 71
    .line 72
    if-eqz v2, :cond_2

    .line 73
    .line 74
    const-string v2, "CSSuperFilter"

    .line 75
    .line 76
    const-string v3, "local_super_filter_batch"

    .line 77
    .line 78
    invoke-static {v2, v3}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    :cond_2
    iget v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 82
    .line 83
    invoke-static {v2}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getImagePtr(I)J

    .line 84
    .line 85
    .line 86
    move-result-wide v2

    .line 87
    iget v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->trimResult:I

    .line 88
    .line 89
    const/4 v5, 0x4

    .line 90
    const/4 v6, 0x1

    .line 91
    if-ne v4, v5, :cond_3

    .line 92
    .line 93
    const/4 v4, 0x0

    .line 94
    goto :goto_0

    .line 95
    :cond_3
    const/4 v4, 0x1

    .line 96
    :goto_0
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->logForCostLog(Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog;)V

    .line 97
    .line 98
    .line 99
    new-instance v0, Lcom/intsig/camscanner/util/ImageProgressClient$saveSuperFilterByLocal$result$1;

    .line 100
    .line 101
    invoke-direct {v0, v2, v3, v4, p0}, Lcom/intsig/camscanner/util/ImageProgressClient$saveSuperFilterByLocal$result$1;-><init>(JILcom/intsig/camscanner/util/ImageProgressClient;)V

    .line 102
    .line 103
    .line 104
    const-string v2, "super_filter_issue_count"

    .line 105
    .line 106
    invoke-static {v2, v0}, Lcom/intsig/camscanner/booksplitter/Util/NoResponseRecorder;->oO80(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    check-cast v0, Ljava/lang/Integer;

    .line 111
    .line 112
    if-eqz v0, :cond_4

    .line 113
    .line 114
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 115
    .line 116
    .line 117
    move-result v0

    .line 118
    goto :goto_1

    .line 119
    :cond_4
    const/4 v0, -0x1

    .line 120
    :goto_1
    sget-object v2, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 121
    .line 122
    invoke-virtual {v2}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->oo88o8O()Z

    .line 123
    .line 124
    .line 125
    move-result v3

    .line 126
    if-nez v3, :cond_5

    .line 127
    .line 128
    invoke-virtual {v2}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇oOO8O8()Z

    .line 129
    .line 130
    .line 131
    move-result v3

    .line 132
    if-eqz v3, :cond_6

    .line 133
    .line 134
    :cond_5
    invoke-virtual {v2}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 135
    .line 136
    .line 137
    move-result-object v2

    .line 138
    const-string v3, "\u672c\u5730\u8d85\u7ea7\u6ee4\u955c"

    .line 139
    .line 140
    invoke-static {v2, v3}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    :cond_6
    invoke-static {}, Lcom/intsig/camscanner/util/ProcessUtil;->〇080()Ljava/lang/String;

    .line 144
    .line 145
    .line 146
    move-result-object v2

    .line 147
    iget v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->trimResult:I

    .line 148
    .line 149
    new-instance v4, Ljava/lang/StringBuilder;

    .line 150
    .line 151
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 152
    .line 153
    .line 154
    const-string v5, "saveSuperFilterByLocal result: "

    .line 155
    .line 156
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    .line 158
    .line 159
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 160
    .line 161
    .line 162
    const-string v5, " , processName: "

    .line 163
    .line 164
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    .line 166
    .line 167
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    .line 169
    .line 170
    const-string v2, ", trimResult: "

    .line 171
    .line 172
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    .line 174
    .line 175
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 179
    .line 180
    .line 181
    move-result-object v2

    .line 182
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 183
    .line 184
    .line 185
    if-ltz v0, :cond_7

    .line 186
    .line 187
    goto :goto_2

    .line 188
    :cond_7
    const/4 v6, 0x0

    .line 189
    :goto_2
    if-eqz v6, :cond_8

    .line 190
    .line 191
    goto :goto_3

    .line 192
    :cond_8
    const/4 v1, 0x2

    .line 193
    :goto_3
    iput v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverFilterRes:I

    .line 194
    .line 195
    return v6
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private final saveTrimmedPaper()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mTrimmedPaperPath:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mTrimmedPaperPath:Ljava/lang/String;

    .line 11
    .line 12
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mTrimmedPaperPath:Ljava/lang/String;

    .line 22
    .line 23
    :goto_0
    move-object v2, v0

    .line 24
    iget v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 25
    .line 26
    iget v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImageQualityForTemp:I

    .line 27
    .line 28
    iget-boolean v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->encodeImageSMoz:Z

    .line 29
    .line 30
    const/4 v5, 0x0

    .line 31
    const/4 v6, 0x1

    .line 32
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/scanner/ScannerUtils;->encodeImageS(ILjava/lang/String;IZZZ)I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    new-instance v1, Ljava/lang/StringBuilder;

    .line 37
    .line 38
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .line 40
    .line 41
    const-string v2, "saveTrimmedPaper result="

    .line 42
    .line 43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private final scaleBorder(F[I)[I
    .locals 4

    .line 1
    if-eqz p2, :cond_2

    .line 2
    .line 3
    const/high16 v0, 0x3f800000    # 1.0f

    .line 4
    .line 5
    invoke-static {p1, v0}, Ljava/lang/Float;->compare(FF)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_1

    .line 12
    :cond_0
    array-length v0, p2

    .line 13
    new-array v1, v0, [I

    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    :goto_0
    if-ge v2, v0, :cond_1

    .line 17
    .line 18
    aget v3, p2, v2

    .line 19
    .line 20
    int-to-float v3, v3

    .line 21
    mul-float v3, v3, p1

    .line 22
    .line 23
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 24
    .line 25
    .line 26
    move-result v3

    .line 27
    aput v3, v1, v2

    .line 28
    .line 29
    add-int/lit8 v2, v2, 0x1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    return-object v1

    .line 33
    :cond_2
    :goto_1
    return-object p2
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final trimImage()V
    .locals 13

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const-string v2, "ImageProgressClient"

    .line 5
    .line 6
    if-nez v0, :cond_2

    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needCropWhenNoBorder:Z

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 13
    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    const/4 v3, 0x0

    .line 17
    invoke-static {v0, v3}, Lkotlin/collections/ArraysKt;->〇oOO8O8([II)Ljava/lang/Integer;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    iget-object v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 28
    .line 29
    if-eqz v3, :cond_0

    .line 30
    .line 31
    invoke-static {v3, v1}, Lkotlin/collections/ArraysKt;->〇oOO8O8([II)Ljava/lang/Integer;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    if-eqz v3, :cond_0

    .line 36
    .line 37
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    invoke-static {v0, v3}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getFullBorder(II)[I

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    iput-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    return-void

    .line 49
    :cond_1
    const-string v0, "trimImage border is null "

    .line 50
    .line 51
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    return-void

    .line 55
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageProgressListener:Lcom/intsig/camscanner/image_progress/ImageProgressListener;

    .line 56
    .line 57
    if-eqz v0, :cond_3

    .line 58
    .line 59
    iget-object v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 60
    .line 61
    iget-object v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 62
    .line 63
    invoke-interface {v0, v3, v4}, Lcom/intsig/camscanner/image_progress/ImageProgressListener;->setTrimmedImageBorder([I[I)V

    .line 64
    .line 65
    .line 66
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 67
    .line 68
    iget-object v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 69
    .line 70
    invoke-static {v0, v3}, Lcom/intsig/camscanner/scanner/ScannerUtils;->isNeedTrim([I[I)Z

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    if-nez v0, :cond_4

    .line 75
    .line 76
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needCropWhenNoBorder:Z

    .line 77
    .line 78
    if-nez v0, :cond_4

    .line 79
    .line 80
    const-string v0, "trimImage not need crop "

    .line 81
    .line 82
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    return-void

    .line 86
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 87
    .line 88
    iget-object v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 89
    .line 90
    iget v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->autoScaleForEnhance:F

    .line 91
    .line 92
    const/high16 v4, 0x3f800000    # 1.0f

    .line 93
    .line 94
    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    .line 95
    .line 96
    .line 97
    move-result v3

    .line 98
    if-eqz v3, :cond_6

    .line 99
    .line 100
    iget-object v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 101
    .line 102
    if-eqz v3, :cond_5

    .line 103
    .line 104
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->autoScaleForEnhance:F

    .line 105
    .line 106
    invoke-direct {p0, v0, v3}, Lcom/intsig/camscanner/util/ImageProgressClient;->scaleBorder(F[I)[I

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    :cond_5
    iget-object v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 111
    .line 112
    if-eqz v3, :cond_6

    .line 113
    .line 114
    iget v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->autoScaleForEnhance:F

    .line 115
    .line 116
    invoke-direct {p0, v2, v3}, Lcom/intsig/camscanner/util/ImageProgressClient;->scaleBorder(F[I)[I

    .line 117
    .line 118
    .line 119
    move-result-object v2

    .line 120
    :cond_6
    invoke-static {v2, v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->overBoundary([I[I)Z

    .line 121
    .line 122
    .line 123
    move-result v3

    .line 124
    if-eqz v3, :cond_7

    .line 125
    .line 126
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getScanBound([I[II)[I

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    :cond_7
    iget-object v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageProgressListener:Lcom/intsig/camscanner/image_progress/ImageProgressListener;

    .line 131
    .line 132
    if-eqz v1, :cond_8

    .line 133
    .line 134
    invoke-interface {v1}, Lcom/intsig/camscanner/image_progress/ImageProgressListener;->startTrimImage()V

    .line 135
    .line 136
    .line 137
    :cond_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 138
    .line 139
    .line 140
    move-result-wide v10

    .line 141
    if-eqz v0, :cond_9

    .line 142
    .line 143
    new-instance v12, Lcom/intsig/camscanner/scanner/ScannerEntities$TrimParam;

    .line 144
    .line 145
    const/4 v3, 0x0

    .line 146
    iget-boolean v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isUsingNewTrimLib:Z

    .line 147
    .line 148
    iget-boolean v5, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->usingDewarpNewModel:Z

    .line 149
    .line 150
    const/4 v6, 0x0

    .line 151
    iget-boolean v7, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needCropDewrap:Z

    .line 152
    .line 153
    iget v8, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEngineUseCpuCount:I

    .line 154
    .line 155
    const/4 v9, 0x1

    .line 156
    move-object v1, v12

    .line 157
    move-object v2, v0

    .line 158
    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scanner/ScannerEntities$TrimParam;-><init>([IZZZZZIZ)V

    .line 159
    .line 160
    .line 161
    iget v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mThreadContext:I

    .line 162
    .line 163
    iget v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 164
    .line 165
    invoke-static {v1, v2, v12}, Lcom/intsig/camscanner/scanner/ScannerUtils;->trimImageSMultiProcess(IILcom/intsig/camscanner/scanner/ScannerEntities$TrimParam;)I

    .line 166
    .line 167
    .line 168
    move-result v1

    .line 169
    iput v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->trimResult:I

    .line 170
    .line 171
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 172
    .line 173
    .line 174
    move-result-wide v2

    .line 175
    sub-long/2addr v2, v10

    .line 176
    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 177
    .line 178
    .line 179
    move-result-object v0

    .line 180
    iget-boolean v4, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needCropDewrap:Z

    .line 181
    .line 182
    new-instance v5, Ljava/lang/StringBuilder;

    .line 183
    .line 184
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 185
    .line 186
    .line 187
    const-string v6, "trimImageS result="

    .line 188
    .line 189
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    .line 191
    .line 192
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    const-string v1, " costTime="

    .line 196
    .line 197
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    .line 199
    .line 200
    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 201
    .line 202
    .line 203
    const-string v1, "; borders="

    .line 204
    .line 205
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    .line 207
    .line 208
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    .line 210
    .line 211
    const-string v0, "; needCropDewrap="

    .line 212
    .line 213
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    .line 215
    .line 216
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 217
    .line 218
    .line 219
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 220
    .line 221
    .line 222
    move-result-object v0

    .line 223
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 224
    .line 225
    .line 226
    :cond_9
    return-void
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private final useServerSuperFilterEngine()Z
    .locals 8

    .line 1
    sget-object v0, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->oo88o8O()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/4 v2, 0x2

    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x1

    .line 10
    if-nez v1, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇oOO8O8()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->Companion:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;->getSuperFilterMode()I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-eq v0, v4, :cond_d

    .line 25
    .line 26
    if-eq v0, v2, :cond_c

    .line 27
    .line 28
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mOnlyUseLocalSuperFilter:Z

    .line 29
    .line 30
    if-eqz v0, :cond_2

    .line 31
    .line 32
    const-string v0, "useServerSuperFilterEngine use local super filter engine"

    .line 33
    .line 34
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    return v3

    .line 38
    :cond_2
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mAllowLocalSuperFilterEngine:Z

    .line 39
    .line 40
    new-instance v1, Ljava/lang/StringBuilder;

    .line 41
    .line 42
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    .line 44
    .line 45
    const-string v5, "useServerSuperFilterEngine allowLocalSuperFilter: "

    .line 46
    .line 47
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mAllowLocalSuperFilterEngine:Z

    .line 61
    .line 62
    if-nez v0, :cond_3

    .line 63
    .line 64
    return v4

    .line 65
    :cond_3
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEnableCloudServiceWithServerCfg:Z

    .line 66
    .line 67
    if-nez v0, :cond_4

    .line 68
    .line 69
    return v3

    .line 70
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageQualityHelper:Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 71
    .line 72
    invoke-virtual {v0}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->o800o8O()I

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    const-string v1, "moire_detected_batch"

    .line 77
    .line 78
    const-string v5, "CSSuperFilter"

    .line 79
    .line 80
    if-ne v0, v2, :cond_6

    .line 81
    .line 82
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSuperFilterNeedTrace:Z

    .line 83
    .line 84
    if-eqz v0, :cond_5

    .line 85
    .line 86
    invoke-static {v5, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    :cond_5
    return v4

    .line 90
    :cond_6
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 91
    .line 92
    iget v6, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEngineUseCpuCount:I

    .line 93
    .line 94
    invoke-static {v0, v6}, Lcom/intsig/camscanner/scanner/ScannerUtils;->detectFinger(II)I

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    new-instance v6, Ljava/lang/StringBuilder;

    .line 99
    .line 100
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    .line 102
    .line 103
    const-string v7, "useServerSuperFilterEngine: withFinger: "

    .line 104
    .line 105
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    const-string v7, ","

    .line 112
    .line 113
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v6

    .line 120
    invoke-direct {p0, v6}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    if-ne v0, v4, :cond_8

    .line 124
    .line 125
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSuperFilterNeedTrace:Z

    .line 126
    .line 127
    if-eqz v0, :cond_7

    .line 128
    .line 129
    const-string v0, "finger_detected_batch"

    .line 130
    .line 131
    invoke-static {v5, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    :cond_7
    return v4

    .line 135
    :cond_8
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageQualityHelper:Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 136
    .line 137
    invoke-virtual {v0}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->o800o8O()I

    .line 138
    .line 139
    .line 140
    move-result v0

    .line 141
    if-nez v0, :cond_9

    .line 142
    .line 143
    return v3

    .line 144
    :cond_9
    new-instance v0, Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 145
    .line 146
    sget-object v6, Lcom/intsig/camscanner/demoire/DeMoireManager;->〇080:Lcom/intsig/camscanner/demoire/DeMoireManager;

    .line 147
    .line 148
    iget v7, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mImageStruct:I

    .line 149
    .line 150
    invoke-virtual {v6, v7}, Lcom/intsig/camscanner/demoire/DeMoireManager;->Oooo8o0〇(I)I

    .line 151
    .line 152
    .line 153
    move-result v6

    .line 154
    invoke-direct {v0, v6}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;-><init>(I)V

    .line 155
    .line 156
    .line 157
    invoke-virtual {v0}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->o800o8O()I

    .line 158
    .line 159
    .line 160
    move-result v6

    .line 161
    if-ne v6, v2, :cond_a

    .line 162
    .line 163
    iget-boolean v6, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSuperFilterNeedTrace:Z

    .line 164
    .line 165
    if-eqz v6, :cond_a

    .line 166
    .line 167
    invoke-static {v5, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    .line 169
    .line 170
    :cond_a
    invoke-virtual {v0}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->o800o8O()I

    .line 171
    .line 172
    .line 173
    move-result v1

    .line 174
    new-instance v5, Ljava/lang/StringBuilder;

    .line 175
    .line 176
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 177
    .line 178
    .line 179
    const-string v6, "useServerSuperFilterEngine: withMoire: "

    .line 180
    .line 181
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    .line 183
    .line 184
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 185
    .line 186
    .line 187
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 188
    .line 189
    .line 190
    move-result-object v1

    .line 191
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->logD(Ljava/lang/String;)V

    .line 192
    .line 193
    .line 194
    invoke-virtual {v0}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->o800o8O()I

    .line 195
    .line 196
    .line 197
    move-result v0

    .line 198
    if-ne v0, v2, :cond_b

    .line 199
    .line 200
    const/4 v3, 0x1

    .line 201
    :cond_b
    return v3

    .line 202
    :cond_c
    return v4

    .line 203
    :cond_d
    return v3
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final enableTrim(Z)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEnableTrim:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final executeProgress(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "uuid"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0, v0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->executeProgress(Ljava/lang/String;Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;Lcom/intsig/camscanner/image_progress/ImageProcessCallback;Lcom/intsig/camscanner/image_progress/ImageProgressListener;)V

    return-void
.end method

.method public final executeProgress(Ljava/lang/String;Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;Lcom/intsig/camscanner/image_progress/ImageProcessCallback;Lcom/intsig/camscanner/image_progress/ImageProgressListener;)V
    .locals 9

    .line 2
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->threadTimeOutChecker:Lcom/intsig/thread/ThreadTimeOutChecker;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "executeProgress_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v8, Lcom/intsig/camscanner/util/ImageProgressClient$executeProgress$1;

    move-object v2, v8

    move-object v3, p1

    move-object v4, p0

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/util/ImageProgressClient$executeProgress$1;-><init>(Ljava/lang/String;Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;Lcom/intsig/camscanner/image_progress/ImageProcessCallback;Lcom/intsig/camscanner/image_progress/ImageProgressListener;)V

    invoke-virtual {v0, v1, v8}, Lcom/intsig/thread/ThreadTimeOutChecker;->〇o〇(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    return-void
.end method

.method public final getBorder()[I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getEnhanceMode()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMBrightnessIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mBrightnessIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMContrastIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mContrastIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMDetailIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mDetailIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMEnableAutoFindRotation()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEnableAutoFindRotation:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMEnableTrim()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEnableTrim:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMPadPath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mPadPath:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMPageIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mPageIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMRawImageSize()[I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMSaveImagePath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMSaveOnlyTrimImage()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMSrcImagePath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSrcImagePath:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMThreadContext()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mThreadContext:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMTrimmedPaperPath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mTrimmedPaperPath:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getRotation()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->rotation:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getServerFilterRes()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverFilterRes:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getServerHandleImageErrorCode()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverHandleImageErrorCode:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getTrimResult()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->trimResult:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final isDecodeOk()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isDecodeOk:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final isEncodeOk()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isEncodeOk:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final isUsingNewTrimLib()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isUsingNewTrimLib:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final reset()Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const/high16 v0, 0x3f800000    # 1.0f

    .line 2
    .line 3
    iput v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->autoScaleForEnhance:F

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isDecodeOk:Z

    .line 7
    .line 8
    iput-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isEncodeOk:Z

    .line 9
    .line 10
    const/4 v1, -0x1

    .line 11
    iput v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->trimResult:I

    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    iput v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverFilterRes:I

    .line 15
    .line 16
    iput v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverHandleImageErrorCode:I

    .line 17
    .line 18
    iput v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->trimImageMaxSide:I

    .line 19
    .line 20
    const/4 v3, 0x0

    .line 21
    iput-object v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 22
    .line 23
    iput-object v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 24
    .line 25
    iput-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEnableTrim:Z

    .line 26
    .line 27
    iput-object v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 28
    .line 29
    iput v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->rotation:I

    .line 30
    .line 31
    iput v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 32
    .line 33
    const/16 v1, 0x64

    .line 34
    .line 35
    iput v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mDetailIndex:I

    .line 36
    .line 37
    iput v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mContrastIndex:I

    .line 38
    .line 39
    iput v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mBrightnessIndex:I

    .line 40
    .line 41
    invoke-static {}, Lcom/intsig/camscanner/tsapp/Const;->〇080()I

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    iput v1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImageQualityForTemp:I

    .line 46
    .line 47
    iput-object v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->classificationCallback:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;

    .line 48
    .line 49
    iput-boolean v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needDeBlurImage:Z

    .line 50
    .line 51
    iput-boolean v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needCropDewrap:Z

    .line 52
    .line 53
    iput-boolean v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needCropWhenNoBorder:Z

    .line 54
    .line 55
    iput-boolean v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needAutoDeMoire:Z

    .line 56
    .line 57
    iput-boolean v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->onlyNeedTrimmedImage:Z

    .line 58
    .line 59
    iput-boolean v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mDeMoireSuccessful:Z

    .line 60
    .line 61
    iput-object v3, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mCallbackWhenFinish:Lcom/intsig/camscanner/image_progress/ImageProcessCallback;

    .line 62
    .line 63
    iput-boolean v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->forbidDeMoire:Z

    .line 64
    .line 65
    iput-boolean v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needClearTrimImageCache:Z

    .line 66
    .line 67
    iput v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRotationBeforeTrim:I

    .line 68
    .line 69
    iput-boolean v2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mNeedSmallEnhanceMode:Z

    .line 70
    .line 71
    iput-boolean v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needDetectBorder:Z

    .line 72
    .line 73
    const-string v0, "UNKNOWN_UUID"

    .line 74
    .line 75
    iput-object v0, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->imageUuid:Ljava/lang/String;

    .line 76
    .line 77
    return-object p0
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public final setAllowLocalSuperFilter(Z)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mAllowLocalSuperFilterEngine:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setBorder([I)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setBrightness(I)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mBrightnessIndex:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setCloudFilterImageCallback(Lcom/intsig/camscanner/imagescanner/CloudFilterImageCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->cloudFilterImageCallback:Lcom/intsig/camscanner/imagescanner/CloudFilterImageCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setContrast(I)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mContrastIndex:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setDeMoireWithTrimmedBackBack(Lcom/intsig/camscanner/imagescanner/DeMoireWithTrimmedBackBack;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->eeMoireWithTrimmedBackBack:Lcom/intsig/camscanner/imagescanner/DeMoireWithTrimmedBackBack;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setDecodeOk(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isDecodeOk:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setDetail(I)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mDetailIndex:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setEnableAutoFindRotation(Z)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEnableAutoFindRotation:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setEncodeImageSMoz(Z)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->encodeImageSMoz:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setEncodeOk(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isEncodeOk:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setEnhanceMode(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setEraseMaskAllServerCallback(Lcom/intsig/camscanner/imagescanner/EraseMaskAllServerCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->eraseMaskAllServerCallback:Lcom/intsig/camscanner/imagescanner/EraseMaskAllServerCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setImageBorder([I)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setImageEnhanceMode(I)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setMBrightnessIndex(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mBrightnessIndex:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setMContrastIndex(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mContrastIndex:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setMDetailIndex(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mDetailIndex:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setMEnableAutoFindRotation(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEnableAutoFindRotation:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setMEnableTrim(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEnableTrim:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setMPadPath(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mPadPath:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setMPageIndex(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mPageIndex:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setMRawImageSize([I)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setMSaveImagePath(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setMSaveOnlyTrimImage(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setMSrcImagePath(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSrcImagePath:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setMThreadContext(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mThreadContext:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setMTrimmedPaperPath(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mTrimmedPaperPath:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setNeedAutoDeMoire(Z)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needAutoDeMoire:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setNeedCropDewrap(Z)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needCropDewrap:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setNeedCropWhenNoBorder(Z)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needCropWhenNoBorder:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setNeedDeBlurImage(Z)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needDeBlurImage:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setNeedDetectBorder(Z)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needDetectBorder:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setNeedSmallEnhanceMode(Z)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mNeedSmallEnhanceMode:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setOnlyNeedTrimmedImage(Z)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->onlyNeedTrimmedImage:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setPadPath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mPadPath:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setPageIndex(I)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mPageIndex:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setRation(I)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->rotation:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setRawImageSize([I)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setRotation(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->rotation:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setRotationBeforeTrim(I)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRotationBeforeTrim:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setSaveImagePath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setSaveOnlyTrimImage(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setServerFilterRes(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverFilterRes:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setServerHandleImageErrorCode(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverHandleImageErrorCode:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setSrcImagePath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSrcImagePath:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setSuperFilterImageCallback(Lcom/intsig/camscanner/imagescanner/SuperFilterImageCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->superFilterImageCallback:Lcom/intsig/camscanner/imagescanner/SuperFilterImageCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setSuperFilterTrace(Z)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSuperFilterNeedTrace:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setThreadContext(I)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mThreadContext:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setTrimImageMaxSide(I)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->trimImageMaxSide:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setTrimResult(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->trimResult:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setTrimmedPaperPath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mTrimmedPaperPath:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setUsingNewTrimLib(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isUsingNewTrimLib:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p2, "out"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSrcImagePath:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveImagePath:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSaveOnlyTrimImage:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    iget-object p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->border:[I

    .line 22
    .line 23
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 24
    .line 25
    .line 26
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needDetectBorder:Z

    .line 27
    .line 28
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 29
    .line 30
    .line 31
    iget p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->rotation:I

    .line 32
    .line 33
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 34
    .line 35
    .line 36
    iget p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->enhanceMode:I

    .line 37
    .line 38
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 39
    .line 40
    .line 41
    iget p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mDetailIndex:I

    .line 42
    .line 43
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 44
    .line 45
    .line 46
    iget p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mContrastIndex:I

    .line 47
    .line 48
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 49
    .line 50
    .line 51
    iget p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mBrightnessIndex:I

    .line 52
    .line 53
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 54
    .line 55
    .line 56
    iget-object p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRawImageSize:[I

    .line 57
    .line 58
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 59
    .line 60
    .line 61
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEnableTrim:Z

    .line 62
    .line 63
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    .line 65
    .line 66
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEnableAutoFindRotation:Z

    .line 67
    .line 68
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 69
    .line 70
    .line 71
    iget-object p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mTrimmedPaperPath:Ljava/lang/String;

    .line 72
    .line 73
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    iget-object p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mPadPath:Ljava/lang/String;

    .line 77
    .line 78
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->encodeImageSMoz:Z

    .line 82
    .line 83
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 84
    .line 85
    .line 86
    iget p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->trimImageMaxSide:I

    .line 87
    .line 88
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 89
    .line 90
    .line 91
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isDecodeOk:Z

    .line 92
    .line 93
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    .line 95
    .line 96
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isEncodeOk:Z

    .line 97
    .line 98
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 99
    .line 100
    .line 101
    iget p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mPageIndex:I

    .line 102
    .line 103
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    .line 105
    .line 106
    iget p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverFilterRes:I

    .line 107
    .line 108
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    .line 110
    .line 111
    iget p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->serverHandleImageErrorCode:I

    .line 112
    .line 113
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 114
    .line 115
    .line 116
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needDeBlurImage:Z

    .line 117
    .line 118
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 119
    .line 120
    .line 121
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needCropDewrap:Z

    .line 122
    .line 123
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 124
    .line 125
    .line 126
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needAutoDeMoire:Z

    .line 127
    .line 128
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 129
    .line 130
    .line 131
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->onlyNeedTrimmedImage:Z

    .line 132
    .line 133
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 134
    .line 135
    .line 136
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->needCropWhenNoBorder:Z

    .line 137
    .line 138
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 139
    .line 140
    .line 141
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mAllowLocalSuperFilterEngine:Z

    .line 142
    .line 143
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 144
    .line 145
    .line 146
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mOnlyUseLocalSuperFilter:Z

    .line 147
    .line 148
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 149
    .line 150
    .line 151
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEnableCloudServiceWithServerCfg:Z

    .line 152
    .line 153
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 154
    .line 155
    .line 156
    iget p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mEngineUseCpuCount:I

    .line 157
    .line 158
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 159
    .line 160
    .line 161
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mSuperFilterNeedTrace:Z

    .line 162
    .line 163
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 164
    .line 165
    .line 166
    iget p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->trimResult:I

    .line 167
    .line 168
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 169
    .line 170
    .line 171
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->usingNewDeShadow:Z

    .line 172
    .line 173
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 174
    .line 175
    .line 176
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->usingDewarpNewModel:Z

    .line 177
    .line 178
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 179
    .line 180
    .line 181
    iget p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->dewarpPostType:I

    .line 182
    .line 183
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 184
    .line 185
    .line 186
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->isUsingNewTrimLib:Z

    .line 187
    .line 188
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 189
    .line 190
    .line 191
    iget p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mRotationBeforeTrim:I

    .line 192
    .line 193
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 194
    .line 195
    .line 196
    iget-boolean p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->mNeedSmallEnhanceMode:Z

    .line 197
    .line 198
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 199
    .line 200
    .line 201
    iget-object p2, p0, Lcom/intsig/camscanner/util/ImageProgressClient;->imageUuid:Ljava/lang/String;

    .line 202
    .line 203
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 204
    .line 205
    .line 206
    return-void
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

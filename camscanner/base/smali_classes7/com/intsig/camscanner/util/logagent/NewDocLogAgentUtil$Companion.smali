.class public final Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil$Companion;
.super Ljava/lang/Object;
.source "NewDocLogAgentUtil.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil$Companion;-><init>()V

    return-void
.end method

.method private final 〇o00〇〇Oo(Lcom/intsig/camscanner/capture/scene/CaptureSceneData;)Ljava/lang/String;
    .locals 3

    .line 1
    const-string v0, "other"

    .line 2
    .line 3
    if-eqz p1, :cond_2

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->getArchiveDirData()Lcom/intsig/camscanner/capture/scene/AutoArchiveDirData;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    if-eqz p1, :cond_2

    .line 10
    .line 11
    sget-object v1, Lcom/intsig/camscanner/capture/scene/AutoArchiveUtil;->〇080:Lcom/intsig/camscanner/capture/scene/AutoArchiveUtil;

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/scene/AutoArchiveDirData;->getDirSyncId()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/capture/scene/AutoArchiveUtil;->Oo08(Ljava/lang/String;)Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_0

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/scene/AutoArchiveDirData;->getDirSyncId()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    goto :goto_0

    .line 28
    :cond_0
    move-object p1, v0

    .line 29
    :goto_0
    if-nez p1, :cond_1

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_1
    move-object v0, p1

    .line 33
    :cond_2
    :goto_1
    return-object v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public final O8()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil;->〇o〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oo08(ZLcom/intsig/camscanner/datastruct/DocProperty;)V
    .locals 9

    .line 1
    const-string v0, "newdoc"

    .line 2
    .line 3
    if-eqz p1, :cond_10

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocProperty;->〇080()Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object v1, p1

    .line 14
    :goto_0
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil$Companion;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/scene/CaptureSceneData;)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    if-eqz p2, :cond_1

    .line 19
    .line 20
    iget v2, p2, Lcom/intsig/camscanner/datastruct/DocProperty;->〇8o8o〇:I

    .line 21
    .line 22
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    goto :goto_1

    .line 27
    :cond_1
    move-object v2, p1

    .line 28
    :goto_1
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    if-eqz p2, :cond_2

    .line 33
    .line 34
    iget-object v3, p2, Lcom/intsig/camscanner/datastruct/DocProperty;->〇〇808〇:Ljava/lang/String;

    .line 35
    .line 36
    goto :goto_2

    .line 37
    :cond_2
    move-object v3, p1

    .line 38
    :goto_2
    if-nez v3, :cond_3

    .line 39
    .line 40
    const-string v3, "non_preset"

    .line 41
    .line 42
    :cond_3
    const-string v4, "preset"

    .line 43
    .line 44
    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    if-eqz v4, :cond_4

    .line 49
    .line 50
    const-string v4, "server_folder"

    .line 51
    .line 52
    goto :goto_3

    .line 53
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil$Companion;->〇o〇()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v4

    .line 57
    :goto_3
    if-eqz p2, :cond_5

    .line 58
    .line 59
    iget-object v5, p2, Lcom/intsig/camscanner/datastruct/DocProperty;->〇O〇:Ljava/lang/String;

    .line 60
    .line 61
    goto :goto_4

    .line 62
    :cond_5
    move-object v5, p1

    .line 63
    :goto_4
    if-nez v5, :cond_6

    .line 64
    .line 65
    const-string v5, ""

    .line 66
    .line 67
    :cond_6
    invoke-static {v5}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇8o8o〇(Ljava/lang/String;)Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v5

    .line 71
    if-nez v5, :cond_7

    .line 72
    .line 73
    const-string v5, "others"

    .line 74
    .line 75
    :cond_7
    invoke-static {}, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil;->〇080()Ljava/util/Set;

    .line 76
    .line 77
    .line 78
    move-result-object v6

    .line 79
    check-cast v6, Ljava/lang/Iterable;

    .line 80
    .line 81
    invoke-static {v6, v4}, Lkotlin/collections/CollectionsKt;->o8(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    .line 82
    .line 83
    .line 84
    move-result v6

    .line 85
    if-eqz v6, :cond_8

    .line 86
    .line 87
    sget-object v6, Lcom/intsig/camscanner/util/DefaultAppUtils;->〇080:Lcom/intsig/camscanner/util/DefaultAppUtils;

    .line 88
    .line 89
    invoke-virtual {v6}, Lcom/intsig/camscanner/util/DefaultAppUtils;->Oo08()Z

    .line 90
    .line 91
    .line 92
    move-result v6

    .line 93
    if-eqz v6, :cond_8

    .line 94
    .line 95
    const-string v6, "pdf"

    .line 96
    .line 97
    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 98
    .line 99
    .line 100
    move-result v6

    .line 101
    if-eqz v6, :cond_8

    .line 102
    .line 103
    const-string v4, "default_open_pdf"

    .line 104
    .line 105
    :cond_8
    const/4 v6, 0x1

    .line 106
    const/4 v7, 0x0

    .line 107
    if-eqz p2, :cond_9

    .line 108
    .line 109
    iget-object v8, p2, Lcom/intsig/camscanner/datastruct/DocProperty;->O8:Ljava/lang/String;

    .line 110
    .line 111
    if-eqz v8, :cond_9

    .line 112
    .line 113
    invoke-static {v8}, Lcom/intsig/camscanner/backup/BackUpHelperKt;->〇o00〇〇Oo(Ljava/lang/String;)Z

    .line 114
    .line 115
    .line 116
    move-result v8

    .line 117
    if-ne v8, v6, :cond_9

    .line 118
    .line 119
    const/4 v8, 0x1

    .line 120
    goto :goto_5

    .line 121
    :cond_9
    const/4 v8, 0x0

    .line 122
    :goto_5
    if-eqz v8, :cond_a

    .line 123
    .line 124
    const-string v4, "document_backup"

    .line 125
    .line 126
    :cond_a
    if-eqz p2, :cond_b

    .line 127
    .line 128
    iget-object p1, p2, Lcom/intsig/camscanner/datastruct/DocProperty;->O8ooOoo〇:Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;

    .line 129
    .line 130
    :cond_b
    new-instance p2, Ljava/lang/StringBuilder;

    .line 131
    .line 132
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 133
    .line 134
    .line 135
    const-string v8, "logAgentNewDoc: reportInfo: "

    .line 136
    .line 137
    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    .line 142
    .line 143
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 144
    .line 145
    .line 146
    move-result-object p2

    .line 147
    const-string v8, "NewDocLogAgentUtil"

    .line 148
    .line 149
    invoke-static {v8, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    .line 151
    .line 152
    new-instance p2, Lorg/json/JSONObject;

    .line 153
    .line 154
    invoke-direct {p2}, Lorg/json/JSONObject;-><init>()V

    .line 155
    .line 156
    .line 157
    const-string v8, "doc_type"

    .line 158
    .line 159
    invoke-virtual {p2, v8, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 160
    .line 161
    .line 162
    move-result-object p2

    .line 163
    const-string v1, "newdoc_type"

    .line 164
    .line 165
    invoke-virtual {p2, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 166
    .line 167
    .line 168
    move-result-object p2

    .line 169
    const-string v1, "is_preset"

    .line 170
    .line 171
    invoke-virtual {p2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 172
    .line 173
    .line 174
    move-result-object p2

    .line 175
    if-eqz v4, :cond_d

    .line 176
    .line 177
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    .line 178
    .line 179
    .line 180
    move-result v1

    .line 181
    if-nez v1, :cond_c

    .line 182
    .line 183
    goto :goto_6

    .line 184
    :cond_c
    const/4 v6, 0x0

    .line 185
    :cond_d
    :goto_6
    if-eqz v6, :cond_e

    .line 186
    .line 187
    const-string v4, "other"

    .line 188
    .line 189
    :cond_e
    const-string v1, "from"

    .line 190
    .line 191
    invoke-virtual {p2, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 192
    .line 193
    .line 194
    move-result-object p2

    .line 195
    const-string v1, "scheme"

    .line 196
    .line 197
    invoke-virtual {p2, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 198
    .line 199
    .line 200
    move-result-object p2

    .line 201
    if-eqz p1, :cond_f

    .line 202
    .line 203
    const-string v1, "link_type"

    .line 204
    .line 205
    invoke-virtual {p1}, Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;->〇080()Ljava/lang/String;

    .line 206
    .line 207
    .line 208
    move-result-object v2

    .line 209
    invoke-virtual {p2, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 210
    .line 211
    .line 212
    const-string v1, "link_flag"

    .line 213
    .line 214
    invoke-virtual {p1}, Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;->〇o〇()Ljava/lang/String;

    .line 215
    .line 216
    .line 217
    move-result-object v2

    .line 218
    invoke-virtual {p2, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 219
    .line 220
    .line 221
    const-string v1, "share_doc_type"

    .line 222
    .line 223
    invoke-virtual {p1}, Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;->〇o00〇〇Oo()Ljava/lang/String;

    .line 224
    .line 225
    .line 226
    move-result-object p1

    .line 227
    invoke-virtual {p2, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 228
    .line 229
    .line 230
    :cond_f
    const-string p1, "CSNewDoc"

    .line 231
    .line 232
    invoke-static {p1, v0, p2}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 233
    .line 234
    .line 235
    goto :goto_7

    .line 236
    :cond_10
    const-string p1, "CSList"

    .line 237
    .line 238
    const-string p2, "0"

    .line 239
    .line 240
    invoke-static {p1, v0, p2}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    .line 242
    .line 243
    :goto_7
    return-void
    .line 244
    .line 245
.end method

.method public final o〇0(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil$Companion;->〇〇888(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    invoke-static {p1}, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil;->O8(Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇080(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/capture/scene/AutoArchiveUtil;->〇080:Lcom/intsig/camscanner/capture/scene/AutoArchiveUtil;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/scene/AutoArchiveUtil;->Oo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const-string v1, "other"

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    if-nez p1, :cond_1

    .line 12
    .line 13
    :cond_0
    move-object p1, v1

    .line 14
    :cond_1
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇o〇()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil;->〇o00〇〇Oo()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-static {v1}, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil;->O8(Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇888(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil;->Oo08(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

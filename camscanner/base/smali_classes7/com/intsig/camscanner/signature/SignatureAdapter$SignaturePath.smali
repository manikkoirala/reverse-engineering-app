.class public Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;
.super Ljava/lang/Object;
.source "SignatureAdapter.java"

# interfaces
.implements Lcom/intsig/camscanner/newsign/main/me/adapter/ESignMePageType;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/signature/SignatureAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SignaturePath"
.end annotation


# instance fields
.field private color:I

.field private mTempSignaturePath:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field private size:Lcom/intsig/camscanner/util/ParcelSize;

.field private strokeSize:I

.field public type:I


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->path:Ljava/lang/String;

    .line 4
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o8〇O〇0O0〇()Z

    move-result p1

    if-eqz p1, :cond_0

    iget p1, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->type:I

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    .line 5
    iput p1, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->color:I

    goto :goto_0

    :cond_0
    const/high16 p1, -0x1000000

    .line 6
    iput p1, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->color:I

    :goto_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object p1, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->path:Ljava/lang/String;

    .line 9
    iput p2, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->color:I

    return-void
.end method

.method public static makeSignaturePathFromParcelTemp(Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;)Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;
    .locals 2
    .param p0    # Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    new-instance v0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;->〇o〇()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setPath(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;->〇o00〇〇Oo()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setTempSignaturePath(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;->〇080()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setColor(I)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;->Oo08()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setStrokeSize(I)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;->O8()Lcom/intsig/camscanner/util/ParcelSize;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setSize(Lcom/intsig/camscanner/util/ParcelSize;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p0}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;->getType()I

    .line 42
    .line 43
    .line 44
    move-result p0

    .line 45
    iput p0, v0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->type:I

    .line 46
    .line 47
    return-object v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->color:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->path:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->strokeSize:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public getColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->color:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->path:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSize()Lcom/intsig/camscanner/util/ParcelSize;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->size:Lcom/intsig/camscanner/util/ParcelSize;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStrokeSize()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->strokeSize:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTempSignaturePath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->mTempSignaturePath:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public makeParcelTempSignaturePath()Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;
    .locals 8

    .line 1
    new-instance v7, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->path:Ljava/lang/String;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->mTempSignaturePath:Ljava/lang/String;

    .line 6
    .line 7
    iget v3, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->color:I

    .line 8
    .line 9
    iget v4, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->strokeSize:I

    .line 10
    .line 11
    iget-object v5, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->size:Lcom/intsig/camscanner/util/ParcelSize;

    .line 12
    .line 13
    iget v6, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->type:I

    .line 14
    .line 15
    move-object v0, v7

    .line 16
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;-><init>(Ljava/lang/String;Ljava/lang/String;IILcom/intsig/camscanner/util/ParcelSize;I)V

    .line 17
    .line 18
    .line 19
    return-object v7
    .line 20
    .line 21
.end method

.method public setColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->color:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->path:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSize(Lcom/intsig/camscanner/util/ParcelSize;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->size:Lcom/intsig/camscanner/util/ParcelSize;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setStrokeSize(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->strokeSize:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTempSignaturePath(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->mTempSignaturePath:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "SignaturePath{path=\'"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->path:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const/16 v1, 0x27

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    const-string v1, ", color="

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    iget v1, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->color:I

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string v1, ", size="

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->size:Lcom/intsig/camscanner/util/ParcelSize;

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    const-string v1, ", strokeSize"

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    iget v1, p0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->strokeSize:I

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    const/16 v1, 0x7d

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    return-object v0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

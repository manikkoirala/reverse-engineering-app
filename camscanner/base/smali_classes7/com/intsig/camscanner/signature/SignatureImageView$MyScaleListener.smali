.class public Lcom/intsig/camscanner/signature/SignatureImageView$MyScaleListener;
.super Lcom/intsig/camscanner/multitouch/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "SignatureImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/signature/SignatureImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "MyScaleListener"
.end annotation


# instance fields
.field final synthetic 〇080:Lcom/intsig/camscanner/signature/SignatureImageView;


# direct methods
.method protected constructor <init>(Lcom/intsig/camscanner/signature/SignatureImageView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/signature/SignatureImageView$MyScaleListener;->〇080:Lcom/intsig/camscanner/signature/SignatureImageView;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/multitouch/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public 〇080(Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;)Z
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureImageView$MyScaleListener;->〇080:Lcom/intsig/camscanner/signature/SignatureImageView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;->〇〇888()F

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    mul-float v0, v0, p1

    .line 12
    .line 13
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    const/4 v1, 0x0

    .line 18
    if-eqz p1, :cond_0

    .line 19
    .line 20
    return v1

    .line 21
    :cond_0
    const/high16 p1, 0x3f800000    # 1.0f

    .line 22
    .line 23
    cmpg-float v2, v0, p1

    .line 24
    .line 25
    if-gez v2, :cond_1

    .line 26
    .line 27
    const/high16 v0, 0x3f800000    # 1.0f

    .line 28
    .line 29
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/signature/SignatureImageView$MyScaleListener;->〇080:Lcom/intsig/camscanner/signature/SignatureImageView;

    .line 30
    .line 31
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oo88o8O(F)[F

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureImageView$MyScaleListener;->〇080:Lcom/intsig/camscanner/signature/SignatureImageView;

    .line 36
    .line 37
    invoke-static {v0}, Lcom/intsig/camscanner/signature/SignatureImageView;->OOO〇O0(Lcom/intsig/camscanner/signature/SignatureImageView;)Ljava/util/List;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    const/4 v3, 0x1

    .line 50
    if-eqz v2, :cond_2

    .line 51
    .line 52
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    check-cast v2, Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 57
    .line 58
    aget v4, p1, v1

    .line 59
    .line 60
    iget-object v5, p0, Lcom/intsig/camscanner/signature/SignatureImageView$MyScaleListener;->〇080:Lcom/intsig/camscanner/signature/SignatureImageView;

    .line 61
    .line 62
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    .line 63
    .line 64
    .line 65
    move-result v5

    .line 66
    shr-int/2addr v5, v3

    .line 67
    int-to-float v5, v5

    .line 68
    iget-object v6, p0, Lcom/intsig/camscanner/signature/SignatureImageView$MyScaleListener;->〇080:Lcom/intsig/camscanner/signature/SignatureImageView;

    .line 69
    .line 70
    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    .line 71
    .line 72
    .line 73
    move-result v6

    .line 74
    shr-int/2addr v6, v3

    .line 75
    int-to-float v6, v6

    .line 76
    invoke-interface {v2, v4, v5, v6}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->OoO8(FFF)V

    .line 77
    .line 78
    .line 79
    aget v3, p1, v3

    .line 80
    .line 81
    const/4 v4, 0x2

    .line 82
    aget v4, p1, v4

    .line 83
    .line 84
    invoke-interface {v2, v3, v4}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->〇O〇(FF)V

    .line 85
    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_2
    return v3
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

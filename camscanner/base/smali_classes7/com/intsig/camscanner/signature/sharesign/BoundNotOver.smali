.class public final Lcom/intsig/camscanner/signature/sharesign/BoundNotOver;
.super Ljava/lang/Object;
.source "SignOverBound.kt"

# interfaces
.implements Lcom/intsig/camscanner/signature/sharesign/DragOverBoundStrategy;


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public 〇080(Lcom/intsig/camscanner/signature/SignatureViewInterface;Landroid/graphics/RectF;FF)[F
    .locals 10
    .param p1    # Lcom/intsig/camscanner/signature/SignatureViewInterface;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/RectF;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "focusSignature"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "bitmapBound"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-interface {p1}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->o800o8O()[F

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    const/4 v0, 0x2

    .line 16
    new-array v1, v0, [F

    .line 17
    .line 18
    const/4 v2, 0x6

    .line 19
    const/4 v3, 0x3

    .line 20
    const/4 v4, 0x4

    .line 21
    const/4 v5, 0x1

    .line 22
    const/4 v6, 0x0

    .line 23
    const/4 v7, 0x0

    .line 24
    cmpg-float v8, p3, v7

    .line 25
    .line 26
    if-gez v8, :cond_0

    .line 27
    .line 28
    new-array v8, v4, [Ljava/lang/Float;

    .line 29
    .line 30
    aget v9, p1, v6

    .line 31
    .line 32
    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 33
    .line 34
    .line 35
    move-result-object v9

    .line 36
    aput-object v9, v8, v6

    .line 37
    .line 38
    aget v2, p1, v2

    .line 39
    .line 40
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 41
    .line 42
    .line 43
    move-result-object v2

    .line 44
    aput-object v2, v8, v5

    .line 45
    .line 46
    aget v2, p1, v0

    .line 47
    .line 48
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    aput-object v2, v8, v0

    .line 53
    .line 54
    aget v2, p1, v4

    .line 55
    .line 56
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    aput-object v2, v8, v3

    .line 61
    .line 62
    check-cast v8, [Ljava/lang/Comparable;

    .line 63
    .line 64
    invoke-static {v8}, Lcom/intsig/camscanner/util/ComparableUtil;->〇o00〇〇Oo([Ljava/lang/Comparable;)Ljava/lang/Comparable;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    check-cast v2, Ljava/lang/Number;

    .line 69
    .line 70
    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    .line 71
    .line 72
    .line 73
    move-result v2

    .line 74
    iget v8, p2, Landroid/graphics/RectF;->right:F

    .line 75
    .line 76
    sub-float/2addr v2, v8

    .line 77
    invoke-static {p3, v2}, Ljava/lang/Math;->max(FF)F

    .line 78
    .line 79
    .line 80
    move-result p3

    .line 81
    goto :goto_0

    .line 82
    :cond_0
    new-array v8, v4, [Ljava/lang/Float;

    .line 83
    .line 84
    aget v9, p1, v6

    .line 85
    .line 86
    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 87
    .line 88
    .line 89
    move-result-object v9

    .line 90
    aput-object v9, v8, v6

    .line 91
    .line 92
    aget v2, p1, v2

    .line 93
    .line 94
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 95
    .line 96
    .line 97
    move-result-object v2

    .line 98
    aput-object v2, v8, v5

    .line 99
    .line 100
    aget v2, p1, v0

    .line 101
    .line 102
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 103
    .line 104
    .line 105
    move-result-object v2

    .line 106
    aput-object v2, v8, v0

    .line 107
    .line 108
    aget v2, p1, v4

    .line 109
    .line 110
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 111
    .line 112
    .line 113
    move-result-object v2

    .line 114
    aput-object v2, v8, v3

    .line 115
    .line 116
    check-cast v8, [Ljava/lang/Comparable;

    .line 117
    .line 118
    invoke-static {v8}, Lcom/intsig/camscanner/util/ComparableUtil;->〇080([Ljava/lang/Comparable;)Ljava/lang/Comparable;

    .line 119
    .line 120
    .line 121
    move-result-object v2

    .line 122
    check-cast v2, Ljava/lang/Number;

    .line 123
    .line 124
    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    .line 125
    .line 126
    .line 127
    move-result v2

    .line 128
    iget v8, p2, Landroid/graphics/RectF;->left:F

    .line 129
    .line 130
    sub-float/2addr v2, v8

    .line 131
    invoke-static {p3, v2}, Ljava/lang/Math;->min(FF)F

    .line 132
    .line 133
    .line 134
    move-result p3

    .line 135
    :goto_0
    aput p3, v1, v6

    .line 136
    .line 137
    const/4 p3, 0x5

    .line 138
    const/4 v2, 0x7

    .line 139
    cmpg-float v7, p4, v7

    .line 140
    .line 141
    if-gez v7, :cond_1

    .line 142
    .line 143
    new-array v4, v4, [Ljava/lang/Float;

    .line 144
    .line 145
    aget v7, p1, v5

    .line 146
    .line 147
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 148
    .line 149
    .line 150
    move-result-object v7

    .line 151
    aput-object v7, v4, v6

    .line 152
    .line 153
    aget v2, p1, v2

    .line 154
    .line 155
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 156
    .line 157
    .line 158
    move-result-object v2

    .line 159
    aput-object v2, v4, v5

    .line 160
    .line 161
    aget v2, p1, v3

    .line 162
    .line 163
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 164
    .line 165
    .line 166
    move-result-object v2

    .line 167
    aput-object v2, v4, v0

    .line 168
    .line 169
    aget p1, p1, p3

    .line 170
    .line 171
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 172
    .line 173
    .line 174
    move-result-object p1

    .line 175
    aput-object p1, v4, v3

    .line 176
    .line 177
    check-cast v4, [Ljava/lang/Comparable;

    .line 178
    .line 179
    invoke-static {v4}, Lcom/intsig/camscanner/util/ComparableUtil;->〇o00〇〇Oo([Ljava/lang/Comparable;)Ljava/lang/Comparable;

    .line 180
    .line 181
    .line 182
    move-result-object p1

    .line 183
    check-cast p1, Ljava/lang/Number;

    .line 184
    .line 185
    invoke-virtual {p1}, Ljava/lang/Number;->floatValue()F

    .line 186
    .line 187
    .line 188
    move-result p1

    .line 189
    iget p2, p2, Landroid/graphics/RectF;->bottom:F

    .line 190
    .line 191
    sub-float/2addr p1, p2

    .line 192
    invoke-static {p4, p1}, Ljava/lang/Math;->max(FF)F

    .line 193
    .line 194
    .line 195
    move-result p1

    .line 196
    goto :goto_1

    .line 197
    :cond_1
    new-array v4, v4, [Ljava/lang/Float;

    .line 198
    .line 199
    aget v7, p1, v5

    .line 200
    .line 201
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 202
    .line 203
    .line 204
    move-result-object v7

    .line 205
    aput-object v7, v4, v6

    .line 206
    .line 207
    aget v2, p1, v2

    .line 208
    .line 209
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 210
    .line 211
    .line 212
    move-result-object v2

    .line 213
    aput-object v2, v4, v5

    .line 214
    .line 215
    aget v2, p1, v3

    .line 216
    .line 217
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 218
    .line 219
    .line 220
    move-result-object v2

    .line 221
    aput-object v2, v4, v0

    .line 222
    .line 223
    aget p1, p1, p3

    .line 224
    .line 225
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 226
    .line 227
    .line 228
    move-result-object p1

    .line 229
    aput-object p1, v4, v3

    .line 230
    .line 231
    check-cast v4, [Ljava/lang/Comparable;

    .line 232
    .line 233
    invoke-static {v4}, Lcom/intsig/camscanner/util/ComparableUtil;->〇080([Ljava/lang/Comparable;)Ljava/lang/Comparable;

    .line 234
    .line 235
    .line 236
    move-result-object p1

    .line 237
    check-cast p1, Ljava/lang/Number;

    .line 238
    .line 239
    invoke-virtual {p1}, Ljava/lang/Number;->floatValue()F

    .line 240
    .line 241
    .line 242
    move-result p1

    .line 243
    iget p2, p2, Landroid/graphics/RectF;->top:F

    .line 244
    .line 245
    sub-float/2addr p1, p2

    .line 246
    invoke-static {p4, p1}, Ljava/lang/Math;->min(FF)F

    .line 247
    .line 248
    .line 249
    move-result p1

    .line 250
    :goto_1
    aput p1, v1, v5

    .line 251
    .line 252
    return-object v1
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

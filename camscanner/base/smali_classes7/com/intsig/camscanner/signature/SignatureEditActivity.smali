.class public Lcom/intsig/camscanner/signature/SignatureEditActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "SignatureEditActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/signature/SignatureEditActivity$GenerateSignatureTask;,
        Lcom/intsig/camscanner/signature/SignatureEditActivity$LoadBitmapTask;
    }
.end annotation


# instance fields
.field private O0O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

.field private O88O:F

.field private o8o:I

.field private o8oOOo:Landroid/net/Uri;

.field private oOO〇〇:Lcom/intsig/view/ImageTextButton;

.field private oo8ooo8O:Z

.field private ooo0〇〇O:Lcom/intsig/view/ImageTextButton;

.field private o〇oO:I

.field private 〇08〇o0O:[I

.field private 〇O〇〇O8:Ljava/lang/String;

.field private 〇o0O:F

.field private 〇〇08O:Lcom/intsig/camscanner/view/ImageEditView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->o8o:I

    .line 6
    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->oo8ooo8O:Z

    .line 8
    .line 9
    iput v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->o〇oO:I

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇08〇o0O:[I

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic O0〇(Lcom/intsig/camscanner/signature/SignatureEditActivity;)Lcom/intsig/camscanner/view/ImageEditView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇〇08O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private O880O〇([ILandroid/graphics/Rect;)[F
    .locals 16
    .param p2    # Landroid/graphics/Rect;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move-object/from16 v2, p2

    .line 6
    .line 7
    iget-object v3, v0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O0O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 8
    .line 9
    invoke-virtual {v3}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->Oo08()I

    .line 10
    .line 11
    .line 12
    move-result v3

    .line 13
    int-to-float v3, v3

    .line 14
    iget-object v4, v0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O0O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 15
    .line 16
    invoke-virtual {v4}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇o00〇〇Oo()I

    .line 17
    .line 18
    .line 19
    move-result v4

    .line 20
    int-to-float v4, v4

    .line 21
    float-to-int v5, v4

    .line 22
    const/4 v6, 0x2

    .line 23
    div-int/2addr v5, v6

    .line 24
    float-to-int v7, v3

    .line 25
    div-int/2addr v7, v6

    .line 26
    const/4 v9, 0x6

    .line 27
    const/4 v10, 0x5

    .line 28
    const/4 v11, 0x4

    .line 29
    const/4 v12, 0x3

    .line 30
    const/4 v13, 0x1

    .line 31
    const/4 v14, 0x0

    .line 32
    const/16 v15, 0x8

    .line 33
    .line 34
    if-eqz v2, :cond_0

    .line 35
    .line 36
    if-eqz v1, :cond_0

    .line 37
    .line 38
    array-length v1, v1

    .line 39
    if-lt v1, v6, :cond_0

    .line 40
    .line 41
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->OooO〇()Landroid/graphics/Matrix;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    if-eqz v1, :cond_0

    .line 46
    .line 47
    invoke-virtual {v1, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 48
    .line 49
    .line 50
    new-instance v3, Landroid/graphics/RectF;

    .line 51
    .line 52
    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 53
    .line 54
    .line 55
    new-instance v4, Landroid/graphics/RectF;

    .line 56
    .line 57
    iget v5, v2, Landroid/graphics/Rect;->left:I

    .line 58
    .line 59
    int-to-float v5, v5

    .line 60
    iget v7, v2, Landroid/graphics/Rect;->top:I

    .line 61
    .line 62
    int-to-float v7, v7

    .line 63
    iget v8, v2, Landroid/graphics/Rect;->right:I

    .line 64
    .line 65
    int-to-float v8, v8

    .line 66
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    .line 67
    .line 68
    int-to-float v2, v2

    .line 69
    invoke-direct {v4, v5, v7, v8, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {v1, v3, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 73
    .line 74
    .line 75
    new-instance v1, Landroid/graphics/Rect;

    .line 76
    .line 77
    iget v2, v3, Landroid/graphics/RectF;->left:F

    .line 78
    .line 79
    float-to-int v2, v2

    .line 80
    iget v4, v3, Landroid/graphics/RectF;->top:F

    .line 81
    .line 82
    float-to-int v4, v4

    .line 83
    iget v5, v3, Landroid/graphics/RectF;->right:F

    .line 84
    .line 85
    float-to-int v5, v5

    .line 86
    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    .line 87
    .line 88
    float-to-int v3, v3

    .line 89
    invoke-direct {v1, v2, v4, v5, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 90
    .line 91
    .line 92
    new-array v2, v15, [F

    .line 93
    .line 94
    iget v3, v1, Landroid/graphics/Rect;->left:I

    .line 95
    .line 96
    int-to-float v4, v3

    .line 97
    aput v4, v2, v14

    .line 98
    .line 99
    iget v4, v1, Landroid/graphics/Rect;->top:I

    .line 100
    .line 101
    int-to-float v5, v4

    .line 102
    aput v5, v2, v13

    .line 103
    .line 104
    iget v5, v1, Landroid/graphics/Rect;->right:I

    .line 105
    .line 106
    int-to-float v7, v5

    .line 107
    aput v7, v2, v6

    .line 108
    .line 109
    int-to-float v4, v4

    .line 110
    aput v4, v2, v12

    .line 111
    .line 112
    int-to-float v4, v5

    .line 113
    aput v4, v2, v11

    .line 114
    .line 115
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    .line 116
    .line 117
    int-to-float v4, v1

    .line 118
    aput v4, v2, v10

    .line 119
    .line 120
    int-to-float v3, v3

    .line 121
    aput v3, v2, v9

    .line 122
    .line 123
    int-to-float v1, v1

    .line 124
    const/4 v3, 0x7

    .line 125
    aput v1, v2, v3

    .line 126
    .line 127
    return-object v2

    .line 128
    :cond_0
    iget v1, v0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇o0O:F

    .line 129
    .line 130
    const/4 v2, 0x0

    .line 131
    cmpg-float v1, v1, v2

    .line 132
    .line 133
    if-gtz v1, :cond_3

    .line 134
    .line 135
    iget v1, v0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O88O:F

    .line 136
    .line 137
    cmpg-float v1, v1, v2

    .line 138
    .line 139
    if-gtz v1, :cond_3

    .line 140
    .line 141
    iget-object v1, v0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇〇08O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 142
    .line 143
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 144
    .line 145
    .line 146
    move-result v1

    .line 147
    int-to-float v1, v1

    .line 148
    const/high16 v2, 0x3f800000    # 1.0f

    .line 149
    .line 150
    mul-float v1, v1, v2

    .line 151
    .line 152
    div-float/2addr v1, v3

    .line 153
    iget-object v8, v0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇〇08O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 154
    .line 155
    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    .line 156
    .line 157
    .line 158
    move-result v8

    .line 159
    int-to-float v8, v8

    .line 160
    mul-float v8, v8, v2

    .line 161
    .line 162
    div-float/2addr v8, v4

    .line 163
    invoke-static {v1, v8}, Ljava/lang/Math;->min(FF)F

    .line 164
    .line 165
    .line 166
    move-result v1

    .line 167
    mul-float v2, v1, v3

    .line 168
    .line 169
    mul-float v8, v1, v4

    .line 170
    .line 171
    const/16 v9, 0xf0

    .line 172
    .line 173
    invoke-static {v0, v9}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 174
    .line 175
    .line 176
    move-result v10

    .line 177
    int-to-float v10, v10

    .line 178
    const v11, 0x3f666666    # 0.9f

    .line 179
    .line 180
    .line 181
    cmpl-float v2, v2, v10

    .line 182
    .line 183
    if-lez v2, :cond_1

    .line 184
    .line 185
    invoke-static {v0, v9}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 186
    .line 187
    .line 188
    move-result v2

    .line 189
    int-to-float v2, v2

    .line 190
    div-float/2addr v2, v1

    .line 191
    div-float/2addr v2, v3

    .line 192
    iput v2, v0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇o0O:F

    .line 193
    .line 194
    goto :goto_0

    .line 195
    :cond_1
    iput v11, v0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇o0O:F

    .line 196
    .line 197
    :goto_0
    const/16 v2, 0xb4

    .line 198
    .line 199
    invoke-static {v0, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 200
    .line 201
    .line 202
    move-result v9

    .line 203
    int-to-float v9, v9

    .line 204
    cmpl-float v8, v8, v9

    .line 205
    .line 206
    if-lez v8, :cond_2

    .line 207
    .line 208
    invoke-static {v0, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 209
    .line 210
    .line 211
    move-result v2

    .line 212
    int-to-float v2, v2

    .line 213
    div-float/2addr v2, v1

    .line 214
    div-float/2addr v2, v4

    .line 215
    iput v2, v0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O88O:F

    .line 216
    .line 217
    goto :goto_1

    .line 218
    :cond_2
    iput v11, v0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O88O:F

    .line 219
    .line 220
    :cond_3
    :goto_1
    iget v1, v0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇o0O:F

    .line 221
    .line 222
    mul-float v3, v3, v1

    .line 223
    .line 224
    iget v1, v0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O88O:F

    .line 225
    .line 226
    mul-float v4, v4, v1

    .line 227
    .line 228
    new-array v1, v15, [F

    .line 229
    .line 230
    int-to-float v2, v7

    .line 231
    const/high16 v7, 0x40000000    # 2.0f

    .line 232
    .line 233
    div-float/2addr v3, v7

    .line 234
    sub-float v8, v2, v3

    .line 235
    .line 236
    aput v8, v1, v14

    .line 237
    .line 238
    int-to-float v5, v5

    .line 239
    div-float/2addr v4, v7

    .line 240
    sub-float v7, v5, v4

    .line 241
    .line 242
    aput v7, v1, v13

    .line 243
    .line 244
    add-float/2addr v2, v3

    .line 245
    aput v2, v1, v6

    .line 246
    .line 247
    aput v7, v1, v12

    .line 248
    .line 249
    const/4 v3, 0x4

    .line 250
    aput v2, v1, v3

    .line 251
    .line 252
    add-float/2addr v5, v4

    .line 253
    const/4 v2, 0x5

    .line 254
    aput v5, v1, v2

    .line 255
    .line 256
    const/4 v2, 0x6

    .line 257
    aput v8, v1, v2

    .line 258
    .line 259
    const/4 v2, 0x7

    .line 260
    aput v5, v1, v2

    .line 261
    .line 262
    return-object v1
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private O8O()Ljava/lang/String;
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->o〇〇0〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-nez v1, :cond_1

    .line 10
    .line 11
    new-instance v1, Ljava/io/File;

    .line 12
    .line 13
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    if-nez v2, :cond_0

    .line 21
    .line 22
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 23
    .line 24
    .line 25
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 26
    .line 27
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string v0, "signature_"

    .line 34
    .line 35
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 39
    .line 40
    .line 41
    move-result-wide v2

    .line 42
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    iput-object v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 50
    .line 51
    return-object v0

    .line 52
    :cond_1
    const/4 v0, 0x0

    .line 53
    return-object v0
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic OO0O(Lcom/intsig/camscanner/signature/SignatureEditActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O8O()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private OoO〇OOo8o([I)Z
    .locals 8

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->oo8ooo8O:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->OooO〇()Landroid/graphics/Matrix;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const/4 v2, 0x1

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    array-length v3, p1

    .line 16
    const/4 v4, 0x6

    .line 17
    if-lt v3, v4, :cond_0

    .line 18
    .line 19
    new-instance v3, Landroid/graphics/RectF;

    .line 20
    .line 21
    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 22
    .line 23
    .line 24
    new-instance v4, Landroid/graphics/RectF;

    .line 25
    .line 26
    aget v1, p1, v1

    .line 27
    .line 28
    int-to-float v1, v1

    .line 29
    aget v5, p1, v2

    .line 30
    .line 31
    int-to-float v5, v5

    .line 32
    const/4 v6, 0x2

    .line 33
    aget v6, p1, v6

    .line 34
    .line 35
    int-to-float v6, v6

    .line 36
    const/4 v7, 0x5

    .line 37
    aget p1, p1, v7

    .line 38
    .line 39
    int-to-float p1, p1

    .line 40
    invoke-direct {v4, v1, v5, v6, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 44
    .line 45
    .line 46
    new-instance p1, Landroid/graphics/Rect;

    .line 47
    .line 48
    iget v0, v3, Landroid/graphics/RectF;->left:F

    .line 49
    .line 50
    float-to-int v0, v0

    .line 51
    iget v1, v3, Landroid/graphics/RectF;->top:F

    .line 52
    .line 53
    float-to-int v1, v1

    .line 54
    iget v4, v3, Landroid/graphics/RectF;->right:F

    .line 55
    .line 56
    float-to-int v4, v4

    .line 57
    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    .line 58
    .line 59
    float-to-int v3, v3

    .line 60
    invoke-direct {p1, v0, v1, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 61
    .line 62
    .line 63
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇ooO〇000(Landroid/graphics/Rect;)V

    .line 64
    .line 65
    .line 66
    :cond_0
    return v2

    .line 67
    :cond_1
    return v1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private OooO〇()Landroid/graphics/Matrix;
    .locals 8
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O0O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_2

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇08〇o0O:[I

    .line 7
    .line 8
    if-eqz v0, :cond_2

    .line 9
    .line 10
    array-length v0, v0

    .line 11
    const/4 v2, 0x2

    .line 12
    if-lt v0, v2, :cond_2

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇〇08O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 15
    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O0O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 24
    .line 25
    invoke-virtual {v2}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->Oo08()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    int-to-float v2, v2

    .line 30
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O0O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 31
    .line 32
    invoke-virtual {v3}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇o00〇〇Oo()I

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    int-to-float v3, v3

    .line 37
    iget-object v4, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇08〇o0O:[I

    .line 38
    .line 39
    const/4 v5, 0x0

    .line 40
    aget v5, v4, v5

    .line 41
    .line 42
    if-lez v5, :cond_2

    .line 43
    .line 44
    const/4 v6, 0x1

    .line 45
    aget v4, v4, v6

    .line 46
    .line 47
    if-lez v4, :cond_2

    .line 48
    .line 49
    const/4 v6, 0x0

    .line 50
    cmpg-float v7, v2, v6

    .line 51
    .line 52
    if-lez v7, :cond_2

    .line 53
    .line 54
    cmpg-float v6, v3, v6

    .line 55
    .line 56
    if-lez v6, :cond_2

    .line 57
    .line 58
    if-nez v0, :cond_1

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_1
    int-to-float v0, v5

    .line 62
    const/high16 v1, 0x3f800000    # 1.0f

    .line 63
    .line 64
    mul-float v0, v0, v1

    .line 65
    .line 66
    div-float/2addr v0, v2

    .line 67
    int-to-float v2, v4

    .line 68
    mul-float v2, v2, v1

    .line 69
    .line 70
    div-float/2addr v2, v3

    .line 71
    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    new-instance v1, Landroid/graphics/Matrix;

    .line 76
    .line 77
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 78
    .line 79
    .line 80
    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 81
    .line 82
    .line 83
    :cond_2
    :goto_0
    return-object v1
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic O〇080〇o0(Lcom/intsig/camscanner/signature/SignatureEditActivity;)Lcom/intsig/camscanner/loadimage/RotateBitmap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O0O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/signature/SignatureEditActivity;)Landroid/net/Uri;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->o8oOOo:Landroid/net/Uri;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic O〇〇O80o8(Lcom/intsig/camscanner/signature/SignatureEditActivity;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->o8o:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private o0O0O〇〇〇0()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->OO〇〇o0oO()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSSignCompliance;->〇o〇(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    const v2, 0x7f0d0259

    .line 19
    .line 20
    .line 21
    const/4 v3, 0x0

    .line 22
    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    const v2, 0x7f0a0315

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    check-cast v2, Landroid/widget/CheckBox;

    .line 34
    .line 35
    const v3, 0x7f130014

    .line 36
    .line 37
    .line 38
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 39
    .line 40
    .line 41
    const/4 v3, 0x0

    .line 42
    invoke-virtual {v2, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 43
    .line 44
    .line 45
    const v3, 0x7f0a1960

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 49
    .line 50
    .line 51
    move-result-object v3

    .line 52
    check-cast v3, Landroid/widget/TextView;

    .line 53
    .line 54
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 55
    .line 56
    .line 57
    move-result-object v4

    .line 58
    const v5, 0x7f131e8a

    .line 59
    .line 60
    .line 61
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v4

    .line 65
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    .line 67
    .line 68
    sget-object v4, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEntranceUtil;->〇080:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEntranceUtil;

    .line 69
    .line 70
    invoke-virtual {v4}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEntranceUtil;->O8()Z

    .line 71
    .line 72
    .line 73
    move-result v4

    .line 74
    if-nez v4, :cond_1

    .line 75
    .line 76
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 77
    .line 78
    .line 79
    move-result-object v4

    .line 80
    invoke-virtual {v4}, Lcom/intsig/tsapp/sync/AppConfigJson;->openNewESign()Z

    .line 81
    .line 82
    .line 83
    move-result v4

    .line 84
    if-eqz v4, :cond_2

    .line 85
    .line 86
    :cond_1
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 87
    .line 88
    .line 89
    move-result-object v4

    .line 90
    const v5, 0x7f131234

    .line 91
    .line 92
    .line 93
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v4

    .line 97
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    .line 99
    .line 100
    :cond_2
    new-instance v3, Lcom/intsig/app/AlertDialog$Builder;

    .line 101
    .line 102
    invoke-direct {v3, p0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 103
    .line 104
    .line 105
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 106
    .line 107
    .line 108
    move-result-object v4

    .line 109
    const v5, 0x7f1300a9

    .line 110
    .line 111
    .line 112
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v4

    .line 116
    invoke-virtual {v3, v4}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 117
    .line 118
    .line 119
    move-result-object v3

    .line 120
    invoke-virtual {v3, v1}, Lcom/intsig/app/AlertDialog$Builder;->oO(Landroid/view/View;)Lcom/intsig/app/AlertDialog$Builder;

    .line 121
    .line 122
    .line 123
    move-result-object v1

    .line 124
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 125
    .line 126
    .line 127
    move-result-object v3

    .line 128
    const v4, 0x7f130019

    .line 129
    .line 130
    .line 131
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 132
    .line 133
    .line 134
    move-result-object v3

    .line 135
    new-instance v4, Lcom/intsig/camscanner/signature/SignatureEditActivity$1;

    .line 136
    .line 137
    invoke-direct {v4, p0, v2, v0}, Lcom/intsig/camscanner/signature/SignatureEditActivity$1;-><init>(Lcom/intsig/camscanner/signature/SignatureEditActivity;Landroid/widget/CheckBox;Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {v1, v3, v4}, Lcom/intsig/app/AlertDialog$Builder;->oo〇(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇8()V

    .line 145
    .line 146
    .line 147
    return-void
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method static bridge synthetic o0Oo(Lcom/intsig/camscanner/signature/SignatureEditActivity;[I)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇08〇o0O:[I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic o808o8o08(Lcom/intsig/camscanner/signature/SignatureEditActivity;[ILandroid/graphics/Rect;)[F
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O880O〇([ILandroid/graphics/Rect;)[F

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method static bridge synthetic o〇08oO80o(Lcom/intsig/camscanner/signature/SignatureEditActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇ooO8Ooo〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static o〇o08〇(Landroid/app/Activity;Landroid/net/Uri;FF)Landroid/content/Intent;
    .locals 2

    .line 1
    new-instance v0, Landroid/content/Intent;

    .line 2
    .line 3
    const-class v1, Lcom/intsig/camscanner/signature/SignatureEditActivity;

    .line 4
    .line 5
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 6
    .line 7
    .line 8
    const-string p0, "extra_path"

    .line 9
    .line 10
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 11
    .line 12
    .line 13
    const-string p0, "extra_ratio_width"

    .line 14
    .line 15
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 16
    .line 17
    .line 18
    const-string p0, "extra_ratio_height"

    .line 19
    .line 20
    invoke-virtual {v0, p0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 21
    .line 22
    .line 23
    return-object v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public static startActivityForResult(Landroid/app/Activity;Landroid/net/Uri;FFI)V
    .locals 7

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    .line 1
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->startActivityForResult(Landroid/app/Activity;Landroid/net/Uri;FFIZLandroid/graphics/Rect;)V

    return-void
.end method

.method public static startActivityForResult(Landroid/app/Activity;Landroid/net/Uri;FFII)V
    .locals 0

    .line 7
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->o〇o08〇(Landroid/app/Activity;Landroid/net/Uri;FF)Landroid/content/Intent;

    move-result-object p1

    const-string p2, "extra_signature_filetype"

    .line 8
    invoke-virtual {p1, p2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 9
    invoke-virtual {p0, p1, p4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public static startActivityForResult(Landroid/app/Activity;Landroid/net/Uri;FFIZLandroid/graphics/Rect;)V
    .locals 0
    .param p6    # Landroid/graphics/Rect;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 2
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->o〇o08〇(Landroid/app/Activity;Landroid/net/Uri;FF)Landroid/content/Intent;

    move-result-object p1

    const-string p2, "extra_boolean_only_need_region"

    .line 3
    invoke-virtual {p1, p2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p2, "extra_rect_init_region"

    .line 4
    invoke-virtual {p1, p2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string p2, "extra_from"

    .line 5
    invoke-virtual {p1, p2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 6
    invoke-virtual {p0, p1, p4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private 〇OoO0o0(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O0O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget v1, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->o8o:I

    .line 7
    .line 8
    add-int/2addr v1, p1

    .line 9
    rem-int/lit16 v1, v1, 0x168

    .line 10
    .line 11
    iput v1, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->o8o:I

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->oO80(I)V

    .line 14
    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇〇08O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O0O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 19
    .line 20
    const/4 v1, 0x1

    .line 21
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/view/ImageEditView;->oO(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    .line 22
    .line 23
    .line 24
    return-void
.end method

.method static bridge synthetic 〇oO88o(Lcom/intsig/camscanner/signature/SignatureEditActivity;Lcom/intsig/camscanner/loadimage/RotateBitmap;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O0O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇ooO8Ooo〇()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "extra_path"

    .line 7
    .line 8
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 9
    .line 10
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 11
    .line 12
    .line 13
    const/4 v1, -0x1

    .line 14
    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
.end method

.method private 〇ooO〇000(Landroid/graphics/Rect;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/content/Intent;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    const-string v1, "finish_extra_rect_region"

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 11
    .line 12
    .line 13
    :cond_0
    const/4 p1, -0x1

    .line 14
    invoke-virtual {p0, p1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public OO〇〇o0oO()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "extra_signature_filetype"

    .line 6
    .line 7
    const/4 v2, -0x1

    .line 8
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/ESignLogAgent;->〇〇0o(Ljava/lang/Integer;)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    return-object v0
    .line 21
.end method

.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/app/AppUtil;->〇〇o8(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    const-string v0, "extra_path"

    .line 9
    .line 10
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    check-cast p1, Landroid/net/Uri;

    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->o8oOOo:Landroid/net/Uri;

    .line 17
    .line 18
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    const-string v0, "extra_ratio_width"

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    iput p1, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇o0O:F

    .line 30
    .line 31
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    const-string v0, "extra_ratio_height"

    .line 36
    .line 37
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    iput p1, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O88O:F

    .line 42
    .line 43
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    const-string v0, "extra_boolean_only_need_region"

    .line 48
    .line 49
    const/4 v1, 0x0

    .line 50
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    iput-boolean p1, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->oo8ooo8O:Z

    .line 55
    .line 56
    if-eqz p1, :cond_0

    .line 57
    .line 58
    const p1, 0x7f131a57

    .line 59
    .line 60
    .line 61
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    invoke-virtual {p0, p1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->O08〇(Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    const-string v0, "extra_rect_init_region"

    .line 73
    .line 74
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 75
    .line 76
    .line 77
    move-result-object p1

    .line 78
    check-cast p1, Landroid/graphics/Rect;

    .line 79
    .line 80
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    const-string v2, "extra_from"

    .line 85
    .line 86
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    iput v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->o〇oO:I

    .line 91
    .line 92
    const v0, 0x7f0a073b

    .line 93
    .line 94
    .line 95
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    check-cast v0, Lcom/intsig/camscanner/view/ImageEditView;

    .line 100
    .line 101
    iput-object v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇〇08O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 102
    .line 103
    const v0, 0x7f0a0746

    .line 104
    .line 105
    .line 106
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    check-cast v0, Lcom/intsig/view/ImageTextButton;

    .line 111
    .line 112
    iput-object v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->oOO〇〇:Lcom/intsig/view/ImageTextButton;

    .line 113
    .line 114
    iget-boolean v2, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->oo8ooo8O:Z

    .line 115
    .line 116
    if-eqz v2, :cond_1

    .line 117
    .line 118
    const/16 v2, 0x8

    .line 119
    .line 120
    goto :goto_0

    .line 121
    :cond_1
    const/4 v2, 0x0

    .line 122
    :goto_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 123
    .line 124
    .line 125
    const v0, 0x7f0a0741

    .line 126
    .line 127
    .line 128
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    check-cast v0, Lcom/intsig/view/ImageTextButton;

    .line 133
    .line 134
    iput-object v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->ooo0〇〇O:Lcom/intsig/view/ImageTextButton;

    .line 135
    .line 136
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->oOO〇〇:Lcom/intsig/view/ImageTextButton;

    .line 137
    .line 138
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    .line 140
    .line 141
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->ooo0〇〇O:Lcom/intsig/view/ImageTextButton;

    .line 142
    .line 143
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    .line 145
    .line 146
    new-instance v0, Lcom/intsig/camscanner/signature/SignatureEditActivity$LoadBitmapTask;

    .line 147
    .line 148
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/signature/SignatureEditActivity$LoadBitmapTask;-><init>(Lcom/intsig/camscanner/signature/SignatureEditActivity;Landroid/graphics/Rect;)V

    .line 149
    .line 150
    .line 151
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->〇8o8o〇()Ljava/util/concurrent/ExecutorService;

    .line 152
    .line 153
    .line 154
    move-result-object p1

    .line 155
    new-array v1, v1, [Ljava/lang/Void;

    .line 156
    .line 157
    invoke-virtual {v0, p1, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 158
    .line 159
    .line 160
    return-void
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const v0, 0x7f0a0741

    .line 6
    .line 7
    .line 8
    if-eq p1, v0, :cond_1

    .line 9
    .line 10
    const v0, 0x7f0a0746

    .line 11
    .line 12
    .line 13
    if-eq p1, v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const-string p1, "SignatureEditActivity"

    .line 17
    .line 18
    const-string v0, "User Operation:  onclick image rotate"

    .line 19
    .line 20
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/16 p1, 0x5a

    .line 24
    .line 25
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇OoO0o0(I)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    iget p1, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->o〇oO:I

    .line 30
    .line 31
    const/16 v0, 0x19fa

    .line 32
    .line 33
    if-ne p1, v0, :cond_2

    .line 34
    .line 35
    const-string p1, "CSCountCrop"

    .line 36
    .line 37
    const-string v0, "confirm"

    .line 38
    .line 39
    invoke-static {p1, v0}, Lcom/intsig/log/LogAgentHelper;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->〇〇08O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 43
    .line 44
    const/4 v0, 0x0

    .line 45
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/ImageEditView;->〇oOO8O8(Z)[I

    .line 46
    .line 47
    .line 48
    move-result-object v6

    .line 49
    invoke-direct {p0, v6}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->OoO〇OOo8o([I)Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    if-eqz p1, :cond_3

    .line 54
    .line 55
    return-void

    .line 56
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o88()Z

    .line 57
    .line 58
    .line 59
    move-result p1

    .line 60
    if-eqz p1, :cond_4

    .line 61
    .line 62
    invoke-direct {p0}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->o0O0O〇〇〇0()V

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_4
    new-instance p1, Lcom/intsig/camscanner/signature/SignatureEditActivity$GenerateSignatureTask;

    .line 67
    .line 68
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O0O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 69
    .line 70
    invoke-virtual {v1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 71
    .line 72
    .line 73
    move-result-object v3

    .line 74
    iget v4, p0, Lcom/intsig/camscanner/signature/SignatureEditActivity;->o8o:I

    .line 75
    .line 76
    invoke-direct {p0}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->O8O()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v5

    .line 80
    move-object v1, p1

    .line 81
    move-object v2, p0

    .line 82
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/signature/SignatureEditActivity$GenerateSignatureTask;-><init>(Lcom/intsig/camscanner/signature/SignatureEditActivity;Landroid/graphics/Bitmap;ILjava/lang/String;[I)V

    .line 83
    .line 84
    .line 85
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 86
    .line 87
    .line 88
    move-result-object v1

    .line 89
    new-array v0, v0, [Ljava/lang/Void;

    .line 90
    .line 91
    invoke-virtual {p1, v1, v0}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 92
    .line 93
    .line 94
    :goto_0
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    const v0, 0x7f0d00bc

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

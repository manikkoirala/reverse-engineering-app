.class public Lcom/intsig/camscanner/signature/SignatureView;
.super Ljava/lang/Object;
.source "SignatureView.java"

# interfaces
.implements Lcom/intsig/camscanner/signature/SignatureViewInterface;


# instance fields
.field private final O08000:I

.field private O8:Landroid/graphics/Bitmap;

.field private final O8ooOoo〇:I

.field private final O8〇o:Landroid/graphics/PathEffect;

.field private final OO0o〇〇:J

.field private OO0o〇〇〇〇0:Landroid/graphics/Paint;

.field private final OOO〇O0:Landroid/graphics/Paint;

.field private Oo08:Landroid/graphics/drawable/Drawable;

.field private Oo8Oo00oo:Z

.field private OoO8:[F

.field private Oooo8o0〇:I

.field private final O〇8O8〇008:I

.field private final o0ooO:Landroid/graphics/Paint;

.field private final o8:Lcom/intsig/camscanner/signature/sharesign/DragOverBoundStrategy;

.field private final o800o8O:[F

.field private final oO:Landroid/graphics/RectF;

.field private oO80:Z

.field private oo88o8O:I

.field private oo〇:Z

.field private o〇0:Landroid/graphics/drawable/Drawable;

.field o〇0OOo〇0:[F

.field private final o〇8:Ljava/lang/String;

.field private o〇O8〇〇o:Z

.field private final o〇〇0〇:Landroid/graphics/Path;

.field private final 〇00:I

.field private 〇0000OOO:I

.field private 〇00〇8:F

.field private 〇080:Landroid/view/View;

.field private 〇08O8o〇0:[F

.field private final 〇0〇O0088o:Landroid/graphics/RectF;

.field private final 〇8:I

.field private 〇80〇808〇O:Landroid/graphics/Paint;

.field private final 〇8o8o〇:Landroid/graphics/Matrix;

.field private final 〇8〇0〇o〇O:I

.field private final 〇O00:[F

.field 〇O888o0o:Landroid/graphics/Matrix;

.field private final 〇O8o08O:Landroid/graphics/Matrix;

.field public 〇O〇:[F

.field private 〇o:Z

.field private 〇o00〇〇Oo:Landroid/content/Context;

.field private final 〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

.field private 〇oo〇:Z

.field private 〇o〇:Z

.field 〇〇0o:[F

.field private 〇〇808〇:I

.field private 〇〇888:Landroid/graphics/drawable/Drawable;

.field private 〇〇8O0〇8:[F

.field 〇〇〇0〇〇0:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Landroid/content/Context;JLcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;Ljava/lang/String;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o〇:Z

    .line 6
    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->oO80:Z

    .line 8
    .line 9
    new-instance v1, Landroid/graphics/Matrix;

    .line 10
    .line 11
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇8o8o〇:Landroid/graphics/Matrix;

    .line 15
    .line 16
    new-instance v1, Landroid/graphics/Matrix;

    .line 17
    .line 18
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 19
    .line 20
    .line 21
    iput-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 22
    .line 23
    const/16 v1, 0x8

    .line 24
    .line 25
    new-array v2, v1, [F

    .line 26
    .line 27
    iput-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 28
    .line 29
    new-array v1, v1, [F

    .line 30
    .line 31
    iput-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O00:[F

    .line 32
    .line 33
    const/16 v1, 0x9

    .line 34
    .line 35
    new-array v1, v1, [F

    .line 36
    .line 37
    iput-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇8O0〇8:[F

    .line 38
    .line 39
    new-instance v1, Landroid/graphics/RectF;

    .line 40
    .line 41
    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 42
    .line 43
    .line 44
    iput-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇0〇O0088o:Landroid/graphics/RectF;

    .line 45
    .line 46
    const/4 v1, 0x2

    .line 47
    new-array v2, v1, [F

    .line 48
    .line 49
    iput-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 50
    .line 51
    new-instance v2, Landroid/graphics/Matrix;

    .line 52
    .line 53
    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 54
    .line 55
    .line 56
    iput-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O888o0o:Landroid/graphics/Matrix;

    .line 57
    .line 58
    iput v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->oo88o8O:I

    .line 59
    .line 60
    const/4 v2, 0x1

    .line 61
    iput-boolean v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oo〇:Z

    .line 62
    .line 63
    iput-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇O8〇〇o:Z

    .line 64
    .line 65
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o00〇〇Oo:Landroid/content/Context;

    .line 66
    .line 67
    const/16 v4, 0x18

    .line 68
    .line 69
    invoke-static {v3, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    iput v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00:I

    .line 74
    .line 75
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o00〇〇Oo:Landroid/content/Context;

    .line 76
    .line 77
    invoke-static {v3, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 78
    .line 79
    .line 80
    move-result v3

    .line 81
    iput v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->O〇8O8〇008:I

    .line 82
    .line 83
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o00〇〇Oo:Landroid/content/Context;

    .line 84
    .line 85
    invoke-static {v3, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 86
    .line 87
    .line 88
    move-result v3

    .line 89
    iput v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8ooOoo〇:I

    .line 90
    .line 91
    new-instance v3, Landroid/graphics/Path;

    .line 92
    .line 93
    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 94
    .line 95
    .line 96
    iput-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇〇0〇:Landroid/graphics/Path;

    .line 97
    .line 98
    new-instance v3, Landroid/graphics/Paint;

    .line 99
    .line 100
    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 101
    .line 102
    .line 103
    iput-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->OOO〇O0:Landroid/graphics/Paint;

    .line 104
    .line 105
    iput-boolean v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->oo〇:Z

    .line 106
    .line 107
    iput-boolean v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o:Z

    .line 108
    .line 109
    new-instance v4, Landroid/graphics/Paint;

    .line 110
    .line 111
    invoke-direct {v4, v2}, Landroid/graphics/Paint;-><init>(I)V

    .line 112
    .line 113
    .line 114
    iput-object v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 115
    .line 116
    iput-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oo8Oo00oo:Z

    .line 117
    .line 118
    new-instance v4, Landroid/graphics/Matrix;

    .line 119
    .line 120
    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 121
    .line 122
    .line 123
    iput-object v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇〇0〇〇0:Landroid/graphics/Matrix;

    .line 124
    .line 125
    new-array v4, v1, [F

    .line 126
    .line 127
    iput-object v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇0OOo〇0:[F

    .line 128
    .line 129
    new-array v4, v1, [F

    .line 130
    .line 131
    iput-object v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇0o:[F

    .line 132
    .line 133
    new-instance v4, Landroid/graphics/RectF;

    .line 134
    .line 135
    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 136
    .line 137
    .line 138
    iput-object v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->oO:Landroid/graphics/RectF;

    .line 139
    .line 140
    const/high16 v4, 0x42b00000    # 88.0f

    .line 141
    .line 142
    invoke-static {v4}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 143
    .line 144
    .line 145
    move-result v4

    .line 146
    iput v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇8:I

    .line 147
    .line 148
    const/high16 v4, 0x42300000    # 44.0f

    .line 149
    .line 150
    invoke-static {v4}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 151
    .line 152
    .line 153
    move-result v4

    .line 154
    iput v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->O08000:I

    .line 155
    .line 156
    const/high16 v4, 0x42f00000    # 120.0f

    .line 157
    .line 158
    invoke-static {v4}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 159
    .line 160
    .line 161
    move-result v4

    .line 162
    iput v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇8〇0〇o〇O:I

    .line 163
    .line 164
    iput-object p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o00〇〇Oo:Landroid/content/Context;

    .line 165
    .line 166
    iput-wide p2, p0, Lcom/intsig/camscanner/signature/SignatureView;->OO0o〇〇:J

    .line 167
    .line 168
    iput-object p4, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 169
    .line 170
    iput-object p5, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇8:Ljava/lang/String;

    .line 171
    .line 172
    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 173
    .line 174
    .line 175
    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 176
    .line 177
    invoke-virtual {v3, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 178
    .line 179
    .line 180
    invoke-static {p1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 181
    .line 182
    .line 183
    move-result p2

    .line 184
    int-to-float p2, p2

    .line 185
    invoke-virtual {v3, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 186
    .line 187
    .line 188
    const p2, -0xe64364

    .line 189
    .line 190
    .line 191
    invoke-virtual {v3, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 192
    .line 193
    .line 194
    const/4 p2, 0x3

    .line 195
    invoke-static {p1, p2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 196
    .line 197
    .line 198
    move-result p3

    .line 199
    new-instance p4, Landroid/graphics/DashPathEffect;

    .line 200
    .line 201
    const/4 v4, 0x4

    .line 202
    new-array v4, v4, [F

    .line 203
    .line 204
    int-to-float p3, p3

    .line 205
    aput p3, v4, v0

    .line 206
    .line 207
    aput p3, v4, v2

    .line 208
    .line 209
    aput p3, v4, v1

    .line 210
    .line 211
    aput p3, v4, p2

    .line 212
    .line 213
    invoke-static {p1, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 214
    .line 215
    .line 216
    move-result p1

    .line 217
    int-to-float p1, p1

    .line 218
    invoke-direct {p4, v4, p1}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 219
    .line 220
    .line 221
    iput-object p4, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8〇o:Landroid/graphics/PathEffect;

    .line 222
    .line 223
    invoke-virtual {v3, p4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 224
    .line 225
    .line 226
    const-string p1, "TYPE_SIGNATURE_AREA"

    .line 227
    .line 228
    invoke-virtual {p1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 229
    .line 230
    .line 231
    move-result p1

    .line 232
    if-nez p1, :cond_2

    .line 233
    .line 234
    const-string p1, "TYPE_SIGNATURE_AREA_RECOVER"

    .line 235
    .line 236
    invoke-virtual {p1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 237
    .line 238
    .line 239
    move-result p1

    .line 240
    if-eqz p1, :cond_0

    .line 241
    .line 242
    goto :goto_0

    .line 243
    :cond_0
    const-string p1, "TYPE_SIGNATURE"

    .line 244
    .line 245
    invoke-virtual {p1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 246
    .line 247
    .line 248
    move-result p1

    .line 249
    if-eqz p1, :cond_1

    .line 250
    .line 251
    invoke-static {}, Lcom/intsig/camscanner/signature/scale/SignatureScaleManager;->〇080()Z

    .line 252
    .line 253
    .line 254
    move-result p1

    .line 255
    if-eqz p1, :cond_1

    .line 256
    .line 257
    new-instance p1, Lcom/intsig/camscanner/signature/sharesign/BoundNotOver;

    .line 258
    .line 259
    invoke-direct {p1}, Lcom/intsig/camscanner/signature/sharesign/BoundNotOver;-><init>()V

    .line 260
    .line 261
    .line 262
    iput-object p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o8:Lcom/intsig/camscanner/signature/sharesign/DragOverBoundStrategy;

    .line 263
    .line 264
    goto :goto_1

    .line 265
    :cond_1
    new-instance p1, Lcom/intsig/camscanner/signature/sharesign/CenterNotOver;

    .line 266
    .line 267
    invoke-direct {p1}, Lcom/intsig/camscanner/signature/sharesign/CenterNotOver;-><init>()V

    .line 268
    .line 269
    .line 270
    iput-object p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o8:Lcom/intsig/camscanner/signature/sharesign/DragOverBoundStrategy;

    .line 271
    .line 272
    goto :goto_1

    .line 273
    :cond_2
    :goto_0
    new-instance p1, Lcom/intsig/camscanner/signature/sharesign/BorderNotOver;

    .line 274
    .line 275
    invoke-direct {p1}, Lcom/intsig/camscanner/signature/sharesign/BorderNotOver;-><init>()V

    .line 276
    .line 277
    .line 278
    iput-object p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o8:Lcom/intsig/camscanner/signature/sharesign/DragOverBoundStrategy;

    .line 279
    .line 280
    :goto_1
    return-void
    .line 281
    .line 282
    .line 283
.end method

.method private O8ooOoo〇(Landroid/graphics/Canvas;)V
    .locals 8

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oo8Oo00oo:Z

    .line 2
    .line 3
    const/4 v1, 0x5

    .line 4
    const/4 v2, 0x4

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇〇0〇〇0:Landroid/graphics/Matrix;

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇〇0〇〇0:Landroid/graphics/Matrix;

    .line 13
    .line 14
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 15
    .line 16
    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇〇0〇〇0:Landroid/graphics/Matrix;

    .line 20
    .line 21
    iget v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8:F

    .line 22
    .line 23
    neg-float v3, v3

    .line 24
    iget-object v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 25
    .line 26
    const/4 v5, 0x0

    .line 27
    aget v6, v4, v5

    .line 28
    .line 29
    const/4 v7, 0x1

    .line 30
    aget v4, v4, v7

    .line 31
    .line 32
    invoke-virtual {v0, v3, v6, v4}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇0OOo〇0:[F

    .line 36
    .line 37
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O00:[F

    .line 38
    .line 39
    aget v2, v3, v2

    .line 40
    .line 41
    aput v2, v0, v5

    .line 42
    .line 43
    aget v1, v3, v1

    .line 44
    .line 45
    aput v1, v0, v7

    .line 46
    .line 47
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇〇0〇〇0:Landroid/graphics/Matrix;

    .line 48
    .line 49
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇0o:[F

    .line 50
    .line 51
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 52
    .line 53
    .line 54
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇0o:[F

    .line 55
    .line 56
    aget v1, v0, v5

    .line 57
    .line 58
    float-to-int v2, v1

    .line 59
    iget v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8ooOoo〇:I

    .line 60
    .line 61
    div-int/lit8 v4, v3, 0x2

    .line 62
    .line 63
    sub-int/2addr v2, v4

    .line 64
    aget v0, v0, v7

    .line 65
    .line 66
    float-to-int v4, v0

    .line 67
    div-int/lit8 v6, v3, 0x2

    .line 68
    .line 69
    sub-int/2addr v4, v6

    .line 70
    float-to-int v1, v1

    .line 71
    div-int/lit8 v6, v3, 0x2

    .line 72
    .line 73
    add-int/2addr v1, v6

    .line 74
    float-to-int v0, v0

    .line 75
    div-int/lit8 v3, v3, 0x2

    .line 76
    .line 77
    add-int/2addr v0, v3

    .line 78
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 79
    .line 80
    .line 81
    iget v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8:F

    .line 82
    .line 83
    iget-object v6, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 84
    .line 85
    aget v5, v6, v5

    .line 86
    .line 87
    aget v6, v6, v7

    .line 88
    .line 89
    invoke-virtual {p1, v3, v5, v6}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 90
    .line 91
    .line 92
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇0:Landroid/graphics/drawable/Drawable;

    .line 93
    .line 94
    invoke-virtual {v3, v2, v4, v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 95
    .line 96
    .line 97
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇0:Landroid/graphics/drawable/Drawable;

    .line 98
    .line 99
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 103
    .line 104
    .line 105
    goto :goto_0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇0:Landroid/graphics/drawable/Drawable;

    .line 107
    .line 108
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 109
    .line 110
    aget v2, v3, v2

    .line 111
    .line 112
    float-to-int v4, v2

    .line 113
    iget v5, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8ooOoo〇:I

    .line 114
    .line 115
    div-int/lit8 v6, v5, 0x2

    .line 116
    .line 117
    sub-int/2addr v4, v6

    .line 118
    aget v1, v3, v1

    .line 119
    .line 120
    float-to-int v3, v1

    .line 121
    div-int/lit8 v6, v5, 0x2

    .line 122
    .line 123
    sub-int/2addr v3, v6

    .line 124
    float-to-int v2, v2

    .line 125
    div-int/lit8 v6, v5, 0x2

    .line 126
    .line 127
    add-int/2addr v2, v6

    .line 128
    float-to-int v1, v1

    .line 129
    div-int/lit8 v5, v5, 0x2

    .line 130
    .line 131
    add-int/2addr v1, v5

    .line 132
    invoke-virtual {v0, v4, v3, v2, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 133
    .line 134
    .line 135
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇0:Landroid/graphics/drawable/Drawable;

    .line 136
    .line 137
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 138
    .line 139
    .line 140
    :goto_0
    return-void
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private O8〇o(Lcom/intsig/camscanner/util/ParcelSize;F)V
    .locals 3

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇80〇808〇O:Landroid/graphics/Paint;

    .line 7
    .line 8
    const-string v1, "#19BC9C"

    .line 9
    .line 10
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇80〇808〇O:Landroid/graphics/Paint;

    .line 18
    .line 19
    const/4 v1, 0x1

    .line 20
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇80〇808〇O:Landroid/graphics/Paint;

    .line 24
    .line 25
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 26
    .line 27
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 28
    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇80〇808〇O:Landroid/graphics/Paint;

    .line 31
    .line 32
    iget v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->O〇8O8〇008:I

    .line 33
    .line 34
    int-to-float v2, v2

    .line 35
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 36
    .line 37
    .line 38
    new-instance v0, Landroid/graphics/Paint;

    .line 39
    .line 40
    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 41
    .line 42
    .line 43
    iput-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->OO0o〇〇〇〇0:Landroid/graphics/Paint;

    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o00〇〇Oo:Landroid/content/Context;

    .line 46
    .line 47
    const v1, 0x7f0811fd

    .line 48
    .line 49
    .line 50
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    iput-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇0:Landroid/graphics/drawable/Drawable;

    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o00〇〇Oo:Landroid/content/Context;

    .line 57
    .line 58
    const v1, 0x7f0811fb

    .line 59
    .line 60
    .line 61
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    iput-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oo08:Landroid/graphics/drawable/Drawable;

    .line 66
    .line 67
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o00〇〇Oo:Landroid/content/Context;

    .line 68
    .line 69
    const v1, 0x7f0811fc

    .line 70
    .line 71
    .line 72
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    iput-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇888:Landroid/graphics/drawable/Drawable;

    .line 77
    .line 78
    const-string v0, "TYPE_SIGNATURE"

    .line 79
    .line 80
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇8:Ljava/lang/String;

    .line 81
    .line 82
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    if-eqz v0, :cond_0

    .line 87
    .line 88
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/signature/SignatureView;->o8(Lcom/intsig/camscanner/util/ParcelSize;)V

    .line 89
    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_0
    const-string v0, "TYPE_JIGSAW"

    .line 93
    .line 94
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇8:Ljava/lang/String;

    .line 95
    .line 96
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 97
    .line 98
    .line 99
    move-result v0

    .line 100
    if-eqz v0, :cond_2

    .line 101
    .line 102
    if-nez p1, :cond_1

    .line 103
    .line 104
    const-string p1, "SignatureView"

    .line 105
    .line 106
    const-string v0, "this should happen"

    .line 107
    .line 108
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    goto :goto_0

    .line 112
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/signature/SignatureView;->〇o(Lcom/intsig/camscanner/util/ParcelSize;)V

    .line 113
    .line 114
    .line 115
    goto :goto_0

    .line 116
    :cond_2
    const-string p1, "TYPE_SIGNATURE_AREA"

    .line 117
    .line 118
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇8:Ljava/lang/String;

    .line 119
    .line 120
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 121
    .line 122
    .line 123
    move-result p1

    .line 124
    if-eqz p1, :cond_3

    .line 125
    .line 126
    invoke-direct {p0}, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO()V

    .line 127
    .line 128
    .line 129
    goto :goto_0

    .line 130
    :cond_3
    const-string p1, "TYPE_SIGNATURE_AREA_RECOVER"

    .line 131
    .line 132
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇8:Ljava/lang/String;

    .line 133
    .line 134
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 135
    .line 136
    .line 137
    move-result p1

    .line 138
    if-eqz p1, :cond_4

    .line 139
    .line 140
    invoke-virtual {p0}, Lcom/intsig/camscanner/signature/SignatureView;->o〇8()V

    .line 141
    .line 142
    .line 143
    :cond_4
    :goto_0
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/signature/SignatureView;->〇080(F)V

    .line 144
    .line 145
    .line 146
    return-void
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private OOO〇O0(Landroid/graphics/Canvas;)V
    .locals 10

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget v3, v1, v2

    .line 7
    .line 8
    const/4 v4, 0x1

    .line 9
    aget v5, v1, v4

    .line 10
    .line 11
    const/4 v6, 0x4

    .line 12
    aget v7, v1, v6

    .line 13
    .line 14
    const/4 v8, 0x5

    .line 15
    aget v1, v1, v8

    .line 16
    .line 17
    invoke-direct {v0, v3, v5, v7, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 18
    .line 19
    .line 20
    iget-boolean v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o〇:Z

    .line 21
    .line 22
    const/4 v3, -0x1

    .line 23
    if-nez v1, :cond_0

    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 26
    .line 27
    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 28
    .line 29
    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 30
    .line 31
    .line 32
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 33
    .line 34
    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 35
    .line 36
    .line 37
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 38
    .line 39
    const/16 v3, 0x99

    .line 40
    .line 41
    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 42
    .line 43
    .line 44
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 45
    .line 46
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 47
    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 50
    .line 51
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 57
    .line 58
    const v1, -0xe64364

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 62
    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 65
    .line 66
    iget v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->O〇8O8〇008:I

    .line 67
    .line 68
    int-to-float v1, v1

    .line 69
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 70
    .line 71
    .line 72
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 73
    .line 74
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8〇o:Landroid/graphics/PathEffect;

    .line 75
    .line 76
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 77
    .line 78
    .line 79
    new-instance v0, Landroid/graphics/RectF;

    .line 80
    .line 81
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 82
    .line 83
    aget v3, v1, v2

    .line 84
    .line 85
    iget v5, p0, Lcom/intsig/camscanner/signature/SignatureView;->O〇8O8〇008:I

    .line 86
    .line 87
    shr-int/lit8 v7, v5, 0x1

    .line 88
    .line 89
    int-to-float v7, v7

    .line 90
    sub-float/2addr v3, v7

    .line 91
    aget v7, v1, v4

    .line 92
    .line 93
    shr-int/lit8 v9, v5, 0x1

    .line 94
    .line 95
    int-to-float v9, v9

    .line 96
    sub-float/2addr v7, v9

    .line 97
    aget v6, v1, v6

    .line 98
    .line 99
    shr-int/lit8 v9, v5, 0x1

    .line 100
    .line 101
    int-to-float v9, v9

    .line 102
    add-float/2addr v6, v9

    .line 103
    aget v1, v1, v8

    .line 104
    .line 105
    shr-int/2addr v5, v4

    .line 106
    int-to-float v5, v5

    .line 107
    add-float/2addr v1, v5

    .line 108
    invoke-direct {v0, v3, v7, v6, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 109
    .line 110
    .line 111
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 112
    .line 113
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 114
    .line 115
    .line 116
    goto :goto_0

    .line 117
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 118
    .line 119
    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 120
    .line 121
    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 122
    .line 123
    .line 124
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 125
    .line 126
    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 127
    .line 128
    .line 129
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 130
    .line 131
    const/16 v3, 0xe5

    .line 132
    .line 133
    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 134
    .line 135
    .line 136
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 137
    .line 138
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 139
    .line 140
    .line 141
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 142
    .line 143
    invoke-virtual {v0}, Landroid/graphics/Paint;->reset()V

    .line 144
    .line 145
    .line 146
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 147
    .line 148
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 149
    .line 150
    .line 151
    move-result-object v1

    .line 152
    const/16 v3, 0x10

    .line 153
    .line 154
    invoke-static {v1, v3}, Lcom/intsig/utils/DisplayUtil;->〇〇8O0〇8(Landroid/content/Context;I)I

    .line 155
    .line 156
    .line 157
    move-result v1

    .line 158
    int-to-float v1, v1

    .line 159
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 160
    .line 161
    .line 162
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 163
    .line 164
    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    .line 165
    .line 166
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 167
    .line 168
    .line 169
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 170
    .line 171
    const v1, -0xa5a5a6

    .line 172
    .line 173
    .line 174
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 175
    .line 176
    .line 177
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o00〇〇Oo:Landroid/content/Context;

    .line 178
    .line 179
    const v1, 0x7f130a54

    .line 180
    .line 181
    .line 182
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 183
    .line 184
    .line 185
    move-result-object v0

    .line 186
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 187
    .line 188
    invoke-virtual {v1}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 189
    .line 190
    .line 191
    move-result-object v1

    .line 192
    iget v3, v1, Landroid/graphics/Paint$FontMetrics;->bottom:F

    .line 193
    .line 194
    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->top:F

    .line 195
    .line 196
    sub-float v1, v3, v1

    .line 197
    .line 198
    const/high16 v5, 0x40000000    # 2.0f

    .line 199
    .line 200
    div-float/2addr v1, v5

    .line 201
    sub-float/2addr v1, v3

    .line 202
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 203
    .line 204
    aget v4, v3, v4

    .line 205
    .line 206
    add-float/2addr v4, v1

    .line 207
    aget v1, v3, v2

    .line 208
    .line 209
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->o0ooO:Landroid/graphics/Paint;

    .line 210
    .line 211
    invoke-virtual {p1, v0, v1, v4, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 212
    .line 213
    .line 214
    return-void
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private Oo8Oo00oo(Landroid/graphics/Point;)Z
    .locals 6

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v1, v0, [F

    .line 3
    .line 4
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 5
    .line 6
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O888o0o:Landroid/graphics/Matrix;

    .line 7
    .line 8
    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 9
    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O888o0o:Landroid/graphics/Matrix;

    .line 12
    .line 13
    new-array v0, v0, [F

    .line 14
    .line 15
    iget v3, p1, Landroid/graphics/Point;->x:I

    .line 16
    .line 17
    int-to-float v3, v3

    .line 18
    const/4 v4, 0x0

    .line 19
    aput v3, v0, v4

    .line 20
    .line 21
    iget v3, p1, Landroid/graphics/Point;->y:I

    .line 22
    .line 23
    int-to-float v3, v3

    .line 24
    const/4 v5, 0x1

    .line 25
    aput v3, v0, v5

    .line 26
    .line 27
    invoke-virtual {v2, v1, v0}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 28
    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇0〇O0088o:Landroid/graphics/RectF;

    .line 31
    .line 32
    aget v2, v1, v4

    .line 33
    .line 34
    aget v3, v1, v5

    .line 35
    .line 36
    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    new-instance v2, Ljava/lang/StringBuilder;

    .line 41
    .line 42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    .line 44
    .line 45
    const-string v3, "isPointInsideDrawArea isContain="

    .line 46
    .line 47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    const-string v3, ",point x"

    .line 54
    .line 55
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    iget v3, p1, Landroid/graphics/Point;->x:I

    .line 59
    .line 60
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    const-string v3, ",point y"

    .line 64
    .line 65
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    iget p1, p1, Landroid/graphics/Point;->y:I

    .line 69
    .line 70
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    const-string p1, ", x="

    .line 74
    .line 75
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    aget p1, v1, v4

    .line 79
    .line 80
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    const-string p1, ",y="

    .line 84
    .line 85
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    aget p1, v1, v5

    .line 89
    .line 90
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object p1

    .line 97
    const-string v1, "SignatureView"

    .line 98
    .line 99
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    return v0
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private O〇8O8〇008(Landroid/graphics/Canvas;)V
    .locals 12

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O〇8O8〇008:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    shr-int/2addr v0, v1

    .line 5
    const/16 v2, 0x8

    .line 6
    .line 7
    new-array v3, v2, [F

    .line 8
    .line 9
    iget-object v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O00:[F

    .line 10
    .line 11
    const/4 v5, 0x0

    .line 12
    aget v6, v4, v5

    .line 13
    .line 14
    int-to-float v0, v0

    .line 15
    sub-float/2addr v6, v0

    .line 16
    aput v6, v3, v5

    .line 17
    .line 18
    aget v6, v4, v1

    .line 19
    .line 20
    sub-float/2addr v6, v0

    .line 21
    aput v6, v3, v1

    .line 22
    .line 23
    const/4 v6, 0x2

    .line 24
    aget v7, v4, v6

    .line 25
    .line 26
    add-float/2addr v7, v0

    .line 27
    aput v7, v3, v6

    .line 28
    .line 29
    const/4 v7, 0x3

    .line 30
    aget v8, v4, v7

    .line 31
    .line 32
    sub-float/2addr v8, v0

    .line 33
    aput v8, v3, v7

    .line 34
    .line 35
    const/4 v8, 0x4

    .line 36
    aget v9, v4, v8

    .line 37
    .line 38
    add-float/2addr v9, v0

    .line 39
    aput v9, v3, v8

    .line 40
    .line 41
    const/4 v9, 0x5

    .line 42
    aget v10, v4, v9

    .line 43
    .line 44
    add-float/2addr v10, v0

    .line 45
    aput v10, v3, v9

    .line 46
    .line 47
    const/4 v10, 0x6

    .line 48
    aget v11, v4, v10

    .line 49
    .line 50
    sub-float/2addr v11, v0

    .line 51
    aput v11, v3, v10

    .line 52
    .line 53
    const/4 v11, 0x7

    .line 54
    aget v4, v4, v11

    .line 55
    .line 56
    add-float/2addr v4, v0

    .line 57
    aput v4, v3, v11

    .line 58
    .line 59
    new-array v0, v2, [F

    .line 60
    .line 61
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 62
    .line 63
    invoke-virtual {v2, v0, v3}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 64
    .line 65
    .line 66
    new-instance v2, Landroid/graphics/Path;

    .line 67
    .line 68
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 69
    .line 70
    .line 71
    aget v3, v0, v5

    .line 72
    .line 73
    aget v1, v0, v1

    .line 74
    .line 75
    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 76
    .line 77
    .line 78
    aget v1, v0, v6

    .line 79
    .line 80
    aget v3, v0, v7

    .line 81
    .line 82
    invoke-virtual {v2, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 83
    .line 84
    .line 85
    aget v1, v0, v8

    .line 86
    .line 87
    aget v3, v0, v9

    .line 88
    .line 89
    invoke-virtual {v2, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 90
    .line 91
    .line 92
    aget v1, v0, v10

    .line 93
    .line 94
    aget v0, v0, v11

    .line 95
    .line 96
    invoke-virtual {v2, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 97
    .line 98
    .line 99
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 100
    .line 101
    .line 102
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇80〇808〇O:Landroid/graphics/Paint;

    .line 103
    .line 104
    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 105
    .line 106
    .line 107
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private o0ooO()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o:Z

    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/signature/SignatureView;->〇O00(Z)V

    .line 5
    .line 6
    .line 7
    const/high16 v0, 0x42f00000    # 120.0f

    .line 8
    .line 9
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/high16 v1, 0x42700000    # 60.0f

    .line 14
    .line 15
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8(II)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private o8(Lcom/intsig/camscanner/util/ParcelSize;)V
    .locals 5
    .param p1    # Lcom/intsig/camscanner/util/ParcelSize;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o00〇〇Oo:Landroid/content/Context;

    .line 2
    .line 3
    const/16 v1, 0x32

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/signature/SignatureView;->〇〇〇0〇〇0(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 25
    .line 26
    invoke-virtual {v2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getColor()I

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 31
    .line 32
    invoke-virtual {v3}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getStrokeSize()I

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    invoke-direct {p0, v1, v2, v3}, Lcom/intsig/camscanner/signature/SignatureView;->o〇0OOo〇0(Ljava/lang/String;II)V

    .line 37
    .line 38
    .line 39
    const/high16 v1, 0x3f800000    # 1.0f

    .line 40
    .line 41
    if-eqz p1, :cond_0

    .line 42
    .line 43
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    if-lez v2, :cond_0

    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    int-to-float p1, p1

    .line 54
    mul-float p1, p1, v1

    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 57
    .line 58
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 59
    .line 60
    .line 61
    move-result v0

    .line 62
    int-to-float v0, v0

    .line 63
    div-float/2addr p1, v0

    .line 64
    goto :goto_0

    .line 65
    :cond_0
    iget p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oooo8o0〇:I

    .line 66
    .line 67
    iget v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇808〇:I

    .line 68
    .line 69
    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    .line 70
    .line 71
    .line 72
    move-result p1

    .line 73
    int-to-float v0, v0

    .line 74
    mul-float v0, v0, v1

    .line 75
    .line 76
    int-to-float p1, p1

    .line 77
    div-float p1, v0, p1

    .line 78
    .line 79
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇8o8o〇:Landroid/graphics/Matrix;

    .line 80
    .line 81
    invoke-virtual {v0, p1, p1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 82
    .line 83
    .line 84
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇8o8o〇:Landroid/graphics/Matrix;

    .line 85
    .line 86
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 87
    .line 88
    const/4 v2, 0x0

    .line 89
    aget v2, v1, v2

    .line 90
    .line 91
    iget v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oooo8o0〇:I

    .line 92
    .line 93
    int-to-float v3, v3

    .line 94
    mul-float v3, v3, p1

    .line 95
    .line 96
    const/high16 v4, 0x40000000    # 2.0f

    .line 97
    .line 98
    div-float/2addr v3, v4

    .line 99
    sub-float/2addr v2, v3

    .line 100
    const/4 v3, 0x1

    .line 101
    aget v1, v1, v3

    .line 102
    .line 103
    iget v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇808〇:I

    .line 104
    .line 105
    int-to-float v3, v3

    .line 106
    mul-float p1, p1, v3

    .line 107
    .line 108
    div-float/2addr p1, v4

    .line 109
    sub-float/2addr v1, p1

    .line 110
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 111
    .line 112
    .line 113
    iget p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oooo8o0〇:I

    .line 114
    .line 115
    iget v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇808〇:I

    .line 116
    .line 117
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8(II)V

    .line 118
    .line 119
    .line 120
    return-void
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private oo〇(FFFF)F
    .locals 0

    .line 1
    sub-float/2addr p1, p3

    .line 2
    mul-float p1, p1, p1

    .line 3
    .line 4
    sub-float/2addr p2, p4

    .line 5
    mul-float p2, p2, p2

    .line 6
    .line 7
    add-float/2addr p1, p2

    .line 8
    float-to-double p1, p1

    .line 9
    invoke-static {p1, p2}, Ljava/lang/Math;->sqrt(D)D

    .line 10
    .line 11
    .line 12
    move-result-wide p1

    .line 13
    double-to-float p1, p1

    .line 14
    return p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private o〇0OOo〇0(Ljava/lang/String;II)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 12
    .line 13
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 14
    .line 15
    .line 16
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v1, "loadSignatureBitmap imagePath:"

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    const-string v1, " strokeColor:"

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    const-string v1, " strokeSize:"

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    const-string v1, "SignatureView"

    .line 50
    .line 51
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    iget v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oooo8o0〇:I

    .line 55
    .line 56
    if-lez v0, :cond_5

    .line 57
    .line 58
    iget v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇808〇:I

    .line 59
    .line 60
    if-gtz v2, :cond_1

    .line 61
    .line 62
    goto :goto_2

    .line 63
    :cond_1
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 64
    .line 65
    invoke-static {v0, v2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    iput-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 70
    .line 71
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o8〇O〇0O0〇()Z

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    const/4 v1, 0x0

    .line 76
    if-eqz v0, :cond_2

    .line 77
    .line 78
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 79
    .line 80
    invoke-static {p1, v0, v1, v1}, Lcom/intsig/nativelib/DraftEngine;->CleanImageKeepColor(Ljava/lang/String;Landroid/graphics/Bitmap;II)I

    .line 81
    .line 82
    .line 83
    move-result p1

    .line 84
    goto :goto_0

    .line 85
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 86
    .line 87
    invoke-static {p1, v0, v1, v1}, Lcom/intsig/nativelib/DraftEngine;->CleanImage(Ljava/lang/String;Landroid/graphics/Bitmap;II)I

    .line 88
    .line 89
    .line 90
    move-result p1

    .line 91
    :goto_0
    const/4 v0, -0x1

    .line 92
    if-le p1, v0, :cond_4

    .line 93
    .line 94
    if-eqz p2, :cond_3

    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 97
    .line 98
    invoke-static {p1, v0, p2}, Lcom/intsig/nativelib/DraftEngine;->StrokeColor(ILandroid/graphics/Bitmap;I)V

    .line 99
    .line 100
    .line 101
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 102
    .line 103
    int-to-float v1, p3

    .line 104
    invoke-static {p1, v0, v1}, Lcom/intsig/nativelib/DraftEngine;->StrokeSize(ILandroid/graphics/Bitmap;F)V

    .line 105
    .line 106
    .line 107
    :cond_3
    invoke-static {p1}, Lcom/intsig/nativelib/DraftEngine;->FreeContext(I)V

    .line 108
    .line 109
    .line 110
    goto :goto_1

    .line 111
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 112
    .line 113
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 118
    .line 119
    .line 120
    move-result-object p1

    .line 121
    iput-object p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 122
    .line 123
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 124
    .line 125
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setColor(I)V

    .line 126
    .line 127
    .line 128
    iput p2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇0000OOO:I

    .line 129
    .line 130
    iput p3, p0, Lcom/intsig/camscanner/signature/SignatureView;->oo88o8O:I

    .line 131
    .line 132
    return-void

    .line 133
    :cond_5
    :goto_2
    const-string p1, "decode error, width or height < 0"

    .line 134
    .line 135
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    return-void
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private o〇〇0〇(Landroid/graphics/Canvas;)V
    .locals 9

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oo8Oo00oo:Z

    .line 2
    .line 3
    const/4 v1, 0x3

    .line 4
    const/4 v2, 0x2

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇〇0〇〇0:Landroid/graphics/Matrix;

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇〇0〇〇0:Landroid/graphics/Matrix;

    .line 13
    .line 14
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 15
    .line 16
    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇〇0〇〇0:Landroid/graphics/Matrix;

    .line 20
    .line 21
    iget v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8:F

    .line 22
    .line 23
    neg-float v3, v3

    .line 24
    iget-object v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 25
    .line 26
    const/4 v5, 0x0

    .line 27
    aget v6, v4, v5

    .line 28
    .line 29
    const/4 v7, 0x1

    .line 30
    aget v4, v4, v7

    .line 31
    .line 32
    invoke-virtual {v0, v3, v6, v4}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇0OOo〇0:[F

    .line 36
    .line 37
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O00:[F

    .line 38
    .line 39
    aget v4, v3, v2

    .line 40
    .line 41
    aput v4, v0, v5

    .line 42
    .line 43
    aget v1, v3, v1

    .line 44
    .line 45
    aput v1, v0, v7

    .line 46
    .line 47
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇〇0〇〇0:Landroid/graphics/Matrix;

    .line 48
    .line 49
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇0o:[F

    .line 50
    .line 51
    invoke-virtual {v1, v3, v0}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 52
    .line 53
    .line 54
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇0o:[F

    .line 55
    .line 56
    aget v1, v0, v5

    .line 57
    .line 58
    float-to-int v3, v1

    .line 59
    iget v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8ooOoo〇:I

    .line 60
    .line 61
    div-int/lit8 v6, v4, 0x2

    .line 62
    .line 63
    sub-int/2addr v3, v6

    .line 64
    aget v0, v0, v7

    .line 65
    .line 66
    float-to-int v6, v0

    .line 67
    div-int/lit8 v8, v4, 0x2

    .line 68
    .line 69
    sub-int/2addr v6, v8

    .line 70
    float-to-int v1, v1

    .line 71
    div-int/lit8 v8, v4, 0x2

    .line 72
    .line 73
    add-int/2addr v1, v8

    .line 74
    float-to-int v0, v0

    .line 75
    div-int/2addr v4, v2

    .line 76
    add-int/2addr v0, v4

    .line 77
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 78
    .line 79
    .line 80
    iget v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8:F

    .line 81
    .line 82
    iget-object v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 83
    .line 84
    aget v5, v4, v5

    .line 85
    .line 86
    aget v4, v4, v7

    .line 87
    .line 88
    invoke-virtual {p1, v2, v5, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 89
    .line 90
    .line 91
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇888:Landroid/graphics/drawable/Drawable;

    .line 92
    .line 93
    invoke-virtual {v2, v3, v6, v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 94
    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇888:Landroid/graphics/drawable/Drawable;

    .line 97
    .line 98
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 99
    .line 100
    .line 101
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 102
    .line 103
    .line 104
    goto :goto_0

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇888:Landroid/graphics/drawable/Drawable;

    .line 106
    .line 107
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 108
    .line 109
    aget v4, v3, v2

    .line 110
    .line 111
    float-to-int v5, v4

    .line 112
    iget v6, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8ooOoo〇:I

    .line 113
    .line 114
    div-int/lit8 v7, v6, 0x2

    .line 115
    .line 116
    sub-int/2addr v5, v7

    .line 117
    aget v1, v3, v1

    .line 118
    .line 119
    float-to-int v3, v1

    .line 120
    div-int/lit8 v7, v6, 0x2

    .line 121
    .line 122
    sub-int/2addr v3, v7

    .line 123
    float-to-int v4, v4

    .line 124
    div-int/lit8 v7, v6, 0x2

    .line 125
    .line 126
    add-int/2addr v4, v7

    .line 127
    float-to-int v1, v1

    .line 128
    div-int/2addr v6, v2

    .line 129
    add-int/2addr v1, v6

    .line 130
    invoke-virtual {v0, v5, v3, v4, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 131
    .line 132
    .line 133
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇888:Landroid/graphics/drawable/Drawable;

    .line 134
    .line 135
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 136
    .line 137
    .line 138
    :goto_0
    return-void
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private 〇0000OOO(Landroid/graphics/Canvas;)V
    .locals 8

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oo8Oo00oo:Z

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const/4 v2, 0x0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇〇0〇〇0:Landroid/graphics/Matrix;

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇〇0〇〇0:Landroid/graphics/Matrix;

    .line 13
    .line 14
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 15
    .line 16
    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇〇0〇〇0:Landroid/graphics/Matrix;

    .line 20
    .line 21
    iget v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8:F

    .line 22
    .line 23
    neg-float v3, v3

    .line 24
    iget-object v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 25
    .line 26
    aget v5, v4, v2

    .line 27
    .line 28
    aget v4, v4, v1

    .line 29
    .line 30
    invoke-virtual {v0, v3, v5, v4}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇0OOo〇0:[F

    .line 34
    .line 35
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O00:[F

    .line 36
    .line 37
    aget v4, v3, v2

    .line 38
    .line 39
    aput v4, v0, v2

    .line 40
    .line 41
    aget v3, v3, v1

    .line 42
    .line 43
    aput v3, v0, v1

    .line 44
    .line 45
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇〇0〇〇0:Landroid/graphics/Matrix;

    .line 46
    .line 47
    iget-object v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇0o:[F

    .line 48
    .line 49
    invoke-virtual {v3, v4, v0}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 50
    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇0o:[F

    .line 53
    .line 54
    aget v3, v0, v2

    .line 55
    .line 56
    float-to-int v4, v3

    .line 57
    iget v5, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8ooOoo〇:I

    .line 58
    .line 59
    div-int/lit8 v6, v5, 0x2

    .line 60
    .line 61
    sub-int/2addr v4, v6

    .line 62
    aget v0, v0, v1

    .line 63
    .line 64
    float-to-int v6, v0

    .line 65
    div-int/lit8 v7, v5, 0x2

    .line 66
    .line 67
    sub-int/2addr v6, v7

    .line 68
    float-to-int v3, v3

    .line 69
    div-int/lit8 v7, v5, 0x2

    .line 70
    .line 71
    add-int/2addr v3, v7

    .line 72
    float-to-int v0, v0

    .line 73
    div-int/lit8 v5, v5, 0x2

    .line 74
    .line 75
    add-int/2addr v0, v5

    .line 76
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 77
    .line 78
    .line 79
    iget v5, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8:F

    .line 80
    .line 81
    iget-object v7, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 82
    .line 83
    aget v2, v7, v2

    .line 84
    .line 85
    aget v1, v7, v1

    .line 86
    .line 87
    invoke-virtual {p1, v5, v2, v1}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 88
    .line 89
    .line 90
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oo08:Landroid/graphics/drawable/Drawable;

    .line 91
    .line 92
    invoke-virtual {v1, v4, v6, v3, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 93
    .line 94
    .line 95
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oo08:Landroid/graphics/drawable/Drawable;

    .line 96
    .line 97
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 101
    .line 102
    .line 103
    goto :goto_0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oo08:Landroid/graphics/drawable/Drawable;

    .line 105
    .line 106
    iget-object v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 107
    .line 108
    aget v2, v3, v2

    .line 109
    .line 110
    float-to-int v4, v2

    .line 111
    iget v5, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8ooOoo〇:I

    .line 112
    .line 113
    div-int/lit8 v6, v5, 0x2

    .line 114
    .line 115
    sub-int/2addr v4, v6

    .line 116
    aget v1, v3, v1

    .line 117
    .line 118
    float-to-int v3, v1

    .line 119
    div-int/lit8 v6, v5, 0x2

    .line 120
    .line 121
    sub-int/2addr v3, v6

    .line 122
    float-to-int v2, v2

    .line 123
    div-int/lit8 v6, v5, 0x2

    .line 124
    .line 125
    add-int/2addr v2, v6

    .line 126
    float-to-int v1, v1

    .line 127
    div-int/lit8 v5, v5, 0x2

    .line 128
    .line 129
    add-int/2addr v1, v5

    .line 130
    invoke-virtual {v0, v4, v3, v2, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 131
    .line 132
    .line 133
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oo08:Landroid/graphics/drawable/Drawable;

    .line 134
    .line 135
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 136
    .line 137
    .line 138
    :goto_0
    return-void
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private 〇00〇8(II)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇8o8o〇:Landroid/graphics/Matrix;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇8O0〇8:[F

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇8O0〇8:[F

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    aget v0, v0, v1

    .line 12
    .line 13
    int-to-float p1, p1

    .line 14
    mul-float p1, p1, v0

    .line 15
    .line 16
    const/high16 v2, 0x40000000    # 2.0f

    .line 17
    .line 18
    div-float/2addr p1, v2

    .line 19
    int-to-float p2, p2

    .line 20
    mul-float v0, v0, p2

    .line 21
    .line 22
    div-float/2addr v0, v2

    .line 23
    iget-object p2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 24
    .line 25
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 26
    .line 27
    aget v3, v2, v1

    .line 28
    .line 29
    sub-float/2addr v3, p1

    .line 30
    aput v3, p2, v1

    .line 31
    .line 32
    const/4 v3, 0x1

    .line 33
    aget v4, v2, v3

    .line 34
    .line 35
    sub-float/2addr v4, v0

    .line 36
    aput v4, p2, v3

    .line 37
    .line 38
    aget v4, v2, v1

    .line 39
    .line 40
    add-float v5, v4, p1

    .line 41
    .line 42
    const/4 v6, 0x2

    .line 43
    aput v5, p2, v6

    .line 44
    .line 45
    aget v2, v2, v3

    .line 46
    .line 47
    sub-float v5, v2, v0

    .line 48
    .line 49
    const/4 v6, 0x3

    .line 50
    aput v5, p2, v6

    .line 51
    .line 52
    add-float v5, v4, p1

    .line 53
    .line 54
    const/4 v6, 0x4

    .line 55
    aput v5, p2, v6

    .line 56
    .line 57
    add-float v5, v2, v0

    .line 58
    .line 59
    const/4 v7, 0x5

    .line 60
    aput v5, p2, v7

    .line 61
    .line 62
    const/4 v5, 0x6

    .line 63
    sub-float/2addr v4, p1

    .line 64
    aput v4, p2, v5

    .line 65
    .line 66
    const/4 p1, 0x7

    .line 67
    add-float/2addr v2, v0

    .line 68
    aput v2, p2, p1

    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O00:[F

    .line 71
    .line 72
    array-length v0, p2

    .line 73
    invoke-static {p2, v1, p1, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 74
    .line 75
    .line 76
    iget-object p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇0〇O0088o:Landroid/graphics/RectF;

    .line 77
    .line 78
    iget-object p2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 79
    .line 80
    aget v0, p2, v1

    .line 81
    .line 82
    aget v1, p2, v3

    .line 83
    .line 84
    aget v2, p2, v6

    .line 85
    .line 86
    aget p2, p2, v7

    .line 87
    .line 88
    invoke-virtual {p1, v0, v1, v2, p2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 89
    .line 90
    .line 91
    new-instance p1, Ljava/lang/StringBuilder;

    .line 92
    .line 93
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 94
    .line 95
    .line 96
    const-string p2, "startBoundRF ="

    .line 97
    .line 98
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    iget-object p2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇0〇O0088o:Landroid/graphics/RectF;

    .line 102
    .line 103
    invoke-virtual {p2}, Landroid/graphics/RectF;->toString()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object p2

    .line 107
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object p1

    .line 114
    const-string p2, "SignatureView"

    .line 115
    .line 116
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private 〇o(Lcom/intsig/camscanner/util/ParcelSize;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o00〇〇Oo:Landroid/content/Context;

    .line 8
    .line 9
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o00〇〇Oo:Landroid/content/Context;

    .line 14
    .line 15
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇O〇(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 24
    .line 25
    if-nez v0, :cond_0

    .line 26
    .line 27
    const-string p1, "SignatureView"

    .line 28
    .line 29
    const-string v0, "mSignatureBitmap == null"

    .line 30
    .line 31
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    return-void

    .line 35
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    iput v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oooo8o0〇:I

    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 42
    .line 43
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    iput v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇808〇:I

    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    int-to-float p1, p1

    .line 54
    const/high16 v0, 0x3f800000    # 1.0f

    .line 55
    .line 56
    mul-float p1, p1, v0

    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 59
    .line 60
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    int-to-float v0, v0

    .line 65
    div-float/2addr p1, v0

    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇8o8o〇:Landroid/graphics/Matrix;

    .line 67
    .line 68
    invoke-virtual {v0, p1, p1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 69
    .line 70
    .line 71
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇8o8o〇:Landroid/graphics/Matrix;

    .line 72
    .line 73
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 74
    .line 75
    const/4 v2, 0x0

    .line 76
    aget v2, v1, v2

    .line 77
    .line 78
    iget v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oooo8o0〇:I

    .line 79
    .line 80
    int-to-float v3, v3

    .line 81
    mul-float v3, v3, p1

    .line 82
    .line 83
    const/high16 v4, 0x40000000    # 2.0f

    .line 84
    .line 85
    div-float/2addr v3, v4

    .line 86
    sub-float/2addr v2, v3

    .line 87
    const/4 v3, 0x1

    .line 88
    aget v1, v1, v3

    .line 89
    .line 90
    iget v3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇808〇:I

    .line 91
    .line 92
    int-to-float v3, v3

    .line 93
    mul-float p1, p1, v3

    .line 94
    .line 95
    div-float/2addr p1, v4

    .line 96
    sub-float/2addr v1, p1

    .line 97
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 98
    .line 99
    .line 100
    iget p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oooo8o0〇:I

    .line 101
    .line 102
    iget v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇808〇:I

    .line 103
    .line 104
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8(II)V

    .line 105
    .line 106
    .line 107
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private 〇oOO8O8(Landroid/graphics/Canvas;FFFFI)V
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "drawDash degree = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "SignatureView"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-static {p6}, Ljava/lang/Math;->abs(I)I

    .line 24
    .line 25
    .line 26
    move-result p6

    .line 27
    if-eqz p6, :cond_0

    .line 28
    .line 29
    const/16 v0, 0x168

    .line 30
    .line 31
    if-eq p6, v0, :cond_0

    .line 32
    .line 33
    const/16 v0, 0xb4

    .line 34
    .line 35
    if-eq p6, v0, :cond_0

    .line 36
    .line 37
    const/16 v0, 0x5a

    .line 38
    .line 39
    if-eq p6, v0, :cond_0

    .line 40
    .line 41
    const/16 v0, 0x10e

    .line 42
    .line 43
    if-ne p6, v0, :cond_1

    .line 44
    .line 45
    :cond_0
    sub-float p6, p4, p2

    .line 46
    .line 47
    const/high16 v0, 0x40800000    # 4.0f

    .line 48
    .line 49
    div-float/2addr p6, v0

    .line 50
    sub-float v1, p5, p3

    .line 51
    .line 52
    div-float/2addr v1, v0

    .line 53
    add-float v0, p4, p2

    .line 54
    .line 55
    const/high16 v2, 0x40000000    # 2.0f

    .line 56
    .line 57
    div-float/2addr v0, v2

    .line 58
    add-float v3, p5, p3

    .line 59
    .line 60
    div-float/2addr v3, v2

    .line 61
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇〇0〇:Landroid/graphics/Path;

    .line 62
    .line 63
    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 64
    .line 65
    .line 66
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇〇0〇:Landroid/graphics/Path;

    .line 67
    .line 68
    add-float/2addr p2, p6

    .line 69
    invoke-virtual {v2, p2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 70
    .line 71
    .line 72
    iget-object p2, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇〇0〇:Landroid/graphics/Path;

    .line 73
    .line 74
    sub-float/2addr p4, p6

    .line 75
    invoke-virtual {p2, p4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 76
    .line 77
    .line 78
    iget-object p2, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇〇0〇:Landroid/graphics/Path;

    .line 79
    .line 80
    add-float/2addr p3, v1

    .line 81
    invoke-virtual {p2, v0, p3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 82
    .line 83
    .line 84
    iget-object p2, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇〇0〇:Landroid/graphics/Path;

    .line 85
    .line 86
    sub-float/2addr p5, v1

    .line 87
    invoke-virtual {p2, v0, p5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 88
    .line 89
    .line 90
    iget-object p2, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇〇0〇:Landroid/graphics/Path;

    .line 91
    .line 92
    iget-object p3, p0, Lcom/intsig/camscanner/signature/SignatureView;->OOO〇O0:Landroid/graphics/Paint;

    .line 93
    .line 94
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 95
    .line 96
    .line 97
    :cond_1
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
.end method

.method private 〇〇〇0〇〇0(Ljava/lang/String;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 8
    .line 9
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 10
    .line 11
    .line 12
    iget p1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 13
    .line 14
    iput p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oooo8o0〇:I

    .line 15
    .line 16
    iget p1, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 17
    .line 18
    iput p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇808〇:I

    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public O8()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getTempSignaturePath()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    return-object v0

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getTempSignaturePath()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public OO0o〇〇()[I
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    aget v2, v0, v1

    .line 5
    .line 6
    const/4 v3, 0x1

    .line 7
    aget v4, v0, v3

    .line 8
    .line 9
    const/4 v5, 0x2

    .line 10
    aget v6, v0, v5

    .line 11
    .line 12
    const/4 v7, 0x3

    .line 13
    aget v0, v0, v7

    .line 14
    .line 15
    invoke-direct {p0, v2, v4, v6, v0}, Lcom/intsig/camscanner/signature/SignatureView;->oo〇(FFFF)F

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    float-to-int v0, v0

    .line 20
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 21
    .line 22
    const/4 v4, 0x4

    .line 23
    aget v4, v2, v4

    .line 24
    .line 25
    const/4 v6, 0x5

    .line 26
    aget v6, v2, v6

    .line 27
    .line 28
    aget v8, v2, v5

    .line 29
    .line 30
    aget v2, v2, v7

    .line 31
    .line 32
    invoke-direct {p0, v4, v6, v8, v2}, Lcom/intsig/camscanner/signature/SignatureView;->oo〇(FFFF)F

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    float-to-int v2, v2

    .line 37
    new-array v4, v5, [I

    .line 38
    .line 39
    aput v0, v4, v1

    .line 40
    .line 41
    aput v2, v4, v3

    .line 42
    .line 43
    return-object v4
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public OO0o〇〇〇〇0()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oo08(F)V
    .locals 6

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8:F

    .line 7
    .line 8
    sub-float v0, p1, v0

    .line 9
    .line 10
    new-instance v1, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v2, "setRotateView rotate2="

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    const-string v2, "SignatureView"

    .line 28
    .line 29
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇8o8o〇:Landroid/graphics/Matrix;

    .line 33
    .line 34
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 35
    .line 36
    const/4 v3, 0x0

    .line 37
    aget v4, v2, v3

    .line 38
    .line 39
    const/4 v5, 0x1

    .line 40
    aget v2, v2, v5

    .line 41
    .line 42
    invoke-virtual {v1, v0, v4, v2}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 43
    .line 44
    .line 45
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 46
    .line 47
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 48
    .line 49
    aget v3, v2, v3

    .line 50
    .line 51
    aget v2, v2, v5

    .line 52
    .line 53
    invoke-virtual {v1, v0, v3, v2}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 57
    .line 58
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 59
    .line 60
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O00:[F

    .line 61
    .line 62
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 63
    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 66
    .line 67
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 68
    .line 69
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->OoO8:[F

    .line 70
    .line 71
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 72
    .line 73
    .line 74
    iput p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8:F

    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public OoO8(FFF)V
    .locals 3

    .line 1
    const-string v0, "TYPE_SIGNATURE_AREA"

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇8:Ljava/lang/String;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const-string v0, "TYPE_SIGNATURE_AREA_RECOVER"

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇8:Ljava/lang/String;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_2

    .line 20
    .line 21
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 22
    .line 23
    cmpg-float v0, p1, v0

    .line 24
    .line 25
    if-gez v0, :cond_2

    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/signature/SignatureView;->OO0o〇〇()[I

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    const/4 v1, 0x0

    .line 32
    aget v1, v0, v1

    .line 33
    .line 34
    iget v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇8:I

    .line 35
    .line 36
    if-lt v1, v2, :cond_1

    .line 37
    .line 38
    const/4 v1, 0x1

    .line 39
    aget v0, v0, v1

    .line 40
    .line 41
    iget v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->O08000:I

    .line 42
    .line 43
    if-ge v0, v1, :cond_2

    .line 44
    .line 45
    :cond_1
    return-void

    .line 46
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇8o8o〇:Landroid/graphics/Matrix;

    .line 47
    .line 48
    invoke-virtual {v0, p1, p1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 52
    .line 53
    invoke-virtual {v0, p1, p1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 54
    .line 55
    .line 56
    iget-object p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 57
    .line 58
    iget-object p2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 59
    .line 60
    iget-object p3, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O00:[F

    .line 61
    .line 62
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 63
    .line 64
    .line 65
    iget-object p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 66
    .line 67
    iget-object p2, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 68
    .line 69
    iget-object p3, p0, Lcom/intsig/camscanner/signature/SignatureView;->OoO8:[F

    .line 70
    .line 71
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public Oooo8o0〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oo〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getCenter()[F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getId()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->OO0o〇〇:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o800o8O()[F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO80(I)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "changeStrokeSize Color = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string v1, " mCurrentColor:"

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    iget v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇0000OOO:I

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const-string v1, "SignatureView"

    .line 29
    .line 30
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    iget v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->oo88o8O:I

    .line 40
    .line 41
    invoke-direct {p0, v0, p1, v1}, Lcom/intsig/camscanner/signature/SignatureView;->o〇0OOo〇0(Ljava/lang/String;II)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public onDelete()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 12
    .line 13
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
.end method

.method public oo88o8O(I)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "changeStrokeSize progress = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "SignatureView"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getColor()I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    invoke-direct {p0, v0, v1, p1}, Lcom/intsig/camscanner/signature/SignatureView;->o〇0OOo〇0(Ljava/lang/String;II)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public o〇0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public o〇8()V
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o:Z

    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/signature/SignatureView;->〇O00(Z)V

    .line 5
    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇08O8o〇0:[F

    .line 8
    .line 9
    const/4 v2, 0x2

    .line 10
    aget v2, v1, v2

    .line 11
    .line 12
    aget v3, v1, v0

    .line 13
    .line 14
    sub-float/2addr v2, v3

    .line 15
    float-to-int v2, v2

    .line 16
    const/4 v3, 0x3

    .line 17
    aget v3, v1, v3

    .line 18
    .line 19
    const/4 v4, 0x1

    .line 20
    aget v1, v1, v4

    .line 21
    .line 22
    sub-float/2addr v3, v1

    .line 23
    float-to-int v1, v3

    .line 24
    invoke-direct {p0, v2, v1}, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8(II)V

    .line 25
    .line 26
    .line 27
    iput-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o〇:Z

    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public o〇O8〇〇o(Landroid/view/View;Landroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;F)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 2
    .line 3
    iget v1, p2, Landroid/graphics/Point;->x:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    const/4 v2, 0x0

    .line 7
    aput v1, v0, v2

    .line 8
    .line 9
    iget p2, p2, Landroid/graphics/Point;->y:I

    .line 10
    .line 11
    int-to-float p2, p2

    .line 12
    const/4 v1, 0x1

    .line 13
    aput p2, v0, v1

    .line 14
    .line 15
    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    check-cast p2, [F

    .line 20
    .line 21
    iput-object p2, p0, Lcom/intsig/camscanner/signature/SignatureView;->OoO8:[F

    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇080:Landroid/view/View;

    .line 24
    .line 25
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/signature/SignatureView;->o〇0(Z)V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0, p3, p4}, Lcom/intsig/camscanner/signature/SignatureView;->O8〇o(Lcom/intsig/camscanner/util/ParcelSize;F)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public 〇00()Landroid/graphics/Matrix;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇8o8o〇:Landroid/graphics/Matrix;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080(F)V
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8:F

    .line 7
    .line 8
    add-float/2addr v0, p1

    .line 9
    iput v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8:F

    .line 10
    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v1, "rotateView rotate="

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const-string v1, "SignatureView"

    .line 29
    .line 30
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇8o8o〇:Landroid/graphics/Matrix;

    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 36
    .line 37
    const/4 v2, 0x0

    .line 38
    aget v3, v1, v2

    .line 39
    .line 40
    const/4 v4, 0x1

    .line 41
    aget v1, v1, v4

    .line 42
    .line 43
    invoke-virtual {v0, p1, v3, v1}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 47
    .line 48
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 49
    .line 50
    aget v2, v1, v2

    .line 51
    .line 52
    aget v1, v1, v4

    .line 53
    .line 54
    invoke-virtual {v0, p1, v2, v1}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 55
    .line 56
    .line 57
    iget-object p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 60
    .line 61
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O00:[F

    .line 62
    .line 63
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 64
    .line 65
    .line 66
    iget-object p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 69
    .line 70
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->OoO8:[F

    .line 71
    .line 72
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 73
    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇0〇O0088o(Landroid/graphics/Canvas;)V
    .locals 9

    .line 1
    const-string v0, "TYPE_SIGNATURE_AREA"

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇8:Ljava/lang/String;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_2

    .line 10
    .line 11
    const-string v0, "TYPE_SIGNATURE_AREA_RECOVER"

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇8:Ljava/lang/String;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 23
    .line 24
    if-nez v0, :cond_1

    .line 25
    .line 26
    const-string p1, "SignatureView"

    .line 27
    .line 28
    const-string v0, "null == mSignatureBitmap"

    .line 29
    .line 30
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    return-void

    .line 34
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇8o8o〇:Landroid/graphics/Matrix;

    .line 35
    .line 36
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->OO0o〇〇〇〇0:Landroid/graphics/Paint;

    .line 37
    .line 38
    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 39
    .line 40
    .line 41
    goto :goto_1

    .line 42
    :cond_2
    :goto_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/signature/SignatureView;->OOO〇O0(Landroid/graphics/Canvas;)V

    .line 43
    .line 44
    .line 45
    :goto_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o〇:Z

    .line 46
    .line 47
    if-eqz v0, :cond_5

    .line 48
    .line 49
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/signature/SignatureView;->O〇8O8〇008(Landroid/graphics/Canvas;)V

    .line 50
    .line 51
    .line 52
    iget-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oo〇:Z

    .line 53
    .line 54
    if-eqz v0, :cond_3

    .line 55
    .line 56
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/signature/SignatureView;->〇0000OOO(Landroid/graphics/Canvas;)V

    .line 57
    .line 58
    .line 59
    :cond_3
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/signature/SignatureView;->O8ooOoo〇(Landroid/graphics/Canvas;)V

    .line 60
    .line 61
    .line 62
    iget-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->oo〇:Z

    .line 63
    .line 64
    if-eqz v0, :cond_4

    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 67
    .line 68
    const/4 v1, 0x0

    .line 69
    aget v4, v0, v1

    .line 70
    .line 71
    const/4 v1, 0x1

    .line 72
    aget v5, v0, v1

    .line 73
    .line 74
    const/4 v1, 0x4

    .line 75
    aget v6, v0, v1

    .line 76
    .line 77
    const/4 v1, 0x5

    .line 78
    aget v7, v0, v1

    .line 79
    .line 80
    iget v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00〇8:F

    .line 81
    .line 82
    float-to-int v8, v0

    .line 83
    move-object v2, p0

    .line 84
    move-object v3, p1

    .line 85
    invoke-direct/range {v2 .. v8}, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8(Landroid/graphics/Canvas;FFFFI)V

    .line 86
    .line 87
    .line 88
    :cond_4
    iget-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->oO80:Z

    .line 89
    .line 90
    if-eqz v0, :cond_5

    .line 91
    .line 92
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/signature/SignatureView;->o〇〇0〇(Landroid/graphics/Canvas;)V

    .line 93
    .line 94
    .line 95
    :cond_5
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public 〇80〇808〇O(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oo8Oo00oo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇8o8o〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->oO80:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇O00(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->oo〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇O888o0o()Lcom/intsig/camscanner/signature/sharesign/DragOverBoundStrategy;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o8:Lcom/intsig/camscanner/signature/sharesign/DragOverBoundStrategy;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O8o08O()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->oo88o8O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O〇(FF)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇8o8o〇:Landroid/graphics/Matrix;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 7
    .line 8
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o800o8O:[F

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    aget v2, v0, v1

    .line 15
    .line 16
    add-float/2addr v2, p1

    .line 17
    aput v2, v0, v1

    .line 18
    .line 19
    const/4 p1, 0x1

    .line 20
    aget v1, v0, p1

    .line 21
    .line 22
    add-float/2addr v1, p2

    .line 23
    aput v1, v0, p1

    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O8o08O:Landroid/graphics/Matrix;

    .line 26
    .line 27
    iget-object p2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O00:[F

    .line 30
    .line 31
    invoke-virtual {p1, p2, v0}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇o00〇〇Oo(Landroid/graphics/Point;)Lcom/intsig/camscanner/signature/ActionType;
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o〇:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget v0, p1, Landroid/graphics/Point;->x:I

    .line 6
    .line 7
    int-to-float v0, v0

    .line 8
    iget v1, p1, Landroid/graphics/Point;->y:I

    .line 9
    .line 10
    int-to-float v1, v1

    .line 11
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 12
    .line 13
    const/4 v3, 0x4

    .line 14
    aget v3, v2, v3

    .line 15
    .line 16
    const/4 v4, 0x5

    .line 17
    aget v2, v2, v4

    .line 18
    .line 19
    invoke-direct {p0, v0, v1, v3, v2}, Lcom/intsig/camscanner/signature/SignatureView;->oo〇(FFFF)F

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    iget v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00:I

    .line 24
    .line 25
    int-to-float v1, v1

    .line 26
    cmpg-float v0, v0, v1

    .line 27
    .line 28
    if-gez v0, :cond_0

    .line 29
    .line 30
    sget-object p1, Lcom/intsig/camscanner/signature/ActionType;->ActionControl:Lcom/intsig/camscanner/signature/ActionType;

    .line 31
    .line 32
    return-object p1

    .line 33
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o〇:Z

    .line 34
    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    iget-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oo〇:Z

    .line 38
    .line 39
    if-eqz v0, :cond_1

    .line 40
    .line 41
    iget v0, p1, Landroid/graphics/Point;->x:I

    .line 42
    .line 43
    int-to-float v0, v0

    .line 44
    iget v1, p1, Landroid/graphics/Point;->y:I

    .line 45
    .line 46
    int-to-float v1, v1

    .line 47
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 48
    .line 49
    const/4 v3, 0x0

    .line 50
    aget v3, v2, v3

    .line 51
    .line 52
    const/4 v4, 0x1

    .line 53
    aget v2, v2, v4

    .line 54
    .line 55
    invoke-direct {p0, v0, v1, v3, v2}, Lcom/intsig/camscanner/signature/SignatureView;->oo〇(FFFF)F

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    iget v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00:I

    .line 60
    .line 61
    int-to-float v1, v1

    .line 62
    cmpg-float v0, v0, v1

    .line 63
    .line 64
    if-gez v0, :cond_1

    .line 65
    .line 66
    sget-object p1, Lcom/intsig/camscanner/signature/ActionType;->ActionDelete:Lcom/intsig/camscanner/signature/ActionType;

    .line 67
    .line 68
    return-object p1

    .line 69
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o〇:Z

    .line 70
    .line 71
    if-eqz v0, :cond_2

    .line 72
    .line 73
    iget-boolean v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->oO80:Z

    .line 74
    .line 75
    if-eqz v0, :cond_2

    .line 76
    .line 77
    iget v0, p1, Landroid/graphics/Point;->x:I

    .line 78
    .line 79
    int-to-float v0, v0

    .line 80
    iget v1, p1, Landroid/graphics/Point;->y:I

    .line 81
    .line 82
    int-to-float v1, v1

    .line 83
    iget-object v2, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇O〇:[F

    .line 84
    .line 85
    const/4 v3, 0x2

    .line 86
    aget v3, v2, v3

    .line 87
    .line 88
    const/4 v4, 0x3

    .line 89
    aget v2, v2, v4

    .line 90
    .line 91
    invoke-direct {p0, v0, v1, v3, v2}, Lcom/intsig/camscanner/signature/SignatureView;->oo〇(FFFF)F

    .line 92
    .line 93
    .line 94
    move-result v0

    .line 95
    iget v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇00:I

    .line 96
    .line 97
    int-to-float v1, v1

    .line 98
    cmpg-float v0, v0, v1

    .line 99
    .line 100
    if-gez v0, :cond_2

    .line 101
    .line 102
    sget-object p1, Lcom/intsig/camscanner/signature/ActionType;->ActionEdit:Lcom/intsig/camscanner/signature/ActionType;

    .line 103
    .line 104
    return-object p1

    .line 105
    :cond_2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/signature/SignatureView;->Oo8Oo00oo(Landroid/graphics/Point;)Z

    .line 106
    .line 107
    .line 108
    move-result p1

    .line 109
    if-eqz p1, :cond_3

    .line 110
    .line 111
    sget-object p1, Lcom/intsig/camscanner/signature/ActionType;->ActionTouch:Lcom/intsig/camscanner/signature/ActionType;

    .line 112
    .line 113
    return-object p1

    .line 114
    :cond_3
    sget-object p1, Lcom/intsig/camscanner/signature/ActionType;->ActionNone:Lcom/intsig/camscanner/signature/ActionType;

    .line 115
    .line 116
    return-object p1
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public 〇oo〇()[I
    .locals 8

    .line 1
    const-string v0, "TYPE_SIGNATURE_AREA"

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇8:Ljava/lang/String;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x0

    .line 10
    const/4 v2, 0x2

    .line 11
    const/4 v3, 0x1

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    new-array v0, v2, [I

    .line 15
    .line 16
    const/high16 v2, 0x42f00000    # 120.0f

    .line 17
    .line 18
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    aput v2, v0, v1

    .line 23
    .line 24
    const/high16 v1, 0x42700000    # 60.0f

    .line 25
    .line 26
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    aput v1, v0, v3

    .line 31
    .line 32
    return-object v0

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇o00〇〇Oo:Landroid/content/Context;

    .line 34
    .line 35
    const/16 v4, 0x32

    .line 36
    .line 37
    invoke-static {v0, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    iget v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oooo8o0〇:I

    .line 42
    .line 43
    const/high16 v5, 0x3f800000    # 1.0f

    .line 44
    .line 45
    if-lez v4, :cond_1

    .line 46
    .line 47
    iget v6, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇808〇:I

    .line 48
    .line 49
    if-lez v6, :cond_1

    .line 50
    .line 51
    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    .line 52
    .line 53
    .line 54
    move-result v4

    .line 55
    int-to-float v0, v0

    .line 56
    mul-float v0, v0, v5

    .line 57
    .line 58
    int-to-float v4, v4

    .line 59
    div-float/2addr v0, v4

    .line 60
    new-array v2, v2, [I

    .line 61
    .line 62
    iget v4, p0, Lcom/intsig/camscanner/signature/SignatureView;->Oooo8o0〇:I

    .line 63
    .line 64
    int-to-float v4, v4

    .line 65
    mul-float v4, v4, v0

    .line 66
    .line 67
    float-to-int v4, v4

    .line 68
    aput v4, v2, v1

    .line 69
    .line 70
    iget v1, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇〇808〇:I

    .line 71
    .line 72
    int-to-float v1, v1

    .line 73
    mul-float v1, v1, v0

    .line 74
    .line 75
    float-to-int v0, v1

    .line 76
    aput v0, v2, v3

    .line 77
    .line 78
    return-object v2

    .line 79
    :cond_1
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    .line 80
    .line 81
    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 82
    .line 83
    .line 84
    iput-boolean v3, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 85
    .line 86
    iget-object v6, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇oOO8O8:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 87
    .line 88
    invoke-virtual {v6}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v6

    .line 92
    invoke-static {v6, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 93
    .line 94
    .line 95
    iget v6, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 96
    .line 97
    iget v7, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 98
    .line 99
    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    .line 100
    .line 101
    .line 102
    move-result v6

    .line 103
    int-to-float v0, v0

    .line 104
    mul-float v0, v0, v5

    .line 105
    .line 106
    int-to-float v5, v6

    .line 107
    div-float/2addr v0, v5

    .line 108
    new-array v2, v2, [I

    .line 109
    .line 110
    iget v5, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 111
    .line 112
    int-to-float v5, v5

    .line 113
    mul-float v5, v5, v0

    .line 114
    .line 115
    float-to-int v5, v5

    .line 116
    aput v5, v2, v1

    .line 117
    .line 118
    iget v1, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 119
    .line 120
    int-to-float v1, v1

    .line 121
    mul-float v1, v1, v0

    .line 122
    .line 123
    float-to-int v0, v1

    .line 124
    aput v0, v2, v3

    .line 125
    .line 126
    return-object v2
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public 〇o〇()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->O8:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇808〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇O8〇〇o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇〇888()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->o〇8:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇8O0〇8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/signature/SignatureView;->〇0000OOO:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

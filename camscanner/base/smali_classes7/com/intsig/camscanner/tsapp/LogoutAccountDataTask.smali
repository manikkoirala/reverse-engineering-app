.class public Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;
.super Landroid/os/AsyncTask;
.source "LogoutAccountDataTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$LoginOutEvent;,
        Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$InsertListener;,
        Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$LogoutFinishCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private O8:Z

.field private Oo08:Z

.field private oO80:Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$LogoutFinishCallback;

.field private o〇0:Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$InsertListener;

.field private 〇080:Landroid/app/Activity;

.field private 〇o00〇〇Oo:Z

.field private 〇o〇:Lcom/intsig/app/BaseProgressDialog;

.field private 〇〇888:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;ZZ)V
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    .line 1
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;-><init>(Landroid/app/Activity;ZZZLcom/intsig/camscanner/tsapp/LogoutAccountDataTask$InsertListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;ZZLcom/intsig/camscanner/tsapp/LogoutAccountDataTask$InsertListener;)V
    .locals 6

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    .line 2
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;-><init>(Landroid/app/Activity;ZZZLcom/intsig/camscanner/tsapp/LogoutAccountDataTask$InsertListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;ZZZLcom/intsig/camscanner/tsapp/LogoutAccountDataTask$InsertListener;)V
    .locals 1

    .line 3
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->Oo08:Z

    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇〇888:Z

    .line 6
    iput-object p5, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->o〇0:Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$InsertListener;

    .line 7
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇080:Landroid/app/Activity;

    .line 8
    iput-boolean p2, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇o00〇〇Oo:Z

    .line 9
    iput-boolean p3, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->O8:Z

    .line 10
    invoke-static {p1, v0}, Lcom/intsig/utils/DialogUtils;->〇o〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 11
    iget-object p2, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇080:Landroid/app/Activity;

    const p3, 0x7f13008b

    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 12
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇o〇:Lcom/intsig/app/BaseProgressDialog;

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇o〇:Lcom/intsig/app/BaseProgressDialog;

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 14
    iput-boolean p4, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->Oo08:Z

    return-void
.end method

.method public static O8(Landroid/app/Activity;Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$InsertListener;ZZZZ)V
    .locals 13

    .line 1
    move-object v1, p0

    .line 2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 3
    .line 4
    .line 5
    move-result-wide v2

    .line 6
    const-string v4, "LogoutAccountDataTask"

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    const-string v0, "doInsert"

    .line 11
    .line 12
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-interface {p1}, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$InsertListener;->〇080()V

    .line 16
    .line 17
    .line 18
    :cond_0
    invoke-static {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OOo(Landroid/content/Context;)V

    .line 19
    .line 20
    .line 21
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    .line 22
    .line 23
    .line 24
    move-result-object v5

    .line 25
    const-string v6, "Account_ID"

    .line 26
    .line 27
    const-wide/16 v7, -0x1

    .line 28
    .line 29
    invoke-interface {v5, v6, v7, v8}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    .line 30
    .line 31
    .line 32
    move-result-wide v9

    .line 33
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o〇8oOO88()Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-nez v0, :cond_1

    .line 38
    .line 39
    invoke-static {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O〇oO〇oo8o(Landroid/content/Context;)Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_1

    .line 44
    .line 45
    invoke-static {p0}, Lcom/intsig/camscanner/db/dao/DocumentDao;->O0O8OO088(Landroid/content/Context;)V

    .line 46
    .line 47
    .line 48
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o〇()V

    .line 53
    .line 54
    .line 55
    const/4 v11, 0x0

    .line 56
    const/4 v12, 0x0

    .line 57
    cmp-long v0, v9, v7

    .line 58
    .line 59
    if-eqz v0, :cond_4

    .line 60
    .line 61
    new-instance v0, Landroid/content/ContentValues;

    .line 62
    .line 63
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 64
    .line 65
    .line 66
    if-eqz p2, :cond_2

    .line 67
    .line 68
    const/4 v7, 0x2

    .line 69
    goto :goto_0

    .line 70
    :cond_2
    const/4 v7, 0x0

    .line 71
    :goto_0
    const-string v8, "account_state"

    .line 72
    .line 73
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 74
    .line 75
    .line 76
    move-result-object v7

    .line 77
    invoke-virtual {v0, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 78
    .line 79
    .line 80
    sget-object v7, Lcom/intsig/camscanner/provider/Documents$SyncAccount;->〇080:Landroid/net/Uri;

    .line 81
    .line 82
    invoke-static {v7, v9, v10}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 83
    .line 84
    .line 85
    move-result-object v7

    .line 86
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 87
    .line 88
    .line 89
    move-result-object v8

    .line 90
    invoke-virtual {v8, v7, v0, v11, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 91
    .line 92
    .line 93
    if-nez p3, :cond_3

    .line 94
    .line 95
    :try_start_0
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->〇o〇8()V
    :try_end_0
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    .line 97
    .line 98
    goto :goto_1

    .line 99
    :catch_0
    move-exception v0

    .line 100
    move-object v7, v0

    .line 101
    invoke-static {v4, v7}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 102
    .line 103
    .line 104
    :cond_3
    :goto_1
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->〇0000OOO()V

    .line 105
    .line 106
    .line 107
    :cond_4
    const-string v0, ""

    .line 108
    .line 109
    if-eqz p3, :cond_5

    .line 110
    .line 111
    invoke-static {v0}, Lcom/intsig/comm/account_data/AccountPreference;->o0O0(Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    invoke-static {p0, v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇〇888O〇0(Landroid/content/Context;Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    goto :goto_2

    .line 118
    :cond_5
    if-nez p5, :cond_7

    .line 119
    .line 120
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->OoO8()Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v7

    .line 124
    invoke-static {v7}, Lcom/intsig/comm/account_data/AccountPreference;->o0O0(Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o〇O0〇()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v7

    .line 131
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 132
    .line 133
    .line 134
    move-result v8

    .line 135
    if-eqz v8, :cond_6

    .line 136
    .line 137
    move-object v7, v0

    .line 138
    :cond_6
    invoke-static {p0, v7}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇〇888O〇0(Landroid/content/Context;Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    :cond_7
    :goto_2
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 142
    .line 143
    .line 144
    move-result-object v5

    .line 145
    invoke-static {v12}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO〇〇(I)V

    .line 146
    .line 147
    .line 148
    invoke-static {v12}, Lcom/intsig/camscanner/util/PreferenceHelper;->Oo0o(Z)V

    .line 149
    .line 150
    .line 151
    if-eqz p4, :cond_8

    .line 152
    .line 153
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->〇o()Ljava/util/HashMap;

    .line 154
    .line 155
    .line 156
    move-result-object v7

    .line 157
    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    .line 158
    .line 159
    .line 160
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 161
    .line 162
    .line 163
    move-result-object v6

    .line 164
    const-string v7, "account_login_password"

    .line 165
    .line 166
    invoke-interface {v6, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 167
    .line 168
    .line 169
    move-result-object v6

    .line 170
    const-string v7, "token_Password"

    .line 171
    .line 172
    invoke-interface {v6, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 173
    .line 174
    .line 175
    move-result-object v6

    .line 176
    const-string v7, "KEY_NICK_NAME"

    .line 177
    .line 178
    invoke-interface {v6, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 179
    .line 180
    .line 181
    move-result-object v6

    .line 182
    const-string v7, "Account_UID"

    .line 183
    .line 184
    invoke-interface {v6, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 185
    .line 186
    .line 187
    :cond_8
    if-nez p5, :cond_9

    .line 188
    .line 189
    const-string v6, "Area_Code"

    .line 190
    .line 191
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 192
    .line 193
    .line 194
    move-result-object v6

    .line 195
    const-string v7, "Account"

    .line 196
    .line 197
    invoke-interface {v6, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 198
    .line 199
    .line 200
    :cond_9
    if-eqz p3, :cond_a

    .line 201
    .line 202
    invoke-static {p0, v9, v10}, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇080(Landroid/content/Context;J)V

    .line 203
    .line 204
    .line 205
    goto :goto_3

    .line 206
    :cond_a
    invoke-static {p0, v9, v10}, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->Oo08(Landroid/content/Context;J)V

    .line 207
    .line 208
    .line 209
    :goto_3
    new-instance v6, Ljava/lang/StringBuilder;

    .line 210
    .line 211
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 212
    .line 213
    .line 214
    const-string v7, "isLogin after loginOutAccount? "

    .line 215
    .line 216
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    .line 218
    .line 219
    invoke-static {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 220
    .line 221
    .line 222
    move-result v7

    .line 223
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 224
    .line 225
    .line 226
    const-string v7, ", getKeySync? "

    .line 227
    .line 228
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    .line 230
    .line 231
    invoke-static {}, Lcom/intsig/camscanner/app/AppUtil;->〇00()Z

    .line 232
    .line 233
    .line 234
    move-result v7

    .line 235
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 236
    .line 237
    .line 238
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 239
    .line 240
    .line 241
    move-result-object v6

    .line 242
    invoke-static {v4, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    .line 244
    .line 245
    const-string v6, "qp3sdfsdesdfwsvvs20161108"

    .line 246
    .line 247
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 248
    .line 249
    .line 250
    move-result-object v5

    .line 251
    const-string v6, "qp3sdfsdesdfwsvvs20200312"

    .line 252
    .line 253
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 254
    .line 255
    .line 256
    move-result-object v5

    .line 257
    const-string v6, "qp3sdfsdehorizonwsvvs2020032014"

    .line 258
    .line 259
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 260
    .line 261
    .line 262
    move-result-object v5

    .line 263
    const-string v6, "qp3sdfsdehorizonwsvvs2020032813"

    .line 264
    .line 265
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 266
    .line 267
    .line 268
    move-result-object v5

    .line 269
    const-string v6, "KEY_SYNC"

    .line 270
    .line 271
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 272
    .line 273
    .line 274
    move-result-object v5

    .line 275
    const-string v6, "keysetspecialaccount"

    .line 276
    .line 277
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 278
    .line 279
    .line 280
    move-result-object v5

    .line 281
    const-string v6, "key_last_account_storage"

    .line 282
    .line 283
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 284
    .line 285
    .line 286
    move-result-object v5

    .line 287
    const-string v6, "qp3sdjd30renew02sd"

    .line 288
    .line 289
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 290
    .line 291
    .line 292
    move-result-object v5

    .line 293
    const-string v6, "qp3sddfa0renewergw"

    .line 294
    .line 295
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 296
    .line 297
    .line 298
    move-result-object v5

    .line 299
    const-string v6, "qp3sdnex3initial2time02sd"

    .line 300
    .line 301
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 302
    .line 303
    .line 304
    move-result-object v5

    .line 305
    const-string v6, "qp3sdnex3rwmethod02sd"

    .line 306
    .line 307
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 308
    .line 309
    .line 310
    move-result-object v5

    .line 311
    const-string v6, "qp3sdjd79xhdas02sd"

    .line 312
    .line 313
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 314
    .line 315
    .line 316
    move-result-object v5

    .line 317
    const-string v6, "tkreds3sdvv22ccsx3xd3"

    .line 318
    .line 319
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 320
    .line 321
    .line 322
    move-result-object v5

    .line 323
    const-string v6, "33xd5adju9elexedadsxln"

    .line 324
    .line 325
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 326
    .line 327
    .line 328
    move-result-object v5

    .line 329
    const-string v6, "t43543fgdfsfdfweweffsfsdfe"

    .line 330
    .line 331
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 332
    .line 333
    .line 334
    move-result-object v5

    .line 335
    const-string v6, "t121212121sffewdfweefe"

    .line 336
    .line 337
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 338
    .line 339
    .line 340
    move-result-object v5

    .line 341
    const-string v6, "tafdseddfeasfeafaewf"

    .line 342
    .line 343
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 344
    .line 345
    .line 346
    move-result-object v5

    .line 347
    const-string v6, "key_current_icon_tag"

    .line 348
    .line 349
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 350
    .line 351
    .line 352
    move-result-object v5

    .line 353
    const-string v6, "dgkggrigjporgjghh"

    .line 354
    .line 355
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 356
    .line 357
    .line 358
    move-result-object v5

    .line 359
    const-string v6, "fagagiolnglangnae"

    .line 360
    .line 361
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 362
    .line 363
    .line 364
    move-result-object v5

    .line 365
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 366
    .line 367
    .line 368
    invoke-static {v11}, Lcom/intsig/camscanner/tsapp/account/util/AccountUtil;->〇〇8O0〇8(Lcom/intsig/tianshu/UserInfo;)V

    .line 369
    .line 370
    .line 371
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇O8O(Ljava/lang/String;)V

    .line 372
    .line 373
    .line 374
    invoke-static {v12}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇O〇〇O8(Z)Z

    .line 375
    .line 376
    .line 377
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 378
    .line 379
    .line 380
    move-result v5

    .line 381
    invoke-static {v5}, Lcom/intsig/camscanner/launch/CsApplication;->〇O〇80o08O(Z)V

    .line 382
    .line 383
    .line 384
    sget-object v5, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇:Ljava/lang/String;

    .line 385
    .line 386
    invoke-static {v11, v5}, Lcom/intsig/camscanner/log/LogAgentData;->〇oo〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    .line 388
    .line 389
    invoke-static {p0, v12}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oO0〇〇O8o(Landroid/content/Context;Z)V

    .line 390
    .line 391
    .line 392
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 393
    .line 394
    .line 395
    move-result-object v5

    .line 396
    const-string v6, "key_need_show_security"

    .line 397
    .line 398
    invoke-virtual {v5, v6, v12}, Lcom/intsig/utils/PreferenceUtil;->〇O00(Ljava/lang/String;Z)V

    .line 399
    .line 400
    .line 401
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oo88o8O()V

    .line 402
    .line 403
    .line 404
    invoke-static {p0, v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O00o8o(Landroid/content/Context;Ljava/lang/String;)V

    .line 405
    .line 406
    .line 407
    const-wide/16 v5, 0x0

    .line 408
    .line 409
    invoke-static {p0, v5, v6}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo0〇o〇(Landroid/content/Context;J)V

    .line 410
    .line 411
    .line 412
    invoke-static {p0}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->oO80(Landroid/content/Context;)V

    .line 413
    .line 414
    .line 415
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 416
    .line 417
    .line 418
    move-result-object v0

    .line 419
    invoke-virtual {v0}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o00〇〇Oo()V

    .line 420
    .line 421
    .line 422
    invoke-static {}, Landroid/webkit/WebStorage;->getInstance()Landroid/webkit/WebStorage;

    .line 423
    .line 424
    .line 425
    move-result-object v0

    .line 426
    invoke-virtual {v0}, Landroid/webkit/WebStorage;->deleteAllData()V

    .line 427
    .line 428
    .line 429
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 430
    .line 431
    .line 432
    move-result-object v0

    .line 433
    const-string v5, "CS_ACTIVITY_PRODUCT"

    .line 434
    .line 435
    invoke-virtual {v0, v5}, Lcom/intsig/utils/PreferenceUtil;->o〇O8〇〇o(Ljava/lang/String;)V

    .line 436
    .line 437
    .line 438
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 439
    .line 440
    .line 441
    move-result-object v0

    .line 442
    const/4 v5, 0x1

    .line 443
    invoke-virtual {v0, p0, v5}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->Oooo8o0〇(Landroid/content/Context;Z)V

    .line 444
    .line 445
    .line 446
    invoke-static {v11}, Lcom/intsig/camscanner/message/ApiChangeReqWrapper;->〇O8o08O(Lkotlin/jvm/functions/Function0;)V

    .line 447
    .line 448
    .line 449
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o0oO()V

    .line 450
    .line 451
    .line 452
    invoke-static {v12}, Lcom/intsig/comm/account_data/AccountPreference;->〇O〇80o08O(Z)V

    .line 453
    .line 454
    .line 455
    invoke-static {p0}, Lcom/intsig/tsapp/account/login_task/GoogleLoginControl;->〇O〇(Landroid/app/Activity;)V

    .line 456
    .line 457
    .line 458
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->Oo08:Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;

    .line 459
    .line 460
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;->Oo08()Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;

    .line 461
    .line 462
    .line 463
    move-result-object v0

    .line 464
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->〇o〇()V

    .line 465
    .line 466
    .line 467
    sget-object v0, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇080:Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;

    .line 468
    .line 469
    invoke-virtual {v0}, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇O00()V

    .line 470
    .line 471
    .line 472
    new-instance v0, Lcom/intsig/camscanner/backup/BackUpStatus$Init;

    .line 473
    .line 474
    sget-object v1, Lcom/intsig/camscanner/backup/BackUpHelper;->〇080:Lcom/intsig/camscanner/backup/BackUpHelper;

    .line 475
    .line 476
    invoke-virtual {v1}, Lcom/intsig/camscanner/backup/BackUpHelper;->〇〇888()Z

    .line 477
    .line 478
    .line 479
    move-result v1

    .line 480
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/backup/BackUpStatus$Init;-><init>(Z)V

    .line 481
    .line 482
    .line 483
    invoke-static {v0}, Lcom/intsig/camscanner/backup/BackUpManager;->o8(Lcom/intsig/camscanner/backup/BackUpStatus;)V

    .line 484
    .line 485
    .line 486
    new-instance v0, Ljava/lang/StringBuilder;

    .line 487
    .line 488
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 489
    .line 490
    .line 491
    const-string v1, "handleInBackground costTime="

    .line 492
    .line 493
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 494
    .line 495
    .line 496
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 497
    .line 498
    .line 499
    move-result-wide v5

    .line 500
    sub-long/2addr v5, v2

    .line 501
    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 502
    .line 503
    .line 504
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 505
    .line 506
    .line 507
    move-result-object v0

    .line 508
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    .line 510
    .line 511
    return-void
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
.end method

.method private static Oo08(Landroid/content/Context;J)V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/ContentValues;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x2

    .line 7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const-string v2, "account_state"

    .line 12
    .line 13
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 14
    .line 15
    .line 16
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$SyncAccount;->〇080:Landroid/net/Uri;

    .line 17
    .line 18
    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 23
    .line 24
    .line 25
    move-result-object p0

    .line 26
    const/4 p2, 0x0

    .line 27
    invoke-virtual {p0, p1, v0, p2, p2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static 〇080(Landroid/content/Context;J)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$SyncAccount;->〇080:Landroid/net/Uri;

    .line 2
    .line 3
    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    const/4 v1, 0x0

    .line 12
    invoke-virtual {p0, v0, v1, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 13
    .line 14
    .line 15
    move-result p0

    .line 16
    new-instance v0, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v1, "cancelAccount accountId="

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    const-string p1, " number="

    .line 30
    .line 31
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p0

    .line 41
    const-string p1, "LogoutAccountDataTask"

    .line 42
    .line 43
    invoke-static {p1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Boolean;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇o00〇〇Oo([Ljava/lang/Boolean;)Ljava/lang/Void;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->o〇0(Ljava/lang/Void;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected onPreExecute()V
    .locals 2

    .line 1
    const-string v0, "onPreExecute"

    .line 2
    .line 3
    const-string v1, "LogoutAccountDataTask"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    :try_start_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->Oo08:Z

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :catch_0
    move-exception v0

    .line 19
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    :goto_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected o〇0(Ljava/lang/Void;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    const-string p1, "onPostExecute"

    .line 5
    .line 6
    const-string v0, "LogoutAccountDataTask"

    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    :try_start_0
    iget-boolean p1, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->Oo08:Z

    .line 12
    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :catch_0
    nop

    .line 22
    :cond_0
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇080:Landroid/app/Activity;

    .line 23
    .line 24
    invoke-static {p1}, Lcom/intsig/camscanner/app/AppSwitch;->〇0〇O0088o(Landroid/content/Context;)Z

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    if-eqz p1, :cond_1

    .line 29
    .line 30
    :try_start_1
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇080:Landroid/app/Activity;

    .line 31
    .line 32
    invoke-static {p1}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->oO80(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 33
    .line 34
    .line 35
    goto :goto_1

    .line 36
    :catch_1
    move-exception p1

    .line 37
    const-string v1, "onPostExecute requireLoginFeature "

    .line 38
    .line 39
    invoke-static {v0, v1, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 40
    .line 41
    .line 42
    :cond_1
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->oO80:Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$LogoutFinishCallback;

    .line 43
    .line 44
    if-eqz p1, :cond_2

    .line 45
    .line 46
    invoke-interface {p1}, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$LogoutFinishCallback;->〇080()V

    .line 47
    .line 48
    .line 49
    :cond_2
    iget-boolean p1, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇o00〇〇Oo:Z

    .line 50
    .line 51
    if-eqz p1, :cond_3

    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇080:Landroid/app/Activity;

    .line 54
    .line 55
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 56
    .line 57
    .line 58
    :cond_3
    new-instance p1, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$LoginOutEvent;

    .line 59
    .line 60
    invoke-direct {p1}, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$LoginOutEvent;-><init>()V

    .line 61
    .line 62
    .line 63
    invoke-static {p1}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 64
    .line 65
    .line 66
    sget-object p1, Lcom/intsig/camscanner/message/MessageClient;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/message/MessageClient$Companion;

    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/camscanner/message/MessageClient$Companion;->〇080()Lcom/intsig/camscanner/message/MessageClient;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    const/4 v0, -0x1

    .line 73
    const/4 v1, 0x0

    .line 74
    invoke-virtual {p1, v1, v0}, Lcom/intsig/camscanner/message/MessageClient;->〇O8o08O(Ljava/lang/String;I)Z

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    if-nez p1, :cond_4

    .line 79
    .line 80
    invoke-static {v1}, Lcom/intsig/camscanner/message/ApiChangeReqWrapper;->〇O8o08O(Lkotlin/jvm/functions/Function0;)V

    .line 81
    .line 82
    .line 83
    :cond_4
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected varargs 〇o00〇〇Oo([Ljava/lang/Boolean;)Ljava/lang/Void;
    .locals 8

    .line 1
    const-string v0, "LogoutAccountDataTask"

    .line 2
    .line 3
    const-string v1, "doInBackground"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇080:Landroid/app/Activity;

    .line 9
    .line 10
    iget-object v3, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->o〇0:Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$InsertListener;

    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    aget-object p1, p1, v0

    .line 14
    .line 15
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 16
    .line 17
    .line 18
    move-result v4

    .line 19
    iget-boolean v5, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->〇〇888:Z

    .line 20
    .line 21
    iget-boolean v6, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->O8:Z

    .line 22
    .line 23
    const/4 v7, 0x0

    .line 24
    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->O8(Landroid/app/Activity;Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$InsertListener;ZZZZ)V

    .line 25
    .line 26
    .line 27
    const/4 p1, 0x0

    .line 28
    return-object p1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇o〇(Z)V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x1

    .line 6
    new-array v1, v1, [Ljava/lang/Boolean;

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    aput-object p1, v1, v2

    .line 14
    .line 15
    invoke-virtual {p0, v0, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇〇888(Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$LogoutFinishCallback;)Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask;->oO80:Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$LogoutFinishCallback;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

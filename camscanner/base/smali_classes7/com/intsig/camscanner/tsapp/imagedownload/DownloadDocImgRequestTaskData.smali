.class public final Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;
.super Lcom/intsig/camscanner/tsapp/request/RequestTaskData;
.source "DownloadDocImgRequestTaskData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o〇0:Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇888:[Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:J

.field private final Oo08:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;->o〇0:Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData$Companion;

    .line 8
    .line 9
    const-string v0, "office_first_page_id"

    .line 10
    .line 11
    const-string v1, "office_thumb_sync_state"

    .line 12
    .line 13
    const-string v2, "sync_doc_id"

    .line 14
    .line 15
    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    sput-object v0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;->〇〇888:[Ljava/lang/String;

    .line 20
    .line 21
    return-void
.end method

.method public constructor <init>(JJILcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;)V
    .locals 0

    .line 3
    invoke-direct {p0, p3, p4, p5}, Lcom/intsig/camscanner/tsapp/request/RequestTaskData;-><init>(JI)V

    .line 4
    iput-wide p1, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;->O8:J

    .line 5
    iput-object p6, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;->Oo08:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;

    return-void
.end method

.method public synthetic constructor <init>(JJILcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_0

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p3

    :cond_0
    move-wide v3, p3

    and-int/lit8 p3, p7, 0x4

    if-eqz p3, :cond_1

    const/4 p5, 0x1

    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    move v5, p5

    :goto_0
    and-int/lit8 p3, p7, 0x8

    if-eqz p3, :cond_2

    const/4 p6, 0x0

    :cond_2
    move-object v6, p6

    move-object v0, p0

    move-wide v1, p1

    .line 2
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;-><init>(JJILcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;)V

    return-void
.end method


# virtual methods
.method public final O8(JLcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;)V
    .locals 10

    .line 1
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 2
    .line 3
    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 4
    .line 5
    .line 6
    move-result-object v2

    .line 7
    const-string v0, "withAppendedId(Documents\u2026ument.CONTENT_URI, docId)"

    .line 8
    .line 9
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    sget-object v3, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;->〇〇888:[Ljava/lang/String;

    .line 23
    .line 24
    const/4 v4, 0x0

    .line 25
    const/4 v5, 0x0

    .line 26
    const/4 v6, 0x0

    .line 27
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    const/4 v1, 0x1

    .line 32
    const/4 v2, 0x0

    .line 33
    const/4 v3, 0x0

    .line 34
    if-eqz v0, :cond_1

    .line 35
    .line 36
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 37
    .line 38
    .line 39
    move-result v4

    .line 40
    if-eqz v4, :cond_0

    .line 41
    .line 42
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    const/4 v4, 0x2

    .line 50
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    .line 51
    .line 52
    .line 53
    move-result v4

    .line 54
    goto :goto_0

    .line 55
    :cond_0
    const/4 v4, 0x0

    .line 56
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 57
    .line 58
    .line 59
    goto :goto_1

    .line 60
    :cond_1
    const/4 v4, 0x0

    .line 61
    :goto_1
    if-nez v2, :cond_2

    .line 62
    .line 63
    return-void

    .line 64
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇080:Lcom/intsig/camscanner/office_doc/util/OfficeUtils;

    .line 65
    .line 66
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 71
    .line 72
    .line 73
    move-result v5

    .line 74
    if-eqz v5, :cond_4

    .line 75
    .line 76
    new-instance v5, Ljava/io/File;

    .line 77
    .line 78
    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {v5}, Ljava/io/File;->length()J

    .line 82
    .line 83
    .line 84
    move-result-wide v5

    .line 85
    const-wide/16 v7, 0x0

    .line 86
    .line 87
    cmp-long v9, v5, v7

    .line 88
    .line 89
    if-eqz v9, :cond_3

    .line 90
    .line 91
    return-void

    .line 92
    :cond_3
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 93
    .line 94
    .line 95
    :cond_4
    const-string v5, "DownloadDocImgRequestTaskData"

    .line 96
    .line 97
    if-ne v4, v1, :cond_5

    .line 98
    .line 99
    new-instance p1, Ljava/lang/StringBuilder;

    .line 100
    .line 101
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    .line 103
    .line 104
    const-string p2, "officeFirstPageId:"

    .line 105
    .line 106
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    const-string p2, ", jpgSyncState STATUS_ADD "

    .line 113
    .line 114
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object p1

    .line 121
    invoke-static {v5, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    return-void

    .line 125
    :cond_5
    const/4 v4, 0x6

    .line 126
    :try_start_0
    sget-object v6, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;

    .line 127
    .line 128
    invoke-virtual {v6, p1, p2, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->O8〇o(JI)V

    .line 129
    .line 130
    .line 131
    sget-object v1, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncApi;->〇080:Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncApi;

    .line 132
    .line 133
    new-instance v7, Ljava/lang/StringBuilder;

    .line 134
    .line 135
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 136
    .line 137
    .line 138
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 139
    .line 140
    .line 141
    const-string v8, ".jpg"

    .line 142
    .line 143
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    .line 145
    .line 146
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 147
    .line 148
    .line 149
    move-result-object v7

    .line 150
    invoke-virtual {v1, v7, v0}, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncApi;->〇o〇(Ljava/lang/String;Ljava/lang/String;)[I

    .line 151
    .line 152
    .line 153
    move-result-object v0

    .line 154
    aget v1, v0, v3

    .line 155
    .line 156
    if-lez v1, :cond_6

    .line 157
    .line 158
    if-eqz p3, :cond_6

    .line 159
    .line 160
    invoke-interface {p3}, Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;->〇Oo〇o8()V

    .line 161
    .line 162
    .line 163
    :cond_6
    new-instance p3, Ljava/lang/StringBuilder;

    .line 164
    .line 165
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 166
    .line 167
    .line 168
    const-string v1, "downloadImageFile pageSyncId:"

    .line 169
    .line 170
    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    .line 172
    .line 173
    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 174
    .line 175
    .line 176
    const-string v1, ",, res:"

    .line 177
    .line 178
    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    .line 180
    .line 181
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 182
    .line 183
    .line 184
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 185
    .line 186
    .line 187
    move-result-object p3

    .line 188
    invoke-static {v5, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    .line 190
    .line 191
    invoke-virtual {v6, p1, p2, v4}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->O8〇o(JI)V

    .line 192
    .line 193
    .line 194
    goto :goto_2

    .line 195
    :catchall_0
    move-exception p3

    .line 196
    goto :goto_3

    .line 197
    :catch_0
    move-exception p3

    .line 198
    :try_start_1
    invoke-static {v5, p3}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 199
    .line 200
    .line 201
    sget-object p3, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;

    .line 202
    .line 203
    invoke-virtual {p3, p1, p2, v4}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->O8〇o(JI)V

    .line 204
    .line 205
    .line 206
    :goto_2
    return-void

    .line 207
    :goto_3
    sget-object v0, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;

    .line 208
    .line 209
    invoke-virtual {v0, p1, p2, v4}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->O8〇o(JI)V

    .line 210
    .line 211
    .line 212
    throw p3
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    instance-of v1, p1, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;

    .line 6
    .line 7
    if-eqz v1, :cond_1

    .line 8
    .line 9
    move-object v1, p1

    .line 10
    check-cast v1, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;

    .line 11
    .line 12
    iget-wide v1, v1, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;->O8:J

    .line 13
    .line 14
    iget-wide v3, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;->O8:J

    .line 15
    .line 16
    cmp-long v5, v1, v3

    .line 17
    .line 18
    if-nez v5, :cond_1

    .line 19
    .line 20
    return v0

    .line 21
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    return p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇080(Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataCallback;)V
    .locals 5

    .line 1
    const/4 v0, 0x1

    .line 2
    const/4 v1, 0x0

    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    invoke-interface {p1}, Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataCallback;->〇080()Z

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    if-ne v2, v0, :cond_0

    .line 10
    .line 11
    const/4 v2, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v2, 0x0

    .line 14
    :goto_0
    if-eqz v2, :cond_1

    .line 15
    .line 16
    return-void

    .line 17
    :cond_1
    if-eqz p1, :cond_2

    .line 18
    .line 19
    invoke-interface {p1}, Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataCallback;->〇o00〇〇Oo()Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-ne v2, v0, :cond_2

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_2
    const/4 v0, 0x0

    .line 27
    :goto_1
    if-eqz v0, :cond_3

    .line 28
    .line 29
    invoke-interface {p1, p0}, Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataCallback;->O8(Lcom/intsig/camscanner/tsapp/request/RequestTaskData;)V

    .line 30
    .line 31
    .line 32
    return-void

    .line 33
    :cond_3
    iget-wide v0, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;->O8:J

    .line 34
    .line 35
    new-instance v2, Ljava/lang/StringBuilder;

    .line 36
    .line 37
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 38
    .line 39
    .line 40
    const-string v3, "execute docId:"

    .line 41
    .line 42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    const-string v1, "DownloadDocImgRequestTaskData"

    .line 53
    .line 54
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    iget-wide v0, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;->O8:J

    .line 58
    .line 59
    const-wide/16 v2, 0x0

    .line 60
    .line 61
    cmp-long v4, v0, v2

    .line 62
    .line 63
    if-ltz v4, :cond_4

    .line 64
    .line 65
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;->Oo08:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;

    .line 66
    .line 67
    invoke-virtual {p0, v0, v1, v2}, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;->O8(JLcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;)V

    .line 68
    .line 69
    .line 70
    :cond_4
    if-eqz p1, :cond_5

    .line 71
    .line 72
    const/4 v0, 0x0

    .line 73
    invoke-interface {p1, v0, v0}, Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataCallback;->〇o〇(FF)V

    .line 74
    .line 75
    .line 76
    :cond_5
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.class public final Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;
.super Lcom/intsig/camscanner/tsapp/request/RequestTaskData;
.source "DownloadPageRequestTaskData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$OnImageCompleteListener;,
        Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final oO80:Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇80〇808〇O:[Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:J

.field private final Oo08:Z

.field private final o〇0:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;

.field private 〇〇888:Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$OnImageCompleteListener;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->oO80:Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$Companion;

    .line 8
    .line 9
    const-string v0, "sync_state"

    .line 10
    .line 11
    const-string v1, "sync_timestamp"

    .line 12
    .line 13
    const-string v2, "sync_image_id"

    .line 14
    .line 15
    const-string v3, "_data"

    .line 16
    .line 17
    const-string v4, "thumb_data"

    .line 18
    .line 19
    filled-new-array {v2, v3, v4, v0, v1}, [Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    sput-object v0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->〇80〇808〇O:[Ljava/lang/String;

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>(JJZILcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;)V
    .locals 0

    .line 3
    invoke-direct {p0, p3, p4, p6}, Lcom/intsig/camscanner/tsapp/request/RequestTaskData;-><init>(JI)V

    .line 4
    iput-wide p1, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->O8:J

    .line 5
    iput-boolean p5, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->Oo08:Z

    .line 6
    iput-object p7, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->o〇0:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;

    return-void
.end method

.method public synthetic constructor <init>(JJZILcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 10

    and-int/lit8 v0, p8, 0x2

    if-eqz v0, :cond_0

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    move-wide v5, v0

    goto :goto_0

    :cond_0
    move-wide v5, p3

    :goto_0
    and-int/lit8 v0, p8, 0x4

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    const/4 v7, 0x0

    goto :goto_1

    :cond_1
    move v7, p5

    :goto_1
    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    const/4 v8, 0x2

    goto :goto_2

    :cond_2
    move/from16 v8, p6

    :goto_2
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    move-object v9, v0

    goto :goto_3

    :cond_3
    move-object/from16 v9, p7

    :goto_3
    move-object v2, p0

    move-wide v3, p1

    .line 2
    invoke-direct/range {v2 .. v9}, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;-><init>(JJZILcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;)V

    return-void
.end method

.method public static final synthetic O8()[Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->〇80〇808〇O:[Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final Oo08()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->O8:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    instance-of v1, p1, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;

    .line 6
    .line 7
    if-eqz v1, :cond_1

    .line 8
    .line 9
    move-object v1, p1

    .line 10
    check-cast v1, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;

    .line 11
    .line 12
    iget-wide v1, v1, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->O8:J

    .line 13
    .line 14
    iget-wide v3, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->O8:J

    .line 15
    .line 16
    cmp-long v5, v1, v3

    .line 17
    .line 18
    if-nez v5, :cond_1

    .line 19
    .line 20
    return v0

    .line 21
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    return p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public final o〇0()Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->o〇0:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080(Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataCallback;)V
    .locals 9

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->O8:J

    .line 2
    .line 3
    new-instance v2, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v3, "execute pageId:"

    .line 9
    .line 10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "DownloadPageRequestTaskData"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->o〇0:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;

    .line 26
    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    iget-wide v1, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->O8:J

    .line 30
    .line 31
    invoke-interface {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;->oO00〇o(J)V

    .line 32
    .line 33
    .line 34
    :cond_0
    iget-wide v4, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->O8:J

    .line 35
    .line 36
    const-wide/16 v0, 0x0

    .line 37
    .line 38
    cmp-long v2, v4, v0

    .line 39
    .line 40
    if-ltz v2, :cond_1

    .line 41
    .line 42
    sget-object v3, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->oO80:Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$Companion;

    .line 43
    .line 44
    new-instance v6, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$execute$1;

    .line 45
    .line 46
    invoke-direct {v6, p0, p1}, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$execute$1;-><init>(Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataCallback;)V

    .line 47
    .line 48
    .line 49
    iget-boolean v7, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->Oo08:Z

    .line 50
    .line 51
    iget-object v8, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->o〇0:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;

    .line 52
    .line 53
    invoke-virtual/range {v3 .. v8}, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$Companion;->〇o00〇〇Oo(JLcom/intsig/tianshu/TianShuAPI$OnProgressListener;ZLcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;)V

    .line 54
    .line 55
    .line 56
    :cond_1
    if-eqz p1, :cond_2

    .line 57
    .line 58
    const/4 v0, 0x0

    .line 59
    invoke-interface {p1, v0, v0}, Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataCallback;->〇o〇(FF)V

    .line 60
    .line 61
    .line 62
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->o〇0:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;

    .line 63
    .line 64
    if-eqz p1, :cond_3

    .line 65
    .line 66
    iget-wide v0, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->O8:J

    .line 67
    .line 68
    const/high16 v2, 0x42c80000    # 100.0f

    .line 69
    .line 70
    invoke-interface {p1, v0, v1, v2}, Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;->O〇Oo(JF)V

    .line 71
    .line 72
    .line 73
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->〇〇888:Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$OnImageCompleteListener;

    .line 74
    .line 75
    if-eqz p1, :cond_4

    .line 76
    .line 77
    invoke-interface {p1}, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$OnImageCompleteListener;->onComplete()V

    .line 78
    .line 79
    .line 80
    :cond_4
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public final 〇〇888(Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$OnImageCompleteListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->〇〇888:Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$OnImageCompleteListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

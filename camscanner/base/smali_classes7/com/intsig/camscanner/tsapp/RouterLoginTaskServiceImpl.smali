.class public Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;
.super Lcom/intsig/router/service/BaseRouterServiceImpl;
.source "RouterLoginTaskServiceImpl.java"

# interfaces
.implements Lcom/intsig/router/service/RouterLoginTaskService;


# annotations
.annotation build Lcom/alibaba/android/arouter/facade/annotation/Route;
    name = "Login_TASK"
    path = "/main/login_task"
.end annotation


# static fields
.field public static final SPECIAL_ACCOUT_EDU:I = 0x1

.field private static final TAG:Ljava/lang/String; = "LoginTask"


# instance fields
.field final Fail:I

.field final Success:I

.field errorMsg:Ljava/lang/String;

.field private mAccount:Ljava/lang/String;

.field private mAreaCode:Ljava/lang/String;

.field private mEduMsg:Lcom/intsig/tsapp/account/model/EduMsg;

.field private mIntentAction:Ljava/lang/String;

.field private mIsChangeAccount:Z

.field private mIsNeedShowVipDlg:Z

.field private mLoginTaskListener:Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;

.field private mOperateGuideModels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/operategrowth/OperateGuideModel;",
            ">;"
        }
    .end annotation
.end field

.field private mPasswords:Ljava/lang/String;

.field private mShareMsg:Lcom/intsig/tsapp/account/model/ShareMsg;

.field private mSpecialAccoutType:I

.field private mTag:Ljava/lang/String;

.field mUid:Ljava/lang/String;

.field private mVipMsg:Lcom/intsig/tsapp/account/model/EduMsg;

.field private needSync:Z

.field private personalSync:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/router/service/BaseRouterServiceImpl;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, -0x1

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mSpecialAccoutType:I

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    iput v1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->Success:I

    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->Fail:I

    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mEduMsg:Lcom/intsig/tsapp/account/model/EduMsg;

    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mShareMsg:Lcom/intsig/tsapp/account/model/ShareMsg;

    .line 16
    .line 17
    const-string v2, "LoginTask"

    .line 18
    .line 19
    iput-object v2, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 20
    .line 21
    iput-boolean v1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mIsChangeAccount:Z

    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->personalSync:Ljava/lang/String;

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mVipMsg:Lcom/intsig/tsapp/account/model/EduMsg;

    .line 26
    .line 27
    iput-boolean v1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->needSync:Z

    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private cacheApiInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "cacheApiInfo>>> account = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string v1, " areaCode = "

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string v1, " uid = "

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const-string v1, "LoginTask"

    .line 35
    .line 36
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-nez v0, :cond_0

    .line 44
    .line 45
    invoke-static {p1}, Lcom/intsig/tsapp/account/util/AccountUtils;->O8〇o(Ljava/lang/String;)Z

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    if-nez v0, :cond_0

    .line 50
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    .line 52
    .line 53
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 67
    .line 68
    .line 69
    move-result p2

    .line 70
    if-eqz p2, :cond_1

    .line 71
    .line 72
    const-string p1, "CACHE_KEY is empty"

    .line 73
    .line 74
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    return-void

    .line 78
    :cond_1
    invoke-static {p1}, Lcom/intsig/tianshu/utils/UserInfoSettingUtil;->Oo08(Ljava/lang/String;)Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 83
    .line 84
    .line 85
    move-result p2

    .line 86
    if-eqz p2, :cond_2

    .line 87
    .line 88
    const-string p1, "cacheLoginApiInfo is empty"

    .line 89
    .line 90
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    return-void

    .line 94
    :cond_2
    invoke-static {p3, p1}, Lcom/intsig/tianshu/utils/UserInfoSettingUtil;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    invoke-static {p1}, Lcom/intsig/tianshu/UserInfo;->updateApisByServer(Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private checkLoginAccountIsSpecial()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/AppUtil;->ooo〇8oO(Landroid/content/Context;Z)V

    .line 5
    .line 6
    .line 7
    iget v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mSpecialAccoutType:I

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    if-eq v0, v1, :cond_1

    .line 11
    .line 12
    const/4 v3, 0x2

    .line 13
    if-ne v0, v3, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    goto :goto_1

    .line 18
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 19
    :goto_1
    iget-object v3, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 20
    .line 21
    new-instance v4, Ljava/lang/StringBuilder;

    .line 22
    .line 23
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 24
    .line 25
    .line 26
    const-string v5, "checkLoginAccountIsSpecial showSpecialDialog="

    .line 27
    .line 28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v4

    .line 38
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    if-eqz v0, :cond_2

    .line 42
    .line 43
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o〇8oOO88()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-nez v0, :cond_2

    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 50
    .line 51
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O〇oO〇oo8o(Landroid/content/Context;)Z

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-eqz v0, :cond_2

    .line 56
    .line 57
    goto :goto_2

    .line 58
    :cond_2
    const/4 v1, 0x0

    .line 59
    :goto_2
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 60
    .line 61
    new-instance v2, Ljava/lang/StringBuilder;

    .line 62
    .line 63
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 64
    .line 65
    .line 66
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v2

    .line 76
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 84
    .line 85
    new-instance v3, Ljava/lang/StringBuilder;

    .line 86
    .line 87
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    .line 89
    .line 90
    const-string v4, "checkLoginAccountIsSpecial isVip "

    .line 91
    .line 92
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v3

    .line 102
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    if-eqz v1, :cond_4

    .line 106
    .line 107
    if-nez v0, :cond_4

    .line 108
    .line 109
    iget-object v0, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 110
    .line 111
    invoke-static {v0}, Lcom/intsig/camscanner/app/DBUtil;->O88o〇(Landroid/content/Context;)V

    .line 112
    .line 113
    .line 114
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mLoginTaskListener:Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;

    .line 115
    .line 116
    instance-of v1, v0, Lcom/intsig/tsapp/account/login_task/LoginTaskListener;

    .line 117
    .line 118
    if-eqz v1, :cond_3

    .line 119
    .line 120
    check-cast v0, Lcom/intsig/tsapp/account/login_task/LoginTaskListener;

    .line 121
    .line 122
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mUid:Ljava/lang/String;

    .line 123
    .line 124
    iget v2, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mSpecialAccoutType:I

    .line 125
    .line 126
    iget-object v3, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mShareMsg:Lcom/intsig/tsapp/account/model/ShareMsg;

    .line 127
    .line 128
    iget-object v4, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mEduMsg:Lcom/intsig/tsapp/account/model/EduMsg;

    .line 129
    .line 130
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/intsig/tsapp/account/login_task/LoginTaskListener;->showSpecialDialog(Ljava/lang/String;ILcom/intsig/tsapp/account/model/ShareMsg;Lcom/intsig/tsapp/account/model/EduMsg;)V

    .line 131
    .line 132
    .line 133
    goto :goto_3

    .line 134
    :cond_3
    if-eqz v0, :cond_5

    .line 135
    .line 136
    invoke-interface {v0}, Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;->onLoginFinish()V

    .line 137
    .line 138
    .line 139
    goto :goto_3

    .line 140
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mLoginTaskListener:Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;

    .line 141
    .line 142
    if-eqz v0, :cond_5

    .line 143
    .line 144
    invoke-interface {v0}, Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;->onLoginFinish()V

    .line 145
    .line 146
    .line 147
    :cond_5
    :goto_3
    return-void
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private getAreaCodeFromUserInfo()Ljava/lang/String;
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const-string v2, "LoginTask"

    .line 7
    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    const-string v0, "userInfo is null"

    .line 11
    .line 12
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-object v1

    .line 16
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/tianshu/UserInfo;->getProfile()Lcom/intsig/tianshu/UserInfo$Profile;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    const-string v0, "profile is null"

    .line 23
    .line 24
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-object v1

    .line 28
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/tianshu/UserInfo$Profile;->getAreaCode()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    new-instance v1, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v3, "getAreaCodeFromUserInfo >>> areaCode = "

    .line 38
    .line 39
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    return-object v0
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private inheritProperty()V
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lorg/json/JSONObject;

    .line 6
    .line 7
    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 8
    .line 9
    .line 10
    :try_start_0
    const-string v2, "cs_ept_f"

    .line 11
    .line 12
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇〇888()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 17
    .line 18
    .line 19
    const-string v2, "token"

    .line 20
    .line 21
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 22
    .line 23
    .line 24
    const-string v0, ""

    .line 25
    .line 26
    invoke-static {}, Lcom/intsig/huaweipaylib/HuaweiPayConfig;->〇o00〇〇Oo()Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-eqz v2, :cond_0

    .line 31
    .line 32
    sget-object v0, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇:Ljava/lang/String;

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    iget-object v2, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 36
    .line 37
    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O〇oO〇oo8o(Landroid/content/Context;)Z

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    if-eqz v2, :cond_1

    .line 42
    .line 43
    const-string v0, "Android_Edu"

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->O08000()Z

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    if-eqz v2, :cond_2

    .line 51
    .line 52
    const-string v0, "Android_License"

    .line 53
    .line 54
    :cond_2
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    if-nez v2, :cond_3

    .line 59
    .line 60
    const-string v2, "app_type"

    .line 61
    .line 62
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 63
    .line 64
    .line 65
    :cond_3
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-static {}, Lcom/intsig/utils/RsaSignManager;->〇080()Lcom/intsig/utils/RsaSignManager;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    iget-object v2, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 74
    .line 75
    invoke-virtual {v1, v2, v0}, Lcom/intsig/utils/RsaSignManager;->〇o00〇〇Oo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    invoke-static {v1, v0}, Lcom/intsig/tianshu/TianShuAPI;->O00(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 84
    .line 85
    new-instance v2, Ljava/lang/StringBuilder;

    .line 86
    .line 87
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    .line 89
    .line 90
    const-string v3, "inheritRedeemEdPoints sucess points="

    .line 91
    .line 92
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v2

    .line 102
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    new-instance v1, Lorg/json/JSONObject;

    .line 106
    .line 107
    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    const-string v2, "data"

    .line 111
    .line 112
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    .line 113
    .line 114
    .line 115
    move-result-object v1

    .line 116
    if-eqz v1, :cond_4

    .line 117
    .line 118
    const-string v2, "points"

    .line 119
    .line 120
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    .line 121
    .line 122
    .line 123
    move-result v1

    .line 124
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO〇〇(I)V

    .line 125
    .line 126
    .line 127
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 128
    .line 129
    new-instance v2, Ljava/lang/StringBuilder;

    .line 130
    .line 131
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 132
    .line 133
    .line 134
    const-string v3, "update points when inherit redeeming points successfully "

    .line 135
    .line 136
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    .line 138
    .line 139
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    .line 141
    .line 142
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    .line 148
    .line 149
    goto :goto_1

    .line 150
    :catch_0
    move-exception v0

    .line 151
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 152
    .line 153
    new-instance v2, Ljava/lang/StringBuilder;

    .line 154
    .line 155
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 156
    .line 157
    .line 158
    const-string v3, "inheritProperty e3"

    .line 159
    .line 160
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    .line 162
    .line 163
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 164
    .line 165
    .line 166
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 167
    .line 168
    .line 169
    move-result-object v0

    .line 170
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    .line 172
    .line 173
    goto :goto_1

    .line 174
    :catch_1
    move-exception v0

    .line 175
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 176
    .line 177
    new-instance v2, Ljava/lang/StringBuilder;

    .line 178
    .line 179
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 180
    .line 181
    .line 182
    const-string v3, "inheritRedeemEdPoints fail errorcode="

    .line 183
    .line 184
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    .line 186
    .line 187
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 188
    .line 189
    .line 190
    move-result v3

    .line 191
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 192
    .line 193
    .line 194
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 195
    .line 196
    .line 197
    move-result-object v2

    .line 198
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    .line 200
    .line 201
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 202
    .line 203
    .line 204
    goto :goto_1

    .line 205
    :catch_2
    move-exception v0

    .line 206
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 207
    .line 208
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209
    .line 210
    .line 211
    :cond_4
    :goto_1
    return-void
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private initPushModuleBasicPara()V
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/push/common/PushMsgClient;->〇o〇()Lcom/intsig/camscanner/push/common/PushMsgClient;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO8oO0o〇()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    iget-object v2, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 10
    .line 11
    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇0(Landroid/content/Context;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    iget-object v3, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 16
    .line 17
    invoke-static {v3}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o0O0(Landroid/content/Context;)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->O8()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v4

    .line 25
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/push/common/PushMsgClient;->〇〇888(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/push/common/PushMsgClient;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/push/common/PushMsgClient;->〇80〇808〇O()V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private synthetic lambda$onPostExecute$0()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o0oO()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-static {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o800o8O(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private saveAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 20

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    move-object/from16 v2, p3

    .line 6
    .line 7
    move-object/from16 v3, p5

    .line 8
    .line 9
    move-object/from16 v4, p6

    .line 10
    .line 11
    invoke-static/range {p3 .. p3}, Lcom/intsig/utils/AESEncUtil;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v5

    .line 15
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 16
    .line 17
    .line 18
    move-result v6

    .line 19
    if-eqz v6, :cond_0

    .line 20
    .line 21
    move-object/from16 v6, p4

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    move-object/from16 v6, p4

    .line 25
    .line 26
    invoke-static {v6, v0}, Lcom/intsig/tianshu/TianShuAPI;->Oo〇O(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v6

    .line 30
    :goto_0
    iget-object v7, v1, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 31
    .line 32
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 33
    .line 34
    .line 35
    move-result-object v7

    .line 36
    iget-object v8, v1, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 37
    .line 38
    invoke-static {v8}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    .line 39
    .line 40
    .line 41
    move-result-object v14

    .line 42
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->OoO8()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v8

    .line 46
    const-string v9, "Account_ID"

    .line 47
    .line 48
    const-wide/16 v12, -0x1

    .line 49
    .line 50
    invoke-interface {v14, v9, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    .line 51
    .line 52
    .line 53
    move-result-wide v9

    .line 54
    const-string v11, "Account_UID"

    .line 55
    .line 56
    const/4 v15, 0x0

    .line 57
    invoke-interface {v14, v11, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v11

    .line 61
    const-string v15, "KEY_SYNC"

    .line 62
    .line 63
    const/4 v12, 0x0

    .line 64
    invoke-interface {v14, v15, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    .line 65
    .line 66
    .line 67
    move-result v13

    .line 68
    const/4 v15, 0x2

    .line 69
    if-eqz v13, :cond_2

    .line 70
    .line 71
    const-wide/16 v16, -0x1

    .line 72
    .line 73
    cmp-long v13, v9, v16

    .line 74
    .line 75
    if-eqz v13, :cond_1

    .line 76
    .line 77
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    move-result v13

    .line 81
    if-nez v13, :cond_1

    .line 82
    .line 83
    new-instance v13, Landroid/content/ContentValues;

    .line 84
    .line 85
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 86
    .line 87
    .line 88
    const-string v12, "account_state"

    .line 89
    .line 90
    move-object/from16 v19, v6

    .line 91
    .line 92
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 93
    .line 94
    .line 95
    move-result-object v6

    .line 96
    invoke-virtual {v13, v12, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 97
    .line 98
    .line 99
    sget-object v6, Lcom/intsig/camscanner/provider/Documents$SyncAccount;->〇080:Landroid/net/Uri;

    .line 100
    .line 101
    invoke-static {v6, v9, v10}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 102
    .line 103
    .line 104
    move-result-object v6

    .line 105
    const/4 v9, 0x0

    .line 106
    invoke-virtual {v7, v6, v13, v9, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 107
    .line 108
    .line 109
    move-result v6

    .line 110
    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 111
    .line 112
    .line 113
    move-result-object v9

    .line 114
    const-string v10, "KEY_SYNC"

    .line 115
    .line 116
    const/4 v12, 0x0

    .line 117
    invoke-interface {v9, v10, v12}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 118
    .line 119
    .line 120
    move-result-object v9

    .line 121
    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 122
    .line 123
    .line 124
    iget-object v9, v1, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 125
    .line 126
    new-instance v10, Ljava/lang/StringBuilder;

    .line 127
    .line 128
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    .line 130
    .line 131
    const-string v12, "saveAccount login another account,row="

    .line 132
    .line 133
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    .line 135
    .line 136
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 137
    .line 138
    .line 139
    const-string v6, " oldUid="

    .line 140
    .line 141
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    .line 143
    .line 144
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    .line 146
    .line 147
    const-string v6, " uid="

    .line 148
    .line 149
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    .line 154
    .line 155
    const-string v6, " oldEmail="

    .line 156
    .line 157
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    .line 159
    .line 160
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    .line 162
    .line 163
    const-string v6, " email="

    .line 164
    .line 165
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    .line 167
    .line 168
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    .line 170
    .line 171
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 172
    .line 173
    .line 174
    move-result-object v6

    .line 175
    invoke-static {v9, v6}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .line 177
    .line 178
    goto :goto_1

    .line 179
    :cond_1
    move-object/from16 v19, v6

    .line 180
    .line 181
    goto :goto_1

    .line 182
    :cond_2
    move-object/from16 v19, v6

    .line 183
    .line 184
    const-wide/16 v16, -0x1

    .line 185
    .line 186
    :goto_1
    const/4 v6, 0x1

    .line 187
    if-eqz v2, :cond_8

    .line 188
    .line 189
    sget-object v9, Lcom/intsig/camscanner/provider/Documents$SyncAccount;->〇080:Landroid/net/Uri;

    .line 190
    .line 191
    const-string v8, "_id"

    .line 192
    .line 193
    const-string v10, "account_uid"

    .line 194
    .line 195
    const-string v11, "account_type"

    .line 196
    .line 197
    filled-new-array {v8, v10, v11}, [Ljava/lang/String;

    .line 198
    .line 199
    .line 200
    move-result-object v10

    .line 201
    const-string v11, "account_name=?"

    .line 202
    .line 203
    new-array v12, v6, [Ljava/lang/String;

    .line 204
    .line 205
    const/4 v13, 0x0

    .line 206
    aput-object v5, v12, v13

    .line 207
    .line 208
    const/16 v18, 0x0

    .line 209
    .line 210
    move-object v8, v7

    .line 211
    move-object/from16 v13, v18

    .line 212
    .line 213
    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 214
    .line 215
    .line 216
    move-result-object v8

    .line 217
    if-eqz v8, :cond_8

    .line 218
    .line 219
    :goto_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    .line 220
    .line 221
    .line 222
    move-result v9

    .line 223
    if-eqz v9, :cond_7

    .line 224
    .line 225
    invoke-interface {v8, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 226
    .line 227
    .line 228
    move-result-object v9

    .line 229
    if-eqz v9, :cond_6

    .line 230
    .line 231
    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 232
    .line 233
    .line 234
    move-result v9

    .line 235
    if-nez v9, :cond_6

    .line 236
    .line 237
    invoke-interface {v8, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 238
    .line 239
    .line 240
    move-result-object v9

    .line 241
    if-eqz v9, :cond_4

    .line 242
    .line 243
    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 244
    .line 245
    .line 246
    move-result v10

    .line 247
    if-eqz v10, :cond_3

    .line 248
    .line 249
    goto :goto_4

    .line 250
    :cond_3
    :goto_3
    const/4 v13, 0x0

    .line 251
    goto :goto_5

    .line 252
    :cond_4
    :goto_4
    if-eqz v4, :cond_5

    .line 253
    .line 254
    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 255
    .line 256
    .line 257
    move-result v10

    .line 258
    if-eqz v10, :cond_3

    .line 259
    .line 260
    :cond_5
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 261
    .line 262
    .line 263
    move-result v10

    .line 264
    if-eqz v10, :cond_6

    .line 265
    .line 266
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 267
    .line 268
    .line 269
    move-result v9

    .line 270
    if-eqz v9, :cond_6

    .line 271
    .line 272
    goto :goto_3

    .line 273
    :goto_5
    invoke-interface {v8, v13}, Landroid/database/Cursor;->getLong(I)J

    .line 274
    .line 275
    .line 276
    move-result-wide v9

    .line 277
    new-instance v11, Landroid/content/ContentValues;

    .line 278
    .line 279
    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 280
    .line 281
    .line 282
    const-string v12, "sync_state"

    .line 283
    .line 284
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 285
    .line 286
    .line 287
    move-result-object v15

    .line 288
    invoke-virtual {v11, v12, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 289
    .line 290
    .line 291
    sget-object v12, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 292
    .line 293
    new-instance v15, Ljava/lang/StringBuilder;

    .line 294
    .line 295
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 296
    .line 297
    .line 298
    const-string v13, "sync_account_id="

    .line 299
    .line 300
    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    .line 302
    .line 303
    invoke-virtual {v15, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 304
    .line 305
    .line 306
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 307
    .line 308
    .line 309
    move-result-object v13

    .line 310
    const/4 v15, 0x0

    .line 311
    invoke-virtual {v7, v12, v11, v13, v15}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 312
    .line 313
    .line 314
    move-result v12

    .line 315
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    .line 316
    .line 317
    .line 318
    const-string v13, "sync_state"

    .line 319
    .line 320
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 321
    .line 322
    .line 323
    move-result-object v15

    .line 324
    invoke-virtual {v11, v13, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 325
    .line 326
    .line 327
    const-string v13, "sync_jpage_state"

    .line 328
    .line 329
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 330
    .line 331
    .line 332
    move-result-object v15

    .line 333
    invoke-virtual {v11, v13, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 334
    .line 335
    .line 336
    sget-object v13, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 337
    .line 338
    new-instance v15, Ljava/lang/StringBuilder;

    .line 339
    .line 340
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 341
    .line 342
    .line 343
    const-string v6, "sync_account_id="

    .line 344
    .line 345
    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    .line 347
    .line 348
    invoke-virtual {v15, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 349
    .line 350
    .line 351
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 352
    .line 353
    .line 354
    move-result-object v6

    .line 355
    const/4 v15, 0x0

    .line 356
    invoke-virtual {v7, v13, v11, v6, v15}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 357
    .line 358
    .line 359
    move-result v6

    .line 360
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    .line 361
    .line 362
    .line 363
    const-string v13, "sync_state"

    .line 364
    .line 365
    const/4 v15, 0x1

    .line 366
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 367
    .line 368
    .line 369
    move-result-object v2

    .line 370
    invoke-virtual {v11, v13, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 371
    .line 372
    .line 373
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Tag;->〇080:Landroid/net/Uri;

    .line 374
    .line 375
    new-instance v13, Ljava/lang/StringBuilder;

    .line 376
    .line 377
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 378
    .line 379
    .line 380
    const-string v15, "sync_account_id="

    .line 381
    .line 382
    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    .line 384
    .line 385
    invoke-virtual {v13, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 386
    .line 387
    .line 388
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 389
    .line 390
    .line 391
    move-result-object v9

    .line 392
    const/4 v10, 0x0

    .line 393
    invoke-virtual {v7, v2, v11, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 394
    .line 395
    .line 396
    move-result v2

    .line 397
    iget-object v9, v1, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 398
    .line 399
    new-instance v10, Ljava/lang/StringBuilder;

    .line 400
    .line 401
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 402
    .line 403
    .line 404
    const-string v11, "server crash change local sync state to be add or login a changed email account. doc="

    .line 405
    .line 406
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 407
    .line 408
    .line 409
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 410
    .line 411
    .line 412
    const-string v11, " img="

    .line 413
    .line 414
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    .line 416
    .line 417
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 418
    .line 419
    .line 420
    const-string v6, " tag="

    .line 421
    .line 422
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 423
    .line 424
    .line 425
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 426
    .line 427
    .line 428
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 429
    .line 430
    .line 431
    move-result-object v2

    .line 432
    invoke-static {v9, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    .line 434
    .line 435
    :cond_6
    move-object/from16 v2, p3

    .line 436
    .line 437
    const/4 v6, 0x1

    .line 438
    const/4 v15, 0x2

    .line 439
    goto/16 :goto_2

    .line 440
    .line 441
    :cond_7
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 442
    .line 443
    .line 444
    :cond_8
    new-instance v2, Landroid/content/ContentValues;

    .line 445
    .line 446
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 447
    .line 448
    .line 449
    const-string v6, "account_name"

    .line 450
    .line 451
    invoke-virtual {v2, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    .line 453
    .line 454
    const-string v5, "account_uid"

    .line 455
    .line 456
    invoke-virtual {v2, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    .line 458
    .line 459
    const-string v5, "account_state"

    .line 460
    .line 461
    const/4 v6, 0x1

    .line 462
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 463
    .line 464
    .line 465
    move-result-object v8

    .line 466
    invoke-virtual {v2, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 467
    .line 468
    .line 469
    const-string v5, "account_sns_token"

    .line 470
    .line 471
    invoke-virtual {v2, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 472
    .line 473
    .line 474
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 475
    .line 476
    .line 477
    move-result v5

    .line 478
    if-nez v5, :cond_9

    .line 479
    .line 480
    const-string v5, "account_type"

    .line 481
    .line 482
    invoke-virtual {v2, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    .line 484
    .line 485
    goto :goto_6

    .line 486
    :cond_9
    const-string v4, "account_type"

    .line 487
    .line 488
    invoke-virtual {v2, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 489
    .line 490
    .line 491
    :goto_6
    const-class v4, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;

    .line 492
    .line 493
    monitor-enter v4

    .line 494
    :try_start_0
    sget-object v9, Lcom/intsig/camscanner/provider/Documents$SyncAccount;->〇080:Landroid/net/Uri;

    .line 495
    .line 496
    const-string v5, "_id"

    .line 497
    .line 498
    const-string v6, "account_state"

    .line 499
    .line 500
    filled-new-array {v5, v6}, [Ljava/lang/String;

    .line 501
    .line 502
    .line 503
    move-result-object v10

    .line 504
    const-string v11, "account_uid=?"

    .line 505
    .line 506
    const/4 v5, 0x1

    .line 507
    new-array v12, v5, [Ljava/lang/String;

    .line 508
    .line 509
    const/4 v5, 0x0

    .line 510
    aput-object v0, v12, v5

    .line 511
    .line 512
    const/4 v13, 0x0

    .line 513
    move-object v8, v7

    .line 514
    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 515
    .line 516
    .line 517
    move-result-object v6

    .line 518
    const/4 v8, -0x1

    .line 519
    if-eqz v6, :cond_b

    .line 520
    .line 521
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 522
    .line 523
    .line 524
    move-result v9

    .line 525
    if-eqz v9, :cond_a

    .line 526
    .line 527
    invoke-interface {v6, v5}, Landroid/database/Cursor;->getLong(I)J

    .line 528
    .line 529
    .line 530
    move-result-wide v8

    .line 531
    const/4 v10, 0x1

    .line 532
    invoke-interface {v6, v10}, Landroid/database/Cursor;->getInt(I)I

    .line 533
    .line 534
    .line 535
    move-result v11

    .line 536
    move-wide v12, v8

    .line 537
    move v8, v11

    .line 538
    goto :goto_7

    .line 539
    :cond_a
    move-wide/from16 v12, v16

    .line 540
    .line 541
    :goto_7
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 542
    .line 543
    .line 544
    goto :goto_8

    .line 545
    :cond_b
    move-wide/from16 v12, v16

    .line 546
    .line 547
    :goto_8
    cmp-long v6, v12, v16

    .line 548
    .line 549
    if-lez v6, :cond_f

    .line 550
    .line 551
    if-eqz v8, :cond_e

    .line 552
    .line 553
    const/4 v6, 0x1

    .line 554
    if-eq v8, v6, :cond_c

    .line 555
    .line 556
    const/4 v6, 0x2

    .line 557
    if-eq v8, v6, :cond_e

    .line 558
    .line 559
    goto/16 :goto_a

    .line 560
    .line 561
    :cond_c
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇0O〇Oo〇o()Ljava/lang/String;

    .line 562
    .line 563
    .line 564
    move-result-object v6

    .line 565
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 566
    .line 567
    .line 568
    move-result v6

    .line 569
    if-eqz v6, :cond_d

    .line 570
    .line 571
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇08O〇00〇o()Ljava/lang/String;

    .line 572
    .line 573
    .line 574
    move-result-object v6

    .line 575
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 576
    .line 577
    .line 578
    move-result v6

    .line 579
    if-nez v6, :cond_d

    .line 580
    .line 581
    const/4 v6, 0x1

    .line 582
    goto :goto_9

    .line 583
    :cond_d
    const/4 v6, 0x0

    .line 584
    :goto_9
    if-eqz v6, :cond_11

    .line 585
    .line 586
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇08O〇00〇o()Ljava/lang/String;

    .line 587
    .line 588
    .line 589
    move-result-object v6

    .line 590
    invoke-static {v6}, Lcom/intsig/camscanner/util/PreferenceHelper;->oOOo8〇00(Ljava/lang/String;)V

    .line 591
    .line 592
    .line 593
    const/4 v6, 0x0

    .line 594
    invoke-static {v6}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOo80〇80(Ljava/lang/String;)V

    .line 595
    .line 596
    .line 597
    goto :goto_a

    .line 598
    :cond_e
    sget-object v6, Lcom/intsig/camscanner/provider/Documents$SyncAccount;->〇080:Landroid/net/Uri;

    .line 599
    .line 600
    invoke-static {v6, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 601
    .line 602
    .line 603
    move-result-object v6

    .line 604
    const/4 v8, 0x0

    .line 605
    invoke-virtual {v7, v6, v2, v8, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 606
    .line 607
    .line 608
    move-result v9

    .line 609
    iget-object v8, v1, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 610
    .line 611
    new-instance v10, Ljava/lang/StringBuilder;

    .line 612
    .line 613
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 614
    .line 615
    .line 616
    const-string v11, "relogin row="

    .line 617
    .line 618
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 619
    .line 620
    .line 621
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 622
    .line 623
    .line 624
    const-string v9, " mUri="

    .line 625
    .line 626
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 627
    .line 628
    .line 629
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 630
    .line 631
    .line 632
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 633
    .line 634
    .line 635
    move-result-object v6

    .line 636
    invoke-static {v8, v6}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    .line 638
    .line 639
    goto :goto_a

    .line 640
    :cond_f
    sget-object v6, Lcom/intsig/camscanner/provider/Documents$SyncAccount;->〇080:Landroid/net/Uri;

    .line 641
    .line 642
    invoke-virtual {v7, v6, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 643
    .line 644
    .line 645
    move-result-object v6

    .line 646
    iget-object v8, v1, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 647
    .line 648
    new-instance v9, Ljava/lang/StringBuilder;

    .line 649
    .line 650
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 651
    .line 652
    .line 653
    const-string v10, "insert account u="

    .line 654
    .line 655
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 656
    .line 657
    .line 658
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 659
    .line 660
    .line 661
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 662
    .line 663
    .line 664
    move-result-object v9

    .line 665
    invoke-static {v8, v9}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    .line 667
    .line 668
    if-nez v6, :cond_10

    .line 669
    .line 670
    monitor-exit v4

    .line 671
    return v5

    .line 672
    :cond_10
    invoke-static {v6}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 673
    .line 674
    .line 675
    move-result-wide v12

    .line 676
    :cond_11
    :goto_a
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 677
    iget-object v4, v1, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 678
    .line 679
    new-instance v6, Ljava/lang/StringBuilder;

    .line 680
    .line 681
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 682
    .line 683
    .line 684
    const-string v8, "tokenPwd="

    .line 685
    .line 686
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 687
    .line 688
    .line 689
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 690
    .line 691
    .line 692
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 693
    .line 694
    .line 695
    move-result-object v6

    .line 696
    invoke-static {v4, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    .line 698
    .line 699
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇0O〇Oo〇o()Ljava/lang/String;

    .line 700
    .line 701
    .line 702
    move-result-object v4

    .line 703
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 704
    .line 705
    .line 706
    move-result v4

    .line 707
    if-eqz v4, :cond_12

    .line 708
    .line 709
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇08O〇00〇o()Ljava/lang/String;

    .line 710
    .line 711
    .line 712
    move-result-object v4

    .line 713
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 714
    .line 715
    .line 716
    move-result v4

    .line 717
    if-nez v4, :cond_12

    .line 718
    .line 719
    const/4 v5, 0x1

    .line 720
    :cond_12
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 721
    .line 722
    .line 723
    move-result v4

    .line 724
    if-eqz v4, :cond_13

    .line 725
    .line 726
    iget-object v4, v1, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mAccount:Ljava/lang/String;

    .line 727
    .line 728
    invoke-static {v4}, Lcom/intsig/tsapp/account/util/AccountUtils;->O8〇o(Ljava/lang/String;)Z

    .line 729
    .line 730
    .line 731
    move-result v4

    .line 732
    if-nez v4, :cond_13

    .line 733
    .line 734
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->getAreaCodeFromUserInfo()Ljava/lang/String;

    .line 735
    .line 736
    .line 737
    move-result-object v4

    .line 738
    goto :goto_b

    .line 739
    :cond_13
    move-object/from16 v4, p2

    .line 740
    .line 741
    :goto_b
    invoke-static/range {p3 .. p3}, Lcom/intsig/comm/account_data/AccountPreference;->〇o0O0O8(Ljava/lang/String;)V

    .line 742
    .line 743
    .line 744
    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 745
    .line 746
    .line 747
    move-result-object v6

    .line 748
    const-string v8, "Area_Code"

    .line 749
    .line 750
    invoke-interface {v6, v8, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 751
    .line 752
    .line 753
    move-result-object v4

    .line 754
    const-string v6, "account_login_password"

    .line 755
    .line 756
    move-object/from16 v8, v19

    .line 757
    .line 758
    invoke-interface {v4, v6, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 759
    .line 760
    .line 761
    move-result-object v4

    .line 762
    const-string v6, "token_Password"

    .line 763
    .line 764
    invoke-interface {v4, v6, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 765
    .line 766
    .line 767
    move-result-object v3

    .line 768
    const-string v4, "Account_UID"

    .line 769
    .line 770
    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 771
    .line 772
    .line 773
    move-result-object v0

    .line 774
    const-string v3, "Account_ID"

    .line 775
    .line 776
    invoke-interface {v0, v3, v12, v13}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 777
    .line 778
    .line 779
    move-result-object v0

    .line 780
    const-string v3, "KEY_SYNC"

    .line 781
    .line 782
    const/4 v4, 0x1

    .line 783
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 784
    .line 785
    .line 786
    move-result-object v0

    .line 787
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 788
    .line 789
    .line 790
    if-eqz v5, :cond_14

    .line 791
    .line 792
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇08O〇00〇o()Ljava/lang/String;

    .line 793
    .line 794
    .line 795
    move-result-object v0

    .line 796
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->oOOo8〇00(Ljava/lang/String;)V

    .line 797
    .line 798
    .line 799
    const/4 v0, 0x0

    .line 800
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOo80〇80(Ljava/lang/String;)V

    .line 801
    .line 802
    .line 803
    :cond_14
    const-string v0, "-1"

    .line 804
    .line 805
    filled-new-array {v0}, [Ljava/lang/String;

    .line 806
    .line 807
    .line 808
    move-result-object v0

    .line 809
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 810
    .line 811
    .line 812
    const-string v3, "sync_account_id"

    .line 813
    .line 814
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 815
    .line 816
    .line 817
    move-result-object v4

    .line 818
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 819
    .line 820
    .line 821
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Dir;->〇080:Landroid/net/Uri;

    .line 822
    .line 823
    const-string v4, "sync_account_id=?"

    .line 824
    .line 825
    invoke-virtual {v7, v3, v2, v4, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 826
    .line 827
    .line 828
    move-result v3

    .line 829
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 830
    .line 831
    .line 832
    const-string v4, "sync_account_id"

    .line 833
    .line 834
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 835
    .line 836
    .line 837
    move-result-object v5

    .line 838
    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 839
    .line 840
    .line 841
    sget-object v4, Lcom/intsig/camscanner/provider/Documents$SystemMessage;->〇080:Landroid/net/Uri;

    .line 842
    .line 843
    const-string v5, "sync_account_id=?"

    .line 844
    .line 845
    invoke-virtual {v7, v4, v2, v5, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 846
    .line 847
    .line 848
    move-result v4

    .line 849
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 850
    .line 851
    .line 852
    const-string v5, "sync_account_id"

    .line 853
    .line 854
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 855
    .line 856
    .line 857
    move-result-object v6

    .line 858
    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 859
    .line 860
    .line 861
    sget-object v5, Lcom/intsig/camscanner/provider/Documents$Tag;->〇080:Landroid/net/Uri;

    .line 862
    .line 863
    const-string v6, "sync_account_id=?"

    .line 864
    .line 865
    invoke-virtual {v7, v5, v2, v6, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 866
    .line 867
    .line 868
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 869
    .line 870
    .line 871
    const-string v5, "sync_account_id"

    .line 872
    .line 873
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 874
    .line 875
    .line 876
    move-result-object v6

    .line 877
    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 878
    .line 879
    .line 880
    sget-object v5, Lcom/intsig/camscanner/provider/Documents$Document;->Oo08:Landroid/net/Uri;

    .line 881
    .line 882
    const-string v6, "sync_account_id=?"

    .line 883
    .line 884
    invoke-virtual {v7, v5, v2, v6, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 885
    .line 886
    .line 887
    move-result v5

    .line 888
    iget-object v6, v1, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 889
    .line 890
    new-instance v8, Ljava/lang/StringBuilder;

    .line 891
    .line 892
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 893
    .line 894
    .line 895
    const-string v9, "saveAccount update SYNC_ACCOUNT_ID = "

    .line 896
    .line 897
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 898
    .line 899
    .line 900
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 901
    .line 902
    .line 903
    const-string v5, " to "

    .line 904
    .line 905
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 906
    .line 907
    .line 908
    invoke-virtual {v8, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 909
    .line 910
    .line 911
    const-string v5, " updateDirRow="

    .line 912
    .line 913
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 914
    .line 915
    .line 916
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 917
    .line 918
    .line 919
    const-string v3, " updateSystemMessageRow="

    .line 920
    .line 921
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 922
    .line 923
    .line 924
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 925
    .line 926
    .line 927
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 928
    .line 929
    .line 930
    move-result-object v3

    .line 931
    invoke-static {v6, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 932
    .line 933
    .line 934
    iget-object v3, v1, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 935
    .line 936
    invoke-static {v3}, Lcom/intsig/camscanner/app/DBUtil;->〇Oo〇o8(Landroid/content/Context;)V

    .line 937
    .line 938
    .line 939
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 940
    .line 941
    .line 942
    const-string v3, "sync_account_id"

    .line 943
    .line 944
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 945
    .line 946
    .line 947
    move-result-object v4

    .line 948
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 949
    .line 950
    .line 951
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 952
    .line 953
    const-string v4, "sync_account_id=?"

    .line 954
    .line 955
    invoke-virtual {v7, v3, v2, v4, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 956
    .line 957
    .line 958
    iget-object v0, v1, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->personalSync:Ljava/lang/String;

    .line 959
    .line 960
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->ooo0〇〇O(Ljava/lang/String;)V

    .line 961
    .line 962
    .line 963
    const/4 v0, 0x1

    .line 964
    return v0

    .line 965
    :catchall_0
    move-exception v0

    .line 966
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 967
    throw v0
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
.end method

.method private saveAndStartSync(Z)I
    .locals 9

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->needSync:Z

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mLoginTaskListener:Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;

    .line 5
    .line 6
    instance-of v2, v1, Lcom/intsig/tsapp/account/login_task/FastLoginTaskListener;

    .line 7
    .line 8
    if-eqz v2, :cond_0

    .line 9
    .line 10
    check-cast v1, Lcom/intsig/tsapp/account/login_task/FastLoginTaskListener;

    .line 11
    .line 12
    invoke-interface {v1}, Lcom/intsig/tsapp/account/login_task/FastLoginTaskListener;->getTokenPwd()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v1, 0x0

    .line 18
    :goto_0
    move-object v7, v1

    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 20
    .line 21
    new-instance v2, Ljava/lang/StringBuilder;

    .line 22
    .line 23
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 24
    .line 25
    .line 26
    const-string v3, "tokenPwd="

    .line 27
    .line 28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    const-string v3, " mUid\uff1a"

    .line 35
    .line 36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    iget-object v3, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mUid:Ljava/lang/String;

    .line 40
    .line 41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    const-string v3, " sync="

    .line 45
    .line 46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mUid:Ljava/lang/String;

    .line 60
    .line 61
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    if-nez v1, :cond_2

    .line 66
    .line 67
    iget-object v3, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mUid:Ljava/lang/String;

    .line 68
    .line 69
    iget-object v4, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mAreaCode:Ljava/lang/String;

    .line 70
    .line 71
    iget-object v5, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mAccount:Ljava/lang/String;

    .line 72
    .line 73
    iget-object v6, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mPasswords:Ljava/lang/String;

    .line 74
    .line 75
    const/4 v8, 0x0

    .line 76
    move-object v2, p0

    .line 77
    invoke-direct/range {v2 .. v8}, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->saveAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    if-nez v1, :cond_1

    .line 82
    .line 83
    goto :goto_1

    .line 84
    :cond_1
    iget-object v1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 85
    .line 86
    const/4 v2, 0x1

    .line 87
    invoke-static {v1, v2}, Lcom/intsig/camscanner/app/AppUtil;->Oo〇O(Landroid/content/Context;Z)V

    .line 88
    .line 89
    .line 90
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8oOOo()Z

    .line 91
    .line 92
    .line 93
    move-result v1

    .line 94
    iput-boolean v1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mIsNeedShowVipDlg:Z

    .line 95
    .line 96
    iput-boolean p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->needSync:Z

    .line 97
    .line 98
    if-eqz p1, :cond_3

    .line 99
    .line 100
    sput-boolean v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OoO8:Z

    .line 101
    .line 102
    iget-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 103
    .line 104
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oO80OOO〇(Landroid/content/Context;)V

    .line 109
    .line 110
    .line 111
    goto :goto_2

    .line 112
    :cond_2
    :goto_1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8oOOo()Z

    .line 113
    .line 114
    .line 115
    move-result p1

    .line 116
    iput-boolean p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mIsNeedShowVipDlg:Z

    .line 117
    .line 118
    const/4 v0, -0x1

    .line 119
    :cond_3
    :goto_2
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 120
    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    .line 122
    .line 123
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 124
    .line 125
    .line 126
    const-string v2, "saveAndStartSync result="

    .line 127
    .line 128
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 132
    .line 133
    .line 134
    const-string v2, " mUid="

    .line 135
    .line 136
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    .line 138
    .line 139
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mUid:Ljava/lang/String;

    .line 140
    .line 141
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    .line 143
    .line 144
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object v1

    .line 148
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    return v0
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->lambda$onPostExecute$0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public varargs doInBackground([Ljava/lang/String;)Ljava/lang/Integer;
    .locals 12

    .line 1
    const-string p1, "com.intsig.camcard.LOGIN_SYNC"

    .line 2
    .line 3
    const-string v0, "com.intsig.camcard.LOGIN_CHANGE_PWD"

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    iput-object v1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->personalSync:Ljava/lang/String;

    .line 7
    .line 8
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mLoginTaskListener:Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;

    .line 9
    .line 10
    const-string v3, "LoginTask"

    .line 11
    .line 12
    const/4 v4, -0x1

    .line 13
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 14
    .line 15
    .line 16
    move-result-object v5

    .line 17
    if-nez v2, :cond_0

    .line 18
    .line 19
    const-string p1, "mLoginTaskListener == null"

    .line 20
    .line 21
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    return-object v5

    .line 25
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 26
    .line 27
    .line 28
    move-result-wide v6

    .line 29
    iget-object v2, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 30
    .line 31
    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO〇(Landroid/content/Context;)I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    if-ne v2, v4, :cond_1

    .line 36
    .line 37
    const/16 p1, -0x63

    .line 38
    .line 39
    goto/16 :goto_6

    .line 40
    .line 41
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mLoginTaskListener:Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;

    .line 42
    .line 43
    invoke-interface {v2}, Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;->operationLogin()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    iput-object v2, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mAccount:Ljava/lang/String;

    .line 48
    .line 49
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    if-nez v2, :cond_2

    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 56
    .line 57
    const-string v0, "userInfo == null"

    .line 58
    .line 59
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    return-object v5

    .line 63
    :cond_2
    iget-object v5, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mAccount:Ljava/lang/String;

    .line 64
    .line 65
    iget-object v8, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mAreaCode:Ljava/lang/String;

    .line 66
    .line 67
    invoke-virtual {v2}, Lcom/intsig/tianshu/UserInfo;->getUserID()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v9

    .line 71
    invoke-direct {p0, v5, v8, v9}, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->cacheApiInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {v2}, Lcom/intsig/tianshu/UserInfo;->getProfile()Lcom/intsig/tianshu/UserInfo$Profile;

    .line 75
    .line 76
    .line 77
    move-result-object v5

    .line 78
    if-eqz v5, :cond_3

    .line 79
    .line 80
    invoke-virtual {v5}, Lcom/intsig/tianshu/UserInfo$Profile;->getDisplayName()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v5

    .line 84
    invoke-static {v5}, Lcom/intsig/comm/account_data/AccountPreference;->Ooo(Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_3
    iget-object v5, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 89
    .line 90
    const-string v8, "TianShuAPI.getUserInfo().getProfile() return null "

    .line 91
    .line 92
    invoke-static {v5, v8}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    :goto_0
    invoke-virtual {v2}, Lcom/intsig/tianshu/UserInfo;->getUserID()Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object v2

    .line 99
    iput-object v2, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mUid:Ljava/lang/String;

    .line 100
    .line 101
    new-instance v2, Ljava/lang/StringBuilder;

    .line 102
    .line 103
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .line 105
    .line 106
    const-string v5, "mUid = "

    .line 107
    .line 108
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    iget-object v5, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mUid:Ljava/lang/String;

    .line 112
    .line 113
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v2

    .line 120
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mIntentAction:Ljava/lang/String;

    .line 124
    .line 125
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 126
    .line 127
    .line 128
    move-result v2

    .line 129
    if-nez v2, :cond_4

    .line 130
    .line 131
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->inheritProperty()V

    .line 132
    .line 133
    .line 134
    :cond_4
    const-string v2, "personal_sync"

    .line 135
    .line 136
    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o0(Ljava/lang/String;)Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v2

    .line 140
    iput-object v2, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->personalSync:Ljava/lang/String;

    .line 141
    .line 142
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇8〇oO〇〇8o()V

    .line 143
    .line 144
    .line 145
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mIntentAction:Ljava/lang/String;

    .line 146
    .line 147
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 148
    .line 149
    .line 150
    move-result v2

    .line 151
    const/4 v3, 0x1

    .line 152
    if-nez v2, :cond_9

    .line 153
    .line 154
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mIntentAction:Ljava/lang/String;

    .line 155
    .line 156
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 157
    .line 158
    .line 159
    move-result v0

    .line 160
    if-eqz v0, :cond_5

    .line 161
    .line 162
    goto/16 :goto_2

    .line 163
    .line 164
    :cond_5
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->saveAndStartSync(Z)I

    .line 165
    .line 166
    .line 167
    move-result v4

    .line 168
    if-nez v4, :cond_a

    .line 169
    .line 170
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 171
    .line 172
    .line 173
    move-result-object p1

    .line 174
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OOo88OOo(Lcom/intsig/tianshu/UserInfo;)Z

    .line 175
    .line 176
    .line 177
    move-result p1

    .line 178
    if-eqz p1, :cond_8

    .line 179
    .line 180
    iget-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 181
    .line 182
    invoke-static {p1, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8〇OO0〇0o(Landroid/content/Context;Z)V

    .line 183
    .line 184
    .line 185
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 186
    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    .line 188
    .line 189
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 190
    .line 191
    .line 192
    const-string v2, "isSepcialAccount = true getPromoteMsgLink() = "

    .line 193
    .line 194
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    .line 196
    .line 197
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 198
    .line 199
    .line 200
    move-result-object v2

    .line 201
    invoke-virtual {v2}, Lcom/intsig/tianshu/UserInfo;->getPromoteMsgLink()Ljava/lang/String;

    .line 202
    .line 203
    .line 204
    move-result-object v2

    .line 205
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    .line 207
    .line 208
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 209
    .line 210
    .line 211
    move-result-object v0

    .line 212
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 213
    .line 214
    .line 215
    const/4 p1, 0x2

    .line 216
    :try_start_1
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 217
    .line 218
    .line 219
    move-result-object v0

    .line 220
    invoke-virtual {v0}, Lcom/intsig/tianshu/UserInfo;->getPromoteMsgLink()Ljava/lang/String;

    .line 221
    .line 222
    .line 223
    move-result-object v0

    .line 224
    invoke-static {v0}, Lcom/intsig/tsapp/account/model/ShareMsg;->〇〇888(Ljava/lang/String;)Lcom/intsig/tsapp/account/model/ShareMsg;

    .line 225
    .line 226
    .line 227
    move-result-object v0

    .line 228
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mShareMsg:Lcom/intsig/tsapp/account/model/ShareMsg;

    .line 229
    .line 230
    if-eqz v0, :cond_6

    .line 231
    .line 232
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 233
    .line 234
    new-instance v2, Ljava/lang/StringBuilder;

    .line 235
    .line 236
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 237
    .line 238
    .line 239
    const-string v5, "getShare mShareMsg.toString() = "

    .line 240
    .line 241
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    .line 243
    .line 244
    iget-object v5, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mShareMsg:Lcom/intsig/tsapp/account/model/ShareMsg;

    .line 245
    .line 246
    invoke-virtual {v5}, Lcom/intsig/tsapp/account/model/ShareMsg;->toString()Ljava/lang/String;

    .line 247
    .line 248
    .line 249
    move-result-object v5

    .line 250
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    .line 252
    .line 253
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 254
    .line 255
    .line 256
    move-result-object v2

    .line 257
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    .line 259
    .line 260
    :cond_6
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mAccount:Ljava/lang/String;

    .line 261
    .line 262
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->O0o〇O0〇(Ljava/lang/String;)V

    .line 263
    .line 264
    .line 265
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 266
    .line 267
    .line 268
    move-result-object v0

    .line 269
    invoke-virtual {v0}, Lcom/intsig/tianshu/UserInfo;->isEduAccount()Z

    .line 270
    .line 271
    .line 272
    move-result v0

    .line 273
    if-eqz v0, :cond_7

    .line 274
    .line 275
    iput v3, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mSpecialAccoutType:I

    .line 276
    .line 277
    goto :goto_1

    .line 278
    :cond_7
    iput p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mSpecialAccoutType:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 279
    .line 280
    goto :goto_1

    .line 281
    :catch_0
    move-exception v0

    .line 282
    :try_start_2
    iput p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mSpecialAccoutType:I

    .line 283
    .line 284
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 285
    .line 286
    new-instance v2, Ljava/lang/StringBuilder;

    .line 287
    .line 288
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 289
    .line 290
    .line 291
    const-string v5, "isEduMail false "

    .line 292
    .line 293
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    .line 295
    .line 296
    iget-object v5, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mAccount:Ljava/lang/String;

    .line 297
    .line 298
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    .line 300
    .line 301
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 302
    .line 303
    .line 304
    move-result-object v2

    .line 305
    invoke-static {p1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 306
    .line 307
    .line 308
    goto :goto_1

    .line 309
    :cond_8
    iget-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 310
    .line 311
    const/4 v0, 0x0

    .line 312
    invoke-static {p1, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8〇OO0〇0o(Landroid/content/Context;Z)V

    .line 313
    .line 314
    .line 315
    :goto_1
    iget-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 316
    .line 317
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 318
    .line 319
    .line 320
    move-result-object v0

    .line 321
    invoke-virtual {v0}, Lcom/intsig/tianshu/UserInfo;->isEduAccount()Z

    .line 322
    .line 323
    .line 324
    move-result v0

    .line 325
    invoke-static {p1, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oO0〇〇O8o(Landroid/content/Context;Z)V

    .line 326
    .line 327
    .line 328
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 329
    .line 330
    .line 331
    move-result-object p1

    .line 332
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/account/util/AccountUtil;->〇〇8O0〇8(Lcom/intsig/tianshu/UserInfo;)V

    .line 333
    .line 334
    .line 335
    goto :goto_3

    .line 336
    :cond_9
    :goto_2
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mIntentAction:Ljava/lang/String;

    .line 337
    .line 338
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 339
    .line 340
    .line 341
    move-result p1

    .line 342
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->saveAndStartSync(Z)I

    .line 343
    .line 344
    .line 345
    move-result p1

    .line 346
    move v4, p1

    .line 347
    :cond_a
    :goto_3
    iget-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 348
    .line 349
    const-wide/16 v8, 0x0

    .line 350
    .line 351
    if-eqz p1, :cond_b

    .line 352
    .line 353
    invoke-static {p1, v8, v9}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo0〇o〇(Landroid/content/Context;J)V

    .line 354
    .line 355
    .line 356
    :cond_b
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇000〇〇08()I

    .line 357
    .line 358
    .line 359
    move-result p1

    .line 360
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO80(I)V

    .line 361
    .line 362
    .line 363
    new-instance p1, Lcom/intsig/camscanner/eventbus/SyncSettingEvent;

    .line 364
    .line 365
    const-string v0, ""

    .line 366
    .line 367
    invoke-direct {p1, v0}, Lcom/intsig/camscanner/eventbus/SyncSettingEvent;-><init>(Ljava/lang/String;)V

    .line 368
    .line 369
    .line 370
    invoke-static {p1}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 371
    .line 372
    .line 373
    invoke-static {}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->clearNormalPdf()V

    .line 374
    .line 375
    .line 376
    if-nez v4, :cond_d

    .line 377
    .line 378
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇08〇0〇o〇8()[J

    .line 379
    .line 380
    .line 381
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 382
    .line 383
    .line 384
    move-result-wide v10

    .line 385
    sub-long/2addr v10, v6

    .line 386
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 387
    .line 388
    new-instance v0, Ljava/lang/StringBuilder;

    .line 389
    .line 390
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 391
    .line 392
    .line 393
    const-string v2, "updateVipInfo consume = "

    .line 394
    .line 395
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    .line 397
    .line 398
    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 399
    .line 400
    .line 401
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 402
    .line 403
    .line 404
    move-result-object v0

    .line 405
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    .line 407
    .line 408
    iget-boolean p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mIsNeedShowVipDlg:Z

    .line 409
    .line 410
    if-eqz p1, :cond_c

    .line 411
    .line 412
    iget-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 413
    .line 414
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->〇080()Ljava/lang/String;

    .line 415
    .line 416
    .line 417
    move-result-object v0

    .line 418
    sget-object v2, Lcom/intsig/tsapp/account/model/EduMsg;->O8o08O8O:Ljava/lang/String;

    .line 419
    .line 420
    invoke-static {p1, v0, v2}, Lcom/intsig/tsapp/account/model/EduMsg;->〇080(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tsapp/account/model/EduMsg;

    .line 421
    .line 422
    .line 423
    move-result-object p1

    .line 424
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mVipMsg:Lcom/intsig/tsapp/account/model/EduMsg;

    .line 425
    .line 426
    :cond_c
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->〇8()V

    .line 427
    .line 428
    .line 429
    iget-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 430
    .line 431
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o()Ljava/lang/String;

    .line 432
    .line 433
    .line 434
    move-result-object v0

    .line 435
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogTrackerUserData;->OO0o〇〇〇〇0(Landroid/content/Context;Ljava/lang/String;)V

    .line 436
    .line 437
    .line 438
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o()Ljava/lang/String;

    .line 439
    .line 440
    .line 441
    move-result-object p1

    .line 442
    sget-object v0, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇:Ljava/lang/String;

    .line 443
    .line 444
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇oo〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    .line 446
    .line 447
    invoke-static {}, Lcom/intsig/camscanner/control/PdfImportControl;->O8()Lcom/intsig/camscanner/control/PdfImportControl;

    .line 448
    .line 449
    .line 450
    move-result-object p1

    .line 451
    iget-object v0, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 452
    .line 453
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/control/PdfImportControl;->〇o〇(Landroid/content/Context;)V

    .line 454
    .line 455
    .line 456
    invoke-static {}, Lcom/intsig/camscanner/launch/tasks/NonBlockTask;->o〇〇0〇()V

    .line 457
    .line 458
    .line 459
    :cond_d
    iget-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 460
    .line 461
    invoke-static {p1, v8, v9}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo0〇o〇(Landroid/content/Context;J)V

    .line 462
    .line 463
    .line 464
    iget-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 465
    .line 466
    invoke-static {p1}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->o〇0(Landroid/content/Context;)V

    .line 467
    .line 468
    .line 469
    iget-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 470
    .line 471
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->oO80(Landroid/content/Context;)V

    .line 472
    .line 473
    .line 474
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 475
    .line 476
    .line 477
    move-result-object p1

    .line 478
    iget-object v0, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 479
    .line 480
    invoke-virtual {p1, v0, v3}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->Oooo8o0〇(Landroid/content/Context;Z)V

    .line 481
    .line 482
    .line 483
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 484
    .line 485
    .line 486
    move-result-object p1

    .line 487
    invoke-virtual {p1}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇O〇()Z

    .line 488
    .line 489
    .line 490
    move-result p1

    .line 491
    if-eqz p1, :cond_e

    .line 492
    .line 493
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 494
    .line 495
    .line 496
    move-result-object p1

    .line 497
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mUid:Ljava/lang/String;

    .line 498
    .line 499
    const-string v2, "cs_task"

    .line 500
    .line 501
    invoke-virtual {p1, v0, v2}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    .line 502
    .line 503
    .line 504
    move-result-object p1

    .line 505
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mOperateGuideModels:Ljava/util/List;

    .line 506
    .line 507
    goto :goto_4

    .line 508
    :cond_e
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 509
    .line 510
    .line 511
    move-result-object p1

    .line 512
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mUid:Ljava/lang/String;

    .line 513
    .line 514
    const-string v2, "cs_storage"

    .line 515
    .line 516
    invoke-virtual {p1, v0, v2}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    .line 517
    .line 518
    .line 519
    move-result-object p1

    .line 520
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mOperateGuideModels:Ljava/util/List;

    .line 521
    .line 522
    :goto_4
    invoke-static {}, Lcom/intsig/camscanner/docexplore/DocExploreHelper;->O8()Lcom/intsig/camscanner/docexplore/DocExploreHelper;

    .line 523
    .line 524
    .line 525
    move-result-object p1

    .line 526
    invoke-virtual {p1}, Lcom/intsig/camscanner/docexplore/DocExploreHelper;->〇8o8o〇()V
    :try_end_2
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    .line 527
    .line 528
    .line 529
    goto :goto_5

    .line 530
    :catch_1
    move-exception p1

    .line 531
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 532
    .line 533
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 534
    .line 535
    .line 536
    :goto_5
    move p1, v4

    .line 537
    goto :goto_6

    .line 538
    :catch_2
    move-exception p1

    .line 539
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 540
    .line 541
    new-instance v2, Ljava/lang/StringBuilder;

    .line 542
    .line 543
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 544
    .line 545
    .line 546
    const-string v3, "TianShuException email = "

    .line 547
    .line 548
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 549
    .line 550
    .line 551
    iget-object v3, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mAccount:Ljava/lang/String;

    .line 552
    .line 553
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 554
    .line 555
    .line 556
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 557
    .line 558
    .line 559
    move-result-object v2

    .line 560
    invoke-static {v0, v2, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 561
    .line 562
    .line 563
    invoke-virtual {p1}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorMsg()Ljava/lang/String;

    .line 564
    .line 565
    .line 566
    move-result-object v0

    .line 567
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->errorMsg:Ljava/lang/String;

    .line 568
    .line 569
    invoke-virtual {p1}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 570
    .line 571
    .line 572
    move-result p1

    .line 573
    :goto_6
    new-instance v0, Lcom/intsig/tsapp/account/login_task/WXLoginControl;

    .line 574
    .line 575
    iget-object v2, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 576
    .line 577
    invoke-direct {v0, v2, v1}, Lcom/intsig/tsapp/account/login_task/WXLoginControl;-><init>(Landroid/content/Context;Lcom/intsig/login/WXNetCallBack;)V

    .line 578
    .line 579
    .line 580
    sget-object v1, Lcom/intsig/tsapp/account/login_task/WXLoginControl$WXType;->QUERY_BIND:Lcom/intsig/tsapp/account/login_task/WXLoginControl$WXType;

    .line 581
    .line 582
    invoke-virtual {v0, v1}, Lcom/intsig/tsapp/account/login_task/WXLoginControl;->〇〇8O0〇8(Lcom/intsig/tsapp/account/login_task/WXLoginControl$WXType;)V

    .line 583
    .line 584
    .line 585
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->initPushModuleBasicPara()V

    .line 586
    .line 587
    .line 588
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->〇o〇o()V

    .line 589
    .line 590
    .line 591
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 592
    .line 593
    .line 594
    move-result-wide v0

    .line 595
    sub-long/2addr v0, v6

    .line 596
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 597
    .line 598
    new-instance v3, Ljava/lang/StringBuilder;

    .line 599
    .line 600
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 601
    .line 602
    .line 603
    const-string v4, "total consume = "

    .line 604
    .line 605
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 606
    .line 607
    .line 608
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 609
    .line 610
    .line 611
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 612
    .line 613
    .line 614
    move-result-object v0

    .line 615
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    .line 617
    .line 618
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 619
    .line 620
    .line 621
    move-result-object p1

    .line 622
    return-object p1
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method public getVipMsg()Lcom/intsig/tsapp/account/model/EduMsg;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mVipMsg:Lcom/intsig/tsapp/account/model/EduMsg;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public init(Landroid/content/Context;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onPostExecute(Ljava/lang/Integer;)V
    .locals 5

    .line 1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    if-nez v0, :cond_3

    .line 8
    .line 9
    invoke-static {}, Lcom/intsig/camscanner/ads/csAd/CsAdManager;->OO0o〇〇〇〇0()Lcom/intsig/camscanner/ads/csAd/CsAdManager;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads/csAd/CsAdManager;->〇〇888()V

    .line 14
    .line 15
    .line 16
    iget-boolean p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mIsChangeAccount:Z

    .line 17
    .line 18
    const/4 v0, 0x1

    .line 19
    if-eqz p1, :cond_0

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 22
    .line 23
    invoke-static {p1}, Lcom/intsig/camscanner/app/AppUtil;->oO(Landroid/content/Context;)Z

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    if-eqz p1, :cond_0

    .line 28
    .line 29
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O8ooOoo〇(Z)V

    .line 30
    .line 31
    .line 32
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    iget-object v3, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mOperateGuideModels:Ljava/util/List;

    .line 37
    .line 38
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->OO0o〇〇(Ljava/util/List;)V

    .line 39
    .line 40
    .line 41
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->O8()Z

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    if-eqz p1, :cond_1

    .line 46
    .line 47
    invoke-static {}, Lcom/intsig/appsflyer/AppsFlyerHelper;->OO0o〇〇()V

    .line 48
    .line 49
    .line 50
    :cond_1
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->〇〇888()Z

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    sget-object v3, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 55
    .line 56
    invoke-static {p1, v3}, Lcom/intsig/crashapm/log/FabricUtils;->O8(ZLandroid/content/Context;)V

    .line 57
    .line 58
    .line 59
    iget-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 60
    .line 61
    invoke-static {p1, v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->O80o(Landroid/content/Context;Z)V

    .line 62
    .line 63
    .line 64
    iget-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 65
    .line 66
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->o08800〇88(Landroid/content/Context;Z)V

    .line 67
    .line 68
    .line 69
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->checkLoginAccountIsSpecial()V

    .line 70
    .line 71
    .line 72
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    new-instance v0, Lcom/intsig/camscanner/tsapp/〇o00〇〇Oo;

    .line 77
    .line 78
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/tsapp/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {p1, v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 82
    .line 83
    .line 84
    const/4 p1, -0x1

    .line 85
    invoke-static {p1}, Lcom/intsig/camscanner/edu/EduGroupHelper;->oO80(I)V

    .line 86
    .line 87
    .line 88
    iget-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 89
    .line 90
    invoke-static {p1}, Lcom/intsig/camscanner/edu/EduGroupHelper;->〇o〇(Landroid/content/Context;)V

    .line 91
    .line 92
    .line 93
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 94
    .line 95
    invoke-static {p1}, Lcom/intsig/camscanner/uploadinfo/UploadDeviceInfo;->〇〇808〇(Ljava/lang/Boolean;)V

    .line 96
    .line 97
    .line 98
    sget-object p1, Lcom/intsig/camscanner/capture/util/BranchSdkUtils;->INSTANCE:Lcom/intsig/camscanner/capture/util/BranchSdkUtils;

    .line 99
    .line 100
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/util/BranchSdkUtils;->uploadBranchId()V

    .line 101
    .line 102
    .line 103
    iget-boolean p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->needSync:Z

    .line 104
    .line 105
    if-eqz p1, :cond_2

    .line 106
    .line 107
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 108
    .line 109
    invoke-static {p1}, Lcom/intsig/camscanner/app/AppUtil;->oO(Landroid/content/Context;)Z

    .line 110
    .line 111
    .line 112
    move-result p1

    .line 113
    if-nez p1, :cond_2

    .line 114
    .line 115
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O8ooOoo〇()Lcom/intsig/camscanner/tsapp/sync/SyncClient;

    .line 116
    .line 117
    .line 118
    move-result-object p1

    .line 119
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O0o〇〇Oo(Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    :cond_2
    new-instance p1, Lcom/intsig/tsapp/account/event/LoginSuccessEvent;

    .line 123
    .line 124
    invoke-direct {p1}, Lcom/intsig/tsapp/account/event/LoginSuccessEvent;-><init>()V

    .line 125
    .line 126
    .line 127
    invoke-static {p1}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 128
    .line 129
    .line 130
    invoke-static {v2}, Lcom/intsig/comm/account_data/AccountPreference;->O0o〇〇Oo(Z)V

    .line 131
    .line 132
    .line 133
    invoke-static {v2}, Lcom/intsig/tsapp/account/exp/ShowLoginGuideBubbleExp;->〇o〇(Z)V

    .line 134
    .line 135
    .line 136
    invoke-static {v2}, Lcom/intsig/tsapp/account/exp/ShowLoginGuideBubbleExp;->〇o00〇〇Oo(Z)V

    .line 137
    .line 138
    .line 139
    invoke-static {}, Lcom/intsig/camscanner/external_import/ExternalImportHelper;->Oo08()V

    .line 140
    .line 141
    .line 142
    goto :goto_1

    .line 143
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 144
    .line 145
    new-instance v3, Ljava/lang/StringBuilder;

    .line 146
    .line 147
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 148
    .line 149
    .line 150
    const-string v4, "LoginTask run result: "

    .line 151
    .line 152
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    .line 154
    .line 155
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 156
    .line 157
    .line 158
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 159
    .line 160
    .line 161
    move-result-object v3

    .line 162
    invoke-static {v0, v3}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    .line 164
    .line 165
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mAccount:Ljava/lang/String;

    .line 166
    .line 167
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 168
    .line 169
    .line 170
    move-result v0

    .line 171
    if-eqz v0, :cond_4

    .line 172
    .line 173
    goto :goto_0

    .line 174
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mAccount:Ljava/lang/String;

    .line 175
    .line 176
    const-string v2, "@"

    .line 177
    .line 178
    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 179
    .line 180
    .line 181
    move-result v2

    .line 182
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mLoginTaskListener:Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;

    .line 183
    .line 184
    if-eqz v0, :cond_6

    .line 185
    .line 186
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 187
    .line 188
    .line 189
    move-result v3

    .line 190
    invoke-interface {v0, v3}, Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;->isSafeVerifyConsumed(I)Z

    .line 191
    .line 192
    .line 193
    move-result v0

    .line 194
    if-eqz v0, :cond_5

    .line 195
    .line 196
    const-string p1, "LoginTask"

    .line 197
    .line 198
    const-string v0, "has already consumed."

    .line 199
    .line 200
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    .line 202
    .line 203
    goto :goto_1

    .line 204
    :cond_5
    iget-object v0, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 205
    .line 206
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 207
    .line 208
    .line 209
    move-result v3

    .line 210
    invoke-static {v0, v3, v2}, Lcom/intsig/camscanner/util/StringUtil;->〇O00(Landroid/content/Context;IZ)Ljava/lang/String;

    .line 211
    .line 212
    .line 213
    move-result-object v0

    .line 214
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mLoginTaskListener:Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;

    .line 215
    .line 216
    iget-object v3, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 217
    .line 218
    const v4, 0x7f131d87

    .line 219
    .line 220
    .line 221
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 222
    .line 223
    .line 224
    move-result-object v3

    .line 225
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 226
    .line 227
    .line 228
    move-result p1

    .line 229
    invoke-interface {v2, v3, p1, v0}, Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;->showErrorDialog(Ljava/lang/String;ILjava/lang/String;)V

    .line 230
    .line 231
    .line 232
    :cond_6
    :goto_1
    iput-object v1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mLoginTaskListener:Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;

    .line 233
    .line 234
    return-void
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public setBasicPara(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const-string v0, "extra_area_code"

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mAreaCode:Ljava/lang/String;

    .line 10
    .line 11
    const-string v0, "extra_account"

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mAccount:Ljava/lang/String;

    .line 18
    .line 19
    const-string v0, "extra_pwd"

    .line 20
    .line 21
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mPasswords:Ljava/lang/String;

    .line 26
    .line 27
    const-string v0, "extra_intent_action"

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mIntentAction:Ljava/lang/String;

    .line 34
    .line 35
    const-string v0, "extra_tag"

    .line 36
    .line 37
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mTag:Ljava/lang/String;

    .line 42
    .line 43
    const-string v0, "extra_login_task_listener"

    .line 44
    .line 45
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    check-cast p1, Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;

    .line 50
    .line 51
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mLoginTaskListener:Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;

    .line 52
    .line 53
    :cond_0
    iget-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 54
    .line 55
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oO〇(Landroid/content/Context;)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    if-nez v0, :cond_1

    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mAccount:Ljava/lang/String;

    .line 66
    .line 67
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 68
    .line 69
    .line 70
    move-result p1

    .line 71
    if-nez p1, :cond_1

    .line 72
    .line 73
    const/4 p1, 0x1

    .line 74
    iput-boolean p1, p0, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;->mIsChangeAccount:Z

    .line 75
    .line 76
    :cond_1
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

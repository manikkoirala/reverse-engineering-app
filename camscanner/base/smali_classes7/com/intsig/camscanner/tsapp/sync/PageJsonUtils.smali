.class public final Lcom/intsig/camscanner/tsapp/sync/PageJsonUtils;
.super Ljava/lang/Object;
.source "PageJsonUtils.java"


# static fields
.field private static final 〇080:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 26

    .line 1
    const-string v0, "_id"

    .line 2
    .line 3
    const-string v1, "sync_jpage_state"

    .line 4
    .line 5
    const-string v2, "sync_image_id"

    .line 6
    .line 7
    const-string v3, "note"

    .line 8
    .line 9
    const-string v4, "ocr_result"

    .line 10
    .line 11
    const-string v5, "enhance_mode"

    .line 12
    .line 13
    const-string v6, "last_modified"

    .line 14
    .line 15
    const-string v7, "image_border"

    .line 16
    .line 17
    const-string v8, "image_titile"

    .line 18
    .line 19
    const-string v9, "created_time"

    .line 20
    .line 21
    const-string v10, "min_version"

    .line 22
    .line 23
    const-string v11, "max_version"

    .line 24
    .line 25
    const-string v12, "detail_index"

    .line 26
    .line 27
    const-string v13, "contrast_index"

    .line 28
    .line 29
    const-string v14, "bright_index"

    .line 30
    .line 31
    const-string v15, "image_rotation"

    .line 32
    .line 33
    const-string v16, "sync_extra_data1"

    .line 34
    .line 35
    const-string v17, "document_id"

    .line 36
    .line 37
    const-string v18, "sync_state"

    .line 38
    .line 39
    const-string v19, "ocr_border"

    .line 40
    .line 41
    const-string v20, "ocr_result_user"

    .line 42
    .line 43
    const-string v21, "sync_jpage_version"

    .line 44
    .line 45
    const-string v22, "camcard_state"

    .line 46
    .line 47
    const-string v23, "ori_rotation"

    .line 48
    .line 49
    const-string v24, "ocr_paragraph"

    .line 50
    .line 51
    const-string v25, "_data"

    .line 52
    .line 53
    filled-new-array/range {v0 .. v25}, [Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    sput-object v0, Lcom/intsig/camscanner/tsapp/sync/PageJsonUtils;->〇080:[Ljava/lang/String;

    .line 58
    .line 59
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private static O8(J)Lorg/json/JSONArray;
    .locals 7

    .line 1
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$NotePath;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 2
    .line 3
    invoke-static {v0, p0, p1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 4
    .line 5
    .line 6
    move-result-object v2

    .line 7
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/DocJsonUtils;->o〇0()Landroid/content/ContentResolver;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const-string p0, "pen_points"

    .line 12
    .line 13
    const-string p1, "sync_extra_data1"

    .line 14
    .line 15
    const-string v0, "pen_type"

    .line 16
    .line 17
    const-string v3, "pen_color"

    .line 18
    .line 19
    const-string v4, "pen_width"

    .line 20
    .line 21
    filled-new-array {v0, v3, v4, p0, p1}, [Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    const/4 v4, 0x0

    .line 26
    const/4 v5, 0x0

    .line 27
    const/4 v6, 0x0

    .line 28
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 29
    .line 30
    .line 31
    move-result-object p0

    .line 32
    if-eqz p0, :cond_3

    .line 33
    .line 34
    new-instance p1, Lorg/json/JSONArray;

    .line 35
    .line 36
    invoke-direct {p1}, Lorg/json/JSONArray;-><init>()V

    .line 37
    .line 38
    .line 39
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_2

    .line 44
    .line 45
    new-instance v0, Lcom/intsig/camscanner/tsapp/sync/PenJson;

    .line 46
    .line 47
    invoke-direct {v0}, Lcom/intsig/camscanner/tsapp/sync/PenJson;-><init>()V

    .line 48
    .line 49
    .line 50
    const/4 v1, 0x0

    .line 51
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/PenJson;->〇8o8o〇(I)V

    .line 56
    .line 57
    .line 58
    const/4 v1, 0x1

    .line 59
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    .line 60
    .line 61
    .line 62
    move-result v1

    .line 63
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/PenJson;->〇80〇808〇O(I)V

    .line 64
    .line 65
    .line 66
    const/4 v1, 0x2

    .line 67
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getFloat(I)F

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    float-to-double v1, v1

    .line 72
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/sync/PenJson;->〇O8o08O(D)V

    .line 73
    .line 74
    .line 75
    const/4 v1, 0x3

    .line 76
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 81
    .line 82
    .line 83
    move-result v2

    .line 84
    const-string v3, ""

    .line 85
    .line 86
    if-eqz v2, :cond_0

    .line 87
    .line 88
    move-object v1, v3

    .line 89
    :cond_0
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/PenJson;->OO0o〇〇〇〇0(Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    const/4 v1, 0x4

    .line 93
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v1

    .line 97
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 98
    .line 99
    .line 100
    move-result v2

    .line 101
    if-eqz v2, :cond_1

    .line 102
    .line 103
    goto :goto_1

    .line 104
    :cond_1
    move-object v3, v1

    .line 105
    :goto_1
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/tsapp/sync/PenJson;->oO80(Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    .line 109
    .line 110
    .line 111
    move-result v1

    .line 112
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/PenJson;->〇080()Lorg/json/JSONObject;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    .line 118
    .line 119
    goto :goto_0

    .line 120
    :catch_0
    move-exception v0

    .line 121
    const-string v1, "PageJsonUtils"

    .line 122
    .line 123
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 124
    .line 125
    .line 126
    goto :goto_0

    .line 127
    :cond_2
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 128
    .line 129
    .line 130
    goto :goto_2

    .line 131
    :cond_3
    const/4 p1, 0x0

    .line 132
    :goto_2
    return-object p1
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public static Oo08(J)Landroid/database/Cursor;
    .locals 7

    .line 1
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 2
    .line 3
    invoke-static {v0, p0, p1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 4
    .line 5
    .line 6
    move-result-object v2

    .line 7
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/DocJsonUtils;->o〇0()Landroid/content/ContentResolver;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    sget-object v3, Lcom/intsig/camscanner/tsapp/sync/PageJsonUtils;->〇080:[Ljava/lang/String;

    .line 12
    .line 13
    const/4 v4, 0x0

    .line 14
    const/4 v5, 0x0

    .line 15
    const/4 v6, 0x0

    .line 16
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    return-object p0
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static o〇0(J)Landroid/database/Cursor;
    .locals 6

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/DocJsonUtils;->o〇0()Landroid/content/ContentResolver;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {p0, p1}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    sget-object v2, Lcom/intsig/camscanner/tsapp/sync/PageJsonUtils;->〇080:[Ljava/lang/String;

    .line 10
    .line 11
    const/4 v3, 0x0

    .line 12
    const/4 v4, 0x0

    .line 13
    const/4 v5, 0x0

    .line 14
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 15
    .line 16
    .line 17
    move-result-object p0

    .line 18
    return-object p0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static 〇080(Landroid/database/Cursor;JLjava/lang/String;)Lcom/intsig/camscanner/tsapp/sync/PageJson;
    .locals 7

    .line 1
    new-instance v0, Lcom/intsig/camscanner/tsapp/sync/PageJson;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/tsapp/sync/PageJson;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->O〇O〇oO(Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    const/4 p3, 0x3

    .line 10
    invoke-interface {p0, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object p3

    .line 14
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    const-string v2, ""

    .line 19
    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    move-object p3, v2

    .line 23
    :cond_0
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->Ooo(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    new-instance p3, Lcom/intsig/camscanner/tsapp/sync/OcrJson;

    .line 27
    .line 28
    invoke-direct {p3}, Lcom/intsig/camscanner/tsapp/sync/OcrJson;-><init>()V

    .line 29
    .line 30
    .line 31
    const/16 v1, 0x13

    .line 32
    .line 33
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-virtual {p3, v1}, Lcom/intsig/camscanner/tsapp/sync/OcrJson;->oO80(Ljava/lang/String;)Z

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    if-eqz v1, :cond_2

    .line 42
    .line 43
    invoke-virtual {p3}, Lcom/intsig/camscanner/tsapp/sync/OcrJson;->〇080()Lorg/json/JSONObject;

    .line 44
    .line 45
    .line 46
    move-result-object p3

    .line 47
    if-eqz p3, :cond_1

    .line 48
    .line 49
    invoke-virtual {p3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object p3

    .line 53
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->〇O〇80o08O(Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_1
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->〇O〇80o08O(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_2
    const-string p3, "PageJsonUtils"

    .line 62
    .line 63
    const-string v1, "fail to read ocr from file"

    .line 64
    .line 65
    invoke-static {p3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->〇O〇80o08O(Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    :goto_0
    const/4 p3, 0x5

    .line 72
    invoke-interface {p0, p3}, Landroid/database/Cursor;->getInt(I)I

    .line 73
    .line 74
    .line 75
    move-result p3

    .line 76
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->Oo〇O(I)V

    .line 77
    .line 78
    .line 79
    const/4 p3, 0x6

    .line 80
    invoke-interface {p0, p3}, Landroid/database/Cursor;->getLong(I)J

    .line 81
    .line 82
    .line 83
    move-result-wide v3

    .line 84
    const-wide/16 v5, 0x3e8

    .line 85
    .line 86
    div-long/2addr v3, v5

    .line 87
    invoke-virtual {v0, v3, v4}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->o88〇OO08〇(J)V

    .line 88
    .line 89
    .line 90
    const/4 p3, 0x7

    .line 91
    invoke-interface {p0, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object p3

    .line 95
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 96
    .line 97
    .line 98
    move-result v1

    .line 99
    if-eqz v1, :cond_3

    .line 100
    .line 101
    move-object p3, v2

    .line 102
    :cond_3
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->Oo8Oo00oo(Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    const/16 p3, 0x8

    .line 106
    .line 107
    invoke-interface {p0, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object p3

    .line 111
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 112
    .line 113
    .line 114
    move-result v1

    .line 115
    if-eqz v1, :cond_4

    .line 116
    .line 117
    move-object p3, v2

    .line 118
    :cond_4
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->O8O〇(Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    const/16 p3, 0x9

    .line 122
    .line 123
    invoke-interface {p0, p3}, Landroid/database/Cursor;->getLong(I)J

    .line 124
    .line 125
    .line 126
    move-result-wide v3

    .line 127
    div-long/2addr v3, v5

    .line 128
    invoke-virtual {v0, v3, v4}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->o0O0(J)V

    .line 129
    .line 130
    .line 131
    const/16 p3, 0xa

    .line 132
    .line 133
    invoke-interface {p0, p3}, Landroid/database/Cursor;->getFloat(I)F

    .line 134
    .line 135
    .line 136
    move-result p3

    .line 137
    const/16 v1, 0xb

    .line 138
    .line 139
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getFloat(I)F

    .line 140
    .line 141
    .line 142
    move-result v1

    .line 143
    const/high16 v3, 0x3f800000    # 1.0f

    .line 144
    .line 145
    cmpl-float v4, v3, v1

    .line 146
    .line 147
    if-lez v4, :cond_5

    .line 148
    .line 149
    const/high16 p3, 0x3f800000    # 1.0f

    .line 150
    .line 151
    const/high16 v1, 0x3f800000    # 1.0f

    .line 152
    .line 153
    :cond_5
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->〇80(F)V

    .line 154
    .line 155
    .line 156
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->O000(F)V

    .line 157
    .line 158
    .line 159
    const/16 p3, 0xc

    .line 160
    .line 161
    invoke-interface {p0, p3}, Landroid/database/Cursor;->getInt(I)I

    .line 162
    .line 163
    .line 164
    move-result p3

    .line 165
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->O08000(I)V

    .line 166
    .line 167
    .line 168
    const/16 p3, 0xd

    .line 169
    .line 170
    invoke-interface {p0, p3}, Landroid/database/Cursor;->getInt(I)I

    .line 171
    .line 172
    .line 173
    move-result p3

    .line 174
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->〇8(I)V

    .line 175
    .line 176
    .line 177
    const/16 p3, 0xe

    .line 178
    .line 179
    invoke-interface {p0, p3}, Landroid/database/Cursor;->getInt(I)I

    .line 180
    .line 181
    .line 182
    move-result p3

    .line 183
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->〇〇〇0〇〇0(I)V

    .line 184
    .line 185
    .line 186
    const/16 p3, 0xf

    .line 187
    .line 188
    invoke-interface {p0, p3}, Landroid/database/Cursor;->getInt(I)I

    .line 189
    .line 190
    .line 191
    move-result p3

    .line 192
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->〇〇o8(I)V

    .line 193
    .line 194
    .line 195
    const/16 p3, 0x10

    .line 196
    .line 197
    invoke-interface {p0, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 198
    .line 199
    .line 200
    move-result-object p3

    .line 201
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 202
    .line 203
    .line 204
    move-result v1

    .line 205
    if-eqz v1, :cond_6

    .line 206
    .line 207
    goto :goto_1

    .line 208
    :cond_6
    move-object v2, p3

    .line 209
    :goto_1
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->o8oO〇(Ljava/lang/String;)V

    .line 210
    .line 211
    .line 212
    invoke-static {p1, p2}, Lcom/intsig/camscanner/tsapp/sync/PageJsonUtils;->〇o00〇〇Oo(J)Lorg/json/JSONArray;

    .line 213
    .line 214
    .line 215
    move-result-object p3

    .line 216
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->oO00OOO(Lorg/json/JSONArray;)V

    .line 217
    .line 218
    .line 219
    const-string p3, "Android_CS6.56.0"

    .line 220
    .line 221
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->o〇8(Ljava/lang/String;)V

    .line 222
    .line 223
    .line 224
    invoke-static {p1, p2}, Lcom/intsig/camscanner/tsapp/sync/PageJsonUtils;->〇o〇(J)Lorg/json/JSONArray;

    .line 225
    .line 226
    .line 227
    move-result-object p1

    .line 228
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->〇0(Lorg/json/JSONArray;)V

    .line 229
    .line 230
    .line 231
    const/16 p1, 0x14

    .line 232
    .line 233
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 234
    .line 235
    .line 236
    move-result-object p2

    .line 237
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->O0o〇〇Oo(Ljava/lang/String;)V

    .line 238
    .line 239
    .line 240
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 241
    .line 242
    .line 243
    move-result-object p1

    .line 244
    const/4 p2, 0x0

    .line 245
    if-eqz p1, :cond_7

    .line 246
    .line 247
    const/4 p1, 0x1

    .line 248
    goto :goto_2

    .line 249
    :cond_7
    const/4 p1, 0x0

    .line 250
    :goto_2
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->oO(Z)V

    .line 251
    .line 252
    .line 253
    const/16 p1, 0x18

    .line 254
    .line 255
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 256
    .line 257
    .line 258
    move-result-object p1

    .line 259
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->OOO(Ljava/lang/String;)V

    .line 260
    .line 261
    .line 262
    const/16 p1, 0x16

    .line 263
    .line 264
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getInt(I)I

    .line 265
    .line 266
    .line 267
    move-result p1

    .line 268
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->o〇0OOo〇0(I)V

    .line 269
    .line 270
    .line 271
    const/16 p1, 0x17

    .line 272
    .line 273
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getInt(I)I

    .line 274
    .line 275
    .line 276
    move-result p0

    .line 277
    if-gez p0, :cond_8

    .line 278
    .line 279
    goto :goto_3

    .line 280
    :cond_8
    move p2, p0

    .line 281
    :goto_3
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/tsapp/sync/PageJson;->OO8oO0o〇(I)V

    .line 282
    .line 283
    .line 284
    return-object v0
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method static 〇o00〇〇Oo(J)Lorg/json/JSONArray;
    .locals 11

    .line 1
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Graphics;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 2
    .line 3
    invoke-static {v0, p0, p1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 4
    .line 5
    .line 6
    move-result-object v2

    .line 7
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/DocJsonUtils;->o〇0()Landroid/content/ContentResolver;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const-string v3, "_id"

    .line 12
    .line 13
    const-string v4, "pos_x"

    .line 14
    .line 15
    const-string v5, "pos_y"

    .line 16
    .line 17
    const-string v6, "width"

    .line 18
    .line 19
    const-string v7, "height"

    .line 20
    .line 21
    const-string v8, "sync_extra_data1"

    .line 22
    .line 23
    const-string v9, "scale"

    .line 24
    .line 25
    const-string v10, "dpi"

    .line 26
    .line 27
    filled-new-array/range {v3 .. v10}, [Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    const/4 v4, 0x0

    .line 32
    const/4 v5, 0x0

    .line 33
    const/4 v6, 0x0

    .line 34
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 35
    .line 36
    .line 37
    move-result-object p0

    .line 38
    const-string p1, "PageJsonUtils"

    .line 39
    .line 40
    if-eqz p0, :cond_4

    .line 41
    .line 42
    new-instance v0, Lorg/json/JSONArray;

    .line 43
    .line 44
    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 45
    .line 46
    .line 47
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    if-eqz v1, :cond_3

    .line 52
    .line 53
    const/4 v1, 0x0

    .line 54
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    .line 55
    .line 56
    .line 57
    move-result-wide v1

    .line 58
    const/4 v3, 0x1

    .line 59
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    .line 60
    .line 61
    .line 62
    move-result v3

    .line 63
    const/4 v4, 0x2

    .line 64
    invoke-interface {p0, v4}, Landroid/database/Cursor;->getInt(I)I

    .line 65
    .line 66
    .line 67
    move-result v4

    .line 68
    const/4 v5, 0x3

    .line 69
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getInt(I)I

    .line 70
    .line 71
    .line 72
    move-result v5

    .line 73
    const/4 v6, 0x4

    .line 74
    invoke-interface {p0, v6}, Landroid/database/Cursor;->getInt(I)I

    .line 75
    .line 76
    .line 77
    move-result v6

    .line 78
    new-instance v7, Lcom/intsig/camscanner/tsapp/sync/GraphicJson;

    .line 79
    .line 80
    invoke-direct {v7}, Lcom/intsig/camscanner/tsapp/sync/GraphicJson;-><init>()V

    .line 81
    .line 82
    .line 83
    const/4 v8, 0x5

    .line 84
    invoke-interface {p0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v8

    .line 88
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 89
    .line 90
    .line 91
    move-result v9

    .line 92
    if-eqz v9, :cond_0

    .line 93
    .line 94
    const-string v8, ""

    .line 95
    .line 96
    :cond_0
    invoke-virtual {v7, v8}, Lcom/intsig/camscanner/tsapp/sync/GraphicJson;->〇O8o08O(Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    new-instance v8, Ljava/lang/StringBuilder;

    .line 100
    .line 101
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    .line 103
    .line 104
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    const-string v3, "x"

    .line 108
    .line 109
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 113
    .line 114
    .line 115
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v4

    .line 119
    invoke-virtual {v7, v4}, Lcom/intsig/camscanner/tsapp/sync/GraphicJson;->OO0o〇〇(Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    new-instance v4, Ljava/lang/StringBuilder;

    .line 123
    .line 124
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    .line 126
    .line 127
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 128
    .line 129
    .line 130
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 134
    .line 135
    .line 136
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v3

    .line 140
    invoke-virtual {v7, v3}, Lcom/intsig/camscanner/tsapp/sync/GraphicJson;->Oooo8o0〇(Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    const/4 v3, 0x6

    .line 144
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getFloat(I)F

    .line 145
    .line 146
    .line 147
    move-result v3

    .line 148
    const/4 v4, 0x0

    .line 149
    cmpg-float v5, v3, v4

    .line 150
    .line 151
    if-gtz v5, :cond_1

    .line 152
    .line 153
    const/high16 v3, 0x3f800000    # 1.0f

    .line 154
    .line 155
    :cond_1
    const/4 v5, 0x7

    .line 156
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getFloat(I)F

    .line 157
    .line 158
    .line 159
    move-result v5

    .line 160
    cmpg-float v4, v5, v4

    .line 161
    .line 162
    if-gtz v4, :cond_2

    .line 163
    .line 164
    const/high16 v5, 0x43840000    # 264.0f

    .line 165
    .line 166
    :cond_2
    invoke-virtual {v7, v3}, Lcom/intsig/camscanner/tsapp/sync/GraphicJson;->〇O〇(F)V

    .line 167
    .line 168
    .line 169
    invoke-virtual {v7, v5}, Lcom/intsig/camscanner/tsapp/sync/GraphicJson;->〇8o8o〇(F)V

    .line 170
    .line 171
    .line 172
    invoke-static {v1, v2}, Lcom/intsig/camscanner/tsapp/sync/PageJsonUtils;->O8(J)Lorg/json/JSONArray;

    .line 173
    .line 174
    .line 175
    move-result-object v1

    .line 176
    invoke-virtual {v7, v1}, Lcom/intsig/camscanner/tsapp/sync/GraphicJson;->〇〇808〇(Lorg/json/JSONArray;)V

    .line 177
    .line 178
    .line 179
    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    .line 180
    .line 181
    .line 182
    move-result v1

    .line 183
    invoke-virtual {v7}, Lcom/intsig/camscanner/tsapp/sync/GraphicJson;->〇080()Lorg/json/JSONObject;

    .line 184
    .line 185
    .line 186
    move-result-object v2

    .line 187
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    .line 189
    .line 190
    goto/16 :goto_0

    .line 191
    .line 192
    :catch_0
    move-exception v1

    .line 193
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 194
    .line 195
    .line 196
    goto/16 :goto_0

    .line 197
    .line 198
    :cond_3
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 199
    .line 200
    .line 201
    goto :goto_1

    .line 202
    :cond_4
    const-string p0, "cursor = null"

    .line 203
    .line 204
    invoke-static {p1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    const/4 v0, 0x0

    .line 208
    :goto_1
    return-object v0
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private static 〇o〇(J)Lorg/json/JSONArray;
    .locals 12

    .line 1
    const-wide/16 v0, 0x0

    .line 2
    .line 3
    cmp-long v2, p0, v0

    .line 4
    .line 5
    if-lez v2, :cond_2

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$PageMark;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 8
    .line 9
    invoke-static {v0, p0, p1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    const-string v3, "mark_text"

    .line 14
    .line 15
    const-string v4, "mark_rotate"

    .line 16
    .line 17
    const-string v5, "mark_text_color"

    .line 18
    .line 19
    const-string v6, "mark_text_font_size"

    .line 20
    .line 21
    const-string v7, "mark_x"

    .line 22
    .line 23
    const-string v8, "mark_y"

    .line 24
    .line 25
    const-string v9, "mark_rect_height"

    .line 26
    .line 27
    const-string v10, "mark_rect_width"

    .line 28
    .line 29
    const-string v11, "sync_extra_data1"

    .line 30
    .line 31
    filled-new-array/range {v3 .. v11}, [Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/DocJsonUtils;->o〇0()Landroid/content/ContentResolver;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    const/4 v4, 0x0

    .line 40
    const/4 v5, 0x0

    .line 41
    const/4 v6, 0x0

    .line 42
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 43
    .line 44
    .line 45
    move-result-object p0

    .line 46
    if-eqz p0, :cond_2

    .line 47
    .line 48
    new-instance p1, Lorg/json/JSONArray;

    .line 49
    .line 50
    invoke-direct {p1}, Lorg/json/JSONArray;-><init>()V

    .line 51
    .line 52
    .line 53
    const/4 v0, 0x0

    .line 54
    const/4 v1, 0x0

    .line 55
    :cond_0
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    .line 56
    .line 57
    .line 58
    move-result v2

    .line 59
    if-eqz v2, :cond_1

    .line 60
    .line 61
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v2

    .line 65
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 66
    .line 67
    .line 68
    move-result v3

    .line 69
    if-nez v3, :cond_0

    .line 70
    .line 71
    new-instance v3, Lcom/intsig/camscanner/tsapp/sync/MarkJson;

    .line 72
    .line 73
    invoke-direct {v3}, Lcom/intsig/camscanner/tsapp/sync/MarkJson;-><init>()V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v3, v2}, Lcom/intsig/camscanner/tsapp/sync/MarkJson;->〇O〇(Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    const/4 v2, 0x1

    .line 80
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    .line 81
    .line 82
    .line 83
    move-result v2

    .line 84
    invoke-virtual {v3, v2}, Lcom/intsig/camscanner/tsapp/sync/MarkJson;->〇O888o0o(F)V

    .line 85
    .line 86
    .line 87
    const/4 v2, 0x2

    .line 88
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    .line 89
    .line 90
    .line 91
    move-result v2

    .line 92
    invoke-virtual {v3, v2}, Lcom/intsig/camscanner/tsapp/sync/MarkJson;->〇O00(I)V

    .line 93
    .line 94
    .line 95
    const/4 v2, 0x3

    .line 96
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    .line 97
    .line 98
    .line 99
    move-result v2

    .line 100
    invoke-virtual {v3, v2}, Lcom/intsig/camscanner/tsapp/sync/MarkJson;->oo88o8O(I)V

    .line 101
    .line 102
    .line 103
    const/4 v2, 0x4

    .line 104
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    .line 105
    .line 106
    .line 107
    move-result v2

    .line 108
    invoke-virtual {v3, v2}, Lcom/intsig/camscanner/tsapp/sync/MarkJson;->〇oo〇(F)V

    .line 109
    .line 110
    .line 111
    const/4 v2, 0x5

    .line 112
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    .line 113
    .line 114
    .line 115
    move-result v2

    .line 116
    invoke-virtual {v3, v2}, Lcom/intsig/camscanner/tsapp/sync/MarkJson;->o〇O8〇〇o(F)V

    .line 117
    .line 118
    .line 119
    const/4 v2, 0x6

    .line 120
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    .line 121
    .line 122
    .line 123
    move-result v2

    .line 124
    invoke-virtual {v3, v2}, Lcom/intsig/camscanner/tsapp/sync/MarkJson;->OoO8(F)V

    .line 125
    .line 126
    .line 127
    const/4 v2, 0x7

    .line 128
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    .line 129
    .line 130
    .line 131
    move-result v2

    .line 132
    invoke-virtual {v3, v2}, Lcom/intsig/camscanner/tsapp/sync/MarkJson;->o800o8O(F)V

    .line 133
    .line 134
    .line 135
    const/16 v2, 0x8

    .line 136
    .line 137
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    move-result-object v2

    .line 141
    invoke-virtual {v3, v2}, Lcom/intsig/camscanner/tsapp/sync/MarkJson;->〇〇808〇(Ljava/lang/String;)V

    .line 142
    .line 143
    .line 144
    :try_start_0
    invoke-virtual {v3}, Lcom/intsig/camscanner/tsapp/sync/MarkJson;->〇080()Lorg/json/JSONObject;

    .line 145
    .line 146
    .line 147
    move-result-object v2

    .line 148
    invoke-virtual {p1, v1, v2}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    .line 150
    .line 151
    add-int/lit8 v1, v1, 0x1

    .line 152
    .line 153
    goto :goto_0

    .line 154
    :catch_0
    move-exception v2

    .line 155
    const-string v3, "PageJsonUtils"

    .line 156
    .line 157
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 158
    .line 159
    .line 160
    goto :goto_0

    .line 161
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 162
    .line 163
    .line 164
    goto :goto_1

    .line 165
    :cond_2
    const/4 p1, 0x0

    .line 166
    :goto_1
    return-object p1
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

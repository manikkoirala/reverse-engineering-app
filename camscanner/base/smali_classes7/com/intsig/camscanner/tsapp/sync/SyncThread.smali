.class public Lcom/intsig/camscanner/tsapp/sync/SyncThread;
.super Ljava/lang/Object;
.source "SyncThread.java"

# interfaces
.implements Lcom/intsig/camscanner/tsapp/sync/SyncThreadContactListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/tsapp/sync/SyncThread$SyncMsgObject;,
        Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;,
        Lcom/intsig/camscanner/tsapp/sync/SyncThread$WorkHandler;,
        Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncFinishedListener;
    }
.end annotation


# static fields
.field public static volatile OoO8:Z = false

.field public static O〇8O8〇008:Z

.field private static o800o8O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;",
            ">;"
        }
    .end annotation
.end field

.field private static oo88o8O:Lcom/intsig/camscanner/tsapp/sync/SyncThread;

.field private static o〇O8〇〇o:Z

.field private static 〇00:Z

.field static 〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

.field private static 〇oo〇:Landroid/content/Context;


# instance fields
.field private O8:Lcom/intsig/camscanner/tsapp/sync/TeamDownloadState;

.field private final OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

.field private OO0o〇〇〇〇0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/tsapp/SyncCallbackListener;",
            ">;"
        }
    .end annotation
.end field

.field Oo08:Landroid/os/Handler;

.field private final Oooo8o0〇:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;",
            ">;"
        }
    .end annotation
.end field

.field oO80:I

.field o〇0:Landroid/os/HandlerThread;

.field private final 〇080:I

.field private 〇0〇O0088o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;",
            ">;"
        }
    .end annotation
.end field

.field private 〇80〇808〇O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/tsapp/SyncListener;",
            ">;"
        }
    .end annotation
.end field

.field private 〇8o8o〇:Z

.field private 〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

.field private 〇O8o08O:Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncFinishedListener;

.field private 〇O〇:[B

.field private final 〇o00〇〇Oo:I

.field private final 〇o〇:F

.field private 〇〇808〇:Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

.field private 〇〇888:J

.field 〇〇8O0〇8:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o800o8O:Ljava/util/List;

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    sput-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oo88o8O:Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    sput-boolean v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇O8〇〇o:Z

    .line 13
    .line 14
    sput-boolean v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇00:Z

    .line 15
    .line 16
    const/4 v0, 0x1

    .line 17
    sput-boolean v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇8O8〇008:Z

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0xbb8

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇080:I

    .line 7
    .line 8
    const/16 v0, 0x3e8

    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇o00〇〇Oo:I

    .line 11
    .line 12
    const/high16 v0, 0x42c80000    # 100.0f

    .line 13
    .line 14
    iput v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇o〇:F

    .line 15
    .line 16
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/TeamDownloadState;->O8()Lcom/intsig/camscanner/tsapp/sync/TeamDownloadState;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O8:Lcom/intsig/camscanner/tsapp/sync/TeamDownloadState;

    .line 21
    .line 22
    const-wide/16 v0, 0x0

    .line 23
    .line 24
    iput-wide v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇888:J

    .line 25
    .line 26
    const/4 v0, 0x0

    .line 27
    iput v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oO80:I

    .line 28
    .line 29
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 30
    .line 31
    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 32
    .line 33
    .line 34
    iput-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇80〇808〇O:Ljava/util/List;

    .line 35
    .line 36
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 37
    .line 38
    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 39
    .line 40
    .line 41
    iput-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇〇〇0:Ljava/util/List;

    .line 42
    .line 43
    iput-boolean v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇8o8o〇:Z

    .line 44
    .line 45
    new-instance v1, Lcom/intsig/camscanner/tsapp/SyncStatus;

    .line 46
    .line 47
    invoke-direct {v1}, Lcom/intsig/camscanner/tsapp/SyncStatus;-><init>()V

    .line 48
    .line 49
    .line 50
    iput-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    .line 51
    .line 52
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 53
    .line 54
    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 55
    .line 56
    .line 57
    iput-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oooo8o0〇:Ljava/util/List;

    .line 58
    .line 59
    new-instance v1, Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

    .line 60
    .line 61
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;-><init>(Lcom/intsig/camscanner/tsapp/sync/SyncThreadContactListener;)V

    .line 62
    .line 63
    .line 64
    iput-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇808〇:Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

    .line 65
    .line 66
    new-array v1, v0, [B

    .line 67
    .line 68
    iput-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O〇:[B

    .line 69
    .line 70
    new-instance v1, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 71
    .line 72
    invoke-direct {v1}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;-><init>()V

    .line 73
    .line 74
    .line 75
    iput-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 76
    .line 77
    const/4 v1, 0x0

    .line 78
    iput v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇8O0〇8:F

    .line 79
    .line 80
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 81
    .line 82
    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 83
    .line 84
    .line 85
    iput-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o:Ljava/util/List;

    .line 86
    .line 87
    const-string v1, "init SyncThread"

    .line 88
    .line 89
    const-string v2, "SyncThread"

    .line 90
    .line 91
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    new-instance v1, Landroid/os/HandlerThread;

    .line 95
    .line 96
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    iput-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0:Landroid/os/HandlerThread;

    .line 100
    .line 101
    const/4 v2, 0x4

    .line 102
    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setPriority(I)V

    .line 103
    .line 104
    .line 105
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0:Landroid/os/HandlerThread;

    .line 106
    .line 107
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 108
    .line 109
    .line 110
    new-instance v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread$WorkHandler;

    .line 111
    .line 112
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0:Landroid/os/HandlerThread;

    .line 113
    .line 114
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    .line 115
    .line 116
    .line 117
    move-result-object v2

    .line 118
    invoke-direct {v1, p0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread$WorkHandler;-><init>(Lcom/intsig/camscanner/tsapp/sync/SyncThread;Landroid/os/Looper;)V

    .line 119
    .line 120
    .line 121
    iput-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo08:Landroid/os/Handler;

    .line 122
    .line 123
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->o〇OOo000(Z)V

    .line 124
    .line 125
    .line 126
    sput-boolean v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OoO8:Z

    .line 127
    .line 128
    return-void
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private O00(Ljava/lang/String;)Z
    .locals 3

    .line 1
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇O〇(Ljava/lang/String;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "uploadRestorePoint result="

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v2, " json="

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    const-string v1, "SyncThread"

    .line 31
    .line 32
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    return v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private O000()Z
    .locals 6

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/utils/AppHelper;->〇o〇(Landroid/content/Context;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, 0x0

    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    return v2

    .line 17
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    const-string v3, "some_entrance_hide"

    .line 30
    .line 31
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-static {v1}, Lcom/intsig/utils/AppHelper;->O8([B)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    const-string v3, "10D13B4AA35BC5B21189CF7CEA331141"

    .line 47
    .line 48
    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 49
    .line 50
    .line 51
    move-result v3

    .line 52
    const/4 v4, 0x1

    .line 53
    if-nez v3, :cond_1

    .line 54
    .line 55
    const-string v3, "2F73B127C13CC300C75348F4AD24E681"

    .line 56
    .line 57
    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    if-eqz v3, :cond_2

    .line 62
    .line 63
    :cond_1
    const/4 v2, 0x1

    .line 64
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    .line 65
    .line 66
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    const-string v5, "sig = "

    .line 70
    .line 71
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    const-string v0, "     md5 = "

    .line 78
    .line 79
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    const-string v1, "SyncThread"

    .line 90
    .line 91
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    xor-int/lit8 v0, v2, 0x1

    .line 95
    .line 96
    return v0
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private O0OO8〇0(Lcom/intsig/camscanner/tsapp/SyncStatus;ILjava/lang/String;)Ljava/util/concurrent/Future;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/tsapp/SyncStatus;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/concurrent/Future<",
            "*>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇808〇:Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;->O〇8O8〇008()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇o〇()F

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇o(F)V

    .line 11
    .line 12
    .line 13
    sget-boolean v0, Lcom/intsig/camscanner/app/AppConfig;->〇o00〇〇Oo:Z

    .line 14
    .line 15
    const-string v1, "SyncThread"

    .line 16
    .line 17
    const/4 v2, 0x0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    sget-boolean v0, Lcom/intsig/camscanner/app/AppConfig;->O8:Z

    .line 21
    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const-string p1, "syncTeam current is padDevice"

    .line 26
    .line 27
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    return-object v2

    .line 31
    :cond_1
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 32
    .line 33
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇080O0(Landroid/content/Context;)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    const-string v3, " MANUAL_SYNC="

    .line 38
    .line 39
    const/4 v4, 0x1

    .line 40
    if-eqz v0, :cond_2

    .line 41
    .line 42
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 43
    .line 44
    invoke-static {v0}, Lcom/intsig/camscanner/app/DBUtil;->Oo(Landroid/content/Context;)I

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    if-lez v0, :cond_3

    .line 49
    .line 50
    :cond_2
    sget-boolean v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇8O8〇008:Z

    .line 51
    .line 52
    if-nez v0, :cond_4

    .line 53
    .line 54
    if-eq p2, v4, :cond_4

    .line 55
    .line 56
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 57
    .line 58
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->O000(Landroid/content/Context;)Z

    .line 59
    .line 60
    .line 61
    move-result v0

    .line 62
    if-eqz v0, :cond_3

    .line 63
    .line 64
    goto :goto_1

    .line 65
    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    .line 66
    .line 67
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 68
    .line 69
    .line 70
    const-string p2, "isTeamUser not sEnableTeamSync="

    .line 71
    .line 72
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    sget-boolean p2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇8O8〇008:Z

    .line 76
    .line 77
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object p1

    .line 90
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    goto/16 :goto_3

    .line 94
    .line 95
    :cond_4
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 96
    .line 97
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    .line 99
    .line 100
    const-string v5, "sEnableTeamSync="

    .line 101
    .line 102
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    sget-boolean v5, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇8O8〇008:Z

    .line 106
    .line 107
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    :try_start_0
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->〇8O0O808〇()Ljava/lang/String;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    new-instance v3, Ljava/lang/StringBuilder;

    .line 128
    .line 129
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 130
    .line 131
    .line 132
    const-string v5, "TianShuAPI.queryTeamList() teamListStr="

    .line 133
    .line 134
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object v3

    .line 144
    invoke-static {v1, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    .line 146
    .line 147
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 148
    .line 149
    .line 150
    move-result v3

    .line 151
    if-nez v3, :cond_5

    .line 152
    .line 153
    new-instance v3, Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson;

    .line 154
    .line 155
    invoke-direct {v3, v0}, Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    .line 157
    .line 158
    goto :goto_2

    .line 159
    :catch_0
    move-exception v0

    .line 160
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 161
    .line 162
    .line 163
    :cond_5
    move-object v3, v2

    .line 164
    :goto_2
    const/4 v0, 0x0

    .line 165
    sput-boolean v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇8O8〇008:Z

    .line 166
    .line 167
    const/high16 v0, 0x3fc00000    # 1.5f

    .line 168
    .line 169
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇o(F)V

    .line 170
    .line 171
    .line 172
    const/4 v0, -0x1

    .line 173
    invoke-virtual {p0, v0, p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 174
    .line 175
    .line 176
    if-nez v3, :cond_6

    .line 177
    .line 178
    const-string p1, "teamListJson == null"

    .line 179
    .line 180
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    .line 182
    .line 183
    goto :goto_3

    .line 184
    :cond_6
    const-string v5, "CSBusinesssyn"

    .line 185
    .line 186
    const-string v6, "synact"

    .line 187
    .line 188
    invoke-static {v5, v6}, Lcom/intsig/camscanner/log/LogAgentData;->o800o8O(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    .line 190
    .line 191
    iget-object v5, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇808〇:Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

    .line 192
    .line 193
    iget-object v6, v3, Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson;->list:[Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson$TeamInfoJson;

    .line 194
    .line 195
    iget-wide v7, v3, Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson;->service_time:J

    .line 196
    .line 197
    invoke-virtual {v5, v6, v7, v8}, Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;->oo〇([Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson$TeamInfoJson;J)V

    .line 198
    .line 199
    .line 200
    const v5, 0x3fcccccd    # 1.6f

    .line 201
    .line 202
    .line 203
    invoke-direct {p0, v5}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇o(F)V

    .line 204
    .line 205
    .line 206
    invoke-virtual {p0, v0, p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 207
    .line 208
    .line 209
    iget-object v0, v3, Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson;->list:[Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson$TeamInfoJson;

    .line 210
    .line 211
    if-eqz v0, :cond_7

    .line 212
    .line 213
    array-length v5, v0

    .line 214
    if-lez v5, :cond_7

    .line 215
    .line 216
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇808〇:Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

    .line 217
    .line 218
    array-length v2, v0

    .line 219
    int-to-float v2, v2

    .line 220
    const/high16 v5, 0x42c80000    # 100.0f

    .line 221
    .line 222
    iget v6, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇8O0〇8:F

    .line 223
    .line 224
    sub-float/2addr v5, v6

    .line 225
    mul-float v2, v2, v5

    .line 226
    .line 227
    array-length v0, v0

    .line 228
    add-int/2addr v0, v4

    .line 229
    int-to-float v0, v0

    .line 230
    div-float/2addr v2, v0

    .line 231
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;->OOO〇O0(F)V

    .line 232
    .line 233
    .line 234
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇808〇:Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

    .line 235
    .line 236
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;->〇oOO8O8(Lcom/intsig/camscanner/tsapp/SyncStatus;)V

    .line 237
    .line 238
    .line 239
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇808〇:Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

    .line 240
    .line 241
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;->o〇〇0〇(Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson;)V

    .line 242
    .line 243
    .line 244
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇808〇:Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

    .line 245
    .line 246
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;->〇0000OOO(I)V

    .line 247
    .line 248
    .line 249
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->〇0〇O0088o()Ljava/util/concurrent/ExecutorService;

    .line 250
    .line 251
    .line 252
    move-result-object p1

    .line 253
    new-instance p2, Lcom/intsig/camscanner/tsapp/sync/SyncThread$1;

    .line 254
    .line 255
    invoke-direct {p2, p0, p3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread$1;-><init>(Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/lang/String;)V

    .line 256
    .line 257
    .line 258
    invoke-interface {p1, p2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 259
    .line 260
    .line 261
    move-result-object p1

    .line 262
    move-object v2, p1

    .line 263
    goto :goto_3

    .line 264
    :cond_7
    const-string p1, "teamListJson.list is empty"

    .line 265
    .line 266
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    .line 268
    .line 269
    :goto_3
    return-object v2
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private O0o〇〇Oo()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o0O0()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oO00OOO()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇80()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_0

    .line 24
    .line 25
    const/4 v0, 0x1

    .line 26
    goto :goto_0

    .line 27
    :cond_0
    const/4 v0, 0x0

    .line 28
    :goto_0
    return v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private O0〇OO8(JZZ)Z
    .locals 12

    .line 1
    move-wide v6, p1

    .line 2
    move v0, p3

    .line 3
    move/from16 v8, p4

    .line 4
    .line 5
    const-string v1, " , hasMissedPage: "

    .line 6
    .line 7
    const-string v2, " , isOfficeDocType: "

    .line 8
    .line 9
    const-string v3, "updateLocalStateByDocUploadSuccess docId: "

    .line 10
    .line 11
    const-string v4, "SyncThread"

    .line 12
    .line 13
    const/4 v9, 0x1

    .line 14
    const/4 v10, 0x3

    .line 15
    const/4 v11, 0x0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    sget-object v5, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 19
    .line 20
    invoke-static {v5, p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O〇〇(Landroid/content/Context;J)Z

    .line 21
    .line 22
    .line 23
    move-result v5

    .line 24
    if-eqz v5, :cond_0

    .line 25
    .line 26
    sget-object v10, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 27
    .line 28
    invoke-static {v10, p1, p2, v11, v11}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 29
    .line 30
    .line 31
    sget-object v10, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 32
    .line 33
    invoke-static {v10, p1, p2, v11}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO88〇OOO(Landroid/content/Context;JI)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    sget-object v9, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 38
    .line 39
    invoke-static {v9, p1, p2, v10}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO88〇OOO(Landroid/content/Context;JI)V

    .line 40
    .line 41
    .line 42
    const/4 v9, 0x0

    .line 43
    :goto_0
    new-instance v10, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v10, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    const-string v0, " isOfficeDocComplete: "

    .line 67
    .line 68
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    goto :goto_3

    .line 82
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    .line 83
    .line 84
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 85
    .line 86
    .line 87
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 113
    .line 114
    invoke-static {v0, p1, p2}, Lcom/intsig/camscanner/db/dao/ImageDao;->oO(Landroid/content/Context;J)I

    .line 115
    .line 116
    .line 117
    move-result v0

    .line 118
    if-lez v0, :cond_3

    .line 119
    .line 120
    invoke-static {p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇Oo〇o8(J)Z

    .line 121
    .line 122
    .line 123
    move-result v0

    .line 124
    if-eqz v0, :cond_2

    .line 125
    .line 126
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 127
    .line 128
    const/4 v3, 0x0

    .line 129
    const/4 v4, 0x0

    .line 130
    const/4 v5, 0x0

    .line 131
    move-wide v1, p1

    .line 132
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    .line 133
    .line 134
    .line 135
    goto :goto_1

    .line 136
    :cond_2
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 137
    .line 138
    invoke-static {v0, p1, p2}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇O8o08O(Landroid/content/Context;J)I

    .line 139
    .line 140
    .line 141
    move-result v0

    .line 142
    if-lez v0, :cond_3

    .line 143
    .line 144
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 145
    .line 146
    invoke-static {v0, p1, p2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇00(Landroid/content/Context;J)I

    .line 147
    .line 148
    .line 149
    move-result v0

    .line 150
    if-ne v0, v9, :cond_3

    .line 151
    .line 152
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 153
    .line 154
    invoke-static {v0, p1, p2, v10, v11}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 155
    .line 156
    .line 157
    :cond_3
    const/4 v9, 0x0

    .line 158
    :goto_1
    if-eqz v9, :cond_5

    .line 159
    .line 160
    if-eqz v8, :cond_4

    .line 161
    .line 162
    goto :goto_2

    .line 163
    :cond_4
    const/4 v10, 0x0

    .line 164
    :goto_2
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 165
    .line 166
    invoke-static {v0, p1, p2, v10}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO88〇OOO(Landroid/content/Context;JI)V

    .line 167
    .line 168
    .line 169
    :cond_5
    :goto_3
    return v9
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private O880oOO08(J)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "startUploadDoc docId == "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "SyncThread"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o:Ljava/util/List;

    .line 24
    .line 25
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-nez v0, :cond_0

    .line 30
    .line 31
    return-void

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o:Ljava/util/List;

    .line 33
    .line 34
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    if-eqz v1, :cond_1

    .line 43
    .line 44
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    check-cast v1, Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;

    .line 49
    .line 50
    invoke-interface {v1, p1, p2}, Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;->O8(J)V

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_1
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private synthetic O8O〇(Ljava/util/concurrent/Future;[ZLcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;[Z[IJLcom/intsig/tianshu/sync/SyncState;IFFF[Z)V
    .locals 25

    move-object/from16 v12, p0

    move-object/from16 v0, p1

    .line 1
    new-instance v13, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;

    invoke-direct {v13}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;-><init>()V

    .line 2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sync doFuture: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v14, "SyncThread"

    invoke-static {v14, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x139

    const/4 v15, 0x1

    const/16 v16, 0x0

    if-eqz v0, :cond_6

    .line 3
    :try_start_0
    invoke-interface/range {p1 .. p1}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/camscanner/tsapp/sync/BaseUploadResponse;

    .line 4
    instance-of v2, v0, Lcom/intsig/camscanner/tsapp/sync/UploadImageResponse;

    if-eqz v2, :cond_0

    .line 5
    move-object v2, v0

    check-cast v2, Lcom/intsig/camscanner/tsapp/sync/UploadImageResponse;

    iget-boolean v2, v2, Lcom/intsig/camscanner/tsapp/sync/UploadImageResponse;->o〇0:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 6
    :goto_0
    :try_start_1
    iget-boolean v4, v0, Lcom/intsig/camscanner/tsapp/sync/BaseUploadResponse;->O8:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v4, :cond_1

    .line 7
    :try_start_2
    iget-object v5, v12, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-virtual {v5, v1}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 8
    invoke-virtual {v13, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->〇〇888(I)V

    goto :goto_1

    .line 9
    :cond_1
    iget v5, v0, Lcom/intsig/camscanner/tsapp/sync/BaseUploadResponse;->〇o00〇〇Oo:I

    const/16 v6, 0xc8

    if-eq v5, v6, :cond_2

    .line 10
    invoke-virtual {v13, v5}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->〇〇888(I)V

    .line 11
    :cond_2
    :goto_1
    iget-boolean v0, v0, Lcom/intsig/camscanner/tsapp/sync/BaseUploadResponse;->〇o〇:Z

    if-nez v0, :cond_5

    invoke-virtual {v13}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->O8()I

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x6

    .line 12
    invoke-virtual {v13, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->〇〇888(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_2
    const/4 v4, 0x0

    .line 13
    :goto_3
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 14
    instance-of v5, v0, Lcom/intsig/tianshu/exception/TianShuException;

    if-eqz v5, :cond_3

    .line 15
    check-cast v0, Lcom/intsig/tianshu/exception/TianShuException;

    .line 16
    iget-object v5, v12, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 17
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    move-result v0

    invoke-virtual {v13, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->〇〇888(I)V

    goto :goto_4

    .line 18
    :cond_3
    instance-of v0, v0, Ljava/lang/InterruptedException;

    if-eqz v0, :cond_4

    .line 19
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    const/4 v0, 0x5

    .line 20
    invoke-virtual {v13, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->〇〇888(I)V

    goto :goto_4

    :cond_4
    const/4 v0, 0x3

    .line 21
    invoke-virtual {v13, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->〇〇888(I)V

    :cond_5
    :goto_4
    move v0, v2

    move v11, v3

    move/from16 v17, v4

    goto :goto_5

    :cond_6
    const/4 v0, 0x0

    const/4 v11, 0x0

    const/16 v17, 0x0

    .line 22
    :goto_5
    aput-boolean v16, p2, v16

    .line 23
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->〇8o()Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v0, "uploadJsonFutureList needTerminate"

    .line 24
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 25
    :cond_7
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->o0O〇8o0O()Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v0, "upload doc isNeedInterruptUploadDoc"

    .line 26
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 27
    :cond_8
    invoke-virtual/range {p3 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->〇〇808〇(Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;)V

    .line 28
    iget-object v2, v12, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-virtual {v2}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇o00〇〇Oo()I

    move-result v2

    if-ne v2, v1, :cond_9

    const/4 v10, 0x1

    goto :goto_6

    :cond_9
    const/4 v10, 0x0

    :goto_6
    if-nez v10, :cond_a

    if-eqz v17, :cond_b

    .line 29
    :cond_a
    aput-boolean v15, p5, v16

    :cond_b
    if-nez v10, :cond_c

    if-eqz v17, :cond_d

    .line 30
    :cond_c
    aget v1, p6, v16

    add-int/2addr v1, v15

    aput v1, p6, v16

    .line 31
    :cond_d
    sget-object v18, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    invoke-virtual/range {v18 .. v18}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v2, v2, p7

    invoke-virtual {v1, v2, v3}, Lcom/intsig/tianshu/sync/SyncTimeCount;->OOO〇O0(J)V

    if-eqz v11, :cond_e

    .line 32
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "uploadLocalOfficeDoc2Server cost time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v2, v2, p7

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v14, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 33
    :cond_e
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "uploadLocalJpg2Server cost time ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v2, v2, p7

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ", hasMissedPage:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v14, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    :goto_7
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->Oooo8o0〇(Landroid/content/Context;J)Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

    move-result-object v19

    .line 35
    sget-object v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8〇(Landroid/content/Context;Ljava/lang/Long;)Z

    move-result v1

    .line 36
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sync upload file docId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, " , isCloudOver: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v3, " , isCloudOverrun: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-boolean v3, p5, v16

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v3, " , onlyDocAdd: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v14, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 38
    invoke-virtual {v13}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->O8()I

    move-result v2

    if-eqz v2, :cond_11

    if-nez v10, :cond_f

    aget-boolean v2, p5, v16

    if-eqz v2, :cond_10

    :cond_f
    if-nez v1, :cond_10

    goto :goto_8

    :cond_10
    move/from16 v24, v10

    move v15, v11

    goto :goto_9

    .line 39
    :cond_11
    :goto_8
    iget-object v2, v12, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    const/4 v5, 0x1

    .line 40
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v6

    const/4 v9, 0x5

    .line 41
    invoke-virtual/range {v19 .. v19}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o00〇〇Oo()I

    move-result v23

    move-object/from16 v1, p0

    move-object/from16 v3, p9

    move/from16 v4, p10

    move/from16 v8, p11

    move/from16 v24, v10

    move-object/from16 v10, v22

    move v15, v11

    move/from16 v11, v23

    .line 42
    invoke-direct/range {v1 .. v11}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇08(Lcom/intsig/camscanner/tsapp/SyncStatus;Lcom/intsig/tianshu/sync/SyncState;IZJFILjava/lang/String;I)I

    move-result v1

    .line 43
    invoke-virtual {v13, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->o〇0(I)V

    .line 44
    invoke-virtual/range {v18 .. v18}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v2, v2, v20

    invoke-virtual {v1, v2, v3}, Lcom/intsig/tianshu/sync/SyncTimeCount;->O8〇o(J)V

    .line 45
    :goto_9
    invoke-virtual {v13}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->O8()I

    move-result v1

    if-nez v1, :cond_14

    invoke-virtual {v13}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->〇o〇()I

    move-result v1

    if-nez v1, :cond_14

    .line 46
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 47
    iget-object v2, v12, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    const/4 v5, 0x1

    .line 48
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v6

    const/4 v9, 0x7

    .line 49
    invoke-virtual/range {v19 .. v19}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o00〇〇Oo()I

    move-result v11

    move-object/from16 v1, p0

    move-object/from16 v3, p9

    move/from16 v4, p10

    move/from16 v8, p12

    .line 50
    invoke-direct/range {v1 .. v11}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇OO(Lcom/intsig/camscanner/tsapp/SyncStatus;Lcom/intsig/tianshu/sync/SyncState;IZJFILjava/lang/String;I)I

    move-result v1

    .line 51
    invoke-virtual {v13, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->Oo08(I)V

    .line 52
    invoke-virtual/range {v18 .. v18}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v2, v2, v20

    invoke-virtual {v1, v2, v3}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇0000OOO(J)V

    .line 53
    invoke-virtual {v13}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->〇o00〇〇Oo()I

    move-result v1

    if-nez v1, :cond_13

    .line 54
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 55
    iget-object v2, v12, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    const/4 v5, 0x1

    .line 56
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v6

    const/4 v9, 0x2

    .line 57
    invoke-virtual/range {v19 .. v19}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o00〇〇Oo()I

    move-result v11

    move-object/from16 v1, p0

    move-object/from16 v3, p9

    move/from16 v4, p10

    move/from16 v8, p13

    .line 58
    invoke-direct/range {v1 .. v11}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇08(Lcom/intsig/camscanner/tsapp/SyncStatus;Lcom/intsig/tianshu/sync/SyncState;IZJFILjava/lang/String;I)I

    move-result v1

    .line 59
    invoke-virtual {v13, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->o〇0(I)V

    .line 60
    invoke-virtual/range {v18 .. v18}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v2, v2, v20

    invoke-virtual {v1, v2, v3}, Lcom/intsig/tianshu/sync/SyncTimeCount;->O8〇o(J)V

    .line 61
    invoke-virtual {v13}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->〇o〇()I

    move-result v1

    if-nez v1, :cond_12

    .line 62
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v1

    invoke-direct {v12, v1, v2, v15, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O0〇OO8(JZZ)Z

    move-result v0

    aput-boolean v0, p2, v16

    .line 63
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v0

    invoke-virtual {v12, v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo08(J)V

    goto :goto_a

    :cond_12
    const-string v0, "fail to delete jpage"

    .line 64
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    .line 65
    :cond_13
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "syncDocFolder failed docId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    .line 66
    :cond_14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "uploadLocalJpg2Server failed docId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    :goto_a
    if-nez v24, :cond_15

    if-eqz v17, :cond_18

    .line 67
    :cond_15
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇Oo〇o8(J)Z

    move-result v0

    const/4 v1, 0x4

    if-eqz v15, :cond_16

    .line 68
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v2

    invoke-direct {v12, v2, v3, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇000O0(JI)V

    goto :goto_b

    :cond_16
    if-nez v0, :cond_17

    .line 69
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/intsig/camscanner/db/dao/ImageDao;->oO(Landroid/content/Context;J)I

    move-result v0

    if-lez v0, :cond_17

    .line 70
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v2

    invoke-direct {v12, v2, v3, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇000O0(JI)V

    .line 71
    :cond_17
    :goto_b
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇8oOO88()Z

    move-result v0

    aput-boolean v0, p5, v16

    if-nez v0, :cond_18

    const v0, 0x7f130350

    .line 72
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o8〇O(I)V

    .line 73
    :cond_18
    aget-boolean v0, p14, v16

    invoke-virtual {v13}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;->〇080()I

    move-result v1

    if-nez v1, :cond_19

    const/4 v15, 0x1

    goto :goto_c

    :cond_19
    const/4 v15, 0x0

    :goto_c
    and-int/2addr v0, v15

    aput-boolean v0, p14, v16

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "finishUploadDoc docId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, " errorCode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v12, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    .line 75
    invoke-virtual {v1}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇o00〇〇Oo()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " success[0]:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-boolean v1, p14, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, " syncErrorCode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 76
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v0

    aget-boolean v2, p2, v16

    invoke-direct {v12, v0, v1, v2, v13}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OOO〇O0(JZLcom/intsig/camscanner/tsapp/sync/SyncErrorCode;)V

    return-void
.end method

.method private O8O〇88oO0(Lcom/intsig/camscanner/tsapp/SyncStatus;IJFLcom/intsig/camscanner/tsapp/sync/SyncProgressValue;I)Ljava/util/concurrent/Future;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/tsapp/SyncStatus;",
            "IJF",
            "Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;",
            "I)",
            "Ljava/util/concurrent/Future<",
            "Lcom/intsig/camscanner/tsapp/sync/BaseUploadResponse;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1, p5, p2, p6}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇O〇oO(Lcom/intsig/camscanner/tsapp/SyncStatus;FILcom/intsig/camscanner/tsapp/sync/SyncProgressValue;)Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    sget-object p2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 6
    .line 7
    invoke-static {p2, p3, p4, p1, p7}, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager;->OO0o〇〇〇〇0(Landroid/content/Context;JLcom/intsig/tianshu/sync/SyncApi$SyncProgress;I)Ljava/util/concurrent/Future;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    return-object p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
.end method

.method private O8ooOoo〇(Landroid/content/ContentResolver;Ljava/util/HashSet;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o0ooO(Ljava/util/HashSet;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "( sync_state <> ?  or sync_jpage_state <> ? ) and document_id in "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v6

    .line 22
    const-string v0, "5"

    .line 23
    .line 24
    filled-new-array {v0, v0}, [Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v7

    .line 28
    sget-object v4, Lcom/intsig/camscanner/provider/Documents$Image;->〇o〇:Landroid/net/Uri;

    .line 29
    .line 30
    const-string v0, "document_id"

    .line 31
    .line 32
    const-string v1, "sync_image_id"

    .line 33
    .line 34
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v5

    .line 38
    const/4 v8, 0x0

    .line 39
    move-object v3, p1

    .line 40
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    if-eqz p1, :cond_1

    .line 45
    .line 46
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    if-eqz v0, :cond_0

    .line 51
    .line 52
    const/4 v0, 0x0

    .line 53
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    .line 54
    .line 55
    .line 56
    move-result-wide v0

    .line 57
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    invoke-virtual {p2, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    const/4 v0, 0x1

    .line 65
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 70
    .line 71
    .line 72
    :cond_1
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private O8〇o()J
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo(Landroid/content/Context;)[J

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    array-length v1, v0

    .line 10
    const/4 v2, 0x2

    .line 11
    if-ne v1, v2, :cond_0

    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    aget-wide v1, v0, v1

    .line 15
    .line 16
    const/4 v3, 0x0

    .line 17
    aget-wide v3, v0, v3

    .line 18
    .line 19
    sub-long/2addr v1, v3

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const-wide/16 v1, 0x0

    .line 22
    .line 23
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string v3, "getAvailableStorage storage="

    .line 29
    .line 30
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    const-string v3, "SyncThread"

    .line 41
    .line 42
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    return-wide v1
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic OO0o〇〇(Lcom/intsig/camscanner/tsapp/sync/SyncThread;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇o0O0O8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/tsapp/sync/SyncThread;)Lcom/intsig/camscanner/tsapp/sync/TeamDownloadState;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O8:Lcom/intsig/camscanner/tsapp/sync/TeamDownloadState;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static OOO()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oo88o8O:Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-direct {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O0o〇〇Oo()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    xor-int/lit8 v0, v0, 0x1

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const-string v0, "SyncThread"

    .line 13
    .line 14
    const-string v1, "isNeedToUploadFiles mSyncThread is null"

    .line 15
    .line 16
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    const/4 v0, 0x0

    .line 20
    :goto_0
    return v0
    .line 21
.end method

.method private OOO8o〇〇(Z)V
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "ReLoginSyncThread:login error, need relogin, isPwdWrong = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "SyncThread"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 24
    .line 25
    invoke-static {v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    new-instance v1, Landroid/content/Intent;

    .line 30
    .line 31
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v2, "com.intsig.camscanner.relogin"

    .line 35
    .line 36
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    .line 38
    .line 39
    const-string v2, "is_pwd_wrong"

    .line 40
    .line 41
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private OOO〇O0(JZLcom/intsig/camscanner/tsapp/sync/SyncErrorCode;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o:Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_1

    .line 21
    .line 22
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    check-cast v1, Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;

    .line 27
    .line 28
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;->〇080(JZLcom/intsig/camscanner/tsapp/sync/SyncErrorCode;)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private OOo8o〇O(Ljava/util/List;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_7

    .line 7
    .line 8
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-nez v1, :cond_0

    .line 13
    .line 14
    goto/16 :goto_2

    .line 15
    .line 16
    :cond_0
    invoke-static {p1}, Lcom/intsig/camscanner/app/DBUtil;->Oo08(Ljava/util/Collection;)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    new-instance v1, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v2, "sync_state !=? AND _id in ("

    .line 26
    .line 27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string p1, ")"

    .line 34
    .line 35
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-static {}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->〇80〇808〇O()Ljava/util/List;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    if-lez v2, :cond_3

    .line 51
    .line 52
    new-instance v2, Ljava/lang/StringBuilder;

    .line 53
    .line 54
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 55
    .line 56
    .line 57
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 62
    .line 63
    .line 64
    move-result v3

    .line 65
    if-eqz v3, :cond_2

    .line 66
    .line 67
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 68
    .line 69
    .line 70
    move-result-object v3

    .line 71
    check-cast v3, Ljava/lang/String;

    .line 72
    .line 73
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    .line 74
    .line 75
    .line 76
    move-result v4

    .line 77
    if-lez v4, :cond_1

    .line 78
    .line 79
    const-string v4, ","

    .line 80
    .line 81
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    :cond_1
    const-string v4, "\'"

    .line 85
    .line 86
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    goto :goto_0

    .line 96
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    .line 97
    .line 98
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .line 100
    .line 101
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    const-string p1, " and ( "

    .line 105
    .line 106
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    const-string p1, "sync_dir_id"

    .line 110
    .line 111
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    const-string v3, " not in ("

    .line 115
    .line 116
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    const-string v2, ") or "

    .line 123
    .line 124
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    .line 129
    .line 130
    const-string p1, " IS NULL )"

    .line 131
    .line 132
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    .line 134
    .line 135
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object p1

    .line 139
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/newsign/data/dao/ESignDbDao;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 140
    .line 141
    .line 142
    move-result-object v1

    .line 143
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 144
    .line 145
    .line 146
    move-result v2

    .line 147
    if-nez v2, :cond_4

    .line 148
    .line 149
    new-instance v2, Ljava/lang/StringBuilder;

    .line 150
    .line 151
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 152
    .line 153
    .line 154
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    .line 156
    .line 157
    const-string p1, " AND "

    .line 158
    .line 159
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    .line 161
    .line 162
    const-string p1, "sync_doc_id"

    .line 163
    .line 164
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    .line 166
    .line 167
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    .line 169
    .line 170
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 171
    .line 172
    .line 173
    move-result-object p1

    .line 174
    :cond_4
    move-object v4, p1

    .line 175
    const-string p1, "5"

    .line 176
    .line 177
    filled-new-array {p1}, [Ljava/lang/String;

    .line 178
    .line 179
    .line 180
    move-result-object v5

    .line 181
    const-string p1, "_id"

    .line 182
    .line 183
    const-string v1, "modified"

    .line 184
    .line 185
    filled-new-array {p1, v1}, [Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v3

    .line 189
    sget-object p1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 190
    .line 191
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 192
    .line 193
    .line 194
    move-result-object v1

    .line 195
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Document;->O8:Landroid/net/Uri;

    .line 196
    .line 197
    sget-object p1, Lcom/intsig/camscanner/util/CONSTANT;->〇o00〇〇Oo:[Ljava/lang/String;

    .line 198
    .line 199
    const/4 v7, 0x1

    .line 200
    aget-object v6, p1, v7

    .line 201
    .line 202
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 203
    .line 204
    .line 205
    move-result-object p1

    .line 206
    if-eqz p1, :cond_6

    .line 207
    .line 208
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 209
    .line 210
    .line 211
    move-result v1

    .line 212
    if-eqz v1, :cond_5

    .line 213
    .line 214
    new-instance v1, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;

    .line 215
    .line 216
    const/4 v2, 0x0

    .line 217
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    .line 218
    .line 219
    .line 220
    move-result-wide v9

    .line 221
    const/4 v11, 0x0

    .line 222
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getLong(I)J

    .line 223
    .line 224
    .line 225
    move-result-wide v12

    .line 226
    move-object v8, v1

    .line 227
    invoke-direct/range {v8 .. v13}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;-><init>(JIJ)V

    .line 228
    .line 229
    .line 230
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    .line 232
    .line 233
    goto :goto_1

    .line 234
    :cond_5
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 235
    .line 236
    .line 237
    :cond_6
    return-object v0

    .line 238
    :cond_7
    :goto_2
    const-string p1, "SyncThread"

    .line 239
    .line 240
    const-string v1, "sortDownLoadDoc inputDocIdList is empty"

    .line 241
    .line 242
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    .line 244
    .line 245
    return-object v0
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private Oo8Oo00oo()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/tianshu/FolderState;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/tianshu/exception/TianShuException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o800o8O:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-eqz v2, :cond_2

    .line 17
    .line 18
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    check-cast v2, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;

    .line 23
    .line 24
    iget-boolean v3, v2, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇〇888:Z

    .line 25
    .line 26
    if-eqz v3, :cond_1

    .line 27
    .line 28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    if-lez v3, :cond_0

    .line 33
    .line 34
    const-string v3, ","

    .line 35
    .line 36
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    :cond_0
    iget-object v3, v2, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇080:Ljava/lang/String;

    .line 40
    .line 41
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    const-string v3, ":"

    .line 45
    .line 46
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-interface {v2}, Lcom/intsig/tianshu/sync/SyncAdapter;->O8()Lcom/intsig/tianshu/TSFolder;

    .line 50
    .line 51
    .line 52
    move-result-object v3

    .line 53
    invoke-virtual {v3}, Lcom/intsig/tianshu/TSFile;->〇o00〇〇Oo()I

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    :cond_1
    const/4 v3, -0x1

    .line 61
    iput v3, v2, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o00〇〇Oo:I

    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    .line 65
    .line 66
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    const-string v2, "queryUpdate what="

    .line 70
    .line 71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v1

    .line 85
    const-string v2, "SyncThread"

    .line 86
    .line 87
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    new-instance v1, Ljava/util/ArrayList;

    .line 91
    .line 92
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 93
    .line 94
    .line 95
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 96
    .line 97
    .line 98
    move-result-wide v3

    .line 99
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object v0

    .line 103
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->oo0O〇0〇〇〇(Ljava/lang/String;)Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    sget-object v5, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 108
    .line 109
    invoke-virtual {v5}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 110
    .line 111
    .line 112
    move-result-object v6

    .line 113
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 114
    .line 115
    .line 116
    move-result-wide v7

    .line 117
    sub-long/2addr v7, v3

    .line 118
    invoke-virtual {v6, v7, v8}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇00(J)V

    .line 119
    .line 120
    .line 121
    invoke-virtual {v5}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 122
    .line 123
    .line 124
    move-result-object v3

    .line 125
    const/4 v4, 0x1

    .line 126
    invoke-virtual {v3, v4}, Lcom/intsig/tianshu/sync/SyncTimeCount;->O〇8O8〇008(I)V

    .line 127
    .line 128
    .line 129
    new-instance v3, Ljava/lang/StringBuilder;

    .line 130
    .line 131
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 132
    .line 133
    .line 134
    const-string v5, "queryFolderUpdate2 result:"

    .line 135
    .line 136
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    .line 138
    .line 139
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    .line 141
    .line 142
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object v3

    .line 146
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    .line 148
    .line 149
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 150
    .line 151
    .line 152
    move-result v3

    .line 153
    if-nez v3, :cond_7

    .line 154
    .line 155
    :try_start_0
    new-instance v3, Lcom/intsig/tianshu/base/QueryFolderUpdate2Result;

    .line 156
    .line 157
    invoke-direct {v3, v0}, Lcom/intsig/tianshu/base/QueryFolderUpdate2Result;-><init>(Ljava/lang/String;)V

    .line 158
    .line 159
    .line 160
    iget-object v0, v3, Lcom/intsig/tianshu/base/QueryFolderUpdate2Result;->CamScanner_Doc:Lcom/intsig/tianshu/base/QueryFolderUpdate2Result$FolderInfo;

    .line 161
    .line 162
    if-eqz v0, :cond_3

    .line 163
    .line 164
    const-string v5, "CamScanner_Doc"

    .line 165
    .line 166
    invoke-static {v5, v0}, Lcom/intsig/tianshu/FolderState;->〇080(Ljava/lang/String;Lcom/intsig/tianshu/base/QueryFolderUpdate2Result$FolderInfo;)Lcom/intsig/tianshu/FolderState;

    .line 167
    .line 168
    .line 169
    move-result-object v0

    .line 170
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    .line 172
    .line 173
    :cond_3
    iget-object v0, v3, Lcom/intsig/tianshu/base/QueryFolderUpdate2Result;->CamScanner_Tag:Lcom/intsig/tianshu/base/QueryFolderUpdate2Result$FolderInfo;

    .line 174
    .line 175
    if-eqz v0, :cond_4

    .line 176
    .line 177
    const-string v5, "CamScanner_Tag"

    .line 178
    .line 179
    invoke-static {v5, v0}, Lcom/intsig/tianshu/FolderState;->〇080(Ljava/lang/String;Lcom/intsig/tianshu/base/QueryFolderUpdate2Result$FolderInfo;)Lcom/intsig/tianshu/FolderState;

    .line 180
    .line 181
    .line 182
    move-result-object v0

    .line 183
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    .line 185
    .line 186
    :cond_4
    iget-object v0, v3, Lcom/intsig/tianshu/base/QueryFolderUpdate2Result;->CamScanner_Page:Lcom/intsig/tianshu/base/QueryFolderUpdate2Result$FolderInfo;

    .line 187
    .line 188
    if-eqz v0, :cond_5

    .line 189
    .line 190
    const-string v5, "CamScanner_Page"

    .line 191
    .line 192
    invoke-static {v5, v0}, Lcom/intsig/tianshu/FolderState;->〇080(Ljava/lang/String;Lcom/intsig/tianshu/base/QueryFolderUpdate2Result$FolderInfo;)Lcom/intsig/tianshu/FolderState;

    .line 193
    .line 194
    .line 195
    move-result-object v0

    .line 196
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    .line 198
    .line 199
    :cond_5
    new-instance v0, Lcom/intsig/tianshu/FolderState;

    .line 200
    .line 201
    const-string v5, "data_check"

    .line 202
    .line 203
    iget v3, v3, Lcom/intsig/tianshu/base/QueryFolderUpdate2Result;->data_check:I

    .line 204
    .line 205
    if-ne v3, v4, :cond_6

    .line 206
    .line 207
    goto :goto_1

    .line 208
    :cond_6
    const/4 v4, 0x0

    .line 209
    :goto_1
    invoke-direct {v0, v5, v4}, Lcom/intsig/tianshu/FolderState;-><init>(Ljava/lang/String;Z)V

    .line 210
    .line 211
    .line 212
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    .line 214
    .line 215
    goto :goto_2

    .line 216
    :catch_0
    move-exception v0

    .line 217
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 218
    .line 219
    .line 220
    :cond_7
    :goto_2
    return-object v1
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private OoO8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient;->〇O8o08O:Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient$Companion;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient$Companion;->O8()Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient;->Oooo8o0〇()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static Ooo()Z
    .locals 1

    .line 1
    sget-boolean v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇O8〇〇o:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static Ooo8〇〇(Lcom/intsig/camscanner/tsapp/AccountListener;)V
    .locals 0

    .line 1
    sput-object p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic Oooo8o0〇(Lcom/intsig/camscanner/tsapp/sync/SyncThread;IIIJLjava/lang/String;)Lcom/intsig/tianshu/sync/SyncState;
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p6}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇Oooo〇〇(IIIJLjava/lang/String;)Lcom/intsig/tianshu/sync/SyncState;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
.end method

.method private Oo〇O8o〇8()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o800o8O:Ljava/util/List;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    check-cast v1, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇〇808〇()Z

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private Oo〇o(Lcom/intsig/camscanner/tsapp/SyncStatus;)V
    .locals 6

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 2
    .line 3
    const-string v1, "SyncThread"

    .line 4
    .line 5
    if-eqz v0, :cond_6

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇o00〇〇Oo()I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v2, "showSyncError errorCode="

    .line 17
    .line 18
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    const/16 v0, 0xcb

    .line 32
    .line 33
    if-eq p1, v0, :cond_5

    .line 34
    .line 35
    const/16 v0, 0xce

    .line 36
    .line 37
    if-eq p1, v0, :cond_4

    .line 38
    .line 39
    const/16 v0, 0x139

    .line 40
    .line 41
    const-class v2, Lcom/intsig/camscanner/SyncErrorActivity;

    .line 42
    .line 43
    const/4 v3, 0x0

    .line 44
    if-eq p1, v0, :cond_2

    .line 45
    .line 46
    const/16 v0, 0x13b

    .line 47
    .line 48
    if-eq p1, v0, :cond_0

    .line 49
    .line 50
    const-string v0, "com.intsig.camscanner.resyncnotification"

    .line 51
    .line 52
    packed-switch p1, :pswitch_data_0

    .line 53
    .line 54
    .line 55
    packed-switch p1, :pswitch_data_1

    .line 56
    .line 57
    .line 58
    packed-switch p1, :pswitch_data_2

    .line 59
    .line 60
    .line 61
    goto/16 :goto_1

    .line 62
    .line 63
    :pswitch_0
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇00O0O0(I)V

    .line 64
    .line 65
    .line 66
    goto/16 :goto_1

    .line 67
    .line 68
    :pswitch_1
    sget-object p1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 69
    .line 70
    new-instance v1, Landroid/content/Intent;

    .line 71
    .line 72
    sget-object v4, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 73
    .line 74
    invoke-direct {v1, v0, v3, v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 75
    .line 76
    .line 77
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 78
    .line 79
    const v2, 0x7f130432

    .line 80
    .line 81
    .line 82
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    sget-object v2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 87
    .line 88
    const v3, 0x7f1302f5

    .line 89
    .line 90
    .line 91
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v2

    .line 95
    invoke-static {p1, v1, v0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇〇0O8ooO(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    goto/16 :goto_1

    .line 99
    .line 100
    :pswitch_2
    sget-object p1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 101
    .line 102
    new-instance v1, Landroid/content/Intent;

    .line 103
    .line 104
    sget-object v4, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 105
    .line 106
    invoke-direct {v1, v0, v3, v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    .line 108
    .line 109
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 110
    .line 111
    const v2, 0x7f130239

    .line 112
    .line 113
    .line 114
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    sget-object v2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 119
    .line 120
    const v3, 0x7f1302c9

    .line 121
    .line 122
    .line 123
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 124
    .line 125
    .line 126
    move-result-object v2

    .line 127
    invoke-static {p1, v1, v0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇〇0O8ooO(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    .line 129
    .line 130
    goto/16 :goto_1

    .line 131
    .line 132
    :pswitch_3
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 133
    .line 134
    invoke-static {p1}, Lcom/intsig/camscanner/app/AppUtil;->oO(Landroid/content/Context;)Z

    .line 135
    .line 136
    .line 137
    move-result p1

    .line 138
    if-nez p1, :cond_7

    .line 139
    .line 140
    sget-object p1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 141
    .line 142
    new-instance v1, Landroid/content/Intent;

    .line 143
    .line 144
    sget-object v4, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 145
    .line 146
    invoke-direct {v1, v0, v3, v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 147
    .line 148
    .line 149
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 150
    .line 151
    const v2, 0x7f130555

    .line 152
    .line 153
    .line 154
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v0

    .line 158
    sget-object v2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 159
    .line 160
    const v3, 0x7f130564

    .line 161
    .line 162
    .line 163
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 164
    .line 165
    .line 166
    move-result-object v2

    .line 167
    invoke-static {p1, v1, v0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇〇0O8ooO(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    .line 169
    .line 170
    goto/16 :goto_1

    .line 171
    .line 172
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 173
    .line 174
    .line 175
    move-result p1

    .line 176
    if-eqz p1, :cond_1

    .line 177
    .line 178
    const-string p1, "VIP Dir  limit reached"

    .line 179
    .line 180
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    .line 182
    .line 183
    goto/16 :goto_1

    .line 184
    .line 185
    :cond_1
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 186
    .line 187
    invoke-static {p1}, Lcom/intsig/camscanner/app/AppUtil;->oO(Landroid/content/Context;)Z

    .line 188
    .line 189
    .line 190
    move-result p1

    .line 191
    if-nez p1, :cond_7

    .line 192
    .line 193
    const-string p1, "CSPush_notify"

    .line 194
    .line 195
    invoke-static {p1}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 196
    .line 197
    .line 198
    sget-object p1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 199
    .line 200
    invoke-static {p1}, Lcom/intsig/camscanner/settings/DirLimitReceiverManager;->〇o〇(Landroid/content/Context;)V

    .line 201
    .line 202
    .line 203
    sget-object p1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 204
    .line 205
    const v0, 0x7f13063d

    .line 206
    .line 207
    .line 208
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 209
    .line 210
    .line 211
    move-result-object p1

    .line 212
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 213
    .line 214
    new-instance v1, Landroid/content/Intent;

    .line 215
    .line 216
    const-string v4, "com.intsig.camscanner.persional_dir_limit"

    .line 217
    .line 218
    sget-object v5, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 219
    .line 220
    invoke-direct {v1, v4, v3, v5, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 221
    .line 222
    .line 223
    sget-object v2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 224
    .line 225
    const v3, 0x7f13041e

    .line 226
    .line 227
    .line 228
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 229
    .line 230
    .line 231
    move-result-object v2

    .line 232
    invoke-static {v0, v1, v2, p1, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇〇8(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;I)V

    .line 233
    .line 234
    .line 235
    goto :goto_1

    .line 236
    :cond_2
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 237
    .line 238
    invoke-static {p1}, Lcom/intsig/camscanner/app/AppUtil;->oO(Landroid/content/Context;)Z

    .line 239
    .line 240
    .line 241
    move-result p1

    .line 242
    if-nez p1, :cond_7

    .line 243
    .line 244
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 245
    .line 246
    .line 247
    move-result p1

    .line 248
    if-eqz p1, :cond_3

    .line 249
    .line 250
    const-string p1, "CSPushNotify"

    .line 251
    .line 252
    invoke-static {p1}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 253
    .line 254
    .line 255
    sget-object p1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 256
    .line 257
    const v0, 0x7f13034f

    .line 258
    .line 259
    .line 260
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 261
    .line 262
    .line 263
    move-result-object p1

    .line 264
    goto :goto_0

    .line 265
    :cond_3
    sget-object p1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 266
    .line 267
    const v0, 0x7f13034e

    .line 268
    .line 269
    .line 270
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 271
    .line 272
    .line 273
    move-result-object p1

    .line 274
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 275
    .line 276
    new-instance v1, Landroid/content/Intent;

    .line 277
    .line 278
    const-string v4, "com.intsig.camscanner.storagelimit"

    .line 279
    .line 280
    sget-object v5, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 281
    .line 282
    invoke-direct {v1, v4, v3, v5, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 283
    .line 284
    .line 285
    sget-object v2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 286
    .line 287
    const v3, 0x7f130350

    .line 288
    .line 289
    .line 290
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 291
    .line 292
    .line 293
    move-result-object v2

    .line 294
    invoke-static {v0, v1, v2, p1, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇〇8(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;I)V

    .line 295
    .line 296
    .line 297
    goto :goto_1

    .line 298
    :cond_4
    :pswitch_4
    const/4 p1, 0x1

    .line 299
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OOO8o〇〇(Z)V

    .line 300
    .line 301
    .line 302
    goto :goto_1

    .line 303
    :cond_5
    :pswitch_5
    const/4 p1, 0x0

    .line 304
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OOO8o〇〇(Z)V

    .line 305
    .line 306
    .line 307
    goto :goto_1

    .line 308
    :cond_6
    const-string p1, "showSyncError context = null"

    .line 309
    .line 310
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    .line 312
    .line 313
    :cond_7
    :goto_1
    return-void

    .line 314
    nop

    .line 315
    :pswitch_data_0
    .packed-switch -0x130
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_5
    .end packed-switch

    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    :pswitch_data_1
    .packed-switch -0x69
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    :pswitch_data_2
    .packed-switch 0x71
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
    .end packed-switch
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method private O〇08(Lcom/intsig/camscanner/tsapp/SyncStatus;Lcom/intsig/tianshu/sync/SyncState;IZJFILjava/lang/String;I)I
    .locals 20

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    move/from16 v2, p3

    .line 6
    .line 7
    move/from16 v3, p4

    .line 8
    .line 9
    const-string v4, "syncTagFolder"

    .line 10
    .line 11
    const-string v5, "SyncThread"

    .line 12
    .line 13
    invoke-static {v5, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 17
    .line 18
    .line 19
    move-result-wide v6

    .line 20
    sget-object v4, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o800o8O:Ljava/util/List;

    .line 21
    .line 22
    const-string v8, " errorCode="

    .line 23
    .line 24
    const/4 v9, 0x0

    .line 25
    if-eqz v4, :cond_9

    .line 26
    .line 27
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 28
    .line 29
    .line 30
    move-result v4

    .line 31
    if-lez v4, :cond_9

    .line 32
    .line 33
    new-instance v4, Ljava/lang/StringBuilder;

    .line 34
    .line 35
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .line 37
    .line 38
    const-string v10, "sync operation size ="

    .line 39
    .line 40
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    sget-object v10, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o800o8O:Ljava/util/List;

    .line 44
    .line 45
    invoke-interface {v10}, Ljava/util/List;->size()I

    .line 46
    .line 47
    .line 48
    move-result v10

    .line 49
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    const-string v10, "enableListfile="

    .line 53
    .line 54
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v4

    .line 64
    invoke-static {v5, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    sget-object v4, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o800o8O:Ljava/util/List;

    .line 68
    .line 69
    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 70
    .line 71
    .line 72
    move-result-object v4

    .line 73
    check-cast v4, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;

    .line 74
    .line 75
    invoke-virtual {v4, v3}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->o800o8O(Z)V

    .line 76
    .line 77
    .line 78
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇o〇()Ljava/util/Vector;

    .line 79
    .line 80
    .line 81
    move-result-object v10

    .line 82
    invoke-virtual {v4, v10}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇oo〇(Ljava/util/Vector;)V

    .line 83
    .line 84
    .line 85
    move-wide/from16 v10, p5

    .line 86
    .line 87
    invoke-virtual {v4, v10, v11}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->OoO8(J)V

    .line 88
    .line 89
    .line 90
    iget-boolean v10, v4, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇〇888:Z

    .line 91
    .line 92
    if-eqz v10, :cond_8

    .line 93
    .line 94
    const/4 v15, 0x3

    .line 95
    :try_start_0
    iget-object v10, v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 96
    .line 97
    invoke-virtual {v10}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇o〇()F

    .line 98
    .line 99
    .line 100
    move-result v14

    .line 101
    move-object/from16 v10, p2

    .line 102
    .line 103
    const/16 v17, 0x0

    .line 104
    .line 105
    :goto_0
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->〇8o()Z

    .line 106
    .line 107
    .line 108
    move-result v11

    .line 109
    const/4 v13, -0x1

    .line 110
    const-wide/16 v18, 0x0

    .line 111
    .line 112
    if-eqz v11, :cond_0

    .line 113
    .line 114
    const/4 v9, 0x1

    .line 115
    :goto_1
    move/from16 v11, v17

    .line 116
    .line 117
    const/4 v3, -0x1

    .line 118
    const/4 v12, 0x3

    .line 119
    goto/16 :goto_3

    .line 120
    .line 121
    :cond_0
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->o0O〇8o0O()Z

    .line 122
    .line 123
    .line 124
    move-result v11

    .line 125
    if-eqz v11, :cond_1

    .line 126
    .line 127
    if-eqz v3, :cond_1

    .line 128
    .line 129
    const/4 v9, 0x2

    .line 130
    goto :goto_1

    .line 131
    :cond_1
    iget-object v10, v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 132
    .line 133
    invoke-virtual {v10, v14}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇80〇808〇O(F)V

    .line 134
    .line 135
    .line 136
    iget-object v10, v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 137
    .line 138
    move/from16 v12, p7

    .line 139
    .line 140
    invoke-virtual {v1, v0, v12, v2, v10}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇O〇oO(Lcom/intsig/camscanner/tsapp/SyncStatus;FILcom/intsig/camscanner/tsapp/sync/SyncProgressValue;)Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;

    .line 141
    .line 142
    .line 143
    move-result-object v11

    .line 144
    iget v10, v4, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o00〇〇Oo:I

    .line 145
    .line 146
    iget v9, v4, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o〇:I

    .line 147
    .line 148
    move/from16 v16, v10

    .line 149
    .line 150
    move-object v10, v4

    .line 151
    move/from16 v12, v16

    .line 152
    .line 153
    const/4 v3, -0x1

    .line 154
    move v13, v9

    .line 155
    move v9, v14

    .line 156
    move/from16 v14, p8

    .line 157
    .line 158
    move-object/from16 v15, p9

    .line 159
    .line 160
    move/from16 v16, p10

    .line 161
    .line 162
    invoke-static/range {v10 .. v16}, Lcom/intsig/tianshu/sync/SyncApi;->Oo08(Lcom/intsig/tianshu/sync/SyncAdapter;Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;IIILjava/lang/String;I)Lcom/intsig/tianshu/sync/SyncState;

    .line 163
    .line 164
    .line 165
    move-result-object v10

    .line 166
    add-int/lit8 v11, v17, 0x1

    .line 167
    .line 168
    new-instance v12, Ljava/lang/StringBuilder;

    .line 169
    .line 170
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 171
    .line 172
    .line 173
    const-string v13, "sync tag operation repeat ="

    .line 174
    .line 175
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 179
    .line 180
    .line 181
    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    .line 183
    .line 184
    invoke-virtual {v10}, Lcom/intsig/tianshu/sync/SyncState;->〇o00〇〇Oo()I

    .line 185
    .line 186
    .line 187
    move-result v13

    .line 188
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 189
    .line 190
    .line 191
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 192
    .line 193
    .line 194
    move-result-object v12

    .line 195
    invoke-static {v5, v12}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    .line 197
    .line 198
    invoke-virtual {v10}, Lcom/intsig/tianshu/sync/SyncState;->〇o00〇〇Oo()I

    .line 199
    .line 200
    .line 201
    move-result v12

    .line 202
    const/16 v13, 0x15f

    .line 203
    .line 204
    if-ne v13, v12, :cond_2

    .line 205
    .line 206
    iput v3, v4, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o00〇〇Oo:I

    .line 207
    .line 208
    :cond_2
    invoke-virtual {v10}, Lcom/intsig/tianshu/sync/SyncState;->o〇0()J

    .line 209
    .line 210
    .line 211
    move-result-wide v12
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 212
    cmp-long v14, v12, v18

    .line 213
    .line 214
    const/4 v12, 0x3

    .line 215
    if-nez v14, :cond_4

    .line 216
    .line 217
    if-lt v11, v12, :cond_3

    .line 218
    .line 219
    goto :goto_2

    .line 220
    :cond_3
    move/from16 v3, p4

    .line 221
    .line 222
    move v14, v9

    .line 223
    move/from16 v17, v11

    .line 224
    .line 225
    const/4 v9, 0x0

    .line 226
    const/4 v15, 0x3

    .line 227
    goto :goto_0

    .line 228
    :cond_4
    :goto_2
    const/4 v9, 0x0

    .line 229
    :goto_3
    if-nez v10, :cond_5

    .line 230
    .line 231
    if-nez v9, :cond_7

    .line 232
    .line 233
    const/4 v15, 0x3

    .line 234
    goto :goto_4

    .line 235
    :cond_5
    :try_start_1
    invoke-virtual {v10}, Lcom/intsig/tianshu/sync/SyncState;->Oo08()Z

    .line 236
    .line 237
    .line 238
    move-result v13

    .line 239
    if-eqz v13, :cond_6

    .line 240
    .line 241
    const-string v9, "sync operation hasError "

    .line 242
    .line 243
    invoke-static {v5, v9}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    .line 245
    .line 246
    invoke-virtual {v10}, Lcom/intsig/tianshu/sync/SyncState;->〇o00〇〇Oo()I

    .line 247
    .line 248
    .line 249
    move-result v9

    .line 250
    invoke-virtual {v0, v9}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 251
    .line 252
    .line 253
    invoke-virtual {v1, v3, v0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 254
    .line 255
    .line 256
    invoke-virtual {v10}, Lcom/intsig/tianshu/sync/SyncState;->〇o00〇〇Oo()I

    .line 257
    .line 258
    .line 259
    move-result v15

    .line 260
    goto :goto_4

    .line 261
    :cond_6
    invoke-virtual {v10}, Lcom/intsig/tianshu/sync/SyncState;->o〇0()J

    .line 262
    .line 263
    .line 264
    move-result-wide v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 265
    cmp-long v0, v2, v18

    .line 266
    .line 267
    if-nez v0, :cond_7

    .line 268
    .line 269
    if-lt v11, v12, :cond_7

    .line 270
    .line 271
    const/4 v15, 0x4

    .line 272
    goto :goto_4

    .line 273
    :cond_7
    move v15, v9

    .line 274
    :goto_4
    move v9, v15

    .line 275
    goto :goto_6

    .line 276
    :catch_0
    move-exception v0

    .line 277
    goto :goto_5

    .line 278
    :catch_1
    move-exception v0

    .line 279
    const/4 v12, 0x3

    .line 280
    :goto_5
    invoke-static {v5, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 281
    .line 282
    .line 283
    const/4 v9, 0x3

    .line 284
    goto :goto_6

    .line 285
    :cond_8
    const/4 v9, 0x0

    .line 286
    :goto_6
    invoke-virtual {v4}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇〇8O0〇8()V

    .line 287
    .line 288
    .line 289
    goto :goto_7

    .line 290
    :cond_9
    const-string v0, "sync syncOperations = null"

    .line 291
    .line 292
    invoke-static {v5, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    .line 294
    .line 295
    const/4 v9, 0x0

    .line 296
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    .line 297
    .line 298
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 299
    .line 300
    .line 301
    const-string v2, "syncTagFolder cost time="

    .line 302
    .line 303
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    .line 305
    .line 306
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 307
    .line 308
    .line 309
    move-result-wide v2

    .line 310
    sub-long/2addr v2, v6

    .line 311
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 312
    .line 313
    .line 314
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    .line 316
    .line 317
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 318
    .line 319
    .line 320
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 321
    .line 322
    .line 323
    move-result-object v0

    .line 324
    invoke-static {v5, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    .line 326
    .line 327
    return v9
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
.end method

.method private O〇8O8〇008(Lcom/intsig/camscanner/tsapp/SyncStatus;IF)Ljava/util/concurrent/Future;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/tsapp/SyncStatus;",
            "IF)",
            "Ljava/util/concurrent/Future<",
            "*>;"
        }
    .end annotation

    .line 1
    const-string v0, "downloadImagesByDocs"

    .line 2
    .line 3
    const-string v1, "SyncThread"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    invoke-static {v0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O8OO08o(Landroid/content/Context;Ljava/lang/String;)Ljava/util/HashSet;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sget-object v3, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 16
    .line 17
    const/4 v4, 0x1

    .line 18
    invoke-static {v3, v4, v2, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Oo(Landroid/content/Context;ZLjava/lang/String;Ljava/util/HashSet;)Ljava/util/ArrayList;

    .line 19
    .line 20
    .line 21
    move-result-object v7

    .line 22
    if-eqz v7, :cond_1

    .line 23
    .line 24
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-lez v0, :cond_0

    .line 29
    .line 30
    int-to-float v0, v0

    .line 31
    div-float v9, p3, v0

    .line 32
    .line 33
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->〇0〇O0088o()Ljava/util/concurrent/ExecutorService;

    .line 34
    .line 35
    .line 36
    move-result-object p3

    .line 37
    new-instance v0, LO0〇O80ooo/o〇〇0〇;

    .line 38
    .line 39
    move-object v5, v0

    .line 40
    move-object v6, p0

    .line 41
    move-object v8, p1

    .line 42
    move v10, p2

    .line 43
    invoke-direct/range {v5 .. v10}, LO0〇O80ooo/o〇〇0〇;-><init>(Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/ArrayList;Lcom/intsig/camscanner/tsapp/SyncStatus;FI)V

    .line 44
    .line 45
    .line 46
    invoke-interface {p3, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    goto :goto_0

    .line 51
    :cond_0
    sget-object p1, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-virtual {p1}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇o〇()V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_1
    sget-object p1, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 62
    .line 63
    invoke-virtual {p1}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    invoke-virtual {p1}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇o〇()V

    .line 68
    .line 69
    .line 70
    const-string p1, "downloadImagesByDocs empty docIds"

    .line 71
    .line 72
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    :goto_0
    return-object v2
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private O〇OO(Lcom/intsig/camscanner/tsapp/SyncStatus;Lcom/intsig/tianshu/sync/SyncState;IZJFILjava/lang/String;I)I
    .locals 20

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    move/from16 v2, p3

    .line 6
    .line 7
    move/from16 v3, p4

    .line 8
    .line 9
    const-string v4, "syncDocFolder"

    .line 10
    .line 11
    const-string v5, "SyncThread"

    .line 12
    .line 13
    invoke-static {v5, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 17
    .line 18
    .line 19
    move-result-wide v6

    .line 20
    sget-object v4, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o800o8O:Ljava/util/List;

    .line 21
    .line 22
    if-eqz v4, :cond_9

    .line 23
    .line 24
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 25
    .line 26
    .line 27
    move-result v4

    .line 28
    const/4 v9, 0x1

    .line 29
    if-le v4, v9, :cond_9

    .line 30
    .line 31
    sget-object v4, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o800o8O:Ljava/util/List;

    .line 32
    .line 33
    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v4

    .line 37
    check-cast v4, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;

    .line 38
    .line 39
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇o〇()Ljava/util/Vector;

    .line 40
    .line 41
    .line 42
    move-result-object v10

    .line 43
    invoke-virtual {v4, v10}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇oo〇(Ljava/util/Vector;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v4, v3}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->o800o8O(Z)V

    .line 47
    .line 48
    .line 49
    move-wide/from16 v10, p5

    .line 50
    .line 51
    invoke-virtual {v4, v10, v11}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->OoO8(J)V

    .line 52
    .line 53
    .line 54
    new-instance v10, Ljava/lang/StringBuilder;

    .line 55
    .line 56
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    .line 58
    .line 59
    const-string v11, "sync doc operation  enableListFile="

    .line 60
    .line 61
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v10

    .line 71
    invoke-static {v5, v10}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    iget-boolean v10, v4, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇〇888:Z

    .line 75
    .line 76
    if-eqz v10, :cond_8

    .line 77
    .line 78
    const/4 v15, 0x3

    .line 79
    :try_start_0
    iget-object v10, v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 80
    .line 81
    invoke-virtual {v10}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇o〇()F

    .line 82
    .line 83
    .line 84
    move-result v14

    .line 85
    move-object/from16 v10, p2

    .line 86
    .line 87
    const/16 v17, 0x0

    .line 88
    .line 89
    :goto_0
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->〇8o()Z

    .line 90
    .line 91
    .line 92
    move-result v11

    .line 93
    const/4 v13, -0x1

    .line 94
    const-wide/16 v18, 0x0

    .line 95
    .line 96
    if-eqz v11, :cond_0

    .line 97
    .line 98
    move/from16 v11, v17

    .line 99
    .line 100
    const/4 v8, 0x1

    .line 101
    :goto_1
    const/4 v9, -0x1

    .line 102
    const/4 v12, 0x3

    .line 103
    goto/16 :goto_3

    .line 104
    .line 105
    :cond_0
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->o0O〇8o0O()Z

    .line 106
    .line 107
    .line 108
    move-result v11

    .line 109
    if-eqz v11, :cond_1

    .line 110
    .line 111
    if-eqz v3, :cond_1

    .line 112
    .line 113
    const/4 v8, 0x2

    .line 114
    move/from16 v11, v17

    .line 115
    .line 116
    goto :goto_1

    .line 117
    :cond_1
    iget-object v10, v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 118
    .line 119
    invoke-virtual {v10, v14}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇80〇808〇O(F)V

    .line 120
    .line 121
    .line 122
    iget-object v10, v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 123
    .line 124
    move/from16 v12, p7

    .line 125
    .line 126
    invoke-virtual {v1, v0, v12, v2, v10}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇O〇oO(Lcom/intsig/camscanner/tsapp/SyncStatus;FILcom/intsig/camscanner/tsapp/sync/SyncProgressValue;)Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;

    .line 127
    .line 128
    .line 129
    move-result-object v11

    .line 130
    iget v10, v4, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o00〇〇Oo:I

    .line 131
    .line 132
    iget v8, v4, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o〇:I

    .line 133
    .line 134
    move/from16 v16, v10

    .line 135
    .line 136
    move-object v10, v4

    .line 137
    move/from16 v12, v16

    .line 138
    .line 139
    const/4 v9, -0x1

    .line 140
    move v13, v8

    .line 141
    move v8, v14

    .line 142
    move/from16 v14, p8

    .line 143
    .line 144
    move-object/from16 v15, p9

    .line 145
    .line 146
    move/from16 v16, p10

    .line 147
    .line 148
    invoke-static/range {v10 .. v16}, Lcom/intsig/tianshu/sync/SyncApi;->Oo08(Lcom/intsig/tianshu/sync/SyncAdapter;Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;IIILjava/lang/String;I)Lcom/intsig/tianshu/sync/SyncState;

    .line 149
    .line 150
    .line 151
    move-result-object v10

    .line 152
    add-int/lit8 v11, v17, 0x1

    .line 153
    .line 154
    new-instance v12, Ljava/lang/StringBuilder;

    .line 155
    .line 156
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 157
    .line 158
    .line 159
    const-string v13, "sync doc operation repeat ="

    .line 160
    .line 161
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    .line 163
    .line 164
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 165
    .line 166
    .line 167
    const-string v13, " error="

    .line 168
    .line 169
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    .line 171
    .line 172
    invoke-virtual {v10}, Lcom/intsig/tianshu/sync/SyncState;->〇o00〇〇Oo()I

    .line 173
    .line 174
    .line 175
    move-result v13

    .line 176
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 177
    .line 178
    .line 179
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 180
    .line 181
    .line 182
    move-result-object v12

    .line 183
    invoke-static {v5, v12}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    .line 185
    .line 186
    invoke-virtual {v10}, Lcom/intsig/tianshu/sync/SyncState;->〇o00〇〇Oo()I

    .line 187
    .line 188
    .line 189
    move-result v12

    .line 190
    const/16 v13, 0x15f

    .line 191
    .line 192
    if-ne v13, v12, :cond_2

    .line 193
    .line 194
    iput v9, v4, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o00〇〇Oo:I

    .line 195
    .line 196
    :cond_2
    invoke-virtual {v10}, Lcom/intsig/tianshu/sync/SyncState;->o〇0()J

    .line 197
    .line 198
    .line 199
    move-result-wide v12
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 200
    cmp-long v14, v12, v18

    .line 201
    .line 202
    const/4 v12, 0x3

    .line 203
    if-nez v14, :cond_4

    .line 204
    .line 205
    if-lt v11, v12, :cond_3

    .line 206
    .line 207
    goto :goto_2

    .line 208
    :cond_3
    move v14, v8

    .line 209
    move/from16 v17, v11

    .line 210
    .line 211
    const/4 v9, 0x1

    .line 212
    const/4 v15, 0x3

    .line 213
    goto :goto_0

    .line 214
    :cond_4
    :goto_2
    const/4 v8, 0x0

    .line 215
    :goto_3
    if-nez v10, :cond_5

    .line 216
    .line 217
    if-nez v8, :cond_7

    .line 218
    .line 219
    const/4 v15, 0x3

    .line 220
    goto :goto_4

    .line 221
    :cond_5
    :try_start_1
    invoke-virtual {v10}, Lcom/intsig/tianshu/sync/SyncState;->Oo08()Z

    .line 222
    .line 223
    .line 224
    move-result v3

    .line 225
    if-eqz v3, :cond_6

    .line 226
    .line 227
    new-instance v3, Ljava/lang/StringBuilder;

    .line 228
    .line 229
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 230
    .line 231
    .line 232
    const-string v8, "sync doc operation hasError: "

    .line 233
    .line 234
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    .line 236
    .line 237
    invoke-virtual {v10}, Lcom/intsig/tianshu/sync/SyncState;->〇o00〇〇Oo()I

    .line 238
    .line 239
    .line 240
    move-result v8

    .line 241
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 242
    .line 243
    .line 244
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 245
    .line 246
    .line 247
    move-result-object v3

    .line 248
    invoke-static {v5, v3}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    .line 250
    .line 251
    invoke-virtual {v10}, Lcom/intsig/tianshu/sync/SyncState;->〇o00〇〇Oo()I

    .line 252
    .line 253
    .line 254
    move-result v3

    .line 255
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 256
    .line 257
    .line 258
    invoke-virtual {v1, v9, v0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 259
    .line 260
    .line 261
    invoke-virtual {v10}, Lcom/intsig/tianshu/sync/SyncState;->〇o00〇〇Oo()I

    .line 262
    .line 263
    .line 264
    move-result v15

    .line 265
    goto :goto_4

    .line 266
    :cond_6
    invoke-virtual {v10}, Lcom/intsig/tianshu/sync/SyncState;->o〇0()J

    .line 267
    .line 268
    .line 269
    move-result-wide v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 270
    cmp-long v0, v2, v18

    .line 271
    .line 272
    if-nez v0, :cond_7

    .line 273
    .line 274
    if-lt v11, v12, :cond_7

    .line 275
    .line 276
    const/4 v15, 0x4

    .line 277
    goto :goto_4

    .line 278
    :cond_7
    move v15, v8

    .line 279
    :goto_4
    move v8, v15

    .line 280
    goto :goto_6

    .line 281
    :catch_0
    move-exception v0

    .line 282
    goto :goto_5

    .line 283
    :catch_1
    move-exception v0

    .line 284
    const/4 v12, 0x3

    .line 285
    :goto_5
    invoke-static {v5, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 286
    .line 287
    .line 288
    const/4 v8, 0x3

    .line 289
    goto :goto_6

    .line 290
    :cond_8
    const/4 v8, 0x0

    .line 291
    :goto_6
    invoke-virtual {v4}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇〇8O0〇8()V

    .line 292
    .line 293
    .line 294
    goto :goto_7

    .line 295
    :cond_9
    const/4 v8, 0x0

    .line 296
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    .line 297
    .line 298
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 299
    .line 300
    .line 301
    const-string v2, "syncDocFolder cost time="

    .line 302
    .line 303
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    .line 305
    .line 306
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 307
    .line 308
    .line 309
    move-result-wide v2

    .line 310
    sub-long/2addr v2, v6

    .line 311
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 312
    .line 313
    .line 314
    const-string v2, ", errorCode:"

    .line 315
    .line 316
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    .line 318
    .line 319
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 320
    .line 321
    .line 322
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 323
    .line 324
    .line 325
    move-result-object v0

    .line 326
    invoke-static {v5, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    .line 328
    .line 329
    return v8
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
.end method

.method private O〇Oooo〇〇(IIIJLjava/lang/String;)Lcom/intsig/tianshu/sync/SyncState;
    .locals 50

    move-object/from16 v15, p0

    move/from16 v14, p1

    move/from16 v13, p3

    move-wide/from16 v11, p4

    move-object/from16 v10, p6

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sync folder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " revision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v9, p2

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " syncType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " uploadTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, " updateTeamToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v8, "SyncThread"

    invoke-static {v8, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O000()Z

    move-result v0

    const/4 v7, 0x0

    if-eqz v0, :cond_0

    return-object v7

    .line 3
    :cond_0
    sget-object v0, Lcom/intsig/tianshu/SyncExecuteState;->〇o00〇〇Oo:Lcom/intsig/tianshu/SyncExecuteState$Companion;

    invoke-virtual {v0}, Lcom/intsig/tianshu/SyncExecuteState$Companion;->〇080()Lcom/intsig/tianshu/SyncExecuteState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/tianshu/SyncExecuteState;->〇o00〇〇Oo()V

    .line 4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    :goto_0
    if-eqz v0, :cond_35

    const/4 v0, 0x3

    if-ge v4, v0, :cond_35

    .line 5
    sget-object v16, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    invoke-virtual/range {v16 .. v16}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/tianshu/sync/SyncTimeCount;->o〇0OOo〇0()V

    .line 6
    iget-object v1, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-virtual {v1}, Lcom/intsig/camscanner/tsapp/SyncStatus;->Oo08()V

    .line 7
    iget-object v1, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    invoke-virtual {v1}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->oO80()V

    .line 8
    iget-object v1, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇808〇:Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

    invoke-virtual {v1}, Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;->O〇8O8〇008()V

    .line 9
    iget-object v1, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇808〇:Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

    sget-object v3, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;->O8ooOoo〇(Landroid/content/Context;)V

    const/4 v3, 0x0

    .line 10
    invoke-direct {v15, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇o(F)V

    .line 11
    iget-object v1, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-direct {v15, v6, v1, v13}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇8O0〇8(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    const/high16 v19, 0x447a0000    # 1000.0f

    const-string v1, "%.2fs"

    if-nez v2, :cond_1

    .line 12
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 13
    iget-object v2, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    move-object v0, v1

    move-object/from16 v1, p0

    const/4 v9, 0x0

    move/from16 v3, p3

    move/from16 v23, v4

    move/from16 v4, p1

    const/4 v9, 0x0

    move/from16 v5, p2

    const/4 v7, 0x1

    move-object/from16 v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o8oO〇(Lcom/intsig/camscanner/tsapp/SyncStatus;IIILjava/lang/String;)Z

    move-result v1

    .line 14
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v2, v2, v20

    .line 15
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initSyncToken consume "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-array v5, v7, [Ljava/lang/Object;

    long-to-float v2, v2

    div-float v2, v2, v19

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v5, v9

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    move/from16 v20, v1

    goto :goto_1

    :cond_1
    move-object v0, v1

    move/from16 v23, v4

    const/4 v7, 0x1

    const/4 v9, 0x0

    move/from16 v20, v2

    :goto_1
    new-array v6, v7, [Z

    aput-boolean v20, v6, v9

    new-array v5, v7, [Z

    aput-boolean v9, v5, v9

    aget-boolean v1, v6, v9

    if-eqz v1, :cond_30

    .line 16
    sget-object v2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    const/4 v4, -0x1

    if-eqz v2, :cond_2b

    .line 17
    iget-object v1, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-direct {v15, v1, v13, v10}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O0OO8〇0(Lcom/intsig/camscanner/tsapp/SyncStatus;ILjava/lang/String;)Ljava/util/concurrent/Future;

    move-result-object v21

    .line 18
    sget-object v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-static {v1}, Lcom/intsig/camscanner/app/AppUtil;->oO(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "Sync is Closed\uff0c please open on Sync settin\uff01"

    .line 19
    invoke-static {v8, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v21, :cond_2

    .line 20
    :try_start_0
    invoke-interface/range {v21 .. v21}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v1, v0

    .line 21
    invoke-static {v8, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v1, v0

    .line 22
    invoke-static {v8, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 23
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_2
    :goto_2
    const/16 v25, 0x0

    return-object v25

    :cond_3
    const/16 v25, 0x0

    const/high16 v1, 0x42c80000    # 100.0f

    .line 24
    iget v2, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇8O0〇8:F

    sub-float/2addr v1, v2

    iget-object v2, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇808〇:Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

    invoke-virtual {v2}, Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;->〇O888o0o()F

    move-result v2

    sub-float/2addr v1, v2

    .line 25
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " personalLeftProgress="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v3, "otherProgress="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇o()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    iget-object v2, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->OO0o〇〇〇〇0(F)V

    .line 27
    iget-object v1, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇80〇808〇O(F)V

    .line 28
    iget-object v1, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-virtual {v15, v4, v1, v13}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 29
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo〇O8o〇8()V

    .line 30
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 31
    iget-object v3, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-direct {v15, v3, v13}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇o8(Lcom/intsig/camscanner/tsapp/SyncStatus;I)Z

    move-result v3

    aput-boolean v3, v6, v9

    .line 32
    iget-object v3, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    const v9, 0x3c23d70a    # 0.01f

    invoke-virtual {v3, v9}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇o00〇〇Oo(F)V

    .line 33
    iget-object v3, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-virtual {v15, v4, v3, v13}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 34
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v27

    sub-long v1, v27, v1

    .line 35
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "queryServerSyncInfo consume "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-array v9, v7, [Ljava/lang/Object;

    long-to-float v1, v1

    div-float v1, v1, v19

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/16 v19, 0x0

    aput-object v1, v9, v19

    invoke-static {v0, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    sget-object v0, Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient;->〇O8o08O:Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient$Companion;

    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient$Companion;->O8()Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient;->o〇8oOO88()V

    .line 37
    invoke-static {}, Lcom/intsig/camscanner/newsign/sync/ESignJsonSync;->〇〇808〇()Lcom/intsig/camscanner/newsign/sync/ESignJsonSync;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/sync/ESignJsonSync;->OoO8()V

    .line 38
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 39
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->oO()Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;

    move-result-object v9

    const/4 v2, 0x2

    if-ne v13, v2, :cond_4

    const/4 v2, 0x3

    if-ne v14, v2, :cond_4

    .line 40
    sget-object v2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-virtual {v9, v2}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇8(Landroid/content/Context;)J

    move-result-wide v2

    .line 41
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "lastServerUploadTime="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v8, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    cmp-long v4, v11, v2

    if-lez v4, :cond_5

    goto :goto_3

    .line 42
    :cond_4
    sget-object v2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-virtual {v9, v2}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇〇888(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    :goto_3
    const/4 v2, 0x1

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    const-string v3, "no new dir download from server"

    if-eqz v2, :cond_8

    .line 43
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v27

    const/4 v2, 0x1

    .line 44
    invoke-virtual {v9, v2}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->o〇〇0〇(Z)Lcom/intsig/camscanner/tsapp/sync/RootDirJson;

    move-result-object v4

    .line 45
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v29

    move-object/from16 v31, v5

    move-object v7, v6

    sub-long v5, v29, v27

    invoke-virtual {v2, v5, v6}, Lcom/intsig/tianshu/sync/SyncTimeCount;->OO0o〇〇〇〇0(J)V

    if-nez v4, :cond_6

    const-string v2, "no dir download from server"

    .line 46
    invoke-static {v8, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v27, v7

    goto :goto_5

    .line 47
    :cond_6
    sget-object v2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-virtual {v9, v2}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇8(Landroid/content/Context;)J

    move-result-wide v5

    .line 48
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v27, v7

    const-string v7, "serverUploadTime="

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v10, v4, Lcom/intsig/camscanner/tsapp/sync/RootDirJson;->upload_time:J

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v7, " lastServerUploadTime="

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    iget-wide v10, v4, Lcom/intsig/camscanner/tsapp/sync/RootDirJson;->upload_time:J

    cmp-long v2, v10, v5

    if-lez v2, :cond_7

    const-string v2, "find new dir download from server"

    .line 50
    invoke-static {v8, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 52
    sget-object v7, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-virtual {v9, v7, v4, v5, v6}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇O(Landroid/content/Context;Lcom/intsig/camscanner/tsapp/sync/RootDirJson;J)V

    .line 53
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v2

    invoke-virtual {v4, v5, v6}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇0〇O0088o(J)V

    goto :goto_5

    .line 54
    :cond_7
    invoke-static {v8, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_8
    move-object/from16 v31, v5

    move-object/from16 v27, v6

    .line 55
    invoke-static {v8, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    :goto_5
    const/4 v6, 0x1

    .line 56
    invoke-virtual {v9, v6}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->ooo〇8oO(Z)V

    .line 57
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "syncDirFolder cost time="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    iget-object v0, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    const v1, 0x3d23d70a    # 0.04f

    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇o00〇〇Oo(F)V

    .line 59
    iget-object v0, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    const/4 v4, -0x1

    invoke-virtual {v15, v4, v0, v13}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 60
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    .line 61
    iget-object v2, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    const/4 v5, 0x0

    const-wide/16 v10, -0x1

    iget-object v0, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    const v12, 0x3d4ccccd    # 0.05f

    .line 62
    invoke-virtual {v0, v12}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->Oo08(F)F

    move-result v0

    const/16 v22, 0x7

    const/16 v26, 0x0

    const/16 v30, 0x1

    const/4 v3, 0x0

    move-object/from16 v1, p0

    const/16 v24, 0x0

    const/4 v7, -0x1

    move/from16 v4, p3

    move-object/from16 v12, v25

    move-object/from16 v25, v27

    move-wide v6, v10

    move-object v11, v8

    move v8, v0

    move-object v10, v9

    const/4 v12, 0x0

    const/16 v19, 0x0

    move/from16 v9, v22

    move-object v12, v10

    move-object/from16 v10, v26

    move-object v14, v11

    move/from16 v11, v30

    .line 63
    invoke-direct/range {v1 .. v11}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇OO(Lcom/intsig/camscanner/tsapp/SyncStatus;Lcom/intsig/tianshu/sync/SyncState;IZJFILjava/lang/String;I)I

    .line 64
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long v0, v0, v28

    .line 65
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇O8o08O(J)V

    .line 66
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-virtual {v12, v0}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->ooOO(Landroid/content/Context;)V

    .line 67
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-virtual {v12, v0}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->o8(Landroid/content/Context;)V

    .line 68
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    .line 69
    iget-object v2, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    const-wide/16 v6, -0x1

    iget-object v0, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    const v11, 0x3dcccccd    # 0.1f

    .line 70
    invoke-virtual {v0, v11}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->Oo08(F)F

    move-result v8

    const/4 v9, 0x7

    const/4 v10, 0x0

    const/4 v0, 0x1

    move-object/from16 v1, p0

    move-object/from16 v28, v12

    const v12, 0x3dcccccd    # 0.1f

    move v11, v0

    .line 71
    invoke-direct/range {v1 .. v11}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇08(Lcom/intsig/camscanner/tsapp/SyncStatus;Lcom/intsig/tianshu/sync/SyncState;IZJFILjava/lang/String;I)I

    .line 72
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long v0, v0, v26

    .line 73
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇O〇(J)V

    .line 74
    iget-object v0, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    iget-object v1, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    const v2, 0x3e99999a    # 0.3f

    .line 75
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->Oo08(F)F

    move-result v1

    .line 76
    invoke-direct {v15, v0, v13, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇8O8〇008(Lcom/intsig/camscanner/tsapp/SyncStatus;IF)Ljava/util/concurrent/Future;

    move-result-object v26

    const/4 v1, 0x0

    .line 77
    invoke-virtual {v15, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O8(Z)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "exit sync at downloadImagesByDocs"

    .line 78
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    return-object v1

    .line 79
    :cond_9
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇08O8o〇0()V

    const/4 v11, 0x1

    new-array v10, v11, [I

    aput v1, v10, v1

    .line 80
    sget-object v0, Lcom/intsig/tianshu/SyncExecuteState;->〇o00〇〇Oo:Lcom/intsig/tianshu/SyncExecuteState$Companion;

    invoke-virtual {v0}, Lcom/intsig/tianshu/SyncExecuteState$Companion;->〇080()Lcom/intsig/tianshu/SyncExecuteState;

    move-result-object v0

    sget v1, Lcom/intsig/tianshu/SyncExecuteState;->Oo08:I

    invoke-virtual {v0, v1}, Lcom/intsig/tianshu/SyncExecuteState;->O8(I)V

    const/16 v27, 0x0

    .line 81
    :goto_6
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->o0O〇8o0O()Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "setInterruptUploadDoc false"

    .line 82
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    const/4 v1, 0x0

    .line 83
    invoke-static {v1}, Lcom/intsig/tianshu/TianShuAPI;->〇oO8O0〇〇O(Z)V

    const/4 v2, 0x0

    .line 84
    invoke-virtual {v15, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇08O8o〇0(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v15, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OOo8o〇O(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    aput v1, v10, v1

    .line 85
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o()[Z

    move-result-object v2

    .line 86
    aget-boolean v3, v2, v1

    aput-boolean v3, v31, v1

    .line 87
    aget-boolean v29, v2, v11

    .line 88
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v34

    .line 89
    iget-object v1, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    invoke-virtual {v1}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->O8()F

    move-result v1

    iget-object v2, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 90
    invoke-virtual {v2, v12}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->Oo08(F)F

    move-result v2

    sub-float/2addr v1, v2

    cmpg-float v2, v1, v19

    if-gez v2, :cond_b

    const/4 v8, 0x0

    goto :goto_7

    :cond_b
    move v8, v1

    :goto_7
    const/4 v9, 0x4

    const/16 v7, 0x139

    if-eqz v29, :cond_d

    .line 91
    sget-object v1, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->Oo08:Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;

    invoke-virtual {v1}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;->Oo08()Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->Oo08()Z

    move-result v1

    if-nez v1, :cond_d

    const/4 v1, 0x0

    aput-boolean v1, v25, v1

    aput-boolean v11, v31, v1

    .line 92
    iget-object v1, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-virtual {v1, v7}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 93
    iget-object v1, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    const/4 v6, -0x1

    invoke-direct {v15, v6, v1, v13}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇8O0〇8(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 94
    invoke-static {v0}, Lcom/intsig/utils/ListUtils;->〇o〇(Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 95
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;

    .line 96
    invoke-virtual {v2}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v2

    invoke-direct {v15, v2, v3, v9}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇000O0(JI)V

    goto :goto_8

    .line 97
    :cond_c
    invoke-static {v0}, Lcom/intsig/utils/ListUtils;->〇080(Ljava/util/List;)I

    move-result v0

    const/4 v1, 0x0

    aput v0, v10, v1

    goto/16 :goto_c

    :cond_d
    const/4 v1, 0x0

    const/4 v6, -0x1

    if-eqz v0, :cond_1a

    .line 98
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1a

    new-array v4, v11, [Z

    aput-boolean v1, v4, v1

    .line 99
    sget-object v1, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->Oo08:Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;

    invoke-virtual {v1}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;->Oo08()Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;

    move-result-object v5

    .line 100
    invoke-virtual {v5, v0}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->Oooo8o0〇(Ljava/util/List;)V

    .line 101
    invoke-virtual {v5}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->〇〇888()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v8, v0

    const v0, 0x3f4ccccd    # 0.8f

    mul-float v0, v0, v8

    mul-float v30, v8, v12

    const v16, 0x3d4ccccd    # 0.05f

    mul-float v32, v8, v16

    .line 102
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 103
    :goto_9
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->o0O〇8o0O()Z

    move-result v1

    if-eqz v1, :cond_e

    const-string v0, "upload doc isNeedInterruptUploadDoc"

    .line 104
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    .line 105
    :cond_e
    invoke-virtual {v5}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->oO80()Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;

    move-result-object v3

    .line 106
    invoke-virtual {v5, v3}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->〇8o8o〇(Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;)Z

    move-result v1

    if-eqz v1, :cond_f

    goto :goto_a

    :cond_f
    if-eqz v29, :cond_10

    .line 107
    invoke-virtual {v3}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->Oo08()Z

    move-result v1

    if-nez v1, :cond_10

    .line 108
    invoke-virtual {v3}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v1

    invoke-direct {v15, v1, v2, v9}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇000O0(JI)V

    const/4 v1, 0x0

    aget v2, v10, v1

    add-int/2addr v2, v11

    aput v2, v10, v1

    goto :goto_9

    .line 109
    :cond_10
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->〇8o()Z

    move-result v1

    if-eqz v1, :cond_14

    const-string v0, "upload doc needTerminate"

    .line 110
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :goto_a
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    .line 112
    :try_start_1
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_b

    :catch_2
    move-exception v0

    move-object v2, v0

    .line 113
    invoke-static {v14, v2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 114
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_b

    :catch_3
    move-exception v0

    move-object v2, v0

    .line 115
    invoke-static {v14, v2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_b

    :cond_11
    const/4 v2, 0x0

    aget-boolean v0, v25, v2

    if-eqz v0, :cond_13

    .line 116
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇000〇〇08()V

    aget-boolean v0, v31, v2

    if-eqz v0, :cond_12

    .line 117
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/ShareDirOwnerCloudSpaceOverLimitEvent;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/ShareDirOwnerCloudSpaceOverLimitEvent;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 118
    iget-object v0, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-virtual {v0, v7}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 119
    iget-object v0, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-direct {v15, v6, v0, v13}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇8O0〇8(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    goto :goto_c

    :cond_12
    aget v0, v10, v2

    if-lez v0, :cond_13

    const/16 v27, 0x1

    :cond_13
    :goto_c
    move-object/from16 v38, v10

    move-object v2, v14

    move-object v14, v15

    move-object/from16 v47, v28

    const v22, 0x3d4ccccd    # 0.05f

    const v24, 0x3dcccccd    # 0.1f

    const/16 v33, 0x139

    goto/16 :goto_10

    :cond_14
    const/4 v2, 0x0

    .line 120
    invoke-virtual {v15, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O8(Z)Z

    move-result v1

    if-eqz v1, :cond_15

    const-string v0, "exit sync at uploading files"

    .line 121
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    return-object v1

    .line 122
    :cond_15
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->〇o0O0O8()I

    move-result v1

    invoke-static {v1}, Lcom/intsig/tianshu/TianShuAPI;->〇00(I)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "stop sync, login error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->〇o0O0O8()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v24, 0x0

    return-object v24

    :cond_16
    const/16 v24, 0x0

    .line 124
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v36

    .line 125
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "docBean:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, " checkSpace:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o00〇〇Oo()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v14, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-virtual {v3}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v1

    invoke-direct {v15, v1, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O880oOO08(J)V

    const/16 v22, 0x0

    aget-boolean v1, v31, v22

    if-eqz v1, :cond_18

    .line 127
    invoke-virtual {v3}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->Oo08()Z

    move-result v1

    if-eqz v1, :cond_17

    goto :goto_e

    :cond_17
    move-object/from16 v41, v3

    move-object/from16 v42, v4

    move-object/from16 v43, v5

    move-object/from16 v44, v8

    move-object/from16 v38, v10

    move-object/from16 v3, v24

    move-object/from16 v47, v28

    const v22, 0x3d4ccccd    # 0.05f

    const v24, 0x3dcccccd    # 0.1f

    const/16 v33, 0x139

    :goto_d
    const/16 v46, 0x4

    goto/16 :goto_f

    .line 128
    :cond_18
    :goto_e
    sget-object v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-virtual {v3}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v6

    invoke-static {v1, v6, v7}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oo0O〇0〇〇〇(Landroid/content/Context;J)Z

    move-result v1

    .line 129
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sync isOfficeDocType: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v14, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_19

    .line 130
    iget-object v2, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-virtual {v3}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v6

    iget-object v1, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 131
    invoke-virtual {v3}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o00〇〇Oo()I

    move-result v39

    move-object/from16 v40, v1

    move-object/from16 v1, p0

    move-object/from16 v41, v3

    move/from16 v3, p3

    move-object/from16 v42, v4

    move-object/from16 v43, v5

    move-wide v4, v6

    const/4 v7, -0x1

    move/from16 v6, v32

    const/16 v33, 0x139

    move-object/from16 v7, v40

    move-object/from16 v44, v8

    move/from16 v8, v39

    .line 132
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O8O〇88oO0(Lcom/intsig/camscanner/tsapp/SyncStatus;IJFLcom/intsig/camscanner/tsapp/sync/SyncProgressValue;I)Ljava/util/concurrent/Future;

    move-result-object v1

    move-object v3, v1

    move-object/from16 v38, v10

    move-object/from16 v47, v28

    const v22, 0x3d4ccccd    # 0.05f

    const v24, 0x3dcccccd    # 0.1f

    goto :goto_d

    :cond_19
    move-object/from16 v41, v3

    move-object/from16 v42, v4

    move-object/from16 v43, v5

    move-object/from16 v44, v8

    const/16 v33, 0x139

    .line 133
    iget-object v2, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-virtual/range {v41 .. v41}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o〇()J

    move-result-wide v4

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    iget-object v6, v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 134
    invoke-virtual/range {v41 .. v41}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;->〇o00〇〇Oo()I

    move-result v40

    move-object/from16 v1, p0

    move/from16 v3, p3

    move-object/from16 v45, v6

    move v6, v0

    const/16 v46, 0x4

    move/from16 v9, v38

    move-object/from16 v38, v10

    move-object/from16 v10, v39

    move-object/from16 v11, v45

    move-object/from16 v47, v28

    const v22, 0x3d4ccccd    # 0.05f

    const v24, 0x3dcccccd    # 0.1f

    move/from16 v12, v40

    .line 135
    invoke-virtual/range {v1 .. v12}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇080(Lcom/intsig/camscanner/tsapp/SyncStatus;IJFLjava/lang/String;Ljava/lang/String;Z[ZLcom/intsig/camscanner/tsapp/sync/SyncProgressValue;I)Ljava/util/concurrent/Future;

    move-result-object v1

    move-object v3, v1

    .line 136
    :goto_f
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->〇〇808〇()Ljava/util/concurrent/ExecutorService;

    move-result-object v12

    new-instance v11, LO0〇O80ooo/〇oOO8O8;

    const/16 v16, 0x0

    move-object v1, v11

    move-object/from16 v2, p0

    move-object/from16 v4, v42

    move-object/from16 v5, v43

    move-object/from16 v6, v41

    move-object/from16 v7, v31

    move-object/from16 v8, v38

    move-wide/from16 v9, v36

    move/from16 v28, v0

    move-object v0, v11

    move-object/from16 v11, v16

    move-object/from16 v48, v12

    move/from16 v12, p3

    move/from16 v13, v30

    move-object/from16 v49, v14

    move/from16 v14, v32

    move/from16 v15, v32

    move-object/from16 v16, v25

    invoke-direct/range {v1 .. v16}, LO0〇O80ooo/〇oOO8O8;-><init>(Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/concurrent/Future;[ZLcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;[Z[IJLcom/intsig/tianshu/sync/SyncState;IFFF[Z)V

    move-object/from16 v1, v48

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    move-object/from16 v1, v44

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v15, p0

    move/from16 v13, p3

    move-object v8, v1

    move/from16 v0, v28

    move-object/from16 v10, v38

    move-object/from16 v28, v47

    move-object/from16 v14, v49

    const/4 v6, -0x1

    const/16 v7, 0x139

    const/4 v9, 0x4

    const/4 v11, 0x1

    const v12, 0x3dcccccd    # 0.1f

    const v16, 0x3d4ccccd    # 0.05f

    goto/16 :goto_9

    :cond_1a
    move-object/from16 v38, v10

    move-object/from16 v49, v14

    move-object/from16 v47, v28

    const v22, 0x3d4ccccd    # 0.05f

    const v24, 0x3dcccccd    # 0.1f

    const/16 v33, 0x139

    .line 137
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    move-object/from16 v14, p0

    .line 138
    iget-object v2, v14, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    const/4 v5, 0x1

    const-wide/16 v6, -0x1

    const/4 v9, 0x7

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v3, 0x0

    move-object/from16 v1, p0

    move/from16 v4, p3

    invoke-direct/range {v1 .. v11}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇08(Lcom/intsig/camscanner/tsapp/SyncStatus;Lcom/intsig/tianshu/sync/SyncState;IZJFILjava/lang/String;I)I

    move-result v0

    .line 139
    sget-object v1, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    invoke-virtual {v1}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v12

    invoke-virtual {v1, v2, v3}, Lcom/intsig/tianshu/sync/SyncTimeCount;->O8〇o(J)V

    .line 140
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "no docIds to upload jpageErrorCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object/from16 v2, v49

    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :goto_10
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "upload costTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long v3, v3, v34

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, " isNeedInterruptUploadDoc()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->o0O〇8o0O()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 143
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->o0O〇8o0O()Z

    move-result v0

    if-nez v0, :cond_2a

    .line 145
    sget-object v0, Lcom/intsig/tianshu/SyncExecuteState;->〇o00〇〇Oo:Lcom/intsig/tianshu/SyncExecuteState$Companion;

    invoke-virtual {v0}, Lcom/intsig/tianshu/SyncExecuteState$Companion;->〇080()Lcom/intsig/tianshu/SyncExecuteState;

    move-result-object v0

    sget v1, Lcom/intsig/tianshu/SyncExecuteState;->o〇0:I

    invoke-virtual {v0, v1}, Lcom/intsig/tianshu/SyncExecuteState;->O8(I)V

    const/4 v3, 0x0

    aget v0, v38, v3

    if-lez v0, :cond_1b

    const/16 v5, 0x139

    goto :goto_11

    :cond_1b
    const/4 v5, 0x0

    .line 146
    :goto_11
    invoke-direct {v14, v5}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0000OOO(I)V

    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "finishUploadAllDoc uploadErrorCode == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->Oo08:Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;

    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;->Oo08()Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->OO0o〇〇()V

    .line 149
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o()V

    .line 150
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    move-object/from16 v4, v47

    invoke-virtual {v4, v0}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇oo〇(Landroid/content/Context;)V

    .line 151
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-virtual {v4, v0}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇8(Landroid/content/Context;)J

    move-result-wide v0

    .line 152
    sget-object v5, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-virtual {v4, v5, v0, v1}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇O〇80o08O(Landroid/content/Context;J)Z

    move-result v5

    if-eqz v5, :cond_22

    .line 153
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 154
    sget-object v5, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇0〇O0088o(Landroid/content/Context;Ljava/lang/String;Z)Lcom/intsig/camscanner/tsapp/sync/UploadDirJson;

    move-result-object v5

    if-eqz v5, :cond_1c

    .line 155
    iget-object v8, v5, Lcom/intsig/camscanner/tsapp/sync/UploadDirJson;->dirs:[Lcom/intsig/camscanner/tsapp/sync/DirJson;

    invoke-static {v8}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->oO80([Lcom/intsig/camscanner/tsapp/sync/DirJson;)V

    .line 156
    :cond_1c
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 157
    invoke-virtual {v5}, Lcom/intsig/tianshu/base/BaseJsonObj;->toJSONObject()Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_2 .. :try_end_2} :catch_5

    .line 158
    :try_start_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "upload dir toJSONObject time="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    sub-long/2addr v11, v8

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v8, " content="

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    sget-object v8, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    invoke-virtual {v8}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v0

    invoke-virtual {v9, v10, v11}, Lcom/intsig/tianshu/sync/SyncTimeCount;->OoO8(J)V

    .line 160
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 161
    invoke-direct {v14, v5}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oO〇(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1d

    const/16 v27, 0x1

    .line 162
    :cond_1d
    invoke-virtual {v8}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v0

    invoke-virtual {v8, v9, v10}, Lcom/intsig/tianshu/sync/SyncTimeCount;->O8ooOoo〇(J)V

    .line 163
    invoke-direct {v14, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇〇0〇(I)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_14

    :catch_4
    move-exception v0

    goto :goto_12

    :catch_5
    move-exception v0

    move-object v5, v6

    .line 164
    :goto_12
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 165
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    move-result v1

    const/16 v8, 0x12e

    if-ne v1, v8, :cond_1f

    .line 166
    :try_start_4
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-virtual {v4, v0}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->Oooo8o0〇(Landroid/content/Context;)V
    :try_end_4
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_4 .. :try_end_4} :catch_6

    goto :goto_13

    :catch_6
    move-exception v0

    .line 167
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 168
    :goto_13
    :try_start_5
    invoke-direct {v14, v5}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oO〇(Ljava/lang/String;)Z

    move-result v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7

    if-eqz v0, :cond_1e

    const/16 v27, 0x1

    :cond_1e
    :goto_14
    move/from16 v5, p3

    const/4 v0, 0x0

    const/4 v8, -0x1

    goto :goto_16

    :catch_7
    move-exception v0

    move-object v1, v0

    const/16 v0, -0x6f

    .line 169
    invoke-direct {v14, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇〇0〇(I)V

    .line 170
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    aput-boolean v3, v25, v3

    move/from16 v5, p3

    const/4 v8, -0x1

    goto :goto_15

    .line 171
    :cond_1f
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    move-result v1

    const/16 v4, 0x15f

    if-ne v1, v4, :cond_20

    .line 172
    invoke-direct {v14, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇〇0〇(I)V

    const-string v0, "uploadtime is not latest"

    .line 173
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    move/from16 v5, p3

    const/4 v0, 0x0

    const/4 v8, -0x1

    const/16 v27, 0x1

    goto :goto_16

    .line 174
    :cond_20
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    move-result v1

    const/16 v4, 0x13b

    if-ne v1, v4, :cond_21

    aput-boolean v3, v25, v3

    .line 175
    iget-object v0, v14, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 176
    iget-object v0, v14, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    move/from16 v5, p3

    const/4 v8, -0x1

    invoke-virtual {v14, v8, v0, v5}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 177
    invoke-direct {v14, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇〇0〇(I)V

    const/4 v0, 0x1

    goto :goto_16

    :cond_21
    move/from16 v5, p3

    const/4 v8, -0x1

    .line 178
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    move-result v0

    invoke-direct {v14, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇〇0〇(I)V

    aput-boolean v3, v25, v3

    goto :goto_15

    :catch_8
    move-exception v0

    move/from16 v5, p3

    const/4 v8, -0x1

    .line 179
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_15
    const/4 v0, 0x0

    :goto_16
    move v1, v0

    goto :goto_17

    :cond_22
    move/from16 v5, p3

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, -0x1

    .line 180
    invoke-direct {v14, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇〇0〇(I)V

    .line 181
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "no local dir info need to update lastServerUploadTime="

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 182
    :goto_17
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OOoo(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "success = false imageInfo="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-static {v4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->ooo〇8oO(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    :cond_23
    if-nez v29, :cond_24

    .line 184
    iget-object v0, v14, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->O8()F

    move-result v0

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v0, v4

    .line 185
    iget-object v4, v14, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    iget-object v9, v14, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    invoke-virtual {v14, v4, v0, v5, v9}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇O〇oO(Lcom/intsig/camscanner/tsapp/SyncStatus;FILcom/intsig/camscanner/tsapp/sync/SyncProgressValue;)Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;

    move-result-object v4

    .line 186
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SyncThread-sync trimmedPaperProcess="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/PaperSyncUtil;->〇080:Lcom/intsig/camscanner/tsapp/sync/PaperSyncUtil;

    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/tsapp/sync/PaperSyncUtil;->O8(Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;)V

    :cond_24
    if-nez v29, :cond_25

    .line 188
    iget-object v0, v14, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-direct {v14, v0, v5}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o80ooO(Lcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 189
    :cond_25
    sget-object v0, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    invoke-virtual {v0}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇o00〇〇Oo()V

    if-eqz v26, :cond_26

    .line 190
    :try_start_6
    invoke-interface/range {v26 .. v26}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_a
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_9

    goto :goto_18

    :catch_9
    move-exception v0

    move-object v4, v0

    .line 191
    invoke-static {v2, v4}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_18

    :catch_a
    move-exception v0

    move-object v4, v0

    .line 192
    invoke-static {v2, v4}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 193
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_26
    :goto_18
    if-eqz v21, :cond_27

    .line 194
    :try_start_7
    invoke-interface/range {v21 .. v21}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_c
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_b

    goto :goto_19

    :catch_b
    move-exception v0

    move-object v4, v0

    .line 195
    invoke-static {v2, v4}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_19

    :catch_c
    move-exception v0

    move-object v4, v0

    .line 196
    invoke-static {v2, v4}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 197
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_27
    :goto_19
    if-ne v5, v7, :cond_29

    aget-boolean v0, v25, v3

    if-eqz v0, :cond_29

    .line 198
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OOoo(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_28

    const/4 v0, 0x1

    goto :goto_1a

    :cond_28
    const-string v0, "success = false"

    .line 199
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    :cond_29
    const/4 v0, 0x0

    goto :goto_1a

    :cond_2a
    move/from16 v13, p3

    move-object v15, v14

    move-object/from16 v10, v38

    move-object/from16 v28, v47

    const/4 v11, 0x1

    const v12, 0x3dcccccd    # 0.1f

    move-object v14, v2

    goto/16 :goto_6

    :cond_2b
    move-object/from16 v31, v5

    move-object/from16 v25, v6

    move-object v2, v8

    move v5, v13

    move-object v14, v15

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v8, -0x1

    if-eqz v1, :cond_2c

    const-string v0, "sync, accountListener = null"

    .line 200
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2c
    const-string v0, "queryServerSyncInfo failed"

    .line 201
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/16 v27, 0x0

    :goto_1a
    aget-boolean v4, v25, v3

    if-nez v4, :cond_2d

    if-nez v1, :cond_2d

    aget-boolean v1, v31, v3

    if-nez v1, :cond_2d

    const-string v1, "ErrorType.SYNC_ERROR"

    .line 202
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget-object v1, v14, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    const/16 v4, -0x12f

    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 204
    iget-object v1, v14, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    invoke-virtual {v14, v8, v1, v5}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    :cond_2d
    aget-boolean v1, v31, v3

    if-eqz v1, :cond_2e

    .line 205
    sget-boolean v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇00:Z

    if-nez v1, :cond_2e

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide v10, 0x18476de7000L

    cmp-long v1, v8, v10

    if-gez v1, :cond_2e

    const-string v1, "CSCloud"

    const-string v4, "overrun"

    .line 206
    invoke-static {v1, v4}, Lcom/intsig/camscanner/log/LogAgentData;->o800o8O(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    sput-boolean v7, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇00:Z

    .line 208
    :cond_2e
    iget-object v1, v14, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇:Lcom/intsig/camscanner/tsapp/SyncStatus;

    aget-boolean v4, v25, v3

    invoke-direct {v14, v1, v4, v5}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇00(Lcom/intsig/camscanner/tsapp/SyncStatus;ZI)V

    .line 209
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "finish sync CurrentSyncProgress="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇o()F

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Ooo()Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 211
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "finish sync isNeedExitAfterSync success="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-boolean v4, v25, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-virtual {v14, v7}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O8(Z)Z

    :cond_2f
    move v1, v0

    goto :goto_1b

    :cond_30
    move-object v2, v8

    move v5, v13

    move-object v14, v15

    const/4 v3, 0x0

    const/4 v6, 0x0

    const-string v0, "initSyncToken failed"

    .line 213
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/16 v27, 0x1

    .line 214
    :goto_1b
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->〇8o()Z

    move-result v0

    if-eqz v0, :cond_31

    const/4 v0, 0x0

    goto :goto_1c

    .line 215
    :cond_31
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->Oo08:Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;

    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;->Oo08()Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->Oo08()Z

    move-result v0

    if-eqz v0, :cond_32

    const/4 v0, 0x1

    goto :goto_1c

    :cond_32
    move/from16 v0, v27

    :goto_1c
    if-eqz v0, :cond_33

    move/from16 v4, v23

    add-int/lit8 v4, v4, 0x1

    goto :goto_1d

    :cond_33
    move/from16 v4, v23

    .line 216
    :goto_1d
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    move-result v8

    if-eqz v8, :cond_34

    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO〇()I

    move-result v8

    if-lez v8, :cond_34

    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Ooo()Z

    move-result v8

    if-nez v8, :cond_34

    .line 217
    sget-object v8, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-static {v8}, Lcom/intsig/camscanner/tsapp/AutoUploadThread;->〇〇808〇(Landroid/content/Context;)Lcom/intsig/camscanner/tsapp/AutoUploadThread;

    move-result-object v8

    .line 218
    invoke-virtual {v8}, Lcom/intsig/camscanner/tsapp/AutoUploadThread;->〇0〇O0088o()V

    :cond_34
    move/from16 v9, p2

    move-wide/from16 v11, p4

    move-object/from16 v10, p6

    move-object v8, v2

    move v13, v5

    move-object v7, v6

    move-object v15, v14

    move/from16 v2, v20

    const/4 v5, 0x0

    const/4 v6, 0x1

    move/from16 v14, p1

    goto/16 :goto_0

    :cond_35
    move-object v6, v7

    move-object v2, v8

    move-object v14, v15

    if-eqz v1, :cond_36

    .line 219
    iget-object v0, v14, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo08:Landroid/os/Handler;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 220
    :cond_36
    sget-object v0, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    invoke-virtual {v0}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇〇〇0〇〇0(I)V

    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sync cost time:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long v3, v3, v17

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object v6
.end method

.method private o0O0()Z
    .locals 8

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 7
    .line 8
    .line 9
    move-result-object v2

    .line 10
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Tag;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 11
    .line 12
    const-string v0, "_id"

    .line 13
    .line 14
    filled-new-array {v0}, [Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v4

    .line 18
    const-string v5, "sync_state != 0"

    .line 19
    .line 20
    const/4 v6, 0x0

    .line 21
    const/4 v7, 0x0

    .line 22
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-lez v2, :cond_0

    .line 33
    .line 34
    const/4 v1, 0x0

    .line 35
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 36
    .line 37
    .line 38
    :cond_1
    return v1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private o0ooO(Ljava/util/HashSet;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-eqz v1, :cond_1

    .line 15
    .line 16
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    check-cast v1, Ljava/lang/Long;

    .line 21
    .line 22
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    .line 23
    .line 24
    .line 25
    move-result-wide v1

    .line 26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    const-string v4, "\'"

    .line 31
    .line 32
    if-lez v3, :cond_0

    .line 33
    .line 34
    const-string v3, ",\'"

    .line 35
    .line 36
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    goto :goto_1

    .line 40
    :cond_0
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    :goto_1
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 51
    .line 52
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 53
    .line 54
    .line 55
    const-string v1, "("

    .line 56
    .line 57
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    const-string v0, ")"

    .line 68
    .line 69
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    return-object p1
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private o8(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "sync_state = ?  and sync_jpage_state = ? and document_id in "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v5

    .line 18
    const-string p2, "5"

    .line 19
    .line 20
    filled-new-array {p2, p2}, [Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v6

    .line 24
    const-string p2, "sync_image_id"

    .line 25
    .line 26
    filled-new-array {p2}, [Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v4

    .line 30
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Image;->〇o〇:Landroid/net/Uri;

    .line 31
    .line 32
    const/4 v7, 0x0

    .line 33
    move-object v2, p1

    .line 34
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    new-instance p2, Ljava/util/ArrayList;

    .line 39
    .line 40
    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 41
    .line 42
    .line 43
    if-eqz p1, :cond_1

    .line 44
    .line 45
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    if-eqz v0, :cond_0

    .line 50
    .line 51
    const/4 v0, 0x0

    .line 52
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 61
    .line 62
    .line 63
    :cond_1
    return-object p2
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method static o800o8O()V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o800o8O:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o80ooO(Lcom/intsig/camscanner/tsapp/SyncStatus;I)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->O8()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 8
    .line 9
    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇O〇oO(Lcom/intsig/camscanner/tsapp/SyncStatus;FILcom/intsig/camscanner/tsapp/sync/SyncProgressValue;)Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 14
    .line 15
    .line 16
    move-result-wide p1

    .line 17
    sget-object v2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 18
    .line 19
    const/4 v4, 0x0

    .line 20
    const/4 v5, 0x0

    .line 21
    const/4 v6, 0x0

    .line 22
    new-instance v7, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

    .line 23
    .line 24
    invoke-direct {v7}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;-><init>()V

    .line 25
    .line 26
    .line 27
    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇〇o〇(Landroid/content/Context;Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;Ljava/lang/String;Landroid/util/LongSparseArray;ZLcom/intsig/camscanner/sharedir/data/ShareDirDBData;)Lcom/intsig/camscanner/tsapp/sync/UploadImageResponse;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    iget-boolean v0, v0, Lcom/intsig/camscanner/tsapp/sync/BaseUploadResponse;->O8:Z

    .line 32
    .line 33
    if-eqz v0, :cond_0

    .line 34
    .line 35
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/ShareDirOwnerCloudSpaceOverLimitEvent;

    .line 36
    .line 37
    const/4 v1, 0x0

    .line 38
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/ShareDirOwnerCloudSpaceOverLimitEvent;-><init>(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    invoke-static {v0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 42
    .line 43
    .line 44
    :cond_0
    sget-object v0, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 51
    .line 52
    .line 53
    move-result-wide v1

    .line 54
    sub-long/2addr v1, p1

    .line 55
    invoke-virtual {v0, v1, v2}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇o(J)V
    :try_end_0
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :catch_0
    move-exception p1

    .line 60
    const-string p2, "SyncThread"

    .line 61
    .line 62
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 63
    .line 64
    .line 65
    :goto_0
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private synthetic o88〇OO08〇(Lorg/json/JSONObject;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0O〇Oo(Lorg/json/JSONObject;)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x0

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private o8oO〇(Lcom/intsig/camscanner/tsapp/SyncStatus;IIILjava/lang/String;)Z
    .locals 24

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    move/from16 v0, p3

    move/from16 v4, p4

    const-string v5, "KEY_SPECIAL_UPGRADE_DLG"

    const-string v6, "initSyncToken"

    const-string v7, "SyncThread"

    .line 1
    invoke-static {v7, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    sget-object v6, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    if-eqz v6, :cond_16

    .line 3
    invoke-interface {v6}, Lcom/intsig/camscanner/tsapp/AccountListener;->〇080()Z

    move-result v6

    const/4 v9, 0x0

    const/4 v10, -0x1

    if-nez v6, :cond_0

    const/16 v0, -0x68

    .line 4
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 5
    invoke-direct {v1, v9}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇o(F)V

    .line 6
    invoke-virtual {v1, v10, v2, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    const-string v0, "initSyncToken no enough space"

    .line 7
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_10

    .line 8
    :cond_0
    sget-object v6, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    invoke-interface {v6}, Lcom/intsig/camscanner/tsapp/AccountListener;->oO80()Z

    move-result v6

    if-nez v6, :cond_1

    const/16 v0, -0x12c

    .line 9
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 10
    invoke-direct {v1, v9}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇o(F)V

    .line 11
    invoke-virtual {v1, v10, v2, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    const-string v0, "initSyncToken sync is false"

    .line 12
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_10

    .line 13
    :cond_1
    sget-object v6, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    invoke-interface {v6}, Lcom/intsig/camscanner/tsapp/AccountListener;->〇〇888()Ljava/lang/String;

    move-result-object v6

    .line 14
    sget-object v11, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    invoke-interface {v11}, Lcom/intsig/camscanner/tsapp/AccountListener;->OO0o〇〇〇〇0()Ljava/lang/String;

    move-result-object v11

    .line 15
    sget-object v12, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    invoke-interface {v12}, Lcom/intsig/camscanner/tsapp/AccountListener;->o〇0()Ljava/lang/String;

    move-result-object v12

    .line 16
    sget-object v13, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    invoke-interface {v13}, Lcom/intsig/camscanner/tsapp/AccountListener;->getTokenPwd()Ljava/lang/String;

    move-result-object v13

    .line 17
    sget-object v14, Lcom/intsig/camscanner/tsapp/Const;->〇080:Ljava/lang/String;

    .line 18
    sget-object v15, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    invoke-interface {v15}, Lcom/intsig/camscanner/tsapp/AccountListener;->〇80〇808〇O()Ljava/lang/String;

    move-result-object v15

    .line 19
    sget-object v16, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    invoke-interface/range {v16 .. v16}, Lcom/intsig/camscanner/tsapp/AccountListener;->〇8o8o〇()Ljava/lang/String;

    move-result-object v8

    .line 20
    sget-object v16, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    invoke-interface/range {v16 .. v16}, Lcom/intsig/camscanner/tsapp/AccountListener;->getAccountType()Ljava/lang/String;

    move-result-object v16

    .line 21
    sget-object v17, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    invoke-interface/range {v17 .. v17}, Lcom/intsig/camscanner/tsapp/AccountListener;->〇o〇()Ljava/lang/String;

    if-nez v16, :cond_4

    .line 22
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_3

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-eqz v16, :cond_2

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-eqz v16, :cond_2

    goto :goto_0

    .line 23
    :cond_2
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-eqz v16, :cond_4

    const-string v10, "pwd is empty"

    .line 24
    invoke-static {v7, v10}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    :goto_0
    const/16 v10, -0x12e

    .line 25
    invoke-virtual {v2, v10}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 26
    invoke-direct {v1, v9}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇o(F)V

    const/4 v10, -0x1

    .line 27
    invoke-virtual {v1, v10, v2, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 28
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "initSyncToken email or pwd  or tokenPwd is empty email="

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, " pwd="

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, " tokenPwd="

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v9, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v9, 0x1

    :goto_2
    if-eqz v9, :cond_15

    .line 29
    sget-object v10, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    move/from16 v18, v9

    const-string v9, "connectivity"

    invoke-virtual {v10, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/ConnectivityManager;

    .line 30
    invoke-virtual {v9}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v9

    .line 31
    sget-object v10, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    invoke-interface {v10}, Lcom/intsig/camscanner/tsapp/AccountListener;->Oo08()Z

    move-result v10

    if-eqz v9, :cond_14

    .line 32
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v19

    if-nez v19, :cond_5

    goto/16 :goto_f

    .line 33
    :cond_5
    sget-object v19, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    move-object/from16 v20, v5

    invoke-virtual/range {v19 .. v19}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v5

    move-object/from16 v19, v8

    invoke-virtual {v9}, Landroid/net/NetworkInfo;->getType()I

    move-result v8

    move-object/from16 v21, v15

    const/4 v15, 0x1

    if-ne v8, v15, :cond_6

    const/4 v8, 0x1

    goto :goto_3

    :cond_6
    const/4 v8, 0x0

    :goto_3
    invoke-virtual {v5, v8}, Lcom/intsig/tianshu/sync/SyncTimeCount;->Oo8Oo00oo(Z)V

    if-eqz v10, :cond_7

    .line 34
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    if-eq v5, v15, :cond_7

    if-eq v3, v15, :cond_7

    const/16 v0, -0x3ea

    .line 35
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    const/4 v4, 0x0

    .line 36
    invoke-direct {v1, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇o(F)V

    const/4 v4, -0x1

    .line 37
    invoke-virtual {v1, v4, v2, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initSyncToken not syncWifi net type="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " syncType="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_10

    :cond_7
    const/4 v5, 0x2

    if-ne v3, v5, :cond_8

    const/4 v8, 0x3

    if-ne v0, v8, :cond_8

    goto :goto_4

    :cond_8
    if-ne v3, v5, :cond_a

    .line 39
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_a

    :cond_9
    :goto_4
    move/from16 v9, v18

    goto/16 :goto_9

    .line 40
    :cond_a
    :try_start_0
    sget-object v8, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o800o8O:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;

    .line 41
    invoke-interface {v9}, Lcom/intsig/tianshu/sync/SyncAdapter;->O8()Lcom/intsig/tianshu/TSFolder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/intsig/tianshu/TSFile;->〇o00〇〇Oo()I

    move-result v10

    .line 42
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initSyncToken localRevision: "

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, ", reversion: "

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, 0x2

    if-ne v3, v5, :cond_d

    .line 43
    iget v5, v9, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->oO80:I

    if-ne v0, v5, :cond_d

    if-eqz v4, :cond_d

    if-lt v10, v4, :cond_d

    .line 44
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O0o〇〇Oo()Z

    move-result v5

    if-eqz v5, :cond_b

    sget-object v5, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-static {v5}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OOoo(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_b

    const/4 v5, 0x1

    goto :goto_6

    :cond_b
    const/4 v5, 0x0

    :goto_6
    if-eqz v5, :cond_c

    const/16 v8, -0x190

    .line 45
    invoke-virtual {v2, v8}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    const/4 v8, 0x0

    .line 46
    invoke-direct {v1, v8}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇o(F)V

    const/4 v8, -0x1

    .line 47
    invoke-virtual {v1, v8, v2, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v9, 0x0

    goto :goto_7

    :cond_c
    move/from16 v9, v18

    .line 48
    :goto_7
    :try_start_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "initSyncToken syncOperations no update folder="

    invoke-virtual {v8, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " localRevision="

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " revision="

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " syncComplete="

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, " result="

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_9

    :catch_0
    move-exception v0

    goto :goto_8

    :cond_d
    const/4 v5, 0x2

    goto/16 :goto_5

    :catch_1
    move-exception v0

    move/from16 v9, v18

    .line 49
    :goto_8
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_9
    if-eqz v9, :cond_13

    const/high16 v0, 0x3f800000    # 1.0f

    .line 50
    invoke-direct {v1, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇o(F)V

    const/4 v4, -0x1

    .line 51
    invoke-virtual {v1, v4, v2, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 52
    :try_start_2
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 53
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 54
    invoke-virtual {v0}, Lcom/intsig/tianshu/UserInfo;->isTokenAvailableByServer()Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "token is available, no need to login"

    .line 55
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_a

    :cond_e
    const/4 v0, 0x1

    .line 56
    :goto_a
    sget-object v8, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    invoke-virtual {v8}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    sub-long v4, v22, v4

    invoke-virtual {v8, v4, v5}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇〇8O0〇8(J)V

    move v15, v0

    goto :goto_b

    :cond_f
    const/4 v15, 0x1

    .line 57
    :goto_b
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-static {v0, v6, v11}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇〇o0o(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v15, :cond_11

    const-string v4, "email"

    const-string v5, "@"

    .line 58
    invoke-virtual {v11, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_10

    const-string v4, "mobile"

    .line 59
    :cond_10
    new-instance v5, Lcom/intsig/login/LoginParameterBuilder;

    invoke-direct {v5}, Lcom/intsig/login/LoginParameterBuilder;-><init>()V

    .line 60
    invoke-virtual {v5, v6}, Lcom/intsig/login/LoginParameterBuilder;->O8(Ljava/lang/String;)Lcom/intsig/login/LoginParameterBuilder;

    move-result-object v5

    .line 61
    invoke-virtual {v5, v11}, Lcom/intsig/login/LoginParameterBuilder;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/login/LoginParameterBuilder;

    move-result-object v5

    .line 62
    invoke-virtual {v5, v12}, Lcom/intsig/login/LoginParameterBuilder;->〇O8o08O(Ljava/lang/String;)Lcom/intsig/login/LoginParameterBuilder;

    move-result-object v5

    .line 63
    invoke-virtual {v5, v13}, Lcom/intsig/login/LoginParameterBuilder;->〇O〇(Ljava/lang/String;)Lcom/intsig/login/LoginParameterBuilder;

    move-result-object v5

    .line 64
    invoke-virtual {v5, v14}, Lcom/intsig/login/LoginParameterBuilder;->Oo08(Ljava/lang/String;)Lcom/intsig/login/LoginParameterBuilder;

    move-result-object v5

    move-object/from16 v6, v21

    .line 65
    invoke-virtual {v5, v6}, Lcom/intsig/login/LoginParameterBuilder;->〇〇888(Ljava/lang/String;)Lcom/intsig/login/LoginParameterBuilder;

    move-result-object v5

    move-object/from16 v6, v19

    .line 66
    invoke-virtual {v5, v6}, Lcom/intsig/login/LoginParameterBuilder;->o〇0(Ljava/lang/String;)Lcom/intsig/login/LoginParameterBuilder;

    move-result-object v5

    .line 67
    invoke-virtual {v5, v4}, Lcom/intsig/login/LoginParameterBuilder;->〇o〇(Ljava/lang/String;)Lcom/intsig/login/LoginParameterBuilder;

    move-result-object v4

    .line 68
    invoke-virtual {v4, v0}, Lcom/intsig/login/LoginParameterBuilder;->〇〇8O0〇8(Ljava/lang/String;)Lcom/intsig/login/LoginParameterBuilder;

    move-result-object v4

    const/4 v5, 0x1

    .line 69
    invoke-virtual {v4, v5}, Lcom/intsig/login/LoginParameterBuilder;->OO0o〇〇〇〇0(I)Lcom/intsig/login/LoginParameterBuilder;

    move-result-object v4

    .line 70
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->O8()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/intsig/login/LoginParameterBuilder;->oO80(Ljava/lang/String;)Lcom/intsig/login/LoginParameterBuilder;

    move-result-object v4
    :try_end_2
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_2 .. :try_end_2} :catch_3

    const/4 v5, 0x0

    .line 71
    :try_start_3
    invoke-virtual {v4, v5}, Lcom/intsig/login/LoginParameterBuilder;->〇8o8o〇(Z)Lcom/intsig/login/LoginParameterBuilder;

    move-result-object v4

    .line 72
    invoke-virtual {v4}, Lcom/intsig/login/LoginParameterBuilder;->〇080()Lcom/intsig/tianshu/parameter/LoginParameter;

    move-result-object v4

    .line 73
    invoke-static {v4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO8〇(Lcom/intsig/tianshu/parameter/LoginParameter;)V

    goto :goto_c

    :cond_11
    const/4 v5, 0x0

    .line 74
    :goto_c
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initSyncToken login success need2Login:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o8oO〇()Z

    move-result v4

    if-nez v4, :cond_13

    .line 76
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    move-result-object v4

    invoke-static {v4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OOo88OOo(Lcom/intsig/tianshu/UserInfo;)Z

    move-result v4

    .line 77
    sget-object v6, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 78
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v10, v20

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v11, -0x1

    invoke-interface {v6, v8, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    const/4 v11, 0x1

    if-eq v8, v11, :cond_12

    .line 79
    sget-object v8, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-static {v8}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O〇oO〇oo8o(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_12

    if-eqz v4, :cond_12

    .line 80
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v8, 0x2

    .line 81
    invoke-interface {v6, v0, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 82
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 83
    :cond_12
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    invoke-static {v0, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8〇OO0〇0o(Landroid/content/Context;Z)V

    .line 84
    invoke-static {v4}, Lcom/intsig/camscanner/launch/CsApplication;->〇O〇80o08O(Z)V
    :try_end_3
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_e

    :catch_2
    move-exception v0

    goto :goto_d

    :catch_3
    move-exception v0

    const/4 v5, 0x0

    .line 85
    :goto_d
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 86
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    const/4 v4, 0x0

    .line 87
    invoke-direct {v1, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇o(F)V

    const/4 v6, -0x1

    .line 88
    invoke-virtual {v1, v6, v2, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    goto :goto_10

    :cond_13
    :goto_e
    move v8, v9

    goto :goto_11

    :cond_14
    :goto_f
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, -0x1

    const/16 v0, -0x65

    .line 89
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 90
    invoke-direct {v1, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇o(F)V

    .line 91
    invoke-virtual {v1, v6, v2, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    const-string v0, "initSyncToken network is not connected"

    .line 92
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_10

    :cond_15
    move/from16 v18, v9

    move/from16 v8, v18

    goto :goto_11

    :cond_16
    const/4 v5, 0x0

    const-string v0, "accountListener = null"

    .line 93
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    :goto_10
    const/4 v8, 0x0

    :goto_11
    return v8
.end method

.method private oO(Landroid/content/ContentResolver;Ljava/lang/String;)I
    .locals 3

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oOO8O8(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/HashSet;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    invoke-virtual {p2}, Ljava/util/HashSet;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    return v1

    .line 13
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O8ooOoo〇(Landroid/content/ContentResolver;Ljava/util/HashSet;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p2}, Ljava/util/HashSet;->size()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    return v1

    .line 23
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string v2, "_id in "

    .line 29
    .line 30
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o0ooO(Ljava/util/HashSet;)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p2

    .line 37
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object p2

    .line 44
    :try_start_0
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->O8:Landroid/net/Uri;

    .line 45
    .line 46
    const/4 v2, 0x0

    .line 47
    invoke-virtual {p1, v0, p2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 48
    .line 49
    .line 50
    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    goto :goto_0

    .line 52
    :catch_0
    move-exception p1

    .line 53
    const-string p2, "SyncThread"

    .line 54
    .line 55
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 56
    .line 57
    .line 58
    :goto_0
    return v1
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private oO00OOO()Z
    .locals 8

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 7
    .line 8
    .line 9
    move-result-object v2

    .line 10
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Document;->O8:Landroid/net/Uri;

    .line 11
    .line 12
    const-string v0, "_id"

    .line 13
    .line 14
    filled-new-array {v0}, [Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v4

    .line 18
    const-string v5, "sync_state != 0 AND sync_state != 5"

    .line 19
    .line 20
    const/4 v6, 0x0

    .line 21
    const/4 v7, 0x0

    .line 22
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-lez v2, :cond_0

    .line 33
    .line 34
    const/4 v1, 0x0

    .line 35
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 36
    .line 37
    .line 38
    :cond_1
    return v1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/ArrayList;Lcom/intsig/camscanner/tsapp/SyncStatus;FI)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0(Ljava/util/ArrayList;Lcom/intsig/camscanner/tsapp/SyncStatus;FI)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method private oO〇(Ljava/lang/String;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/tianshu/exception/TianShuException;
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->oO()Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sget-object v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 6
    .line 7
    invoke-virtual {v0, v1, p1}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇000O0(Landroid/content/Context;Ljava/lang/String;)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    if-eqz p1, :cond_0

    .line 12
    .line 13
    const/4 p1, 0x0

    .line 14
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->o0ooO(Ljava/lang/String;)Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    if-nez p1, :cond_1

    .line 19
    .line 20
    sget-object v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇8(Landroid/content/Context;)J

    .line 23
    .line 24
    .line 25
    move-result-wide v2

    .line 26
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇O〇80o08O(Landroid/content/Context;J)Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eqz v0, :cond_1

    .line 31
    .line 32
    const/4 p1, 0x1

    .line 33
    goto :goto_0

    .line 34
    :cond_0
    const/4 p1, 0x0

    .line 35
    :cond_1
    :goto_0
    return p1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private oo88o8O(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    new-instance v0, Ljava/util/ArrayList;

    .line 4
    .line 5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-eqz v1, :cond_0

    .line 17
    .line 18
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Ljava/lang/Long;

    .line 23
    .line 24
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$SyncRestore;->〇080:Landroid/net/Uri;

    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    .line 27
    .line 28
    .line 29
    move-result-wide v3

    .line 30
    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    if-lez p1, :cond_2

    .line 51
    .line 52
    sget-object p1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 53
    .line 54
    const-string v1, "SyncThread"

    .line 55
    .line 56
    if-eqz p1, :cond_1

    .line 57
    .line 58
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    sget-object v2, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 63
    .line 64
    invoke-virtual {p1, v2, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    .line 66
    .line 67
    goto :goto_1

    .line 68
    :catch_0
    move-exception p1

    .line 69
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 70
    .line 71
    .line 72
    goto :goto_1

    .line 73
    :catch_1
    move-exception p1

    .line 74
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 75
    .line 76
    .line 77
    goto :goto_1

    .line 78
    :cond_1
    const-string p1, "mConText == null"

    .line 79
    .line 80
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    :cond_2
    :goto_1
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private ooO〇00O()V
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/SyncConnection;->OO0o〇〇(I)V

    .line 3
    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OOo0O()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private ooo0〇O88O(Ljava/util/List;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    const-string v0, "SyncThread"

    .line 2
    .line 3
    if-eqz p1, :cond_6

    .line 4
    .line 5
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    goto/16 :goto_2

    .line 12
    .line 13
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 14
    .line 15
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0OO8(Landroid/content/Context;)I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    const-string v2, "sortOrder="

    .line 20
    .line 21
    if-ltz v1, :cond_5

    .line 22
    .line 23
    sget-object v3, Lcom/intsig/camscanner/util/CONSTANT;->〇o00〇〇Oo:[Ljava/lang/String;

    .line 24
    .line 25
    array-length v4, v3

    .line 26
    if-le v1, v4, :cond_1

    .line 27
    .line 28
    goto/16 :goto_1

    .line 29
    .line 30
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    .line 31
    .line 32
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    aget-object v2, v3, v1

    .line 39
    .line 40
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    new-instance v0, Ljava/util/ArrayList;

    .line 51
    .line 52
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 53
    .line 54
    .line 55
    new-instance v2, Ljava/util/ArrayList;

    .line 56
    .line 57
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 58
    .line 59
    .line 60
    invoke-static {p1}, Lcom/intsig/camscanner/app/DBUtil;->Oo08(Ljava/util/Collection;)Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    const-string v4, "_id"

    .line 65
    .line 66
    const-string v5, "sync_dir_id"

    .line 67
    .line 68
    filled-new-array {v4, v5}, [Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v8

    .line 72
    new-instance v4, Ljava/lang/StringBuilder;

    .line 73
    .line 74
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    .line 76
    .line 77
    const-string v5, "_id in ("

    .line 78
    .line 79
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    const-string p1, ")"

    .line 86
    .line 87
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v9

    .line 94
    sget-object p1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 95
    .line 96
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 97
    .line 98
    .line 99
    move-result-object v6

    .line 100
    sget-object v7, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 101
    .line 102
    const/4 v10, 0x0

    .line 103
    aget-object v11, v3, v1

    .line 104
    .line 105
    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 106
    .line 107
    .line 108
    move-result-object p1

    .line 109
    if-eqz p1, :cond_4

    .line 110
    .line 111
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 112
    .line 113
    .line 114
    move-result v1

    .line 115
    if-eqz v1, :cond_3

    .line 116
    .line 117
    const/4 v1, 0x1

    .line 118
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object v1

    .line 122
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 123
    .line 124
    .line 125
    move-result v1

    .line 126
    const/4 v3, 0x0

    .line 127
    if-eqz v1, :cond_2

    .line 128
    .line 129
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    .line 130
    .line 131
    .line 132
    move-result-wide v3

    .line 133
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 134
    .line 135
    .line 136
    move-result-object v1

    .line 137
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    .line 139
    .line 140
    goto :goto_0

    .line 141
    :cond_2
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    .line 142
    .line 143
    .line 144
    move-result-wide v3

    .line 145
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 146
    .line 147
    .line 148
    move-result-object v1

    .line 149
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    .line 151
    .line 152
    goto :goto_0

    .line 153
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 154
    .line 155
    .line 156
    :cond_4
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 157
    .line 158
    .line 159
    return-object v0

    .line 160
    :cond_5
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    .line 161
    .line 162
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 163
    .line 164
    .line 165
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    .line 167
    .line 168
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 169
    .line 170
    .line 171
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 172
    .line 173
    .line 174
    move-result-object v1

    .line 175
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .line 177
    .line 178
    return-object p1

    .line 179
    :cond_6
    :goto_2
    const-string v1, "sortDownLoadDoc inputDocIdList is empty"

    .line 180
    .line 181
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    .line 183
    .line 184
    return-object p1
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public static ooo〇8oO()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oo88o8O:Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO8oO0o〇()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const-string v0, "SyncThread"

    .line 11
    .line 12
    const-string v1, "isSync mSyncThread is null"

    .line 13
    .line 14
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
    .line 21
.end method

.method public static oo〇(Landroid/content/Context;)Lcom/intsig/camscanner/tsapp/sync/SyncThread;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    sput-object p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 6
    .line 7
    :cond_0
    sget-object p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oo88o8O:Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 8
    .line 9
    if-nez p0, :cond_1

    .line 10
    .line 11
    new-instance p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;-><init>()V

    .line 14
    .line 15
    .line 16
    sput-object p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oo88o8O:Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 17
    .line 18
    :cond_1
    sget-object p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oo88o8O:Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 19
    .line 20
    return-object p0
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private o〇8(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "sync_state =? and _id in "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v5

    .line 18
    const-string p2, "5"

    .line 19
    .line 20
    filled-new-array {p2}, [Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v6

    .line 24
    const-string p2, "sync_doc_id"

    .line 25
    .line 26
    filled-new-array {p2}, [Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v4

    .line 30
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Document;->O8:Landroid/net/Uri;

    .line 31
    .line 32
    const/4 v7, 0x0

    .line 33
    move-object v2, p1

    .line 34
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    new-instance p2, Ljava/util/ArrayList;

    .line 39
    .line 40
    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 41
    .line 42
    .line 43
    if-eqz p1, :cond_1

    .line 44
    .line 45
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    if-eqz v0, :cond_0

    .line 50
    .line 51
    const/4 v0, 0x0

    .line 52
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 61
    .line 62
    .line 63
    :cond_1
    return-object p2
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private o〇8oOO88()Z
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O8〇o()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    const-wide/16 v2, 0x0

    .line 6
    .line 7
    cmp-long v4, v0, v2

    .line 8
    .line 9
    if-gtz v4, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o〇O()Z
    .locals 8

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 7
    .line 8
    .line 9
    move-result-object v2

    .line 10
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Dir;->〇080:Landroid/net/Uri;

    .line 11
    .line 12
    const-string v0, "_id"

    .line 13
    .line 14
    filled-new-array {v0}, [Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v4

    .line 18
    const-string v5, "sync_state != 0 AND sync_state != 5"

    .line 19
    .line 20
    const/4 v6, 0x0

    .line 21
    const/4 v7, 0x0

    .line 22
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-lez v2, :cond_0

    .line 33
    .line 34
    const/4 v1, 0x0

    .line 35
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 36
    .line 37
    .line 38
    :cond_1
    return v1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private o〇o(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇8O0〇8:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private o〇〇0〇(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o:Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_1

    .line 21
    .line 22
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    check-cast v1, Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;

    .line 27
    .line 28
    invoke-interface {v1, p1}, Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;->〇o〇(I)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private synthetic 〇0(Ljava/util/ArrayList;Lcom/intsig/camscanner/tsapp/SyncStatus;FI)V
    .locals 11

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->ooo0〇O88O(Ljava/util/List;)Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    new-instance v2, Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    if-eqz v3, :cond_2

    .line 23
    .line 24
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    check-cast v3, Ljava/lang/Long;

    .line 29
    .line 30
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->〇8o()Z

    .line 31
    .line 32
    .line 33
    move-result v4

    .line 34
    if-eqz v4, :cond_1

    .line 35
    .line 36
    goto :goto_1

    .line 37
    :cond_1
    iget-object v4, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 38
    .line 39
    invoke-virtual {p0, p2, p3, p4, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇O〇oO(Lcom/intsig/camscanner/tsapp/SyncStatus;FILcom/intsig/camscanner/tsapp/sync/SyncProgressValue;)Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;

    .line 40
    .line 41
    .line 42
    move-result-object v9

    .line 43
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncImgDownloadHelper;->〇080()Lcom/intsig/camscanner/tsapp/sync/SyncImgDownloadHelper;

    .line 44
    .line 45
    .line 46
    move-result-object v5

    .line 47
    sget-object v6, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 48
    .line 49
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    .line 50
    .line 51
    .line 52
    move-result-wide v7

    .line 53
    invoke-virtual {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇o〇()Ljava/util/Vector;

    .line 54
    .line 55
    .line 56
    move-result-object v10

    .line 57
    invoke-virtual/range {v5 .. v10}, Lcom/intsig/camscanner/tsapp/sync/SyncImgDownloadHelper;->〇o00〇〇Oo(Landroid/content/Context;JLcom/intsig/tianshu/sync/SyncApi$SyncProgress;Ljava/util/Vector;)Ljava/util/concurrent/Future;

    .line 58
    .line 59
    .line 60
    move-result-object v3

    .line 61
    if-eqz v3, :cond_0

    .line 62
    .line 63
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 72
    .line 73
    .line 74
    move-result p2

    .line 75
    if-eqz p2, :cond_3

    .line 76
    .line 77
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 78
    .line 79
    .line 80
    move-result-object p2

    .line 81
    check-cast p2, Ljava/util/concurrent/Future;

    .line 82
    .line 83
    :try_start_0
    invoke-interface {p2}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    .line 85
    .line 86
    goto :goto_2

    .line 87
    :catch_0
    move-exception p2

    .line 88
    const-string p3, "SyncThread"

    .line 89
    .line 90
    invoke-static {p3, p2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 91
    .line 92
    .line 93
    goto :goto_2

    .line 94
    :cond_3
    sget-object p1, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 95
    .line 96
    invoke-virtual {p1}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 97
    .line 98
    .line 99
    move-result-object p2

    .line 100
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 101
    .line 102
    .line 103
    move-result-wide p3

    .line 104
    sub-long/2addr p3, v0

    .line 105
    invoke-virtual {p2, p3, p4}, Lcom/intsig/tianshu/sync/SyncTimeCount;->Oooo8o0〇(J)V

    .line 106
    .line 107
    .line 108
    invoke-virtual {p1}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 109
    .line 110
    .line 111
    move-result-object p1

    .line 112
    invoke-virtual {p1}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇o〇()V

    .line 113
    .line 114
    .line 115
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private 〇00(Lcom/intsig/camscanner/tsapp/SyncStatus;ZI)V
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    .line 2
    .line 3
    const-string v1, "SyncThread"

    .line 4
    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    new-instance v0, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v2, "doWorkAfterSync is success="

    .line 13
    .line 14
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    const/4 v0, -0x1

    .line 28
    const/high16 v2, 0x42c80000    # 100.0f

    .line 29
    .line 30
    if-eqz p2, :cond_0

    .line 31
    .line 32
    sget-object p2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    .line 33
    .line 34
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 35
    .line 36
    .line 37
    move-result-wide v3

    .line 38
    invoke-interface {p2, v3, v4}, Lcom/intsig/camscanner/tsapp/AccountListener;->O8(J)V

    .line 39
    .line 40
    .line 41
    iget-object p2, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 42
    .line 43
    invoke-virtual {p2}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->O8()F

    .line 44
    .line 45
    .line 46
    move-result p2

    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 48
    .line 49
    invoke-virtual {p0, p1, p2, p3, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇O〇oO(Lcom/intsig/camscanner/tsapp/SyncStatus;FILcom/intsig/camscanner/tsapp/sync/SyncProgressValue;)Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;

    .line 50
    .line 51
    .line 52
    move-result-object p2

    .line 53
    sget-object v3, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    .line 54
    .line 55
    invoke-virtual {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇o〇()Ljava/util/Vector;

    .line 56
    .line 57
    .line 58
    move-result-object v4

    .line 59
    invoke-interface {v3, p2, v4}, Lcom/intsig/camscanner/tsapp/AccountListener;->〇o00〇〇Oo(Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;Ljava/util/Vector;)V

    .line 60
    .line 61
    .line 62
    iget-object p2, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 63
    .line 64
    invoke-virtual {p2}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇o〇()F

    .line 65
    .line 66
    .line 67
    move-result p2

    .line 68
    sub-float/2addr v2, p2

    .line 69
    iget-object p2, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇808〇:Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

    .line 70
    .line 71
    invoke-virtual {p2}, Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;->o800o8O()F

    .line 72
    .line 73
    .line 74
    move-result p2

    .line 75
    sub-float/2addr v2, p2

    .line 76
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇o(F)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {p0, v0, p1, p3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 80
    .line 81
    .line 82
    new-instance p1, Ljava/lang/StringBuilder;

    .line 83
    .line 84
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 85
    .line 86
    .line 87
    const-string p2, " isSyncingRawJpg="

    .line 88
    .line 89
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    iget-boolean p2, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇8o8o〇:Z

    .line 93
    .line 94
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    goto :goto_0

    .line 105
    :cond_0
    sget-object p2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O888o0o:Lcom/intsig/camscanner/tsapp/AccountListener;

    .line 106
    .line 107
    invoke-virtual {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇o〇()Ljava/util/Vector;

    .line 108
    .line 109
    .line 110
    move-result-object v1

    .line 111
    const/4 v3, 0x0

    .line 112
    invoke-interface {p2, v3, v1}, Lcom/intsig/camscanner/tsapp/AccountListener;->〇o00〇〇Oo(Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;Ljava/util/Vector;)V

    .line 113
    .line 114
    .line 115
    iget-object p2, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 116
    .line 117
    invoke-virtual {p2}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇o〇()F

    .line 118
    .line 119
    .line 120
    move-result p2

    .line 121
    sub-float/2addr v2, p2

    .line 122
    iget-object p2, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇808〇:Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

    .line 123
    .line 124
    invoke-virtual {p2}, Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;->o800o8O()F

    .line 125
    .line 126
    .line 127
    move-result p2

    .line 128
    sub-float/2addr v2, p2

    .line 129
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇o(F)V

    .line 130
    .line 131
    .line 132
    invoke-virtual {p0, v0, p1, p3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 133
    .line 134
    .line 135
    goto :goto_0

    .line 136
    :cond_1
    const-string p1, "accountListener = null"

    .line 137
    .line 138
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    :goto_0
    return-void
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private 〇0000OOO(I)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "finishUploadAllDoc errorCode == "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "SyncThread"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o:Ljava/util/List;

    .line 24
    .line 25
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-nez v0, :cond_0

    .line 30
    .line 31
    return-void

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o:Ljava/util/List;

    .line 33
    .line 34
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    if-eqz v1, :cond_1

    .line 43
    .line 44
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    check-cast v1, Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;

    .line 49
    .line 50
    invoke-interface {v1, p1}, Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;->〇o00〇〇Oo(I)V

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_1
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private 〇000O0(JI)V
    .locals 1

    .line 1
    invoke-static {p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇0O〇Oo(J)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-ne v0, p3, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 9
    .line 10
    invoke-static {v0, p1, p2, p3}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO88〇OOO(Landroid/content/Context;JI)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇000〇〇08()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O0o〇〇Oo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "updateSyncRestorePoints syncComplete="

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v2, " local data change="

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    sget-boolean v2, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇080:Z

    .line 24
    .line 25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    const-string v2, "SyncThread"

    .line 33
    .line 34
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    if-eqz v0, :cond_2

    .line 38
    .line 39
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O()V

    .line 40
    .line 41
    .line 42
    new-instance v0, Ljava/util/ArrayList;

    .line 43
    .line 44
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 45
    .line 46
    .line 47
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇〇0〇〇0(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    new-instance v2, Ljava/util/ArrayList;

    .line 52
    .line 53
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .line 55
    .line 56
    if-eqz v1, :cond_2

    .line 57
    .line 58
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    const/4 v3, 0x0

    .line 63
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 64
    .line 65
    .line 66
    move-result v4

    .line 67
    if-eqz v4, :cond_1

    .line 68
    .line 69
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 70
    .line 71
    .line 72
    move-result-object v4

    .line 73
    check-cast v4, Ljava/lang/String;

    .line 74
    .line 75
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O00(Ljava/lang/String;)Z

    .line 76
    .line 77
    .line 78
    move-result v4

    .line 79
    if-eqz v4, :cond_0

    .line 80
    .line 81
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 82
    .line 83
    .line 84
    move-result-object v4

    .line 85
    check-cast v4, Ljava/lang/Long;

    .line 86
    .line 87
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    .line 89
    .line 90
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 91
    .line 92
    goto :goto_0

    .line 93
    :cond_1
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oo88o8O(Ljava/util/ArrayList;)V

    .line 94
    .line 95
    .line 96
    :cond_2
    return-void
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private static 〇00O0O0(I)V
    .locals 3

    .line 1
    const-string v0, "SyncThread"

    .line 2
    .line 3
    const-string v1, "sendSafeVerifyBroadcast"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Landroid/content/Intent;

    .line 9
    .line 10
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 11
    .line 12
    .line 13
    sget-object v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 14
    .line 15
    invoke-static {v1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    const-string v2, "com.intsig.camscanner.safetyverification"

    .line 20
    .line 21
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 22
    .line 23
    .line 24
    const-string v2, "hint_tip_code"

    .line 25
    .line 26
    invoke-virtual {v0, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1, v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private 〇0O〇Oo(Lorg/json/JSONObject;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/debug/PerformanceDataCollectedTask;->〇o〇:Lcom/intsig/camscanner/debug/PerformanceDataCollectedTask$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/debug/PerformanceDataCollectedTask$Companion;->〇080()Lcom/intsig/camscanner/debug/PerformanceDataCollectedTask;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "finish_sync"

    .line 8
    .line 9
    const-string v2, "sync_opt"

    .line 10
    .line 11
    invoke-virtual {v0, v2, v1, p1}, Lcom/intsig/camscanner/debug/PerformanceDataCollectedTask;->o〇0(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇0〇O0088o()[Z
    .locals 8

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [Z

    .line 3
    .line 4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 5
    .line 6
    .line 7
    move-result-wide v1

    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O8〇o()J

    .line 9
    .line 10
    .line 11
    move-result-wide v3

    .line 12
    sget-object v5, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 13
    .line 14
    invoke-virtual {v5}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 15
    .line 16
    .line 17
    move-result-object v5

    .line 18
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 19
    .line 20
    .line 21
    move-result-wide v6

    .line 22
    sub-long/2addr v6, v1

    .line 23
    invoke-virtual {v5, v6, v7}, Lcom/intsig/tianshu/sync/SyncTimeCount;->oO80(J)V

    .line 24
    .line 25
    .line 26
    const-wide/16 v1, 0x0

    .line 27
    .line 28
    cmp-long v5, v3, v1

    .line 29
    .line 30
    if-gtz v5, :cond_2

    .line 31
    .line 32
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 33
    .line 34
    invoke-static {v1}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇O888o0o(Landroid/content/Context;)I

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    const/4 v2, 0x0

    .line 39
    const/4 v3, 0x1

    .line 40
    aput-boolean v3, v0, v2

    .line 41
    .line 42
    if-gtz v1, :cond_0

    .line 43
    .line 44
    const/4 v1, 0x1

    .line 45
    goto :goto_0

    .line 46
    :cond_0
    const/4 v1, 0x0

    .line 47
    :goto_0
    aput-boolean v1, v0, v3

    .line 48
    .line 49
    if-eqz v1, :cond_2

    .line 50
    .line 51
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 52
    .line 53
    invoke-static {v1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇80〇808〇O(Landroid/content/Context;)I

    .line 54
    .line 55
    .line 56
    move-result v1

    .line 57
    if-gtz v1, :cond_1

    .line 58
    .line 59
    const/4 v2, 0x1

    .line 60
    :cond_1
    aput-boolean v2, v0, v3

    .line 61
    .line 62
    :cond_2
    return-object v0
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private 〇8(Landroid/content/ContentResolver;Ljava/lang/String;)I
    .locals 10

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "sync_state = ?  and sync_jpage_state = ? and document_id in "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p2

    .line 18
    const-string v0, "5"

    .line 19
    .line 20
    filled-new-array {v0, v0}, [Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Image;->〇o〇:Landroid/net/Uri;

    .line 25
    .line 26
    const-string v4, "_data"

    .line 27
    .line 28
    const-string v5, "raw_data"

    .line 29
    .line 30
    const-string v6, "ocr_border"

    .line 31
    .line 32
    const-string v7, "image_backup"

    .line 33
    .line 34
    const-string v8, "thumb_data"

    .line 35
    .line 36
    const-string v9, "document_id"

    .line 37
    .line 38
    filled-new-array/range {v4 .. v9}, [Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v4

    .line 42
    const/4 v7, 0x0

    .line 43
    move-object v2, p1

    .line 44
    move-object v5, p2

    .line 45
    move-object v6, v0

    .line 46
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    new-instance v2, Ljava/util/ArrayList;

    .line 51
    .line 52
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 53
    .line 54
    .line 55
    const/4 v3, 0x0

    .line 56
    if-eqz v1, :cond_2

    .line 57
    .line 58
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 59
    .line 60
    .line 61
    move-result v4

    .line 62
    if-eqz v4, :cond_1

    .line 63
    .line 64
    const/4 v4, 0x0

    .line 65
    :goto_0
    const/4 v5, 0x5

    .line 66
    if-ge v4, v5, :cond_0

    .line 67
    .line 68
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v5

    .line 72
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    .line 74
    .line 75
    add-int/lit8 v4, v4, 0x1

    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 79
    .line 80
    .line 81
    :cond_2
    :try_start_0
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$Image;->〇o〇:Landroid/net/Uri;

    .line 82
    .line 83
    invoke-virtual {p1, v1, p2, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 84
    .line 85
    .line 86
    move-result v3

    .line 87
    if-lez v3, :cond_3

    .line 88
    .line 89
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->OO0o〇〇〇〇0(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    .line 91
    .line 92
    goto :goto_1

    .line 93
    :catch_0
    move-exception p1

    .line 94
    const-string p2, "SyncThread"

    .line 95
    .line 96
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 97
    .line 98
    .line 99
    :cond_3
    :goto_1
    return v3
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private 〇80()Z
    .locals 8

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 7
    .line 8
    .line 9
    move-result-object v2

    .line 10
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Image;->〇o〇:Landroid/net/Uri;

    .line 11
    .line 12
    const-string v0, "_id"

    .line 13
    .line 14
    filled-new-array {v0}, [Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v4

    .line 18
    const-string v5, "sync_state != 0 AND sync_jpage_state != 0 AND sync_state != 5 AND sync_jpage_state != 5"

    .line 19
    .line 20
    const/4 v6, 0x0

    .line 21
    const/4 v7, 0x0

    .line 22
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-lez v2, :cond_0

    .line 33
    .line 34
    const/4 v1, 0x0

    .line 35
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 36
    .line 37
    .line 38
    :cond_1
    return v1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/concurrent/Future;[ZLcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;[Z[IJLcom/intsig/tianshu/sync/SyncState;IFFF[Z)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p14}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O8O〇(Ljava/util/concurrent/Future;[ZLcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocBean;[Z[IJLcom/intsig/tianshu/sync/SyncState;IFFF[Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method static bridge synthetic 〇8o8o〇(Lcom/intsig/camscanner/tsapp/sync/SyncThread;)Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇808〇:Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇O()V
    .locals 14

    .line 1
    sget-boolean v0, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇080:Z

    .line 2
    .line 3
    if-eqz v0, :cond_6

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 12
    .line 13
    const-string v1, "sync_version"

    .line 14
    .line 15
    filled-new-array {v1}, [Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    const/4 v4, 0x0

    .line 20
    const/4 v5, 0x0

    .line 21
    const-string v6, "sync_version DESC"

    .line 22
    .line 23
    move-object v1, v0

    .line 24
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    const/4 v7, 0x0

    .line 29
    const-wide/16 v8, 0x0

    .line 30
    .line 31
    if-eqz v1, :cond_1

    .line 32
    .line 33
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    if-eqz v2, :cond_0

    .line 38
    .line 39
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getLong(I)J

    .line 40
    .line 41
    .line 42
    move-result-wide v2

    .line 43
    goto :goto_0

    .line 44
    :cond_0
    move-wide v2, v8

    .line 45
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 46
    .line 47
    .line 48
    move-wide v10, v2

    .line 49
    goto :goto_1

    .line 50
    :cond_1
    move-wide v10, v8

    .line 51
    :goto_1
    cmp-long v1, v10, v8

    .line 52
    .line 53
    if-lez v1, :cond_5

    .line 54
    .line 55
    sget-object v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 56
    .line 57
    invoke-static {v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O8O〇88oO0(Landroid/content/Context;)J

    .line 58
    .line 59
    .line 60
    move-result-wide v12

    .line 61
    cmp-long v1, v12, v8

    .line 62
    .line 63
    if-lez v1, :cond_3

    .line 64
    .line 65
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$SyncAccount;->〇080:Landroid/net/Uri;

    .line 66
    .line 67
    invoke-static {v1, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 68
    .line 69
    .line 70
    move-result-object v2

    .line 71
    const-string v1, "sync_doc_version"

    .line 72
    .line 73
    const-string v3, "sync_tag_version"

    .line 74
    .line 75
    filled-new-array {v1, v3}, [Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v3

    .line 79
    const/4 v4, 0x0

    .line 80
    const/4 v5, 0x0

    .line 81
    const/4 v6, 0x0

    .line 82
    move-object v1, v0

    .line 83
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 84
    .line 85
    .line 86
    move-result-object v1

    .line 87
    if-eqz v1, :cond_3

    .line 88
    .line 89
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 90
    .line 91
    .line 92
    move-result v2

    .line 93
    if-eqz v2, :cond_2

    .line 94
    .line 95
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getLong(I)J

    .line 96
    .line 97
    .line 98
    move-result-wide v2

    .line 99
    const/4 v4, 0x1

    .line 100
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    .line 101
    .line 102
    .line 103
    move-result-wide v4

    .line 104
    goto :goto_2

    .line 105
    :cond_2
    move-wide v2, v8

    .line 106
    move-wide v4, v2

    .line 107
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 108
    .line 109
    .line 110
    goto :goto_3

    .line 111
    :cond_3
    move-wide v2, v8

    .line 112
    move-wide v4, v2

    .line 113
    :goto_3
    cmp-long v1, v2, v8

    .line 114
    .line 115
    if-lez v1, :cond_4

    .line 116
    .line 117
    cmp-long v1, v4, v8

    .line 118
    .line 119
    if-lez v1, :cond_4

    .line 120
    .line 121
    new-instance v1, Landroid/content/ContentValues;

    .line 122
    .line 123
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 124
    .line 125
    .line 126
    const-string v6, "doc_version"

    .line 127
    .line 128
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 129
    .line 130
    .line 131
    move-result-object v8

    .line 132
    invoke-virtual {v1, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 133
    .line 134
    .line 135
    const-string v6, "tag_version"

    .line 136
    .line 137
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 138
    .line 139
    .line 140
    move-result-object v8

    .line 141
    invoke-virtual {v1, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 142
    .line 143
    .line 144
    const-string v6, "page_version"

    .line 145
    .line 146
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 147
    .line 148
    .line 149
    move-result-object v8

    .line 150
    invoke-virtual {v1, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 151
    .line 152
    .line 153
    const-string v6, "sync_account_id"

    .line 154
    .line 155
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 156
    .line 157
    .line 158
    move-result-object v8

    .line 159
    invoke-virtual {v1, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 160
    .line 161
    .line 162
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 163
    .line 164
    .line 165
    move-result-wide v8

    .line 166
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 167
    .line 168
    .line 169
    move-result-object v6

    .line 170
    const-string v8, "sync_time"

    .line 171
    .line 172
    invoke-virtual {v1, v8, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 173
    .line 174
    .line 175
    sget-object v6, Lcom/intsig/camscanner/provider/Documents$SyncRestore;->〇080:Landroid/net/Uri;

    .line 176
    .line 177
    invoke-virtual {v0, v6, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 178
    .line 179
    .line 180
    :cond_4
    move-wide v8, v2

    .line 181
    goto :goto_4

    .line 182
    :cond_5
    move-wide v4, v8

    .line 183
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    .line 184
    .line 185
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 186
    .line 187
    .line 188
    const-string v1, "saveRestorePoint pageFolderVersion="

    .line 189
    .line 190
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    .line 192
    .line 193
    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 194
    .line 195
    .line 196
    const-string v1, " docFolderVersion="

    .line 197
    .line 198
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    .line 200
    .line 201
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 202
    .line 203
    .line 204
    const-string v1, " tagFolderVersion="

    .line 205
    .line 206
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    .line 208
    .line 209
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 210
    .line 211
    .line 212
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 213
    .line 214
    .line 215
    move-result-object v0

    .line 216
    const-string v1, "SyncThread"

    .line 217
    .line 218
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    .line 220
    .line 221
    sput-boolean v7, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇080:Z

    .line 222
    .line 223
    :cond_6
    return-void
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private 〇O888o0o()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇O8o08O(Lcom/intsig/camscanner/tsapp/sync/SyncThread;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0000OOO(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static 〇O〇(Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o800o8O:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇O〇80o08O()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Ooo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 8
    .line 9
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Oo〇(Landroid/content/Context;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
    .line 21
.end method

.method private 〇o0O0O8()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/debug/PerformanceDataCollectedTask;->〇o〇:Lcom/intsig/camscanner/debug/PerformanceDataCollectedTask$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/debug/PerformanceDataCollectedTask$Companion;->〇080()Lcom/intsig/camscanner/debug/PerformanceDataCollectedTask;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/debug/PerformanceDataCollectedTask;->〇o〇()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    sget-object v1, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 12
    .line 13
    invoke-virtual {v1}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    new-instance v2, LO0〇O80ooo/〇0000OOO;

    .line 18
    .line 19
    invoke-direct {v2, p0}, LO0〇O80ooo/〇0000OOO;-><init>(Lcom/intsig/camscanner/tsapp/sync/SyncThread;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1, v0, v2}, Lcom/intsig/tianshu/sync/SyncTimeCount;->o0ooO(ZLkotlin/jvm/functions/Function1;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private 〇oOO8O8(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/HashSet;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "sync_state =? and _id in "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v5

    .line 18
    const-string p2, "5"

    .line 19
    .line 20
    filled-new-array {p2}, [Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v6

    .line 24
    const-string p2, "_id"

    .line 25
    .line 26
    filled-new-array {p2}, [Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v4

    .line 30
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Document;->O8:Landroid/net/Uri;

    .line 31
    .line 32
    const/4 v7, 0x0

    .line 33
    move-object v2, p1

    .line 34
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    new-instance p2, Ljava/util/HashSet;

    .line 39
    .line 40
    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    .line 41
    .line 42
    .line 43
    if-eqz p1, :cond_1

    .line 44
    .line 45
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    if-eqz v0, :cond_0

    .line 50
    .line 51
    const/4 v0, 0x0

    .line 52
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    .line 53
    .line 54
    .line 55
    move-result-wide v0

    .line 56
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual {p2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 65
    .line 66
    .line 67
    :cond_1
    return-object p2
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private 〇oo〇(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 3

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇8(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o8(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object p2

    .line 9
    new-instance v1, Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {v1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 15
    .line 16
    .line 17
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 18
    .line 19
    .line 20
    move-result p2

    .line 21
    const-string v0, "SyncThread"

    .line 22
    .line 23
    if-nez p2, :cond_0

    .line 24
    .line 25
    const-string p1, "deleteWebRecycleBinRecord no record"

    .line 26
    .line 27
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    return-void

    .line 31
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v2, "deleteWebRecycleBinRecord aLLSyncIdList="

    .line 37
    .line 38
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p2

    .line 52
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-static {v1}, Lcom/intsig/camscanner/app/DBUtil;->o08O(Ljava/util/List;)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p2

    .line 59
    new-instance v1, Ljava/lang/StringBuilder;

    .line 60
    .line 61
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .line 63
    .line 64
    const-string v2, "file_sync_id in ("

    .line 65
    .line 66
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    const-string p2, ")"

    .line 73
    .line 74
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object p2

    .line 81
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$SyncDeleteStatus;->〇080:Landroid/net/Uri;

    .line 82
    .line 83
    const/4 v2, 0x0

    .line 84
    invoke-virtual {p1, v1, p2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 85
    .line 86
    .line 87
    move-result p1

    .line 88
    new-instance p2, Ljava/lang/StringBuilder;

    .line 89
    .line 90
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    .line 92
    .line 93
    const-string v1, "deleteWebRecycleBinRecord deleteNumber="

    .line 94
    .line 95
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object p1

    .line 105
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    return-void
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private 〇〇0o()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/tsapp/SyncListener;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇80〇808〇O:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-eqz v2, :cond_1

    .line 17
    .line 18
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    check-cast v2, Lcom/intsig/camscanner/tsapp/SyncListener;

    .line 23
    .line 24
    invoke-interface {v2}, Lcom/intsig/camscanner/tsapp/SyncListener;->O8()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    if-nez v3, :cond_0

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    return-object v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static synthetic 〇〇888(Lcom/intsig/camscanner/tsapp/sync/SyncThread;Lorg/json/JSONObject;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o88〇OO08〇(Lorg/json/JSONObject;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇〇8O0〇8(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V
    .locals 3

    .line 1
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇0o()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_2

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    check-cast v1, Lcom/intsig/camscanner/tsapp/SyncListener;

    .line 20
    .line 21
    if-lez p1, :cond_0

    .line 22
    .line 23
    invoke-interface {v1, p1}, Lcom/intsig/camscanner/tsapp/SyncListener;->o〇0(I)V

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇080()Z

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    if-eqz v2, :cond_1

    .line 32
    .line 33
    invoke-interface {v1, p2}, Lcom/intsig/camscanner/tsapp/SyncListener;->oO80(Lcom/intsig/camscanner/tsapp/SyncStatus;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_1
    invoke-interface {v1, p2}, Lcom/intsig/camscanner/tsapp/SyncListener;->〇〇888(Lcom/intsig/camscanner/tsapp/SyncStatus;)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_2
    const/4 v0, -0x1

    .line 42
    if-ne p1, v0, :cond_8

    .line 43
    .line 44
    sget-boolean p1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OoO8:Z

    .line 45
    .line 46
    if-nez p1, :cond_3

    .line 47
    .line 48
    invoke-virtual {p2}, Lcom/intsig/camscanner/tsapp/SyncStatus;->O8()Z

    .line 49
    .line 50
    .line 51
    move-result p1

    .line 52
    if-nez p1, :cond_3

    .line 53
    .line 54
    goto :goto_2

    .line 55
    :cond_3
    invoke-virtual {p2}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇o00〇〇Oo()I

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    const/16 v0, 0x139

    .line 60
    .line 61
    if-eq p1, v0, :cond_7

    .line 62
    .line 63
    invoke-virtual {p2}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇o00〇〇Oo()I

    .line 64
    .line 65
    .line 66
    move-result p1

    .line 67
    const/16 v0, 0x13b

    .line 68
    .line 69
    if-ne p1, v0, :cond_4

    .line 70
    .line 71
    goto :goto_1

    .line 72
    :cond_4
    const/4 p1, 0x1

    .line 73
    if-ne p3, p1, :cond_5

    .line 74
    .line 75
    sget-boolean p1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OoO8:Z

    .line 76
    .line 77
    if-nez p1, :cond_5

    .line 78
    .line 79
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo〇o(Lcom/intsig/camscanner/tsapp/SyncStatus;)V

    .line 80
    .line 81
    .line 82
    goto :goto_2

    .line 83
    :cond_5
    invoke-virtual {p2}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇o00〇〇Oo()I

    .line 84
    .line 85
    .line 86
    move-result p1

    .line 87
    invoke-static {p1}, Lcom/intsig/tianshu/TianShuAPI;->〇00(I)Z

    .line 88
    .line 89
    .line 90
    move-result p1

    .line 91
    if-eqz p1, :cond_6

    .line 92
    .line 93
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo〇o(Lcom/intsig/camscanner/tsapp/SyncStatus;)V

    .line 94
    .line 95
    .line 96
    goto :goto_2

    .line 97
    :cond_6
    const p1, 0x7f0d0743

    .line 98
    .line 99
    .line 100
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o8〇O(I)V

    .line 101
    .line 102
    .line 103
    goto :goto_2

    .line 104
    :cond_7
    :goto_1
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo〇o(Lcom/intsig/camscanner/tsapp/SyncStatus;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    .line 106
    .line 107
    goto :goto_2

    .line 108
    :catch_0
    move-exception p1

    .line 109
    const-string p2, "SyncThread"

    .line 110
    .line 111
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 112
    .line 113
    .line 114
    :cond_8
    :goto_2
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private 〇〇o8(Lcom/intsig/camscanner/tsapp/SyncStatus;I)Z
    .locals 13

    .line 1
    const-string v0, " folderRevision="

    .line 2
    .line 3
    const-string v1, " operation.updateFileNum="

    .line 4
    .line 5
    const-string v2, "queryServerSyncInfo"

    .line 6
    .line 7
    const-string v3, "SyncThread"

    .line 8
    .line 9
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const/4 v2, -0x1

    .line 13
    const/4 v4, 0x0

    .line 14
    const/4 v5, 0x1

    .line 15
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo8Oo00oo()Ljava/util/List;

    .line 16
    .line 17
    .line 18
    move-result-object v6

    .line 19
    new-instance v7, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    const-string v8, "folderStates size="

    .line 25
    .line 26
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-interface {v6}, Ljava/util/List;->size()I

    .line 30
    .line 31
    .line 32
    move-result v8

    .line 33
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v7

    .line 40
    invoke-static {v3, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    invoke-interface {v6}, Ljava/util/List;->size()I

    .line 44
    .line 45
    .line 46
    move-result v7
    :try_end_0
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    const-string v8, "folderState name="

    .line 48
    .line 49
    if-lez v7, :cond_3

    .line 50
    .line 51
    :try_start_1
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 52
    .line 53
    .line 54
    move-result-object v6

    .line 55
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    .line 56
    .line 57
    .line 58
    move-result v7

    .line 59
    if-eqz v7, :cond_4

    .line 60
    .line 61
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 62
    .line 63
    .line 64
    move-result-object v7

    .line 65
    check-cast v7, Lcom/intsig/tianshu/FolderState;

    .line 66
    .line 67
    invoke-virtual {p1, v7}, Lcom/intsig/camscanner/tsapp/SyncStatus;->OO0o〇〇〇〇0(Lcom/intsig/tianshu/FolderState;)V

    .line 68
    .line 69
    .line 70
    sget-object v9, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o800o8O:Ljava/util/List;

    .line 71
    .line 72
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 73
    .line 74
    .line 75
    move-result-object v9

    .line 76
    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    .line 77
    .line 78
    .line 79
    move-result v10

    .line 80
    if-eqz v10, :cond_2

    .line 81
    .line 82
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 83
    .line 84
    .line 85
    move-result-object v10

    .line 86
    check-cast v10, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;

    .line 87
    .line 88
    invoke-virtual {v7}, Lcom/intsig/tianshu/FolderState;->O8()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v11

    .line 92
    iget-object v12, v10, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇080:Ljava/lang/String;

    .line 93
    .line 94
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 95
    .line 96
    .line 97
    move-result v11

    .line 98
    if-eqz v11, :cond_1

    .line 99
    .line 100
    invoke-virtual {v7}, Lcom/intsig/tianshu/FolderState;->Oo08()I

    .line 101
    .line 102
    .line 103
    move-result v9

    .line 104
    iput v9, v10, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o00〇〇Oo:I

    .line 105
    .line 106
    invoke-virtual {v7}, Lcom/intsig/tianshu/FolderState;->〇o00〇〇Oo()I

    .line 107
    .line 108
    .line 109
    move-result v9

    .line 110
    invoke-virtual {v7}, Lcom/intsig/tianshu/FolderState;->〇o〇()I

    .line 111
    .line 112
    .line 113
    move-result v11

    .line 114
    add-int/2addr v9, v11

    .line 115
    iput v9, v10, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o〇:I

    .line 116
    .line 117
    new-instance v9, Ljava/lang/StringBuilder;

    .line 118
    .line 119
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 120
    .line 121
    .line 122
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    invoke-virtual {v7}, Lcom/intsig/tianshu/FolderState;->O8()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v11

    .line 129
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    const-string v11, " server operation vision="

    .line 133
    .line 134
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    iget v11, v10, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o00〇〇Oo:I

    .line 138
    .line 139
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 140
    .line 141
    .line 142
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    iget v10, v10, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o〇:I

    .line 146
    .line 147
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 148
    .line 149
    .line 150
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 151
    .line 152
    .line 153
    move-result-object v9

    .line 154
    invoke-static {v3, v9}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    :cond_2
    const-string v9, "data_check"

    .line 158
    .line 159
    invoke-virtual {v7}, Lcom/intsig/tianshu/FolderState;->O8()Ljava/lang/String;

    .line 160
    .line 161
    .line 162
    move-result-object v10

    .line 163
    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 164
    .line 165
    .line 166
    move-result v9

    .line 167
    if-eqz v9, :cond_0

    .line 168
    .line 169
    invoke-virtual {v7}, Lcom/intsig/tianshu/FolderState;->o〇0()Z

    .line 170
    .line 171
    .line 172
    move-result v7

    .line 173
    if-eqz v7, :cond_0

    .line 174
    .line 175
    const-string v7, "folderStates need check data!"

    .line 176
    .line 177
    invoke-static {v3, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    .line 179
    .line 180
    sget-object v7, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 181
    .line 182
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇()Lcom/intsig/camscanner/tsapp/sync/DataRevertJson;

    .line 183
    .line 184
    .line 185
    move-result-object v9

    .line 186
    invoke-static {v7, v9}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇O80〇oOo(Landroid/content/Context;Lcom/intsig/camscanner/tsapp/sync/DataRevertJson;)V

    .line 187
    .line 188
    .line 189
    goto/16 :goto_0

    .line 190
    .line 191
    :cond_3
    sget-object v6, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o800o8O:Ljava/util/List;

    .line 192
    .line 193
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 194
    .line 195
    .line 196
    move-result-object v6

    .line 197
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    .line 198
    .line 199
    .line 200
    move-result v7

    .line 201
    if-eqz v7, :cond_4

    .line 202
    .line 203
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 204
    .line 205
    .line 206
    move-result-object v7

    .line 207
    check-cast v7, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;

    .line 208
    .line 209
    iput v2, v7, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o00〇〇Oo:I

    .line 210
    .line 211
    iput v4, v7, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o〇:I

    .line 212
    .line 213
    new-instance v9, Ljava/lang/StringBuilder;

    .line 214
    .line 215
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 216
    .line 217
    .line 218
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    .line 220
    .line 221
    iget-object v10, v7, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇080:Ljava/lang/String;

    .line 222
    .line 223
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    .line 225
    .line 226
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    .line 228
    .line 229
    iget v10, v7, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o00〇〇Oo:I

    .line 230
    .line 231
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 232
    .line 233
    .line 234
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    .line 236
    .line 237
    iget v7, v7, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o〇:I

    .line 238
    .line 239
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 240
    .line 241
    .line 242
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 243
    .line 244
    .line 245
    move-result-object v7

    .line 246
    invoke-static {v3, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    .line 248
    .line 249
    goto :goto_1

    .line 250
    :cond_4
    sget-object v6, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 251
    .line 252
    invoke-static {v6}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oO8o(Landroid/content/Context;)Z

    .line 253
    .line 254
    .line 255
    move-result v6

    .line 256
    if-eqz v6, :cond_9

    .line 257
    .line 258
    const-string v6, "folderStates isNeedUploadRevertData !"

    .line 259
    .line 260
    invoke-static {v3, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    .line 262
    .line 263
    sget-object v6, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 264
    .line 265
    invoke-static {v6}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo08(Landroid/content/Context;)V
    :try_end_1
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/ConcurrentModificationException; {:try_start_1 .. :try_end_1} :catch_0

    .line 266
    .line 267
    .line 268
    goto/16 :goto_4

    .line 269
    .line 270
    :catch_0
    move-exception p1

    .line 271
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 272
    .line 273
    .line 274
    goto/16 :goto_4

    .line 275
    .line 276
    :catch_1
    move-exception v6

    .line 277
    invoke-static {v3, v6}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 278
    .line 279
    .line 280
    invoke-virtual {v6}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 281
    .line 282
    .line 283
    move-result v7

    .line 284
    const/16 v8, -0x190

    .line 285
    .line 286
    if-ne v7, v8, :cond_5

    .line 287
    .line 288
    invoke-virtual {p1, v8}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 289
    .line 290
    .line 291
    invoke-virtual {p0, v2, p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 292
    .line 293
    .line 294
    goto :goto_3

    .line 295
    :cond_5
    invoke-virtual {v6}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 296
    .line 297
    .line 298
    move-result v7

    .line 299
    const/16 v8, -0x130

    .line 300
    .line 301
    if-ne v7, v8, :cond_6

    .line 302
    .line 303
    invoke-virtual {p1, v8}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 304
    .line 305
    .line 306
    invoke-virtual {p0, v2, p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 307
    .line 308
    .line 309
    goto :goto_3

    .line 310
    :cond_6
    invoke-virtual {v6}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 311
    .line 312
    .line 313
    move-result p2

    .line 314
    const/16 v7, 0x12e

    .line 315
    .line 316
    if-ne p2, v7, :cond_8

    .line 317
    .line 318
    sget-object p1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o800o8O:Ljava/util/List;

    .line 319
    .line 320
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 321
    .line 322
    .line 323
    move-result-object p1

    .line 324
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 325
    .line 326
    .line 327
    move-result p2

    .line 328
    if-eqz p2, :cond_7

    .line 329
    .line 330
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 331
    .line 332
    .line 333
    move-result-object p2

    .line 334
    check-cast p2, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;

    .line 335
    .line 336
    iput v2, p2, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o00〇〇Oo:I

    .line 337
    .line 338
    iput v4, p2, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o〇:I

    .line 339
    .line 340
    new-instance v6, Ljava/lang/StringBuilder;

    .line 341
    .line 342
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 343
    .line 344
    .line 345
    const-string v7, "queryServerSyncInfo TianShuException folderState name="

    .line 346
    .line 347
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 348
    .line 349
    .line 350
    iget-object v7, p2, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇080:Ljava/lang/String;

    .line 351
    .line 352
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353
    .line 354
    .line 355
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    .line 357
    .line 358
    iget v7, p2, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o00〇〇Oo:I

    .line 359
    .line 360
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 361
    .line 362
    .line 363
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 364
    .line 365
    .line 366
    iget p2, p2, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇o〇:I

    .line 367
    .line 368
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 369
    .line 370
    .line 371
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 372
    .line 373
    .line 374
    move-result-object p2

    .line 375
    invoke-static {v3, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    .line 377
    .line 378
    goto :goto_2

    .line 379
    :cond_7
    const/4 v4, 0x1

    .line 380
    goto :goto_3

    .line 381
    :cond_8
    invoke-virtual {v6}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 382
    .line 383
    .line 384
    move-result p2

    .line 385
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 386
    .line 387
    .line 388
    :goto_3
    move v5, v4

    .line 389
    :cond_9
    :goto_4
    return v5
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private 〇〇〇0〇〇0(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O8O〇88oO0(Landroid/content/Context;)J

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    sget-object v2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 8
    .line 9
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    sget-object v4, Lcom/intsig/camscanner/provider/Documents$SyncRestore;->〇080:Landroid/net/Uri;

    .line 14
    .line 15
    const-string v2, "sync_time"

    .line 16
    .line 17
    const-string v5, "_id"

    .line 18
    .line 19
    const-string v6, "doc_version"

    .line 20
    .line 21
    const-string v7, "tag_version"

    .line 22
    .line 23
    const-string v8, "page_version"

    .line 24
    .line 25
    filled-new-array {v6, v7, v8, v2, v5}, [Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v5

    .line 29
    new-instance v2, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v6, "sync_account_id = "

    .line 35
    .line 36
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v6

    .line 46
    const/4 v7, 0x0

    .line 47
    const/4 v8, 0x0

    .line 48
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    if-eqz v0, :cond_1

    .line 53
    .line 54
    new-instance v1, Ljava/util/ArrayList;

    .line 55
    .line 56
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 57
    .line 58
    .line 59
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    if-eqz v2, :cond_0

    .line 64
    .line 65
    new-instance v2, Lcom/intsig/camscanner/tsapp/sync/RestorePointJson;

    .line 66
    .line 67
    invoke-direct {v2}, Lcom/intsig/camscanner/tsapp/sync/RestorePointJson;-><init>()V

    .line 68
    .line 69
    .line 70
    const/4 v3, 0x0

    .line 71
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    .line 72
    .line 73
    .line 74
    move-result-wide v3

    .line 75
    invoke-virtual {v2, v3, v4}, Lcom/intsig/camscanner/tsapp/sync/RestorePointJson;->〇o00〇〇Oo(J)V

    .line 76
    .line 77
    .line 78
    const/4 v3, 0x1

    .line 79
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    .line 80
    .line 81
    .line 82
    move-result-wide v3

    .line 83
    invoke-virtual {v2, v3, v4}, Lcom/intsig/camscanner/tsapp/sync/RestorePointJson;->O8(J)V

    .line 84
    .line 85
    .line 86
    const/4 v3, 0x2

    .line 87
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    .line 88
    .line 89
    .line 90
    move-result-wide v3

    .line 91
    invoke-virtual {v2, v3, v4}, Lcom/intsig/camscanner/tsapp/sync/RestorePointJson;->〇o〇(J)V

    .line 92
    .line 93
    .line 94
    const/4 v3, 0x3

    .line 95
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    .line 96
    .line 97
    .line 98
    move-result-wide v3

    .line 99
    const-wide/16 v5, 0x3e8

    .line 100
    .line 101
    div-long/2addr v3, v5

    .line 102
    invoke-virtual {v2, v3, v4}, Lcom/intsig/camscanner/tsapp/sync/RestorePointJson;->Oo08(J)V

    .line 103
    .line 104
    .line 105
    invoke-virtual {v2}, Lcom/intsig/camscanner/tsapp/sync/RestorePointJson;->〇080()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v2

    .line 109
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    .line 111
    .line 112
    const/4 v2, 0x4

    .line 113
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    .line 114
    .line 115
    .line 116
    move-result-wide v2

    .line 117
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 118
    .line 119
    .line 120
    move-result-object v2

    .line 121
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    .line 123
    .line 124
    goto :goto_0

    .line 125
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 126
    .line 127
    .line 128
    goto :goto_1

    .line 129
    :cond_1
    const/4 v1, 0x0

    .line 130
    :goto_1
    return-object v1
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method


# virtual methods
.method O0(Lcom/intsig/camscanner/tsapp/SyncListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇80〇808〇O:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public O08000(Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;)Z
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oooo8o0〇:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oooo8o0〇:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    return p1

    .line 19
    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 20
    return p1
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public O0O8OO088()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oooo8o0〇:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;

    .line 18
    .line 19
    invoke-interface {v1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;->〇080()V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public O8(Z)Z
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O〇80o08O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    if-eqz p1, :cond_1

    .line 8
    .line 9
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->ooO〇00O()V

    .line 10
    .line 11
    .line 12
    new-instance v1, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v2, "exitSync result="

    .line 18
    .line 19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const-string v0, " forceStop="

    .line 26
    .line 27
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    const-string v0, "SyncThread"

    .line 38
    .line 39
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    const/4 v0, 0x1

    .line 43
    :cond_1
    return v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public OO8oO0o〇()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oO80:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OOo0O()V
    .locals 3

    .line 1
    const-string v0, "SyncThread"

    .line 2
    .line 3
    const-string v1, "stopSync SyncThread"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    sput-boolean v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OoO8:Z

    .line 10
    .line 11
    new-instance v1, Lcom/intsig/camscanner/eventbus/SyncEvent;

    .line 12
    .line 13
    invoke-direct {v1}, Lcom/intsig/camscanner/eventbus/SyncEvent;-><init>()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/eventbus/SyncEvent;->〇o〇(Z)Lcom/intsig/camscanner/eventbus/SyncEvent;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v1}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0:Landroid/os/HandlerThread;

    .line 24
    .line 25
    if-eqz v1, :cond_0

    .line 26
    .line 27
    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    .line 28
    .line 29
    .line 30
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OoO8()V

    .line 31
    .line 32
    .line 33
    const/4 v1, 0x0

    .line 34
    sput-object v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oo88o8O:Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 35
    .line 36
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->o〇OOo000(Z)V

    .line 37
    .line 38
    .line 39
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇0o()Ljava/util/List;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    if-eqz v1, :cond_1

    .line 52
    .line 53
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    check-cast v1, Lcom/intsig/camscanner/tsapp/SyncListener;

    .line 58
    .line 59
    new-instance v2, Lcom/intsig/camscanner/tsapp/SyncStatus;

    .line 60
    .line 61
    invoke-direct {v2}, Lcom/intsig/camscanner/tsapp/SyncStatus;-><init>()V

    .line 62
    .line 63
    .line 64
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/tsapp/SyncListener;->oO80(Lcom/intsig/camscanner/tsapp/SyncStatus;)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_1
    const v0, 0x7f0d0743

    .line 69
    .line 70
    .line 71
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o8〇O(I)V

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method Oo(Lcom/intsig/camscanner/tsapp/SyncListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇80〇808〇O:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public Oo08(J)V
    .locals 5

    .line 1
    new-instance v0, Landroid/content/ContentValues;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const-string v2, "sync_state"

    .line 12
    .line 13
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 14
    .line 15
    .line 16
    const-wide/16 v1, 0x0

    .line 17
    .line 18
    const-string v3, "sync_state = 6"

    .line 19
    .line 20
    cmp-long v4, p1, v1

    .line 21
    .line 22
    if-lez v4, :cond_0

    .line 23
    .line 24
    new-instance v1, Ljava/lang/StringBuilder;

    .line 25
    .line 26
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 27
    .line 28
    .line 29
    const-string v2, "document_id = "

    .line 30
    .line 31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    const-string p1, " AND "

    .line 38
    .line 39
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v3

    .line 49
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 50
    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    sget-object p2, Lcom/intsig/camscanner/provider/Documents$Image;->Oo08:Landroid/net/Uri;

    .line 56
    .line 57
    const/4 v1, 0x0

    .line 58
    invoke-virtual {p1, p2, v0, v3, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 59
    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public Oo〇O(Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oooo8o0〇:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oooo8o0〇:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    :cond_1
    :goto_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method O〇0(Lcom/intsig/camscanner/tsapp/SyncCallbackListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇〇〇0:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public O〇O〇oO(Lcom/intsig/camscanner/tsapp/SyncStatus;FILcom/intsig/camscanner/tsapp/sync/SyncProgressValue;)Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;
    .locals 10

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇80〇808〇O(I)V

    .line 3
    .line 4
    .line 5
    const/high16 v1, 0x42c80000    # 100.0f

    .line 6
    .line 7
    div-float v6, p2, v1

    .line 8
    .line 9
    const/4 p2, 0x1

    .line 10
    new-array v5, p2, [F

    .line 11
    .line 12
    new-array v4, p2, [F

    .line 13
    .line 14
    const/4 p2, 0x0

    .line 15
    aput p2, v5, v0

    .line 16
    .line 17
    new-instance p2, Lcom/intsig/camscanner/tsapp/sync/SyncThread$2;

    .line 18
    .line 19
    move-object v2, p2

    .line 20
    move-object v3, p0

    .line 21
    move-object v7, p4

    .line 22
    move-object v8, p1

    .line 23
    move v9, p3

    .line 24
    invoke-direct/range {v2 .. v9}, Lcom/intsig/camscanner/tsapp/sync/SyncThread$2;-><init>(Lcom/intsig/camscanner/tsapp/sync/SyncThread;[F[FFLcom/intsig/camscanner/tsapp/sync/SyncProgressValue;Lcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 25
    .line 26
    .line 27
    return-object p2
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method o8O〇(Lcom/intsig/camscanner/tsapp/SyncCallbackListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇〇〇0:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method oo(Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncFinishedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O8o08O:Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncFinishedListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public ooOO(Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    sget-object v0, Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient;->〇O8o08O:Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient$Companion;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient$Companion;->O8()Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient;->o8oO〇(Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;)V

    .line 24
    .line 25
    .line 26
    :cond_1
    :goto_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public o〇0(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O〇:[B

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇o()F

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    if-eqz p2, :cond_0

    .line 9
    .line 10
    const/high16 v2, 0x42c80000    # 100.0f

    .line 11
    .line 12
    cmpg-float v2, v1, v2

    .line 13
    .line 14
    if-gtz v2, :cond_0

    .line 15
    .line 16
    invoke-virtual {p2, v1}, Lcom/intsig/camscanner/tsapp/SyncStatus;->〇8o8o〇(F)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇8O0〇8(ILcom/intsig/camscanner/tsapp/SyncStatus;I)V

    .line 20
    .line 21
    .line 22
    :cond_0
    monitor-exit v0

    .line 23
    return-void

    .line 24
    :catchall_0
    move-exception p1

    .line 25
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 26
    throw p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public o〇0OOo〇0()Ljava/util/Vector;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/intsig/camscanner/tsapp/SyncCallbackListener;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/Vector;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO0o〇〇〇〇0:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-eqz v2, :cond_1

    .line 17
    .line 18
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    check-cast v2, Lcom/intsig/camscanner/tsapp/SyncCallbackListener;

    .line 23
    .line 24
    invoke-interface {v2}, Lcom/intsig/camscanner/tsapp/SyncCallbackListener;->O8()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    if-nez v3, :cond_0

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    invoke-virtual {v0, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    return-object v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method o〇O8〇〇o(Landroid/os/Message;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo08:Landroid/os/Handler;

    .line 2
    .line 3
    const/16 v1, 0x64

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo08:Landroid/os/Handler;

    .line 9
    .line 10
    const/16 v2, 0x65

    .line 11
    .line 12
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo08:Landroid/os/Handler;

    .line 16
    .line 17
    const/16 v2, 0x66

    .line 18
    .line 19
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 20
    .line 21
    .line 22
    iget v0, p1, Landroid/os/Message;->what:I

    .line 23
    .line 24
    if-ne v0, v1, :cond_0

    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo08:Landroid/os/Handler;

    .line 27
    .line 28
    const-wide/16 v1, 0x3e8

    .line 29
    .line 30
    invoke-virtual {v0, p1, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    if-ne v0, v2, :cond_1

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo08:Landroid/os/Handler;

    .line 37
    .line 38
    const-wide/16 v1, 0xbb8

    .line 39
    .line 40
    invoke-virtual {v0, p1, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo08:Landroid/os/Handler;

    .line 45
    .line 46
    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 47
    .line 48
    .line 49
    :goto_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇00〇8()J
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->Oo08:Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;->Oo08()Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->o〇0()J

    .line 8
    .line 9
    .line 10
    move-result-wide v0

    .line 11
    return-wide v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080(Lcom/intsig/camscanner/tsapp/SyncStatus;IJFLjava/lang/String;Ljava/lang/String;Z[ZLcom/intsig/camscanner/tsapp/sync/SyncProgressValue;I)Ljava/util/concurrent/Future;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/tsapp/SyncStatus;",
            "IJF",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z[Z",
            "Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;",
            "I)",
            "Ljava/util/concurrent/Future<",
            "Lcom/intsig/camscanner/tsapp/sync/BaseUploadResponse;",
            ">;"
        }
    .end annotation

    .line 1
    move-wide v9, p3

    .line 2
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 3
    .line 4
    invoke-static {v0, v9, v10}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->Oooo8o0〇(Landroid/content/Context;J)Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

    .line 5
    .line 6
    .line 7
    move-result-object v7

    .line 8
    move-object v11, p0

    .line 9
    move-object v0, p1

    .line 10
    move v1, p2

    .line 11
    move/from16 v2, p5

    .line 12
    .line 13
    move-object/from16 v3, p10

    .line 14
    .line 15
    invoke-virtual {p0, p1, v2, p2, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O〇O〇oO(Lcom/intsig/camscanner/tsapp/SyncStatus;FILcom/intsig/camscanner/tsapp/sync/SyncProgressValue;)Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 20
    .line 21
    const/4 v1, 0x2

    .line 22
    invoke-static {v0, v9, v10, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO88〇OOO(Landroid/content/Context;JI)V

    .line 23
    .line 24
    .line 25
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_0

    .line 30
    .line 31
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 32
    .line 33
    const/4 v4, 0x0

    .line 34
    const/4 v5, 0x0

    .line 35
    const/4 v6, 0x0

    .line 36
    move-wide v1, p3

    .line 37
    move/from16 v8, p11

    .line 38
    .line 39
    invoke-static/range {v0 .. v8}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇oO(Landroid/content/Context;JLcom/intsig/tianshu/sync/SyncApi$SyncProgress;ZZLjava/lang/String;Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;I)Ljava/util/concurrent/Future;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    goto :goto_0

    .line 44
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 45
    .line 46
    move-wide v1, p3

    .line 47
    move-object/from16 v4, p6

    .line 48
    .line 49
    move-object/from16 v5, p7

    .line 50
    .line 51
    move/from16 v6, p8

    .line 52
    .line 53
    move/from16 v8, p11

    .line 54
    .line 55
    invoke-static/range {v0 .. v8}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oo8ooo8O(Landroid/content/Context;JLcom/intsig/tianshu/sync/SyncApi$SyncProgress;Ljava/lang/String;Ljava/lang/String;ZLcom/intsig/camscanner/sharedir/data/ShareDirDBData;I)Ljava/util/concurrent/Future;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    :goto_0
    sget-object v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 60
    .line 61
    invoke-static {v1, v9, v10}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇o8OO0(Landroid/content/Context;J)Z

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    if-eqz v1, :cond_1

    .line 66
    .line 67
    goto :goto_1

    .line 68
    :cond_1
    sget-object v1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 69
    .line 70
    const/4 v2, 0x1

    .line 71
    invoke-static {v1, v9, v10, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO88〇OOO(Landroid/content/Context;JI)V

    .line 72
    .line 73
    .line 74
    new-instance v1, Ljava/lang/StringBuilder;

    .line 75
    .line 76
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .line 78
    .line 79
    const-string v2, "uploadLocalImages2Server isDocImageJpgComplete false "

    .line 80
    .line 81
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    const-string v2, "SyncThread"

    .line 92
    .line 93
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    :goto_1
    return-object v0
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

.method public 〇08O8o〇0(Ljava/lang/String;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇〇o8(Landroid/content/Context;)Ljava/util/Set;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    new-instance v1, Ljava/util/ArrayList;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 10
    .line 11
    .line 12
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    const-string v3, "-1"

    .line 17
    .line 18
    const-string v4, "0"

    .line 19
    .line 20
    const/4 v5, 0x1

    .line 21
    const-string v6, "1"

    .line 22
    .line 23
    const/4 v7, 0x0

    .line 24
    if-eqz v2, :cond_0

    .line 25
    .line 26
    filled-new-array {v6, v4, v3, v6}, [Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    const-string v3, "folder_type !=? AND sync_state !=? AND sync_state !=? AND belong_state !=? AND team_token IS NULL "

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    const/4 v2, 0x5

    .line 34
    new-array v2, v2, [Ljava/lang/String;

    .line 35
    .line 36
    aput-object v6, v2, v7

    .line 37
    .line 38
    aput-object v4, v2, v5

    .line 39
    .line 40
    const/4 v4, 0x2

    .line 41
    aput-object v3, v2, v4

    .line 42
    .line 43
    const/4 v3, 0x3

    .line 44
    const-string v4, "5"

    .line 45
    .line 46
    aput-object v4, v2, v3

    .line 47
    .line 48
    const/4 v3, 0x4

    .line 49
    aput-object p1, v2, v3

    .line 50
    .line 51
    const-string v3, "folder_type !=? AND sync_state !=? AND sync_state !=? AND sync_state !=? AND team_token =? "

    .line 52
    .line 53
    :goto_0
    move-object v12, v2

    .line 54
    move-object v11, v3

    .line 55
    sget-object v2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 56
    .line 57
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 58
    .line 59
    .line 60
    move-result-object v8

    .line 61
    sget-object v9, Lcom/intsig/camscanner/provider/Documents$Document;->O8:Landroid/net/Uri;

    .line 62
    .line 63
    const-string v2, "sync_state"

    .line 64
    .line 65
    const-string v3, "sync_doc_id"

    .line 66
    .line 67
    const-string v4, "_id"

    .line 68
    .line 69
    const-string v6, "title"

    .line 70
    .line 71
    filled-new-array {v4, v6, v2, v3}, [Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v10

    .line 75
    const/4 v13, 0x0

    .line 76
    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    const-string v3, ""

    .line 81
    .line 82
    move-object v6, v3

    .line 83
    if-eqz v2, :cond_3

    .line 84
    .line 85
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 86
    .line 87
    .line 88
    move-result v8

    .line 89
    if-eqz v8, :cond_2

    .line 90
    .line 91
    invoke-interface {v2, v7}, Landroid/database/Cursor;->getLong(I)J

    .line 92
    .line 93
    .line 94
    move-result-wide v8

    .line 95
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 96
    .line 97
    .line 98
    move-result-object v10

    .line 99
    invoke-interface {v0, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    .line 100
    .line 101
    .line 102
    move-result v10

    .line 103
    if-eqz v10, :cond_1

    .line 104
    .line 105
    goto :goto_1

    .line 106
    :cond_1
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 107
    .line 108
    .line 109
    move-result-object v10

    .line 110
    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    .line 112
    .line 113
    new-instance v10, Ljava/lang/StringBuilder;

    .line 114
    .line 115
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    .line 117
    .line 118
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    const-string v6, ","

    .line 125
    .line 126
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object v6

    .line 133
    goto :goto_1

    .line 134
    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 135
    .line 136
    .line 137
    :cond_3
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 138
    .line 139
    .line 140
    move-result v2

    .line 141
    if-nez v2, :cond_4

    .line 142
    .line 143
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    .line 144
    .line 145
    .line 146
    move-result v2

    .line 147
    sub-int/2addr v2, v5

    .line 148
    invoke-virtual {v6, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v2

    .line 152
    new-instance v3, Ljava/lang/StringBuilder;

    .line 153
    .line 154
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 155
    .line 156
    .line 157
    const-string v6, " AND document_id not in ("

    .line 158
    .line 159
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    .line 161
    .line 162
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    .line 164
    .line 165
    const-string v2, ")"

    .line 166
    .line 167
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    .line 169
    .line 170
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 171
    .line 172
    .line 173
    move-result-object v3

    .line 174
    :cond_4
    new-instance v2, Ljava/util/HashSet;

    .line 175
    .line 176
    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 177
    .line 178
    .line 179
    sget-object v6, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 180
    .line 181
    invoke-static {v6, p1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->Ooo(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashSet;)V

    .line 182
    .line 183
    .line 184
    new-instance v6, Ljava/lang/StringBuilder;

    .line 185
    .line 186
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 187
    .line 188
    .line 189
    const-string v8, "sync_jpage_state<>0 AND sync_jpage_state<>-1 AND sync_jpage_state<>5 AND belong_state != 1"

    .line 190
    .line 191
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    .line 193
    .line 194
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    .line 196
    .line 197
    const-string v3, ") group by (document_id"

    .line 198
    .line 199
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    .line 201
    .line 202
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 203
    .line 204
    .line 205
    move-result-object v11

    .line 206
    sget-object v3, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 207
    .line 208
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 209
    .line 210
    .line 211
    move-result-object v8

    .line 212
    sget-object v9, Lcom/intsig/camscanner/provider/Documents$Image;->〇o〇:Landroid/net/Uri;

    .line 213
    .line 214
    const-string v3, "document_id"

    .line 215
    .line 216
    const-string v6, "folder_type"

    .line 217
    .line 218
    filled-new-array {v3, v6}, [Ljava/lang/String;

    .line 219
    .line 220
    .line 221
    move-result-object v10

    .line 222
    const/4 v12, 0x0

    .line 223
    const-string v13, "document_id ASC"

    .line 224
    .line 225
    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 226
    .line 227
    .line 228
    move-result-object v3

    .line 229
    if-eqz v3, :cond_8

    .line 230
    .line 231
    :cond_5
    :goto_2
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    .line 232
    .line 233
    .line 234
    move-result v6

    .line 235
    if-eqz v6, :cond_7

    .line 236
    .line 237
    invoke-interface {v3, v7}, Landroid/database/Cursor;->getLong(I)J

    .line 238
    .line 239
    .line 240
    move-result-wide v8

    .line 241
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 242
    .line 243
    .line 244
    move-result-object v6

    .line 245
    invoke-interface {v0, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    .line 246
    .line 247
    .line 248
    move-result v6

    .line 249
    if-eqz v6, :cond_6

    .line 250
    .line 251
    goto :goto_2

    .line 252
    :cond_6
    invoke-interface {v3, v7}, Landroid/database/Cursor;->getLong(I)J

    .line 253
    .line 254
    .line 255
    move-result-wide v8

    .line 256
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 257
    .line 258
    .line 259
    move-result-object v6

    .line 260
    invoke-virtual {v2, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    .line 261
    .line 262
    .line 263
    move-result v6

    .line 264
    if-eqz v6, :cond_5

    .line 265
    .line 266
    invoke-interface {v3, v5}, Landroid/database/Cursor;->getInt(I)I

    .line 267
    .line 268
    .line 269
    move-result v6

    .line 270
    invoke-static {v6}, Lcom/intsig/camscanner/business/folders/OfflineFolder;->OO0o〇〇(I)Z

    .line 271
    .line 272
    .line 273
    move-result v6

    .line 274
    if-nez v6, :cond_5

    .line 275
    .line 276
    invoke-interface {v3, v7}, Landroid/database/Cursor;->getLong(I)J

    .line 277
    .line 278
    .line 279
    move-result-wide v8

    .line 280
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 281
    .line 282
    .line 283
    move-result-object v6

    .line 284
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    .line 286
    .line 287
    goto :goto_2

    .line 288
    :cond_7
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 289
    .line 290
    .line 291
    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    .line 292
    .line 293
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 294
    .line 295
    .line 296
    const-string v3, "getUploadedDocIds end teamToken="

    .line 297
    .line 298
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    .line 300
    .line 301
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    .line 303
    .line 304
    const-string p1, " docSize="

    .line 305
    .line 306
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    .line 308
    .line 309
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 310
    .line 311
    .line 312
    move-result p1

    .line 313
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 314
    .line 315
    .line 316
    const-string p1, " docIds="

    .line 317
    .line 318
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    .line 320
    .line 321
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 322
    .line 323
    .line 324
    const-string p1, " unFinishProcessDocSet="

    .line 325
    .line 326
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    .line 328
    .line 329
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 330
    .line 331
    .line 332
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 333
    .line 334
    .line 335
    move-result-object p1

    .line 336
    const-string v0, "SyncThread"

    .line 337
    .line 338
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    .line 340
    .line 341
    const-string v11, "office_file_sync_state<>0 AND office_file_sync_state<>5 AND office_file_sync_state<>-1 AND office_file_path<> NULL"

    .line 342
    .line 343
    sget-object p1, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 344
    .line 345
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 346
    .line 347
    .line 348
    move-result-object v8

    .line 349
    sget-object v9, Lcom/intsig/camscanner/provider/Documents$Document;->O8:Landroid/net/Uri;

    .line 350
    .line 351
    filled-new-array {v4}, [Ljava/lang/String;

    .line 352
    .line 353
    .line 354
    move-result-object v10

    .line 355
    const/4 v12, 0x0

    .line 356
    const/4 v13, 0x0

    .line 357
    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 358
    .line 359
    .line 360
    move-result-object p1

    .line 361
    if-eqz p1, :cond_b

    .line 362
    .line 363
    :cond_9
    :goto_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 364
    .line 365
    .line 366
    move-result v0

    .line 367
    if-eqz v0, :cond_a

    .line 368
    .line 369
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getLong(I)J

    .line 370
    .line 371
    .line 372
    move-result-wide v2

    .line 373
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 374
    .line 375
    .line 376
    move-result-object v0

    .line 377
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    .line 378
    .line 379
    .line 380
    move-result v0

    .line 381
    if-nez v0, :cond_9

    .line 382
    .line 383
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 384
    .line 385
    .line 386
    move-result-object v0

    .line 387
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 388
    .line 389
    .line 390
    goto :goto_3

    .line 391
    :cond_a
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 392
    .line 393
    .line 394
    :cond_b
    return-object v1
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method public 〇8〇0〇o〇O(Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;)Z
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    return p1

    .line 19
    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 20
    return p1
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇O00(Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇0〇O0088o:Ljava/util/List;

    .line 5
    .line 6
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 7
    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient;->〇O8o08O:Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient$Companion;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient$Companion;->O8()Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient;->OO0o〇〇(Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇o()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00:Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇o〇()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇808〇:Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/camscanner/tsapp/sync/team/TeamSync;->o800o8O()F

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    add-float/2addr v0, v1

    .line 14
    iget v1, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇8O0〇8:F

    .line 15
    .line 16
    add-float/2addr v0, v1

    .line 17
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo(Ljava/lang/String;)V
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 2
    .line 3
    const-string v1, "SyncThread"

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const-string p1, "mContext == null"

    .line 8
    .line 9
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    const/4 v2, 0x1

    .line 14
    invoke-static {v0, p1, v2}, Lcom/intsig/camscanner/app/DBUtil;->O08000(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_1

    .line 23
    .line 24
    return-void

    .line 25
    :cond_1
    sget-object v2, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇:Landroid/content/Context;

    .line 26
    .line 27
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-direct {p0, v2, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇oo〇(Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-direct {p0, v2, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇8(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    invoke-direct {p0, v2, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oO(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    new-instance v2, Ljava/lang/StringBuilder;

    .line 43
    .line 44
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    .line 46
    .line 47
    const-string v4, "deleteLocalPagesAndDocs deleteDocNum ="

    .line 48
    .line 49
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    const-string v0, " deletePageNum="

    .line 56
    .line 57
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    const-string v0, " teamToken="

    .line 64
    .line 65
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇o〇()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector<",
            "Lcom/intsig/camscanner/tsapp/SyncCallbackListener;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0OOo〇0()Ljava/util/Vector;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇808〇(Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oooo8o0〇:Ljava/util/List;

    .line 5
    .line 6
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson$TeamInfoJson;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "TeamListJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TeamInfoJson"
.end annotation


# instance fields
.field public area:I

.field public content:Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson$Content;

.field public create_time:J

.field public expire:J

.field public m_user_id:Ljava/lang/String;

.field public num:I

.field public team_token:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public upload_time:J


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public contentChange(II)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson$TeamInfoJson;->content:Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson$Content;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    iget v2, v0, Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson$Content;->review:I

    .line 8
    .line 9
    if-ne p1, v2, :cond_2

    .line 10
    .line 11
    iget p1, v0, Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson$Content;->top_doc:I

    .line 12
    .line 13
    if-eq p2, p1, :cond_1

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_1
    return v1

    .line 17
    :cond_2
    :goto_0
    const/4 p1, 0x1

    .line 18
    return p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public isForeign()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamListJson$TeamInfoJson;->area:I

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

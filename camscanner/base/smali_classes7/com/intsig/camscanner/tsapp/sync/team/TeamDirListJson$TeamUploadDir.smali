.class public Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$TeamUploadDir;
.super Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDir;
.source "TeamDirListJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TeamUploadDir"
.end annotation


# instance fields
.field public dirs:[Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$TeamUploadDir;

.field public docs:[Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;

.field private mSyncState:I

.field private mTopModifyDataLastUploadTime:J

.field private mTopModifyDataSyncId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDir;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 3
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDir;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDir;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public getSyncState()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$TeamUploadDir;->mSyncState:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTopModifyDataLastUploadTime()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$TeamUploadDir;->mTopModifyDataLastUploadTime:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTopModifyDataSyncId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$TeamUploadDir;->mTopModifyDataSyncId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setSyncState(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$TeamUploadDir;->mSyncState:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTopModifyDataId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$TeamUploadDir;->mTopModifyDataSyncId:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTopModifyDataLastUploadTime(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$TeamUploadDir;->mTopModifyDataLastUploadTime:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public final Lcom/intsig/camscanner/tsapp/sync/DeviceInfoResponse;
.super Ljava/lang/Object;
.source "DeviceInfoResponse.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final data:Lcom/intsig/camscanner/tsapp/sync/DeviceInfoResponseData;

.field private final err:Ljava/lang/String;

.field private final ret:I


# direct methods
.method public constructor <init>(ILcom/intsig/camscanner/tsapp/sync/DeviceInfoResponseData;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/intsig/camscanner/tsapp/sync/DeviceInfoResponse;->ret:I

    .line 3
    iput-object p2, p0, Lcom/intsig/camscanner/tsapp/sync/DeviceInfoResponse;->data:Lcom/intsig/camscanner/tsapp/sync/DeviceInfoResponseData;

    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/tsapp/sync/DeviceInfoResponse;->err:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/intsig/camscanner/tsapp/sync/DeviceInfoResponseData;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x1

    if-eqz p4, :cond_0

    const/4 p1, 0x0

    .line 5
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/tsapp/sync/DeviceInfoResponse;-><init>(ILcom/intsig/camscanner/tsapp/sync/DeviceInfoResponseData;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getData()Lcom/intsig/camscanner/tsapp/sync/DeviceInfoResponseData;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/DeviceInfoResponse;->data:Lcom/intsig/camscanner/tsapp/sync/DeviceInfoResponseData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getErr()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/DeviceInfoResponse;->err:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getRet()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/tsapp/sync/DeviceInfoResponse;->ret:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

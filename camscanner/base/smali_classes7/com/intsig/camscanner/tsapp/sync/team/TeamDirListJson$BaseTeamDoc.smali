.class public Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "TeamDirListJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BaseTeamDoc"
.end annotation


# instance fields
.field public doc_id:Ljava/lang/String;

.field private mDirId:Ljava/lang/String;

.field private mDocId:J

.field private mLastDirUploadTime:J

.field private mLastUploadTime:J

.field private mSyncState:I

.field private mTopModifyDataLastUploadTime:J

.field private mTopModifyDataSyncId:Ljava/lang/String;

.field public permission:I

.field public upload_time:J

.field public user_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 3
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public getDirId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;->mDirId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDocId()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;->mDocId:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLastDirUploadTime()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;->mLastDirUploadTime:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLastDocUploadTime()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;->mLastUploadTime:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSyncState()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;->mSyncState:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTopModifyDataLastUploadTime()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;->mTopModifyDataLastUploadTime:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTopModifyDataSyncId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;->mTopModifyDataSyncId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setDirId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;->mDirId:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDocId(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;->mDocId:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLastDirUploadTime(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;->mLastDirUploadTime:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLastDocUploadTime(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;->mLastUploadTime:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSyncState(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;->mSyncState:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTopModifyDataId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;->mTopModifyDataSyncId:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTopModifyDataLastUploadTime(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;->mTopModifyDataLastUploadTime:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class final Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl;
.super Ljava/lang/Object;
.source "OfficeDocSyncManager.kt"

# interfaces
.implements Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncListener$DownloadListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DownloadListenerImpl"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final o0:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncListener$DownloadListener;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Landroid/os/Handler;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/CopyOnWriteArrayList;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/CopyOnWriteArrayList;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncListener$DownloadListener;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "mDownloadListenerList"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl;->o0:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 10
    .line 11
    new-instance p1, Landroid/os/Handler;

    .line 12
    .line 13
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 18
    .line 19
    .line 20
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl;->〇OOo8〇0:Landroid/os/Handler;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl;->〇o〇(Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl;Lkotlin/jvm/functions/Function1;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final 〇o00〇〇Oo(Lkotlin/jvm/functions/Function1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncListener$DownloadListener;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl;->o0:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    if-eqz v1, :cond_1

    .line 26
    .line 27
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    check-cast v1, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncListener$DownloadListener;

    .line 32
    .line 33
    const-string v2, "it"

    .line 34
    .line 35
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl;->〇OOo8〇0:Landroid/os/Handler;

    .line 43
    .line 44
    new-instance v1, Lcom/intsig/camscanner/tsapp/sync/office/〇o00〇〇Oo;

    .line 45
    .line 46
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/tsapp/sync/office/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl;Lkotlin/jvm/functions/Function1;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 50
    .line 51
    .line 52
    :cond_1
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private static final 〇o〇(Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl;Lkotlin/jvm/functions/Function1;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$callback"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object p0, p0, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl;->o0:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 12
    .line 13
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    check-cast v0, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncListener$DownloadListener;

    .line 28
    .line 29
    const-string v1, "it"

    .line 30
    .line 31
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public O8O〇88oO0(JF)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl$onDownloadProgress$1;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2, p3}, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl$onDownloadProgress$1;-><init>(JF)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function1;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public o8〇OO0〇0o(J)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl$onDownloadSuccess$1;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2}, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl$onDownloadSuccess$1;-><init>(J)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function1;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public oo(JI)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl$onDownloadFailure$1;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2, p3}, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl$onDownloadFailure$1;-><init>(JI)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function1;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇008〇oo(J)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl$onDownloadStart$1;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2}, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl$onDownloadStart$1;-><init>(J)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager$DownloadListenerImpl;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function1;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

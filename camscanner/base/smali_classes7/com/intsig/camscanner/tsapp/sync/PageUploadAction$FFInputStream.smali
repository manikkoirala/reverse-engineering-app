.class Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;
.super Ljava/io/InputStream;
.source "PageUploadAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/tsapp/sync/PageUploadAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FFInputStream"
.end annotation


# instance fields
.field final OO:Ljava/io/InputStream;

.field o0:Z

.field o〇00O:I

.field final 〇08O〇00〇o:[I

.field final 〇OOo8〇0:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/ByteArrayInputStream;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->o0:Z

    .line 6
    .line 7
    const/4 v0, 0x5

    .line 8
    new-array v0, v0, [I

    .line 9
    .line 10
    fill-array-data v0, :array_0

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->〇08O〇00〇o:[I

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    iput v0, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->o〇00O:I

    .line 17
    .line 18
    new-instance v0, Ljava/io/BufferedInputStream;

    .line 19
    .line 20
    invoke-direct {v0, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->OO:Ljava/io/InputStream;

    .line 24
    .line 25
    new-instance p1, Ljava/io/BufferedInputStream;

    .line 26
    .line 27
    new-instance v0, Ljava/io/FileInputStream;

    .line 28
    .line 29
    invoke-direct {v0, p2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-direct {p1, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 33
    .line 34
    .line 35
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->〇OOo8〇0:Ljava/io/InputStream;

    .line 36
    .line 37
    return-void

    .line 38
    nop

    .line 39
    :array_0
    .array-data 4
        0xd
        0xa
        0xd
        0xa
        -0x1
    .end array-data
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public close()V
    .locals 3

    .line 1
    const-string v0, "IOException"

    .line 2
    .line 3
    const-string v1, "PageUploadAction"

    .line 4
    .line 5
    :try_start_0
    invoke-super {p0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :catch_0
    move-exception v2

    .line 10
    invoke-static {v1, v0, v2}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 11
    .line 12
    .line 13
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->OO:Ljava/io/InputStream;

    .line 14
    .line 15
    if-eqz v2, :cond_0

    .line 16
    .line 17
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 18
    .line 19
    .line 20
    goto :goto_1

    .line 21
    :catch_1
    move-exception v2

    .line 22
    invoke-static {v1, v0, v2}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 23
    .line 24
    .line 25
    :cond_0
    :goto_1
    :try_start_2
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->〇OOo8〇0:Ljava/io/InputStream;

    .line 26
    .line 27
    if-eqz v2, :cond_1

    .line 28
    .line 29
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 30
    .line 31
    .line 32
    goto :goto_2

    .line 33
    :catch_2
    move-exception v2

    .line 34
    invoke-static {v1, v0, v2}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 35
    .line 36
    .line 37
    :cond_1
    :goto_2
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->o0:Z

    const/4 v1, -0x1

    if-eqz v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->OO:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    if-eq v0, v1, :cond_0

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->o0:Z

    .line 4
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->〇OOo8〇0:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    if-eq v0, v1, :cond_2

    return v0

    .line 5
    :cond_2
    iget v0, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->o〇00O:I

    const/4 v2, 0x5

    if-ge v0, v2, :cond_3

    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->〇08O〇00〇o:[I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->o〇00O:I

    aget v0, v1, v0

    return v0

    :cond_3
    return v1
.end method

.method public read([B)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7
    iget-boolean v0, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->o0:Z

    const/4 v1, 0x0

    const/4 v2, -0x1

    if-eqz v0, :cond_1

    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->OO:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I

    move-result v0

    if-eq v0, v2, :cond_0

    return v0

    .line 9
    :cond_0
    iput-boolean v1, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->o0:Z

    .line 10
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->〇OOo8〇0:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I

    move-result v0

    if-eq v0, v2, :cond_2

    return v0

    .line 11
    :cond_2
    iget v0, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->o〇00O:I

    const/4 v3, 0x5

    if-ge v0, v3, :cond_3

    const/16 v0, 0xd

    .line 12
    aput-byte v0, p1, v1

    const/4 v1, 0x1

    const/16 v2, 0xa

    .line 13
    aput-byte v2, p1, v1

    const/4 v1, 0x2

    .line 14
    aput-byte v0, p1, v1

    const/4 v0, 0x3

    .line 15
    aput-byte v2, p1, v0

    .line 16
    iput v3, p0, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction$FFInputStream;->o〇00O:I

    const/4 p1, 0x4

    return p1

    :cond_3
    return v2
.end method

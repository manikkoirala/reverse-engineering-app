.class public Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;
.super Ljava/lang/Object;
.source "ConnectReceiverLifecycle.java"

# interfaces
.implements Landroidx/lifecycle/LifecycleObserver;


# instance fields
.field private OO:Landroid/content/Context;

.field private o0:Landroidx/lifecycle/LifecycleOwner;

.field private 〇OOo8〇0:Lcom/intsig/camscanner/tsapp/sync/ConnectReceiver;


# direct methods
.method public constructor <init>(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->o0:Landroidx/lifecycle/LifecycleOwner;

    .line 5
    .line 6
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 7
    .line 8
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->OO:Landroid/content/Context;

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->〇080()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private O8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->〇OOo8〇0:Lcom/intsig/camscanner/tsapp/sync/ConnectReceiver;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->OO:Landroid/content/Context;

    .line 6
    .line 7
    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->〇OOo8〇0:Lcom/intsig/camscanner/tsapp/sync/ConnectReceiver;

    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private onCreate()V
    .locals 2
    .annotation runtime Landroidx/lifecycle/OnLifecycleEvent;
        value = .enum Landroidx/lifecycle/Lifecycle$Event;->ON_CREATE:Landroidx/lifecycle/Lifecycle$Event;
    .end annotation

    .line 1
    const-string v0, "ConnectReceiverLifecycle"

    .line 2
    .line 3
    const-string v1, "ON_CREATE"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {p0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->O8(Ljava/lang/Object;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->〇o〇()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private onDestroy()V
    .locals 2
    .annotation runtime Landroidx/lifecycle/OnLifecycleEvent;
        value = .enum Landroidx/lifecycle/Lifecycle$Event;->ON_DESTROY:Landroidx/lifecycle/Lifecycle$Event;
    .end annotation

    .line 1
    const-string v0, "ConnectReceiverLifecycle"

    .line 2
    .line 3
    const-string v1, "ON_DESTROY"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {p0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->o〇0(Ljava/lang/Object;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->O8()V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->o0:Landroidx/lifecycle/LifecycleOwner;

    .line 15
    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    invoke-interface {v0}, Landroidx/lifecycle/LifecycleOwner;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-virtual {v0, p0}, Landroidx/lifecycle/Lifecycle;->removeObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 23
    .line 24
    .line 25
    :cond_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private 〇080()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->o0:Landroidx/lifecycle/LifecycleOwner;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Landroidx/lifecycle/LifecycleOwner;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0, p0}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static 〇o00〇〇Oo(Landroidx/lifecycle/LifecycleOwner;)Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;-><init>(Landroidx/lifecycle/LifecycleOwner;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇o〇()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/IntentFilter;

    .line 2
    .line 3
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v1, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiver;

    .line 9
    .line 10
    invoke-direct {v1}, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiver;-><init>()V

    .line 11
    .line 12
    .line 13
    iput-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->〇OOo8〇0:Lcom/intsig/camscanner/tsapp/sync/ConnectReceiver;

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->OO:Landroid/content/Context;

    .line 16
    .line 17
    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
.end method


# virtual methods
.method public onConnectReceiver(Lcom/intsig/camscanner/eventbus/ConnectReceiverEvent;)V
    .locals 2
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->BACKGROUND:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    const-string v0, "onConnectReceiver"

    .line 2
    .line 3
    const-string v1, "ConnectReceiverLifecycle"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    if-nez p1, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->〇OOo8〇0:Lcom/intsig/camscanner/tsapp/sync/ConnectReceiver;

    .line 12
    .line 13
    if-nez v0, :cond_1

    .line 14
    .line 15
    return-void

    .line 16
    :cond_1
    iget-boolean p1, p1, Lcom/intsig/camscanner/eventbus/ConnectReceiverEvent;->〇080:Z

    .line 17
    .line 18
    if-eqz p1, :cond_2

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->OO:Landroid/content/Context;

    .line 21
    .line 22
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-eqz v0, :cond_2

    .line 27
    .line 28
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O8ooOoo〇()Lcom/intsig/camscanner/tsapp/sync/SyncClient;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->ooo〇8oO()V

    .line 33
    .line 34
    .line 35
    :cond_2
    if-eqz p1, :cond_3

    .line 36
    .line 37
    const-string p1, "ConnectReceiver communicateOnAppStartInThread"

    .line 38
    .line 39
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->OO:Landroid/content/Context;

    .line 43
    .line 44
    const/4 v0, 0x0

    .line 45
    invoke-static {p1, v0}, Lcom/intsig/camscanner/app/AppToServer;->o〇0(Landroid/content/Context;Z)V

    .line 46
    .line 47
    .line 48
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->OO:Landroid/content/Context;

    .line 49
    .line 50
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/AutoUploadThread;->〇〇808〇(Landroid/content/Context;)Lcom/intsig/camscanner/tsapp/AutoUploadThread;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    invoke-virtual {p1}, Lcom/intsig/camscanner/tsapp/AutoUploadThread;->〇0〇O0088o()V

    .line 55
    .line 56
    .line 57
    sget-object p1, Lcom/intsig/camscanner/message/MessageClient;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/message/MessageClient$Companion;

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/camscanner/message/MessageClient$Companion;->〇080()Lcom/intsig/camscanner/message/MessageClient;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    invoke-virtual {p1}, Lcom/intsig/camscanner/message/MessageClient;->〇oOO8O8()V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;->OO:Landroid/content/Context;

    .line 68
    .line 69
    invoke-static {p1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->O8(Landroid/content/Context;)V

    .line 70
    .line 71
    .line 72
    sget-object p1, Lcom/intsig/camscanner/message/MessageClient;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/message/MessageClient$Companion;

    .line 73
    .line 74
    invoke-virtual {p1}, Lcom/intsig/camscanner/message/MessageClient$Companion;->〇080()Lcom/intsig/camscanner/message/MessageClient;

    .line 75
    .line 76
    .line 77
    move-result-object p1

    .line 78
    invoke-virtual {p1}, Lcom/intsig/camscanner/message/MessageClient;->〇〇808〇()V

    .line 79
    .line 80
    .line 81
    :goto_0
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.class public Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;
.super Ljava/lang/Object;
.source "ImageDownloadControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Request"
.end annotation


# instance fields
.field public O8:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic OO0o〇〇〇〇0:Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl;

.field public Oo08:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private oO80:Lcom/intsig/camscanner/control/ProgressAnimHandler;

.field o〇0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public 〇080:Landroid/content/Context;

.field 〇80〇808〇O:I

.field final 〇o00〇〇Oo:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public 〇o〇:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public 〇〇888:Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$DownloadListener;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl;Landroid/content/Context;Ljava/util/ArrayList;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;I)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇080:Landroid/content/Context;

    .line 3
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇o00〇〇Oo:Ljava/lang/ref/WeakReference;

    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇o〇:Ljava/util/ArrayList;

    .line 5
    iput p4, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇80〇808〇O:I

    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇o00〇〇Oo()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl;Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 7
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇080:Landroid/content/Context;

    .line 9
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇o00〇〇Oo:Ljava/lang/ref/WeakReference;

    .line 10
    iput-object p3, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->O8:Ljava/util/ArrayList;

    .line 11
    iput-object p4, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->Oo08:Ljava/util/ArrayList;

    .line 12
    iput-object p5, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->o〇0:Ljava/util/ArrayList;

    .line 13
    iput p6, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇80〇808〇O:I

    return-void
.end method

.method public constructor <init>(Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl;Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$DownloadListener;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$DownloadListener;",
            "I)V"
        }
    .end annotation

    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇080:Landroid/content/Context;

    .line 16
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇o00〇〇Oo:Ljava/lang/ref/WeakReference;

    .line 17
    iput-object p3, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->O8:Ljava/util/ArrayList;

    .line 18
    iput-object p4, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->Oo08:Ljava/util/ArrayList;

    .line 19
    iput-object p5, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->o〇0:Ljava/util/ArrayList;

    .line 20
    iput-object p6, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇〇888:Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$DownloadListener;

    .line 21
    iput p7, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇80〇808〇O:I

    .line 22
    invoke-virtual {p0}, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇o〇()V

    return-void
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;)Lcom/intsig/camscanner/control/ProgressAnimHandler;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->oO80:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇o00〇〇Oo()V
    .locals 13

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇o〇:Ljava/util/ArrayList;

    .line 2
    .line 3
    if-eqz v0, :cond_4

    .line 4
    .line 5
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->O8:Ljava/util/ArrayList;

    .line 11
    .line 12
    new-instance v0, Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->Oo08:Ljava/util/ArrayList;

    .line 18
    .line 19
    new-instance v0, Ljava/util/ArrayList;

    .line 20
    .line 21
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 22
    .line 23
    .line 24
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->o〇0:Ljava/util/ArrayList;

    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇080:Landroid/content/Context;

    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇o〇:Ljava/util/ArrayList;

    .line 29
    .line 30
    const/4 v2, 0x0

    .line 31
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    check-cast v1, Ljava/lang/Long;

    .line 36
    .line 37
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    .line 38
    .line 39
    .line 40
    move-result-wide v3

    .line 41
    invoke-static {v0, v3, v4}, Lcom/intsig/camscanner/tsapp/collaborate/CollaborateUtil;->oO80(Landroid/content/Context;J)Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇o〇:Ljava/util/ArrayList;

    .line 46
    .line 47
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    const/4 v3, 0x0

    .line 52
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 53
    .line 54
    .line 55
    move-result v4

    .line 56
    if-eqz v4, :cond_4

    .line 57
    .line 58
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    move-result-object v4

    .line 62
    check-cast v4, Ljava/lang/Long;

    .line 63
    .line 64
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    .line 65
    .line 66
    .line 67
    move-result-wide v4

    .line 68
    if-eqz v0, :cond_1

    .line 69
    .line 70
    iget-object v3, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇080:Landroid/content/Context;

    .line 71
    .line 72
    invoke-static {v3, v4, v5}, Lcom/intsig/camscanner/tsapp/collaborate/CollaborateUtil;->o〇0(Landroid/content/Context;J)Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v3

    .line 76
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 77
    .line 78
    .line 79
    move-result v6

    .line 80
    if-eqz v6, :cond_1

    .line 81
    .line 82
    const-string v0, "ImageDownloadControl"

    .line 83
    .line 84
    const-string v1, "\u4e0d\u652f\u6301\u6df7\u5408\u4e0b\u8f7d\u534f\u4f5c\u4e0e\u975e\u534f\u4f5c"

    .line 85
    .line 86
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    return-void

    .line 90
    :cond_1
    iget-object v6, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇080:Landroid/content/Context;

    .line 91
    .line 92
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 93
    .line 94
    .line 95
    move-result-object v7

    .line 96
    invoke-static {v4, v5}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 97
    .line 98
    .line 99
    move-result-object v8

    .line 100
    const-string v4, "sync_image_id"

    .line 101
    .line 102
    const-string v5, "sync_version"

    .line 103
    .line 104
    const-string v6, "_id"

    .line 105
    .line 106
    filled-new-array {v6, v4, v5}, [Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v9

    .line 110
    const-string v10, "cache_state=1"

    .line 111
    .line 112
    const/4 v11, 0x0

    .line 113
    const/4 v12, 0x0

    .line 114
    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 115
    .line 116
    .line 117
    move-result-object v4

    .line 118
    if-eqz v4, :cond_0

    .line 119
    .line 120
    :cond_2
    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    .line 121
    .line 122
    .line 123
    move-result v5

    .line 124
    if-eqz v5, :cond_3

    .line 125
    .line 126
    iget-object v5, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->O8:Ljava/util/ArrayList;

    .line 127
    .line 128
    invoke-interface {v4, v2}, Landroid/database/Cursor;->getLong(I)J

    .line 129
    .line 130
    .line 131
    move-result-wide v6

    .line 132
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 133
    .line 134
    .line 135
    move-result-object v6

    .line 136
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    .line 138
    .line 139
    iget-object v5, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->Oo08:Ljava/util/ArrayList;

    .line 140
    .line 141
    const/4 v6, 0x1

    .line 142
    invoke-interface {v4, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object v6

    .line 146
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    .line 148
    .line 149
    if-eqz v0, :cond_2

    .line 150
    .line 151
    iget-object v5, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->o〇0:Ljava/util/ArrayList;

    .line 152
    .line 153
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    .line 155
    .line 156
    goto :goto_1

    .line 157
    :cond_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 158
    .line 159
    .line 160
    goto :goto_0

    .line 161
    :cond_4
    return-void
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_4

    .line 3
    .line 4
    instance-of v1, p1, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;

    .line 5
    .line 6
    if-eqz v1, :cond_4

    .line 7
    .line 8
    check-cast p1, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇o〇:Ljava/util/ArrayList;

    .line 11
    .line 12
    const/4 v2, 0x1

    .line 13
    if-eqz v1, :cond_2

    .line 14
    .line 15
    iget-object v3, p1, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇o〇:Ljava/util/ArrayList;

    .line 16
    .line 17
    if-eqz v3, :cond_2

    .line 18
    .line 19
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    iget-object v3, p1, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇o〇:Ljava/util/ArrayList;

    .line 24
    .line 25
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    if-ne v1, v3, :cond_2

    .line 30
    .line 31
    const/4 v1, 0x0

    .line 32
    :goto_0
    iget-object v3, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇o〇:Ljava/util/ArrayList;

    .line 33
    .line 34
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    if-ge v1, v3, :cond_1

    .line 39
    .line 40
    iget-object v3, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇o〇:Ljava/util/ArrayList;

    .line 41
    .line 42
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    iget-object v4, p1, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇o〇:Ljava/util/ArrayList;

    .line 47
    .line 48
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    move-result-object v4

    .line 52
    if-eq v3, v4, :cond_0

    .line 53
    .line 54
    goto :goto_2

    .line 55
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_1
    const/4 v0, 0x1

    .line 59
    goto :goto_2

    .line 60
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->O8:Ljava/util/ArrayList;

    .line 61
    .line 62
    if-eqz v1, :cond_4

    .line 63
    .line 64
    iget-object v3, p1, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->O8:Ljava/util/ArrayList;

    .line 65
    .line 66
    if-eqz v3, :cond_4

    .line 67
    .line 68
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    iget-object v3, p1, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->O8:Ljava/util/ArrayList;

    .line 73
    .line 74
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    .line 75
    .line 76
    .line 77
    move-result v3

    .line 78
    if-ne v1, v3, :cond_4

    .line 79
    .line 80
    const/4 v1, 0x0

    .line 81
    :goto_1
    iget-object v3, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->O8:Ljava/util/ArrayList;

    .line 82
    .line 83
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    .line 84
    .line 85
    .line 86
    move-result v3

    .line 87
    if-ge v1, v3, :cond_1

    .line 88
    .line 89
    iget-object v3, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->O8:Ljava/util/ArrayList;

    .line 90
    .line 91
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 92
    .line 93
    .line 94
    move-result-object v3

    .line 95
    iget-object v4, p1, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->O8:Ljava/util/ArrayList;

    .line 96
    .line 97
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 98
    .line 99
    .line 100
    move-result-object v4

    .line 101
    if-eq v3, v4, :cond_3

    .line 102
    .line 103
    goto :goto_2

    .line 104
    :cond_3
    add-int/lit8 v1, v1, 0x1

    .line 105
    .line 106
    goto :goto_1

    .line 107
    :cond_4
    :goto_2
    return v0
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public 〇o〇()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->〇080:Landroid/content/Context;

    .line 4
    .line 5
    new-instance v2, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request$1;

    .line 6
    .line 7
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request$1;-><init>(Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;)V

    .line 8
    .line 9
    .line 10
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/control/ProgressAnimHandler;-><init>(Ljava/lang/Object;Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl$Request;->oO80:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

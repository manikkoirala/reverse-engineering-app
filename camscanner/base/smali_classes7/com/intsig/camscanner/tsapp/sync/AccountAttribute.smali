.class public final Lcom/intsig/camscanner/tsapp/sync/AccountAttribute;
.super Ljava/lang/Object;
.source "AccountAttribute.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final attribute:Lcom/intsig/camscanner/tsapp/sync/AttributeResult;

.field private final error_code:I

.field private final error_msg:Ljava/lang/String;

.field private final status:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILcom/intsig/camscanner/tsapp/sync/AttributeResult;I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/AccountAttribute;->error_msg:Ljava/lang/String;

    .line 3
    iput p2, p0, Lcom/intsig/camscanner/tsapp/sync/AccountAttribute;->error_code:I

    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/tsapp/sync/AccountAttribute;->attribute:Lcom/intsig/camscanner/tsapp/sync/AttributeResult;

    .line 5
    iput p4, p0, Lcom/intsig/camscanner/tsapp/sync/AccountAttribute;->status:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;ILcom/intsig/camscanner/tsapp/sync/AttributeResult;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x2

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    const/4 p3, 0x0

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    const/4 p4, 0x0

    .line 6
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/tsapp/sync/AccountAttribute;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/tsapp/sync/AttributeResult;I)V

    return-void
.end method


# virtual methods
.method public final getAttribute()Lcom/intsig/camscanner/tsapp/sync/AttributeResult;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/AccountAttribute;->attribute:Lcom/intsig/camscanner/tsapp/sync/AttributeResult;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getError_code()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/tsapp/sync/AccountAttribute;->error_code:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getError_msg()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/AccountAttribute;->error_msg:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getStatus()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/tsapp/sync/AccountAttribute;->status:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

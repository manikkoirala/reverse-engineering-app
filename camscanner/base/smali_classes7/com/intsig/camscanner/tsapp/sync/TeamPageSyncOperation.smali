.class public Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;
.super Ljava/lang/Object;
.source "TeamPageSyncOperation.java"

# interfaces
.implements Lcom/intsig/tianshu/sync/TeamBatchUpdateInterface;


# instance fields
.field private O8:J

.field private Oo08:Ljava/lang/String;

.field private oO80:Lcom/intsig/camscanner/tsapp/sync/TeamImageSyncListener;

.field private o〇0:J

.field private 〇080:Landroid/content/Context;

.field private 〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

.field private 〇o〇:Landroid/content/ContentResolver;

.field private 〇〇888:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-wide/16 v0, 0x0

    .line 5
    .line 6
    iput-wide v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->O8:J

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation$1;

    .line 9
    .line 10
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation$1;-><init>(Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->oO80:Lcom/intsig/camscanner/tsapp/sync/TeamImageSyncListener;

    .line 14
    .line 15
    new-instance v0, Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 16
    .line 17
    invoke-direct {v0, p1, p2, p3}, Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;-><init>(Landroid/content/Context;J)V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->oO80:Lcom/intsig/camscanner/tsapp/sync/TeamImageSyncListener;

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;->o0O0(Lcom/intsig/camscanner/tsapp/sync/TeamImageSyncListener;)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 28
    .line 29
    const/4 v1, 0x1

    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;->OO8oO0o〇(Z)V

    .line 31
    .line 32
    .line 33
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇080:Landroid/content/Context;

    .line 34
    .line 35
    iput-wide p2, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->o〇0:J

    .line 36
    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o〇:Landroid/content/ContentResolver;

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic oO80(Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic o〇0(Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇080:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇O〇(J)V
    .locals 3

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->O8:J

    .line 2
    .line 3
    cmp-long v2, v0, p1

    .line 4
    .line 5
    if-eqz v2, :cond_0

    .line 6
    .line 7
    const-wide/16 v0, 0x0

    .line 8
    .line 9
    cmp-long v2, p1, v0

    .line 10
    .line 11
    if-lez v2, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇080:Landroid/content/Context;

    .line 14
    .line 15
    const/4 v1, 0x0

    .line 16
    invoke-static {v0, p1, p2, v1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->o〇o(Landroid/content/Context;JZ)V

    .line 17
    .line 18
    .line 19
    :cond_0
    iput-wide p1, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->O8:J

    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇〇888:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public O8(JLjava/lang/String;JLjava/lang/String;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "addFile fileName="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "TeamPageSyncOperation"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual/range {p0 .. p6}, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->Oo08(JLjava/lang/String;JLjava/lang/String;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public OO0o〇〇(Ljava/util/Vector;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "Lcom/intsig/camscanner/tsapp/SyncCallbackListener;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇oo〇(Ljava/util/Vector;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public OO0o〇〇〇〇0()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;->OO0o〇〇〇〇0()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oo08(JLjava/lang/String;JLjava/lang/String;)V
    .locals 0

    .line 1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string p2, "modifyFile fileName="

    .line 7
    .line 8
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string p2, " content="

    .line 15
    .line 16
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    const-string p2, "TeamPageSyncOperation"

    .line 27
    .line 28
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    if-nez p1, :cond_1

    .line 36
    .line 37
    const-string p1, ".jpage"

    .line 38
    .line 39
    invoke-virtual {p3, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    if-eqz p1, :cond_1

    .line 44
    .line 45
    const-string p1, "."

    .line 46
    .line 47
    invoke-virtual {p3, p1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    .line 48
    .line 49
    .line 50
    move-result p1

    .line 51
    const/4 p2, -0x1

    .line 52
    if-eq p1, p2, :cond_0

    .line 53
    .line 54
    const/4 p2, 0x0

    .line 55
    invoke-virtual {p3, p2, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p3

    .line 59
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 60
    .line 61
    iget-object p2, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o〇:Landroid/content/ContentResolver;

    .line 62
    .line 63
    invoke-virtual {p1, p2, p3, p6}, Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;->O0O8OO088(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 68
    .line 69
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 70
    .line 71
    .line 72
    const-string p4, "modifyFile error fileName="

    .line 73
    .line 74
    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object p1

    .line 84
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    :goto_0
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public Oooo8o0〇(Ljava/lang/String;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;->〇0(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇080(Lcom/intsig/tianshu/UploadAction;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;->〇080(Lcom/intsig/tianshu/UploadAction;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇80〇808〇O()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;->〇〇888()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8o8o〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->o800o8O(Z)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O00(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;->O0O8OO088(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public 〇O8o08O(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇〇888:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇o00〇〇Oo(JLjava/lang/String;)V
    .locals 19

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p3

    .line 4
    .line 5
    new-instance v2, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v3, "deleteFile fileName="

    .line 11
    .line 12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    const-string v3, "TeamPageSyncOperation"

    .line 23
    .line 24
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    if-nez v2, :cond_5

    .line 32
    .line 33
    const-string v2, ".jpage"

    .line 34
    .line 35
    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    if-eqz v2, :cond_5

    .line 40
    .line 41
    const-string v2, "."

    .line 42
    .line 43
    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    const/4 v4, -0x1

    .line 48
    const/4 v5, 0x0

    .line 49
    if-eq v2, v4, :cond_0

    .line 50
    .line 51
    invoke-virtual {v1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    :cond_0
    iget-object v6, v0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o〇:Landroid/content/ContentResolver;

    .line 56
    .line 57
    sget-object v7, Lcom/intsig/camscanner/provider/Documents$Image;->〇o〇:Landroid/net/Uri;

    .line 58
    .line 59
    const-string v2, "document_id"

    .line 60
    .line 61
    const-string v4, "page_num"

    .line 62
    .line 63
    const-string v8, "_id"

    .line 64
    .line 65
    filled-new-array {v8, v2, v4}, [Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v8

    .line 69
    const-string v9, "sync_image_id =? "

    .line 70
    .line 71
    const/4 v2, 0x1

    .line 72
    new-array v10, v2, [Ljava/lang/String;

    .line 73
    .line 74
    aput-object v1, v10, v5

    .line 75
    .line 76
    const/4 v11, 0x0

    .line 77
    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    const-wide/16 v6, -0x1

    .line 82
    .line 83
    if-eqz v1, :cond_2

    .line 84
    .line 85
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 86
    .line 87
    .line 88
    move-result v4

    .line 89
    if-eqz v4, :cond_1

    .line 90
    .line 91
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getLong(I)J

    .line 92
    .line 93
    .line 94
    move-result-wide v4

    .line 95
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    .line 96
    .line 97
    .line 98
    move-result-wide v6

    .line 99
    const/4 v2, 0x2

    .line 100
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    .line 101
    .line 102
    .line 103
    move-wide/from16 v17, v4

    .line 104
    .line 105
    move-wide v4, v6

    .line 106
    move-wide/from16 v6, v17

    .line 107
    .line 108
    goto :goto_0

    .line 109
    :cond_1
    move-wide v4, v6

    .line 110
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 111
    .line 112
    .line 113
    move-wide v11, v6

    .line 114
    move-wide v6, v4

    .line 115
    goto :goto_1

    .line 116
    :cond_2
    move-wide v11, v6

    .line 117
    :goto_1
    const-wide/16 v1, 0x0

    .line 118
    .line 119
    cmp-long v4, v11, v1

    .line 120
    .line 121
    if-lez v4, :cond_4

    .line 122
    .line 123
    iget-object v1, v0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇080:Landroid/content/Context;

    .line 124
    .line 125
    invoke-static {v1, v11, v12}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0O8OO088(Landroid/content/Context;J)Ljava/util/List;

    .line 126
    .line 127
    .line 128
    move-result-object v1

    .line 129
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Image;->〇o〇:Landroid/net/Uri;

    .line 130
    .line 131
    invoke-static {v2, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 132
    .line 133
    .line 134
    move-result-object v2

    .line 135
    iget-object v4, v0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o〇:Landroid/content/ContentResolver;

    .line 136
    .line 137
    const/4 v5, 0x0

    .line 138
    invoke-virtual {v4, v2, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 139
    .line 140
    .line 141
    move-result v2

    .line 142
    if-lez v2, :cond_3

    .line 143
    .line 144
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->OO0o〇〇〇〇0(Ljava/util/List;)V

    .line 145
    .line 146
    .line 147
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 148
    .line 149
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    .line 151
    .line 152
    const-string v4, "deleteFile deleteNum="

    .line 153
    .line 154
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    .line 156
    .line 157
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 158
    .line 159
    .line 160
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 161
    .line 162
    .line 163
    move-result-object v1

    .line 164
    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    .line 166
    .line 167
    iget-object v8, v0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 168
    .line 169
    const-wide/16 v13, -0x1

    .line 170
    .line 171
    const/4 v15, 0x2

    .line 172
    const/16 v16, 0x1

    .line 173
    .line 174
    move-wide v9, v6

    .line 175
    invoke-virtual/range {v8 .. v16}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->o〇O8〇〇o(JJJIZ)V

    .line 176
    .line 177
    .line 178
    :cond_4
    invoke-direct {v0, v6, v7}, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇O〇(J)V

    .line 179
    .line 180
    .line 181
    goto :goto_2

    .line 182
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    .line 183
    .line 184
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 185
    .line 186
    .line 187
    const-string v4, "deleteFile error fileName="

    .line 188
    .line 189
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    .line 191
    .line 192
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 196
    .line 197
    .line 198
    move-result-object v1

    .line 199
    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    .line 201
    .line 202
    :goto_2
    return-void
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public 〇o〇(IJ)Ljava/util/Vector;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 2
    .line 3
    invoke-virtual {v0, p2, p3}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->OoO8(J)V

    .line 4
    .line 5
    .line 6
    iget-object p2, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 7
    .line 8
    const/4 p3, 0x1

    .line 9
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->o800o8O(Z)V

    .line 10
    .line 11
    .line 12
    iget-object p2, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 13
    .line 14
    const/4 p3, 0x0

    .line 15
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;->O0o〇〇Oo(Z)V

    .line 16
    .line 17
    .line 18
    iget-object p2, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 19
    .line 20
    const-string v0, "CamScanner_Tag"

    .line 21
    .line 22
    invoke-virtual {p2, v0, p1, p3}, Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;->〇o00〇〇Oo(Ljava/lang/String;II)Ljava/util/Vector;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇〇808〇(J)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 2
    .line 3
    const-wide/16 v1, 0x0

    .line 4
    .line 5
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->OoO8(J)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->〇o00〇〇Oo:Lcom/intsig/camscanner/tsapp/sync/TagSyncOperation;

    .line 9
    .line 10
    invoke-virtual {v0, p1, p2}, Lcom/intsig/camscanner/tsapp/sync/AbstractSyncOperation;->〇0〇O0088o(J)V

    .line 11
    .line 12
    .line 13
    iput-wide p1, p0, Lcom/intsig/camscanner/tsapp/sync/TeamPageSyncOperation;->o〇0:J

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

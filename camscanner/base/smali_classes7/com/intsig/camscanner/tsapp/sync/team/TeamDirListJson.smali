.class public Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "TeamDirListJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$TeamDoc;,
        Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDoc;,
        Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$TeamDir;,
        Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$TeamUploadDir;,
        Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$BaseTeamDir;,
        Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$Team;
    }
.end annotation


# instance fields
.field public cur_total_num:I

.field public list:[Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$TeamDir;

.field public max_layer_num:I

.field public max_vip_total_num:I

.field public team:Lcom/intsig/camscanner/tsapp/sync/team/TeamDirListJson$Team;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

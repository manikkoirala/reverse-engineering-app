.class public Lcom/intsig/camscanner/tsapp/message/OperationalMsgJson;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "OperationalMsgJson.java"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/tsapp/message/OperationalMsgJson$OperationDialogEntity;
    }
.end annotation


# instance fields
.field public Type:Ljava/lang/String;

.field public body:Lcom/intsig/camscanner/tsapp/message/OperationalMsgJson$OperationDialogEntity;

.field public content:Ljava/lang/String;

.field public msg_id:Ljava/lang/String;

.field public msg_num:Ljava/lang/String;

.field public pop_title:Ljava/lang/String;

.field public sub_type:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 3
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

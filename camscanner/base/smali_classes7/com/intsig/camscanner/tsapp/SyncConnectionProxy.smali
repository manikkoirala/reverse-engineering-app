.class public Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;
.super Ljava/lang/Object;
.source "SyncConnectionProxy.java"

# interfaces
.implements Lcom/intsig/tianshu/Connection;


# instance fields
.field private 〇080:Lcom/intsig/tianshu/Connection;

.field private 〇o00〇〇Oo:Ljava/lang/String;

.field private 〇o〇:Lcom/intsig/crashapm/firebase/FirebaseNetTrack;


# direct methods
.method constructor <init>(Lcom/intsig/tianshu/Connection;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 5
    .line 6
    new-instance p1, Lcom/intsig/crashapm/firebase/FirebaseNetTrack;

    .line 7
    .line 8
    invoke-direct {p1}, Lcom/intsig/crashapm/firebase/FirebaseNetTrack;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇o〇:Lcom/intsig/crashapm/firebase/FirebaseNetTrack;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private OO0o〇〇(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    :try_start_0
    new-instance v1, Ljava/net/URL;

    .line 14
    .line 15
    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-static {v1}, Ljava/net/InetAddress;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :catch_0
    move-exception v1

    .line 35
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    const-string p1, " ## "

    .line 39
    .line 40
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    :goto_0
    const-string p1, "SyncConnectionProxy"

    .line 51
    .line 52
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public O8(Ljava/lang/String;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇o〇:Lcom/intsig/crashapm/firebase/FirebaseNetTrack;

    .line 4
    .line 5
    const-string v1, "GET"

    .line 6
    .line 7
    invoke-virtual {v0, p1, v1}, Lcom/intsig/crashapm/firebase/FirebaseNetTrack;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 11
    .line 12
    invoke-interface {v0, p1, p2, p3}, Lcom/intsig/tianshu/Connection;->O8(Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 13
    .line 14
    .line 15
    return-void

    .line 16
    :catch_0
    move-exception p1

    .line 17
    iget-object p2, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇o〇:Lcom/intsig/crashapm/firebase/FirebaseNetTrack;

    .line 18
    .line 19
    const/4 p3, -0x1

    .line 20
    invoke-virtual {p2, p3}, Lcom/intsig/crashapm/firebase/FirebaseNetTrack;->〇o00〇〇Oo(I)V

    .line 21
    .line 22
    .line 23
    iget-object p2, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇o00〇〇Oo:Ljava/lang/String;

    .line 24
    .line 25
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->OO0o〇〇(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    throw p1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public OO0o〇〇〇〇0(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/tianshu/Connection;->OO0o〇〇〇〇0(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public Oo08()Ljava/io/InputStream;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/tianshu/Connection;->Oo08()Ljava/io/InputStream;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public disconnect()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/tianshu/Connection;->disconnect()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇o〇:Lcom/intsig/crashapm/firebase/FirebaseNetTrack;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/crashapm/firebase/FirebaseNetTrack;->O8()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/tianshu/Connection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/tianshu/Connection;->getInputStream()Ljava/io/InputStream;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRequestMethod()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "NULL"

    .line 6
    .line 7
    return-object v0

    .line 8
    :cond_0
    invoke-interface {v0}, Lcom/intsig/tianshu/Connection;->getRequestMethod()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getResponseCode()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, -0x2

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 3
    .line 4
    invoke-interface {v1}, Lcom/intsig/tianshu/Connection;->getResponseCode()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    const/16 v2, 0x196

    .line 9
    .line 10
    if-ne v1, v2, :cond_0

    .line 11
    .line 12
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 13
    .line 14
    const-string v3, "X-IS-Error-Code"

    .line 15
    .line 16
    const/4 v4, 0x0

    .line 17
    invoke-interface {v2, v3, v4}, Lcom/intsig/tianshu/Connection;->〇o〇(Ljava/lang/String;I)I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    if-eqz v2, :cond_0

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    move v2, v1

    .line 25
    :goto_0
    iget-object v3, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇o〇:Lcom/intsig/crashapm/firebase/FirebaseNetTrack;

    .line 26
    .line 27
    invoke-virtual {v3, v2}, Lcom/intsig/crashapm/firebase/FirebaseNetTrack;->Oo08(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    .line 29
    .line 30
    return v1

    .line 31
    :catchall_0
    move-exception v1

    .line 32
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇o〇:Lcom/intsig/crashapm/firebase/FirebaseNetTrack;

    .line 33
    .line 34
    invoke-virtual {v2, v0}, Lcom/intsig/crashapm/firebase/FirebaseNetTrack;->〇o00〇〇Oo(I)V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇o00〇〇Oo:Ljava/lang/String;

    .line 38
    .line 39
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->OO0o〇〇(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    new-instance v0, Ljava/io/IOException;

    .line 43
    .line 44
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    .line 45
    .line 46
    .line 47
    throw v0

    .line 48
    :catch_0
    move-exception v1

    .line 49
    iget-object v2, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇o〇:Lcom/intsig/crashapm/firebase/FirebaseNetTrack;

    .line 50
    .line 51
    invoke-virtual {v2, v0}, Lcom/intsig/crashapm/firebase/FirebaseNetTrack;->〇o00〇〇Oo(I)V

    .line 52
    .line 53
    .line 54
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇o00〇〇Oo:Ljava/lang/String;

    .line 55
    .line 56
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->OO0o〇〇(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    throw v1
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public oO80(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    invoke-interface {v0, p1, p2}, Lcom/intsig/tianshu/Connection;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public o〇0(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/tianshu/Connection;->o〇0(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setReadTimeout(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/tianshu/Connection;->setReadTimeout(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇080(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/tianshu/Connection;->〇080(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇80〇808〇O(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/tianshu/Connection;->〇80〇808〇O(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇8o8o〇(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/tianshu/Connection;->〇8o8o〇(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇O8o08O(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/tianshu/Connection;->〇O8o08O(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇o00〇〇Oo(Z)Ljava/io/OutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/tianshu/Connection;->〇o00〇〇Oo(Z)Ljava/io/OutputStream;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇o〇(Ljava/lang/String;I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    invoke-interface {v0, p1, p2}, Lcom/intsig/tianshu/Connection;->〇o〇(Ljava/lang/String;I)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇〇888(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tsapp/SyncConnectionProxy;->〇080:Lcom/intsig/tianshu/Connection;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/tianshu/Connection;->〇〇888(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

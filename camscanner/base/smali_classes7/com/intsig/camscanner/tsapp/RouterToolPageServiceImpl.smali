.class public final Lcom/intsig/camscanner/tsapp/RouterToolPageServiceImpl;
.super Ljava/lang/Object;
.source "RouterToolPageServiceImpl.kt"

# interfaces
.implements Lcom/intsig/router/service/RouterToolPageService;


# annotations
.annotation build Lcom/alibaba/android/arouter/facade/annotation/Route;
    name = "TOOL_PAGE"
    path = "/main/toolpage"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/tsapp/RouterToolPageServiceImpl$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Companion:Lcom/intsig/camscanner/tsapp/RouterToolPageServiceImpl$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final TAG:Ljava/lang/String; = "RouterToolPageServiceImpl"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/tsapp/RouterToolPageServiceImpl$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/tsapp/RouterToolPageServiceImpl$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/tsapp/RouterToolPageServiceImpl;->Companion:Lcom/intsig/camscanner/tsapp/RouterToolPageServiceImpl$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public init(Landroid/content/Context;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public startFunction(Landroidx/fragment/app/FragmentActivity;IZ)V
    .locals 10
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "RouterToolPageServiceImpl"

    .line 7
    .line 8
    const-string v1, "startFunction"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;

    .line 14
    .line 15
    new-instance v4, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    const/4 v8, 0x1

    .line 19
    const/4 v9, 0x0

    .line 20
    invoke-direct {v4, v1, p2, v8, v9}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 21
    .line 22
    .line 23
    const/4 v5, 0x0

    .line 24
    const/4 v6, 0x4

    .line 25
    const/4 v7, 0x0

    .line 26
    move-object v2, v0

    .line 27
    move-object v3, p1

    .line 28
    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 29
    .line 30
    .line 31
    if-eqz p3, :cond_2

    .line 32
    .line 33
    const/16 p1, 0xc9

    .line 34
    .line 35
    if-ne p1, p2, :cond_0

    .line 36
    .line 37
    const/4 p3, 0x1

    .line 38
    goto :goto_0

    .line 39
    :cond_0
    const/4 p3, 0x0

    .line 40
    :goto_0
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->O0(Z)V

    .line 41
    .line 42
    .line 43
    if-ne p1, p2, :cond_1

    .line 44
    .line 45
    const/4 v1, 0x1

    .line 46
    :cond_1
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o8O〇(Z)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v8}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->O0O8OO088(I)V

    .line 50
    .line 51
    .line 52
    :cond_2
    const-string p1, "CLICK_TOOL_FROM_TOOLPAGE"

    .line 53
    .line 54
    invoke-virtual {v0, v9, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->OOo8o〇O(Lcom/chad/library/adapter/base/BaseQuickAdapter;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

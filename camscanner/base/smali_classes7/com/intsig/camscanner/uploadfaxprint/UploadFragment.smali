.class public Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;
.super Landroidx/fragment/app/Fragment;
.source "UploadFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/uploadfaxprint/UploadFragment$MyDialogFragment;
    }
.end annotation


# instance fields
.field private O0O:I

.field private O88O:Landroid/view/View;

.field private O8o08O8O:Landroid/widget/Button;

.field private OO:Landroid/widget/Button;

.field private final OO〇00〇8oO:I

.field private Oo0〇Ooo:Z

.field private Oo80:Landroid/widget/TextView;

.field private Ooo08:Landroid/widget/TextView;

.field private O〇08oOOO0:Landroid/widget/TextView;

.field private O〇o88o08〇:Landroid/widget/TextView;

.field private o0:I

.field private o8o:I

.field private o8oOOo:I

.field private o8〇OO:Landroid/widget/TextView;

.field private final o8〇OO0〇0o:I

.field private oOO〇〇:Z

.field private final oOo0:I

.field private oOo〇8o008:Landroid/widget/Button;

.field private oO〇8O8oOo:Ljava/lang/String;

.field oo8ooo8O:Ljava/lang/String;

.field private ooO:[I

.field private ooo0〇〇O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/webstorage/UploadFile;",
            ">;"
        }
    .end annotation
.end field

.field private o〇00O:Landroid/widget/Button;

.field private o〇oO:Landroid/widget/LinearLayout;

.field private 〇00O0:Landroid/widget/TextView;

.field private 〇080OO8〇0:Landroid/widget/Button;

.field private 〇08O〇00〇o:Landroid/widget/Button;

.field private 〇08〇o0O:Landroid/view/View;

.field private 〇0O:Landroid/widget/Button;

.field private 〇0O〇O00O:Lcom/intsig/webstorage/WebStorageApi;

.field private 〇8〇oO〇〇8o:Z

.field private 〇OO8ooO8〇:[Ljava/lang/String;

.field private 〇OOo8〇0:Landroid/widget/Button;

.field public 〇OO〇00〇0O:Landroid/os/Handler;

.field private 〇O〇〇O8:J

.field private 〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

.field private 〇〇08O:[I

.field private 〇〇o〇:Landroid/widget/TextView;

.field private 〇〇〇0o〇〇0:Landroidx/fragment/app/DialogFragment;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x2

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oOo0:I

    .line 6
    .line 7
    const/4 v0, 0x3

    .line 8
    iput v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->OO〇00〇8oO:I

    .line 9
    .line 10
    const/4 v1, 0x4

    .line 11
    iput v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o8〇OO0〇0o:I

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    iput-boolean v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇8〇oO〇〇8o:Z

    .line 15
    .line 16
    new-instance v1, Ljava/util/ArrayList;

    .line 17
    .line 18
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 19
    .line 20
    .line 21
    iput-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 22
    .line 23
    const/4 v1, 0x1

    .line 24
    iput-boolean v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oOO〇〇:Z

    .line 25
    .line 26
    const-string v1, ""

    .line 27
    .line 28
    iput-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oo8ooo8O:Ljava/lang/String;

    .line 29
    .line 30
    const/4 v1, 0x0

    .line 31
    iput-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇OO8ooO8〇:[Ljava/lang/String;

    .line 32
    .line 33
    new-array v0, v0, [I

    .line 34
    .line 35
    fill-array-data v0, :array_0

    .line 36
    .line 37
    .line 38
    iput-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->ooO:[I

    .line 39
    .line 40
    new-instance v0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment$1;

    .line 41
    .line 42
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    invoke-direct {v0, p0, v2}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment$1;-><init>(Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;Landroid/os/Looper;)V

    .line 47
    .line 48
    .line 49
    iput-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇OO〇00〇0O:Landroid/os/Handler;

    .line 50
    .line 51
    iput-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇〇〇0o〇〇0:Landroidx/fragment/app/DialogFragment;

    .line 52
    .line 53
    return-void

    .line 54
    nop

    .line 55
    :array_0
    .array-data 4
        0x65
        0x66
        0x67
    .end array-data
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic O0〇0(Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;Landroid/widget/Button;ILjava/lang/String;Landroid/widget/TextView;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇0ooOOo(Landroid/widget/Button;ILjava/lang/String;Landroid/widget/TextView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method private Ooo8o()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇〇〇00()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->OO:Landroid/widget/Button;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇OO8ooO8〇:[Ljava/lang/String;

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    aget-object v1, v1, v2

    .line 10
    .line 11
    iget-object v3, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->Oo80:Landroid/widget/TextView;

    .line 12
    .line 13
    invoke-direct {p0, v0, v2, v1, v3}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇0ooOOo(Landroid/widget/Button;ILjava/lang/String;Landroid/widget/TextView;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇08O〇00〇o:Landroid/widget/Button;

    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇OO8ooO8〇:[Ljava/lang/String;

    .line 19
    .line 20
    const/4 v2, 0x1

    .line 21
    aget-object v1, v1, v2

    .line 22
    .line 23
    iget-object v3, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇〇o〇:Landroid/widget/TextView;

    .line 24
    .line 25
    invoke-direct {p0, v0, v2, v1, v3}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇0ooOOo(Landroid/widget/Button;ILjava/lang/String;Landroid/widget/TextView;)V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇00O:Landroid/widget/Button;

    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇OO8ooO8〇:[Ljava/lang/String;

    .line 31
    .line 32
    const/4 v2, 0x2

    .line 33
    aget-object v1, v1, v2

    .line 34
    .line 35
    iget-object v3, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O〇o88o08〇:Landroid/widget/TextView;

    .line 36
    .line 37
    invoke-direct {p0, v0, v2, v1, v3}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇0ooOOo(Landroid/widget/Button;ILjava/lang/String;Landroid/widget/TextView;)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O8o08O8O:Landroid/widget/Button;

    .line 41
    .line 42
    new-instance v1, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment$2;

    .line 43
    .line 44
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment$2;-><init>(Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;)V

    .line 45
    .line 46
    .line 47
    const-wide/16 v2, 0x1f4

    .line 48
    .line 49
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 50
    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇080OO8〇0:Landroid/widget/Button;

    .line 53
    .line 54
    iget-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇OO8ooO8〇:[Ljava/lang/String;

    .line 55
    .line 56
    const/4 v2, 0x4

    .line 57
    aget-object v1, v1, v2

    .line 58
    .line 59
    iget-object v3, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O〇08oOOO0:Landroid/widget/TextView;

    .line 60
    .line 61
    invoke-direct {p0, v0, v2, v1, v3}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇0ooOOo(Landroid/widget/Button;ILjava/lang/String;Landroid/widget/TextView;)V

    .line 62
    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oOo〇8o008:Landroid/widget/Button;

    .line 65
    .line 66
    iget-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇OO8ooO8〇:[Ljava/lang/String;

    .line 67
    .line 68
    const/4 v2, 0x5

    .line 69
    aget-object v1, v1, v2

    .line 70
    .line 71
    iget-object v3, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o8〇OO:Landroid/widget/TextView;

    .line 72
    .line 73
    invoke-direct {p0, v0, v2, v1, v3}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇0ooOOo(Landroid/widget/Button;ILjava/lang/String;Landroid/widget/TextView;)V

    .line 74
    .line 75
    .line 76
    invoke-direct {p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o08()V

    .line 77
    .line 78
    .line 79
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic o00〇88〇08(Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇8〇80o(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic o880(Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o〇88〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private oOoO8OO〇()V
    .locals 9

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o8o:I

    .line 2
    .line 3
    const/4 v1, 0x4

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 7
    .line 8
    check-cast v0, Lcom/intsig/camscanner/uploadfaxprint/UploadFaxPrintActivity;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFaxPrintActivity;->O〇080〇o0()Lcom/intsig/camscanner/fragment/PadSendingDocInfo;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_2

    .line 15
    .line 16
    iget-wide v1, v0, Lcom/intsig/camscanner/fragment/PadSendingDocInfo;->o0:J

    .line 17
    .line 18
    iput-wide v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇O〇〇O8:J

    .line 19
    .line 20
    iget-object v1, v0, Lcom/intsig/camscanner/fragment/PadSendingDocInfo;->O8o08O8O:[I

    .line 21
    .line 22
    iput-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇〇08O:[I

    .line 23
    .line 24
    iget v1, v0, Lcom/intsig/camscanner/fragment/PadSendingDocInfo;->o〇00O:I

    .line 25
    .line 26
    iput v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O0O:I

    .line 27
    .line 28
    iget v1, v0, Lcom/intsig/camscanner/fragment/PadSendingDocInfo;->〇08O〇00〇o:I

    .line 29
    .line 30
    iput v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o8oOOo:I

    .line 31
    .line 32
    iget-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 33
    .line 34
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 35
    .line 36
    .line 37
    iget-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 38
    .line 39
    new-instance v8, Lcom/intsig/webstorage/UploadFile;

    .line 40
    .line 41
    iget-wide v3, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇O〇〇O8:J

    .line 42
    .line 43
    const/4 v5, 0x0

    .line 44
    iget-object v6, v0, Lcom/intsig/camscanner/fragment/PadSendingDocInfo;->〇OOo8〇0:Ljava/lang/String;

    .line 45
    .line 46
    const/4 v7, 0x0

    .line 47
    move-object v2, v8

    .line 48
    invoke-direct/range {v2 .. v7}, Lcom/intsig/webstorage/UploadFile;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    const/4 v1, 0x5

    .line 56
    if-ne v0, v1, :cond_2

    .line 57
    .line 58
    iget-boolean v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oOO〇〇:Z

    .line 59
    .line 60
    if-eqz v0, :cond_1

    .line 61
    .line 62
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 63
    .line 64
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    const-string v1, "ids"

    .line 69
    .line 70
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    check-cast v0, Ljava/util/ArrayList;

    .line 75
    .line 76
    iput-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 80
    .line 81
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 82
    .line 83
    .line 84
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 85
    .line 86
    check-cast v0, Lcom/intsig/camscanner/uploadfaxprint/UploadFaxPrintActivity;

    .line 87
    .line 88
    invoke-virtual {v0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFaxPrintActivity;->O〇0O〇Oo〇o()Ljava/util/ArrayList;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    iput-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 93
    .line 94
    :cond_2
    :goto_0
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method static bridge synthetic oOo〇08〇(Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;)Lcom/intsig/mvp/activity/BaseChangeActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic oO〇oo(Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇00O0:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic oooO888(Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇088O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private o〇0〇o()Z
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oOoO8OO〇()V

    .line 2
    .line 3
    .line 4
    iget v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o8o:I

    .line 5
    .line 6
    const/4 v1, 0x4

    .line 7
    const/4 v2, 0x0

    .line 8
    const/4 v3, 0x1

    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    iget v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O0O:I

    .line 12
    .line 13
    if-ge v0, v3, :cond_1

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 16
    .line 17
    const v1, 0x7f130299

    .line 18
    .line 19
    .line 20
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 21
    .line 22
    .line 23
    return v2

    .line 24
    :cond_0
    const/4 v1, 0x5

    .line 25
    if-ne v0, v1, :cond_1

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-ge v0, v3, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 36
    .line 37
    const v1, 0x7f131e27

    .line 38
    .line 39
    .line 40
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 41
    .line 42
    .line 43
    return v2

    .line 44
    :cond_1
    return v3
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private o〇O8OO(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Lcom/intsig/webstorage/WebStorageAccount;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/webstorage/UploadFile;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/intsig/webstorage/WebStorageAccount;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-static {p2, p3, p4}, Lcom/intsig/camscanner/service/UploadUtils;->〇0〇O0088o(Ljava/util/List;Ljava/lang/String;Lcom/intsig/webstorage/WebStorageAccount;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 5
    .line 6
    .line 7
    move-result-object p2

    .line 8
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 9
    .line 10
    .line 11
    move-result p3

    .line 12
    if-eqz p3, :cond_0

    .line 13
    .line 14
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object p3

    .line 18
    check-cast p3, Lcom/intsig/webstorage/UploadFile;

    .line 19
    .line 20
    new-instance p4, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v0, "doUpload() "

    .line 26
    .line 27
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {p3}, Lcom/intsig/webstorage/UploadFile;->〇o〇()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    const-string v0, ","

    .line 38
    .line 39
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {p3}, Lcom/intsig/webstorage/UploadFile;->〇o00〇〇Oo()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p3

    .line 46
    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object p3

    .line 53
    const-string p4, "UploadFragment"

    .line 54
    .line 55
    invoke-static {p4, p3}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_0
    if-eqz p1, :cond_1

    .line 60
    .line 61
    new-instance p2, Landroid/content/Intent;

    .line 62
    .line 63
    const-class p3, Lcom/intsig/camscanner/uploadfaxprint/TaskStateActivity;

    .line 64
    .line 65
    invoke-direct {p2, p1, p3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 66
    .line 67
    .line 68
    const-string p3, "task_type"

    .line 69
    .line 70
    const/4 p4, 0x0

    .line 71
    invoke-virtual {p2, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 72
    .line 73
    .line 74
    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 75
    .line 76
    .line 77
    :cond_1
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private showDialog(I)V
    .locals 2

    .line 1
    const-string v0, "UploadFragment"

    .line 2
    .line 3
    :try_start_0
    invoke-static {p1}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment$MyDialogFragment;->〇80O8o8O〇(I)Lcom/intsig/camscanner/uploadfaxprint/UploadFragment$MyDialogFragment;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇〇〇0o〇〇0:Landroidx/fragment/app/DialogFragment;

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    invoke-virtual {p1, p0, v1}, Landroidx/fragment/app/Fragment;->setTargetFragment(Landroidx/fragment/app/Fragment;I)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇〇〇0o〇〇0:Landroidx/fragment/app/DialogFragment;

    .line 14
    .line 15
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {p1, v1, v0}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :catch_0
    move-exception p1

    .line 24
    const-string v1, "Exception"

    .line 25
    .line 26
    invoke-static {v0, v1, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 27
    .line 28
    .line 29
    :goto_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private 〇088O()V
    .locals 3

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇〇〇0o〇〇0:Landroidx/fragment/app/DialogFragment;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroidx/fragment/app/DialogFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4
    .line 5
    .line 6
    goto :goto_0

    .line 7
    :catch_0
    move-exception v0

    .line 8
    const-string v1, "UploadFragment"

    .line 9
    .line 10
    const-string v2, "Exception"

    .line 11
    .line 12
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    .line 14
    .line 15
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇0ooOOo(Landroid/widget/Button;ILjava/lang/String;Landroid/widget/TextView;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/webstorage/WebStorageAPIFactory;->〇o00〇〇Oo()Lcom/intsig/webstorage/WebStorageAPIFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 6
    .line 7
    invoke-virtual {v0, p2, v1}, Lcom/intsig/webstorage/WebStorageAPIFactory;->〇080(ILandroid/content/Context;)Lcom/intsig/webstorage/WebStorageApi;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇0O〇O00O:Lcom/intsig/webstorage/WebStorageApi;

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string p1, "UploadFragment"

    .line 16
    .line 17
    const-string p2, "webStorageApi == null"

    .line 18
    .line 19
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    goto :goto_2

    .line 23
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/webstorage/WebStorageApi;->〇80〇808〇O()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_3

    .line 28
    .line 29
    iget-object p2, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇0O〇O00O:Lcom/intsig/webstorage/WebStorageApi;

    .line 30
    .line 31
    invoke-virtual {p2}, Lcom/intsig/webstorage/WebStorageApi;->oO80()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    iput-object p2, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oO〇8O8oOo:Ljava/lang/String;

    .line 36
    .line 37
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 38
    .line 39
    .line 40
    move-result p2

    .line 41
    if-nez p2, :cond_1

    .line 42
    .line 43
    iget-object p2, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oO〇8O8oOo:Ljava/lang/String;

    .line 44
    .line 45
    invoke-virtual {p4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_1
    invoke-virtual {p4, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    .line 51
    .line 52
    :goto_0
    iget-boolean p2, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇8〇oO〇〇8o:Z

    .line 53
    .line 54
    if-eqz p2, :cond_2

    .line 55
    .line 56
    const p2, 0x7f131f87

    .line 57
    .line 58
    .line 59
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 60
    .line 61
    .line 62
    const/4 p2, 0x4

    .line 63
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 64
    .line 65
    .line 66
    move-result-object p2

    .line 67
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 68
    .line 69
    .line 70
    goto :goto_2

    .line 71
    :cond_2
    const p2, 0x7f130006

    .line 72
    .line 73
    .line 74
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 75
    .line 76
    .line 77
    const/4 p2, 0x3

    .line 78
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 79
    .line 80
    .line 81
    move-result-object p2

    .line 82
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 83
    .line 84
    .line 85
    goto :goto_2

    .line 86
    :cond_3
    invoke-virtual {p4, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    .line 88
    .line 89
    if-nez p2, :cond_4

    .line 90
    .line 91
    const p2, 0x7f131120

    .line 92
    .line 93
    .line 94
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 95
    .line 96
    .line 97
    goto :goto_1

    .line 98
    :cond_4
    const p2, 0x7f1304b1

    .line 99
    .line 100
    .line 101
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 102
    .line 103
    .line 104
    :goto_1
    const/4 p2, 0x2

    .line 105
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 106
    .line 107
    .line 108
    move-result-object p2

    .line 109
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 110
    .line 111
    .line 112
    :goto_2
    return-void
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private 〇0〇0(Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/webstorage/UploadFile;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "UploadFragment"

    .line 2
    .line 3
    if-eqz p1, :cond_5

    .line 4
    .line 5
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    goto/16 :goto_3

    .line 12
    .line 13
    :cond_0
    iget v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 14
    .line 15
    const/4 v2, 0x5

    .line 16
    const/4 v3, 0x1

    .line 17
    const/4 v4, 0x0

    .line 18
    if-ne v1, v2, :cond_3

    .line 19
    .line 20
    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    const-wide/16 v5, 0x0

    .line 25
    .line 26
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-eqz v2, :cond_2

    .line 31
    .line 32
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    check-cast v2, Lcom/intsig/webstorage/UploadFile;

    .line 37
    .line 38
    if-eqz v2, :cond_1

    .line 39
    .line 40
    iget-object v7, v2, Lcom/intsig/webstorage/UploadFile;->〇OOo8〇0:Ljava/lang/String;

    .line 41
    .line 42
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 43
    .line 44
    .line 45
    move-result v7

    .line 46
    if-nez v7, :cond_1

    .line 47
    .line 48
    new-instance v7, Ljava/io/File;

    .line 49
    .line 50
    iget-object v2, v2, Lcom/intsig/webstorage/UploadFile;->〇OOo8〇0:Ljava/lang/String;

    .line 51
    .line 52
    invoke-direct {v7, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v7}, Ljava/io/File;->length()J

    .line 56
    .line 57
    .line 58
    move-result-wide v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    add-long/2addr v5, v7

    .line 60
    goto :goto_0

    .line 61
    :cond_2
    const-wide/32 v0, 0x1800000

    .line 62
    .line 63
    .line 64
    cmp-long v2, v5, v0

    .line 65
    .line 66
    if-lez v2, :cond_3

    .line 67
    .line 68
    const/4 v0, 0x1

    .line 69
    goto :goto_1

    .line 70
    :catch_0
    move-exception v1

    .line 71
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 72
    .line 73
    .line 74
    :cond_3
    const/4 v0, 0x0

    .line 75
    :goto_1
    if-eqz v0, :cond_4

    .line 76
    .line 77
    new-instance p1, Lcom/intsig/app/AlertDialog$Builder;

    .line 78
    .line 79
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 80
    .line 81
    invoke-direct {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 82
    .line 83
    .line 84
    const v0, 0x7f131d10

    .line 85
    .line 86
    .line 87
    invoke-virtual {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    new-array v0, v3, [Ljava/lang/Object;

    .line 92
    .line 93
    const/16 v1, 0x19

    .line 94
    .line 95
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 96
    .line 97
    .line 98
    move-result-object v1

    .line 99
    aput-object v1, v0, v4

    .line 100
    .line 101
    const v1, 0x7f1302a2

    .line 102
    .line 103
    .line 104
    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    invoke-virtual {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 109
    .line 110
    .line 111
    move-result-object p1

    .line 112
    const v0, 0x7f130019

    .line 113
    .line 114
    .line 115
    const/4 v1, 0x0

    .line 116
    invoke-virtual {p1, v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 117
    .line 118
    .line 119
    move-result-object p1

    .line 120
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 121
    .line 122
    .line 123
    move-result-object p1

    .line 124
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 125
    .line 126
    .line 127
    goto :goto_2

    .line 128
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 129
    .line 130
    iget-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oo8ooo8O:Ljava/lang/String;

    .line 131
    .line 132
    new-instance v2, Lcom/intsig/webstorage/WebStorageAccount;

    .line 133
    .line 134
    iget v3, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 135
    .line 136
    invoke-direct {v2, v3}, Lcom/intsig/webstorage/WebStorageAccount;-><init>(I)V

    .line 137
    .line 138
    .line 139
    invoke-direct {p0, v0, p1, v1, v2}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇O8OO(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Lcom/intsig/webstorage/WebStorageAccount;)V

    .line 140
    .line 141
    .line 142
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 143
    .line 144
    .line 145
    move-result-object p1

    .line 146
    const v0, 0x7f131f86

    .line 147
    .line 148
    .line 149
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 150
    .line 151
    .line 152
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 153
    .line 154
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 155
    .line 156
    .line 157
    :goto_2
    return-void

    .line 158
    :cond_5
    :goto_3
    const-string p1, "uploadFiles is empty"

    .line 159
    .line 160
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    return-void
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method static bridge synthetic 〇80O8o8O〇(Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;)[Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇OO8ooO8〇:[Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇8〇80o(I)V
    .locals 10

    .line 1
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->〇〇888()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 6
    .line 7
    const-string v2, "upload"

    .line 8
    .line 9
    invoke-static {v2, v0, v1}, Lcom/intsig/crashapm/log/FabricUtils;->oO80(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 10
    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    const/4 v1, 0x5

    .line 14
    if-nez p1, :cond_1

    .line 15
    .line 16
    new-instance v6, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment$3;

    .line 17
    .line 18
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment$3;-><init>(Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;)V

    .line 19
    .line 20
    .line 21
    iget p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o8o:I

    .line 22
    .line 23
    if-ne p1, v1, :cond_0

    .line 24
    .line 25
    new-instance p1, Lcom/intsig/camscanner/service/UploadZipTask;

    .line 26
    .line 27
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    iget-object v2, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 32
    .line 33
    invoke-direct {p1, v1, v2, v6}, Lcom/intsig/camscanner/service/UploadZipTask;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/service/UploadZipTask$ZipCallback;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    new-instance p1, Lcom/intsig/camscanner/service/UploadZipTask;

    .line 38
    .line 39
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    iget-object v4, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 44
    .line 45
    iget-object v5, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇〇08O:[I

    .line 46
    .line 47
    iget-boolean v7, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->Oo0〇Ooo:Z

    .line 48
    .line 49
    move-object v2, p1

    .line 50
    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/service/UploadZipTask;-><init>(Landroid/content/Context;Ljava/util/ArrayList;[ILcom/intsig/camscanner/service/UploadZipTask$ZipCallback;Z)V

    .line 51
    .line 52
    .line 53
    :goto_0
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    new-array v0, v0, [Ljava/lang/Long;

    .line 58
    .line 59
    invoke-virtual {p1, v1, v0}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 60
    .line 61
    .line 62
    goto :goto_2

    .line 63
    :cond_1
    new-instance v8, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment$4;

    .line 64
    .line 65
    invoke-direct {v8, p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment$4;-><init>(Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;)V

    .line 66
    .line 67
    .line 68
    iget p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o8o:I

    .line 69
    .line 70
    if-ne p1, v1, :cond_2

    .line 71
    .line 72
    new-instance p1, Lcom/intsig/camscanner/service/UploadPdfTask;

    .line 73
    .line 74
    iget-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 75
    .line 76
    iget-object v2, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 77
    .line 78
    invoke-direct {p1, v1, v2, v8}, Lcom/intsig/camscanner/service/UploadPdfTask;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/service/UploadPdfTask$PdfCallback;)V

    .line 79
    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_2
    new-instance p1, Lcom/intsig/camscanner/service/UploadPdfTask;

    .line 83
    .line 84
    iget-object v3, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 85
    .line 86
    iget-object v4, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 87
    .line 88
    iget-wide v5, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇O〇〇O8:J

    .line 89
    .line 90
    iget-object v7, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇〇08O:[I

    .line 91
    .line 92
    iget-boolean v9, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->Oo0〇Ooo:Z

    .line 93
    .line 94
    move-object v2, p1

    .line 95
    invoke-direct/range {v2 .. v9}, Lcom/intsig/camscanner/service/UploadPdfTask;-><init>(Landroid/content/Context;Ljava/util/ArrayList;J[ILcom/intsig/camscanner/service/UploadPdfTask$PdfCallback;Z)V

    .line 96
    .line 97
    .line 98
    :goto_1
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    new-array v0, v0, [Ljava/lang/Long;

    .line 103
    .line 104
    invoke-virtual {p1, v1, v0}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 105
    .line 106
    .line 107
    :goto_2
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method static bridge synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->showDialog(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇O8oOo0(Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;)Landroid/widget/Button;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O8o08O8O:Landroid/widget/Button;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇o08()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 2
    .line 3
    const v1, 0x7f0a0b0b

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const/16 v1, 0x8

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇o〇88〇8()V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "Sign out mSelectedAccount = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "UploadFragment"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 26
    .line 27
    iget v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 28
    .line 29
    invoke-static {v0, v1}, Lcom/intsig/camscanner/service/UploadUtils;->〇80〇808〇O(Landroid/content/Context;I)V

    .line 30
    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->Ooo8o()V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic 〇〇o0〇8(Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇0〇0(Ljava/util/ArrayList;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .line 1
    const-string v0, "UploadFragment"

    .line 2
    .line 3
    const-string v1, "onAttach()"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    move-object v0, p1

    .line 9
    check-cast v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 12
    .line 13
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const v1, 0x7f0a0042

    .line 6
    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    const/4 v3, 0x0

    .line 10
    const v4, 0x7f13008d

    .line 11
    .line 12
    .line 13
    const/16 v5, 0x130

    .line 14
    .line 15
    const/16 v6, 0x12e

    .line 16
    .line 17
    const/4 v7, 0x3

    .line 18
    const/4 v8, 0x2

    .line 19
    const/4 v9, 0x4

    .line 20
    const-string v10, "UploadFragment"

    .line 21
    .line 22
    if-ne v0, v1, :cond_3

    .line 23
    .line 24
    iput v3, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->OO:Landroid/widget/Button;

    .line 27
    .line 28
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    check-cast p1, Ljava/lang/Integer;

    .line 33
    .line 34
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    if-ne p1, v7, :cond_0

    .line 39
    .line 40
    invoke-direct {p0, v6}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->showDialog(I)V

    .line 41
    .line 42
    .line 43
    const-string p1, "Clear gdoc"

    .line 44
    .line 45
    invoke-static {v10, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    goto/16 :goto_0

    .line 49
    .line 50
    :cond_0
    if-ne p1, v9, :cond_1

    .line 51
    .line 52
    iput-object v2, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oo8ooo8O:Ljava/lang/String;

    .line 53
    .line 54
    invoke-direct {p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇0〇o()Z

    .line 55
    .line 56
    .line 57
    move-result p1

    .line 58
    if-eqz p1, :cond_1d

    .line 59
    .line 60
    invoke-direct {p0, v5}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->showDialog(I)V

    .line 61
    .line 62
    .line 63
    const-string p1, "GDoc Upload"

    .line 64
    .line 65
    invoke-static {v10, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    goto/16 :goto_0

    .line 69
    .line 70
    :cond_1
    if-ne p1, v8, :cond_1d

    .line 71
    .line 72
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 73
    .line 74
    invoke-static {p1}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    if-eqz p1, :cond_2

    .line 79
    .line 80
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 81
    .line 82
    invoke-static {p1, v3}, Lcom/intsig/camscanner/service/UploadUtils;->〇〇888(Landroid/content/Context;I)V

    .line 83
    .line 84
    .line 85
    const-string p1, "Gdoc Login"

    .line 86
    .line 87
    invoke-static {v10, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    goto/16 :goto_0

    .line 91
    .line 92
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 93
    .line 94
    invoke-static {p1, v4}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 95
    .line 96
    .line 97
    goto/16 :goto_0

    .line 98
    .line 99
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 100
    .line 101
    .line 102
    move-result v0

    .line 103
    const v1, 0x7f0a003f

    .line 104
    .line 105
    .line 106
    if-ne v0, v1, :cond_7

    .line 107
    .line 108
    const/4 p1, 0x1

    .line 109
    iput p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 110
    .line 111
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇08O〇00〇o:Landroid/widget/Button;

    .line 112
    .line 113
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    check-cast p1, Ljava/lang/Integer;

    .line 118
    .line 119
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 120
    .line 121
    .line 122
    move-result p1

    .line 123
    if-ne p1, v7, :cond_4

    .line 124
    .line 125
    invoke-direct {p0, v6}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->showDialog(I)V

    .line 126
    .line 127
    .line 128
    return-void

    .line 129
    :cond_4
    if-ne p1, v9, :cond_5

    .line 130
    .line 131
    const-string p1, "0"

    .line 132
    .line 133
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oo8ooo8O:Ljava/lang/String;

    .line 134
    .line 135
    invoke-direct {p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇0〇o()Z

    .line 136
    .line 137
    .line 138
    move-result p1

    .line 139
    if-eqz p1, :cond_1d

    .line 140
    .line 141
    invoke-direct {p0, v5}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->showDialog(I)V

    .line 142
    .line 143
    .line 144
    const-string p1, "Box Upload"

    .line 145
    .line 146
    invoke-static {v10, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    .line 148
    .line 149
    goto/16 :goto_0

    .line 150
    .line 151
    :cond_5
    if-ne p1, v8, :cond_1d

    .line 152
    .line 153
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 154
    .line 155
    invoke-static {p1}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 156
    .line 157
    .line 158
    move-result p1

    .line 159
    if-eqz p1, :cond_6

    .line 160
    .line 161
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 162
    .line 163
    iget v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 164
    .line 165
    invoke-static {p1, v0}, Lcom/intsig/camscanner/service/UploadUtils;->〇〇888(Landroid/content/Context;I)V

    .line 166
    .line 167
    .line 168
    goto/16 :goto_0

    .line 169
    .line 170
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 171
    .line 172
    invoke-static {p1, v4}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 173
    .line 174
    .line 175
    goto/16 :goto_0

    .line 176
    .line 177
    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 178
    .line 179
    .line 180
    move-result v0

    .line 181
    const v1, 0x7f0a0040

    .line 182
    .line 183
    .line 184
    if-ne v0, v1, :cond_b

    .line 185
    .line 186
    iput v8, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 187
    .line 188
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇00O:Landroid/widget/Button;

    .line 189
    .line 190
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 191
    .line 192
    .line 193
    move-result-object p1

    .line 194
    check-cast p1, Ljava/lang/Integer;

    .line 195
    .line 196
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 197
    .line 198
    .line 199
    move-result p1

    .line 200
    if-ne p1, v7, :cond_8

    .line 201
    .line 202
    invoke-direct {p0, v6}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->showDialog(I)V

    .line 203
    .line 204
    .line 205
    return-void

    .line 206
    :cond_8
    if-ne p1, v9, :cond_9

    .line 207
    .line 208
    const-string p1, "/"

    .line 209
    .line 210
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oo8ooo8O:Ljava/lang/String;

    .line 211
    .line 212
    invoke-direct {p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇0〇o()Z

    .line 213
    .line 214
    .line 215
    move-result p1

    .line 216
    if-eqz p1, :cond_1d

    .line 217
    .line 218
    invoke-direct {p0, v5}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->showDialog(I)V

    .line 219
    .line 220
    .line 221
    const-string p1, "Dropbox Upload"

    .line 222
    .line 223
    invoke-static {v10, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    .line 225
    .line 226
    goto/16 :goto_0

    .line 227
    .line 228
    :cond_9
    if-ne p1, v8, :cond_1d

    .line 229
    .line 230
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 231
    .line 232
    invoke-static {p1}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 233
    .line 234
    .line 235
    move-result p1

    .line 236
    if-eqz p1, :cond_a

    .line 237
    .line 238
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 239
    .line 240
    iget v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 241
    .line 242
    invoke-static {p1, v0}, Lcom/intsig/camscanner/service/UploadUtils;->〇〇888(Landroid/content/Context;I)V

    .line 243
    .line 244
    .line 245
    goto/16 :goto_0

    .line 246
    .line 247
    :cond_a
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 248
    .line 249
    invoke-static {p1, v4}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 250
    .line 251
    .line 252
    goto/16 :goto_0

    .line 253
    .line 254
    :cond_b
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 255
    .line 256
    .line 257
    move-result v0

    .line 258
    const v1, 0x7f0a0041

    .line 259
    .line 260
    .line 261
    if-ne v0, v1, :cond_f

    .line 262
    .line 263
    iput v7, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 264
    .line 265
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O8o08O8O:Landroid/widget/Button;

    .line 266
    .line 267
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 268
    .line 269
    .line 270
    move-result-object p1

    .line 271
    check-cast p1, Ljava/lang/Integer;

    .line 272
    .line 273
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 274
    .line 275
    .line 276
    move-result p1

    .line 277
    if-ne p1, v9, :cond_c

    .line 278
    .line 279
    iput-object v2, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oo8ooo8O:Ljava/lang/String;

    .line 280
    .line 281
    invoke-direct {p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇0〇o()Z

    .line 282
    .line 283
    .line 284
    move-result p1

    .line 285
    if-eqz p1, :cond_1d

    .line 286
    .line 287
    invoke-direct {p0, v5}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->showDialog(I)V

    .line 288
    .line 289
    .line 290
    goto/16 :goto_0

    .line 291
    .line 292
    :cond_c
    if-ne p1, v8, :cond_e

    .line 293
    .line 294
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 295
    .line 296
    invoke-static {p1}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 297
    .line 298
    .line 299
    move-result p1

    .line 300
    if-eqz p1, :cond_d

    .line 301
    .line 302
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 303
    .line 304
    iget v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 305
    .line 306
    invoke-static {p1, v0}, Lcom/intsig/camscanner/service/UploadUtils;->〇〇888(Landroid/content/Context;I)V

    .line 307
    .line 308
    .line 309
    goto/16 :goto_0

    .line 310
    .line 311
    :cond_d
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 312
    .line 313
    invoke-static {p1, v4}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 314
    .line 315
    .line 316
    goto/16 :goto_0

    .line 317
    .line 318
    :cond_e
    if-ne p1, v7, :cond_1d

    .line 319
    .line 320
    const-string p1, "Evernote SIGNOUT"

    .line 321
    .line 322
    invoke-static {v10, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    .line 324
    .line 325
    invoke-direct {p0, v6}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->showDialog(I)V

    .line 326
    .line 327
    .line 328
    goto/16 :goto_0

    .line 329
    .line 330
    :cond_f
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 331
    .line 332
    .line 333
    move-result v0

    .line 334
    const v1, 0x7f0a0043

    .line 335
    .line 336
    .line 337
    if-ne v0, v1, :cond_13

    .line 338
    .line 339
    iput v9, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 340
    .line 341
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇080OO8〇0:Landroid/widget/Button;

    .line 342
    .line 343
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 344
    .line 345
    .line 346
    move-result-object p1

    .line 347
    check-cast p1, Ljava/lang/Integer;

    .line 348
    .line 349
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 350
    .line 351
    .line 352
    move-result p1

    .line 353
    if-ne p1, v9, :cond_10

    .line 354
    .line 355
    const-string p1, "OneDrive UPLOAD"

    .line 356
    .line 357
    invoke-static {v10, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    .line 359
    .line 360
    iput-object v2, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oo8ooo8O:Ljava/lang/String;

    .line 361
    .line 362
    invoke-direct {p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇0〇o()Z

    .line 363
    .line 364
    .line 365
    move-result p1

    .line 366
    if-eqz p1, :cond_1d

    .line 367
    .line 368
    invoke-direct {p0, v5}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->showDialog(I)V

    .line 369
    .line 370
    .line 371
    goto/16 :goto_0

    .line 372
    .line 373
    :cond_10
    if-ne p1, v7, :cond_11

    .line 374
    .line 375
    const-string p1, "OneDrive SIGNOUT"

    .line 376
    .line 377
    invoke-static {v10, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    .line 379
    .line 380
    invoke-direct {p0, v6}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->showDialog(I)V

    .line 381
    .line 382
    .line 383
    goto/16 :goto_0

    .line 384
    .line 385
    :cond_11
    if-ne p1, v8, :cond_1d

    .line 386
    .line 387
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 388
    .line 389
    invoke-static {p1}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 390
    .line 391
    .line 392
    move-result p1

    .line 393
    if-eqz p1, :cond_12

    .line 394
    .line 395
    const-string p1, "OneDrive SIGNIN"

    .line 396
    .line 397
    invoke-static {v10, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    .line 399
    .line 400
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 401
    .line 402
    iget v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 403
    .line 404
    invoke-static {p1, v0}, Lcom/intsig/camscanner/service/UploadUtils;->〇〇888(Landroid/content/Context;I)V

    .line 405
    .line 406
    .line 407
    goto/16 :goto_0

    .line 408
    .line 409
    :cond_12
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 410
    .line 411
    invoke-static {p1, v4}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 412
    .line 413
    .line 414
    goto/16 :goto_0

    .line 415
    .line 416
    :cond_13
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 417
    .line 418
    .line 419
    move-result v0

    .line 420
    const v1, 0x7f0a0224

    .line 421
    .line 422
    .line 423
    if-ne v0, v1, :cond_17

    .line 424
    .line 425
    const/4 p1, 0x6

    .line 426
    iput p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 427
    .line 428
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇0O:Landroid/widget/Button;

    .line 429
    .line 430
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 431
    .line 432
    .line 433
    move-result-object p1

    .line 434
    check-cast p1, Ljava/lang/Integer;

    .line 435
    .line 436
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 437
    .line 438
    .line 439
    move-result p1

    .line 440
    if-ne p1, v7, :cond_14

    .line 441
    .line 442
    const-string p1, "baidu SIGNOUT"

    .line 443
    .line 444
    invoke-static {v10, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    .line 446
    .line 447
    invoke-direct {p0, v6}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->showDialog(I)V

    .line 448
    .line 449
    .line 450
    goto/16 :goto_0

    .line 451
    .line 452
    :cond_14
    if-ne p1, v9, :cond_15

    .line 453
    .line 454
    const-string p1, "/apps/\u626b\u63cf\u5168\u80fd\u738b/CamScanner"

    .line 455
    .line 456
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oo8ooo8O:Ljava/lang/String;

    .line 457
    .line 458
    invoke-direct {p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇0〇o()Z

    .line 459
    .line 460
    .line 461
    move-result p1

    .line 462
    if-eqz p1, :cond_1d

    .line 463
    .line 464
    invoke-direct {p0, v5}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->showDialog(I)V

    .line 465
    .line 466
    .line 467
    goto/16 :goto_0

    .line 468
    .line 469
    :cond_15
    if-ne p1, v8, :cond_1d

    .line 470
    .line 471
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 472
    .line 473
    invoke-static {p1}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 474
    .line 475
    .line 476
    move-result p1

    .line 477
    if-eqz p1, :cond_16

    .line 478
    .line 479
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 480
    .line 481
    iget v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 482
    .line 483
    invoke-static {p1, v0}, Lcom/intsig/camscanner/service/UploadUtils;->〇〇888(Landroid/content/Context;I)V

    .line 484
    .line 485
    .line 486
    goto/16 :goto_0

    .line 487
    .line 488
    :cond_16
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 489
    .line 490
    invoke-static {p1, v4}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 491
    .line 492
    .line 493
    goto/16 :goto_0

    .line 494
    .line 495
    :cond_17
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 496
    .line 497
    .line 498
    move-result v0

    .line 499
    const v1, 0x7f0a0044

    .line 500
    .line 501
    .line 502
    if-ne v0, v1, :cond_19

    .line 503
    .line 504
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇OOo8〇0:Landroid/widget/Button;

    .line 505
    .line 506
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 507
    .line 508
    .line 509
    move-result-object p1

    .line 510
    check-cast p1, Ljava/lang/Integer;

    .line 511
    .line 512
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 513
    .line 514
    .line 515
    move-result p1

    .line 516
    if-ne p1, v9, :cond_18

    .line 517
    .line 518
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 519
    .line 520
    invoke-static {p1}, Lcom/intsig/camscanner/app/AppUtil;->〇80〇808〇O(Landroid/content/Context;)V

    .line 521
    .line 522
    .line 523
    goto :goto_0

    .line 524
    :cond_18
    if-ne p1, v8, :cond_1d

    .line 525
    .line 526
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 527
    .line 528
    const/4 v0, -0x1

    .line 529
    invoke-static {p1, v0, v3}, Lcom/intsig/camscanner/app/AppUtil;->Oo8Oo00oo(Landroid/app/Activity;IZ)V

    .line 530
    .line 531
    .line 532
    goto :goto_0

    .line 533
    :cond_19
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 534
    .line 535
    .line 536
    move-result p1

    .line 537
    const v0, 0x7f0a0287

    .line 538
    .line 539
    .line 540
    if-ne p1, v0, :cond_1d

    .line 541
    .line 542
    const/4 p1, 0x5

    .line 543
    iput p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 544
    .line 545
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oOo〇8o008:Landroid/widget/Button;

    .line 546
    .line 547
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 548
    .line 549
    .line 550
    move-result-object p1

    .line 551
    check-cast p1, Ljava/lang/Integer;

    .line 552
    .line 553
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 554
    .line 555
    .line 556
    move-result p1

    .line 557
    if-ne p1, v7, :cond_1a

    .line 558
    .line 559
    invoke-direct {p0, v6}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->showDialog(I)V

    .line 560
    .line 561
    .line 562
    goto :goto_0

    .line 563
    :cond_1a
    if-ne p1, v9, :cond_1b

    .line 564
    .line 565
    const-string p1, ""

    .line 566
    .line 567
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oo8ooo8O:Ljava/lang/String;

    .line 568
    .line 569
    invoke-direct {p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇0〇o()Z

    .line 570
    .line 571
    .line 572
    move-result p1

    .line 573
    if-eqz p1, :cond_1d

    .line 574
    .line 575
    invoke-direct {p0, v5}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->showDialog(I)V

    .line 576
    .line 577
    .line 578
    goto :goto_0

    .line 579
    :cond_1b
    if-ne p1, v8, :cond_1d

    .line 580
    .line 581
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 582
    .line 583
    invoke-static {p1}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 584
    .line 585
    .line 586
    move-result p1

    .line 587
    if-eqz p1, :cond_1c

    .line 588
    .line 589
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 590
    .line 591
    iget v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o0:I

    .line 592
    .line 593
    invoke-static {p1, v0}, Lcom/intsig/camscanner/service/UploadUtils;->〇〇888(Landroid/content/Context;I)V

    .line 594
    .line 595
    .line 596
    goto :goto_0

    .line 597
    :cond_1c
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 598
    .line 599
    invoke-static {p1, v4}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 600
    .line 601
    .line 602
    :cond_1d
    :goto_0
    return-void
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .line 1
    const-string p2, "onCreateView()"

    .line 2
    .line 3
    const-string p3, "UploadFragment"

    .line 4
    .line 5
    invoke-static {p3, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {p3}, Lcom/intsig/camscanner/CustomExceptionHandler;->O8(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    sget-boolean p2, Lcom/intsig/camscanner/app/AppConfig;->〇080:Z

    .line 12
    .line 13
    iput-boolean p2, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oOO〇〇:Z

    .line 14
    .line 15
    iget-object p2, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 16
    .line 17
    invoke-static {p2}, Lcom/intsig/webstorage/WebStorageAPIFactory;->〇o〇(Landroid/content/Context;)[Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p2

    .line 21
    iput-object p2, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇OO8ooO8〇:[Ljava/lang/String;

    .line 22
    .line 23
    iget-object p2, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 24
    .line 25
    invoke-virtual {p2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 26
    .line 27
    .line 28
    move-result-object p2

    .line 29
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    const-string v1, "android.intent.action.SEND"

    .line 34
    .line 35
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    const/4 v1, 0x1

    .line 40
    const/4 v2, 0x0

    .line 41
    if-eqz v0, :cond_0

    .line 42
    .line 43
    iput-boolean v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇8〇oO〇〇8o:Z

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_0
    iput-boolean v2, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇8〇oO〇〇8o:Z

    .line 47
    .line 48
    :goto_0
    const-string v0, "doc_id"

    .line 49
    .line 50
    const-wide/16 v3, -0x1

    .line 51
    .line 52
    invoke-virtual {p2, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 53
    .line 54
    .line 55
    move-result-wide v5

    .line 56
    iput-wide v5, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇O〇〇O8:J

    .line 57
    .line 58
    const-string v0, "is_need_suffix"

    .line 59
    .line 60
    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    iput-boolean v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->Oo0〇Ooo:Z

    .line 65
    .line 66
    iget-wide v5, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇O〇〇O8:J

    .line 67
    .line 68
    cmp-long v0, v5, v3

    .line 69
    .line 70
    if-eqz v0, :cond_2

    .line 71
    .line 72
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 73
    .line 74
    invoke-static {v0, v5, v6}, Lcom/intsig/camscanner/tsapp/collaborate/CollaborateUtil;->O8(Landroid/content/Context;J)I

    .line 75
    .line 76
    .line 77
    move-result v0

    .line 78
    if-ne v0, v1, :cond_1

    .line 79
    .line 80
    goto :goto_1

    .line 81
    :cond_1
    const/4 v1, 0x0

    .line 82
    :goto_1
    move v2, v1

    .line 83
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    .line 84
    .line 85
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .line 87
    .line 88
    const-string v1, "mIsCollaborateDoc = "

    .line 89
    .line 90
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    const-string v1, ", docId = "

    .line 97
    .line 98
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    iget-wide v3, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇O〇〇O8:J

    .line 102
    .line 103
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    invoke-static {p3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    const p3, 0x7f0d072b

    .line 114
    .line 115
    .line 116
    const/4 v0, 0x0

    .line 117
    invoke-virtual {p1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 118
    .line 119
    .line 120
    move-result-object p1

    .line 121
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 122
    .line 123
    const p3, 0x7f0a0224

    .line 124
    .line 125
    .line 126
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 127
    .line 128
    .line 129
    move-result-object p1

    .line 130
    check-cast p1, Landroid/widget/Button;

    .line 131
    .line 132
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇0O:Landroid/widget/Button;

    .line 133
    .line 134
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 135
    .line 136
    const p3, 0x7f0a1287

    .line 137
    .line 138
    .line 139
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 140
    .line 141
    .line 142
    move-result-object p1

    .line 143
    check-cast p1, Landroid/widget/TextView;

    .line 144
    .line 145
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->Ooo08:Landroid/widget/TextView;

    .line 146
    .line 147
    sget-boolean p1, Lcom/intsig/camscanner/app/AppSwitch;->OO0o〇〇〇〇0:Z

    .line 148
    .line 149
    const/16 p3, 0x8

    .line 150
    .line 151
    if-eqz p1, :cond_3

    .line 152
    .line 153
    invoke-static {}, Lcom/intsig/camscanner/app/AppUtil;->〇8()Z

    .line 154
    .line 155
    .line 156
    move-result p1

    .line 157
    if-eqz p1, :cond_4

    .line 158
    .line 159
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 160
    .line 161
    const v0, 0x7f0a0b56

    .line 162
    .line 163
    .line 164
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 165
    .line 166
    .line 167
    move-result-object p1

    .line 168
    invoke-virtual {p1, p3}, Landroid/view/View;->setVisibility(I)V

    .line 169
    .line 170
    .line 171
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 172
    .line 173
    const v0, 0x7f0a0042

    .line 174
    .line 175
    .line 176
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 177
    .line 178
    .line 179
    move-result-object p1

    .line 180
    check-cast p1, Landroid/widget/Button;

    .line 181
    .line 182
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->OO:Landroid/widget/Button;

    .line 183
    .line 184
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 185
    .line 186
    const v0, 0x7f0a068e

    .line 187
    .line 188
    .line 189
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 190
    .line 191
    .line 192
    move-result-object p1

    .line 193
    check-cast p1, Landroid/widget/TextView;

    .line 194
    .line 195
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->Oo80:Landroid/widget/TextView;

    .line 196
    .line 197
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 198
    .line 199
    const v0, 0x7f0a003f

    .line 200
    .line 201
    .line 202
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 203
    .line 204
    .line 205
    move-result-object p1

    .line 206
    check-cast p1, Landroid/widget/Button;

    .line 207
    .line 208
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇08O〇00〇o:Landroid/widget/Button;

    .line 209
    .line 210
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 211
    .line 212
    const v0, 0x7f0a0202

    .line 213
    .line 214
    .line 215
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 216
    .line 217
    .line 218
    move-result-object p1

    .line 219
    check-cast p1, Landroid/widget/TextView;

    .line 220
    .line 221
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇〇o〇:Landroid/widget/TextView;

    .line 222
    .line 223
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 224
    .line 225
    const v0, 0x7f0a0040

    .line 226
    .line 227
    .line 228
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 229
    .line 230
    .line 231
    move-result-object p1

    .line 232
    check-cast p1, Landroid/widget/Button;

    .line 233
    .line 234
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇00O:Landroid/widget/Button;

    .line 235
    .line 236
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 237
    .line 238
    const v0, 0x7f0a0560

    .line 239
    .line 240
    .line 241
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 242
    .line 243
    .line 244
    move-result-object p1

    .line 245
    check-cast p1, Landroid/widget/TextView;

    .line 246
    .line 247
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O〇o88o08〇:Landroid/widget/TextView;

    .line 248
    .line 249
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 250
    .line 251
    const v0, 0x7f0a0041

    .line 252
    .line 253
    .line 254
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 255
    .line 256
    .line 257
    move-result-object p1

    .line 258
    check-cast p1, Landroid/widget/Button;

    .line 259
    .line 260
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O8o08O8O:Landroid/widget/Button;

    .line 261
    .line 262
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 263
    .line 264
    const v0, 0x7f0a11b5

    .line 265
    .line 266
    .line 267
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 268
    .line 269
    .line 270
    move-result-object p1

    .line 271
    check-cast p1, Landroid/widget/TextView;

    .line 272
    .line 273
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇00O0:Landroid/widget/TextView;

    .line 274
    .line 275
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 276
    .line 277
    const v0, 0x7f0a0043

    .line 278
    .line 279
    .line 280
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 281
    .line 282
    .line 283
    move-result-object p1

    .line 284
    check-cast p1, Landroid/widget/Button;

    .line 285
    .line 286
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇080OO8〇0:Landroid/widget/Button;

    .line 287
    .line 288
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 289
    .line 290
    const v0, 0x7f0a11ba

    .line 291
    .line 292
    .line 293
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 294
    .line 295
    .line 296
    move-result-object p1

    .line 297
    check-cast p1, Landroid/widget/TextView;

    .line 298
    .line 299
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O〇08oOOO0:Landroid/widget/TextView;

    .line 300
    .line 301
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 302
    .line 303
    const v0, 0x7f0a0287

    .line 304
    .line 305
    .line 306
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 307
    .line 308
    .line 309
    move-result-object p1

    .line 310
    check-cast p1, Landroid/widget/Button;

    .line 311
    .line 312
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oOo〇8o008:Landroid/widget/Button;

    .line 313
    .line 314
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 315
    .line 316
    const v0, 0x7f0a1640

    .line 317
    .line 318
    .line 319
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 320
    .line 321
    .line 322
    move-result-object p1

    .line 323
    check-cast p1, Landroid/widget/TextView;

    .line 324
    .line 325
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o8〇OO:Landroid/widget/TextView;

    .line 326
    .line 327
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 328
    .line 329
    const v0, 0x7f0a0044

    .line 330
    .line 331
    .line 332
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 333
    .line 334
    .line 335
    move-result-object p1

    .line 336
    check-cast p1, Landroid/widget/Button;

    .line 337
    .line 338
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇OOo8〇0:Landroid/widget/Button;

    .line 339
    .line 340
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 341
    .line 342
    const v0, 0x7f0a0b57

    .line 343
    .line 344
    .line 345
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 346
    .line 347
    .line 348
    move-result-object p1

    .line 349
    check-cast p1, Landroid/widget/LinearLayout;

    .line 350
    .line 351
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇oO:Landroid/widget/LinearLayout;

    .line 352
    .line 353
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 354
    .line 355
    const v1, 0x7f0a1a48

    .line 356
    .line 357
    .line 358
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 359
    .line 360
    .line 361
    move-result-object p1

    .line 362
    iput-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇08〇o0O:Landroid/view/View;

    .line 363
    .line 364
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 365
    .line 366
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 367
    .line 368
    .line 369
    move-result-object p1

    .line 370
    if-eqz v2, :cond_5

    .line 371
    .line 372
    invoke-virtual {p1, p3}, Landroid/view/View;->setVisibility(I)V

    .line 373
    .line 374
    .line 375
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇080OO8〇0:Landroid/widget/Button;

    .line 376
    .line 377
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 378
    .line 379
    .line 380
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O8o08O8O:Landroid/widget/Button;

    .line 381
    .line 382
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 383
    .line 384
    .line 385
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇00O:Landroid/widget/Button;

    .line 386
    .line 387
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 388
    .line 389
    .line 390
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇08O〇00〇o:Landroid/widget/Button;

    .line 391
    .line 392
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 393
    .line 394
    .line 395
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->OO:Landroid/widget/Button;

    .line 396
    .line 397
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 398
    .line 399
    .line 400
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇OOo8〇0:Landroid/widget/Button;

    .line 401
    .line 402
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 403
    .line 404
    .line 405
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇0O:Landroid/widget/Button;

    .line 406
    .line 407
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 408
    .line 409
    .line 410
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->oOo〇8o008:Landroid/widget/Button;

    .line 411
    .line 412
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 413
    .line 414
    .line 415
    const-string p1, "SEND_TYPE"

    .line 416
    .line 417
    const/16 p3, 0xa

    .line 418
    .line 419
    invoke-virtual {p2, p1, p3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 420
    .line 421
    .line 422
    move-result p1

    .line 423
    iput p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o8o:I

    .line 424
    .line 425
    const/16 p2, 0xb

    .line 426
    .line 427
    if-ne p1, p2, :cond_6

    .line 428
    .line 429
    const/4 p1, 0x5

    .line 430
    iput p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o8o:I

    .line 431
    .line 432
    goto :goto_2

    .line 433
    :cond_6
    const/4 p1, 0x4

    .line 434
    iput p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o8o:I

    .line 435
    .line 436
    :goto_2
    iget-object p1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->O88O:Landroid/view/View;

    .line 437
    .line 438
    return-object p1
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method public onDestroy()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇OO〇00〇0O:Landroid/os/Handler;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->ooO:[I

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const-string v3, "UploadFragment"

    .line 7
    .line 8
    invoke-static {v3, v0, v1, v2}, Lcom/intsig/comm/recycle/HandlerMsglerRecycle;->〇o〇(Ljava/lang/String;Landroid/os/Handler;[I[Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onDetach()V
    .locals 2

    .line 1
    const-string v0, "UploadFragment"

    .line 2
    .line 3
    const-string v1, "onDetach()"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDetach()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onPause()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇8〇oO〇〇8o:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    invoke-static {v0}, Lcom/intsig/camscanner/service/UploadUtils;->〇〇808〇(Z)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 10
    .line 11
    invoke-static {v0}, Lcom/intsig/camscanner/service/UploadUtils;->〇O〇(Landroid/content/Context;)V

    .line 12
    .line 13
    .line 14
    :cond_0
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onResume()V
    .locals 2

    .line 1
    const-string v0, "UploadFragment"

    .line 2
    .line 3
    const-string v1, "onResume()"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇8〇oO〇〇8o:Z

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-static {v1}, Lcom/intsig/camscanner/service/UploadUtils;->〇〇808〇(Z)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 17
    .line 18
    invoke-static {v0}, Lcom/intsig/camscanner/service/UploadUtils;->〇080(Landroid/content/Context;)Z

    .line 19
    .line 20
    .line 21
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o〇8oOO88()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-nez v0, :cond_1

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 28
    .line 29
    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-static {v0}, Lcom/intsig/camscanner/app/AppActivateUtils;->〇o〇(Landroid/content/Context;)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_1

    .line 38
    .line 39
    invoke-static {v1}, Lcom/intsig/camscanner/launch/CsApplication;->O0o〇〇Oo(Z)V

    .line 40
    .line 41
    .line 42
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->Ooo8o()V

    .line 43
    .line 44
    .line 45
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public onStop()V
    .locals 2

    .line 1
    const-string v0, "UploadFragment"

    .line 2
    .line 3
    const-string v1, "onStop"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStop()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method 〇〇〇00()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇o0O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇oO:Landroid/widget/LinearLayout;

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    const/16 v1, 0x8

    .line 16
    .line 17
    if-eq v0, v1, :cond_0

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇oO:Landroid/widget/LinearLayout;

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 22
    .line 23
    .line 24
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇08〇o0O:Landroid/view/View;

    .line 25
    .line 26
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eq v0, v1, :cond_4

    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇08〇o0O:Landroid/view/View;

    .line 33
    .line 34
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇oO:Landroid/widget/LinearLayout;

    .line 39
    .line 40
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    const/4 v1, 0x0

    .line 45
    if-eqz v0, :cond_2

    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->o〇oO:Landroid/widget/LinearLayout;

    .line 48
    .line 49
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 50
    .line 51
    .line 52
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇08〇o0O:Landroid/view/View;

    .line 53
    .line 54
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    if-eqz v0, :cond_3

    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇08〇o0O:Landroid/view/View;

    .line 61
    .line 62
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 63
    .line 64
    .line 65
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇OOo8〇0:Landroid/widget/Button;

    .line 66
    .line 67
    const v1, 0x7f1304b1

    .line 68
    .line 69
    .line 70
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 71
    .line 72
    .line 73
    iget-object v0, p0, Lcom/intsig/camscanner/uploadfaxprint/UploadFragment;->〇OOo8〇0:Landroid/widget/Button;

    .line 74
    .line 75
    const/4 v1, 0x2

    .line 76
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 81
    .line 82
    .line 83
    :cond_4
    :goto_0
    return-void
    .line 84
    .line 85
    .line 86
.end method

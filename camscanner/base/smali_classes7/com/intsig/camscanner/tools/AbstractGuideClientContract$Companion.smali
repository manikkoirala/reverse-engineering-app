.class public final Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion;
.super Ljava/lang/Object;
.source "AbstractGuideClientContract.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/tools/AbstractGuideClientContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion;-><init>()V

    return-void
.end method

.method private final O8(Landroid/view/View;Landroid/view/View;Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 9

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v1, v0, [I

    .line 3
    .line 4
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 5
    .line 6
    .line 7
    new-array v2, v0, [I

    .line 8
    .line 9
    invoke-virtual {p2, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 10
    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    aget v4, v2, v3

    .line 14
    .line 15
    aget v5, v1, v3

    .line 16
    .line 17
    sub-int/2addr v4, v5

    .line 18
    invoke-virtual {p3}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->O8()I

    .line 19
    .line 20
    .line 21
    move-result v5

    .line 22
    sub-int/2addr v4, v5

    .line 23
    aput v4, v2, v3

    .line 24
    .line 25
    const/4 v4, 0x1

    .line 26
    aget v5, v2, v4

    .line 27
    .line 28
    aget v1, v1, v4

    .line 29
    .line 30
    sub-int/2addr v5, v1

    .line 31
    aput v5, v2, v4

    .line 32
    .line 33
    invoke-virtual {p3}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->OO0o〇〇〇〇0()Landroid/graphics/Rect;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    if-eqz v1, :cond_1

    .line 38
    .line 39
    invoke-virtual {p3}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->oO80()[I

    .line 40
    .line 41
    .line 42
    move-result-object v5

    .line 43
    if-eqz v5, :cond_0

    .line 44
    .line 45
    aget v5, v5, v4

    .line 46
    .line 47
    iget v6, v1, Landroid/graphics/Rect;->top:I

    .line 48
    .line 49
    sub-int v6, v5, v6

    .line 50
    .line 51
    iput v5, v1, Landroid/graphics/Rect;->top:I

    .line 52
    .line 53
    iget v5, v1, Landroid/graphics/Rect;->bottom:I

    .line 54
    .line 55
    add-int/2addr v5, v6

    .line 56
    iput v5, v1, Landroid/graphics/Rect;->bottom:I

    .line 57
    .line 58
    :cond_0
    new-array v5, v0, [I

    .line 59
    .line 60
    invoke-virtual {p1, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 61
    .line 62
    .line 63
    aget v6, v5, v3

    .line 64
    .line 65
    aget v5, v5, v4

    .line 66
    .line 67
    iget v7, v1, Landroid/graphics/Rect;->left:I

    .line 68
    .line 69
    sub-int/2addr v7, v6

    .line 70
    iput v7, v1, Landroid/graphics/Rect;->left:I

    .line 71
    .line 72
    iget v7, v1, Landroid/graphics/Rect;->right:I

    .line 73
    .line 74
    sub-int/2addr v7, v6

    .line 75
    iput v7, v1, Landroid/graphics/Rect;->right:I

    .line 76
    .line 77
    iget v6, v1, Landroid/graphics/Rect;->top:I

    .line 78
    .line 79
    sub-int/2addr v6, v5

    .line 80
    iput v6, v1, Landroid/graphics/Rect;->top:I

    .line 81
    .line 82
    iget v7, v1, Landroid/graphics/Rect;->bottom:I

    .line 83
    .line 84
    sub-int/2addr v7, v5

    .line 85
    iput v7, v1, Landroid/graphics/Rect;->bottom:I

    .line 86
    .line 87
    const/high16 v5, 0x40400000    # 3.0f

    .line 88
    .line 89
    invoke-static {v5}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 90
    .line 91
    .line 92
    move-result v7

    .line 93
    add-int/2addr v6, v7

    .line 94
    iput v6, v1, Landroid/graphics/Rect;->top:I

    .line 95
    .line 96
    iget v6, v1, Landroid/graphics/Rect;->bottom:I

    .line 97
    .line 98
    invoke-static {v5}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 99
    .line 100
    .line 101
    move-result v5

    .line 102
    sub-int/2addr v6, v5

    .line 103
    iput v6, v1, Landroid/graphics/Rect;->bottom:I

    .line 104
    .line 105
    :cond_1
    aget v1, v2, v3

    .line 106
    .line 107
    aget v5, v2, v4

    .line 108
    .line 109
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 110
    .line 111
    .line 112
    move-result v6

    .line 113
    new-instance v7, Ljava/lang/StringBuilder;

    .line 114
    .line 115
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    .line 117
    .line 118
    const-string v8, "updateLayoutParamsOnlyPosition:  - parentLocations="

    .line 119
    .line 120
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    const-string v1, " X "

    .line 127
    .line 128
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 132
    .line 133
    .line 134
    const-string v1, ", tipsRoot.height="

    .line 135
    .line 136
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    .line 138
    .line 139
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 140
    .line 141
    .line 142
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    const-string v5, "AbstractGuideClientContract"

    .line 147
    .line 148
    invoke-static {v5, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 152
    .line 153
    .line 154
    move-result-object v1

    .line 155
    instance-of v5, v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 156
    .line 157
    const/4 v6, 0x0

    .line 158
    if-eqz v5, :cond_2

    .line 159
    .line 160
    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 161
    .line 162
    goto :goto_0

    .line 163
    :cond_2
    move-object v1, v6

    .line 164
    :goto_0
    if-eqz v1, :cond_3

    .line 165
    .line 166
    aget v5, v2, v4

    .line 167
    .line 168
    const v7, 0x7f0a0300

    .line 169
    .line 170
    .line 171
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 172
    .line 173
    .line 174
    move-result-object v7

    .line 175
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    .line 176
    .line 177
    .line 178
    move-result v7

    .line 179
    sub-int/2addr v5, v7

    .line 180
    const v7, 0x7f0a00a2

    .line 181
    .line 182
    .line 183
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 184
    .line 185
    .line 186
    move-result-object v7

    .line 187
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    .line 188
    .line 189
    .line 190
    move-result v7

    .line 191
    sub-int/2addr v5, v7

    .line 192
    iput v5, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 193
    .line 194
    aget v2, v2, v3

    .line 195
    .line 196
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    .line 197
    .line 198
    .line 199
    move-result p2

    .line 200
    div-int/2addr p2, v0

    .line 201
    sub-int/2addr v2, p2

    .line 202
    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 203
    .line 204
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 205
    .line 206
    .line 207
    :cond_3
    invoke-virtual {p3}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->〇〇888()I

    .line 208
    .line 209
    .line 210
    move-result p1

    .line 211
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 212
    .line 213
    .line 214
    move-result-object p1

    .line 215
    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    .line 216
    .line 217
    .line 218
    move-result p2

    .line 219
    if-eqz p2, :cond_4

    .line 220
    .line 221
    const/4 p2, 0x1

    .line 222
    goto :goto_1

    .line 223
    :cond_4
    const/4 p2, 0x0

    .line 224
    :goto_1
    if-eqz p2, :cond_5

    .line 225
    .line 226
    goto :goto_2

    .line 227
    :cond_5
    move-object p1, v6

    .line 228
    :goto_2
    if-eqz p1, :cond_6

    .line 229
    .line 230
    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    .line 231
    .line 232
    .line 233
    move-result p1

    .line 234
    invoke-virtual {p4, p1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 235
    .line 236
    .line 237
    invoke-virtual {p4}, Lcom/airbnb/lottie/LottieAnimationView;->〇O〇()V

    .line 238
    .line 239
    .line 240
    goto :goto_6

    .line 241
    :cond_6
    invoke-virtual {p3}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->Oo08()I

    .line 242
    .line 243
    .line 244
    move-result p1

    .line 245
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 246
    .line 247
    .line 248
    move-result-object p1

    .line 249
    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    .line 250
    .line 251
    .line 252
    move-result p2

    .line 253
    if-eqz p2, :cond_7

    .line 254
    .line 255
    const/4 v3, 0x1

    .line 256
    :cond_7
    if-eqz v3, :cond_8

    .line 257
    .line 258
    goto :goto_3

    .line 259
    :cond_8
    move-object p1, v6

    .line 260
    :goto_3
    if-eqz p1, :cond_d

    .line 261
    .line 262
    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    .line 263
    .line 264
    .line 265
    move-result p1

    .line 266
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 267
    .line 268
    .line 269
    move-result-object p2

    .line 270
    instance-of p3, p2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 271
    .line 272
    if-eqz p3, :cond_9

    .line 273
    .line 274
    move-object v6, p2

    .line 275
    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 276
    .line 277
    :cond_9
    const/4 p2, -0x2

    .line 278
    if-nez v6, :cond_a

    .line 279
    .line 280
    goto :goto_4

    .line 281
    :cond_a
    iput p2, v6, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 282
    .line 283
    :goto_4
    if-nez v6, :cond_b

    .line 284
    .line 285
    goto :goto_5

    .line 286
    :cond_b
    iput p2, v6, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 287
    .line 288
    :goto_5
    if-eqz v6, :cond_c

    .line 289
    .line 290
    invoke-virtual {p4, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 291
    .line 292
    .line 293
    :cond_c
    invoke-virtual {p4, p1}, Lcom/airbnb/lottie/LottieAnimationView;->setImageResource(I)V

    .line 294
    .line 295
    .line 296
    invoke-virtual {p4}, Landroid/view/View;->requestLayout()V

    .line 297
    .line 298
    .line 299
    :cond_d
    :goto_6
    return-void
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion;Landroid/app/Activity;Landroid/view/View;Lcom/intsig/camscanner/view/IArrowViewContract;Landroid/view/View;Lcom/intsig/camscanner/view/MaskClickableView;Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion;->〇o〇(Landroid/app/Activity;Landroid/view/View;Lcom/intsig/camscanner/view/IArrowViewContract;Landroid/view/View;Lcom/intsig/camscanner/view/MaskClickableView;Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion;Landroid/view/View;Landroid/view/View;Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion;->O8(Landroid/view/View;Landroid/view/View;Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;Lcom/airbnb/lottie/LottieAnimationView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method private final 〇o〇(Landroid/app/Activity;Landroid/view/View;Lcom/intsig/camscanner/view/IArrowViewContract;Landroid/view/View;Lcom/intsig/camscanner/view/MaskClickableView;Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;)V
    .locals 17

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    move-object/from16 v1, p2

    .line 4
    .line 5
    move-object/from16 v2, p3

    .line 6
    .line 7
    move-object/from16 v3, p5

    .line 8
    .line 9
    const/4 v4, 0x2

    .line 10
    new-array v5, v4, [I

    .line 11
    .line 12
    invoke-virtual {v1, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 13
    .line 14
    .line 15
    new-array v6, v4, [I

    .line 16
    .line 17
    move-object/from16 v7, p4

    .line 18
    .line 19
    invoke-virtual {v7, v6}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 20
    .line 21
    .line 22
    const/4 v8, 0x0

    .line 23
    aget v9, v6, v8

    .line 24
    .line 25
    aget v10, v5, v8

    .line 26
    .line 27
    sub-int/2addr v9, v10

    .line 28
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->O8()I

    .line 29
    .line 30
    .line 31
    move-result v10

    .line 32
    sub-int/2addr v9, v10

    .line 33
    aput v9, v6, v8

    .line 34
    .line 35
    const/4 v9, 0x1

    .line 36
    aget v10, v6, v9

    .line 37
    .line 38
    aget v5, v5, v9

    .line 39
    .line 40
    sub-int/2addr v10, v5

    .line 41
    aput v10, v6, v9

    .line 42
    .line 43
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->OO0o〇〇〇〇0()Landroid/graphics/Rect;

    .line 44
    .line 45
    .line 46
    move-result-object v5

    .line 47
    if-eqz v5, :cond_2

    .line 48
    .line 49
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->oO80()[I

    .line 50
    .line 51
    .line 52
    move-result-object v10

    .line 53
    if-eqz v10, :cond_0

    .line 54
    .line 55
    aget v10, v10, v9

    .line 56
    .line 57
    iget v11, v5, Landroid/graphics/Rect;->top:I

    .line 58
    .line 59
    sub-int v11, v10, v11

    .line 60
    .line 61
    iput v10, v5, Landroid/graphics/Rect;->top:I

    .line 62
    .line 63
    iget v10, v5, Landroid/graphics/Rect;->bottom:I

    .line 64
    .line 65
    add-int/2addr v10, v11

    .line 66
    iput v10, v5, Landroid/graphics/Rect;->bottom:I

    .line 67
    .line 68
    :cond_0
    new-array v10, v4, [I

    .line 69
    .line 70
    invoke-virtual {v1, v10}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 71
    .line 72
    .line 73
    aget v11, v10, v8

    .line 74
    .line 75
    aget v10, v10, v9

    .line 76
    .line 77
    iget v12, v5, Landroid/graphics/Rect;->left:I

    .line 78
    .line 79
    sub-int/2addr v12, v11

    .line 80
    iput v12, v5, Landroid/graphics/Rect;->left:I

    .line 81
    .line 82
    iget v12, v5, Landroid/graphics/Rect;->right:I

    .line 83
    .line 84
    sub-int/2addr v12, v11

    .line 85
    iput v12, v5, Landroid/graphics/Rect;->right:I

    .line 86
    .line 87
    iget v11, v5, Landroid/graphics/Rect;->top:I

    .line 88
    .line 89
    sub-int/2addr v11, v10

    .line 90
    iput v11, v5, Landroid/graphics/Rect;->top:I

    .line 91
    .line 92
    iget v11, v5, Landroid/graphics/Rect;->bottom:I

    .line 93
    .line 94
    sub-int/2addr v11, v10

    .line 95
    iput v11, v5, Landroid/graphics/Rect;->bottom:I

    .line 96
    .line 97
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->〇80〇808〇O()I

    .line 98
    .line 99
    .line 100
    move-result v10

    .line 101
    if-lez v10, :cond_1

    .line 102
    .line 103
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->〇80〇808〇O()I

    .line 104
    .line 105
    .line 106
    move-result v10

    .line 107
    invoke-virtual {v3, v5, v8, v10}, Lcom/intsig/camscanner/view/MaskView;->〇o〇(Landroid/graphics/Rect;ZI)V

    .line 108
    .line 109
    .line 110
    goto :goto_0

    .line 111
    :cond_1
    iget v10, v5, Landroid/graphics/Rect;->top:I

    .line 112
    .line 113
    const/high16 v11, 0x40400000    # 3.0f

    .line 114
    .line 115
    invoke-static {v11}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 116
    .line 117
    .line 118
    move-result v12

    .line 119
    add-int/2addr v10, v12

    .line 120
    iput v10, v5, Landroid/graphics/Rect;->top:I

    .line 121
    .line 122
    iget v10, v5, Landroid/graphics/Rect;->bottom:I

    .line 123
    .line 124
    invoke-static {v11}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 125
    .line 126
    .line 127
    move-result v11

    .line 128
    sub-int/2addr v10, v11

    .line 129
    iput v10, v5, Landroid/graphics/Rect;->bottom:I

    .line 130
    .line 131
    invoke-virtual {v3, v5, v8}, Lcom/intsig/camscanner/view/MaskView;->〇o00〇〇Oo(Landroid/graphics/Rect;Z)V

    .line 132
    .line 133
    .line 134
    :cond_2
    :goto_0
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->〇080()Lcom/intsig/camscanner/view/IArrowViewContract$ArrowDirection;

    .line 135
    .line 136
    .line 137
    move-result-object v5

    .line 138
    sget-object v10, Lcom/intsig/camscanner/view/IArrowViewContract$ArrowDirection;->TOP:Lcom/intsig/camscanner/view/IArrowViewContract$ArrowDirection;

    .line 139
    .line 140
    const-string v11, "AbstractGuideClientContract"

    .line 141
    .line 142
    const/4 v12, 0x0

    .line 143
    if-eq v5, v10, :cond_4

    .line 144
    .line 145
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->〇080()Lcom/intsig/camscanner/view/IArrowViewContract$ArrowDirection;

    .line 146
    .line 147
    .line 148
    move-result-object v5

    .line 149
    sget-object v13, Lcom/intsig/camscanner/view/IArrowViewContract$ArrowDirection;->BOTTOM:Lcom/intsig/camscanner/view/IArrowViewContract$ArrowDirection;

    .line 150
    .line 151
    if-ne v5, v13, :cond_3

    .line 152
    .line 153
    goto :goto_1

    .line 154
    :cond_3
    const-string v0, "not support now!"

    .line 155
    .line 156
    invoke-static {v11, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    .line 158
    .line 159
    goto/16 :goto_9

    .line 160
    .line 161
    :cond_4
    :goto_1
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->o〇0()I

    .line 162
    .line 163
    .line 164
    move-result v5

    .line 165
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->〇〇808〇()I

    .line 166
    .line 167
    .line 168
    move-result v13

    .line 169
    aget v14, v6, v8

    .line 170
    .line 171
    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getWidth()I

    .line 172
    .line 173
    .line 174
    move-result v15

    .line 175
    div-int/2addr v15, v4

    .line 176
    add-int/2addr v14, v15

    .line 177
    const/16 v15, 0xa

    .line 178
    .line 179
    if-le v5, v14, :cond_6

    .line 180
    .line 181
    invoke-interface/range {p3 .. p3}, Lcom/intsig/camscanner/view/IArrowViewContract;->getView()Landroid/view/View;

    .line 182
    .line 183
    .line 184
    move-result-object v1

    .line 185
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 186
    .line 187
    .line 188
    move-result v1

    .line 189
    if-le v14, v1, :cond_5

    .line 190
    .line 191
    invoke-interface/range {p3 .. p3}, Lcom/intsig/camscanner/view/IArrowViewContract;->getView()Landroid/view/View;

    .line 192
    .line 193
    .line 194
    move-result-object v1

    .line 195
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 196
    .line 197
    .line 198
    move-result v1

    .line 199
    invoke-static {v0, v15}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 200
    .line 201
    .line 202
    move-result v4

    .line 203
    sub-int/2addr v1, v4

    .line 204
    invoke-interface {v2, v1}, Lcom/intsig/camscanner/view/IArrowViewContract;->setArrowMarginLeft(I)V

    .line 205
    .line 206
    .line 207
    invoke-interface/range {p3 .. p3}, Lcom/intsig/camscanner/view/IArrowViewContract;->getArrowMarginLeft()I

    .line 208
    .line 209
    .line 210
    move-result v1

    .line 211
    sub-int/2addr v14, v1

    .line 212
    goto :goto_3

    .line 213
    :cond_5
    invoke-interface {v2, v14}, Lcom/intsig/camscanner/view/IArrowViewContract;->setArrowMarginLeft(I)V

    .line 214
    .line 215
    .line 216
    const/4 v14, 0x0

    .line 217
    goto :goto_3

    .line 218
    :cond_6
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getWidth()I

    .line 219
    .line 220
    .line 221
    move-result v1

    .line 222
    invoke-interface/range {p3 .. p3}, Lcom/intsig/camscanner/view/IArrowViewContract;->getView()Landroid/view/View;

    .line 223
    .line 224
    .line 225
    move-result-object v16

    .line 226
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getWidth()I

    .line 227
    .line 228
    .line 229
    move-result v16

    .line 230
    div-int/lit8 v16, v16, 0x2

    .line 231
    .line 232
    add-int v8, v14, v16

    .line 233
    .line 234
    if-ge v8, v1, :cond_8

    .line 235
    .line 236
    invoke-interface/range {p3 .. p3}, Lcom/intsig/camscanner/view/IArrowViewContract;->getView()Landroid/view/View;

    .line 237
    .line 238
    .line 239
    move-result-object v1

    .line 240
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 241
    .line 242
    .line 243
    move-result v1

    .line 244
    div-int/2addr v1, v4

    .line 245
    add-int/2addr v1, v5

    .line 246
    if-ge v14, v1, :cond_7

    .line 247
    .line 248
    sub-int v1, v14, v5

    .line 249
    .line 250
    invoke-interface {v2, v1}, Lcom/intsig/camscanner/view/IArrowViewContract;->setArrowMarginLeft(I)V

    .line 251
    .line 252
    .line 253
    goto :goto_2

    .line 254
    :cond_7
    invoke-interface/range {p3 .. p3}, Lcom/intsig/camscanner/view/IArrowViewContract;->getView()Landroid/view/View;

    .line 255
    .line 256
    .line 257
    move-result-object v1

    .line 258
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 259
    .line 260
    .line 261
    move-result v1

    .line 262
    div-int/2addr v1, v4

    .line 263
    invoke-interface {v2, v1}, Lcom/intsig/camscanner/view/IArrowViewContract;->setArrowMarginLeft(I)V

    .line 264
    .line 265
    .line 266
    goto :goto_2

    .line 267
    :cond_8
    invoke-interface/range {p3 .. p3}, Lcom/intsig/camscanner/view/IArrowViewContract;->getView()Landroid/view/View;

    .line 268
    .line 269
    .line 270
    move-result-object v1

    .line 271
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 272
    .line 273
    .line 274
    move-result v1

    .line 275
    add-int/2addr v1, v5

    .line 276
    if-ge v1, v14, :cond_9

    .line 277
    .line 278
    invoke-interface/range {p3 .. p3}, Lcom/intsig/camscanner/view/IArrowViewContract;->getView()Landroid/view/View;

    .line 279
    .line 280
    .line 281
    move-result-object v1

    .line 282
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 283
    .line 284
    .line 285
    move-result v1

    .line 286
    invoke-static {v0, v15}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 287
    .line 288
    .line 289
    move-result v4

    .line 290
    sub-int/2addr v1, v4

    .line 291
    invoke-interface {v2, v1}, Lcom/intsig/camscanner/view/IArrowViewContract;->setArrowMarginLeft(I)V

    .line 292
    .line 293
    .line 294
    goto :goto_2

    .line 295
    :cond_9
    sub-int v1, v14, v5

    .line 296
    .line 297
    invoke-interface {v2, v1}, Lcom/intsig/camscanner/view/IArrowViewContract;->setArrowMarginLeft(I)V

    .line 298
    .line 299
    .line 300
    :goto_2
    invoke-interface/range {p3 .. p3}, Lcom/intsig/camscanner/view/IArrowViewContract;->getArrowMarginLeft()I

    .line 301
    .line 302
    .line 303
    move-result v1

    .line 304
    sub-int/2addr v14, v1

    .line 305
    if-gez v14, :cond_a

    .line 306
    .line 307
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->o〇0()I

    .line 308
    .line 309
    .line 310
    move-result v14

    .line 311
    :cond_a
    :goto_3
    invoke-interface/range {p3 .. p3}, Lcom/intsig/camscanner/view/IArrowViewContract;->getView()Landroid/view/View;

    .line 312
    .line 313
    .line 314
    move-result-object v1

    .line 315
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 316
    .line 317
    .line 318
    move-result-object v1

    .line 319
    instance-of v4, v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 320
    .line 321
    if-eqz v4, :cond_b

    .line 322
    .line 323
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 324
    .line 325
    goto :goto_4

    .line 326
    :cond_b
    move-object v1, v12

    .line 327
    :goto_4
    if-nez v1, :cond_c

    .line 328
    .line 329
    goto :goto_5

    .line 330
    :cond_c
    iput v14, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 331
    .line 332
    :goto_5
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->〇080()Lcom/intsig/camscanner/view/IArrowViewContract$ArrowDirection;

    .line 333
    .line 334
    .line 335
    move-result-object v4

    .line 336
    const/16 v5, 0x8

    .line 337
    .line 338
    if-ne v4, v10, :cond_e

    .line 339
    .line 340
    if-nez v1, :cond_d

    .line 341
    .line 342
    goto :goto_6

    .line 343
    :cond_d
    aget v4, v6, v9

    .line 344
    .line 345
    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getHeight()I

    .line 346
    .line 347
    .line 348
    move-result v6

    .line 349
    add-int/2addr v4, v6

    .line 350
    invoke-static {v0, v5}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 351
    .line 352
    .line 353
    move-result v0

    .line 354
    add-int/2addr v4, v0

    .line 355
    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 356
    .line 357
    goto :goto_6

    .line 358
    :cond_e
    if-nez v1, :cond_f

    .line 359
    .line 360
    goto :goto_6

    .line 361
    :cond_f
    aget v4, v6, v9

    .line 362
    .line 363
    invoke-interface/range {p3 .. p3}, Lcom/intsig/camscanner/view/IArrowViewContract;->getView()Landroid/view/View;

    .line 364
    .line 365
    .line 366
    move-result-object v6

    .line 367
    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    .line 368
    .line 369
    .line 370
    move-result v6

    .line 371
    sub-int/2addr v4, v6

    .line 372
    invoke-static {v0, v5}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 373
    .line 374
    .line 375
    move-result v0

    .line 376
    sub-int/2addr v4, v0

    .line 377
    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 378
    .line 379
    :goto_6
    if-eqz v1, :cond_10

    .line 380
    .line 381
    iget v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 382
    .line 383
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 384
    .line 385
    .line 386
    move-result-object v0

    .line 387
    goto :goto_7

    .line 388
    :cond_10
    move-object v0, v12

    .line 389
    :goto_7
    new-instance v4, Ljava/lang/StringBuilder;

    .line 390
    .line 391
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 392
    .line 393
    .line 394
    const-string v5, "top margin\uff1a"

    .line 395
    .line 396
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    .line 398
    .line 399
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 400
    .line 401
    .line 402
    const-string v0, " topMarginExtraOffset: "

    .line 403
    .line 404
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 405
    .line 406
    .line 407
    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 408
    .line 409
    .line 410
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 411
    .line 412
    .line 413
    move-result-object v0

    .line 414
    invoke-static {v11, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    .line 416
    .line 417
    if-nez v1, :cond_11

    .line 418
    .line 419
    goto :goto_8

    .line 420
    :cond_11
    iget v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 421
    .line 422
    add-int/2addr v0, v13

    .line 423
    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 424
    .line 425
    :goto_8
    invoke-interface/range {p3 .. p3}, Lcom/intsig/camscanner/view/IArrowViewContract;->getView()Landroid/view/View;

    .line 426
    .line 427
    .line 428
    move-result-object v0

    .line 429
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 430
    .line 431
    .line 432
    :goto_9
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->〇8o8o〇()Lcom/intsig/callback/Callback0;

    .line 433
    .line 434
    .line 435
    move-result-object v0

    .line 436
    if-eqz v0, :cond_12

    .line 437
    .line 438
    invoke-virtual {v3, v0}, Lcom/intsig/camscanner/view/MaskClickableView;->setTransparentClickCallback(Lcom/intsig/callback/Callback0;)V

    .line 439
    .line 440
    .line 441
    :cond_12
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->〇O8o08O()Lcom/intsig/callback/Callback;

    .line 442
    .line 443
    .line 444
    move-result-object v0

    .line 445
    if-eqz v0, :cond_13

    .line 446
    .line 447
    invoke-virtual {v3, v0}, Lcom/intsig/camscanner/view/MaskClickableView;->setTransparentClickCallback1(Lcom/intsig/callback/Callback;)V

    .line 448
    .line 449
    .line 450
    :cond_13
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->Oooo8o0〇()Ljava/lang/CharSequence;

    .line 451
    .line 452
    .line 453
    move-result-object v0

    .line 454
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 455
    .line 456
    .line 457
    move-result v1

    .line 458
    if-lez v1, :cond_14

    .line 459
    .line 460
    const/4 v1, 0x1

    .line 461
    goto :goto_a

    .line 462
    :cond_14
    const/4 v1, 0x0

    .line 463
    :goto_a
    if-eqz v1, :cond_15

    .line 464
    .line 465
    goto :goto_b

    .line 466
    :cond_15
    move-object v0, v12

    .line 467
    :goto_b
    if-eqz v0, :cond_17

    .line 468
    .line 469
    invoke-interface/range {p3 .. p3}, Lcom/intsig/camscanner/view/IArrowViewContract;->getView()Landroid/view/View;

    .line 470
    .line 471
    .line 472
    move-result-object v1

    .line 473
    const v3, 0x7f0a187b

    .line 474
    .line 475
    .line 476
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 477
    .line 478
    .line 479
    move-result-object v1

    .line 480
    check-cast v1, Landroid/widget/TextView;

    .line 481
    .line 482
    if-nez v1, :cond_16

    .line 483
    .line 484
    goto :goto_c

    .line 485
    :cond_16
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 486
    .line 487
    .line 488
    :cond_17
    :goto_c
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->OO0o〇〇()Ljava/lang/CharSequence;

    .line 489
    .line 490
    .line 491
    move-result-object v0

    .line 492
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 493
    .line 494
    .line 495
    move-result v1

    .line 496
    if-lez v1, :cond_18

    .line 497
    .line 498
    const/4 v1, 0x1

    .line 499
    goto :goto_d

    .line 500
    :cond_18
    const/4 v1, 0x0

    .line 501
    :goto_d
    if-eqz v1, :cond_19

    .line 502
    .line 503
    goto :goto_e

    .line 504
    :cond_19
    move-object v0, v12

    .line 505
    :goto_e
    if-eqz v0, :cond_1b

    .line 506
    .line 507
    invoke-interface/range {p3 .. p3}, Lcom/intsig/camscanner/view/IArrowViewContract;->getView()Landroid/view/View;

    .line 508
    .line 509
    .line 510
    move-result-object v1

    .line 511
    const v3, 0x7f0a1810

    .line 512
    .line 513
    .line 514
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 515
    .line 516
    .line 517
    move-result-object v1

    .line 518
    check-cast v1, Landroid/widget/TextView;

    .line 519
    .line 520
    if-nez v1, :cond_1a

    .line 521
    .line 522
    goto :goto_f

    .line 523
    :cond_1a
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 524
    .line 525
    .line 526
    :cond_1b
    :goto_f
    invoke-interface/range {p3 .. p3}, Lcom/intsig/camscanner/view/IArrowViewContract;->getView()Landroid/view/View;

    .line 527
    .line 528
    .line 529
    move-result-object v0

    .line 530
    const v1, 0x7f0a0d5a

    .line 531
    .line 532
    .line 533
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 534
    .line 535
    .line 536
    move-result-object v0

    .line 537
    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    .line 538
    .line 539
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->〇〇888()I

    .line 540
    .line 541
    .line 542
    move-result v1

    .line 543
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 544
    .line 545
    .line 546
    move-result-object v1

    .line 547
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    .line 548
    .line 549
    .line 550
    move-result v2

    .line 551
    if-eqz v2, :cond_1c

    .line 552
    .line 553
    const/4 v2, 0x1

    .line 554
    goto :goto_10

    .line 555
    :cond_1c
    const/4 v2, 0x0

    .line 556
    :goto_10
    if-eqz v2, :cond_1d

    .line 557
    .line 558
    goto :goto_11

    .line 559
    :cond_1d
    move-object v1, v12

    .line 560
    :goto_11
    if-eqz v1, :cond_1f

    .line 561
    .line 562
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    .line 563
    .line 564
    .line 565
    move-result v1

    .line 566
    if-eqz v0, :cond_1e

    .line 567
    .line 568
    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 569
    .line 570
    .line 571
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 572
    .line 573
    goto :goto_12

    .line 574
    :cond_1e
    move-object v1, v12

    .line 575
    :goto_12
    if-nez v1, :cond_28

    .line 576
    .line 577
    :cond_1f
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->Oo08()I

    .line 578
    .line 579
    .line 580
    move-result v1

    .line 581
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 582
    .line 583
    .line 584
    move-result-object v1

    .line 585
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    .line 586
    .line 587
    .line 588
    move-result v2

    .line 589
    if-eqz v2, :cond_20

    .line 590
    .line 591
    const/4 v8, 0x1

    .line 592
    goto :goto_13

    .line 593
    :cond_20
    const/4 v8, 0x0

    .line 594
    :goto_13
    if-eqz v8, :cond_21

    .line 595
    .line 596
    goto :goto_14

    .line 597
    :cond_21
    move-object v1, v12

    .line 598
    :goto_14
    if-eqz v1, :cond_28

    .line 599
    .line 600
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    .line 601
    .line 602
    .line 603
    move-result v1

    .line 604
    if-eqz v0, :cond_22

    .line 605
    .line 606
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 607
    .line 608
    .line 609
    move-result-object v2

    .line 610
    goto :goto_15

    .line 611
    :cond_22
    move-object v2, v12

    .line 612
    :goto_15
    instance-of v3, v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 613
    .line 614
    if-eqz v3, :cond_23

    .line 615
    .line 616
    move-object v12, v2

    .line 617
    check-cast v12, Landroid/widget/RelativeLayout$LayoutParams;

    .line 618
    .line 619
    :cond_23
    const/4 v2, -0x2

    .line 620
    if-nez v12, :cond_24

    .line 621
    .line 622
    goto :goto_16

    .line 623
    :cond_24
    iput v2, v12, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 624
    .line 625
    :goto_16
    if-nez v12, :cond_25

    .line 626
    .line 627
    goto :goto_17

    .line 628
    :cond_25
    iput v2, v12, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 629
    .line 630
    :goto_17
    if-eqz v12, :cond_26

    .line 631
    .line 632
    invoke-virtual {v0, v12}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 633
    .line 634
    .line 635
    :cond_26
    if-eqz v0, :cond_27

    .line 636
    .line 637
    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setImageResource(I)V

    .line 638
    .line 639
    .line 640
    :cond_27
    if-eqz v0, :cond_28

    .line 641
    .line 642
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 643
    .line 644
    .line 645
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 646
    .line 647
    :cond_28
    return-void
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
.end method

.class public Lcom/intsig/camscanner/tools/OcrTextShareClient;
.super Ljava/lang/Object;
.source "OcrTextShareClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;
    }
.end annotation


# instance fields
.field private O8:Lcom/intsig/camscanner/share/type/ShareWord;

.field private Oo08:Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;

.field private final 〇080:Landroidx/fragment/app/FragmentActivity;

.field private 〇o00〇〇Oo:Z

.field private 〇o〇:Lcom/intsig/camscanner/share/ShareHelper;


# direct methods
.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->Oo08:Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic O8(Lcom/intsig/camscanner/tools/OcrTextShareClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/tools/OcrTextShareClient;->o〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private OO0o〇〇〇〇0()V
    .locals 5

    .line 1
    new-instance v0, Ljava/text/SimpleDateFormat;

    .line 2
    .line 3
    const-string v1, "yyyy-MM-dd HH.mm.ss"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v1, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    sget-object v2, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 14
    .line 15
    const v3, 0x7f130b3d

    .line 16
    .line 17
    .line 18
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const-string v2, " "

    .line 26
    .line 27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    new-instance v2, Ljava/util/Date;

    .line 31
    .line 32
    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    const-string v0, ".txt"

    .line 43
    .line 44
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    new-instance v1, Ljava/io/File;

    .line 52
    .line 53
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->Oo08:Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;

    .line 61
    .line 62
    iget-boolean v2, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇o00〇〇Oo:Z

    .line 63
    .line 64
    invoke-interface {v0, v2}, Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;->〇o00〇〇Oo(Z)Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 69
    .line 70
    .line 71
    move-result v2

    .line 72
    if-eqz v2, :cond_0

    .line 73
    .line 74
    iget-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 75
    .line 76
    const v1, 0x7f130fbc

    .line 77
    .line 78
    .line 79
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    .line 80
    .line 81
    .line 82
    return-void

    .line 83
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    invoke-static {v0, v2}, Lcom/intsig/utils/FileUtil;->〇8〇0〇o〇O(Ljava/lang/String;Ljava/lang/String;)Z

    .line 88
    .line 89
    .line 90
    move-result v0

    .line 91
    if-eqz v0, :cond_1

    .line 92
    .line 93
    iget-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 94
    .line 95
    invoke-static {v0}, Lcom/intsig/camscanner/share/ShareHelper;->ooo8o〇o〇(Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    new-instance v2, Ljava/util/ArrayList;

    .line 100
    .line 101
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 102
    .line 103
    .line 104
    iget-object v3, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->Oo08:Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;

    .line 105
    .line 106
    invoke-interface {v3}, Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;->〇080()J

    .line 107
    .line 108
    .line 109
    move-result-wide v3

    .line 110
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 111
    .line 112
    .line 113
    move-result-object v3

    .line 114
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    .line 116
    .line 117
    new-instance v3, Lcom/intsig/camscanner/share/type/ShareTxtFile;

    .line 118
    .line 119
    iget-object v4, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 120
    .line 121
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 122
    .line 123
    .line 124
    move-result-object v1

    .line 125
    invoke-direct {v3, v4, v2, v1}, Lcom/intsig/camscanner/share/type/ShareTxtFile;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 126
    .line 127
    .line 128
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 129
    .line 130
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/ShareHelper;->〇〇00O〇0o(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 131
    .line 132
    .line 133
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 134
    .line 135
    .line 136
    goto :goto_0

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 138
    .line 139
    const v1, 0x7f130241

    .line 140
    .line 141
    .line 142
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    .line 143
    .line 144
    .line 145
    :goto_0
    return-void
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method static bridge synthetic Oo08(Lcom/intsig/camscanner/tools/OcrTextShareClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/tools/OcrTextShareClient;->OO0o〇〇〇〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private o〇0()V
    .locals 5

    .line 1
    const-string v0, "OcrTextShareClient"

    .line 2
    .line 3
    const-string v1, "doShareMore"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->Oo08:Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;

    .line 9
    .line 10
    new-instance v1, Landroid/util/Pair;

    .line 11
    .line 12
    const-string v2, "type"

    .line 13
    .line 14
    const-string v3, "more"

    .line 15
    .line 16
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 17
    .line 18
    .line 19
    const-string v2, "send_text"

    .line 20
    .line 21
    invoke-interface {v0, v2, v1}, Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;->〇o〇(Ljava/lang/String;Landroid/util/Pair;)V

    .line 22
    .line 23
    .line 24
    invoke-static {v3}, Lcom/intsig/camscanner/share/ShareRecorder;->oO80(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 28
    .line 29
    invoke-static {v0}, Lcom/intsig/camscanner/share/ShareHelper;->ooo8o〇o〇(Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    iput-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇o〇:Lcom/intsig/camscanner/share/ShareHelper;

    .line 34
    .line 35
    new-instance v1, Lcom/intsig/camscanner/share/channel/item/MoreShareChannel;

    .line 36
    .line 37
    invoke-direct {v1}, Lcom/intsig/camscanner/share/channel/item/MoreShareChannel;-><init>()V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/ShareHelper;->Oo08(Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)V

    .line 41
    .line 42
    .line 43
    new-instance v0, Ljava/util/ArrayList;

    .line 44
    .line 45
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .line 47
    .line 48
    iget-object v1, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->Oo08:Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;

    .line 49
    .line 50
    invoke-interface {v1}, Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;->〇080()J

    .line 51
    .line 52
    .line 53
    move-result-wide v1

    .line 54
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    .line 60
    .line 61
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareTextCharacter;

    .line 62
    .line 63
    iget-object v2, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 64
    .line 65
    iget-object v3, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->Oo08:Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;

    .line 66
    .line 67
    iget-boolean v4, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇o00〇〇Oo:Z

    .line 68
    .line 69
    invoke-interface {v3, v4}, Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;->〇o00〇〇Oo(Z)Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v3

    .line 73
    invoke-direct {v1, v2, v0, v3}, Lcom/intsig/camscanner/share/type/ShareTextCharacter;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    iget-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇o〇:Lcom/intsig/camscanner/share/ShareHelper;

    .line 77
    .line 78
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 79
    .line 80
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/ShareHelper;->〇〇00O〇0o(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 81
    .line 82
    .line 83
    iget-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇o〇:Lcom/intsig/camscanner/share/ShareHelper;

    .line 84
    .line 85
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 86
    .line 87
    .line 88
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/tools/OcrTextShareClient;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇o00〇〇Oo:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/tools/OcrTextShareClient;)Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->Oo08:Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/tools/OcrTextShareClient;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇o00〇〇Oo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public oO80(Z)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/camscanner/tools/OcrTextShareClient$1;

    .line 7
    .line 8
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/tools/OcrTextShareClient$1;-><init>(Lcom/intsig/camscanner/tools/OcrTextShareClient;Z)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇8〇80o(Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;)V

    .line 12
    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    const-string v1, "OcrTextShareClient"

    .line 21
    .line 22
    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇80〇808〇O()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->O8:Lcom/intsig/camscanner/share/type/ShareWord;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/ShareWord;->〇00O0O0()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8o8o〇(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇o〇:Lcom/intsig/camscanner/share/ShareHelper;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 6
    .line 7
    invoke-static {v0}, Lcom/intsig/camscanner/share/ShareHelper;->ooo8o〇o〇(Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇o〇:Lcom/intsig/camscanner/share/ShareHelper;

    .line 12
    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->Oo08:Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;

    .line 14
    .line 15
    iget-boolean v1, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇o00〇〇Oo:Z

    .line 16
    .line 17
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;->〇o00〇〇Oo(Z)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    if-eqz v1, :cond_1

    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 28
    .line 29
    const v0, 0x7f130fbc

    .line 30
    .line 31
    .line 32
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    .line 33
    .line 34
    .line 35
    return-void

    .line 36
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->Oo08:Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;

    .line 37
    .line 38
    iget-boolean v2, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇o00〇〇Oo:Z

    .line 39
    .line 40
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;->〇〇888(Z)Ljava/util/List;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    invoke-static {v0, v1}, Lcom/intsig/camscanner/word/GenerateWordClient;->OO0o〇〇(Ljava/lang/String;Ljava/util/List;)Lcom/intsig/camscanner/word/WordSourceData;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareWord;

    .line 49
    .line 50
    iget-object v2, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 51
    .line 52
    invoke-direct {v1, v2, v0}, Lcom/intsig/camscanner/share/type/ShareWord;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/word/WordSourceData;)V

    .line 53
    .line 54
    .line 55
    iput-object v1, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->O8:Lcom/intsig/camscanner/share/type/ShareWord;

    .line 56
    .line 57
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/share/type/ShareWord;->Oo(Z)V

    .line 58
    .line 59
    .line 60
    iget-object p1, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->O8:Lcom/intsig/camscanner/share/type/ShareWord;

    .line 61
    .line 62
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->BATCH_OCR_RESULT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 63
    .line 64
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/ShareWord;->oo(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 65
    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->O8:Lcom/intsig/camscanner/share/type/ShareWord;

    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->Oo08:Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;

    .line 70
    .line 71
    invoke-interface {v0}, Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;->O8()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/ShareWord;->Oo〇o(Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    iget-object p1, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇o〇:Lcom/intsig/camscanner/share/ShareHelper;

    .line 79
    .line 80
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 81
    .line 82
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/ShareHelper;->〇〇00O〇0o(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 83
    .line 84
    .line 85
    iget-object p1, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇o〇:Lcom/intsig/camscanner/share/ShareHelper;

    .line 86
    .line 87
    iget-object v0, p0, Lcom/intsig/camscanner/tools/OcrTextShareClient;->O8:Lcom/intsig/camscanner/share/type/ShareWord;

    .line 88
    .line 89
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 90
    .line 91
    .line 92
    const-string p1, "CSOcr"

    .line 93
    .line 94
    const-string v0, "share_word"

    .line 95
    .line 96
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public 〇〇888()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/tools/OcrTextShareClient;->oO80(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

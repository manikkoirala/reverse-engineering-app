.class public Lcom/intsig/camscanner/topic/TopicPreviewFragment;
.super Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;
.source "TopicPreviewFragment.java"

# interfaces
.implements Lcom/intsig/camscanner/topic/contract/TopicContract$View;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/intsig/camscanner/topic/contract/IEditTopic;
.implements Lcom/intsig/camscanner/topic/contract/ITopicAdapter;
.implements Lcom/intsig/camscanner/topic/adapter/JigsawTemplateAdapter$TemplateClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;",
        "Lcom/intsig/camscanner/topic/contract/TopicContract$View<",
        "Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/intsig/camscanner/topic/contract/IEditTopic;",
        "Lcom/intsig/camscanner/topic/contract/ITopicAdapter;",
        "Lcom/intsig/camscanner/topic/adapter/JigsawTemplateAdapter$TemplateClickListener;"
    }
.end annotation


# static fields
.field private static final oOoo80oO:Lcom/intsig/camscanner/topic/model/JigsawTemplate;


# instance fields
.field private O0O:Lcom/intsig/camscanner/topic/model/TopicModel;

.field private O88O:Z

.field private OO〇00〇8oO:Lcom/intsig/view/ImageTextButton;

.field private Oo0〇Ooo:I

.field private Oo80:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

.field private Ooo08:I

.field private O〇08oOOO0:Ljava/lang/String;

.field private O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

.field private o0OoOOo0:Z

.field private o8o:Lcom/intsig/view/ImageTextButton;

.field private final o8oOOo:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

.field private o8〇OO:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field private o8〇OO0〇0o:Lcom/intsig/view/ImageTextButton;

.field private oO00〇o:Lcom/intsig/camscanner/tools/UndoTool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/camscanner/tools/UndoTool<",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;"
        }
    .end annotation
.end field

.field private oOO0880O:Lcom/intsig/camscanner/tools/UndoTool$UndoToolCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/camscanner/tools/UndoTool$UndoToolCallback<",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;"
        }
    .end annotation
.end field

.field private oOO〇〇:Landroid/widget/LinearLayout;

.field private oOo0:Lcom/intsig/camscanner/topic/view/FloatActionView;

.field private oOo〇8o008:Landroid/view/View;

.field private oO〇8O8oOo:I

.field private oo8ooo8O:Lcom/intsig/view/ImageTextButton;

.field private ooO:Ljava/lang/String;

.field private ooo0〇〇O:Landroidx/collection/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;"
        }
    .end annotation
.end field

.field private o〇oO:Landroidx/recyclerview/widget/RecyclerView;

.field private o〇o〇Oo88:Lcom/intsig/app/BaseProgressDialog;

.field private 〇00O0:Z

.field private 〇08〇o0O:Lcom/intsig/camscanner/topic/adapter/JigsawTemplateAdapter;

.field private 〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

.field private 〇0O〇O00O:Lcom/intsig/app/BaseProgressDialog;

.field private 〇8〇oO〇〇8o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;"
        }
    .end annotation
.end field

.field private 〇OO8ooO8〇:Z

.field private 〇OO〇00〇0O:I

.field private 〇O〇〇O8:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

.field private 〇o0O:Z

.field private 〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

.field private 〇〇o〇:Lcom/intsig/app/BaseProgressDialog;

.field private 〇〇〇0o〇〇0:Lcom/intsig/camscanner/datastruct/PageProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->defaultShowTemplate()Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sput-object v0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOoo80oO:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;-><init>(Lcom/intsig/camscanner/topic/contract/TopicContract$View;)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 10
    .line 11
    new-instance v0, Landroidx/collection/ArrayMap;

    .line 12
    .line 13
    invoke-direct {v0}, Landroidx/collection/ArrayMap;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->ooo0〇〇O:Landroidx/collection/ArrayMap;

    .line 17
    .line 18
    sget-object v0, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->A4:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8oOOo:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O〇〇O8:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 23
    .line 24
    const/4 v0, 0x0

    .line 25
    iput-boolean v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇o0O:Z

    .line 26
    .line 27
    iput-boolean v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O88O:Z

    .line 28
    .line 29
    sget-object v1, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOoo80oO:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 30
    .line 31
    iput-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 32
    .line 33
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NONE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 34
    .line 35
    iput-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8〇OO:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 36
    .line 37
    iput-boolean v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇OO8ooO8〇:Z

    .line 38
    .line 39
    const/4 v1, 0x2

    .line 40
    iput v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇OO〇00〇0O:I

    .line 41
    .line 42
    const/4 v1, -0x1

    .line 43
    iput v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->Oo0〇Ooo:I

    .line 44
    .line 45
    iput v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oO〇8O8oOo:I

    .line 46
    .line 47
    iput-boolean v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o0OoOOo0:Z

    .line 48
    .line 49
    new-instance v0, Lcom/intsig/camscanner/tools/UndoTool;

    .line 50
    .line 51
    invoke-direct {v0}, Lcom/intsig/camscanner/tools/UndoTool;-><init>()V

    .line 52
    .line 53
    .line 54
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oO00〇o:Lcom/intsig/camscanner/tools/UndoTool;

    .line 55
    .line 56
    new-instance v0, L〇0o88O/o〇0;

    .line 57
    .line 58
    invoke-direct {v0, p0}, L〇0o88O/o〇0;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V

    .line 59
    .line 60
    .line 61
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOO0880O:Lcom/intsig/camscanner/tools/UndoTool$UndoToolCallback;

    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private synthetic O00OoO〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const/4 p1, 0x1

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O0o0(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic O0O0〇(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OOo00(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private O0o0(Z)V
    .locals 5

    .line 1
    const v0, 0x7f130418

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/Util;->oo〇(Ljava/lang/String;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const/4 v1, 0x0

    .line 15
    if-eqz p1, :cond_1

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O0〇()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    if-nez v2, :cond_0

    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇0()Landroid/content/Context;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    iget-object v3, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 32
    .line 33
    iget-object v3, v3, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 34
    .line 35
    const/4 v4, 0x1

    .line 36
    invoke-static {v2, v3, v4, p1}, Lcom/intsig/camscanner/util/Util;->O〇8O8〇008(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    :cond_0
    const-string v2, "save_all"

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    const-string v2, "save_qbook"

    .line 44
    .line 45
    move-object p1, v1

    .line 46
    :goto_0
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇8o0o0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private O0〇()Ljava/lang/String;
    .locals 2

    .line 1
    const v0, 0x7f130419

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/Util;->oo〇(Ljava/lang/String;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic O0〇0(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O88()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private synthetic O80OO(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    iget-object p2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O〇〇O8:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 9
    .line 10
    invoke-static {p1, p2}, Lcom/intsig/camscanner/topic/TopicInchSelectActivity;->O〇0O〇Oo〇o(Landroid/content/Context;Lcom/intsig/camscanner/topic/model/PageSizeEnumType;)Landroid/content/Intent;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    const/4 p2, 0x1

    .line 15
    invoke-virtual {p0, p1, p2}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private synthetic O88()V
    .locals 6

    .line 1
    const-string v0, "TopicPreviewFragment"

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    new-array v2, v1, [Z

    .line 5
    .line 6
    :try_start_0
    iget-object v3, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 7
    .line 8
    invoke-interface {v3}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->Oo08()[Z

    .line 9
    .line 10
    .line 11
    move-result-object v3

    .line 12
    const/4 v4, 0x0

    .line 13
    aget-boolean v5, v3, v4

    .line 14
    .line 15
    if-eqz v5, :cond_0

    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    aget-boolean v1, v3, v1

    .line 19
    .line 20
    aput-boolean v1, v2, v4
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 21
    .line 22
    new-instance v1, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string v3, "saveCertificatesByPoints  :  isSuccess"

    .line 28
    .line 29
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    aget-boolean v3, v2, v4

    .line 33
    .line 34
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    if-eqz v1, :cond_2

    .line 49
    .line 50
    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    .line 51
    .line 52
    .line 53
    move-result v3

    .line 54
    if-eqz v3, :cond_1

    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_1
    new-instance v0, L〇0o88O/〇o00〇〇Oo;

    .line 58
    .line 59
    invoke-direct {v0, p0, v2}, L〇0o88O/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;[Z)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 63
    .line 64
    .line 65
    return-void

    .line 66
    :cond_2
    :goto_0
    const-string v1, "activity == null || activity.isFinishing()"

    .line 67
    .line 68
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    return-void

    .line 72
    :catch_0
    move-exception v1

    .line 73
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 74
    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private O880O〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "TopicPreviewFragment"

    .line 6
    .line 7
    const-string v1, "It occurs data exception "

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    iget v0, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo0:I

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    if-eq v0, v1, :cond_4

    .line 17
    .line 18
    const/4 v1, 0x4

    .line 19
    if-eq v0, v1, :cond_3

    .line 20
    .line 21
    const/16 v1, 0x8

    .line 22
    .line 23
    if-eq v0, v1, :cond_3

    .line 24
    .line 25
    const/16 v1, 0xd

    .line 26
    .line 27
    if-eq v0, v1, :cond_2

    .line 28
    .line 29
    const/16 v1, 0x71

    .line 30
    .line 31
    if-eq v0, v1, :cond_3

    .line 32
    .line 33
    const/16 v1, 0x72

    .line 34
    .line 35
    if-eq v0, v1, :cond_1

    .line 36
    .line 37
    sget-object v0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOoo80oO:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 38
    .line 39
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->CN_TRAVEL:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 43
    .line 44
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->BANK_CARD:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 48
    .line 49
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_3
    sget-object v0, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->CN_DRIVER:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 53
    .line 54
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_4
    sget-object v0, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->ID_CARD:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 58
    .line 59
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 60
    .line 61
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 62
    .line 63
    check-cast v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;

    .line 64
    .line 65
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 66
    .line 67
    iput-object v1, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇〇808〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private O8O()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->ENTERPRISE_CERTIFICATE:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic O8〇8〇O80(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇0o8〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private O8〇o0〇〇8(Lcom/intsig/camscanner/topic/model/TopicModel;)V
    .locals 8

    .line 1
    iget-object v0, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->〇o〇:Landroid/graphics/Point;

    .line 2
    .line 3
    iget-object v1, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->O8:Lcom/intsig/camscanner/util/ParcelSize;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    if-lez v2, :cond_5

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    if-gtz v2, :cond_0

    .line 16
    .line 17
    goto/16 :goto_2

    .line 18
    .line 19
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->〇08O〇00〇o:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 20
    .line 21
    invoke-virtual {v2}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    iget-object v3, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->〇08O〇00〇o:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 26
    .line 27
    invoke-virtual {v3}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    iget v4, v0, Landroid/graphics/Point;->y:I

    .line 32
    .line 33
    iget-object v5, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 34
    .line 35
    invoke-interface {v5}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->O8()I

    .line 36
    .line 37
    .line 38
    move-result v5

    .line 39
    add-int/2addr v4, v5

    .line 40
    :goto_0
    const/4 v5, 0x2

    .line 41
    if-lt v3, v2, :cond_2

    .line 42
    .line 43
    iget-object v6, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->〇08O〇00〇o:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 44
    .line 45
    invoke-virtual {v6, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object v6

    .line 49
    if-eqz v6, :cond_1

    .line 50
    .line 51
    new-array v7, v5, [I

    .line 52
    .line 53
    invoke-virtual {v6, v7}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 54
    .line 55
    .line 56
    const/4 v6, 0x1

    .line 57
    aget v6, v7, v6

    .line 58
    .line 59
    if-le v4, v6, :cond_1

    .line 60
    .line 61
    sub-int/2addr v4, v6

    .line 62
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    div-int/2addr v2, v5

    .line 67
    sub-int/2addr v4, v2

    .line 68
    goto :goto_1

    .line 69
    :cond_1
    add-int/lit8 v3, v3, -0x1

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_2
    const/4 v3, -0x1

    .line 73
    const/4 v4, -0x1

    .line 74
    :goto_1
    const-string v2, "TopicPreviewFragment"

    .line 75
    .line 76
    if-gez v3, :cond_3

    .line 77
    .line 78
    const-string p1, "onFinishEdit error ! "

    .line 79
    .line 80
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    return-void

    .line 84
    :cond_3
    iget v0, v0, Landroid/graphics/Point;->x:I

    .line 85
    .line 86
    iget-object v6, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 87
    .line 88
    invoke-interface {v6}, Lcom/intsig/camscanner/topic/contract/TopicContract$IPageProperty;->〇o〇()I

    .line 89
    .line 90
    .line 91
    move-result v6

    .line 92
    sub-int/2addr v0, v6

    .line 93
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 94
    .line 95
    .line 96
    move-result v6

    .line 97
    div-int/2addr v6, v5

    .line 98
    sub-int/2addr v0, v6

    .line 99
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 100
    .line 101
    .line 102
    move-result v6

    .line 103
    add-int/2addr v6, v0

    .line 104
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 105
    .line 106
    .line 107
    move-result v1

    .line 108
    add-int/2addr v1, v4

    .line 109
    new-instance v7, Landroid/graphics/Rect;

    .line 110
    .line 111
    invoke-direct {v7, v0, v4, v6, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 112
    .line 113
    .line 114
    iput-object v7, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->oO80:Landroid/graphics/Rect;

    .line 115
    .line 116
    new-instance v7, Landroid/graphics/Point;

    .line 117
    .line 118
    add-int/2addr v0, v6

    .line 119
    div-int/2addr v0, v5

    .line 120
    add-int/2addr v4, v1

    .line 121
    div-int/2addr v4, v5

    .line 122
    invoke-direct {v7, v0, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 123
    .line 124
    .line 125
    iput-object v7, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->〇o〇:Landroid/graphics/Point;

    .line 126
    .line 127
    iget-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->OO:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 128
    .line 129
    invoke-virtual {v0, v3}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForLayoutPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 130
    .line 131
    .line 132
    move-result-object v0

    .line 133
    if-eqz v0, :cond_4

    .line 134
    .line 135
    iget-object v1, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->o〇00O:Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;

    .line 136
    .line 137
    invoke-virtual {v1, v0, v3, p1}, Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;->〇O00(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;ILcom/intsig/camscanner/topic/model/TopicModel;)V

    .line 138
    .line 139
    .line 140
    goto :goto_2

    .line 141
    :cond_4
    const-string p1, "onFinishEdit error"

    .line 142
    .line 143
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    .line 145
    .line 146
    :cond_5
    :goto_2
    return-void
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private OO0O()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇8o8O〇O()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO88〇OOO()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-lez v0, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO88〇OOO()V

    .line 21
    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 25
    .line 26
    invoke-interface {v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->〇80〇808〇O()Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    const v1, 0x7f131d10

    .line 31
    .line 32
    .line 33
    if-eqz v0, :cond_2

    .line 34
    .line 35
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 36
    .line 37
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    invoke-direct {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    const v1, 0x7f130405

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    const v1, 0x7f130019

    .line 56
    .line 57
    .line 58
    const/4 v2, 0x0

    .line 59
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 68
    .line 69
    .line 70
    goto :goto_1

    .line 71
    :cond_2
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 72
    .line 73
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    invoke-direct {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    const v1, 0x7f130339

    .line 85
    .line 86
    .line 87
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    new-instance v1, L〇0o88O/OO0o〇〇〇〇0;

    .line 92
    .line 93
    invoke-direct {v1, p0}, L〇0o88O/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V

    .line 94
    .line 95
    .line 96
    const v2, 0x7f13002a

    .line 97
    .line 98
    .line 99
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 100
    .line 101
    .line 102
    move-result-object v0

    .line 103
    new-instance v1, L〇0o88O/〇8o8o〇;

    .line 104
    .line 105
    invoke-direct {v1, p0}, L〇0o88O/〇8o8o〇;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V

    .line 106
    .line 107
    .line 108
    const v2, 0x7f130029

    .line 109
    .line 110
    .line 111
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 112
    .line 113
    .line 114
    move-result-object v0

    .line 115
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 120
    .line 121
    .line 122
    :goto_1
    return-void
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private synthetic OO0o(Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O8oOo80()Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo8〇〇()V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO〇000()V

    .line 12
    .line 13
    .line 14
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oo()V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private OO8〇O8()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O8o08O8O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo0:Lcom/intsig/camscanner/topic/view/FloatActionView;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/view/FloatActionView;->〇〇808〇()V

    .line 10
    .line 11
    .line 12
    :cond_0
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 13
    .line 14
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 19
    .line 20
    .line 21
    const v1, 0x7f131ede

    .line 22
    .line 23
    .line 24
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const v1, 0x7f130584

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    new-instance v1, L〇0o88O/oO80;

    .line 44
    .line 45
    invoke-direct {v1}, L〇0o88O/oO80;-><init>()V

    .line 46
    .line 47
    .line 48
    const v2, 0x7f13057e

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    new-instance v1, L〇0o88O/〇80〇808〇O;

    .line 56
    .line 57
    invoke-direct {v1, p0}, L〇0o88O/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V

    .line 58
    .line 59
    .line 60
    const v2, 0x7f131e36

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private OOo00(Ljava/lang/String;)V
    .locals 4

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [Landroid/util/Pair;

    .line 3
    .line 4
    new-instance v1, Landroid/util/Pair;

    .line 5
    .line 6
    const-string v2, "from"

    .line 7
    .line 8
    const-string v3, "qbook_mode"

    .line 9
    .line 10
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 11
    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    aput-object v1, v0, v2

    .line 15
    .line 16
    new-instance v1, Landroid/util/Pair;

    .line 17
    .line 18
    iget-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O〇〇O8:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 19
    .line 20
    if-eqz v2, :cond_0

    .line 21
    .line 22
    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    goto :goto_0

    .line 27
    :cond_0
    const-string v2, ""

    .line 28
    .line 29
    :goto_0
    const-string v3, "size"

    .line 30
    .line 31
    invoke-direct {v1, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 32
    .line 33
    .line 34
    const/4 v2, 0x1

    .line 35
    aput-object v1, v0, v2

    .line 36
    .line 37
    const-string v1, "CSCollagePreview"

    .line 38
    .line 39
    invoke-static {v1, p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private OO〇〇o0oO()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 2
    .line 3
    iget-boolean v0, v0, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->isEnableRoundCorner:Z

    .line 4
    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private OoO〇OOo8o(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O8oOo80()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇0Oo0880()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/NewUserGuideCleaner;->O8()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oO:Landroidx/recyclerview/widget/RecyclerView;

    .line 20
    .line 21
    new-instance v1, L〇0o88O/OO0o〇〇;

    .line 22
    .line 23
    invoke-direct {v1, p0, p1}, L〇0o88O/OO0o〇〇;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Ljava/util/List;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->OO:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 31
    .line 32
    new-instance v0, L〇0o88O/Oooo8o0〇;

    .line 33
    .line 34
    invoke-direct {v0, p0}, L〇0o88O/Oooo8o0〇;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8880()Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-eqz v0, :cond_2

    .line 46
    .line 47
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/NewUserGuideCleaner;->O8()Z

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    if-eqz v0, :cond_2

    .line 52
    .line 53
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇8〇〇8o(Ljava/util/List;)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->OO:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 58
    .line 59
    new-instance v0, L〇0o88O/Oooo8o0〇;

    .line 60
    .line 61
    invoke-direct {v0, p0}, L〇0o88O/Oooo8o0〇;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 65
    .line 66
    .line 67
    :goto_0
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static synthetic Ooo8o(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O80OO(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private OooO〇()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/base/ToolbarThemeGet;->Oo08()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo〇8o008:Landroid/view/View;

    .line 8
    .line 9
    const/high16 v1, -0x1000000

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 12
    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo〇8o008:Landroid/view/View;

    .line 15
    .line 16
    const v1, 0x7f0a0b0e

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    check-cast v0, Landroid/widget/LinearLayout;

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOO〇〇:Landroid/widget/LinearLayout;

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo〇8o008:Landroid/view/View;

    .line 28
    .line 29
    const v1, 0x7f0a18b0

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    check-cast v0, Landroid/widget/TextView;

    .line 37
    .line 38
    iput-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->〇OOo8〇0:Landroid/widget/TextView;

    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo〇8o008:Landroid/view/View;

    .line 41
    .line 42
    const v1, 0x7f0a080f

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    check-cast v0, Lcom/intsig/view/ImageTextButton;

    .line 50
    .line 51
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO〇00〇8oO:Lcom/intsig/view/ImageTextButton;

    .line 52
    .line 53
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo〇8o008:Landroid/view/View;

    .line 54
    .line 55
    const v1, 0x7f0a080e

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    check-cast v0, Lcom/intsig/view/ImageTextButton;

    .line 63
    .line 64
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8〇OO0〇0o:Lcom/intsig/view/ImageTextButton;

    .line 65
    .line 66
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO〇00〇8oO:Lcom/intsig/view/ImageTextButton;

    .line 70
    .line 71
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    .line 73
    .line 74
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo〇8o008:Landroid/view/View;

    .line 75
    .line 76
    const v1, 0x7f0a0662

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    check-cast v0, Lcom/intsig/camscanner/topic/view/FloatActionView;

    .line 84
    .line 85
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo0:Lcom/intsig/camscanner/topic/view/FloatActionView;

    .line 86
    .line 87
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/topic/view/FloatActionView;->setFloatActionViewListener(Lcom/intsig/camscanner/topic/contract/IEditTopic;)V

    .line 88
    .line 89
    .line 90
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo〇8o008:Landroid/view/View;

    .line 91
    .line 92
    const v1, 0x7f0a0fb3

    .line 93
    .line 94
    .line 95
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 100
    .line 101
    const/4 v1, 0x0

    .line 102
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 103
    .line 104
    .line 105
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    .line 107
    .line 108
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo〇8o008:Landroid/view/View;

    .line 109
    .line 110
    const v2, 0x7f0a1070

    .line 111
    .line 112
    .line 113
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    check-cast v0, Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 118
    .line 119
    iput-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->OO:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 120
    .line 121
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    check-cast v0, Landroidx/recyclerview/widget/DefaultItemAnimator;

    .line 126
    .line 127
    if-eqz v0, :cond_1

    .line 128
    .line 129
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/SimpleItemAnimator;->setSupportsChangeAnimations(Z)V

    .line 130
    .line 131
    .line 132
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->oOo〇08〇()V

    .line 133
    .line 134
    .line 135
    iget-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->OO:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 136
    .line 137
    iget-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo0:Lcom/intsig/camscanner/topic/view/FloatActionView;

    .line 138
    .line 139
    invoke-virtual {v2}, Lcom/intsig/camscanner/topic/view/FloatActionView;->getRvOnScrollListener()Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;

    .line 140
    .line 141
    .line 142
    move-result-object v2

    .line 143
    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    .line 144
    .line 145
    .line 146
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O8oOo80()Z

    .line 147
    .line 148
    .line 149
    move-result v0

    .line 150
    if-nez v0, :cond_2

    .line 151
    .line 152
    return-void

    .line 153
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo〇8o008:Landroid/view/View;

    .line 154
    .line 155
    const v2, 0x7f0a0811

    .line 156
    .line 157
    .line 158
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 159
    .line 160
    .line 161
    move-result-object v0

    .line 162
    check-cast v0, Lcom/intsig/view/ImageTextButton;

    .line 163
    .line 164
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8o:Lcom/intsig/view/ImageTextButton;

    .line 165
    .line 166
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo〇8o008:Landroid/view/View;

    .line 167
    .line 168
    const v2, 0x7f0a0812

    .line 169
    .line 170
    .line 171
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 172
    .line 173
    .line 174
    move-result-object v0

    .line 175
    check-cast v0, Lcom/intsig/view/ImageTextButton;

    .line 176
    .line 177
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oo8ooo8O:Lcom/intsig/view/ImageTextButton;

    .line 178
    .line 179
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo〇8o008:Landroid/view/View;

    .line 180
    .line 181
    const v2, 0x7f0a106d

    .line 182
    .line 183
    .line 184
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 189
    .line 190
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oO:Landroidx/recyclerview/widget/RecyclerView;

    .line 191
    .line 192
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8o:Lcom/intsig/view/ImageTextButton;

    .line 193
    .line 194
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 195
    .line 196
    .line 197
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oo8ooo8O:Lcom/intsig/view/ImageTextButton;

    .line 198
    .line 199
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 200
    .line 201
    .line 202
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oO:Landroidx/recyclerview/widget/RecyclerView;

    .line 203
    .line 204
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 205
    .line 206
    .line 207
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O8Oo(Z)V

    .line 208
    .line 209
    .line 210
    new-instance v0, Lcom/intsig/camscanner/topic/view/SlowLayoutManager;

    .line 211
    .line 212
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 213
    .line 214
    .line 215
    move-result-object v2

    .line 216
    invoke-direct {v0, v2, v1, v1}, Lcom/intsig/camscanner/topic/view/SlowLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 217
    .line 218
    .line 219
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oO:Landroidx/recyclerview/widget/RecyclerView;

    .line 220
    .line 221
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 222
    .line 223
    .line 224
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8o:Lcom/intsig/view/ImageTextButton;

    .line 225
    .line 226
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    .line 228
    .line 229
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oo8ooo8O:Lcom/intsig/view/ImageTextButton;

    .line 230
    .line 231
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    .line 233
    .line 234
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 235
    .line 236
    .line 237
    move-result-object v0

    .line 238
    instance-of v1, v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 239
    .line 240
    if-eqz v1, :cond_3

    .line 241
    .line 242
    iget-object v1, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->OO:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 243
    .line 244
    check-cast v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 245
    .line 246
    invoke-virtual {v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇o08()Landroidx/appcompat/widget/Toolbar;

    .line 247
    .line 248
    .line 249
    move-result-object v0

    .line 250
    invoke-static {v1, v0}, Lcom/intsig/camscanner/util/RvUtils;->〇080(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;)V

    .line 251
    .line 252
    .line 253
    :cond_3
    return-void
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private O〇00O(Landroidx/fragment/app/FragmentActivity;)V
    .locals 4

    .line 1
    const-string v0, "TopicPreviewFragment"

    .line 2
    .line 3
    const-string v1, "queryAndCacheLeftTopicNum"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/utils/CommonLoadingTask;

    .line 9
    .line 10
    new-instance v1, Lcom/intsig/camscanner/topic/TopicPreviewFragment$7;

    .line 11
    .line 12
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment$7;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V

    .line 13
    .line 14
    .line 15
    const v2, 0x7f13024d

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    const/4 v3, 0x1

    .line 23
    invoke-direct {v0, p1, v1, v2, v3}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;Z)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/utils/CommonLoadingTask;->O8()V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private O〇080〇o0()I
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/topic/TopicPreviewFragment$8;->〇080:[I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 4
    .line 5
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    aget v0, v0, v1

    .line 10
    .line 11
    packed-switch v0, :pswitch_data_0

    .line 12
    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    goto :goto_0

    .line 16
    :pswitch_0
    const/4 v0, 0x7

    .line 17
    goto :goto_0

    .line 18
    :pswitch_1
    const/16 v0, 0x9

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :pswitch_2
    const/16 v0, 0x72

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :pswitch_3
    const/16 v0, 0x71

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :pswitch_4
    const/4 v0, 0x6

    .line 28
    goto :goto_0

    .line 29
    :pswitch_5
    const/4 v0, 0x3

    .line 30
    goto :goto_0

    .line 31
    :pswitch_6
    const/4 v0, 0x5

    .line 32
    goto :goto_0

    .line 33
    :pswitch_7
    const/4 v0, 0x4

    .line 34
    goto :goto_0

    .line 35
    :pswitch_8
    const/16 v0, 0xd

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :pswitch_9
    const/16 v0, 0xa

    .line 39
    .line 40
    :goto_0
    return v0

    .line 41
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private O〇0o8o8〇(Ljava/lang/String;)V
    .locals 4

    .line 1
    const/4 v0, -0x1

    .line 2
    :try_start_0
    iput v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->Oo0〇Ooo:I

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    :goto_0
    iget-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇8〇oO〇〇8o:Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    if-ge v1, v2, :cond_1

    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇8〇oO〇〇8o:Ljava/util/ArrayList;

    .line 14
    .line 15
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    check-cast v2, Lcom/intsig/camscanner/datastruct/PageProperty;

    .line 20
    .line 21
    iget-object v3, v2, Lcom/intsig/camscanner/datastruct/PageProperty;->〇OOo8〇0:Ljava/lang/String;

    .line 22
    .line 23
    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 24
    .line 25
    .line 26
    move-result v3

    .line 27
    if-eqz v3, :cond_0

    .line 28
    .line 29
    iput-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/datastruct/PageProperty;

    .line 30
    .line 31
    iput v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->Oo0〇Ooo:I

    .line 32
    .line 33
    goto :goto_1

    .line 34
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_1
    :goto_1
    iget p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->Oo0〇Ooo:I

    .line 38
    .line 39
    if-le p1, v0, :cond_2

    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇8〇oO〇〇8o:Ljava/util/ArrayList;

    .line 42
    .line 43
    invoke-static {v0}, Lcom/intsig/utils/ListUtils;->〇080(Ljava/util/List;)I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-ge p1, v0, :cond_2

    .line 48
    .line 49
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇8〇oO〇〇8o:Ljava/util/ArrayList;

    .line 50
    .line 51
    iget v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->Oo0〇Ooo:I

    .line 52
    .line 53
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    .line 55
    .line 56
    goto :goto_2

    .line 57
    :catch_0
    move-exception p1

    .line 58
    const-string v0, "TopicPreviewFragment"

    .line 59
    .line 60
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61
    .line 62
    .line 63
    :cond_2
    :goto_2
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private O〇0o8〇()V
    .locals 4

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [Landroid/util/Pair;

    .line 3
    .line 4
    new-instance v1, Landroid/util/Pair;

    .line 5
    .line 6
    const-string v2, "from"

    .line 7
    .line 8
    const-string v3, "qbook_mode"

    .line 9
    .line 10
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 11
    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    aput-object v1, v0, v2

    .line 15
    .line 16
    new-instance v1, Landroid/util/Pair;

    .line 17
    .line 18
    iget-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇08oOOO0:Ljava/lang/String;

    .line 19
    .line 20
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    if-eqz v2, :cond_0

    .line 25
    .line 26
    const-string v2, "no"

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    const-string v2, "yes"

    .line 30
    .line 31
    :goto_0
    const-string v3, "watermark"

    .line 32
    .line 33
    invoke-direct {v1, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 34
    .line 35
    .line 36
    const/4 v2, 0x1

    .line 37
    aput-object v1, v0, v2

    .line 38
    .line 39
    const-string v1, "CSCollagePreview"

    .line 40
    .line 41
    const-string v2, "complete"

    .line 42
    .line 43
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private O〇8〇008(Z)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO〇00〇8oO:Lcom/intsig/view/ImageTextButton;

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->setEnableTouch(Z)V

    .line 7
    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO〇00〇8oO:Lcom/intsig/view/ImageTextButton;

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->A4:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->getIconRes()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->setTipIcon(I)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    const v0, 0x7f06007e

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO〇00〇8oO:Lcom/intsig/view/ImageTextButton;

    .line 32
    .line 33
    invoke-virtual {v0, p1}, Lcom/intsig/view/ImageTextButton;->setIconAndTextColor(I)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO〇00〇8oO:Lcom/intsig/view/ImageTextButton;

    .line 38
    .line 39
    const/4 v0, 0x1

    .line 40
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->setEnableTouch(Z)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    sget-object v0, Lcom/intsig/base/ToolbarThemeGet;->〇080:Lcom/intsig/base/ToolbarThemeGet;

    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/base/ToolbarThemeGet;->〇o〇()I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    .line 54
    .line 55
    .line 56
    move-result p1

    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO〇00〇8oO:Lcom/intsig/view/ImageTextButton;

    .line 58
    .line 59
    invoke-virtual {v0, p1}, Lcom/intsig/view/ImageTextButton;->setIconAndTextColor(I)V

    .line 60
    .line 61
    .line 62
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO〇00〇8oO:Lcom/intsig/view/ImageTextButton;

    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O〇〇O8:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 65
    .line 66
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->getIconRes()I

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->setTipIcon(I)V

    .line 71
    .line 72
    .line 73
    :goto_0
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private O〇o8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oO:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O8Oo(Z)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private O〇〇O80o8()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇0()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 6
    .line 7
    iget-wide v1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 8
    .line 9
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->O〇8O8〇008(Landroid/content/Context;J)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    const v2, 0x7f130113

    .line 18
    .line 19
    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    goto :goto_0

    .line 27
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 28
    .line 29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    const-string v2, "-"

    .line 40
    .line 41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇0()Landroid/content/Context;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    const/4 v2, 0x1

    .line 56
    invoke-static {v1, v0, v2}, Lcom/intsig/camscanner/util/Util;->OOO(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    return-object v0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private synthetic O〇〇o8O(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O0o0(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O00OoO〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private synthetic o0O0O〇〇〇0([Z)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    aget-boolean p1, p1, v0

    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇〇O80o8()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇〇OOO〇〇(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    const v0, 0x7f1301ab

    .line 19
    .line 20
    .line 21
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 22
    .line 23
    .line 24
    :goto_0
    return-void
.end method

.method private synthetic o0OO(Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇o〇88(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private o0Oo(Lcom/intsig/camscanner/topic/model/TopicModel;)I
    .locals 9

    .line 1
    iget-object v0, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->〇o〇:Landroid/graphics/Point;

    .line 2
    .line 3
    iget-object v1, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->O8:Lcom/intsig/camscanner/util/ParcelSize;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    const/4 v3, -0x1

    .line 10
    if-lez v2, :cond_4

    .line 11
    .line 12
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-gtz v2, :cond_0

    .line 17
    .line 18
    goto :goto_2

    .line 19
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->〇08O〇00〇o:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 20
    .line 21
    invoke-virtual {v2}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    iget-object v4, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->〇08O〇00〇o:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 26
    .line 27
    invoke-virtual {v4}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    .line 28
    .line 29
    .line 30
    move-result v4

    .line 31
    iget v5, v0, Landroid/graphics/Point;->y:I

    .line 32
    .line 33
    iget-object v6, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 34
    .line 35
    invoke-interface {v6}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->O8()I

    .line 36
    .line 37
    .line 38
    move-result v6

    .line 39
    add-int/2addr v5, v6

    .line 40
    :goto_0
    const/4 v6, 0x2

    .line 41
    if-lt v4, v2, :cond_2

    .line 42
    .line 43
    iget-object v7, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->〇08O〇00〇o:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 44
    .line 45
    invoke-virtual {v7, v4}, Landroidx/recyclerview/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object v7

    .line 49
    if-eqz v7, :cond_1

    .line 50
    .line 51
    new-array v8, v6, [I

    .line 52
    .line 53
    invoke-virtual {v7, v8}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 54
    .line 55
    .line 56
    const/4 v7, 0x1

    .line 57
    aget v7, v8, v7

    .line 58
    .line 59
    if-le v5, v7, :cond_1

    .line 60
    .line 61
    sub-int/2addr v5, v7

    .line 62
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    div-int/2addr v2, v6

    .line 67
    sub-int/2addr v5, v2

    .line 68
    goto :goto_1

    .line 69
    :cond_1
    add-int/lit8 v4, v4, -0x1

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_2
    const/4 v4, -0x1

    .line 73
    const/4 v5, -0x1

    .line 74
    :goto_1
    if-gez v4, :cond_3

    .line 75
    .line 76
    const-string p1, "TopicPreviewFragment"

    .line 77
    .line 78
    const-string v0, "onFinishEdit error ! "

    .line 79
    .line 80
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    return v3

    .line 84
    :cond_3
    iget v0, v0, Landroid/graphics/Point;->x:I

    .line 85
    .line 86
    iget-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 87
    .line 88
    invoke-interface {v2}, Lcom/intsig/camscanner/topic/contract/TopicContract$IPageProperty;->〇o〇()I

    .line 89
    .line 90
    .line 91
    move-result v2

    .line 92
    sub-int/2addr v0, v2

    .line 93
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 94
    .line 95
    .line 96
    move-result v2

    .line 97
    div-int/2addr v2, v6

    .line 98
    sub-int/2addr v0, v2

    .line 99
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 100
    .line 101
    .line 102
    move-result v2

    .line 103
    add-int/2addr v2, v0

    .line 104
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 105
    .line 106
    .line 107
    move-result v1

    .line 108
    add-int/2addr v1, v5

    .line 109
    new-instance v3, Landroid/graphics/Rect;

    .line 110
    .line 111
    invoke-direct {v3, v0, v5, v2, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 112
    .line 113
    .line 114
    iput-object v3, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->oO80:Landroid/graphics/Rect;

    .line 115
    .line 116
    new-instance v3, Landroid/graphics/Point;

    .line 117
    .line 118
    add-int/2addr v0, v2

    .line 119
    div-int/2addr v0, v6

    .line 120
    add-int/2addr v5, v1

    .line 121
    div-int/2addr v5, v6

    .line 122
    invoke-direct {v3, v0, v5}, Landroid/graphics/Point;-><init>(II)V

    .line 123
    .line 124
    .line 125
    iput-object v3, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->〇o〇:Landroid/graphics/Point;

    .line 126
    .line 127
    return v4

    .line 128
    :cond_4
    :goto_2
    return v3
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private o808o8o08()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->〇80〇808〇O()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 10
    .line 11
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 16
    .line 17
    .line 18
    const v1, 0x7f131d10

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const v1, 0x7f130405

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const v1, 0x7f130019

    .line 33
    .line 34
    .line 35
    const/4 v2, 0x0

    .line 36
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 45
    .line 46
    .line 47
    goto/16 :goto_2

    .line 48
    .line 49
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇08〇o0O:Lcom/intsig/camscanner/topic/adapter/JigsawTemplateAdapter;

    .line 50
    .line 51
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/adapter/JigsawTemplateAdapter;->OoO8()Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    if-eqz v0, :cond_1

    .line 56
    .line 57
    invoke-static {v0}, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->getTypeForLogAgent(Lcom/intsig/camscanner/topic/model/JigsawTemplate;)Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    if-nez v1, :cond_1

    .line 66
    .line 67
    const-string v1, "type"

    .line 68
    .line 69
    invoke-static {v0}, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->getTypeForLogAgent(Lcom/intsig/camscanner/topic/model/JigsawTemplate;)Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    const-string v2, "CSCollagePreview"

    .line 74
    .line 75
    const-string v3, "save"

    .line 76
    .line 77
    invoke-static {v2, v3, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 81
    .line 82
    .line 83
    move-result v0

    .line 84
    const-string v1, "TopicPreviewFragment"

    .line 85
    .line 86
    if-nez v0, :cond_5

    .line 87
    .line 88
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇8o8O〇O()Z

    .line 89
    .line 90
    .line 91
    move-result v0

    .line 92
    if-nez v0, :cond_5

    .line 93
    .line 94
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O8O()Z

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    if-nez v0, :cond_5

    .line 99
    .line 100
    iget-boolean v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇OO8ooO8〇:Z

    .line 101
    .line 102
    if-nez v0, :cond_5

    .line 103
    .line 104
    sget-object v0, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇080:Lcom/intsig/camscanner/ads/reward/AdRewardedManager;

    .line 105
    .line 106
    sget-object v2, Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;->PICTURE_PUZZLE:Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;

    .line 107
    .line 108
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇oo〇(Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;)Z

    .line 109
    .line 110
    .line 111
    move-result v0

    .line 112
    if-eqz v0, :cond_2

    .line 113
    .line 114
    goto :goto_1

    .line 115
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    .line 116
    .line 117
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    .line 119
    .line 120
    const-string v2, "is Vip\uff1a"

    .line 121
    .line 122
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 126
    .line 127
    .line 128
    move-result v2

    .line 129
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    const-string v2, " isFreeCertificateTemplate() "

    .line 133
    .line 134
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O8O()Z

    .line 138
    .line 139
    .line 140
    move-result v2

    .line 141
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 142
    .line 143
    .line 144
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object v0

    .line 148
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇ooO8Ooo〇()Z

    .line 152
    .line 153
    .line 154
    move-result v0

    .line 155
    if-eqz v0, :cond_4

    .line 156
    .line 157
    const-string v0, "CamScanner_CertMode"

    .line 158
    .line 159
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->oOO0880O(Ljava/lang/String;)I

    .line 160
    .line 161
    .line 162
    move-result v0

    .line 163
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0o〇()I

    .line 164
    .line 165
    .line 166
    move-result v2

    .line 167
    if-lt v2, v0, :cond_3

    .line 168
    .line 169
    const/4 v3, 0x1

    .line 170
    goto :goto_0

    .line 171
    :cond_3
    const/4 v3, 0x0

    .line 172
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    .line 173
    .line 174
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 175
    .line 176
    .line 177
    const-string v5, "pointsCost="

    .line 178
    .line 179
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    .line 181
    .line 182
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 183
    .line 184
    .line 185
    const-string v0, ",storagePoint="

    .line 186
    .line 187
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    .line 189
    .line 190
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 191
    .line 192
    .line 193
    const-string v0, ",hasEnoughPoints="

    .line 194
    .line 195
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 199
    .line 200
    .line 201
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 202
    .line 203
    .line 204
    move-result-object v0

    .line 205
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    .line 207
    .line 208
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇(Z)V

    .line 209
    .line 210
    .line 211
    goto :goto_2

    .line 212
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO88〇OOO()V

    .line 213
    .line 214
    .line 215
    goto :goto_2

    .line 216
    :cond_5
    :goto_1
    const-string v0, "button done vip account"

    .line 217
    .line 218
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    .line 220
    .line 221
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇〇O80o8()Ljava/lang/String;

    .line 222
    .line 223
    .line 224
    move-result-object v0

    .line 225
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇〇OOO〇〇(Ljava/lang/String;)V

    .line 226
    .line 227
    .line 228
    :goto_2
    return-void
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private o88(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;)V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 2
    .line 3
    iget-object v3, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->ooo0〇〇O:Landroidx/collection/ArrayMap;

    .line 4
    .line 5
    iget-object v4, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->Oo80:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 6
    .line 7
    iget v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇OO〇00〇0O:I

    .line 8
    .line 9
    const/4 v8, 0x1

    .line 10
    if-ne v1, v8, :cond_0

    .line 11
    .line 12
    const/4 v5, 0x1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v1, 0x0

    .line 15
    const/4 v5, 0x0

    .line 16
    :goto_0
    iget-object v6, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 17
    .line 18
    move-object v1, p1

    .line 19
    move-object v2, p2

    .line 20
    move-object v7, p3

    .line 21
    invoke-interface/range {v0 .. v7}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->OO0o〇〇(Ljava/lang/String;Ljava/lang/String;Landroidx/collection/ArrayMap;Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;ZLcom/intsig/camscanner/topic/model/JigsawTemplate;Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;)V

    .line 22
    .line 23
    .line 24
    sget-object p1, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇080:Lcom/intsig/camscanner/ads/reward/AdRewardedManager;

    .line 25
    .line 26
    sget-object p2, Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;->PICTURE_PUZZLE:Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;

    .line 27
    .line 28
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇oo〇(Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;)Z

    .line 29
    .line 30
    .line 31
    move-result p3

    .line 32
    if-eqz p3, :cond_1

    .line 33
    .line 34
    invoke-virtual {p1, p2, v8}, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->Oooo8o0〇(Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;I)V

    .line 35
    .line 36
    .line 37
    :cond_1
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇OoO0o0(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private o88o88(Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 6
    .line 7
    iget-object v1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 8
    .line 9
    const v2, 0x7f130009

    .line 10
    .line 11
    .line 12
    const/4 v3, 0x1

    .line 13
    new-instance v6, Lcom/intsig/camscanner/topic/TopicPreviewFragment$6;

    .line 14
    .line 15
    invoke-direct {v6, p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment$6;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    const-wide/16 v7, -0x1

    .line 19
    .line 20
    const/4 v9, 0x0

    .line 21
    iget-object v4, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 22
    .line 23
    iget-wide v10, v4, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 24
    .line 25
    move-object v4, p1

    .line 26
    move-object v5, p2

    .line 27
    invoke-static/range {v0 .. v11}, Lcom/intsig/camscanner/app/DialogUtils;->〇O〇80o08O(Landroid/app/Activity;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/app/DialogUtils$OnDocTitleEditListener;JZJ)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private o88oo〇O(Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->oO80()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const-string v1, "CSScan"

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const/4 p1, 0x0

    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->Oo80:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->o〇00O:Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;->〇00()V

    .line 19
    .line 20
    .line 21
    const-string p1, "scan_id_removemarket"

    .line 22
    .line 23
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oo8ooo8O:Lcom/intsig/view/ImageTextButton;

    .line 27
    .line 28
    const v0, 0x7f13052f

    .line 29
    .line 30
    .line 31
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->setTipText(I)V

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    iput-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->Oo80:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->o〇00O:Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;

    .line 38
    .line 39
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;->〇〇8O0〇8(Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V

    .line 40
    .line 41
    .line 42
    const-string p1, "scan_id_addmarket"

    .line 43
    .line 44
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oo8ooo8O:Lcom/intsig/view/ImageTextButton;

    .line 48
    .line 49
    const v0, 0x7f13053a

    .line 50
    .line 51
    .line 52
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->setTipText(I)V

    .line 53
    .line 54
    .line 55
    :goto_0
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private o8O〇008()V
    .locals 2

    .line 1
    const-string v0, "CSCollagePreview"

    .line 2
    .line 3
    const-string v1, "add_security_watermark"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇0()Landroid/content/Context;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    new-instance v1, Lcom/intsig/camscanner/topic/TopicPreviewFragment$1;

    .line 13
    .line 14
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment$1;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V

    .line 15
    .line 16
    .line 17
    invoke-static {v0, v1}, Lcom/intsig/camscanner/securitymark/ModifySecurityMarkDialog;->〇O〇(Landroid/content/Context;Lcom/intsig/camscanner/securitymark/ModifySecurityMarkDialog$SecurityMarkChangeListener;)Lcom/intsig/camscanner/securitymark/ModifySecurityMarkDialog;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/ModifySecurityMarkDialog;->o800o8O()V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private o8o0o8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->Oo80:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->oO80()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->Oo08()Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o88oo〇O(Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V

    .line 21
    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8O〇008()V

    .line 25
    .line 26
    .line 27
    :goto_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private oO8()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/attention/〇oo〇;

    .line 6
    .line 7
    invoke-direct {v1}, Lcom/intsig/camscanner/attention/〇oo〇;-><init>()V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic oOO8oo0(Ljava/util/List;Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇8〇〇8o(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic oOoO8OO〇(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO0o(Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private o〇08oO80o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oO00〇o:Lcom/intsig/camscanner/tools/UndoTool;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/tools/UndoTool;->〇o00〇〇Oo()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic o〇0〇o(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Landroid/net/Uri;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇oOO80o(Landroid/net/Uri;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic o〇O8OO(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Lcom/intsig/camscanner/topic/model/TopicModel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇OoO0(Lcom/intsig/camscanner/topic/model/TopicModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private synthetic o〇OoO0(Lcom/intsig/camscanner/topic/model/TopicModel;)V
    .locals 2

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    :try_start_0
    iget v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oO〇8O8oOo:I

    .line 5
    .line 6
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇o〇OO80oO(Lcom/intsig/camscanner/topic/model/TopicModel;I)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->ooo0〇〇O:Landroidx/collection/ArrayMap;

    .line 10
    .line 11
    iget-object p1, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->〇080:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Landroidx/collection/SimpleArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    iget p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->Oo0〇Ooo:I

    .line 17
    .line 18
    const/4 v0, -0x1

    .line 19
    if-le p1, v0, :cond_1

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/datastruct/PageProperty;

    .line 22
    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇8〇oO〇〇8o:Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-virtual {v1, p1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :catch_0
    move-exception p1

    .line 32
    const-string v0, "TopicPreviewFragment"

    .line 33
    .line 34
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 35
    .line 36
    .line 37
    :cond_1
    :goto_0
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private o〇o08〇()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    const-string v2, "key_page_properties"

    .line 25
    .line 26
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    iput-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇8〇oO〇〇8o:Ljava/util/ArrayList;

    .line 31
    .line 32
    const-string v2, "extra_doc_info"

    .line 33
    .line 34
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    check-cast v2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 39
    .line 40
    iput-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 41
    .line 42
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O880O〇()V

    .line 43
    .line 44
    .line 45
    iget-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 46
    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 48
    .line 49
    invoke-interface {v2, v3}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->〇o00〇〇Oo(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;)V

    .line 50
    .line 51
    .line 52
    const-string v2, "key_topic_property_type"

    .line 53
    .line 54
    const/4 v3, 0x2

    .line 55
    invoke-virtual {v0, v2, v3}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;I)I

    .line 56
    .line 57
    .line 58
    move-result v2

    .line 59
    iput v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇OO〇00〇0O:I

    .line 60
    .line 61
    const-string v2, "key_topic_from_part"

    .line 62
    .line 63
    invoke-virtual {v0, v2, v1}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    iput-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->ooO:Ljava/lang/String;

    .line 68
    .line 69
    const-string v2, "KEY_TOPIC_FROM_COLLAGE_ENTRANCE"

    .line 70
    .line 71
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    instance-of v2, v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 76
    .line 77
    if-eqz v2, :cond_0

    .line 78
    .line 79
    check-cast v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 80
    .line 81
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8〇OO:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 82
    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇0()Landroid/content/Context;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 88
    .line 89
    .line 90
    move-result v0

    .line 91
    shr-int/lit8 v0, v0, 0x1

    .line 92
    .line 93
    iput v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->o0:I

    .line 94
    .line 95
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇8〇oO〇〇8o:Ljava/util/ArrayList;

    .line 96
    .line 97
    if-eqz v0, :cond_1

    .line 98
    .line 99
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 100
    .line 101
    .line 102
    move-result v0

    .line 103
    if-nez v0, :cond_2

    .line 104
    .line 105
    :cond_1
    const-string v0, "TopicPreviewFragment"

    .line 106
    .line 107
    const-string v2, "initPreData error!"

    .line 108
    .line 109
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇0OOo〇0(Landroid/net/Uri;)V

    .line 113
    .line 114
    .line 115
    :cond_2
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private o〇oo()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->〇08O〇00〇o:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->OO:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 8
    .line 9
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForLayoutPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/4 v2, 0x1

    .line 14
    iput-boolean v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇00O0:Z

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->o〇00O:Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;

    .line 17
    .line 18
    invoke-virtual {v2, v0, v1}, Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;->〇oo〇(ILandroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    .line 19
    .line 20
    .line 21
    return-void
.end method

.method public static synthetic 〇088O(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇08O(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇8o0o0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method private 〇0O8Oo(Z)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O8oOo80()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oO:Landroidx/recyclerview/widget/RecyclerView;

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    sget-object v1, Lcom/intsig/base/ToolbarThemeGet;->〇080:Lcom/intsig/base/ToolbarThemeGet;

    .line 23
    .line 24
    invoke-virtual {v1}, Lcom/intsig/base/ToolbarThemeGet;->〇o〇()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    const v2, 0x7f060527

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    if-eqz p1, :cond_1

    .line 44
    .line 45
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oO:Landroidx/recyclerview/widget/RecyclerView;

    .line 46
    .line 47
    const/16 v1, 0x8

    .line 48
    .line 49
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 50
    .line 51
    .line 52
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8o:Lcom/intsig/view/ImageTextButton;

    .line 53
    .line 54
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->setIconAndTextColor(I)V

    .line 55
    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oO:Landroidx/recyclerview/widget/RecyclerView;

    .line 59
    .line 60
    const/4 v0, 0x0

    .line 61
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 62
    .line 63
    .line 64
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8o:Lcom/intsig/view/ImageTextButton;

    .line 65
    .line 66
    invoke-virtual {p1, v1}, Lcom/intsig/view/ImageTextButton;->setIconAndTextColor(I)V

    .line 67
    .line 68
    .line 69
    :cond_2
    :goto_0
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private static synthetic 〇0o0oO〇〇0(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private synthetic 〇0o88Oo〇()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇0oO〇oo00(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇oO〇08o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic 〇0ooOOo(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0o88Oo〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic 〇0〇0(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o0OO(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇80〇()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->Ooo08:I

    .line 2
    .line 3
    if-ltz v0, :cond_0

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oO:Landroidx/recyclerview/widget/RecyclerView;

    .line 6
    .line 7
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollToPosition(I)V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇8O0880(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o88o88(Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private 〇8o0o0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14

    .line 1
    move-object v0, p0

    .line 2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 3
    .line 4
    .line 5
    move-result-object v1

    .line 6
    iget-object v2, v0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 7
    .line 8
    iget-object v2, v2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 9
    .line 10
    const v3, 0x7f130009

    .line 11
    .line 12
    .line 13
    const/4 v4, 0x1

    .line 14
    new-instance v7, Lcom/intsig/camscanner/topic/TopicPreviewFragment$2;

    .line 15
    .line 16
    move-object v5, p1

    .line 17
    move-object/from16 v6, p2

    .line 18
    .line 19
    move-object/from16 v8, p3

    .line 20
    .line 21
    invoke-direct {v7, p0, p1, v6, v8}, Lcom/intsig/camscanner/topic/TopicPreviewFragment$2;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    const-wide/16 v9, -0x1

    .line 25
    .line 26
    const/4 v11, 0x1

    .line 27
    iget-object v5, v0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 28
    .line 29
    iget-wide v12, v5, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 30
    .line 31
    move-object/from16 v5, p3

    .line 32
    .line 33
    move-object/from16 v6, p4

    .line 34
    .line 35
    move-wide v8, v9

    .line 36
    move v10, v11

    .line 37
    move-wide v11, v12

    .line 38
    invoke-static/range {v1 .. v12}, Lcom/intsig/camscanner/app/DialogUtils;->〇O〇80o08O(Landroid/app/Activity;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/app/DialogUtils$OnDocTitleEditListener;JZJ)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public static synthetic 〇8〇80o(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0o0oO〇〇0(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/topic/TopicPreviewFragment;[Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o0O0O〇〇〇0([Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇8〇〇8o(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    const-string v0, "TopicPreviewFragment"

    .line 2
    .line 3
    if-eqz p1, :cond_6

    .line 4
    .line 5
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-gtz v1, :cond_0

    .line 10
    .line 11
    goto/16 :goto_1

    .line 12
    .line 13
    :cond_0
    const/4 v1, 0x0

    .line 14
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    check-cast p1, Ljava/util/List;

    .line 19
    .line 20
    if-eqz p1, :cond_5

    .line 21
    .line 22
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    if-lez v2, :cond_5

    .line 27
    .line 28
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    if-nez v2, :cond_1

    .line 33
    .line 34
    goto/16 :goto_0

    .line 35
    .line 36
    :cond_1
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    check-cast p1, Lcom/intsig/camscanner/topic/model/TopicModel;

    .line 41
    .line 42
    iget-object v1, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->O8:Lcom/intsig/camscanner/util/ParcelSize;

    .line 43
    .line 44
    iget-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 45
    .line 46
    invoke-interface {v2}, Lcom/intsig/camscanner/topic/contract/TopicContract$IPageProperty;->〇o〇()I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    iget-object v3, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->oO80:Landroid/graphics/Rect;

    .line 51
    .line 52
    iget v3, v3, Landroid/graphics/Rect;->left:I

    .line 53
    .line 54
    add-int/2addr v2, v3

    .line 55
    iget-object v3, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 56
    .line 57
    invoke-interface {v3}, Lcom/intsig/camscanner/topic/contract/TopicContract$IPageProperty;->〇o〇()I

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 62
    .line 63
    .line 64
    move-result-object v4

    .line 65
    const v5, 0x7f070059

    .line 66
    .line 67
    .line 68
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 69
    .line 70
    .line 71
    move-result v4

    .line 72
    add-int/2addr v3, v4

    .line 73
    iget-object v4, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->oO80:Landroid/graphics/Rect;

    .line 74
    .line 75
    iget v4, v4, Landroid/graphics/Rect;->top:I

    .line 76
    .line 77
    add-int/2addr v3, v4

    .line 78
    new-instance v4, Ljava/lang/StringBuilder;

    .line 79
    .line 80
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 81
    .line 82
    .line 83
    const-string v5, "left: "

    .line 84
    .line 85
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    const-string v5, " top: "

    .line 92
    .line 93
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object v4

    .line 103
    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    if-eqz v1, :cond_7

    .line 107
    .line 108
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 109
    .line 110
    .line 111
    move-result v4

    .line 112
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 113
    .line 114
    .line 115
    move-result v1

    .line 116
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 117
    .line 118
    .line 119
    move-result-object v5

    .line 120
    invoke-static {v5}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 121
    .line 122
    .line 123
    move-result v5

    .line 124
    div-int/lit8 v5, v5, 0x2

    .line 125
    .line 126
    if-lez v5, :cond_2

    .line 127
    .line 128
    if-le v1, v5, :cond_2

    .line 129
    .line 130
    move v1, v5

    .line 131
    :cond_2
    new-instance v5, Landroid/graphics/Rect;

    .line 132
    .line 133
    add-int/2addr v4, v2

    .line 134
    add-int/2addr v1, v3

    .line 135
    invoke-direct {v5, v2, v3, v4, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 136
    .line 137
    .line 138
    iget p1, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->〇〇888:F

    .line 139
    .line 140
    const/high16 v1, 0x42b40000    # 90.0f

    .line 141
    .line 142
    cmpl-float p1, p1, v1

    .line 143
    .line 144
    if-nez p1, :cond_3

    .line 145
    .line 146
    new-instance p1, Landroid/graphics/RectF;

    .line 147
    .line 148
    invoke-direct {p1, v5}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 149
    .line 150
    .line 151
    new-instance v1, Landroid/graphics/RectF;

    .line 152
    .line 153
    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 154
    .line 155
    .line 156
    new-instance v2, Landroid/graphics/Matrix;

    .line 157
    .line 158
    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 159
    .line 160
    .line 161
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    .line 162
    .line 163
    .line 164
    move-result v3

    .line 165
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerY()F

    .line 166
    .line 167
    .line 168
    move-result v4

    .line 169
    const/high16 v5, -0x3d4c0000    # -90.0f

    .line 170
    .line 171
    invoke-virtual {v2, v5, v3, v4}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 172
    .line 173
    .line 174
    invoke-virtual {v2, v1, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 175
    .line 176
    .line 177
    new-instance v5, Landroid/graphics/Rect;

    .line 178
    .line 179
    iget p1, v1, Landroid/graphics/RectF;->left:F

    .line 180
    .line 181
    float-to-int p1, p1

    .line 182
    iget v2, v1, Landroid/graphics/RectF;->top:F

    .line 183
    .line 184
    float-to-int v2, v2

    .line 185
    iget v3, v1, Landroid/graphics/RectF;->right:F

    .line 186
    .line 187
    float-to-int v3, v3

    .line 188
    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    .line 189
    .line 190
    float-to-int v1, v1

    .line 191
    invoke-direct {v5, p1, v2, v3, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 192
    .line 193
    .line 194
    :cond_3
    iget p1, v5, Landroid/graphics/Rect;->bottom:I

    .line 195
    .line 196
    new-instance v1, Lcom/intsig/camscanner/topic/dialog/JigsawEditViewGuideFragment;

    .line 197
    .line 198
    invoke-direct {v1}, Lcom/intsig/camscanner/topic/dialog/JigsawEditViewGuideFragment;-><init>()V

    .line 199
    .line 200
    .line 201
    invoke-static {v1, v5, p1}, Lcom/intsig/camscanner/topic/dialog/JigsawBaseDialogFragment;->oOo〇08〇(Lcom/intsig/camscanner/topic/dialog/JigsawBaseDialogFragment;Landroid/graphics/Rect;I)Lcom/intsig/camscanner/topic/dialog/JigsawBaseDialogFragment;

    .line 202
    .line 203
    .line 204
    move-result-object p1

    .line 205
    check-cast p1, Lcom/intsig/camscanner/topic/dialog/JigsawEditViewGuideFragment;

    .line 206
    .line 207
    new-instance v1, L〇0o88O/〇o〇;

    .line 208
    .line 209
    invoke-direct {v1, p0}, L〇0o88O/〇o〇;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V

    .line 210
    .line 211
    .line 212
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/topic/dialog/JigsawBaseDialogFragment;->〇O8oOo0(Landroid/content/DialogInterface$OnDismissListener;)Lcom/intsig/camscanner/topic/dialog/JigsawBaseDialogFragment;

    .line 213
    .line 214
    .line 215
    move-result-object p1

    .line 216
    check-cast p1, Lcom/intsig/camscanner/topic/dialog/JigsawEditViewGuideFragment;

    .line 217
    .line 218
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O8oOo80()Z

    .line 219
    .line 220
    .line 221
    move-result v1

    .line 222
    if-eqz v1, :cond_4

    .line 223
    .line 224
    const v1, 0x7f131d14

    .line 225
    .line 226
    .line 227
    const v2, 0x7f13003b

    .line 228
    .line 229
    .line 230
    invoke-virtual {p1, v1, v2}, Lcom/intsig/camscanner/topic/dialog/JigsawEditViewGuideFragment;->oooO888(II)Lcom/intsig/camscanner/topic/dialog/JigsawEditViewGuideFragment;

    .line 231
    .line 232
    .line 233
    :cond_4
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 234
    .line 235
    .line 236
    move-result-object v1

    .line 237
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/topic/dialog/JigsawBaseDialogFragment;->oO〇oo(Landroidx/fragment/app/FragmentManager;)V

    .line 238
    .line 239
    .line 240
    goto :goto_3

    .line 241
    :cond_5
    :goto_0
    const-string p1, "showEditViewGuide topicModels is null!"

    .line 242
    .line 243
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    .line 245
    .line 246
    return-void

    .line 247
    :catch_0
    move-exception p1

    .line 248
    goto :goto_2

    .line 249
    :cond_6
    :goto_1
    const-string p1, "showEditViewGuide topicLists is null!"

    .line 250
    .line 251
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    .line 253
    .line 254
    return-void

    .line 255
    :goto_2
    const-string v1, "showEditViewGuide"

    .line 256
    .line 257
    invoke-static {v0, v1, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 258
    .line 259
    .line 260
    :cond_7
    :goto_3
    return-void
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method static bridge synthetic 〇O0o〇〇o(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o88oo〇O(Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇O8〇8000(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)I
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇080〇o0()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇O8〇8O0oO(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo0:Lcom/intsig/camscanner/topic/view/FloatActionView;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/16 p1, 0x8

    .line 8
    .line 9
    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private synthetic 〇OoO0o0(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇0OOo〇0(Landroid/net/Uri;)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇Oo〇O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->oO80()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->o〇00O:Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;->getItemCount()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-le v0, v1, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->o〇00O:Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;->〇O〇()V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->o〇00O:Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;

    .line 21
    .line 22
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇〇O〇()V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->oooO888()V

    .line 29
    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->oO〇oo()V

    .line 32
    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic 〇o08(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇oO88o(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/topic/model/JigsawTemplate;",
            ">;)I"
        }
    .end annotation

    .line 1
    const-string v0, "exception data occurs"

    .line 2
    .line 3
    const-string v1, "TopicPreviewFragment"

    .line 4
    .line 5
    if-eqz p1, :cond_8

    .line 6
    .line 7
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    if-gtz v2, :cond_0

    .line 12
    .line 13
    goto :goto_3

    .line 14
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 15
    .line 16
    invoke-interface {p1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    iget-object v3, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 21
    .line 22
    if-nez v3, :cond_1

    .line 23
    .line 24
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return v2

    .line 28
    :cond_1
    iget v0, v3, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo0:I

    .line 29
    .line 30
    const/4 v1, 0x2

    .line 31
    if-ne v0, v1, :cond_2

    .line 32
    .line 33
    sget-object v0, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->ID_CARD:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 34
    .line 35
    invoke-interface {p1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    goto :goto_2

    .line 40
    :cond_2
    const/4 v1, 0x4

    .line 41
    if-eq v0, v1, :cond_6

    .line 42
    .line 43
    const/16 v1, 0x71

    .line 44
    .line 45
    if-ne v0, v1, :cond_3

    .line 46
    .line 47
    goto :goto_1

    .line 48
    :cond_3
    const/16 v1, 0x8

    .line 49
    .line 50
    if-eq v0, v1, :cond_5

    .line 51
    .line 52
    const/16 v1, 0x72

    .line 53
    .line 54
    if-ne v0, v1, :cond_4

    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_4
    const/16 v1, 0xd

    .line 58
    .line 59
    if-ne v0, v1, :cond_7

    .line 60
    .line 61
    sget-object v0, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->BANK_CARD:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 62
    .line 63
    invoke-interface {p1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    goto :goto_2

    .line 68
    :cond_5
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->CN_TRAVEL:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 69
    .line 70
    invoke-interface {p1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 71
    .line 72
    .line 73
    move-result v2

    .line 74
    goto :goto_2

    .line 75
    :cond_6
    :goto_1
    sget-object v0, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->CN_DRIVER:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 76
    .line 77
    invoke-interface {p1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 78
    .line 79
    .line 80
    move-result v2

    .line 81
    :cond_7
    :goto_2
    return v2

    .line 82
    :cond_8
    :goto_3
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    const/4 p1, 0x0

    .line 86
    return p1
    .line 87
.end method

.method private synthetic 〇oOO80o(Landroid/net/Uri;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iget-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 15
    .line 16
    iget-object v2, v2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo〇8o008:Ljava/util/List;

    .line 17
    .line 18
    invoke-interface {v1, v0, v2, p1}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->〇O8o08O(Landroid/content/Context;Ljava/util/List;Landroid/net/Uri;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
.end method

.method private 〇oO〇08o()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, L〇0o88O/〇O8o08O;

    .line 6
    .line 7
    invoke-direct {v1, p0}, L〇0o88O/〇O8o08O;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇ooO8Ooo〇()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/topic/TopicPreviewFragment$8;->〇080:[I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 4
    .line 5
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    aget v0, v0, v1

    .line 10
    .line 11
    packed-switch v0, :pswitch_data_0

    .line 12
    .line 13
    .line 14
    :pswitch_0
    const/4 v0, 0x0

    .line 15
    return v0

    .line 16
    :pswitch_1
    const/4 v0, 0x1

    .line 17
    return v0

    .line 18
    nop

    .line 19
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
    .line 20
    .line 21
.end method

.method private 〇ooO〇000(Lcom/intsig/camscanner/topic/model/JigsawTemplate;)Z
    .locals 1
    .param p1    # Lcom/intsig/camscanner/topic/model/JigsawTemplate;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/intsig/camscanner/topic/TopicPreviewFragment$8;->〇080:[I

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    aget p1, v0, p1

    .line 8
    .line 9
    packed-switch p1, :pswitch_data_0

    .line 10
    .line 11
    .line 12
    const/4 p1, 0x0

    .line 13
    return p1

    .line 14
    :pswitch_0
    const/4 p1, 0x1

    .line 15
    return p1

    .line 16
    nop

    .line 17
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇o〇88(Ljava/util/List;)V
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [I

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oO:Landroidx/recyclerview/widget/RecyclerView;

    .line 5
    .line 6
    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 7
    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    aget v0, v0, v1

    .line 11
    .line 12
    invoke-static {}, Lcom/intsig/camscanner/util/StatusBarHelper;->〇o00〇〇Oo()Lcom/intsig/camscanner/util/StatusBarHelper;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/StatusBarHelper;->〇o〇()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    sub-int/2addr v0, v1

    .line 21
    new-instance v1, Landroid/graphics/Rect;

    .line 22
    .line 23
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    iget-object v3, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oO:Landroidx/recyclerview/widget/RecyclerView;

    .line 32
    .line 33
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    .line 34
    .line 35
    .line 36
    move-result v3

    .line 37
    add-int/2addr v3, v0

    .line 38
    const/4 v4, 0x0

    .line 39
    invoke-direct {v1, v4, v0, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 40
    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oO:Landroidx/recyclerview/widget/RecyclerView;

    .line 43
    .line 44
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    const/16 v3, 0x39

    .line 53
    .line 54
    invoke-static {v2, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    add-int/2addr v0, v2

    .line 59
    new-instance v2, Lcom/intsig/camscanner/topic/dialog/JigsawTemplateGuideFragment;

    .line 60
    .line 61
    invoke-direct {v2}, Lcom/intsig/camscanner/topic/dialog/JigsawTemplateGuideFragment;-><init>()V

    .line 62
    .line 63
    .line 64
    invoke-static {v2, v1, v0}, Lcom/intsig/camscanner/topic/dialog/JigsawBaseDialogFragment;->oOo〇08〇(Lcom/intsig/camscanner/topic/dialog/JigsawBaseDialogFragment;Landroid/graphics/Rect;I)Lcom/intsig/camscanner/topic/dialog/JigsawBaseDialogFragment;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    check-cast v0, Lcom/intsig/camscanner/topic/dialog/JigsawTemplateGuideFragment;

    .line 69
    .line 70
    new-instance v1, L〇0o88O/Oo08;

    .line 71
    .line 72
    invoke-direct {v1, p0, p1}, L〇0o88O/Oo08;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Ljava/util/List;)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/topic/dialog/JigsawBaseDialogFragment;->〇O8oOo0(Landroid/content/DialogInterface$OnDismissListener;)Lcom/intsig/camscanner/topic/dialog/JigsawBaseDialogFragment;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/topic/dialog/JigsawBaseDialogFragment;->oO〇oo(Landroidx/fragment/app/FragmentManager;)V

    .line 84
    .line 85
    .line 86
    return-void
    .line 87
.end method

.method public static synthetic 〇o〇88〇8(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇〇o8O(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private 〇o〇OO80oO(Lcom/intsig/camscanner/topic/model/TopicModel;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->o〇00O:Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p2, p1}, Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;->o〇O8〇〇o(ILcom/intsig/camscanner/topic/model/TopicModel;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇〇(Z)V
    .locals 7

    .line 1
    const-string v0, "idcard"

    .line 2
    .line 3
    const-string v1, "CamScanner_CertMode"

    .line 4
    .line 5
    const-string v2, "TopicPreviewFragment"

    .line 6
    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    const-string p1, "showBuyCloudStorageByPointsDialog"

    .line 10
    .line 11
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    new-instance p1, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;

    .line 15
    .line 16
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    invoke-direct {p1, v2}, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 21
    .line 22
    .line 23
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->oOO0880O(Ljava/lang/String;)I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;->Oo08(I)Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;->〇〇888(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    new-instance v0, Lcom/intsig/camscanner/topic/TopicPreviewFragment$3;

    .line 36
    .line 37
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment$3;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;->oO80(Lcom/intsig/camscanner/purchase/UsePointsDialog$UseCallback;)Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-virtual {p1}, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;->〇80〇808〇O()V

    .line 45
    .line 46
    .line 47
    goto/16 :goto_0

    .line 48
    .line 49
    :cond_0
    new-instance p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 50
    .line 51
    invoke-direct {p1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 52
    .line 53
    .line 54
    sget-object v3, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_COMPOSITE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 55
    .line 56
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 57
    .line 58
    .line 59
    iget-object v3, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8〇OO:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 60
    .line 61
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 62
    .line 63
    .line 64
    sget-object v3, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 65
    .line 66
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 67
    .line 68
    .line 69
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    const/4 v4, 0x1

    .line 74
    const/16 v5, 0x69

    .line 75
    .line 76
    if-nez v3, :cond_1

    .line 77
    .line 78
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇8o8O〇O()Z

    .line 79
    .line 80
    .line 81
    move-result v3

    .line 82
    if-nez v3, :cond_1

    .line 83
    .line 84
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇o〇88()Z

    .line 85
    .line 86
    .line 87
    move-result v3

    .line 88
    if-eqz v3, :cond_2

    .line 89
    .line 90
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇0()Landroid/content/Context;

    .line 91
    .line 92
    .line 93
    move-result-object v3

    .line 94
    invoke-static {v3}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 95
    .line 96
    .line 97
    move-result v3

    .line 98
    if-nez v3, :cond_4

    .line 99
    .line 100
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇oo()Z

    .line 101
    .line 102
    .line 103
    move-result v0

    .line 104
    if-eqz v0, :cond_3

    .line 105
    .line 106
    iget-boolean v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o0OoOOo0:Z

    .line 107
    .line 108
    if-nez v0, :cond_5

    .line 109
    .line 110
    iput-boolean v4, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o0OoOOo0:Z

    .line 111
    .line 112
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oO〇O0O()V

    .line 113
    .line 114
    .line 115
    new-instance v0, Lcom/intsig/camscanner/topic/TopicPreviewFragment$4;

    .line 116
    .line 117
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment$4;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 118
    .line 119
    .line 120
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->o〇0()V

    .line 121
    .line 122
    .line 123
    goto :goto_0

    .line 124
    :cond_3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 125
    .line 126
    .line 127
    move-result-object v0

    .line 128
    const-string v1, "ID_Mode_Scan"

    .line 129
    .line 130
    invoke-static {v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseExtraData;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/camscanner/tsapp/purchase/PurchaseExtraData;

    .line 131
    .line 132
    .line 133
    move-result-object v1

    .line 134
    invoke-static {v0, p1, v5, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇〇808〇(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;ILcom/intsig/camscanner/tsapp/purchase/PurchaseExtraData;)V

    .line 135
    .line 136
    .line 137
    goto :goto_0

    .line 138
    :cond_4
    new-instance v3, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;

    .line 139
    .line 140
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 141
    .line 142
    .line 143
    move-result-object v6

    .line 144
    invoke-direct {v3, v6}, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;-><init>(Landroid/app/Activity;)V

    .line 145
    .line 146
    .line 147
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->oOO0880O(Ljava/lang/String;)I

    .line 148
    .line 149
    .line 150
    move-result v1

    .line 151
    invoke-virtual {v3, v1}, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->oO80(I)Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;

    .line 152
    .line 153
    .line 154
    move-result-object v1

    .line 155
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->OO0o〇〇〇〇0(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;

    .line 156
    .line 157
    .line 158
    move-result-object v0

    .line 159
    invoke-virtual {v0, v5}, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->OO0o〇〇(I)Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;

    .line 160
    .line 161
    .line 162
    move-result-object v0

    .line 163
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->Oooo8o0〇(I)Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;

    .line 164
    .line 165
    .line 166
    move-result-object v0

    .line 167
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->〇O8o08O(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;

    .line 168
    .line 169
    .line 170
    move-result-object p1

    .line 171
    new-instance v0, Lcom/intsig/camscanner/topic/TopicPreviewFragment$5;

    .line 172
    .line 173
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment$5;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V

    .line 174
    .line 175
    .line 176
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->〇8o8o〇(Lcom/intsig/camscanner/purchase/PurchasePointsDialog$PurchaseCallback;)Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;

    .line 177
    .line 178
    .line 179
    move-result-object p1

    .line 180
    invoke-virtual {p1}, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->〇〇808〇()Lcom/intsig/camscanner/purchase/PurchasePointsDialog;

    .line 181
    .line 182
    .line 183
    const-string p1, "show showBuyPointsDialog"

    .line 184
    .line 185
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    .line 187
    .line 188
    :cond_5
    :goto_0
    return-void
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method static bridge synthetic 〇〇O80〇0o(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o88(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public static synthetic 〇〇o0〇8(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Ljava/util/List;Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOO8oo0(Ljava/util/List;Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method static bridge synthetic 〇〇〇0(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o0OoOOo0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇〇〇00(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇OO〇00〇0O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇〇〇OOO〇〇(Ljava/lang/String;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o88o88(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇〇〇O〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->oO80()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->o〇00O:Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;->getItemCount()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-ne v0, v1, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8〇OO0〇0o:Lcom/intsig/view/ImageTextButton;

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/view/ImageTextButton;->setEnableTouch(Z)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const v1, 0x7f06007e

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8〇OO0〇0o:Lcom/intsig/view/ImageTextButton;

    .line 33
    .line 34
    invoke-virtual {v1, v0}, Lcom/intsig/view/ImageTextButton;->setIconAndTextColor(I)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8〇OO0〇0o:Lcom/intsig/view/ImageTextButton;

    .line 39
    .line 40
    const/4 v1, 0x1

    .line 41
    invoke-virtual {v0, v1}, Lcom/intsig/view/ImageTextButton;->setEnableTouch(Z)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    sget-object v1, Lcom/intsig/base/ToolbarThemeGet;->〇080:Lcom/intsig/base/ToolbarThemeGet;

    .line 49
    .line 50
    invoke-virtual {v1}, Lcom/intsig/base/ToolbarThemeGet;->〇o〇()I

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8〇OO0〇0o:Lcom/intsig/view/ImageTextButton;

    .line 59
    .line 60
    invoke-virtual {v1, v0}, Lcom/intsig/view/ImageTextButton;->setIconAndTextColor(I)V

    .line 61
    .line 62
    .line 63
    :goto_0
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method


# virtual methods
.method public O08〇()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O8o08O8O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo0:Lcom/intsig/camscanner/topic/view/FloatActionView;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/view/FloatActionView;->〇〇808〇()V

    .line 10
    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 13
    .line 14
    const-string v1, "TopicPreviewFragment"

    .line 15
    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    const-string v0, "mParcelDocInfo == null"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    return-void

    .line 24
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 25
    .line 26
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 27
    .line 28
    .line 29
    const-string v2, "mPropertyType="

    .line 30
    .line 31
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    iget v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇OO〇00〇0O:I

    .line 35
    .line 36
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    iget v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇OO〇00〇0O:I

    .line 47
    .line 48
    const/4 v1, 0x1

    .line 49
    if-ne v0, v1, :cond_2

    .line 50
    .line 51
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO0O()V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_2
    const/4 v1, 0x2

    .line 56
    if-ne v0, v1, :cond_3

    .line 57
    .line 58
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o808o8o08()V

    .line 59
    .line 60
    .line 61
    :cond_3
    :goto_0
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public O8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x1

    .line 10
    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/AppUtil;->o〇〇0〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 15
    .line 16
    const v1, 0x7f130255

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 27
    .line 28
    const/4 v1, 0x0

    .line 29
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 33
    .line 34
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseProgressDialog;->oO(I)V

    .line 35
    .line 36
    .line 37
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 38
    .line 39
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-nez v0, :cond_1

    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 48
    .line 49
    .line 50
    :cond_1
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public O8O〇8oo08()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8o08O8O()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo0:Lcom/intsig/camscanner/topic/view/FloatActionView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8oOo80()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇OO〇00〇0O:I

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8〇o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->OO:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;->Oo08()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO88〇OOO()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const-string v0, "TopicPreviewFragment"

    .line 8
    .line 9
    const-string v1, "activity == null"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    iget v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇OO〇00〇0O:I

    .line 16
    .line 17
    const/4 v2, 0x1

    .line 18
    if-ne v1, v2, :cond_1

    .line 19
    .line 20
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_TOPIC:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 23
    .line 24
    iget-object v3, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8〇OO:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 25
    .line 26
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 27
    .line 28
    .line 29
    invoke-static {v0, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇0〇O0088o(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    const/4 v2, 0x2

    .line 34
    if-ne v1, v2, :cond_2

    .line 35
    .line 36
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 37
    .line 38
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_COMPOSITE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 39
    .line 40
    iget-object v3, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8〇OO:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 41
    .line 42
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 43
    .line 44
    .line 45
    sget-object v2, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 46
    .line 47
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    const/16 v2, 0x6c

    .line 52
    .line 53
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->Oooo8o0〇(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 54
    .line 55
    .line 56
    :cond_2
    :goto_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public OOo8o〇O(F)V
    .locals 0

    .line 1
    const/4 p1, 0x1

    .line 2
    iput-boolean p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇o0O:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public Oo08(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O〇O00O:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O〇O00O:Lcom/intsig/app/BaseProgressDialog;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O〇O00O:Lcom/intsig/app/BaseProgressDialog;

    .line 18
    .line 19
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O〇O00O:Lcom/intsig/app/BaseProgressDialog;

    .line 20
    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const/4 v1, 0x1

    .line 28
    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/AppUtil;->o〇〇0〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O〇O00O:Lcom/intsig/app/BaseProgressDialog;

    .line 33
    .line 34
    const/4 v1, 0x0

    .line 35
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 36
    .line 37
    .line 38
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O〇O00O:Lcom/intsig/app/BaseProgressDialog;

    .line 39
    .line 40
    const v1, 0x7f131ec6

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 48
    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O〇O00O:Lcom/intsig/app/BaseProgressDialog;

    .line 51
    .line 52
    invoke-virtual {v0, p1}, Lcom/intsig/app/BaseProgressDialog;->o〇0OOo〇0(I)V

    .line 53
    .line 54
    .line 55
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O〇O00O:Lcom/intsig/app/BaseProgressDialog;

    .line 56
    .line 57
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :catch_0
    move-exception p1

    .line 62
    const-string v0, "TopicPreviewFragment"

    .line 63
    .line 64
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    .line 66
    .line 67
    :goto_0
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public Oo0oO〇O〇O()Landroidx/fragment/app/Fragment;
    .locals 0

    .line 1
    return-object p0
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oooo8o0〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->OO:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;->o〇0()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O〇0O〇Oo〇o()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇o〇Oo88:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    :try_start_0
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇o〇Oo88:Lcom/intsig/app/BaseProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :catch_0
    move-exception v0

    .line 13
    const-string v1, "TopicPreviewFragment"

    .line 14
    .line 15
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 16
    .line 17
    .line 18
    :cond_0
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public O〇0〇o808〇()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    const v1, 0x7f131d10

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const v1, 0x7f1301ab

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const v1, 0x7f131e36

    .line 25
    .line 26
    .line 27
    const/4 v2, 0x0

    .line 28
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public O〇Oooo〇〇(ILcom/intsig/camscanner/topic/model/JigsawTemplate;)V
    .locals 2
    .param p2    # Lcom/intsig/camscanner/topic/model/JigsawTemplate;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O8o08O8O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo0:Lcom/intsig/camscanner/topic/view/FloatActionView;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/view/FloatActionView;->〇〇808〇()V

    .line 10
    .line 11
    .line 12
    :cond_0
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇ooO〇000(Lcom/intsig/camscanner/topic/model/JigsawTemplate;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇8〇008(Z)V

    .line 17
    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oO:Landroidx/recyclerview/widget/RecyclerView;

    .line 20
    .line 21
    invoke-virtual {v1, p1}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollToPosition(I)V

    .line 22
    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 25
    .line 26
    if-ne p1, p2, :cond_1

    .line 27
    .line 28
    return-void

    .line 29
    :cond_1
    iput-object p2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 32
    .line 33
    if-eqz v0, :cond_2

    .line 34
    .line 35
    sget-object v0, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->A4:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O〇〇O8:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 39
    .line 40
    :goto_0
    invoke-interface {p1, p2, v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->〇8o8o〇(Lcom/intsig/camscanner/topic/model/JigsawTemplate;Lcom/intsig/camscanner/topic/model/PageSizeEnumType;)V

    .line 41
    .line 42
    .line 43
    const/4 p1, 0x0

    .line 44
    iput-boolean p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇o0O:Z

    .line 45
    .line 46
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇08oO80o()V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public o0〇〇00()V
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇OO〇00〇0O:I

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    const/4 v2, 0x1

    .line 5
    const/4 v3, 0x0

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O〇〇O8:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8oOOo:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 11
    .line 12
    if-ne v0, v1, :cond_2

    .line 13
    .line 14
    iget-boolean v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇o0O:Z

    .line 15
    .line 16
    if-nez v0, :cond_2

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->Oo80:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 19
    .line 20
    if-eqz v0, :cond_2

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->oO80()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eqz v0, :cond_2

    .line 31
    .line 32
    sget-object v0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOoo80oO:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o88o08〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 35
    .line 36
    if-eq v0, v1, :cond_1

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    if-ne v0, v2, :cond_1

    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O〇〇O8:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8oOOo:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 44
    .line 45
    if-ne v0, v1, :cond_2

    .line 46
    .line 47
    iget-boolean v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇o0O:Z

    .line 48
    .line 49
    if-eqz v0, :cond_1

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_1
    const/4 v2, 0x0

    .line 53
    :cond_2
    :goto_0
    const/4 v0, 0x0

    .line 54
    if-eqz v2, :cond_3

    .line 55
    .line 56
    new-instance v1, Lcom/intsig/app/AlertDialog$Builder;

    .line 57
    .line 58
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 59
    .line 60
    .line 61
    move-result-object v2

    .line 62
    invoke-direct {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 63
    .line 64
    .line 65
    const v2, 0x7f131d10

    .line 66
    .line 67
    .line 68
    invoke-virtual {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    const v2, 0x7f130403

    .line 73
    .line 74
    .line 75
    invoke-virtual {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    invoke-virtual {v1, v3}, Lcom/intsig/app/AlertDialog$Builder;->o〇0(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    const v2, 0x7f13057e

    .line 84
    .line 85
    .line 86
    invoke-virtual {v1, v2, v0}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    new-instance v1, L〇0o88O/〇080;

    .line 91
    .line 92
    invoke-direct {v1, p0}, L〇0o88O/〇080;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V

    .line 93
    .line 94
    .line 95
    const v2, 0x7f13053b

    .line 96
    .line 97
    .line 98
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 107
    .line 108
    .line 109
    goto :goto_1

    .line 110
    :cond_3
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇0OOo〇0(Landroid/net/Uri;)V

    .line 111
    .line 112
    .line 113
    :goto_1
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public o80ooO(Ljava/util/List;)V
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    const-string v0, "refreshData"

    .line 2
    .line 3
    const-string v1, "TopicPreviewFragment"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->o〇00O:Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    new-instance v0, Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;

    .line 13
    .line 14
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    iget-object v3, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 19
    .line 20
    invoke-direct {v0, v2, v3}, Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/topic/contract/TopicContract$IPageProperty;)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->o〇00O:Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;

    .line 24
    .line 25
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;->O〇8O8〇008(Lcom/intsig/camscanner/topic/contract/ITopicAdapter;)V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->OO:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->o〇00O:Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;

    .line 31
    .line 32
    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 33
    .line 34
    .line 35
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->o〇00O:Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;

    .line 36
    .line 37
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO〇〇o0oO()Z

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    invoke-virtual {v0, p1, v2}, Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;->O8ooOoo〇(Ljava/util/List;Z)V

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->OO:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 45
    .line 46
    const/4 v2, 0x0

    .line 47
    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    .line 48
    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->o〇00O:Lcom/intsig/camscanner/topic/adapter/TopicPreviewAdapter;

    .line 51
    .line 52
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O8oOo80()Z

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    if-eqz v0, :cond_3

    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇08〇o0O:Lcom/intsig/camscanner/topic/adapter/JigsawTemplateAdapter;

    .line 62
    .line 63
    if-nez v0, :cond_3

    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 66
    .line 67
    invoke-interface {v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->〇〇888()Ljava/util/List;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 72
    .line 73
    .line 74
    move-result v3

    .line 75
    if-lez v3, :cond_3

    .line 76
    .line 77
    new-instance v3, Lcom/intsig/camscanner/topic/adapter/JigsawTemplateAdapter;

    .line 78
    .line 79
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 80
    .line 81
    .line 82
    move-result-object v4

    .line 83
    invoke-direct {v3, v4, v0}, Lcom/intsig/camscanner/topic/adapter/JigsawTemplateAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 84
    .line 85
    .line 86
    iput-object v3, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇08〇o0O:Lcom/intsig/camscanner/topic/adapter/JigsawTemplateAdapter;

    .line 87
    .line 88
    invoke-virtual {v3, p0}, Lcom/intsig/camscanner/topic/adapter/JigsawTemplateAdapter;->〇O888o0o(Lcom/intsig/camscanner/topic/adapter/JigsawTemplateAdapter$TemplateClickListener;)V

    .line 89
    .line 90
    .line 91
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇oO88o(Ljava/util/List;)I

    .line 92
    .line 93
    .line 94
    move-result v0

    .line 95
    iput v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->Ooo08:I

    .line 96
    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    .line 98
    .line 99
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    .line 101
    .line 102
    const-string v3, "refreshData() mDocType : "

    .line 103
    .line 104
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    iget-object v3, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 108
    .line 109
    if-nez v3, :cond_1

    .line 110
    .line 111
    goto :goto_0

    .line 112
    :cond_1
    iget v2, v3, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo0:I

    .line 113
    .line 114
    :goto_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    const-string v2, " rightIndex "

    .line 118
    .line 119
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    iget v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->Ooo08:I

    .line 123
    .line 124
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v0

    .line 131
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    iget v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->Ooo08:I

    .line 135
    .line 136
    if-ltz v0, :cond_2

    .line 137
    .line 138
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇08〇o0O:Lcom/intsig/camscanner/topic/adapter/JigsawTemplateAdapter;

    .line 139
    .line 140
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/topic/adapter/JigsawTemplateAdapter;->o800o8O(I)V

    .line 141
    .line 142
    .line 143
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇oO:Landroidx/recyclerview/widget/RecyclerView;

    .line 144
    .line 145
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇08〇o0O:Lcom/intsig/camscanner/topic/adapter/JigsawTemplateAdapter;

    .line 146
    .line 147
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 148
    .line 149
    .line 150
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇08〇o0O:Lcom/intsig/camscanner/topic/adapter/JigsawTemplateAdapter;

    .line 151
    .line 152
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 153
    .line 154
    .line 155
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇80〇()V

    .line 156
    .line 157
    .line 158
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇〇O〇()V

    .line 159
    .line 160
    .line 161
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OoO〇OOo8o(Ljava/util/List;)V

    .line 162
    .line 163
    .line 164
    return-void
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public oO〇O0O()V
    .locals 3

    .line 1
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/AppUtil;->o〇〇0〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇o〇Oo88:Lcom/intsig/app/BaseProgressDialog;

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇o〇Oo88:Lcom/intsig/app/BaseProgressDialog;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :catch_0
    move-exception v0

    .line 22
    const-string v1, "TopicPreviewFragment"

    .line 23
    .line 24
    const-string v2, "showLoadingDialog"

    .line 25
    .line 26
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 27
    .line 28
    .line 29
    :goto_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/lang/StringBuilder;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7
    .line 8
    .line 9
    const-string v1, "onActivityResult requestCode="

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v1, " resultCode="

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    const-string v1, "TopicPreviewFragment"

    .line 30
    .line 31
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const/4 v0, 0x1

    .line 35
    if-ne p1, v0, :cond_0

    .line 36
    .line 37
    const/4 p1, -0x1

    .line 38
    if-ne p2, p1, :cond_3

    .line 39
    .line 40
    invoke-static {p3}, Lcom/intsig/camscanner/topic/TopicInchSelectActivity;->O〇080〇o0(Landroid/content/Intent;)Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    iget-object p2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O〇〇O8:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 45
    .line 46
    if-eq p2, p1, :cond_3

    .line 47
    .line 48
    if-eqz p1, :cond_3

    .line 49
    .line 50
    iget-object p2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->ooo0〇〇O:Landroidx/collection/ArrayMap;

    .line 51
    .line 52
    invoke-virtual {p2}, Landroidx/collection/SimpleArrayMap;->clear()V

    .line 53
    .line 54
    .line 55
    const/4 p2, 0x0

    .line 56
    iput-boolean p2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇o0O:Z

    .line 57
    .line 58
    iput-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O〇〇O8:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 59
    .line 60
    iget-object p2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO〇00〇8oO:Lcom/intsig/view/ImageTextButton;

    .line 61
    .line 62
    invoke-virtual {p1}, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->getIconRes()I

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    invoke-virtual {p2, p1}, Lcom/intsig/view/ImageTextButton;->setTipIcon(I)V

    .line 67
    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_0
    const/16 p2, 0x69

    .line 71
    .line 72
    if-ne p1, p2, :cond_1

    .line 73
    .line 74
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oO8()V

    .line 75
    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_1
    const/16 p2, 0x6a

    .line 79
    .line 80
    if-ne p1, p2, :cond_2

    .line 81
    .line 82
    new-instance p1, Ljava/lang/StringBuilder;

    .line 83
    .line 84
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 85
    .line 86
    .line 87
    const-string p2, "is login="

    .line 88
    .line 89
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 93
    .line 94
    .line 95
    move-result-object p2

    .line 96
    invoke-static {p2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 97
    .line 98
    .line 99
    move-result p2

    .line 100
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object p1

    .line 107
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    goto :goto_0

    .line 111
    :cond_2
    const/16 p2, 0x6b

    .line 112
    .line 113
    if-ne p1, p2, :cond_3

    .line 114
    .line 115
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 116
    .line 117
    .line 118
    move-result-object p1

    .line 119
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇00O(Landroidx/fragment/app/FragmentActivity;)V

    .line 120
    .line 121
    .line 122
    :cond_3
    :goto_0
    return-void
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇08oO80o()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    const/4 v0, 0x1

    .line 9
    sparse-switch p1, :sswitch_data_0

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :sswitch_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O08〇()V

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :sswitch_1
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O8Oo(Z)V

    .line 18
    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8o0o8()V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :sswitch_2
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇o8()V

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :sswitch_3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O8Oo(Z)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO8〇O8()V

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :sswitch_4
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O8Oo(Z)V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇Oo〇O()V

    .line 39
    .line 40
    .line 41
    :goto_0
    return-void

    .line 42
    nop

    .line 43
    :sswitch_data_0
    .sparse-switch
        0x7f0a080e -> :sswitch_4
        0x7f0a080f -> :sswitch_3
        0x7f0a0811 -> :sswitch_2
        0x7f0a0812 -> :sswitch_1
        0x7f0a0fb3 -> :sswitch_0
    .end sparse-switch
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1    # Landroid/content/res/Configuration;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    invoke-static {p1}, Lcom/intsig/utils/DeviceUtil;->o〇O8〇〇o(Landroid/content/Context;)Z

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 15
    .line 16
    invoke-interface {p1}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->〇O〇()V

    .line 17
    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->ooo0〇〇O:Landroidx/collection/ArrayMap;

    .line 20
    .line 21
    invoke-virtual {p1}, Landroidx/collection/SimpleArrayMap;->clear()V

    .line 22
    .line 23
    .line 24
    const/4 p1, 0x0

    .line 25
    iput-boolean p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇o0O:Z

    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇8〇oO〇〇8o:Ljava/util/ArrayList;

    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O〇〇O8:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 32
    .line 33
    const/4 v2, 0x1

    .line 34
    invoke-interface {p1, v0, v1, v2}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->〇〇808〇(Ljava/util/List;Lcom/intsig/camscanner/topic/model/PageSizeEnumType;Z)V

    .line 35
    .line 36
    .line 37
    :cond_0
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public onDestroyView()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroyView()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O8O〇8oo08()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onResume()V
    .locals 4

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇8〇oO〇〇8o:Ljava/util/ArrayList;

    .line 7
    .line 8
    iget-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O〇〇O8:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 9
    .line 10
    const/4 v3, 0x0

    .line 11
    invoke-interface {v0, v1, v2, v3}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->〇〇808〇(Ljava/util/List;Lcom/intsig/camscanner/topic/model/PageSizeEnumType;Z)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onStart()V
    .locals 5

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "from_part"

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->ooO:Ljava/lang/String;

    .line 7
    .line 8
    const-string v2, "CSCollagePreview"

    .line 9
    .line 10
    const-string v3, "from"

    .line 11
    .line 12
    const-string v4, "qbook_mode"

    .line 13
    .line 14
    invoke-static {v2, v3, v4, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oo88o8O()V
    .locals 2

    .line 1
    const-string v0, "CSCollagePreview"

    .line 2
    .line 3
    const-string v1, "drag_image"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->OO:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;->〇〇888()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public ooO〇00O()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-nez v1, :cond_0

    .line 12
    .line 13
    new-instance v1, L〇0o88O/O8;

    .line 14
    .line 15
    invoke-direct {v1, p0}, L〇0o88O/O8;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    const-string v0, "TopicPreviewFragment"

    .line 23
    .line 24
    const-string v1, "activity == null || activity.isFinishing()"

    .line 25
    .line 26
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    new-instance v0, Ljava/lang/RuntimeException;

    .line 30
    .line 31
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    throw v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public ooo0〇O88O()V
    .locals 2

    .line 1
    const-string v0, "CSCollagePreview"

    .line 2
    .line 3
    const-string v1, "zoom"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oo〇(FF)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/topic/BaseJigSawPreviewFragment;->OO:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;->〇〇888()V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x1

    .line 7
    iput-boolean p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇o0O:Z

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public o〇0()Landroid/content/Context;
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    :cond_0
    if-nez v0, :cond_1

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    :cond_1
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0OOo〇0(Landroid/net/Uri;)V
    .locals 3

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    iget v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇OO〇00〇0O:I

    .line 4
    .line 5
    const/4 v1, 0x1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    const-string v0, "CSQuestionbook"

    .line 9
    .line 10
    const-string v1, "questionbook_success"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->o800o8O(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v1, 0x2

    .line 17
    if-ne v0, v1, :cond_1

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇08O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 20
    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    new-instance v1, L〇0o88O/〇〇888;

    .line 28
    .line 29
    invoke-direct {v1, p0, p1}, L〇0o88O/〇〇888;-><init>(Lcom/intsig/camscanner/topic/TopicPreviewFragment;Landroid/net/Uri;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 33
    .line 34
    .line 35
    :cond_1
    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    if-eqz v0, :cond_2

    .line 40
    .line 41
    new-instance v0, Landroid/content/Intent;

    .line 42
    .line 43
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 47
    .line 48
    .line 49
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    const/4 v2, -0x1

    .line 54
    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 55
    .line 56
    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    .line 58
    .line 59
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .line 61
    .line 62
    const-string v1, "docUri="

    .line 63
    .line 64
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    const-string v0, "TopicPreviewFragment"

    .line 75
    .line 76
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 84
    .line 85
    .line 86
    :cond_2
    return-void
    .line 87
.end method

.method public 〇0000OOO()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O8Oo(Z)V

    .line 3
    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo0:Lcom/intsig/camscanner/topic/view/FloatActionView;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/view/FloatActionView;->〇O〇()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8O0O808〇(Ljava/lang/String;Landroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;F)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Point;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/util/ParcelSize;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-boolean p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O88O:Z

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O0O:Lcom/intsig/camscanner/topic/model/TopicModel;

    .line 7
    .line 8
    iput-object p2, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->〇o〇:Landroid/graphics/Point;

    .line 9
    .line 10
    iput-object p3, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->O8:Lcom/intsig/camscanner/util/ParcelSize;

    .line 11
    .line 12
    iput p4, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->〇〇888:F

    .line 13
    .line 14
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O8〇o0〇〇8(Lcom/intsig/camscanner/topic/model/TopicModel;)V

    .line 15
    .line 16
    .line 17
    const/4 p1, 0x0

    .line 18
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O8〇8O0oO(Z)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public 〇8o8O〇O(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 12
    .line 13
    invoke-virtual {v0, p2}, Lcom/intsig/app/BaseProgressDialog;->oO(I)V

    .line 14
    .line 15
    .line 16
    iget-object p2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 17
    .line 18
    invoke-virtual {p2, p1}, Lcom/intsig/app/BaseProgressDialog;->o〇0OOo〇0(I)V

    .line 19
    .line 20
    .line 21
    :cond_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇8o8o〇(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O〇O00O:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/app/BaseProgressDialog;->oO(I)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected 〇O8oOo0(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    const v0, 0x7f0d0327

    .line 2
    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo〇8o008:Landroid/view/View;

    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o〇o08〇()V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OooO〇()V

    .line 15
    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo〇8o008:Landroid/view/View;

    .line 18
    .line 19
    return-object p1
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇o0O0O8(IILcom/intsig/camscanner/topic/model/TopicModel;Landroid/graphics/Point;)V
    .locals 8
    .param p3    # Lcom/intsig/camscanner/topic/model/TopicModel;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Point;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo0:Lcom/intsig/camscanner/topic/view/FloatActionView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/view/FloatActionView;->〇O〇()V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O88O:Z

    .line 8
    .line 9
    new-instance v1, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v2, "mFloatActionView page: "

    .line 15
    .line 16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string p1, " | topicIndex : "

    .line 23
    .line 24
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    const-string p2, "TopicPreviewFragment"

    .line 35
    .line 36
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    iput-object p3, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O0O:Lcom/intsig/camscanner/topic/model/TopicModel;

    .line 40
    .line 41
    iget p1, p4, Landroid/graphics/Point;->y:I

    .line 42
    .line 43
    iget-object p2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 44
    .line 45
    invoke-interface {p2}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->O8()I

    .line 46
    .line 47
    .line 48
    move-result p2

    .line 49
    sub-int/2addr p1, p2

    .line 50
    iput p1, p4, Landroid/graphics/Point;->y:I

    .line 51
    .line 52
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo0:Lcom/intsig/camscanner/topic/view/FloatActionView;

    .line 53
    .line 54
    iget-object v2, p3, Lcom/intsig/camscanner/topic/model/TopicModel;->〇080:Ljava/lang/String;

    .line 55
    .line 56
    iget-object v4, p3, Lcom/intsig/camscanner/topic/model/TopicModel;->O8:Lcom/intsig/camscanner/util/ParcelSize;

    .line 57
    .line 58
    iget v5, p3, Lcom/intsig/camscanner/topic/model/TopicModel;->〇〇888:F

    .line 59
    .line 60
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 61
    .line 62
    invoke-interface {p1}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->Oooo8o0〇()Z

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    const/4 p2, 0x1

    .line 67
    xor-int/lit8 v6, p1, 0x1

    .line 68
    .line 69
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 70
    .line 71
    invoke-interface {p1}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->oO80()I

    .line 72
    .line 73
    .line 74
    move-result p1

    .line 75
    if-lez p1, :cond_0

    .line 76
    .line 77
    const/4 v7, 0x1

    .line 78
    goto :goto_0

    .line 79
    :cond_0
    const/4 v7, 0x0

    .line 80
    :goto_0
    move-object v3, p4

    .line 81
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/camscanner/topic/view/FloatActionView;->OO0o〇〇(Ljava/lang/String;Landroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;FZZ)V

    .line 82
    .line 83
    .line 84
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O8〇8O0oO(Z)V

    .line 85
    .line 86
    .line 87
    iget-boolean p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇00O0:Z

    .line 88
    .line 89
    if-eqz p1, :cond_1

    .line 90
    .line 91
    iput-boolean v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇00O0:Z

    .line 92
    .line 93
    goto :goto_1

    .line 94
    :cond_1
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O8Oo(Z)V

    .line 95
    .line 96
    .line 97
    :goto_1
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public 〇oOo〇()V
    .locals 3

    .line 1
    const-string v0, "TopicPreviewFragment"

    .line 2
    .line 3
    const-string v1, "ReLoginTopic:login error, need relogin"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Landroid/content/Intent;

    .line 9
    .line 10
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    const-class v2, Lcom/intsig/camscanner/ReLoginDialogActivity;

    .line 15
    .line 16
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 17
    .line 18
    .line 19
    const-string v1, "is_pwd_wrong"

    .line 20
    .line 21
    const/4 v2, 0x1

    .line 22
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 23
    .line 24
    .line 25
    const/16 v1, 0x6a

    .line 26
    .line 27
    invoke-virtual {p0, v0, v1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇oo(Lcom/intsig/camscanner/topic/model/TopicModel;)V
    .locals 3
    .param p1    # Lcom/intsig/camscanner/topic/model/TopicModel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "CSCollagePreview"

    .line 2
    .line 3
    const-string v1, "delete"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O8〇8O0oO(Z)V

    .line 10
    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    iput-boolean v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇o0O:Z

    .line 14
    .line 15
    iput-boolean v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O88O:Z

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O0O:Lcom/intsig/camscanner/topic/model/TopicModel;

    .line 18
    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    iget-object v1, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->〇080:Ljava/lang/String;

    .line 22
    .line 23
    iget-object v0, v0, Lcom/intsig/camscanner/topic/model/TopicModel;->〇080:Ljava/lang/String;

    .line 24
    .line 25
    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_1

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->ooo0〇〇O:Landroidx/collection/ArrayMap;

    .line 32
    .line 33
    iget-object v1, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->〇080:Ljava/lang/String;

    .line 34
    .line 35
    iget-object v2, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O0O:Lcom/intsig/camscanner/topic/model/TopicModel;

    .line 36
    .line 37
    invoke-virtual {v0, v1, v2}, Landroidx/collection/SimpleArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O0O:Lcom/intsig/camscanner/topic/model/TopicModel;

    .line 41
    .line 42
    iget-object v0, v0, Lcom/intsig/camscanner/topic/model/TopicModel;->Oo08:Lcom/intsig/camscanner/util/ParcelSize;

    .line 43
    .line 44
    if-eqz v0, :cond_0

    .line 45
    .line 46
    new-instance v1, Lcom/intsig/camscanner/util/ParcelSize;

    .line 47
    .line 48
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    invoke-direct {v1, v2, v0}, Lcom/intsig/camscanner/util/ParcelSize;-><init>(II)V

    .line 57
    .line 58
    .line 59
    iput-object v1, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->Oo08:Lcom/intsig/camscanner/util/ParcelSize;

    .line 60
    .line 61
    :cond_0
    iget-object v0, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->〇080:Ljava/lang/String;

    .line 62
    .line 63
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->O〇0o8o8〇(Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 67
    .line 68
    invoke-interface {v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;->〇080()I

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O:Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;

    .line 73
    .line 74
    invoke-interface {v1}, Lcom/intsig/camscanner/topic/contract/TopicContract$IPageProperty;->o〇0()I

    .line 75
    .line 76
    .line 77
    move-result v1

    .line 78
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/topic/model/TopicModel;->〇o00〇〇Oo(II)V

    .line 79
    .line 80
    .line 81
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o0Oo(Lcom/intsig/camscanner/topic/model/TopicModel;)I

    .line 82
    .line 83
    .line 84
    move-result v0

    .line 85
    iput v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oO〇8O8oOo:I

    .line 86
    .line 87
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oO00〇o:Lcom/intsig/camscanner/tools/UndoTool;

    .line 88
    .line 89
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/tools/UndoTool;->Oo08(Ljava/lang/Object;)V

    .line 90
    .line 91
    .line 92
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oO00〇o:Lcom/intsig/camscanner/tools/UndoTool;

    .line 93
    .line 94
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOO0880O:Lcom/intsig/camscanner/tools/UndoTool$UndoToolCallback;

    .line 95
    .line 96
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/tools/UndoTool;->o〇0(Lcom/intsig/camscanner/tools/UndoTool$UndoToolCallback;)V

    .line 97
    .line 98
    .line 99
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oO00〇o:Lcom/intsig/camscanner/tools/UndoTool;

    .line 100
    .line 101
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOo〇8o008:Landroid/view/View;

    .line 102
    .line 103
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->oOO〇〇:Landroid/widget/LinearLayout;

    .line 104
    .line 105
    const/16 v2, 0xbb8

    .line 106
    .line 107
    invoke-virtual {p1, v0, v1, v2}, Lcom/intsig/camscanner/tools/UndoTool;->〇080(Landroid/view/View;Landroid/view/View;I)V

    .line 108
    .line 109
    .line 110
    :cond_1
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public 〇o〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇0O〇O00O:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    :try_start_0
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :catch_0
    move-exception v0

    .line 10
    const-string v1, "TopicPreviewFragment"

    .line 11
    .line 12
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇o()Lcom/intsig/camscanner/topic/model/PageSizeEnumType;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->o8oOOo:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 2
    .line 3
    iput-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O〇〇O8:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->OO〇00〇8oO:Lcom/intsig/view/ImageTextButton;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->getIconRes()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    invoke-virtual {v1, v0}, Lcom/intsig/view/ImageTextButton;->setImageResource(I)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicPreviewFragment;->〇O〇〇O8:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 15
    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

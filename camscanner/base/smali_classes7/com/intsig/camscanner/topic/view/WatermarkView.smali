.class public Lcom/intsig/camscanner/topic/view/WatermarkView;
.super Landroid/view/View;
.source "WatermarkView.java"


# instance fields
.field private o0:Lcom/intsig/camscanner/topic/model/DrawWaterMark;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/topic/view/WatermarkView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/camscanner/topic/view/WatermarkView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/view/WatermarkView;->〇080()V

    return-void
.end method

.method private 〇080()V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Landroid/view/View;->setClickable(Z)V

    .line 3
    .line 4
    .line 5
    invoke-virtual {p0, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/topic/model/DrawWaterMark;

    .line 12
    .line 13
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const/high16 v2, 0x3f800000    # 1.0f

    .line 18
    .line 19
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/topic/model/DrawWaterMark;-><init>(Landroid/content/Context;F)V

    .line 20
    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/camscanner/topic/view/WatermarkView;->o0:Lcom/intsig/camscanner/topic/model/DrawWaterMark;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/topic/view/WatermarkView;->o0:Lcom/intsig/camscanner/topic/model/DrawWaterMark;

    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/topic/model/DrawWaterMark;->Oo08(II)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/topic/view/WatermarkView;->o0:Lcom/intsig/camscanner/topic/model/DrawWaterMark;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/model/DrawWaterMark;->〇080()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_0

    .line 24
    .line 25
    return-void

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/topic/view/WatermarkView;->o0:Lcom/intsig/camscanner/topic/model/DrawWaterMark;

    .line 27
    .line 28
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/topic/model/DrawWaterMark;->〇o00〇〇Oo(Landroid/graphics/Canvas;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getWaterEntity()Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/view/WatermarkView;->o0:Lcom/intsig/camscanner/topic/model/DrawWaterMark;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/model/DrawWaterMark;->〇o〇()Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setWaterEntity(Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->oO80()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 12
    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 16
    .line 17
    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/topic/view/WatermarkView;->o0:Lcom/intsig/camscanner/topic/model/DrawWaterMark;

    .line 19
    .line 20
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/topic/model/DrawWaterMark;->o〇0(Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
.end method

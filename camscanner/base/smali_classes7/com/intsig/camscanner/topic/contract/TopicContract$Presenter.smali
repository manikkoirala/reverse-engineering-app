.class public interface abstract Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;
.super Ljava/lang/Object;
.source "TopicContract.java"

# interfaces
.implements Lcom/intsig/camscanner/topic/contract/TopicContract$IPageProperty;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<View:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/intsig/camscanner/topic/contract/TopicContract$IPageProperty;"
    }
.end annotation


# virtual methods
.method public abstract O8()I
.end method

.method public abstract OO0o〇〇(Ljava/lang/String;Ljava/lang/String;Landroidx/collection/ArrayMap;Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;ZLcom/intsig/camscanner/topic/model/JigsawTemplate;Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroidx/collection/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;",
            "Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;",
            "Z",
            "Lcom/intsig/camscanner/topic/model/JigsawTemplate;",
            "Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;",
            ")V"
        }
    .end annotation
.end method

.method public abstract Oo08()[Z
.end method

.method public abstract Oooo8o0〇()Z
.end method

.method public abstract oO80()I
.end method

.method public abstract 〇080()I
.end method

.method public abstract 〇80〇808〇O()Z
.end method

.method public abstract 〇8o8o〇(Lcom/intsig/camscanner/topic/model/JigsawTemplate;Lcom/intsig/camscanner/topic/model/PageSizeEnumType;)V
    .param p1    # Lcom/intsig/camscanner/topic/model/JigsawTemplate;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/topic/model/PageSizeEnumType;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract 〇O8o08O(Landroid/content/Context;Ljava/util/List;Landroid/net/Uri;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation
.end method

.method public abstract 〇O〇()V
.end method

.method public abstract 〇o00〇〇Oo(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;)V
.end method

.method public abstract 〇〇808〇(Ljava/util/List;Lcom/intsig/camscanner/topic/model/PageSizeEnumType;Z)V
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/topic/model/PageSizeEnumType;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;",
            "Lcom/intsig/camscanner/topic/model/PageSizeEnumType;",
            "Z)V"
        }
    .end annotation
.end method

.method public abstract 〇〇888()Ljava/util/List;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/topic/model/JigsawTemplate;",
            ">;"
        }
    .end annotation
.end method

.class Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;
.super Landroid/os/AsyncTask;
.source "TopicManagerPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ImageProgressTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private O8:Lcom/intsig/camscanner/util/ImageProgressClient;

.field private Oo08:I

.field private o〇0:Z

.field private 〇080:Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;

.field private 〇o00〇〇Oo:I

.field private 〇o〇:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x1

    .line 3
    iput-boolean v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->o〇0:Z

    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇080:Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;

    .line 5
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇o〇(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇o〇:Ljava/util/List;

    .line 6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    iput p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇o00〇〇Oo:I

    .line 7
    iput p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->Oo08:I

    .line 8
    new-instance p1, Lcom/intsig/camscanner/util/ImageProgressClient;

    invoke-direct {p1}, Lcom/intsig/camscanner/util/ImageProgressClient;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->O8:Lcom/intsig/camscanner/util/ImageProgressClient;

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;Ljava/util/List;L〇80O80O〇0/〇080;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;-><init>(Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;Ljava/util/List;)V

    return-void
.end method

.method private 〇o〇(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    if-eqz v2, :cond_2

    .line 15
    .line 16
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    check-cast v2, Lcom/intsig/camscanner/datastruct/PageProperty;

    .line 21
    .line 22
    iget v3, v2, Lcom/intsig/camscanner/datastruct/PageProperty;->〇〇08O:I

    .line 23
    .line 24
    if-gez v3, :cond_1

    .line 25
    .line 26
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    iget-object v3, v2, Lcom/intsig/camscanner/datastruct/PageProperty;->〇OOo8〇0:Ljava/lang/String;

    .line 31
    .line 32
    invoke-static {v3}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    if-nez v3, :cond_0

    .line 37
    .line 38
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    .line 43
    .line 44
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    .line 46
    .line 47
    const-string v2, "oriListSize = "

    .line 48
    .line 49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    const-string p1, " filterListSize = "

    .line 56
    .line 57
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    const-string v1, "TopicManagerPresenter"

    .line 68
    .line 69
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    return-object v0
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private 〇〇888(ILcom/intsig/camscanner/datastruct/PageProperty;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->O8:Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/util/ImageProgressClient;->setThreadContext(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->O8:Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 7
    .line 8
    iget-object v0, p2, Lcom/intsig/camscanner/datastruct/PageProperty;->OO:Ljava/lang/String;

    .line 9
    .line 10
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->〇08O8o〇0(Ljava/lang/String;)[I

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setRawImageSize([I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 15
    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->O8:Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 18
    .line 19
    iget-object v0, p2, Lcom/intsig/camscanner/datastruct/PageProperty;->OO:Ljava/lang/String;

    .line 20
    .line 21
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSrcImagePath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 22
    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->O8:Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 25
    .line 26
    iget-object v0, p2, Lcom/intsig/camscanner/datastruct/PageProperty;->〇0O:Ljava/lang/String;

    .line 27
    .line 28
    invoke-static {v0}, Lcom/intsig/camscanner/db/dao/ImageDaoUtil;->〇080(Ljava/lang/String;)[I

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setImageBorder([I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 33
    .line 34
    .line 35
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->O8:Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 36
    .line 37
    const/16 v0, 0x11

    .line 38
    .line 39
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setImageEnhanceMode(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 40
    .line 41
    .line 42
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->O8:Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 43
    .line 44
    iget v0, p2, Lcom/intsig/camscanner/datastruct/PageProperty;->oOo0:I

    .line 45
    .line 46
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setBrightness(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 47
    .line 48
    .line 49
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->O8:Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 50
    .line 51
    iget v0, p2, Lcom/intsig/camscanner/datastruct/PageProperty;->oOo〇8o008:I

    .line 52
    .line 53
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setContrast(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 54
    .line 55
    .line 56
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->O8:Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 57
    .line 58
    iget v0, p2, Lcom/intsig/camscanner/datastruct/PageProperty;->OO〇00〇8oO:I

    .line 59
    .line 60
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setDetail(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 61
    .line 62
    .line 63
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->O8:Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 64
    .line 65
    iget v0, p2, Lcom/intsig/camscanner/datastruct/PageProperty;->o8〇OO0〇0o:I

    .line 66
    .line 67
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setRation(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 68
    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->O8:Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 71
    .line 72
    iget-boolean v0, p2, Lcom/intsig/camscanner/datastruct/PageProperty;->o8oOOo:Z

    .line 73
    .line 74
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->enableTrim(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 75
    .line 76
    .line 77
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->O8:Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 78
    .line 79
    iget-object p2, p2, Lcom/intsig/camscanner/datastruct/PageProperty;->〇OOo8〇0:Ljava/lang/String;

    .line 80
    .line 81
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSaveImagePath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 82
    .line 83
    .line 84
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method


# virtual methods
.method public O8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇o00〇〇Oo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected Oo08(Ljava/lang/Void;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇080:Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;->〇o〇()V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇080:Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;

    .line 7
    .line 8
    invoke-interface {p1}, Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;->o8〇()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇080([Ljava/lang/Void;)Ljava/lang/Void;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public oO80(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->Oo08:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->Oo08(Ljava/lang/Void;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected onPreExecute()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇080:Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇o00〇〇Oo:I

    .line 4
    .line 5
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;->Oo08(I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Integer;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->o〇0([Ljava/lang/Integer;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected varargs o〇0([Ljava/lang/Integer;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇080:Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    aget-object p1, p1, v1

    .line 5
    .line 6
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;->〇8o8o〇(I)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected varargs 〇080([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 12

    .line 1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v0, "TOPIC_"

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string v1, "mImageProgressNumber="

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    iget v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇o00〇〇Oo:I

    .line 33
    .line 34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    const-string v1, "; uuid="

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    const-string v1, "TopicManagerPresenter"

    .line 50
    .line 51
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    iget v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇o00〇〇Oo:I

    .line 55
    .line 56
    const/4 v1, 0x0

    .line 57
    if-gtz v0, :cond_0

    .line 58
    .line 59
    return-object v1

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇080:Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;

    .line 61
    .line 62
    invoke-interface {v0}, Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;->o〇0()Landroid/content/Context;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-static {v0}, Lcom/intsig/nativelib/OcrLanguage;->getSysAndDefLanguage(Landroid/content/Context;)J

    .line 67
    .line 68
    .line 69
    move-result-wide v2

    .line 70
    new-instance v0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressStep;

    .line 71
    .line 72
    iget-object v4, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇080:Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;

    .line 73
    .line 74
    invoke-interface {v4}, Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;->o〇0()Landroid/content/Context;

    .line 75
    .line 76
    .line 77
    move-result-object v4

    .line 78
    invoke-direct {v0, v4}, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressStep;-><init>(Landroid/content/Context;)V

    .line 79
    .line 80
    .line 81
    invoke-static {v0}, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressStep;->〇o〇(Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressStep;)V

    .line 82
    .line 83
    .line 84
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerUtils;->initThreadContext()I

    .line 85
    .line 86
    .line 87
    move-result v4

    .line 88
    invoke-static {}, Lcom/intsig/camscanner/booksplitter/Util/BooksplitterUtils;->OO0o〇〇()I

    .line 89
    .line 90
    .line 91
    move-result v5

    .line 92
    iget-object v6, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇o〇:Ljava/util/List;

    .line 93
    .line 94
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 95
    .line 96
    .line 97
    move-result-object v6

    .line 98
    const/4 v7, 0x0

    .line 99
    const/4 v8, 0x0

    .line 100
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    .line 101
    .line 102
    .line 103
    move-result v9

    .line 104
    if-eqz v9, :cond_2

    .line 105
    .line 106
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 107
    .line 108
    .line 109
    move-result-object v9

    .line 110
    check-cast v9, Lcom/intsig/camscanner/datastruct/PageProperty;

    .line 111
    .line 112
    iget-object v10, v9, Lcom/intsig/camscanner/datastruct/PageProperty;->OO:Ljava/lang/String;

    .line 113
    .line 114
    invoke-static {v0, v10}, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressStep;->〇080(Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressStep;Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    iget-object v10, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->O8:Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 118
    .line 119
    invoke-virtual {v10}, Lcom/intsig/camscanner/util/ImageProgressClient;->reset()Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 120
    .line 121
    .line 122
    invoke-direct {p0, v4, v9}, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇〇888(ILcom/intsig/camscanner/datastruct/PageProperty;)V

    .line 123
    .line 124
    .line 125
    iget-object v10, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->O8:Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 126
    .line 127
    sget-object v11, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->INSTANCE:Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;

    .line 128
    .line 129
    invoke-virtual {v11}, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->isCropDewrapOn()Z

    .line 130
    .line 131
    .line 132
    move-result v11

    .line 133
    invoke-virtual {v10, v11}, Lcom/intsig/camscanner/util/ImageProgressClient;->setNeedCropDewrap(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 134
    .line 135
    .line 136
    move-result-object v10

    .line 137
    invoke-static {}, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->getNeedTrimWhenNoBorder()Z

    .line 138
    .line 139
    .line 140
    move-result v11

    .line 141
    invoke-virtual {v10, v11}, Lcom/intsig/camscanner/util/ImageProgressClient;->setNeedCropWhenNoBorder(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 142
    .line 143
    .line 144
    iget-object v10, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->O8:Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 145
    .line 146
    invoke-virtual {v10, p1, v1, v1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->executeProgress(Ljava/lang/String;Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;Lcom/intsig/camscanner/image_progress/ImageProcessCallback;Lcom/intsig/camscanner/image_progress/ImageProgressListener;)V

    .line 147
    .line 148
    .line 149
    const-string v10, ""

    .line 150
    .line 151
    invoke-static {v0, v10}, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressStep;->〇080(Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressStep;Ljava/lang/String;)V

    .line 152
    .line 153
    .line 154
    iget-boolean v10, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->o〇0:Z

    .line 155
    .line 156
    if-eqz v10, :cond_1

    .line 157
    .line 158
    iget-object v10, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇080:Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;

    .line 159
    .line 160
    invoke-interface {v10}, Lcom/intsig/camscanner/topic/contract/TopicManagerContract$View;->o〇0()Landroid/content/Context;

    .line 161
    .line 162
    .line 163
    move-result-object v10

    .line 164
    invoke-virtual {v10}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 165
    .line 166
    .line 167
    move-result-object v10

    .line 168
    iget-object v11, v9, Lcom/intsig/camscanner/datastruct/PageProperty;->〇OOo8〇0:Ljava/lang/String;

    .line 169
    .line 170
    invoke-static {v10, v2, v3, v11}, Lcom/intsig/ocrapi/OCRUtil;->oO80(Landroid/content/Context;JLjava/lang/String;)I

    .line 171
    .line 172
    .line 173
    move-result v10

    .line 174
    iput v10, v9, Lcom/intsig/camscanner/datastruct/PageProperty;->〇〇08O:I

    .line 175
    .line 176
    :cond_1
    const/4 v9, 0x1

    .line 177
    add-int/2addr v8, v9

    .line 178
    new-array v9, v9, [Ljava/lang/Integer;

    .line 179
    .line 180
    iget v10, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->Oo08:I

    .line 181
    .line 182
    int-to-float v10, v10

    .line 183
    const/high16 v11, 0x3f800000    # 1.0f

    .line 184
    .line 185
    mul-float v10, v10, v11

    .line 186
    .line 187
    int-to-float v11, v8

    .line 188
    mul-float v10, v10, v11

    .line 189
    .line 190
    iget v11, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->〇o00〇〇Oo:I

    .line 191
    .line 192
    int-to-float v11, v11

    .line 193
    div-float/2addr v10, v11

    .line 194
    float-to-int v10, v10

    .line 195
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 196
    .line 197
    .line 198
    move-result-object v10

    .line 199
    aput-object v10, v9, v7

    .line 200
    .line 201
    invoke-virtual {p0, v9}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 202
    .line 203
    .line 204
    goto :goto_0

    .line 205
    :cond_2
    invoke-static {v5}, Lcom/intsig/camscanner/booksplitter/Util/BooksplitterUtils;->〇〇808〇(I)V

    .line 206
    .line 207
    .line 208
    invoke-static {v0}, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressStep;->〇o00〇〇Oo(Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressStep;)V

    .line 209
    .line 210
    .line 211
    invoke-static {v4}, Lcom/intsig/camscanner/scanner/ScannerUtils;->destroyThreadContext(I)V

    .line 212
    .line 213
    .line 214
    return-object v1
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public 〇o00〇〇Oo(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicManagerPresenter$ImageProgressTask;->o〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

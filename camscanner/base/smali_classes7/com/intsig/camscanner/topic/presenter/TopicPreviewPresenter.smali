.class public Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;
.super Ljava/lang/Object;
.source "TopicPreviewPresenter.java"

# interfaces
.implements Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$IdentifyTextSizeTask;,
        Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/intsig/camscanner/topic/contract/TopicContract$Presenter<",
        "Lcom/intsig/camscanner/topic/TopicPreviewFragment;",
        ">;"
    }
.end annotation


# instance fields
.field private O8:I

.field private OO0o〇〇:Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;

.field private OO0o〇〇〇〇0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

.field private Oo08:I

.field private Oooo8o0〇:I

.field private oO80:I

.field private o〇0:I

.field private 〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

.field private 〇80〇808〇O:I

.field private 〇8o8o〇:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;"
        }
    .end annotation
.end field

.field private 〇O8o08O:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

.field private 〇o00〇〇Oo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private 〇o〇:I

.field public 〇〇808〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

.field private 〇〇888:I


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/topic/contract/TopicContract$View;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, -0x1

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->Oooo8o0〇:I

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->X2X1:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇〇808〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 10
    .line 11
    if-eqz p1, :cond_0

    .line 12
    .line 13
    new-instance v0, Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;

    .line 14
    .line 15
    invoke-direct {v0}, Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;-><init>()V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->OO0o〇〇:Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;

    .line 19
    .line 20
    iput-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 21
    .line 22
    return-void

    .line 23
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    .line 24
    .line 25
    const-string v0, "view can\'t be null!"

    .line 26
    .line 27
    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    throw p1
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private O8ooOoo〇(II)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->O8oOo80()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇〇808〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 10
    .line 11
    sget-object v1, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->TOPIC:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 12
    .line 13
    if-eq v0, v1, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->OO0o〇〇:Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇O8o08O:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 18
    .line 19
    iget v2, v1, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->width:F

    .line 20
    .line 21
    iget v1, v1, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->height:F

    .line 22
    .line 23
    invoke-virtual {v0, v2, v1}, Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;->o〇〇0〇(FF)V

    .line 24
    .line 25
    .line 26
    int-to-float p1, p1

    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->OO0o〇〇:Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;->O8()F

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    mul-float p1, p1, v0

    .line 34
    .line 35
    float-to-int p1, p1

    .line 36
    iput p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->Oo08:I

    .line 37
    .line 38
    int-to-float p1, p2

    .line 39
    iget-object p2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->OO0o〇〇:Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;

    .line 40
    .line 41
    invoke-virtual {p2}, Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;->〇oOO8O8()F

    .line 42
    .line 43
    .line 44
    move-result p2

    .line 45
    mul-float p2, p2, p1

    .line 46
    .line 47
    float-to-int p2, p2

    .line 48
    iput p2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o〇0:I

    .line 49
    .line 50
    iget-object p2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->OO0o〇〇:Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;

    .line 51
    .line 52
    invoke-virtual {p2}, Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;->〇80〇808〇O()F

    .line 53
    .line 54
    .line 55
    move-result p2

    .line 56
    mul-float p1, p1, p2

    .line 57
    .line 58
    float-to-int p1, p1

    .line 59
    iput p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇〇888:I

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_0
    int-to-float p1, p1

    .line 63
    const v0, 0x3ff47ae1    # 1.91f

    .line 64
    .line 65
    .line 66
    mul-float p1, p1, v0

    .line 67
    .line 68
    iget-object v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇O8o08O:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 69
    .line 70
    iget v2, v1, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->width:F

    .line 71
    .line 72
    div-float/2addr p1, v2

    .line 73
    float-to-int p1, p1

    .line 74
    iput p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->Oo08:I

    .line 75
    .line 76
    int-to-float p1, p2

    .line 77
    const p2, 0x40228f5c    # 2.54f

    .line 78
    .line 79
    .line 80
    mul-float p2, p2, p1

    .line 81
    .line 82
    iget v1, v1, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->height:F

    .line 83
    .line 84
    div-float/2addr p2, v1

    .line 85
    float-to-int p2, p2

    .line 86
    iput p2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o〇0:I

    .line 87
    .line 88
    mul-float p1, p1, v0

    .line 89
    .line 90
    div-float/2addr p1, v1

    .line 91
    float-to-int p1, p1

    .line 92
    iput p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇〇888:I

    .line 93
    .line 94
    :goto_0
    new-instance p1, Ljava/lang/StringBuilder;

    .line 95
    .line 96
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    .line 98
    .line 99
    const-string p2, "begin mStandTextSize="

    .line 100
    .line 101
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    iget p2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->Oooo8o0〇:I

    .line 105
    .line 106
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    const-string p2, " mPageSizeEnumType="

    .line 110
    .line 111
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    iget-object p2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇O8o08O:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 115
    .line 116
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object p1

    .line 123
    const-string p2, "TopicPreviewPresenter"

    .line 124
    .line 125
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    .line 127
    .line 128
    return-void
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private O8〇o(Ljava/util/List;)V
    .locals 18
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;)V"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o〇〇0〇()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    iget-object v2, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 8
    .line 9
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 10
    .line 11
    .line 12
    iget-object v2, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->OO0o〇〇:Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;

    .line 13
    .line 14
    iget-object v3, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇O8o08O:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 15
    .line 16
    iget v4, v3, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->width:F

    .line 17
    .line 18
    iget v3, v3, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->height:F

    .line 19
    .line 20
    invoke-virtual {v2, v4, v3}, Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;->o〇〇0〇(FF)V

    .line 21
    .line 22
    .line 23
    iget-object v2, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->OO0o〇〇:Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;

    .line 24
    .line 25
    iget-object v3, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇〇808〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 26
    .line 27
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;->O〇8O8〇008(Lcom/intsig/camscanner/topic/model/JigsawTemplate;)Ljava/util/List;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    const/4 v5, 0x0

    .line 36
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    .line 37
    .line 38
    .line 39
    move-result v6

    .line 40
    if-ge v5, v6, :cond_6

    .line 41
    .line 42
    new-instance v6, Ljava/util/ArrayList;

    .line 43
    .line 44
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 45
    .line 46
    .line 47
    const/4 v7, 0x0

    .line 48
    :goto_1
    if-ge v7, v3, :cond_5

    .line 49
    .line 50
    add-int v8, v5, v7

    .line 51
    .line 52
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    .line 53
    .line 54
    .line 55
    move-result v9

    .line 56
    if-ge v8, v9, :cond_3

    .line 57
    .line 58
    move-object/from16 v9, p1

    .line 59
    .line 60
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v8

    .line 64
    check-cast v8, Lcom/intsig/camscanner/topic/model/TopicModel;

    .line 65
    .line 66
    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 67
    .line 68
    .line 69
    move-result-object v10

    .line 70
    check-cast v10, Landroid/graphics/RectF;

    .line 71
    .line 72
    invoke-virtual {v10}, Landroid/graphics/RectF;->width()F

    .line 73
    .line 74
    .line 75
    move-result v11

    .line 76
    iget v12, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o〇:I

    .line 77
    .line 78
    int-to-float v12, v12

    .line 79
    mul-float v11, v11, v12

    .line 80
    .line 81
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    .line 82
    .line 83
    .line 84
    move-result v12

    .line 85
    iget v13, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O8:I

    .line 86
    .line 87
    int-to-float v13, v13

    .line 88
    mul-float v12, v12, v13

    .line 89
    .line 90
    iget-object v13, v8, Lcom/intsig/camscanner/topic/model/TopicModel;->〇080:Ljava/lang/String;

    .line 91
    .line 92
    invoke-direct {v0, v13}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o800o8O(Ljava/lang/String;)Lcom/intsig/camscanner/util/ParcelSize;

    .line 93
    .line 94
    .line 95
    move-result-object v13

    .line 96
    invoke-virtual {v13}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 97
    .line 98
    .line 99
    move-result v14

    .line 100
    invoke-virtual {v13}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 101
    .line 102
    .line 103
    move-result v15

    .line 104
    new-instance v4, Landroid/graphics/RectF;

    .line 105
    .line 106
    invoke-direct {v4, v10}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 107
    .line 108
    .line 109
    move-object/from16 v16, v2

    .line 110
    .line 111
    const/high16 v2, -0x3d4c0000    # -90.0f

    .line 112
    .line 113
    if-ge v14, v15, :cond_0

    .line 114
    .line 115
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->Oooo8o0〇()Z

    .line 116
    .line 117
    .line 118
    move-result v17

    .line 119
    if-eqz v17, :cond_0

    .line 120
    .line 121
    new-instance v11, Landroid/graphics/Matrix;

    .line 122
    .line 123
    invoke-direct {v11}, Landroid/graphics/Matrix;-><init>()V

    .line 124
    .line 125
    .line 126
    invoke-virtual {v10}, Landroid/graphics/RectF;->centerX()F

    .line 127
    .line 128
    .line 129
    move-result v12

    .line 130
    invoke-virtual {v10}, Landroid/graphics/RectF;->centerY()F

    .line 131
    .line 132
    .line 133
    move-result v9

    .line 134
    invoke-virtual {v11, v2, v12, v9}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 135
    .line 136
    .line 137
    invoke-virtual {v11, v4, v10}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 138
    .line 139
    .line 140
    const/high16 v9, 0x42b40000    # 90.0f

    .line 141
    .line 142
    iput v9, v8, Lcom/intsig/camscanner/topic/model/TopicModel;->〇〇888:F

    .line 143
    .line 144
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    .line 145
    .line 146
    .line 147
    move-result v9

    .line 148
    iget v11, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O8:I

    .line 149
    .line 150
    int-to-float v11, v11

    .line 151
    mul-float v11, v11, v9

    .line 152
    .line 153
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    .line 154
    .line 155
    .line 156
    move-result v9

    .line 157
    iget v12, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o〇:I

    .line 158
    .line 159
    int-to-float v12, v12

    .line 160
    mul-float v12, v12, v9

    .line 161
    .line 162
    :cond_0
    if-eqz v1, :cond_2

    .line 163
    .line 164
    invoke-virtual {v10}, Landroid/graphics/RectF;->width()F

    .line 165
    .line 166
    .line 167
    move-result v9

    .line 168
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    .line 169
    .line 170
    .line 171
    move-result v17

    .line 172
    cmpl-float v9, v9, v17

    .line 173
    .line 174
    if-lez v9, :cond_1

    .line 175
    .line 176
    if-ge v14, v15, :cond_2

    .line 177
    .line 178
    new-instance v9, Landroid/graphics/Matrix;

    .line 179
    .line 180
    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 181
    .line 182
    .line 183
    invoke-virtual {v10}, Landroid/graphics/RectF;->centerX()F

    .line 184
    .line 185
    .line 186
    move-result v11

    .line 187
    invoke-virtual {v10}, Landroid/graphics/RectF;->centerY()F

    .line 188
    .line 189
    .line 190
    move-result v12

    .line 191
    invoke-virtual {v9, v2, v11, v12}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 192
    .line 193
    .line 194
    invoke-virtual {v9, v4, v10}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 195
    .line 196
    .line 197
    const/high16 v2, 0x42b40000    # 90.0f

    .line 198
    .line 199
    iput v2, v8, Lcom/intsig/camscanner/topic/model/TopicModel;->〇〇888:F

    .line 200
    .line 201
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    .line 202
    .line 203
    .line 204
    move-result v2

    .line 205
    iget v9, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O8:I

    .line 206
    .line 207
    int-to-float v9, v9

    .line 208
    mul-float v11, v2, v9

    .line 209
    .line 210
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    .line 211
    .line 212
    .line 213
    move-result v2

    .line 214
    iget v9, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o〇:I

    .line 215
    .line 216
    goto :goto_2

    .line 217
    :cond_1
    if-le v14, v15, :cond_2

    .line 218
    .line 219
    new-instance v9, Landroid/graphics/Matrix;

    .line 220
    .line 221
    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 222
    .line 223
    .line 224
    invoke-virtual {v10}, Landroid/graphics/RectF;->centerX()F

    .line 225
    .line 226
    .line 227
    move-result v11

    .line 228
    invoke-virtual {v10}, Landroid/graphics/RectF;->centerY()F

    .line 229
    .line 230
    .line 231
    move-result v12

    .line 232
    invoke-virtual {v9, v2, v11, v12}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 233
    .line 234
    .line 235
    invoke-virtual {v9, v4, v10}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 236
    .line 237
    .line 238
    const/high16 v2, 0x42b40000    # 90.0f

    .line 239
    .line 240
    iput v2, v8, Lcom/intsig/camscanner/topic/model/TopicModel;->〇〇888:F

    .line 241
    .line 242
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    .line 243
    .line 244
    .line 245
    move-result v2

    .line 246
    iget v9, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O8:I

    .line 247
    .line 248
    int-to-float v9, v9

    .line 249
    mul-float v11, v2, v9

    .line 250
    .line 251
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    .line 252
    .line 253
    .line 254
    move-result v2

    .line 255
    iget v9, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o〇:I

    .line 256
    .line 257
    :goto_2
    int-to-float v9, v9

    .line 258
    mul-float v12, v2, v9

    .line 259
    .line 260
    :cond_2
    int-to-float v2, v14

    .line 261
    div-float/2addr v11, v2

    .line 262
    int-to-float v9, v15

    .line 263
    div-float/2addr v12, v9

    .line 264
    invoke-static {v11, v12}, Ljava/lang/Math;->min(FF)F

    .line 265
    .line 266
    .line 267
    move-result v10

    .line 268
    mul-float v2, v2, v10

    .line 269
    .line 270
    const/high16 v11, 0x40000000    # 2.0f

    .line 271
    .line 272
    div-float/2addr v2, v11

    .line 273
    mul-float v10, v10, v9

    .line 274
    .line 275
    div-float/2addr v10, v11

    .line 276
    new-instance v9, Landroid/graphics/Rect;

    .line 277
    .line 278
    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    .line 279
    .line 280
    .line 281
    move-result v11

    .line 282
    iget v12, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o〇:I

    .line 283
    .line 284
    int-to-float v12, v12

    .line 285
    mul-float v11, v11, v12

    .line 286
    .line 287
    sub-float/2addr v11, v2

    .line 288
    float-to-int v11, v11

    .line 289
    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    .line 290
    .line 291
    .line 292
    move-result v12

    .line 293
    iget v14, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O8:I

    .line 294
    .line 295
    int-to-float v14, v14

    .line 296
    mul-float v12, v12, v14

    .line 297
    .line 298
    sub-float/2addr v12, v10

    .line 299
    float-to-int v12, v12

    .line 300
    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    .line 301
    .line 302
    .line 303
    move-result v14

    .line 304
    iget v15, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o〇:I

    .line 305
    .line 306
    int-to-float v15, v15

    .line 307
    mul-float v14, v14, v15

    .line 308
    .line 309
    add-float/2addr v14, v2

    .line 310
    float-to-int v2, v14

    .line 311
    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    .line 312
    .line 313
    .line 314
    move-result v4

    .line 315
    iget v14, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O8:I

    .line 316
    .line 317
    int-to-float v14, v14

    .line 318
    mul-float v4, v4, v14

    .line 319
    .line 320
    add-float/2addr v4, v10

    .line 321
    float-to-int v4, v4

    .line 322
    invoke-direct {v9, v11, v12, v2, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 323
    .line 324
    .line 325
    iput-object v9, v8, Lcom/intsig/camscanner/topic/model/TopicModel;->oO80:Landroid/graphics/Rect;

    .line 326
    .line 327
    iput-object v13, v8, Lcom/intsig/camscanner/topic/model/TopicModel;->Oo08:Lcom/intsig/camscanner/util/ParcelSize;

    .line 328
    .line 329
    new-instance v2, Lcom/intsig/camscanner/util/ParcelSize;

    .line 330
    .line 331
    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    .line 332
    .line 333
    .line 334
    move-result v4

    .line 335
    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    .line 336
    .line 337
    .line 338
    move-result v9

    .line 339
    invoke-direct {v2, v4, v9}, Lcom/intsig/camscanner/util/ParcelSize;-><init>(II)V

    .line 340
    .line 341
    .line 342
    iput-object v2, v8, Lcom/intsig/camscanner/topic/model/TopicModel;->O8:Lcom/intsig/camscanner/util/ParcelSize;

    .line 343
    .line 344
    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 345
    .line 346
    .line 347
    goto :goto_3

    .line 348
    :cond_3
    move-object/from16 v16, v2

    .line 349
    .line 350
    invoke-interface {v6}, Ljava/util/List;->size()I

    .line 351
    .line 352
    .line 353
    move-result v2

    .line 354
    if-lez v2, :cond_4

    .line 355
    .line 356
    iget-object v1, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 357
    .line 358
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    .line 360
    .line 361
    return-void

    .line 362
    :cond_4
    :goto_3
    add-int/lit8 v7, v7, 0x1

    .line 363
    .line 364
    move-object/from16 v2, v16

    .line 365
    .line 366
    goto/16 :goto_1

    .line 367
    .line 368
    :cond_5
    move-object/from16 v16, v2

    .line 369
    .line 370
    iget-object v2, v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 371
    .line 372
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    .line 374
    .line 375
    add-int/2addr v5, v3

    .line 376
    move-object/from16 v2, v16

    .line 377
    .line 378
    goto/16 :goto_0

    .line 379
    .line 380
    :cond_6
    return-void
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method private OOO〇O0(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;)Z"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->O8oOo80()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇〇808〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 11
    .line 12
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->TOPIC:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 13
    .line 14
    if-ne v0, v2, :cond_0

    .line 15
    .line 16
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    check-cast v0, Lcom/intsig/camscanner/datastruct/PageProperty;

    .line 21
    .line 22
    iget v0, v0, Lcom/intsig/camscanner/datastruct/PageProperty;->〇〇08O:I

    .line 23
    .line 24
    if-gtz v0, :cond_0

    .line 25
    .line 26
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    const/4 v2, 0x1

    .line 31
    sub-int/2addr v0, v2

    .line 32
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    check-cast p1, Lcom/intsig/camscanner/datastruct/PageProperty;

    .line 37
    .line 38
    iget p1, p1, Lcom/intsig/camscanner/datastruct/PageProperty;->〇〇08O:I

    .line 39
    .line 40
    if-gtz p1, :cond_0

    .line 41
    .line 42
    const/4 v1, 0x1

    .line 43
    :cond_0
    return v1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private OoO8(Landroidx/collection/ArrayMap;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/collection/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Landroidx/collection/SimpleArrayMap;->size()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-lez v0, :cond_2

    .line 6
    .line 7
    new-instance v0, Ljava/util/ArrayList;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 10
    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇8o8o〇:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_3

    .line 23
    .line 24
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Lcom/intsig/camscanner/datastruct/PageProperty;

    .line 29
    .line 30
    if-nez v2, :cond_1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    iget-object v3, v2, Lcom/intsig/camscanner/datastruct/PageProperty;->〇OOo8〇0:Ljava/lang/String;

    .line 34
    .line 35
    invoke-virtual {p1, v3}, Landroidx/collection/SimpleArrayMap;->containsKey(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    if-nez v3, :cond_0

    .line 40
    .line 41
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇8o8o〇:Ljava/util/List;

    .line 46
    .line 47
    :cond_3
    return-object v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private O〇8O8〇008(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->o〇0()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lcom/intsig/nativelib/OcrLanguage;->getSysAndDefLanguage(Landroid/content/Context;)J

    .line 8
    .line 9
    .line 10
    move-result-wide v0

    .line 11
    new-instance v2, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$IdentifyTextSizeTask;

    .line 12
    .line 13
    invoke-direct {v2, p1, v0, v1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$IdentifyTextSizeTask;-><init>(Ljava/util/List;J)V

    .line 14
    .line 15
    .line 16
    new-instance p1, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$1;

    .line 17
    .line 18
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$1;-><init>(Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v2, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$IdentifyTextSizeTask;->O8(Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$IdentifyTextSizeTask$AsyncListener;)V

    .line 22
    .line 23
    .line 24
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const/4 v0, 0x0

    .line 29
    new-array v0, v0, [Ljava/util/List;

    .line 30
    .line 31
    invoke-virtual {v2, p1, v0}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private o0ooO(Lcom/intsig/camscanner/topic/model/TopicModel;IIILcom/intsig/camscanner/util/ParcelSize;)V
    .locals 6
    .param p1    # Lcom/intsig/camscanner/topic/model/TopicModel;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Lcom/intsig/camscanner/util/ParcelSize;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->Oo08:I

    .line 2
    .line 3
    add-int v1, v0, p3

    .line 4
    .line 5
    add-int v2, p2, p4

    .line 6
    .line 7
    add-int v3, v0, v1

    .line 8
    .line 9
    shr-int/lit8 v3, v3, 0x1

    .line 10
    .line 11
    add-int v4, p2, v2

    .line 12
    .line 13
    shr-int/lit8 v4, v4, 0x1

    .line 14
    .line 15
    new-instance v5, Landroid/graphics/Point;

    .line 16
    .line 17
    invoke-direct {v5, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 18
    .line 19
    .line 20
    new-instance v3, Lcom/intsig/camscanner/util/ParcelSize;

    .line 21
    .line 22
    invoke-direct {v3, p3, p4}, Lcom/intsig/camscanner/util/ParcelSize;-><init>(II)V

    .line 23
    .line 24
    .line 25
    new-instance p3, Landroid/graphics/Rect;

    .line 26
    .line 27
    invoke-direct {p3, v0, p2, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 28
    .line 29
    .line 30
    iput-object v5, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->〇o〇:Landroid/graphics/Point;

    .line 31
    .line 32
    iput-object v3, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->O8:Lcom/intsig/camscanner/util/ParcelSize;

    .line 33
    .line 34
    iput-object p3, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->oO80:Landroid/graphics/Rect;

    .line 35
    .line 36
    iput-object p5, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->Oo08:Lcom/intsig/camscanner/util/ParcelSize;

    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method private o800o8O(Ljava/lang/String;)Lcom/intsig/camscanner/util/ParcelSize;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇0〇O0088o(Ljava/lang/String;)Lcom/intsig/camscanner/util/ParcelSize;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private oo〇(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 11
    .line 12
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-gtz v0, :cond_1

    .line 17
    .line 18
    const-string p1, "TopicPreviewPresenter"

    .line 19
    .line 20
    const-string v0, "input data error"

    .line 21
    .line 22
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 26
    .line 27
    const/4 v0, 0x0

    .line 28
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->o〇0OOo〇0(Landroid/net/Uri;)V

    .line 29
    .line 30
    .line 31
    return-void

    .line 32
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->OOO〇O0(Ljava/util/List;)Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O〇8O8〇008(Ljava/util/List;)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇0〇O0088o(Ljava/util/List;)V

    .line 43
    .line 44
    .line 45
    :goto_0
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private o〇O8〇〇o()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o〇0()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇oo〇()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    mul-int/lit8 v1, v1, 0x2

    .line 10
    .line 11
    sub-int/2addr v0, v1

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇00()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->oo88o8O()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    mul-int/lit8 v1, v1, 0x2

    .line 10
    .line 11
    sub-int/2addr v0, v1

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇0000OOO(JLjava/util/List;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;)V"
        }
    .end annotation

    .line 1
    move-object v6, p0

    .line 2
    new-instance v0, Ljava/lang/StringBuilder;

    .line 3
    .line 4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5
    .line 6
    .line 7
    const-string v1, "preTransTopicData begin totalSize: "

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10
    .line 11
    .line 12
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    const-string v7, "TopicPreviewPresenter"

    .line 24
    .line 25
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    new-instance v0, Ljava/util/ArrayList;

    .line 29
    .line 30
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 31
    .line 32
    .line 33
    iget v1, v6, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o〇0:I

    .line 34
    .line 35
    new-instance v8, Ljava/lang/StringBuilder;

    .line 36
    .line 37
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 38
    .line 39
    .line 40
    const-string v9, "\n"

    .line 41
    .line 42
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 46
    .line 47
    .line 48
    move-result-object v10

    .line 49
    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    .line 50
    .line 51
    .line 52
    move-result v2

    .line 53
    if-eqz v2, :cond_4

    .line 54
    .line 55
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    move-object v11, v2

    .line 60
    check-cast v11, Lcom/intsig/camscanner/topic/model/TopicModel;

    .line 61
    .line 62
    iget-object v2, v11, Lcom/intsig/camscanner/topic/model/TopicModel;->〇080:Ljava/lang/String;

    .line 63
    .line 64
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o800o8O(Ljava/lang/String;)Lcom/intsig/camscanner/util/ParcelSize;

    .line 65
    .line 66
    .line 67
    move-result-object v5

    .line 68
    invoke-virtual {v5}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 69
    .line 70
    .line 71
    move-result v2

    .line 72
    if-ltz v2, :cond_0

    .line 73
    .line 74
    invoke-virtual {v5}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 75
    .line 76
    .line 77
    move-result v2

    .line 78
    if-gtz v2, :cond_1

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_1
    invoke-direct {p0, v11, v5}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇O888o0o(Lcom/intsig/camscanner/topic/model/TopicModel;Lcom/intsig/camscanner/util/ParcelSize;)F

    .line 82
    .line 83
    .line 84
    move-result v2

    .line 85
    invoke-virtual {v5}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 86
    .line 87
    .line 88
    move-result v3

    .line 89
    int-to-float v3, v3

    .line 90
    mul-float v3, v3, v2

    .line 91
    .line 92
    float-to-int v12, v3

    .line 93
    invoke-virtual {v5}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 94
    .line 95
    .line 96
    move-result v3

    .line 97
    int-to-float v3, v3

    .line 98
    mul-float v2, v2, v3

    .line 99
    .line 100
    float-to-int v3, v2

    .line 101
    add-int v2, v1, v12

    .line 102
    .line 103
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o〇0()I

    .line 104
    .line 105
    .line 106
    move-result v4

    .line 107
    if-le v2, v4, :cond_2

    .line 108
    .line 109
    iget-object v1, v6, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 110
    .line 111
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    .line 113
    .line 114
    new-instance v0, Ljava/util/ArrayList;

    .line 115
    .line 116
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .line 118
    .line 119
    iget v1, v6, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o〇0:I

    .line 120
    .line 121
    :cond_2
    move-object v13, v0

    .line 122
    move v14, v1

    .line 123
    move-object v0, p0

    .line 124
    move-object v1, v11

    .line 125
    move v2, v14

    .line 126
    move v4, v12

    .line 127
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o0ooO(Lcom/intsig/camscanner/topic/model/TopicModel;IIILcom/intsig/camscanner/util/ParcelSize;)V

    .line 128
    .line 129
    .line 130
    invoke-interface {v13, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    .line 132
    .line 133
    invoke-virtual {v11}, Lcom/intsig/camscanner/topic/model/TopicModel;->toString()Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v0

    .line 137
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    .line 142
    .line 143
    add-int/2addr v14, v12

    .line 144
    iget v0, v6, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇〇888:I

    .line 145
    .line 146
    add-int/2addr v14, v0

    .line 147
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o〇0()I

    .line 148
    .line 149
    .line 150
    move-result v0

    .line 151
    if-lt v14, v0, :cond_3

    .line 152
    .line 153
    iget-object v0, v6, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 154
    .line 155
    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    .line 157
    .line 158
    new-instance v0, Ljava/util/ArrayList;

    .line 159
    .line 160
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 161
    .line 162
    .line 163
    iget v1, v6, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o〇0:I

    .line 164
    .line 165
    goto :goto_0

    .line 166
    :cond_3
    move-object v0, v13

    .line 167
    move v1, v14

    .line 168
    goto :goto_0

    .line 169
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 170
    .line 171
    .line 172
    move-result v1

    .line 173
    if-lez v1, :cond_6

    .line 174
    .line 175
    iget-object v1, v6, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 176
    .line 177
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 178
    .line 179
    .line 180
    move-result v1

    .line 181
    if-eqz v1, :cond_5

    .line 182
    .line 183
    iget-object v1, v6, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 184
    .line 185
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 186
    .line 187
    .line 188
    move-result v2

    .line 189
    add-int/lit8 v2, v2, -0x1

    .line 190
    .line 191
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 192
    .line 193
    .line 194
    move-result-object v1

    .line 195
    if-eq v0, v1, :cond_6

    .line 196
    .line 197
    :cond_5
    iget-object v1, v6, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 198
    .line 199
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    .line 201
    .line 202
    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 203
    .line 204
    .line 205
    move-result-wide v0

    .line 206
    sub-long v0, v0, p1

    .line 207
    .line 208
    new-instance v2, Ljava/lang/StringBuilder;

    .line 209
    .line 210
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 211
    .line 212
    .line 213
    const-string v3, "preTransTopicData end costTime: "

    .line 214
    .line 215
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    .line 217
    .line 218
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 219
    .line 220
    .line 221
    const-string v0, "   | page : "

    .line 222
    .line 223
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    .line 225
    .line 226
    iget-object v0, v6, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 227
    .line 228
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 229
    .line 230
    .line 231
    move-result v0

    .line 232
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 233
    .line 234
    .line 235
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 236
    .line 237
    .line 238
    move-result-object v0

    .line 239
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    .line 241
    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    .line 243
    .line 244
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 245
    .line 246
    .line 247
    const-string v1, "preview data: "

    .line 248
    .line 249
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    .line 251
    .line 252
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 253
    .line 254
    .line 255
    move-result-object v1

    .line 256
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    .line 258
    .line 259
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 260
    .line 261
    .line 262
    move-result-object v0

    .line 263
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    .line 265
    .line 266
    return-void
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private 〇00〇8(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-eqz v1, :cond_1

    .line 15
    .line 16
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    check-cast v1, Lcom/intsig/camscanner/datastruct/PageProperty;

    .line 21
    .line 22
    if-eqz v1, :cond_0

    .line 23
    .line 24
    iget-object v2, v1, Lcom/intsig/camscanner/datastruct/PageProperty;->〇OOo8〇0:Ljava/lang/String;

    .line 25
    .line 26
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-nez v2, :cond_0

    .line 31
    .line 32
    new-instance v2, Lcom/intsig/camscanner/topic/model/TopicModel;

    .line 33
    .line 34
    iget-object v3, v1, Lcom/intsig/camscanner/datastruct/PageProperty;->〇OOo8〇0:Ljava/lang/String;

    .line 35
    .line 36
    iget v1, v1, Lcom/intsig/camscanner/datastruct/PageProperty;->〇〇08O:I

    .line 37
    .line 38
    invoke-direct {v2, v3, v1}, Lcom/intsig/camscanner/topic/model/TopicModel;-><init>(Ljava/lang/String;I)V

    .line 39
    .line 40
    .line 41
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private 〇0〇O0088o(Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇00〇8(Ljava/util/List;)Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    if-nez v2, :cond_0

    .line 14
    .line 15
    const-string p1, "TopicPreviewPresenter"

    .line 16
    .line 17
    const-string v0, "preTransTopicData error"

    .line 18
    .line 19
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 23
    .line 24
    const/4 v0, 0x0

    .line 25
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->o〇0OOo〇0(Landroid/net/Uri;)V

    .line 26
    .line 27
    .line 28
    return-void

    .line 29
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 30
    .line 31
    invoke-interface {v2}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->O8oOo80()Z

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    if-eqz v2, :cond_1

    .line 36
    .line 37
    iget-object v2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇〇808〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 38
    .line 39
    sget-object v3, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->TOPIC:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 40
    .line 41
    if-eq v2, v3, :cond_1

    .line 42
    .line 43
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O8〇o(Ljava/util/List;)V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_1
    invoke-direct {p0, v0, v1, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇0000OOO(JLjava/util/List;)V

    .line 48
    .line 49
    .line 50
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 53
    .line 54
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->o80ooO(Ljava/util/List;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method static bridge synthetic 〇O00(Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;)Lcom/intsig/camscanner/topic/contract/TopicContract$View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇O888o0o(Lcom/intsig/camscanner/topic/model/TopicModel;Lcom/intsig/camscanner/util/ParcelSize;)F
    .locals 6
    .param p1    # Lcom/intsig/camscanner/topic/model/TopicModel;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/util/ParcelSize;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget v0, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->〇o00〇〇Oo:I

    .line 2
    .line 3
    const/16 v1, 0x352

    .line 4
    .line 5
    const/4 v2, -0x1

    .line 6
    if-le v0, v1, :cond_0

    .line 7
    .line 8
    iput v2, p1, Lcom/intsig/camscanner/topic/model/TopicModel;->〇o00〇〇Oo:I

    .line 9
    .line 10
    new-instance p1, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v1, "getFinalScale text size: "

    .line 16
    .line 17
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    const-string v0, "TopicPreviewPresenter"

    .line 28
    .line 29
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    const/4 v0, -0x1

    .line 33
    :cond_0
    iget p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->Oooo8o0〇:I

    .line 34
    .line 35
    int-to-float p1, p1

    .line 36
    const/high16 v1, 0x3f800000    # 1.0f

    .line 37
    .line 38
    mul-float p1, p1, v1

    .line 39
    .line 40
    int-to-float v3, v0

    .line 41
    div-float/2addr p1, v3

    .line 42
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇00()I

    .line 43
    .line 44
    .line 45
    move-result v3

    .line 46
    int-to-float v3, v3

    .line 47
    mul-float v3, v3, v1

    .line 48
    .line 49
    invoke-virtual {p2}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 50
    .line 51
    .line 52
    move-result v4

    .line 53
    int-to-float v4, v4

    .line 54
    div-float/2addr v3, v4

    .line 55
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o〇O8〇〇o()I

    .line 56
    .line 57
    .line 58
    move-result v4

    .line 59
    int-to-float v4, v4

    .line 60
    mul-float v4, v4, v1

    .line 61
    .line 62
    invoke-virtual {p2}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 63
    .line 64
    .line 65
    move-result v5

    .line 66
    int-to-float v5, v5

    .line 67
    div-float/2addr v4, v5

    .line 68
    if-eq v2, v0, :cond_2

    .line 69
    .line 70
    if-lez v0, :cond_2

    .line 71
    .line 72
    cmpg-float v0, p1, v3

    .line 73
    .line 74
    if-gtz v0, :cond_1

    .line 75
    .line 76
    cmpg-float v0, p1, v4

    .line 77
    .line 78
    if-gtz v0, :cond_1

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_1
    invoke-static {v4, v3}, Ljava/lang/Math;->min(FF)F

    .line 82
    .line 83
    .line 84
    move-result p1

    .line 85
    goto :goto_0

    .line 86
    :cond_2
    const/high16 p1, -0x40800000    # -1.0f

    .line 87
    .line 88
    :goto_0
    const/4 v0, 0x0

    .line 89
    cmpg-float v0, p1, v0

    .line 90
    .line 91
    if-gez v0, :cond_3

    .line 92
    .line 93
    iget p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->oO80:I

    .line 94
    .line 95
    int-to-float p1, p1

    .line 96
    mul-float p1, p1, v1

    .line 97
    .line 98
    invoke-virtual {p2}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 99
    .line 100
    .line 101
    move-result p2

    .line 102
    int-to-float p2, p2

    .line 103
    div-float/2addr p1, p2

    .line 104
    invoke-static {p1, v3}, Ljava/lang/Math;->min(FF)F

    .line 105
    .line 106
    .line 107
    move-result p1

    .line 108
    :cond_3
    return p1
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private 〇o(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o〇0()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/topic/model/TopicModel;->〇080(Ljava/util/List;II)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇oOO8O8()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇O8o08O:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 6
    .line 7
    invoke-interface {v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->〇o〇o()Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇O8o08O:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 12
    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 15
    .line 16
    const/4 v1, -0x1

    .line 17
    iput v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o〇:I

    .line 18
    .line 19
    iput v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O8:I

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o〇0()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    int-to-float v3, v2

    .line 30
    const v4, 0x3ebd70a4    # 0.37f

    .line 31
    .line 32
    .line 33
    mul-float v3, v3, v4

    .line 34
    .line 35
    iget-object v4, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇O8o08O:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 36
    .line 37
    iget v4, v4, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->height:F

    .line 38
    .line 39
    div-float/2addr v3, v4

    .line 40
    float-to-int v3, v3

    .line 41
    iput v3, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->Oooo8o0〇:I

    .line 42
    .line 43
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O8ooOoo〇(II)V

    .line 44
    .line 45
    .line 46
    iget v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O8:I

    .line 47
    .line 48
    div-int/lit8 v2, v1, 0xc

    .line 49
    .line 50
    iput v2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->oO80:I

    .line 51
    .line 52
    iget v2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o〇:I

    .line 53
    .line 54
    if-ltz v2, :cond_1

    .line 55
    .line 56
    if-gez v1, :cond_2

    .line 57
    .line 58
    :cond_1
    const-string v1, "TopicPreviewPresenter"

    .line 59
    .line 60
    const-string v2, "initPreData error!"

    .line 61
    .line 62
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    iget-object v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 66
    .line 67
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->o〇0OOo〇0(Landroid/net/Uri;)V

    .line 68
    .line 69
    .line 70
    :cond_2
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇0〇O0088o(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public O8()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇80〇808〇O:I

    .line 2
    .line 3
    if-gtz v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/util/StatusBarHelper;->〇o00〇〇Oo()Lcom/intsig/camscanner/util/StatusBarHelper;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/StatusBarHelper;->〇o〇()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    iget-object v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 15
    .line 16
    invoke-interface {v1}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->o〇0()Landroid/content/Context;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    const v2, 0x7f070059

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    add-float/2addr v0, v1

    .line 32
    float-to-int v0, v0

    .line 33
    iput v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇80〇808〇O:I

    .line 34
    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    .line 36
    .line 37
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 38
    .line 39
    .line 40
    const-string v1, "getRootMarginToScreen: "

    .line 41
    .line 42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    iget v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇80〇808〇O:I

    .line 46
    .line 47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    const-string v1, "TopicPreviewPresenter"

    .line 55
    .line 56
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇80〇808〇O:I

    .line 60
    .line 61
    return v0
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public OO0o〇〇(Ljava/lang/String;Ljava/lang/String;Landroidx/collection/ArrayMap;Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;ZLcom/intsig/camscanner/topic/model/JigsawTemplate;Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroidx/collection/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;",
            "Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;",
            "Z",
            "Lcom/intsig/camscanner/topic/model/JigsawTemplate;",
            "Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    xor-int/lit8 v0, v0, 0x1

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 8
    .line 9
    if-eqz v1, :cond_1

    .line 10
    .line 11
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-nez v1, :cond_1

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 18
    .line 19
    iput-object p1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 20
    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    iput-object p2, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇080OO8〇0:Ljava/lang/String;

    .line 24
    .line 25
    :cond_0
    invoke-virtual {p7}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->getType()I

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    iput p1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇8〇oO〇〇8o:I

    .line 30
    .line 31
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 32
    .line 33
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o(Ljava/util/List;)V

    .line 34
    .line 35
    .line 36
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    iget-object p2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇O8o08O:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 41
    .line 42
    if-nez p2, :cond_2

    .line 43
    .line 44
    sget-object p7, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->A4:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 45
    .line 46
    iget p7, p7, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->width:F

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_2
    iget p7, p2, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->width:F

    .line 50
    .line 51
    :goto_0
    if-nez p2, :cond_3

    .line 52
    .line 53
    sget-object p2, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->A4:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 54
    .line 55
    :cond_3
    iget p2, p2, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->height:F

    .line 56
    .line 57
    invoke-static {p1, p7, p2}, Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;->〇8o8o〇(Landroid/content/Context;FF)Lcom/intsig/camscanner/util/ParcelSize;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    if-eqz v0, :cond_4

    .line 62
    .line 63
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->OoO8(Landroidx/collection/ArrayMap;)Ljava/util/List;

    .line 64
    .line 65
    .line 66
    move-result-object p2

    .line 67
    goto :goto_1

    .line 68
    :cond_4
    const/4 p2, 0x0

    .line 69
    :goto_1
    move-object v4, p2

    .line 70
    new-instance p2, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;

    .line 71
    .line 72
    iget-object v2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 73
    .line 74
    iget-object v3, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 75
    .line 76
    iget-object v6, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 77
    .line 78
    const/4 v7, 0x0

    .line 79
    move-object v1, p2

    .line 80
    move-object v5, p1

    .line 81
    invoke-direct/range {v1 .. v7}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;-><init>(Lcom/intsig/camscanner/topic/contract/TopicContract$View;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/util/List;Lcom/intsig/camscanner/util/ParcelSize;Ljava/util/List;L〇80O80O〇0/〇o〇;)V

    .line 82
    .line 83
    .line 84
    invoke-virtual {p2, p4}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇80〇808〇O(Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 88
    .line 89
    .line 90
    move-result p3

    .line 91
    int-to-float p3, p3

    .line 92
    const/high16 p4, 0x3f800000    # 1.0f

    .line 93
    .line 94
    mul-float p3, p3, p4

    .line 95
    .line 96
    iget p4, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o〇:I

    .line 97
    .line 98
    int-to-float p4, p4

    .line 99
    div-float/2addr p3, p4

    .line 100
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->OO0o〇〇〇〇0(F)V

    .line 101
    .line 102
    .line 103
    if-eqz p6, :cond_5

    .line 104
    .line 105
    iget-boolean p3, p6, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->isEnableRoundCorner:Z

    .line 106
    .line 107
    if-eqz p3, :cond_5

    .line 108
    .line 109
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 110
    .line 111
    .line 112
    move-result p1

    .line 113
    int-to-float p1, p1

    .line 114
    iget-object p3, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->OO0o〇〇:Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;

    .line 115
    .line 116
    invoke-virtual {p3}, Lcom/intsig/camscanner/topic/util/JigsawTemplateUtil;->〇o〇()F

    .line 117
    .line 118
    .line 119
    move-result p3

    .line 120
    mul-float p1, p1, p3

    .line 121
    .line 122
    float-to-int p1, p1

    .line 123
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->oO80(I)V

    .line 124
    .line 125
    .line 126
    :cond_5
    invoke-virtual {p2, p5}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇〇888(Z)V

    .line 127
    .line 128
    .line 129
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 130
    .line 131
    .line 132
    move-result-object p1

    .line 133
    const/4 p3, 0x0

    .line 134
    new-array p3, p3, [Ljava/lang/Void;

    .line 135
    .line 136
    invoke-virtual {p2, p1, p3}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 137
    .line 138
    .line 139
    return-void
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
.end method

.method public OO0o〇〇〇〇0()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-lez v0, :cond_0

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_0

    .line 23
    .line 24
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Ljava/util/List;

    .line 29
    .line 30
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    goto :goto_0

    .line 39
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 40
    .line 41
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .line 43
    .line 44
    const-string v2, "getMaxPageChildCount: "

    .line 45
    .line 46
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    const-string v2, "TopicPreviewPresenter"

    .line 57
    .line 58
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    return v1
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public Oo08()[Z
    .locals 7

    .line 1
    const-string v0, "TopicPreviewPresenter"

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    new-array v1, v1, [Z

    .line 5
    .line 6
    fill-array-data v1, :array_0

    .line 7
    .line 8
    .line 9
    :try_start_0
    iget-object v2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 10
    .line 11
    invoke-interface {v2}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->o〇0()Landroid/content/Context;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    const-string v3, "CamScanner_CertMode"

    .line 20
    .line 21
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v4

    .line 25
    iget-object v5, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 26
    .line 27
    invoke-interface {v5}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->o〇0()Landroid/content/Context;

    .line 28
    .line 29
    .line 30
    move-result-object v5

    .line 31
    invoke-static {v5}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o0O0(Landroid/content/Context;)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v5

    .line 35
    const/4 v6, 0x0

    .line 36
    invoke-static {v2, v3, v4, v5, v6}, Lcom/intsig/tianshu/TianShuAPI;->o8o〇〇0O(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    if-eqz v3, :cond_0

    .line 45
    .line 46
    new-instance v3, Ljava/lang/StringBuilder;

    .line 47
    .line 48
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    .line 50
    .line 51
    const-string v4, "TianShuAPI.purchaseByPoint result="

    .line 52
    .line 53
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_0 .. :try_end_0} :catch_1

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_0
    :try_start_1
    new-instance v3, Lorg/json/JSONObject;

    .line 68
    .line 69
    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    const-string v2, "ret"

    .line 73
    .line 74
    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    const-string v4, "103"

    .line 79
    .line 80
    invoke-static {v2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 81
    .line 82
    .line 83
    move-result v2

    .line 84
    const/4 v4, 0x1

    .line 85
    if-eqz v2, :cond_1

    .line 86
    .line 87
    iget-object v2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 88
    .line 89
    invoke-interface {v2}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->ooO〇00O()V

    .line 90
    .line 91
    .line 92
    const/4 v2, 0x0

    .line 93
    aput-boolean v4, v1, v2

    .line 94
    .line 95
    goto :goto_0

    .line 96
    :cond_1
    const-string v2, "data"

    .line 97
    .line 98
    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    .line 99
    .line 100
    .line 101
    move-result-object v2

    .line 102
    if-eqz v2, :cond_2

    .line 103
    .line 104
    const-string v3, "points"

    .line 105
    .line 106
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    .line 107
    .line 108
    .line 109
    move-result v2

    .line 110
    invoke-static {v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO〇〇(I)V

    .line 111
    .line 112
    .line 113
    :cond_2
    aput-boolean v4, v1, v4
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_1 .. :try_end_1} :catch_1

    .line 114
    .line 115
    goto :goto_0

    .line 116
    :catch_0
    move-exception v2

    .line 117
    :try_start_2
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_2 .. :try_end_2} :catch_1

    .line 118
    .line 119
    .line 120
    goto :goto_0

    .line 121
    :catch_1
    move-exception v2

    .line 122
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 123
    .line 124
    .line 125
    :goto_0
    return-object v1

    .line 126
    nop

    .line 127
    :array_0
    .array-data 1
        0x0t
        0x0t
    .end array-data
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public Oooo8o0〇()Z
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$2;->〇080:[I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇〇808〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 4
    .line 5
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    aget v0, v0, v1

    .line 10
    .line 11
    const/4 v1, 0x1

    .line 12
    if-eq v0, v1, :cond_0

    .line 13
    .line 14
    const/4 v2, 0x2

    .line 15
    if-eq v0, v2, :cond_0

    .line 16
    .line 17
    const/4 v2, 0x3

    .line 18
    if-eq v0, v2, :cond_0

    .line 19
    .line 20
    const/4 v2, 0x4

    .line 21
    if-eq v0, v2, :cond_0

    .line 22
    .line 23
    const/4 v0, 0x0

    .line 24
    return v0

    .line 25
    :cond_0
    return v1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public oO80()I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_2

    .line 5
    .line 6
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-lez v0, :cond_2

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_2

    .line 23
    .line 24
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Ljava/util/List;

    .line 29
    .line 30
    if-eqz v2, :cond_0

    .line 31
    .line 32
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    if-lez v3, :cond_0

    .line 37
    .line 38
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 39
    .line 40
    .line 41
    move-result-object v2

    .line 42
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 43
    .line 44
    .line 45
    move-result v3

    .line 46
    if-eqz v3, :cond_0

    .line 47
    .line 48
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    move-result-object v3

    .line 52
    check-cast v3, Lcom/intsig/camscanner/topic/model/TopicModel;

    .line 53
    .line 54
    if-eqz v3, :cond_1

    .line 55
    .line 56
    add-int/lit8 v1, v1, 0x1

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 60
    .line 61
    invoke-interface {v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->O8o08O8O()Z

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    if-eqz v0, :cond_3

    .line 66
    .line 67
    add-int/lit8 v1, v1, 0x1

    .line 68
    .line 69
    :cond_3
    return v1
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public oo88o8O()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->Oo08:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O8:I

    .line 2
    .line 3
    if-gtz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇O8o08O:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 6
    .line 7
    iget v1, v0, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->height:F

    .line 8
    .line 9
    iget v0, v0, Lcom/intsig/camscanner/topic/model/PageSizeEnumType;->width:F

    .line 10
    .line 11
    div-float/2addr v1, v0

    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    int-to-float v0, v0

    .line 17
    mul-float v1, v1, v0

    .line 18
    .line 19
    float-to-int v0, v1

    .line 20
    iput v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O8:I

    .line 21
    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string v1, "getPageHeight: "

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    iget v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O8:I

    .line 33
    .line 34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    const-string v1, "TopicPreviewPresenter"

    .line 42
    .line 43
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O8:I

    .line 47
    .line 48
    return v0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public o〇〇0〇()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$2;->〇080:[I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇〇808〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 4
    .line 5
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    aget v0, v0, v1

    .line 10
    .line 11
    packed-switch v0, :pswitch_data_0

    .line 12
    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    return v0

    .line 16
    :pswitch_0
    const/4 v0, 0x1

    .line 17
    return v0

    .line 18
    nop

    .line 19
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 20
    .line 21
.end method

.method public 〇080()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o〇:I

    .line 2
    .line 3
    if-gtz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 6
    .line 7
    invoke-interface {v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->o〇0()Landroid/content/Context;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o〇()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    mul-int/lit8 v1, v1, 0x2

    .line 20
    .line 21
    sub-int/2addr v0, v1

    .line 22
    iput v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o〇:I

    .line 23
    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    .line 25
    .line 26
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 27
    .line 28
    .line 29
    const-string v1, "getPageWidth: "

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    iget v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o〇:I

    .line 35
    .line 36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    const-string v1, "TopicPreviewPresenter"

    .line 44
    .line 45
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o〇:I

    .line 49
    .line 50
    return v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇80〇808〇O()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eqz v0, :cond_3

    .line 5
    .line 6
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-lez v0, :cond_3

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o00〇〇Oo:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_3

    .line 23
    .line 24
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Ljava/util/List;

    .line 29
    .line 30
    if-nez v1, :cond_1

    .line 31
    .line 32
    goto :goto_1

    .line 33
    :cond_1
    if-eqz v2, :cond_0

    .line 34
    .line 35
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    if-lez v3, :cond_0

    .line 40
    .line 41
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 46
    .line 47
    .line 48
    move-result v3

    .line 49
    if-eqz v3, :cond_0

    .line 50
    .line 51
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object v3

    .line 55
    check-cast v3, Lcom/intsig/camscanner/topic/model/TopicModel;

    .line 56
    .line 57
    if-eqz v3, :cond_2

    .line 58
    .line 59
    const/4 v1, 0x0

    .line 60
    goto :goto_0

    .line 61
    :cond_3
    :goto_1
    return v1
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇8o8o〇(Lcom/intsig/camscanner/topic/model/JigsawTemplate;Lcom/intsig/camscanner/topic/model/PageSizeEnumType;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/topic/model/JigsawTemplate;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/topic/model/PageSizeEnumType;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇〇808〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 2
    .line 3
    if-ne v0, p1, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iput-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇〇808〇:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 7
    .line 8
    iput-object p2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇O8o08O:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇oOO8O8()V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇8o8o〇:Ljava/util/List;

    .line 14
    .line 15
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->oo〇(Ljava/util/List;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇O8o08O(Landroid/content/Context;Ljava/util/List;Landroid/net/Uri;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-static {p1, p2, p3}, Lcom/intsig/camscanner/app/DBUtil;->O0〇OO8(Landroid/content/Context;Ljava/util/List;Landroid/net/Uri;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public 〇O〇()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->O8:I

    .line 3
    .line 4
    iput v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇o〇:I

    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080()I

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o〇0()I

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇oo〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->o〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇080:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->o〇0()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const v1, 0x7f0705f2

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    return v0
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇808〇(Ljava/util/List;Lcom/intsig/camscanner/topic/model/PageSizeEnumType;Z)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/topic/model/PageSizeEnumType;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;",
            "Lcom/intsig/camscanner/topic/model/PageSizeEnumType;",
            "Z)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇O8o08O:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    if-ne v0, p2, :cond_0

    .line 6
    .line 7
    if-eqz p3, :cond_2

    .line 8
    .line 9
    :cond_0
    iput-object p2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇O8o08O:Lcom/intsig/camscanner/topic/model/PageSizeEnumType;

    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇oOO8O8()V

    .line 12
    .line 13
    .line 14
    iget-object p2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇8o8o〇:Ljava/util/List;

    .line 15
    .line 16
    if-nez p2, :cond_1

    .line 17
    .line 18
    iput-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->〇8o8o〇:Ljava/util/List;

    .line 19
    .line 20
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;->oo〇(Ljava/util/List;)V

    .line 21
    .line 22
    .line 23
    :cond_2
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public 〇〇888()Ljava/util/List;
    .locals 11
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/topic/model/JigsawTemplate;",
            ">;"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 3
    .line 4
    .line 5
    move-result-object v1

    .line 6
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->O8()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    const-string v3, "zh"

    .line 23
    .line 24
    invoke-static {v3, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    const/4 v4, 0x0

    .line 29
    if-nez v3, :cond_3

    .line 30
    .line 31
    const-string v3, "cn"

    .line 32
    .line 33
    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 34
    .line 35
    .line 36
    move-result v3

    .line 37
    if-eqz v3, :cond_0

    .line 38
    .line 39
    goto :goto_1

    .line 40
    :cond_0
    const-string v3, "en"

    .line 41
    .line 42
    invoke-static {v3, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    const/4 v3, 0x6

    .line 47
    const/4 v5, 0x5

    .line 48
    const/4 v6, 0x4

    .line 49
    const/4 v7, 0x3

    .line 50
    const/4 v8, 0x2

    .line 51
    const/4 v9, 0x1

    .line 52
    const/4 v10, 0x7

    .line 53
    if-nez v1, :cond_2

    .line 54
    .line 55
    const-string v1, "us"

    .line 56
    .line 57
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    if-eqz v1, :cond_1

    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_1
    const/16 v1, 0x8

    .line 65
    .line 66
    new-array v1, v1, [Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 67
    .line 68
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->FAMILY_BOOKLET:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 69
    .line 70
    aput-object v2, v1, v4

    .line 71
    .line 72
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->FAMILY_BOOKLET_JIGSAW:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 73
    .line 74
    aput-object v2, v1, v9

    .line 75
    .line 76
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->ENTERPRISE_CERTIFICATE:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 77
    .line 78
    aput-object v2, v1, v8

    .line 79
    .line 80
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->US_DRIVER:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 81
    .line 82
    aput-object v2, v1, v7

    .line 83
    .line 84
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->CN_DRIVER:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 85
    .line 86
    aput-object v2, v1, v6

    .line 87
    .line 88
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->CN_TRAVEL:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 89
    .line 90
    aput-object v2, v1, v5

    .line 91
    .line 92
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->HOUSE_PROPERTY:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 93
    .line 94
    aput-object v2, v1, v3

    .line 95
    .line 96
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->BANK_CARD:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 97
    .line 98
    aput-object v2, v1, v10

    .line 99
    .line 100
    goto :goto_2

    .line 101
    :cond_2
    :goto_0
    new-array v1, v10, [Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 102
    .line 103
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->FAMILY_BOOKLET:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 104
    .line 105
    aput-object v2, v1, v4

    .line 106
    .line 107
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->FAMILY_BOOKLET_JIGSAW:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 108
    .line 109
    aput-object v2, v1, v9

    .line 110
    .line 111
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->ENTERPRISE_CERTIFICATE:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 112
    .line 113
    aput-object v2, v1, v8

    .line 114
    .line 115
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->CN_DRIVER:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 116
    .line 117
    aput-object v2, v1, v7

    .line 118
    .line 119
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->CN_TRAVEL:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 120
    .line 121
    aput-object v2, v1, v6

    .line 122
    .line 123
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->HOUSE_PROPERTY:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 124
    .line 125
    aput-object v2, v1, v5

    .line 126
    .line 127
    sget-object v2, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->BANK_CARD:Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 128
    .line 129
    aput-object v2, v1, v3

    .line 130
    .line 131
    goto :goto_2

    .line 132
    :cond_3
    :goto_1
    new-array v1, v4, [Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 133
    .line 134
    :goto_2
    invoke-static {}, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->getAllTemplates()[Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 135
    .line 136
    .line 137
    move-result-object v2

    .line 138
    new-instance v3, Ljava/util/ArrayList;

    .line 139
    .line 140
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 141
    .line 142
    .line 143
    move-result-object v2

    .line 144
    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 145
    .line 146
    .line 147
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    .line 148
    .line 149
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 150
    .line 151
    .line 152
    move-result-object v1

    .line 153
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 154
    .line 155
    .line 156
    invoke-interface {v3, v0}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 157
    .line 158
    .line 159
    goto :goto_5

    .line 160
    :catch_0
    move-exception v1

    .line 161
    move-object v0, v3

    .line 162
    goto :goto_3

    .line 163
    :catch_1
    move-exception v1

    .line 164
    :goto_3
    const-string v2, "TopicPreviewPresenter"

    .line 165
    .line 166
    const-string v3, "getJigsawTemplateData error"

    .line 167
    .line 168
    invoke-static {v2, v3, v1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 169
    .line 170
    .line 171
    if-eqz v0, :cond_5

    .line 172
    .line 173
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 174
    .line 175
    .line 176
    move-result v1

    .line 177
    if-nez v1, :cond_4

    .line 178
    .line 179
    goto :goto_4

    .line 180
    :cond_4
    move-object v3, v0

    .line 181
    goto :goto_5

    .line 182
    :cond_5
    :goto_4
    new-instance v3, Ljava/util/ArrayList;

    .line 183
    .line 184
    invoke-static {}, Lcom/intsig/camscanner/topic/model/JigsawTemplate;->getAllTemplates()[Lcom/intsig/camscanner/topic/model/JigsawTemplate;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 189
    .line 190
    .line 191
    move-result-object v0

    .line 192
    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 193
    .line 194
    .line 195
    :goto_5
    return-object v3
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

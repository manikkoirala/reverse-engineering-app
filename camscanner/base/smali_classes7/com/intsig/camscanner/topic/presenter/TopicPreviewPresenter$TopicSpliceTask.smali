.class Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;
.super Landroid/os/AsyncTask;
.source "TopicPreviewPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TopicSpliceTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field private O8:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

.field private OO0o〇〇:Z

.field private OO0o〇〇〇〇0:F

.field private Oo08:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;"
        }
    .end annotation
.end field

.field private oO80:Ljava/lang/String;

.field private o〇0:I

.field private 〇080:Lcom/intsig/camscanner/topic/splice/TopicSplice;

.field private 〇80〇808〇O:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

.field private 〇8o8o〇:I

.field private 〇O8o08O:Z

.field private 〇o00〇〇Oo:I

.field private 〇o〇:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

.field private 〇〇888:I


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/topic/contract/TopicContract$View;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/util/List;Lcom/intsig/camscanner/util/ParcelSize;Ljava/util/List;)V
    .locals 1
    .param p4    # Lcom/intsig/camscanner/util/ParcelSize;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/topic/contract/TopicContract$View;",
            "Lcom/intsig/camscanner/datastruct/ParcelDocInfo;",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;",
            "Lcom/intsig/camscanner/util/ParcelSize;",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/topic/model/TopicModel;",
            ">;>;)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->oO80:Ljava/lang/String;

    const/high16 v0, 0x3f800000    # 1.0f

    .line 4
    iput v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->OO0o〇〇〇〇0:F

    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇8o8o〇:I

    .line 6
    iput-boolean v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇O8o08O:Z

    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->OO0o〇〇:Z

    .line 8
    iput-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o〇:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 9
    iput-object p2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->O8:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 10
    iput-object p3, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->Oo08:Ljava/util/List;

    .line 11
    new-instance p1, Lcom/intsig/camscanner/topic/splice/TopicSplice;

    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->O8〇o()Landroid/graphics/Bitmap$Config;

    move-result-object p2

    invoke-direct {p1, p4, p2, p5}, Lcom/intsig/camscanner/topic/splice/TopicSplice;-><init>(Lcom/intsig/camscanner/util/ParcelSize;Landroid/graphics/Bitmap$Config;Ljava/util/List;)V

    iput-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇080:Lcom/intsig/camscanner/topic/splice/TopicSplice;

    .line 12
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result p1

    iput p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->o〇0:I

    if-eqz p3, :cond_0

    .line 13
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p1

    iput p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇〇888:I

    .line 14
    :cond_0
    iget p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->o〇0:I

    iget p2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇〇888:I

    add-int/2addr p1, p2

    iput p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o00〇〇Oo:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/topic/contract/TopicContract$View;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/util/List;Lcom/intsig/camscanner/util/ParcelSize;Ljava/util/List;L〇80O80O〇0/〇o〇;)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;-><init>(Lcom/intsig/camscanner/topic/contract/TopicContract$View;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/util/List;Lcom/intsig/camscanner/util/ParcelSize;Ljava/util/List;)V

    return-void
.end method

.method static synthetic 〇080(Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;[Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;[Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇o〇()Ljava/lang/String;
    .locals 6

    .line 1
    const-string v0, "TopicPreviewPresenter"

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o〇:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 4
    .line 5
    invoke-interface {v1}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->o〇0()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-static {v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const-string v2, "CamScanner_Patting"

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    iget-object v4, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o〇:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 20
    .line 21
    invoke-interface {v4}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->o〇0()Landroid/content/Context;

    .line 22
    .line 23
    .line 24
    move-result-object v4

    .line 25
    invoke-static {v4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o0O0(Landroid/content/Context;)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v4

    .line 29
    const/4 v5, 0x0

    .line 30
    :try_start_0
    invoke-static {v1, v2, v3, v4, v5}, Lcom/intsig/tianshu/TianShuAPI;->o8o〇〇0O(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    new-instance v2, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v3, "checkTopicTryNumber result="

    .line 40
    .line 41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    if-nez v2, :cond_2

    .line 59
    .line 60
    new-instance v2, Lcom/intsig/tianshu/purchase/TryFuncDeductionResut;

    .line 61
    .line 62
    invoke-direct {v2, v1}, Lcom/intsig/tianshu/purchase/TryFuncDeductionResut;-><init>(Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v2}, Lcom/intsig/tianshu/purchase/TryFuncDeductionResut;->isOK()Z

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    if-eqz v1, :cond_1

    .line 70
    .line 71
    iget-object v1, v2, Lcom/intsig/tianshu/purchase/TryFuncDeductionResut;->data:Lcom/intsig/tianshu/purchase/TryFuncDeductionResut$TryFuncDeductionData;

    .line 72
    .line 73
    if-nez v1, :cond_0

    .line 74
    .line 75
    const-string v1, "tryFuncDeductionResut.data == null"

    .line 76
    .line 77
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    goto :goto_1

    .line 81
    :cond_0
    iget v1, v1, Lcom/intsig/tianshu/purchase/TryFuncDeductionResut$TryFuncDeductionData;->balance:I

    .line 82
    .line 83
    const/4 v2, -0x1

    .line 84
    if-le v1, v2, :cond_2

    .line 85
    .line 86
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8oo8〇(I)V

    .line 87
    .line 88
    .line 89
    goto :goto_1

    .line 90
    :cond_1
    iget-object v5, v2, Lcom/intsig/tianshu/purchase/TryFuncDeductionResut;->ret:Ljava/lang/String;
    :try_end_0
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    .line 92
    goto :goto_1

    .line 93
    :catch_0
    move-exception v1

    .line 94
    goto :goto_0

    .line 95
    :catch_1
    move-exception v1

    .line 96
    :goto_0
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 97
    .line 98
    .line 99
    const-string v5, "101"

    .line 100
    .line 101
    goto :goto_1

    .line 102
    :catch_2
    move-exception v1

    .line 103
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 104
    .line 105
    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    .line 107
    .line 108
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 109
    .line 110
    .line 111
    invoke-virtual {v1}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 112
    .line 113
    .line 114
    move-result v1

    .line 115
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    const-string v1, ""

    .line 119
    .line 120
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 124
    .line 125
    .line 126
    move-result-object v5

    .line 127
    :cond_2
    :goto_1
    return-object v5
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method


# virtual methods
.method protected varargs O8([Ljava/lang/Void;)Landroid/net/Uri;
    .locals 14

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const/4 v0, 0x1

    .line 6
    if-nez p1, :cond_2

    .line 7
    .line 8
    iget-boolean p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->OO0o〇〇:Z

    .line 9
    .line 10
    if-eqz p1, :cond_2

    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o〇()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    iput-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->oO80:Ljava/lang/String;

    .line 17
    .line 18
    const-string v1, "102"

    .line 19
    .line 20
    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    if-eqz p1, :cond_0

    .line 25
    .line 26
    iput-boolean v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇O8o08O:Z

    .line 27
    .line 28
    :try_start_0
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OO88o()V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o〇()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    iput-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->oO80:Ljava/lang/String;

    .line 36
    .line 37
    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    iput-boolean p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇O8o08O:Z
    :try_end_0
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :catch_0
    move-exception p1

    .line 45
    invoke-virtual {p1}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    invoke-static {v1}, Lcom/intsig/tianshu/TianShuAPI;->〇00(I)Z

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    if-eqz v1, :cond_0

    .line 54
    .line 55
    invoke-virtual {p1}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    invoke-static {v1}, Lcom/intsig/tianshu/TianShuAPI;->OO8〇(I)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {p1}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇00O〇8o(I)V

    .line 67
    .line 68
    .line 69
    :cond_0
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->oO80:Ljava/lang/String;

    .line 70
    .line 71
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 72
    .line 73
    .line 74
    move-result p1

    .line 75
    if-eqz p1, :cond_1

    .line 76
    .line 77
    iget-boolean p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇O8o08O:Z

    .line 78
    .line 79
    if-eqz p1, :cond_2

    .line 80
    .line 81
    :cond_1
    const/4 p1, 0x0

    .line 82
    return-object p1

    .line 83
    :cond_2
    new-array p1, v0, [I

    .line 84
    .line 85
    iget v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->o〇0:I

    .line 86
    .line 87
    add-int v2, v1, v1

    .line 88
    .line 89
    iget v3, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇〇888:I

    .line 90
    .line 91
    add-int/2addr v2, v3

    .line 92
    const/high16 v3, 0x3f800000    # 1.0f

    .line 93
    .line 94
    int-to-float v1, v1

    .line 95
    mul-float v1, v1, v3

    .line 96
    .line 97
    int-to-float v2, v2

    .line 98
    div-float/2addr v1, v2

    .line 99
    iget v2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o00〇〇Oo:I

    .line 100
    .line 101
    int-to-float v2, v2

    .line 102
    mul-float v2, v2, v1

    .line 103
    .line 104
    iget-object v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇080:Lcom/intsig/camscanner/topic/splice/TopicSplice;

    .line 105
    .line 106
    new-instance v3, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask$1;

    .line 107
    .line 108
    invoke-direct {v3, p0, p1, v2}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask$1;-><init>(Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;[IF)V

    .line 109
    .line 110
    .line 111
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/topic/splice/TopicSplice;->OO0o〇〇(Lcom/intsig/camscanner/topic/splice/TopicSplice$SpliceProgressListener;)V

    .line 112
    .line 113
    .line 114
    iget v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o00〇〇Oo:I

    .line 115
    .line 116
    int-to-float v1, v1

    .line 117
    sub-float/2addr v1, v2

    .line 118
    iget-object v2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇080:Lcom/intsig/camscanner/topic/splice/TopicSplice;

    .line 119
    .line 120
    iget-object v3, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o〇:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 121
    .line 122
    invoke-interface {v3}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->o〇0()Landroid/content/Context;

    .line 123
    .line 124
    .line 125
    move-result-object v3

    .line 126
    iget-object v4, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇80〇808〇O:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 127
    .line 128
    iget v5, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->OO0o〇〇〇〇0:F

    .line 129
    .line 130
    iget v6, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇8o8o〇:I

    .line 131
    .line 132
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/intsig/camscanner/topic/splice/TopicSplice;->〇80〇808〇O(Landroid/content/Context;Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;FI)Ljava/util/List;

    .line 133
    .line 134
    .line 135
    move-result-object v11

    .line 136
    iget-object v2, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o〇:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 137
    .line 138
    invoke-interface {v2}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->o〇0()Landroid/content/Context;

    .line 139
    .line 140
    .line 141
    move-result-object v2

    .line 142
    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 143
    .line 144
    .line 145
    move-result-object v8

    .line 146
    new-instance v7, Lcom/intsig/camscanner/topic/database/operation/TopicDatabaseOperation;

    .line 147
    .line 148
    invoke-direct {v7}, Lcom/intsig/camscanner/topic/database/operation/TopicDatabaseOperation;-><init>()V

    .line 149
    .line 150
    .line 151
    iget-object v9, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->O8:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 152
    .line 153
    iget-object v10, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->Oo08:Ljava/util/List;

    .line 154
    .line 155
    new-instance v12, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask$2;

    .line 156
    .line 157
    invoke-direct {v12, p0, v1, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask$2;-><init>(Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;F[I)V

    .line 158
    .line 159
    .line 160
    iget-boolean v13, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->OO0o〇〇:Z

    .line 161
    .line 162
    invoke-virtual/range {v7 .. v13}, Lcom/intsig/camscanner/topic/database/operation/TopicDatabaseOperation;->o〇0(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/util/List;Ljava/util/List;Lcom/intsig/camscanner/topic/database/operation/TopicDatabaseOperation$HandleProgressListener;Z)Landroid/net/Uri;

    .line 163
    .line 164
    .line 165
    move-result-object p1

    .line 166
    new-array v0, v0, [Ljava/lang/Integer;

    .line 167
    .line 168
    iget v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o00〇〇Oo:I

    .line 169
    .line 170
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 171
    .line 172
    .line 173
    move-result-object v1

    .line 174
    const/4 v2, 0x0

    .line 175
    aput-object v1, v0, v2

    .line 176
    .line 177
    invoke-virtual {p0, v0}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 178
    .line 179
    .line 180
    return-object p1
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method OO0o〇〇〇〇0(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->OO0o〇〇〇〇0:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected Oo08(Landroid/net/Uri;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o〇:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->〇o〇()V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇O8o08O:Z

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o〇:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 11
    .line 12
    invoke-interface {p1}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->〇oOo〇()V

    .line 13
    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->oO80:Ljava/lang/String;

    .line 17
    .line 18
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o〇:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 25
    .line 26
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->o〇0OOo〇0(Landroid/net/Uri;)V

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->oO80:Ljava/lang/String;

    .line 31
    .line 32
    const-string v0, "103"

    .line 33
    .line 34
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    if-eqz p1, :cond_2

    .line 39
    .line 40
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o〇:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 41
    .line 42
    invoke-interface {p1}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->OO88〇OOO()V

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o〇:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 47
    .line 48
    invoke-interface {p1}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->O〇0〇o808〇()V

    .line 49
    .line 50
    .line 51
    :goto_0
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->O8([Ljava/lang/Void;)Landroid/net/Uri;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method oO80(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇8o8o〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Landroid/net/Uri;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->Oo08(Landroid/net/Uri;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected onPreExecute()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o〇:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o00〇〇Oo:I

    .line 4
    .line 5
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->Oo08(I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Integer;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->o〇0([Ljava/lang/Integer;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected varargs o〇0([Ljava/lang/Integer;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇o〇:Lcom/intsig/camscanner/topic/contract/TopicContract$View;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    aget-object p1, p1, v1

    .line 5
    .line 6
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/topic/contract/TopicContract$View;->〇8o8o〇(I)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method 〇80〇808〇O(Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->〇80〇808〇O:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method 〇〇888(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/topic/presenter/TopicPreviewPresenter$TopicSpliceTask;->OO0o〇〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;
.super Ljava/lang/Object;
.source "TopicScannerActivity.java"

# interfaces
.implements Lcom/intsig/scanner/ScannerEngine$ScannerProcessListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/topic/TopicScannerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageProcessListener"
.end annotation


# instance fields
.field private O8:[I

.field private Oo08:I

.field private o〇0:J

.field private 〇080:Landroid/os/Handler;

.field private 〇o00〇〇Oo:Landroid/graphics/Bitmap;

.field private 〇o〇:Landroid/graphics/Bitmap;

.field final synthetic 〇〇888:Lcom/intsig/camscanner/topic/TopicScannerActivity;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/topic/TopicScannerActivity;Landroid/os/Handler;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇〇888:Lcom/intsig/camscanner/topic/TopicScannerActivity;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    const-wide/16 v0, 0x0

    .line 7
    .line 8
    iput-wide v0, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->o〇0:J

    .line 9
    .line 10
    iput-object p2, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇080:Landroid/os/Handler;

    .line 11
    .line 12
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 13
    .line 14
    const/16 p2, 0x1a

    .line 15
    .line 16
    if-lt p1, p2, :cond_0

    .line 17
    .line 18
    const/16 p1, 0x1e

    .line 19
    .line 20
    iput p1, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->Oo08:I

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/16 p2, 0x18

    .line 24
    .line 25
    if-lt p1, p2, :cond_1

    .line 26
    .line 27
    const/16 p1, 0x32

    .line 28
    .line 29
    iput p1, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->Oo08:I

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    const/16 p1, 0x64

    .line 33
    .line 34
    iput p1, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->Oo08:I

    .line 35
    .line 36
    :goto_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public onProcess(II)Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇〇888:Lcom/intsig/camscanner/topic/TopicScannerActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    return v1

    .line 11
    :cond_0
    const/4 v0, 0x4

    .line 12
    if-eq p1, v0, :cond_1

    .line 13
    .line 14
    if-nez p1, :cond_2

    .line 15
    .line 16
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇080:Landroid/os/Handler;

    .line 17
    .line 18
    const/16 v2, 0x3ee

    .line 19
    .line 20
    invoke-static {v0, v2, p2, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 25
    .line 26
    .line 27
    :cond_2
    const/4 v0, 0x3

    .line 28
    if-ne p1, v0, :cond_5

    .line 29
    .line 30
    add-int/lit8 p2, p2, 0xa

    .line 31
    .line 32
    const/16 p1, 0x64

    .line 33
    .line 34
    if-le p2, p1, :cond_3

    .line 35
    .line 36
    const/16 p2, 0x64

    .line 37
    .line 38
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 39
    .line 40
    invoke-static {p1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->OO0o〇〇〇〇0(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    iput-object p1, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇o〇:Landroid/graphics/Bitmap;

    .line 45
    .line 46
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇〇888:Lcom/intsig/camscanner/topic/TopicScannerActivity;

    .line 47
    .line 48
    invoke-static {p1}, Lcom/intsig/camscanner/topic/TopicScannerActivity;->O00OoO〇(Lcom/intsig/camscanner/topic/TopicScannerActivity;)Lcom/intsig/camscanner/control/ISImageEnhanceHandler;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/intsig/camscanner/control/ISImageEnhanceHandler;->〇O888o0o()I

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    iget-object v3, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 57
    .line 58
    iget-object v4, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->O8:[I

    .line 59
    .line 60
    iget-object v5, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇o〇:Landroid/graphics/Bitmap;

    .line 61
    .line 62
    const/16 v7, 0x64

    .line 63
    .line 64
    move v6, p2

    .line 65
    invoke-static/range {v2 .. v7}, Lcom/intsig/scanner/ScannerEngine;->drawDewarpProgressImage(ILandroid/graphics/Bitmap;[ILandroid/graphics/Bitmap;II)I

    .line 66
    .line 67
    .line 68
    iget p1, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->Oo08:I

    .line 69
    .line 70
    int-to-long v2, p1

    .line 71
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 72
    .line 73
    .line 74
    move-result-wide v4

    .line 75
    iget-wide v6, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->o〇0:J

    .line 76
    .line 77
    sub-long/2addr v4, v6

    .line 78
    sub-long/2addr v2, v4

    .line 79
    const-wide/16 v4, 0x0

    .line 80
    .line 81
    cmp-long p1, v2, v4

    .line 82
    .line 83
    if-lez p1, :cond_4

    .line 84
    .line 85
    invoke-static {}, Lcom/intsig/camscanner/topic/TopicScannerActivity;->O088O()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    .line 90
    .line 91
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    .line 93
    .line 94
    const-string v4, "trim anim sleep: "

    .line 95
    .line 96
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    .line 108
    .line 109
    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    .line 111
    .line 112
    goto :goto_0

    .line 113
    :catch_0
    move-exception p1

    .line 114
    invoke-static {}, Lcom/intsig/camscanner/topic/TopicScannerActivity;->O088O()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 119
    .line 120
    .line 121
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 122
    .line 123
    .line 124
    move-result-object p1

    .line 125
    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    .line 126
    .line 127
    .line 128
    :cond_4
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇080:Landroid/os/Handler;

    .line 129
    .line 130
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇o〇:Landroid/graphics/Bitmap;

    .line 131
    .line 132
    const/16 v2, 0x3ed

    .line 133
    .line 134
    invoke-virtual {p1, v2, p2, v1, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    .line 135
    .line 136
    .line 137
    move-result-object p2

    .line 138
    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 139
    .line 140
    .line 141
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 142
    .line 143
    .line 144
    move-result-wide p1

    .line 145
    iput-wide p1, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->o〇0:J

    .line 146
    .line 147
    const/4 p1, 0x1

    .line 148
    return p1
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public 〇080(Landroid/graphics/Bitmap;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇〇888:Lcom/intsig/camscanner/topic/TopicScannerActivity;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/intsig/camscanner/topic/TopicScannerActivity;->O〇o8(Lcom/intsig/camscanner/topic/TopicScannerActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    iput-object p1, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇〇888:Lcom/intsig/camscanner/topic/TopicScannerActivity;

    .line 10
    .line 11
    invoke-static {p1}, Lcom/intsig/camscanner/topic/TopicScannerActivity;->o808o8o08(Lcom/intsig/camscanner/topic/TopicScannerActivity;)[I

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    array-length p1, p1

    .line 16
    new-array p1, p1, [I

    .line 17
    .line 18
    iput-object p1, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->O8:[I

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->O8:[I

    .line 22
    .line 23
    array-length v1, v0

    .line 24
    if-ge p1, v1, :cond_0

    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇〇888:Lcom/intsig/camscanner/topic/TopicScannerActivity;

    .line 27
    .line 28
    invoke-static {v1}, Lcom/intsig/camscanner/topic/TopicScannerActivity;->o808o8o08(Lcom/intsig/camscanner/topic/TopicScannerActivity;)[I

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    aget v1, v1, p1

    .line 33
    .line 34
    int-to-double v1, v1

    .line 35
    iget-object v3, p0, Lcom/intsig/camscanner/topic/TopicScannerActivity$ImageProcessListener;->〇〇888:Lcom/intsig/camscanner/topic/TopicScannerActivity;

    .line 36
    .line 37
    invoke-static {v3}, Lcom/intsig/camscanner/topic/TopicScannerActivity;->O〇0o8o8〇(Lcom/intsig/camscanner/topic/TopicScannerActivity;)D

    .line 38
    .line 39
    .line 40
    move-result-wide v3

    .line 41
    mul-double v1, v1, v3

    .line 42
    .line 43
    double-to-int v1, v1

    .line 44
    aput v1, v0, p1

    .line 45
    .line 46
    add-int/lit8 p1, p1, 0x1

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

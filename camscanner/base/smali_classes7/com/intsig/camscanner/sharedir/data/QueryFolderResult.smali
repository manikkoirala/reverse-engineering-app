.class public final Lcom/intsig/camscanner/sharedir/data/QueryFolderResult;
.super Ljava/lang/Object;
.source "QueryFolderResult.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final CamScanner_Doc:Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;

.field private final CamScanner_Page:Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;

.field private final CamScanner_Tag:Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;


# direct methods
.method public constructor <init>()V
    .locals 6

    .line 1
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/sharedir/data/QueryFolderResult;-><init>(Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/sharedir/data/QueryFolderResult;->CamScanner_Doc:Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;

    .line 4
    iput-object p2, p0, Lcom/intsig/camscanner/sharedir/data/QueryFolderResult;->CamScanner_Tag:Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;

    .line 5
    iput-object p3, p0, Lcom/intsig/camscanner/sharedir/data/QueryFolderResult;->CamScanner_Page:Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    move-object p1, v0

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    move-object p2, v0

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    move-object p3, v0

    .line 6
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/sharedir/data/QueryFolderResult;-><init>(Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;)V

    return-void
.end method


# virtual methods
.method public final getCamScanner_Doc()Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/sharedir/data/QueryFolderResult;->CamScanner_Doc:Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getCamScanner_Page()Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/sharedir/data/QueryFolderResult;->CamScanner_Page:Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getCamScanner_Tag()Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/sharedir/data/QueryFolderResult;->CamScanner_Tag:Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

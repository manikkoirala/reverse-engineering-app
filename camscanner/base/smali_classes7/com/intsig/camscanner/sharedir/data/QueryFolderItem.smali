.class public final Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;
.super Ljava/lang/Object;
.source "QueryFolderItem.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final add_num:I

.field private final del_num:I

.field private final rev:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .line 1
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;-><init>(IIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(III)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput p1, p0, Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;->rev:I

    .line 4
    iput p2, p0, Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;->add_num:I

    .line 5
    iput p3, p0, Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;->del_num:I

    return-void
.end method

.method public synthetic constructor <init>(IIIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    const/4 p2, 0x0

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    const/4 p3, 0x0

    .line 6
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;-><init>(III)V

    return-void
.end method


# virtual methods
.method public final getAdd_num()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;->add_num:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getDel_num()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;->del_num:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getRev()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/sharedir/data/QueryFolderItem;->rev:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

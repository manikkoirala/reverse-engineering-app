.class public final Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;
.super Ljava/lang/Object;
.source "SmartErasePainter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;,
        Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion;,
        Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PointDrawerInfo;,
        Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$WhenMappings;,
        Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O〇8O8〇008:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8:F

.field private final OO0o〇〇:Landroid/graphics/Path;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO0o〇〇〇〇0:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

.field private Oo08:F

.field private final OoO8:I

.field private final Oooo8o0〇:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o800o8O:I

.field private oO80:Landroid/graphics/RectF;

.field private final oo88o8O:Landroid/graphics/Bitmap;

.field private o〇0:F

.field private o〇O8〇〇o:Landroid/graphics/Matrix;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇00:F

.field private final 〇080:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0〇O0088o:Z

.field private 〇80〇808〇O:Landroid/graphics/RectF;

.field private 〇8o8o〇:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇O00:Z

.field private final 〇O888o0o:Landroid/graphics/Bitmap;

.field private 〇O8o08O:F

.field private 〇O〇:Z

.field private final 〇o00〇〇Oo:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇oo〇:F

.field private 〇o〇:F

.field private 〇〇808〇:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PointDrawerInfo;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇888:I

.field private 〇〇8O0〇8:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->O〇8O8〇008:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .param p1    # Lkotlin/jvm/functions/Function1;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "mPathListener"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇080:Lkotlin/jvm/functions/Function1;

    .line 10
    .line 11
    sget-object p1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 12
    .line 13
    new-instance v0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$mPaint$2;

    .line 14
    .line 15
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$mPaint$2;-><init>(Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;)V

    .line 16
    .line 17
    .line 18
    invoke-static {p1, v0}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇o00〇〇Oo:Lkotlin/Lazy;

    .line 23
    .line 24
    const/high16 p1, -0x1000000

    .line 25
    .line 26
    iput p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇888:I

    .line 27
    .line 28
    sget-object p1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;->NONE:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 29
    .line 30
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇8o8o〇:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 31
    .line 32
    const/high16 p1, 0x41400000    # 12.0f

    .line 33
    .line 34
    iput p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O8o08O:F

    .line 35
    .line 36
    new-instance p1, Landroid/graphics/Path;

    .line 37
    .line 38
    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    .line 39
    .line 40
    .line 41
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 42
    .line 43
    new-instance p1, Ljava/util/ArrayList;

    .line 44
    .line 45
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .line 47
    .line 48
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇:Ljava/util/List;

    .line 49
    .line 50
    new-instance p1, Ljava/util/ArrayList;

    .line 51
    .line 52
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 53
    .line 54
    .line 55
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇808〇:Ljava/util/List;

    .line 56
    .line 57
    const/4 p1, 0x1

    .line 58
    iput-boolean p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O〇:Z

    .line 59
    .line 60
    const-string p1, "#19BCAA"

    .line 61
    .line 62
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    iput p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OoO8:I

    .line 67
    .line 68
    const-string p1, "#4D19BCAA"

    .line 69
    .line 70
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 71
    .line 72
    .line 73
    move-result p1

    .line 74
    iput p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->o800o8O:I

    .line 75
    .line 76
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    const v0, 0x7f080c8f

    .line 81
    .line 82
    .line 83
    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    invoke-static {p1}, Lcom/intsig/utils/ImageUtil;->o〇0(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O888o0o:Landroid/graphics/Bitmap;

    .line 92
    .line 93
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 94
    .line 95
    .line 96
    move-result-object p1

    .line 97
    const v0, 0x7f080c93

    .line 98
    .line 99
    .line 100
    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    invoke-static {p1}, Lcom/intsig/utils/ImageUtil;->o〇0(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->oo88o8O:Landroid/graphics/Bitmap;

    .line 109
    .line 110
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 111
    .line 112
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    const/16 v1, 0xa

    .line 117
    .line 118
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    int-to-float v0, v0

    .line 123
    iput v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇oo〇:F

    .line 124
    .line 125
    new-instance v0, Landroid/graphics/Matrix;

    .line 126
    .line 127
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 128
    .line 129
    .line 130
    iput-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->o〇O8〇〇o:Landroid/graphics/Matrix;

    .line 131
    .line 132
    const/4 v0, 0x2

    .line 133
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 134
    .line 135
    .line 136
    move-result-object p1

    .line 137
    invoke-static {p1, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 138
    .line 139
    .line 140
    move-result p1

    .line 141
    int-to-float p1, p1

    .line 142
    iput p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇00:F

    .line 143
    .line 144
    return-void
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private final OO0o〇〇()Landroid/graphics/Paint;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇o00〇〇Oo:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/graphics/Paint;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final OO0o〇〇〇〇0(Landroid/graphics/Matrix;Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇808〇:Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    check-cast v1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PointDrawerInfo;

    .line 27
    .line 28
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PointDrawerInfo;->o〇0(Landroid/graphics/Matrix;)Landroid/graphics/PointF;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->O8(Landroid/graphics/Matrix;)F

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    iget v3, v2, Landroid/graphics/PointF;->x:F

    .line 37
    .line 38
    iget v2, v2, Landroid/graphics/PointF;->y:F

    .line 39
    .line 40
    const/4 v4, 0x2

    .line 41
    int-to-float v4, v4

    .line 42
    div-float/2addr v1, v4

    .line 43
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 44
    .line 45
    .line 46
    move-result-object v4

    .line 47
    invoke-virtual {p2, v3, v2, v1, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 56
    .line 57
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 58
    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇:Ljava/util/List;

    .line 61
    .line 62
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 67
    .line 68
    .line 69
    move-result v1

    .line 70
    if-eqz v1, :cond_1

    .line 71
    .line 72
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    check-cast v1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 77
    .line 78
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;->oO80(Landroid/graphics/Matrix;)Landroid/graphics/Path;

    .line 79
    .line 80
    .line 81
    move-result-object v2

    .line 82
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 83
    .line 84
    .line 85
    move-result-object v3

    .line 86
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->O8(Landroid/graphics/Matrix;)F

    .line 87
    .line 88
    .line 89
    move-result v1

    .line 90
    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 91
    .line 92
    .line 93
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 94
    .line 95
    .line 96
    move-result-object v1

    .line 97
    invoke-virtual {p2, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 98
    .line 99
    .line 100
    goto :goto_1

    .line 101
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 102
    .line 103
    .line 104
    move-result-object p1

    .line 105
    iget v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O8o08O:F

    .line 106
    .line 107
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 108
    .line 109
    .line 110
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 111
    .line 112
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    invoke-virtual {p2, p1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 117
    .line 118
    .line 119
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private final OoO8(FF)Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇80〇808〇O:Landroid/graphics/RectF;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    const/4 v2, 0x1

    .line 7
    int-to-float v3, v2

    .line 8
    add-float v4, p1, v3

    .line 9
    .line 10
    add-float/2addr v3, p2

    .line 11
    invoke-virtual {v0, p1, p2, v4, v3}, Landroid/graphics/RectF;->contains(FFFF)Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-ne p1, v2, :cond_0

    .line 16
    .line 17
    const/4 v1, 0x1

    .line 18
    :cond_0
    return v1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final Oooo8o0〇(Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;)Landroid/graphics/RectF;
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->o〇O8〇〇o:Landroid/graphics/Matrix;

    .line 9
    .line 10
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;->oO80(Landroid/graphics/Matrix;)Landroid/graphics/Path;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    :goto_0
    if-eqz p1, :cond_1

    .line 17
    .line 18
    const/4 v1, 0x1

    .line 19
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 20
    .line 21
    .line 22
    :cond_1
    return-object v0
    .line 23
    .line 24
.end method

.method private final O〇8O8〇008(Landroid/graphics/Matrix;FF)V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PointDrawerInfo;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O8o08O:F

    .line 4
    .line 5
    new-instance v2, Landroid/graphics/Matrix;

    .line 6
    .line 7
    invoke-direct {v2, p1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 8
    .line 9
    .line 10
    invoke-direct {v0, v1, v2, p2, p3}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PointDrawerInfo;-><init>(FLandroid/graphics/Matrix;FF)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇808〇:Ljava/util/List;

    .line 14
    .line 15
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private final oO80(Landroid/graphics/Paint;Landroid/graphics/RectF;Landroid/graphics/Canvas;)V
    .locals 4

    .line 1
    if-nez p2, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-virtual {p3, p2, p1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O888o0o:Landroid/graphics/Bitmap;

    .line 8
    .line 9
    iget v1, p2, Landroid/graphics/RectF;->left:F

    .line 10
    .line 11
    iget v2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇oo〇:F

    .line 12
    .line 13
    sub-float/2addr v1, v2

    .line 14
    iget v3, p2, Landroid/graphics/RectF;->top:F

    .line 15
    .line 16
    sub-float/2addr v3, v2

    .line 17
    invoke-virtual {p3, v0, v1, v3, p1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->oo88o8O:Landroid/graphics/Bitmap;

    .line 21
    .line 22
    iget v1, p2, Landroid/graphics/RectF;->right:F

    .line 23
    .line 24
    iget v2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇oo〇:F

    .line 25
    .line 26
    sub-float/2addr v1, v2

    .line 27
    iget v3, p2, Landroid/graphics/RectF;->bottom:F

    .line 28
    .line 29
    sub-float/2addr v3, v2

    .line 30
    invoke-virtual {p3, v0, v1, v3, p1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 31
    .line 32
    .line 33
    new-instance p1, Landroid/graphics/RectF;

    .line 34
    .line 35
    iget p3, p2, Landroid/graphics/RectF;->left:F

    .line 36
    .line 37
    iget v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇oo〇:F

    .line 38
    .line 39
    sub-float v1, p3, v0

    .line 40
    .line 41
    iget v2, p2, Landroid/graphics/RectF;->top:F

    .line 42
    .line 43
    sub-float/2addr v2, v0

    .line 44
    sub-float/2addr p3, v0

    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O888o0o:Landroid/graphics/Bitmap;

    .line 46
    .line 47
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    int-to-float v0, v0

    .line 52
    add-float/2addr p3, v0

    .line 53
    iget v0, p2, Landroid/graphics/RectF;->top:F

    .line 54
    .line 55
    iget v3, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇oo〇:F

    .line 56
    .line 57
    sub-float/2addr v0, v3

    .line 58
    iget-object v3, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O888o0o:Landroid/graphics/Bitmap;

    .line 59
    .line 60
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    int-to-float v3, v3

    .line 65
    add-float/2addr v0, v3

    .line 66
    invoke-direct {p1, v1, v2, p3, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 67
    .line 68
    .line 69
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->oO80:Landroid/graphics/RectF;

    .line 70
    .line 71
    new-instance p1, Landroid/graphics/RectF;

    .line 72
    .line 73
    iget p3, p2, Landroid/graphics/RectF;->right:F

    .line 74
    .line 75
    iget v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇oo〇:F

    .line 76
    .line 77
    sub-float v1, p3, v0

    .line 78
    .line 79
    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    .line 80
    .line 81
    sub-float/2addr v2, v0

    .line 82
    sub-float/2addr p3, v0

    .line 83
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->oo88o8O:Landroid/graphics/Bitmap;

    .line 84
    .line 85
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    int-to-float v0, v0

    .line 90
    add-float/2addr p3, v0

    .line 91
    iget p2, p2, Landroid/graphics/RectF;->bottom:F

    .line 92
    .line 93
    iget v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇oo〇:F

    .line 94
    .line 95
    sub-float/2addr p2, v0

    .line 96
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->oo88o8O:Landroid/graphics/Bitmap;

    .line 97
    .line 98
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 99
    .line 100
    .line 101
    move-result v0

    .line 102
    int-to-float v0, v0

    .line 103
    add-float/2addr p2, v0

    .line 104
    invoke-direct {p1, v1, v2, p3, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 105
    .line 106
    .line 107
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇80〇808〇O:Landroid/graphics/RectF;

    .line 108
    .line 109
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private final oo88o8O(IILandroid/graphics/RectF;)V
    .locals 2

    .line 1
    iget v0, p3, Landroid/graphics/RectF;->left:F

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    iput v0, p3, Landroid/graphics/RectF;->left:F

    .line 9
    .line 10
    iget v0, p3, Landroid/graphics/RectF;->top:F

    .line 11
    .line 12
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    iput v0, p3, Landroid/graphics/RectF;->top:F

    .line 17
    .line 18
    int-to-float p1, p1

    .line 19
    iget v0, p3, Landroid/graphics/RectF;->right:F

    .line 20
    .line 21
    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    iput p1, p3, Landroid/graphics/RectF;->right:F

    .line 26
    .line 27
    int-to-float p1, p2

    .line 28
    iget p2, p3, Landroid/graphics/RectF;->bottom:F

    .line 29
    .line 30
    invoke-static {p1, p2}, Ljava/lang/Math;->min(FF)F

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    iput p1, p3, Landroid/graphics/RectF;->bottom:F

    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private final o〇0(Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;)V
    .locals 5

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇:Ljava/util/List;

    .line 10
    .line 11
    check-cast v1, Ljava/util/Collection;

    .line 12
    .line 13
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 14
    .line 15
    .line 16
    new-instance v1, Landroid/graphics/RectF;

    .line 17
    .line 18
    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;->o〇0()Landroid/graphics/Path;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    const/4 v2, 0x1

    .line 26
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_2

    .line 38
    .line 39
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    check-cast v0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 44
    .line 45
    new-instance v3, Landroid/graphics/RectF;

    .line 46
    .line 47
    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;->o〇0()Landroid/graphics/Path;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    invoke-virtual {v4, v3, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 55
    .line 56
    .line 57
    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    if-eqz v3, :cond_1

    .line 62
    .line 63
    iget-object v3, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇:Ljava/util/List;

    .line 64
    .line 65
    invoke-interface {v3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 66
    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_2
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private final o〇O8〇〇o(Landroid/graphics/Rect;)[I
    .locals 5

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    new-array v0, v0, [I

    .line 4
    .line 5
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    aput v1, v0, v2

    .line 9
    .line 10
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 11
    .line 12
    const/4 v3, 0x1

    .line 13
    aput v2, v0, v3

    .line 14
    .line 15
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 16
    .line 17
    const/4 v4, 0x2

    .line 18
    aput v3, v0, v4

    .line 19
    .line 20
    const/4 v4, 0x3

    .line 21
    aput v2, v0, v4

    .line 22
    .line 23
    const/4 v2, 0x4

    .line 24
    aput v3, v0, v2

    .line 25
    .line 26
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 27
    .line 28
    const/4 v2, 0x5

    .line 29
    aput p1, v0, v2

    .line 30
    .line 31
    const/4 v2, 0x6

    .line 32
    aput v1, v0, v2

    .line 33
    .line 34
    const/4 v1, 0x7

    .line 35
    aput p1, v0, v1

    .line 36
    .line 37
    return-object v0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private final 〇00(Landroid/graphics/Matrix;)V
    .locals 4

    .line 1
    new-instance v0, Landroid/graphics/Path;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    .line 6
    .line 7
    .line 8
    new-instance v1, Landroid/graphics/Matrix;

    .line 9
    .line 10
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 14
    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇:Ljava/util/List;

    .line 17
    .line 18
    new-instance v2, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 19
    .line 20
    iget-boolean v3, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O〇:Z

    .line 21
    .line 22
    if-eqz v3, :cond_0

    .line 23
    .line 24
    iget v3, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇00:F

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    iget v3, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O8o08O:F

    .line 28
    .line 29
    :goto_0
    invoke-direct {v2, v3, v1, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;-><init>(FLandroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 30
    .line 31
    .line 32
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O8o08O:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final 〇0〇O0088o(FF)Z
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇:Ljava/util/List;

    .line 2
    .line 3
    check-cast v0, Ljava/lang/Iterable;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    if-eqz v2, :cond_1

    .line 15
    .line 16
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    check-cast v2, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 21
    .line 22
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇(Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;)Landroid/graphics/RectF;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    const/4 v4, 0x1

    .line 27
    int-to-float v5, v4

    .line 28
    add-float v6, p1, v5

    .line 29
    .line 30
    add-float/2addr v5, p2

    .line 31
    invoke-virtual {v3, p1, p2, v6, v5}, Landroid/graphics/RectF;->contains(FFFF)Z

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    if-eqz v3, :cond_0

    .line 36
    .line 37
    new-instance v1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 38
    .line 39
    invoke-virtual {v2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;->〇〇888()F

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    invoke-virtual {v2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->〇o〇()Landroid/graphics/Matrix;

    .line 44
    .line 45
    .line 46
    move-result-object v5

    .line 47
    invoke-virtual {v2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;->o〇0()Landroid/graphics/Path;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    invoke-direct {v1, v3, v5, v2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;-><init>(FLandroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 52
    .line 53
    .line 54
    iput-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 55
    .line 56
    const/4 v1, 0x1

    .line 57
    goto :goto_0

    .line 58
    :cond_1
    return v1
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private final 〇80〇808〇O(Landroid/graphics/Matrix;Landroid/graphics/Canvas;)V
    .locals 6

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/graphics/Matrix;

    .line 7
    .line 8
    invoke-direct {v1, p1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 9
    .line 10
    .line 11
    iput-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->o〇O8〇〇o:Landroid/graphics/Matrix;

    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    sget-object v2, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    .line 18
    .line 19
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 20
    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇:Ljava/util/List;

    .line 23
    .line 24
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    const/4 v3, 0x1

    .line 33
    if-eqz v2, :cond_0

    .line 34
    .line 35
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    check-cast v2, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 40
    .line 41
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 42
    .line 43
    .line 44
    move-result-object v4

    .line 45
    iget v5, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇888:I

    .line 46
    .line 47
    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    .line 49
    .line 50
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    .line 55
    .line 56
    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v2, p1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;->oO80(Landroid/graphics/Matrix;)Landroid/graphics/Path;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    invoke-virtual {v2, v0, v3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 64
    .line 65
    .line 66
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    iget v3, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇00:F

    .line 71
    .line 72
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 73
    .line 74
    .line 75
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 76
    .line 77
    .line 78
    move-result-object v2

    .line 79
    invoke-virtual {p2, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇:Ljava/util/List;

    .line 84
    .line 85
    check-cast v1, Ljava/util/Collection;

    .line 86
    .line 87
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    .line 88
    .line 89
    .line 90
    move-result v1

    .line 91
    xor-int/2addr v1, v3

    .line 92
    if-eqz v1, :cond_3

    .line 93
    .line 94
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇8o8o〇:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 95
    .line 96
    sget-object v2, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;->NONE:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 97
    .line 98
    if-eq v1, v2, :cond_1

    .line 99
    .line 100
    sget-object v4, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;->SELECTING:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 101
    .line 102
    if-ne v1, v4, :cond_3

    .line 103
    .line 104
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 105
    .line 106
    .line 107
    move-result-object v1

    .line 108
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 109
    .line 110
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 111
    .line 112
    .line 113
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 114
    .line 115
    .line 116
    move-result-object v1

    .line 117
    iget v4, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇00:F

    .line 118
    .line 119
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 120
    .line 121
    .line 122
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 123
    .line 124
    .line 125
    move-result-object v1

    .line 126
    iget v4, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OoO8:I

    .line 127
    .line 128
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 129
    .line 130
    .line 131
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇:Ljava/util/List;

    .line 132
    .line 133
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->〇O〇80o08O(Ljava/util/List;)Ljava/lang/Object;

    .line 134
    .line 135
    .line 136
    move-result-object v1

    .line 137
    check-cast v1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 138
    .line 139
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;->oO80(Landroid/graphics/Matrix;)Landroid/graphics/Path;

    .line 140
    .line 141
    .line 142
    move-result-object p1

    .line 143
    invoke-virtual {p1, v0, v3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 144
    .line 145
    .line 146
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇8o8o〇:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 147
    .line 148
    sget-object v4, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;->SELECTING:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 149
    .line 150
    if-ne p1, v4, :cond_2

    .line 151
    .line 152
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 153
    .line 154
    .line 155
    move-result-object p1

    .line 156
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 157
    .line 158
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇(Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;)Landroid/graphics/RectF;

    .line 159
    .line 160
    .line 161
    move-result-object v0

    .line 162
    invoke-direct {p0, p1, v0, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->oO80(Landroid/graphics/Paint;Landroid/graphics/RectF;Landroid/graphics/Canvas;)V

    .line 163
    .line 164
    .line 165
    goto :goto_1

    .line 166
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 167
    .line 168
    .line 169
    move-result-object p1

    .line 170
    invoke-direct {p0, p1, v0, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->oO80(Landroid/graphics/Paint;Landroid/graphics/RectF;Landroid/graphics/Canvas;)V

    .line 171
    .line 172
    .line 173
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇8o8o〇:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 174
    .line 175
    if-ne p1, v2, :cond_3

    .line 176
    .line 177
    new-instance p1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 178
    .line 179
    invoke-virtual {v1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;->〇〇888()F

    .line 180
    .line 181
    .line 182
    move-result v0

    .line 183
    invoke-virtual {v1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->〇o〇()Landroid/graphics/Matrix;

    .line 184
    .line 185
    .line 186
    move-result-object v2

    .line 187
    invoke-virtual {v1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;->o〇0()Landroid/graphics/Path;

    .line 188
    .line 189
    .line 190
    move-result-object v1

    .line 191
    invoke-direct {p1, v0, v2, v1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;-><init>(FLandroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 192
    .line 193
    .line 194
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 195
    .line 196
    :cond_3
    iget-boolean p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O00:Z

    .line 197
    .line 198
    if-eqz p1, :cond_4

    .line 199
    .line 200
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 201
    .line 202
    .line 203
    move-result-object p1

    .line 204
    iget v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇888:I

    .line 205
    .line 206
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 207
    .line 208
    .line 209
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 210
    .line 211
    .line 212
    move-result-object p1

    .line 213
    sget-object v0, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    .line 214
    .line 215
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 216
    .line 217
    .line 218
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 219
    .line 220
    .line 221
    move-result-object p1

    .line 222
    invoke-virtual {p1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 223
    .line 224
    .line 225
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 226
    .line 227
    .line 228
    move-result-object p1

    .line 229
    sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    .line 230
    .line 231
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 232
    .line 233
    .line 234
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 235
    .line 236
    .line 237
    move-result-object p1

    .line 238
    iget v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇00:F

    .line 239
    .line 240
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 241
    .line 242
    .line 243
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 244
    .line 245
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 246
    .line 247
    .line 248
    move-result-object v0

    .line 249
    invoke-virtual {p2, p1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 250
    .line 251
    .line 252
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 253
    .line 254
    .line 255
    move-result-object p1

    .line 256
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 257
    .line 258
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 259
    .line 260
    .line 261
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 262
    .line 263
    .line 264
    move-result-object p1

    .line 265
    iget v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OoO8:I

    .line 266
    .line 267
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 268
    .line 269
    .line 270
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 271
    .line 272
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 273
    .line 274
    .line 275
    move-result-object v0

    .line 276
    invoke-virtual {p2, p1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 277
    .line 278
    .line 279
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 280
    .line 281
    .line 282
    move-result-object p1

    .line 283
    iget p2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇00:F

    .line 284
    .line 285
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 286
    .line 287
    .line 288
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 289
    .line 290
    .line 291
    move-result-object p1

    .line 292
    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 293
    .line 294
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 295
    .line 296
    .line 297
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 298
    .line 299
    .line 300
    move-result-object p1

    .line 301
    sget-object p2, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    .line 302
    .line 303
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 304
    .line 305
    .line 306
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 307
    .line 308
    .line 309
    move-result-object p1

    .line 310
    iget p2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇888:I

    .line 311
    .line 312
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 313
    .line 314
    .line 315
    return-void
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private final 〇8o8o〇(FFFF)V
    .locals 6

    .line 1
    cmpg-float v0, p1, p3

    .line 2
    .line 3
    if-gez v0, :cond_1

    .line 4
    .line 5
    cmpg-float v0, p2, p4

    .line 6
    .line 7
    if-gez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 10
    .line 11
    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 12
    .line 13
    move v1, p1

    .line 14
    move v2, p2

    .line 15
    move v3, p3

    .line 16
    move v4, p4

    .line 17
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 22
    .line 23
    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 24
    .line 25
    move v1, p1

    .line 26
    move v2, p4

    .line 27
    move v3, p3

    .line 28
    move v4, p2

    .line 29
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    cmpg-float v0, p2, p4

    .line 34
    .line 35
    if-gez v0, :cond_2

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 38
    .line 39
    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 40
    .line 41
    move v1, p3

    .line 42
    move v2, p2

    .line 43
    move v3, p1

    .line 44
    move v4, p4

    .line 45
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 50
    .line 51
    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 52
    .line 53
    move v1, p3

    .line 54
    move v2, p4

    .line 55
    move v3, p1

    .line 56
    move v4, p2

    .line 57
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 58
    .line 59
    .line 60
    :goto_0
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private final 〇O00(FF)Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->oO80:Landroid/graphics/RectF;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    const/4 v2, 0x1

    .line 7
    int-to-float v3, v2

    .line 8
    add-float v4, p1, v3

    .line 9
    .line 10
    add-float/2addr v3, p2

    .line 11
    invoke-virtual {v0, p1, p2, v4, v3}, Landroid/graphics/RectF;->contains(FFFF)Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-ne p1, v2, :cond_0

    .line 16
    .line 17
    const/4 v1, 0x1

    .line 18
    :cond_0
    return v1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final 〇O〇(Landroid/graphics/Path;)Z
    .locals 3

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    const/high16 v2, 0x40a00000    # 5.0f

    .line 15
    .line 16
    cmpg-float p1, p1, v2

    .line 17
    .line 18
    if-gtz p1, :cond_0

    .line 19
    .line 20
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    cmpg-float p1, p1, v2

    .line 25
    .line 26
    if-gtz p1, :cond_0

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    const/4 v1, 0x0

    .line 30
    :goto_0
    return v1
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private final 〇o00〇〇Oo(Landroid/view/View;Landroid/graphics/Matrix;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O〇(Landroid/graphics/Path;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object p2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 10
    .line 11
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇00(Landroid/graphics/Matrix;)V

    .line 19
    .line 20
    .line 21
    iget-object p2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 22
    .line 23
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇080:Lkotlin/jvm/functions/Function1;

    .line 30
    .line 31
    sget-object p2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 32
    .line 33
    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    sget-object p1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;->NONE:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 37
    .line 38
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇8o8o〇:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 39
    .line 40
    :goto_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final 〇o〇(Landroid/view/View;Landroid/graphics/Matrix;FF)V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇8O0〇8:Z

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "actionUpGestureMode isDrag: "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "SmartErasePainter"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 26
    .line 27
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O〇(Landroid/graphics/Path;)Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-eqz v0, :cond_0

    .line 32
    .line 33
    iget-boolean v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇8O0〇8:Z

    .line 34
    .line 35
    if-nez v0, :cond_1

    .line 36
    .line 37
    invoke-direct {p0, p2, p3, p4}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->O〇8O8〇008(Landroid/graphics/Matrix;FF)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 41
    .line 42
    .line 43
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇080:Lkotlin/jvm/functions/Function1;

    .line 44
    .line 45
    sget-object p2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 46
    .line 47
    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_0
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇00(Landroid/graphics/Matrix;)V

    .line 52
    .line 53
    .line 54
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇080:Lkotlin/jvm/functions/Function1;

    .line 55
    .line 56
    sget-object p2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 57
    .line 58
    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    :cond_1
    :goto_0
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private final 〇〇808〇(FF)Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇o〇:F

    .line 2
    .line 3
    sub-float/2addr p1, v0

    .line 4
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    iget v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->O8:F

    .line 9
    .line 10
    sub-float/2addr p2, v0

    .line 11
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    iget-boolean v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O〇:Z

    .line 16
    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    const/16 v0, 0xf

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 v0, 0x5

    .line 23
    :goto_0
    int-to-float v0, v0

    .line 24
    cmpg-float p1, p1, v0

    .line 25
    .line 26
    if-gtz p1, :cond_1

    .line 27
    .line 28
    cmpg-float p1, p2, v0

    .line 29
    .line 30
    if-gtz p1, :cond_1

    .line 31
    .line 32
    const/4 p1, 0x1

    .line 33
    goto :goto_1

    .line 34
    :cond_1
    const/4 p1, 0x0

    .line 35
    :goto_1
    return p1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final 〇〇888()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇0〇O0088o:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 7
    .line 8
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->o〇0(Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    iput-boolean v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇0〇O0088o:Z

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇〇8O0〇8(FF)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇(Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;)Landroid/graphics/RectF;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x1

    .line 8
    int-to-float v1, v1

    .line 9
    add-float v2, p1, v1

    .line 10
    .line 11
    add-float/2addr v1, p2

    .line 12
    invoke-virtual {v0, p1, p2, v2, v1}, Landroid/graphics/RectF;->contains(FFFF)Z

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    return p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public final O8(Landroid/view/View;Landroid/graphics/Matrix;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Matrix;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "scaleMatrix"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    iput-boolean v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇8O0〇8:Z

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 15
    .line 16
    invoke-virtual {v0}, Landroid/graphics/Path;->isEmpty()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_0

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 23
    .line 24
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O〇(Landroid/graphics/Path;)Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-nez v0, :cond_0

    .line 29
    .line 30
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇00(Landroid/graphics/Matrix;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 34
    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 37
    .line 38
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    .line 39
    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇080:Lkotlin/jvm/functions/Function1;

    .line 42
    .line 43
    sget-object p2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 44
    .line 45
    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    .line 47
    .line 48
    :cond_0
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public final O8ooOoo〇(Landroid/graphics/Bitmap;Landroid/graphics/PointF;)V
    .locals 4
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/PointF;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "bitmap"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "translateXY"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Landroid/graphics/Canvas;

    .line 12
    .line 13
    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 14
    .line 15
    .line 16
    const/high16 p1, -0x1000000

    .line 17
    .line 18
    invoke-virtual {v0, p1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 19
    .line 20
    .line 21
    new-instance p1, Landroid/graphics/Paint;

    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-direct {p1, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 28
    .line 29
    .line 30
    const/4 v1, -0x1

    .line 31
    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 32
    .line 33
    .line 34
    iget v1, p2, Landroid/graphics/PointF;->x:F

    .line 35
    .line 36
    neg-float v1, v1

    .line 37
    iget p2, p2, Landroid/graphics/PointF;->y:F

    .line 38
    .line 39
    neg-float p2, p2

    .line 40
    invoke-virtual {v0, v1, p2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 41
    .line 42
    .line 43
    iget-object p2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇:Ljava/util/List;

    .line 44
    .line 45
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 46
    .line 47
    .line 48
    move-result-object p2

    .line 49
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    if-eqz v1, :cond_0

    .line 54
    .line 55
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    check-cast v1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 60
    .line 61
    invoke-virtual {v1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;->Oo08()Landroid/graphics/Path;

    .line 62
    .line 63
    .line 64
    move-result-object v2

    .line 65
    invoke-virtual {v1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->〇080()F

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0, v2, p1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 73
    .line 74
    .line 75
    goto :goto_0

    .line 76
    :cond_0
    sget-object p2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 77
    .line 78
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 79
    .line 80
    .line 81
    iget-object p2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇808〇:Ljava/util/List;

    .line 82
    .line 83
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 84
    .line 85
    .line 86
    move-result-object p2

    .line 87
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 88
    .line 89
    .line 90
    move-result v1

    .line 91
    if-eqz v1, :cond_1

    .line 92
    .line 93
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 94
    .line 95
    .line 96
    move-result-object v1

    .line 97
    check-cast v1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PointDrawerInfo;

    .line 98
    .line 99
    invoke-virtual {v1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PointDrawerInfo;->Oo08()Landroid/graphics/PointF;

    .line 100
    .line 101
    .line 102
    move-result-object v2

    .line 103
    invoke-virtual {v1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->〇080()F

    .line 104
    .line 105
    .line 106
    move-result v1

    .line 107
    const/4 v3, 0x2

    .line 108
    int-to-float v3, v3

    .line 109
    div-float/2addr v1, v3

    .line 110
    iget v3, v2, Landroid/graphics/PointF;->x:F

    .line 111
    .line 112
    iget v2, v2, Landroid/graphics/PointF;->y:F

    .line 113
    .line 114
    invoke-virtual {v0, v3, v2, v1, p1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 115
    .line 116
    .line 117
    goto :goto_1

    .line 118
    :cond_1
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public final Oo08()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇808〇:Ljava/util/List;

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o800o8O(Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/RectF;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Matrix;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "displayRectF"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "transformMatrix"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "canvas"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p3}, Landroid/graphics/Canvas;->save()I

    .line 17
    .line 18
    .line 19
    invoke-virtual {p3, p1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 20
    .line 21
    .line 22
    iget-boolean p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O〇:Z

    .line 23
    .line 24
    if-eqz p1, :cond_0

    .line 25
    .line 26
    invoke-direct {p0, p2, p3}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇80〇808〇O(Landroid/graphics/Matrix;Landroid/graphics/Canvas;)V

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇〇〇0(Landroid/graphics/Matrix;Landroid/graphics/Canvas;)V

    .line 31
    .line 32
    .line 33
    :goto_0
    invoke-virtual {p3}, Landroid/graphics/Canvas;->restore()V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public final o〇〇0〇(F)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O8o08O:F

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇0000OOO(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇O888o0o(Landroid/view/View;Landroid/view/MotionEvent;Landroid/graphics/RectF;Landroid/graphics/Matrix;)V
    .locals 8
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/MotionEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/RectF;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Matrix;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "event"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "displayRectF"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "scaleMatrix"

    .line 17
    .line 18
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    and-int/lit16 v0, v0, 0xff

    .line 26
    .line 27
    new-instance v1, Landroid/graphics/RectF;

    .line 28
    .line 29
    invoke-direct {v1, p3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    .line 33
    .line 34
    .line 35
    move-result p3

    .line 36
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    .line 37
    .line 38
    .line 39
    move-result p2

    .line 40
    const/4 v2, 0x0

    .line 41
    if-eqz v0, :cond_9

    .line 42
    .line 43
    const/4 v3, 0x1

    .line 44
    if-eq v0, v3, :cond_5

    .line 45
    .line 46
    const/4 v4, 0x2

    .line 47
    if-eq v0, v4, :cond_0

    .line 48
    .line 49
    const/4 v1, 0x3

    .line 50
    if-eq v0, v1, :cond_5

    .line 51
    .line 52
    goto/16 :goto_2

    .line 53
    .line 54
    :cond_0
    iput-boolean v3, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O00:Z

    .line 55
    .line 56
    iget-object p4, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 57
    .line 58
    invoke-virtual {p4}, Landroid/graphics/Path;->isEmpty()Z

    .line 59
    .line 60
    .line 61
    move-result p4

    .line 62
    if-eqz p4, :cond_1

    .line 63
    .line 64
    return-void

    .line 65
    :cond_1
    invoke-direct {p0, p3, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇808〇(FF)Z

    .line 66
    .line 67
    .line 68
    move-result p4

    .line 69
    if-nez p4, :cond_f

    .line 70
    .line 71
    invoke-virtual {v1, p3, p2}, Landroid/graphics/RectF;->contains(FF)Z

    .line 72
    .line 73
    .line 74
    move-result p4

    .line 75
    if-eqz p4, :cond_f

    .line 76
    .line 77
    iget-boolean p4, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O〇:Z

    .line 78
    .line 79
    if-eqz p4, :cond_4

    .line 80
    .line 81
    iget-object p4, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 82
    .line 83
    invoke-virtual {p4}, Landroid/graphics/Path;->reset()V

    .line 84
    .line 85
    .line 86
    iget-object p4, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇8o8o〇:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 87
    .line 88
    sget-object v0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$WhenMappings;->〇080:[I

    .line 89
    .line 90
    invoke-virtual {p4}, Ljava/lang/Enum;->ordinal()I

    .line 91
    .line 92
    .line 93
    move-result p4

    .line 94
    aget p4, v0, p4

    .line 95
    .line 96
    if-eq p4, v3, :cond_3

    .line 97
    .line 98
    if-eq p4, v4, :cond_2

    .line 99
    .line 100
    iget p4, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oo08:F

    .line 101
    .line 102
    iget v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->o〇0:F

    .line 103
    .line 104
    invoke-direct {p0, p4, v0, p3, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇8o8o〇(FFFF)V

    .line 105
    .line 106
    .line 107
    goto :goto_0

    .line 108
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇888()V

    .line 109
    .line 110
    .line 111
    iget-object p4, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 112
    .line 113
    invoke-direct {p0, p4}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇(Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;)Landroid/graphics/RectF;

    .line 114
    .line 115
    .line 116
    move-result-object p4

    .line 117
    iget v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oo08:F

    .line 118
    .line 119
    sub-float v0, p3, v0

    .line 120
    .line 121
    iget v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->o〇0:F

    .line 122
    .line 123
    sub-float v1, p2, v1

    .line 124
    .line 125
    iget-object v2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 126
    .line 127
    iget v3, p4, Landroid/graphics/RectF;->left:F

    .line 128
    .line 129
    add-float/2addr v3, v0

    .line 130
    iget v4, p4, Landroid/graphics/RectF;->top:F

    .line 131
    .line 132
    add-float/2addr v4, v1

    .line 133
    iget v5, p4, Landroid/graphics/RectF;->right:F

    .line 134
    .line 135
    add-float/2addr v5, v0

    .line 136
    iget p4, p4, Landroid/graphics/RectF;->bottom:F

    .line 137
    .line 138
    add-float v6, p4, v1

    .line 139
    .line 140
    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 141
    .line 142
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 143
    .line 144
    .line 145
    goto :goto_0

    .line 146
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇888()V

    .line 147
    .line 148
    .line 149
    iget-object p4, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 150
    .line 151
    invoke-direct {p0, p4}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇(Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;)Landroid/graphics/RectF;

    .line 152
    .line 153
    .line 154
    move-result-object p4

    .line 155
    iget v0, p4, Landroid/graphics/RectF;->left:F

    .line 156
    .line 157
    iget p4, p4, Landroid/graphics/RectF;->top:F

    .line 158
    .line 159
    invoke-direct {p0, v0, p4, p3, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇8o8o〇(FFFF)V

    .line 160
    .line 161
    .line 162
    goto :goto_0

    .line 163
    :cond_4
    iget-object p4, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 164
    .line 165
    iget v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇o〇:F

    .line 166
    .line 167
    iget v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->O8:F

    .line 168
    .line 169
    add-float v2, p3, v0

    .line 170
    .line 171
    int-to-float v3, v4

    .line 172
    div-float/2addr v2, v3

    .line 173
    add-float v4, p2, v1

    .line 174
    .line 175
    div-float/2addr v4, v3

    .line 176
    invoke-virtual {p4, v0, v1, v2, v4}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 177
    .line 178
    .line 179
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 180
    .line 181
    .line 182
    iput p3, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇o〇:F

    .line 183
    .line 184
    iput p2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->O8:F

    .line 185
    .line 186
    goto/16 :goto_2

    .line 187
    .line 188
    :cond_5
    iput-boolean v2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O00:Z

    .line 189
    .line 190
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 191
    .line 192
    invoke-virtual {v0}, Landroid/graphics/Path;->isEmpty()Z

    .line 193
    .line 194
    .line 195
    move-result v0

    .line 196
    if-nez v0, :cond_7

    .line 197
    .line 198
    iget-boolean v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O〇:Z

    .line 199
    .line 200
    if-eqz v0, :cond_6

    .line 201
    .line 202
    invoke-direct {p0, p1, p4}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇o00〇〇Oo(Landroid/view/View;Landroid/graphics/Matrix;)V

    .line 203
    .line 204
    .line 205
    goto :goto_1

    .line 206
    :cond_6
    invoke-direct {p0, p1, p4, p3, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇o〇(Landroid/view/View;Landroid/graphics/Matrix;FF)V

    .line 207
    .line 208
    .line 209
    goto :goto_1

    .line 210
    :cond_7
    iget-object p2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇8o8o〇:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 211
    .line 212
    sget-object p3, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;->SELECTING:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 213
    .line 214
    if-ne p2, p3, :cond_8

    .line 215
    .line 216
    iget-boolean p2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O〇:Z

    .line 217
    .line 218
    if-eqz p2, :cond_8

    .line 219
    .line 220
    invoke-direct {p0, p1, p4}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇o00〇〇Oo(Landroid/view/View;Landroid/graphics/Matrix;)V

    .line 221
    .line 222
    .line 223
    :cond_8
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 224
    .line 225
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    .line 226
    .line 227
    .line 228
    iput-boolean v2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇8O0〇8:Z

    .line 229
    .line 230
    iput-boolean v2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇0〇O0088o:Z

    .line 231
    .line 232
    goto/16 :goto_2

    .line 233
    .line 234
    :cond_9
    iput-boolean v2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O00:Z

    .line 235
    .line 236
    iput-boolean v2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇8O0〇8:Z

    .line 237
    .line 238
    iput p3, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇o〇:F

    .line 239
    .line 240
    iput p2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->O8:F

    .line 241
    .line 242
    iput p3, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oo08:F

    .line 243
    .line 244
    iput p2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->o〇0:F

    .line 245
    .line 246
    invoke-virtual {v1, p3, p2}, Landroid/graphics/RectF;->contains(FF)Z

    .line 247
    .line 248
    .line 249
    move-result p1

    .line 250
    if-eqz p1, :cond_a

    .line 251
    .line 252
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇:Landroid/graphics/Path;

    .line 253
    .line 254
    invoke-virtual {p1, p3, p2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 255
    .line 256
    .line 257
    :cond_a
    iget-boolean p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O〇:Z

    .line 258
    .line 259
    if-eqz p1, :cond_f

    .line 260
    .line 261
    invoke-direct {p0, p3, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O00(FF)Z

    .line 262
    .line 263
    .line 264
    move-result p1

    .line 265
    const/4 p4, 0x0

    .line 266
    const-string v0, "SmartErasePainter"

    .line 267
    .line 268
    if-eqz p1, :cond_b

    .line 269
    .line 270
    const-string p1, "inCloseArea"

    .line 271
    .line 272
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    .line 274
    .line 275
    sget-object p1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;->DELETING:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 276
    .line 277
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇8o8o〇:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 278
    .line 279
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 280
    .line 281
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->o〇0(Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;)V

    .line 282
    .line 283
    .line 284
    iput-object p4, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 285
    .line 286
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇:Ljava/util/List;

    .line 287
    .line 288
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 289
    .line 290
    .line 291
    move-result p1

    .line 292
    if-nez p1, :cond_f

    .line 293
    .line 294
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇080:Lkotlin/jvm/functions/Function1;

    .line 295
    .line 296
    sget-object p2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 297
    .line 298
    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    .line 300
    .line 301
    goto :goto_2

    .line 302
    :cond_b
    invoke-direct {p0, p3, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OoO8(FF)Z

    .line 303
    .line 304
    .line 305
    move-result p1

    .line 306
    if-eqz p1, :cond_c

    .line 307
    .line 308
    const-string p1, "inScaleArea"

    .line 309
    .line 310
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    .line 312
    .line 313
    sget-object p1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;->SCALING:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 314
    .line 315
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇8o8o〇:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 316
    .line 317
    goto :goto_2

    .line 318
    :cond_c
    invoke-direct {p0, p3, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇8O0〇8(FF)Z

    .line 319
    .line 320
    .line 321
    move-result p1

    .line 322
    if-eqz p1, :cond_d

    .line 323
    .line 324
    const-string p1, "inEditFrame"

    .line 325
    .line 326
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    .line 328
    .line 329
    sget-object p1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;->MOVING:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 330
    .line 331
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇8o8o〇:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 332
    .line 333
    goto :goto_2

    .line 334
    :cond_d
    invoke-direct {p0, p3, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇0〇O0088o(FF)Z

    .line 335
    .line 336
    .line 337
    move-result p1

    .line 338
    if-eqz p1, :cond_e

    .line 339
    .line 340
    sget-object p1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;->SELECTING:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 341
    .line 342
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇8o8o〇:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 343
    .line 344
    const-string p1, "inExistedFrame"

    .line 345
    .line 346
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    .line 348
    .line 349
    goto :goto_2

    .line 350
    :cond_e
    sget-object p1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;->BLANKING:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 351
    .line 352
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇8o8o〇:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$Companion$CurrentMode;

    .line 353
    .line 354
    iput-object p4, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 355
    .line 356
    const-string p1, "inBlankArea"

    .line 357
    .line 358
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    .line 360
    .line 361
    :cond_f
    :goto_2
    return-void
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
.end method

.method public final 〇O8o08O(IILandroid/graphics/Bitmap;Landroid/graphics/Matrix;)Ljava/util/List;
    .locals 5
    .param p3    # Landroid/graphics/Bitmap;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Matrix;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/graphics/Bitmap;",
            "Landroid/graphics/Matrix;",
            ")",
            "Ljava/util/List<",
            "[I>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "showBitmap"

    .line 2
    .line 3
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "baseMatrix"

    .line 7
    .line 8
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 14
    .line 15
    .line 16
    new-instance v1, Landroid/graphics/RectF;

    .line 17
    .line 18
    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 19
    .line 20
    .line 21
    new-instance v2, Landroid/graphics/Matrix;

    .line 22
    .line 23
    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p4, v2}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 27
    .line 28
    .line 29
    int-to-float p4, p1

    .line 30
    const/high16 v3, 0x3f800000    # 1.0f

    .line 31
    .line 32
    mul-float p4, p4, v3

    .line 33
    .line 34
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    .line 35
    .line 36
    .line 37
    move-result v4

    .line 38
    int-to-float v4, v4

    .line 39
    div-float/2addr p4, v4

    .line 40
    int-to-float v4, p2

    .line 41
    mul-float v4, v4, v3

    .line 42
    .line 43
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    .line 44
    .line 45
    .line 46
    move-result p3

    .line 47
    int-to-float p3, p3

    .line 48
    div-float/2addr v4, p3

    .line 49
    invoke-static {p4, v4}, Ljava/lang/Math;->max(FF)F

    .line 50
    .line 51
    .line 52
    move-result p3

    .line 53
    invoke-virtual {v2, p3, p3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 54
    .line 55
    .line 56
    iget-object p3, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇:Ljava/util/List;

    .line 57
    .line 58
    check-cast p3, Ljava/lang/Iterable;

    .line 59
    .line 60
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 61
    .line 62
    .line 63
    move-result-object p3

    .line 64
    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    .line 65
    .line 66
    .line 67
    move-result p4

    .line 68
    if-eqz p4, :cond_0

    .line 69
    .line 70
    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 71
    .line 72
    .line 73
    move-result-object p4

    .line 74
    check-cast p4, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 75
    .line 76
    invoke-virtual {p4}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;->Oo08()Landroid/graphics/Path;

    .line 77
    .line 78
    .line 79
    move-result-object v3

    .line 80
    invoke-virtual {p4}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->〇080()F

    .line 81
    .line 82
    .line 83
    move-result p4

    .line 84
    const/4 v4, 0x1

    .line 85
    invoke-virtual {v3, v1, v4}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 86
    .line 87
    .line 88
    neg-float p4, p4

    .line 89
    const/4 v3, 0x2

    .line 90
    int-to-float v3, v3

    .line 91
    div-float/2addr p4, v3

    .line 92
    invoke-virtual {v1, p4, p4}, Landroid/graphics/RectF;->inset(FF)V

    .line 93
    .line 94
    .line 95
    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 96
    .line 97
    .line 98
    invoke-direct {p0, p1, p2, v1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->oo88o8O(IILandroid/graphics/RectF;)V

    .line 99
    .line 100
    .line 101
    new-instance p4, Landroid/graphics/Rect;

    .line 102
    .line 103
    invoke-direct {p4}, Landroid/graphics/Rect;-><init>()V

    .line 104
    .line 105
    .line 106
    invoke-virtual {v1, p4}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 107
    .line 108
    .line 109
    invoke-direct {p0, p4}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->o〇O8〇〇o(Landroid/graphics/Rect;)[I

    .line 110
    .line 111
    .line 112
    move-result-object p4

    .line 113
    invoke-interface {v0, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    .line 115
    .line 116
    goto :goto_0

    .line 117
    :cond_0
    return-object v0
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public final 〇oOO8O8(I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->OO0o〇〇()Landroid/graphics/Paint;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 6
    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇O〇:Z

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    iget p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->o800o8O:I

    .line 13
    .line 14
    :cond_0
    iput p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇888:I

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇oo〇(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V
    .locals 2
    .param p1    # Landroid/graphics/Matrix;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Matrix;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .line 1
    const-string v0, "lastViewMatrix"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "currentViewMatrix"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇:Ljava/util/List;

    .line 12
    .line 13
    check-cast v0, Ljava/util/Collection;

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    xor-int/lit8 v0, v0, 0x1

    .line 20
    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->Oooo8o0〇:Ljava/util/List;

    .line 24
    .line 25
    check-cast v0, Ljava/lang/Iterable;

    .line 26
    .line 27
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    if-eqz v1, :cond_0

    .line 36
    .line 37
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    check-cast v1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;

    .line 42
    .line 43
    invoke-virtual {v1, p1, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PathDrawerInfo;->〇80〇808〇O(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇808〇:Ljava/util/List;

    .line 48
    .line 49
    check-cast v0, Ljava/util/Collection;

    .line 50
    .line 51
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    xor-int/lit8 v0, v0, 0x1

    .line 56
    .line 57
    if-eqz v0, :cond_1

    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;->〇〇808〇:Ljava/util/List;

    .line 60
    .line 61
    check-cast v0, Ljava/lang/Iterable;

    .line 62
    .line 63
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    if-eqz v1, :cond_1

    .line 72
    .line 73
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    check-cast v1, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PointDrawerInfo;

    .line 78
    .line 79
    invoke-virtual {v1, p1, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$PointDrawerInfo;->〇〇888(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V

    .line 80
    .line 81
    .line 82
    goto :goto_1

    .line 83
    :cond_1
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

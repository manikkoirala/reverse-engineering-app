.class public interface abstract Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;
.super Ljava/lang/Object;
.source "SmartEraseOperateView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SmartEraseOperateViewDelegate"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract O8(Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;)V
    .param p1    # Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract Oo08(Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;)V
    .param p1    # Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract 〇080(Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .param p1    # Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Bitmap;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/Bitmap;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract 〇o00〇〇Oo(Z)V
.end method

.method public abstract 〇o〇(Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;Ljava/util/List;)V
    .param p1    # Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;",
            "Ljava/util/List<",
            "[I>;)V"
        }
    .end annotation
.end method

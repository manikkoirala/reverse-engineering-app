.class public final Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "SmartEraseOperateView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;,
        Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080OO8〇0:Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/intsig/camscanner/smarterase/dialog/SmartEraseTipsDialog;

.field private OO:Landroid/graphics/Bitmap;

.field private final o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;

.field private 〇08O〇00〇o:Landroid/graphics/Bitmap;

.field private 〇OOo8〇0:Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇080OO8〇0:Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const p2, 0x7f0d073c

    .line 4
    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 5
    invoke-static {p2}, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    move-result-object p2

    const-string p3, "bind(view)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    const p3, 0x7f0601e3

    .line 6
    invoke-static {p1, p3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 7
    iget-object p1, p2, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->O8o08O8O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;

    new-instance p3, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$1;

    invoke-direct {p3, p0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$1;-><init>(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;)V

    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;->setMGestureListener(Lkotlin/jvm/functions/Function1;)V

    .line 8
    iget-object p1, p2, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->O8o08O8O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;

    const-string p3, "#8019BCAA"

    invoke-static {p3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;->setPenColor(I)V

    .line 9
    iget-object p1, p2, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->o〇00O:Landroid/widget/LinearLayout;

    const-string p3, "mBinding.llUndo"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "#4D000000"

    .line 10
    invoke-static {p3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    const/high16 v1, 0x41000000    # 8.0f

    .line 11
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    move-result v2

    int-to-float v2, v2

    .line 12
    new-instance v3, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v3}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    invoke-virtual {v3, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    invoke-virtual {p1, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 13
    iget-object p1, p2, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    const-string p2, "mBinding.ivCompare"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-static {p3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p2

    .line 15
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    move-result p3

    int-to-float p3, p3

    .line 16
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    invoke-virtual {v0, p3}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->OoO8()V

    const-string p1, "SmartEraseOperateView"

    const-string p2, "init:"

    .line 18
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 2
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final O8ooOoo〇(Landroid/widget/ImageView;Z)V
    .locals 0

    .line 1
    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 2
    .line 3
    .line 4
    if-eqz p2, :cond_0

    .line 5
    .line 6
    const/high16 p2, 0x3f800000    # 1.0f

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const p2, 0x3e99999a    # 0.3f

    .line 10
    .line 11
    .line 12
    :goto_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic OO0o〇〇(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->OO:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;)Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇OOo8〇0:Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic Oo08(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇O888o0o(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final OoO8()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->OO:Landroid/widget/ImageView;

    .line 4
    .line 5
    new-instance v1, Lo8oo0OOO/O8;

    .line 6
    .line 7
    invoke-direct {v1, p0}, Lo8oo0OOO/O8;-><init>(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 14
    .line 15
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 16
    .line 17
    new-instance v1, Lo8oo0OOO/Oo08;

    .line 18
    .line 19
    invoke-direct {v1, p0}, Lo8oo0OOO/Oo08;-><init>(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 26
    .line 27
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 28
    .line 29
    new-instance v1, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$initListener$3;

    .line 30
    .line 31
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$initListener$3;-><init>(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static final synthetic Oooo8o0〇(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇08O〇00〇o:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final O〇8O8〇008(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$removeImageRotation$2;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p1, v2}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$removeImageRotation$2;-><init>(Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-static {v0, v1, p2}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    if-ne p1, p2, :cond_0

    .line 20
    .line 21
    return-object p1

    .line 22
    :cond_0
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 23
    .line 24
    return-object p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static final o800o8O(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇OOo8〇0:Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;

    .line 7
    .line 8
    if-nez p1, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->〇〇808〇()V

    .line 12
    .line 13
    .line 14
    const/4 p1, 0x0

    .line 15
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇oo〇(Z)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->oo88o8O()V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic oO80(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;)Landroid/graphics/Bitmap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->OO:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic o〇0(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o800o8O(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic 〇00(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    const/4 p3, 0x1

    .line 2
    and-int/2addr p2, p3

    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x1

    .line 6
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o〇O8〇〇o(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private final 〇0000OOO()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇OOo8〇0:Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->getType()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, 0x0

    .line 11
    const/4 v2, 0x1

    .line 12
    if-eq v0, v2, :cond_2

    .line 13
    .line 14
    const/4 v3, 0x2

    .line 15
    if-eq v0, v3, :cond_1

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 18
    .line 19
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->O8o08O8O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;->〇〇〇0〇〇0(Z)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 26
    .line 27
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->O8o08O8O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;

    .line 28
    .line 29
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;->〇〇〇0〇〇0(Z)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 33
    .line 34
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->O8o08O8O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;

    .line 35
    .line 36
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;->setFrameMode(Z)V

    .line 37
    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 41
    .line 42
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->O8o08O8O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;

    .line 43
    .line 44
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;->〇〇〇0〇〇0(Z)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 48
    .line 49
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->O8o08O8O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;

    .line 50
    .line 51
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;->setFrameMode(Z)V

    .line 52
    .line 53
    .line 54
    :goto_0
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static final synthetic 〇80〇808〇O(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;)Landroid/graphics/Bitmap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇08O〇00〇o:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic 〇8o8o〇(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;)Lcom/intsig/camscanner/smarterase/dialog/SmartEraseTipsDialog;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->O8o08O8O:Lcom/intsig/camscanner/smarterase/dialog/SmartEraseTipsDialog;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static final 〇O888o0o(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇OOo8〇0:Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;

    .line 7
    .line 8
    if-nez p1, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->〇O00()V

    .line 12
    .line 13
    .line 14
    const/4 p1, 0x0

    .line 15
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇oo〇(Z)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->oo88o8O()V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic 〇O8o08O(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->O〇8O8〇008(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private final 〇oOO8O8(Ljava/lang/String;)V
    .locals 9

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v1, v0, Landroidx/appcompat/app/AppCompatActivity;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    check-cast v0, Landroidx/appcompat/app/AppCompatActivity;

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object v0, v2

    .line 14
    :goto_0
    if-nez v0, :cond_1

    .line 15
    .line 16
    return-void

    .line 17
    :cond_1
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    const/4 v4, 0x0

    .line 22
    const/4 v5, 0x0

    .line 23
    new-instance v6, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$showTips$1;

    .line 24
    .line 25
    invoke-direct {v6, p0, v0, p1, v2}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$showTips$1;-><init>(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;Landroidx/appcompat/app/AppCompatActivity;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    .line 26
    .line 27
    .line 28
    const/4 v7, 0x3

    .line 29
    const/4 v8, 0x0

    .line 30
    invoke-static/range {v3 .. v8}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private final 〇oo〇(Z)V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇OOo8〇0:Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    instance-of v2, v1, Landroidx/appcompat/app/AppCompatActivity;

    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    if-eqz v2, :cond_1

    .line 14
    .line 15
    check-cast v1, Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    move-object v1, v3

    .line 19
    :goto_0
    if-nez v1, :cond_2

    .line 20
    .line 21
    return-void

    .line 22
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->oO80()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-eqz v2, :cond_3

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->oO80()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    goto :goto_1

    .line 37
    :cond_3
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->〇8o8o〇()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    :goto_1
    if-nez v0, :cond_4

    .line 42
    .line 43
    return-void

    .line 44
    :cond_4
    invoke-static {v1}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 45
    .line 46
    .line 47
    move-result-object v4

    .line 48
    const/4 v5, 0x0

    .line 49
    const/4 v6, 0x0

    .line 50
    new-instance v7, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$refreshPage$1;

    .line 51
    .line 52
    invoke-direct {v7, v0, p0, p1, v3}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$refreshPage$1;-><init>(Ljava/lang/String;Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;ZLkotlin/coroutines/Continuation;)V

    .line 53
    .line 54
    .line 55
    const/4 v8, 0x3

    .line 56
    const/4 v9, 0x0

    .line 57
    invoke-static/range {v4 .. v9}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static final synthetic 〇〇808〇(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;Lcom/intsig/camscanner/smarterase/dialog/SmartEraseTipsDialog;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->O8o08O8O:Lcom/intsig/camscanner/smarterase/dialog/SmartEraseTipsDialog;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic 〇〇888(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;)Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public final OOO〇O0()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇OOo8〇0:Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v2, "updateType: pageData: "

    .line 12
    .line 13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    const-string v2, "SmartEraseOperateView"

    .line 24
    .line 25
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 29
    .line 30
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->O8o08O8O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;

    .line 31
    .line 32
    invoke-virtual {v1}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;->Oo8Oo00oo()V

    .line 33
    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇0000OOO()V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->getType()I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    const/4 v2, 0x1

    .line 43
    if-eq v1, v2, :cond_6

    .line 44
    .line 45
    const/4 v2, 0x2

    .line 46
    if-eq v1, v2, :cond_5

    .line 47
    .line 48
    const/4 v2, 0x4

    .line 49
    if-eq v1, v2, :cond_3

    .line 50
    .line 51
    const/16 v2, 0x8

    .line 52
    .line 53
    if-eq v1, v2, :cond_1

    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_1
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->o〇0(I)Z

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    if-eqz v1, :cond_2

    .line 61
    .line 62
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    const v1, 0x7f1312ea

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    const-string v1, "context.getString(R.string.cs_629_erase_20)"

    .line 74
    .line 75
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇oOO8O8(Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o〇00O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;

    .line 83
    .line 84
    if-eqz v1, :cond_7

    .line 85
    .line 86
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;->O8(Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;)V

    .line 87
    .line 88
    .line 89
    goto :goto_0

    .line 90
    :cond_3
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->o〇0(I)Z

    .line 91
    .line 92
    .line 93
    move-result v1

    .line 94
    if-eqz v1, :cond_4

    .line 95
    .line 96
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    const v1, 0x7f1312e5

    .line 101
    .line 102
    .line 103
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    const-string v1, "context.getString(R.string.cs_629_erase_15)"

    .line 108
    .line 109
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇oOO8O8(Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    goto :goto_0

    .line 116
    :cond_4
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o〇00O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;

    .line 117
    .line 118
    if-eqz v1, :cond_7

    .line 119
    .line 120
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;->Oo08(Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;)V

    .line 121
    .line 122
    .line 123
    goto :goto_0

    .line 124
    :cond_5
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 125
    .line 126
    .line 127
    move-result-object v0

    .line 128
    const v1, 0x7f1312df

    .line 129
    .line 130
    .line 131
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 132
    .line 133
    .line 134
    move-result-object v0

    .line 135
    const-string v1, "context.getString(R.string.cs_629_erase_09)"

    .line 136
    .line 137
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇oOO8O8(Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    goto :goto_0

    .line 144
    :cond_6
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 145
    .line 146
    .line 147
    move-result-object v0

    .line 148
    const v1, 0x7f1312ed

    .line 149
    .line 150
    .line 151
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 152
    .line 153
    .line 154
    move-result-object v0

    .line 155
    const-string v1, "context.getString(R.string.cs_629_erase_23)"

    .line 156
    .line 157
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    .line 159
    .line 160
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇oOO8O8(Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    :cond_7
    :goto_0
    return-void
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public final getMSmartEraseOperateViewDelegate()Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o〇00O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 2
    .line 3
    .line 4
    const-string v0, "SmartEraseOperateView"

    .line 5
    .line 6
    const-string v1, "onDetachedFromWindow:"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oo88o8O()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇OOo8〇0:Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->O8()Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-nez v1, :cond_2

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->〇o00〇〇Oo()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-eqz v1, :cond_1

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 20
    .line 21
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 22
    .line 23
    const/16 v2, 0x8

    .line 24
    .line 25
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 26
    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 29
    .line 30
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->o〇00O:Landroid/widget/LinearLayout;

    .line 31
    .line 32
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 33
    .line 34
    .line 35
    goto :goto_1

    .line 36
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 37
    .line 38
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->o〇00O:Landroid/widget/LinearLayout;

    .line 39
    .line 40
    const/4 v2, 0x0

    .line 41
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 42
    .line 43
    .line 44
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 45
    .line 46
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 47
    .line 48
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 49
    .line 50
    .line 51
    :goto_1
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 52
    .line 53
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 54
    .line 55
    const-string v2, "mBinding.ivUndo"

    .line 56
    .line 57
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->O8()Z

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->O8ooOoo〇(Landroid/widget/ImageView;Z)V

    .line 65
    .line 66
    .line 67
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 68
    .line 69
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->OO:Landroid/widget/ImageView;

    .line 70
    .line 71
    const-string v2, "mBinding.ivRedo"

    .line 72
    .line 73
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->〇o00〇〇Oo()Z

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->O8ooOoo〇(Landroid/widget/ImageView;Z)V

    .line 81
    .line 82
    .line 83
    return-void
    .line 84
    .line 85
    .line 86
.end method

.method public final o〇O8〇〇o(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇OOo8〇0:Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 7
    .line 8
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->O8o08O8O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->OO0o〇〇〇〇0()F

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;->setPenSize(F)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 18
    .line 19
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->O8o08O8O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;->Oo8Oo00oo()V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇oo〇(Z)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->oo88o8O()V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇0000OOO()V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o〇00O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;

    .line 34
    .line 35
    if-eqz p1, :cond_1

    .line 36
    .line 37
    const/4 v0, 0x0

    .line 38
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;->〇o00〇〇Oo(Z)V

    .line 39
    .line 40
    .line 41
    :cond_1
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public final o〇〇0〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇OOo8〇0:Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 7
    .line 8
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->O8o08O8O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->OO0o〇〇〇〇0()F

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;->setPenSize(F)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final setMSmartEraseOperateViewDelegate(Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o〇00O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇0〇O0088o()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇OOo8〇0:Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->oO80()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-eqz v1, :cond_1

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->oO80()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    goto :goto_0

    .line 21
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;->〇8o8o〇()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    :goto_0
    const/4 v2, 0x1

    .line 26
    invoke-static {v1, v2}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    if-nez v1, :cond_2

    .line 31
    .line 32
    return-void

    .line 33
    :cond_2
    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    const-string v4, "toString(this)"

    .line 38
    .line 39
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    new-instance v4, Ljava/lang/StringBuilder;

    .line 43
    .line 44
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    .line 46
    .line 47
    const-string v5, "eraseText bounds: "

    .line 48
    .line 49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v3

    .line 59
    const-string v4, "SmartEraseOperateView"

    .line 60
    .line 61
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    iget-object v3, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 65
    .line 66
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->O8o08O8O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;

    .line 67
    .line 68
    const/4 v5, 0x0

    .line 69
    aget v5, v1, v5

    .line 70
    .line 71
    aget v1, v1, v2

    .line 72
    .line 73
    invoke-virtual {v3, v5, v1}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;->o〇0OOo〇0(II)Ljava/util/List;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    if-nez v1, :cond_3

    .line 78
    .line 79
    return-void

    .line 80
    :cond_3
    move-object v3, v1

    .line 81
    check-cast v3, Ljava/util/Collection;

    .line 82
    .line 83
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    .line 84
    .line 85
    .line 86
    move-result v3

    .line 87
    xor-int/2addr v2, v3

    .line 88
    if-eqz v2, :cond_4

    .line 89
    .line 90
    iget-object v2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o〇00O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;

    .line 91
    .line 92
    if-eqz v2, :cond_5

    .line 93
    .line 94
    invoke-interface {v2, v0, v1}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;->〇o〇(Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;Ljava/util/List;)V

    .line 95
    .line 96
    .line 97
    goto :goto_1

    .line 98
    :cond_4
    const-string v0, "eraseText: is a code failure"

    .line 99
    .line 100
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    :cond_5
    :goto_1
    return-void
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public final 〇O00()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->O8o08O8O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;->Oo8Oo00oo()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O〇(Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "pageData"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v1, "bindData: pageData: "

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    const-string v1, "SmartEraseOperateView"

    .line 24
    .line 25
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    iput-object p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇OOo8〇0:Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;

    .line 29
    .line 30
    const/4 p1, 0x0

    .line 31
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o〇O8〇〇o(Z)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public final 〇〇8O0〇8()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->〇OOo8〇0:Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 7
    .line 8
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->O8o08O8O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;->getBitmapWithPath()Landroid/graphics/Bitmap;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    iget-object v2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o0:Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;

    .line 15
    .line 16
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/SmartEraseViewOperateBinding;->O8o08O8O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;

    .line 17
    .line 18
    invoke-virtual {v2}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseView;->getShowBitmap()Landroid/graphics/Bitmap;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    if-nez v2, :cond_1

    .line 23
    .line 24
    return-void

    .line 25
    :cond_1
    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 26
    .line 27
    const/4 v4, 0x1

    .line 28
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    const/16 v3, 0x200

    .line 33
    .line 34
    invoke-static {v1, v3, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-static {v2, v3, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    .line 39
    .line 40
    .line 41
    move-result-object v2

    .line 42
    iget-object v3, p0, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView;->o〇00O:Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;

    .line 43
    .line 44
    if-eqz v3, :cond_2

    .line 45
    .line 46
    const-string v4, "scaleBitmap"

    .line 47
    .line 48
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    const-string v4, "scaleBitmapWithPath"

    .line 52
    .line 53
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    invoke-interface {v3, v0, v2, v1}, Lcom/intsig/camscanner/smarterase/widget/SmartEraseOperateView$SmartEraseOperateViewDelegate;->〇080(Lcom/intsig/camscanner/smarterase/data/SmartErasePageData;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 57
    .line 58
    .line 59
    :cond_2
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

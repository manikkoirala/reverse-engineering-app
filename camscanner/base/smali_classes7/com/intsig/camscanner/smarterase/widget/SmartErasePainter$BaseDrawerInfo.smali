.class public Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;
.super Ljava/lang/Object;
.source "SmartErasePainter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BaseDrawerInfo"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final 〇080:F

.field private final 〇o00〇〇Oo:Landroid/graphics/Matrix;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(FLandroid/graphics/Matrix;)V
    .locals 1
    .param p2    # Landroid/graphics/Matrix;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "scaleMatrix"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput p1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->〇080:F

    .line 10
    .line 11
    iput-object p2, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->〇o00〇〇Oo:Landroid/graphics/Matrix;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final 〇o00〇〇Oo(Landroid/graphics/Matrix;)F
    .locals 1

    .line 1
    const/16 v0, 0x9

    .line 2
    .line 3
    new-array v0, v0, [F

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    aget p1, v0, p1

    .line 10
    .line 11
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public final O8(Landroid/graphics/Matrix;)F
    .locals 2
    .param p1    # Landroid/graphics/Matrix;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "transformMatrix"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->〇o00〇〇Oo:Landroid/graphics/Matrix;

    .line 7
    .line 8
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->〇o00〇〇Oo(Landroid/graphics/Matrix;)F

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->〇o00〇〇Oo(Landroid/graphics/Matrix;)F

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    iget v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->〇080:F

    .line 17
    .line 18
    div-float/2addr v1, v0

    .line 19
    mul-float v1, v1, p1

    .line 20
    .line 21
    return v1
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇080()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->〇o00〇〇Oo:Landroid/graphics/Matrix;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->〇o00〇〇Oo(Landroid/graphics/Matrix;)F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget v1, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->〇080:F

    .line 8
    .line 9
    div-float/2addr v1, v0

    .line 10
    return v1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇()Landroid/graphics/Matrix;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/smarterase/widget/SmartErasePainter$BaseDrawerInfo;->〇o00〇〇Oo:Landroid/graphics/Matrix;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

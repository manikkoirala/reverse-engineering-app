.class public final Lcom/intsig/camscanner/translate/TranslateToolbar;
.super Landroid/widget/FrameLayout;
.source "TranslateToolbar.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/translate/TranslateToolbar$OnViewClickListener;,
        Lcom/intsig/camscanner/translate/TranslateToolbar$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final o〇00O:I

.field public static final 〇08O〇00〇o:Lcom/intsig/camscanner/translate/TranslateToolbar$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final OO:Lcom/intsig/camscanner/translate/TranslateToolbar$mOnClickListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:Lcom/intsig/camscanner/translate/TranslateToolbar$OnViewClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/translate/TranslateToolbar$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/translate/TranslateToolbar$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/translate/TranslateToolbar;->〇08O〇00〇o:Lcom/intsig/camscanner/translate/TranslateToolbar$Companion;

    .line 8
    .line 9
    const v0, 0x7f060208

    .line 10
    .line 11
    .line 12
    const/high16 v1, 0x3f800000    # 1.0f

    .line 13
    .line 14
    invoke-static {v0, v1}, Lcom/intsig/utils/ColorUtil;->〇o〇(IF)I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    sput v0, Lcom/intsig/camscanner/translate/TranslateToolbar;->o〇00O:I

    .line 19
    .line 20
    return-void
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->inflate(Landroid/view/LayoutInflater;)Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;

    move-result-object p1

    const-string p2, "inflate(LayoutInflater.from(context))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/intsig/camscanner/translate/TranslateToolbar;->o0:Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;

    .line 3
    new-instance p2, Lcom/intsig/camscanner/translate/TranslateToolbar$mOnClickListener$1;

    invoke-direct {p2, p0}, Lcom/intsig/camscanner/translate/TranslateToolbar$mOnClickListener$1;-><init>(Lcom/intsig/camscanner/translate/TranslateToolbar;)V

    iput-object p2, p0, Lcom/intsig/camscanner/translate/TranslateToolbar;->OO:Lcom/intsig/camscanner/translate/TranslateToolbar$mOnClickListener$1;

    .line 4
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 6
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->OO:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 7
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->〇0O:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 8
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 9
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->〇OOo8〇0:Landroid/widget/Button;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->inflate(Landroid/view/LayoutInflater;)Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;

    move-result-object p1

    const-string p2, "inflate(LayoutInflater.from(context))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/intsig/camscanner/translate/TranslateToolbar;->o0:Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;

    .line 12
    new-instance p2, Lcom/intsig/camscanner/translate/TranslateToolbar$mOnClickListener$1;

    invoke-direct {p2, p0}, Lcom/intsig/camscanner/translate/TranslateToolbar$mOnClickListener$1;-><init>(Lcom/intsig/camscanner/translate/TranslateToolbar;)V

    iput-object p2, p0, Lcom/intsig/camscanner/translate/TranslateToolbar;->OO:Lcom/intsig/camscanner/translate/TranslateToolbar$mOnClickListener$1;

    .line 13
    new-instance p3, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v0, -0x1

    invoke-direct {p3, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 15
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->OO:Landroid/widget/ImageView;

    invoke-virtual {p3, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 16
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->〇0O:Landroid/widget/LinearLayout;

    invoke-virtual {p3, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 17
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    invoke-virtual {p3, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 18
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->〇OOo8〇0:Landroid/widget/Button;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final 〇o00〇〇Oo(Landroid/view/View;F)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getRotation()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    cmpg-float v0, v0, p2

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    if-eqz v0, :cond_1

    .line 13
    .line 14
    return-void

    .line 15
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    const-wide/16 v0, 0xc8

    .line 24
    .line 25
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-virtual {p1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public final getBinding()Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/translate/TranslateToolbar;->o0:Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getOnViewClickListener()Lcom/intsig/camscanner/translate/TranslateToolbar$OnViewClickListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/translate/TranslateToolbar;->〇OOo8〇0:Lcom/intsig/camscanner/translate/TranslateToolbar$OnViewClickListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final setFromFocus(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/translate/TranslateToolbar;->o0:Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;

    .line 2
    .line 3
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->〇0O:Landroid/widget/LinearLayout;

    .line 4
    .line 5
    invoke-virtual {v1, p1}, Landroid/view/View;->setSelected(Z)V

    .line 6
    .line 7
    .line 8
    const-string v1, "ivTriangleFrom"

    .line 9
    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->〇08O〇00〇o:Landroid/view/View;

    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 16
    .line 17
    .line 18
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 19
    .line 20
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/high16 v1, 0x43340000    # 180.0f

    .line 24
    .line 25
    invoke-direct {p0, p1, v1}, Lcom/intsig/camscanner/translate/TranslateToolbar;->〇o00〇〇Oo(Landroid/view/View;F)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/translate/TranslateToolbar;->setToFocus(Z)V

    .line 29
    .line 30
    .line 31
    const p1, -0xe64356

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->〇08O〇00〇o:Landroid/view/View;

    .line 36
    .line 37
    const/16 v2, 0x8

    .line 38
    .line 39
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 40
    .line 41
    .line 42
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 43
    .line 44
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0, p1, v1}, Lcom/intsig/camscanner/translate/TranslateToolbar;->〇o00〇〇Oo(Landroid/view/View;F)V

    .line 49
    .line 50
    .line 51
    sget p1, Lcom/intsig/camscanner/translate/TranslateToolbar;->o〇00O:I

    .line 52
    .line 53
    :goto_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 54
    .line 55
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 56
    .line 57
    .line 58
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    const-string v1, "valueOf(color)"

    .line 63
    .line 64
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 68
    .line 69
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public final setOnViewClickListener(Lcom/intsig/camscanner/translate/TranslateToolbar$OnViewClickListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/translate/TranslateToolbar;->〇OOo8〇0:Lcom/intsig/camscanner/translate/TranslateToolbar$OnViewClickListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setToFocus(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/translate/TranslateToolbar;->o0:Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;

    .line 2
    .line 3
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 4
    .line 5
    invoke-virtual {v1, p1}, Landroid/view/View;->setSelected(Z)V

    .line 6
    .line 7
    .line 8
    const-string v1, "ivTriangleTo"

    .line 9
    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->o〇00O:Landroid/view/View;

    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 16
    .line 17
    .line 18
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 19
    .line 20
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/high16 v1, 0x43340000    # 180.0f

    .line 24
    .line 25
    invoke-direct {p0, p1, v1}, Lcom/intsig/camscanner/translate/TranslateToolbar;->〇o00〇〇Oo(Landroid/view/View;F)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/translate/TranslateToolbar;->setFromFocus(Z)V

    .line 29
    .line 30
    .line 31
    const p1, -0xe64356

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->o〇00O:Landroid/view/View;

    .line 36
    .line 37
    const/16 v2, 0x8

    .line 38
    .line 39
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 40
    .line 41
    .line 42
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 43
    .line 44
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0, p1, v1}, Lcom/intsig/camscanner/translate/TranslateToolbar;->〇o00〇〇Oo(Landroid/view/View;F)V

    .line 49
    .line 50
    .line 51
    sget p1, Lcom/intsig/camscanner/translate/TranslateToolbar;->o〇00O:I

    .line 52
    .line 53
    :goto_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 54
    .line 55
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 56
    .line 57
    .line 58
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    const-string v1, "valueOf(color)"

    .line 63
    .line 64
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 68
    .line 69
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public final 〇080()V
    .locals 6

    .line 1
    const/4 v0, -0x1

    .line 2
    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 3
    .line 4
    .line 5
    move-result-object v1

    .line 6
    const-string v2, "valueOf(color)"

    .line 7
    .line 8
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/camscanner/translate/TranslateToolbar;->o0:Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;

    .line 12
    .line 13
    invoke-virtual {v2}, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 14
    .line 15
    .line 16
    move-result-object v3

    .line 17
    sget-object v4, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 18
    .line 19
    invoke-virtual {v4}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 20
    .line 21
    .line 22
    move-result-object v4

    .line 23
    const v5, 0x7f0601e6

    .line 24
    .line 25
    .line 26
    invoke-static {v4, v5}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 27
    .line 28
    .line 29
    move-result v4

    .line 30
    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 31
    .line 32
    .line 33
    iget-object v3, v2, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 34
    .line 35
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 36
    .line 37
    .line 38
    iget-object v3, v2, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 39
    .line 40
    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 41
    .line 42
    .line 43
    iget-object v3, v2, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 44
    .line 45
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 46
    .line 47
    .line 48
    iget-object v0, v2, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 49
    .line 50
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 51
    .line 52
    .line 53
    iget-object v0, v2, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->o8〇OO0〇0o:Landroid/widget/ImageView;

    .line 54
    .line 55
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 56
    .line 57
    .line 58
    iget-object v0, v2, Lcom/intsig/camscanner/databinding/ToolbarTranslateLanBinding;->OO:Landroid/widget/ImageView;

    .line 59
    .line 60
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 61
    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

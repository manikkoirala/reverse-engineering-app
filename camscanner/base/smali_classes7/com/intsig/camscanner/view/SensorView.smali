.class public Lcom/intsig/camscanner/view/SensorView;
.super Landroid/view/View;
.source "SensorView.java"


# instance fields
.field private O0O:F

.field private O8o08O8O:F

.field private OO:Z

.field private OO〇00〇8oO:F

.field private o0:Landroid/graphics/Bitmap;

.field private o8oOOo:F

.field private o8〇OO0〇0o:F

.field private oOo0:F

.field private oOo〇8o008:F

.field private ooo0〇〇O:F

.field private o〇00O:F

.field private 〇080OO8〇0:F

.field private 〇08O〇00〇o:F

.field private 〇0O:F

.field private 〇8〇oO〇〇8o:F

.field private 〇OOo8〇0:Z

.field 〇O〇〇O8:I

.field 〇o0O:I

.field private 〇〇08O:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/high16 p1, -0x40800000    # -1.0f

    .line 2
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->〇08O〇00〇o:F

    .line 3
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->o〇00O:F

    .line 4
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->oOo0:F

    .line 5
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->OO〇00〇8oO:F

    const/4 p1, 0x0

    .line 6
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->o8〇OO0〇0o:F

    .line 7
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->〇8〇oO〇〇8o:F

    const/4 p1, 0x2

    .line 8
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->〇O〇〇O8:I

    .line 9
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->〇o0O:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 10
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 p1, -0x40800000    # -1.0f

    .line 11
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->〇08O〇00〇o:F

    .line 12
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->o〇00O:F

    .line 13
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->oOo0:F

    .line 14
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->OO〇00〇8oO:F

    const/4 p1, 0x0

    .line 15
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->o8〇OO0〇0o:F

    .line 16
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->〇8〇oO〇〇8o:F

    const/4 p1, 0x2

    .line 17
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->〇O〇〇O8:I

    .line 18
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->〇o0O:I

    return-void
.end method

.method private 〇o00〇〇Oo()V
    .locals 11

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/SensorView;->o8〇OO0〇0o:F

    .line 2
    .line 3
    const/high16 v1, 0x43070000    # 135.0f

    .line 4
    .line 5
    const/high16 v2, 0x43340000    # 180.0f

    .line 6
    .line 7
    cmpl-float v1, v0, v1

    .line 8
    .line 9
    if-lez v1, :cond_0

    .line 10
    .line 11
    sub-float/2addr v0, v2

    .line 12
    iput v0, p0, Lcom/intsig/camscanner/view/SensorView;->o8〇OO0〇0o:F

    .line 13
    .line 14
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/view/SensorView;->o8〇OO0〇0o:F

    .line 15
    .line 16
    const/high16 v1, -0x3cf90000    # -135.0f

    .line 17
    .line 18
    cmpg-float v1, v0, v1

    .line 19
    .line 20
    if-gez v1, :cond_1

    .line 21
    .line 22
    add-float/2addr v0, v2

    .line 23
    iput v0, p0, Lcom/intsig/camscanner/view/SensorView;->o8〇OO0〇0o:F

    .line 24
    .line 25
    :cond_1
    iget v0, p0, Lcom/intsig/camscanner/view/SensorView;->o8〇OO0〇0o:F

    .line 26
    .line 27
    const/high16 v1, 0x41f00000    # 30.0f

    .line 28
    .line 29
    cmpl-float v0, v0, v1

    .line 30
    .line 31
    if-lez v0, :cond_2

    .line 32
    .line 33
    iput v1, p0, Lcom/intsig/camscanner/view/SensorView;->o8〇OO0〇0o:F

    .line 34
    .line 35
    :cond_2
    iget v0, p0, Lcom/intsig/camscanner/view/SensorView;->o8〇OO0〇0o:F

    .line 36
    .line 37
    const/high16 v2, -0x3e100000    # -30.0f

    .line 38
    .line 39
    cmpg-float v0, v0, v2

    .line 40
    .line 41
    if-gez v0, :cond_3

    .line 42
    .line 43
    iput v2, p0, Lcom/intsig/camscanner/view/SensorView;->o8〇OO0〇0o:F

    .line 44
    .line 45
    :cond_3
    iget v0, p0, Lcom/intsig/camscanner/view/SensorView;->〇8〇oO〇〇8o:F

    .line 46
    .line 47
    cmpl-float v0, v0, v1

    .line 48
    .line 49
    if-lez v0, :cond_4

    .line 50
    .line 51
    iput v1, p0, Lcom/intsig/camscanner/view/SensorView;->〇8〇oO〇〇8o:F

    .line 52
    .line 53
    :cond_4
    iget v0, p0, Lcom/intsig/camscanner/view/SensorView;->〇8〇oO〇〇8o:F

    .line 54
    .line 55
    cmpg-float v0, v0, v2

    .line 56
    .line 57
    if-gez v0, :cond_5

    .line 58
    .line 59
    iput v2, p0, Lcom/intsig/camscanner/view/SensorView;->〇8〇oO〇〇8o:F

    .line 60
    .line 61
    :cond_5
    iget v0, p0, Lcom/intsig/camscanner/view/SensorView;->o8〇OO0〇0o:F

    .line 62
    .line 63
    iget v2, p0, Lcom/intsig/camscanner/view/SensorView;->oOo0:F

    .line 64
    .line 65
    sub-float v2, v0, v2

    .line 66
    .line 67
    iget v3, p0, Lcom/intsig/camscanner/view/SensorView;->〇8〇oO〇〇8o:F

    .line 68
    .line 69
    iget v4, p0, Lcom/intsig/camscanner/view/SensorView;->OO〇00〇8oO:F

    .line 70
    .line 71
    sub-float v4, v3, v4

    .line 72
    .line 73
    mul-float v2, v2, v2

    .line 74
    .line 75
    mul-float v4, v4, v4

    .line 76
    .line 77
    add-float/2addr v2, v4

    .line 78
    const/high16 v4, 0x447a0000    # 1000.0f

    .line 79
    .line 80
    const/4 v5, 0x0

    .line 81
    cmpl-float v4, v2, v4

    .line 82
    .line 83
    if-lez v4, :cond_6

    .line 84
    .line 85
    iput-boolean v5, p0, Lcom/intsig/camscanner/view/SensorView;->OO:Z

    .line 86
    .line 87
    :cond_6
    const/high16 v4, 0x3f800000    # 1.0f

    .line 88
    .line 89
    cmpg-float v2, v2, v4

    .line 90
    .line 91
    if-gez v2, :cond_7

    .line 92
    .line 93
    iput-boolean v5, p0, Lcom/intsig/camscanner/view/SensorView;->OO:Z

    .line 94
    .line 95
    :cond_7
    iget-boolean v2, p0, Lcom/intsig/camscanner/view/SensorView;->OO:Z

    .line 96
    .line 97
    if-eqz v2, :cond_b

    .line 98
    .line 99
    iput v0, p0, Lcom/intsig/camscanner/view/SensorView;->oOo0:F

    .line 100
    .line 101
    iput v3, p0, Lcom/intsig/camscanner/view/SensorView;->OO〇00〇8oO:F

    .line 102
    .line 103
    add-float/2addr v3, v1

    .line 104
    iget v2, p0, Lcom/intsig/camscanner/view/SensorView;->〇〇08O:F

    .line 105
    .line 106
    mul-float v3, v3, v2

    .line 107
    .line 108
    sub-float/2addr v1, v0

    .line 109
    iget v0, p0, Lcom/intsig/camscanner/view/SensorView;->ooo0〇〇O:F

    .line 110
    .line 111
    mul-float v1, v1, v0

    .line 112
    .line 113
    iget v0, p0, Lcom/intsig/camscanner/view/SensorView;->O0O:F

    .line 114
    .line 115
    sub-float v2, v1, v0

    .line 116
    .line 117
    sub-float v0, v3, v0

    .line 118
    .line 119
    mul-float v4, v2, v2

    .line 120
    .line 121
    mul-float v5, v0, v0

    .line 122
    .line 123
    add-float/2addr v4, v5

    .line 124
    float-to-double v4, v4

    .line 125
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    .line 126
    .line 127
    .line 128
    move-result-wide v4

    .line 129
    iget v6, p0, Lcom/intsig/camscanner/view/SensorView;->O0O:F

    .line 130
    .line 131
    iget v7, p0, Lcom/intsig/camscanner/view/SensorView;->o8oOOo:F

    .line 132
    .line 133
    sub-float v7, v6, v7

    .line 134
    .line 135
    float-to-double v7, v7

    .line 136
    cmpl-double v9, v4, v7

    .line 137
    .line 138
    if-lez v9, :cond_a

    .line 139
    .line 140
    const/4 v1, 0x0

    .line 141
    cmpg-float v3, v2, v1

    .line 142
    .line 143
    if-gez v3, :cond_8

    .line 144
    .line 145
    float-to-double v9, v6

    .line 146
    float-to-double v2, v2

    .line 147
    div-double/2addr v2, v4

    .line 148
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    .line 149
    .line 150
    .line 151
    move-result-wide v2

    .line 152
    mul-double v2, v2, v7

    .line 153
    .line 154
    sub-double/2addr v9, v2

    .line 155
    goto :goto_0

    .line 156
    :cond_8
    float-to-double v9, v6

    .line 157
    float-to-double v2, v2

    .line 158
    div-double/2addr v2, v4

    .line 159
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    .line 160
    .line 161
    .line 162
    move-result-wide v2

    .line 163
    mul-double v2, v2, v7

    .line 164
    .line 165
    add-double/2addr v9, v2

    .line 166
    :goto_0
    double-to-float v2, v9

    .line 167
    cmpg-float v1, v0, v1

    .line 168
    .line 169
    if-gez v1, :cond_9

    .line 170
    .line 171
    iget v1, p0, Lcom/intsig/camscanner/view/SensorView;->O0O:F

    .line 172
    .line 173
    float-to-double v9, v1

    .line 174
    float-to-double v0, v0

    .line 175
    div-double/2addr v0, v4

    .line 176
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    .line 177
    .line 178
    .line 179
    move-result-wide v0

    .line 180
    mul-double v7, v7, v0

    .line 181
    .line 182
    sub-double/2addr v9, v7

    .line 183
    goto :goto_1

    .line 184
    :cond_9
    iget v1, p0, Lcom/intsig/camscanner/view/SensorView;->O0O:F

    .line 185
    .line 186
    float-to-double v9, v1

    .line 187
    float-to-double v0, v0

    .line 188
    div-double/2addr v0, v4

    .line 189
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    .line 190
    .line 191
    .line 192
    move-result-wide v0

    .line 193
    mul-double v7, v7, v0

    .line 194
    .line 195
    add-double/2addr v9, v7

    .line 196
    :goto_1
    double-to-float v3, v9

    .line 197
    move v1, v2

    .line 198
    :cond_a
    iget v0, p0, Lcom/intsig/camscanner/view/SensorView;->o8oOOo:F

    .line 199
    .line 200
    sub-float/2addr v1, v0

    .line 201
    iput v1, p0, Lcom/intsig/camscanner/view/SensorView;->O8o08O8O:F

    .line 202
    .line 203
    sub-float/2addr v3, v0

    .line 204
    iput v3, p0, Lcom/intsig/camscanner/view/SensorView;->〇080OO8〇0:F

    .line 205
    .line 206
    :cond_b
    return-void
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/view/View;->isInEditMode()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-nez v0, :cond_2

    .line 9
    .line 10
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/SensorView;->〇OOo8〇0:Z

    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    int-to-float v0, v0

    .line 20
    iput v0, p0, Lcom/intsig/camscanner/view/SensorView;->o〇00O:F

    .line 21
    .line 22
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    int-to-float v0, v0

    .line 27
    iput v0, p0, Lcom/intsig/camscanner/view/SensorView;->〇08O〇00〇o:F

    .line 28
    .line 29
    iget v2, p0, Lcom/intsig/camscanner/view/SensorView;->o〇00O:F

    .line 30
    .line 31
    const/high16 v3, 0x40000000    # 2.0f

    .line 32
    .line 33
    div-float v4, v2, v3

    .line 34
    .line 35
    iput v4, p0, Lcom/intsig/camscanner/view/SensorView;->O0O:F

    .line 36
    .line 37
    const/high16 v4, 0x42700000    # 60.0f

    .line 38
    .line 39
    div-float/2addr v2, v4

    .line 40
    iput v2, p0, Lcom/intsig/camscanner/view/SensorView;->ooo0〇〇O:F

    .line 41
    .line 42
    div-float/2addr v0, v4

    .line 43
    iput v0, p0, Lcom/intsig/camscanner/view/SensorView;->〇〇08O:F

    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/view/SensorView;->o0:Landroid/graphics/Bitmap;

    .line 46
    .line 47
    if-eqz v0, :cond_0

    .line 48
    .line 49
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    if-nez v0, :cond_0

    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/camscanner/view/SensorView;->o0:Landroid/graphics/Bitmap;

    .line 56
    .line 57
    iget v2, p0, Lcom/intsig/camscanner/view/SensorView;->o〇00O:F

    .line 58
    .line 59
    iget v4, p0, Lcom/intsig/camscanner/view/SensorView;->oOo〇8o008:F

    .line 60
    .line 61
    sub-float/2addr v2, v4

    .line 62
    div-float/2addr v2, v3

    .line 63
    iget v5, p0, Lcom/intsig/camscanner/view/SensorView;->〇08O〇00〇o:F

    .line 64
    .line 65
    sub-float/2addr v5, v4

    .line 66
    div-float/2addr v5, v3

    .line 67
    invoke-virtual {p1, v0, v2, v5, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 68
    .line 69
    .line 70
    :cond_0
    const/4 p1, 0x0

    .line 71
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/SensorView;->〇OOo8〇0:Z

    .line 72
    .line 73
    goto :goto_0

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/view/SensorView;->o0:Landroid/graphics/Bitmap;

    .line 75
    .line 76
    if-eqz v0, :cond_2

    .line 77
    .line 78
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 79
    .line 80
    .line 81
    move-result v0

    .line 82
    if-nez v0, :cond_2

    .line 83
    .line 84
    iget-object v0, p0, Lcom/intsig/camscanner/view/SensorView;->o0:Landroid/graphics/Bitmap;

    .line 85
    .line 86
    iget v2, p0, Lcom/intsig/camscanner/view/SensorView;->O8o08O8O:F

    .line 87
    .line 88
    iget v3, p0, Lcom/intsig/camscanner/view/SensorView;->〇080OO8〇0:F

    .line 89
    .line 90
    invoke-virtual {p1, v0, v2, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 91
    .line 92
    .line 93
    :cond_2
    :goto_0
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/SensorView;->o0:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-float v0, v0

    .line 10
    iput v0, p0, Lcom/intsig/camscanner/view/SensorView;->oOo〇8o008:F

    .line 11
    .line 12
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    int-to-float p1, p1

    .line 17
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->〇0O:F

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const-string p1, "SensorView"

    .line 21
    .line 22
    const-string v0, "image is null"

    .line 23
    .line 24
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    :goto_0
    iget p1, p0, Lcom/intsig/camscanner/view/SensorView;->oOo〇8o008:F

    .line 28
    .line 29
    const/high16 v0, 0x40000000    # 2.0f

    .line 30
    .line 31
    div-float/2addr p1, v0

    .line 32
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->o8oOOo:F

    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇080(FF)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/SensorView;->OO:Z

    .line 3
    .line 4
    iput p1, p0, Lcom/intsig/camscanner/view/SensorView;->o8〇OO0〇0o:F

    .line 5
    .line 6
    iput p2, p0, Lcom/intsig/camscanner/view/SensorView;->〇8〇oO〇〇8o:F

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/view/SensorView;->〇o00〇〇Oo()V

    .line 9
    .line 10
    .line 11
    iget-boolean p1, p0, Lcom/intsig/camscanner/view/SensorView;->OO:Z

    .line 12
    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 16
    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇o〇()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/SensorView;->〇OOo8〇0:Z

    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/view/SensorView;->oOo0:F

    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/camscanner/view/SensorView;->OO〇00〇8oO:F

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

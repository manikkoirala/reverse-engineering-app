.class public final Lcom/intsig/camscanner/view/CircleProgressView;
.super Landroid/view/View;
.source "CircleProgressView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/CircleProgressView$OnProgressChangedListener;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O0O:F

.field private O8o08O8O:F

.field private OO:F

.field private OO〇00〇8oO:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field private o0:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8oOOo:F

.field private o8〇OO0〇0o:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field private oOo0:Z

.field private oOo〇8o008:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field private ooo0〇〇O:J

.field private o〇00O:F

.field private 〇080OO8〇0:F

.field private 〇08O〇00〇o:F

.field private 〇0O:F

.field private 〇8〇oO〇〇8o:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field private 〇OOo8〇0:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇O〇〇O8:F

.field private final 〇o0O:Landroid/graphics/RectF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇08O:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attributeSet"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    new-instance p1, Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-direct {p1, v0}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->o0:Landroid/graphics/Paint;

    .line 3
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1, v0}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇OOo8〇0:Landroid/graphics/Paint;

    const/high16 p1, 0x40000000    # 2.0f

    .line 4
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->OO:F

    const/high16 p1, 0x40800000    # 4.0f

    .line 5
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇08O〇00〇o:F

    const/high16 p1, -0x3d4c0000    # -90.0f

    .line 6
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇080OO8〇0:F

    const/high16 p1, 0x43b40000    # 360.0f

    .line 7
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇0O:F

    const-string p1, "#33212121"

    .line 8
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->oOo〇8o008:I

    const-string p1, "#FFFFFF"

    .line 9
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->OO〇00〇8oO:I

    const-wide/16 v0, 0x1f4

    .line 10
    iput-wide v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->ooo0〇〇O:J

    const/high16 p1, 0x42c80000    # 100.0f

    .line 11
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇〇08O:F

    .line 12
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇o0O:Landroid/graphics/RectF;

    .line 13
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/view/CircleProgressView;->o〇0(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "ctx"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    new-instance p1, Landroid/graphics/Paint;

    const/4 p3, 0x1

    invoke-direct {p1, p3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->o0:Landroid/graphics/Paint;

    .line 16
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1, p3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇OOo8〇0:Landroid/graphics/Paint;

    const/high16 p1, 0x40000000    # 2.0f

    .line 17
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->OO:F

    const/high16 p1, 0x40800000    # 4.0f

    .line 18
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇08O〇00〇o:F

    const/high16 p1, -0x3d4c0000    # -90.0f

    .line 19
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇080OO8〇0:F

    const/high16 p1, 0x43b40000    # 360.0f

    .line 20
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇0O:F

    const-string p1, "#33212121"

    .line 21
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->oOo〇8o008:I

    const-string p1, "#FFFFFF"

    .line 22
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->OO〇00〇8oO:I

    const-wide/16 v0, 0x1f4

    .line 23
    iput-wide v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->ooo0〇〇O:J

    const/high16 p1, 0x42c80000    # 100.0f

    .line 24
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇〇08O:F

    .line 25
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇o0O:Landroid/graphics/RectF;

    .line 26
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/view/CircleProgressView;->o〇0(Landroid/util/AttributeSet;)V

    return-void
.end method

.method private static final Oo08(Lcom/intsig/camscanner/view/CircleProgressView;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/16 v0, 0x8

    .line 7
    .line 8
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final oO80()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->o0:Landroid/graphics/Paint;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->OO:F

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->o0:Landroid/graphics/Paint;

    .line 9
    .line 10
    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->o0:Landroid/graphics/Paint;

    .line 16
    .line 17
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 23
    .line 24
    iget v1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇08O〇00〇o:F

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 30
    .line 31
    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 37
    .line 38
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 39
    .line 40
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private final o〇0(Landroid/util/AttributeSet;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/view/CircleProgressView;->oO80()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CircleProgressView;->update()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/view/CircleProgressView;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/view/CircleProgressView;->Oo08(Lcom/intsig/camscanner/view/CircleProgressView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final 〇o00〇〇Oo(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 8
    .line 9
    iget v1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->oOo〇8o008:I

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 15
    .line 16
    iget v1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇08O〇00〇o:F

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 19
    .line 20
    .line 21
    iget v4, p0, Lcom/intsig/camscanner/view/CircleProgressView;->O8o08O8O:F

    .line 22
    .line 23
    iget v5, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇0O:F

    .line 24
    .line 25
    const/4 v6, 0x0

    .line 26
    iget-object v7, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 27
    .line 28
    move-object v2, p1

    .line 29
    move-object v3, p2

    .line 30
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final 〇o〇(Landroid/graphics/Canvas;IILandroid/graphics/RectF;)V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->o0:Landroid/graphics/Paint;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->OO〇00〇8oO:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->o0:Landroid/graphics/Paint;

    .line 9
    .line 10
    iget v1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->OO:F

    .line 11
    .line 12
    const/high16 v2, 0x3f800000    # 1.0f

    .line 13
    .line 14
    add-float/2addr v1, v2

    .line 15
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 16
    .line 17
    .line 18
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->oOo0:Z

    .line 19
    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->o0:Landroid/graphics/Paint;

    .line 23
    .line 24
    new-instance v1, Landroid/graphics/SweepGradient;

    .line 25
    .line 26
    int-to-float p2, p2

    .line 27
    int-to-float p3, p3

    .line 28
    iget v2, p0, Lcom/intsig/camscanner/view/CircleProgressView;->o8〇OO0〇0o:I

    .line 29
    .line 30
    iget v3, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇8〇oO〇〇8o:I

    .line 31
    .line 32
    invoke-direct {v1, p2, p3, v2, v3}, Landroid/graphics/SweepGradient;-><init>(FFII)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 36
    .line 37
    .line 38
    :cond_0
    iget v6, p0, Lcom/intsig/camscanner/view/CircleProgressView;->O8o08O8O:F

    .line 39
    .line 40
    iget v7, p0, Lcom/intsig/camscanner/view/CircleProgressView;->o8oOOo:F

    .line 41
    .line 42
    const/4 v8, 0x0

    .line 43
    iget-object v9, p0, Lcom/intsig/camscanner/view/CircleProgressView;->o0:Landroid/graphics/Paint;

    .line 44
    .line 45
    move-object v4, p1

    .line 46
    move-object v5, p4

    .line 47
    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method


# virtual methods
.method public final O8()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const/4 v0, 0x2

    .line 9
    new-array v0, v0, [F

    .line 10
    .line 11
    fill-array-data v0, :array_0

    .line 12
    .line 13
    .line 14
    const-string v1, "alpha"

    .line 15
    .line 16
    invoke-static {p0, v1, v0}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/camscanner/view/CircleProgressView$hideAnim$1;

    .line 21
    .line 22
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/view/CircleProgressView$hideAnim$1;-><init>(Lcom/intsig/camscanner/view/CircleProgressView;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 26
    .line 27
    .line 28
    const-wide/16 v1, 0x64

    .line 29
    .line 30
    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 34
    .line 35
    .line 36
    new-instance v0, Lcom/intsig/camscanner/view/o〇0;

    .line 37
    .line 38
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/view/o〇0;-><init>(Lcom/intsig/camscanner/view/CircleProgressView;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 42
    .line 43
    .line 44
    return-void

    .line 45
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public final getProgress()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->O0O:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    const/4 v1, 0x2

    .line 9
    div-int/2addr v0, v1

    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    div-int/2addr v2, v1

    .line 15
    iget v3, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇080OO8〇0:F

    .line 16
    .line 17
    const/4 v4, 0x0

    .line 18
    cmpg-float v4, v3, v4

    .line 19
    .line 20
    if-nez v4, :cond_1

    .line 21
    .line 22
    const/4 v4, 0x1

    .line 23
    goto :goto_0

    .line 24
    :cond_1
    const/4 v4, 0x0

    .line 25
    :goto_0
    if-nez v4, :cond_2

    .line 26
    .line 27
    int-to-float v4, v0

    .line 28
    int-to-float v5, v2

    .line 29
    invoke-virtual {p1, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 30
    .line 31
    .line 32
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    .line 37
    .line 38
    .line 39
    move-result v4

    .line 40
    sub-int/2addr v3, v4

    .line 41
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    .line 42
    .line 43
    .line 44
    move-result v4

    .line 45
    sub-int/2addr v3, v4

    .line 46
    int-to-float v3, v3

    .line 47
    iget v4, p0, Lcom/intsig/camscanner/view/CircleProgressView;->OO:F

    .line 48
    .line 49
    int-to-float v1, v1

    .line 50
    mul-float v4, v4, v1

    .line 51
    .line 52
    sub-float/2addr v3, v4

    .line 53
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 54
    .line 55
    .line 56
    move-result v4

    .line 57
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    .line 58
    .line 59
    .line 60
    move-result v5

    .line 61
    sub-int/2addr v4, v5

    .line 62
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    .line 63
    .line 64
    .line 65
    move-result v5

    .line 66
    sub-int/2addr v4, v5

    .line 67
    int-to-float v4, v4

    .line 68
    iget v5, p0, Lcom/intsig/camscanner/view/CircleProgressView;->OO:F

    .line 69
    .line 70
    mul-float v5, v5, v1

    .line 71
    .line 72
    sub-float/2addr v4, v5

    .line 73
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    .line 74
    .line 75
    .line 76
    move-result v3

    .line 77
    div-float/2addr v3, v1

    .line 78
    float-to-double v3, v3

    .line 79
    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    .line 80
    .line 81
    .line 82
    move-result-wide v3

    .line 83
    double-to-float v1, v3

    .line 84
    iput v1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->o〇00O:F

    .line 85
    .line 86
    iget-object v3, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇o0O:Landroid/graphics/RectF;

    .line 87
    .line 88
    int-to-float v4, v0

    .line 89
    sub-float v5, v4, v1

    .line 90
    .line 91
    int-to-float v6, v2

    .line 92
    sub-float v7, v6, v1

    .line 93
    .line 94
    add-float/2addr v4, v1

    .line 95
    add-float/2addr v6, v1

    .line 96
    invoke-virtual {v3, v5, v7, v4, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 97
    .line 98
    .line 99
    iget-object v1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇o0O:Landroid/graphics/RectF;

    .line 100
    .line 101
    invoke-direct {p0, p1, v1}, Lcom/intsig/camscanner/view/CircleProgressView;->〇o00〇〇Oo(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V

    .line 102
    .line 103
    .line 104
    iget-object v1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇o0O:Landroid/graphics/RectF;

    .line 105
    .line 106
    invoke-direct {p0, p1, v0, v2, v1}, Lcom/intsig/camscanner/view/CircleProgressView;->〇o〇(Landroid/graphics/Canvas;IILandroid/graphics/RectF;)V

    .line 107
    .line 108
    .line 109
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public final setBackColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->oOo〇8o008:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setDrawDegree(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇O〇〇O8:F

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setDuration(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->ooo0〇〇O:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setEndColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setMaxProgress(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇〇08O:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setOnProgressChangeListener(Lcom/intsig/camscanner/view/CircleProgressView$OnProgressChangedListener;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/view/CircleProgressView$OnProgressChangedListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "listener"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setProgress(F)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    cmpg-float v0, p1, v0

    .line 3
    .line 4
    if-ltz v0, :cond_2

    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->O0O:F

    .line 7
    .line 8
    cmpg-float v0, p1, v0

    .line 9
    .line 10
    if-gez v0, :cond_0

    .line 11
    .line 12
    goto :goto_1

    .line 13
    :cond_0
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->O0O:F

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CircleProgressView;->update()V

    .line 16
    .line 17
    .line 18
    const/high16 v0, 0x42c80000    # 100.0f

    .line 19
    .line 20
    cmpg-float p1, p1, v0

    .line 21
    .line 22
    if-nez p1, :cond_1

    .line 23
    .line 24
    const/4 p1, 0x1

    .line 25
    goto :goto_0

    .line 26
    :cond_1
    const/4 p1, 0x0

    .line 27
    :goto_0
    if-eqz p1, :cond_2

    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CircleProgressView;->O8()V

    .line 30
    .line 31
    .line 32
    :cond_2
    :goto_1
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public final setRadius(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->o〇00O:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setRotateDegree(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇080OO8〇0:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setStartColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->o8〇OO0〇0o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setStartDegree(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->O8o08O8O:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setStrokeWidth(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->OO:F

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/view/CircleProgressView;->oO80()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setSweepDegree(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇0O:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setUseGradient(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->oOo0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final update()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->O0O:F

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇〇08O:F

    .line 4
    .line 5
    invoke-static {v0, v1}, Lkotlin/ranges/RangesKt;->Oo08(FF)F

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->O0O:F

    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇〇08O:F

    .line 12
    .line 13
    div-float/2addr v0, v1

    .line 14
    iget v1, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇0O:F

    .line 15
    .line 16
    mul-float v0, v0, v1

    .line 17
    .line 18
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->o8oOOo:F

    .line 19
    .line 20
    const/4 v0, 0x0

    .line 21
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->〇O〇〇O8:F

    .line 22
    .line 23
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public final 〇〇888()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressView;->O0O:F

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public Lcom/intsig/camscanner/view/MaskView;
.super Landroid/view/View;
.source "MaskView.java"


# static fields
.field private static oOo0:I = -0x40000000

.field private static oOo〇8o008:Ljava/lang/String; = "MaskView"


# instance fields
.field private O8o08O8O:Z

.field private OO:I

.field private o0:Landroid/graphics/drawable/ShapeDrawable;

.field private o〇00O:Landroid/graphics/Paint;

.field private 〇080OO8〇0:Landroid/graphics/PorterDuffXfermode;

.field private 〇08O〇00〇o:I

.field protected 〇0O:Landroid/graphics/Rect;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private 〇OOo8〇0:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    const/4 p2, 0x0

    .line 5
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/MaskView;->〇OOo8〇0:Z

    .line 6
    .line 7
    const/4 v0, 0x2

    .line 8
    iput v0, p0, Lcom/intsig/camscanner/view/MaskView;->OO:I

    .line 9
    .line 10
    const v0, -0xe64364

    .line 11
    .line 12
    .line 13
    iput v0, p0, Lcom/intsig/camscanner/view/MaskView;->〇08O〇00〇o:I

    .line 14
    .line 15
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/MaskView;->O8o08O8O:Z

    .line 16
    .line 17
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/MaskView;->〇080(Landroid/content/Context;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇080(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const v1, 0x7f06065f

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    sput v0, Lcom/intsig/camscanner/view/MaskView;->oOo0:I

    .line 17
    .line 18
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    .line 19
    .line 20
    new-instance v1, Landroid/graphics/drawable/shapes/RectShape;

    .line 21
    .line 22
    invoke-direct {v1}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    .line 23
    .line 24
    .line 25
    invoke-direct {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 26
    .line 27
    .line 28
    iput-object v0, p0, Lcom/intsig/camscanner/view/MaskView;->o0:Landroid/graphics/drawable/ShapeDrawable;

    .line 29
    .line 30
    new-instance v0, Landroid/graphics/Paint;

    .line 31
    .line 32
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 33
    .line 34
    .line 35
    iput-object v0, p0, Lcom/intsig/camscanner/view/MaskView;->o〇00O:Landroid/graphics/Paint;

    .line 36
    .line 37
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 40
    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/view/MaskView;->o〇00O:Landroid/graphics/Paint;

    .line 43
    .line 44
    iget v1, p0, Lcom/intsig/camscanner/view/MaskView;->〇08O〇00〇o:I

    .line 45
    .line 46
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 47
    .line 48
    .line 49
    const/4 v0, 0x2

    .line 50
    invoke-static {p1, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    iput p1, p0, Lcom/intsig/camscanner/view/MaskView;->OO:I

    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/view/MaskView;->o〇00O:Landroid/graphics/Paint;

    .line 57
    .line 58
    int-to-float p1, p1

    .line 59
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 60
    .line 61
    .line 62
    new-instance p1, Landroid/graphics/PorterDuffXfermode;

    .line 63
    .line 64
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    .line 65
    .line 66
    invoke-direct {p1, v0}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    .line 67
    .line 68
    .line 69
    iput-object p1, p0, Lcom/intsig/camscanner/view/MaskView;->〇080OO8〇0:Landroid/graphics/PorterDuffXfermode;

    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/view/MaskView;->oOo〇8o008:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "onDraw"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    sget v0, Lcom/intsig/camscanner/view/MaskView;->oOo0:I

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 14
    .line 15
    .line 16
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/MaskView;->〇OOo8〇0:Z

    .line 17
    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/view/MaskView;->o0:Landroid/graphics/drawable/ShapeDrawable;

    .line 21
    .line 22
    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/view/MaskView;->〇080OO8〇0:Landroid/graphics/PorterDuffXfermode;

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/view/MaskView;->o0:Landroid/graphics/drawable/ShapeDrawable;

    .line 32
    .line 33
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 34
    .line 35
    .line 36
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/MaskView;->O8o08O8O:Z

    .line 37
    .line 38
    if-eqz v0, :cond_0

    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/view/MaskView;->o0:Landroid/graphics/drawable/ShapeDrawable;

    .line 41
    .line 42
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/intsig/camscanner/view/MaskView;->o〇00O:Landroid/graphics/Paint;

    .line 47
    .line 48
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 49
    .line 50
    .line 51
    :cond_0
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setShapeDrawable(Landroid/graphics/drawable/shapes/Shape;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/MaskView;->o0:Landroid/graphics/drawable/ShapeDrawable;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ShapeDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setStrokecolor(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/MaskView;->〇08O〇00〇o:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/view/MaskView;->o〇00O:Landroid/graphics/Paint;

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇o00〇〇Oo(Landroid/graphics/Rect;Z)V
    .locals 4

    .line 1
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/MaskView;->O8o08O8O:Z

    .line 2
    .line 3
    const/4 p2, 0x1

    .line 4
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/MaskView;->〇OOo8〇0:Z

    .line 5
    .line 6
    iget p2, p0, Lcom/intsig/camscanner/view/MaskView;->OO:I

    .line 7
    .line 8
    div-int/lit8 p2, p2, 0x2

    .line 9
    .line 10
    new-instance v0, Landroid/graphics/Rect;

    .line 11
    .line 12
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 13
    .line 14
    add-int/2addr v1, p2

    .line 15
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 16
    .line 17
    add-int/2addr v2, p2

    .line 18
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 19
    .line 20
    sub-int/2addr v3, p2

    .line 21
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 22
    .line 23
    sub-int/2addr p1, p2

    .line 24
    invoke-direct {v0, v1, v2, v3, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 25
    .line 26
    .line 27
    iput-object v0, p0, Lcom/intsig/camscanner/view/MaskView;->〇0O:Landroid/graphics/Rect;

    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/view/MaskView;->o0:Landroid/graphics/drawable/ShapeDrawable;

    .line 30
    .line 31
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇o〇(Landroid/graphics/Rect;ZI)V
    .locals 4

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    new-array v0, v0, [F

    .line 4
    .line 5
    int-to-float p3, p3

    .line 6
    const/4 v1, 0x0

    .line 7
    aput p3, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    aput p3, v0, v1

    .line 11
    .line 12
    const/4 v2, 0x2

    .line 13
    aput p3, v0, v2

    .line 14
    .line 15
    const/4 v3, 0x3

    .line 16
    aput p3, v0, v3

    .line 17
    .line 18
    const/4 v3, 0x4

    .line 19
    aput p3, v0, v3

    .line 20
    .line 21
    const/4 v3, 0x5

    .line 22
    aput p3, v0, v3

    .line 23
    .line 24
    const/4 v3, 0x6

    .line 25
    aput p3, v0, v3

    .line 26
    .line 27
    const/4 v3, 0x7

    .line 28
    aput p3, v0, v3

    .line 29
    .line 30
    new-instance p3, Landroid/graphics/drawable/shapes/RoundRectShape;

    .line 31
    .line 32
    const/4 v3, 0x0

    .line 33
    invoke-direct {p3, v0, v3, v3}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    .line 34
    .line 35
    .line 36
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    .line 37
    .line 38
    invoke-direct {v0, p3}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 39
    .line 40
    .line 41
    iput-object v0, p0, Lcom/intsig/camscanner/view/MaskView;->o0:Landroid/graphics/drawable/ShapeDrawable;

    .line 42
    .line 43
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/MaskView;->O8o08O8O:Z

    .line 44
    .line 45
    iput-boolean v1, p0, Lcom/intsig/camscanner/view/MaskView;->〇OOo8〇0:Z

    .line 46
    .line 47
    iget p2, p0, Lcom/intsig/camscanner/view/MaskView;->OO:I

    .line 48
    .line 49
    div-int/2addr p2, v2

    .line 50
    new-instance p3, Landroid/graphics/Rect;

    .line 51
    .line 52
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 53
    .line 54
    add-int/2addr v0, p2

    .line 55
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 56
    .line 57
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 58
    .line 59
    sub-int/2addr v2, p2

    .line 60
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 61
    .line 62
    invoke-direct {p3, v0, v1, v2, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 63
    .line 64
    .line 65
    iput-object p3, p0, Lcom/intsig/camscanner/view/MaskView;->〇0O:Landroid/graphics/Rect;

    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/camscanner/view/MaskView;->o0:Landroid/graphics/drawable/ShapeDrawable;

    .line 68
    .line 69
    invoke-virtual {p1, p3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 73
    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

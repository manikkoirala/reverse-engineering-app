.class Lcom/intsig/camscanner/view/BorderView$BorderScroller;
.super Ljava/lang/Object;
.source "BorderView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/view/BorderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BorderScroller"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/BorderView$BorderScroller$Mode;
    }
.end annotation


# instance fields
.field private O8:[I

.field private OO0o〇〇〇〇0:Z

.field private Oo08:[I

.field private oO80:F

.field private o〇0:J

.field private final 〇080:[I

.field private final 〇80〇808〇O:Landroid/view/animation/Interpolator;

.field private 〇8o8o〇:Lcom/intsig/camscanner/view/BorderView$BorderScroller$Mode;

.field private 〇o00〇〇Oo:[I

.field private 〇o〇:[I

.field private 〇〇888:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/BorderView$BorderScroller;-><init>(Landroid/view/animation/Interpolator;)V

    return-void
.end method

.method public constructor <init>(Landroid/view/animation/Interpolator;)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x8

    new-array v1, v0, [I

    .line 3
    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇080:[I

    new-array v1, v0, [I

    .line 4
    iput-object v1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o00〇〇Oo:[I

    new-array v1, v0, [I

    .line 5
    iput-object v1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o〇:[I

    new-array v1, v0, [I

    .line 6
    iput-object v1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->O8:[I

    new-array v0, v0, [I

    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->Oo08:[I

    if-nez p1, :cond_0

    .line 8
    new-instance p1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {p1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇80〇808〇O:Landroid/view/animation/Interpolator;

    goto :goto_0

    .line 9
    :cond_0
    iput-object p1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇80〇808〇O:Landroid/view/animation/Interpolator;

    .line 10
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->O8()V

    .line 11
    sget-object p1, Lcom/intsig/camscanner/view/BorderView$BorderScroller$Mode;->〇OOo8〇0:Lcom/intsig/camscanner/view/BorderView$BorderScroller$Mode;

    iput-object p1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇8o8o〇:Lcom/intsig/camscanner/view/BorderView$BorderScroller$Mode;

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public O8()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇080:[I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o00〇〇Oo:[I

    .line 4
    .line 5
    array-length v2, v1

    .line 6
    const/4 v3, 0x0

    .line 7
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇080:[I

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o〇:[I

    .line 13
    .line 14
    array-length v2, v1

    .line 15
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇080:[I

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->O8:[I

    .line 21
    .line 22
    array-length v2, v1

    .line 23
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇080:[I

    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->Oo08:[I

    .line 29
    .line 30
    array-length v2, v1

    .line 31
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 32
    .line 33
    .line 34
    const/4 v0, 0x1

    .line 35
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->OO0o〇〇〇〇0:Z

    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public Oo08([II)V
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->OO0o〇〇〇〇0:Z

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o〇:[I

    .line 5
    .line 6
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o〇([I)Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, 0x0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o00〇〇Oo:[I

    .line 14
    .line 15
    array-length v2, v0

    .line 16
    invoke-static {p1, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o〇:[I

    .line 20
    .line 21
    array-length v2, v0

    .line 22
    invoke-static {p1, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->O8:[I

    .line 26
    .line 27
    array-length v2, v0

    .line 28
    invoke-static {p1, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇080:[I

    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->Oo08:[I

    .line 34
    .line 35
    array-length v2, v0

    .line 36
    invoke-static {p1, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 37
    .line 38
    .line 39
    sget-object p1, Lcom/intsig/camscanner/view/BorderView$BorderScroller$Mode;->〇OOo8〇0:Lcom/intsig/camscanner/view/BorderView$BorderScroller$Mode;

    .line 40
    .line 41
    iput-object p1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇8o8o〇:Lcom/intsig/camscanner/view/BorderView$BorderScroller$Mode;

    .line 42
    .line 43
    goto :goto_1

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o〇:[I

    .line 45
    .line 46
    iget-object v2, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o00〇〇Oo:[I

    .line 47
    .line 48
    array-length v3, v2

    .line 49
    invoke-static {v0, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 50
    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->O8:[I

    .line 53
    .line 54
    array-length v2, v0

    .line 55
    invoke-static {p1, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56
    .line 57
    .line 58
    const/4 p1, 0x0

    .line 59
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->O8:[I

    .line 60
    .line 61
    array-length v2, v0

    .line 62
    if-ge p1, v2, :cond_1

    .line 63
    .line 64
    iget-object v2, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->Oo08:[I

    .line 65
    .line 66
    aget v0, v0, p1

    .line 67
    .line 68
    iget-object v3, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o00〇〇Oo:[I

    .line 69
    .line 70
    aget v3, v3, p1

    .line 71
    .line 72
    sub-int/2addr v0, v3

    .line 73
    aput v0, v2, p1

    .line 74
    .line 75
    add-int/lit8 p1, p1, 0x1

    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/view/BorderView$BorderScroller$Mode;->o0:Lcom/intsig/camscanner/view/BorderView$BorderScroller$Mode;

    .line 79
    .line 80
    iput-object p1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇8o8o〇:Lcom/intsig/camscanner/view/BorderView$BorderScroller$Mode;

    .line 81
    .line 82
    :goto_1
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    .line 83
    .line 84
    .line 85
    move-result-wide v2

    .line 86
    iput-wide v2, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->o〇0:J

    .line 87
    .line 88
    iput p2, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇〇888:I

    .line 89
    .line 90
    const/high16 p1, 0x3f800000    # 1.0f

    .line 91
    .line 92
    int-to-float p2, p2

    .line 93
    div-float/2addr p1, p2

    .line 94
    iput p1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->oO80:F

    .line 95
    .line 96
    iput-boolean v1, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->OO0o〇〇〇〇0:Z

    .line 97
    .line 98
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public 〇080()Z
    .locals 7

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->OO0o〇〇〇〇0:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/view/BorderView$1;->〇080:[I

    .line 8
    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇8o8o〇:Lcom/intsig/camscanner/view/BorderView$BorderScroller$Mode;

    .line 10
    .line 11
    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    aget v0, v0, v2

    .line 16
    .line 17
    const/4 v2, 0x1

    .line 18
    if-eq v0, v2, :cond_3

    .line 19
    .line 20
    const/4 v3, 0x2

    .line 21
    if-eq v0, v3, :cond_1

    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_1
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    .line 25
    .line 26
    .line 27
    move-result-wide v3

    .line 28
    iget-wide v5, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->o〇0:J

    .line 29
    .line 30
    sub-long/2addr v3, v5

    .line 31
    long-to-int v0, v3

    .line 32
    iget v3, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇〇888:I

    .line 33
    .line 34
    if-ge v0, v3, :cond_2

    .line 35
    .line 36
    iget-object v3, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇80〇808〇O:Landroid/view/animation/Interpolator;

    .line 37
    .line 38
    int-to-float v0, v0

    .line 39
    iget v4, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->oO80:F

    .line 40
    .line 41
    mul-float v0, v0, v4

    .line 42
    .line 43
    invoke-interface {v3, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    :goto_0
    iget-object v3, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->O8:[I

    .line 48
    .line 49
    array-length v3, v3

    .line 50
    if-ge v1, v3, :cond_4

    .line 51
    .line 52
    iget-object v3, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o〇:[I

    .line 53
    .line 54
    iget-object v4, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o00〇〇Oo:[I

    .line 55
    .line 56
    aget v4, v4, v1

    .line 57
    .line 58
    iget-object v5, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->Oo08:[I

    .line 59
    .line 60
    aget v5, v5, v1

    .line 61
    .line 62
    int-to-float v5, v5

    .line 63
    mul-float v5, v5, v0

    .line 64
    .line 65
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 66
    .line 67
    .line 68
    move-result v5

    .line 69
    add-int/2addr v4, v5

    .line 70
    aput v4, v3, v1

    .line 71
    .line 72
    add-int/lit8 v1, v1, 0x1

    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->O8:[I

    .line 76
    .line 77
    iget-object v3, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o〇:[I

    .line 78
    .line 79
    array-length v4, v0

    .line 80
    invoke-static {v0, v1, v3, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 81
    .line 82
    .line 83
    iput-boolean v2, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->OO0o〇〇〇〇0:Z

    .line 84
    .line 85
    goto :goto_1

    .line 86
    :cond_3
    iput-boolean v2, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->OO0o〇〇〇〇0:Z

    .line 87
    .line 88
    :cond_4
    :goto_1
    return v2
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public 〇o00〇〇Oo()[I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o〇:[I

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇([I)Z
    .locals 5

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eqz p1, :cond_2

    .line 3
    .line 4
    array-length v1, p1

    .line 5
    if-nez v1, :cond_0

    .line 6
    .line 7
    goto :goto_1

    .line 8
    :cond_0
    array-length v1, p1

    .line 9
    const/4 v2, 0x0

    .line 10
    const/4 v3, 0x0

    .line 11
    :goto_0
    if-ge v3, v1, :cond_2

    .line 12
    .line 13
    aget v4, p1, v3

    .line 14
    .line 15
    if-eqz v4, :cond_1

    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    goto :goto_1

    .line 19
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_2
    :goto_1
    return v0
    .line 23
    .line 24
.end method

.class public final Lcom/intsig/camscanner/view/CapturePreviewAssistView;
.super Landroid/view/View;
.source "CapturePreviewAssistView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8o08O8O:I

.field private OO:I

.field private OO〇00〇8oO:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0:I

.field private o8〇OO0〇0o:I

.field private oOo0:I

.field private oOo〇8o008:Landroid/graphics/Path;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private ooo0〇〇O:Landroid/graphics/Path;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:I

.field private 〇080OO8〇0:Landroid/graphics/RectF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:I

.field private 〇0O:Landroid/graphics/Path;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇8〇oO〇〇8o:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 v0, 0x18

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    const/4 v0, 0x3

    .line 3
    iput v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    const/16 v0, 0x40

    .line 4
    iput v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇08O〇00〇o:I

    const/16 v0, 0x48

    .line 6
    iput v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 7
    iput v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->O8o08O8O:I

    .line 8
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇080OO8〇0:Landroid/graphics/RectF;

    .line 9
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇0O:Landroid/graphics/Path;

    .line 10
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->oOo〇8o008:Landroid/graphics/Path;

    const/high16 v0, -0x67000000

    .line 11
    iput v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->oOo0:I

    .line 12
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO〇00〇8oO:Landroid/graphics/Paint;

    const/4 v0, -0x1

    .line 13
    iput v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o8〇OO0〇0o:I

    .line 14
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇8〇oO〇〇8o:Landroid/graphics/Paint;

    .line 15
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇〇888(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 p3, 0x18

    .line 18
    iput p3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    const/4 p3, 0x3

    .line 19
    iput p3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    const/16 p3, 0x40

    .line 20
    iput p3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 21
    iput p3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇08O〇00〇o:I

    const/16 p3, 0x48

    .line 22
    iput p3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 23
    iput p3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->O8o08O8O:I

    .line 24
    new-instance p3, Landroid/graphics/RectF;

    invoke-direct {p3}, Landroid/graphics/RectF;-><init>()V

    iput-object p3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇080OO8〇0:Landroid/graphics/RectF;

    .line 25
    new-instance p3, Landroid/graphics/Path;

    invoke-direct {p3}, Landroid/graphics/Path;-><init>()V

    iput-object p3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇0O:Landroid/graphics/Path;

    .line 26
    new-instance p3, Landroid/graphics/Path;

    invoke-direct {p3}, Landroid/graphics/Path;-><init>()V

    iput-object p3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->oOo〇8o008:Landroid/graphics/Path;

    const/high16 p3, -0x67000000

    .line 27
    iput p3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->oOo0:I

    .line 28
    new-instance p3, Landroid/graphics/Paint;

    invoke-direct {p3}, Landroid/graphics/Paint;-><init>()V

    iput-object p3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO〇00〇8oO:Landroid/graphics/Paint;

    const/4 p3, -0x1

    .line 29
    iput p3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o8〇OO0〇0o:I

    .line 30
    new-instance p3, Landroid/graphics/Paint;

    invoke-direct {p3}, Landroid/graphics/Paint;-><init>()V

    iput-object p3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇8〇oO〇〇8o:Landroid/graphics/Paint;

    .line 31
    new-instance p3, Landroid/graphics/Path;

    invoke-direct {p3}, Landroid/graphics/Path;-><init>()V

    iput-object p3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇〇888(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private final O8(Landroid/graphics/Canvas;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇080OO8〇0:Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    int-to-float v1, v1

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    int-to-float v2, v2

    .line 13
    const/4 v3, 0x0

    .line 14
    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->oOo〇8o008:Landroid/graphics/Path;

    .line 18
    .line 19
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->oOo〇8o008:Landroid/graphics/Path;

    .line 23
    .line 24
    iget v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 25
    .line 26
    int-to-float v1, v1

    .line 27
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 28
    .line 29
    int-to-float v2, v2

    .line 30
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->oOo〇8o008:Landroid/graphics/Path;

    .line 34
    .line 35
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇08O〇00〇o:I

    .line 40
    .line 41
    sub-int/2addr v1, v2

    .line 42
    int-to-float v1, v1

    .line 43
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 44
    .line 45
    int-to-float v2, v2

    .line 46
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 47
    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->oOo〇8o008:Landroid/graphics/Path;

    .line 50
    .line 51
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇08O〇00〇o:I

    .line 56
    .line 57
    sub-int/2addr v1, v2

    .line 58
    int-to-float v1, v1

    .line 59
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    iget v3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->O8o08O8O:I

    .line 64
    .line 65
    sub-int/2addr v2, v3

    .line 66
    int-to-float v2, v2

    .line 67
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 68
    .line 69
    .line 70
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->oOo〇8o008:Landroid/graphics/Path;

    .line 71
    .line 72
    iget v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 73
    .line 74
    int-to-float v1, v1

    .line 75
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 76
    .line 77
    .line 78
    move-result v2

    .line 79
    iget v3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->O8o08O8O:I

    .line 80
    .line 81
    sub-int/2addr v2, v3

    .line 82
    int-to-float v2, v2

    .line 83
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 84
    .line 85
    .line 86
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->oOo〇8o008:Landroid/graphics/Path;

    .line 87
    .line 88
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 89
    .line 90
    .line 91
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇0O:Landroid/graphics/Path;

    .line 92
    .line 93
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 94
    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇0O:Landroid/graphics/Path;

    .line 97
    .line 98
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇080OO8〇0:Landroid/graphics/RectF;

    .line 99
    .line 100
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 101
    .line 102
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 103
    .line 104
    .line 105
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇0O:Landroid/graphics/Path;

    .line 106
    .line 107
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->oOo〇8o008:Landroid/graphics/Path;

    .line 108
    .line 109
    invoke-virtual {v0, v1}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;)V

    .line 110
    .line 111
    .line 112
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇0O:Landroid/graphics/Path;

    .line 113
    .line 114
    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    .line 115
    .line 116
    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 117
    .line 118
    .line 119
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇0O:Landroid/graphics/Path;

    .line 120
    .line 121
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 122
    .line 123
    .line 124
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇0O:Landroid/graphics/Path;

    .line 125
    .line 126
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 127
    .line 128
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 129
    .line 130
    .line 131
    return-void
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private final Oo08(Landroid/graphics/Canvas;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iget v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇08O〇00〇o:I

    .line 11
    .line 12
    sub-int/2addr v0, v1

    .line 13
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->O8o08O8O:I

    .line 18
    .line 19
    sub-int/2addr v1, v2

    .line 20
    iget-object v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 21
    .line 22
    int-to-float v3, v0

    .line 23
    int-to-float v4, v1

    .line 24
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 25
    .line 26
    .line 27
    iget-object v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 28
    .line 29
    iget v5, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 30
    .line 31
    sub-int v5, v0, v5

    .line 32
    .line 33
    int-to-float v5, v5

    .line 34
    invoke-virtual {v2, v5, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 35
    .line 36
    .line 37
    iget-object v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 38
    .line 39
    iget v4, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 40
    .line 41
    sub-int v4, v0, v4

    .line 42
    .line 43
    int-to-float v4, v4

    .line 44
    iget v5, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    .line 45
    .line 46
    sub-int v5, v1, v5

    .line 47
    .line 48
    int-to-float v5, v5

    .line 49
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 50
    .line 51
    .line 52
    iget-object v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 53
    .line 54
    iget v4, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    .line 55
    .line 56
    sub-int v5, v0, v4

    .line 57
    .line 58
    int-to-float v5, v5

    .line 59
    sub-int v4, v1, v4

    .line 60
    .line 61
    int-to-float v4, v4

    .line 62
    invoke-virtual {v2, v5, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 63
    .line 64
    .line 65
    iget-object v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 66
    .line 67
    iget v4, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    .line 68
    .line 69
    sub-int/2addr v0, v4

    .line 70
    int-to-float v0, v0

    .line 71
    iget v4, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 72
    .line 73
    sub-int v4, v1, v4

    .line 74
    .line 75
    int-to-float v4, v4

    .line 76
    invoke-virtual {v2, v0, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 77
    .line 78
    .line 79
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 80
    .line 81
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 82
    .line 83
    sub-int/2addr v1, v2

    .line 84
    int-to-float v1, v1

    .line 85
    invoke-virtual {v0, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 86
    .line 87
    .line 88
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 89
    .line 90
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 91
    .line 92
    .line 93
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 94
    .line 95
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇8〇oO〇〇8o:Landroid/graphics/Paint;

    .line 96
    .line 97
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 98
    .line 99
    .line 100
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private final o〇0(Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iget v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇08O〇00〇o:I

    .line 11
    .line 12
    sub-int/2addr v0, v1

    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 14
    .line 15
    int-to-float v2, v0

    .line 16
    iget v3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 17
    .line 18
    int-to-float v3, v3

    .line 19
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 20
    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 23
    .line 24
    iget v3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 25
    .line 26
    iget v4, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 27
    .line 28
    add-int/2addr v3, v4

    .line 29
    int-to-float v3, v3

    .line 30
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 31
    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 34
    .line 35
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    .line 36
    .line 37
    sub-int v2, v0, v2

    .line 38
    .line 39
    int-to-float v2, v2

    .line 40
    iget v3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 41
    .line 42
    iget v4, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 43
    .line 44
    add-int/2addr v3, v4

    .line 45
    int-to-float v3, v3

    .line 46
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 47
    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 50
    .line 51
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    .line 52
    .line 53
    sub-int v3, v0, v2

    .line 54
    .line 55
    int-to-float v3, v3

    .line 56
    iget v4, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 57
    .line 58
    add-int/2addr v4, v2

    .line 59
    int-to-float v2, v4

    .line 60
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 61
    .line 62
    .line 63
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 64
    .line 65
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 66
    .line 67
    sub-int v2, v0, v2

    .line 68
    .line 69
    int-to-float v2, v2

    .line 70
    iget v3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 71
    .line 72
    iget v4, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    .line 73
    .line 74
    add-int/2addr v3, v4

    .line 75
    int-to-float v3, v3

    .line 76
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 77
    .line 78
    .line 79
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 80
    .line 81
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 82
    .line 83
    sub-int/2addr v0, v2

    .line 84
    int-to-float v0, v0

    .line 85
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 86
    .line 87
    int-to-float v2, v2

    .line 88
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 89
    .line 90
    .line 91
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 92
    .line 93
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 94
    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 97
    .line 98
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇8〇oO〇〇8o:Landroid/graphics/Paint;

    .line 99
    .line 100
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 101
    .line 102
    .line 103
    return-void
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private final 〇080(Landroid/graphics/Canvas;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇o〇(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇0(Landroid/graphics/Canvas;)V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->Oo08(Landroid/graphics/Canvas;)V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇o00〇〇Oo(Landroid/graphics/Canvas;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final 〇o00〇〇Oo(Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iget v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->O8o08O8O:I

    .line 11
    .line 12
    sub-int/2addr v0, v1

    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 14
    .line 15
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 16
    .line 17
    int-to-float v2, v2

    .line 18
    int-to-float v3, v0

    .line 19
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 20
    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 23
    .line 24
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 25
    .line 26
    int-to-float v2, v2

    .line 27
    iget v4, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 28
    .line 29
    sub-int v4, v0, v4

    .line 30
    .line 31
    int-to-float v4, v4

    .line 32
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 33
    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 36
    .line 37
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 38
    .line 39
    iget v4, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    .line 40
    .line 41
    add-int/2addr v2, v4

    .line 42
    int-to-float v2, v2

    .line 43
    iget v4, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 44
    .line 45
    sub-int v4, v0, v4

    .line 46
    .line 47
    int-to-float v4, v4

    .line 48
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 49
    .line 50
    .line 51
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 52
    .line 53
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 54
    .line 55
    iget v4, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    .line 56
    .line 57
    add-int/2addr v2, v4

    .line 58
    int-to-float v2, v2

    .line 59
    sub-int v4, v0, v4

    .line 60
    .line 61
    int-to-float v4, v4

    .line 62
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 63
    .line 64
    .line 65
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 66
    .line 67
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 68
    .line 69
    iget v4, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 70
    .line 71
    add-int/2addr v2, v4

    .line 72
    int-to-float v2, v2

    .line 73
    iget v4, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    .line 74
    .line 75
    sub-int/2addr v0, v4

    .line 76
    int-to-float v0, v0

    .line 77
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 78
    .line 79
    .line 80
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 81
    .line 82
    iget v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 83
    .line 84
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 85
    .line 86
    add-int/2addr v1, v2

    .line 87
    int-to-float v1, v1

    .line 88
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 89
    .line 90
    .line 91
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 92
    .line 93
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 94
    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 97
    .line 98
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇8〇oO〇〇8o:Landroid/graphics/Paint;

    .line 99
    .line 100
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 101
    .line 102
    .line 103
    return-void
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private final 〇o〇(Landroid/graphics/Canvas;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 7
    .line 8
    iget v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 9
    .line 10
    int-to-float v1, v1

    .line 11
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 12
    .line 13
    int-to-float v2, v2

    .line 14
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 18
    .line 19
    iget v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 20
    .line 21
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 22
    .line 23
    add-int/2addr v1, v2

    .line 24
    int-to-float v1, v1

    .line 25
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 26
    .line 27
    int-to-float v2, v2

    .line 28
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 32
    .line 33
    iget v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 34
    .line 35
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 36
    .line 37
    add-int/2addr v1, v2

    .line 38
    int-to-float v1, v1

    .line 39
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 40
    .line 41
    iget v3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    .line 42
    .line 43
    add-int/2addr v2, v3

    .line 44
    int-to-float v2, v2

    .line 45
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 49
    .line 50
    iget v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 51
    .line 52
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    .line 53
    .line 54
    add-int/2addr v1, v2

    .line 55
    int-to-float v1, v1

    .line 56
    iget v3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 57
    .line 58
    add-int/2addr v3, v2

    .line 59
    int-to-float v2, v3

    .line 60
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 64
    .line 65
    iget v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 66
    .line 67
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    .line 68
    .line 69
    add-int/2addr v1, v2

    .line 70
    int-to-float v1, v1

    .line 71
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 72
    .line 73
    iget v3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 74
    .line 75
    add-int/2addr v2, v3

    .line 76
    int-to-float v2, v2

    .line 77
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 78
    .line 79
    .line 80
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 81
    .line 82
    iget v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 83
    .line 84
    int-to-float v1, v1

    .line 85
    iget v2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 86
    .line 87
    iget v3, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 88
    .line 89
    add-int/2addr v2, v3

    .line 90
    int-to-float v2, v2

    .line 91
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 92
    .line 93
    .line 94
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 95
    .line 96
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 97
    .line 98
    .line 99
    iget-object v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->ooo0〇〇O:Landroid/graphics/Path;

    .line 100
    .line 101
    iget-object v1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇8〇oO〇〇8o:Landroid/graphics/Paint;

    .line 102
    .line 103
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 104
    .line 105
    .line 106
    return-void
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private final 〇〇888(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .line 1
    const/16 v0, 0x18

    .line 2
    .line 3
    const/4 v1, 0x3

    .line 4
    const/16 v2, 0x48

    .line 5
    .line 6
    const/16 v3, 0x40

    .line 7
    .line 8
    if-eqz p2, :cond_0

    .line 9
    .line 10
    sget-object v4, Lcom/intsig/camscanner/R$styleable;->CapturePreviewAssistView:[I

    .line 11
    .line 12
    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    .line 13
    .line 14
    .line 15
    move-result-object p2

    .line 16
    const-string v4, "context.obtainStyledAttr\u2026CapturePreviewAssistView)"

    .line 17
    .line 18
    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const/4 v4, 0x6

    .line 22
    invoke-static {p1, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    invoke-virtual {p2, v4, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    iput v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 31
    .line 32
    const/4 v0, 0x5

    .line 33
    invoke-static {p1, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    invoke-virtual {p2, v0, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    iput v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    .line 42
    .line 43
    const/4 v0, 0x2

    .line 44
    invoke-static {p1, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    invoke-virtual {p2, v0, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    iput v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 53
    .line 54
    const/4 v0, 0x1

    .line 55
    invoke-static {p1, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 56
    .line 57
    .line 58
    move-result v3

    .line 59
    invoke-virtual {p2, v0, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    iput v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇08O〇00〇o:I

    .line 64
    .line 65
    invoke-static {p1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    invoke-virtual {p2, v1, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    iput v0, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 74
    .line 75
    const/4 v0, 0x0

    .line 76
    invoke-static {p1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 77
    .line 78
    .line 79
    move-result p1

    .line 80
    invoke-virtual {p2, v0, p1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 81
    .line 82
    .line 83
    move-result p1

    .line 84
    iput p1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->O8o08O8O:I

    .line 85
    .line 86
    const/4 p1, 0x7

    .line 87
    const/high16 v0, -0x67000000

    .line 88
    .line 89
    invoke-virtual {p2, p1, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 90
    .line 91
    .line 92
    move-result p1

    .line 93
    iput p1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->oOo0:I

    .line 94
    .line 95
    const/4 p1, 0x4

    .line 96
    const/4 v0, -0x1

    .line 97
    invoke-virtual {p2, p1, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 98
    .line 99
    .line 100
    move-result p1

    .line 101
    iput p1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o8〇OO0〇0o:I

    .line 102
    .line 103
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 104
    .line 105
    .line 106
    goto :goto_0

    .line 107
    :cond_0
    invoke-static {p1, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 108
    .line 109
    .line 110
    move-result p2

    .line 111
    iput p2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o0:I

    .line 112
    .line 113
    invoke-static {p1, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 114
    .line 115
    .line 116
    move-result p2

    .line 117
    iput p2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇OOo8〇0:I

    .line 118
    .line 119
    invoke-static {p1, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 120
    .line 121
    .line 122
    move-result p2

    .line 123
    iput p2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO:I

    .line 124
    .line 125
    invoke-static {p1, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 126
    .line 127
    .line 128
    move-result p2

    .line 129
    iput p2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇08O〇00〇o:I

    .line 130
    .line 131
    invoke-static {p1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 132
    .line 133
    .line 134
    move-result p2

    .line 135
    iput p2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o〇00O:I

    .line 136
    .line 137
    invoke-static {p1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 138
    .line 139
    .line 140
    move-result p1

    .line 141
    iput p1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->O8o08O8O:I

    .line 142
    .line 143
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 144
    .line 145
    sget-object p2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 146
    .line 147
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 148
    .line 149
    .line 150
    iget-object p1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 151
    .line 152
    iget p2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->oOo0:I

    .line 153
    .line 154
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 155
    .line 156
    .line 157
    iget-object p1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇8〇oO〇〇8o:Landroid/graphics/Paint;

    .line 158
    .line 159
    sget-object p2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 160
    .line 161
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 162
    .line 163
    .line 164
    iget-object p1, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇8〇oO〇〇8o:Landroid/graphics/Paint;

    .line 165
    .line 166
    iget p2, p0, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->o8〇OO0〇0o:I

    .line 167
    .line 168
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 169
    .line 170
    .line 171
    return-void
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-lez v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-lez v0, :cond_0

    .line 17
    .line 18
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->O8(Landroid/graphics/Canvas;)V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/CapturePreviewAssistView;->〇080(Landroid/graphics/Canvas;)V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
.end method

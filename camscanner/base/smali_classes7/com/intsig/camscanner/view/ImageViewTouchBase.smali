.class public abstract Lcom/intsig/camscanner/view/ImageViewTouchBase;
.super Lcom/intsig/view/SafeImageView;
.source "ImageViewTouchBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/ImageViewTouchBase$Recycler;
    }
.end annotation


# instance fields
.field protected O0O:Landroid/os/Handler;

.field protected O8o08O8O:Landroid/graphics/Matrix;

.field OO〇00〇8oO:I

.field private o8oOOo:Ljava/lang/Runnable;

.field protected o8〇OO0〇0o:F

.field oOo0:I

.field protected final oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

.field private ooo0〇〇O:Lcom/intsig/camscanner/view/ImageViewTouchBase$Recycler;

.field protected o〇00O:Landroid/graphics/Matrix;

.field private final 〇080OO8〇0:Landroid/graphics/Matrix;

.field private final 〇0O:[F

.field 〇8〇oO〇〇8o:Z

.field private 〇O〇〇O8:F

.field private 〇o0O:Landroid/graphics/RectF;

.field private 〇〇08O:Lcom/intsig/camscanner/loadimage/RotateBitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/view/SafeImageView;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o〇00O:Landroid/graphics/Matrix;

    .line 3
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->O8o08O8O:Landroid/graphics/Matrix;

    .line 4
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇080OO8〇0:Landroid/graphics/Matrix;

    const/16 p1, 0x9

    new-array p1, p1, [F

    .line 5
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇0O:[F

    .line 6
    new-instance p1, Lcom/intsig/camscanner/loadimage/RotateBitmap;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    const/4 p1, -0x1

    .line 7
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo0:I

    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->OO〇00〇8oO:I

    const/4 p1, 0x1

    .line 8
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇8〇oO〇〇8o:Z

    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇〇08O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 10
    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->O0O:Landroid/os/Handler;

    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o8oOOo:Ljava/lang/Runnable;

    const/4 p1, 0x0

    .line 12
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇O〇〇O8:F

    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇o0O:Landroid/graphics/RectF;

    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇8o8o〇()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2}, Lcom/intsig/view/SafeImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o〇00O:Landroid/graphics/Matrix;

    .line 17
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->O8o08O8O:Landroid/graphics/Matrix;

    .line 18
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇080OO8〇0:Landroid/graphics/Matrix;

    const/16 p1, 0x9

    new-array p1, p1, [F

    .line 19
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇0O:[F

    .line 20
    new-instance p1, Lcom/intsig/camscanner/loadimage/RotateBitmap;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Lcom/intsig/camscanner/loadimage/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    const/4 p1, -0x1

    .line 21
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo0:I

    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->OO〇00〇8oO:I

    const/4 p1, 0x1

    .line 22
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇8〇oO〇〇8o:Z

    .line 23
    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇〇08O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 24
    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->O0O:Landroid/os/Handler;

    .line 25
    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o8oOOo:Ljava/lang/Runnable;

    const/4 p1, 0x0

    .line 26
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇O〇〇O8:F

    .line 27
    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇o0O:Landroid/graphics/RectF;

    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇8o8o〇()V

    return-void
.end method

.method private synthetic OO0o〇〇(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o800o8O(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic Oo08(Lcom/intsig/camscanner/view/ImageViewTouchBase;Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->OO0o〇〇(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private 〇8o8o〇()V
    .locals 1

    .line 1
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method protected OO0o〇〇〇〇0(Landroid/graphics/Matrix;I)F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇0O:[F

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇0O:[F

    .line 7
    .line 8
    aget p1, p1, p2

    .line 9
    .line 10
    return p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public OoO8(Landroid/graphics/Bitmap;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v0, p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o800o8O(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected Oooo8o0〇()F
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/high16 v1, 0x3f800000    # 1.0f

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    return v1

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->Oo08()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    int-to-float v0, v0

    .line 19
    iget v2, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo0:I

    .line 20
    .line 21
    int-to-float v2, v2

    .line 22
    div-float/2addr v0, v2

    .line 23
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 24
    .line 25
    invoke-virtual {v2}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇o00〇〇Oo()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    int-to-float v2, v2

    .line 30
    iget v3, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->OO〇00〇8oO:I

    .line 31
    .line 32
    int-to-float v3, v3

    .line 33
    div-float/2addr v2, v3

    .line 34
    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    const/high16 v2, 0x40800000    # 4.0f

    .line 39
    .line 40
    mul-float v0, v0, v2

    .line 41
    .line 42
    cmpg-float v1, v0, v1

    .line 43
    .line 44
    if-gtz v1, :cond_1

    .line 45
    .line 46
    const/high16 v0, 0x40400000    # 3.0f

    .line 47
    .line 48
    :cond_1
    return v0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getBitmapDisplayed()Lcom/intsig/camscanner/loadimage/RotateBitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCenterPoint()Landroid/graphics/PointF;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇o0O:Landroid/graphics/RectF;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-float v0, v0

    .line 10
    const/high16 v1, 0x40000000    # 2.0f

    .line 11
    .line 12
    div-float/2addr v0, v1

    .line 13
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    int-to-float v2, v2

    .line 18
    div-float/2addr v2, v1

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇o0O:Landroid/graphics/RectF;

    .line 25
    .line 26
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    :goto_0
    new-instance v1, Landroid/graphics/PointF;

    .line 31
    .line 32
    invoke-direct {v1, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 33
    .line 34
    .line 35
    return-object v1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getDisplayBoundRect()Landroid/graphics/RectF;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->Oo08()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇o00〇〇Oo()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    new-instance v2, Landroid/graphics/RectF;

    .line 14
    .line 15
    int-to-float v0, v0

    .line 16
    int-to-float v1, v1

    .line 17
    const/4 v3, 0x0

    .line 18
    invoke-direct {v2, v3, v3, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 26
    .line 27
    .line 28
    return-object v2
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getDisplayBoundRectOnScreen()Landroid/graphics/RectF;
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getDisplayBoundRect()Landroid/graphics/RectF;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x2

    .line 6
    new-array v2, v1, [I

    .line 7
    .line 8
    invoke-virtual {p0, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 9
    .line 10
    .line 11
    const/4 v3, 0x0

    .line 12
    aget v3, v2, v3

    .line 13
    .line 14
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 15
    .line 16
    .line 17
    move-result v4

    .line 18
    div-int/2addr v4, v1

    .line 19
    add-int/2addr v3, v4

    .line 20
    const/4 v4, 0x1

    .line 21
    aget v2, v2, v4

    .line 22
    .line 23
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    div-int/2addr v4, v1

    .line 28
    add-int/2addr v2, v4

    .line 29
    new-instance v1, Landroid/graphics/RectF;

    .line 30
    .line 31
    int-to-float v3, v3

    .line 32
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 33
    .line 34
    .line 35
    move-result v4

    .line 36
    const/high16 v5, 0x40000000    # 2.0f

    .line 37
    .line 38
    div-float/2addr v4, v5

    .line 39
    sub-float v4, v3, v4

    .line 40
    .line 41
    int-to-float v2, v2

    .line 42
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 43
    .line 44
    .line 45
    move-result v6

    .line 46
    div-float/2addr v6, v5

    .line 47
    sub-float v6, v2, v6

    .line 48
    .line 49
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 50
    .line 51
    .line 52
    move-result v7

    .line 53
    div-float/2addr v7, v5

    .line 54
    add-float/2addr v3, v7

    .line 55
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    div-float/2addr v0, v5

    .line 60
    add-float/2addr v2, v0

    .line 61
    invoke-direct {v1, v4, v6, v3, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 62
    .line 63
    .line 64
    return-object v1
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getImageBm()Landroid/graphics/Bitmap;
    .locals 5

    .line 1
    :try_start_0
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 10
    .line 11
    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    new-instance v1, Landroid/graphics/Canvas;

    .line 16
    .line 17
    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0, v1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getDisplayBoundRect()Landroid/graphics/RectF;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    iget v2, v1, Landroid/graphics/RectF;->left:F

    .line 28
    .line 29
    float-to-int v2, v2

    .line 30
    iget v3, v1, Landroid/graphics/RectF;->top:F

    .line 31
    .line 32
    float-to-int v3, v3

    .line 33
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    float-to-int v4, v4

    .line 38
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    float-to-int v1, v1

    .line 43
    invoke-static {v0, v2, v3, v4, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    .line 44
    .line 45
    .line 46
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    return-object v0

    .line 48
    :catch_0
    move-exception v0

    .line 49
    const-string v1, "ImageViewTouchBase"

    .line 50
    .line 51
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 52
    .line 53
    .line 54
    const/4 v0, 0x0

    .line 55
    return-object v0
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getImageViewMatrix()Landroid/graphics/Matrix;
    .locals 2
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇080OO8〇0:Landroid/graphics/Matrix;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o〇00O:Landroid/graphics/Matrix;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇080OO8〇0:Landroid/graphics/Matrix;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->O8o08O8O:Landroid/graphics/Matrix;

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇080OO8〇0:Landroid/graphics/Matrix;

    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOffset()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇O〇〇O8:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOriDisplayRectF()Landroid/graphics/RectF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇o0O:Landroid/graphics/RectF;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRotateBitmap()Lcom/intsig/camscanner/loadimage/RotateBitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇〇08O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getScale()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->O8o08O8O:Landroid/graphics/Matrix;

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇80〇808〇O(Landroid/graphics/Matrix;)F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o800o8O(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-gtz v0, :cond_0

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/camscanner/view/oo〇;

    .line 8
    .line 9
    invoke-direct {v0, p0, p1, p2}, Lcom/intsig/camscanner/view/oo〇;-><init>(Lcom/intsig/camscanner/view/ImageViewTouchBase;Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o8oOOo:Ljava/lang/Runnable;

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    if-eqz p1, :cond_1

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    const-string v0, "ImageViewTouchBase"

    .line 24
    .line 25
    const-string v1, "RotateBitmap bitmap = null"

    .line 26
    .line 27
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    :cond_1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇〇08O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 31
    .line 32
    if-eqz p1, :cond_2

    .line 33
    .line 34
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    if-eqz v0, :cond_2

    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o〇00O:Landroid/graphics/Matrix;

    .line 41
    .line 42
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oO80(Lcom/intsig/camscanner/loadimage/RotateBitmap;Landroid/graphics/Matrix;)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->O8()I

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    invoke-virtual {p0, v0, p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇0〇O0088o(Landroid/graphics/Bitmap;I)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o〇00O:Landroid/graphics/Matrix;

    .line 58
    .line 59
    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    .line 60
    .line 61
    .line 62
    const/4 p1, 0x0

    .line 63
    const/4 v0, 0x0

    .line 64
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇0〇O0088o(Landroid/graphics/Bitmap;I)V

    .line 65
    .line 66
    .line 67
    :goto_0
    if-eqz p2, :cond_3

    .line 68
    .line 69
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->O8o08O8O:Landroid/graphics/Matrix;

    .line 70
    .line 71
    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    .line 72
    .line 73
    .line 74
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 75
    .line 76
    .line 77
    move-result-object p1

    .line 78
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->Oooo8o0〇()F

    .line 82
    .line 83
    .line 84
    move-result p1

    .line 85
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o8〇OO0〇0o:F

    .line 86
    .line 87
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method protected oO80(Lcom/intsig/camscanner/loadimage/RotateBitmap;Landroid/graphics/Matrix;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇o0O:Landroid/graphics/RectF;

    .line 2
    .line 3
    const/high16 v1, 0x40400000    # 3.0f

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    int-to-float v0, v0

    .line 12
    iget v2, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇O〇〇O8:F

    .line 13
    .line 14
    sub-float/2addr v0, v2

    .line 15
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    int-to-float v2, v2

    .line 20
    iget v3, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇O〇〇O8:F

    .line 21
    .line 22
    sub-float/2addr v2, v3

    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->Oo08()I

    .line 24
    .line 25
    .line 26
    move-result v3

    .line 27
    int-to-float v3, v3

    .line 28
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇o00〇〇Oo()I

    .line 29
    .line 30
    .line 31
    move-result v4

    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    .line 34
    .line 35
    .line 36
    div-float v5, v0, v3

    .line 37
    .line 38
    invoke-static {v5, v1}, Ljava/lang/Math;->min(FF)F

    .line 39
    .line 40
    .line 41
    move-result v5

    .line 42
    div-float v6, v2, v4

    .line 43
    .line 44
    invoke-static {v6, v1}, Ljava/lang/Math;->min(FF)F

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    invoke-static {v5, v1}, Ljava/lang/Math;->min(FF)F

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇o〇()Landroid/graphics/Matrix;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    invoke-virtual {p2, p1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 57
    .line 58
    .line 59
    invoke-virtual {p2, v1, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 60
    .line 61
    .line 62
    iget p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇O〇〇O8:F

    .line 63
    .line 64
    add-float/2addr v0, p1

    .line 65
    mul-float v3, v3, v1

    .line 66
    .line 67
    sub-float/2addr v0, v3

    .line 68
    const/high16 v3, 0x40000000    # 2.0f

    .line 69
    .line 70
    div-float/2addr v0, v3

    .line 71
    add-float/2addr v2, p1

    .line 72
    mul-float v4, v4, v1

    .line 73
    .line 74
    sub-float/2addr v2, v4

    .line 75
    div-float/2addr v2, v3

    .line 76
    invoke-virtual {p2, v0, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 77
    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 81
    .line 82
    .line 83
    move-result v0

    .line 84
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇o0O:Landroid/graphics/RectF;

    .line 85
    .line 86
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    .line 87
    .line 88
    .line 89
    move-result v2

    .line 90
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->Oo08()I

    .line 91
    .line 92
    .line 93
    move-result v3

    .line 94
    int-to-float v3, v3

    .line 95
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇o00〇〇Oo()I

    .line 96
    .line 97
    .line 98
    move-result v4

    .line 99
    int-to-float v4, v4

    .line 100
    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    .line 101
    .line 102
    .line 103
    div-float/2addr v0, v3

    .line 104
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    div-float/2addr v2, v4

    .line 109
    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    .line 110
    .line 111
    .line 112
    move-result v1

    .line 113
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    .line 114
    .line 115
    .line 116
    move-result v0

    .line 117
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇o〇()Landroid/graphics/Matrix;

    .line 118
    .line 119
    .line 120
    move-result-object p1

    .line 121
    invoke-virtual {p2, p1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 122
    .line 123
    .line 124
    invoke-virtual {p2, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 125
    .line 126
    .line 127
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇o0O:Landroid/graphics/RectF;

    .line 128
    .line 129
    iget v0, p1, Landroid/graphics/RectF;->left:F

    .line 130
    .line 131
    iget p1, p1, Landroid/graphics/RectF;->top:F

    .line 132
    .line 133
    invoke-virtual {p2, v0, p1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 134
    .line 135
    .line 136
    :goto_0
    return-void
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 1
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 2
    .line 3
    .line 4
    sub-int/2addr p4, p2

    .line 5
    iput p4, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo0:I

    .line 6
    .line 7
    sub-int/2addr p5, p3

    .line 8
    iput p5, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->OO〇00〇8oO:I

    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o8oOOo:Ljava/lang/Runnable;

    .line 11
    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    const/4 p2, 0x0

    .line 15
    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o8oOOo:Ljava/lang/Runnable;

    .line 16
    .line 17
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 18
    .line 19
    .line 20
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    if-eqz p1, :cond_1

    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 29
    .line 30
    iget-object p2, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o〇00O:Landroid/graphics/Matrix;

    .line 31
    .line 32
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oO80(Lcom/intsig/camscanner/loadimage/RotateBitmap;Landroid/graphics/Matrix;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 40
    .line 41
    .line 42
    :cond_1
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method public oo88o8O(F)[F
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getCenterPoint()Landroid/graphics/PointF;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget v1, v0, Landroid/graphics/PointF;->x:F

    .line 6
    .line 7
    iget v0, v0, Landroid/graphics/PointF;->y:F

    .line 8
    .line 9
    invoke-virtual {p0, p1, v1, v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇oo〇(FFF)[F

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    return-object p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected o〇0(ZZ)Landroid/graphics/PointF;
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    new-instance p1, Landroid/graphics/PointF;

    .line 11
    .line 12
    invoke-direct {p1, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 13
    .line 14
    .line 15
    return-object p1

    .line 16
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v2, Landroid/graphics/RectF;

    .line 21
    .line 22
    iget-object v3, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 23
    .line 24
    invoke-virtual {v3}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    int-to-float v3, v3

    .line 33
    iget-object v4, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 34
    .line 35
    invoke-virtual {v4}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 36
    .line 37
    .line 38
    move-result-object v4

    .line 39
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    .line 40
    .line 41
    .line 42
    move-result v4

    .line 43
    int-to-float v4, v4

    .line 44
    invoke-direct {v2, v1, v1, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 48
    .line 49
    .line 50
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    .line 55
    .line 56
    .line 57
    move-result v3

    .line 58
    iget-object v4, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇o0O:Landroid/graphics/RectF;

    .line 59
    .line 60
    if-eqz v4, :cond_1

    .line 61
    .line 62
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    .line 63
    .line 64
    .line 65
    move-result v4

    .line 66
    float-to-int v4, v4

    .line 67
    iget-object v5, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇o0O:Landroid/graphics/RectF;

    .line 68
    .line 69
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    .line 70
    .line 71
    .line 72
    move-result v5

    .line 73
    float-to-int v5, v5

    .line 74
    iget-object v6, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇o0O:Landroid/graphics/RectF;

    .line 75
    .line 76
    iget v7, v6, Landroid/graphics/RectF;->left:F

    .line 77
    .line 78
    iget v8, v6, Landroid/graphics/RectF;->top:F

    .line 79
    .line 80
    iget v9, v6, Landroid/graphics/RectF;->right:F

    .line 81
    .line 82
    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    .line 83
    .line 84
    goto :goto_0

    .line 85
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 86
    .line 87
    .line 88
    move-result v4

    .line 89
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 90
    .line 91
    .line 92
    move-result v5

    .line 93
    int-to-float v9, v4

    .line 94
    int-to-float v6, v5

    .line 95
    const/4 v7, 0x0

    .line 96
    const/4 v8, 0x0

    .line 97
    :goto_0
    const/high16 v10, 0x40000000    # 2.0f

    .line 98
    .line 99
    if-eqz p2, :cond_4

    .line 100
    .line 101
    int-to-float p2, v5

    .line 102
    cmpg-float v5, v0, p2

    .line 103
    .line 104
    if-gez v5, :cond_2

    .line 105
    .line 106
    sub-float/2addr p2, v0

    .line 107
    div-float/2addr p2, v10

    .line 108
    add-float/2addr v8, p2

    .line 109
    iget p2, v2, Landroid/graphics/RectF;->top:F

    .line 110
    .line 111
    :goto_1
    sub-float/2addr v8, p2

    .line 112
    goto :goto_2

    .line 113
    :cond_2
    iget p2, v2, Landroid/graphics/RectF;->top:F

    .line 114
    .line 115
    cmpl-float v0, p2, v8

    .line 116
    .line 117
    if-lez v0, :cond_3

    .line 118
    .line 119
    goto :goto_1

    .line 120
    :cond_3
    iget p2, v2, Landroid/graphics/RectF;->bottom:F

    .line 121
    .line 122
    cmpg-float v0, p2, v6

    .line 123
    .line 124
    if-gez v0, :cond_4

    .line 125
    .line 126
    sub-float v8, v6, p2

    .line 127
    .line 128
    goto :goto_2

    .line 129
    :cond_4
    const/4 v8, 0x0

    .line 130
    :goto_2
    if-eqz p1, :cond_7

    .line 131
    .line 132
    int-to-float p1, v4

    .line 133
    cmpg-float p2, v3, p1

    .line 134
    .line 135
    if-gez p2, :cond_5

    .line 136
    .line 137
    sub-float/2addr p1, v3

    .line 138
    div-float/2addr p1, v10

    .line 139
    add-float/2addr v7, p1

    .line 140
    iget p1, v2, Landroid/graphics/RectF;->left:F

    .line 141
    .line 142
    :goto_3
    sub-float v1, v7, p1

    .line 143
    .line 144
    goto :goto_4

    .line 145
    :cond_5
    iget p1, v2, Landroid/graphics/RectF;->left:F

    .line 146
    .line 147
    cmpl-float p2, p1, v7

    .line 148
    .line 149
    if-lez p2, :cond_6

    .line 150
    .line 151
    goto :goto_3

    .line 152
    :cond_6
    iget p1, v2, Landroid/graphics/RectF;->right:F

    .line 153
    .line 154
    cmpg-float p2, p1, v9

    .line 155
    .line 156
    if-gez p2, :cond_7

    .line 157
    .line 158
    sub-float v1, v9, p1

    .line 159
    .line 160
    :cond_7
    :goto_4
    invoke-virtual {p0, v1, v8}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇O00(FF)V

    .line 161
    .line 162
    .line 163
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 164
    .line 165
    .line 166
    move-result-object p1

    .line 167
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 168
    .line 169
    .line 170
    new-instance p1, Landroid/graphics/PointF;

    .line 171
    .line 172
    invoke-direct {p1, v1, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 173
    .line 174
    .line 175
    return-object p1
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public o〇O8〇〇o(FFF)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇o0O:Landroid/graphics/RectF;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-float v0, v0

    .line 10
    const/high16 v1, 0x40000000    # 2.0f

    .line 11
    .line 12
    div-float/2addr v0, v1

    .line 13
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    int-to-float v2, v2

    .line 18
    div-float/2addr v2, v1

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇o0O:Landroid/graphics/RectF;

    .line 25
    .line 26
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    :goto_0
    sub-float p2, v0, p2

    .line 31
    .line 32
    sub-float p3, v2, p3

    .line 33
    .line 34
    invoke-virtual {p0, p2, p3}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇O〇(FF)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0, p1, v0, v2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇oo〇(FFF)[F

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇0〇O0088o(Landroid/graphics/Bitmap;I)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMaxZoom(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o8〇OO0〇0o:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOffset(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇O〇〇O8:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOriDisplayRectF(Landroid/graphics/RectF;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇o0O:Landroid/graphics/RectF;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRecycler(Lcom/intsig/camscanner/view/ImageViewTouchBase$Recycler;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->ooo0〇〇O:Lcom/intsig/camscanner/view/ImageViewTouchBase$Recycler;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇0〇O0088o(Landroid/graphics/Bitmap;I)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/view/SafeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 21
    .line 22
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇〇888(Landroid/graphics/Bitmap;)V

    .line 23
    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 26
    .line 27
    invoke-virtual {v1, p2}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->oO80(I)V

    .line 28
    .line 29
    .line 30
    if-eqz v0, :cond_1

    .line 31
    .line 32
    if-eq v0, p1, :cond_1

    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->ooo0〇〇O:Lcom/intsig/camscanner/view/ImageViewTouchBase$Recycler;

    .line 35
    .line 36
    if-eqz p1, :cond_1

    .line 37
    .line 38
    iget-boolean p2, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇8〇oO〇〇8o:Z

    .line 39
    .line 40
    if-eqz p2, :cond_1

    .line 41
    .line 42
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase$Recycler;->recycle(Landroid/graphics/Bitmap;)V

    .line 43
    .line 44
    .line 45
    :cond_1
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected 〇80〇808〇O(Landroid/graphics/Matrix;)F
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->OO0o〇〇〇〇0(Landroid/graphics/Matrix;I)F

    .line 3
    .line 4
    .line 5
    move-result p1

    .line 6
    return p1
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected 〇O00(FF)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->O8o08O8O:Landroid/graphics/Matrix;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected 〇O〇(FF)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇O00(FF)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected 〇oo〇(FFF)[F
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o8〇OO0〇0o:F

    .line 2
    .line 3
    cmpl-float v1, p1, v0

    .line 4
    .line 5
    if-lez v1, :cond_0

    .line 6
    .line 7
    move p1, v0

    .line 8
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    div-float/2addr p1, v0

    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->O8o08O8O:Landroid/graphics/Matrix;

    .line 14
    .line 15
    invoke-virtual {v0, p1, p1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 19
    .line 20
    .line 21
    move-result-object p2

    .line 22
    invoke-virtual {p0, p2}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 23
    .line 24
    .line 25
    const/4 p2, 0x1

    .line 26
    invoke-virtual {p0, p2, p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o〇0(ZZ)Landroid/graphics/PointF;

    .line 27
    .line 28
    .line 29
    move-result-object p3

    .line 30
    const/4 v0, 0x3

    .line 31
    new-array v0, v0, [F

    .line 32
    .line 33
    const/4 v1, 0x0

    .line 34
    aput p1, v0, v1

    .line 35
    .line 36
    iget p1, p3, Landroid/graphics/PointF;->x:F

    .line 37
    .line 38
    aput p1, v0, p2

    .line 39
    .line 40
    const/4 p1, 0x2

    .line 41
    iget p2, p3, Landroid/graphics/PointF;->y:F

    .line 42
    .line 43
    aput p2, v0, p1

    .line 44
    .line 45
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public 〇〇808〇()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/high16 v1, 0x3f800000    # 1.0f

    .line 6
    .line 7
    cmpl-float v0, v0, v1

    .line 8
    .line 9
    if-lez v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oo88o8O(F)[F

    .line 12
    .line 13
    .line 14
    const/4 v0, 0x1

    .line 15
    return v0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x1

    .line 3
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->OoO8(Landroid/graphics/Bitmap;Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇8O0〇8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->O8o08O8O:Landroid/graphics/Matrix;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "FastScrollRecyclerView.java"

# interfaces
.implements Landroidx/recyclerview/widget/RecyclerView$OnItemTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;,
        Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$SectionedAdapter;,
        Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollOffsetInvalidator;,
        Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;
    }
.end annotation


# instance fields
.field private O8o08O8O:I

.field private OO:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;

.field private o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

.field private oOo0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/interfaces/OnFastScrollStateChangeListener;

.field private oOo〇8o008:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollOffsetInvalidator;

.field private o〇00O:I

.field private 〇080OO8〇0:I

.field private 〇08O〇00〇o:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;",
            ">;"
        }
    .end annotation
.end field

.field private 〇0O:Landroid/util/SparseIntArray;

.field private 〇OOo8〇0:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .line 2
    invoke-direct {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p3, 0x1

    .line 3
    iput-boolean p3, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇OOo8〇0:Z

    .line 4
    new-instance v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;

    invoke-direct {v0}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->OO:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;

    .line 5
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇08O〇00〇o:Ljava/util/LinkedHashMap;

    .line 6
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/intsig/camscanner/R$styleable;->LocalFastScrollRecyclerView:[I

    const/4 v2, 0x0

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/16 v1, 0xb

    .line 7
    :try_start_0
    invoke-virtual {v0, v1, p3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    iput-boolean p3, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇OOo8〇0:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 9
    new-instance p3, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    invoke-direct {p3, p1, p0, p2}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;Landroid/util/AttributeSet;)V

    iput-object p3, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 10
    new-instance p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollOffsetInvalidator;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollOffsetInvalidator;-><init>(Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/〇080;)V

    iput-object p1, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->oOo〇8o008:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollOffsetInvalidator;

    .line 11
    new-instance p1, Landroid/util/SparseIntArray;

    invoke-direct {p1}, Landroid/util/SparseIntArray;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇0O:Landroid/util/SparseIntArray;

    return-void

    :catchall_0
    move-exception p1

    .line 12
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 13
    throw p1
.end method

.method private O8(I)I
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v0, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;

    .line 6
    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇0O:Landroid/util/SparseIntArray;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-ltz v0, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇0O:Landroid/util/SparseIntArray;

    .line 18
    .line 19
    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    return p1

    .line 24
    :cond_0
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    check-cast v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;

    .line 29
    .line 30
    const/4 v1, 0x0

    .line 31
    const/4 v2, 0x0

    .line 32
    :goto_0
    if-ge v1, p1, :cond_1

    .line 33
    .line 34
    iget-object v3, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇0O:Landroid/util/SparseIntArray;

    .line 35
    .line 36
    invoke-virtual {v3, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    invoke-virtual {v3, v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemViewType(I)I

    .line 44
    .line 45
    .line 46
    move-result v3

    .line 47
    invoke-virtual {p0, v1}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 48
    .line 49
    .line 50
    move-result-object v4

    .line 51
    invoke-interface {v0, p0, v4, v3}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;->〇080(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)I

    .line 52
    .line 53
    .line 54
    move-result v3

    .line 55
    add-int/2addr v2, v3

    .line 56
    add-int/lit8 v1, v1, 0x1

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇0O:Landroid/util/SparseIntArray;

    .line 60
    .line 61
    invoke-virtual {v0, p1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 62
    .line 63
    .line 64
    return v2

    .line 65
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 66
    .line 67
    const-string v0, "calculateScrollDistanceToPosition() should only be called where the RecyclerView.Adapter is an instance of MeasurableAdapter"

    .line 68
    .line 69
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    throw p1
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private OO0o〇〇〇〇0(Landroid/view/MotionEvent;)Z
    .locals 18

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    float-to-int v5, v2

    .line 12
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    float-to-int v10, v2

    .line 17
    const/4 v2, 0x1

    .line 18
    if-eqz v1, :cond_2

    .line 19
    .line 20
    if-eq v1, v2, :cond_1

    .line 21
    .line 22
    const/4 v3, 0x2

    .line 23
    if-eq v1, v3, :cond_0

    .line 24
    .line 25
    const/4 v3, 0x3

    .line 26
    if-eq v1, v3, :cond_1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    iput v10, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇080OO8〇0:I

    .line 30
    .line 31
    iget-object v6, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 32
    .line 33
    iget v8, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o〇00O:I

    .line 34
    .line 35
    iget v9, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->O8o08O8O:I

    .line 36
    .line 37
    iget-object v11, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->oOo0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/interfaces/OnFastScrollStateChangeListener;

    .line 38
    .line 39
    move-object/from16 v7, p1

    .line 40
    .line 41
    invoke-virtual/range {v6 .. v11}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->OO0o〇〇(Landroid/view/MotionEvent;IIILcom/intsig/camscanner/view/recyclerview_fastscroll/interfaces/OnFastScrollStateChangeListener;)V

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    iget-object v12, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 46
    .line 47
    iget v14, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o〇00O:I

    .line 48
    .line 49
    iget v15, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->O8o08O8O:I

    .line 50
    .line 51
    iget v1, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇080OO8〇0:I

    .line 52
    .line 53
    iget-object v3, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->oOo0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/interfaces/OnFastScrollStateChangeListener;

    .line 54
    .line 55
    move-object/from16 v13, p1

    .line 56
    .line 57
    move/from16 v16, v1

    .line 58
    .line 59
    move-object/from16 v17, v3

    .line 60
    .line 61
    invoke-virtual/range {v12 .. v17}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->OO0o〇〇(Landroid/view/MotionEvent;IIILcom/intsig/camscanner/view/recyclerview_fastscroll/interfaces/OnFastScrollStateChangeListener;)V

    .line 62
    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_2
    iput v5, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o〇00O:I

    .line 66
    .line 67
    iput v10, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇080OO8〇0:I

    .line 68
    .line 69
    iput v10, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->O8o08O8O:I

    .line 70
    .line 71
    iget-object v3, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 72
    .line 73
    iget-object v8, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->oOo0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/interfaces/OnFastScrollStateChangeListener;

    .line 74
    .line 75
    move-object/from16 v4, p1

    .line 76
    .line 77
    move v6, v10

    .line 78
    move v7, v10

    .line 79
    invoke-virtual/range {v3 .. v8}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->OO0o〇〇(Landroid/view/MotionEvent;IIILcom/intsig/camscanner/view/recyclerview_fastscroll/interfaces/OnFastScrollStateChangeListener;)V

    .line 80
    .line 81
    .line 82
    :goto_0
    iget-object v1, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 83
    .line 84
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->Oooo8o0〇()Z

    .line 85
    .line 86
    .line 87
    move-result v1

    .line 88
    if-nez v1, :cond_4

    .line 89
    .line 90
    iget-object v1, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 91
    .line 92
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->〇O〇()Z

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    if-eqz v1, :cond_3

    .line 97
    .line 98
    goto :goto_1

    .line 99
    :cond_3
    const/4 v2, 0x0

    .line 100
    :cond_4
    :goto_1
    return v2
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private o〇0(F)F
    .locals 6

    .line 1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v0, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;

    .line 6
    .line 7
    if-eqz v0, :cond_3

    .line 8
    .line 9
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;

    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇o〇()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    int-to-float v1, v1

    .line 20
    mul-float v1, v1, p1

    .line 21
    .line 22
    float-to-int v1, v1

    .line 23
    const/4 v2, 0x0

    .line 24
    :goto_0
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    invoke-virtual {v3}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    if-ge v2, v3, :cond_2

    .line 33
    .line 34
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->O8(I)I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    invoke-virtual {p0, v2}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 39
    .line 40
    .line 41
    move-result-object v4

    .line 42
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 43
    .line 44
    .line 45
    move-result-object v5

    .line 46
    invoke-virtual {v5, v2}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemViewType(I)I

    .line 47
    .line 48
    .line 49
    move-result v5

    .line 50
    invoke-interface {v0, p0, v4, v5}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;->〇080(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)I

    .line 51
    .line 52
    .line 53
    move-result v4

    .line 54
    add-int/2addr v4, v3

    .line 55
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 56
    .line 57
    .line 58
    move-result-object v5

    .line 59
    invoke-virtual {v5}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    .line 60
    .line 61
    .line 62
    move-result v5

    .line 63
    add-int/lit8 v5, v5, -0x1

    .line 64
    .line 65
    if-ne v2, v5, :cond_0

    .line 66
    .line 67
    if-lt v1, v3, :cond_1

    .line 68
    .line 69
    if-gt v1, v4, :cond_1

    .line 70
    .line 71
    int-to-float p1, v2

    .line 72
    return p1

    .line 73
    :cond_0
    if-lt v1, v3, :cond_1

    .line 74
    .line 75
    if-ge v1, v4, :cond_1

    .line 76
    .line 77
    int-to-float p1, v2

    .line 78
    return p1

    .line 79
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    .line 83
    .line 84
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 85
    .line 86
    .line 87
    const-string v1, "Failed to find a view at the provided scroll fraction ("

    .line 88
    .line 89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    const-string v1, ")"

    .line 96
    .line 97
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    int-to-float v0, v0

    .line 109
    mul-float p1, p1, v0

    .line 110
    .line 111
    return p1

    .line 112
    :cond_3
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    .line 117
    .line 118
    .line 119
    move-result v0

    .line 120
    int-to-float v0, v0

    .line 121
    mul-float v0, v0, p1

    .line 122
    .line 123
    return v0
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;)Landroid/util/SparseIntArray;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇0O:Landroid/util/SparseIntArray;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇80〇808〇O(Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;)V
    .locals 4

    .line 1
    const/4 v0, -0x1

    .line 2
    iput v0, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇080:I

    .line 3
    .line 4
    iput v0, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇o00〇〇Oo:I

    .line 5
    .line 6
    iput v0, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇o〇:I

    .line 7
    .line 8
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_4

    .line 17
    .line 18
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-nez v0, :cond_0

    .line 23
    .line 24
    goto/16 :goto_1

    .line 25
    .line 26
    :cond_0
    const/4 v0, 0x0

    .line 27
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    iput v1, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇080:I

    .line 36
    .line 37
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    instance-of v1, v1, Landroidx/recyclerview/widget/GridLayoutManager;

    .line 42
    .line 43
    if-eqz v1, :cond_1

    .line 44
    .line 45
    iget v1, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇080:I

    .line 46
    .line 47
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    check-cast v2, Landroidx/recyclerview/widget/GridLayoutManager;

    .line 52
    .line 53
    invoke-virtual {v2}, Landroidx/recyclerview/widget/GridLayoutManager;->getSpanCount()I

    .line 54
    .line 55
    .line 56
    move-result v2

    .line 57
    div-int/2addr v1, v2

    .line 58
    iput v1, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇080:I

    .line 59
    .line 60
    :cond_1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    instance-of v1, v1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;

    .line 65
    .line 66
    if-eqz v1, :cond_2

    .line 67
    .line 68
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;->getDecoratedTop(Landroid/view/View;)I

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    iput v0, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇o00〇〇Oo:I

    .line 77
    .line 78
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    check-cast v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;

    .line 83
    .line 84
    iget v1, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇080:I

    .line 85
    .line 86
    invoke-virtual {p0, v1}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 87
    .line 88
    .line 89
    move-result-object v1

    .line 90
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 91
    .line 92
    .line 93
    move-result-object v2

    .line 94
    iget v3, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇080:I

    .line 95
    .line 96
    invoke-virtual {v2, v3}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemViewType(I)I

    .line 97
    .line 98
    .line 99
    move-result v2

    .line 100
    invoke-interface {v0, p0, v1, v2}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;->〇080(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)I

    .line 101
    .line 102
    .line 103
    move-result v0

    .line 104
    iput v0, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇o〇:I

    .line 105
    .line 106
    goto :goto_0

    .line 107
    :cond_2
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 108
    .line 109
    .line 110
    move-result-object v1

    .line 111
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;->getDecoratedTop(Landroid/view/View;)I

    .line 112
    .line 113
    .line 114
    move-result v1

    .line 115
    iput v1, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇o00〇〇Oo:I

    .line 116
    .line 117
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 118
    .line 119
    .line 120
    move-result v1

    .line 121
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 122
    .line 123
    .line 124
    move-result-object v2

    .line 125
    invoke-virtual {v2, v0}, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;->getTopDecorationHeight(Landroid/view/View;)I

    .line 126
    .line 127
    .line 128
    move-result v2

    .line 129
    add-int/2addr v1, v2

    .line 130
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 131
    .line 132
    .line 133
    move-result-object v2

    .line 134
    invoke-virtual {v2, v0}, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;->getBottomDecorationHeight(Landroid/view/View;)I

    .line 135
    .line 136
    .line 137
    move-result v0

    .line 138
    add-int/2addr v1, v0

    .line 139
    iput v1, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇o〇:I

    .line 140
    .line 141
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇08O〇00〇o:Ljava/util/LinkedHashMap;

    .line 142
    .line 143
    iget v1, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇080:I

    .line 144
    .line 145
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 146
    .line 147
    .line 148
    move-result-object v1

    .line 149
    invoke-virtual {v0, v1}, Ljava/util/AbstractMap;->containsKey(Ljava/lang/Object;)Z

    .line 150
    .line 151
    .line 152
    move-result v0

    .line 153
    if-eqz v0, :cond_3

    .line 154
    .line 155
    iget v0, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇o〇:I

    .line 156
    .line 157
    if-lez v0, :cond_3

    .line 158
    .line 159
    return-void

    .line 160
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇08O〇00〇o:Ljava/util/LinkedHashMap;

    .line 161
    .line 162
    iget v1, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇080:I

    .line 163
    .line 164
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 165
    .line 166
    .line 167
    move-result-object v1

    .line 168
    invoke-virtual {v0, v1, p1}, Ljava/util/AbstractMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    .line 170
    .line 171
    :cond_4
    :goto_1
    return-void
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private 〇o〇()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v0, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->O8(I)I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    return v0

    .line 22
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 23
    .line 24
    const-string v1, "calculateAdapterHeight() should only be called where the RecyclerView.Adapter is an instance of MeasurableAdapter"

    .line 25
    .line 26
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    throw v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private 〇〇888(I)I
    .locals 8

    .line 1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v0, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;

    .line 6
    .line 7
    if-eqz v0, :cond_3

    .line 8
    .line 9
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;

    .line 14
    .line 15
    const/4 v1, 0x0

    .line 16
    const/4 v2, 0x0

    .line 17
    :goto_0
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    invoke-virtual {v3}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    const/4 v4, 0x1

    .line 26
    if-ge v2, v3, :cond_2

    .line 27
    .line 28
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->O8(I)I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    invoke-virtual {p0, v2}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 33
    .line 34
    .line 35
    move-result-object v5

    .line 36
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 37
    .line 38
    .line 39
    move-result-object v6

    .line 40
    invoke-virtual {v6, v2}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemViewType(I)I

    .line 41
    .line 42
    .line 43
    move-result v6

    .line 44
    invoke-interface {v0, p0, v5, v6}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;->〇080(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)I

    .line 45
    .line 46
    .line 47
    move-result v5

    .line 48
    add-int/2addr v5, v3

    .line 49
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 50
    .line 51
    .line 52
    move-result-object v6

    .line 53
    invoke-virtual {v6}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    .line 54
    .line 55
    .line 56
    move-result v6

    .line 57
    sub-int/2addr v6, v4

    .line 58
    if-ne v2, v6, :cond_0

    .line 59
    .line 60
    if-lt p1, v3, :cond_1

    .line 61
    .line 62
    if-gt p1, v5, :cond_1

    .line 63
    .line 64
    return v2

    .line 65
    :cond_0
    if-lt p1, v3, :cond_1

    .line 66
    .line 67
    if-ge p1, v5, :cond_1

    .line 68
    .line 69
    return v2

    .line 70
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_2
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->O8(I)I

    .line 74
    .line 75
    .line 76
    move-result v2

    .line 77
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 78
    .line 79
    .line 80
    move-result-object v3

    .line 81
    invoke-virtual {v3}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    .line 82
    .line 83
    .line 84
    move-result v3

    .line 85
    sub-int/2addr v3, v4

    .line 86
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->O8(I)I

    .line 87
    .line 88
    .line 89
    move-result v3

    .line 90
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 91
    .line 92
    .line 93
    move-result-object v5

    .line 94
    invoke-virtual {v5}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    .line 95
    .line 96
    .line 97
    move-result v5

    .line 98
    sub-int/2addr v5, v4

    .line 99
    invoke-virtual {p0, v5}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 100
    .line 101
    .line 102
    move-result-object v5

    .line 103
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 104
    .line 105
    .line 106
    move-result-object v6

    .line 107
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 108
    .line 109
    .line 110
    move-result-object v7

    .line 111
    invoke-virtual {v7}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    .line 112
    .line 113
    .line 114
    move-result v7

    .line 115
    sub-int/2addr v7, v4

    .line 116
    invoke-virtual {v6, v7}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemViewType(I)I

    .line 117
    .line 118
    .line 119
    move-result v6

    .line 120
    invoke-interface {v0, p0, v5, v6}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;->〇080(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)I

    .line 121
    .line 122
    .line 123
    move-result v0

    .line 124
    add-int/2addr v3, v0

    .line 125
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 126
    .line 127
    const/4 v5, 0x3

    .line 128
    new-array v5, v5, [Ljava/lang/Object;

    .line 129
    .line 130
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 131
    .line 132
    .line 133
    move-result-object p1

    .line 134
    aput-object p1, v5, v1

    .line 135
    .line 136
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 137
    .line 138
    .line 139
    move-result-object p1

    .line 140
    aput-object p1, v5, v4

    .line 141
    .line 142
    const/4 p1, 0x2

    .line 143
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 144
    .line 145
    .line 146
    move-result-object v1

    .line 147
    aput-object v1, v5, p1

    .line 148
    .line 149
    const-string p1, "Invalid passed height: %d, [low: %d, height: %d]"

    .line 150
    .line 151
    invoke-static {p1, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 152
    .line 153
    .line 154
    move-result-object p1

    .line 155
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 156
    .line 157
    .line 158
    throw v0

    .line 159
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 160
    .line 161
    const-string v0, "findMeasureAdapterFirstVisiblePosition() should only be called where the RecyclerView.Adapter is an instance of MeasurableAdapter"

    .line 162
    .line 163
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 164
    .line 165
    .line 166
    throw p1
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method


# virtual methods
.method public OO0o〇〇(F)Ljava/lang/String;
    .locals 8

    .line 1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const-string v1, ""

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-object v1

    .line 14
    :cond_0
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    instance-of v2, v2, Landroidx/recyclerview/widget/GridLayoutManager;

    .line 19
    .line 20
    const/4 v3, 0x1

    .line 21
    if-eqz v2, :cond_1

    .line 22
    .line 23
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    check-cast v2, Landroidx/recyclerview/widget/GridLayoutManager;

    .line 28
    .line 29
    invoke-virtual {v2}, Landroidx/recyclerview/widget/GridLayoutManager;->getSpanCount()I

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    int-to-double v4, v0

    .line 34
    int-to-double v6, v2

    .line 35
    div-double/2addr v4, v6

    .line 36
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    .line 37
    .line 38
    .line 39
    move-result-wide v4

    .line 40
    double-to-int v0, v4

    .line 41
    goto :goto_0

    .line 42
    :cond_1
    const/4 v2, 0x1

    .line 43
    :goto_0
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->stopScroll()V

    .line 44
    .line 45
    .line 46
    iget-object v4, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->OO:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;

    .line 47
    .line 48
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇80〇808〇O(Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 52
    .line 53
    .line 54
    move-result-object v4

    .line 55
    instance-of v4, v4, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;

    .line 56
    .line 57
    const/4 v5, 0x0

    .line 58
    if-eqz v4, :cond_2

    .line 59
    .line 60
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o〇0(F)F

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    invoke-direct {p0}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇o〇()I

    .line 65
    .line 66
    .line 67
    move-result v2

    .line 68
    invoke-virtual {p0, v2, v5}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->oO80(II)I

    .line 69
    .line 70
    .line 71
    move-result v2

    .line 72
    int-to-float v2, v2

    .line 73
    mul-float v2, v2, p1

    .line 74
    .line 75
    float-to-int v2, v2

    .line 76
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇〇888(I)I

    .line 77
    .line 78
    .line 79
    move-result v4

    .line 80
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->O8(I)I

    .line 81
    .line 82
    .line 83
    move-result v5

    .line 84
    sub-int/2addr v5, v2

    .line 85
    goto :goto_1

    .line 86
    :cond_2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o〇0(F)F

    .line 87
    .line 88
    .line 89
    move-result v4

    .line 90
    iget-object v6, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->OO:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;

    .line 91
    .line 92
    iget v6, v6, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇o〇:I

    .line 93
    .line 94
    mul-int v0, v0, v6

    .line 95
    .line 96
    invoke-virtual {p0, v0, v5}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->oO80(II)I

    .line 97
    .line 98
    .line 99
    move-result v0

    .line 100
    int-to-float v0, v0

    .line 101
    mul-float v0, v0, p1

    .line 102
    .line 103
    float-to-int v0, v0

    .line 104
    mul-int v2, v2, v0

    .line 105
    .line 106
    iget-object v5, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->OO:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;

    .line 107
    .line 108
    iget v5, v5, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇o〇:I

    .line 109
    .line 110
    div-int/2addr v2, v5

    .line 111
    rem-int/2addr v0, v5

    .line 112
    neg-int v5, v0

    .line 113
    move v0, v4

    .line 114
    move v4, v2

    .line 115
    :goto_1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 116
    .line 117
    .line 118
    move-result-object v2

    .line 119
    check-cast v2, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 120
    .line 121
    invoke-virtual {v2, v4, v5}, Landroidx/recyclerview/widget/LinearLayoutManager;->scrollToPositionWithOffset(II)V

    .line 122
    .line 123
    .line 124
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 125
    .line 126
    .line 127
    move-result-object v2

    .line 128
    instance-of v2, v2, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$SectionedAdapter;

    .line 129
    .line 130
    if-nez v2, :cond_3

    .line 131
    .line 132
    return-object v1

    .line 133
    :cond_3
    const/high16 v1, 0x3f800000    # 1.0f

    .line 134
    .line 135
    cmpl-float p1, p1, v1

    .line 136
    .line 137
    if-nez p1, :cond_4

    .line 138
    .line 139
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 140
    .line 141
    .line 142
    move-result-object p1

    .line 143
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    .line 144
    .line 145
    .line 146
    move-result p1

    .line 147
    sub-int/2addr p1, v3

    .line 148
    int-to-float v0, p1

    .line 149
    :cond_4
    float-to-int p1, v0

    .line 150
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 151
    .line 152
    .line 153
    move-result-object v0

    .line 154
    check-cast v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$SectionedAdapter;

    .line 155
    .line 156
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$SectionedAdapter;->〇080(I)Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object p1

    .line 160
    return-object p1
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public Oo08(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->〇〇888(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected Oooo8o0〇(Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;I)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v0, v0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$MeasurableAdapter;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇o〇()I

    .line 11
    .line 12
    .line 13
    move-result p2

    .line 14
    invoke-virtual {p0, p2, v1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->oO80(II)I

    .line 15
    .line 16
    .line 17
    move-result p2

    .line 18
    iget v0, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇080:I

    .line 19
    .line 20
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->O8(I)I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    iget v0, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇o〇:I

    .line 26
    .line 27
    mul-int p2, p2, v0

    .line 28
    .line 29
    invoke-virtual {p0, p2, v1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->oO80(II)I

    .line 30
    .line 31
    .line 32
    move-result p2

    .line 33
    iget v0, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇080:I

    .line 34
    .line 35
    iget v2, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇o〇:I

    .line 36
    .line 37
    mul-int v0, v0, v2

    .line 38
    .line 39
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->getAvailableScrollBarHeight()I

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    if-gtz p2, :cond_1

    .line 44
    .line 45
    iget-object p1, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 46
    .line 47
    const/4 p2, -0x1

    .line 48
    invoke-virtual {p1, p2, p2}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->OOO〇O0(II)V

    .line 49
    .line 50
    .line 51
    return-void

    .line 52
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    add-int/2addr v3, v0

    .line 57
    invoke-static {p2, v3}, Ljava/lang/Math;->min(II)I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇8o8o〇()Z

    .line 62
    .line 63
    .line 64
    move-result v3

    .line 65
    if-eqz v3, :cond_2

    .line 66
    .line 67
    iget p1, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇o00〇〇Oo:I

    .line 68
    .line 69
    add-int/2addr v0, p1

    .line 70
    sub-int/2addr v0, v2

    .line 71
    goto :goto_1

    .line 72
    :cond_2
    iget p1, p1, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇o00〇〇Oo:I

    .line 73
    .line 74
    sub-int/2addr v0, p1

    .line 75
    :goto_1
    int-to-float p1, v0

    .line 76
    int-to-float p2, p2

    .line 77
    div-float/2addr p1, p2

    .line 78
    int-to-float p2, v2

    .line 79
    mul-float p1, p1, p2

    .line 80
    .line 81
    float-to-int p1, p1

    .line 82
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇8o8o〇()Z

    .line 83
    .line 84
    .line 85
    move-result p2

    .line 86
    if-eqz p2, :cond_3

    .line 87
    .line 88
    sub-int/2addr v2, p1

    .line 89
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    .line 90
    .line 91
    .line 92
    move-result p1

    .line 93
    add-int/2addr v2, p1

    .line 94
    goto :goto_2

    .line 95
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    .line 96
    .line 97
    .line 98
    move-result p2

    .line 99
    add-int v2, p1, p2

    .line 100
    .line 101
    :goto_2
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 102
    .line 103
    .line 104
    move-result-object p1

    .line 105
    invoke-static {p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/utils/Utils;->〇080(Landroid/content/res/Resources;)Z

    .line 106
    .line 107
    .line 108
    move-result p1

    .line 109
    if-eqz p1, :cond_4

    .line 110
    .line 111
    goto :goto_3

    .line 112
    :cond_4
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 113
    .line 114
    .line 115
    move-result p1

    .line 116
    iget-object p2, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 117
    .line 118
    invoke-virtual {p2}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->〇O8o08O()I

    .line 119
    .line 120
    .line 121
    move-result p2

    .line 122
    sub-int v1, p1, p2

    .line 123
    .line 124
    :goto_3
    iget-object p1, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 125
    .line 126
    invoke-virtual {p1, v1, v2}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->OOO〇O0(II)V

    .line 127
    .line 128
    .line 129
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 1
    :try_start_0
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->draw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇OOo8〇0:Z

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇O8o08O()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->oO80(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :catch_0
    move-exception p1

    .line 18
    const-string v0, "FastScrollRecyclerView"

    .line 19
    .line 20
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 21
    .line 22
    .line 23
    :cond_0
    :goto_0
    return-void
    .line 24
.end method

.method protected getAvailableScrollBarHeight()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    sub-int/2addr v0, v1

    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    sub-int/2addr v0, v1

    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->OO0o〇〇〇〇0()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    sub-int/2addr v0, v1

    .line 22
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getScrollBarThumbHeight()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->OO0o〇〇〇〇0()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getScrollBarWidth()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->〇O8o08O()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getThumbY()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->〇8o8o〇()Landroid/graphics/Point;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget v0, v0, Landroid/graphics/Point;->y:I

    .line 8
    .line 9
    int-to-float v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected oO80(II)I
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    add-int/2addr v1, p2

    .line 10
    add-int/2addr v1, p1

    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    add-int/2addr v1, p1

    .line 16
    sub-int/2addr v1, v0

    .line 17
    return v1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, p0}, Landroidx/recyclerview/widget/RecyclerView;->addOnItemTouchListener(Landroidx/recyclerview/widget/RecyclerView$OnItemTouchListener;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onInterceptTouchEvent(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/MotionEvent;)Z
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/MotionEvent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->OO0o〇〇〇〇0(Landroid/view/MotionEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    return p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public onRequestDisallowInterceptTouchEvent(Z)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onTouchEvent(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/MotionEvent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->OO0o〇〇〇〇0(Landroid/view/MotionEvent;)Z

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->oOo〇8o008:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollOffsetInvalidator;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->unregisterAdapterDataObserver(Landroidx/recyclerview/widget/RecyclerView$AdapterDataObserver;)V

    .line 14
    .line 15
    .line 16
    :cond_0
    if-eqz p1, :cond_1

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->oOo〇8o008:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollOffsetInvalidator;

    .line 19
    .line 20
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->registerAdapterDataObserver(Landroidx/recyclerview/widget/RecyclerView$AdapterDataObserver;)V

    .line 21
    .line 22
    .line 23
    :cond_1
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setAutoHideDelay(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->OoO8(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setAutoHideEnabled(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->o800o8O(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFastScrollEnabled(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇OOo8〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMaxPages(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->〇O888o0o(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOnFastScrollStateChangeListener(Lcom/intsig/camscanner/view/recyclerview_fastscroll/interfaces/OnFastScrollStateChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->oOo0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/interfaces/OnFastScrollStateChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPopUpTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->O8ooOoo〇(Landroid/graphics/Typeface;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPopupBgColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->〇oo〇(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPopupPosition(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->o〇O8〇〇o(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPopupTextColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->〇00(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPopupTextSize(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->O〇8O8〇008(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSectionName(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->〇oOO8O8(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setStateChangeListener(Lcom/intsig/camscanner/view/recyclerview_fastscroll/interfaces/OnFastScrollStateChangeListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->setOnFastScrollStateChangeListener(Lcom/intsig/camscanner/view/recyclerview_fastscroll/interfaces/OnFastScrollStateChangeListener;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setThumbColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->〇0000OOO(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setThumbEnabled(Z)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->setFastScrollEnabled(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setThumbInactiveColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->o〇〇0〇(I)V

    return-void
.end method

.method public setThumbInactiveColor(Z)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇o00〇〇Oo(Z)V

    return-void
.end method

.method public setTrackColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->oo〇(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected 〇8o8o〇()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v0, v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 14
    .line 15
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->getReverseLayout()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    return v0

    .line 20
    :cond_0
    const/4 v0, 0x0

    .line 21
    return v0
.end method

.method public 〇O8o08O()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    instance-of v1, v1, Landroidx/recyclerview/widget/GridLayoutManager;

    .line 21
    .line 22
    if-eqz v1, :cond_1

    .line 23
    .line 24
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    check-cast v1, Landroidx/recyclerview/widget/GridLayoutManager;

    .line 29
    .line 30
    invoke-virtual {v1}, Landroidx/recyclerview/widget/GridLayoutManager;->getSpanCount()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    int-to-double v2, v0

    .line 35
    int-to-double v0, v1

    .line 36
    div-double/2addr v2, v0

    .line 37
    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    .line 38
    .line 39
    .line 40
    move-result-wide v0

    .line 41
    double-to-int v0, v0

    .line 42
    :cond_1
    const/4 v1, -0x1

    .line 43
    if-nez v0, :cond_2

    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 46
    .line 47
    invoke-virtual {v0, v1, v1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->OOO〇O0(II)V

    .line 48
    .line 49
    .line 50
    return-void

    .line 51
    :cond_2
    iget-object v2, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->OO:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;

    .line 52
    .line 53
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->〇80〇808〇O(Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;)V

    .line 54
    .line 55
    .line 56
    iget-object v2, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->OO:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;

    .line 57
    .line 58
    iget v3, v2, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;->〇080:I

    .line 59
    .line 60
    if-gez v3, :cond_3

    .line 61
    .line 62
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 63
    .line 64
    invoke-virtual {v0, v1, v1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->OOO〇O0(II)V

    .line 65
    .line 66
    .line 67
    return-void

    .line 68
    :cond_3
    invoke-virtual {p0, v2, v0}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->Oooo8o0〇(Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView$ScrollPositionState;I)V

    .line 69
    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇o00〇〇Oo(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScrollRecyclerView;->o0:Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/recyclerview_fastscroll/views/FastScroller;->〇80〇808〇O(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

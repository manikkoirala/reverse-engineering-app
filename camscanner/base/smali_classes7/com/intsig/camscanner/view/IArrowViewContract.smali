.class public interface abstract Lcom/intsig/camscanner/view/IArrowViewContract;
.super Ljava/lang/Object;
.source "IArrowViewContract.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/IArrowViewContract$ArrowDirection;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract getArrowMarginLeft()I
.end method

.method public abstract getView()Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract setArrowMarginLeft(I)V
.end method

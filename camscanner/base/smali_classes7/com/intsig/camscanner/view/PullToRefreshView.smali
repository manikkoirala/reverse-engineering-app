.class public Lcom/intsig/camscanner/view/PullToRefreshView;
.super Landroid/widget/RelativeLayout;
.source "PullToRefreshView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/PullToRefreshView$OnHeaderRefreshListener;
    }
.end annotation


# instance fields
.field private O0O:Lcom/intsig/camscanner/view/PullToRefreshView$OnHeaderRefreshListener;

.field private O8o08O8O:Landroid/widget/ImageView;

.field private OO:Landroid/view/View;

.field private OO〇00〇8oO:I

.field private o0:I

.field private o8oOOo:Z

.field private o8〇OO0〇0o:I

.field private oOo0:Landroid/view/LayoutInflater;

.field private oOo〇8o008:Landroid/widget/ProgressBar;

.field private ooo0〇〇O:Landroid/view/animation/RotateAnimation;

.field private o〇00O:I

.field private 〇080OO8〇0:Landroid/widget/TextView;

.field private 〇08O〇00〇o:Landroid/widget/AdapterView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/AdapterView<",
            "*>;"
        }
    .end annotation
.end field

.field private 〇0O:Landroid/widget/TextView;

.field private 〇8〇oO〇〇8o:Landroid/view/animation/RotateAnimation;

.field private 〇OOo8〇0:Z

.field public 〇O〇〇O8:I

.field private 〇〇08O:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x0

    .line 5
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇〇08O:Z

    .line 6
    .line 7
    const/4 p1, 0x1

    .line 8
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o8oOOo:Z

    .line 9
    .line 10
    const/16 p1, 0xa

    .line 11
    .line 12
    iput p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇O〇〇O8:I

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/view/PullToRefreshView;->Oo08()V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private O8()V
    .locals 2

    .line 1
    const/4 v0, 0x4

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->OO〇00〇8oO:I

    .line 3
    .line 4
    iget v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o〇00O:I

    .line 5
    .line 6
    neg-int v0, v0

    .line 7
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/PullToRefreshView;->setNewScrollY(I)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->O8o08O8O:Landroid/widget/ImageView;

    .line 11
    .line 12
    const/16 v1, 0x8

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->O8o08O8O:Landroid/widget/ImageView;

    .line 18
    .line 19
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->O8o08O8O:Landroid/widget/ImageView;

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->oOo〇8o008:Landroid/widget/ProgressBar;

    .line 29
    .line 30
    const/4 v1, 0x0

    .line 31
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 32
    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇080OO8〇0:Landroid/widget/TextView;

    .line 35
    .line 36
    const v1, 0x7f130110

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 40
    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->O0O:Lcom/intsig/camscanner/view/PullToRefreshView$OnHeaderRefreshListener;

    .line 43
    .line 44
    if-eqz v0, :cond_0

    .line 45
    .line 46
    invoke-interface {v0, p0}, Lcom/intsig/camscanner/view/PullToRefreshView$OnHeaderRefreshListener;->〇080(Lcom/intsig/camscanner/view/PullToRefreshView;)V

    .line 47
    .line 48
    .line 49
    :cond_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private Oo08()V
    .locals 9

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const v1, 0x7f070556

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    iput v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇O〇〇O8:I

    .line 17
    .line 18
    new-instance v0, Landroid/view/animation/RotateAnimation;

    .line 19
    .line 20
    const/4 v2, 0x0

    .line 21
    const/high16 v3, -0x3ccc0000    # -180.0f

    .line 22
    .line 23
    const/4 v4, 0x1

    .line 24
    const/high16 v5, 0x3f000000    # 0.5f

    .line 25
    .line 26
    const/4 v6, 0x1

    .line 27
    const/high16 v7, 0x3f000000    # 0.5f

    .line 28
    .line 29
    move-object v1, v0

    .line 30
    invoke-direct/range {v1 .. v7}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 31
    .line 32
    .line 33
    iput-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇8〇oO〇〇8o:Landroid/view/animation/RotateAnimation;

    .line 34
    .line 35
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    .line 36
    .line 37
    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 41
    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇8〇oO〇〇8o:Landroid/view/animation/RotateAnimation;

    .line 44
    .line 45
    const-wide/16 v1, 0xfa

    .line 46
    .line 47
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 48
    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇8〇oO〇〇8o:Landroid/view/animation/RotateAnimation;

    .line 51
    .line 52
    const/4 v1, 0x1

    .line 53
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 54
    .line 55
    .line 56
    new-instance v0, Landroid/view/animation/RotateAnimation;

    .line 57
    .line 58
    const/4 v4, 0x0

    .line 59
    const/4 v5, 0x1

    .line 60
    const/high16 v6, 0x3f000000    # 0.5f

    .line 61
    .line 62
    const/4 v7, 0x1

    .line 63
    const/high16 v8, 0x3f000000    # 0.5f

    .line 64
    .line 65
    move-object v2, v0

    .line 66
    invoke-direct/range {v2 .. v8}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 67
    .line 68
    .line 69
    iput-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->ooo0〇〇O:Landroid/view/animation/RotateAnimation;

    .line 70
    .line 71
    new-instance v2, Landroid/view/animation/LinearInterpolator;

    .line 72
    .line 73
    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 77
    .line 78
    .line 79
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->ooo0〇〇O:Landroid/view/animation/RotateAnimation;

    .line 80
    .line 81
    const-wide/16 v2, 0xc8

    .line 82
    .line 83
    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 84
    .line 85
    .line 86
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->ooo0〇〇O:Landroid/view/animation/RotateAnimation;

    .line 87
    .line 88
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    iput-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->oOo0:Landroid/view/LayoutInflater;

    .line 100
    .line 101
    invoke-direct {p0}, Lcom/intsig/camscanner/view/PullToRefreshView;->〇080()V

    .line 102
    .line 103
    .line 104
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private o〇0()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    :goto_0
    if-ge v1, v0, :cond_1

    .line 7
    .line 8
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    instance-of v3, v2, Landroid/widget/AdapterView;

    .line 13
    .line 14
    if-eqz v3, :cond_0

    .line 15
    .line 16
    check-cast v2, Landroid/widget/AdapterView;

    .line 17
    .line 18
    iput-object v2, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇08O〇00〇o:Landroid/widget/AdapterView;

    .line 19
    .line 20
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇08O〇00〇o:Landroid/widget/AdapterView;

    .line 24
    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    return-void

    .line 28
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 29
    .line 30
    const-string v1, "must contain a AdapterView in this layout!"

    .line 31
    .line 32
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    throw v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private setNewScrollY(I)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0, p1}, Landroid/view/View;->scrollTo(II)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇080()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->oOo0:Landroid/view/LayoutInflater;

    .line 2
    .line 3
    const v1, 0x7f0d070d

    .line 4
    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->OO:Landroid/view/View;

    .line 12
    .line 13
    const v1, 0x7f0a0ea2

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    check-cast v0, Landroid/widget/ImageView;

    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->O8o08O8O:Landroid/widget/ImageView;

    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->OO:Landroid/view/View;

    .line 25
    .line 26
    const v1, 0x7f0a0ea4

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    check-cast v0, Landroid/widget/TextView;

    .line 34
    .line 35
    iput-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇080OO8〇0:Landroid/widget/TextView;

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->OO:Landroid/view/View;

    .line 38
    .line 39
    const v1, 0x7f0a0ea5

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    check-cast v0, Landroid/widget/TextView;

    .line 47
    .line 48
    iput-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇0O:Landroid/widget/TextView;

    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->OO:Landroid/view/View;

    .line 51
    .line 52
    const v1, 0x7f0a0ea3

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    check-cast v0, Landroid/widget/ProgressBar;

    .line 60
    .line 61
    iput-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->oOo〇8o008:Landroid/widget/ProgressBar;

    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->OO:Landroid/view/View;

    .line 64
    .line 65
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/PullToRefreshView;->〇80〇808〇O(Landroid/view/View;)V

    .line 66
    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->OO:Landroid/view/View;

    .line 69
    .line 70
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    iput v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o〇00O:I

    .line 75
    .line 76
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 77
    .line 78
    const/4 v1, -0x1

    .line 79
    iget v2, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o〇00O:I

    .line 80
    .line 81
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 82
    .line 83
    .line 84
    iget v1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o〇00O:I

    .line 85
    .line 86
    neg-int v1, v1

    .line 87
    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 88
    .line 89
    iget-object v1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->OO:Landroid/view/View;

    .line 90
    .line 91
    invoke-virtual {p0, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 92
    .line 93
    .line 94
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private 〇80〇808〇O(Landroid/view/View;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    .line 8
    .line 9
    const/4 v1, -0x1

    .line 10
    const/4 v2, -0x2

    .line 11
    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 12
    .line 13
    .line 14
    :cond_0
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 15
    .line 16
    const/4 v2, 0x0

    .line 17
    invoke-static {v2, v2, v1}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 22
    .line 23
    if-lez v0, :cond_1

    .line 24
    .line 25
    const/high16 v2, 0x40000000    # 2.0f

    .line 26
    .line 27
    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    goto :goto_0

    .line 32
    :cond_1
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private 〇o00〇〇Oo(I)I
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    int-to-float v1, p1

    .line 7
    const v2, 0x3f19999a    # 0.6f

    .line 8
    .line 9
    .line 10
    mul-float v1, v1, v2

    .line 11
    .line 12
    sub-float/2addr v0, v1

    .line 13
    float-to-int v0, v0

    .line 14
    const/4 v1, 0x0

    .line 15
    if-gez p1, :cond_0

    .line 16
    .line 17
    iget p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o8〇OO0〇0o:I

    .line 18
    .line 19
    const/4 v2, 0x1

    .line 20
    if-ne p1, v2, :cond_0

    .line 21
    .line 22
    if-lez v0, :cond_0

    .line 23
    .line 24
    return v1

    .line 25
    :cond_0
    invoke-virtual {p0, v1, v0}, Landroid/view/View;->scrollTo(II)V

    .line 26
    .line 27
    .line 28
    return v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private 〇o〇(I)V
    .locals 4

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/PullToRefreshView;->〇o00〇〇Oo(I)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    iget v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o〇00O:I

    .line 6
    .line 7
    neg-int v1, v0

    .line 8
    const/4 v2, 0x0

    .line 9
    if-gt p1, v1, :cond_0

    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->OO〇00〇8oO:I

    .line 12
    .line 13
    const/4 v3, 0x3

    .line 14
    if-eq v1, v3, :cond_0

    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇080OO8〇0:Landroid/widget/TextView;

    .line 17
    .line 18
    const v0, 0x7f13010f

    .line 19
    .line 20
    .line 21
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 22
    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇0O:Landroid/widget/TextView;

    .line 25
    .line 26
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->O8o08O8O:Landroid/widget/ImageView;

    .line 30
    .line 31
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->O8o08O8O:Landroid/widget/ImageView;

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇8〇oO〇〇8o:Landroid/view/animation/RotateAnimation;

    .line 37
    .line 38
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 39
    .line 40
    .line 41
    iput v3, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->OO〇00〇8oO:I

    .line 42
    .line 43
    const/4 p1, 0x1

    .line 44
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇〇08O:Z

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    if-gtz p1, :cond_2

    .line 48
    .line 49
    neg-int v0, v0

    .line 50
    if-le p1, v0, :cond_2

    .line 51
    .line 52
    iget-boolean p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇〇08O:Z

    .line 53
    .line 54
    if-eqz p1, :cond_1

    .line 55
    .line 56
    iput-boolean v2, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇〇08O:Z

    .line 57
    .line 58
    iget-object p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->O8o08O8O:Landroid/widget/ImageView;

    .line 59
    .line 60
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 61
    .line 62
    .line 63
    iget-object p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->O8o08O8O:Landroid/widget/ImageView;

    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->ooo0〇〇O:Landroid/view/animation/RotateAnimation;

    .line 66
    .line 67
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 68
    .line 69
    .line 70
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇080OO8〇0:Landroid/widget/TextView;

    .line 71
    .line 72
    const v0, 0x7f13010e

    .line 73
    .line 74
    .line 75
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 76
    .line 77
    .line 78
    const/4 p1, 0x2

    .line 79
    iput p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->OO〇00〇8oO:I

    .line 80
    .line 81
    :cond_2
    :goto_0
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private 〇〇888(I)Z
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->OO〇00〇8oO:I

    .line 2
    .line 3
    const/4 v1, 0x4

    .line 4
    const/4 v2, 0x0

    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    return v2

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇08O〇00〇o:Landroid/widget/AdapterView;

    .line 9
    .line 10
    if-eqz v0, :cond_4

    .line 11
    .line 12
    iget v1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇O〇〇O8:I

    .line 13
    .line 14
    if-le p1, v1, :cond_4

    .line 15
    .line 16
    invoke-virtual {v0}, Landroid/widget/AdapterView;->getCount()I

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    const/4 v0, 0x1

    .line 21
    if-lez p1, :cond_3

    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇08O〇00〇o:Landroid/widget/AdapterView;

    .line 24
    .line 25
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    if-nez p1, :cond_1

    .line 30
    .line 31
    return v2

    .line 32
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇08O〇00〇o:Landroid/widget/AdapterView;

    .line 33
    .line 34
    invoke-virtual {v1}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    if-nez v1, :cond_2

    .line 39
    .line 40
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-nez v1, :cond_2

    .line 45
    .line 46
    iput v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o8〇OO0〇0o:I

    .line 47
    .line 48
    return v0

    .line 49
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    iget-object v1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇08O〇00〇o:Landroid/widget/AdapterView;

    .line 54
    .line 55
    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    iget-object v3, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇08O〇00〇o:Landroid/widget/AdapterView;

    .line 60
    .line 61
    invoke-virtual {v3}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    .line 62
    .line 63
    .line 64
    move-result v3

    .line 65
    if-nez v3, :cond_4

    .line 66
    .line 67
    sub-int/2addr p1, v1

    .line 68
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    .line 69
    .line 70
    .line 71
    move-result p1

    .line 72
    const/16 v1, 0xf

    .line 73
    .line 74
    if-gt p1, v1, :cond_4

    .line 75
    .line 76
    iput v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o8〇OO0〇0o:I

    .line 77
    .line 78
    return v0

    .line 79
    :cond_3
    iput v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o8〇OO0〇0o:I

    .line 80
    .line 81
    return v0

    .line 82
    :cond_4
    return v2
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public OO0o〇〇〇〇0()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/PullToRefreshView;->setNewScrollY(I)V

    .line 3
    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->O8o08O8O:Landroid/widget/ImageView;

    .line 6
    .line 7
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->O8o08O8O:Landroid/widget/ImageView;

    .line 11
    .line 12
    const v1, 0x7f080caf

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇080OO8〇0:Landroid/widget/TextView;

    .line 19
    .line 20
    const v1, 0x7f13010e

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->oOo〇8o008:Landroid/widget/ProgressBar;

    .line 27
    .line 28
    const/16 v1, 0x8

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 31
    .line 32
    .line 33
    const/4 v0, 0x2

    .line 34
    iput v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->OO〇00〇8oO:I

    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public oO80()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇OOo8〇0:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/view/PullToRefreshView;->o〇0()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇OOo8〇0:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1

    .line 10
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    float-to-int v0, v0

    .line 15
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-eqz p1, :cond_3

    .line 20
    .line 21
    const/4 v1, 0x2

    .line 22
    if-eq p1, v1, :cond_1

    .line 23
    .line 24
    goto :goto_1

    .line 25
    :cond_1
    iget p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o0:I

    .line 26
    .line 27
    sub-int/2addr v0, p1

    .line 28
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/PullToRefreshView;->〇〇888(I)Z

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    if-eqz p1, :cond_4

    .line 33
    .line 34
    iget-boolean p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o8oOOo:Z

    .line 35
    .line 36
    if-eqz p1, :cond_2

    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇0O:Landroid/widget/TextView;

    .line 39
    .line 40
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-static {v0}, Lcom/intsig/camscanner/app/AppUtil;->O8ooOoo〇(Landroid/content/Context;)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇0O:Landroid/widget/TextView;

    .line 53
    .line 54
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/collaborate/CollaborateUtil;->〇o〇(Landroid/content/Context;)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    .line 64
    .line 65
    :goto_0
    const/4 p1, 0x1

    .line 66
    return p1

    .line 67
    :cond_3
    iput v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o0:I

    .line 68
    .line 69
    :cond_4
    :goto_1
    const/4 p1, 0x0

    .line 70
    return p1
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇OOo8〇0:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1

    .line 10
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    float-to-int v0, v0

    .line 15
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    const/4 v2, 0x1

    .line 20
    if-eq v1, v2, :cond_3

    .line 21
    .line 22
    const/4 v3, 0x2

    .line 23
    if-eq v1, v3, :cond_1

    .line 24
    .line 25
    const/4 v0, 0x3

    .line 26
    if-eq v1, v0, :cond_3

    .line 27
    .line 28
    goto :goto_1

    .line 29
    :cond_1
    iget v1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o0:I

    .line 30
    .line 31
    sub-int v1, v0, v1

    .line 32
    .line 33
    iget v3, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o8〇OO0〇0o:I

    .line 34
    .line 35
    if-ne v3, v2, :cond_2

    .line 36
    .line 37
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/view/PullToRefreshView;->〇o〇(I)V

    .line 38
    .line 39
    .line 40
    :cond_2
    iput v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o0:I

    .line 41
    .line 42
    goto :goto_1

    .line 43
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    iget v1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o8〇OO0〇0o:I

    .line 48
    .line 49
    const/4 v3, 0x0

    .line 50
    if-ne v1, v2, :cond_5

    .line 51
    .line 52
    iget v1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o〇00O:I

    .line 53
    .line 54
    neg-int v1, v1

    .line 55
    if-ge v0, v1, :cond_4

    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/view/PullToRefreshView;->O8()V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_4
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/view/PullToRefreshView;->setNewScrollY(I)V

    .line 62
    .line 63
    .line 64
    :cond_5
    :goto_0
    iput-boolean v3, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇〇08O:Z

    .line 65
    .line 66
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 67
    .line 68
    .line 69
    move-result p1

    .line 70
    return p1
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setCollaborateMode(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->o8oOOo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLastUpdated(Ljava/lang/CharSequence;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇0O:Landroid/widget/TextView;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇0O:Landroid/widget/TextView;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇0O:Landroid/widget/TextView;

    .line 16
    .line 17
    const/16 v0, 0x8

    .line 18
    .line 19
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 20
    .line 21
    .line 22
    :goto_0
    return-void
    .line 23
    .line 24
.end method

.method public setOnHeaderRefreshListener(Lcom/intsig/camscanner/view/PullToRefreshView$OnHeaderRefreshListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->O0O:Lcom/intsig/camscanner/view/PullToRefreshView$OnHeaderRefreshListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇8o8o〇()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/PullToRefreshView;->〇OOo8〇0:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

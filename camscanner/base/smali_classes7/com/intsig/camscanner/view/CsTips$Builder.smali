.class public final Lcom/intsig/camscanner/view/CsTips$Builder;
.super Ljava/lang/Object;
.source "CsTips.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/view/CsTips;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8:I

.field private Oo08:I

.field private oO80:Lcom/intsig/camscanner/view/CsTips$Type;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇0:Z

.field private final 〇080:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o00〇〇Oo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o〇:I

.field private 〇〇888:Lcom/intsig/camscanner/view/CsTips$OnCloseListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mContext"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->〇080:Landroid/content/Context;

    .line 10
    .line 11
    const-string p1, ""

    .line 12
    .line 13
    iput-object p1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->〇o00〇〇Oo:Ljava/lang/String;

    .line 14
    .line 15
    const/4 p1, 0x4

    .line 16
    iput p1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->〇o〇:I

    .line 17
    .line 18
    sget-object p1, Lcom/intsig/camscanner/view/CsTips$Type;->TRIANGLE:Lcom/intsig/camscanner/view/CsTips$Type;

    .line 19
    .line 20
    iput-object p1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->oO80:Lcom/intsig/camscanner/view/CsTips$Type;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
.end method


# virtual methods
.method public final O8(Lcom/intsig/camscanner/view/CsTips$Type;)Lcom/intsig/camscanner/view/CsTips$Builder;
    .locals 1
    .param p1    # Lcom/intsig/camscanner/view/CsTips$Type;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "tipType"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->oO80:Lcom/intsig/camscanner/view/CsTips$Type;

    .line 7
    .line 8
    return-object p0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final Oo08(Ljava/lang/String;)Lcom/intsig/camscanner/view/CsTips$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "title"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->〇o00〇〇Oo:Ljava/lang/String;

    .line 7
    .line 8
    return-object p0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final o〇0(I)Lcom/intsig/camscanner/view/CsTips$Builder;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->Oo08:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇080()Lcom/intsig/camscanner/view/CsTips;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/view/CsTips;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->〇080:Landroid/content/Context;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/view/CsTips;-><init>(Landroid/content/Context;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 7
    .line 8
    .line 9
    iget v1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->〇o〇:I

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/camscanner/view/CsTips;->〇o00〇〇Oo(Lcom/intsig/camscanner/view/CsTips;I)V

    .line 12
    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->〇o00〇〇Oo:Ljava/lang/String;

    .line 15
    .line 16
    invoke-static {v0, v1}, Lcom/intsig/camscanner/view/CsTips;->o〇0(Lcom/intsig/camscanner/view/CsTips;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    iget v1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->O8:I

    .line 20
    .line 21
    invoke-static {v0, v1}, Lcom/intsig/camscanner/view/CsTips;->〇o〇(Lcom/intsig/camscanner/view/CsTips;I)V

    .line 22
    .line 23
    .line 24
    iget-boolean v1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->o〇0:Z

    .line 25
    .line 26
    invoke-static {v0, v1}, Lcom/intsig/camscanner/view/CsTips;->Oo08(Lcom/intsig/camscanner/view/CsTips;Z)V

    .line 27
    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->〇〇888:Lcom/intsig/camscanner/view/CsTips$OnCloseListener;

    .line 30
    .line 31
    invoke-static {v0, v1}, Lcom/intsig/camscanner/view/CsTips;->O8(Lcom/intsig/camscanner/view/CsTips;Lcom/intsig/camscanner/view/CsTips$OnCloseListener;)V

    .line 32
    .line 33
    .line 34
    iget v1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->Oo08:I

    .line 35
    .line 36
    invoke-static {v0, v1}, Lcom/intsig/camscanner/view/CsTips;->oO80(Lcom/intsig/camscanner/view/CsTips;I)V

    .line 37
    .line 38
    .line 39
    iget-object v1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->oO80:Lcom/intsig/camscanner/view/CsTips$Type;

    .line 40
    .line 41
    invoke-static {v0, v1}, Lcom/intsig/camscanner/view/CsTips;->〇〇888(Lcom/intsig/camscanner/view/CsTips;Lcom/intsig/camscanner/view/CsTips$Type;)V

    .line 42
    .line 43
    .line 44
    return-object v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public final 〇o00〇〇Oo(I)Lcom/intsig/camscanner/view/CsTips$Builder;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->〇o〇:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇o〇(Lcom/intsig/camscanner/view/CsTips$OnCloseListener;)Lcom/intsig/camscanner/view/CsTips$Builder;
    .locals 1
    .param p1    # Lcom/intsig/camscanner/view/CsTips$OnCloseListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "onCloseListener"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->〇〇888:Lcom/intsig/camscanner/view/CsTips$OnCloseListener;

    .line 7
    .line 8
    return-object p0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇〇888(Z)Lcom/intsig/camscanner/view/CsTips$Builder;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/CsTips$Builder;->o〇0:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

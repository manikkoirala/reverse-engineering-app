.class public Lcom/intsig/camscanner/view/GuidePurchaseStyleView;
.super Ljava/lang/Object;
.source "GuidePurchaseStyleView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/GuidePurchaseStyleView$AbsPurchaseStyleUi;,
        Lcom/intsig/camscanner/view/GuidePurchaseStyleView$PurchaseStyleFakeiOS2;,
        Lcom/intsig/camscanner/view/GuidePurchaseStyleView$PurchaseStyleFakeiOS3;,
        Lcom/intsig/camscanner/view/GuidePurchaseStyleView$UserAndComment;
    }
.end annotation


# instance fields
.field private final O8:Landroid/app/Activity;

.field private Oo08:Lcom/intsig/camscanner/view/GuidePurchaseStyleView$AbsPurchaseStyleUi;

.field private final o〇0:Z

.field private final 〇080:Lcom/intsig/camscanner/guide/OnGuideGpPurchaseBottomListener;

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

.field private final 〇o〇:Landroid/view/View;

.field private final 〇〇888:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Lcom/intsig/camscanner/guide/OnGuideGpPurchaseBottomListener;)V
    .locals 7

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 1
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;ZLcom/intsig/camscanner/purchase/track/PurchaseTracker;Lcom/intsig/camscanner/guide/OnGuideGpPurchaseBottomListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;ZLcom/intsig/camscanner/purchase/track/PurchaseTracker;Lcom/intsig/camscanner/guide/OnGuideGpPurchaseBottomListener;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-boolean p4, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->o〇0:Z

    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->O8:Landroid/app/Activity;

    .line 5
    iput-object p2, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->〇o〇:Landroid/view/View;

    .line 6
    iput-object p6, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->〇080:Lcom/intsig/camscanner/guide/OnGuideGpPurchaseBottomListener;

    .line 7
    iput-object p3, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->〇o00〇〇Oo:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 8
    iput-object p5, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->〇〇888:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->〇〇888()V

    return-void
.end method

.method static bridge synthetic O8(Lcom/intsig/camscanner/view/GuidePurchaseStyleView;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->〇o〇:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private synthetic OO0o〇〇〇〇0(Z)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->O8:Landroid/app/Activity;

    .line 4
    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    iget-object p1, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->〇〇888:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 8
    .line 9
    const-string v0, "price_show_success"

    .line 10
    .line 11
    invoke-static {p1, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTrackerUtil;->〇o00〇〇Oo(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->Oo08:Lcom/intsig/camscanner/view/GuidePurchaseStyleView$AbsPurchaseStyleUi;

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/GuidePurchaseStyleView$AbsPurchaseStyleUi;->O8()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :catch_0
    move-exception p1

    .line 21
    const-string v0, "GuidePurchaseStyleView"

    .line 22
    .line 23
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 24
    .line 25
    .line 26
    :cond_0
    :goto_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method static bridge synthetic Oo08(Lcom/intsig/camscanner/view/GuidePurchaseStyleView;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->〇〇888:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private oO80()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->〇o00〇〇Oo:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/view/〇〇808〇;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/view/〇〇808〇;-><init>(Lcom/intsig/camscanner/view/GuidePurchaseStyleView;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->O000(Lcom/intsig/camscanner/purchase/OnProductLoadListener;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/view/GuidePurchaseStyleView;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->OO0o〇〇〇〇0(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇80〇808〇O()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->o〇0:Z

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->o〇0(Z)Lcom/intsig/camscanner/view/GuidePurchaseStyleView$AbsPurchaseStyleUi;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->Oo08:Lcom/intsig/camscanner/view/GuidePurchaseStyleView$AbsPurchaseStyleUi;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v1, "otherUiImpl.class = "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->Oo08:Lcom/intsig/camscanner/view/GuidePurchaseStyleView$AbsPurchaseStyleUi;

    .line 22
    .line 23
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const-string v1, "GuidePurchaseStyleView"

    .line 35
    .line 36
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->Oo08:Lcom/intsig/camscanner/view/GuidePurchaseStyleView$AbsPurchaseStyleUi;

    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/GuidePurchaseStyleView$AbsPurchaseStyleUi;->〇o00〇〇Oo()V

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->Oo08:Lcom/intsig/camscanner/view/GuidePurchaseStyleView$AbsPurchaseStyleUi;

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/GuidePurchaseStyleView$AbsPurchaseStyleUi;->〇080()V

    .line 47
    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->Oo08:Lcom/intsig/camscanner/view/GuidePurchaseStyleView$AbsPurchaseStyleUi;

    .line 50
    .line 51
    iget-object v1, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->〇080:Lcom/intsig/camscanner/guide/OnGuideGpPurchaseBottomListener;

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/GuidePurchaseStyleView$AbsPurchaseStyleUi;->〇o〇(Lcom/intsig/camscanner/guide/OnGuideGpPurchaseBottomListener;)V

    .line 54
    .line 55
    .line 56
    :cond_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/view/GuidePurchaseStyleView;)Landroid/app/Activity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->O8:Landroid/app/Activity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/view/GuidePurchaseStyleView;)Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->〇o00〇〇Oo:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇〇888()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->〇80〇808〇O()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/view/GuidePurchaseStyleView;->oO80()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method o〇0(Z)Lcom/intsig/camscanner/view/GuidePurchaseStyleView$AbsPurchaseStyleUi;
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    new-instance p1, Lcom/intsig/camscanner/view/GuidePurchaseStyleView$PurchaseStyleFakeiOS2;

    .line 4
    .line 5
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/view/GuidePurchaseStyleView$PurchaseStyleFakeiOS2;-><init>(Lcom/intsig/camscanner/view/GuidePurchaseStyleView;)V

    .line 6
    .line 7
    .line 8
    return-object p1

    .line 9
    :cond_0
    new-instance p1, Lcom/intsig/camscanner/view/GuidePurchaseStyleView$PurchaseStyleFakeiOS3;

    .line 10
    .line 11
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/view/GuidePurchaseStyleView$PurchaseStyleFakeiOS3;-><init>(Lcom/intsig/camscanner/view/GuidePurchaseStyleView;)V

    .line 12
    .line 13
    .line 14
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public Lcom/intsig/camscanner/view/ZoomControl;
.super Ljava/lang/Object;
.source "ZoomControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/ZoomControl$OnZoomChangedListener;
    }
.end annotation


# instance fields
.field private O8:Lcom/intsig/camscanner/view/ZoomControl$OnZoomChangedListener;

.field private Oo08:I

.field private 〇080:Z

.field protected 〇o00〇〇Oo:I

.field protected 〇o〇:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/view/ZoomControl;->Oo08:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇080(I)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomControl;->O8:Lcom/intsig/camscanner/view/ZoomControl$OnZoomChangedListener;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eqz v0, :cond_6

    .line 5
    .line 6
    iget-boolean v2, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇080:Z

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    if-eqz v2, :cond_3

    .line 10
    .line 11
    iget v2, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇o〇:I

    .line 12
    .line 13
    if-ge p1, v2, :cond_0

    .line 14
    .line 15
    const/4 v3, 0x1

    .line 16
    :cond_0
    if-nez v3, :cond_1

    .line 17
    .line 18
    iget p1, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇o00〇〇Oo:I

    .line 19
    .line 20
    if-ne v2, p1, :cond_2

    .line 21
    .line 22
    :cond_1
    if-ne v3, v1, :cond_6

    .line 23
    .line 24
    if-eqz v2, :cond_6

    .line 25
    .line 26
    :cond_2
    invoke-interface {v0, v3}, Lcom/intsig/camscanner/view/ZoomControl$OnZoomChangedListener;->〇o00〇〇Oo(I)V

    .line 27
    .line 28
    .line 29
    goto :goto_1

    .line 30
    :cond_3
    iget v2, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇o00〇〇Oo:I

    .line 31
    .line 32
    if-le p1, v2, :cond_4

    .line 33
    .line 34
    move p1, v2

    .line 35
    :cond_4
    if-gez p1, :cond_5

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_5
    move v3, p1

    .line 39
    :goto_0
    invoke-interface {v0, v3}, Lcom/intsig/camscanner/view/ZoomControl$OnZoomChangedListener;->〇080(I)V

    .line 40
    .line 41
    .line 42
    iput v3, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇o〇:I

    .line 43
    .line 44
    :cond_6
    :goto_1
    return v1
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public O8(I)V
    .locals 2

    .line 1
    if-ltz p1, :cond_1

    .line 2
    .line 3
    iget v0, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇o00〇〇Oo:I

    .line 4
    .line 5
    if-le p1, v0, :cond_0

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    iput p1, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇o〇:I

    .line 9
    .line 10
    return-void

    .line 11
    :cond_1
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v1, "setZoomIndex() invalid value:"

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const-string v0, "ZoomControl"

    .line 29
    .line 30
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public OO0o〇〇〇〇0(I)Z
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "zoomOut() steps="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "ZoomControl"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget v0, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇o〇:I

    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    if-nez v0, :cond_0

    .line 27
    .line 28
    return v1

    .line 29
    :cond_0
    sub-int v2, v0, p1

    .line 30
    .line 31
    if-gez v2, :cond_1

    .line 32
    .line 33
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/view/ZoomControl;->〇080(I)Z

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    return p1

    .line 38
    :cond_1
    sub-int/2addr v0, p1

    .line 39
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/ZoomControl;->〇080(I)Z

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    return p1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public Oo08(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇o00〇〇Oo:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public oO80(I)Z
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "zoomIn() steps="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "ZoomControl"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget v0, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇o〇:I

    .line 24
    .line 25
    iget v1, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇o00〇〇Oo:I

    .line 26
    .line 27
    if-ne v0, v1, :cond_0

    .line 28
    .line 29
    const/4 p1, 0x0

    .line 30
    return p1

    .line 31
    :cond_0
    add-int v2, v0, p1

    .line 32
    .line 33
    if-lt v2, v1, :cond_1

    .line 34
    .line 35
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/view/ZoomControl;->〇080(I)Z

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    return p1

    .line 40
    :cond_1
    add-int/2addr v0, p1

    .line 41
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/ZoomControl;->〇080(I)Z

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    return p1
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public o〇0()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇080:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomControl;->O8:Lcom/intsig/camscanner/view/ZoomControl$OnZoomChangedListener;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 v1, 0x2

    .line 10
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/view/ZoomControl$OnZoomChangedListener;->〇o00〇〇Oo(I)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇80〇808〇O()Z
    .locals 2

    .line 1
    const-string v0, "ZoomControl"

    .line 2
    .line 3
    const-string v1, "zoomOut()"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget v0, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇o〇:I

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    iget v1, p0, Lcom/intsig/camscanner/view/ZoomControl;->Oo08:I

    .line 15
    .line 16
    sub-int/2addr v0, v1

    .line 17
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/ZoomControl;->〇080(I)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    :goto_0
    return v0
.end method

.method public 〇o00〇〇Oo(Lcom/intsig/camscanner/view/ZoomControl$OnZoomChangedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ZoomControl;->O8:Lcom/intsig/camscanner/view/ZoomControl$OnZoomChangedListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇o〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇080:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇〇888()Z
    .locals 2

    .line 1
    const-string v0, "ZoomControl"

    .line 2
    .line 3
    const-string v1, "zoomIn()"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget v0, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇o〇:I

    .line 9
    .line 10
    iget v1, p0, Lcom/intsig/camscanner/view/ZoomControl;->〇o00〇〇Oo:I

    .line 11
    .line 12
    if-ne v0, v1, :cond_0

    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    iget v1, p0, Lcom/intsig/camscanner/view/ZoomControl;->Oo08:I

    .line 17
    .line 18
    add-int/2addr v0, v1

    .line 19
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/ZoomControl;->〇080(I)Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    :goto_0
    return v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

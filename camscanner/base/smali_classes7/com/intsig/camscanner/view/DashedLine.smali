.class public final Lcom/intsig/camscanner/view/DashedLine;
.super Landroid/view/View;
.source "DashedLine.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8o08O8O:Landroid/graphics/Path;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO:I

.field private o0:I

.field private o〇00O:Z

.field private 〇080OO8〇0:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:I

.field private 〇OOo8〇0:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 v0, 0x8

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/view/DashedLine;->o0:I

    .line 3
    iput v0, p0, Lcom/intsig/camscanner/view/DashedLine;->〇OOo8〇0:I

    const/4 v0, 0x2

    .line 4
    iput v0, p0, Lcom/intsig/camscanner/view/DashedLine;->OO:I

    const v0, -0x846f5f

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/view/DashedLine;->〇08O〇00〇o:I

    .line 6
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/DashedLine;->O8o08O8O:Landroid/graphics/Path;

    .line 7
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/intsig/camscanner/view/DashedLine;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 8
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/view/DashedLine;->〇080(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 p3, 0x8

    .line 10
    iput p3, p0, Lcom/intsig/camscanner/view/DashedLine;->o0:I

    .line 11
    iput p3, p0, Lcom/intsig/camscanner/view/DashedLine;->〇OOo8〇0:I

    const/4 p3, 0x2

    .line 12
    iput p3, p0, Lcom/intsig/camscanner/view/DashedLine;->OO:I

    const p3, -0x846f5f

    .line 13
    iput p3, p0, Lcom/intsig/camscanner/view/DashedLine;->〇08O〇00〇o:I

    .line 14
    new-instance p3, Landroid/graphics/Path;

    invoke-direct {p3}, Landroid/graphics/Path;-><init>()V

    iput-object p3, p0, Lcom/intsig/camscanner/view/DashedLine;->O8o08O8O:Landroid/graphics/Path;

    .line 15
    new-instance p3, Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-direct {p3, v0}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p3, p0, Lcom/intsig/camscanner/view/DashedLine;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/view/DashedLine;->〇080(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private final 〇080(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    const/4 v1, 0x2

    .line 3
    const/4 v2, 0x0

    .line 4
    if-eqz p2, :cond_1

    .line 5
    .line 6
    sget-object v3, Lcom/intsig/camscanner/R$styleable;->DashedLine:[I

    .line 7
    .line 8
    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    const-string p2, "context.obtainStyledAttr\u2026, R.styleable.DashedLine)"

    .line 13
    .line 14
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const/4 p2, 0x3

    .line 18
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 19
    .line 20
    .line 21
    move-result p2

    .line 22
    iput p2, p0, Lcom/intsig/camscanner/view/DashedLine;->OO:I

    .line 23
    .line 24
    const/high16 p2, -0x1000000

    .line 25
    .line 26
    invoke-virtual {p1, v2, p2}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 27
    .line 28
    .line 29
    move-result p2

    .line 30
    iput p2, p0, Lcom/intsig/camscanner/view/DashedLine;->〇08O〇00〇o:I

    .line 31
    .line 32
    const/4 p2, 0x4

    .line 33
    const/16 v3, 0x8

    .line 34
    .line 35
    invoke-virtual {p1, p2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 36
    .line 37
    .line 38
    move-result p2

    .line 39
    iput p2, p0, Lcom/intsig/camscanner/view/DashedLine;->o0:I

    .line 40
    .line 41
    invoke-virtual {p1, v0, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 42
    .line 43
    .line 44
    move-result p2

    .line 45
    iput p2, p0, Lcom/intsig/camscanner/view/DashedLine;->〇OOo8〇0:I

    .line 46
    .line 47
    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    .line 48
    .line 49
    .line 50
    move-result p2

    .line 51
    if-eqz p2, :cond_0

    .line 52
    .line 53
    const/4 p2, 0x1

    .line 54
    goto :goto_0

    .line 55
    :cond_0
    const/4 p2, 0x0

    .line 56
    :goto_0
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/DashedLine;->o〇00O:Z

    .line 57
    .line 58
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    .line 60
    .line 61
    :cond_1
    new-instance p1, Landroid/graphics/DashPathEffect;

    .line 62
    .line 63
    new-array p2, v1, [F

    .line 64
    .line 65
    iget v1, p0, Lcom/intsig/camscanner/view/DashedLine;->o0:I

    .line 66
    .line 67
    int-to-float v1, v1

    .line 68
    aput v1, p2, v2

    .line 69
    .line 70
    iget v1, p0, Lcom/intsig/camscanner/view/DashedLine;->〇OOo8〇0:I

    .line 71
    .line 72
    int-to-float v1, v1

    .line 73
    aput v1, p2, v0

    .line 74
    .line 75
    const/4 v0, 0x0

    .line 76
    invoke-direct {p1, p2, v0}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 77
    .line 78
    .line 79
    iget-object p2, p0, Lcom/intsig/camscanner/view/DashedLine;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 80
    .line 81
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 82
    .line 83
    .line 84
    iget-object p1, p0, Lcom/intsig/camscanner/view/DashedLine;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 85
    .line 86
    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 87
    .line 88
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 89
    .line 90
    .line 91
    iget-object p1, p0, Lcom/intsig/camscanner/view/DashedLine;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 92
    .line 93
    iget p2, p0, Lcom/intsig/camscanner/view/DashedLine;->OO:I

    .line 94
    .line 95
    int-to-float p2, p2

    .line 96
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 97
    .line 98
    .line 99
    iget-object p1, p0, Lcom/intsig/camscanner/view/DashedLine;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 100
    .line 101
    iget p2, p0, Lcom/intsig/camscanner/view/DashedLine;->〇08O〇00〇o:I

    .line 102
    .line 103
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 104
    .line 105
    .line 106
    return-void
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_1

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/view/DashedLine;->O8o08O8O:Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 9
    .line 10
    .line 11
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/DashedLine;->o〇00O:Z

    .line 12
    .line 13
    const/high16 v1, 0x3f800000    # 1.0f

    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    const/high16 v3, 0x40000000    # 2.0f

    .line 17
    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/view/DashedLine;->O8o08O8O:Landroid/graphics/Path;

    .line 21
    .line 22
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 23
    .line 24
    .line 25
    move-result v4

    .line 26
    int-to-float v4, v4

    .line 27
    div-float/2addr v4, v3

    .line 28
    invoke-virtual {v0, v4, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/view/DashedLine;->O8o08O8O:Landroid/graphics/Path;

    .line 32
    .line 33
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    int-to-float v2, v2

    .line 38
    div-float/2addr v2, v3

    .line 39
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    int-to-float v3, v3

    .line 44
    mul-float v3, v3, v1

    .line 45
    .line 46
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/DashedLine;->O8o08O8O:Landroid/graphics/Path;

    .line 51
    .line 52
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 53
    .line 54
    .line 55
    move-result v4

    .line 56
    int-to-float v4, v4

    .line 57
    div-float/2addr v4, v3

    .line 58
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 59
    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/view/DashedLine;->O8o08O8O:Landroid/graphics/Path;

    .line 62
    .line 63
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    int-to-float v2, v2

    .line 68
    mul-float v2, v2, v1

    .line 69
    .line 70
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 71
    .line 72
    .line 73
    move-result v1

    .line 74
    int-to-float v1, v1

    .line 75
    div-float/2addr v1, v3

    .line 76
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 77
    .line 78
    .line 79
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/DashedLine;->O8o08O8O:Landroid/graphics/Path;

    .line 80
    .line 81
    iget-object v1, p0, Lcom/intsig/camscanner/view/DashedLine;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 82
    .line 83
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 84
    .line 85
    .line 86
    :cond_1
    return-void
    .line 87
.end method

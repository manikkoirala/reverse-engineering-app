.class public Lcom/intsig/camscanner/view/ImageEditViewLayout;
.super Landroid/widget/FrameLayout;
.source "ImageEditViewLayout.java"

# interfaces
.implements Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;


# instance fields
.field private O8o08O8O:Ljava/lang/String;

.field private OO:Lcom/intsig/camscanner/loadimage/RotateBitmap;

.field private OO〇00〇8oO:Z

.field private o0:Lcom/intsig/camscanner/view/ImageEditView;

.field private o8〇OO0〇0o:I

.field private oOo0:Landroidx/collection/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/LruCache<",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/scanner/ScannerUtils$CandidateLinesData;",
            ">;"
        }
    .end annotation
.end field

.field private volatile oOo〇8o008:Z

.field private o〇00O:[I

.field private 〇080OO8〇0:F

.field private 〇08O〇00〇o:[I

.field private 〇0O:Z

.field private 〇OOo8〇0:Lcom/intsig/camscanner/view/MagnifierView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/high16 p2, 0x3f800000    # 1.0f

    .line 2
    iput p2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇080OO8〇0:F

    const/4 p2, 0x1

    .line 3
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇0O:Z

    const/4 p2, 0x0

    .line 4
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->oOo〇8o008:Z

    .line 5
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->OO〇00〇8oO:Z

    .line 6
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/ImageEditViewLayout;->O8(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 7
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 p2, 0x3f800000    # 1.0f

    .line 8
    iput p2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇080OO8〇0:F

    const/4 p2, 0x1

    .line 9
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇0O:Z

    const/4 p2, 0x0

    .line 10
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->oOo〇8o008:Z

    .line 11
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->OO〇00〇8oO:Z

    .line 12
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/ImageEditViewLayout;->O8(Landroid/content/Context;)V

    return-void
.end method

.method private Oo08([I)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o〇00O:[I

    .line 2
    .line 3
    const-string v1, "ImageEditViewLayout"

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const-string p1, "isCanTrim rawImageBound == null"

    .line 8
    .line 9
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const/4 p1, 0x0

    .line 13
    return p1

    .line 14
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o8〇OO0〇0o:I

    .line 15
    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerUtils;->initThreadContext()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    iput v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o8〇OO0〇0o:I

    .line 23
    .line 24
    :cond_1
    iget v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o8〇OO0〇0o:I

    .line 25
    .line 26
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o〇00O:[I

    .line 27
    .line 28
    invoke-static {v0, v2, p1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->checkCropBounds(I[I[I)Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    new-instance v2, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v3, "isCanTrim = "

    .line 38
    .line 39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    const-string v3, ", bounds = "

    .line 46
    .line 47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    return v0
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private synthetic oO80()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->O8o08O8O:Ljava/lang/String;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO0〇Oo()Z

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    iget-object v3, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->oOo0:Landroidx/collection/LruCache;

    .line 10
    .line 11
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/camscanner/scanner/ScannerUtils;->findCandidateLines(Ljava/lang/String;Lcom/intsig/camscanner/view/ImageEditView;ZLandroidx/collection/LruCache;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇080(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇〇888(Landroid/graphics/Bitmap;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇80〇808〇O()V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o8〇OO0〇0o:I

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->destroyThreadContext(I)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    iput v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o8〇OO0〇0o:I

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇O00(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->OO:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->O8()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->OO:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 11
    .line 12
    add-int/lit16 v0, v0, 0x168

    .line 13
    .line 14
    add-int/2addr v0, p1

    .line 15
    rem-int/lit16 v0, v0, 0x168

    .line 16
    .line 17
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->oO80(I)V

    .line 18
    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->OO:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 23
    .line 24
    const/4 v1, 0x1

    .line 25
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/view/ImageEditView;->oO(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/view/ImageEditViewLayout;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/view/ImageEditViewLayout;->oO80()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static synthetic 〇〇888(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 1
    if-eqz p0, :cond_0

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method O8(Landroid/content/Context;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/view/ImageEditView;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/view/ImageEditView;-><init>(Landroid/content/Context;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/view/MagnifierView;

    .line 9
    .line 10
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/view/MagnifierView;-><init>(Landroid/content/Context;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇OOo8〇0:Lcom/intsig/camscanner/view/MagnifierView;

    .line 14
    .line 15
    const/16 p1, 0x8

    .line 16
    .line 17
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 18
    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 21
    .line 22
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 23
    .line 24
    const/4 v1, -0x1

    .line 25
    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0, p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇OOo8〇0:Lcom/intsig/camscanner/view/MagnifierView;

    .line 32
    .line 33
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 34
    .line 35
    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0, p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 39
    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 42
    .line 43
    const/4 v0, 0x1

    .line 44
    const/4 v1, 0x0

    .line 45
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 46
    .line 47
    .line 48
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇OOo8〇0:Lcom/intsig/camscanner/view/MagnifierView;

    .line 49
    .line 50
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 51
    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 54
    .line 55
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    const v1, 0x7f0701eb

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->setOffset(F)V

    .line 67
    .line 68
    .line 69
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 70
    .line 71
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/view/ImageEditView;->setOnCornorChangeListener(Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;)V

    .line 72
    .line 73
    .line 74
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 75
    .line 76
    const/4 v0, 0x0

    .line 77
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/ImageEditView;->setRegionVisibility(Z)V

    .line 78
    .line 79
    .line 80
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 81
    .line 82
    new-instance v0, Lcom/intsig/camscanner/view/〇oOO8O8;

    .line 83
    .line 84
    invoke-direct {v0}, Lcom/intsig/camscanner/view/〇oOO8O8;-><init>()V

    .line 85
    .line 86
    .line 87
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->setRecycler(Lcom/intsig/camscanner/view/ImageViewTouchBase$Recycler;)V

    .line 88
    .line 89
    .line 90
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public OO0o〇〇()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->OO:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    const-string v1, "ImageEditViewLayout"

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const-string v0, "showBorder rotateBitmap == null"

    .line 8
    .line 9
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-nez v0, :cond_1

    .line 18
    .line 19
    const-string v0, "showBorder thumbBitmap == null"

    .line 20
    .line 21
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    return-void

    .line 25
    :cond_1
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->O8o08O8O:Ljava/lang/String;

    .line 26
    .line 27
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    if-nez v2, :cond_2

    .line 32
    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    .line 34
    .line 35
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .line 37
    .line 38
    const-string v2, "showBorder rawImagePath ="

    .line 39
    .line 40
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->O8o08O8O:Ljava/lang/String;

    .line 44
    .line 45
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    const-string v2, " is not exist"

    .line 49
    .line 50
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    return-void

    .line 61
    :cond_2
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->O8o08O8O:Ljava/lang/String;

    .line 62
    .line 63
    const/4 v3, 0x1

    .line 64
    invoke-static {v2, v3}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    iput-object v2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o〇00O:[I

    .line 69
    .line 70
    if-nez v2, :cond_3

    .line 71
    .line 72
    const-string v0, "showBorder rawImageBound == null"

    .line 73
    .line 74
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    return-void

    .line 78
    :cond_3
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 79
    .line 80
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->OO:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 81
    .line 82
    invoke-virtual {v1, v2, v3}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o800o8O(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    .line 83
    .line 84
    .line 85
    new-instance v1, Landroid/graphics/RectF;

    .line 86
    .line 87
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 88
    .line 89
    .line 90
    move-result v2

    .line 91
    int-to-float v2, v2

    .line 92
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 93
    .line 94
    .line 95
    move-result v4

    .line 96
    int-to-float v4, v4

    .line 97
    const/4 v5, 0x0

    .line 98
    invoke-direct {v1, v5, v5, v2, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 99
    .line 100
    .line 101
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 102
    .line 103
    invoke-virtual {v2}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 104
    .line 105
    .line 106
    move-result-object v2

    .line 107
    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 108
    .line 109
    .line 110
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇OOo8〇0:Lcom/intsig/camscanner/view/MagnifierView;

    .line 111
    .line 112
    iget-object v4, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->OO:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 113
    .line 114
    invoke-virtual {v4}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 115
    .line 116
    .line 117
    move-result-object v4

    .line 118
    invoke-virtual {v2, v4, v1}, Lcom/intsig/camscanner/view/MagnifierView;->o〇0(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)V

    .line 119
    .line 120
    .line 121
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 122
    .line 123
    .line 124
    move-result v0

    .line 125
    int-to-float v0, v0

    .line 126
    const/high16 v1, 0x3f800000    # 1.0f

    .line 127
    .line 128
    mul-float v0, v0, v1

    .line 129
    .line 130
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o〇00O:[I

    .line 131
    .line 132
    const/4 v2, 0x0

    .line 133
    aget v4, v1, v2

    .line 134
    .line 135
    int-to-float v4, v4

    .line 136
    div-float/2addr v0, v4

    .line 137
    iput v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇080OO8〇0:F

    .line 138
    .line 139
    iget-object v4, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇08O〇00〇o:[I

    .line 140
    .line 141
    if-eqz v4, :cond_4

    .line 142
    .line 143
    invoke-static {v4}, Lcom/intsig/utils/PointUtil;->Oo08([I)[F

    .line 144
    .line 145
    .line 146
    move-result-object v0

    .line 147
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 148
    .line 149
    iget v4, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇080OO8〇0:F

    .line 150
    .line 151
    invoke-virtual {v1, v0, v4, v3}, Lcom/intsig/camscanner/view/ImageEditView;->o8oO〇([FFZ)V

    .line 152
    .line 153
    .line 154
    goto :goto_0

    .line 155
    :cond_4
    iget-object v4, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 156
    .line 157
    invoke-virtual {v4, v0, v1, v3}, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇0〇o〇O(F[IZ)V

    .line 158
    .line 159
    .line 160
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 161
    .line 162
    invoke-virtual {v0, v3, v2}, Lcom/intsig/camscanner/view/ImageEditView;->o〇O(ZZ)V

    .line 163
    .line 164
    .line 165
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 166
    .line 167
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/view/ImageEditView;->setRegionVisibility(Z)V

    .line 168
    .line 169
    .line 170
    return-void
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public OO0o〇〇〇〇0([I)Lcom/intsig/camscanner/view/ImageEditViewLayout;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇08O〇00〇o:[I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public Oooo8o0〇()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->OO〇00〇8oO:Z

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    xor-int/2addr v0, v1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->OO〇00〇8oO:Z

    .line 6
    .line 7
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇08O〇00〇o:[I

    .line 8
    .line 9
    if-eqz v2, :cond_0

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    invoke-static {v2}, Lcom/intsig/utils/PointUtil;->Oo08([I)[F

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 18
    .line 19
    iget v3, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇080OO8〇0:F

    .line 20
    .line 21
    invoke-virtual {v2, v0, v3, v1}, Lcom/intsig/camscanner/view/ImageEditView;->o8oO〇([FFZ)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 26
    .line 27
    iget v2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇080OO8〇0:F

    .line 28
    .line 29
    iget-object v3, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o〇00O:[I

    .line 30
    .line 31
    invoke-virtual {v0, v2, v3, v1}, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇0〇o〇O(F[IZ)V

    .line 32
    .line 33
    .line 34
    :goto_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getImageRotation()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->OO:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->O8()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o88〇OO08〇(Z)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v1, 0x1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/ImageEditView;->setRegionAvailability(Z)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageEditView;->O8〇o()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 19
    .line 20
    const/4 v2, 0x0

    .line 21
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/view/ImageEditView;->〇oOO8O8(Z)[I

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/ImageEditViewLayout;->Oo08([I)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-nez v0, :cond_1

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 32
    .line 33
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/view/ImageEditView;->setRegionAvailability(Z)V

    .line 34
    .line 35
    .line 36
    const-string v0, "onCornorChanged: isRegionAvailabl = true,  isCanTrim = false"

    .line 37
    .line 38
    const-string v3, "ImageEditViewLayout"

    .line 39
    .line 40
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    const/16 v0, -0x6b00

    .line 44
    .line 45
    iget-object v4, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 46
    .line 47
    invoke-virtual {v4, v0}, Lcom/intsig/camscanner/view/ImageEditView;->setLinePaintColor(I)V

    .line 48
    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 51
    .line 52
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 53
    .line 54
    .line 55
    if-eqz p1, :cond_2

    .line 56
    .line 57
    iget-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇0O:Z

    .line 58
    .line 59
    if-eqz p1, :cond_2

    .line 60
    .line 61
    :try_start_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    const v0, 0x7f130529

    .line 66
    .line 67
    .line 68
    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    invoke-virtual {p1, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 76
    .line 77
    .line 78
    iput-boolean v2, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇0O:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :catch_0
    move-exception p1

    .line 82
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 83
    .line 84
    .line 85
    goto :goto_0

    .line 86
    :cond_1
    const p1, -0xe64364

    .line 87
    .line 88
    .line 89
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 90
    .line 91
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/ImageEditView;->setLinePaintColor(I)V

    .line 92
    .line 93
    .line 94
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 95
    .line 96
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 97
    .line 98
    .line 99
    :cond_2
    :goto_0
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public o〇0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->OO〇00〇8oO:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0o〇〇()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->oOo〇8o008:Z

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->O8o08O8O:Ljava/lang/String;

    .line 6
    .line 7
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->oOo〇8o008:Z

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->oOo0:Landroidx/collection/LruCache;

    .line 17
    .line 18
    if-nez v0, :cond_0

    .line 19
    .line 20
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerUtils;->createCandidateLinesDataLruCache()Landroidx/collection/LruCache;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->oOo0:Landroidx/collection/LruCache;

    .line 25
    .line 26
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/scanner/CandidateLinesManager;->getInstance()Lcom/intsig/camscanner/scanner/CandidateLinesManager;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    new-instance v1, Lcom/intsig/camscanner/view/〇0000OOO;

    .line 31
    .line 32
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/view/〇0000OOO;-><init>(Lcom/intsig/camscanner/view/ImageEditViewLayout;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/scanner/CandidateLinesManager;->findCandidateLines(Ljava/lang/Runnable;)V

    .line 36
    .line 37
    .line 38
    :cond_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇08O〇00〇o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇OOo8〇0:Lcom/intsig/camscanner/view/MagnifierView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/MagnifierView;->〇080()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0O〇Oo()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇80〇808〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8o8o〇(Ljava/lang/String;)Lcom/intsig/camscanner/view/ImageEditViewLayout;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->O8o08O8O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇O8o08O(Lcom/intsig/camscanner/loadimage/RotateBitmap;)Lcom/intsig/camscanner/view/ImageEditViewLayout;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->OO:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇Oo(FF)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->OO:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇OOo8〇0:Lcom/intsig/camscanner/view/MagnifierView;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->O8()I

    .line 9
    .line 10
    .line 11
    move-result v4

    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 13
    .line 14
    invoke-virtual {v0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 15
    .line 16
    .line 17
    move-result-object v5

    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageEditView;->getCropRegion()Lcom/intsig/camscanner/view/HightlightRegion;

    .line 21
    .line 22
    .line 23
    move-result-object v6

    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageEditView;->O8〇o()Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eqz v0, :cond_2

    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 33
    .line 34
    const/4 v2, 0x0

    .line 35
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/view/ImageEditView;->〇oOO8O8(Z)[I

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/ImageEditViewLayout;->Oo08([I)Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_1
    const/4 v7, 0x0

    .line 47
    goto :goto_1

    .line 48
    :cond_2
    :goto_0
    const/4 v0, 0x1

    .line 49
    const/4 v7, 0x1

    .line 50
    :goto_1
    move v2, p1

    .line 51
    move v3, p2

    .line 52
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/camscanner/view/MagnifierView;->update(FFILandroid/graphics/Matrix;Lcom/intsig/camscanner/view/HightlightRegion;Z)V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public 〇O〇()V
    .locals 1

    .line 1
    const/16 v0, 0x5a

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇O00(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇(Z)[I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditViewLayout;->o0:Lcom/intsig/camscanner/view/ImageEditView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/ImageEditView;->〇oOO8O8(Z)[I

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇〇808〇()V
    .locals 1

    .line 1
    const/16 v0, 0x10e

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/ImageEditViewLayout;->〇O00(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/view/BorderView;
.super Landroid/view/View;
.source "BorderView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/BorderView$BorderScroller;,
        Lcom/intsig/camscanner/view/BorderView$Status;
    }
.end annotation


# instance fields
.field private O0O:Z

.field private final O8o08O8O:Landroid/graphics/Paint;

.field private final OO:I

.field private OO〇00〇8oO:Landroid/graphics/Path;

.field private final o0:Ljava/lang/String;

.field private o8oOOo:I

.field private o8〇OO0〇0o:Landroid/graphics/Matrix;

.field private oOo0:Landroid/graphics/Path;

.field private oOo〇8o008:I

.field private ooo0〇〇O:Z

.field private o〇00O:Landroid/graphics/Paint;

.field private 〇080OO8〇0:I

.field private 〇08O〇00〇o:Landroid/graphics/RectF;

.field private 〇0O:I

.field private 〇8〇oO〇〇8o:Lcom/intsig/camscanner/view/BorderView$Status;

.field private 〇OOo8〇0:Lcom/intsig/camscanner/view/BorderView$BorderScroller;

.field private 〇〇08O:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    const-string p2, "BorderView"

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/camscanner/view/BorderView;->o0:Ljava/lang/String;

    .line 7
    .line 8
    const/16 p2, 0x10

    .line 9
    .line 10
    iput p2, p0, Lcom/intsig/camscanner/view/BorderView;->OO:I

    .line 11
    .line 12
    const/4 p2, 0x0

    .line 13
    iput-object p2, p0, Lcom/intsig/camscanner/view/BorderView;->o〇00O:Landroid/graphics/Paint;

    .line 14
    .line 15
    new-instance v0, Landroid/graphics/Paint;

    .line 16
    .line 17
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->O8o08O8O:Landroid/graphics/Paint;

    .line 21
    .line 22
    new-instance v0, Landroid/graphics/Path;

    .line 23
    .line 24
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 25
    .line 26
    .line 27
    iput-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->oOo0:Landroid/graphics/Path;

    .line 28
    .line 29
    new-instance v0, Landroid/graphics/Path;

    .line 30
    .line 31
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 32
    .line 33
    .line 34
    iput-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->OO〇00〇8oO:Landroid/graphics/Path;

    .line 35
    .line 36
    iput-object p2, p0, Lcom/intsig/camscanner/view/BorderView;->o8〇OO0〇0o:Landroid/graphics/Matrix;

    .line 37
    .line 38
    const/4 p2, 0x1

    .line 39
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/BorderView;->〇〇08O:Z

    .line 40
    .line 41
    const/4 p2, 0x0

    .line 42
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/BorderView;->O0O:Z

    .line 43
    .line 44
    const/4 p2, 0x2

    .line 45
    iput p2, p0, Lcom/intsig/camscanner/view/BorderView;->o8oOOo:I

    .line 46
    .line 47
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/BorderView;->〇o〇(Landroid/content/Context;)V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
.end method

.method private O8(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->O8o08O8O:Landroid/graphics/Paint;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->O8o08O8O:Landroid/graphics/Paint;

    .line 8
    .line 9
    const v1, -0xe64364

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->O8o08O8O:Landroid/graphics/Paint;

    .line 16
    .line 17
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->O8o08O8O:Landroid/graphics/Paint;

    .line 23
    .line 24
    int-to-float p1, p1

    .line 25
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private Oo08(Landroid/graphics/Canvas;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->o8〇OO0〇0o:Landroid/graphics/Matrix;

    .line 2
    .line 3
    if-nez v0, :cond_3

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/camscanner/view/BorderView;->〇0O:I

    .line 6
    .line 7
    if-lez v0, :cond_3

    .line 8
    .line 9
    iget v0, p0, Lcom/intsig/camscanner/view/BorderView;->〇080OO8〇0:I

    .line 10
    .line 11
    if-lez v0, :cond_3

    .line 12
    .line 13
    new-instance v0, Landroid/graphics/Matrix;

    .line 14
    .line 15
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->o8〇OO0〇0o:Landroid/graphics/Matrix;

    .line 19
    .line 20
    iget v1, p0, Lcom/intsig/camscanner/view/BorderView;->oOo〇8o008:I

    .line 21
    .line 22
    int-to-float v1, v1

    .line 23
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 24
    .line 25
    .line 26
    iget v0, p0, Lcom/intsig/camscanner/view/BorderView;->oOo〇8o008:I

    .line 27
    .line 28
    const/16 v1, 0x5a

    .line 29
    .line 30
    const/4 v2, 0x0

    .line 31
    if-ne v0, v1, :cond_0

    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->o8〇OO0〇0o:Landroid/graphics/Matrix;

    .line 34
    .line 35
    iget v1, p0, Lcom/intsig/camscanner/view/BorderView;->〇0O:I

    .line 36
    .line 37
    int-to-float v1, v1

    .line 38
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    const/16 v1, 0x10e

    .line 43
    .line 44
    if-ne v0, v1, :cond_1

    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->o8〇OO0〇0o:Landroid/graphics/Matrix;

    .line 47
    .line 48
    iget v1, p0, Lcom/intsig/camscanner/view/BorderView;->〇080OO8〇0:I

    .line 49
    .line 50
    int-to-float v1, v1

    .line 51
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_1
    const/16 v1, 0xb4

    .line 56
    .line 57
    if-ne v0, v1, :cond_2

    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->o8〇OO0〇0o:Landroid/graphics/Matrix;

    .line 60
    .line 61
    iget v1, p0, Lcom/intsig/camscanner/view/BorderView;->〇080OO8〇0:I

    .line 62
    .line 63
    int-to-float v1, v1

    .line 64
    iget v2, p0, Lcom/intsig/camscanner/view/BorderView;->〇0O:I

    .line 65
    .line 66
    int-to-float v2, v2

    .line 67
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 68
    .line 69
    .line 70
    :cond_2
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    .line 79
    .line 80
    .line 81
    move-result p1

    .line 82
    iget v0, p0, Lcom/intsig/camscanner/view/BorderView;->〇080OO8〇0:I

    .line 83
    .line 84
    iget v1, p0, Lcom/intsig/camscanner/view/BorderView;->〇0O:I

    .line 85
    .line 86
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    const/high16 v1, 0x3f800000    # 1.0f

    .line 91
    .line 92
    int-to-float p1, p1

    .line 93
    mul-float p1, p1, v1

    .line 94
    .line 95
    int-to-float v0, v0

    .line 96
    div-float/2addr p1, v0

    .line 97
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->o8〇OO0〇0o:Landroid/graphics/Matrix;

    .line 98
    .line 99
    invoke-virtual {v0, p1, p1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 100
    .line 101
    .line 102
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->O8o08O8O:Landroid/graphics/Paint;

    .line 103
    .line 104
    iget v1, p0, Lcom/intsig/camscanner/view/BorderView;->o8oOOo:I

    .line 105
    .line 106
    int-to-float v1, v1

    .line 107
    div-float/2addr v1, p1

    .line 108
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 109
    .line 110
    .line 111
    :cond_3
    return-void
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private o〇0()V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->o〇00O:Landroid/graphics/Paint;

    .line 7
    .line 8
    const/4 v1, 0x1

    .line 9
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->o〇00O:Landroid/graphics/Paint;

    .line 13
    .line 14
    const/high16 v1, 0x66000000

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->o〇00O:Landroid/graphics/Paint;

    .line 20
    .line 21
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private 〇080(Landroid/graphics/Canvas;Lcom/intsig/camscanner/view/BorderView$Status;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->oOo0:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Path;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_3

    .line 8
    .line 9
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/view/BorderView;->o8〇OO0〇0o:Landroid/graphics/Matrix;

    .line 14
    .line 15
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 16
    .line 17
    .line 18
    iget-boolean v1, p0, Lcom/intsig/camscanner/view/BorderView;->〇〇08O:Z

    .line 19
    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/view/BorderView;->OO〇00〇8oO:Landroid/graphics/Path;

    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/view/BorderView;->o〇00O:Landroid/graphics/Paint;

    .line 25
    .line 26
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 27
    .line 28
    .line 29
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/view/BorderView$Status;->OUT_SIDE:Lcom/intsig/camscanner/view/BorderView$Status;

    .line 30
    .line 31
    if-ne p2, v1, :cond_1

    .line 32
    .line 33
    iget-object p2, p0, Lcom/intsig/camscanner/view/BorderView;->O8o08O8O:Landroid/graphics/Paint;

    .line 34
    .line 35
    const v1, -0x20b2c5

    .line 36
    .line 37
    .line 38
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    iget-object p2, p0, Lcom/intsig/camscanner/view/BorderView;->O8o08O8O:Landroid/graphics/Paint;

    .line 43
    .line 44
    const v1, -0xe64364

    .line 45
    .line 46
    .line 47
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    .line 49
    .line 50
    :goto_0
    iget-object p2, p0, Lcom/intsig/camscanner/view/BorderView;->oOo0:Landroid/graphics/Path;

    .line 51
    .line 52
    iget-object v1, p0, Lcom/intsig/camscanner/view/BorderView;->O8o08O8O:Landroid/graphics/Paint;

    .line 53
    .line 54
    invoke-virtual {p1, p2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 55
    .line 56
    .line 57
    iget-boolean p2, p0, Lcom/intsig/camscanner/view/BorderView;->O0O:Z

    .line 58
    .line 59
    if-eqz p2, :cond_2

    .line 60
    .line 61
    iget-object p2, p0, Lcom/intsig/camscanner/view/BorderView;->oOo0:Landroid/graphics/Path;

    .line 62
    .line 63
    invoke-virtual {p1, p2}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 64
    .line 65
    .line 66
    const/16 p2, 0xbc

    .line 67
    .line 68
    const/16 v1, 0xaa

    .line 69
    .line 70
    const/16 v2, 0x1a

    .line 71
    .line 72
    const/16 v3, 0x19

    .line 73
    .line 74
    invoke-static {v2, v3, p2, v1}, Landroid/graphics/Color;->argb(IIII)I

    .line 75
    .line 76
    .line 77
    move-result p2

    .line 78
    invoke-virtual {p1, p2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 79
    .line 80
    .line 81
    :cond_2
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 82
    .line 83
    .line 84
    :cond_3
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private 〇8o8o〇([I)V
    .locals 3

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->o8〇OO0〇0o:Landroid/graphics/Matrix;

    .line 4
    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->oOo0:Landroid/graphics/Path;

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->oOo0:Landroid/graphics/Path;

    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    aget v1, p1, v1

    .line 16
    .line 17
    int-to-float v1, v1

    .line 18
    const/4 v2, 0x1

    .line 19
    aget v2, p1, v2

    .line 20
    .line 21
    int-to-float v2, v2

    .line 22
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->oOo0:Landroid/graphics/Path;

    .line 26
    .line 27
    const/4 v1, 0x2

    .line 28
    aget v1, p1, v1

    .line 29
    .line 30
    int-to-float v1, v1

    .line 31
    const/4 v2, 0x3

    .line 32
    aget v2, p1, v2

    .line 33
    .line 34
    int-to-float v2, v2

    .line 35
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->oOo0:Landroid/graphics/Path;

    .line 39
    .line 40
    const/4 v1, 0x4

    .line 41
    aget v1, p1, v1

    .line 42
    .line 43
    int-to-float v1, v1

    .line 44
    const/4 v2, 0x5

    .line 45
    aget v2, p1, v2

    .line 46
    .line 47
    int-to-float v2, v2

    .line 48
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->oOo0:Landroid/graphics/Path;

    .line 52
    .line 53
    const/4 v1, 0x6

    .line 54
    aget v1, p1, v1

    .line 55
    .line 56
    int-to-float v1, v1

    .line 57
    const/4 v2, 0x7

    .line 58
    aget p1, p1, v2

    .line 59
    .line 60
    int-to-float p1, p1

    .line 61
    invoke-virtual {v0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 62
    .line 63
    .line 64
    iget-object p1, p0, Lcom/intsig/camscanner/view/BorderView;->oOo0:Landroid/graphics/Path;

    .line 65
    .line 66
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 67
    .line 68
    .line 69
    iget-object p1, p0, Lcom/intsig/camscanner/view/BorderView;->OO〇00〇8oO:Landroid/graphics/Path;

    .line 70
    .line 71
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    .line 72
    .line 73
    .line 74
    iget-object p1, p0, Lcom/intsig/camscanner/view/BorderView;->〇08O〇00〇o:Landroid/graphics/RectF;

    .line 75
    .line 76
    if-nez p1, :cond_0

    .line 77
    .line 78
    new-instance p1, Landroid/graphics/RectF;

    .line 79
    .line 80
    iget v0, p0, Lcom/intsig/camscanner/view/BorderView;->〇080OO8〇0:I

    .line 81
    .line 82
    int-to-float v0, v0

    .line 83
    iget v1, p0, Lcom/intsig/camscanner/view/BorderView;->〇0O:I

    .line 84
    .line 85
    int-to-float v1, v1

    .line 86
    const/4 v2, 0x0

    .line 87
    invoke-direct {p1, v2, v2, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 88
    .line 89
    .line 90
    iput-object p1, p0, Lcom/intsig/camscanner/view/BorderView;->〇08O〇00〇o:Landroid/graphics/RectF;

    .line 91
    .line 92
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/view/BorderView;->OO〇00〇8oO:Landroid/graphics/Path;

    .line 93
    .line 94
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->〇08O〇00〇o:Landroid/graphics/RectF;

    .line 95
    .line 96
    sget-object v1, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 97
    .line 98
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 99
    .line 100
    .line 101
    iget-object p1, p0, Lcom/intsig/camscanner/view/BorderView;->OO〇00〇8oO:Landroid/graphics/Path;

    .line 102
    .line 103
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->oOo0:Landroid/graphics/Path;

    .line 104
    .line 105
    invoke-virtual {p1, v0}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;)V

    .line 106
    .line 107
    .line 108
    iget-object p1, p0, Lcom/intsig/camscanner/view/BorderView;->OO〇00〇8oO:Landroid/graphics/Path;

    .line 109
    .line 110
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    .line 111
    .line 112
    invoke-virtual {p1, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 113
    .line 114
    .line 115
    iget-object p1, p0, Lcom/intsig/camscanner/view/BorderView;->OO〇00〇8oO:Landroid/graphics/Path;

    .line 116
    .line 117
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 118
    .line 119
    .line 120
    :cond_1
    return-void
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private 〇o〇(Landroid/content/Context;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/view/BorderView$BorderScroller;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/view/BorderView$BorderScroller;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->〇OOo8〇0:Lcom/intsig/camscanner/view/BorderView$BorderScroller;

    .line 7
    .line 8
    const/4 v0, 0x2

    .line 9
    invoke-static {p1, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    iput p1, p0, Lcom/intsig/camscanner/view/BorderView;->o8oOOo:I

    .line 14
    .line 15
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/BorderView;->O8(I)V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/view/BorderView;->o〇0()V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public OO0o〇〇〇〇0([IIILcom/intsig/camscanner/view/BorderView$Status;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->〇OOo8〇0:Lcom/intsig/camscanner/view/BorderView$BorderScroller;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p3}, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->Oo08([II)V

    .line 4
    .line 5
    .line 6
    iput p2, p0, Lcom/intsig/camscanner/view/BorderView;->oOo〇8o008:I

    .line 7
    .line 8
    iput-object p4, p0, Lcom/intsig/camscanner/view/BorderView;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/view/BorderView$Status;

    .line 9
    .line 10
    const/4 p1, 0x1

    .line 11
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/BorderView;->ooo0〇〇O:Z

    .line 12
    .line 13
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public oO80()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->o8〇OO0〇0o:Landroid/graphics/Matrix;

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/BorderView;->ooo0〇〇O:Z

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/BorderView;->Oo08(Landroid/graphics/Canvas;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->〇OOo8〇0:Lcom/intsig/camscanner/view/BorderView$BorderScroller;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇o00〇〇Oo()[I

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/BorderView;->〇8o8o〇([I)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/view/BorderView$Status;

    .line 21
    .line 22
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/view/BorderView;->〇080(Landroid/graphics/Canvas;Lcom/intsig/camscanner/view/BorderView$Status;)V

    .line 23
    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/camscanner/view/BorderView;->〇OOo8〇0:Lcom/intsig/camscanner/view/BorderView$BorderScroller;

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->〇080()Z

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    if-eqz p1, :cond_0

    .line 32
    .line 33
    const-wide/16 v0, 0x10

    .line 34
    .line 35
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->postInvalidateDelayed(J)V

    .line 36
    .line 37
    .line 38
    :cond_0
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setShowBackground(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/BorderView;->〇〇08O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setShowMask(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/BorderView;->O0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇80〇808〇O(II)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/BorderView;->〇080OO8〇0:I

    .line 2
    .line 3
    if-ne v0, p1, :cond_0

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/camscanner/view/BorderView;->〇0O:I

    .line 6
    .line 7
    if-eq v0, p2, :cond_1

    .line 8
    .line 9
    :cond_0
    iput p1, p0, Lcom/intsig/camscanner/view/BorderView;->〇080OO8〇0:I

    .line 10
    .line 11
    iput p2, p0, Lcom/intsig/camscanner/view/BorderView;->〇0O:I

    .line 12
    .line 13
    const/4 p1, 0x0

    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/view/BorderView;->〇08O〇00〇o:Landroid/graphics/RectF;

    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/camscanner/view/BorderView;->o8〇OO0〇0o:Landroid/graphics/Matrix;

    .line 17
    .line 18
    :cond_1
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇o00〇〇Oo()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/BorderView;->ooo0〇〇O:Z

    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/BorderView;->〇OOo8〇0:Lcom/intsig/camscanner/view/BorderView$BorderScroller;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/BorderView$BorderScroller;->O8()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

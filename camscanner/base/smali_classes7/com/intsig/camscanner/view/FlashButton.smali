.class public Lcom/intsig/camscanner/view/FlashButton;
.super Lcom/intsig/camscanner/view/RotateImageView;
.source "FlashButton.java"


# static fields
.field public static O88O:Ljava/lang/String; = "auto"

.field public static o8o:Ljava/lang/String; = "off"

.field public static oOO〇〇:Ljava/lang/String; = "on"

.field public static oo8ooo8O:Ljava/lang/String; = "torch"

.field private static o〇oO:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/view/RotateImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private getFlashIcon()I
    .locals 2

    .line 1
    sget v0, Lcom/intsig/camscanner/view/FlashButton;->o〇oO:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eq v0, v1, :cond_2

    .line 5
    .line 6
    const/4 v1, 0x2

    .line 7
    if-eq v0, v1, :cond_1

    .line 8
    .line 9
    const/4 v1, 0x3

    .line 10
    if-eq v0, v1, :cond_0

    .line 11
    .line 12
    const v0, 0x7f08038a

    .line 13
    .line 14
    .line 15
    return v0

    .line 16
    :cond_0
    const v0, 0x7f080389

    .line 17
    .line 18
    .line 19
    return v0

    .line 20
    :cond_1
    const v0, 0x7f08038b

    .line 21
    .line 22
    .line 23
    return v0

    .line 24
    :cond_2
    const v0, 0x7f08038c

    .line 25
    .line 26
    .line 27
    return v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method


# virtual methods
.method public getMode()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/view/FlashButton;->o〇oO:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setMode(I)V
    .locals 0

    .line 1
    sput p1, Lcom/intsig/camscanner/view/FlashButton;->o〇oO:I

    .line 2
    invoke-direct {p0}, Lcom/intsig/camscanner/view/FlashButton;->getFlashIcon()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/view/SafeImageView;->setImageResource(I)V

    return-void
.end method

.method public setMode(Ljava/lang/String;)V
    .locals 1

    const-string v0, "flash"

    .line 3
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    sget-object v0, Lcom/intsig/camscanner/view/FlashButton;->O88O:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/FlashButton;->setMode(I)V

    goto :goto_0

    .line 6
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/view/FlashButton;->oOO〇〇:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, 0x1

    .line 7
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/FlashButton;->setMode(I)V

    goto :goto_0

    .line 8
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/view/FlashButton;->o8o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p1, 0x2

    .line 9
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/FlashButton;->setMode(I)V

    goto :goto_0

    .line 10
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/view/FlashButton;->oo8ooo8O:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p1, 0x3

    .line 11
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/FlashButton;->setMode(I)V

    :cond_3
    :goto_0
    return-void
.end method

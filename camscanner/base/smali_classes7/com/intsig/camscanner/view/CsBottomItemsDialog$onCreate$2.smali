.class public final Lcom/intsig/camscanner/view/CsBottomItemsDialog$onCreate$2;
.super Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;
.source "CsBottomItemsDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/intsig/camscanner/view/CsBottomItemsDialog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/BaseProviderMultiAdapter<",
        "Lcom/intsig/menu/MenuTypeItem;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/view/CsBottomItemsDialog;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x1

    .line 3
    invoke-direct {p0, v0, v1, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;-><init>(Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/intsig/camscanner/view/CsBottomItemsDialog$MenuItemProvider;

    .line 7
    .line 8
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/view/CsBottomItemsDialog$MenuItemProvider;-><init>(Lcom/intsig/camscanner/view/CsBottomItemsDialog;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 12
    .line 13
    .line 14
    new-instance p1, Lcom/intsig/camscanner/view/CsBottomItemsDialog$MenuGroupTitleProvider;

    .line 15
    .line 16
    invoke-direct {p1}, Lcom/intsig/camscanner/view/CsBottomItemsDialog$MenuGroupTitleProvider;-><init>()V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 20
    .line 21
    .line 22
    new-instance p1, Lcom/intsig/camscanner/view/CsBottomItemsDialog$MenuGroupDividerProvider;

    .line 23
    .line 24
    invoke-direct {p1}, Lcom/intsig/camscanner/view/CsBottomItemsDialog$MenuGroupDividerProvider;-><init>()V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 28
    .line 29
    .line 30
    new-instance p1, Lcom/intsig/camscanner/view/CsBottomItemsDialog$MenuRightSwitchProvider;

    .line 31
    .line 32
    invoke-direct {p1}, Lcom/intsig/camscanner/view/CsBottomItemsDialog$MenuRightSwitchProvider;-><init>()V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method protected oO〇(Ljava/util/List;I)I
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/menu/MenuTypeItem;",
            ">;I)I"
        }
    .end annotation

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    check-cast p1, Lcom/intsig/menu/MenuTypeItem;

    .line 11
    .line 12
    instance-of p2, p1, Lcom/intsig/menu/MenuItem;

    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    if-eqz p2, :cond_0

    .line 16
    .line 17
    return v0

    .line 18
    :cond_0
    instance-of p2, p1, Lcom/intsig/camscanner/view/MenuGroupTitle;

    .line 19
    .line 20
    if-eqz p2, :cond_1

    .line 21
    .line 22
    const/4 p1, 0x1

    .line 23
    return p1

    .line 24
    :cond_1
    instance-of p2, p1, Lcom/intsig/camscanner/view/MenuGroupDivider;

    .line 25
    .line 26
    if-eqz p2, :cond_2

    .line 27
    .line 28
    const/4 p1, 0x2

    .line 29
    return p1

    .line 30
    :cond_2
    instance-of p1, p1, Lcom/intsig/camscanner/view/MenuRightSwitch;

    .line 31
    .line 32
    if-eqz p1, :cond_3

    .line 33
    .line 34
    const/4 p1, 0x3

    .line 35
    return p1

    .line 36
    :cond_3
    return v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.class public Lcom/intsig/camscanner/view/CertificateBmView;
.super Landroid/view/View;
.source "CertificateBmView.java"


# instance fields
.field private O0O:I

.field private O88O:I

.field private O8o08O8O:Landroid/graphics/Path;

.field public OO:Z

.field private OO〇00〇8oO:Ljava/lang/String;

.field private Oo80:Z

.field private O〇o88o08〇:I

.field private o0:Landroid/graphics/Bitmap;

.field private o8o:I

.field private o8oOOo:I

.field private o8〇OO0〇0o:Ljava/lang/String;

.field private oOO〇〇:I

.field private oOo0:Ljava/lang/String;

.field private oOo〇8o008:Ljava/lang/String;

.field private oo8ooo8O:I

.field private ooo0〇〇O:Landroid/graphics/Paint;

.field public o〇00O:F

.field private o〇oO:F

.field private 〇080OO8〇0:Landroid/graphics/Paint;

.field public 〇08O〇00〇o:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field private 〇08〇o0O:Z

.field private 〇0O:Landroid/graphics/RectF;

.field private 〇8〇oO〇〇8o:I

.field private final 〇OOo8〇0:Landroid/graphics/Matrix;

.field private 〇O〇〇O8:F

.field private 〇o0O:F

.field private 〇〇08O:Landroid/graphics/Paint;

.field private 〇〇o〇:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    new-instance p1, Landroid/graphics/Matrix;

    .line 5
    .line 6
    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->OO:Z

    .line 13
    .line 14
    iput p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇08O〇00〇o:I

    .line 15
    .line 16
    const/4 p2, 0x0

    .line 17
    iput p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o〇00O:F

    .line 18
    .line 19
    new-instance p2, Landroid/graphics/Path;

    .line 20
    .line 21
    invoke-direct {p2}, Landroid/graphics/Path;-><init>()V

    .line 22
    .line 23
    .line 24
    iput-object p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O8o08O8O:Landroid/graphics/Path;

    .line 25
    .line 26
    new-instance p2, Landroid/graphics/Paint;

    .line 27
    .line 28
    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    .line 29
    .line 30
    .line 31
    iput-object p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 32
    .line 33
    new-instance p2, Landroid/graphics/RectF;

    .line 34
    .line 35
    invoke-direct {p2}, Landroid/graphics/RectF;-><init>()V

    .line 36
    .line 37
    .line 38
    iput-object p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 39
    .line 40
    const-string p2, ""

    .line 41
    .line 42
    iput-object p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->oOo〇8o008:Ljava/lang/String;

    .line 43
    .line 44
    iput-object p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->oOo0:Ljava/lang/String;

    .line 45
    .line 46
    iput-object p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->OO〇00〇8oO:Ljava/lang/String;

    .line 47
    .line 48
    iput-object p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o8〇OO0〇0o:Ljava/lang/String;

    .line 49
    .line 50
    const/high16 p2, 0x41c00000    # 24.0f

    .line 51
    .line 52
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 53
    .line 54
    .line 55
    move-result p2

    .line 56
    iput p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇8〇oO〇〇8o:I

    .line 57
    .line 58
    new-instance p2, Landroid/graphics/Paint;

    .line 59
    .line 60
    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    .line 61
    .line 62
    .line 63
    iput-object p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->ooo0〇〇O:Landroid/graphics/Paint;

    .line 64
    .line 65
    new-instance p2, Landroid/graphics/Paint;

    .line 66
    .line 67
    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    .line 68
    .line 69
    .line 70
    iput-object p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇〇08O:Landroid/graphics/Paint;

    .line 71
    .line 72
    const/high16 p2, 0x42500000    # 52.0f

    .line 73
    .line 74
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 75
    .line 76
    .line 77
    move-result p2

    .line 78
    iput p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O0O:I

    .line 79
    .line 80
    const/high16 p2, 0x41400000    # 12.0f

    .line 81
    .line 82
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 83
    .line 84
    .line 85
    move-result p2

    .line 86
    iput p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o8oOOo:I

    .line 87
    .line 88
    const/high16 p2, 0x42f00000    # 120.0f

    .line 89
    .line 90
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 91
    .line 92
    .line 93
    move-result v0

    .line 94
    int-to-float v0, v0

    .line 95
    iput v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇O〇〇O8:F

    .line 96
    .line 97
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 98
    .line 99
    .line 100
    move-result p2

    .line 101
    int-to-float p2, p2

    .line 102
    iput p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇o0O:F

    .line 103
    .line 104
    const/high16 p2, 0x42100000    # 36.0f

    .line 105
    .line 106
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 107
    .line 108
    .line 109
    move-result p2

    .line 110
    iput p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O88O:I

    .line 111
    .line 112
    const/high16 p2, 0x41600000    # 14.0f

    .line 113
    .line 114
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 115
    .line 116
    .line 117
    move-result p2

    .line 118
    iput p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->oOO〇〇:I

    .line 119
    .line 120
    const/high16 p2, 0x41800000    # 16.0f

    .line 121
    .line 122
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 123
    .line 124
    .line 125
    move-result p2

    .line 126
    iput p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o8o:I

    .line 127
    .line 128
    const-string p2, "#FFFFFF"

    .line 129
    .line 130
    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 131
    .line 132
    .line 133
    move-result p2

    .line 134
    iput p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->oo8ooo8O:I

    .line 135
    .line 136
    const/high16 p2, 0x41200000    # 10.0f

    .line 137
    .line 138
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 139
    .line 140
    .line 141
    move-result p2

    .line 142
    int-to-float p2, p2

    .line 143
    iput p2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o〇oO:F

    .line 144
    .line 145
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇08〇o0O:Z

    .line 146
    .line 147
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇〇o〇:Z

    .line 148
    .line 149
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->Oo80:Z

    .line 150
    .line 151
    iput p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O〇o88o08〇:I

    .line 152
    .line 153
    invoke-direct {p0}, Lcom/intsig/camscanner/view/CertificateBmView;->oO80()V

    .line 154
    .line 155
    .line 156
    return-void
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private O8(Landroid/graphics/Canvas;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 5
    .line 6
    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 7
    .line 8
    iget-object v2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 9
    .line 10
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    int-to-float v2, v2

    .line 15
    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 20
    .line 21
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    int-to-float v2, v2

    .line 26
    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 31
    .line 32
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O8o08O8O:Landroid/graphics/Path;

    .line 36
    .line 37
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 38
    .line 39
    .line 40
    iget v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o〇00O:F

    .line 41
    .line 42
    cmpl-float v1, v0, v1

    .line 43
    .line 44
    if-lez v1, :cond_0

    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O8o08O8O:Landroid/graphics/Path;

    .line 47
    .line 48
    iget-object v2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 49
    .line 50
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 51
    .line 52
    invoke-virtual {v1, v2, v0, v0, v3}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O8o08O8O:Landroid/graphics/Path;

    .line 57
    .line 58
    iget-object v1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 59
    .line 60
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 61
    .line 62
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 63
    .line 64
    .line 65
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 66
    .line 67
    .line 68
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 69
    .line 70
    const/16 v1, 0x1a

    .line 71
    .line 72
    if-lt v0, v1, :cond_1

    .line 73
    .line 74
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O8o08O8O:Landroid/graphics/Path;

    .line 75
    .line 76
    invoke-static {p1, v0}, LOoo8〇〇/OO0o〇〇〇〇0;->〇080(Landroid/graphics/Canvas;Landroid/graphics/Path;)Z

    .line 77
    .line 78
    .line 79
    goto :goto_1

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O8o08O8O:Landroid/graphics/Path;

    .line 81
    .line 82
    sget-object v1, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    .line 83
    .line 84
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 85
    .line 86
    .line 87
    :goto_1
    iget v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇08O〇00〇o:I

    .line 88
    .line 89
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 93
    .line 94
    .line 95
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private Oo08(Landroid/graphics/Canvas;FFLjava/lang/String;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇〇08O:Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇〇08O:Landroid/graphics/Paint;

    .line 8
    .line 9
    invoke-virtual {v1}, Landroid/graphics/Paint;->ascent()F

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇〇08O:Landroid/graphics/Paint;

    .line 14
    .line 15
    invoke-virtual {v2}, Landroid/graphics/Paint;->descent()F

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    add-float/2addr v1, v2

    .line 20
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    const/high16 v2, 0x40000000    # 2.0f

    .line 25
    .line 26
    div-float/2addr v1, v2

    .line 27
    invoke-direct {p0}, Lcom/intsig/camscanner/view/CertificateBmView;->〇80〇808〇O()Z

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    if-eqz v3, :cond_0

    .line 32
    .line 33
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 34
    .line 35
    .line 36
    iget-object v3, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 37
    .line 38
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 39
    .line 40
    .line 41
    :cond_0
    iget v3, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇o0O:F

    .line 42
    .line 43
    iget-object v4, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇〇08O:Landroid/graphics/Paint;

    .line 44
    .line 45
    invoke-virtual {v4, p4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 46
    .line 47
    .line 48
    move-result v4

    .line 49
    iget v5, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o8o:I

    .line 50
    .line 51
    mul-int/lit8 v5, v5, 0x2

    .line 52
    .line 53
    int-to-float v5, v5

    .line 54
    add-float/2addr v4, v5

    .line 55
    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    .line 56
    .line 57
    .line 58
    move-result v3

    .line 59
    iput v3, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇O〇〇O8:F

    .line 60
    .line 61
    new-instance v3, Landroid/graphics/RectF;

    .line 62
    .line 63
    iget v4, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇O〇〇O8:F

    .line 64
    .line 65
    div-float v5, v4, v2

    .line 66
    .line 67
    sub-float v5, p2, v5

    .line 68
    .line 69
    iget v6, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O88O:I

    .line 70
    .line 71
    int-to-float v7, v6

    .line 72
    div-float/2addr v7, v2

    .line 73
    sub-float v7, p3, v7

    .line 74
    .line 75
    div-float/2addr v4, v2

    .line 76
    add-float/2addr v4, p2

    .line 77
    int-to-float v6, v6

    .line 78
    div-float/2addr v6, v2

    .line 79
    add-float/2addr v6, p3

    .line 80
    invoke-direct {v3, v5, v7, v4, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 81
    .line 82
    .line 83
    iget v4, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O0O:I

    .line 84
    .line 85
    int-to-float v5, v4

    .line 86
    int-to-float v4, v4

    .line 87
    iget-object v6, p0, Lcom/intsig/camscanner/view/CertificateBmView;->ooo0〇〇O:Landroid/graphics/Paint;

    .line 88
    .line 89
    invoke-virtual {p1, v3, v5, v4, v6}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 90
    .line 91
    .line 92
    div-float/2addr v0, v2

    .line 93
    sub-float/2addr p2, v0

    .line 94
    add-float/2addr p3, v1

    .line 95
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇〇08O:Landroid/graphics/Paint;

    .line 96
    .line 97
    invoke-virtual {p1, p4, p2, p3, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 98
    .line 99
    .line 100
    invoke-direct {p0}, Lcom/intsig/camscanner/view/CertificateBmView;->〇80〇808〇O()Z

    .line 101
    .line 102
    .line 103
    move-result p2

    .line 104
    if-eqz p2, :cond_1

    .line 105
    .line 106
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 107
    .line 108
    .line 109
    :cond_1
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private oO80()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 8
    .line 9
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->ooo0〇〇O:Landroid/graphics/Paint;

    .line 15
    .line 16
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇〇08O:Landroid/graphics/Paint;

    .line 22
    .line 23
    iget v1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->oOO〇〇:I

    .line 24
    .line 25
    int-to-float v1, v1

    .line 26
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇〇08O:Landroid/graphics/Paint;

    .line 30
    .line 31
    iget v1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->oo8ooo8O:I

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private o〇0(Landroid/graphics/Canvas;)V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇〇o〇:Z

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 6
    .line 7
    iget v1, v0, Landroid/graphics/RectF;->left:F

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/high16 v2, 0x40000000    # 2.0f

    .line 14
    .line 15
    div-float/2addr v0, v2

    .line 16
    add-float/2addr v1, v0

    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 18
    .line 19
    iget v0, v0, Landroid/graphics/RectF;->top:F

    .line 20
    .line 21
    div-float/2addr v0, v2

    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/view/CertificateBmView;->〇80〇808〇O()Z

    .line 23
    .line 24
    .line 25
    move-result v3

    .line 26
    if-eqz v3, :cond_0

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 29
    .line 30
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    div-float v1, v0, v2

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 37
    .line 38
    iget v0, v0, Landroid/graphics/RectF;->left:F

    .line 39
    .line 40
    neg-float v0, v0

    .line 41
    div-float/2addr v0, v2

    .line 42
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->OO〇00〇8oO:Ljava/lang/String;

    .line 43
    .line 44
    invoke-direct {p0, p1, v1, v0, v2}, Lcom/intsig/camscanner/view/CertificateBmView;->Oo08(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    .line 45
    .line 46
    .line 47
    :cond_1
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private 〇080()V
    .locals 5

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o〇oO:F

    .line 2
    .line 3
    float-to-int v0, v0

    .line 4
    iget v1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O〇o88o08〇:I

    .line 5
    .line 6
    const/high16 v2, 0x3f800000    # 1.0f

    .line 7
    .line 8
    if-eqz v1, :cond_2

    .line 9
    .line 10
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    const/16 v3, 0x5a

    .line 15
    .line 16
    if-eq v1, v3, :cond_0

    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O〇o88o08〇:I

    .line 19
    .line 20
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    const/16 v3, 0x10e

    .line 25
    .line 26
    if-ne v1, v3, :cond_2

    .line 27
    .line 28
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 29
    .line 30
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    add-int/lit8 v0, v0, 0x0

    .line 35
    .line 36
    add-int/lit8 v0, v0, 0x0

    .line 37
    .line 38
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    if-gt v0, v1, :cond_1

    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 45
    .line 46
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    add-int/lit8 v0, v0, 0x0

    .line 51
    .line 52
    add-int/lit8 v0, v0, 0x0

    .line 53
    .line 54
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 55
    .line 56
    .line 57
    move-result v1

    .line 58
    if-le v0, v1, :cond_4

    .line 59
    .line 60
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    add-int/lit8 v0, v0, 0x0

    .line 65
    .line 66
    add-int/lit8 v0, v0, 0x0

    .line 67
    .line 68
    int-to-float v0, v0

    .line 69
    mul-float v0, v0, v2

    .line 70
    .line 71
    iget-object v1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 72
    .line 73
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 74
    .line 75
    .line 76
    move-result v1

    .line 77
    int-to-float v1, v1

    .line 78
    div-float/2addr v0, v1

    .line 79
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 80
    .line 81
    .line 82
    move-result v1

    .line 83
    add-int/lit8 v1, v1, 0x0

    .line 84
    .line 85
    add-int/lit8 v1, v1, 0x0

    .line 86
    .line 87
    int-to-float v1, v1

    .line 88
    mul-float v1, v1, v2

    .line 89
    .line 90
    iget-object v2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 91
    .line 92
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    .line 93
    .line 94
    .line 95
    move-result v2

    .line 96
    int-to-float v2, v2

    .line 97
    div-float/2addr v1, v2

    .line 98
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    .line 99
    .line 100
    .line 101
    move-result v2

    .line 102
    goto :goto_0

    .line 103
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 104
    .line 105
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 106
    .line 107
    .line 108
    move-result v1

    .line 109
    add-int/2addr v1, v0

    .line 110
    add-int/2addr v1, v0

    .line 111
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 112
    .line 113
    .line 114
    move-result v3

    .line 115
    if-gt v1, v3, :cond_3

    .line 116
    .line 117
    iget-object v1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 118
    .line 119
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 120
    .line 121
    .line 122
    move-result v1

    .line 123
    add-int/2addr v1, v0

    .line 124
    add-int/2addr v1, v0

    .line 125
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 126
    .line 127
    .line 128
    move-result v3

    .line 129
    if-le v1, v3, :cond_4

    .line 130
    .line 131
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 132
    .line 133
    .line 134
    move-result v1

    .line 135
    sub-int/2addr v1, v0

    .line 136
    sub-int/2addr v1, v0

    .line 137
    int-to-float v1, v1

    .line 138
    mul-float v1, v1, v2

    .line 139
    .line 140
    iget-object v3, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 141
    .line 142
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    .line 143
    .line 144
    .line 145
    move-result v3

    .line 146
    int-to-float v3, v3

    .line 147
    div-float/2addr v1, v3

    .line 148
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 149
    .line 150
    .line 151
    move-result v3

    .line 152
    sub-int/2addr v3, v0

    .line 153
    sub-int/2addr v3, v0

    .line 154
    int-to-float v0, v3

    .line 155
    mul-float v0, v0, v2

    .line 156
    .line 157
    iget-object v2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 158
    .line 159
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    .line 160
    .line 161
    .line 162
    move-result v2

    .line 163
    int-to-float v2, v2

    .line 164
    div-float/2addr v0, v2

    .line 165
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    .line 166
    .line 167
    .line 168
    move-result v2

    .line 169
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 170
    .line 171
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 172
    .line 173
    .line 174
    move-result v0

    .line 175
    int-to-float v0, v0

    .line 176
    mul-float v0, v0, v2

    .line 177
    .line 178
    iget-object v1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 179
    .line 180
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 181
    .line 182
    .line 183
    move-result v1

    .line 184
    int-to-float v1, v1

    .line 185
    mul-float v1, v1, v2

    .line 186
    .line 187
    iget-object v3, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 188
    .line 189
    invoke-virtual {v3}, Landroid/graphics/Matrix;->reset()V

    .line 190
    .line 191
    .line 192
    invoke-direct {p0}, Lcom/intsig/camscanner/view/CertificateBmView;->〇80〇808〇O()Z

    .line 193
    .line 194
    .line 195
    move-result v3

    .line 196
    const/high16 v4, 0x40000000    # 2.0f

    .line 197
    .line 198
    if-eqz v3, :cond_5

    .line 199
    .line 200
    iget-object v3, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 201
    .line 202
    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 203
    .line 204
    .line 205
    iget-object v2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 206
    .line 207
    const/high16 v3, -0x3d4c0000    # -90.0f

    .line 208
    .line 209
    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 210
    .line 211
    .line 212
    iget-object v2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 213
    .line 214
    const/4 v3, 0x0

    .line 215
    invoke-virtual {v2, v3, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 216
    .line 217
    .line 218
    iget-object v2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 219
    .line 220
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 221
    .line 222
    .line 223
    move-result v3

    .line 224
    int-to-float v3, v3

    .line 225
    sub-float/2addr v3, v1

    .line 226
    div-float/2addr v3, v4

    .line 227
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 228
    .line 229
    .line 230
    move-result v1

    .line 231
    int-to-float v1, v1

    .line 232
    sub-float/2addr v1, v0

    .line 233
    div-float/2addr v1, v4

    .line 234
    invoke-virtual {v2, v3, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 235
    .line 236
    .line 237
    goto :goto_1

    .line 238
    :cond_5
    iget-object v3, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 239
    .line 240
    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 241
    .line 242
    .line 243
    iget-object v2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 244
    .line 245
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 246
    .line 247
    .line 248
    move-result v3

    .line 249
    int-to-float v3, v3

    .line 250
    sub-float/2addr v3, v0

    .line 251
    div-float/2addr v3, v4

    .line 252
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 253
    .line 254
    .line 255
    move-result v0

    .line 256
    int-to-float v0, v0

    .line 257
    sub-float/2addr v0, v1

    .line 258
    div-float/2addr v0, v4

    .line 259
    invoke-virtual {v2, v3, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 260
    .line 261
    .line 262
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 263
    .line 264
    iget v1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O〇o88o08〇:I

    .line 265
    .line 266
    int-to-float v1, v1

    .line 267
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 268
    .line 269
    .line 270
    move-result v2

    .line 271
    int-to-float v2, v2

    .line 272
    div-float/2addr v2, v4

    .line 273
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 274
    .line 275
    .line 276
    move-result v3

    .line 277
    int-to-float v3, v3

    .line 278
    div-float/2addr v3, v4

    .line 279
    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 280
    .line 281
    .line 282
    :goto_1
    return-void
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private 〇80〇808〇O()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-le v0, v1, :cond_0

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-static {v0}, Lcom/intsig/utils/DeviceUtil;->o〇O8〇〇o(Landroid/content/Context;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    const/4 v0, 0x1

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v0, 0x0

    .line 24
    :goto_0
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private 〇o00〇〇Oo(Landroid/graphics/Canvas;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇o〇(Landroid/graphics/Canvas;)V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇08〇o0O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 6
    .line 7
    iget v1, v0, Landroid/graphics/RectF;->left:F

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/high16 v2, 0x40000000    # 2.0f

    .line 14
    .line 15
    div-float/2addr v0, v2

    .line 16
    add-float/2addr v1, v0

    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 18
    .line 19
    iget v0, v0, Landroid/graphics/RectF;->top:F

    .line 20
    .line 21
    iget v3, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o8oOOo:I

    .line 22
    .line 23
    int-to-float v3, v3

    .line 24
    add-float/2addr v0, v3

    .line 25
    iget v3, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O88O:I

    .line 26
    .line 27
    int-to-float v3, v3

    .line 28
    div-float/2addr v3, v2

    .line 29
    add-float/2addr v0, v3

    .line 30
    invoke-direct {p0}, Lcom/intsig/camscanner/view/CertificateBmView;->〇80〇808〇O()Z

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    if-eqz v3, :cond_0

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 37
    .line 38
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    div-float v1, v0, v2

    .line 43
    .line 44
    iget v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o8oOOo:I

    .line 45
    .line 46
    int-to-float v0, v0

    .line 47
    iget v3, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O88O:I

    .line 48
    .line 49
    int-to-float v3, v3

    .line 50
    div-float/2addr v3, v2

    .line 51
    add-float/2addr v0, v3

    .line 52
    :cond_0
    iget-object v3, p0, Lcom/intsig/camscanner/view/CertificateBmView;->oOo〇8o008:Ljava/lang/String;

    .line 53
    .line 54
    invoke-direct {p0, p1, v1, v0, v3}, Lcom/intsig/camscanner/view/CertificateBmView;->Oo08(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    .line 55
    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 58
    .line 59
    iget v1, v0, Landroid/graphics/RectF;->left:F

    .line 60
    .line 61
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    div-float/2addr v0, v2

    .line 66
    add-float/2addr v1, v0

    .line 67
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 68
    .line 69
    iget v3, v0, Landroid/graphics/RectF;->top:F

    .line 70
    .line 71
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    div-float/2addr v0, v2

    .line 76
    add-float/2addr v3, v0

    .line 77
    iget v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o8oOOo:I

    .line 78
    .line 79
    int-to-float v0, v0

    .line 80
    add-float/2addr v3, v0

    .line 81
    iget v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O88O:I

    .line 82
    .line 83
    int-to-float v0, v0

    .line 84
    div-float/2addr v0, v2

    .line 85
    add-float/2addr v3, v0

    .line 86
    invoke-direct {p0}, Lcom/intsig/camscanner/view/CertificateBmView;->〇80〇808〇O()Z

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    if-eqz v0, :cond_1

    .line 91
    .line 92
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 93
    .line 94
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    div-float v1, v0, v2

    .line 99
    .line 100
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 101
    .line 102
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 103
    .line 104
    .line 105
    move-result v0

    .line 106
    div-float/2addr v0, v2

    .line 107
    iget v3, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o8oOOo:I

    .line 108
    .line 109
    int-to-float v3, v3

    .line 110
    add-float/2addr v0, v3

    .line 111
    iget v3, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O88O:I

    .line 112
    .line 113
    int-to-float v3, v3

    .line 114
    div-float/2addr v3, v2

    .line 115
    add-float/2addr v3, v0

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->oOo0:Ljava/lang/String;

    .line 117
    .line 118
    invoke-direct {p0, p1, v1, v3, v0}, Lcom/intsig/camscanner/view/CertificateBmView;->Oo08(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    .line 119
    .line 120
    .line 121
    :cond_2
    return-void
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private 〇〇888(Landroid/graphics/Canvas;)V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->Oo80:Z

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 6
    .line 7
    iget v1, v0, Landroid/graphics/RectF;->left:F

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/high16 v2, 0x40000000    # 2.0f

    .line 14
    .line 15
    div-float/2addr v0, v2

    .line 16
    add-float/2addr v1, v0

    .line 17
    iget v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇8〇oO〇〇8o:I

    .line 18
    .line 19
    int-to-float v0, v0

    .line 20
    iget v3, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O88O:I

    .line 21
    .line 22
    int-to-float v3, v3

    .line 23
    div-float/2addr v3, v2

    .line 24
    add-float/2addr v0, v3

    .line 25
    invoke-direct {p0}, Lcom/intsig/camscanner/view/CertificateBmView;->〇80〇808〇O()Z

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    if-eqz v3, :cond_0

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 32
    .line 33
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    div-float v1, v0, v2

    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇0O:Landroid/graphics/RectF;

    .line 40
    .line 41
    iget v0, v0, Landroid/graphics/RectF;->left:F

    .line 42
    .line 43
    neg-float v0, v0

    .line 44
    div-float/2addr v0, v2

    .line 45
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o8〇OO0〇0o:Ljava/lang/String;

    .line 46
    .line 47
    invoke-direct {p0, p1, v1, v0, v2}, Lcom/intsig/camscanner/view/CertificateBmView;->Oo08(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    .line 48
    .line 49
    .line 50
    :cond_1
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public OO0o〇〇〇〇0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇08〇o0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 5
    .line 6
    if-eqz v0, :cond_1

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 15
    .line 16
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_0

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/view/CertificateBmView;->〇080()V

    .line 24
    .line 25
    .line 26
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/CertificateBmView;->〇o00〇〇Oo(Landroid/graphics/Canvas;)V

    .line 27
    .line 28
    .line 29
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->OO:Z

    .line 30
    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    iget v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇08O〇00〇o:I

    .line 34
    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/CertificateBmView;->O8(Landroid/graphics/Canvas;)V

    .line 38
    .line 39
    .line 40
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/CertificateBmView;->〇o〇(Landroid/graphics/Canvas;)V

    .line 41
    .line 42
    .line 43
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/CertificateBmView;->o〇0(Landroid/graphics/Canvas;)V

    .line 44
    .line 45
    .line 46
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/CertificateBmView;->〇〇888(Landroid/graphics/Canvas;)V

    .line 47
    .line 48
    .line 49
    :cond_1
    :goto_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o0:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFrameMargin(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o〇oO:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFrameRotation(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O〇o88o08〇:I

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/view/CertificateBmView;->oO80()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRadium(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o〇00O:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setShowOutsideTopCenterTip(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇〇o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTipBg(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CertificateBmView;->ooo0〇〇O:Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTipBottom(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->oOo0:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTipOutsideTop(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->o8〇OO0〇0o:Ljava/lang/String;

    .line 2
    .line 3
    const/4 p1, 0x1

    .line 4
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->Oo80:Z

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTipOutsideTopCenter(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->OO〇00〇8oO:Ljava/lang/String;

    .line 2
    .line 3
    const/4 p1, 0x1

    .line 4
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->〇〇o〇:Z

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTipRadius(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->O0O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTipTop(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/CertificateBmView;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

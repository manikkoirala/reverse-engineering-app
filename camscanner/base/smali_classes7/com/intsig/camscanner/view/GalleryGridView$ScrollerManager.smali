.class Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;
.super Ljava/lang/Object;
.source "GalleryGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/view/GalleryGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScrollerManager"
.end annotation


# instance fields
.field private 〇080:Landroid/widget/Scroller;

.field private 〇o00〇〇Oo:I

.field private final 〇o〇:Lcom/intsig/camscanner/view/GalleryGridView;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/intsig/camscanner/view/GalleryGridView;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Landroid/widget/Scroller;

    .line 5
    .line 6
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    .line 7
    .line 8
    const/high16 v2, 0x3f000000    # 0.5f

    .line 9
    .line 10
    invoke-direct {v1, v2}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    .line 11
    .line 12
    .line 13
    invoke-direct {v0, p1, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇080:Landroid/widget/Scroller;

    .line 17
    .line 18
    iput-object p2, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇o〇:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private O8(I)I
    .locals 1

    .line 1
    int-to-float p1, p1

    .line 2
    const/high16 v0, 0x3f880000    # 1.0625f

    .line 3
    .line 4
    mul-float p1, p1, v0

    .line 5
    .line 6
    float-to-int p1, p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇o00〇〇Oo(I)V
    .locals 12

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇080:Landroid/widget/Scroller;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇o〇(I)I

    .line 11
    .line 12
    .line 13
    move-result v5

    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇o〇:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 15
    .line 16
    invoke-virtual {v0, v5}, Landroid/view/View;->canScrollVertically(I)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    return-void

    .line 23
    :cond_1
    invoke-direct {p0, v5}, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->O8(I)I

    .line 24
    .line 25
    .line 26
    move-result v11

    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇080:Landroid/widget/Scroller;

    .line 28
    .line 29
    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    iput v0, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇o00〇〇Oo:I

    .line 34
    .line 35
    const/4 v0, 0x2

    .line 36
    if-ne v0, p1, :cond_2

    .line 37
    .line 38
    iget-object v6, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇080:Landroid/widget/Scroller;

    .line 39
    .line 40
    const/4 v7, 0x0

    .line 41
    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrY()I

    .line 42
    .line 43
    .line 44
    move-result v8

    .line 45
    const/4 v9, 0x0

    .line 46
    neg-int v10, v5

    .line 47
    invoke-virtual/range {v6 .. v11}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_2
    const/4 v0, 0x1

    .line 52
    if-ne v0, p1, :cond_3

    .line 53
    .line 54
    iget-object v1, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇080:Landroid/widget/Scroller;

    .line 55
    .line 56
    const/4 v2, 0x0

    .line 57
    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    const/4 v4, 0x0

    .line 62
    move v6, v11

    .line 63
    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 64
    .line 65
    .line 66
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇o〇:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 67
    .line 68
    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    .line 69
    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private 〇o〇(I)I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇o〇:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/GalleryGridView;->getComputeVerticalScrollRange()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇o〇:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/GalleryGridView;->getComputeVerticalScrollOffset()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, 0x1

    .line 14
    if-ne v2, p1, :cond_0

    .line 15
    .line 16
    sub-int/2addr v0, v1

    .line 17
    return v0

    .line 18
    :cond_0
    const/4 v0, 0x2

    .line 19
    if-ne v0, p1, :cond_1

    .line 20
    .line 21
    return v1

    .line 22
    :cond_1
    const/4 p1, 0x0

    .line 23
    return p1
    .line 24
.end method


# virtual methods
.method Oo08()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇o00〇〇Oo(I)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method o〇0()V
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇o00〇〇Oo(I)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method 〇080()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇080:Landroid/widget/Scroller;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇o〇:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇080:Landroid/widget/Scroller;

    .line 14
    .line 15
    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    iget v2, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇o00〇〇Oo:I

    .line 20
    .line 21
    sub-int/2addr v1, v2

    .line 22
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->scrollListBy(I)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇080:Landroid/widget/Scroller;

    .line 26
    .line 27
    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    iput v0, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇o00〇〇Oo:I

    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇o〇:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 34
    .line 35
    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    .line 36
    .line 37
    .line 38
    :cond_0
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method 〇〇888()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇080:Landroid/widget/Scroller;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public Lcom/intsig/camscanner/view/CircleProgressBar;
.super Landroid/view/View;
.source "CircleProgressBar.java"


# static fields
.field private static O88O:I = -0x1

.field private static final 〇O〇〇O8:Ljava/lang/String; = "CircleProgressBar"

.field private static 〇o0O:I = 0x1


# instance fields
.field private O0O:I

.field private O8o08O8O:I

.field private OO:F

.field private OO〇00〇8oO:[I

.field private o0:I

.field private o8oOOo:Landroid/graphics/RectF;

.field private o8〇OO0〇0o:I

.field private oOo0:I

.field private oOo〇8o008:I

.field private ooo0〇〇O:[J

.field private o〇00O:Z

.field private 〇080OO8〇0:I

.field private 〇08O〇00〇o:Landroid/graphics/Paint;

.field private 〇0O:Z

.field private 〇8〇oO〇〇8o:I

.field private 〇OOo8〇0:I

.field private 〇〇08O:J


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->O8o08O8O:I

    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇080OO8〇0:I

    .line 8
    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇0O:Z

    .line 10
    .line 11
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->oOo〇8o008:I

    .line 12
    .line 13
    const/4 v0, 0x5

    .line 14
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->oOo0:I

    .line 15
    .line 16
    const/4 v0, 0x1

    .line 17
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇8〇oO〇〇8o:I

    .line 18
    .line 19
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->O0O:I

    .line 20
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/view/CircleProgressBar;->〇080(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇080(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 11

    .line 1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const v1, 0x7f0700eb

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->oOo0:I

    .line 13
    .line 14
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 23
    .line 24
    float-to-double v1, v0

    .line 25
    const-wide/high16 v3, 0x3ff8000000000000L    # 1.5

    .line 26
    .line 27
    const/4 v5, 0x2

    .line 28
    const/4 v6, 0x1

    .line 29
    cmpl-double v7, v1, v3

    .line 30
    .line 31
    if-lez v7, :cond_0

    .line 32
    .line 33
    iput v5, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->O0O:I

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_0
    iput v6, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->O0O:I

    .line 37
    .line 38
    :goto_0
    sget-object v1, Lcom/intsig/camscanner/view/CircleProgressBar;->〇O〇〇O8:Ljava/lang/String;

    .line 39
    .line 40
    new-instance v2, Ljava/lang/StringBuilder;

    .line 41
    .line 42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    .line 44
    .line 45
    const-string v3, "mChangeStep="

    .line 46
    .line 47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    iget v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->oOo0:I

    .line 51
    .line 52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    const-string v3, " density="

    .line 56
    .line 57
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    const-string v0, " mScaleStep="

    .line 64
    .line 65
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    iget v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->O0O:I

    .line 69
    .line 70
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    iget v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->oOo0:I

    .line 81
    .line 82
    const/4 v1, 0x0

    .line 83
    if-lez v0, :cond_1

    .line 84
    .line 85
    iget v2, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->O0O:I

    .line 86
    .line 87
    if-lez v2, :cond_1

    .line 88
    .line 89
    add-int/2addr v0, v0

    .line 90
    div-int/2addr v0, v2

    .line 91
    new-array v2, v0, [I

    .line 92
    .line 93
    iput-object v2, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->OO〇00〇8oO:[I

    .line 94
    .line 95
    new-array v2, v0, [J

    .line 96
    .line 97
    iput-object v2, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->ooo0〇〇O:[J

    .line 98
    .line 99
    const/4 v2, 0x0

    .line 100
    const/4 v3, 0x0

    .line 101
    :goto_1
    if-ge v2, v0, :cond_2

    .line 102
    .line 103
    iget-object v4, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->OO〇00〇8oO:[I

    .line 104
    .line 105
    aput v3, v4, v2

    .line 106
    .line 107
    iget v4, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->O0O:I

    .line 108
    .line 109
    add-int/2addr v3, v4

    .line 110
    iget-object v4, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->ooo0〇〇O:[J

    .line 111
    .line 112
    const-wide/16 v7, 0x28

    .line 113
    .line 114
    int-to-long v9, v2

    .line 115
    add-long/2addr v9, v7

    .line 116
    aput-wide v9, v4, v2

    .line 117
    .line 118
    add-int/lit8 v2, v2, 0x1

    .line 119
    .line 120
    goto :goto_1

    .line 121
    :cond_1
    const/4 v0, 0x5

    .line 122
    new-array v2, v0, [I

    .line 123
    .line 124
    fill-array-data v2, :array_0

    .line 125
    .line 126
    .line 127
    iput-object v2, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->OO〇00〇8oO:[I

    .line 128
    .line 129
    new-array v2, v0, [J

    .line 130
    .line 131
    fill-array-data v2, :array_1

    .line 132
    .line 133
    .line 134
    iput-object v2, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->ooo0〇〇O:[J

    .line 135
    .line 136
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->oOo0:I

    .line 137
    .line 138
    :cond_2
    iput v1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o8〇OO0〇0o:I

    .line 139
    .line 140
    if-eqz p2, :cond_3

    .line 141
    .line 142
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    sget-object v2, Lcom/intsig/camscanner/R$styleable;->CircleProgressBar:[I

    .line 147
    .line 148
    invoke-virtual {v0, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    .line 149
    .line 150
    .line 151
    move-result-object p2

    .line 152
    const/4 v0, 0x4

    .line 153
    iget v2, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->oOo0:I

    .line 154
    .line 155
    invoke-virtual {p2, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 156
    .line 157
    .line 158
    move-result v0

    .line 159
    int-to-float v0, v0

    .line 160
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->OO:F

    .line 161
    .line 162
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 163
    .line 164
    .line 165
    move-result-object v0

    .line 166
    const v2, 0x7f0601ec

    .line 167
    .line 168
    .line 169
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    .line 170
    .line 171
    .line 172
    move-result v0

    .line 173
    const/4 v2, 0x3

    .line 174
    invoke-virtual {p2, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 175
    .line 176
    .line 177
    move-result v0

    .line 178
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o0:I

    .line 179
    .line 180
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 181
    .line 182
    .line 183
    move-result-object p1

    .line 184
    const v0, 0x7f0601ee

    .line 185
    .line 186
    .line 187
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    .line 188
    .line 189
    .line 190
    move-result p1

    .line 191
    invoke-virtual {p2, v5, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 192
    .line 193
    .line 194
    move-result p1

    .line 195
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇OOo8〇0:I

    .line 196
    .line 197
    invoke-virtual {p2, v1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    .line 198
    .line 199
    .line 200
    move-result p1

    .line 201
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o〇00O:Z

    .line 202
    .line 203
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 204
    .line 205
    .line 206
    :cond_3
    new-instance p1, Landroid/graphics/Paint;

    .line 207
    .line 208
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 209
    .line 210
    .line 211
    iput-object p1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 212
    .line 213
    invoke-virtual {p1, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 214
    .line 215
    .line 216
    iget-object p1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 217
    .line 218
    iget p2, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->OO:F

    .line 219
    .line 220
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 221
    .line 222
    .line 223
    iget-object p1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 224
    .line 225
    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 226
    .line 227
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 228
    .line 229
    .line 230
    new-instance p1, Landroid/graphics/RectF;

    .line 231
    .line 232
    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    .line 233
    .line 234
    .line 235
    iput-object p1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o8oOOo:Landroid/graphics/RectF;

    .line 236
    .line 237
    return-void

    .line 238
    nop

    .line 239
    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
    .end array-data

    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    :array_1
    .array-data 8
        0x29
        0x2a
        0x2b
        0x2c
        0x2d
    .end array-data
.end method


# virtual methods
.method public O8(F)V
    .locals 1

    .line 1
    const/high16 v0, 0x43b40000    # 360.0f

    .line 2
    .line 3
    mul-float p1, p1, v0

    .line 4
    .line 5
    float-to-int p1, p1

    .line 6
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->O8o08O8O:I

    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇0O:Z

    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-lez v0, :cond_6

    .line 13
    .line 14
    if-lez v1, :cond_6

    .line 15
    .line 16
    int-to-float v0, v0

    .line 17
    const/high16 v2, 0x40000000    # 2.0f

    .line 18
    .line 19
    div-float/2addr v0, v2

    .line 20
    int-to-float v1, v1

    .line 21
    div-float/2addr v1, v2

    .line 22
    iget-boolean v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇0O:Z

    .line 23
    .line 24
    const/4 v4, 0x0

    .line 25
    if-eqz v3, :cond_2

    .line 26
    .line 27
    iget v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o8〇OO0〇0o:I

    .line 28
    .line 29
    if-gez v3, :cond_0

    .line 30
    .line 31
    iput v4, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o8〇OO0〇0o:I

    .line 32
    .line 33
    sget v3, Lcom/intsig/camscanner/view/CircleProgressBar;->〇o0O:I

    .line 34
    .line 35
    iput v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇8〇oO〇〇8o:I

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    iget-object v5, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->OO〇00〇8oO:[I

    .line 39
    .line 40
    array-length v6, v5

    .line 41
    if-lt v3, v6, :cond_1

    .line 42
    .line 43
    array-length v3, v5

    .line 44
    add-int/lit8 v3, v3, -0x1

    .line 45
    .line 46
    iput v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o8〇OO0〇0o:I

    .line 47
    .line 48
    sget v3, Lcom/intsig/camscanner/view/CircleProgressBar;->O88O:I

    .line 49
    .line 50
    iput v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇8〇oO〇〇8o:I

    .line 51
    .line 52
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->ooo0〇〇O:[J

    .line 53
    .line 54
    iget v5, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o8〇OO0〇0o:I

    .line 55
    .line 56
    aget-wide v6, v3, v5

    .line 57
    .line 58
    iput-wide v6, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇〇08O:J

    .line 59
    .line 60
    iget-object v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->OO〇00〇8oO:[I

    .line 61
    .line 62
    aget v3, v3, v5

    .line 63
    .line 64
    iput v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->oOo〇8o008:I

    .line 65
    .line 66
    iget v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇8〇oO〇〇8o:I

    .line 67
    .line 68
    add-int/2addr v5, v3

    .line 69
    iput v5, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o8〇OO0〇0o:I

    .line 70
    .line 71
    goto :goto_1

    .line 72
    :cond_2
    iput v4, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->oOo〇8o008:I

    .line 73
    .line 74
    :goto_1
    iget v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->OO:F

    .line 75
    .line 76
    div-float/2addr v3, v2

    .line 77
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    .line 78
    .line 79
    .line 80
    move-result v2

    .line 81
    sub-float/2addr v2, v3

    .line 82
    iget v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->oOo〇8o008:I

    .line 83
    .line 84
    int-to-float v3, v3

    .line 85
    sub-float/2addr v2, v3

    .line 86
    iget-object v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o8oOOo:Landroid/graphics/RectF;

    .line 87
    .line 88
    sub-float v5, v0, v2

    .line 89
    .line 90
    sub-float v6, v1, v2

    .line 91
    .line 92
    add-float v7, v0, v2

    .line 93
    .line 94
    add-float v8, v1, v2

    .line 95
    .line 96
    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 97
    .line 98
    .line 99
    iget-object v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 100
    .line 101
    iget v5, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o0:I

    .line 102
    .line 103
    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 104
    .line 105
    .line 106
    iget-object v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 107
    .line 108
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 109
    .line 110
    .line 111
    iget-object v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 112
    .line 113
    iget v1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇OOo8〇0:I

    .line 114
    .line 115
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 116
    .line 117
    .line 118
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o〇00O:Z

    .line 119
    .line 120
    const/16 v1, 0x168

    .line 121
    .line 122
    if-eqz v0, :cond_4

    .line 123
    .line 124
    iget v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇080OO8〇0:I

    .line 125
    .line 126
    if-le v0, v1, :cond_3

    .line 127
    .line 128
    iput v4, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇080OO8〇0:I

    .line 129
    .line 130
    :cond_3
    iget-object v6, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o8oOOo:Landroid/graphics/RectF;

    .line 131
    .line 132
    iget v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇080OO8〇0:I

    .line 133
    .line 134
    int-to-float v7, v0

    .line 135
    const/high16 v8, 0x41f00000    # 30.0f

    .line 136
    .line 137
    const/4 v9, 0x0

    .line 138
    iget-object v10, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 139
    .line 140
    move-object v5, p1

    .line 141
    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 142
    .line 143
    .line 144
    iget p1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇080OO8〇0:I

    .line 145
    .line 146
    add-int/lit8 p1, p1, 0xa

    .line 147
    .line 148
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇080OO8〇0:I

    .line 149
    .line 150
    const-wide/16 v0, 0x1e

    .line 151
    .line 152
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->postInvalidateDelayed(J)V

    .line 153
    .line 154
    .line 155
    goto :goto_2

    .line 156
    :cond_4
    iget v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->O8o08O8O:I

    .line 157
    .line 158
    if-le v0, v1, :cond_5

    .line 159
    .line 160
    iput v1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->O8o08O8O:I

    .line 161
    .line 162
    :cond_5
    iget-object v3, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o8oOOo:Landroid/graphics/RectF;

    .line 163
    .line 164
    const/high16 v4, 0x43870000    # 270.0f

    .line 165
    .line 166
    iget v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->O8o08O8O:I

    .line 167
    .line 168
    int-to-float v5, v0

    .line 169
    const/4 v6, 0x0

    .line 170
    iget-object v7, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 171
    .line 172
    move-object v2, p1

    .line 173
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 174
    .line 175
    .line 176
    iget-boolean p1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇0O:Z

    .line 177
    .line 178
    if-eqz p1, :cond_6

    .line 179
    .line 180
    iget-wide v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇〇08O:J

    .line 181
    .line 182
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->postInvalidateDelayed(J)V

    .line 183
    .line 184
    .line 185
    :cond_6
    :goto_2
    return-void
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public declared-synchronized setProgress(F)V
    .locals 1

    monitor-enter p0

    const/high16 v0, 0x43b40000    # 360.0f

    mul-float p1, p1, v0

    const/high16 v0, 0x42c80000    # 100.0f

    div-float/2addr p1, v0

    float-to-int p1, p1

    .line 5
    :try_start_0
    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->O8o08O8O:I

    const/4 p1, 0x0

    .line 6
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇0O:Z

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setProgress(I)V
    .locals 0

    monitor-enter p0

    mul-int/lit16 p1, p1, 0x168

    .line 1
    :try_start_0
    div-int/lit8 p1, p1, 0x64

    iput p1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->O8o08O8O:I

    const/4 p1, 0x0

    .line 2
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇0O:Z

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public 〇o00〇〇Oo()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o8〇OO0〇0o:I

    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇8〇oO〇〇8o:I

    .line 6
    .line 7
    const/16 v1, 0x168

    .line 8
    .line 9
    iput v1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->O8o08O8O:I

    .line 10
    .line 11
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇0O:Z

    .line 12
    .line 13
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->o8〇OO0〇0o:I

    .line 3
    .line 4
    const/4 v1, 0x1

    .line 5
    iput v1, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇8〇oO〇〇8o:I

    .line 6
    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/CircleProgressBar;->〇0O:Z

    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

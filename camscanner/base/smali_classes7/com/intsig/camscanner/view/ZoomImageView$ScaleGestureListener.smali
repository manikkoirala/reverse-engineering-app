.class Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;
.super Lcom/intsig/camscanner/multitouch/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "ZoomImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/view/ZoomImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleGestureListener"
.end annotation


# instance fields
.field final synthetic 〇080:Lcom/intsig/camscanner/view/ZoomImageView;


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/view/ZoomImageView;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    invoke-direct {p0}, Lcom/intsig/camscanner/multitouch/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/view/ZoomImageView;Lcom/intsig/camscanner/view/O0O8OO088;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;-><init>(Lcom/intsig/camscanner/view/ZoomImageView;)V

    return-void
.end method


# virtual methods
.method public 〇080(Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;)Z
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;->〇〇888()F

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    mul-float v1, v1, v2

    .line 16
    .line 17
    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    if-eqz v2, :cond_1

    .line 22
    .line 23
    return v0

    .line 24
    :cond_1
    const v0, 0x3f666666    # 0.9f

    .line 25
    .line 26
    .line 27
    cmpg-float v2, v1, v0

    .line 28
    .line 29
    if-gez v2, :cond_2

    .line 30
    .line 31
    const v1, 0x3f666666    # 0.9f

    .line 32
    .line 33
    .line 34
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 35
    .line 36
    invoke-static {v0}, Lcom/intsig/camscanner/view/ZoomImageView;->Oo8Oo00oo(Lcom/intsig/camscanner/view/ZoomImageView;)F

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    const/4 v2, 0x0

    .line 41
    cmpl-float v0, v0, v2

    .line 42
    .line 43
    if-lez v0, :cond_3

    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 46
    .line 47
    invoke-static {v0}, Lcom/intsig/camscanner/view/ZoomImageView;->Oo8Oo00oo(Lcom/intsig/camscanner/view/ZoomImageView;)F

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    const v2, 0x3fa66666    # 1.3f

    .line 52
    .line 53
    .line 54
    mul-float v0, v0, v2

    .line 55
    .line 56
    cmpl-float v0, v1, v0

    .line 57
    .line 58
    if-lez v0, :cond_3

    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 61
    .line 62
    invoke-static {v0}, Lcom/intsig/camscanner/view/ZoomImageView;->Oo8Oo00oo(Lcom/intsig/camscanner/view/ZoomImageView;)F

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    mul-float v1, v0, v2

    .line 67
    .line 68
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 69
    .line 70
    invoke-virtual {p1}, Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;->〇o00〇〇Oo()F

    .line 71
    .line 72
    .line 73
    move-result v2

    .line 74
    invoke-virtual {p1}, Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;->〇o〇()F

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    invoke-virtual {v0, v1, v2, p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇oo〇(FFF)[F

    .line 79
    .line 80
    .line 81
    iget-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 82
    .line 83
    invoke-static {p1}, Lcom/intsig/camscanner/view/ZoomImageView;->〇〇〇0〇〇0(Lcom/intsig/camscanner/view/ZoomImageView;)Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    if-eqz p1, :cond_4

    .line 88
    .line 89
    iget-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 90
    .line 91
    invoke-static {p1}, Lcom/intsig/camscanner/view/ZoomImageView;->〇〇〇0〇〇0(Lcom/intsig/camscanner/view/ZoomImageView;)Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 96
    .line 97
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 98
    .line 99
    .line 100
    move-result v1

    .line 101
    invoke-interface {p1, v0, v1}, Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;->O8(Lcom/intsig/camscanner/view/ZoomImageView;F)V

    .line 102
    .line 103
    .line 104
    :cond_4
    const/4 p1, 0x1

    .line 105
    return p1
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public 〇o00〇〇Oo(Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-static {v0, v1}, Lcom/intsig/camscanner/view/ZoomImageView;->〇08O8o〇0(Lcom/intsig/camscanner/view/ZoomImageView;Z)V

    .line 5
    .line 6
    .line 7
    invoke-super {p0, p1}, Lcom/intsig/camscanner/multitouch/ScaleGestureDetector$SimpleOnScaleGestureListener;->〇o00〇〇Oo(Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇o〇(Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;)V
    .locals 9

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/multitouch/ScaleGestureDetector$SimpleOnScaleGestureListener;->〇o〇(Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/high16 v1, 0x3f800000    # 1.0f

    .line 11
    .line 12
    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/16 v1, 0x64

    .line 17
    .line 18
    if-gez v0, :cond_0

    .line 19
    .line 20
    iget-object v2, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 21
    .line 22
    invoke-virtual {v2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 23
    .line 24
    .line 25
    move-result v3

    .line 26
    const/high16 v4, 0x3f800000    # 1.0f

    .line 27
    .line 28
    invoke-virtual {p1}, Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;->〇o00〇〇Oo()F

    .line 29
    .line 30
    .line 31
    move-result v5

    .line 32
    invoke-virtual {p1}, Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;->〇o〇()F

    .line 33
    .line 34
    .line 35
    move-result v6

    .line 36
    int-to-long v7, v1

    .line 37
    invoke-static/range {v2 .. v8}, Lcom/intsig/camscanner/view/ZoomImageView;->O08000(Lcom/intsig/camscanner/view/ZoomImageView;FFFFJ)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 42
    .line 43
    invoke-static {v0}, Lcom/intsig/camscanner/view/ZoomImageView;->Oo8Oo00oo(Lcom/intsig/camscanner/view/ZoomImageView;)F

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    const/4 v2, 0x0

    .line 48
    cmpl-float v0, v0, v2

    .line 49
    .line 50
    if-lez v0, :cond_1

    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 53
    .line 54
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    iget-object v2, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 59
    .line 60
    invoke-static {v2}, Lcom/intsig/camscanner/view/ZoomImageView;->Oo8Oo00oo(Lcom/intsig/camscanner/view/ZoomImageView;)F

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    invoke-static {v0, v2}, Ljava/lang/Float;->compare(FF)I

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    if-lez v0, :cond_1

    .line 69
    .line 70
    iget-object v2, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 71
    .line 72
    invoke-virtual {v2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 73
    .line 74
    .line 75
    move-result v3

    .line 76
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 77
    .line 78
    invoke-static {v0}, Lcom/intsig/camscanner/view/ZoomImageView;->Oo8Oo00oo(Lcom/intsig/camscanner/view/ZoomImageView;)F

    .line 79
    .line 80
    .line 81
    move-result v4

    .line 82
    invoke-virtual {p1}, Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;->〇o00〇〇Oo()F

    .line 83
    .line 84
    .line 85
    move-result v5

    .line 86
    invoke-virtual {p1}, Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;->〇o〇()F

    .line 87
    .line 88
    .line 89
    move-result v6

    .line 90
    int-to-long v7, v1

    .line 91
    invoke-static/range {v2 .. v8}, Lcom/intsig/camscanner/view/ZoomImageView;->O08000(Lcom/intsig/camscanner/view/ZoomImageView;FFFFJ)V

    .line 92
    .line 93
    .line 94
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 95
    .line 96
    invoke-static {p1}, Lcom/intsig/camscanner/view/ZoomImageView;->〇〇〇0〇〇0(Lcom/intsig/camscanner/view/ZoomImageView;)Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    if-eqz p1, :cond_2

    .line 101
    .line 102
    iget-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 103
    .line 104
    invoke-static {p1}, Lcom/intsig/camscanner/view/ZoomImageView;->〇〇〇0〇〇0(Lcom/intsig/camscanner/view/ZoomImageView;)Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    invoke-interface {p1}, Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;->〇o〇()V

    .line 109
    .line 110
    .line 111
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 112
    .line 113
    const/4 v0, 0x0

    .line 114
    invoke-static {p1, v0}, Lcom/intsig/camscanner/view/ZoomImageView;->〇08O8o〇0(Lcom/intsig/camscanner/view/ZoomImageView;Z)V

    .line 115
    .line 116
    .line 117
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

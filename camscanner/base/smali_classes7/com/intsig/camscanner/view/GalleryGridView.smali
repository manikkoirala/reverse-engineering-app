.class public Lcom/intsig/camscanner/view/GalleryGridView;
.super Landroid/widget/GridView;
.source "GalleryGridView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;,
        Lcom/intsig/camscanner/view/GalleryGridView$ScrollSelectManager;,
        Lcom/intsig/camscanner/view/GalleryGridView$InterceptCallBack;
    }
.end annotation


# instance fields
.field private o0:Lcom/intsig/camscanner/view/GalleryGridView$ScrollSelectManager;

.field private 〇OOo8〇0:Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/GalleryGridView;->〇080(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/GalleryGridView;->〇080(Landroid/content/Context;)V

    return-void
.end method

.method private 〇080(Landroid/content/Context;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;

    .line 2
    .line 3
    invoke-direct {v0, p1, p0}, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/view/GalleryGridView;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/view/GalleryGridView;->〇OOo8〇0:Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;

    .line 7
    .line 8
    new-instance v1, Lcom/intsig/camscanner/view/GalleryGridView$ScrollSelectManager;

    .line 9
    .line 10
    invoke-direct {v1, p1, v0, p0}, Lcom/intsig/camscanner/view/GalleryGridView$ScrollSelectManager;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;Landroid/widget/GridView;)V

    .line 11
    .line 12
    .line 13
    iput-object v1, p0, Lcom/intsig/camscanner/view/GalleryGridView;->o0:Lcom/intsig/camscanner/view/GalleryGridView$ScrollSelectManager;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public computeScroll()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/GridView;->computeScroll()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/view/GalleryGridView;->〇OOo8〇0:Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇080()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/GalleryGridView;->o0:Lcom/intsig/camscanner/view/GalleryGridView$ScrollSelectManager;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/GalleryGridView$ScrollSelectManager;->〇080(Landroid/view/MotionEvent;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    return p1

    .line 11
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/GridView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    return p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getComputeVerticalScrollOffset()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->computeVerticalScrollOffset()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getComputeVerticalScrollRange()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->computeVerticalScrollRange()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/GridView;->onDetachedFromWindow()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/view/GalleryGridView;->〇OOo8〇0:Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/GalleryGridView$ScrollerManager;->〇〇888()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public performClick()Z
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/GridView;->performClick()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setInterceptCallBack(Lcom/intsig/camscanner/view/GalleryGridView$InterceptCallBack;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/GalleryGridView;->o0:Lcom/intsig/camscanner/view/GalleryGridView$ScrollSelectManager;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/GalleryGridView$ScrollSelectManager;->O8(Lcom/intsig/camscanner/view/GalleryGridView$InterceptCallBack;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

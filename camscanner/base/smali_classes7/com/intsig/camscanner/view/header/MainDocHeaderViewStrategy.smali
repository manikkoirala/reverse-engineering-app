.class public Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;
.super Ljava/lang/Object;
.source "MainDocHeaderViewStrategy.java"

# interfaces
.implements Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;"
    }
.end annotation


# instance fields
.field private O8:Landroid/view/View;

.field private OO0o〇〇:Landroid/view/animation/RotateAnimation;

.field private OO0o〇〇〇〇0:Z

.field private Oo08:Landroid/widget/ProgressBar;

.field private Oooo8o0〇:Landroid/view/View;

.field private oO80:Landroid/widget/TextView;

.field private o〇0:Landroid/widget/TextView;

.field private final 〇080:Ljava/lang/String;

.field private 〇0〇O0088o:Ljava/util/TimerTask;

.field private 〇80〇808〇O:Landroid/widget/ImageView;

.field private 〇8o8o〇:Lcom/intsig/camscanner/view/OnHeaderRefreshListener;

.field private 〇O00:Landroid/os/Handler;

.field private 〇O8o08O:Landroid/view/animation/RotateAnimation;

.field private 〇O〇:J

.field private final 〇o00〇〇Oo:Landroid/content/Context;

.field private 〇o〇:Landroid/view/View;

.field private 〇〇808〇:Ljava/util/Timer;

.field private 〇〇888:Landroid/widget/TextView;

.field private 〇〇8O0〇8:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, "MainDocHeaderViewStrategy"

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇080:Ljava/lang/String;

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->OO0o〇〇〇〇0:Z

    .line 10
    .line 11
    const-wide/16 v0, 0xbb8

    .line 12
    .line 13
    iput-wide v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇O〇:J

    .line 14
    .line 15
    new-instance v0, Landroid/os/Handler;

    .line 16
    .line 17
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 22
    .line 23
    .line 24
    iput-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇O00:Landroid/os/Handler;

    .line 25
    .line 26
    new-instance v0, Ljava/lang/ref/WeakReference;

    .line 27
    .line 28
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 29
    .line 30
    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇〇8O0〇8:Ljava/lang/ref/WeakReference;

    .line 32
    .line 33
    iput-object p2, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇o00〇〇Oo:Landroid/content/Context;

    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->o800o8O()V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->oo88o8O()V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p0, p3}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇O888o0o(Landroid/view/ViewGroup;)V

    .line 42
    .line 43
    .line 44
    new-instance p1, Ljava/util/Timer;

    .line 45
    .line 46
    invoke-direct {p1}, Ljava/util/Timer;-><init>()V

    .line 47
    .line 48
    .line 49
    iput-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇〇808〇:Ljava/util/Timer;

    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private OoO8()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy$1;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy$1;-><init>(Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇0〇O0088o:Ljava/util/TimerTask;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o800o8O()V
    .locals 8

    .line 1
    new-instance v7, Landroid/view/animation/RotateAnimation;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/high16 v2, -0x3ccc0000    # -180.0f

    .line 5
    .line 6
    const/4 v3, 0x1

    .line 7
    const/high16 v4, 0x3f000000    # 0.5f

    .line 8
    .line 9
    const/4 v5, 0x1

    .line 10
    const/high16 v6, 0x3f000000    # 0.5f

    .line 11
    .line 12
    move-object v0, v7

    .line 13
    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 14
    .line 15
    .line 16
    iput-object v7, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇O8o08O:Landroid/view/animation/RotateAnimation;

    .line 17
    .line 18
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    .line 19
    .line 20
    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v7, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇O8o08O:Landroid/view/animation/RotateAnimation;

    .line 27
    .line 28
    const-wide/16 v1, 0xfa

    .line 29
    .line 30
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇O8o08O:Landroid/view/animation/RotateAnimation;

    .line 34
    .line 35
    const/4 v1, 0x1

    .line 36
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private oo88o8O()V
    .locals 8

    .line 1
    new-instance v7, Landroid/view/animation/RotateAnimation;

    .line 2
    .line 3
    const/high16 v1, -0x3ccc0000    # -180.0f

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x1

    .line 7
    const/high16 v4, 0x3f000000    # 0.5f

    .line 8
    .line 9
    const/4 v5, 0x1

    .line 10
    const/high16 v6, 0x3f000000    # 0.5f

    .line 11
    .line 12
    move-object v0, v7

    .line 13
    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 14
    .line 15
    .line 16
    iput-object v7, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->OO0o〇〇:Landroid/view/animation/RotateAnimation;

    .line 17
    .line 18
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    .line 19
    .line 20
    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v7, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->OO0o〇〇:Landroid/view/animation/RotateAnimation;

    .line 27
    .line 28
    const-wide/16 v1, 0xc8

    .line 29
    .line 30
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->OO0o〇〇:Landroid/view/animation/RotateAnimation;

    .line 34
    .line 35
    const/4 v1, 0x1

    .line 36
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private o〇O8〇〇o(Z)V
    .locals 8

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇o〇:Landroid/view/View;

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇〇888:Landroid/widget/TextView;

    .line 12
    .line 13
    const v1, 0x7f13064b

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇0〇O0088o()V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->OoO8()V

    .line 23
    .line 24
    .line 25
    iget-object v2, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇〇808〇:Ljava/util/Timer;

    .line 26
    .line 27
    iget-object v3, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇0〇O0088o:Ljava/util/TimerTask;

    .line 28
    .line 29
    iget-wide v6, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇O〇:J

    .line 30
    .line 31
    move-wide v4, v6

    .line 32
    invoke-virtual/range {v2 .. v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_0
    if-nez p1, :cond_1

    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇0〇O0088o()V

    .line 39
    .line 40
    .line 41
    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 42
    const/4 v1, 0x4

    .line 43
    if-eqz p1, :cond_2

    .line 44
    .line 45
    iget-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇o〇:Landroid/view/View;

    .line 46
    .line 47
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 48
    .line 49
    .line 50
    iget-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->O8:Landroid/view/View;

    .line 51
    .line 52
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 53
    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇o〇:Landroid/view/View;

    .line 57
    .line 58
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 59
    .line 60
    .line 61
    iget-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->O8:Landroid/view/View;

    .line 62
    .line 63
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 64
    .line 65
    .line 66
    :goto_1
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private 〇0〇O0088o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇0〇O0088o:Ljava/util/TimerTask;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇0〇O0088o:Ljava/util/TimerTask;

    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇O00(Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇oo〇()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇O〇(Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇〇888:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇oo〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇〇8O0〇8:Ljava/lang/ref/WeakReference;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    return v0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇〇808〇(Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;)Landroid/os/Handler;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇O00:Landroid/os/Handler;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public O8(F)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇oo〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->o〇0:Landroid/widget/TextView;

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    new-array v2, v1, [Ljava/lang/Object;

    .line 12
    .line 13
    const/4 v3, 0x0

    .line 14
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    aput-object p1, v2, v3

    .line 19
    .line 20
    const-string p1, "%.2f%%"

    .line 21
    .line 22
    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 27
    .line 28
    .line 29
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->o〇O8〇〇o(Z)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public OO0o〇〇(Z)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public OO0o〇〇〇〇0(Landroid/view/View;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇oo〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const/4 v0, 0x1

    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->o〇O8〇〇o(Z)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇80〇808〇O:Landroid/widget/ImageView;

    .line 13
    .line 14
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇8o8o〇:Lcom/intsig/camscanner/view/OnHeaderRefreshListener;

    .line 18
    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/view/OnHeaderRefreshListener;->O8(Landroid/view/View;)V

    .line 22
    .line 23
    .line 24
    :cond_1
    return-void
.end method

.method public Oo08()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇oo〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇8o8o〇:Lcom/intsig/camscanner/view/OnHeaderRefreshListener;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-interface {v0}, Lcom/intsig/camscanner/view/OnHeaderRefreshListener;->〇o〇()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->oO80:Landroid/widget/TextView;

    .line 20
    .line 21
    const v1, 0x7f130648

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 25
    .line 26
    .line 27
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->o〇O8〇〇o(Z)V

    .line 29
    .line 30
    .line 31
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇80〇808〇O:Landroid/widget/ImageView;

    .line 32
    .line 33
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-nez v0, :cond_2

    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇80〇808〇O:Landroid/widget/ImageView;

    .line 40
    .line 41
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇80〇808〇O:Landroid/widget/ImageView;

    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇O8o08O:Landroid/view/animation/RotateAnimation;

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 49
    .line 50
    .line 51
    :cond_2
    const/4 v0, 0x1

    .line 52
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->OO0o〇〇〇〇0:Z

    .line 53
    .line 54
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public Oooo8o0〇()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇o00〇〇Oo:Landroid/content/Context;

    .line 2
    .line 3
    const/16 v1, 0x46

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO80()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇oo〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->o〇O8〇〇o(Z)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇80〇808〇O:Landroid/widget/ImageView;

    .line 13
    .line 14
    const v1, 0x7f08095c

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->oO80:Landroid/widget/TextView;

    .line 21
    .line 22
    const v1, 0x7f131e51

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public onDestroy()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇0〇O0088o()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇O00:Landroid/os/Handler;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->OO0o〇〇〇〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇080()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇8o8o〇:Lcom/intsig/camscanner/view/OnHeaderRefreshListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/view/OnHeaderRefreshListener;->Oo08()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇80〇808〇O()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇oo〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->o〇O8〇〇o(Z)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇80〇808〇O:Landroid/widget/ImageView;

    .line 13
    .line 14
    const v1, 0x7f08095c

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇8o8o〇:Lcom/intsig/camscanner/view/OnHeaderRefreshListener;

    .line 21
    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    invoke-interface {v0}, Lcom/intsig/camscanner/view/OnHeaderRefreshListener;->〇o〇()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->oO80:Landroid/widget/TextView;

    .line 32
    .line 33
    const v1, 0x7f131e51

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 37
    .line 38
    .line 39
    :goto_0
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇8o8o〇(Lcom/intsig/camscanner/view/OnHeaderRefreshListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇8o8o〇:Lcom/intsig/camscanner/view/OnHeaderRefreshListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇O888o0o(Landroid/view/ViewGroup;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇o00〇〇Oo:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const v1, 0x7f0d0744

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    iput-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->Oooo8o0〇:Landroid/view/View;

    .line 16
    .line 17
    const v0, 0x7f0a0cf7

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    iput-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇o〇:Landroid/view/View;

    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->Oooo8o0〇:Landroid/view/View;

    .line 27
    .line 28
    const v0, 0x7f0a0caa

    .line 29
    .line 30
    .line 31
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    iput-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->O8:Landroid/view/View;

    .line 36
    .line 37
    iget-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->Oooo8o0〇:Landroid/view/View;

    .line 38
    .line 39
    const v0, 0x7f0a0e60

    .line 40
    .line 41
    .line 42
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    check-cast p1, Landroid/widget/ProgressBar;

    .line 47
    .line 48
    iput-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->Oo08:Landroid/widget/ProgressBar;

    .line 49
    .line 50
    iget-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->Oooo8o0〇:Landroid/view/View;

    .line 51
    .line 52
    const v0, 0x7f0a16dd

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    check-cast p1, Landroid/widget/TextView;

    .line 60
    .line 61
    iput-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->o〇0:Landroid/widget/TextView;

    .line 62
    .line 63
    iget-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->Oooo8o0〇:Landroid/view/View;

    .line 64
    .line 65
    const v0, 0x7f0a1827

    .line 66
    .line 67
    .line 68
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    check-cast p1, Landroid/widget/TextView;

    .line 73
    .line 74
    iput-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇〇888:Landroid/widget/TextView;

    .line 75
    .line 76
    iget-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->Oooo8o0〇:Landroid/view/View;

    .line 77
    .line 78
    const v0, 0x7f0a16e8

    .line 79
    .line 80
    .line 81
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    check-cast p1, Landroid/widget/TextView;

    .line 86
    .line 87
    iput-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->oO80:Landroid/widget/TextView;

    .line 88
    .line 89
    iget-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->Oooo8o0〇:Landroid/view/View;

    .line 90
    .line 91
    const v0, 0x7f0a0ea2

    .line 92
    .line 93
    .line 94
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    check-cast p1, Landroid/widget/ImageView;

    .line 99
    .line 100
    iput-object p1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇80〇808〇O:Landroid/widget/ImageView;

    .line 101
    .line 102
    return-void
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public 〇O8o08O()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇8o8o〇:Lcom/intsig/camscanner/view/OnHeaderRefreshListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/view/OnHeaderRefreshListener;->〇o00〇〇Oo(I)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇o〇()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇oo〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->OO0o〇〇〇〇0:Z

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    iput-boolean v1, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->OO0o〇〇〇〇0:Z

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇80〇808〇O:Landroid/widget/ImageView;

    .line 16
    .line 17
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇80〇808〇O:Landroid/widget/ImageView;

    .line 24
    .line 25
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇80〇808〇O:Landroid/widget/ImageView;

    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->OO0o〇〇:Landroid/view/animation/RotateAnimation;

    .line 31
    .line 32
    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 33
    .line 34
    .line 35
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇8o8o〇:Lcom/intsig/camscanner/view/OnHeaderRefreshListener;

    .line 36
    .line 37
    if-eqz v0, :cond_2

    .line 38
    .line 39
    invoke-interface {v0}, Lcom/intsig/camscanner/view/OnHeaderRefreshListener;->〇o〇()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_2

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->oO80:Landroid/widget/TextView;

    .line 47
    .line 48
    const v2, 0x7f130645

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 52
    .line 53
    .line 54
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->o〇O8〇〇o(Z)V

    .line 55
    .line 56
    .line 57
    :goto_0
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇〇888()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->Oooo8o0〇:Landroid/view/View;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇8O0〇8(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->o〇0:Landroid/widget/TextView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇〇888:Landroid/widget/TextView;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->oO80:Landroid/widget/TextView;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 14
    .line 15
    .line 16
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->〇80〇808〇O:Landroid/widget/ImageView;

    .line 21
    .line 22
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;->Oo08:Landroid/widget/ProgressBar;

    .line 26
    .line 27
    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setIndeterminateTintList(Landroid/content/res/ColorStateList;)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

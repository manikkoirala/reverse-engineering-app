.class public final Lcom/intsig/camscanner/view/CaptureGuideMaskView;
.super Landroid/view/View;
.source "CaptureGuideMaskView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/CaptureGuideMaskView$AnimatorUpdateListener;,
        Lcom/intsig/camscanner/view/CaptureGuideMaskView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final oo8ooo8O:Lcom/intsig/camscanner/view/CaptureGuideMaskView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O0O:Lcom/intsig/Interpolator/EaseCubicInterpolator;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final O88O:Landroid/graphics/PointF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final O8o08O8O:Landroid/graphics/Path;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO〇00〇8oO:J

.field private o0:Landroid/graphics/PointF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8o:Lcom/intsig/camscanner/view/CaptureGuideMaskView$AnimatorUpdateListener;

.field private final o8oOOo:Lcom/intsig/Interpolator/EaseCubicInterpolator;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8〇OO0〇0o:Z

.field private oOO〇〇:F

.field private oOo0:J

.field private oOo〇8o008:I

.field private ooo0〇〇O:Z

.field private final o〇00O:Landroid/graphics/RectF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇080OO8〇0:F

.field private 〇08O〇00〇o:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:I

.field private final 〇8〇oO〇〇8o:Landroid/graphics/Matrix;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:F

.field private 〇O〇〇O8:I

.field private 〇o0O:I

.field private 〇〇08O:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/view/CaptureGuideMaskView$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/view/CaptureGuideMaskView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->oo8ooo8O:Lcom/intsig/camscanner/view/CaptureGuideMaskView$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "attributeSet"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 12
    .line 13
    .line 14
    new-instance p1, Landroid/graphics/PointF;

    .line 15
    .line 16
    const/4 p2, 0x0

    .line 17
    invoke-direct {p1, p2, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 18
    .line 19
    .line 20
    iput-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o0:Landroid/graphics/PointF;

    .line 21
    .line 22
    new-instance p1, Landroid/graphics/Paint;

    .line 23
    .line 24
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 25
    .line 26
    .line 27
    iput-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->OO:Landroid/graphics/Paint;

    .line 28
    .line 29
    new-instance p1, Landroid/graphics/Paint;

    .line 30
    .line 31
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 32
    .line 33
    .line 34
    iput-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 35
    .line 36
    new-instance p1, Landroid/graphics/RectF;

    .line 37
    .line 38
    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    .line 39
    .line 40
    .line 41
    iput-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o〇00O:Landroid/graphics/RectF;

    .line 42
    .line 43
    new-instance p1, Landroid/graphics/Path;

    .line 44
    .line 45
    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    .line 46
    .line 47
    .line 48
    iput-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->O8o08O8O:Landroid/graphics/Path;

    .line 49
    .line 50
    const/high16 p1, 0x40400000    # 3.0f

    .line 51
    .line 52
    iput p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇080OO8〇0:F

    .line 53
    .line 54
    new-instance p1, Landroid/graphics/Matrix;

    .line 55
    .line 56
    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    .line 57
    .line 58
    .line 59
    iput-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇8〇oO〇〇8o:Landroid/graphics/Matrix;

    .line 60
    .line 61
    new-instance p1, Lcom/intsig/Interpolator/EaseCubicInterpolator;

    .line 62
    .line 63
    const v0, 0x3f266666    # 0.65f

    .line 64
    .line 65
    .line 66
    const v1, 0x3eb33333    # 0.35f

    .line 67
    .line 68
    .line 69
    const/high16 v2, 0x3f800000    # 1.0f

    .line 70
    .line 71
    invoke-direct {p1, v0, p2, v1, v2}, Lcom/intsig/Interpolator/EaseCubicInterpolator;-><init>(FFFF)V

    .line 72
    .line 73
    .line 74
    iput-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->O0O:Lcom/intsig/Interpolator/EaseCubicInterpolator;

    .line 75
    .line 76
    new-instance p1, Lcom/intsig/Interpolator/EaseCubicInterpolator;

    .line 77
    .line 78
    invoke-direct {p1, v0, p2, v1, v2}, Lcom/intsig/Interpolator/EaseCubicInterpolator;-><init>(FFFF)V

    .line 79
    .line 80
    .line 81
    iput-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o8oOOo:Lcom/intsig/Interpolator/EaseCubicInterpolator;

    .line 82
    .line 83
    iget-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->OO:Landroid/graphics/Paint;

    .line 84
    .line 85
    const/4 p2, 0x1

    .line 86
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 87
    .line 88
    .line 89
    iget-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->OO:Landroid/graphics/Paint;

    .line 90
    .line 91
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 92
    .line 93
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 94
    .line 95
    .line 96
    iget-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 97
    .line 98
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 99
    .line 100
    .line 101
    iget-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 102
    .line 103
    sget-object p2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 104
    .line 105
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 106
    .line 107
    .line 108
    new-instance p1, Landroid/graphics/PointF;

    .line 109
    .line 110
    invoke-direct {p1}, Landroid/graphics/PointF;-><init>()V

    .line 111
    .line 112
    .line 113
    iput-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->O88O:Landroid/graphics/PointF;

    .line 114
    .line 115
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private final O8()V
    .locals 5

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇O〇〇O8:I

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-ne v0, v1, :cond_1

    .line 8
    .line 9
    iget v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇o0O:I

    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-ne v0, v1, :cond_1

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->O88O:Landroid/graphics/PointF;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o0:Landroid/graphics/PointF;

    .line 20
    .line 21
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    iget v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇OOo8〇0:F

    .line 28
    .line 29
    iget v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->oOO〇〇:F

    .line 30
    .line 31
    cmpg-float v0, v0, v1

    .line 32
    .line 33
    if-nez v0, :cond_0

    .line 34
    .line 35
    const/4 v0, 0x1

    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const/4 v0, 0x0

    .line 38
    :goto_0
    if-eqz v0, :cond_1

    .line 39
    .line 40
    return-void

    .line 41
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    iput v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇O〇〇O8:I

    .line 46
    .line 47
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    iput v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇o0O:I

    .line 52
    .line 53
    iget-object v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->O88O:Landroid/graphics/PointF;

    .line 54
    .line 55
    iget-object v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o0:Landroid/graphics/PointF;

    .line 56
    .line 57
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 58
    .line 59
    iput v2, v0, Landroid/graphics/PointF;->x:F

    .line 60
    .line 61
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 62
    .line 63
    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 64
    .line 65
    iget v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇OOo8〇0:F

    .line 66
    .line 67
    iput v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->oOO〇〇:F

    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o〇00O:Landroid/graphics/RectF;

    .line 70
    .line 71
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 72
    .line 73
    .line 74
    move-result v1

    .line 75
    int-to-float v1, v1

    .line 76
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 77
    .line 78
    .line 79
    move-result v2

    .line 80
    int-to-float v2, v2

    .line 81
    const/4 v3, 0x0

    .line 82
    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 83
    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->O8o08O8O:Landroid/graphics/Path;

    .line 86
    .line 87
    iget-object v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o〇00O:Landroid/graphics/RectF;

    .line 88
    .line 89
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 90
    .line 91
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 92
    .line 93
    .line 94
    iget-object v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->O8o08O8O:Landroid/graphics/Path;

    .line 95
    .line 96
    iget-object v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o0:Landroid/graphics/PointF;

    .line 97
    .line 98
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 99
    .line 100
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 101
    .line 102
    iget v3, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇OOo8〇0:F

    .line 103
    .line 104
    sget-object v4, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 105
    .line 106
    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 107
    .line 108
    .line 109
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private final Oo08()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    iget v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇080OO8〇0:F

    .line 7
    .line 8
    mul-float v0, v0, v1

    .line 9
    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    int-to-float v1, v1

    .line 15
    iget v2, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇080OO8〇0:F

    .line 16
    .line 17
    mul-float v1, v1, v2

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇8〇oO〇〇8o:Landroid/graphics/Matrix;

    .line 20
    .line 21
    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 22
    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇8〇oO〇〇8o:Landroid/graphics/Matrix;

    .line 25
    .line 26
    iget v3, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇080OO8〇0:F

    .line 27
    .line 28
    invoke-virtual {v2, v3, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 29
    .line 30
    .line 31
    iget v2, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇〇08O:I

    .line 32
    .line 33
    const/4 v3, 0x1

    .line 34
    if-ne v2, v3, :cond_0

    .line 35
    .line 36
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    int-to-float v2, v2

    .line 41
    sub-float/2addr v2, v0

    .line 42
    const/high16 v0, 0x40000000    # 2.0f

    .line 43
    .line 44
    div-float/2addr v2, v0

    .line 45
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    int-to-float v0, v0

    .line 50
    sub-float/2addr v0, v1

    .line 51
    iget-object v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇8〇oO〇〇8o:Landroid/graphics/Matrix;

    .line 52
    .line 53
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 58
    .line 59
    .line 60
    move-result v2

    .line 61
    int-to-float v2, v2

    .line 62
    sub-float/2addr v2, v0

    .line 63
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    int-to-float v0, v0

    .line 68
    sub-float/2addr v0, v1

    .line 69
    iget-object v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇8〇oO〇〇8o:Landroid/graphics/Matrix;

    .line 70
    .line 71
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 72
    .line 73
    .line 74
    :goto_0
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static synthetic getScaleReference$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇080(II)I
    .locals 1

    .line 1
    const v0, 0xffffff

    .line 2
    .line 3
    .line 4
    and-int/2addr p2, v0

    .line 5
    shl-int/lit8 p1, p1, 0x18

    .line 6
    .line 7
    or-int/2addr p1, p2

    .line 8
    return p1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final 〇o〇()V
    .locals 11

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->oOo0:J

    .line 2
    .line 3
    const-wide/16 v2, 0x320

    .line 4
    .line 5
    add-long/2addr v0, v2

    .line 6
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    .line 7
    .line 8
    .line 9
    move-result-wide v4

    .line 10
    const/16 v6, 0xff

    .line 11
    .line 12
    const/16 v7, 0xf2

    .line 13
    .line 14
    const/high16 v8, 0x3f800000    # 1.0f

    .line 15
    .line 16
    cmp-long v9, v4, v0

    .line 17
    .line 18
    if-lez v9, :cond_1

    .line 19
    .line 20
    iput v8, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇080OO8〇0:F

    .line 21
    .line 22
    iput v7, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇0O:I

    .line 23
    .line 24
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->ooo0〇〇O:Z

    .line 25
    .line 26
    if-nez v0, :cond_0

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o8o:Lcom/intsig/camscanner/view/CaptureGuideMaskView$AnimatorUpdateListener;

    .line 29
    .line 30
    if-eqz v0, :cond_0

    .line 31
    .line 32
    invoke-interface {v0, v8}, Lcom/intsig/camscanner/view/CaptureGuideMaskView$AnimatorUpdateListener;->onProgress(F)V

    .line 33
    .line 34
    .line 35
    :cond_0
    const/4 v0, 0x1

    .line 36
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->ooo0〇〇O:Z

    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->O0O:Lcom/intsig/Interpolator/EaseCubicInterpolator;

    .line 40
    .line 41
    iget-wide v9, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->oOo0:J

    .line 42
    .line 43
    sub-long v9, v4, v9

    .line 44
    .line 45
    long-to-float v1, v9

    .line 46
    mul-float v1, v1, v8

    .line 47
    .line 48
    long-to-float v9, v2

    .line 49
    div-float/2addr v1, v9

    .line 50
    invoke-virtual {v0, v1}, Lcom/intsig/Interpolator/EaseCubicInterpolator;->getInterpolation(F)F

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    const/high16 v1, 0x40000000    # 2.0f

    .line 55
    .line 56
    mul-float v1, v1, v0

    .line 57
    .line 58
    const/high16 v9, 0x40400000    # 3.0f

    .line 59
    .line 60
    sub-float/2addr v9, v1

    .line 61
    iput v9, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇080OO8〇0:F

    .line 62
    .line 63
    cmpg-float v1, v9, v8

    .line 64
    .line 65
    if-gez v1, :cond_2

    .line 66
    .line 67
    iput v8, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇080OO8〇0:F

    .line 68
    .line 69
    :cond_2
    const v1, 0x3f733333    # 0.95f

    .line 70
    .line 71
    .line 72
    mul-float v9, v0, v1

    .line 73
    .line 74
    cmpl-float v10, v9, v1

    .line 75
    .line 76
    if-lez v10, :cond_3

    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_3
    move v1, v9

    .line 80
    :goto_0
    int-to-float v9, v6

    .line 81
    mul-float v1, v1, v9

    .line 82
    .line 83
    float-to-int v1, v1

    .line 84
    iput v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇0O:I

    .line 85
    .line 86
    cmpl-float v1, v0, v8

    .line 87
    .line 88
    if-lez v1, :cond_4

    .line 89
    .line 90
    iget-object v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o8o:Lcom/intsig/camscanner/view/CaptureGuideMaskView$AnimatorUpdateListener;

    .line 91
    .line 92
    if-eqz v0, :cond_5

    .line 93
    .line 94
    invoke-interface {v0, v8}, Lcom/intsig/camscanner/view/CaptureGuideMaskView$AnimatorUpdateListener;->onProgress(F)V

    .line 95
    .line 96
    .line 97
    goto :goto_1

    .line 98
    :cond_4
    iget-object v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o8o:Lcom/intsig/camscanner/view/CaptureGuideMaskView$AnimatorUpdateListener;

    .line 99
    .line 100
    if-eqz v1, :cond_5

    .line 101
    .line 102
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/view/CaptureGuideMaskView$AnimatorUpdateListener;->onProgress(F)V

    .line 103
    .line 104
    .line 105
    :cond_5
    :goto_1
    iget-wide v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->OO〇00〇8oO:J

    .line 106
    .line 107
    cmp-long v9, v4, v0

    .line 108
    .line 109
    if-gez v9, :cond_6

    .line 110
    .line 111
    const/4 v7, 0x0

    .line 112
    goto :goto_2

    .line 113
    :cond_6
    const-wide/16 v9, 0x258

    .line 114
    .line 115
    add-long/2addr v0, v9

    .line 116
    cmp-long v9, v4, v0

    .line 117
    .line 118
    if-lez v9, :cond_7

    .line 119
    .line 120
    goto :goto_2

    .line 121
    :cond_7
    iget-object v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o8oOOo:Lcom/intsig/Interpolator/EaseCubicInterpolator;

    .line 122
    .line 123
    iget-wide v9, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->oOo0:J

    .line 124
    .line 125
    sub-long/2addr v4, v9

    .line 126
    long-to-float v1, v4

    .line 127
    mul-float v1, v1, v8

    .line 128
    .line 129
    long-to-float v2, v2

    .line 130
    div-float/2addr v1, v2

    .line 131
    invoke-virtual {v0, v1}, Lcom/intsig/Interpolator/EaseCubicInterpolator;->getInterpolation(F)F

    .line 132
    .line 133
    .line 134
    move-result v0

    .line 135
    int-to-float v1, v6

    .line 136
    mul-float v1, v1, v0

    .line 137
    .line 138
    float-to-int v7, v1

    .line 139
    :goto_2
    iput v7, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->oOo〇8o008:I

    .line 140
    .line 141
    return-void
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method


# virtual methods
.method public final getBackgroundPaint()Landroid/graphics/Paint;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->OO:Landroid/graphics/Paint;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getCenterPoint()Landroid/graphics/PointF;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o0:Landroid/graphics/PointF;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getCycleBackgroundPaint()Landroid/graphics/Paint;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getEnableAnimation()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o8〇OO0〇0o:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getEndListener()Lcom/intsig/camscanner/view/CaptureGuideMaskView$AnimatorUpdateListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o8o:Lcom/intsig/camscanner/view/CaptureGuideMaskView$AnimatorUpdateListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getRadius()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇OOo8〇0:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getScaleReference()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇〇08O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    iget-boolean v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o8〇OO0〇0o:Z

    .line 5
    .line 6
    if-eqz v1, :cond_7

    .line 7
    .line 8
    iget v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇OOo8〇0:F

    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    int-to-float v2, v2

    .line 12
    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-lez v1, :cond_7

    .line 17
    .line 18
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-lez v1, :cond_7

    .line 23
    .line 24
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-gtz v1, :cond_0

    .line 29
    .line 30
    goto/16 :goto_0

    .line 31
    .line 32
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇o〇()V

    .line 33
    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->OO:Landroid/graphics/Paint;

    .line 36
    .line 37
    iget v2, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇0O:I

    .line 38
    .line 39
    const v3, 0x2caea0

    .line 40
    .line 41
    .line 42
    invoke-direct {p0, v2, v3}, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇080(II)I

    .line 43
    .line 44
    .line 45
    move-result v2

    .line 46
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 47
    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 50
    .line 51
    iget v2, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->oOo〇8o008:I

    .line 52
    .line 53
    const v3, 0xffffff

    .line 54
    .line 55
    .line 56
    invoke-direct {p0, v2, v3}, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇080(II)I

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 61
    .line 62
    .line 63
    invoke-direct {p0}, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->O8()V

    .line 64
    .line 65
    .line 66
    invoke-direct {p0}, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->Oo08()V

    .line 67
    .line 68
    .line 69
    if-eqz p1, :cond_1

    .line 70
    .line 71
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 72
    .line 73
    .line 74
    :cond_1
    if-eqz p1, :cond_2

    .line 75
    .line 76
    iget-object v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇8〇oO〇〇8o:Landroid/graphics/Matrix;

    .line 77
    .line 78
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 79
    .line 80
    .line 81
    :cond_2
    if-eqz p1, :cond_3

    .line 82
    .line 83
    iget-object v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->O8o08O8O:Landroid/graphics/Path;

    .line 84
    .line 85
    iget-object v2, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->OO:Landroid/graphics/Paint;

    .line 86
    .line 87
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 88
    .line 89
    .line 90
    :cond_3
    if-eqz p1, :cond_4

    .line 91
    .line 92
    iget-object v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o0:Landroid/graphics/PointF;

    .line 93
    .line 94
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 95
    .line 96
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 97
    .line 98
    iget v3, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇OOo8〇0:F

    .line 99
    .line 100
    iget-object v4, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 101
    .line 102
    invoke-virtual {p1, v2, v1, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 103
    .line 104
    .line 105
    :cond_4
    if-eqz p1, :cond_5

    .line 106
    .line 107
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 108
    .line 109
    .line 110
    :cond_5
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->ooo0〇〇O:Z

    .line 111
    .line 112
    if-nez v0, :cond_7

    .line 113
    .line 114
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 115
    .line 116
    .line 117
    move-result-wide v0

    .line 118
    iget-wide v2, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->oOo0:J

    .line 119
    .line 120
    sub-long/2addr v0, v2

    .line 121
    const-wide/16 v2, 0x8

    .line 122
    .line 123
    cmp-long v4, v0, v2

    .line 124
    .line 125
    if-gez v4, :cond_6

    .line 126
    .line 127
    sub-long v1, v2, v0

    .line 128
    .line 129
    const/4 v3, 0x0

    .line 130
    const/4 v4, 0x0

    .line 131
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 132
    .line 133
    .line 134
    move-result v5

    .line 135
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 136
    .line 137
    .line 138
    move-result v6

    .line 139
    move-object v0, p0

    .line 140
    invoke-virtual/range {v0 .. v6}, Landroid/view/View;->postInvalidateDelayed(JIIII)V

    .line 141
    .line 142
    .line 143
    goto :goto_0

    .line 144
    :cond_6
    const/4 v3, 0x0

    .line 145
    const/4 v4, 0x0

    .line 146
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 147
    .line 148
    .line 149
    move-result v5

    .line 150
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 151
    .line 152
    .line 153
    move-result v6

    .line 154
    const-wide/16 v1, 0x8

    .line 155
    .line 156
    move-object v0, p0

    .line 157
    invoke-virtual/range {v0 .. v6}, Landroid/view/View;->postInvalidateDelayed(JIIII)V

    .line 158
    .line 159
    .line 160
    :cond_7
    :goto_0
    return-void
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public final setBackgroundPaint(Landroid/graphics/Paint;)V
    .locals 1
    .param p1    # Landroid/graphics/Paint;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->OO:Landroid/graphics/Paint;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setCenterPoint(Landroid/graphics/PointF;)V
    .locals 1
    .param p1    # Landroid/graphics/PointF;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o0:Landroid/graphics/PointF;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setCycleBackgroundPaint(Landroid/graphics/Paint;)V
    .locals 1
    .param p1    # Landroid/graphics/Paint;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setEnableAnimation(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o8〇OO0〇0o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setEndListener(Lcom/intsig/camscanner/view/CaptureGuideMaskView$AnimatorUpdateListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o8o:Lcom/intsig/camscanner/view/CaptureGuideMaskView$AnimatorUpdateListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setRadius(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇OOo8〇0:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setScaleReference(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇〇08O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇o00〇〇Oo()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->O0O:Lcom/intsig/Interpolator/EaseCubicInterpolator;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/Interpolator/EaseCubicInterpolator;->〇o00〇〇Oo()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o8oOOo:Lcom/intsig/Interpolator/EaseCubicInterpolator;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/Interpolator/EaseCubicInterpolator;->〇o00〇〇Oo()V

    .line 9
    .line 10
    .line 11
    const/high16 v0, 0x40400000    # 3.0f

    .line 12
    .line 13
    iput v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇080OO8〇0:F

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    iput v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->〇0O:I

    .line 17
    .line 18
    iput v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->oOo〇8o008:I

    .line 19
    .line 20
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    .line 21
    .line 22
    .line 23
    move-result-wide v1

    .line 24
    iput-wide v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->oOo0:J

    .line 25
    .line 26
    const-wide/16 v3, 0xc8

    .line 27
    .line 28
    add-long/2addr v1, v3

    .line 29
    iput-wide v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->OO〇00〇8oO:J

    .line 30
    .line 31
    const/4 v1, 0x1

    .line 32
    iput-boolean v1, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->o8〇OO0〇0o:Z

    .line 33
    .line 34
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/CaptureGuideMaskView;->ooo0〇〇O:Z

    .line 35
    .line 36
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

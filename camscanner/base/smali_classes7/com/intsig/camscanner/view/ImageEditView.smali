.class public Lcom/intsig/camscanner/view/ImageEditView;
.super Lcom/intsig/camscanner/view/ImageViewTouchBase;
.source "ImageEditView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;
    }
.end annotation


# static fields
.field private static final 〇8〇80o:I

.field private static 〇o〇88〇8:Landroid/graphics/Paint;


# instance fields
.field private O0〇0:Z

.field private O88O:Lcom/intsig/camscanner/view/HightlightRegion;

.field private O8o〇O0:Z

.field private O8〇o〇88:Z

.field private OO〇OOo:Landroid/view/ViewGroup;

.field Oo0O0o8:Landroid/graphics/RectF;

.field private Oo0〇Ooo:Z

.field private Oo80:I

.field private Ooo08:[F

.field private O〇08oOOO0:Landroid/graphics/Paint;

.field private O〇O:Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;

.field private O〇o88o08〇:Z

.field o00〇88〇08:I

.field private o0OoOOo0:[I

.field private o880:Landroid/graphics/Matrix;

.field private o8O:[F

.field private o8o:Landroid/graphics/Bitmap;

.field private o8〇OO:I

.field private oO00〇o:Z

.field private oOO0880O:I

.field private oOO8:Z

.field private oOO〇〇:I

.field oOoo80oO:Landroid/graphics/Rect;

.field private oOo〇08〇:F

.field private oO〇8O8oOo:Z

.field private oO〇oo:Z

.field private oo8ooo8O:Landroid/graphics/Bitmap;

.field private ooO:[I

.field private oooO888:Landroid/graphics/RectF;

.field private o〇0〇o:[I

.field private o〇oO:Z

.field private o〇o〇Oo88:Z

.field private 〇00O0:F

.field private 〇088O:Z

.field private 〇08〇o0O:Z

.field private 〇0O〇O00O:Z

.field private 〇800OO〇0O:Z

.field private 〇80O8o8O〇:F

.field private 〇8〇OOoooo:[I

.field private 〇8〇o88:I

.field private 〇O8oOo0:F

.field private 〇OO8ooO8〇:F

.field private 〇OO〇00〇0O:I

.field private 〇oo〇O〇80:Z

.field private 〇〇o0〇8:I

.field private 〇〇o〇:I

.field private 〇〇〇0o〇〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 2
    .line 3
    const/16 v1, 0x14

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/Util;->〇0〇O0088o(Landroid/content/Context;I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    sput v0, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇80o:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 33
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 34
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oOO〇〇:I

    .line 35
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇oO:Z

    const/4 v0, 0x1

    .line 36
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇08〇o0O:Z

    .line 37
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇〇o〇:I

    const/4 v1, -0x1

    .line 38
    iput v1, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo80:I

    const/16 v1, 0x14

    .line 39
    iput v1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o8〇OO:I

    .line 40
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo0〇Ooo:Z

    .line 41
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇〇〇0o〇〇0:I

    .line 42
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇8O8oOo:Z

    .line 43
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇0O〇O00O:Z

    .line 44
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇o〇Oo88:Z

    .line 45
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oO00〇o:Z

    .line 46
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oOoo80oO:Landroid/graphics/Rect;

    .line 47
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo0O0o8:Landroid/graphics/RectF;

    .line 48
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇800OO〇0O:Z

    .line 49
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O8o〇O0:Z

    .line 50
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->oOO8:Z

    .line 51
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇oo〇O〇80:Z

    .line 52
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->O8〇o〇88:Z

    const v1, -0xe64364

    .line 53
    iput v1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇o88:I

    .line 54
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇oo:Z

    .line 55
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oooO888:Landroid/graphics/RectF;

    .line 56
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o880:Landroid/graphics/Matrix;

    .line 57
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇〇o0〇8:I

    .line 58
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O0〇0:Z

    const/4 p1, 0x0

    .line 59
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇OOoooo:[I

    .line 60
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇0〇o:[I

    .line 61
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇088O:Z

    .line 62
    invoke-direct {p0}, Lcom/intsig/camscanner/view/ImageEditView;->o〇〇0〇()V

    .line 63
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    if-nez p1, :cond_0

    .line 64
    new-instance p1, Lcom/intsig/camscanner/view/HightlightRegion;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/view/HightlightRegion;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 2
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oOO〇〇:I

    .line 3
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇oO:Z

    const/4 p2, 0x1

    .line 4
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇08〇o0O:Z

    .line 5
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇〇o〇:I

    const/4 v0, -0x1

    .line 6
    iput v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo80:I

    const/16 v0, 0x14

    .line 7
    iput v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->o8〇OO:I

    .line 8
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo0〇Ooo:Z

    .line 9
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇〇〇0o〇〇0:I

    .line 10
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇8O8oOo:Z

    .line 11
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇0O〇O00O:Z

    .line 12
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇o〇Oo88:Z

    .line 13
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oO00〇o:Z

    .line 14
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->oOoo80oO:Landroid/graphics/Rect;

    .line 15
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo0O0o8:Landroid/graphics/RectF;

    .line 16
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇800OO〇0O:Z

    .line 17
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/ImageEditView;->O8o〇O0:Z

    .line 18
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/ImageEditView;->oOO8:Z

    .line 19
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇oo〇O〇80:Z

    .line 20
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->O8〇o〇88:Z

    const v0, -0xe64364

    .line 21
    iput v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇o88:I

    .line 22
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇oo:Z

    .line 23
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->oooO888:Landroid/graphics/RectF;

    .line 24
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->o880:Landroid/graphics/Matrix;

    .line 25
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇〇o0〇8:I

    .line 26
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/ImageEditView;->O0〇0:Z

    const/4 p1, 0x0

    .line 27
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇OOoooo:[I

    .line 28
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇0〇o:[I

    .line 29
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇088O:Z

    .line 30
    invoke-direct {p0}, Lcom/intsig/camscanner/view/ImageEditView;->o〇〇0〇()V

    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    if-nez p1, :cond_0

    .line 32
    new-instance p1, Lcom/intsig/camscanner/view/HightlightRegion;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/view/HightlightRegion;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    :cond_0
    return-void
.end method

.method private static Oo8Oo00oo(III)I
    .locals 0

    .line 1
    invoke-static {p2, p1}, Ljava/lang/Math;->min(II)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    invoke-static {p1, p0}, Ljava/lang/Math;->max(II)I

    .line 6
    .line 7
    .line 8
    move-result p0

    .line 9
    return p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private O〇O〇oO()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇08〇o0O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->o8O:[F

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget v1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇80O8o8O〇:F

    .line 10
    .line 11
    const/4 v2, 0x1

    .line 12
    invoke-virtual {p0, v0, v1, v2}, Lcom/intsig/camscanner/view/ImageEditView;->o8oO〇([FFZ)V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private getDirtyRect()Landroid/graphics/Rect;
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/HightlightRegion;->o〇0()[F

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    aget v2, v0, v1

    .line 9
    .line 10
    const/4 v3, 0x0

    .line 11
    :goto_0
    array-length v4, v0

    .line 12
    if-ge v3, v4, :cond_1

    .line 13
    .line 14
    aget v4, v0, v3

    .line 15
    .line 16
    cmpg-float v5, v4, v2

    .line 17
    .line 18
    if-gez v5, :cond_0

    .line 19
    .line 20
    move v2, v4

    .line 21
    :cond_0
    add-int/lit8 v3, v3, 0x2

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    aget v3, v0, v1

    .line 25
    .line 26
    :goto_1
    array-length v4, v0

    .line 27
    if-ge v1, v4, :cond_3

    .line 28
    .line 29
    aget v4, v0, v1

    .line 30
    .line 31
    cmpl-float v5, v4, v3

    .line 32
    .line 33
    if-lez v5, :cond_2

    .line 34
    .line 35
    move v3, v4

    .line 36
    :cond_2
    add-int/lit8 v1, v1, 0x2

    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_3
    const/4 v1, 0x1

    .line 40
    aget v4, v0, v1

    .line 41
    .line 42
    const/4 v5, 0x1

    .line 43
    :goto_2
    array-length v6, v0

    .line 44
    if-ge v5, v6, :cond_5

    .line 45
    .line 46
    aget v6, v0, v5

    .line 47
    .line 48
    cmpg-float v7, v6, v4

    .line 49
    .line 50
    if-gez v7, :cond_4

    .line 51
    .line 52
    move v4, v6

    .line 53
    :cond_4
    add-int/lit8 v5, v5, 0x2

    .line 54
    .line 55
    goto :goto_2

    .line 56
    :cond_5
    aget v5, v0, v1

    .line 57
    .line 58
    :goto_3
    array-length v6, v0

    .line 59
    if-ge v1, v6, :cond_7

    .line 60
    .line 61
    aget v6, v0, v1

    .line 62
    .line 63
    cmpl-float v7, v6, v5

    .line 64
    .line 65
    if-lez v7, :cond_6

    .line 66
    .line 67
    move v5, v6

    .line 68
    :cond_6
    add-int/lit8 v1, v1, 0x2

    .line 69
    .line 70
    goto :goto_3

    .line 71
    :cond_7
    new-instance v0, Landroid/graphics/Rect;

    .line 72
    .line 73
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getOffset()F

    .line 74
    .line 75
    .line 76
    move-result v1

    .line 77
    sub-float/2addr v2, v1

    .line 78
    float-to-int v1, v2

    .line 79
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getOffset()F

    .line 80
    .line 81
    .line 82
    move-result v2

    .line 83
    sub-float/2addr v4, v2

    .line 84
    float-to-int v2, v4

    .line 85
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getOffset()F

    .line 86
    .line 87
    .line 88
    move-result v4

    .line 89
    add-float/2addr v3, v4

    .line 90
    float-to-int v3, v3

    .line 91
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getOffset()F

    .line 92
    .line 93
    .line 94
    move-result v4

    .line 95
    add-float/2addr v5, v4

    .line 96
    float-to-int v4, v5

    .line 97
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 98
    .line 99
    .line 100
    return-object v0
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private static o0ooO(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, p1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 5
    .line 6
    .line 7
    const/16 p1, 0x80

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-static {p1, v0, v0, v0}, Landroid/graphics/Color;->argb(IIII)I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-virtual {p0, p1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private o8(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇OO〇00〇0O:I

    .line 2
    .line 3
    if-ltz v0, :cond_2

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->ooO:[I

    .line 6
    .line 7
    invoke-static {v0}, Lcom/intsig/utils/PointUtil;->Oo08([I)[F

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 18
    .line 19
    .line 20
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇O〇:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$Companion;

    .line 21
    .line 22
    iget-boolean v2, p0, Lcom/intsig/camscanner/view/ImageEditView;->O〇o88o08〇:Z

    .line 23
    .line 24
    if-nez v2, :cond_1

    .line 25
    .line 26
    const/4 v2, 0x0

    .line 27
    goto :goto_0

    .line 28
    :cond_1
    iget v2, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇OO〇00〇0O:I

    .line 29
    .line 30
    mul-int/lit16 v2, v2, 0x80

    .line 31
    .line 32
    div-int/lit8 v2, v2, 0x64

    .line 33
    .line 34
    :goto_0
    invoke-virtual {v1, p1, v0, v2, p2}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$Companion;->〇o〇(Landroid/graphics/Canvas;[FILandroid/graphics/Rect;)Lkotlin/Unit;

    .line 35
    .line 36
    .line 37
    const/4 p1, 0x0

    .line 38
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->ooO:[I

    .line 39
    .line 40
    const/4 p1, -0x1

    .line 41
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇OO〇00〇0O:I

    .line 42
    .line 43
    :cond_2
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static o〇8(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;IIZI)V
    .locals 6

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/view/ImageEditView;->〇〇0o()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080()Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->isUsingBitmapForEnhanceLine()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_5

    .line 15
    .line 16
    if-eqz p1, :cond_5

    .line 17
    .line 18
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-nez v0, :cond_5

    .line 23
    .line 24
    const/16 p2, 0x64

    .line 25
    .line 26
    const/16 p4, 0x1e

    .line 27
    .line 28
    const/16 v0, 0xff

    .line 29
    .line 30
    if-gt p6, p2, :cond_0

    .line 31
    .line 32
    const/16 v1, 0x46

    .line 33
    .line 34
    if-lt p6, v1, :cond_0

    .line 35
    .line 36
    sub-int/2addr p2, p6

    .line 37
    mul-int/lit16 p2, p2, 0xff

    .line 38
    .line 39
    div-int/2addr p2, p4

    .line 40
    goto :goto_0

    .line 41
    :cond_0
    if-gt p6, p4, :cond_1

    .line 42
    .line 43
    if-ltz p6, :cond_1

    .line 44
    .line 45
    mul-int/lit16 p6, p6, 0xff

    .line 46
    .line 47
    div-int/lit8 p2, p6, 0x1e

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_1
    const/16 p2, 0xff

    .line 51
    .line 52
    :goto_0
    if-ge p2, v0, :cond_2

    .line 53
    .line 54
    sget-object p4, Lcom/intsig/camscanner/view/ImageEditView;->〇o〇88〇8:Landroid/graphics/Paint;

    .line 55
    .line 56
    invoke-virtual {p4, p2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 57
    .line 58
    .line 59
    :cond_2
    if-eqz p5, :cond_3

    .line 60
    .line 61
    sget p4, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇80o:I

    .line 62
    .line 63
    goto :goto_1

    .line 64
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 65
    .line 66
    .line 67
    move-result p4

    .line 68
    sget p5, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇80o:I

    .line 69
    .line 70
    sub-int/2addr p4, p5

    .line 71
    :goto_1
    sub-int/2addr p3, p4

    .line 72
    int-to-float p3, p3

    .line 73
    if-ge p2, v0, :cond_4

    .line 74
    .line 75
    sget-object p2, Lcom/intsig/camscanner/view/ImageEditView;->〇o〇88〇8:Landroid/graphics/Paint;

    .line 76
    .line 77
    goto :goto_2

    .line 78
    :cond_4
    const/4 p2, 0x0

    .line 79
    :goto_2
    const/4 p4, 0x0

    .line 80
    invoke-virtual {p0, p1, p4, p3, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 81
    .line 82
    .line 83
    goto :goto_3

    .line 84
    :cond_5
    if-eqz p2, :cond_6

    .line 85
    .line 86
    const/4 v1, 0x0

    .line 87
    int-to-float v4, p3

    .line 88
    int-to-float v3, p4

    .line 89
    move-object v0, p0

    .line 90
    move v2, v4

    .line 91
    move-object v5, p2

    .line 92
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 93
    .line 94
    .line 95
    :cond_6
    :goto_3
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
.end method

.method private o〇〇0〇()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 10
    .line 11
    iput v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->oOO0880O:I

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇o(Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getOffset()F

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo0O0o8:Landroid/graphics/RectF;

    .line 16
    .line 17
    const/4 v2, 0x0

    .line 18
    sub-float/2addr v2, v0

    .line 19
    iget-object v3, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 20
    .line 21
    invoke-virtual {v3}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    int-to-float v3, v3

    .line 30
    add-float/2addr v3, v0

    .line 31
    iget-object v4, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 32
    .line 33
    invoke-virtual {v4}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 34
    .line 35
    .line 36
    move-result-object v4

    .line 37
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    .line 38
    .line 39
    .line 40
    move-result v4

    .line 41
    int-to-float v4, v4

    .line 42
    add-float/2addr v4, v0

    .line 43
    invoke-virtual {v1, v2, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 47
    .line 48
    .line 49
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo0O0o8:Landroid/graphics/RectF;

    .line 54
    .line 55
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 56
    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo0O0o8:Landroid/graphics/RectF;

    .line 59
    .line 60
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 61
    .line 62
    .line 63
    goto :goto_1

    .line 64
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 65
    .line 66
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    .line 68
    .line 69
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 70
    .line 71
    .line 72
    const-string v1, "onDraw Method Exception! Step2 , drawing border, find mBitmapDisplayed is "

    .line 73
    .line 74
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 78
    .line 79
    if-eqz v1, :cond_1

    .line 80
    .line 81
    const-string v1, "Not"

    .line 82
    .line 83
    goto :goto_0

    .line 84
    :cond_1
    const-string v1, ""

    .line 85
    .line 86
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    const-string v1, " NULL"

    .line 90
    .line 91
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    const-string v1, "ImageEditView"

    .line 99
    .line 100
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 104
    .line 105
    iget v1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇o88:I

    .line 106
    .line 107
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/HightlightRegion;->〇0000OOO(I)V

    .line 108
    .line 109
    .line 110
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 111
    .line 112
    iget-boolean v1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oOO8:Z

    .line 113
    .line 114
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/HightlightRegion;->〇o〇(Z)V

    .line 115
    .line 116
    .line 117
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 118
    .line 119
    if-eqz v0, :cond_2

    .line 120
    .line 121
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    if-eqz v0, :cond_2

    .line 126
    .line 127
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 128
    .line 129
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/HightlightRegion;->〇080(Landroid/graphics/Canvas;)V

    .line 130
    .line 131
    .line 132
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 133
    .line 134
    .line 135
    return-void
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public static 〇〇0o()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/view/ImageEditView;->〇o〇88〇8:Landroid/graphics/Paint;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroid/graphics/Paint;

    .line 6
    .line 7
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/intsig/camscanner/view/ImageEditView;->〇o〇88〇8:Landroid/graphics/Paint;

    .line 11
    .line 12
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public O08000(FLjava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-static {p2}, Lcom/intsig/camscanner/util/Util;->〇08O8o〇0(Ljava/lang/String;)[I

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇0〇o〇O(F[IZ)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public O8ooOoo〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇oo〇O〇80:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public O8〇o()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/HightlightRegion;->〇8o8o〇()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OOO〇O0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇0O〇O00O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O〇8O8〇008(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->O8o〇O0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getBitmapEnhanced()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->o8o:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCropRegion()Lcom/intsig/camscanner/view/HightlightRegion;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCurrentRealSize()[I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->o0OoOOo0:[I

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDisplayedBitmapRect()Landroid/graphics/Rect;
    .locals 5

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v0, Landroid/graphics/RectF;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    int-to-float v1, v1

    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 25
    .line 26
    invoke-virtual {v2}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    int-to-float v2, v2

    .line 35
    const/4 v3, 0x0

    .line 36
    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 44
    .line 45
    .line 46
    new-instance v1, Landroid/graphics/Rect;

    .line 47
    .line 48
    iget v2, v0, Landroid/graphics/RectF;->left:F

    .line 49
    .line 50
    float-to-int v2, v2

    .line 51
    iget v3, v0, Landroid/graphics/RectF;->top:F

    .line 52
    .line 53
    float-to-int v3, v3

    .line 54
    iget v4, v0, Landroid/graphics/RectF;->right:F

    .line 55
    .line 56
    float-to-int v4, v4

    .line 57
    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    .line 58
    .line 59
    float-to-int v0, v0

    .line 60
    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    .line 62
    .line 63
    return-object v1

    .line 64
    :catch_0
    move-exception v0

    .line 65
    const-string v1, "ImageEditView"

    .line 66
    .line 67
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 68
    .line 69
    .line 70
    :cond_0
    const/4 v0, 0x0

    .line 71
    return-object v0
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getLines()[I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇OOoooo:[I

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNLine()[I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇0〇o:[I

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRealDisplayBitmapRect()Landroid/graphics/Rect;
    .locals 8
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageEditView;->o0OoOOo0:[I

    .line 32
    .line 33
    if-eqz v2, :cond_0

    .line 34
    .line 35
    const/4 v3, 0x0

    .line 36
    aget v3, v2, v3

    .line 37
    .line 38
    if-lez v3, :cond_0

    .line 39
    .line 40
    const/4 v4, 0x1

    .line 41
    aget v2, v2, v4

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_0
    move v3, v0

    .line 45
    move v2, v1

    .line 46
    :goto_0
    new-instance v4, Landroid/graphics/RectF;

    .line 47
    .line 48
    sub-int v5, v0, v3

    .line 49
    .line 50
    int-to-float v5, v5

    .line 51
    const/high16 v6, 0x40000000    # 2.0f

    .line 52
    .line 53
    div-float/2addr v5, v6

    .line 54
    sub-int v7, v1, v2

    .line 55
    .line 56
    int-to-float v7, v7

    .line 57
    div-float/2addr v7, v6

    .line 58
    add-int/2addr v0, v3

    .line 59
    int-to-float v0, v0

    .line 60
    div-float/2addr v0, v6

    .line 61
    add-int/2addr v1, v2

    .line 62
    int-to-float v1, v1

    .line 63
    div-float/2addr v1, v6

    .line 64
    invoke-direct {v4, v5, v7, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 72
    .line 73
    .line 74
    new-instance v0, Landroid/graphics/Rect;

    .line 75
    .line 76
    iget v1, v4, Landroid/graphics/RectF;->left:F

    .line 77
    .line 78
    float-to-int v1, v1

    .line 79
    iget v2, v4, Landroid/graphics/RectF;->top:F

    .line 80
    .line 81
    float-to-int v2, v2

    .line 82
    iget v3, v4, Landroid/graphics/RectF;->right:F

    .line 83
    .line 84
    float-to-int v3, v3

    .line 85
    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    .line 86
    .line 87
    float-to-int v4, v4

    .line 88
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    .line 90
    .line 91
    return-object v0

    .line 92
    :catch_0
    move-exception v0

    .line 93
    const-string v1, "ImageEditView"

    .line 94
    .line 95
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 96
    .line 97
    .line 98
    :cond_1
    const/4 v0, 0x0

    .line 99
    return-object v0
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public o8oO〇([FFZ)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/intsig/camscanner/view/ImageEditView;->o〇8oOO88([FFZZ)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public oO(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/view/ImageEditView;->〇0000OOO(Z)[F

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    const/4 v1, 0x1

    .line 7
    invoke-virtual {p0, p1, v1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o800o8O(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    .line 8
    .line 9
    .line 10
    iget p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇00O0:F

    .line 11
    .line 12
    invoke-virtual {p0, v0, p1, p2}, Lcom/intsig/camscanner/view/ImageEditView;->o8oO〇([FFZ)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2
    .line 3
    .line 4
    iget v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->oOO0880O:I

    .line 5
    .line 6
    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    .line 7
    .line 8
    if-eq v0, p1, :cond_0

    .line 9
    .line 10
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oOO0880O:I

    .line 11
    .line 12
    const/4 p1, 0x1

    .line 13
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oO00〇o:Z

    .line 14
    .line 15
    const/4 p1, 0x0

    .line 16
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/ImageEditView;->〇0000OOO(Z)[F

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->Ooo08:[F

    .line 21
    .line 22
    iget p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇00O0:F

    .line 23
    .line 24
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇OO8ooO8〇:F

    .line 25
    .line 26
    new-instance p1, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v0, "before change setRegion:"

    .line 32
    .line 33
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->Ooo08:[F

    .line 37
    .line 38
    invoke-static {v0}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    const-string v0, "ImageEditView"

    .line 50
    .line 51
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    :cond_0
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    .line 1
    :try_start_0
    invoke-super {p0, p1}, Lcom/intsig/view/SafeImageView;->onDraw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageEditView;->getRealDisplayBitmapRect()Landroid/graphics/Rect;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    const/4 v1, 0x0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget v2, v0, Landroid/graphics/Rect;->top:I

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v2, 0x0

    .line 15
    :goto_0
    if-eqz v0, :cond_1

    .line 16
    .line 17
    iget v1, v0, Landroid/graphics/Rect;->left:I

    .line 18
    .line 19
    :cond_1
    if-eqz v0, :cond_2

    .line 20
    .line 21
    iget v3, v0, Landroid/graphics/Rect;->right:I

    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    :goto_1
    if-eqz v0, :cond_3

    .line 29
    .line 30
    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    .line 31
    .line 32
    goto :goto_2

    .line 33
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    :goto_2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/ImageEditView;->〇o(Landroid/graphics/Canvas;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageEditView;->getDisplayedBitmapRect()Landroid/graphics/Rect;

    .line 41
    .line 42
    .line 43
    move-result-object v5

    .line 44
    invoke-direct {p0, p1, v5}, Lcom/intsig/camscanner/view/ImageEditView;->o8(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageEditView;->OOO〇O0()Z

    .line 48
    .line 49
    .line 50
    move-result v5

    .line 51
    if-eqz v5, :cond_4

    .line 52
    .line 53
    invoke-static {p1, v0}, Lcom/intsig/camscanner/view/ImageEditView;->o0ooO(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 54
    .line 55
    .line 56
    :cond_4
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇8O8oOo:Z

    .line 57
    .line 58
    const/4 v5, 0x0

    .line 59
    if-eqz v0, :cond_6

    .line 60
    .line 61
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇o〇Oo88:Z

    .line 62
    .line 63
    if-eqz v0, :cond_5

    .line 64
    .line 65
    iget-object v5, p0, Lcom/intsig/camscanner/view/ImageEditView;->oo8ooo8O:Landroid/graphics/Bitmap;

    .line 66
    .line 67
    :cond_5
    move-object v7, v5

    .line 68
    iget-object v8, p0, Lcom/intsig/camscanner/view/ImageEditView;->O〇08oOOO0:Landroid/graphics/Paint;

    .line 69
    .line 70
    iget v9, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇〇〇0o〇〇0:I

    .line 71
    .line 72
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 73
    .line 74
    .line 75
    move-result v10

    .line 76
    const/4 v11, 0x0

    .line 77
    const/4 v12, -0x1

    .line 78
    move-object v6, p1

    .line 79
    invoke-static/range {v6 .. v12}, Lcom/intsig/camscanner/view/ImageEditView;->o〇8(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;IIZI)V

    .line 80
    .line 81
    .line 82
    return-void

    .line 83
    :cond_6
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->o8o:Landroid/graphics/Bitmap;

    .line 84
    .line 85
    if-eqz v0, :cond_d

    .line 86
    .line 87
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo0〇Ooo:Z

    .line 88
    .line 89
    if-eqz v0, :cond_7

    .line 90
    .line 91
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 92
    .line 93
    .line 94
    move-result v0

    .line 95
    iget v6, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇〇o〇:I

    .line 96
    .line 97
    sub-int/2addr v0, v6

    .line 98
    goto :goto_3

    .line 99
    :cond_7
    iget v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇〇o〇:I

    .line 100
    .line 101
    :goto_3
    move v9, v0

    .line 102
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->oOoo80oO:Landroid/graphics/Rect;

    .line 103
    .line 104
    iget-boolean v6, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo0〇Ooo:Z

    .line 105
    .line 106
    if-eqz v6, :cond_8

    .line 107
    .line 108
    invoke-static {v2, v4, v9}, Lcom/intsig/camscanner/view/ImageEditView;->Oo8Oo00oo(III)I

    .line 109
    .line 110
    .line 111
    move-result v6

    .line 112
    goto :goto_4

    .line 113
    :cond_8
    move v6, v2

    .line 114
    :goto_4
    iget-boolean v7, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo0〇Ooo:Z

    .line 115
    .line 116
    if-eqz v7, :cond_9

    .line 117
    .line 118
    move v7, v4

    .line 119
    goto :goto_5

    .line 120
    :cond_9
    invoke-static {v2, v4, v9}, Lcom/intsig/camscanner/view/ImageEditView;->Oo8Oo00oo(III)I

    .line 121
    .line 122
    .line 123
    move-result v7

    .line 124
    :goto_5
    invoke-virtual {v0, v1, v6, v3, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 125
    .line 126
    .line 127
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 128
    .line 129
    .line 130
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->oOoo80oO:Landroid/graphics/Rect;

    .line 131
    .line 132
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 133
    .line 134
    .line 135
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->o8o:Landroid/graphics/Bitmap;

    .line 136
    .line 137
    invoke-virtual {p0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 138
    .line 139
    .line 140
    move-result-object v6

    .line 141
    invoke-virtual {p1, v0, v6, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 142
    .line 143
    .line 144
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 145
    .line 146
    .line 147
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->oOoo80oO:Landroid/graphics/Rect;

    .line 148
    .line 149
    iget-boolean v6, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo0〇Ooo:Z

    .line 150
    .line 151
    if-eqz v6, :cond_a

    .line 152
    .line 153
    move v6, v2

    .line 154
    goto :goto_6

    .line 155
    :cond_a
    invoke-static {v2, v4, v9}, Lcom/intsig/camscanner/view/ImageEditView;->Oo8Oo00oo(III)I

    .line 156
    .line 157
    .line 158
    move-result v6

    .line 159
    :goto_6
    iget-boolean v7, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo0〇Ooo:Z

    .line 160
    .line 161
    if-eqz v7, :cond_b

    .line 162
    .line 163
    invoke-static {v2, v4, v9}, Lcom/intsig/camscanner/view/ImageEditView;->Oo8Oo00oo(III)I

    .line 164
    .line 165
    .line 166
    move-result v4

    .line 167
    :cond_b
    invoke-virtual {v0, v1, v6, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 168
    .line 169
    .line 170
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->oOoo80oO:Landroid/graphics/Rect;

    .line 171
    .line 172
    invoke-static {p1, v0}, Lcom/intsig/camscanner/view/ImageEditView;->o0ooO(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 173
    .line 174
    .line 175
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo0〇Ooo:Z

    .line 176
    .line 177
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/view/ImageEditView;->〇〇〇0〇〇0(Z)V

    .line 178
    .line 179
    .line 180
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇o〇Oo88:Z

    .line 181
    .line 182
    if-eqz v0, :cond_c

    .line 183
    .line 184
    iget-object v5, p0, Lcom/intsig/camscanner/view/ImageEditView;->oo8ooo8O:Landroid/graphics/Bitmap;

    .line 185
    .line 186
    :cond_c
    move-object v7, v5

    .line 187
    iget-object v8, p0, Lcom/intsig/camscanner/view/ImageEditView;->O〇08oOOO0:Landroid/graphics/Paint;

    .line 188
    .line 189
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 190
    .line 191
    .line 192
    move-result v10

    .line 193
    iget-boolean v11, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo0〇Ooo:Z

    .line 194
    .line 195
    iget v12, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo80:I

    .line 196
    .line 197
    move-object v6, p1

    .line 198
    invoke-static/range {v6 .. v12}, Lcom/intsig/camscanner/view/ImageEditView;->o〇8(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;IIZI)V

    .line 199
    .line 200
    .line 201
    :cond_d
    return-void

    .line 202
    :catch_0
    move-exception p1

    .line 203
    const-string v0, "ImageEditView"

    .line 204
    .line 205
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 206
    .line 207
    .line 208
    return-void
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 1
    invoke-super/range {p0 .. p5}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->onLayout(ZIIII)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_2

    .line 5
    .line 6
    iget-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oO00〇o:Z

    .line 7
    .line 8
    if-eqz p1, :cond_1

    .line 9
    .line 10
    iget-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇08〇o0O:Z

    .line 11
    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->Ooo08:[F

    .line 15
    .line 16
    if-eqz p1, :cond_0

    .line 17
    .line 18
    iget p2, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇OO8ooO8〇:F

    .line 19
    .line 20
    const/4 p3, 0x1

    .line 21
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/camscanner/view/ImageEditView;->o8oO〇([FFZ)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/view/ImageEditView;->O〇O〇oO()V

    .line 26
    .line 27
    .line 28
    :goto_0
    const/4 p1, 0x0

    .line 29
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oO00〇o:Z

    .line 30
    .line 31
    const-string p1, "ImageEditView"

    .line 32
    .line 33
    const-string p2, "onLayout after orientation changed"

    .line 34
    .line 35
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/view/ImageEditView;->O〇O〇oO()V

    .line 40
    .line 41
    .line 42
    :cond_2
    :goto_1
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 21

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget-boolean v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇08〇o0O:Z

    .line 4
    .line 5
    if-nez v1, :cond_0

    .line 6
    .line 7
    invoke-super/range {p0 .. p1}, Landroid/widget/ImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    return v1

    .line 12
    :cond_0
    iget-boolean v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->O8o〇O0:Z

    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    if-nez v1, :cond_1

    .line 16
    .line 17
    return v2

    .line 18
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    .line 23
    .line 24
    .line 25
    move-result v3

    .line 26
    iget-object v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->o880:Landroid/graphics/Matrix;

    .line 27
    .line 28
    invoke-virtual {v4}, Landroid/graphics/Matrix;->reset()V

    .line 29
    .line 30
    .line 31
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 32
    .line 33
    .line 34
    move-result-object v4

    .line 35
    iget-object v5, v0, Lcom/intsig/camscanner/view/ImageEditView;->o880:Landroid/graphics/Matrix;

    .line 36
    .line 37
    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 38
    .line 39
    .line 40
    const/4 v4, 0x2

    .line 41
    new-array v5, v4, [F

    .line 42
    .line 43
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    .line 44
    .line 45
    .line 46
    move-result v6

    .line 47
    const/16 v9, 0x8

    .line 48
    .line 49
    const/4 v10, 0x3

    .line 50
    const/4 v11, 0x1

    .line 51
    if-eqz v6, :cond_26

    .line 52
    .line 53
    if-eq v6, v11, :cond_20

    .line 54
    .line 55
    if-eq v6, v4, :cond_6

    .line 56
    .line 57
    if-eq v6, v10, :cond_2

    .line 58
    .line 59
    goto/16 :goto_c

    .line 60
    .line 61
    :cond_2
    iget-object v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->O〇O:Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;

    .line 62
    .line 63
    if-eqz v1, :cond_4

    .line 64
    .line 65
    iget v3, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOO〇〇:I

    .line 66
    .line 67
    if-ne v3, v11, :cond_3

    .line 68
    .line 69
    invoke-interface {v1}, Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;->〇08O〇00〇o()V

    .line 70
    .line 71
    .line 72
    :cond_3
    iget-object v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->O〇O:Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;

    .line 73
    .line 74
    invoke-interface {v1}, Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;->〇0O〇Oo()V

    .line 75
    .line 76
    .line 77
    :cond_4
    iget-object v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->OO〇OOo:Landroid/view/ViewGroup;

    .line 78
    .line 79
    if-eqz v1, :cond_5

    .line 80
    .line 81
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 82
    .line 83
    .line 84
    :cond_5
    iput-boolean v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->O0〇0:Z

    .line 85
    .line 86
    goto/16 :goto_c

    .line 87
    .line 88
    :cond_6
    iget v6, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOO〇〇:I

    .line 89
    .line 90
    const/4 v12, 0x0

    .line 91
    if-ne v6, v11, :cond_a

    .line 92
    .line 93
    aput v1, v5, v2

    .line 94
    .line 95
    aput v3, v5, v11

    .line 96
    .line 97
    iget-object v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->o880:Landroid/graphics/Matrix;

    .line 98
    .line 99
    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 100
    .line 101
    .line 102
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/view/ImageEditView;->getDirtyRect()Landroid/graphics/Rect;

    .line 103
    .line 104
    .line 105
    move-result-object v4

    .line 106
    iget-object v6, v0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 107
    .line 108
    iget v7, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOo〇08〇:F

    .line 109
    .line 110
    sub-float v7, v1, v7

    .line 111
    .line 112
    iget v8, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇O8oOo0:F

    .line 113
    .line 114
    sub-float v8, v3, v8

    .line 115
    .line 116
    invoke-virtual {v6, v7, v8}, Lcom/intsig/camscanner/view/HightlightRegion;->〇〇8O0〇8(FF)V

    .line 117
    .line 118
    .line 119
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/view/ImageEditView;->getDirtyRect()Landroid/graphics/Rect;

    .line 120
    .line 121
    .line 122
    move-result-object v6

    .line 123
    invoke-virtual {v6, v4}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 124
    .line 125
    .line 126
    invoke-virtual {v0, v6}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    .line 127
    .line 128
    .line 129
    iget-object v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 130
    .line 131
    invoke-virtual {v4}, Lcom/intsig/camscanner/view/HightlightRegion;->oO80()Landroid/graphics/Point;

    .line 132
    .line 133
    .line 134
    move-result-object v4

    .line 135
    if-eqz v4, :cond_9

    .line 136
    .line 137
    iget v6, v4, Landroid/graphics/Point;->x:I

    .line 138
    .line 139
    int-to-float v6, v6

    .line 140
    aput v6, v5, v2

    .line 141
    .line 142
    iget v2, v4, Landroid/graphics/Point;->y:I

    .line 143
    .line 144
    int-to-float v2, v2

    .line 145
    aput v2, v5, v11

    .line 146
    .line 147
    iget-object v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->o880:Landroid/graphics/Matrix;

    .line 148
    .line 149
    invoke-virtual {v2, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 150
    .line 151
    .line 152
    iget-object v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->O〇O:Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;

    .line 153
    .line 154
    if-eqz v2, :cond_7

    .line 155
    .line 156
    iget v5, v4, Landroid/graphics/Point;->x:I

    .line 157
    .line 158
    int-to-float v5, v5

    .line 159
    iget v4, v4, Landroid/graphics/Point;->y:I

    .line 160
    .line 161
    int-to-float v4, v4

    .line 162
    invoke-interface {v2, v5, v4}, Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;->〇Oo(FF)V

    .line 163
    .line 164
    .line 165
    :cond_7
    iget v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOo〇08〇:F

    .line 166
    .line 167
    sub-float v2, v1, v2

    .line 168
    .line 169
    cmpl-float v2, v2, v12

    .line 170
    .line 171
    if-nez v2, :cond_8

    .line 172
    .line 173
    iget v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇O8oOo0:F

    .line 174
    .line 175
    sub-float v2, v3, v2

    .line 176
    .line 177
    cmpl-float v2, v2, v12

    .line 178
    .line 179
    if-eqz v2, :cond_9

    .line 180
    .line 181
    :cond_8
    iput-boolean v11, v0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇oo:Z

    .line 182
    .line 183
    :cond_9
    iput v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOo〇08〇:F

    .line 184
    .line 185
    iput v3, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇O8oOo0:F

    .line 186
    .line 187
    goto/16 :goto_c

    .line 188
    .line 189
    :cond_a
    if-ne v6, v4, :cond_1e

    .line 190
    .line 191
    aput v1, v5, v2

    .line 192
    .line 193
    aput v3, v5, v11

    .line 194
    .line 195
    iget-object v6, v0, Lcom/intsig/camscanner/view/ImageEditView;->o880:Landroid/graphics/Matrix;

    .line 196
    .line 197
    invoke-virtual {v6, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 198
    .line 199
    .line 200
    iget-object v6, v0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 201
    .line 202
    invoke-virtual {v6}, Lcom/intsig/camscanner/view/HightlightRegion;->O8()[F

    .line 203
    .line 204
    .line 205
    move-result-object v6

    .line 206
    iget-object v13, v0, Lcom/intsig/camscanner/view/ImageEditView;->o880:Landroid/graphics/Matrix;

    .line 207
    .line 208
    invoke-virtual {v13, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 209
    .line 210
    .line 211
    new-array v14, v9, [I

    .line 212
    .line 213
    const/4 v13, 0x0

    .line 214
    :goto_0
    if-ge v13, v9, :cond_b

    .line 215
    .line 216
    aget v15, v6, v13

    .line 217
    .line 218
    iget v12, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇00O0:F

    .line 219
    .line 220
    div-float/2addr v15, v12

    .line 221
    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    .line 222
    .line 223
    .line 224
    move-result v12

    .line 225
    aput v12, v14, v13

    .line 226
    .line 227
    add-int/lit8 v13, v13, 0x1

    .line 228
    .line 229
    const/4 v12, 0x0

    .line 230
    goto :goto_0

    .line 231
    :cond_b
    new-array v6, v9, [I

    .line 232
    .line 233
    new-array v15, v4, [I

    .line 234
    .line 235
    aget v12, v5, v2

    .line 236
    .line 237
    iget v13, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇00O0:F

    .line 238
    .line 239
    div-float/2addr v12, v13

    .line 240
    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    .line 241
    .line 242
    .line 243
    move-result v12

    .line 244
    aput v12, v15, v2

    .line 245
    .line 246
    aget v12, v5, v11

    .line 247
    .line 248
    iget v13, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇00O0:F

    .line 249
    .line 250
    div-float/2addr v12, v13

    .line 251
    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    .line 252
    .line 253
    .line 254
    move-result v12

    .line 255
    aput v12, v15, v11

    .line 256
    .line 257
    iget-object v12, v0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 258
    .line 259
    invoke-virtual {v12}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 260
    .line 261
    .line 262
    move-result-object v12

    .line 263
    if-eqz v12, :cond_c

    .line 264
    .line 265
    iget-object v12, v0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 266
    .line 267
    invoke-virtual {v12}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 268
    .line 269
    .line 270
    move-result-object v12

    .line 271
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    .line 272
    .line 273
    .line 274
    move-result v12

    .line 275
    iget-object v13, v0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 276
    .line 277
    invoke-virtual {v13}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 278
    .line 279
    .line 280
    move-result-object v13

    .line 281
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    .line 282
    .line 283
    .line 284
    move-result v13

    .line 285
    goto :goto_1

    .line 286
    :cond_c
    const/4 v12, 0x0

    .line 287
    const/4 v13, 0x0

    .line 288
    :goto_1
    iget-object v8, v0, Lcom/intsig/camscanner/view/ImageEditView;->oooO888:Landroid/graphics/RectF;

    .line 289
    .line 290
    sget v7, Lcom/intsig/camscanner/view/HightlightRegion;->O〇8O8〇008:I

    .line 291
    .line 292
    neg-int v10, v7

    .line 293
    int-to-float v10, v10

    .line 294
    neg-int v4, v7

    .line 295
    int-to-float v4, v4

    .line 296
    add-int v9, v12, v7

    .line 297
    .line 298
    int-to-float v9, v9

    .line 299
    add-int/2addr v7, v13

    .line 300
    int-to-float v7, v7

    .line 301
    invoke-virtual {v8, v10, v4, v9, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 302
    .line 303
    .line 304
    iget-object v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->oooO888:Landroid/graphics/RectF;

    .line 305
    .line 306
    aget v7, v5, v2

    .line 307
    .line 308
    aget v5, v5, v11

    .line 309
    .line 310
    invoke-virtual {v4, v7, v5}, Landroid/graphics/RectF;->contains(FF)Z

    .line 311
    .line 312
    .line 313
    move-result v4

    .line 314
    if-nez v4, :cond_d

    .line 315
    .line 316
    goto/16 :goto_c

    .line 317
    .line 318
    :cond_d
    iget-boolean v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->O8〇o〇88:Z

    .line 319
    .line 320
    if-eqz v4, :cond_14

    .line 321
    .line 322
    iget-object v4, v0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 323
    .line 324
    invoke-virtual {v4}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->O8()I

    .line 325
    .line 326
    .line 327
    move-result v4

    .line 328
    const/16 v5, 0xb4

    .line 329
    .line 330
    if-eq v4, v5, :cond_11

    .line 331
    .line 332
    if-nez v4, :cond_e

    .line 333
    .line 334
    goto :goto_3

    .line 335
    :cond_e
    iget v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->o00〇88〇08:I

    .line 336
    .line 337
    const/4 v5, 0x6

    .line 338
    if-eq v4, v5, :cond_10

    .line 339
    .line 340
    const/16 v5, 0x8

    .line 341
    .line 342
    if-ne v4, v5, :cond_f

    .line 343
    .line 344
    goto :goto_2

    .line 345
    :cond_f
    aget v4, v15, v2

    .line 346
    .line 347
    aput v4, v14, v2

    .line 348
    .line 349
    aget v4, v15, v2

    .line 350
    .line 351
    const/4 v5, 0x2

    .line 352
    aput v4, v14, v5

    .line 353
    .line 354
    goto/16 :goto_5

    .line 355
    .line 356
    :cond_10
    :goto_2
    aget v4, v15, v11

    .line 357
    .line 358
    aput v4, v14, v11

    .line 359
    .line 360
    aget v4, v15, v11

    .line 361
    .line 362
    const/4 v5, 0x3

    .line 363
    aput v4, v14, v5

    .line 364
    .line 365
    goto/16 :goto_5

    .line 366
    .line 367
    :cond_11
    :goto_3
    iget v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->o00〇88〇08:I

    .line 368
    .line 369
    const/4 v5, 0x5

    .line 370
    if-eq v4, v5, :cond_13

    .line 371
    .line 372
    const/4 v5, 0x7

    .line 373
    if-ne v4, v5, :cond_12

    .line 374
    .line 375
    goto :goto_4

    .line 376
    :cond_12
    const/4 v4, 0x4

    .line 377
    aget v4, v14, v4

    .line 378
    .line 379
    aget v5, v15, v2

    .line 380
    .line 381
    sub-int/2addr v4, v5

    .line 382
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    .line 383
    .line 384
    .line 385
    move-result v4

    .line 386
    iget v5, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇〇o0〇8:I

    .line 387
    .line 388
    if-lt v4, v5, :cond_2d

    .line 389
    .line 390
    aget v4, v15, v2

    .line 391
    .line 392
    aput v4, v14, v2

    .line 393
    .line 394
    aget v4, v15, v2

    .line 395
    .line 396
    const/4 v5, 0x2

    .line 397
    aput v4, v14, v5

    .line 398
    .line 399
    goto :goto_5

    .line 400
    :cond_13
    :goto_4
    const/4 v4, 0x5

    .line 401
    aget v4, v14, v4

    .line 402
    .line 403
    aget v5, v15, v11

    .line 404
    .line 405
    sub-int/2addr v4, v5

    .line 406
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    .line 407
    .line 408
    .line 409
    move-result v4

    .line 410
    iget v5, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇〇o0〇8:I

    .line 411
    .line 412
    if-lt v4, v5, :cond_2d

    .line 413
    .line 414
    aget v4, v15, v11

    .line 415
    .line 416
    aput v4, v14, v11

    .line 417
    .line 418
    aget v4, v15, v11

    .line 419
    .line 420
    const/4 v5, 0x3

    .line 421
    aput v4, v14, v5

    .line 422
    .line 423
    goto :goto_5

    .line 424
    :cond_14
    iget v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->o8〇OO:I

    .line 425
    .line 426
    iget-object v5, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇OOoooo:[I

    .line 427
    .line 428
    iget-object v7, v0, Lcom/intsig/camscanner/view/ImageEditView;->o〇0〇o:[I

    .line 429
    .line 430
    iget-boolean v8, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇088O:Z

    .line 431
    .line 432
    move-object/from16 v16, v6

    .line 433
    .line 434
    move/from16 v17, v4

    .line 435
    .line 436
    move-object/from16 v18, v5

    .line 437
    .line 438
    move-object/from16 v19, v7

    .line 439
    .line 440
    move/from16 v20, v8

    .line 441
    .line 442
    invoke-static/range {v14 .. v20}, Lcom/intsig/camscanner/scanner/ScannerUtils;->adjustRect([I[I[II[I[IZ)I

    .line 443
    .line 444
    .line 445
    move-result v4

    .line 446
    if-gez v4, :cond_15

    .line 447
    .line 448
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/view/ImageEditView;->getDirtyRect()Landroid/graphics/Rect;

    .line 449
    .line 450
    .line 451
    move-result-object v2

    .line 452
    iget-object v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 453
    .line 454
    iget v5, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOo〇08〇:F

    .line 455
    .line 456
    sub-float v5, v1, v5

    .line 457
    .line 458
    iget v6, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇O8oOo0:F

    .line 459
    .line 460
    sub-float v6, v3, v6

    .line 461
    .line 462
    invoke-virtual {v4, v5, v6}, Lcom/intsig/camscanner/view/HightlightRegion;->〇O〇(FF)V

    .line 463
    .line 464
    .line 465
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/view/ImageEditView;->getDirtyRect()Landroid/graphics/Rect;

    .line 466
    .line 467
    .line 468
    move-result-object v4

    .line 469
    invoke-virtual {v4, v2}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 470
    .line 471
    .line 472
    invoke-virtual {v0, v4}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    .line 473
    .line 474
    .line 475
    iput v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOo〇08〇:F

    .line 476
    .line 477
    iput v3, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇O8oOo0:F

    .line 478
    .line 479
    iput-boolean v11, v0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇oo:Z

    .line 480
    .line 481
    goto/16 :goto_c

    .line 482
    .line 483
    :cond_15
    move-object v14, v6

    .line 484
    :goto_5
    const/16 v4, 0x8

    .line 485
    .line 486
    new-array v4, v4, [F

    .line 487
    .line 488
    const/4 v5, 0x0

    .line 489
    :goto_6
    array-length v6, v14

    .line 490
    if-ge v5, v6, :cond_16

    .line 491
    .line 492
    aget v6, v14, v5

    .line 493
    .line 494
    int-to-float v6, v6

    .line 495
    iget v7, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇00O0:F

    .line 496
    .line 497
    mul-float v6, v6, v7

    .line 498
    .line 499
    aput v6, v4, v5

    .line 500
    .line 501
    add-int/lit8 v5, v5, 0x1

    .line 502
    .line 503
    goto :goto_6

    .line 504
    :cond_16
    iget-object v5, v0, Lcom/intsig/camscanner/view/ImageEditView;->oooO888:Landroid/graphics/RectF;

    .line 505
    .line 506
    int-to-float v6, v12

    .line 507
    iget v7, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇00O0:F

    .line 508
    .line 509
    div-float v8, v6, v7

    .line 510
    .line 511
    int-to-float v9, v13

    .line 512
    div-float v7, v9, v7

    .line 513
    .line 514
    const/4 v10, 0x0

    .line 515
    invoke-virtual {v5, v10, v10, v8, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 516
    .line 517
    .line 518
    :goto_7
    array-length v5, v14

    .line 519
    if-ge v2, v5, :cond_1c

    .line 520
    .line 521
    iget-object v5, v0, Lcom/intsig/camscanner/view/ImageEditView;->oooO888:Landroid/graphics/RectF;

    .line 522
    .line 523
    aget v7, v14, v2

    .line 524
    .line 525
    int-to-float v7, v7

    .line 526
    add-int/lit8 v8, v2, 0x1

    .line 527
    .line 528
    aget v10, v14, v8

    .line 529
    .line 530
    int-to-float v10, v10

    .line 531
    invoke-virtual {v5, v7, v10}, Landroid/graphics/RectF;->contains(FF)Z

    .line 532
    .line 533
    .line 534
    move-result v5

    .line 535
    if-nez v5, :cond_1a

    .line 536
    .line 537
    aget v5, v14, v2

    .line 538
    .line 539
    if-gez v5, :cond_17

    .line 540
    .line 541
    const/4 v7, 0x0

    .line 542
    aput v7, v4, v2

    .line 543
    .line 544
    :cond_17
    int-to-float v5, v5

    .line 545
    iget v7, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇00O0:F

    .line 546
    .line 547
    div-float v10, v6, v7

    .line 548
    .line 549
    cmpl-float v5, v5, v10

    .line 550
    .line 551
    if-lez v5, :cond_18

    .line 552
    .line 553
    aput v6, v4, v2

    .line 554
    .line 555
    :cond_18
    aget v5, v14, v8

    .line 556
    .line 557
    const/4 v10, 0x0

    .line 558
    if-gez v5, :cond_19

    .line 559
    .line 560
    aput v10, v4, v8

    .line 561
    .line 562
    :cond_19
    int-to-float v5, v5

    .line 563
    div-float v7, v9, v7

    .line 564
    .line 565
    cmpl-float v5, v5, v7

    .line 566
    .line 567
    if-lez v5, :cond_1b

    .line 568
    .line 569
    aput v9, v4, v8

    .line 570
    .line 571
    goto :goto_8

    .line 572
    :cond_1a
    const/4 v10, 0x0

    .line 573
    :cond_1b
    :goto_8
    add-int/lit8 v2, v2, 0x2

    .line 574
    .line 575
    goto :goto_7

    .line 576
    :cond_1c
    new-instance v2, Landroid/graphics/Matrix;

    .line 577
    .line 578
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 579
    .line 580
    .line 581
    move-result-object v5

    .line 582
    invoke-direct {v2, v5}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 583
    .line 584
    .line 585
    invoke-virtual {v2, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 586
    .line 587
    .line 588
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/view/ImageEditView;->getDirtyRect()Landroid/graphics/Rect;

    .line 589
    .line 590
    .line 591
    move-result-object v2

    .line 592
    iget-object v5, v0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 593
    .line 594
    invoke-virtual {v5, v4}, Lcom/intsig/camscanner/view/HightlightRegion;->〇o([F)Z

    .line 595
    .line 596
    .line 597
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/view/ImageEditView;->getDirtyRect()Landroid/graphics/Rect;

    .line 598
    .line 599
    .line 600
    move-result-object v4

    .line 601
    invoke-virtual {v4, v2}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 602
    .line 603
    .line 604
    iget-boolean v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->O8〇o〇88:Z

    .line 605
    .line 606
    if-eqz v2, :cond_1d

    .line 607
    .line 608
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->invalidate()V

    .line 609
    .line 610
    .line 611
    goto :goto_9

    .line 612
    :cond_1d
    invoke-virtual {v0, v4}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    .line 613
    .line 614
    .line 615
    :goto_9
    iput-boolean v11, v0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇oo:Z

    .line 616
    .line 617
    iput v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOo〇08〇:F

    .line 618
    .line 619
    iput v3, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇O8oOo0:F

    .line 620
    .line 621
    goto/16 :goto_c

    .line 622
    .line 623
    :cond_1e
    const/4 v4, 0x3

    .line 624
    if-ne v6, v4, :cond_1f

    .line 625
    .line 626
    aput v1, v5, v2

    .line 627
    .line 628
    aput v3, v5, v11

    .line 629
    .line 630
    iget-object v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->o880:Landroid/graphics/Matrix;

    .line 631
    .line 632
    invoke-virtual {v2, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 633
    .line 634
    .line 635
    iget-object v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 636
    .line 637
    iget v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOo〇08〇:F

    .line 638
    .line 639
    sub-float v4, v1, v4

    .line 640
    .line 641
    iget v5, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇O8oOo0:F

    .line 642
    .line 643
    sub-float v5, v3, v5

    .line 644
    .line 645
    invoke-virtual {v2, v4, v5}, Lcom/intsig/camscanner/view/HightlightRegion;->Oooo8o0〇(FF)V

    .line 646
    .line 647
    .line 648
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/view/ImageEditView;->getDirtyRect()Landroid/graphics/Rect;

    .line 649
    .line 650
    .line 651
    move-result-object v2

    .line 652
    invoke-virtual {v2, v2}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 653
    .line 654
    .line 655
    invoke-virtual {v0, v2}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    .line 656
    .line 657
    .line 658
    iput v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOo〇08〇:F

    .line 659
    .line 660
    iput v3, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇O8oOo0:F

    .line 661
    .line 662
    iput-boolean v11, v0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇oo:Z

    .line 663
    .line 664
    goto/16 :goto_c

    .line 665
    .line 666
    :cond_1f
    const/4 v4, 0x4

    .line 667
    if-ne v6, v4, :cond_2d

    .line 668
    .line 669
    aput v1, v5, v2

    .line 670
    .line 671
    aput v3, v5, v11

    .line 672
    .line 673
    iget-object v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->o880:Landroid/graphics/Matrix;

    .line 674
    .line 675
    invoke-virtual {v2, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 676
    .line 677
    .line 678
    iget-object v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 679
    .line 680
    iget v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOo〇08〇:F

    .line 681
    .line 682
    sub-float v4, v1, v4

    .line 683
    .line 684
    iget v5, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇O8oOo0:F

    .line 685
    .line 686
    sub-float v5, v3, v5

    .line 687
    .line 688
    invoke-virtual {v2, v4, v5}, Lcom/intsig/camscanner/view/HightlightRegion;->OoO8(FF)V

    .line 689
    .line 690
    .line 691
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/view/ImageEditView;->getDirtyRect()Landroid/graphics/Rect;

    .line 692
    .line 693
    .line 694
    move-result-object v2

    .line 695
    invoke-virtual {v2, v2}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 696
    .line 697
    .line 698
    invoke-virtual {v0, v2}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    .line 699
    .line 700
    .line 701
    iput v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOo〇08〇:F

    .line 702
    .line 703
    iput v3, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇O8oOo0:F

    .line 704
    .line 705
    iput-boolean v11, v0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇oo:Z

    .line 706
    .line 707
    goto/16 :goto_c

    .line 708
    .line 709
    :cond_20
    iget v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOO〇〇:I

    .line 710
    .line 711
    if-ne v1, v11, :cond_21

    .line 712
    .line 713
    iget-object v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->O〇O:Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;

    .line 714
    .line 715
    if-eqz v1, :cond_21

    .line 716
    .line 717
    invoke-interface {v1}, Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;->〇08O〇00〇o()V

    .line 718
    .line 719
    .line 720
    :cond_21
    iget-boolean v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇oo:Z

    .line 721
    .line 722
    if-eqz v1, :cond_23

    .line 723
    .line 724
    iget-object v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->O〇O:Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;

    .line 725
    .line 726
    if-eqz v1, :cond_22

    .line 727
    .line 728
    invoke-interface {v1, v11}, Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;->o88〇OO08〇(Z)V

    .line 729
    .line 730
    .line 731
    iput-boolean v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇oo:Z

    .line 732
    .line 733
    :cond_22
    iget-object v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 734
    .line 735
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/HightlightRegion;->〇oo〇()V

    .line 736
    .line 737
    .line 738
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->invalidate()V

    .line 739
    .line 740
    .line 741
    :cond_23
    iput v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOO〇〇:I

    .line 742
    .line 743
    iget-object v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->O〇O:Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;

    .line 744
    .line 745
    if-eqz v1, :cond_24

    .line 746
    .line 747
    invoke-interface {v1}, Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;->〇0O〇Oo()V

    .line 748
    .line 749
    .line 750
    :cond_24
    iget-object v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->OO〇OOo:Landroid/view/ViewGroup;

    .line 751
    .line 752
    if-eqz v1, :cond_25

    .line 753
    .line 754
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 755
    .line 756
    .line 757
    :cond_25
    iput-boolean v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->O0〇0:Z

    .line 758
    .line 759
    goto/16 :goto_c

    .line 760
    .line 761
    :cond_26
    iget-object v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->O〇O:Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;

    .line 762
    .line 763
    if-eqz v4, :cond_27

    .line 764
    .line 765
    invoke-interface {v4}, Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;->o〇0o〇〇()V

    .line 766
    .line 767
    .line 768
    :cond_27
    iget-object v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 769
    .line 770
    invoke-virtual {v4, v1, v3}, Lcom/intsig/camscanner/view/HightlightRegion;->〇〇888(FF)I

    .line 771
    .line 772
    .line 773
    move-result v4

    .line 774
    iput v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->o00〇88〇08:I

    .line 775
    .line 776
    iput-boolean v11, v0, Lcom/intsig/camscanner/view/ImageEditView;->O0〇0:Z

    .line 777
    .line 778
    if-lt v4, v11, :cond_29

    .line 779
    .line 780
    const/4 v6, 0x4

    .line 781
    if-gt v4, v6, :cond_29

    .line 782
    .line 783
    iget-boolean v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇oo〇O〇80:Z

    .line 784
    .line 785
    if-eqz v4, :cond_28

    .line 786
    .line 787
    iput v6, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOO〇〇:I

    .line 788
    .line 789
    goto :goto_a

    .line 790
    :cond_28
    iput v11, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOO〇〇:I

    .line 791
    .line 792
    :goto_a
    iput v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOo〇08〇:F

    .line 793
    .line 794
    iput v3, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇O8oOo0:F

    .line 795
    .line 796
    iget-object v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 797
    .line 798
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/HightlightRegion;->oO80()Landroid/graphics/Point;

    .line 799
    .line 800
    .line 801
    move-result-object v1

    .line 802
    if-eqz v1, :cond_2c

    .line 803
    .line 804
    iget v3, v1, Landroid/graphics/Point;->x:I

    .line 805
    .line 806
    int-to-float v3, v3

    .line 807
    aput v3, v5, v2

    .line 808
    .line 809
    iget v2, v1, Landroid/graphics/Point;->y:I

    .line 810
    .line 811
    int-to-float v2, v2

    .line 812
    aput v2, v5, v11

    .line 813
    .line 814
    iget-object v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->o880:Landroid/graphics/Matrix;

    .line 815
    .line 816
    invoke-virtual {v2, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 817
    .line 818
    .line 819
    iget-object v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->O〇O:Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;

    .line 820
    .line 821
    if-eqz v2, :cond_2c

    .line 822
    .line 823
    iget v3, v1, Landroid/graphics/Point;->x:I

    .line 824
    .line 825
    int-to-float v3, v3

    .line 826
    iget v1, v1, Landroid/graphics/Point;->y:I

    .line 827
    .line 828
    int-to-float v1, v1

    .line 829
    invoke-interface {v2, v3, v1}, Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;->〇Oo(FF)V

    .line 830
    .line 831
    .line 832
    goto :goto_b

    .line 833
    :cond_29
    const/4 v5, 0x5

    .line 834
    if-lt v4, v5, :cond_2a

    .line 835
    .line 836
    const/16 v5, 0x8

    .line 837
    .line 838
    if-gt v4, v5, :cond_2a

    .line 839
    .line 840
    const/4 v5, 0x2

    .line 841
    iput v5, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOO〇〇:I

    .line 842
    .line 843
    iput v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOo〇08〇:F

    .line 844
    .line 845
    iput v3, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇O8oOo0:F

    .line 846
    .line 847
    goto :goto_b

    .line 848
    :cond_2a
    const/16 v5, 0x9

    .line 849
    .line 850
    if-ne v4, v5, :cond_2b

    .line 851
    .line 852
    iget-boolean v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇800OO〇0O:Z

    .line 853
    .line 854
    if-nez v4, :cond_2b

    .line 855
    .line 856
    const/4 v4, 0x3

    .line 857
    iput v4, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOO〇〇:I

    .line 858
    .line 859
    iput v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOo〇08〇:F

    .line 860
    .line 861
    iput v3, v0, Lcom/intsig/camscanner/view/ImageEditView;->〇O8oOo0:F

    .line 862
    .line 863
    goto :goto_b

    .line 864
    :cond_2b
    iput v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->oOO〇〇:I

    .line 865
    .line 866
    iput-boolean v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->O0〇0:Z

    .line 867
    .line 868
    :cond_2c
    :goto_b
    iget-object v1, v0, Lcom/intsig/camscanner/view/ImageEditView;->OO〇OOo:Landroid/view/ViewGroup;

    .line 869
    .line 870
    if-eqz v1, :cond_2d

    .line 871
    .line 872
    iget-boolean v2, v0, Lcom/intsig/camscanner/view/ImageEditView;->O0〇0:Z

    .line 873
    .line 874
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 875
    .line 876
    .line 877
    :cond_2d
    :goto_c
    return v11
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method public oo〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O8o〇O0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0OOo〇0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O〇08oOOO0:Landroid/graphics/Paint;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroid/graphics/Paint;

    .line 6
    .line 7
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O〇08oOOO0:Landroid/graphics/Paint;

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O〇08oOOO0:Landroid/graphics/Paint;

    .line 17
    .line 18
    const v1, -0xff0100

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public o〇8oOO88([FFZZ)V
    .locals 7

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "setRegion: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-static {p1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v1, " scale="

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    const-string v1, "ImageEditView"

    .line 31
    .line 32
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    if-eqz p1, :cond_5

    .line 36
    .line 37
    array-length v0, p1

    .line 38
    if-nez v0, :cond_0

    .line 39
    .line 40
    goto :goto_3

    .line 41
    :cond_0
    const/16 v0, 0x8

    .line 42
    .line 43
    new-array v1, v0, [F

    .line 44
    .line 45
    const/4 v2, 0x0

    .line 46
    const/4 v3, 0x0

    .line 47
    :goto_0
    if-ge v3, v0, :cond_1

    .line 48
    .line 49
    aget v4, p1, v3

    .line 50
    .line 51
    mul-float v4, v4, p2

    .line 52
    .line 53
    aput v4, v1, v3

    .line 54
    .line 55
    add-int/lit8 v3, v3, 0x1

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_1
    new-instance v0, Landroid/graphics/Matrix;

    .line 59
    .line 60
    invoke-virtual {p0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 61
    .line 62
    .line 63
    move-result-object v3

    .line 64
    invoke-direct {v0, v3}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 68
    .line 69
    .line 70
    new-instance v3, Landroid/graphics/RectF;

    .line 71
    .line 72
    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 73
    .line 74
    .line 75
    iget-object v4, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 76
    .line 77
    invoke-virtual {v4}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 78
    .line 79
    .line 80
    move-result-object v4

    .line 81
    if-eqz v4, :cond_2

    .line 82
    .line 83
    iget-object v4, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 84
    .line 85
    invoke-virtual {v4}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 86
    .line 87
    .line 88
    move-result-object v4

    .line 89
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    .line 90
    .line 91
    .line 92
    move-result v4

    .line 93
    iget-object v5, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oOo〇8o008:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 94
    .line 95
    invoke-virtual {v5}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 96
    .line 97
    .line 98
    move-result-object v5

    .line 99
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    .line 100
    .line 101
    .line 102
    move-result v5

    .line 103
    goto :goto_1

    .line 104
    :cond_2
    const/4 v4, 0x0

    .line 105
    const/4 v5, 0x0

    .line 106
    :goto_1
    int-to-float v4, v4

    .line 107
    int-to-float v5, v5

    .line 108
    const/4 v6, 0x0

    .line 109
    invoke-virtual {v3, v6, v6, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 110
    .line 111
    .line 112
    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 113
    .line 114
    .line 115
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 116
    .line 117
    invoke-virtual {v0, v3, v1}, Lcom/intsig/camscanner/view/HightlightRegion;->O8〇o(Landroid/graphics/RectF;[F)V

    .line 118
    .line 119
    .line 120
    if-eqz p3, :cond_3

    .line 121
    .line 122
    const/4 p3, 0x1

    .line 123
    iput-boolean p3, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇08〇o0O:Z

    .line 124
    .line 125
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o8O:[F

    .line 126
    .line 127
    iput p2, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇80O8o8O〇:F

    .line 128
    .line 129
    goto :goto_2

    .line 130
    :cond_3
    iput-boolean v2, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇08〇o0O:Z

    .line 131
    .line 132
    :goto_2
    if-eqz p4, :cond_4

    .line 133
    .line 134
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 135
    .line 136
    .line 137
    :cond_4
    iput p2, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇00O0:F

    .line 138
    .line 139
    const/high16 p1, 0x41a00000    # 20.0f

    .line 140
    .line 141
    div-float/2addr p1, p2

    .line 142
    float-to-int p1, p1

    .line 143
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o8〇OO:I

    .line 144
    .line 145
    :cond_5
    :goto_3
    return-void
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public o〇O(ZZ)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/HightlightRegion;->〇00(Z)V

    .line 4
    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setBackgroundMask(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/HightlightRegion;->O〇8O8〇008(I)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBitmapEnhanced(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o8o:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageEditView;->o〇0OOo〇0()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCoveringFullBlackBg(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇0O〇O00O:Z

    .line 2
    .line 3
    if-eq v0, p1, :cond_0

    .line 4
    .line 5
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇0O〇O00O:Z

    .line 6
    .line 7
    :cond_0
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCurrentRealSize([I)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o0OoOOo0:[I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setEnableBitmapLine(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇o〇Oo88:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setEnableMoveAll(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/HightlightRegion;->O8ooOoo〇(Z)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setEnableNewFindBorder(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇088O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setEnableParentViewScroll(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇800OO〇0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setEnhanceLineFromBottom(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo0〇Ooo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setEnhanceProcess(I)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇8O8oOo:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇8O8oOo:Z

    .line 7
    .line 8
    :cond_0
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->Oo80:I

    .line 9
    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    mul-int p1, p1, v0

    .line 15
    .line 16
    int-to-float p1, p1

    .line 17
    const/high16 v0, 0x42c80000    # 100.0f

    .line 18
    .line 19
    div-float/2addr p1, v0

    .line 20
    float-to-int p1, p1

    .line 21
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇〇o〇:I

    .line 22
    .line 23
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLinePaintColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇o88:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLines([I)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇OOoooo:[I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMinDistance(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇〇o0〇8:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setNLine([I)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇0〇o:[I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setNeedRecycler(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇8〇oO〇〇8o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setNeedTrimCover(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->O〇o88o08〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOnCornorChangeListener(Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->O〇O:Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOnleyShowFourCornerDrawPoints(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/HightlightRegion;->o〇〇0〇(Z)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOnlyParallelDrawPoints(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->O8〇o〇88:Z

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/HightlightRegion;->OOO〇O0(Z)V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setParentViewGroup(Landroid/view/ViewGroup;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->OO〇OOo:Landroid/view/ViewGroup;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPreEnhanceProcess(I)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇8O8oOo:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->oO〇8O8oOo:Z

    .line 7
    .line 8
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    mul-int p1, p1, v0

    .line 13
    .line 14
    int-to-float p1, p1

    .line 15
    const/high16 v0, 0x42c80000    # 100.0f

    .line 16
    .line 17
    div-float/2addr p1, v0

    .line 18
    float-to-int p1, p1

    .line 19
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇〇〇0o〇〇0:I

    .line 20
    .line 21
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 22
    .line 23
    .line 24
    return-void
.end method

.method public setRegionAvailability(Z)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/view/ImageEditView;->o〇O(ZZ)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRegionVisibility(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/HightlightRegion;->oo〇(Z)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 7
    .line 8
    xor-int/lit8 v1, p1, 0x1

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/HightlightRegion;->〇oOO8O8(Z)V

    .line 11
    .line 12
    .line 13
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇08〇o0O:Z

    .line 14
    .line 15
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTrimFrameBorder([I)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->ooO:[I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTrimProcess(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇OO〇00〇0O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇00(ZZ)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oOO8:Z

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇0000OOO(Z)[F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/HightlightRegion;->o〇0()[F

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    new-instance v1, Landroid/graphics/Matrix;

    .line 8
    .line 9
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 20
    .line 21
    .line 22
    if-nez p1, :cond_0

    .line 23
    .line 24
    const/4 p1, 0x0

    .line 25
    :goto_0
    const/16 v1, 0x8

    .line 26
    .line 27
    if-ge p1, v1, :cond_0

    .line 28
    .line 29
    aget v1, v0, p1

    .line 30
    .line 31
    iget v2, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇00O0:F

    .line 32
    .line 33
    div-float/2addr v1, v2

    .line 34
    aput v1, v0, p1

    .line 35
    .line 36
    add-int/lit8 p1, p1, 0x1

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    return-object v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇00〇8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇08〇o0O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇08O8o〇0()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇0〇o:[I

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇8〇OOoooo:[I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8(FLjava/lang/String;)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-virtual {p0, p1, p2, v0}, Lcom/intsig/camscanner/view/ImageEditView;->O08000(FLjava/lang/String;Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇8〇0〇o〇O(F[IZ)V
    .locals 6

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    new-array v0, v0, [F

    .line 4
    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    const/4 v3, 0x0

    .line 10
    aput v3, v0, v2

    .line 11
    .line 12
    aput v3, v0, v1

    .line 13
    .line 14
    aget v2, p2, v2

    .line 15
    .line 16
    int-to-float v4, v2

    .line 17
    const/4 v5, 0x2

    .line 18
    aput v4, v0, v5

    .line 19
    .line 20
    const/4 v4, 0x3

    .line 21
    aput v3, v0, v4

    .line 22
    .line 23
    const/4 v4, 0x4

    .line 24
    int-to-float v2, v2

    .line 25
    aput v2, v0, v4

    .line 26
    .line 27
    aget p2, p2, v1

    .line 28
    .line 29
    int-to-float v2, p2

    .line 30
    const/4 v4, 0x5

    .line 31
    aput v2, v0, v4

    .line 32
    .line 33
    const/4 v2, 0x6

    .line 34
    aput v3, v0, v2

    .line 35
    .line 36
    const/4 v2, 0x7

    .line 37
    int-to-float p2, p2

    .line 38
    aput p2, v0, v2

    .line 39
    .line 40
    :cond_0
    invoke-virtual {p0, v0, p1, v1, p3}, Lcom/intsig/camscanner/view/ImageEditView;->o〇8oOO88([FFZZ)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public 〇oOO8O8(Z)[I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->O88O:Lcom/intsig/camscanner/view/HightlightRegion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/HightlightRegion;->o〇0()[F

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    new-instance v1, Landroid/graphics/Matrix;

    .line 8
    .line 9
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 20
    .line 21
    .line 22
    invoke-static {v0}, Lcom/intsig/camscanner/view/HightlightRegion;->oo88o8O([F)V

    .line 23
    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    if-nez p1, :cond_0

    .line 27
    .line 28
    const/4 p1, 0x0

    .line 29
    :goto_0
    const/16 v2, 0x8

    .line 30
    .line 31
    if-ge p1, v2, :cond_0

    .line 32
    .line 33
    aget v2, v0, p1

    .line 34
    .line 35
    iget v3, p0, Lcom/intsig/camscanner/view/ImageEditView;->〇00O0:F

    .line 36
    .line 37
    div-float/2addr v2, v3

    .line 38
    aput v2, v0, p1

    .line 39
    .line 40
    add-int/lit8 p1, p1, 0x1

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_0
    array-length p1, v0

    .line 44
    new-array v2, p1, [I

    .line 45
    .line 46
    :goto_1
    if-ge v1, p1, :cond_1

    .line 47
    .line 48
    aget v3, v0, v1

    .line 49
    .line 50
    float-to-int v3, v3

    .line 51
    aput v3, v2, v1

    .line 52
    .line 53
    add-int/lit8 v1, v1, 0x1

    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_1
    return-object v2
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇〇〇0〇〇0(Z)V
    .locals 6

    .line 1
    const-string v0, "ImageEditView"

    .line 2
    .line 3
    iget-boolean v1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇o〇Oo88:Z

    .line 4
    .line 5
    if-nez v1, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oo8ooo8O:Landroid/graphics/Bitmap;

    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    if-nez v1, :cond_1

    .line 12
    .line 13
    :try_start_0
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 14
    .line 15
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    const v3, 0x7f08045c

    .line 20
    .line 21
    .line 22
    invoke-static {v1, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 23
    .line 24
    .line 25
    move-result-object v1
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    goto :goto_0

    .line 27
    :catch_0
    move-exception v1

    .line 28
    new-instance v3, Ljava/lang/StringBuilder;

    .line 29
    .line 30
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    const-string v4, "decodeResource_"

    .line 34
    .line 35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    const/4 v1, 0x0

    .line 49
    :goto_0
    if-eqz v1, :cond_1

    .line 50
    .line 51
    :try_start_1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 52
    .line 53
    .line 54
    move-result v3

    .line 55
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 56
    .line 57
    .line 58
    move-result v4

    .line 59
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 60
    .line 61
    .line 62
    move-result v5

    .line 63
    mul-int v4, v4, v5

    .line 64
    .line 65
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 66
    .line 67
    .line 68
    move-result v5

    .line 69
    div-int/2addr v4, v5

    .line 70
    invoke-static {v1, v3, v4, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    .line 71
    .line 72
    .line 73
    move-result-object v3

    .line 74
    iput-object v3, p0, Lcom/intsig/camscanner/view/ImageEditView;->oo8ooo8O:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    .line 75
    .line 76
    goto :goto_1

    .line 77
    :catch_1
    move-exception v3

    .line 78
    new-instance v4, Ljava/lang/StringBuilder;

    .line 79
    .line 80
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 81
    .line 82
    .line 83
    const-string v5, "createScaledBitmap_"

    .line 84
    .line 85
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v3

    .line 95
    invoke-static {v0, v3}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->oo8ooo8O:Landroid/graphics/Bitmap;

    .line 99
    .line 100
    if-eq v0, v1, :cond_1

    .line 101
    .line 102
    invoke-static {v1}, Lcom/intsig/camscanner/util/Util;->OOo0O(Landroid/graphics/Bitmap;)V

    .line 103
    .line 104
    .line 105
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇oO:Z

    .line 106
    .line 107
    xor-int/2addr p1, v0

    .line 108
    if-eqz p1, :cond_2

    .line 109
    .line 110
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oo8ooo8O:Landroid/graphics/Bitmap;

    .line 111
    .line 112
    const/16 v0, 0xb4

    .line 113
    .line 114
    invoke-static {p1, v0}, Lcom/intsig/utils/ImageUtil;->〇oOO8O8(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    .line 115
    .line 116
    .line 117
    move-result-object p1

    .line 118
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->oo8ooo8O:Landroid/graphics/Bitmap;

    .line 119
    .line 120
    iget-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇oO:Z

    .line 121
    .line 122
    xor-int/2addr p1, v2

    .line 123
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageEditView;->o〇oO:Z

    .line 124
    .line 125
    :cond_2
    return-void
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.class public Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;
.super Landroid/widget/LinearLayout;
.source "FiveStarLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout$OnMarkListener;
    }
.end annotation


# instance fields
.field private O8o08O8O:Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout$OnMarkListener;

.field private OO:Landroid/widget/ImageView;

.field private o0:Landroid/widget/ImageView;

.field private o〇00O:Landroid/widget/ImageView;

.field private 〇08O〇00〇o:Landroid/widget/ImageView;

.field private 〇OOo8〇0:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->〇080()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private varargs setBright([Landroid/widget/ImageView;)V
    .locals 5

    .line 1
    array-length v0, p1

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    if-ge v1, v0, :cond_0

    .line 4
    .line 5
    aget-object v2, p1, v1

    .line 6
    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 8
    .line 9
    .line 10
    move-result-object v3

    .line 11
    const v4, 0x7f080ca0

    .line 12
    .line 13
    .line 14
    invoke-virtual {v3, v4}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 15
    .line 16
    .line 17
    move-result-object v3

    .line 18
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 19
    .line 20
    .line 21
    add-int/lit8 v1, v1, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    return-void
.end method

.method private varargs setGray([Landroid/widget/ImageView;)V
    .locals 5

    .line 1
    array-length v0, p1

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    if-ge v1, v0, :cond_0

    .line 4
    .line 5
    aget-object v2, p1, v1

    .line 6
    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 8
    .line 9
    .line 10
    move-result-object v3

    .line 11
    const v4, 0x7f080ca1

    .line 12
    .line 13
    .line 14
    invoke-virtual {v3, v4}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 15
    .line 16
    .line 17
    move-result-object v3

    .line 18
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 19
    .line 20
    .line 21
    add-int/lit8 v1, v1, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    return-void
.end method

.method private setMark(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->O8o08O8O:Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout$OnMarkListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout$OnMarkListener;->〇080(I)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇080()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const v1, 0x7f0d0520

    .line 10
    .line 11
    .line 12
    const/4 v2, 0x1

    .line 13
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    const v0, 0x7f0a08eb

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    check-cast v0, Landroid/widget/ImageView;

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->o0:Landroid/widget/ImageView;

    .line 26
    .line 27
    const v0, 0x7f0a08ed

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    check-cast v0, Landroid/widget/ImageView;

    .line 35
    .line 36
    iput-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 37
    .line 38
    const v0, 0x7f0a08ee

    .line 39
    .line 40
    .line 41
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    check-cast v0, Landroid/widget/ImageView;

    .line 46
    .line 47
    iput-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->OO:Landroid/widget/ImageView;

    .line 48
    .line 49
    const v0, 0x7f0a08ec

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    check-cast v0, Landroid/widget/ImageView;

    .line 57
    .line 58
    iput-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 59
    .line 60
    const v0, 0x7f0a08ea

    .line 61
    .line 62
    .line 63
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    check-cast v0, Landroid/widget/ImageView;

    .line 68
    .line 69
    iput-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->o〇00O:Landroid/widget/ImageView;

    .line 70
    .line 71
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->o0:Landroid/widget/ImageView;

    .line 72
    .line 73
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    .line 75
    .line 76
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 77
    .line 78
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->OO:Landroid/widget/ImageView;

    .line 82
    .line 83
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    .line 85
    .line 86
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 87
    .line 88
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    .line 90
    .line 91
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->o〇00O:Landroid/widget/ImageView;

    .line 92
    .line 93
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    .line 95
    .line 96
    return-void
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const/4 v0, 0x4

    .line 6
    const/4 v1, 0x3

    .line 7
    const/4 v2, 0x2

    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x1

    .line 10
    packed-switch p1, :pswitch_data_0

    .line 11
    .line 12
    .line 13
    goto/16 :goto_0

    .line 14
    .line 15
    :pswitch_0
    new-array p1, v1, [Landroid/widget/ImageView;

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->o0:Landroid/widget/ImageView;

    .line 18
    .line 19
    aput-object v0, p1, v3

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 22
    .line 23
    aput-object v0, p1, v4

    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->OO:Landroid/widget/ImageView;

    .line 26
    .line 27
    aput-object v0, p1, v2

    .line 28
    .line 29
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->setBright([Landroid/widget/ImageView;)V

    .line 30
    .line 31
    .line 32
    new-array p1, v2, [Landroid/widget/ImageView;

    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 35
    .line 36
    aput-object v0, p1, v3

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->o〇00O:Landroid/widget/ImageView;

    .line 39
    .line 40
    aput-object v0, p1, v4

    .line 41
    .line 42
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->setGray([Landroid/widget/ImageView;)V

    .line 43
    .line 44
    .line 45
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->setMark(I)V

    .line 46
    .line 47
    .line 48
    goto/16 :goto_0

    .line 49
    .line 50
    :pswitch_1
    new-array p1, v2, [Landroid/widget/ImageView;

    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->o0:Landroid/widget/ImageView;

    .line 53
    .line 54
    aput-object v0, p1, v3

    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 57
    .line 58
    aput-object v0, p1, v4

    .line 59
    .line 60
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->setBright([Landroid/widget/ImageView;)V

    .line 61
    .line 62
    .line 63
    new-array p1, v1, [Landroid/widget/ImageView;

    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->OO:Landroid/widget/ImageView;

    .line 66
    .line 67
    aput-object v0, p1, v3

    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 70
    .line 71
    aput-object v0, p1, v4

    .line 72
    .line 73
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->o〇00O:Landroid/widget/ImageView;

    .line 74
    .line 75
    aput-object v0, p1, v2

    .line 76
    .line 77
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->setGray([Landroid/widget/ImageView;)V

    .line 78
    .line 79
    .line 80
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->setMark(I)V

    .line 81
    .line 82
    .line 83
    goto :goto_0

    .line 84
    :pswitch_2
    new-array p1, v0, [Landroid/widget/ImageView;

    .line 85
    .line 86
    iget-object v5, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->o0:Landroid/widget/ImageView;

    .line 87
    .line 88
    aput-object v5, p1, v3

    .line 89
    .line 90
    iget-object v5, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 91
    .line 92
    aput-object v5, p1, v4

    .line 93
    .line 94
    iget-object v5, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->OO:Landroid/widget/ImageView;

    .line 95
    .line 96
    aput-object v5, p1, v2

    .line 97
    .line 98
    iget-object v2, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 99
    .line 100
    aput-object v2, p1, v1

    .line 101
    .line 102
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->setBright([Landroid/widget/ImageView;)V

    .line 103
    .line 104
    .line 105
    new-array p1, v4, [Landroid/widget/ImageView;

    .line 106
    .line 107
    iget-object v1, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->o〇00O:Landroid/widget/ImageView;

    .line 108
    .line 109
    aput-object v1, p1, v3

    .line 110
    .line 111
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->setGray([Landroid/widget/ImageView;)V

    .line 112
    .line 113
    .line 114
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->setMark(I)V

    .line 115
    .line 116
    .line 117
    goto :goto_0

    .line 118
    :pswitch_3
    new-array p1, v4, [Landroid/widget/ImageView;

    .line 119
    .line 120
    iget-object v5, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->o0:Landroid/widget/ImageView;

    .line 121
    .line 122
    aput-object v5, p1, v3

    .line 123
    .line 124
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->setBright([Landroid/widget/ImageView;)V

    .line 125
    .line 126
    .line 127
    new-array p1, v0, [Landroid/widget/ImageView;

    .line 128
    .line 129
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 130
    .line 131
    aput-object v0, p1, v3

    .line 132
    .line 133
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->OO:Landroid/widget/ImageView;

    .line 134
    .line 135
    aput-object v0, p1, v4

    .line 136
    .line 137
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 138
    .line 139
    aput-object v0, p1, v2

    .line 140
    .line 141
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->o〇00O:Landroid/widget/ImageView;

    .line 142
    .line 143
    aput-object v0, p1, v1

    .line 144
    .line 145
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->setGray([Landroid/widget/ImageView;)V

    .line 146
    .line 147
    .line 148
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->setMark(I)V

    .line 149
    .line 150
    .line 151
    goto :goto_0

    .line 152
    :pswitch_4
    const/4 p1, 0x5

    .line 153
    new-array v5, p1, [Landroid/widget/ImageView;

    .line 154
    .line 155
    iget-object v6, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->o0:Landroid/widget/ImageView;

    .line 156
    .line 157
    aput-object v6, v5, v3

    .line 158
    .line 159
    iget-object v3, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 160
    .line 161
    aput-object v3, v5, v4

    .line 162
    .line 163
    iget-object v3, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->OO:Landroid/widget/ImageView;

    .line 164
    .line 165
    aput-object v3, v5, v2

    .line 166
    .line 167
    iget-object v2, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 168
    .line 169
    aput-object v2, v5, v1

    .line 170
    .line 171
    iget-object v1, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->o〇00O:Landroid/widget/ImageView;

    .line 172
    .line 173
    aput-object v1, v5, v0

    .line 174
    .line 175
    invoke-direct {p0, v5}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->setBright([Landroid/widget/ImageView;)V

    .line 176
    .line 177
    .line 178
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->setMark(I)V

    .line 179
    .line 180
    .line 181
    :goto_0
    return-void

    .line 182
    nop

    .line 183
    :pswitch_data_0
    .packed-switch 0x7f0a08ea
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public setMarkListener(Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout$OnMarkListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout;->O8o08O8O:Lcom/intsig/camscanner/view/dialog/impl/guidemark/FiveStarLayout$OnMarkListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public abstract Lcom/intsig/camscanner/view/dialog/AbsCSDialog;
.super Landroid/app/Dialog;
.source "AbsCSDialog.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/app/Dialog;"
    }
.end annotation


# static fields
.field private static final O8o08O8O:Ljava/lang/String; = "AbsCSDialog"


# instance fields
.field private OO:Z

.field protected o0:Landroid/content/Context;

.field protected o〇00O:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private 〇08O〇00〇o:Z

.field protected 〇OOo8〇0:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;ZZI)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    .line 1
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;-><init>(Landroid/content/Context;ZZILjava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZILjava/lang/Object;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "ZZITT;)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1, p4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 3
    sget-object p4, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->O8o08O8O:Ljava/lang/String;

    const-string v0, "AbsCSDialog"

    invoke-static {p4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    iput-object p5, p0, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->o〇00O:Ljava/lang/Object;

    .line 5
    iput-object p1, p0, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->o0:Landroid/content/Context;

    .line 6
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->OO:Z

    .line 7
    iput-boolean p3, p0, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->〇08O〇00〇o:Z

    .line 8
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->Oo08(Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->〇OOo8〇0:Landroid/view/View;

    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->o〇0()V

    .line 10
    iget-object p1, p0, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->〇OOo8〇0:Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->〇80〇808〇O(Landroid/view/View;)V

    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->〇〇888()V

    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->oO80()V

    return-void
.end method

.method private o〇0()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->O8o08O8O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "init"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->〇OOo8〇0:Landroid/view/View;

    .line 9
    .line 10
    invoke-virtual {p0, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 11
    .line 12
    .line 13
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->OO:Z

    .line 14
    .line 15
    invoke-virtual {p0, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 16
    .line 17
    .line 18
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->〇08O〇00〇o:Z

    .line 19
    .line 20
    invoke-virtual {p0, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->O8()I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->〇o〇()I

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 45
    .line 46
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/dialog/AbsCSDialog;->〇o00〇〇Oo()I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 51
    .line 52
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 53
    .line 54
    .line 55
    :cond_0
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method


# virtual methods
.method public abstract O8()I
.end method

.method public abstract Oo08(Landroid/content/Context;)Landroid/view/View;
.end method

.method public abstract oO80()V
.end method

.method public abstract 〇80〇808〇O(Landroid/view/View;)V
.end method

.method public abstract 〇o00〇〇Oo()I
.end method

.method public abstract 〇o〇()I
.end method

.method public abstract 〇〇888()V
.end method

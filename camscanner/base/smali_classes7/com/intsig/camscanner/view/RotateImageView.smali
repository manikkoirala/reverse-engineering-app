.class public Lcom/intsig/camscanner/view/RotateImageView;
.super Lcom/intsig/camscanner/view/TipImageView;
.source "RotateImageView.java"


# instance fields
.field private O0O:Lcom/intsig/view/ViewDotStrategy;

.field private OO〇00〇8oO:Z

.field private o8oOOo:Landroid/graphics/Bitmap;

.field private o8〇OO0〇0o:Z

.field private oOo0:I

.field private oOo〇8o008:I

.field private ooo0〇〇O:J

.field private 〇0O:I

.field private 〇8〇oO〇〇8o:J

.field private 〇O〇〇O8:[Landroid/graphics/drawable/Drawable;

.field private 〇o0O:Landroid/graphics/drawable/TransitionDrawable;

.field private 〇〇08O:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/view/TipImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    const/4 p2, 0x0

    .line 5
    iput p2, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇0O:I

    .line 6
    .line 7
    iput p2, p0, Lcom/intsig/camscanner/view/RotateImageView;->oOo〇8o008:I

    .line 8
    .line 9
    iput p2, p0, Lcom/intsig/camscanner/view/RotateImageView;->oOo0:I

    .line 10
    .line 11
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/RotateImageView;->OO〇00〇8oO:Z

    .line 12
    .line 13
    const/4 p2, 0x1

    .line 14
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/RotateImageView;->o8〇OO0〇0o:Z

    .line 15
    .line 16
    const-wide/16 v0, 0x0

    .line 17
    .line 18
    iput-wide v0, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇8〇oO〇〇8o:J

    .line 19
    .line 20
    iput-wide v0, p0, Lcom/intsig/camscanner/view/RotateImageView;->ooo0〇〇O:J

    .line 21
    .line 22
    iput-boolean p2, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇〇08O:Z

    .line 23
    .line 24
    new-instance p2, Lcom/intsig/view/ViewDotStrategy;

    .line 25
    .line 26
    invoke-direct {p2}, Lcom/intsig/view/ViewDotStrategy;-><init>()V

    .line 27
    .line 28
    .line 29
    iput-object p2, p0, Lcom/intsig/camscanner/view/RotateImageView;->O0O:Lcom/intsig/view/ViewDotStrategy;

    .line 30
    .line 31
    invoke-virtual {p2, p1}, Lcom/intsig/view/ViewDotStrategy;->O8(Landroid/content/Context;)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method protected getDegree()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/RotateImageView;->oOo0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇〇08O:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-super {p0, p1}, Lcom/intsig/view/SafeImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 6
    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-nez v0, :cond_1

    .line 14
    .line 15
    return-void

    .line 16
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    iget v2, v1, Landroid/graphics/Rect;->right:I

    .line 21
    .line 22
    iget v3, v1, Landroid/graphics/Rect;->left:I

    .line 23
    .line 24
    sub-int/2addr v2, v3

    .line 25
    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    .line 26
    .line 27
    iget v1, v1, Landroid/graphics/Rect;->top:I

    .line 28
    .line 29
    sub-int/2addr v3, v1

    .line 30
    if-eqz v2, :cond_9

    .line 31
    .line 32
    if-nez v3, :cond_2

    .line 33
    .line 34
    goto/16 :goto_3

    .line 35
    .line 36
    :cond_2
    iget v1, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇0O:I

    .line 37
    .line 38
    iget v4, p0, Lcom/intsig/camscanner/view/RotateImageView;->oOo0:I

    .line 39
    .line 40
    if-eq v1, v4, :cond_6

    .line 41
    .line 42
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    .line 43
    .line 44
    .line 45
    move-result-wide v4

    .line 46
    iget-wide v6, p0, Lcom/intsig/camscanner/view/RotateImageView;->ooo0〇〇O:J

    .line 47
    .line 48
    cmp-long v1, v4, v6

    .line 49
    .line 50
    if-gez v1, :cond_5

    .line 51
    .line 52
    iget-wide v6, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇8〇oO〇〇8o:J

    .line 53
    .line 54
    sub-long/2addr v4, v6

    .line 55
    long-to-int v1, v4

    .line 56
    iget v4, p0, Lcom/intsig/camscanner/view/RotateImageView;->oOo〇8o008:I

    .line 57
    .line 58
    iget-boolean v5, p0, Lcom/intsig/camscanner/view/RotateImageView;->OO〇00〇8oO:Z

    .line 59
    .line 60
    if-eqz v5, :cond_3

    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_3
    neg-int v1, v1

    .line 64
    :goto_0
    mul-int/lit16 v1, v1, 0x10e

    .line 65
    .line 66
    div-int/lit16 v1, v1, 0x3e8

    .line 67
    .line 68
    add-int/2addr v4, v1

    .line 69
    if-ltz v4, :cond_4

    .line 70
    .line 71
    rem-int/lit16 v4, v4, 0x168

    .line 72
    .line 73
    goto :goto_1

    .line 74
    :cond_4
    rem-int/lit16 v4, v4, 0x168

    .line 75
    .line 76
    add-int/lit16 v4, v4, 0x168

    .line 77
    .line 78
    :goto_1
    iput v4, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇0O:I

    .line 79
    .line 80
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 81
    .line 82
    .line 83
    goto :goto_2

    .line 84
    :cond_5
    iget v1, p0, Lcom/intsig/camscanner/view/RotateImageView;->oOo0:I

    .line 85
    .line 86
    iput v1, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇0O:I

    .line 87
    .line 88
    :cond_6
    :goto_2
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    .line 89
    .line 90
    .line 91
    move-result v1

    .line 92
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    .line 93
    .line 94
    .line 95
    move-result v4

    .line 96
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    .line 97
    .line 98
    .line 99
    move-result v5

    .line 100
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    .line 101
    .line 102
    .line 103
    move-result v6

    .line 104
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 105
    .line 106
    .line 107
    move-result v7

    .line 108
    sub-int/2addr v7, v1

    .line 109
    sub-int/2addr v7, v5

    .line 110
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 111
    .line 112
    .line 113
    move-result v5

    .line 114
    sub-int/2addr v5, v4

    .line 115
    sub-int/2addr v5, v6

    .line 116
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    .line 117
    .line 118
    .line 119
    move-result v6

    .line 120
    invoke-virtual {p0}, Landroid/widget/ImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    .line 121
    .line 122
    .line 123
    move-result-object v8

    .line 124
    sget-object v9, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    .line 125
    .line 126
    if-ne v8, v9, :cond_8

    .line 127
    .line 128
    if-lt v7, v2, :cond_7

    .line 129
    .line 130
    if-ge v5, v3, :cond_8

    .line 131
    .line 132
    :cond_7
    int-to-float v8, v7

    .line 133
    int-to-float v9, v2

    .line 134
    div-float v9, v8, v9

    .line 135
    .line 136
    int-to-float v10, v5

    .line 137
    int-to-float v11, v3

    .line 138
    div-float v11, v10, v11

    .line 139
    .line 140
    invoke-static {v9, v11}, Ljava/lang/Math;->min(FF)F

    .line 141
    .line 142
    .line 143
    move-result v9

    .line 144
    const/high16 v11, 0x40000000    # 2.0f

    .line 145
    .line 146
    div-float/2addr v8, v11

    .line 147
    div-float/2addr v10, v11

    .line 148
    invoke-virtual {p1, v9, v9, v8, v10}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 149
    .line 150
    .line 151
    :cond_8
    div-int/lit8 v7, v7, 0x2

    .line 152
    .line 153
    add-int/2addr v1, v7

    .line 154
    int-to-float v1, v1

    .line 155
    div-int/lit8 v5, v5, 0x2

    .line 156
    .line 157
    add-int/2addr v4, v5

    .line 158
    int-to-float v4, v4

    .line 159
    invoke-virtual {p1, v1, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 160
    .line 161
    .line 162
    iget v1, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇0O:I

    .line 163
    .line 164
    neg-int v1, v1

    .line 165
    int-to-float v1, v1

    .line 166
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->rotate(F)V

    .line 167
    .line 168
    .line 169
    neg-int v1, v2

    .line 170
    div-int/lit8 v1, v1, 0x2

    .line 171
    .line 172
    int-to-float v1, v1

    .line 173
    neg-int v3, v3

    .line 174
    div-int/lit8 v3, v3, 0x2

    .line 175
    .line 176
    int-to-float v3, v3

    .line 177
    invoke-virtual {p1, v1, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 178
    .line 179
    .line 180
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 181
    .line 182
    .line 183
    iget-object v0, p0, Lcom/intsig/camscanner/view/RotateImageView;->O0O:Lcom/intsig/view/ViewDotStrategy;

    .line 184
    .line 185
    int-to-float v1, v2

    .line 186
    const/4 v2, 0x0

    .line 187
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/view/ViewDotStrategy;->〇080(Landroid/graphics/Canvas;FF)V

    .line 188
    .line 189
    .line 190
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 191
    .line 192
    .line 193
    :cond_9
    :goto_3
    return-void
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public o〇0(IZ)V
    .locals 0

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/RotateImageView;->setDegree(I)V

    .line 4
    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/RotateImageView;->setDegree2(I)V

    .line 8
    .line 9
    .line 10
    :goto_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 5

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/view/RotateImageView;->o8oOOo:Landroid/graphics/Bitmap;

    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇O〇〇O8:[Landroid/graphics/drawable/Drawable;

    .line 7
    .line 8
    invoke-virtual {p0, p1}, Lcom/intsig/view/SafeImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 9
    .line 10
    .line 11
    const/16 p1, 0x8

    .line 12
    .line 13
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 22
    .line 23
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    sub-int/2addr v1, v2

    .line 28
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    sub-int/2addr v1, v2

    .line 33
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 34
    .line 35
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    sub-int/2addr v0, v2

    .line 40
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    sub-int/2addr v0, v2

    .line 45
    invoke-static {p1, v1, v0}, Landroid/media/ThumbnailUtils;->extractThumbnail(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    iput-object p1, p0, Lcom/intsig/camscanner/view/RotateImageView;->o8oOOo:Landroid/graphics/Bitmap;

    .line 50
    .line 51
    iget-object p1, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇O〇〇O8:[Landroid/graphics/drawable/Drawable;

    .line 52
    .line 53
    const/4 v0, 0x0

    .line 54
    const/4 v1, 0x1

    .line 55
    if-eqz p1, :cond_2

    .line 56
    .line 57
    iget-boolean v2, p0, Lcom/intsig/camscanner/view/RotateImageView;->o8〇OO0〇0o:Z

    .line 58
    .line 59
    if-nez v2, :cond_1

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_1
    aget-object v2, p1, v1

    .line 63
    .line 64
    aput-object v2, p1, v0

    .line 65
    .line 66
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    .line 67
    .line 68
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 69
    .line 70
    .line 71
    move-result-object v3

    .line 72
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 73
    .line 74
    .line 75
    move-result-object v3

    .line 76
    iget-object v4, p0, Lcom/intsig/camscanner/view/RotateImageView;->o8oOOo:Landroid/graphics/Bitmap;

    .line 77
    .line 78
    invoke-direct {v2, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 79
    .line 80
    .line 81
    aput-object v2, p1, v1

    .line 82
    .line 83
    new-instance p1, Landroid/graphics/drawable/TransitionDrawable;

    .line 84
    .line 85
    iget-object v1, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇O〇〇O8:[Landroid/graphics/drawable/Drawable;

    .line 86
    .line 87
    invoke-direct {p1, v1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 88
    .line 89
    .line 90
    iput-object p1, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇o0O:Landroid/graphics/drawable/TransitionDrawable;

    .line 91
    .line 92
    invoke-virtual {p0, p1}, Lcom/intsig/view/SafeImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 93
    .line 94
    .line 95
    iget-object p1, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇o0O:Landroid/graphics/drawable/TransitionDrawable;

    .line 96
    .line 97
    const/16 v1, 0x1f4

    .line 98
    .line 99
    invoke-virtual {p1, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 100
    .line 101
    .line 102
    goto :goto_1

    .line 103
    :cond_2
    :goto_0
    const/4 p1, 0x2

    .line 104
    new-array p1, p1, [Landroid/graphics/drawable/Drawable;

    .line 105
    .line 106
    iput-object p1, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇O〇〇O8:[Landroid/graphics/drawable/Drawable;

    .line 107
    .line 108
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    .line 109
    .line 110
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 111
    .line 112
    .line 113
    move-result-object v3

    .line 114
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 115
    .line 116
    .line 117
    move-result-object v3

    .line 118
    iget-object v4, p0, Lcom/intsig/camscanner/view/RotateImageView;->o8oOOo:Landroid/graphics/Bitmap;

    .line 119
    .line 120
    invoke-direct {v2, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 121
    .line 122
    .line 123
    aput-object v2, p1, v1

    .line 124
    .line 125
    iget-object p1, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇O〇〇O8:[Landroid/graphics/drawable/Drawable;

    .line 126
    .line 127
    aget-object p1, p1, v1

    .line 128
    .line 129
    invoke-virtual {p0, p1}, Lcom/intsig/view/SafeImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 130
    .line 131
    .line 132
    :goto_1
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    .line 133
    .line 134
    .line 135
    move-result p1

    .line 136
    if-eqz p1, :cond_3

    .line 137
    .line 138
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 139
    .line 140
    .line 141
    :cond_3
    return-void
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public setDegree(I)V
    .locals 4

    .line 1
    if-ltz p1, :cond_0

    .line 2
    .line 3
    rem-int/lit16 p1, p1, 0x168

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    rem-int/lit16 p1, p1, 0x168

    .line 7
    .line 8
    add-int/lit16 p1, p1, 0x168

    .line 9
    .line 10
    :goto_0
    iget v0, p0, Lcom/intsig/camscanner/view/RotateImageView;->oOo0:I

    .line 11
    .line 12
    if-ne p1, v0, :cond_1

    .line 13
    .line 14
    return-void

    .line 15
    :cond_1
    iput p1, p0, Lcom/intsig/camscanner/view/RotateImageView;->oOo0:I

    .line 16
    .line 17
    iget p1, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇0O:I

    .line 18
    .line 19
    iput p1, p0, Lcom/intsig/camscanner/view/RotateImageView;->oOo〇8o008:I

    .line 20
    .line 21
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    .line 22
    .line 23
    .line 24
    move-result-wide v0

    .line 25
    iput-wide v0, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇8〇oO〇〇8o:J

    .line 26
    .line 27
    iget p1, p0, Lcom/intsig/camscanner/view/RotateImageView;->oOo0:I

    .line 28
    .line 29
    iget v2, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇0O:I

    .line 30
    .line 31
    sub-int/2addr p1, v2

    .line 32
    if-ltz p1, :cond_2

    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_2
    add-int/lit16 p1, p1, 0x168

    .line 36
    .line 37
    :goto_1
    const/16 v2, 0xb4

    .line 38
    .line 39
    if-le p1, v2, :cond_3

    .line 40
    .line 41
    add-int/lit16 p1, p1, -0x168

    .line 42
    .line 43
    :cond_3
    if-ltz p1, :cond_4

    .line 44
    .line 45
    const/4 v2, 0x1

    .line 46
    goto :goto_2

    .line 47
    :cond_4
    const/4 v2, 0x0

    .line 48
    :goto_2
    iput-boolean v2, p0, Lcom/intsig/camscanner/view/RotateImageView;->OO〇00〇8oO:Z

    .line 49
    .line 50
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    mul-int/lit16 p1, p1, 0x3e8

    .line 55
    .line 56
    div-int/lit16 p1, p1, 0x10e

    .line 57
    .line 58
    int-to-long v2, p1

    .line 59
    add-long/2addr v0, v2

    .line 60
    iput-wide v0, p0, Lcom/intsig/camscanner/view/RotateImageView;->ooo0〇〇O:J

    .line 61
    .line 62
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 63
    .line 64
    .line 65
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setDegree2(I)V
    .locals 1

    .line 1
    if-ltz p1, :cond_0

    .line 2
    .line 3
    rem-int/lit16 p1, p1, 0x168

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    rem-int/lit16 p1, p1, 0x168

    .line 7
    .line 8
    add-int/lit16 p1, p1, 0x168

    .line 9
    .line 10
    :goto_0
    iget v0, p0, Lcom/intsig/camscanner/view/RotateImageView;->oOo0:I

    .line 11
    .line 12
    if-ne p1, v0, :cond_1

    .line 13
    .line 14
    return-void

    .line 15
    :cond_1
    iput p1, p0, Lcom/intsig/camscanner/view/RotateImageView;->oOo0:I

    .line 16
    .line 17
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setEnableRotate(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/RotateImageView;->〇〇08O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOrientation(I)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/RotateImageView;->setDegree(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public abstract Lcom/intsig/camscanner/view/AbstractPullToSyncView;
.super Landroid/widget/RelativeLayout;
.source "AbstractPullToSyncView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;
    }
.end annotation


# instance fields
.field protected O8o08O8O:I

.field private OO:Z

.field private OO〇00〇8oO:Landroid/view/View;

.field private o0:I

.field private o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

.field private oOo0:Z

.field protected oOo〇8o008:I

.field private o〇00O:I

.field protected 〇080OO8〇0:I

.field private 〇08O〇00〇o:I

.field protected 〇0O:Z

.field private 〇8〇oO〇〇8o:Landroid/view/View;

.field private 〇OOo8〇0:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x0

    .line 5
    iput p1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->O8o08O8O:I

    .line 6
    .line 7
    iput p1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇080OO8〇0:I

    .line 8
    .line 9
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇0O:Z

    .line 10
    .line 11
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->oOo0:Z

    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o〇0()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private OO0o〇〇〇〇0(Landroid/view/View;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    .line 8
    .line 9
    const/4 v1, -0x1

    .line 10
    const/4 v2, -0x2

    .line 11
    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 12
    .line 13
    .line 14
    :cond_0
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 15
    .line 16
    const/4 v2, 0x0

    .line 17
    invoke-static {v2, v2, v1}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 22
    .line 23
    if-lez v0, :cond_1

    .line 24
    .line 25
    const/high16 v2, 0x40000000    # 2.0f

    .line 26
    .line 27
    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    goto :goto_0

    .line 32
    :cond_1
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private Oo08()V
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->O8o08O8O:I

    .line 3
    .line 4
    iget v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇08O〇00〇o:I

    .line 5
    .line 6
    neg-int v0, v0

    .line 7
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setNewScrollY(I)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 11
    .line 12
    invoke-interface {v0, p0}, Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;->OO0o〇〇〇〇0(Landroid/view/View;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o〇0()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x5

    .line 6
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iput v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o〇00O:I

    .line 11
    .line 12
    new-instance v0, Lcom/intsig/camscanner/view/header/DefaultHeaderViewStrategy;

    .line 13
    .line 14
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-direct {v0, v1, p0}, Lcom/intsig/camscanner/view/header/DefaultHeaderViewStrategy;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇080()V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private setNewScrollY(I)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0, p1}, Landroid/view/View;->scrollTo(II)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇080()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇8〇oO〇〇8o:Landroid/view/View;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;->〇〇888()Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iget-object v1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 15
    .line 16
    invoke-interface {v1}, Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;->Oooo8o0〇()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    iput v1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇08O〇00〇o:I

    .line 21
    .line 22
    if-gtz v1, :cond_1

    .line 23
    .line 24
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->OO0o〇〇〇〇0(Landroid/view/View;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    iput v1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇08O〇00〇o:I

    .line 32
    .line 33
    :cond_1
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 34
    .line 35
    const/4 v2, -0x1

    .line 36
    iget v3, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇08O〇00〇o:I

    .line 37
    .line 38
    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 39
    .line 40
    .line 41
    iget v2, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇08O〇00〇o:I

    .line 42
    .line 43
    neg-int v2, v2

    .line 44
    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 45
    .line 46
    iput-object v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇8〇oO〇〇8o:Landroid/view/View;

    .line 47
    .line 48
    invoke-virtual {p0, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private 〇o〇(I)I
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    int-to-float v1, p1

    .line 7
    const v2, 0x3f19999a    # 0.6f

    .line 8
    .line 9
    .line 10
    mul-float v1, v1, v2

    .line 11
    .line 12
    sub-float/2addr v0, v1

    .line 13
    float-to-int v0, v0

    .line 14
    const/4 v1, 0x1

    .line 15
    const/4 v2, 0x0

    .line 16
    if-gez p1, :cond_0

    .line 17
    .line 18
    iget v3, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇080OO8〇0:I

    .line 19
    .line 20
    if-ne v3, v1, :cond_0

    .line 21
    .line 22
    if-lez v0, :cond_0

    .line 23
    .line 24
    return v2

    .line 25
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    .line 30
    .line 31
    .line 32
    move-result-object v3

    .line 33
    const-string v4, "noshowloginonsync"

    .line 34
    .line 35
    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 40
    .line 41
    .line 42
    move-result-object v4

    .line 43
    invoke-static {v4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 44
    .line 45
    .line 46
    move-result v4

    .line 47
    if-nez v4, :cond_1

    .line 48
    .line 49
    if-eqz v3, :cond_1

    .line 50
    .line 51
    iget v3, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇080OO8〇0:I

    .line 52
    .line 53
    if-ne v3, v1, :cond_1

    .line 54
    .line 55
    if-lez p1, :cond_1

    .line 56
    .line 57
    iget p1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->oOo〇8o008:I

    .line 58
    .line 59
    neg-int p1, p1

    .line 60
    if-ge v0, p1, :cond_1

    .line 61
    .line 62
    return v2

    .line 63
    :cond_1
    invoke-virtual {p0, v2, v0}, Landroid/view/View;->scrollTo(II)V

    .line 64
    .line 65
    .line 66
    return v0
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method protected O8(I)V
    .locals 3

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇o〇(I)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 6
    .line 7
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;->〇o00〇〇Oo(I)V

    .line 8
    .line 9
    .line 10
    iget v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇08O〇00〇o:I

    .line 11
    .line 12
    neg-int v1, v0

    .line 13
    if-gt p1, v1, :cond_0

    .line 14
    .line 15
    iget v1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->O8o08O8O:I

    .line 16
    .line 17
    const/4 v2, 0x3

    .line 18
    if-eq v1, v2, :cond_0

    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 21
    .line 22
    invoke-interface {p1}, Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;->Oo08()V

    .line 23
    .line 24
    .line 25
    iput v2, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->O8o08O8O:I

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    if-gtz p1, :cond_1

    .line 29
    .line 30
    neg-int v0, v0

    .line 31
    iget v1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o〇00O:I

    .line 32
    .line 33
    add-int/2addr v0, v1

    .line 34
    if-le p1, v0, :cond_1

    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 37
    .line 38
    invoke-interface {p1}, Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;->〇o〇()V

    .line 39
    .line 40
    .line 41
    const/4 p1, 0x2

    .line 42
    iput p1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->O8o08O8O:I

    .line 43
    .line 44
    :cond_1
    :goto_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public OO0o〇〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;->oO80()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oooo8o0〇(F)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;->O8(F)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public oO80()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->oOo0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇〇888()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->O8o08O8O:I

    .line 2
    .line 3
    const/4 v1, 0x4

    .line 4
    const/4 v2, 0x1

    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    return v2

    .line 8
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->OO:Z

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    return p1

    .line 17
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    float-to-int v0, v0

    .line 22
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    float-to-int v1, v1

    .line 27
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    if-eqz p1, :cond_3

    .line 32
    .line 33
    const/4 v3, 0x2

    .line 34
    if-eq p1, v3, :cond_2

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_2
    iget p1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o0:I

    .line 38
    .line 39
    sub-int/2addr v0, p1

    .line 40
    iget p1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇OOo8〇0:I

    .line 41
    .line 42
    sub-int/2addr v1, p1

    .line 43
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    int-to-float p1, p1

    .line 48
    const/high16 v3, 0x41200000    # 10.0f

    .line 49
    .line 50
    cmpl-float p1, p1, v3

    .line 51
    .line 52
    if-lez p1, :cond_4

    .line 53
    .line 54
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    .line 55
    .line 56
    .line 57
    move-result p1

    .line 58
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    .line 59
    .line 60
    .line 61
    move-result v1

    .line 62
    if-le p1, v1, :cond_4

    .line 63
    .line 64
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇80〇808〇O(I)Z

    .line 65
    .line 66
    .line 67
    move-result p1

    .line 68
    if-eqz p1, :cond_4

    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 71
    .line 72
    invoke-interface {p1}, Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;->〇O8o08O()V

    .line 73
    .line 74
    .line 75
    return v2

    .line 76
    :cond_3
    iput v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o0:I

    .line 77
    .line 78
    iput v1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇OOo8〇0:I

    .line 79
    .line 80
    iput-boolean v2, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->oOo0:Z

    .line 81
    .line 82
    :cond_4
    :goto_0
    const/4 p1, 0x0

    .line 83
    return p1
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->OO:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1

    .line 10
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    float-to-int v0, v0

    .line 15
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_8

    .line 20
    .line 21
    const/4 v2, 0x3

    .line 22
    const/4 v3, 0x1

    .line 23
    if-eq v1, v3, :cond_3

    .line 24
    .line 25
    const/4 v4, 0x2

    .line 26
    if-eq v1, v4, :cond_1

    .line 27
    .line 28
    if-eq v1, v2, :cond_3

    .line 29
    .line 30
    goto :goto_2

    .line 31
    :cond_1
    iget v1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o0:I

    .line 32
    .line 33
    sub-int v1, v0, v1

    .line 34
    .line 35
    iget v2, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇080OO8〇0:I

    .line 36
    .line 37
    if-ne v2, v3, :cond_2

    .line 38
    .line 39
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->O8(I)V

    .line 40
    .line 41
    .line 42
    :cond_2
    iput v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o0:I

    .line 43
    .line 44
    goto :goto_2

    .line 45
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    iget v1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇080OO8〇0:I

    .line 50
    .line 51
    const/4 v4, 0x0

    .line 52
    if-ne v1, v3, :cond_7

    .line 53
    .line 54
    iget v1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇08O〇00〇o:I

    .line 55
    .line 56
    neg-int v5, v1

    .line 57
    if-lt v0, v5, :cond_6

    .line 58
    .line 59
    iget v5, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->O8o08O8O:I

    .line 60
    .line 61
    if-ne v5, v2, :cond_4

    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_4
    int-to-float v2, v0

    .line 65
    iget v5, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->oOo〇8o008:I

    .line 66
    .line 67
    neg-int v6, v5

    .line 68
    mul-int/lit8 v6, v6, 0x1

    .line 69
    .line 70
    int-to-float v3, v6

    .line 71
    const/high16 v6, 0x40000000    # 2.0f

    .line 72
    .line 73
    div-float/2addr v3, v6

    .line 74
    cmpg-float v2, v2, v3

    .line 75
    .line 76
    if-gez v2, :cond_5

    .line 77
    .line 78
    neg-int v1, v1

    .line 79
    iget v2, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o〇00O:I

    .line 80
    .line 81
    add-int/2addr v1, v2

    .line 82
    if-lt v0, v1, :cond_5

    .line 83
    .line 84
    iput v4, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇080OO8〇0:I

    .line 85
    .line 86
    neg-int v0, v5

    .line 87
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setNewScrollY(I)V

    .line 88
    .line 89
    .line 90
    iput-boolean v4, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇0O:Z

    .line 91
    .line 92
    goto :goto_1

    .line 93
    :cond_5
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setNewScrollY(I)V

    .line 94
    .line 95
    .line 96
    iput-boolean v4, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇0O:Z

    .line 97
    .line 98
    goto :goto_1

    .line 99
    :cond_6
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->Oo08()V

    .line 100
    .line 101
    .line 102
    :cond_7
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 103
    .line 104
    invoke-interface {v0, v4}, Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;->o〇0(Z)V

    .line 105
    .line 106
    .line 107
    goto :goto_2

    .line 108
    :cond_8
    iget-object v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 109
    .line 110
    invoke-interface {v0}, Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;->〇080()V

    .line 111
    .line 112
    .line 113
    :goto_2
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 114
    .line 115
    .line 116
    move-result p1

    .line 117
    return p1
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public setDownBeforeScroll(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->oOo0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setIHeaderViewStrategy(Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;)V
    .locals 0
    .param p1    # Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇080()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLock(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->OO:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setNormalMode(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;->OO0o〇〇(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOnHeaderRefreshListener(Lcom/intsig/camscanner/view/OnHeaderRefreshListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;->〇8o8o〇(Lcom/intsig/camscanner/view/OnHeaderRefreshListener;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOverlayView(Landroid/view/View;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->OO〇00〇8oO:Landroid/view/View;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public abstract 〇80〇808〇O(I)Z
.end method

.method public 〇8o8o〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->o8〇OO0〇0o:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;->〇80〇808〇O()V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇0O:Z

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->oOo〇8o008:I

    .line 12
    .line 13
    neg-int v0, v0

    .line 14
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setNewScrollY(I)V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setNewScrollY(I)V

    .line 19
    .line 20
    .line 21
    :goto_0
    iput v1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇080OO8〇0:I

    .line 22
    .line 23
    iput v1, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->O8o08O8O:I

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇O8o08O()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setNewScrollY(I)V

    .line 3
    .line 4
    .line 5
    const/4 v0, 0x2

    .line 6
    iput v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->O8o08O8O:I

    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    iput v0, p0, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇080OO8〇0:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public abstract 〇〇888()V
.end method

.class public Lcom/intsig/camscanner/view/RoundAngleLayout;
.super Landroid/widget/FrameLayout;
.source "RoundAngleLayout.java"


# instance fields
.field O8o08O8O:Landroid/graphics/Path;

.field private OO:I

.field private o0:I

.field private final o〇00O:Landroid/graphics/PaintFlagsDrawFilter;

.field private 〇08O〇00〇o:I

.field private 〇OOo8〇0:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x4

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->o0:I

    .line 3
    iput v0, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->〇OOo8〇0:I

    .line 4
    iput v0, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->OO:I

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->〇08O〇00〇o:I

    .line 6
    new-instance v0, Landroid/graphics/PaintFlagsDrawFilter;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Landroid/graphics/PaintFlagsDrawFilter;-><init>(II)V

    iput-object v0, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->o〇00O:Landroid/graphics/PaintFlagsDrawFilter;

    .line 7
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->O8o08O8O:Landroid/graphics/Path;

    .line 8
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/view/RoundAngleLayout;->〇080(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 9
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p3, 0x4

    .line 10
    iput p3, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->o0:I

    .line 11
    iput p3, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->〇OOo8〇0:I

    .line 12
    iput p3, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->OO:I

    .line 13
    iput p3, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->〇08O〇00〇o:I

    .line 14
    new-instance p3, Landroid/graphics/PaintFlagsDrawFilter;

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p3, v0, v1}, Landroid/graphics/PaintFlagsDrawFilter;-><init>(II)V

    iput-object p3, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->o〇00O:Landroid/graphics/PaintFlagsDrawFilter;

    .line 15
    new-instance p3, Landroid/graphics/Path;

    invoke-direct {p3}, Landroid/graphics/Path;-><init>()V

    iput-object p3, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->O8o08O8O:Landroid/graphics/Path;

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/view/RoundAngleLayout;->〇080(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private 〇080(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eqz p2, :cond_0

    .line 3
    .line 4
    sget-object v1, Lcom/intsig/camscanner/R$styleable;->RoundAngleImageView:[I

    .line 5
    .line 6
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    const/4 p2, 0x0

    .line 11
    invoke-virtual {p1, v0, p2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    iput v1, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->o0:I

    .line 16
    .line 17
    const/4 v1, 0x3

    .line 18
    invoke-virtual {p1, v1, p2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    iput v1, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->〇OOo8〇0:I

    .line 23
    .line 24
    const/4 v1, 0x2

    .line 25
    invoke-virtual {p1, v1, p2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    iput v1, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->OO:I

    .line 30
    .line 31
    invoke-virtual {p1, p2, p2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 32
    .line 33
    .line 34
    move-result p2

    .line 35
    iput p2, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->〇08O〇00〇o:I

    .line 36
    .line 37
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 38
    .line 39
    .line 40
    :cond_0
    const/4 p1, 0x0

    .line 41
    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const-string v2, "RoundAngleImageView"

    .line 10
    .line 11
    if-lez v1, :cond_3

    .line 12
    .line 13
    if-gtz v0, :cond_0

    .line 14
    .line 15
    goto/16 :goto_0

    .line 16
    .line 17
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    const-string v4, "leftTopRadius="

    .line 23
    .line 24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    iget v4, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->o0:I

    .line 28
    .line 29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    const-string v4, " rightTopRadius="

    .line 33
    .line 34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    iget v4, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->〇OOo8〇0:I

    .line 38
    .line 39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    iget v2, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->o0:I

    .line 50
    .line 51
    if-gtz v2, :cond_1

    .line 52
    .line 53
    iget v2, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->〇08O〇00〇o:I

    .line 54
    .line 55
    if-gtz v2, :cond_1

    .line 56
    .line 57
    iget v2, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->〇OOo8〇0:I

    .line 58
    .line 59
    if-gtz v2, :cond_1

    .line 60
    .line 61
    iget v2, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->OO:I

    .line 62
    .line 63
    if-lez v2, :cond_2

    .line 64
    .line 65
    :cond_1
    iget-object v2, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->O8o08O8O:Landroid/graphics/Path;

    .line 66
    .line 67
    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 68
    .line 69
    .line 70
    iget-object v2, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->O8o08O8O:Landroid/graphics/Path;

    .line 71
    .line 72
    iget v3, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->o0:I

    .line 73
    .line 74
    int-to-float v3, v3

    .line 75
    const/4 v4, 0x0

    .line 76
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 77
    .line 78
    .line 79
    iget-object v2, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->O8o08O8O:Landroid/graphics/Path;

    .line 80
    .line 81
    iget v3, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->〇OOo8〇0:I

    .line 82
    .line 83
    sub-int v3, v0, v3

    .line 84
    .line 85
    int-to-float v3, v3

    .line 86
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 87
    .line 88
    .line 89
    iget-object v2, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->O8o08O8O:Landroid/graphics/Path;

    .line 90
    .line 91
    int-to-float v3, v0

    .line 92
    iget v5, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->〇OOo8〇0:I

    .line 93
    .line 94
    int-to-float v5, v5

    .line 95
    invoke-virtual {v2, v3, v4, v3, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 96
    .line 97
    .line 98
    iget-object v2, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->O8o08O8O:Landroid/graphics/Path;

    .line 99
    .line 100
    iget v5, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->〇OOo8〇0:I

    .line 101
    .line 102
    sub-int v5, v1, v5

    .line 103
    .line 104
    int-to-float v5, v5

    .line 105
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 106
    .line 107
    .line 108
    iget-object v2, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->O8o08O8O:Landroid/graphics/Path;

    .line 109
    .line 110
    int-to-float v5, v1

    .line 111
    iget v6, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->OO:I

    .line 112
    .line 113
    sub-int/2addr v0, v6

    .line 114
    int-to-float v0, v0

    .line 115
    invoke-virtual {v2, v3, v5, v0, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 116
    .line 117
    .line 118
    iget-object v0, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->O8o08O8O:Landroid/graphics/Path;

    .line 119
    .line 120
    const/high16 v2, 0x41400000    # 12.0f

    .line 121
    .line 122
    invoke-virtual {v0, v2, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 123
    .line 124
    .line 125
    iget-object v0, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->O8o08O8O:Landroid/graphics/Path;

    .line 126
    .line 127
    iget v2, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->〇08O〇00〇o:I

    .line 128
    .line 129
    sub-int/2addr v1, v2

    .line 130
    int-to-float v1, v1

    .line 131
    invoke-virtual {v0, v4, v5, v4, v1}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 132
    .line 133
    .line 134
    iget-object v0, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->O8o08O8O:Landroid/graphics/Path;

    .line 135
    .line 136
    iget v1, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->o0:I

    .line 137
    .line 138
    int-to-float v1, v1

    .line 139
    invoke-virtual {v0, v4, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 140
    .line 141
    .line 142
    iget-object v0, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->O8o08O8O:Landroid/graphics/Path;

    .line 143
    .line 144
    iget v1, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->o0:I

    .line 145
    .line 146
    int-to-float v1, v1

    .line 147
    invoke-virtual {v0, v4, v4, v1, v4}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 148
    .line 149
    .line 150
    iget-object v0, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->o〇00O:Landroid/graphics/PaintFlagsDrawFilter;

    .line 151
    .line 152
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->setDrawFilter(Landroid/graphics/DrawFilter;)V

    .line 153
    .line 154
    .line 155
    iget-object v0, p0, Lcom/intsig/camscanner/view/RoundAngleLayout;->O8o08O8O:Landroid/graphics/Path;

    .line 156
    .line 157
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 158
    .line 159
    .line 160
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 161
    .line 162
    .line 163
    return-void

    .line 164
    :cond_3
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    .line 165
    .line 166
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 167
    .line 168
    .line 169
    const-string v4, "height="

    .line 170
    .line 171
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    .line 173
    .line 174
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 175
    .line 176
    .line 177
    const-string v1, " width="

    .line 178
    .line 179
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    .line 181
    .line 182
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 183
    .line 184
    .line 185
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v0

    .line 189
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    .line 191
    .line 192
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 193
    .line 194
    .line 195
    return-void
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

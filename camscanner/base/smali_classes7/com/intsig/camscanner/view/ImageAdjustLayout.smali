.class public Lcom/intsig/camscanner/view/ImageAdjustLayout;
.super Landroid/widget/FrameLayout;
.source "ImageAdjustLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;,
        Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;
    }
.end annotation


# static fields
.field private static final OO〇00〇8oO:Ljava/lang/String; = "ImageAdjustLayout"


# instance fields
.field private final O8o08O8O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

.field private OO:Lcom/intsig/utils/ClickLimit;

.field private o0:Landroidx/appcompat/widget/AppCompatSeekBar;

.field private oOo0:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private oOo〇8o008:[I

.field private final o〇00O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

.field private 〇080OO8〇0:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

.field private final 〇08O〇00〇o:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

.field private 〇0O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;

.field private 〇OOo8〇0:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->OO:Lcom/intsig/utils/ClickLimit;

    .line 3
    new-instance p2, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    const/4 v0, 0x0

    invoke-direct {p2, v0}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;-><init>(Lcom/intsig/camscanner/view/O8ooOoo〇;)V

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇08O〇00〇o:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 4
    new-instance p2, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    invoke-direct {p2, v0}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;-><init>(Lcom/intsig/camscanner/view/O8ooOoo〇;)V

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->o〇00O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 5
    new-instance p2, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    invoke-direct {p2, v0}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;-><init>(Lcom/intsig/camscanner/view/O8ooOoo〇;)V

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->O8o08O8O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    const/4 p2, 0x3

    new-array p2, p2, [I

    .line 6
    fill-array-data p2, :array_0

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->oOo〇8o008:[I

    .line 7
    new-instance p2, Lcom/intsig/camscanner/view/ImageAdjustLayout$1;

    invoke-direct {p2, p0}, Lcom/intsig/camscanner/view/ImageAdjustLayout$1;-><init>(Lcom/intsig/camscanner/view/ImageAdjustLayout;)V

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->oOo0:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 8
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->oO80(Landroid/content/Context;)V

    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇〇888()V

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0a009d
        0x7f0a009b
        0x7f0a009e
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 10
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 11
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->OO:Lcom/intsig/utils/ClickLimit;

    .line 12
    new-instance p2, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    const/4 p3, 0x0

    invoke-direct {p2, p3}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;-><init>(Lcom/intsig/camscanner/view/O8ooOoo〇;)V

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇08O〇00〇o:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 13
    new-instance p2, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    invoke-direct {p2, p3}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;-><init>(Lcom/intsig/camscanner/view/O8ooOoo〇;)V

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->o〇00O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 14
    new-instance p2, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    invoke-direct {p2, p3}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;-><init>(Lcom/intsig/camscanner/view/O8ooOoo〇;)V

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->O8o08O8O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    const/4 p2, 0x3

    new-array p2, p2, [I

    .line 15
    fill-array-data p2, :array_0

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->oOo〇8o008:[I

    .line 16
    new-instance p2, Lcom/intsig/camscanner/view/ImageAdjustLayout$1;

    invoke-direct {p2, p0}, Lcom/intsig/camscanner/view/ImageAdjustLayout$1;-><init>(Lcom/intsig/camscanner/view/ImageAdjustLayout;)V

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->oOo0:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 17
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->oO80(Landroid/content/Context;)V

    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇〇888()V

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0a009d
        0x7f0a009b
        0x7f0a009e
    .end array-data
.end method

.method static bridge synthetic O8(Lcom/intsig/camscanner/view/ImageAdjustLayout;)Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇0O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic Oo08(Lcom/intsig/camscanner/view/ImageAdjustLayout;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇OOo8〇0:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private oO80(Landroid/content/Context;)V
    .locals 3

    .line 1
    const-string v0, "layout_inflater"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Landroid/view/LayoutInflater;

    .line 8
    .line 9
    if-eqz p1, :cond_1

    .line 10
    .line 11
    const v0, 0x7f0d052f

    .line 12
    .line 13
    .line 14
    const/4 v1, 0x1

    .line 15
    invoke-virtual {p1, v0, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 16
    .line 17
    .line 18
    const p1, 0x7f0a009a

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    check-cast p1, Landroidx/appcompat/widget/AppCompatSeekBar;

    .line 26
    .line 27
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->o0:Landroidx/appcompat/widget/AppCompatSeekBar;

    .line 28
    .line 29
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 30
    .line 31
    const/16 v1, 0x16

    .line 32
    .line 33
    if-le v0, v1, :cond_0

    .line 34
    .line 35
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    const/4 v0, -0x1

    .line 40
    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->o0:Landroidx/appcompat/widget/AppCompatSeekBar;

    .line 43
    .line 44
    invoke-virtual {v0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 45
    .line 46
    .line 47
    :cond_0
    const p1, 0x7f0a009c

    .line 48
    .line 49
    .line 50
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    check-cast p1, Landroid/widget/TextView;

    .line 55
    .line 56
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇OOo8〇0:Landroid/widget/TextView;

    .line 57
    .line 58
    const p1, 0x7f0a0098

    .line 59
    .line 60
    .line 61
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    .line 67
    .line 68
    const p1, 0x7f0a0099

    .line 69
    .line 70
    .line 71
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    .line 77
    .line 78
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->oOo〇8o008:[I

    .line 79
    .line 80
    array-length v0, p1

    .line 81
    const/4 v1, 0x0

    .line 82
    :goto_0
    if-ge v1, v0, :cond_1

    .line 83
    .line 84
    aget v2, p1, v1

    .line 85
    .line 86
    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 87
    .line 88
    .line 89
    move-result-object v2

    .line 90
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    .line 92
    .line 93
    add-int/lit8 v1, v1, 0x1

    .line 94
    .line 95
    goto :goto_0

    .line 96
    :cond_1
    return-void
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method static bridge synthetic o〇0()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->OO〇00〇8oO:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private setCurrentImageAdjustData(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇080OO8〇0:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 2
    .line 3
    if-ne v0, p1, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇080OO8〇0:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇80〇808〇O()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/view/ImageAdjustLayout;)Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->o〇00O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/view/ImageAdjustLayout;)Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇08O〇00〇o:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/view/ImageAdjustLayout;)Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇080OO8〇0:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇〇888()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇08O〇00〇o:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 2
    .line 3
    const/16 v1, 0x32

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;->〇o〇(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;I)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->o〇00O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;->〇o〇(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;I)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->O8o08O8O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 14
    .line 15
    const/16 v1, 0x64

    .line 16
    .line 17
    invoke-static {v0, v1}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;->〇o〇(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;I)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇08O〇00〇o:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 21
    .line 22
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->setCurrentImageAdjustData(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method


# virtual methods
.method OO0o〇〇(Landroid/widget/TextView;Z)V
    .locals 0

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    const p2, -0xe74566

    .line 4
    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 p2, -0x1

    .line 8
    :goto_0
    invoke-static {p2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 9
    .line 10
    .line 11
    move-result-object p2

    .line 12
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 13
    .line 14
    .line 15
    invoke-static {p1, p2}, Landroidx/core/widget/TextViewCompat;->setCompoundDrawableTintList(Landroid/widget/TextView;Landroid/content/res/ColorStateList;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public OO0o〇〇〇〇0(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->o〇00O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;->〇o〇(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;I)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->o〇00O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 7
    .line 8
    invoke-static {p1, p2}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;->O8(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;I)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->OO:Lcom/intsig/utils/ClickLimit;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    const v0, 0x7f0a0098

    .line 15
    .line 16
    .line 17
    if-ne p1, v0, :cond_1

    .line 18
    .line 19
    sget-object p1, Lcom/intsig/camscanner/view/ImageAdjustLayout;->OO〇00〇8oO:Ljava/lang/String;

    .line 20
    .line 21
    const-string v0, "reset"

    .line 22
    .line 23
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇〇888()V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇0O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;

    .line 30
    .line 31
    if-eqz p1, :cond_8

    .line 32
    .line 33
    invoke-interface {p1}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;->reset()V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_1
    const v0, 0x7f0a0099

    .line 38
    .line 39
    .line 40
    if-ne p1, v0, :cond_2

    .line 41
    .line 42
    sget-object p1, Lcom/intsig/camscanner/view/ImageAdjustLayout;->OO〇00〇8oO:Ljava/lang/String;

    .line 43
    .line 44
    const-string v0, "save"

    .line 45
    .line 46
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇0O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;

    .line 50
    .line 51
    if-eqz p1, :cond_8

    .line 52
    .line 53
    invoke-interface {p1}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;->o〇8()V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_2
    const v0, 0x7f0a009d

    .line 58
    .line 59
    .line 60
    if-ne p1, v0, :cond_4

    .line 61
    .line 62
    sget-object p1, Lcom/intsig/camscanner/view/ImageAdjustLayout;->OO〇00〇8oO:Ljava/lang/String;

    .line 63
    .line 64
    const-string v0, "contrast"

    .line 65
    .line 66
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇0O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;

    .line 70
    .line 71
    if-eqz p1, :cond_3

    .line 72
    .line 73
    invoke-interface {p1}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;->〇080()V

    .line 74
    .line 75
    .line 76
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇08O〇00〇o:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 77
    .line 78
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->setCurrentImageAdjustData(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;)V

    .line 79
    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_4
    const v0, 0x7f0a009b

    .line 83
    .line 84
    .line 85
    if-ne p1, v0, :cond_6

    .line 86
    .line 87
    sget-object p1, Lcom/intsig/camscanner/view/ImageAdjustLayout;->OO〇00〇8oO:Ljava/lang/String;

    .line 88
    .line 89
    const-string v0, "brightness"

    .line 90
    .line 91
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇0O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;

    .line 95
    .line 96
    if-eqz p1, :cond_5

    .line 97
    .line 98
    invoke-interface {p1}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;->Oo08()V

    .line 99
    .line 100
    .line 101
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->o〇00O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 102
    .line 103
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->setCurrentImageAdjustData(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;)V

    .line 104
    .line 105
    .line 106
    goto :goto_0

    .line 107
    :cond_6
    const v0, 0x7f0a009e

    .line 108
    .line 109
    .line 110
    if-ne p1, v0, :cond_8

    .line 111
    .line 112
    sget-object p1, Lcom/intsig/camscanner/view/ImageAdjustLayout;->OO〇00〇8oO:Ljava/lang/String;

    .line 113
    .line 114
    const-string v0, "detail"

    .line 115
    .line 116
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇0O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;

    .line 120
    .line 121
    if-eqz p1, :cond_7

    .line 122
    .line 123
    invoke-interface {p1}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;->〇o00〇〇Oo()V

    .line 124
    .line 125
    .line 126
    :cond_7
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->O8o08O8O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 127
    .line 128
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->setCurrentImageAdjustData(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;)V

    .line 129
    .line 130
    .line 131
    :cond_8
    :goto_0
    return-void
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public setImageAdjustListener(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇0O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method setSelected(I)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->oOo〇8o008:[I

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    const/4 v2, 0x0

    .line 5
    const/4 v3, 0x0

    .line 6
    :goto_0
    if-ge v3, v1, :cond_1

    .line 7
    .line 8
    aget v4, v0, v3

    .line 9
    .line 10
    invoke-virtual {p0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object v5

    .line 14
    check-cast v5, Landroid/widget/TextView;

    .line 15
    .line 16
    if-ne v4, p1, :cond_0

    .line 17
    .line 18
    const/4 v4, 0x1

    .line 19
    goto :goto_1

    .line 20
    :cond_0
    const/4 v4, 0x0

    .line 21
    :goto_1
    invoke-virtual {p0, v5, v4}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->OO0o〇〇(Landroid/widget/TextView;Z)V

    .line 22
    .line 23
    .line 24
    add-int/lit8 v3, v3, 0x1

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇80〇808〇O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇080OO8〇0:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->o0:Landroidx/appcompat/widget/AppCompatSeekBar;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;->〇o00〇〇Oo(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;)I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->o0:Landroidx/appcompat/widget/AppCompatSeekBar;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇080OO8〇0:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 18
    .line 19
    invoke-static {v1}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;->〇080(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;)I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->o0:Landroidx/appcompat/widget/AppCompatSeekBar;

    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->oOo0:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇080OO8〇0:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇08O〇00〇o:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 36
    .line 37
    if-ne v0, v1, :cond_1

    .line 38
    .line 39
    const v0, 0x7f0a009d

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->setSelected(I)V

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->o〇00O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 47
    .line 48
    if-ne v0, v1, :cond_2

    .line 49
    .line 50
    const v0, 0x7f0a009b

    .line 51
    .line 52
    .line 53
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->setSelected(I)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_2
    const v0, 0x7f0a009e

    .line 58
    .line 59
    .line 60
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->setSelected(I)V

    .line 61
    .line 62
    .line 63
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇OOo8〇0:Landroid/widget/TextView;

    .line 64
    .line 65
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇080OO8〇0:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 66
    .line 67
    invoke-static {v1}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;->〇080(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;)I

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    .line 77
    .line 78
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇8o8o〇(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇08O〇00〇o:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;->〇o〇(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;I)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇08O〇00〇o:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 7
    .line 8
    invoke-static {p1, p2}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;->O8(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;I)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇O8o08O(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->O8o08O8O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;->〇o〇(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;I)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageAdjustLayout;->O8o08O8O:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;

    .line 7
    .line 8
    invoke-static {p1, p2}, Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;->O8(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustValueData;I)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

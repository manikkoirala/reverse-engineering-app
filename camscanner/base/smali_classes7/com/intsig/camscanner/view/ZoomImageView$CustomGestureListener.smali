.class Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ZoomImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/view/ZoomImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomGestureListener"
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/view/ZoomImageView;


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/view/ZoomImageView;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/view/ZoomImageView;Lcom/intsig/camscanner/view/O8O〇;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;-><init>(Lcom/intsig/camscanner/view/ZoomImageView;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 9

    .line 1
    const-string v0, "ZoomImageView"

    .line 2
    .line 3
    const-string v1, "ScaleGestureListener onDoubleTap"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    invoke-static {v0, v1}, Lcom/intsig/camscanner/view/ZoomImageView;->〇〇0o(Lcom/intsig/camscanner/view/ZoomImageView;Z)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    const v2, 0x3f99999a    # 1.2f

    .line 21
    .line 22
    .line 23
    invoke-static {v0, v2}, Ljava/lang/Float;->compare(FF)I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-lez v0, :cond_0

    .line 28
    .line 29
    const/high16 v0, 0x3f800000    # 1.0f

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 33
    .line 34
    invoke-static {v0}, Lcom/intsig/camscanner/view/ZoomImageView;->o〇8(Lcom/intsig/camscanner/view/ZoomImageView;)F

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    const/4 v2, 0x0

    .line 39
    cmpl-float v0, v0, v2

    .line 40
    .line 41
    if-lez v0, :cond_1

    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 44
    .line 45
    invoke-static {v0}, Lcom/intsig/camscanner/view/ZoomImageView;->o〇8(Lcom/intsig/camscanner/view/ZoomImageView;)F

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    goto :goto_0

    .line 50
    :cond_1
    const/high16 v0, 0x40400000    # 3.0f

    .line 51
    .line 52
    :goto_0
    iget-object v2, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 53
    .line 54
    invoke-virtual {v2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 55
    .line 56
    .line 57
    move-result v3

    .line 58
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 59
    .line 60
    .line 61
    move-result v5

    .line 62
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 63
    .line 64
    .line 65
    move-result v6

    .line 66
    const-wide/16 v7, 0xc8

    .line 67
    .line 68
    move v4, v0

    .line 69
    invoke-static/range {v2 .. v8}, Lcom/intsig/camscanner/view/ZoomImageView;->O08000(Lcom/intsig/camscanner/view/ZoomImageView;FFFFJ)V

    .line 70
    .line 71
    .line 72
    iget-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 73
    .line 74
    invoke-static {p1}, Lcom/intsig/camscanner/view/ZoomImageView;->〇〇〇0〇〇0(Lcom/intsig/camscanner/view/ZoomImageView;)Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;

    .line 75
    .line 76
    .line 77
    move-result-object p1

    .line 78
    if-eqz p1, :cond_2

    .line 79
    .line 80
    iget-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 81
    .line 82
    invoke-static {p1}, Lcom/intsig/camscanner/view/ZoomImageView;->〇〇〇0〇〇0(Lcom/intsig/camscanner/view/ZoomImageView;)Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;

    .line 83
    .line 84
    .line 85
    move-result-object p1

    .line 86
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;->〇o00〇〇Oo(F)V

    .line 87
    .line 88
    .line 89
    :cond_2
    return v1
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    const/4 p1, 0x1

    .line 2
    return p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 2
    .line 3
    invoke-static {p1}, Lcom/intsig/camscanner/view/ZoomImageView;->o8(Lcom/intsig/camscanner/view/ZoomImageView;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/4 p2, 0x0

    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 11
    .line 12
    invoke-static {p1, p2}, Lcom/intsig/camscanner/view/ZoomImageView;->o〇0OOo〇0(Lcom/intsig/camscanner/view/ZoomImageView;Z)V

    .line 13
    .line 14
    .line 15
    return p2

    .line 16
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    const/high16 v0, 0x3f800000    # 1.0f

    .line 23
    .line 24
    cmpl-float p1, p1, v0

    .line 25
    .line 26
    if-lez p1, :cond_3

    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 29
    .line 30
    neg-float v0, p3

    .line 31
    neg-float p4, p4

    .line 32
    invoke-virtual {p1, v0, p4}, Lcom/intsig/camscanner/view/ImageViewTouch;->O8〇o(FF)Landroid/graphics/PointF;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    iget p1, p1, Landroid/graphics/PointF;->x:F

    .line 37
    .line 38
    const/4 p4, 0x0

    .line 39
    const/4 v0, 0x1

    .line 40
    cmpl-float p1, p1, p4

    .line 41
    .line 42
    if-lez p1, :cond_1

    .line 43
    .line 44
    const/4 p2, 0x1

    .line 45
    :cond_1
    xor-int/2addr p2, v0

    .line 46
    cmpl-float p1, p3, p4

    .line 47
    .line 48
    if-lez p1, :cond_2

    .line 49
    .line 50
    iget-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 51
    .line 52
    invoke-static {p1, v0}, Lcom/intsig/camscanner/view/ZoomImageView;->oO(Lcom/intsig/camscanner/view/ZoomImageView;Z)V

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_2
    cmpg-float p1, p3, p4

    .line 57
    .line 58
    if-gez p1, :cond_3

    .line 59
    .line 60
    iget-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 61
    .line 62
    invoke-static {p1, v0}, Lcom/intsig/camscanner/view/ZoomImageView;->〇8(Lcom/intsig/camscanner/view/ZoomImageView;Z)V

    .line 63
    .line 64
    .line 65
    :cond_3
    :goto_0
    return p2
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/view/ZoomImageView;->〇〇〇0〇〇0(Lcom/intsig/camscanner/view/ZoomImageView;)Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    float-to-int v1, v1

    .line 16
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    float-to-int p1, p1

    .line 21
    invoke-virtual {v0, v1, p1}, Lcom/intsig/camscanner/view/ImageViewTouch;->〇0000OOO(II)Z

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    if-eqz p1, :cond_0

    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 28
    .line 29
    invoke-static {p1}, Lcom/intsig/camscanner/view/ZoomImageView;->〇〇〇0〇〇0(Lcom/intsig/camscanner/view/ZoomImageView;)Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 34
    .line 35
    iget-object v0, v0, Lcom/intsig/camscanner/view/ImageViewTouch;->o〇oO:Lcom/intsig/camscanner/capture/qrcode/model/CsBarResultModel;

    .line 36
    .line 37
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;->〇080(Lcom/intsig/camscanner/capture/qrcode/model/CsBarResultModel;)V

    .line 38
    .line 39
    .line 40
    const/4 p1, 0x1

    .line 41
    return p1

    .line 42
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/view/ZoomImageView$CustomGestureListener;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 43
    .line 44
    invoke-static {p1}, Lcom/intsig/camscanner/view/ZoomImageView;->〇〇〇0〇〇0(Lcom/intsig/camscanner/view/ZoomImageView;)Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    invoke-interface {p1}, Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;->Oo08()Z

    .line 49
    .line 50
    .line 51
    move-result p1

    .line 52
    return p1

    .line 53
    :cond_1
    const/4 p1, 0x0

    .line 54
    return p1
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

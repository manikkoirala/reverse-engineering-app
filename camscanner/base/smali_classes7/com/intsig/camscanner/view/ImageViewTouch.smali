.class public Lcom/intsig/camscanner/view/ImageViewTouch;
.super Lcom/intsig/camscanner/view/ImageViewTouchBase;
.source "ImageViewTouch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/view/ImageViewTouch$OcrAnimationListener;
    }
.end annotation


# static fields
.field private static o8O:Landroid/graphics/Paint;


# instance fields
.field private O88O:Lcom/intsig/nativelib/OcrArea;

.field private O8o〇O0:I

.field private O8〇o〇88:Z

.field private OO〇OOo:Landroid/graphics/RectF;

.field private Oo0O0o8:Landroid/graphics/RectF;

.field private Oo0〇Ooo:F

.field private Oo80:Z

.field private final Ooo08:I

.field private O〇08oOOO0:I

.field private O〇O:Landroid/graphics/Bitmap;

.field private O〇o88o08〇:Landroid/graphics/Bitmap;

.field private o0OoOOo0:I

.field private o8o:Landroid/graphics/Matrix;

.field private o8〇OO:I

.field private oO00〇o:I

.field private oOO0880O:I

.field private oOO8:I

.field private oOO〇〇:Landroid/graphics/Matrix;

.field private oOoo80oO:Landroid/graphics/Rect;

.field private oO〇8O8oOo:I

.field private oo8ooo8O:Z

.field private final ooO:[F

.field protected o〇oO:Lcom/intsig/camscanner/capture/qrcode/model/CsBarResultModel;

.field private final o〇o〇Oo88:I

.field private 〇00O0:I

.field private 〇08〇o0O:I

.field private 〇0O〇O00O:I

.field private 〇800OO〇0O:J

.field public 〇8〇o88:Landroid/graphics/Rect;

.field private final 〇OO8ooO8〇:I

.field private 〇OO〇00〇0O:I

.field private 〇oo〇O〇80:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private 〇〇o〇:Z

.field private 〇〇〇0o〇〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8O:Landroid/graphics/Paint;

    .line 7
    .line 8
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 11
    .line 12
    .line 13
    sget-object v0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8O:Landroid/graphics/Paint;

    .line 14
    .line 15
    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 18
    .line 19
    .line 20
    sget-object v0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8O:Landroid/graphics/Paint;

    .line 21
    .line 22
    const/16 v1, -0x100

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 25
    .line 26
    .line 27
    sget-object v0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8O:Landroid/graphics/Paint;

    .line 28
    .line 29
    const/16 v1, 0x78

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8o:Landroid/graphics/Matrix;

    const/4 p1, 0x1

    .line 3
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oo8ooo8O:Z

    const/4 p1, 0x0

    .line 4
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇〇o〇:Z

    .line 5
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo80:Z

    .line 6
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8〇OO:I

    const/16 v0, 0x10

    .line 7
    iput v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Ooo08:I

    const/16 v1, 0x8

    .line 8
    iput v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇OO8ooO8〇:I

    new-array v0, v0, [F

    .line 9
    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->ooO:[F

    .line 10
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇〇〇0o〇〇0:I

    .line 11
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oO〇8O8oOo:I

    .line 12
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇0O〇O00O:I

    const/4 v0, 0x5

    .line 13
    iput v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o〇o〇Oo88:I

    .line 14
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOoo80oO:Landroid/graphics/Rect;

    .line 15
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo0O0o8:Landroid/graphics/RectF;

    .line 16
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->OO〇OOo:Landroid/graphics/RectF;

    const-wide/16 v0, 0x0

    .line 17
    iput-wide v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇800OO〇0O:J

    const/16 v0, 0x1e

    .line 18
    iput v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O8o〇O0:I

    const/16 v0, 0x3c

    .line 19
    iput v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOO8:I

    .line 20
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O8〇o〇88:Z

    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/view/ImageViewTouch;->〇8o8o〇()V

    return-void

    nop

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        0x0
        -0x40cccccd    # -0.7f
        -0x40cccccd    # -0.7f
        0x0
        -0x40800000    # -1.0f
        0x3f333333    # 0.7f
        -0x40cccccd    # -0.7f
        0x3f800000    # 1.0f
        0x0
        0x3f333333    # 0.7f
        0x3f333333    # 0.7f
        0x0
        0x3f800000    # 1.0f
        -0x40cccccd    # -0.7f
        0x3f333333    # 0.7f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8o:Landroid/graphics/Matrix;

    const/4 p1, 0x1

    .line 24
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oo8ooo8O:Z

    const/4 p1, 0x0

    .line 25
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇〇o〇:Z

    .line 26
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo80:Z

    .line 27
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8〇OO:I

    const/16 p2, 0x10

    .line 28
    iput p2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Ooo08:I

    const/16 v0, 0x8

    .line 29
    iput v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇OO8ooO8〇:I

    new-array p2, p2, [F

    .line 30
    fill-array-data p2, :array_0

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->ooO:[F

    .line 31
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇〇〇0o〇〇0:I

    .line 32
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oO〇8O8oOo:I

    .line 33
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇0O〇O00O:I

    const/4 p2, 0x5

    .line 34
    iput p2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o〇o〇Oo88:I

    .line 35
    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2}, Landroid/graphics/Rect;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOoo80oO:Landroid/graphics/Rect;

    .line 36
    new-instance p2, Landroid/graphics/RectF;

    invoke-direct {p2}, Landroid/graphics/RectF;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo0O0o8:Landroid/graphics/RectF;

    .line 37
    new-instance p2, Landroid/graphics/RectF;

    invoke-direct {p2}, Landroid/graphics/RectF;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->OO〇OOo:Landroid/graphics/RectF;

    const-wide/16 v0, 0x0

    .line 38
    iput-wide v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇800OO〇0O:J

    const/16 p2, 0x1e

    .line 39
    iput p2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O8o〇O0:I

    const/16 p2, 0x3c

    .line 40
    iput p2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOO8:I

    .line 41
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O8〇o〇88:Z

    .line 42
    invoke-direct {p0}, Lcom/intsig/camscanner/view/ImageViewTouch;->〇8o8o〇()V

    return-void

    nop

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        0x0
        -0x40cccccd    # -0.7f
        -0x40cccccd    # -0.7f
        0x0
        -0x40800000    # -1.0f
        0x3f333333    # 0.7f
        -0x40cccccd    # -0.7f
        0x3f800000    # 1.0f
        0x0
        0x3f333333    # 0.7f
        0x3f333333    # 0.7f
        0x0
        0x3f800000    # 1.0f
        -0x40cccccd    # -0.7f
        0x3f333333    # 0.7f
    .end array-data
.end method

.method private OOO〇O0(Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇oo〇O〇80:Ljava/util/ArrayList;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x0

    .line 7
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇oo〇O〇80:Ljava/util/ArrayList;

    .line 8
    .line 9
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-ge v0, v1, :cond_1

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇oo〇O〇80:Ljava/util/ArrayList;

    .line 16
    .line 17
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    check-cast v1, Landroid/graphics/Rect;

    .line 22
    .line 23
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8o:Landroid/graphics/Matrix;

    .line 24
    .line 25
    iget-object v3, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOO〇〇:Landroid/graphics/Matrix;

    .line 26
    .line 27
    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 28
    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8o:Landroid/graphics/Matrix;

    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 37
    .line 38
    .line 39
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8o:Landroid/graphics/Matrix;

    .line 40
    .line 41
    iget-object v3, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo0O0o8:Landroid/graphics/RectF;

    .line 42
    .line 43
    new-instance v4, Landroid/graphics/RectF;

    .line 44
    .line 45
    invoke-direct {v4, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 49
    .line 50
    .line 51
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo0O0o8:Landroid/graphics/RectF;

    .line 52
    .line 53
    sget-object v2, Lcom/intsig/camscanner/view/ImageViewTouch;->o8O:Landroid/graphics/Paint;

    .line 54
    .line 55
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 56
    .line 57
    .line 58
    add-int/lit8 v0, v0, 0x1

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_1
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private 〇00(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 11

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8〇OO:I

    .line 2
    .line 3
    const/16 v1, 0x10

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    if-ge v0, v1, :cond_7

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOoo80oO:Landroid/graphics/Rect;

    .line 9
    .line 10
    invoke-static {p2, v0}, Lcom/intsig/camscanner/util/Util;->O880oOO08(Landroid/graphics/RectF;Landroid/graphics/Rect;)V

    .line 11
    .line 12
    .line 13
    iget v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o0OoOOo0:I

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    rem-int/2addr v0, v1

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    iget v0, p2, Landroid/graphics/RectF;->left:F

    .line 20
    .line 21
    iget v3, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo0〇Ooo:F

    .line 22
    .line 23
    add-float/2addr v0, v3

    .line 24
    iget v4, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇00O0:I

    .line 25
    .line 26
    int-to-float v4, v4

    .line 27
    add-float/2addr v0, v4

    .line 28
    iget p2, p2, Landroid/graphics/RectF;->top:F

    .line 29
    .line 30
    add-float/2addr p2, v3

    .line 31
    iget v3, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O〇08oOOO0:I

    .line 32
    .line 33
    int-to-float v3, v3

    .line 34
    add-float/2addr p2, v3

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    iget v0, p2, Landroid/graphics/RectF;->right:F

    .line 37
    .line 38
    iget v3, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo0〇Ooo:F

    .line 39
    .line 40
    sub-float/2addr v0, v3

    .line 41
    iget v4, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇00O0:I

    .line 42
    .line 43
    int-to-float v4, v4

    .line 44
    sub-float/2addr v0, v4

    .line 45
    iget p2, p2, Landroid/graphics/RectF;->bottom:F

    .line 46
    .line 47
    sub-float/2addr p2, v3

    .line 48
    iget v3, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O〇08oOOO0:I

    .line 49
    .line 50
    int-to-float v3, v3

    .line 51
    sub-float/2addr p2, v3

    .line 52
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 53
    .line 54
    .line 55
    move-result-wide v3

    .line 56
    iget-wide v5, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇800OO〇0O:J

    .line 57
    .line 58
    const-wide/16 v7, 0x0

    .line 59
    .line 60
    cmp-long v9, v5, v7

    .line 61
    .line 62
    if-nez v9, :cond_1

    .line 63
    .line 64
    const-wide/16 v5, 0x28

    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_1
    sub-long v5, v3, v5

    .line 68
    .line 69
    :goto_1
    iget v7, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o0OoOOo0:I

    .line 70
    .line 71
    if-ne v7, v2, :cond_3

    .line 72
    .line 73
    iget v7, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8〇OO:I

    .line 74
    .line 75
    if-ne v7, v1, :cond_3

    .line 76
    .line 77
    const-wide/16 v7, 0x2d

    .line 78
    .line 79
    cmp-long v9, v5, v7

    .line 80
    .line 81
    if-lez v9, :cond_2

    .line 82
    .line 83
    const/16 v7, 0x19

    .line 84
    .line 85
    iput v7, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O8o〇O0:I

    .line 86
    .line 87
    goto :goto_2

    .line 88
    :cond_2
    const-wide/16 v7, 0x26

    .line 89
    .line 90
    cmp-long v9, v5, v7

    .line 91
    .line 92
    if-gez v9, :cond_3

    .line 93
    .line 94
    const/16 v7, 0x23

    .line 95
    .line 96
    iput v7, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O8o〇O0:I

    .line 97
    .line 98
    :cond_3
    :goto_2
    iget v7, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O8o〇O0:I

    .line 99
    .line 100
    int-to-long v8, v7

    .line 101
    cmp-long v10, v5, v8

    .line 102
    .line 103
    if-gez v10, :cond_4

    .line 104
    .line 105
    goto :goto_3

    .line 106
    :cond_4
    iget v8, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOO8:I

    .line 107
    .line 108
    int-to-long v8, v8

    .line 109
    cmp-long v10, v5, v8

    .line 110
    .line 111
    if-gez v10, :cond_5

    .line 112
    .line 113
    iget v5, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8〇OO:I

    .line 114
    .line 115
    rem-int/lit8 v5, v5, 0x8

    .line 116
    .line 117
    iget-object v6, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->ooO:[F

    .line 118
    .line 119
    mul-int/lit8 v5, v5, 0x2

    .line 120
    .line 121
    aget v7, v6, v5

    .line 122
    .line 123
    iget v8, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo0〇Ooo:F

    .line 124
    .line 125
    mul-float v7, v7, v8

    .line 126
    .line 127
    add-float/2addr v0, v7

    .line 128
    iget v7, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇00O0:I

    .line 129
    .line 130
    int-to-float v7, v7

    .line 131
    sub-float/2addr v0, v7

    .line 132
    add-int/2addr v5, v2

    .line 133
    aget v5, v6, v5

    .line 134
    .line 135
    mul-float v5, v5, v8

    .line 136
    .line 137
    add-float/2addr p2, v5

    .line 138
    iget v5, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O〇08oOOO0:I

    .line 139
    .line 140
    int-to-float v5, v5

    .line 141
    sub-float/2addr p2, v5

    .line 142
    iget-object v5, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O〇o88o08〇:Landroid/graphics/Bitmap;

    .line 143
    .line 144
    const/4 v6, 0x0

    .line 145
    invoke-virtual {p1, v5, v0, p2, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 146
    .line 147
    .line 148
    iput-wide v3, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇800OO〇0O:J

    .line 149
    .line 150
    iget p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8〇OO:I

    .line 151
    .line 152
    add-int/2addr p1, v2

    .line 153
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8〇OO:I

    .line 154
    .line 155
    goto :goto_3

    .line 156
    :cond_5
    iget p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8〇OO:I

    .line 157
    .line 158
    const/16 p2, 0xf

    .line 159
    .line 160
    if-eq p1, p2, :cond_6

    .line 161
    .line 162
    sub-int/2addr v7, v1

    .line 163
    iput v7, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O8o〇O0:I

    .line 164
    .line 165
    :cond_6
    iput-wide v3, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇800OO〇0O:J

    .line 166
    .line 167
    add-int/2addr p1, v2

    .line 168
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8〇OO:I

    .line 169
    .line 170
    :goto_3
    iget p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O8o〇O0:I

    .line 171
    .line 172
    int-to-long v3, p1

    .line 173
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOoo80oO:Landroid/graphics/Rect;

    .line 174
    .line 175
    iget p2, p1, Landroid/graphics/Rect;->left:I

    .line 176
    .line 177
    iget v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇00O0:I

    .line 178
    .line 179
    mul-int/lit8 v2, v0, 0x2

    .line 180
    .line 181
    sub-int v5, p2, v2

    .line 182
    .line 183
    iget p2, p1, Landroid/graphics/Rect;->top:I

    .line 184
    .line 185
    iget v2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O〇08oOOO0:I

    .line 186
    .line 187
    mul-int/lit8 v6, v2, 0x2

    .line 188
    .line 189
    sub-int v6, p2, v6

    .line 190
    .line 191
    iget p2, p1, Landroid/graphics/Rect;->right:I

    .line 192
    .line 193
    mul-int/lit8 v0, v0, 0x2

    .line 194
    .line 195
    add-int v7, p2, v0

    .line 196
    .line 197
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 198
    .line 199
    mul-int/lit8 v2, v2, 0x2

    .line 200
    .line 201
    add-int v8, p1, v2

    .line 202
    .line 203
    move-object v2, p0

    .line 204
    invoke-virtual/range {v2 .. v8}, Landroid/view/View;->postInvalidateDelayed(JIIII)V

    .line 205
    .line 206
    .line 207
    goto :goto_4

    .line 208
    :cond_7
    iget p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oO〇8O8oOo:I

    .line 209
    .line 210
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇0O〇O00O:I

    .line 211
    .line 212
    iget p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o0OoOOo0:I

    .line 213
    .line 214
    const/4 p2, 0x5

    .line 215
    if-ge p1, p2, :cond_8

    .line 216
    .line 217
    add-int/2addr p1, v2

    .line 218
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o0OoOOo0:I

    .line 219
    .line 220
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/ImageViewTouch;->〇o(I)V

    .line 221
    .line 222
    .line 223
    :cond_8
    :goto_4
    return-void
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private 〇8o8o〇()V
    .locals 4

    .line 1
    const-string v0, "ImageViewTouch"

    .line 2
    .line 3
    :try_start_0
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const v2, 0x7f08100f

    .line 8
    .line 9
    .line 10
    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    iput-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O〇o88o08〇:Landroid/graphics/Bitmap;

    .line 15
    .line 16
    if-nez v1, :cond_0

    .line 17
    .line 18
    const-string v1, "mKonb == null"

    .line 19
    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    int-to-float v1, v1

    .line 29
    const/high16 v2, 0x3f800000    # 1.0f

    .line 30
    .line 31
    mul-float v1, v1, v2

    .line 32
    .line 33
    const/high16 v3, 0x40000000    # 2.0f

    .line 34
    .line 35
    div-float/2addr v1, v3

    .line 36
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    iput v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O〇08oOOO0:I

    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O〇o88o08〇:Landroid/graphics/Bitmap;

    .line 43
    .line 44
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    int-to-float v1, v1

    .line 49
    mul-float v1, v1, v2

    .line 50
    .line 51
    div-float/2addr v1, v3

    .line 52
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    iput v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇00O0:I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :catch_0
    move-exception v1

    .line 60
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61
    .line 62
    .line 63
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    const v1, 0x7f0704ed

    .line 68
    .line 69
    .line 70
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    iput v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇OO〇00〇0O:I

    .line 75
    .line 76
    mul-int/lit8 v0, v0, 0xa

    .line 77
    .line 78
    int-to-float v0, v0

    .line 79
    iput v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo0〇Ooo:F

    .line 80
    .line 81
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private 〇oOO8O8(II)Landroid/graphics/RectF;
    .locals 5

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4
    .line 5
    .line 6
    :goto_0
    if-ge p1, p2, :cond_1

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O88O:Lcom/intsig/nativelib/OcrArea;

    .line 9
    .line 10
    invoke-virtual {v1, p1}, Lcom/intsig/nativelib/OcrArea;->getRectAt(I)Landroid/graphics/Rect;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8o:Landroid/graphics/Matrix;

    .line 17
    .line 18
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOO〇〇:Landroid/graphics/Matrix;

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 21
    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8o:Landroid/graphics/Matrix;

    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 30
    .line 31
    .line 32
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8o:Landroid/graphics/Matrix;

    .line 33
    .line 34
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo0O0o8:Landroid/graphics/RectF;

    .line 35
    .line 36
    new-instance v3, Landroid/graphics/RectF;

    .line 37
    .line 38
    iget-object v4, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O88O:Lcom/intsig/nativelib/OcrArea;

    .line 39
    .line 40
    invoke-virtual {v4, p1}, Lcom/intsig/nativelib/OcrArea;->getRectAt(I)Landroid/graphics/Rect;

    .line 41
    .line 42
    .line 43
    move-result-object v4

    .line 44
    invoke-direct {v3, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 48
    .line 49
    .line 50
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo0O0o8:Landroid/graphics/RectF;

    .line 51
    .line 52
    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 53
    .line 54
    .line 55
    :cond_0
    add-int/lit8 p1, p1, 0x1

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_1
    return-object v0
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method


# virtual methods
.method public O8ooOoo〇(Landroid/graphics/Canvas;I)V
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    if-ge v0, p2, :cond_1

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O88O:Lcom/intsig/nativelib/OcrArea;

    .line 5
    .line 6
    invoke-virtual {v1, v0}, Lcom/intsig/nativelib/OcrArea;->getRectAt(I)Landroid/graphics/Rect;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8o:Landroid/graphics/Matrix;

    .line 13
    .line 14
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOO〇〇:Landroid/graphics/Matrix;

    .line 15
    .line 16
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 17
    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8o:Landroid/graphics/Matrix;

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 26
    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8o:Landroid/graphics/Matrix;

    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo0O0o8:Landroid/graphics/RectF;

    .line 31
    .line 32
    new-instance v3, Landroid/graphics/RectF;

    .line 33
    .line 34
    iget-object v4, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O88O:Lcom/intsig/nativelib/OcrArea;

    .line 35
    .line 36
    invoke-virtual {v4, v0}, Lcom/intsig/nativelib/OcrArea;->getRectAt(I)Landroid/graphics/Rect;

    .line 37
    .line 38
    .line 39
    move-result-object v4

    .line 40
    invoke-direct {v3, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 44
    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo0O0o8:Landroid/graphics/RectF;

    .line 47
    .line 48
    sget-object v2, Lcom/intsig/camscanner/view/ImageViewTouch;->o8O:Landroid/graphics/Paint;

    .line 49
    .line 50
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 51
    .line 52
    .line 53
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_1
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public O8〇o(FF)Landroid/graphics/PointF;
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇O00(FF)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x1

    .line 5
    invoke-virtual {p0, p1, p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o〇0(ZZ)Landroid/graphics/PointF;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public O〇8O8〇008(Landroid/graphics/Canvas;)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oO〇8O8oOo:I

    .line 2
    .line 3
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/view/ImageViewTouch;->O8ooOoo〇(Landroid/graphics/Canvas;I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->OO〇OOo:Landroid/graphics/RectF;

    .line 7
    .line 8
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/view/ImageViewTouch;->〇00(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getImageBottom()I
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    const/16 v1, 0x9

    .line 14
    .line 15
    new-array v1, v1, [F

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 18
    .line 19
    .line 20
    const/4 v0, 0x5

    .line 21
    aget v0, v1, v0

    .line 22
    .line 23
    float-to-int v0, v0

    .line 24
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    int-to-float v2, v2

    .line 37
    const/4 v3, 0x0

    .line 38
    aget v1, v1, v3

    .line 39
    .line 40
    mul-float v2, v2, v1

    .line 41
    .line 42
    float-to-int v1, v2

    .line 43
    add-int/2addr v0, v1

    .line 44
    goto :goto_0

    .line 45
    :cond_0
    const/4 v0, -0x1

    .line 46
    :goto_0
    return v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/view/SafeImageView;->onDetachedFromWindow()V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O8〇o〇88:Z

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O〇o88o08〇:Landroid/graphics/Bitmap;

    .line 13
    .line 14
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->OOo0O(Landroid/graphics/Bitmap;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/view/SafeImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oo8ooo8O:Z

    .line 5
    .line 6
    if-eqz v0, :cond_3

    .line 7
    .line 8
    iget v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇08〇o0O:I

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    if-ne v0, v1, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O88O:Lcom/intsig/nativelib/OcrArea;

    .line 14
    .line 15
    if-eqz v0, :cond_3

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/nativelib/OcrArea;->getRectNum()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-lez v0, :cond_3

    .line 22
    .line 23
    iput-boolean v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo80:Z

    .line 24
    .line 25
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/ImageViewTouch;->O〇8O8〇008(Landroid/graphics/Canvas;)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    const/4 v2, 0x2

    .line 30
    if-ne v0, v2, :cond_2

    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O88O:Lcom/intsig/nativelib/OcrArea;

    .line 33
    .line 34
    if-eqz v0, :cond_3

    .line 35
    .line 36
    invoke-virtual {v0}, Lcom/intsig/nativelib/OcrArea;->getRectNum()I

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    if-lez v0, :cond_3

    .line 41
    .line 42
    iget v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇〇〇0o〇〇0:I

    .line 43
    .line 44
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O88O:Lcom/intsig/nativelib/OcrArea;

    .line 45
    .line 46
    invoke-virtual {v2}, Lcom/intsig/nativelib/OcrArea;->getRectNum()I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    if-ge v0, v2, :cond_3

    .line 51
    .line 52
    iput-boolean v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo80:Z

    .line 53
    .line 54
    iget v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇〇〇0o〇〇0:I

    .line 55
    .line 56
    add-int/2addr v0, v1

    .line 57
    iput v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇〇〇0o〇〇0:I

    .line 58
    .line 59
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/view/ImageViewTouch;->O8ooOoo〇(Landroid/graphics/Canvas;I)V

    .line 60
    .line 61
    .line 62
    iget v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇〇〇0o〇〇0:I

    .line 63
    .line 64
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O88O:Lcom/intsig/nativelib/OcrArea;

    .line 65
    .line 66
    invoke-virtual {v1}, Lcom/intsig/nativelib/OcrArea;->getRectNum()I

    .line 67
    .line 68
    .line 69
    move-result v1

    .line 70
    if-ne v0, v1, :cond_1

    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_1
    iget v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇〇〇0o〇〇0:I

    .line 74
    .line 75
    add-int/lit8 v1, v0, 0x1

    .line 76
    .line 77
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/view/ImageViewTouch;->〇oOO8O8(II)Landroid/graphics/RectF;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->OO〇OOo:Landroid/graphics/RectF;

    .line 82
    .line 83
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOoo80oO:Landroid/graphics/Rect;

    .line 84
    .line 85
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/Util;->O880oOO08(Landroid/graphics/RectF;Landroid/graphics/Rect;)V

    .line 86
    .line 87
    .line 88
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOoo80oO:Landroid/graphics/Rect;

    .line 89
    .line 90
    const/16 v1, -0xa

    .line 91
    .line 92
    invoke-virtual {v0, v1, v1}, Landroid/graphics/Rect;->inset(II)V

    .line 93
    .line 94
    .line 95
    const-wide/16 v3, 0x23

    .line 96
    .line 97
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOoo80oO:Landroid/graphics/Rect;

    .line 98
    .line 99
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 100
    .line 101
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 102
    .line 103
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 104
    .line 105
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    .line 106
    .line 107
    move-object v2, p0

    .line 108
    invoke-virtual/range {v2 .. v8}, Landroid/view/View;->postInvalidateDelayed(JIIII)V

    .line 109
    .line 110
    .line 111
    goto :goto_0

    .line 112
    :cond_2
    const/4 v1, 0x3

    .line 113
    if-ne v0, v1, :cond_3

    .line 114
    .line 115
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/view/ImageViewTouch;->OOO〇O0(Landroid/graphics/Canvas;)V

    .line 116
    .line 117
    .line 118
    :cond_3
    :goto_0
    invoke-static {}, Lcom/intsig/camscanner/experiment/QRCodeOptExp;->〇080()Z

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    if-eqz v0, :cond_5

    .line 123
    .line 124
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o〇oO:Lcom/intsig/camscanner/capture/qrcode/model/CsBarResultModel;

    .line 125
    .line 126
    if-eqz v0, :cond_5

    .line 127
    .line 128
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O〇O:Landroid/graphics/Bitmap;

    .line 129
    .line 130
    if-nez v0, :cond_4

    .line 131
    .line 132
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 133
    .line 134
    .line 135
    move-result-object v0

    .line 136
    const v1, 0x7f080b53

    .line 137
    .line 138
    .line 139
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 140
    .line 141
    .line 142
    move-result-object v0

    .line 143
    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O〇O:Landroid/graphics/Bitmap;

    .line 144
    .line 145
    :cond_4
    new-instance v0, Landroid/graphics/Rect;

    .line 146
    .line 147
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 148
    .line 149
    .line 150
    move-result-object v1

    .line 151
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 152
    .line 153
    .line 154
    move-result-object v1

    .line 155
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 156
    .line 157
    .line 158
    move-result v1

    .line 159
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 160
    .line 161
    .line 162
    move-result-object v2

    .line 163
    const/16 v3, 0x28

    .line 164
    .line 165
    invoke-static {v2, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 166
    .line 167
    .line 168
    move-result v2

    .line 169
    sub-int/2addr v1, v2

    .line 170
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouch;->getImageBottom()I

    .line 171
    .line 172
    .line 173
    move-result v2

    .line 174
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 175
    .line 176
    .line 177
    move-result-object v4

    .line 178
    invoke-static {v4, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 179
    .line 180
    .line 181
    move-result v3

    .line 182
    sub-int/2addr v2, v3

    .line 183
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 184
    .line 185
    .line 186
    move-result-object v3

    .line 187
    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 188
    .line 189
    .line 190
    move-result-object v3

    .line 191
    invoke-static {v3}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 192
    .line 193
    .line 194
    move-result v3

    .line 195
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 196
    .line 197
    .line 198
    move-result-object v4

    .line 199
    const/16 v5, 0xa

    .line 200
    .line 201
    invoke-static {v4, v5}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 202
    .line 203
    .line 204
    move-result v4

    .line 205
    sub-int/2addr v3, v4

    .line 206
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouch;->getImageBottom()I

    .line 207
    .line 208
    .line 209
    move-result v4

    .line 210
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 211
    .line 212
    .line 213
    move-result-object v6

    .line 214
    invoke-static {v6, v5}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 215
    .line 216
    .line 217
    move-result v5

    .line 218
    sub-int/2addr v4, v5

    .line 219
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 220
    .line 221
    .line 222
    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇8〇o88:Landroid/graphics/Rect;

    .line 223
    .line 224
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O〇O:Landroid/graphics/Bitmap;

    .line 225
    .line 226
    const/4 v2, 0x0

    .line 227
    invoke-virtual {p1, v1, v2, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 228
    .line 229
    .line 230
    :cond_5
    return-void
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public oo〇(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oo8ooo8O:Z

    .line 3
    .line 4
    const/4 v0, 0x3

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇08〇o0O:I

    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇oo〇O〇80:Ljava/util/ArrayList;

    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public o〇〇0〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->Oo80:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setListView(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O8〇o〇88:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMatrix(Landroid/graphics/Matrix;)V
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/Matrix;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOO〇〇:Landroid/graphics/Matrix;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOcrAnimationListener(Lcom/intsig/camscanner/view/ImageViewTouch$OcrAnimationListener;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOcrArea(Lcom/intsig/nativelib/OcrArea;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->O88O:Lcom/intsig/nativelib/OcrArea;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOcrEnable(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oo8ooo8O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setQRCodeResult(Lcom/intsig/camscanner/capture/qrcode/model/CsBarResultModel;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o〇oO:Lcom/intsig/camscanner/capture/qrcode/model/CsBarResultModel;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected 〇0000OOO(II)Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/experiment/QRCodeOptExp;->〇080()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇8〇o88:Landroid/graphics/Rect;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    const/4 p1, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    :goto_0
    return p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇00〇8()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇08〇o0O:I

    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o(I)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o8〇OO:I

    .line 3
    .line 4
    iput p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->o0OoOOo0:I

    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oO00〇o:I

    .line 7
    .line 8
    if-gt p1, v0, :cond_0

    .line 9
    .line 10
    iget v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOO0880O:I

    .line 11
    .line 12
    add-int/lit8 v0, v0, 0x1

    .line 13
    .line 14
    mul-int v0, v0, p1

    .line 15
    .line 16
    iput v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oO〇8O8oOo:I

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    iget v1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOO0880O:I

    .line 20
    .line 21
    mul-int p1, p1, v1

    .line 22
    .line 23
    add-int/2addr v0, p1

    .line 24
    iput v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oO〇8O8oOo:I

    .line 25
    .line 26
    :goto_0
    iget p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->〇0O〇O00O:I

    .line 27
    .line 28
    iget v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oO〇8O8oOo:I

    .line 29
    .line 30
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/view/ImageViewTouch;->〇oOO8O8(II)Landroid/graphics/RectF;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    iput-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->OO〇OOo:Landroid/graphics/RectF;

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOoo80oO:Landroid/graphics/Rect;

    .line 37
    .line 38
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/Util;->O880oOO08(Landroid/graphics/RectF;Landroid/graphics/Rect;)V

    .line 39
    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOoo80oO:Landroid/graphics/Rect;

    .line 42
    .line 43
    const/16 v0, -0xa

    .line 44
    .line 45
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Rect;->inset(II)V

    .line 46
    .line 47
    .line 48
    iget-object p1, p0, Lcom/intsig/camscanner/view/ImageViewTouch;->oOoo80oO:Landroid/graphics/Rect;

    .line 49
    .line 50
    invoke-virtual {p0, p1}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    .line 51
    .line 52
    .line 53
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

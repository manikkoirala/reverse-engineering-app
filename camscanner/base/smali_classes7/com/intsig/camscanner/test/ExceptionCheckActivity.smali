.class public final Lcom/intsig/camscanner/test/ExceptionCheckActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "ExceptionCheckActivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/test/ExceptionCheckActivity$ClassUtils;,
        Lcom/intsig/camscanner/test/ExceptionCheckActivity$ResumedActivityCallback;,
        Lcom/intsig/camscanner/test/ExceptionCheckActivity$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static Oo80:Lcom/intsig/camscanner/test/ExceptionCheckActivity$ResumedActivityCallback;

.field public static final 〇08〇o0O:Lcom/intsig/camscanner/test/ExceptionCheckActivity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic 〇〇o〇:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private O0O:J

.field private O88O:Ljava/lang/String;

.field private o8o:J

.field private o8oOOo:J

.field private oOO〇〇:I

.field private oo8ooo8O:I

.field private final ooo0〇〇O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇oO:I

.field private 〇O〇〇O8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o0O:Landroid/os/Handler;

.field private final 〇〇08O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "binding"

    .line 7
    .line 8
    const-string v3, "getBinding()Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/test/ExceptionCheckActivity;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇〇o〇:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/test/ExceptionCheckActivity$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/test/ExceptionCheckActivity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇08〇o0O:Lcom/intsig/camscanner/test/ExceptionCheckActivity$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, "ExceptionCheckActivity"

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 9
    .line 10
    const-class v1, Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;

    .line 11
    .line 12
    invoke-direct {v0, v1, p0}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;-><init>(Ljava/lang/Class;Landroid/app/Activity;)V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇〇08O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 16
    .line 17
    const-wide/16 v0, -0x1

    .line 18
    .line 19
    iput-wide v0, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O0O:J

    .line 20
    .line 21
    iput-wide v0, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8oOOo:J

    .line 22
    .line 23
    const-string v0, ""

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 26
    .line 27
    const-wide/16 v0, 0xbb8

    .line 28
    .line 29
    iput-wide v0, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private static final O00OoO〇(Landroid/app/Activity;)V
    .locals 2

    .line 1
    if-eqz p0, :cond_0

    .line 2
    .line 3
    new-instance v0, Landroid/content/Intent;

    .line 4
    .line 5
    const-class v1, Lcom/intsig/camscanner/test/ExceptionCheckActivity;

    .line 6
    .line 7
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic O0〇(Landroid/app/Activity;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O00OoO〇(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final O88()V
    .locals 7
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;->〇08O〇00〇o:Landroid/widget/LinearLayout;

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    move-object v0, v1

    .line 12
    :goto_0
    if-nez v0, :cond_1

    .line 13
    .line 14
    goto :goto_1

    .line 15
    :cond_1
    const/16 v2, 0x8

    .line 16
    .line 17
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 18
    .line 19
    .line 20
    :goto_1
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    if-eqz v0, :cond_2

    .line 25
    .line 26
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;->〇OOo8〇0:Landroid/widget/EditText;

    .line 27
    .line 28
    if-eqz v0, :cond_2

    .line 29
    .line 30
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    if-eqz v0, :cond_2

    .line 35
    .line 36
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    if-eqz v0, :cond_2

    .line 41
    .line 42
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 43
    .line 44
    .line 45
    move-result-wide v2

    .line 46
    goto :goto_2

    .line 47
    :cond_2
    const-wide/16 v2, 0xbb8

    .line 48
    .line 49
    :goto_2
    iput-wide v2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    .line 51
    goto :goto_3

    .line 52
    :catchall_0
    move-exception v0

    .line 53
    iget-object v2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 54
    .line 55
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 56
    .line 57
    .line 58
    :goto_3
    const/4 v0, 0x0

    .line 59
    :try_start_1
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    if-eqz v2, :cond_3

    .line 64
    .line 65
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;->OO:Landroid/widget/EditText;

    .line 66
    .line 67
    if-eqz v2, :cond_3

    .line 68
    .line 69
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    if-eqz v2, :cond_3

    .line 74
    .line 75
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v2

    .line 79
    if-eqz v2, :cond_3

    .line 80
    .line 81
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 82
    .line 83
    .line 84
    move-result v2

    .line 85
    goto :goto_4

    .line 86
    :cond_3
    const/4 v2, 0x0

    .line 87
    :goto_4
    iput v2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->oo8ooo8O:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 88
    .line 89
    goto :goto_5

    .line 90
    :catchall_1
    move-exception v2

    .line 91
    iget-object v3, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 92
    .line 93
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 94
    .line 95
    .line 96
    :goto_5
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v2

    .line 100
    new-instance v3, Landroid/os/HandlerThread;

    .line 101
    .line 102
    const-string v4, "activity_check"

    .line 103
    .line 104
    invoke-direct {v3, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 108
    .line 109
    .line 110
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->OoO〇OOo8o()Ljava/util/List;

    .line 111
    .line 112
    .line 113
    move-result-object v4

    .line 114
    sget-object v5, Lcom/intsig/camscanner/test/ExceptionCheckActivity$ClassUtils;->〇080:Lcom/intsig/camscanner/test/ExceptionCheckActivity$ClassUtils;

    .line 115
    .line 116
    invoke-virtual {v5, p0, v2, v4}, Lcom/intsig/camscanner/test/ExceptionCheckActivity$ClassUtils;->〇080(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    .line 117
    .line 118
    .line 119
    move-result-object v2

    .line 120
    move-object v4, v2

    .line 121
    check-cast v4, Ljava/util/Collection;

    .line 122
    .line 123
    if-eqz v4, :cond_5

    .line 124
    .line 125
    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    .line 126
    .line 127
    .line 128
    move-result v4

    .line 129
    if-eqz v4, :cond_4

    .line 130
    .line 131
    goto :goto_6

    .line 132
    :cond_4
    const/4 v4, 0x0

    .line 133
    goto :goto_7

    .line 134
    :cond_5
    :goto_6
    const/4 v4, 0x1

    .line 135
    :goto_7
    if-eqz v4, :cond_6

    .line 136
    .line 137
    return-void

    .line 138
    :cond_6
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 139
    .line 140
    .line 141
    move-result-object v4

    .line 142
    iput v0, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o〇oO:I

    .line 143
    .line 144
    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    .line 145
    .line 146
    .line 147
    move-result-object v3

    .line 148
    new-instance v5, Lcom/intsig/camscanner/test/ExceptionCheckActivity$startCheckException$1;

    .line 149
    .line 150
    invoke-direct {v5, p0, v2, v4, v3}, Lcom/intsig/camscanner/test/ExceptionCheckActivity$startCheckException$1;-><init>(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Ljava/util/List;Ljava/util/Iterator;Landroid/os/Looper;)V

    .line 151
    .line 152
    .line 153
    iput-object v5, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 154
    .line 155
    iget-object v3, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 156
    .line 157
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 158
    .line 159
    .line 160
    move-result v4

    .line 161
    new-instance v5, Ljava/lang/StringBuilder;

    .line 162
    .line 163
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 164
    .line 165
    .line 166
    const-string v6, "startCheckException, and we have "

    .line 167
    .line 168
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    .line 170
    .line 171
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 172
    .line 173
    .line 174
    const-string v4, " activities to start"

    .line 175
    .line 176
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    .line 178
    .line 179
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 180
    .line 181
    .line 182
    move-result-object v4

    .line 183
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    .line 185
    .line 186
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;

    .line 187
    .line 188
    .line 189
    move-result-object v3

    .line 190
    if-eqz v3, :cond_7

    .line 191
    .line 192
    iget-object v1, v3, Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 193
    .line 194
    :cond_7
    if-nez v1, :cond_8

    .line 195
    .line 196
    goto :goto_8

    .line 197
    :cond_8
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 198
    .line 199
    .line 200
    move-result v2

    .line 201
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 202
    .line 203
    .line 204
    move-result-object v2

    .line 205
    new-instance v3, Ljava/lang/StringBuilder;

    .line 206
    .line 207
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 208
    .line 209
    .line 210
    const-string v4, "3\u79d2\u540e\u5f00\u59cb\u6267\u884c\u68c0\u67e5\u64cd\u4f5c\uff0c\u4f1a\u5f00\u59cb\u81ea\u52a8\u8df3\u8f6c\u9875\u9762\uff0c\u4e2d\u95f4\u8bf7\u52ff\u64cd\u4f5c\u3002\n \u5171\u641c\u7d22\u5230"

    .line 211
    .line 212
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    .line 214
    .line 215
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 216
    .line 217
    .line 218
    const-string v2, " \u4e2a\u9875\u9762"

    .line 219
    .line 220
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    .line 222
    .line 223
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 224
    .line 225
    .line 226
    move-result-object v2

    .line 227
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    .line 229
    .line 230
    :goto_8
    iget-object v1, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 231
    .line 232
    if-eqz v1, :cond_9

    .line 233
    .line 234
    iget-wide v2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 235
    .line 236
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 237
    .line 238
    .line 239
    :cond_9
    return-void
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public static final synthetic O880O〇(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Ljava/util/List;Ljava/util/Iterator;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇oOO80o(Ljava/util/List;Ljava/util/Iterator;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private final O8O(Ljava/lang/Class;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)Z"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x1

    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    invoke-virtual {p1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    if-eqz v2, :cond_0

    .line 10
    .line 11
    const-string v3, "HwPushMsgLoadActivity"

    .line 12
    .line 13
    invoke-static {v2, v3, v1}, Lkotlin/text/StringsKt;->o〇8(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    if-ne v2, v1, :cond_0

    .line 18
    .line 19
    const/4 v2, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v2, 0x0

    .line 22
    :goto_0
    if-eqz v2, :cond_1

    .line 23
    .line 24
    return v1

    .line 25
    :cond_1
    if-eqz p1, :cond_2

    .line 26
    .line 27
    invoke-virtual {p1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    if-eqz v2, :cond_2

    .line 32
    .line 33
    const-string v3, "MiPushLoadActivity"

    .line 34
    .line 35
    invoke-static {v2, v3, v1}, Lkotlin/text/StringsKt;->o〇8(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    if-ne v2, v1, :cond_2

    .line 40
    .line 41
    const/4 v2, 0x1

    .line 42
    goto :goto_1

    .line 43
    :cond_2
    const/4 v2, 0x0

    .line 44
    :goto_1
    if-eqz v2, :cond_3

    .line 45
    .line 46
    return v1

    .line 47
    :cond_3
    if-eqz p1, :cond_4

    .line 48
    .line 49
    invoke-virtual {p1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    if-eqz v2, :cond_4

    .line 54
    .line 55
    const-string v3, "OPPOPushMsgActivity"

    .line 56
    .line 57
    invoke-static {v2, v3, v1}, Lkotlin/text/StringsKt;->o〇8(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    .line 58
    .line 59
    .line 60
    move-result v2

    .line 61
    if-ne v2, v1, :cond_4

    .line 62
    .line 63
    const/4 v2, 0x1

    .line 64
    goto :goto_2

    .line 65
    :cond_4
    const/4 v2, 0x0

    .line 66
    :goto_2
    if-eqz v2, :cond_5

    .line 67
    .line 68
    return v1

    .line 69
    :cond_5
    if-eqz p1, :cond_6

    .line 70
    .line 71
    invoke-virtual {p1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    if-eqz v2, :cond_6

    .line 76
    .line 77
    const-string v3, "VivoPushMsgActivity"

    .line 78
    .line 79
    invoke-static {v2, v3, v1}, Lkotlin/text/StringsKt;->o〇8(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    .line 80
    .line 81
    .line 82
    move-result v2

    .line 83
    if-ne v2, v1, :cond_6

    .line 84
    .line 85
    const/4 v2, 0x1

    .line 86
    goto :goto_3

    .line 87
    :cond_6
    const/4 v2, 0x0

    .line 88
    :goto_3
    if-eqz v2, :cond_7

    .line 89
    .line 90
    return v1

    .line 91
    :cond_7
    if-eqz p1, :cond_8

    .line 92
    .line 93
    invoke-virtual {p1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v2

    .line 97
    if-eqz v2, :cond_8

    .line 98
    .line 99
    const-string v3, "AuthActivity"

    .line 100
    .line 101
    invoke-static {v2, v3, v1}, Lkotlin/text/StringsKt;->o〇8(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    .line 102
    .line 103
    .line 104
    move-result v2

    .line 105
    if-ne v2, v1, :cond_8

    .line 106
    .line 107
    const/4 v2, 0x1

    .line 108
    goto :goto_4

    .line 109
    :cond_8
    const/4 v2, 0x0

    .line 110
    :goto_4
    if-eqz v2, :cond_9

    .line 111
    .line 112
    return v1

    .line 113
    :cond_9
    if-eqz p1, :cond_a

    .line 114
    .line 115
    invoke-virtual {p1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v2

    .line 119
    if-eqz v2, :cond_a

    .line 120
    .line 121
    const-string v3, "FullScreenITActivity"

    .line 122
    .line 123
    invoke-static {v2, v3, v1}, Lkotlin/text/StringsKt;->o〇8(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    .line 124
    .line 125
    .line 126
    move-result v2

    .line 127
    if-ne v2, v1, :cond_a

    .line 128
    .line 129
    const/4 v2, 0x1

    .line 130
    goto :goto_5

    .line 131
    :cond_a
    const/4 v2, 0x0

    .line 132
    :goto_5
    if-eqz v2, :cond_b

    .line 133
    .line 134
    return v1

    .line 135
    :cond_b
    if-eqz p1, :cond_c

    .line 136
    .line 137
    invoke-virtual {p1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    move-result-object v2

    .line 141
    if-eqz v2, :cond_c

    .line 142
    .line 143
    const-string v3, "CloudDiskActivity"

    .line 144
    .line 145
    invoke-static {v2, v3, v1}, Lkotlin/text/StringsKt;->o〇8(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    .line 146
    .line 147
    .line 148
    move-result v2

    .line 149
    if-ne v2, v1, :cond_c

    .line 150
    .line 151
    const/4 v2, 0x1

    .line 152
    goto :goto_6

    .line 153
    :cond_c
    const/4 v2, 0x0

    .line 154
    :goto_6
    if-eqz v2, :cond_d

    .line 155
    .line 156
    return v1

    .line 157
    :cond_d
    if-eqz p1, :cond_e

    .line 158
    .line 159
    invoke-virtual {p1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    .line 160
    .line 161
    .line 162
    move-result-object p1

    .line 163
    if-eqz p1, :cond_e

    .line 164
    .line 165
    const-string v2, "CertificatePreviewActivity"

    .line 166
    .line 167
    invoke-static {p1, v2, v1}, Lkotlin/text/StringsKt;->o〇8(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    .line 168
    .line 169
    .line 170
    move-result p1

    .line 171
    if-ne p1, v1, :cond_e

    .line 172
    .line 173
    const/4 p1, 0x1

    .line 174
    goto :goto_7

    .line 175
    :cond_e
    const/4 p1, 0x0

    .line 176
    :goto_7
    if-eqz p1, :cond_f

    .line 177
    .line 178
    return v1

    .line 179
    :cond_f
    return v0
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public static final synthetic OO0O(Lcom/intsig/camscanner/test/ExceptionCheckActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final OO〇〇o0oO()V
    .locals 3

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8oOOo:J

    .line 2
    .line 3
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇OoO0o0(J)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇00〇8(Ljava/lang/String;)Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-nez v1, :cond_2

    .line 12
    .line 13
    invoke-static {p0}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇888(Landroid/app/Activity;)Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_3

    .line 18
    .line 19
    invoke-static {p0}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    if-nez v1, :cond_0

    .line 24
    .line 25
    const v0, 0x7f13008d

    .line 26
    .line 27
    .line 28
    invoke-static {p0, v0}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    .line 29
    .line 30
    .line 31
    return-void

    .line 32
    :cond_0
    invoke-static {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    if-nez v1, :cond_1

    .line 37
    .line 38
    const-string v0, "log in please, in order to get Raw Image"

    .line 39
    .line 40
    invoke-static {p0, v0}, Lcom/intsig/utils/ToastUtils;->〇80〇808〇O(Landroid/content/Context;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    return-void

    .line 44
    :cond_1
    new-instance v1, Lcom/intsig/utils/CommonLoadingTask;

    .line 45
    .line 46
    new-instance v2, Lcom/intsig/camscanner/test/ExceptionCheckActivity$checkRawImageAndStartCheckException$loadRawImageTask$1;

    .line 47
    .line 48
    invoke-direct {v2, p0, v0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity$checkRawImageAndStartCheckException$loadRawImageTask$1;-><init>(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    const-string v0, "\u52a0\u8f7d\u539f\u56fe\u4e2d"

    .line 52
    .line 53
    invoke-direct {v1, p0, v2, v0}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/utils/CommonLoadingTask;->O8()V

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O88()V

    .line 61
    .line 62
    .line 63
    :cond_3
    :goto_0
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private final OoO〇OOo8o()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Class<",
            "*>;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/test/ExceptionCheckActivity;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    const-class v1, Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    const-class v1, Lcom/intsig/camscanner/settings/PdfSettingActivity;

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    const-class v1, Lcom/intsig/camscanner/guide/GuideGpActivity;

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    const-class v1, Lcom/intsig/camscanner/test/docjson/DocJsonTestActivity;

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    const-class v1, Lcom/intsig/camscanner/translate/LanguageSelectActivity;

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    const-class v1, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    const-class v1, Lcom/intsig/camscanner/scenariodir/cardpack/EditOverseaCardDetailInfoActivity;

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    const-class v1, Lcom/intsig/camscanner/bankcardjournal/BankCardJournalResultActivity;

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    iput v1, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->oOO〇〇:I

    .line 56
    .line 57
    return-object v0
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static final synthetic OooO〇(Lcom/intsig/camscanner/test/ExceptionCheckActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O88()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic O〇080〇o0(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O〇〇o8O(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Lkotlin/collections/IndexedValue;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o0OO(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Lkotlin/collections/IndexedValue;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic O〇〇O80o8(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o0O0O〇〇〇0(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static final O〇〇o8O(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Ljava/lang/String;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$desc"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    if-eqz p0, :cond_0

    .line 16
    .line 17
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 p0, 0x0

    .line 21
    :goto_0
    if-nez p0, :cond_1

    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_1
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 25
    .line 26
    .line 27
    :goto_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static final o0O0O〇〇〇0(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->OO〇〇o0oO()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static final o0OO(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Lkotlin/collections/IndexedValue;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$it"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Landroid/content/Intent;

    .line 12
    .line 13
    invoke-virtual {p1}, Lkotlin/collections/IndexedValue;->〇o00〇〇Oo()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    check-cast p1, Ljava/lang/Class;

    .line 18
    .line 19
    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic o0Oo(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇0o88Oo〇(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic o808o8o08(Lcom/intsig/camscanner/test/ExceptionCheckActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O88O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic o〇08oO80o(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O88O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final o〇OoO0(Landroid/app/Activity;Ljava/lang/Class;)Z
    .locals 30
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/Class<",
            "*>;)Z"
        }
    .end annotation

    .line 1
    move-object/from16 v8, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    move-object/from16 v1, p2

    .line 6
    .line 7
    const-class v2, Lcom/intsig/camscanner/DocumentActivity;

    .line 8
    .line 9
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v3

    .line 13
    const-string v4, "android.intent.action.VIEW"

    .line 14
    .line 15
    const/4 v9, 0x1

    .line 16
    const/4 v10, 0x0

    .line 17
    if-eqz v3, :cond_1

    .line 18
    .line 19
    new-instance v1, Landroid/content/Intent;

    .line 20
    .line 21
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 22
    .line 23
    iget-wide v5, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O0O:J

    .line 24
    .line 25
    invoke-static {v3, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    invoke-direct {v1, v4, v3, v8, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 30
    .line 31
    .line 32
    if-eqz v0, :cond_0

    .line 33
    .line 34
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 35
    .line 36
    .line 37
    :cond_0
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 38
    .line 39
    if-eqz v0, :cond_15

    .line 40
    .line 41
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 42
    .line 43
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 44
    .line 45
    .line 46
    goto/16 :goto_2

    .line 47
    .line 48
    :cond_1
    const-class v2, Lcom/intsig/camscanner/ImageScannerActivity;

    .line 49
    .line 50
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    move-result v3

    .line 54
    if-eqz v3, :cond_3

    .line 55
    .line 56
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 57
    .line 58
    iget-wide v3, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8oOOo:J

    .line 59
    .line 60
    invoke-static {v0, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    const-string v1, "withAppendedId(Documents\u2026CONTENT_URI, debugPageId)"

    .line 65
    .line 66
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    iget-wide v3, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8oOOo:J

    .line 70
    .line 71
    invoke-direct {v8, v3, v4}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇OoO0o0(J)Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 76
    .line 77
    .line 78
    move-result v3

    .line 79
    if-eqz v3, :cond_2

    .line 80
    .line 81
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0〇O0088o(Ljava/lang/String;)Landroid/net/Uri;

    .line 82
    .line 83
    .line 84
    move-result-object v1

    .line 85
    new-instance v3, Landroid/content/Intent;

    .line 86
    .line 87
    const-string v4, "com.intsig.camscanner.REEDIT_PAGE"

    .line 88
    .line 89
    invoke-direct {v3, v4, v1, v8, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 90
    .line 91
    .line 92
    const-string v1, "scanner_image_src"

    .line 93
    .line 94
    const/4 v2, 0x3

    .line 95
    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 96
    .line 97
    .line 98
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8oOOo:J

    .line 99
    .line 100
    invoke-static {v8, v1, v2}, Lcom/intsig/camscanner/db/dao/ImageDao;->O0o〇〇Oo(Landroid/content/Context;J)Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object v1

    .line 104
    const-string v2, "image_sync_id"

    .line 105
    .line 106
    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    .line 108
    .line 109
    const-string v1, "pageuri"

    .line 110
    .line 111
    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 112
    .line 113
    .line 114
    const-class v0, Lcom/intsig/camscanner/fragment/ImagePageViewFragment;

    .line 115
    .line 116
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    const-string v1, "extra_from_where"

    .line 121
    .line 122
    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 123
    .line 124
    .line 125
    invoke-virtual {v8, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 126
    .line 127
    .line 128
    :cond_2
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 129
    .line 130
    if-eqz v0, :cond_15

    .line 131
    .line 132
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 133
    .line 134
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 135
    .line 136
    .line 137
    goto/16 :goto_2

    .line 138
    .line 139
    :cond_3
    const-class v2, Lcom/intsig/camscanner/ImagePageViewActivity;

    .line 140
    .line 141
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 142
    .line 143
    .line 144
    move-result v3

    .line 145
    const-string v5, "doc_title"

    .line 146
    .line 147
    if-eqz v3, :cond_4

    .line 148
    .line 149
    iget-wide v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O0O:J

    .line 150
    .line 151
    invoke-static {v0, v1}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 152
    .line 153
    .line 154
    move-result-object v0

    .line 155
    new-instance v1, Landroid/content/Intent;

    .line 156
    .line 157
    invoke-direct {v1, v4, v0, v8, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 158
    .line 159
    .line 160
    const-string v0, "image_id"

    .line 161
    .line 162
    iget-wide v2, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8oOOo:J

    .line 163
    .line 164
    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 165
    .line 166
    .line 167
    const-string v0, "\u6d4b\u8bd5"

    .line 168
    .line 169
    invoke-virtual {v1, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    .line 171
    .line 172
    const-string v0, "image_page_view_key_offline_folder"

    .line 173
    .line 174
    invoke-virtual {v1, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 175
    .line 176
    .line 177
    invoke-virtual {v8, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 178
    .line 179
    .line 180
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 181
    .line 182
    if-eqz v0, :cond_15

    .line 183
    .line 184
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 185
    .line 186
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 187
    .line 188
    .line 189
    goto/16 :goto_2

    .line 190
    .line 191
    :cond_4
    const-class v2, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 192
    .line 193
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 194
    .line 195
    .line 196
    move-result v2

    .line 197
    if-eqz v2, :cond_5

    .line 198
    .line 199
    sget-object v0, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;->O8o08O8O:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity$Companion;

    .line 200
    .line 201
    sget-object v1, Lcom/intsig/advertisement/enums/PositionType;->ShareDone:Lcom/intsig/advertisement/enums/PositionType;

    .line 202
    .line 203
    invoke-virtual {v0, v8, v9, v1}, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity$Companion;->〇o00〇〇Oo(Landroid/app/Activity;ILcom/intsig/advertisement/enums/PositionType;)V

    .line 204
    .line 205
    .line 206
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 207
    .line 208
    if-eqz v0, :cond_15

    .line 209
    .line 210
    const-wide/16 v1, 0x1388

    .line 211
    .line 212
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 213
    .line 214
    .line 215
    goto/16 :goto_2

    .line 216
    .line 217
    :cond_5
    const-class v2, Lcom/intsig/camscanner/signature/SignatureEditActivity;

    .line 218
    .line 219
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 220
    .line 221
    .line 222
    move-result v2

    .line 223
    const-string v3, ""

    .line 224
    .line 225
    if-eqz v2, :cond_7

    .line 226
    .line 227
    new-instance v0, Ljava/io/File;

    .line 228
    .line 229
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8oOOo:J

    .line 230
    .line 231
    invoke-direct {v8, v1, v2}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇OoO0o0(J)Ljava/lang/String;

    .line 232
    .line 233
    .line 234
    move-result-object v1

    .line 235
    if-nez v1, :cond_6

    .line 236
    .line 237
    goto :goto_0

    .line 238
    :cond_6
    move-object v3, v1

    .line 239
    :goto_0
    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 240
    .line 241
    .line 242
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    .line 243
    .line 244
    .line 245
    move-result-object v0

    .line 246
    const/high16 v1, 0x3f000000    # 0.5f

    .line 247
    .line 248
    invoke-static {v8, v0, v1, v1, v10}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->startActivityForResult(Landroid/app/Activity;Landroid/net/Uri;FFI)V

    .line 249
    .line 250
    .line 251
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 252
    .line 253
    if-eqz v0, :cond_15

    .line 254
    .line 255
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 256
    .line 257
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 258
    .line 259
    .line 260
    goto/16 :goto_2

    .line 261
    .line 262
    :cond_7
    const-class v2, Lcom/intsig/camscanner/pdf/preshare/PdfEditingActivity;

    .line 263
    .line 264
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 265
    .line 266
    .line 267
    move-result v4

    .line 268
    if-eqz v4, :cond_8

    .line 269
    .line 270
    new-instance v0, Landroid/content/Intent;

    .line 271
    .line 272
    invoke-direct {v0, v8, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 273
    .line 274
    .line 275
    const-string v1, "\u6d4b\u8bd5\u6587\u6863"

    .line 276
    .line 277
    invoke-virtual {v0, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    .line 279
    .line 280
    invoke-virtual {v8, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 281
    .line 282
    .line 283
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 284
    .line 285
    if-eqz v0, :cond_15

    .line 286
    .line 287
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 288
    .line 289
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 290
    .line 291
    .line 292
    goto/16 :goto_2

    .line 293
    .line 294
    :cond_8
    const-class v2, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;

    .line 295
    .line 296
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 297
    .line 298
    .line 299
    move-result v4

    .line 300
    if-eqz v4, :cond_9

    .line 301
    .line 302
    new-instance v0, Landroid/content/Intent;

    .line 303
    .line 304
    invoke-direct {v0, v8, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 305
    .line 306
    .line 307
    new-array v1, v9, [Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 308
    .line 309
    new-instance v2, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 310
    .line 311
    iget-wide v12, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8oOOo:J

    .line 312
    .line 313
    invoke-direct {v8, v12, v13}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇OoO0o0(J)Ljava/lang/String;

    .line 314
    .line 315
    .line 316
    move-result-object v14

    .line 317
    const/16 v15, 0x1f4

    .line 318
    .line 319
    const/16 v16, 0x1f4

    .line 320
    .line 321
    const/16 v17, 0x1f4

    .line 322
    .line 323
    const/16 v18, 0x1f4

    .line 324
    .line 325
    move-object v11, v2

    .line 326
    invoke-direct/range {v11 .. v18}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;-><init>(JLjava/lang/String;IIII)V

    .line 327
    .line 328
    .line 329
    aput-object v2, v1, v10

    .line 330
    .line 331
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 332
    .line 333
    .line 334
    move-result-object v1

    .line 335
    const-string v2, "pdf_signature_image_list"

    .line 336
    .line 337
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 338
    .line 339
    .line 340
    invoke-virtual {v8, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 341
    .line 342
    .line 343
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 344
    .line 345
    if-eqz v0, :cond_15

    .line 346
    .line 347
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 348
    .line 349
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 350
    .line 351
    .line 352
    goto/16 :goto_2

    .line 353
    .line 354
    :cond_9
    const-class v2, Lcom/intsig/camscanner/miniprogram/OtherShareInImgActivity;

    .line 355
    .line 356
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 357
    .line 358
    .line 359
    move-result v4

    .line 360
    if-eqz v4, :cond_a

    .line 361
    .line 362
    new-instance v1, Landroid/content/Intent;

    .line 363
    .line 364
    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 365
    .line 366
    .line 367
    const-string v0, "other_share_in_img_doc_data"

    .line 368
    .line 369
    const-string v2, "{\"type\":\"share\",\"url\":\"https%3A%2F%2Fopen-sandbox.camscanner.com%2Fsync%2Fdownload_jpg_nologin%3Fid%3Dabe0d0df6de4af733da38fe2b2092a091617872075\"}"

    .line 370
    .line 371
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372
    .line 373
    .line 374
    invoke-virtual {v8, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 375
    .line 376
    .line 377
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 378
    .line 379
    if-eqz v0, :cond_15

    .line 380
    .line 381
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 382
    .line 383
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 384
    .line 385
    .line 386
    goto/16 :goto_2

    .line 387
    .line 388
    :cond_a
    const-class v2, Lcom/intsig/camscanner/miniprogram/OtherShareInDocActivity;

    .line 389
    .line 390
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 391
    .line 392
    .line 393
    move-result v4

    .line 394
    if-eqz v4, :cond_b

    .line 395
    .line 396
    new-instance v1, Landroid/content/Intent;

    .line 397
    .line 398
    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 399
    .line 400
    .line 401
    const-string v0, "doc_data"

    .line 402
    .line 403
    const-string v2, "{\"content\":{\"encrypt_id\":\"MHhjMjEwZGRl\",\"sid\":\"14890F\"},\"isMulDocsShare\":false,\"server_url\":\"https://cs1-sandbox.intsig.net/sync\",\"type\":\"share\"}"

    .line 404
    .line 405
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 406
    .line 407
    .line 408
    invoke-virtual {v8, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 409
    .line 410
    .line 411
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 412
    .line 413
    if-eqz v0, :cond_15

    .line 414
    .line 415
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 416
    .line 417
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 418
    .line 419
    .line 420
    goto/16 :goto_2

    .line 421
    .line 422
    :cond_b
    const-class v0, Lcom/intsig/camscanner/guide/CancelAdShowCnGuidePurchaseActivity;

    .line 423
    .line 424
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 425
    .line 426
    .line 427
    move-result v2

    .line 428
    if-eqz v2, :cond_c

    .line 429
    .line 430
    new-instance v1, Landroid/content/Intent;

    .line 431
    .line 432
    invoke-direct {v1, v8, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 433
    .line 434
    .line 435
    const-string v0, "extra_activity_from"

    .line 436
    .line 437
    const/4 v2, 0x5

    .line 438
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 439
    .line 440
    .line 441
    invoke-virtual {v8, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 442
    .line 443
    .line 444
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 445
    .line 446
    if-eqz v0, :cond_15

    .line 447
    .line 448
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 449
    .line 450
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 451
    .line 452
    .line 453
    goto/16 :goto_2

    .line 454
    .line 455
    :cond_c
    const-class v0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;

    .line 456
    .line 457
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 458
    .line 459
    .line 460
    move-result v0

    .line 461
    if-eqz v0, :cond_e

    .line 462
    .line 463
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O0O:J

    .line 464
    .line 465
    iget-wide v4, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8oOOo:J

    .line 466
    .line 467
    invoke-direct {v8, v4, v5}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇OoO0o0(J)Ljava/lang/String;

    .line 468
    .line 469
    .line 470
    move-result-object v0

    .line 471
    if-nez v0, :cond_d

    .line 472
    .line 473
    goto :goto_1

    .line 474
    :cond_d
    move-object v3, v0

    .line 475
    :goto_1
    const/4 v4, 0x0

    .line 476
    const-string v5, "test"

    .line 477
    .line 478
    move-object/from16 v0, p0

    .line 479
    .line 480
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/doodle/Doodle;->〇o〇(Landroid/content/Context;JLjava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    .line 481
    .line 482
    .line 483
    move-result-object v0

    .line 484
    invoke-virtual {v8, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 485
    .line 486
    .line 487
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 488
    .line 489
    if-eqz v0, :cond_15

    .line 490
    .line 491
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 492
    .line 493
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 494
    .line 495
    .line 496
    goto/16 :goto_2

    .line 497
    .line 498
    :cond_e
    const-class v0, Lcom/intsig/camscanner/purchase/activity/NegativePurchaseActivity;

    .line 499
    .line 500
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 501
    .line 502
    .line 503
    move-result v0

    .line 504
    if-eqz v0, :cond_f

    .line 505
    .line 506
    sget-object v0, Lcom/intsig/camscanner/purchase/activity/NegativePurchaseActivity;->〇〇08O:Lcom/intsig/camscanner/purchase/activity/NegativePurchaseActivity$Companion;

    .line 507
    .line 508
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 509
    .line 510
    invoke-direct {v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 511
    .line 512
    .line 513
    invoke-virtual {v0, v8, v1}, Lcom/intsig/camscanner/purchase/activity/NegativePurchaseActivity$Companion;->startActivity(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 514
    .line 515
    .line 516
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 517
    .line 518
    if-eqz v0, :cond_15

    .line 519
    .line 520
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 521
    .line 522
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 523
    .line 524
    .line 525
    goto/16 :goto_2

    .line 526
    .line 527
    :cond_f
    const-class v0, Lcom/intsig/camscanner/scandone/ScanDoneNewActivity;

    .line 528
    .line 529
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 530
    .line 531
    .line 532
    move-result v0

    .line 533
    if-eqz v0, :cond_10

    .line 534
    .line 535
    new-instance v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;

    .line 536
    .line 537
    move-object v11, v0

    .line 538
    const-string v12, "debug\u9875\u9762\u8df3\u8f6c\u8fdb\u6765\u7684\u6587\u6863"

    .line 539
    .line 540
    const-wide/16 v13, 0xa

    .line 541
    .line 542
    const/4 v15, 0x0

    .line 543
    const-string v16, "Doc_finish_type_default"

    .line 544
    .line 545
    const/16 v17, 0x0

    .line 546
    .line 547
    const/16 v18, 0x0

    .line 548
    .line 549
    const/16 v19, 0x0

    .line 550
    .line 551
    sget-object v20, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NONE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 552
    .line 553
    const/16 v21, 0x1

    .line 554
    .line 555
    const/16 v22, 0x0

    .line 556
    .line 557
    const-wide/16 v23, -0x2

    .line 558
    .line 559
    const/16 v25, 0x1

    .line 560
    .line 561
    const/16 v26, 0x0

    .line 562
    .line 563
    const/16 v27, 0x0

    .line 564
    .line 565
    const/16 v28, 0x2000

    .line 566
    .line 567
    const/16 v29, 0x0

    .line 568
    .line 569
    invoke-direct/range {v11 .. v29}, Lcom/intsig/camscanner/scandone/ScanDoneModel;-><init>(Ljava/lang/String;JZLjava/lang/String;ZIZLcom/intsig/camscanner/purchase/track/FunctionEntrance;ILjava/lang/String;JZLcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 570
    .line 571
    .line 572
    sget-object v1, Lcom/intsig/camscanner/scandone/ScanDoneNewActivity;->O0O:Lcom/intsig/camscanner/scandone/ScanDoneNewActivity$Companion;

    .line 573
    .line 574
    const/16 v2, 0x3f7

    .line 575
    .line 576
    invoke-virtual {v1, v8, v0, v2}, Lcom/intsig/camscanner/scandone/ScanDoneNewActivity$Companion;->startActivityForResult(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/scandone/ScanDoneModel;I)V

    .line 577
    .line 578
    .line 579
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 580
    .line 581
    if-eqz v0, :cond_15

    .line 582
    .line 583
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 584
    .line 585
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 586
    .line 587
    .line 588
    goto/16 :goto_2

    .line 589
    .line 590
    :cond_10
    const-class v0, Lcom/intsig/camscanner/mode_ocr/BatchOCRDataResultActivity;

    .line 591
    .line 592
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 593
    .line 594
    .line 595
    move-result v0

    .line 596
    if-eqz v0, :cond_11

    .line 597
    .line 598
    new-instance v3, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 599
    .line 600
    invoke-direct {v3}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;-><init>()V

    .line 601
    .line 602
    .line 603
    iget-wide v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O0O:J

    .line 604
    .line 605
    iput-wide v0, v3, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 606
    .line 607
    const-string v0, "\u6d4b\u8bd5doc"

    .line 608
    .line 609
    iput-object v0, v3, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 610
    .line 611
    sget-object v0, Lcom/intsig/camscanner/mode_ocr/BatchOCRDataResultActivity;->〇〇08O:Lcom/intsig/camscanner/mode_ocr/BatchOCRDataResultActivity$Companion;

    .line 612
    .line 613
    new-instance v2, Ljava/util/ArrayList;

    .line 614
    .line 615
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 616
    .line 617
    .line 618
    sget-object v4, Lcom/intsig/camscanner/mode_ocr/PageFromType;->FROM_PAGE_DETAIL:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 619
    .line 620
    const/16 v5, 0x64

    .line 621
    .line 622
    const/4 v6, 0x0

    .line 623
    const/4 v7, 0x1

    .line 624
    move-object/from16 v1, p0

    .line 625
    .line 626
    invoke-virtual/range {v0 .. v7}, Lcom/intsig/camscanner/mode_ocr/BatchOCRDataResultActivity$Companion;->〇080(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/mode_ocr/PageFromType;ILjava/lang/String;Z)Landroid/content/Intent;

    .line 627
    .line 628
    .line 629
    move-result-object v0

    .line 630
    invoke-virtual {v8, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 631
    .line 632
    .line 633
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 634
    .line 635
    if-eqz v0, :cond_15

    .line 636
    .line 637
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 638
    .line 639
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 640
    .line 641
    .line 642
    goto :goto_2

    .line 643
    :cond_11
    const-class v0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;

    .line 644
    .line 645
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 646
    .line 647
    .line 648
    move-result v0

    .line 649
    const-string v2, "mActivity"

    .line 650
    .line 651
    if-eqz v0, :cond_12

    .line 652
    .line 653
    sget-object v0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->〇o0O:Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$Companion;

    .line 654
    .line 655
    iget-object v1, v8, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 656
    .line 657
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 658
    .line 659
    .line 660
    iget-object v2, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 661
    .line 662
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$Companion;->startActivity(Landroid/content/Context;Ljava/lang/String;)V

    .line 663
    .line 664
    .line 665
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 666
    .line 667
    if-eqz v0, :cond_15

    .line 668
    .line 669
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 670
    .line 671
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 672
    .line 673
    .line 674
    goto :goto_2

    .line 675
    :cond_12
    const-class v0, Lcom/intsig/camscanner/ads/reward/EmptyForFgDialogActivity;

    .line 676
    .line 677
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 678
    .line 679
    .line 680
    move-result v0

    .line 681
    if-eqz v0, :cond_13

    .line 682
    .line 683
    sget-object v0, Lcom/intsig/camscanner/ads/reward/EmptyForFgDialogActivity;->ooo0〇〇O:Lcom/intsig/camscanner/ads/reward/EmptyForFgDialogActivity$Companion;

    .line 684
    .line 685
    iget-object v1, v8, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 686
    .line 687
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 688
    .line 689
    .line 690
    new-instance v2, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 691
    .line 692
    invoke-direct {v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 693
    .line 694
    .line 695
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/ads/reward/EmptyForFgDialogActivity$Companion;->〇080(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 696
    .line 697
    .line 698
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 699
    .line 700
    if-eqz v0, :cond_15

    .line 701
    .line 702
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 703
    .line 704
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 705
    .line 706
    .line 707
    goto :goto_2

    .line 708
    :cond_13
    const-class v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;

    .line 709
    .line 710
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 711
    .line 712
    .line 713
    move-result v1

    .line 714
    if-eqz v1, :cond_14

    .line 715
    .line 716
    new-instance v1, Landroid/content/Intent;

    .line 717
    .line 718
    iget-object v2, v8, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 719
    .line 720
    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 721
    .line 722
    .line 723
    const-string v0, "INTENT_KEY_DOC_ID"

    .line 724
    .line 725
    iget-wide v2, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O0O:J

    .line 726
    .line 727
    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 728
    .line 729
    .line 730
    move-result-object v0

    .line 731
    const-string v1, "Intent(mActivity, CardDe\u2026T_KEY_DOC_ID, debugDocId)"

    .line 732
    .line 733
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 734
    .line 735
    .line 736
    invoke-virtual {v8, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 737
    .line 738
    .line 739
    iget-object v0, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 740
    .line 741
    if-eqz v0, :cond_15

    .line 742
    .line 743
    iget-wide v1, v8, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 744
    .line 745
    invoke-virtual {v0, v10, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 746
    .line 747
    .line 748
    goto :goto_2

    .line 749
    :cond_14
    const/4 v9, 0x0

    .line 750
    :cond_15
    :goto_2
    return v9
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method public static final synthetic o〇o08〇(Lcom/intsig/camscanner/test/ExceptionCheckActivity$ResumedActivityCallback;)V
    .locals 0

    .line 1
    sput-object p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->Oo80:Lcom/intsig/camscanner/test/ExceptionCheckActivity$ResumedActivityCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static final 〇0o88Oo〇(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Ljava/util/List;)V
    .locals 4

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const/4 v1, 0x0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;->〇08O〇00〇o:Landroid/widget/LinearLayout;

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    move-object v0, v1

    .line 17
    :goto_0
    if-nez v0, :cond_1

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_1
    const/4 v2, 0x0

    .line 21
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 22
    .line 23
    .line 24
    :goto_1
    iget-wide v2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 25
    .line 26
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    if-eqz v0, :cond_4

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    if-eqz v2, :cond_2

    .line 37
    .line 38
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;->〇OOo8〇0:Landroid/widget/EditText;

    .line 39
    .line 40
    goto :goto_2

    .line 41
    :cond_2
    move-object v2, v1

    .line 42
    :goto_2
    if-nez v2, :cond_3

    .line 43
    .line 44
    goto :goto_3

    .line 45
    :cond_3
    invoke-static {}, Landroid/text/Editable$Factory;->getInstance()Landroid/text/Editable$Factory;

    .line 46
    .line 47
    .line 48
    move-result-object v3

    .line 49
    invoke-virtual {v3, v0}, Landroid/text/Editable$Factory;->newEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    .line 55
    .line 56
    :cond_4
    :goto_3
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    if-eqz v0, :cond_5

    .line 61
    .line 62
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 63
    .line 64
    goto :goto_4

    .line 65
    :cond_5
    move-object v0, v1

    .line 66
    :goto_4
    if-nez v0, :cond_6

    .line 67
    .line 68
    goto :goto_5

    .line 69
    :cond_6
    if-eqz p1, :cond_7

    .line 70
    .line 71
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 72
    .line 73
    .line 74
    move-result p1

    .line 75
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    :cond_7
    iget p0, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->oOO〇〇:I

    .line 80
    .line 81
    new-instance p1, Ljava/lang/StringBuilder;

    .line 82
    .line 83
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    .line 85
    .line 86
    const-string v2, "\u5171"

    .line 87
    .line 88
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    const-string v1, " \u4e2a\u9875\u9762\uff0c\u5168\u90e8\u68c0\u67e5\u5b8c\u6210\n\u5171\u8df3\u8fc7\u4e86"

    .line 95
    .line 96
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    const-string p0, " \u4e2a\u9875\u9762\n\u7ed3\u679c\u65e0\u5d29\u6e83~"

    .line 103
    .line 104
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object p0

    .line 111
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    .line 113
    .line 114
    :goto_5
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private final 〇OoO0o0(J)Ljava/lang/String;
    .locals 10

    .line 1
    const-wide/16 v0, 0x0

    .line 2
    .line 3
    const/4 v2, 0x0

    .line 4
    cmp-long v3, p1, v0

    .line 5
    .line 6
    if-lez v3, :cond_2

    .line 7
    .line 8
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 9
    .line 10
    .line 11
    move-result-object v4

    .line 12
    if-eqz v4, :cond_0

    .line 13
    .line 14
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 15
    .line 16
    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 17
    .line 18
    .line 19
    move-result-object v5

    .line 20
    const-string p1, "raw_data"

    .line 21
    .line 22
    filled-new-array {p1}, [Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v6

    .line 26
    const/4 v7, 0x0

    .line 27
    const/4 v8, 0x0

    .line 28
    const/4 v9, 0x0

    .line 29
    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    goto :goto_0

    .line 34
    :cond_0
    move-object p1, v2

    .line 35
    :goto_0
    if-eqz p1, :cond_2

    .line 36
    .line 37
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 38
    .line 39
    .line 40
    move-result p2

    .line 41
    if-eqz p2, :cond_1

    .line 42
    .line 43
    const/4 p2, 0x0

    .line 44
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p2

    .line 48
    move-object v2, p2

    .line 49
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 50
    .line 51
    .line 52
    :cond_2
    return-object v2
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static final synthetic 〇oO88o(Lcom/intsig/camscanner/test/ExceptionCheckActivity;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8oOOo:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final 〇oOO80o(Ljava/util/List;Ljava/util/Iterator;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/Class<",
            "*>;>;",
            "Ljava/util/Iterator<",
            "+",
            "Ljava/lang/Class<",
            "*>;>;)V"
        }
    .end annotation

    .line 1
    if-eqz p2, :cond_f

    .line 2
    .line 3
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->〇O888o0o(Ljava/util/Iterator;)Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object p2

    .line 7
    if-eqz p2, :cond_f

    .line 8
    .line 9
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_1

    .line 14
    .line 15
    iget-object p2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 16
    .line 17
    const-string v0, "END"

    .line 18
    .line 19
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    sget-object p2, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->Oo80:Lcom/intsig/camscanner/test/ExceptionCheckActivity$ResumedActivityCallback;

    .line 23
    .line 24
    if-eqz p2, :cond_0

    .line 25
    .line 26
    invoke-interface {p2}, Lcom/intsig/camscanner/test/ExceptionCheckActivity$ResumedActivityCallback;->〇080()Landroid/app/Activity;

    .line 27
    .line 28
    .line 29
    move-result-object p2

    .line 30
    if-eqz p2, :cond_0

    .line 31
    .line 32
    new-instance v0, Landroid/content/Intent;

    .line 33
    .line 34
    const-class v1, Lcom/intsig/camscanner/test/ExceptionCheckActivity;

    .line 35
    .line 36
    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 40
    .line 41
    .line 42
    :cond_0
    new-instance p2, LO8〇/Oo08;

    .line 43
    .line 44
    invoke-direct {p2, p0, p1}, LO8〇/Oo08;-><init>(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Ljava/util/List;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {p0, p2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 48
    .line 49
    .line 50
    return-void

    .line 51
    :cond_1
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object p2

    .line 55
    check-cast p2, Lkotlin/collections/IndexedValue;

    .line 56
    .line 57
    sget-object v0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->Oo80:Lcom/intsig/camscanner/test/ExceptionCheckActivity$ResumedActivityCallback;

    .line 58
    .line 59
    const/4 v1, 0x0

    .line 60
    if-eqz v0, :cond_2

    .line 61
    .line 62
    invoke-interface {v0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity$ResumedActivityCallback;->〇080()Landroid/app/Activity;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    goto :goto_0

    .line 67
    :cond_2
    move-object v0, v1

    .line 68
    :goto_0
    if-nez v0, :cond_4

    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 71
    .line 72
    invoke-virtual {p2}, Lkotlin/collections/IndexedValue;->〇o00〇〇Oo()Ljava/lang/Object;

    .line 73
    .line 74
    .line 75
    move-result-object p2

    .line 76
    check-cast p2, Ljava/lang/Class;

    .line 77
    .line 78
    if-eqz p2, :cond_3

    .line 79
    .line 80
    invoke-virtual {p2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    :cond_3
    new-instance p2, Ljava/lang/StringBuilder;

    .line 85
    .line 86
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    .line 88
    .line 89
    const-string v0, "checking Exception, activity is NULL, ---- jump out, name is "

    .line 90
    .line 91
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object p2

    .line 101
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    const-string p1, "Error activity\u4e3a\u7a7a\uff0c\u6682\u505c\u81ea\u68c0\uff0c\u8bf7\u8054\u7cfb\u5f00\u53d1\u4eba\u5458"

    .line 105
    .line 106
    invoke-static {p0, p1}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 107
    .line 108
    .line 109
    return-void

    .line 110
    :cond_4
    iget v2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o〇oO:I

    .line 111
    .line 112
    add-int/lit8 v2, v2, 0x1

    .line 113
    .line 114
    iput v2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o〇oO:I

    .line 115
    .line 116
    invoke-virtual {p2}, Lkotlin/collections/IndexedValue;->〇o00〇〇Oo()Ljava/lang/Object;

    .line 117
    .line 118
    .line 119
    move-result-object v2

    .line 120
    check-cast v2, Ljava/lang/Class;

    .line 121
    .line 122
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O8O(Ljava/lang/Class;)Z

    .line 123
    .line 124
    .line 125
    move-result v2

    .line 126
    const/4 v3, 0x0

    .line 127
    if-eqz v2, :cond_7

    .line 128
    .line 129
    iget-object p1, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 130
    .line 131
    invoke-virtual {p2}, Lkotlin/collections/IndexedValue;->〇o00〇〇Oo()Ljava/lang/Object;

    .line 132
    .line 133
    .line 134
    move-result-object p2

    .line 135
    check-cast p2, Ljava/lang/Class;

    .line 136
    .line 137
    if-eqz p2, :cond_5

    .line 138
    .line 139
    invoke-virtual {p2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    .line 140
    .line 141
    .line 142
    move-result-object v1

    .line 143
    :cond_5
    new-instance p2, Ljava/lang/StringBuilder;

    .line 144
    .line 145
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 146
    .line 147
    .line 148
    const-string v0, "excludeClass, ---- jump out, name is "

    .line 149
    .line 150
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    .line 152
    .line 153
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    .line 155
    .line 156
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object p2

    .line 160
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    iget-object p1, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 164
    .line 165
    if-eqz p1, :cond_6

    .line 166
    .line 167
    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 168
    .line 169
    .line 170
    :cond_6
    return-void

    .line 171
    :cond_7
    iget v2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o〇oO:I

    .line 172
    .line 173
    iget v4, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->oo8ooo8O:I

    .line 174
    .line 175
    if-ge v2, v4, :cond_a

    .line 176
    .line 177
    iget-object p1, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 178
    .line 179
    invoke-virtual {p2}, Lkotlin/collections/IndexedValue;->〇o00〇〇Oo()Ljava/lang/Object;

    .line 180
    .line 181
    .line 182
    move-result-object p2

    .line 183
    check-cast p2, Ljava/lang/Class;

    .line 184
    .line 185
    if-eqz p2, :cond_8

    .line 186
    .line 187
    invoke-virtual {p2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    .line 188
    .line 189
    .line 190
    move-result-object v1

    .line 191
    :cond_8
    iget p2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o〇oO:I

    .line 192
    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    .line 194
    .line 195
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 196
    .line 197
    .line 198
    const-string v2, "not start index "

    .line 199
    .line 200
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    .line 202
    .line 203
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 204
    .line 205
    .line 206
    const-string v2, ", ---- jump out, name is "

    .line 207
    .line 208
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    .line 210
    .line 211
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    .line 213
    .line 214
    const-string v1, ", index="

    .line 215
    .line 216
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    .line 218
    .line 219
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 220
    .line 221
    .line 222
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 223
    .line 224
    .line 225
    move-result-object p2

    .line 226
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    .line 228
    .line 229
    iget-object p1, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 230
    .line 231
    if-eqz p1, :cond_9

    .line 232
    .line 233
    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 234
    .line 235
    .line 236
    :cond_9
    return-void

    .line 237
    :cond_a
    new-instance v2, LO8〇/o〇0;

    .line 238
    .line 239
    invoke-direct {v2, v0}, LO8〇/o〇0;-><init>(Landroid/app/Activity;)V

    .line 240
    .line 241
    .line 242
    invoke-virtual {p0, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 243
    .line 244
    .line 245
    iget v2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o〇oO:I

    .line 246
    .line 247
    if-eqz p1, :cond_b

    .line 248
    .line 249
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 250
    .line 251
    .line 252
    move-result p1

    .line 253
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 254
    .line 255
    .line 256
    move-result-object p1

    .line 257
    goto :goto_1

    .line 258
    :cond_b
    move-object p1, v1

    .line 259
    :goto_1
    invoke-virtual {p2}, Lkotlin/collections/IndexedValue;->〇o00〇〇Oo()Ljava/lang/Object;

    .line 260
    .line 261
    .line 262
    move-result-object v4

    .line 263
    check-cast v4, Ljava/lang/Class;

    .line 264
    .line 265
    if-eqz v4, :cond_c

    .line 266
    .line 267
    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    .line 268
    .line 269
    .line 270
    move-result-object v4

    .line 271
    goto :goto_2

    .line 272
    :cond_c
    move-object v4, v1

    .line 273
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    .line 274
    .line 275
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 276
    .line 277
    .line 278
    const-string v6, "\u5f53\u524d\u8981\u8df3\u8f6c\u5230\u7b2c "

    .line 279
    .line 280
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    .line 282
    .line 283
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 284
    .line 285
    .line 286
    const-string v2, " \u4e2a\u9875\u9762\uff0c\u5171 "

    .line 287
    .line 288
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    .line 290
    .line 291
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 292
    .line 293
    .line 294
    const-string p1, " \u4e2a\n \u9875\u9762\u540d\uff1a "

    .line 295
    .line 296
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    .line 298
    .line 299
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    .line 301
    .line 302
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 303
    .line 304
    .line 305
    move-result-object p1

    .line 306
    new-instance v2, LO8〇/〇〇888;

    .line 307
    .line 308
    invoke-direct {v2, p0, p1}, LO8〇/〇〇888;-><init>(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Ljava/lang/String;)V

    .line 309
    .line 310
    .line 311
    invoke-virtual {p0, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 312
    .line 313
    .line 314
    iget-object v2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 315
    .line 316
    new-instance v4, Ljava/lang/StringBuilder;

    .line 317
    .line 318
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 319
    .line 320
    .line 321
    const-string v5, "checking Exception, "

    .line 322
    .line 323
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    .line 325
    .line 326
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    .line 328
    .line 329
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 330
    .line 331
    .line 332
    move-result-object p1

    .line 333
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    .line 335
    .line 336
    const-wide/16 v4, 0x320

    .line 337
    .line 338
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 339
    .line 340
    .line 341
    invoke-virtual {p2}, Lkotlin/collections/IndexedValue;->〇o00〇〇Oo()Ljava/lang/Object;

    .line 342
    .line 343
    .line 344
    move-result-object p1

    .line 345
    check-cast p1, Ljava/lang/Class;

    .line 346
    .line 347
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o〇OoO0(Landroid/app/Activity;Ljava/lang/Class;)Z

    .line 348
    .line 349
    .line 350
    move-result p1

    .line 351
    if-eqz p1, :cond_e

    .line 352
    .line 353
    iget-object p1, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 354
    .line 355
    invoke-virtual {p2}, Lkotlin/collections/IndexedValue;->〇o00〇〇Oo()Ljava/lang/Object;

    .line 356
    .line 357
    .line 358
    move-result-object p2

    .line 359
    check-cast p2, Ljava/lang/Class;

    .line 360
    .line 361
    if-eqz p2, :cond_d

    .line 362
    .line 363
    invoke-virtual {p2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    .line 364
    .line 365
    .line 366
    move-result-object v1

    .line 367
    :cond_d
    new-instance p2, Ljava/lang/StringBuilder;

    .line 368
    .line 369
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 370
    .line 371
    .line 372
    const-string v0, "checking Exception, ---- jump out, name is "

    .line 373
    .line 374
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    .line 376
    .line 377
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378
    .line 379
    .line 380
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 381
    .line 382
    .line 383
    move-result-object p2

    .line 384
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    .line 386
    .line 387
    return-void

    .line 388
    :cond_e
    new-instance p1, LO8〇/oO80;

    .line 389
    .line 390
    invoke-direct {p1, p0, p2}, LO8〇/oO80;-><init>(Lcom/intsig/camscanner/test/ExceptionCheckActivity;Lkotlin/collections/IndexedValue;)V

    .line 391
    .line 392
    .line 393
    invoke-virtual {p0, p1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 394
    .line 395
    .line 396
    iget-object p1, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇o0O:Landroid/os/Handler;

    .line 397
    .line 398
    if-eqz p1, :cond_f

    .line 399
    .line 400
    iget-wide v0, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8o:J

    .line 401
    .line 402
    invoke-virtual {p1, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 403
    .line 404
    .line 405
    :cond_f
    return-void
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private final 〇ooO8Ooo〇()Z
    .locals 12

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v6, "_id"

    .line 6
    .line 7
    const/4 v7, 0x0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 11
    .line 12
    filled-new-array {v6}, [Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    const/4 v3, 0x0

    .line 17
    const/4 v4, 0x0

    .line 18
    const/4 v5, 0x0

    .line 19
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    move-object v0, v7

    .line 25
    :goto_0
    const/4 v1, 0x0

    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-eqz v2, :cond_1

    .line 33
    .line 34
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    .line 35
    .line 36
    .line 37
    move-result-wide v2

    .line 38
    iput-wide v2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O0O:J

    .line 39
    .line 40
    iget-object v4, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 41
    .line 42
    new-instance v5, Ljava/lang/StringBuilder;

    .line 43
    .line 44
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    .line 46
    .line 47
    const-string v8, "\u627e\u5230\u4e86\u6d4b\u8bd5\u6837\u672cDOC "

    .line 48
    .line 49
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    invoke-static {v4, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    :cond_1
    if-eqz v0, :cond_2

    .line 63
    .line 64
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 65
    .line 66
    .line 67
    :cond_2
    iget-wide v2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O0O:J

    .line 68
    .line 69
    const-string v0, "Error, noDocId"

    .line 70
    .line 71
    const-wide/16 v4, 0x0

    .line 72
    .line 73
    cmp-long v8, v2, v4

    .line 74
    .line 75
    if-gez v8, :cond_3

    .line 76
    .line 77
    iget-object v2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 78
    .line 79
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    return v1

    .line 83
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    if-eqz v2, :cond_4

    .line 88
    .line 89
    iget-wide v7, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->O0O:J

    .line 90
    .line 91
    invoke-static {v7, v8}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 92
    .line 93
    .line 94
    move-result-object v7

    .line 95
    const-string v3, "raw_data"

    .line 96
    .line 97
    filled-new-array {v6, v3}, [Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v8

    .line 101
    const/4 v9, 0x0

    .line 102
    const/4 v10, 0x0

    .line 103
    const/4 v11, 0x0

    .line 104
    move-object v6, v2

    .line 105
    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 106
    .line 107
    .line 108
    move-result-object v7

    .line 109
    :cond_4
    const/4 v2, 0x1

    .line 110
    if-eqz v7, :cond_6

    .line 111
    .line 112
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 113
    .line 114
    .line 115
    move-result v3

    .line 116
    if-eqz v3, :cond_6

    .line 117
    .line 118
    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object v3

    .line 122
    if-nez v3, :cond_5

    .line 123
    .line 124
    const-string v3, ""

    .line 125
    .line 126
    goto :goto_1

    .line 127
    :cond_5
    const-string v6, "it.getString(1) ?: \"\""

    .line 128
    .line 129
    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    :goto_1
    iput-object v3, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 133
    .line 134
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    .line 135
    .line 136
    .line 137
    move-result-wide v8

    .line 138
    iput-wide v8, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8oOOo:J

    .line 139
    .line 140
    iget-object v3, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 141
    .line 142
    new-instance v6, Ljava/lang/StringBuilder;

    .line 143
    .line 144
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 145
    .line 146
    .line 147
    const-string v10, "\u627e\u5230\u4e86\u6d4b\u8bd5\u6837\u672cPAGE "

    .line 148
    .line 149
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 153
    .line 154
    .line 155
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 156
    .line 157
    .line 158
    move-result-object v6

    .line 159
    invoke-static {v3, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    .line 161
    .line 162
    :cond_6
    if-eqz v7, :cond_7

    .line 163
    .line 164
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 165
    .line 166
    .line 167
    :cond_7
    iget-wide v6, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->o8oOOo:J

    .line 168
    .line 169
    cmp-long v3, v6, v4

    .line 170
    .line 171
    if-gez v3, :cond_8

    .line 172
    .line 173
    iget-object v2, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 174
    .line 175
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .line 177
    .line 178
    return v1

    .line 179
    :cond_8
    return v2
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private final 〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇〇08O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇〇o〇:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;->〇〇888(Landroid/app/Activity;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const/4 v0, 0x0

    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;->〇080OO8〇0:Landroid/widget/Button;

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    move-object p1, v0

    .line 12
    :goto_0
    if-nez p1, :cond_1

    .line 13
    .line 14
    goto :goto_1

    .line 15
    :cond_1
    const-string v1, "\u5f00\u59cb\u81ea\u68c0\u6d4b\u8bd5"

    .line 16
    .line 17
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 18
    .line 19
    .line 20
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    if-eqz p1, :cond_2

    .line 25
    .line 26
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;->〇080OO8〇0:Landroid/widget/Button;

    .line 27
    .line 28
    if-eqz p1, :cond_2

    .line 29
    .line 30
    new-instance v1, LO8〇/O8;

    .line 31
    .line 32
    invoke-direct {v1, p0}, LO8〇/O8;-><init>(Lcom/intsig/camscanner/test/ExceptionCheckActivity;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    .line 37
    .line 38
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇ooO8Ooo〇()Z

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    if-nez p1, :cond_7

    .line 43
    .line 44
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    if-eqz p1, :cond_3

    .line 49
    .line 50
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;->〇080OO8〇0:Landroid/widget/Button;

    .line 51
    .line 52
    goto :goto_2

    .line 53
    :cond_3
    move-object p1, v0

    .line 54
    :goto_2
    if-nez p1, :cond_4

    .line 55
    .line 56
    goto :goto_3

    .line 57
    :cond_4
    const/4 v1, 0x0

    .line 58
    invoke-virtual {p1, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 59
    .line 60
    .line 61
    :goto_3
    const-string p1, "\u8bf7\u4fdd\u8bc1\u81f3\u5c11\u67091\u4e2a\u6587\u6863\u53ca1\u4e2a\u5185\u90e8\u6587\u6863\u9875"

    .line 62
    .line 63
    invoke-static {p0, p1}, Lcom/intsig/utils/ToastUtils;->o〇0(Landroid/content/Context;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    if-eqz p1, :cond_5

    .line 71
    .line 72
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 73
    .line 74
    :cond_5
    if-nez v0, :cond_6

    .line 75
    .line 76
    goto :goto_4

    .line 77
    :cond_6
    const-string p1, "\u8bf7\u4fdd\u8bc1\u81f3\u5c11\u67091\u4e2a\u6587\u6863\u53ca1\u4e2a\u5185\u90e8\u6587\u6863\u9875\n \u8bf7\u8fd4\u56de\u9996\u9875\u521b\u5efa\u4e00\u4e2a\u6587\u6863\uff0c\u4fdd\u8bc1\u6587\u6863\u4e2d\u6709\u539f\u56fe\u518d\u6765\u6d4b\u8bd5\n\u65e0\u6587\u6863\u72b6\u6001\u4e0b\uff0c\u90e8\u5206\u9875\u9762\u65e0\u6cd5\u8df3\u8fdb\u53bb\uff0c\u6d4b\u8bd5\u4e0d\u5b8c\u5168~"

    .line 78
    .line 79
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    .line 81
    .line 82
    goto :goto_4

    .line 83
    :cond_7
    invoke-direct {p0}, Lcom/intsig/camscanner/test/ExceptionCheckActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    if-eqz p1, :cond_8

    .line 88
    .line 89
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityExceptionCheckBinding;->〇080OO8〇0:Landroid/widget/Button;

    .line 90
    .line 91
    :cond_8
    if-nez v0, :cond_9

    .line 92
    .line 93
    goto :goto_4

    .line 94
    :cond_9
    const/4 p1, 0x1

    .line 95
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 96
    .line 97
    .line 98
    :goto_4
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

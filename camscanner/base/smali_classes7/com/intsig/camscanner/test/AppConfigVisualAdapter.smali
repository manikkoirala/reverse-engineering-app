.class public Lcom/intsig/camscanner/test/AppConfigVisualAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "AppConfigVisualAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/test/AppConfigVisualAdapter$ItemClickListener;,
        Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;,
        Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterFilterViewHolder;,
        Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterTipsViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private O8o08O8O:Landroid/view/View$OnClickListener;

.field private OO:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/test/AppConfigEntity;",
            ">;"
        }
    .end annotation
.end field

.field private o0:Landroid/content/Context;

.field private o〇00O:Ljava/lang/String;

.field private 〇080OO8〇0:[Ljava/lang/String;

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/test/AppConfigVisualAdapter$ItemClickListener;

.field private 〇0O:I

.field private 〇OOo8〇0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/test/AppConfigEntity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;Lcom/intsig/camscanner/test/AppConfigVisualAdapter$ItemClickListener;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/test/AppConfigEntity;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "Lcom/intsig/camscanner/test/AppConfigVisualAdapter$ItemClickListener;",
            ")V"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-direct/range {p0 .. p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "#AA009688"

    .line 7
    .line 8
    const-string v2, "#AA4CAF50"

    .line 9
    .line 10
    const-string v3, "#AA8BC34A"

    .line 11
    .line 12
    const-string v4, "#AACDDC39"

    .line 13
    .line 14
    const-string v5, "#AAFFEB3B"

    .line 15
    .line 16
    const-string v6, "#AAFFC107"

    .line 17
    .line 18
    const-string v7, "#AAFF9800"

    .line 19
    .line 20
    const-string v8, "#AAFF5722"

    .line 21
    .line 22
    const-string v9, "#AAF44336"

    .line 23
    .line 24
    const-string v10, "#AAE91E63"

    .line 25
    .line 26
    const-string v11, "#AA9C27B0"

    .line 27
    .line 28
    const-string v12, "#AA673AB7"

    .line 29
    .line 30
    const-string v13, "#AA3F51B5"

    .line 31
    .line 32
    const-string v14, "#AA2196F3"

    .line 33
    .line 34
    const-string v15, "#AA03A9F4"

    .line 35
    .line 36
    const-string v16, "#AA00BCD4"

    .line 37
    .line 38
    filled-new-array/range {v1 .. v16}, [Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    iput-object v1, v0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->〇080OO8〇0:[Ljava/lang/String;

    .line 43
    .line 44
    move-object/from16 v1, p1

    .line 45
    .line 46
    iput-object v1, v0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->o0:Landroid/content/Context;

    .line 47
    .line 48
    move-object/from16 v1, p2

    .line 49
    .line 50
    iput-object v1, v0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 51
    .line 52
    new-instance v1, Ljava/util/ArrayList;

    .line 53
    .line 54
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .line 56
    .line 57
    iput-object v1, v0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->OO:Ljava/util/ArrayList;

    .line 58
    .line 59
    iget-object v2, v0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 60
    .line 61
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 62
    .line 63
    .line 64
    move-object/from16 v1, p3

    .line 65
    .line 66
    iput-object v1, v0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->O8o08O8O:Landroid/view/View$OnClickListener;

    .line 67
    .line 68
    move-object/from16 v1, p4

    .line 69
    .line 70
    iput-object v1, v0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->〇08O〇00〇o:Lcom/intsig/camscanner/test/AppConfigVisualAdapter$ItemClickListener;

    .line 71
    .line 72
    invoke-static {}, Lcom/intsig/utils/CommonUtil;->OO0o〇〇〇〇0()Ljava/util/Random;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    const/16 v2, 0x64

    .line 77
    .line 78
    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    .line 79
    .line 80
    .line 81
    move-result v1

    .line 82
    iput v1, v0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->〇0O:I

    .line 83
    .line 84
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method static bridge synthetic 〇O〇(Lcom/intsig/camscanner/test/AppConfigVisualAdapter;)Lcom/intsig/camscanner/test/AppConfigVisualAdapter$ItemClickListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->〇08O〇00〇o:Lcom/intsig/camscanner/test/AppConfigVisualAdapter$ItemClickListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public OoO8(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/test/AppConfigEntity;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->OO:Ljava/util/ArrayList;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->OO:Ljava/util/ArrayList;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->OO:Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->OO:Ljava/util/ArrayList;

    .line 18
    .line 19
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
.end method

.method public getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->OO:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    add-int/lit8 v0, v0, 0x2

    .line 8
    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const p1, 0x7f0d03ae

    .line 4
    .line 5
    .line 6
    return p1

    .line 7
    :cond_0
    const/4 v0, 0x1

    .line 8
    if-ne p1, v0, :cond_1

    .line 9
    .line 10
    const p1, 0x7f0d03ad

    .line 11
    .line 12
    .line 13
    return p1

    .line 14
    :cond_1
    const p1, 0x7f0d03ac

    .line 15
    .line 16
    .line 17
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public o800o8O(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/test/AppConfigEntity;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 6
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 2
    .line 3
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->getItemViewType(I)I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    const/4 v1, 0x0

    .line 15
    packed-switch v0, :pswitch_data_0

    .line 16
    .line 17
    .line 18
    goto/16 :goto_2

    .line 19
    .line 20
    :pswitch_0
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 21
    .line 22
    const/4 p2, 0x0

    .line 23
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 24
    .line 25
    .line 26
    goto/16 :goto_2

    .line 27
    .line 28
    :pswitch_1
    move-object p2, p1

    .line 29
    check-cast p2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterFilterViewHolder;

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->o〇00O:Ljava/lang/String;

    .line 32
    .line 33
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_0

    .line 38
    .line 39
    iget-object p2, p2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterFilterViewHolder;->o0:Landroid/widget/TextView;

    .line 40
    .line 41
    const v0, 0x7f13096f

    .line 42
    .line 43
    .line 44
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    iget-object p2, p2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterFilterViewHolder;->o0:Landroid/widget/TextView;

    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->o0:Landroid/content/Context;

    .line 51
    .line 52
    const/4 v2, 0x1

    .line 53
    new-array v2, v2, [Ljava/lang/Object;

    .line 54
    .line 55
    iget-object v3, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->o〇00O:Ljava/lang/String;

    .line 56
    .line 57
    aput-object v3, v2, v1

    .line 58
    .line 59
    const v1, 0x7f130970

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    .line 68
    .line 69
    :goto_0
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 70
    .line 71
    iget-object p2, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->O8o08O8O:Landroid/view/View$OnClickListener;

    .line 72
    .line 73
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    .line 75
    .line 76
    goto/16 :goto_2

    .line 77
    .line 78
    :pswitch_2
    iget-object v0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->OO:Ljava/util/ArrayList;

    .line 79
    .line 80
    add-int/lit8 v2, p2, -0x2

    .line 81
    .line 82
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    check-cast v0, Lcom/intsig/camscanner/test/AppConfigEntity;

    .line 87
    .line 88
    move-object v2, p1

    .line 89
    check-cast v2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;

    .line 90
    .line 91
    iget-object v3, v2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;->〇OOo8〇0:Landroid/widget/TextView;

    .line 92
    .line 93
    new-instance v4, Ljava/lang/StringBuilder;

    .line 94
    .line 95
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 96
    .line 97
    .line 98
    const-string v5, "key: "

    .line 99
    .line 100
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    invoke-virtual {v0}, Lcom/intsig/camscanner/test/AppConfigEntity;->getKey()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v5

    .line 107
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object v4

    .line 114
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    .line 116
    .line 117
    iget-object v3, v2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;->OO:Landroid/widget/TextView;

    .line 118
    .line 119
    new-instance v4, Ljava/lang/StringBuilder;

    .line 120
    .line 121
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 122
    .line 123
    .line 124
    const-string v5, "value: "

    .line 125
    .line 126
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    invoke-virtual {v0}, Lcom/intsig/camscanner/test/AppConfigEntity;->getValue()Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object v5

    .line 133
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    .line 135
    .line 136
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v4

    .line 140
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    .line 142
    .line 143
    invoke-virtual {v0}, Lcom/intsig/camscanner/test/AppConfigEntity;->getRemark()Ljava/lang/String;

    .line 144
    .line 145
    .line 146
    move-result-object v3

    .line 147
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 148
    .line 149
    .line 150
    move-result v3

    .line 151
    if-eqz v3, :cond_1

    .line 152
    .line 153
    iget-object v1, v2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 154
    .line 155
    const/16 v3, 0x8

    .line 156
    .line 157
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 158
    .line 159
    .line 160
    goto :goto_1

    .line 161
    :cond_1
    iget-object v3, v2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 162
    .line 163
    new-instance v4, Ljava/lang/StringBuilder;

    .line 164
    .line 165
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 166
    .line 167
    .line 168
    const-string v5, "remark: "

    .line 169
    .line 170
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    .line 172
    .line 173
    invoke-virtual {v0}, Lcom/intsig/camscanner/test/AppConfigEntity;->getRemark()Ljava/lang/String;

    .line 174
    .line 175
    .line 176
    move-result-object v5

    .line 177
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    .line 179
    .line 180
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 181
    .line 182
    .line 183
    move-result-object v4

    .line 184
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    .line 186
    .line 187
    iget-object v3, v2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 188
    .line 189
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 190
    .line 191
    .line 192
    :goto_1
    iget-object v1, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->〇080OO8〇0:[Ljava/lang/String;

    .line 193
    .line 194
    iget v3, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->〇0O:I

    .line 195
    .line 196
    add-int/2addr v3, p2

    .line 197
    array-length p2, v1

    .line 198
    rem-int/2addr v3, p2

    .line 199
    aget-object p2, v1, v3

    .line 200
    .line 201
    iget-object v1, v2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;->o0:Landroid/view/View;

    .line 202
    .line 203
    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 204
    .line 205
    .line 206
    move-result p2

    .line 207
    invoke-virtual {v1, p2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 208
    .line 209
    .line 210
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 211
    .line 212
    new-instance p2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$1;

    .line 213
    .line 214
    invoke-direct {p2, p0, v0}, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$1;-><init>(Lcom/intsig/camscanner/test/AppConfigVisualAdapter;Lcom/intsig/camscanner/test/AppConfigEntity;)V

    .line 215
    .line 216
    .line 217
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 218
    .line 219
    .line 220
    :goto_2
    return-void

    .line 221
    :pswitch_data_0
    .packed-switch 0x7f0d03ac
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->o0:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    packed-switch p2, :pswitch_data_0

    .line 13
    .line 14
    .line 15
    new-instance p2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterTipsViewHolder;

    .line 16
    .line 17
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterTipsViewHolder;-><init>(Landroid/view/View;)V

    .line 18
    .line 19
    .line 20
    return-object p2

    .line 21
    :pswitch_0
    new-instance p2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterFilterViewHolder;

    .line 22
    .line 23
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterFilterViewHolder;-><init>(Landroid/view/View;)V

    .line 24
    .line 25
    .line 26
    return-object p2

    .line 27
    :pswitch_1
    new-instance p2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;

    .line 28
    .line 29
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;-><init>(Landroid/view/View;)V

    .line 30
    .line 31
    .line 32
    return-object p2

    .line 33
    :pswitch_data_0
    .packed-switch 0x7f0d03ac
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇0〇O0088o()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->OO:Ljava/util/ArrayList;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->OO:Ljava/util/ArrayList;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->OO:Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->OO:Ljava/util/ArrayList;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇O00()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->o〇00O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇8O0〇8(Ljava/lang/String;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->o〇00O:Ljava/lang/String;

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->OO:Ljava/util/ArrayList;

    .line 4
    .line 5
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 6
    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->o〇00O:Ljava/lang/String;

    .line 9
    .line 10
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->OO:Ljava/util/ArrayList;

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 19
    .line 20
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 21
    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 25
    .line 26
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    if-eqz v0, :cond_2

    .line 35
    .line 36
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    check-cast v0, Lcom/intsig/camscanner/test/AppConfigEntity;

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/camscanner/test/AppConfigEntity;->getKey()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    if-nez v1, :cond_1

    .line 51
    .line 52
    invoke-virtual {v0}, Lcom/intsig/camscanner/test/AppConfigEntity;->getKey()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    iget-object v2, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->o〇00O:Ljava/lang/String;

    .line 57
    .line 58
    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 59
    .line 60
    .line 61
    move-result v1

    .line 62
    if-eqz v1, :cond_1

    .line 63
    .line 64
    iget-object v1, p0, Lcom/intsig/camscanner/test/AppConfigVisualAdapter;->OO:Ljava/util/ArrayList;

    .line 65
    .line 66
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_2
    :goto_1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 71
    .line 72
    .line 73
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

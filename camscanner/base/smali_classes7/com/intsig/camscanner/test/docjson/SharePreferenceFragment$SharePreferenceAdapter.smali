.class public final Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SharePreferenceFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "SharePreferenceAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8o08O8O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/test/AppConfigEntity;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO:I

.field private o0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/test/AppConfigEntity;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic oOo0:Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment;

.field private oOo〇8o008:Lcom/intsig/camscanner/test/AppConfigVisualAdapter$ItemClickListener;

.field private o〇00O:Landroid/view/View$OnClickListener;

.field private final 〇080OO8〇0:[Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:I

.field private final 〇OOo8〇0:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment;Ljava/util/ArrayList;Landroid/content/Context;)V
    .locals 18
    .param p2    # Ljava/util/ArrayList;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/test/AppConfigEntity;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p3

    .line 4
    .line 5
    const-string v2, "context"

    .line 6
    .line 7
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    move-object/from16 v2, p1

    .line 11
    .line 12
    iput-object v2, v0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->oOo0:Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment;

    .line 13
    .line 14
    invoke-direct/range {p0 .. p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 15
    .line 16
    .line 17
    move-object/from16 v2, p2

    .line 18
    .line 19
    iput-object v2, v0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->o0:Ljava/util/ArrayList;

    .line 20
    .line 21
    iput-object v1, v0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->〇OOo8〇0:Landroid/content/Context;

    .line 22
    .line 23
    const/4 v1, 0x2

    .line 24
    iput v1, v0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->OO:I

    .line 25
    .line 26
    const-string v1, ""

    .line 27
    .line 28
    iput-object v1, v0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->〇08O〇00〇o:Ljava/lang/String;

    .line 29
    .line 30
    new-instance v1, Ljava/util/ArrayList;

    .line 31
    .line 32
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 33
    .line 34
    .line 35
    iput-object v1, v0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->O8o08O8O:Ljava/util/ArrayList;

    .line 36
    .line 37
    const-string v2, "#AA009688"

    .line 38
    .line 39
    const-string v3, "#AA4CAF50"

    .line 40
    .line 41
    const-string v4, "#AA8BC34A"

    .line 42
    .line 43
    const-string v5, "#AACDDC39"

    .line 44
    .line 45
    const-string v6, "#AAFFEB3B"

    .line 46
    .line 47
    const-string v7, "#AAFFC107"

    .line 48
    .line 49
    const-string v8, "#AAFF9800"

    .line 50
    .line 51
    const-string v9, "#AAFF5722"

    .line 52
    .line 53
    const-string v10, "#AAF44336"

    .line 54
    .line 55
    const-string v11, "#AAE91E63"

    .line 56
    .line 57
    const-string v12, "#AA9C27B0"

    .line 58
    .line 59
    const-string v13, "#AA673AB7"

    .line 60
    .line 61
    const-string v14, "#AA3F51B5"

    .line 62
    .line 63
    const-string v15, "#AA2196F3"

    .line 64
    .line 65
    const-string v16, "#AA03A9F4"

    .line 66
    .line 67
    const-string v17, "#AA00BCD4"

    .line 68
    .line 69
    filled-new-array/range {v2 .. v17}, [Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    iput-object v1, v0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->〇080OO8〇0:[Ljava/lang/String;

    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static synthetic 〇O〇(Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;Lcom/intsig/camscanner/test/AppConfigEntity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->〇〇8O0〇8(Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;Lcom/intsig/camscanner/test/AppConfigEntity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private static final 〇〇8O0〇8(Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;Lcom/intsig/camscanner/test/AppConfigEntity;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->oOo〇8o008:Lcom/intsig/camscanner/test/AppConfigVisualAdapter$ItemClickListener;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    invoke-interface {p0, p1, p2}, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$ItemClickListener;->〇080(Lcom/intsig/camscanner/test/AppConfigEntity;Landroid/view/View;)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public final OoO8(Landroid/view/View$OnClickListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->o〇00O:Landroid/view/View$OnClickListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->〇OOo8〇0:Landroid/content/Context;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getItemCount()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->o0:Ljava/util/ArrayList;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    iget v1, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->OO:I

    .line 12
    .line 13
    add-int/2addr v0, v1

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    const/4 v0, 0x1

    .line 4
    if-eq p1, v0, :cond_0

    .line 5
    .line 6
    const p1, 0x7f0d03ac

    .line 7
    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const p1, 0x7f0d03ad

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_1
    const p1, 0x7f0d03ae

    .line 15
    .line 16
    .line 17
    :goto_0
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final o800o8O(Lcom/intsig/camscanner/test/AppConfigVisualAdapter$ItemClickListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->oOo〇8o008:Lcom/intsig/camscanner/test/AppConfigVisualAdapter$ItemClickListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 7
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "viewHolder"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->getItemViewType(I)I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    packed-switch v0, :pswitch_data_0

    .line 11
    .line 12
    .line 13
    check-cast p1, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterTipsViewHolder;

    .line 14
    .line 15
    iget-object p1, p1, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterTipsViewHolder;->o0:Landroid/widget/TextView;

    .line 16
    .line 17
    const-string p2, "\u66f4\u65b0 SP \u503c\u8bf7\u9075\u5faa\u539f\u6709\u6570\u636e\u7c7b\u578b\uff01\uff01\uff01"

    .line 18
    .line 19
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 20
    .line 21
    .line 22
    goto/16 :goto_3

    .line 23
    .line 24
    :pswitch_0
    move-object p2, p1

    .line 25
    check-cast p2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterFilterViewHolder;

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->〇08O〇00〇o:Ljava/lang/String;

    .line 28
    .line 29
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    .line 35
    iget-object p2, p2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterFilterViewHolder;->o0:Landroid/widget/TextView;

    .line 36
    .line 37
    const v0, 0x7f13096f

    .line 38
    .line 39
    .line 40
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_0
    iget-object p2, p2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterFilterViewHolder;->o0:Landroid/widget/TextView;

    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->〇OOo8〇0:Landroid/content/Context;

    .line 47
    .line 48
    const/4 v1, 0x1

    .line 49
    new-array v1, v1, [Ljava/lang/Object;

    .line 50
    .line 51
    const/4 v2, 0x0

    .line 52
    iget-object v3, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->〇08O〇00〇o:Ljava/lang/String;

    .line 53
    .line 54
    aput-object v3, v1, v2

    .line 55
    .line 56
    const v2, 0x7f130970

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    .line 65
    .line 66
    :goto_0
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 67
    .line 68
    iget-object p2, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->o〇00O:Landroid/view/View$OnClickListener;

    .line 69
    .line 70
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    .line 72
    .line 73
    goto :goto_3

    .line 74
    :pswitch_1
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->o0:Ljava/util/ArrayList;

    .line 75
    .line 76
    const/4 v1, 0x0

    .line 77
    if-eqz v0, :cond_1

    .line 78
    .line 79
    iget v2, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->OO:I

    .line 80
    .line 81
    sub-int v2, p2, v2

    .line 82
    .line 83
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    check-cast v0, Lcom/intsig/camscanner/test/AppConfigEntity;

    .line 88
    .line 89
    goto :goto_1

    .line 90
    :cond_1
    move-object v0, v1

    .line 91
    :goto_1
    move-object v2, p1

    .line 92
    check-cast v2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;

    .line 93
    .line 94
    iget-object v3, v2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;->〇OOo8〇0:Landroid/widget/TextView;

    .line 95
    .line 96
    if-eqz v0, :cond_2

    .line 97
    .line 98
    invoke-virtual {v0}, Lcom/intsig/camscanner/test/AppConfigEntity;->getKey()Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v4

    .line 102
    goto :goto_2

    .line 103
    :cond_2
    move-object v4, v1

    .line 104
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    .line 105
    .line 106
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    .line 108
    .line 109
    const-string v6, "key: "

    .line 110
    .line 111
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object v4

    .line 121
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    .line 123
    .line 124
    iget-object v3, v2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;->OO:Landroid/widget/TextView;

    .line 125
    .line 126
    if-eqz v0, :cond_3

    .line 127
    .line 128
    invoke-virtual {v0}, Lcom/intsig/camscanner/test/AppConfigEntity;->getValue()Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v1

    .line 132
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    .line 133
    .line 134
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 135
    .line 136
    .line 137
    const-string v5, "value: "

    .line 138
    .line 139
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    .line 141
    .line 142
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v1

    .line 149
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    .line 151
    .line 152
    iget-object v1, v2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 153
    .line 154
    const/16 v3, 0x8

    .line 155
    .line 156
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 157
    .line 158
    .line 159
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->〇080OO8〇0:[Ljava/lang/String;

    .line 160
    .line 161
    iget v3, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->〇0O:I

    .line 162
    .line 163
    add-int/2addr v3, p2

    .line 164
    array-length p2, v1

    .line 165
    rem-int/2addr v3, p2

    .line 166
    aget-object p2, v1, v3

    .line 167
    .line 168
    iget-object v1, v2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;->o0:Landroid/view/View;

    .line 169
    .line 170
    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 171
    .line 172
    .line 173
    move-result p2

    .line 174
    invoke-virtual {v1, p2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 175
    .line 176
    .line 177
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 178
    .line 179
    new-instance p2, Lcom/intsig/camscanner/test/docjson/〇O80;

    .line 180
    .line 181
    invoke-direct {p2, p0, v0}, Lcom/intsig/camscanner/test/docjson/〇O80;-><init>(Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;Lcom/intsig/camscanner/test/AppConfigEntity;)V

    .line 182
    .line 183
    .line 184
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    .line 186
    .line 187
    :goto_3
    return-void

    .line 188
    nop

    .line 189
    :pswitch_data_0
    .packed-switch 0x7f0d03ac
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "parent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->〇OOo8〇0:Landroid/content/Context;

    .line 7
    .line 8
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    packed-switch p2, :pswitch_data_0

    .line 18
    .line 19
    .line 20
    new-instance p2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterTipsViewHolder;

    .line 21
    .line 22
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterTipsViewHolder;-><init>(Landroid/view/View;)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :pswitch_0
    new-instance p2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterFilterViewHolder;

    .line 27
    .line 28
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterFilterViewHolder;-><init>(Landroid/view/View;)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :pswitch_1
    new-instance p2, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;

    .line 33
    .line 34
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/test/AppConfigVisualAdapter$AppConfigVisualAdapterViewHolder;-><init>(Landroid/view/View;)V

    .line 35
    .line 36
    .line 37
    :goto_0
    return-object p2

    .line 38
    nop

    .line 39
    :pswitch_data_0
    .packed-switch 0x7f0d03ac
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public final 〇0〇O0088o(Ljava/lang/String;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->O8o08O8O:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_2

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->o0:Ljava/util/ArrayList;

    .line 11
    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v0, 0x0

    .line 22
    goto :goto_1

    .line 23
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 24
    :goto_1
    if-nez v0, :cond_2

    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->O8o08O8O:Ljava/util/ArrayList;

    .line 27
    .line 28
    iget-object v2, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->o0:Ljava/util/ArrayList;

    .line 29
    .line 30
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 34
    .line 35
    .line 36
    :cond_2
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 37
    .line 38
    .line 39
    iput-object p1, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->〇08O〇00〇o:Ljava/lang/String;

    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->o0:Ljava/util/ArrayList;

    .line 42
    .line 43
    if-eqz p1, :cond_3

    .line 44
    .line 45
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 46
    .line 47
    .line 48
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->〇08O〇00〇o:Ljava/lang/String;

    .line 49
    .line 50
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    if-eqz p1, :cond_4

    .line 55
    .line 56
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->o0:Ljava/util/ArrayList;

    .line 57
    .line 58
    if-eqz p1, :cond_6

    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->O8o08O8O:Ljava/util/ArrayList;

    .line 61
    .line 62
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 63
    .line 64
    .line 65
    goto :goto_3

    .line 66
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->O8o08O8O:Ljava/util/ArrayList;

    .line 67
    .line 68
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    :cond_5
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    if-eqz v0, :cond_6

    .line 77
    .line 78
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    check-cast v0, Lcom/intsig/camscanner/test/AppConfigEntity;

    .line 83
    .line 84
    invoke-virtual {v0}, Lcom/intsig/camscanner/test/AppConfigEntity;->getKey()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v2

    .line 88
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 89
    .line 90
    .line 91
    move-result v2

    .line 92
    if-nez v2, :cond_5

    .line 93
    .line 94
    invoke-virtual {v0}, Lcom/intsig/camscanner/test/AppConfigEntity;->getKey()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v2

    .line 98
    const-string v3, "item.key"

    .line 99
    .line 100
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    iget-object v3, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->〇08O〇00〇o:Ljava/lang/String;

    .line 104
    .line 105
    const/4 v4, 0x2

    .line 106
    const/4 v5, 0x0

    .line 107
    invoke-static {v2, v3, v1, v4, v5}, Lkotlin/text/StringsKt;->Oo8Oo00oo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    .line 108
    .line 109
    .line 110
    move-result v2

    .line 111
    if-eqz v2, :cond_5

    .line 112
    .line 113
    iget-object v2, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->o0:Ljava/util/ArrayList;

    .line 114
    .line 115
    if-eqz v2, :cond_5

    .line 116
    .line 117
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    .line 119
    .line 120
    goto :goto_2

    .line 121
    :cond_6
    :goto_3
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 122
    .line 123
    .line 124
    return-void
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public final 〇O00()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/SharePreferenceFragment$SharePreferenceAdapter;->〇08O〇00〇o:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

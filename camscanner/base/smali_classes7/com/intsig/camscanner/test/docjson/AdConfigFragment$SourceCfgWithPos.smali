.class public final Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;
.super Ljava/lang/Object;
.source "AdConfigFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/test/docjson/AdConfigFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SourceCfgWithPos"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final 〇080:Lcom/intsig/advertisement/bean/SourceCfg;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:I


# direct methods
.method public constructor <init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;I)V
    .locals 1
    .param p1    # Lcom/intsig/advertisement/bean/SourceCfg;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "sourceCfg"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "position"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇080:Lcom/intsig/advertisement/bean/SourceCfg;

    .line 3
    iput-object p2, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o00〇〇Oo:Ljava/lang/String;

    .line 4
    iput p3, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o〇:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, -0x1

    .line 5
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    instance-of v1, p1, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-nez v1, :cond_1

    .line 9
    .line 10
    return v2

    .line 11
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇080:Lcom/intsig/advertisement/bean/SourceCfg;

    .line 14
    .line 15
    iget-object v3, p1, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇080:Lcom/intsig/advertisement/bean/SourceCfg;

    .line 16
    .line 17
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-nez v1, :cond_2

    .line 22
    .line 23
    return v2

    .line 24
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o00〇〇Oo:Ljava/lang/String;

    .line 25
    .line 26
    iget-object v3, p1, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o00〇〇Oo:Ljava/lang/String;

    .line 27
    .line 28
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    if-nez v1, :cond_3

    .line 33
    .line 34
    return v2

    .line 35
    :cond_3
    iget v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o〇:I

    .line 36
    .line 37
    iget p1, p1, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o〇:I

    .line 38
    .line 39
    if-eq v1, p1, :cond_4

    .line 40
    .line 41
    return v2

    .line 42
    :cond_4
    return v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇080:Lcom/intsig/advertisement/bean/SourceCfg;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    mul-int/lit8 v0, v0, 0x1f

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o00〇〇Oo:Ljava/lang/String;

    .line 10
    .line 11
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    add-int/2addr v0, v1

    .line 16
    mul-int/lit8 v0, v0, 0x1f

    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o〇:I

    .line 19
    .line 20
    add-int/2addr v0, v1

    .line 21
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇080:Lcom/intsig/advertisement/bean/SourceCfg;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o00〇〇Oo:Ljava/lang/String;

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o〇:I

    .line 6
    .line 7
    new-instance v3, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v4, "SourceCfgWithPos(sourceCfg="

    .line 13
    .line 14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string v0, ", position="

    .line 21
    .line 22
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string v0, ", index="

    .line 29
    .line 30
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string v0, ")"

    .line 37
    .line 38
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public final 〇080()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o00〇〇Oo()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇()Lcom/intsig/advertisement/bean/SourceCfg;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇080:Lcom/intsig/advertisement/bean/SourceCfg;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/test/docjson/purchase/PurchaseDebugViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "PurchaseDebugViewModel.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final o0:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/test/docjson/purchase/type/IPurchaseDebugType;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Landroidx/lifecycle/LiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/LiveData<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/test/docjson/purchase/type/IPurchaseDebugType;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    .line 5
    .line 6
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/test/docjson/purchase/PurchaseDebugViewModel;->o0:Landroidx/lifecycle/MutableLiveData;

    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/test/docjson/purchase/PurchaseDebugViewModel;->〇OOo8〇0:Landroidx/lifecycle/LiveData;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final 〇80〇808〇O()Landroidx/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/LiveData<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/test/docjson/purchase/type/IPurchaseDebugType;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/purchase/PurchaseDebugViewModel;->〇OOo8〇0:Landroidx/lifecycle/LiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8o8o〇()V
    .locals 7

    .line 1
    const/4 v0, 0x7

    .line 2
    new-array v0, v0, [Lcom/intsig/camscanner/test/docjson/purchase/type/IPurchaseDebugType;

    .line 3
    .line 4
    new-instance v1, Lcom/intsig/camscanner/test/docjson/purchase/type/CommonPurchaseDebugType;

    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/test/docjson/purchase/type/CommonPurchaseDebugType;-><init>(I)V

    .line 8
    .line 9
    .line 10
    aput-object v1, v0, v2

    .line 11
    .line 12
    new-instance v1, Lcom/intsig/camscanner/test/docjson/purchase/type/CommonPurchaseDebugType;

    .line 13
    .line 14
    const/4 v3, 0x1

    .line 15
    invoke-direct {v1, v3}, Lcom/intsig/camscanner/test/docjson/purchase/type/CommonPurchaseDebugType;-><init>(I)V

    .line 16
    .line 17
    .line 18
    aput-object v1, v0, v3

    .line 19
    .line 20
    new-instance v1, Lcom/intsig/camscanner/test/docjson/purchase/type/CommonPurchaseDebugType;

    .line 21
    .line 22
    const/4 v4, 0x2

    .line 23
    invoke-direct {v1, v4}, Lcom/intsig/camscanner/test/docjson/purchase/type/CommonPurchaseDebugType;-><init>(I)V

    .line 24
    .line 25
    .line 26
    aput-object v1, v0, v4

    .line 27
    .line 28
    new-instance v1, Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;

    .line 29
    .line 30
    const-string v5, "guide\u9875"

    .line 31
    .line 32
    const-string v6, "\u5f53\u524d\u56fd\u5bb6\u6ca1\u6709guide\u4ed8\u8d39\u9875"

    .line 33
    .line 34
    invoke-direct {v1, v2, v5, v6}, Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    const/4 v2, 0x3

    .line 38
    aput-object v1, v0, v2

    .line 39
    .line 40
    new-instance v1, Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;

    .line 41
    .line 42
    const-string v5, "\u88ab\u52a8\u5f39\u7a97"

    .line 43
    .line 44
    const-string v6, "\u5f53\u524d\u56fd\u5bb6\u6ca1\u6709\u88ab\u52a8\u5f39\u7a97"

    .line 45
    .line 46
    invoke-direct {v1, v3, v5, v6}, Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    const/4 v3, 0x4

    .line 50
    aput-object v1, v0, v3

    .line 51
    .line 52
    new-instance v1, Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;

    .line 53
    .line 54
    const-string v5, "\u4e3b\u52a8\u5f39\u7a97"

    .line 55
    .line 56
    const-string v6, "\u5f53\u524d\u56fd\u5bb6\u6ca1\u6709\u4e3b\u52a8\u5f39\u7a97"

    .line 57
    .line 58
    invoke-direct {v1, v4, v5, v6}, Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    const/4 v4, 0x5

    .line 62
    aput-object v1, v0, v4

    .line 63
    .line 64
    new-instance v1, Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;

    .line 65
    .line 66
    const-string v5, "\u626b\u63cf\u540e\u4ed8\u8d39\u9875"

    .line 67
    .line 68
    const-string v6, "\u5f53\u524d\u56fd\u5bb6\u6ca1\u6709\u626b\u63cf\u540e\u4ed8\u8d39\u9875"

    .line 69
    .line 70
    invoke-direct {v1, v2, v5, v6}, Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    const/4 v2, 0x6

    .line 74
    aput-object v1, v0, v2

    .line 75
    .line 76
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->Oooo8o0〇([Ljava/lang/Object;)Ljava/util/List;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇()Z

    .line 81
    .line 82
    .line 83
    move-result v1

    .line 84
    if-eqz v1, :cond_0

    .line 85
    .line 86
    new-instance v1, Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;

    .line 87
    .line 88
    const-string v2, "\u56fd\u5185\u5b89\u5353\u6295\u653e\u7528\u6237\u7684\u626b\u63cf\u540e\u4ed8\u8d39\u9875\uff08\u8865\u66dd\u5149\uff09"

    .line 89
    .line 90
    const-string v5, "\u5f53\u524d\u56fd\u5bb6\u6ca1\u6709\u56fd\u5185\u5b89\u5353\u6295\u653e\u7528\u6237\u7684\u626b\u63cf\u540e\u4ed8\u8d39\u9875\uff08\u8865\u66dd\u5149\uff09"

    .line 91
    .line 92
    invoke-direct {v1, v3, v2, v5}, Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    .line 97
    .line 98
    :cond_0
    new-instance v1, Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;

    .line 99
    .line 100
    const-string v2, "\u8bc4\u8bba\u8bc4\u5206\u5f39\u7a97"

    .line 101
    .line 102
    const-string v3, "\u5f53\u524d\u56fd\u5bb6\u6ca1\u6709\u8bc4\u8bba\u8bc4\u5206\u5f39\u7a97"

    .line 103
    .line 104
    invoke-direct {v1, v4, v2, v3}, Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    .line 109
    .line 110
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/purchase/PurchaseDebugViewModel;->o0:Landroidx/lifecycle/MutableLiveData;

    .line 111
    .line 112
    invoke-virtual {v1, v0}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 113
    .line 114
    .line 115
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.class public final Lcom/intsig/camscanner/test/docjson/AdConfigFragment;
.super Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;
.source "AdConfigFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;,
        Lcom/intsig/camscanner/test/docjson/AdConfigFragment$OpeSourceCfg;,
        Lcom/intsig/camscanner/test/docjson/AdConfigFragment$AdConfigAdapter;,
        Lcom/intsig/camscanner/test/docjson/AdConfigFragment$Operation;,
        Lcom/intsig/camscanner/test/docjson/AdConfigFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final O008oO0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O00OoO〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O088O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O08〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O08〇oO8〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O0O0〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O0o0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O0oO:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O0〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O0〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O0〇8〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O80OO:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O88:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O880O〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O888Oo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O88O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O8O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O8o〇O0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O8〇:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O8〇8〇O80:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O8〇o0〇〇8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O8〇o〇88:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final OO0O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final OO0o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final OO0〇O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final OO8〇O8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final OOo00:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final OO〇000:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final OO〇80oO〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final OO〇OOo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final OO〇〇o0oO:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final Oo0O0o8:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final Oo0O〇8800:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final Oo0〇Ooo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final Oo80:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final OoO〇OOo8o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final Ooo08:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final Ooo8o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final OooO〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇00O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇080〇o0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇08oOOO0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇0O〇Oo〇o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇0o8o8〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇0o8〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇8O0O80〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇8〇008:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇O800oo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇o8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇o88o08〇:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇oo8O80:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇〇O80o8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final O〇〇o8O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o00〇88〇08:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o088O8800:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o0O0O〇〇〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o0OO:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o0Oo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o0OoOOo0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o0〇〇00:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o0〇〇00〇o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o808o8o08:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o88:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o880:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o88o88:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o88oo〇O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o8O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o8O〇008:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o8o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o8o0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o8o0o8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final o8oOOo:Lcom/intsig/camscanner/test/docjson/AdConfigFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o8oo0OOO:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o8〇OO:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oO00〇o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oO8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oO88〇0O8O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oO8o〇08〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oOO0880O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oOO8:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oOO8oo0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oOO〇〇:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oOoO8OO〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oOoo80oO:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oOo〇08〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oO〇8O8oOo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oO〇O0O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oO〇oo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oo0O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oo8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oo88:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oo8ooo8O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oo8〇〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final ooO:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final ooo008:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oooO888:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oooO8〇00:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oooo800〇〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final ooooo0O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o〇08oO80o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o〇0〇o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o〇O8OO:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o〇OoO0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o〇o08〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o〇o0oOO8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o〇o8〇〇O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o〇oO:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o〇oO08〇o0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o〇oo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o〇o〇Oo88:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇00O0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇00o〇O8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇00〇〇〇o〇8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇0888:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇088O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇08O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇08〇o0O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇0O8Oo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇0O〇O00O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇0o0oO〇〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇0o88Oo〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇0oO〇oo00:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇0ooOOo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇0o〇o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇0〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇0〇o8〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇800OO〇0O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇80O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇80O8o8O〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇80〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8O0880:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8o0o0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8oo0oO0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8oo8888:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8ooOO:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8ooo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8oo〇〇oO:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8〇0O〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8〇80o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8〇OOoooo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8〇o88:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8〇o〇OoO8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8〇〇8o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8〇〇8〇8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇O0o〇〇o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇O8oOo0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇O8〇8000:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇O8〇8O0oO:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇OO8ooO8〇:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇OO〇00〇0O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇OoO0o0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇Oo〇O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇O〇〇O8:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇o08:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇o0O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇o8〇8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇oO88o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇oOO80o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇oO〇08o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇ooO8Ooo〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇ooO〇000:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇oo〇O〇80:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇o〇88:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇o〇88〇8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇o〇OO80oO:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇8o0OOOo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇O80〇0o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇o0〇8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇o〇:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇〇00:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇〇0o〇〇0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇〇OOO〇〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇〇O〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O0O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final O8o08O8O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO〇00〇8oO:Lcom/intsig/app/AlertDialog;

.field private o8〇OO0〇0o:Landroidx/recyclerview/widget/RecyclerView;

.field private oOo0:Lcom/intsig/app/AlertDialog;

.field private oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private ooo0〇〇O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇080OO8〇0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/test/docjson/AdConfigFragment$OpeSourceCfg;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇8〇oO〇〇8o:Lcom/intsig/camscanner/test/docjson/AdConfigFragment$AdConfigAdapter;

.field private 〇〇08O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 19

    .line 1
    new-instance v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o8oOOo:Lcom/intsig/camscanner/test/docjson/AdConfigFragment$Companion;

    .line 8
    .line 9
    const-string v2, ""

    .line 10
    .line 11
    const-string v3, "app_launch"

    .line 12
    .line 13
    const-string v4, "share_done"

    .line 14
    .line 15
    const-string v5, "reward_video"

    .line 16
    .line 17
    const-string v6, "page_list_banner"

    .line 18
    .line 19
    const-string v7, "main_middle_banner"

    .line 20
    .line 21
    const-string v8, "scan_done_reward"

    .line 22
    .line 23
    const-string v9, "ocr_reward"

    .line 24
    .line 25
    const-string v10, "purchase_exit"

    .line 26
    .line 27
    const-string v11, "shot_done"

    .line 28
    .line 29
    const-string v12, "scan_done_top"

    .line 30
    .line 31
    const-string v13, "doc_list_popup"

    .line 32
    .line 33
    const-string v14, "function_reward"

    .line 34
    .line 35
    const-string v15, "doc_list"

    .line 36
    .line 37
    const-string v16, "scan_done"

    .line 38
    .line 39
    const-string v17, "space_lottery"

    .line 40
    .line 41
    const-string v18, "pdf_watermark_video"

    .line 42
    .line 43
    filled-new-array/range {v2 .. v18}, [Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇O〇〇O8:Ljava/util/ArrayList;

    .line 52
    .line 53
    const-string v1, "admob"

    .line 54
    .line 55
    const-string v2, "api"

    .line 56
    .line 57
    const-string v3, "applovin"

    .line 58
    .line 59
    const-string v4, "cs"

    .line 60
    .line 61
    const-string v5, "facebook"

    .line 62
    .line 63
    const-string v6, "huawei"

    .line 64
    .line 65
    const-string v7, "meishu"

    .line 66
    .line 67
    const-string v8, "pangle"

    .line 68
    .line 69
    const-string v9, "tencent"

    .line 70
    .line 71
    const-string v10, "toutiao"

    .line 72
    .line 73
    const-string v11, "traplus"

    .line 74
    .line 75
    const-string v12, "vungle"

    .line 76
    .line 77
    const-string v13, "xiaomi"

    .line 78
    .line 79
    filled-new-array/range {v1 .. v13}, [Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇o0O:Ljava/util/ArrayList;

    .line 88
    .line 89
    const-string v0, "native"

    .line 90
    .line 91
    const-string v1, "interstitial"

    .line 92
    .line 93
    const-string v2, "splash"

    .line 94
    .line 95
    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object v3

    .line 99
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 100
    .line 101
    .line 102
    move-result-object v3

    .line 103
    sput-object v3, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O88O:Ljava/util/ArrayList;

    .line 104
    .line 105
    const-string v4, "admob"

    .line 106
    .line 107
    const-string v5, "api"

    .line 108
    .line 109
    const-string v6, "cs"

    .line 110
    .line 111
    const-string v7, "facebook"

    .line 112
    .line 113
    const-string v8, "huawei"

    .line 114
    .line 115
    const-string v9, "meishu"

    .line 116
    .line 117
    const-string v10, "pangle"

    .line 118
    .line 119
    const-string v11, "tencent"

    .line 120
    .line 121
    const-string v12, "toutiao"

    .line 122
    .line 123
    const-string v13, "traplus"

    .line 124
    .line 125
    filled-new-array/range {v4 .. v13}, [Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v3

    .line 129
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 130
    .line 131
    .line 132
    move-result-object v3

    .line 133
    sput-object v3, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOO〇〇:Ljava/util/ArrayList;

    .line 134
    .line 135
    const-string v3, "banner"

    .line 136
    .line 137
    filled-new-array {v0, v3}, [Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    move-result-object v4

    .line 141
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 142
    .line 143
    .line 144
    move-result-object v4

    .line 145
    sput-object v4, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o8o:Ljava/util/ArrayList;

    .line 146
    .line 147
    const-string v5, "admob"

    .line 148
    .line 149
    const-string v6, "api"

    .line 150
    .line 151
    const-string v7, "applovin"

    .line 152
    .line 153
    const-string v8, "cs"

    .line 154
    .line 155
    const-string v9, "facebook"

    .line 156
    .line 157
    const-string v10, "huawei"

    .line 158
    .line 159
    const-string v11, "ironsrc"

    .line 160
    .line 161
    const-string v12, "meishu"

    .line 162
    .line 163
    const-string v13, "pangle"

    .line 164
    .line 165
    const-string v14, "tencent"

    .line 166
    .line 167
    const-string v15, "toutiao"

    .line 168
    .line 169
    const-string v16, "traplus"

    .line 170
    .line 171
    const-string v17, "vungle"

    .line 172
    .line 173
    filled-new-array/range {v5 .. v17}, [Ljava/lang/String;

    .line 174
    .line 175
    .line 176
    move-result-object v4

    .line 177
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 178
    .line 179
    .line 180
    move-result-object v4

    .line 181
    sput-object v4, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oo8ooo8O:Ljava/util/ArrayList;

    .line 182
    .line 183
    const-string v4, "rewardinters"

    .line 184
    .line 185
    filled-new-array {v0, v1, v2, v4, v3}, [Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v2

    .line 189
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 190
    .line 191
    .line 192
    move-result-object v2

    .line 193
    sput-object v2, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o〇oO:Ljava/util/ArrayList;

    .line 194
    .line 195
    const-string v5, "admob"

    .line 196
    .line 197
    const-string v6, "api"

    .line 198
    .line 199
    const-string v7, "cs"

    .line 200
    .line 201
    const-string v8, "facebook"

    .line 202
    .line 203
    const-string v9, "huawei"

    .line 204
    .line 205
    const-string v10, "meishu"

    .line 206
    .line 207
    const-string v11, "pangle"

    .line 208
    .line 209
    const-string v12, "tencent"

    .line 210
    .line 211
    const-string v13, "toutiao"

    .line 212
    .line 213
    const-string v14, "traplus"

    .line 214
    .line 215
    filled-new-array/range {v5 .. v14}, [Ljava/lang/String;

    .line 216
    .line 217
    .line 218
    move-result-object v2

    .line 219
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 220
    .line 221
    .line 222
    move-result-object v2

    .line 223
    sput-object v2, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇08〇o0O:Ljava/util/ArrayList;

    .line 224
    .line 225
    filled-new-array {v0}, [Ljava/lang/String;

    .line 226
    .line 227
    .line 228
    move-result-object v2

    .line 229
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 230
    .line 231
    .line 232
    move-result-object v2

    .line 233
    sput-object v2, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇o〇:Ljava/util/ArrayList;

    .line 234
    .line 235
    const-string v2, "cs"

    .line 236
    .line 237
    filled-new-array {v2}, [Ljava/lang/String;

    .line 238
    .line 239
    .line 240
    move-result-object v5

    .line 241
    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 242
    .line 243
    .line 244
    move-result-object v5

    .line 245
    sput-object v5, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->Oo80:Ljava/util/ArrayList;

    .line 246
    .line 247
    filled-new-array {v0}, [Ljava/lang/String;

    .line 248
    .line 249
    .line 250
    move-result-object v5

    .line 251
    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 252
    .line 253
    .line 254
    move-result-object v5

    .line 255
    sput-object v5, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇o88o08〇:Ljava/util/ArrayList;

    .line 256
    .line 257
    const-string v6, "admob"

    .line 258
    .line 259
    const-string v7, "applovin"

    .line 260
    .line 261
    const-string v8, "cs"

    .line 262
    .line 263
    const-string v9, "pangle"

    .line 264
    .line 265
    const-string v10, "toutiao"

    .line 266
    .line 267
    const-string v11, "vungle"

    .line 268
    .line 269
    filled-new-array/range {v6 .. v11}, [Ljava/lang/String;

    .line 270
    .line 271
    .line 272
    move-result-object v5

    .line 273
    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 274
    .line 275
    .line 276
    move-result-object v5

    .line 277
    sput-object v5, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇00O0:Ljava/util/ArrayList;

    .line 278
    .line 279
    const-string v5, "rewardvideo"

    .line 280
    .line 281
    filled-new-array {v5}, [Ljava/lang/String;

    .line 282
    .line 283
    .line 284
    move-result-object v6

    .line 285
    invoke-static {v6}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 286
    .line 287
    .line 288
    move-result-object v6

    .line 289
    sput-object v6, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇08oOOO0:Ljava/util/ArrayList;

    .line 290
    .line 291
    const-string v7, "admob"

    .line 292
    .line 293
    const-string v8, "applovin"

    .line 294
    .line 295
    const-string v9, "cs"

    .line 296
    .line 297
    const-string v10, "pangle"

    .line 298
    .line 299
    const-string v11, "toutiao"

    .line 300
    .line 301
    const-string v12, "vungle"

    .line 302
    .line 303
    filled-new-array/range {v7 .. v12}, [Ljava/lang/String;

    .line 304
    .line 305
    .line 306
    move-result-object v6

    .line 307
    invoke-static {v6}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 308
    .line 309
    .line 310
    move-result-object v6

    .line 311
    sput-object v6, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o8〇OO:Ljava/util/ArrayList;

    .line 312
    .line 313
    filled-new-array {v5}, [Ljava/lang/String;

    .line 314
    .line 315
    .line 316
    move-result-object v6

    .line 317
    invoke-static {v6}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 318
    .line 319
    .line 320
    move-result-object v6

    .line 321
    sput-object v6, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->Ooo08:Ljava/util/ArrayList;

    .line 322
    .line 323
    const-string v7, "admob"

    .line 324
    .line 325
    const-string v8, "applovin"

    .line 326
    .line 327
    const-string v9, "cs"

    .line 328
    .line 329
    const-string v10, "pangle"

    .line 330
    .line 331
    const-string v11, "toutiao"

    .line 332
    .line 333
    const-string v12, "vungle"

    .line 334
    .line 335
    filled-new-array/range {v7 .. v12}, [Ljava/lang/String;

    .line 336
    .line 337
    .line 338
    move-result-object v6

    .line 339
    invoke-static {v6}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 340
    .line 341
    .line 342
    move-result-object v6

    .line 343
    sput-object v6, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇OO8ooO8〇:Ljava/util/ArrayList;

    .line 344
    .line 345
    filled-new-array {v5}, [Ljava/lang/String;

    .line 346
    .line 347
    .line 348
    move-result-object v6

    .line 349
    invoke-static {v6}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 350
    .line 351
    .line 352
    move-result-object v6

    .line 353
    sput-object v6, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->ooO:Ljava/util/ArrayList;

    .line 354
    .line 355
    const-string v7, "admob"

    .line 356
    .line 357
    const-string v8, "applovin"

    .line 358
    .line 359
    const-string v9, "cs"

    .line 360
    .line 361
    const-string v10, "ironsrc"

    .line 362
    .line 363
    const-string v11, "tencent"

    .line 364
    .line 365
    const-string v12, "toutiao"

    .line 366
    .line 367
    const-string v13, "vungle"

    .line 368
    .line 369
    filled-new-array/range {v7 .. v13}, [Ljava/lang/String;

    .line 370
    .line 371
    .line 372
    move-result-object v6

    .line 373
    invoke-static {v6}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 374
    .line 375
    .line 376
    move-result-object v6

    .line 377
    sput-object v6, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇OO〇00〇0O:Ljava/util/ArrayList;

    .line 378
    .line 379
    filled-new-array {v1, v5, v4}, [Ljava/lang/String;

    .line 380
    .line 381
    .line 382
    move-result-object v4

    .line 383
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 384
    .line 385
    .line 386
    move-result-object v4

    .line 387
    sput-object v4, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 388
    .line 389
    const-string v6, "admob"

    .line 390
    .line 391
    const-string v7, "api"

    .line 392
    .line 393
    const-string v8, "applovin"

    .line 394
    .line 395
    const-string v9, "cs"

    .line 396
    .line 397
    const-string v10, "facebook"

    .line 398
    .line 399
    const-string v11, "huawei"

    .line 400
    .line 401
    const-string v12, "pangle"

    .line 402
    .line 403
    const-string v13, "tencent"

    .line 404
    .line 405
    const-string v14, "toutiao"

    .line 406
    .line 407
    const-string v15, "vungle"

    .line 408
    .line 409
    filled-new-array/range {v6 .. v15}, [Ljava/lang/String;

    .line 410
    .line 411
    .line 412
    move-result-object v4

    .line 413
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 414
    .line 415
    .line 416
    move-result-object v4

    .line 417
    sput-object v4, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇〇0o〇〇0:Ljava/util/ArrayList;

    .line 418
    .line 419
    filled-new-array {v0, v3}, [Ljava/lang/String;

    .line 420
    .line 421
    .line 422
    move-result-object v4

    .line 423
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 424
    .line 425
    .line 426
    move-result-object v4

    .line 427
    sput-object v4, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oO〇8O8oOo:Ljava/util/ArrayList;

    .line 428
    .line 429
    const-string v6, "admob"

    .line 430
    .line 431
    const-string v7, "api"

    .line 432
    .line 433
    const-string v8, "applovin"

    .line 434
    .line 435
    const-string v9, "cs"

    .line 436
    .line 437
    const-string v10, "huawei"

    .line 438
    .line 439
    const-string v11, "vungle"

    .line 440
    .line 441
    filled-new-array/range {v6 .. v11}, [Ljava/lang/String;

    .line 442
    .line 443
    .line 444
    move-result-object v4

    .line 445
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 446
    .line 447
    .line 448
    move-result-object v4

    .line 449
    sput-object v4, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0O〇O00O:Ljava/util/ArrayList;

    .line 450
    .line 451
    filled-new-array {v0, v3}, [Ljava/lang/String;

    .line 452
    .line 453
    .line 454
    move-result-object v4

    .line 455
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 456
    .line 457
    .line 458
    move-result-object v4

    .line 459
    sput-object v4, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o0OoOOo0:Ljava/util/ArrayList;

    .line 460
    .line 461
    const-string v4, "admob"

    .line 462
    .line 463
    const-string v6, "facebook"

    .line 464
    .line 465
    const-string v7, "ironsrc"

    .line 466
    .line 467
    const-string v8, "vungle"

    .line 468
    .line 469
    filled-new-array {v4, v6, v7, v8}, [Ljava/lang/String;

    .line 470
    .line 471
    .line 472
    move-result-object v6

    .line 473
    invoke-static {v6}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 474
    .line 475
    .line 476
    move-result-object v6

    .line 477
    sput-object v6, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o〇o〇Oo88:Ljava/util/ArrayList;

    .line 478
    .line 479
    filled-new-array {v1}, [Ljava/lang/String;

    .line 480
    .line 481
    .line 482
    move-result-object v6

    .line 483
    invoke-static {v6}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 484
    .line 485
    .line 486
    move-result-object v6

    .line 487
    sput-object v6, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oO00〇o:Ljava/util/ArrayList;

    .line 488
    .line 489
    const-string v6, "toutiao"

    .line 490
    .line 491
    filled-new-array {v6}, [Ljava/lang/String;

    .line 492
    .line 493
    .line 494
    move-result-object v7

    .line 495
    invoke-static {v7}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 496
    .line 497
    .line 498
    move-result-object v7

    .line 499
    sput-object v7, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOO0880O:Ljava/util/ArrayList;

    .line 500
    .line 501
    filled-new-array {v5}, [Ljava/lang/String;

    .line 502
    .line 503
    .line 504
    move-result-object v7

    .line 505
    invoke-static {v7}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 506
    .line 507
    .line 508
    move-result-object v7

    .line 509
    sput-object v7, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOoo80oO:Ljava/util/ArrayList;

    .line 510
    .line 511
    const-string v7, "applovin"

    .line 512
    .line 513
    const-string v9, "pangle"

    .line 514
    .line 515
    filled-new-array {v4, v7, v9, v6, v8}, [Ljava/lang/String;

    .line 516
    .line 517
    .line 518
    move-result-object v4

    .line 519
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 520
    .line 521
    .line 522
    move-result-object v4

    .line 523
    sput-object v4, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->Oo0O0o8:Ljava/util/ArrayList;

    .line 524
    .line 525
    filled-new-array {v1, v5}, [Ljava/lang/String;

    .line 526
    .line 527
    .line 528
    move-result-object v4

    .line 529
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 530
    .line 531
    .line 532
    move-result-object v4

    .line 533
    sput-object v4, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OO〇OOo:Ljava/util/ArrayList;

    .line 534
    .line 535
    const-string v5, "admob"

    .line 536
    .line 537
    const-string v6, "api"

    .line 538
    .line 539
    const-string v7, "applovin"

    .line 540
    .line 541
    const-string v8, "facebook"

    .line 542
    .line 543
    const-string v9, "pangle"

    .line 544
    .line 545
    const-string v10, "toutiao"

    .line 546
    .line 547
    const-string v11, "vungle"

    .line 548
    .line 549
    filled-new-array/range {v5 .. v11}, [Ljava/lang/String;

    .line 550
    .line 551
    .line 552
    move-result-object v4

    .line 553
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 554
    .line 555
    .line 556
    move-result-object v4

    .line 557
    sput-object v4, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇800OO〇0O:Ljava/util/ArrayList;

    .line 558
    .line 559
    filled-new-array {v0, v1, v3}, [Ljava/lang/String;

    .line 560
    .line 561
    .line 562
    move-result-object v1

    .line 563
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 564
    .line 565
    .line 566
    move-result-object v1

    .line 567
    sput-object v1, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O8o〇O0:Ljava/util/ArrayList;

    .line 568
    .line 569
    const-string v1, "api"

    .line 570
    .line 571
    filled-new-array {v1, v2}, [Ljava/lang/String;

    .line 572
    .line 573
    .line 574
    move-result-object v1

    .line 575
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 576
    .line 577
    .line 578
    move-result-object v1

    .line 579
    sput-object v1, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOO8:Ljava/util/ArrayList;

    .line 580
    .line 581
    filled-new-array {v0}, [Ljava/lang/String;

    .line 582
    .line 583
    .line 584
    move-result-object v0

    .line 585
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 586
    .line 587
    .line 588
    move-result-object v0

    .line 589
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇oo〇O〇80:Ljava/util/ArrayList;

    .line 590
    .line 591
    const-string v0, ""

    .line 592
    .line 593
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O8〇o〇88:Ljava/lang/String;

    .line 594
    .line 595
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇O:Ljava/lang/String;

    .line 596
    .line 597
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8〇o88:Ljava/lang/String;

    .line 598
    .line 599
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o8O:Ljava/lang/String;

    .line 600
    .line 601
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇80O8o8O〇:Ljava/lang/String;

    .line 602
    .line 603
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇08〇:Ljava/lang/String;

    .line 604
    .line 605
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇O8oOo0:Ljava/lang/String;

    .line 606
    .line 607
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oO〇oo:Ljava/lang/String;

    .line 608
    .line 609
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oooO888:Ljava/lang/String;

    .line 610
    .line 611
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o880:Ljava/lang/String;

    .line 612
    .line 613
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o00〇88〇08:Ljava/lang/String;

    .line 614
    .line 615
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇o0〇8:Ljava/lang/String;

    .line 616
    .line 617
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O0〇0:Ljava/lang/String;

    .line 618
    .line 619
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8〇OOoooo:Ljava/lang/String;

    .line 620
    .line 621
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o〇0〇o:Ljava/lang/String;

    .line 622
    .line 623
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇088O:Ljava/lang/String;

    .line 624
    .line 625
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇o〇88〇8:Ljava/lang/String;

    .line 626
    .line 627
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8〇80o:Ljava/lang/String;

    .line 628
    .line 629
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o〇O8OO:Ljava/lang/String;

    .line 630
    .line 631
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0〇0:Ljava/lang/String;

    .line 632
    .line 633
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOoO8OO〇:Ljava/lang/String;

    .line 634
    .line 635
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0ooOOo:Ljava/lang/String;

    .line 636
    .line 637
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->Ooo8o:Ljava/lang/String;

    .line 638
    .line 639
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇o08:Ljava/lang/String;

    .line 640
    .line 641
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇〇00:Ljava/lang/String;

    .line 642
    .line 643
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇〇0:Ljava/lang/String;

    .line 644
    .line 645
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇O80〇0o:Ljava/lang/String;

    .line 646
    .line 647
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0oO〇oo00:Ljava/lang/String;

    .line 648
    .line 649
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇O8〇8000:Ljava/lang/String;

    .line 650
    .line 651
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8O0880:Ljava/lang/String;

    .line 652
    .line 653
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇08O:Ljava/lang/String;

    .line 654
    .line 655
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇O0o〇〇o:Ljava/lang/String;

    .line 656
    .line 657
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O8〇8〇O80:Ljava/lang/String;

    .line 658
    .line 659
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O0O0〇:Ljava/lang/String;

    .line 660
    .line 661
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇Oo〇O:Ljava/lang/String;

    .line 662
    .line 663
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o〇oo:Ljava/lang/String;

    .line 664
    .line 665
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o88:Ljava/lang/String;

    .line 666
    .line 667
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇8〇008:Ljava/lang/String;

    .line 668
    .line 669
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇〇O〇:Ljava/lang/String;

    .line 670
    .line 671
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇O8〇8O0oO:Ljava/lang/String;

    .line 672
    .line 673
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇:Ljava/lang/String;

    .line 674
    .line 675
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O08〇:Ljava/lang/String;

    .line 676
    .line 677
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o0〇〇00:Ljava/lang/String;

    .line 678
    .line 679
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇oO〇08o:Ljava/lang/String;

    .line 680
    .line 681
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O0〇:Ljava/lang/String;

    .line 682
    .line 683
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇0O〇Oo〇o:Ljava/lang/String;

    .line 684
    .line 685
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇080〇o0:Ljava/lang/String;

    .line 686
    .line 687
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇〇O80o8:Ljava/lang/String;

    .line 688
    .line 689
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o0Oo:Ljava/lang/String;

    .line 690
    .line 691
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇oO88o:Ljava/lang/String;

    .line 692
    .line 693
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o808o8o08:Ljava/lang/String;

    .line 694
    .line 695
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OO0O:Ljava/lang/String;

    .line 696
    .line 697
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o〇08oO80o:Ljava/lang/String;

    .line 698
    .line 699
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o〇o08〇:Ljava/lang/String;

    .line 700
    .line 701
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OooO〇:Ljava/lang/String;

    .line 702
    .line 703
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O880O〇:Ljava/lang/String;

    .line 704
    .line 705
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OO〇〇o0oO:Ljava/lang/String;

    .line 706
    .line 707
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O8O:Ljava/lang/String;

    .line 708
    .line 709
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇ooO〇000:Ljava/lang/String;

    .line 710
    .line 711
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇ooO8Ooo〇:Ljava/lang/String;

    .line 712
    .line 713
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OoO〇OOo8o:Ljava/lang/String;

    .line 714
    .line 715
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇OoO0o0:Ljava/lang/String;

    .line 716
    .line 717
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o0O0O〇〇〇0:Ljava/lang/String;

    .line 718
    .line 719
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O88:Ljava/lang/String;

    .line 720
    .line 721
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇oOO80o:Ljava/lang/String;

    .line 722
    .line 723
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0o88Oo〇:Ljava/lang/String;

    .line 724
    .line 725
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O00OoO〇:Ljava/lang/String;

    .line 726
    .line 727
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇〇o8O:Ljava/lang/String;

    .line 728
    .line 729
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o0OO:Ljava/lang/String;

    .line 730
    .line 731
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o〇OoO0:Ljava/lang/String;

    .line 732
    .line 733
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OO0o:Ljava/lang/String;

    .line 734
    .line 735
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOO8oo0:Ljava/lang/String;

    .line 736
    .line 737
    const-string v1, "{\n                        \"max_impression\":100,\n                        \"name\":\"cs\",\n                        \"type\":\"native\",\n                        \"priority\":-1,\n                        \"min_interval\":0\n                    }\n"

    .line 738
    .line 739
    sput-object v1, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0o0oO〇〇0:Ljava/lang/String;

    .line 740
    .line 741
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O80OO:Ljava/lang/String;

    .line 742
    .line 743
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O8〇o0〇〇8:Ljava/lang/String;

    .line 744
    .line 745
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇o〇OO80oO:Ljava/lang/String;

    .line 746
    .line 747
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇00O:Ljava/lang/String;

    .line 748
    .line 749
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇0o8o8〇:Ljava/lang/String;

    .line 750
    .line 751
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O0o0:Ljava/lang/String;

    .line 752
    .line 753
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o8O〇008:Ljava/lang/String;

    .line 754
    .line 755
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇〇OOO〇〇:Ljava/lang/String;

    .line 756
    .line 757
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o88o88:Ljava/lang/String;

    .line 758
    .line 759
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8〇〇8o:Ljava/lang/String;

    .line 760
    .line 761
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇o〇88:Ljava/lang/String;

    .line 762
    .line 763
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oO〇O0O:Ljava/lang/String;

    .line 764
    .line 765
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8o0o0:Ljava/lang/String;

    .line 766
    .line 767
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇80〇:Ljava/lang/String;

    .line 768
    .line 769
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OO8〇O8:Ljava/lang/String;

    .line 770
    .line 771
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇o8:Ljava/lang/String;

    .line 772
    .line 773
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0O8Oo:Ljava/lang/String;

    .line 774
    .line 775
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o8o0o8:Ljava/lang/String;

    .line 776
    .line 777
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o88oo〇O:Ljava/lang/String;

    .line 778
    .line 779
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇0o8〇:Ljava/lang/String;

    .line 780
    .line 781
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OOo00:Ljava/lang/String;

    .line 782
    .line 783
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oO8:Ljava/lang/String;

    .line 784
    .line 785
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o088O8800:Ljava/lang/String;

    .line 786
    .line 787
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O08〇oO8〇:Ljava/lang/String;

    .line 788
    .line 789
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8oo8888:Ljava/lang/String;

    .line 790
    .line 791
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o0〇〇00〇o:Ljava/lang/String;

    .line 792
    .line 793
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇00o〇O8:Ljava/lang/String;

    .line 794
    .line 795
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O088O:Ljava/lang/String;

    .line 796
    .line 797
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o〇o0oOO8:Ljava/lang/String;

    .line 798
    .line 799
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8oo〇〇oO:Ljava/lang/String;

    .line 800
    .line 801
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OO〇000:Ljava/lang/String;

    .line 802
    .line 803
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oo8〇〇:Ljava/lang/String;

    .line 804
    .line 805
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇80O:Ljava/lang/String;

    .line 806
    .line 807
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8oo0oO0:Ljava/lang/String;

    .line 808
    .line 809
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇O800oo:Ljava/lang/String;

    .line 810
    .line 811
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oooO8〇00:Ljava/lang/String;

    .line 812
    .line 813
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇8O0O80〇:Ljava/lang/String;

    .line 814
    .line 815
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oo88:Ljava/lang/String;

    .line 816
    .line 817
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇8:Ljava/lang/String;

    .line 818
    .line 819
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇oo8O80:Ljava/lang/String;

    .line 820
    .line 821
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oo8:Ljava/lang/String;

    .line 822
    .line 823
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->ooooo0O:Ljava/lang/String;

    .line 824
    .line 825
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0888:Ljava/lang/String;

    .line 826
    .line 827
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8〇0O〇:Ljava/lang/String;

    .line 828
    .line 829
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o8o0:Ljava/lang/String;

    .line 830
    .line 831
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇o8〇8:Ljava/lang/String;

    .line 832
    .line 833
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇0:Ljava/lang/String;

    .line 834
    .line 835
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O888Oo:Ljava/lang/String;

    .line 836
    .line 837
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8ooo:Ljava/lang/String;

    .line 838
    .line 839
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oO88〇0O8O:Ljava/lang/String;

    .line 840
    .line 841
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8ooOO:Ljava/lang/String;

    .line 842
    .line 843
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8O:Ljava/lang/String;

    .line 844
    .line 845
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->Oo0O〇8800:Ljava/lang/String;

    .line 846
    .line 847
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o〇o8〇〇O:Ljava/lang/String;

    .line 848
    .line 849
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O008oO0:Ljava/lang/String;

    .line 850
    .line 851
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0〇o8〇:Ljava/lang/String;

    .line 852
    .line 853
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇00〇〇〇o〇8:Ljava/lang/String;

    .line 854
    .line 855
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oO8o〇08〇:Ljava/lang/String;

    .line 856
    .line 857
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O0oO:Ljava/lang/String;

    .line 858
    .line 859
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇8o0OOOo:Ljava/lang/String;

    .line 860
    .line 861
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OO0〇O:Ljava/lang/String;

    .line 862
    .line 863
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0o〇o:Ljava/lang/String;

    .line 864
    .line 865
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->ooo008:Ljava/lang/String;

    .line 866
    .line 867
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oo0O:Ljava/lang/String;

    .line 868
    .line 869
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oooo800〇〇:Ljava/lang/String;

    .line 870
    .line 871
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8〇o〇OoO8:Ljava/lang/String;

    .line 872
    .line 873
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OO〇80oO〇:Ljava/lang/String;

    .line 874
    .line 875
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O0〇8〇:Ljava/lang/String;

    .line 876
    .line 877
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o〇oO08〇o0:Ljava/lang/String;

    .line 878
    .line 879
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o8oo0OOO:Ljava/lang/String;

    .line 880
    .line 881
    sput-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8〇〇8〇8:Ljava/lang/String;

    .line 882
    .line 883
    new-instance v2, Ljava/util/LinkedHashMap;

    .line 884
    .line 885
    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 886
    .line 887
    .line 888
    const-string v3, "app_launch_cs_splash"

    .line 889
    .line 890
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 891
    .line 892
    .line 893
    const-string v3, "app_launch_api_splash"

    .line 894
    .line 895
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 896
    .line 897
    .line 898
    const-string v3, "app_launch_tencent_splash"

    .line 899
    .line 900
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 901
    .line 902
    .line 903
    const-string v3, "app_launch_toutiao_splash"

    .line 904
    .line 905
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 906
    .line 907
    .line 908
    const-string v3, "app_launch_admob_splash"

    .line 909
    .line 910
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 911
    .line 912
    .line 913
    const-string v3, "app_launch_meishu_splash"

    .line 914
    .line 915
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 916
    .line 917
    .line 918
    const-string v3, "app_launch_tradplus_splash"

    .line 919
    .line 920
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 921
    .line 922
    .line 923
    const-string v3, "app_launch_huawei_splash"

    .line 924
    .line 925
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 926
    .line 927
    .line 928
    const-string v3, "app_launch_admob_native"

    .line 929
    .line 930
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 931
    .line 932
    .line 933
    const-string v3, "app_launch_toutiao_native"

    .line 934
    .line 935
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 936
    .line 937
    .line 938
    const-string v3, "app_launch_vungle_native"

    .line 939
    .line 940
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 941
    .line 942
    .line 943
    const-string v3, "app_launch_facebook_native"

    .line 944
    .line 945
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 946
    .line 947
    .line 948
    const-string v3, "app_launch_xiaomi_splash"

    .line 949
    .line 950
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 951
    .line 952
    .line 953
    const-string v3, "app_launch_admob_interstitial"

    .line 954
    .line 955
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 956
    .line 957
    .line 958
    const-string v3, "app_launch_applovin_interstitial"

    .line 959
    .line 960
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 961
    .line 962
    .line 963
    const-string v3, "app_launch_pangle_splash"

    .line 964
    .line 965
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 966
    .line 967
    .line 968
    const-string v3, "app_launch_pangle_interstitial"

    .line 969
    .line 970
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 971
    .line 972
    .line 973
    const-string v3, "doc_list_cs_native"

    .line 974
    .line 975
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 976
    .line 977
    .line 978
    const-string v3, "doc_list_tencent_native"

    .line 979
    .line 980
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 981
    .line 982
    .line 983
    const-string v3, "doc_list_admob_native"

    .line 984
    .line 985
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 986
    .line 987
    .line 988
    const-string v3, "doc_list_api_native"

    .line 989
    .line 990
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 991
    .line 992
    .line 993
    const-string v3, "doc_list_facebook_native"

    .line 994
    .line 995
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 996
    .line 997
    .line 998
    const-string v3, "doc_list_toutiao_native"

    .line 999
    .line 1000
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1001
    .line 1002
    .line 1003
    const-string v3, "doc_list_meishu_native"

    .line 1004
    .line 1005
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1006
    .line 1007
    .line 1008
    const-string v3, "doc_list_tradplus_native"

    .line 1009
    .line 1010
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011
    .line 1012
    .line 1013
    const-string v3, "doc_list_huawei_native"

    .line 1014
    .line 1015
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1016
    .line 1017
    .line 1018
    const-string v3, "doc_list_cs_banner"

    .line 1019
    .line 1020
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1021
    .line 1022
    .line 1023
    const-string v3, "doc_list_pangle_native"

    .line 1024
    .line 1025
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1026
    .line 1027
    .line 1028
    const-string v3, "doc_list_popup_api_native"

    .line 1029
    .line 1030
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1031
    .line 1032
    .line 1033
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1034
    .line 1035
    .line 1036
    const-string v3, "page_list_banner_tencent_banner"

    .line 1037
    .line 1038
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1039
    .line 1040
    .line 1041
    const-string v3, "page_list_banner_admob_banner"

    .line 1042
    .line 1043
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1044
    .line 1045
    .line 1046
    const-string v3, "page_list_banner_toutiao_banner"

    .line 1047
    .line 1048
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1049
    .line 1050
    .line 1051
    const-string v3, "page_list_banner_facebook_banner"

    .line 1052
    .line 1053
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1054
    .line 1055
    .line 1056
    const-string v3, "page_list_banner_vungle_banner"

    .line 1057
    .line 1058
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1059
    .line 1060
    .line 1061
    const-string v3, "page_list_banner_huawei_banner"

    .line 1062
    .line 1063
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1064
    .line 1065
    .line 1066
    const-string v3, "page_list_banner_applovin_banner"

    .line 1067
    .line 1068
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069
    .line 1070
    .line 1071
    const-string v3, "page_list_banner_cs_native"

    .line 1072
    .line 1073
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1074
    .line 1075
    .line 1076
    const-string v3, "page_list_banner_api_native"

    .line 1077
    .line 1078
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1079
    .line 1080
    .line 1081
    const-string v3, "page_list_banner_tencent_native"

    .line 1082
    .line 1083
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1084
    .line 1085
    .line 1086
    const-string v3, "page_list_banner_toutiao_native"

    .line 1087
    .line 1088
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1089
    .line 1090
    .line 1091
    const-string v3, "page_list_banner_pangle_banner"

    .line 1092
    .line 1093
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1094
    .line 1095
    .line 1096
    const-string v3, "main_middle_banner_admob_banner"

    .line 1097
    .line 1098
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1099
    .line 1100
    .line 1101
    const-string v3, "main_middle_banner_vungle_banner"

    .line 1102
    .line 1103
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1104
    .line 1105
    .line 1106
    const-string v3, "main_middle_banner_huawei_banner"

    .line 1107
    .line 1108
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1109
    .line 1110
    .line 1111
    const-string v3, "main_middle_banner_applovin_banner"

    .line 1112
    .line 1113
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1114
    .line 1115
    .line 1116
    const-string v3, "main_middle_banner_cs_native"

    .line 1117
    .line 1118
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1119
    .line 1120
    .line 1121
    const-string v3, "main_middle_banner_api_native"

    .line 1122
    .line 1123
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1124
    .line 1125
    .line 1126
    const-string v3, "scan_done_reward_admob_interstitial"

    .line 1127
    .line 1128
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1129
    .line 1130
    .line 1131
    const-string v3, "scan_done_reward_vungle_interstitial"

    .line 1132
    .line 1133
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1134
    .line 1135
    .line 1136
    const-string v3, "scan_done_reward_facebook_interstitial"

    .line 1137
    .line 1138
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1139
    .line 1140
    .line 1141
    const-string v3, "scan_done_reward_ironsrc_interstitial"

    .line 1142
    .line 1143
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1144
    .line 1145
    .line 1146
    const-string v3, "pdf_watermark_video_admob_rewardvideo"

    .line 1147
    .line 1148
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1149
    .line 1150
    .line 1151
    const-string v3, "pdf_watermark_video_vungle_rewardvideo"

    .line 1152
    .line 1153
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1154
    .line 1155
    .line 1156
    const-string v3, "pdf_watermark_video_toutiao_rewardvideo"

    .line 1157
    .line 1158
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1159
    .line 1160
    .line 1161
    const-string v3, "pdf_watermark_video_cs_rewardvideo"

    .line 1162
    .line 1163
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1164
    .line 1165
    .line 1166
    const-string v3, "pdf_watermark_video_applovin_rewardvideo"

    .line 1167
    .line 1168
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1169
    .line 1170
    .line 1171
    const-string v3, "pdf_watermark_video_tencent_interstitial"

    .line 1172
    .line 1173
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1174
    .line 1175
    .line 1176
    const-string v3, "pdf_watermark_video_toutiao_interstitial"

    .line 1177
    .line 1178
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1179
    .line 1180
    .line 1181
    const-string v3, "pdf_watermark_video_admob_interstitial"

    .line 1182
    .line 1183
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1184
    .line 1185
    .line 1186
    const-string v3, "pdf_watermark_video_vungle_interstitial"

    .line 1187
    .line 1188
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1189
    .line 1190
    .line 1191
    const-string v3, "pdf_watermark_video_ironsrc_interstitial"

    .line 1192
    .line 1193
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1194
    .line 1195
    .line 1196
    const-string v3, "pdf_watermark_video_admob_rewardinters"

    .line 1197
    .line 1198
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1199
    .line 1200
    .line 1201
    const-string v3, "scan_done_cs_native"

    .line 1202
    .line 1203
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1204
    .line 1205
    .line 1206
    const-string v3, "scan_done_api_native"

    .line 1207
    .line 1208
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1209
    .line 1210
    .line 1211
    const-string v3, "scan_done_tencent_native"

    .line 1212
    .line 1213
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1214
    .line 1215
    .line 1216
    const-string v3, "scan_done_toutiao_native"

    .line 1217
    .line 1218
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1219
    .line 1220
    .line 1221
    const-string v3, "scan_done_meishu_native"

    .line 1222
    .line 1223
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1224
    .line 1225
    .line 1226
    const-string v3, "scan_done_tradplus_native"

    .line 1227
    .line 1228
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1229
    .line 1230
    .line 1231
    const-string v3, "scan_done_huawei_native"

    .line 1232
    .line 1233
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1234
    .line 1235
    .line 1236
    const-string v3, "scan_done_admob_native"

    .line 1237
    .line 1238
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1239
    .line 1240
    .line 1241
    const-string v3, "scan_done_facebook_native"

    .line 1242
    .line 1243
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1244
    .line 1245
    .line 1246
    const-string v3, "scan_done_top_cs_native"

    .line 1247
    .line 1248
    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1249
    .line 1250
    .line 1251
    const-string v1, "share_done_cs_native"

    .line 1252
    .line 1253
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1254
    .line 1255
    .line 1256
    const-string v1, "share_done_api_native"

    .line 1257
    .line 1258
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1259
    .line 1260
    .line 1261
    const-string v1, "share_done_admob_native"

    .line 1262
    .line 1263
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1264
    .line 1265
    .line 1266
    const-string v1, "share_done_tencent_native"

    .line 1267
    .line 1268
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1269
    .line 1270
    .line 1271
    const-string v1, "share_done_toutiao_native"

    .line 1272
    .line 1273
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1274
    .line 1275
    .line 1276
    const-string v1, "share_done_facebook_native"

    .line 1277
    .line 1278
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1279
    .line 1280
    .line 1281
    const-string v1, "share_done_applovin_native"

    .line 1282
    .line 1283
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1284
    .line 1285
    .line 1286
    const-string v1, "share_done_admob_interstitial"

    .line 1287
    .line 1288
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1289
    .line 1290
    .line 1291
    const-string v1, "share_done_tencent_interstitial"

    .line 1292
    .line 1293
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1294
    .line 1295
    .line 1296
    const-string v1, "share_done_toutiao_interstitial"

    .line 1297
    .line 1298
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1299
    .line 1300
    .line 1301
    const-string v1, "share_done_vungle_interstitial"

    .line 1302
    .line 1303
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1304
    .line 1305
    .line 1306
    const-string v1, "share_done_facebook_interstitial"

    .line 1307
    .line 1308
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309
    .line 1310
    .line 1311
    const-string v1, "share_done_ironsrc_interstitial"

    .line 1312
    .line 1313
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1314
    .line 1315
    .line 1316
    const-string v1, "share_done_meishu_interstitial"

    .line 1317
    .line 1318
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319
    .line 1320
    .line 1321
    const-string v1, "share_done_api_interstitial"

    .line 1322
    .line 1323
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1324
    .line 1325
    .line 1326
    const-string v1, "share_done_tradplus_interstitial"

    .line 1327
    .line 1328
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1329
    .line 1330
    .line 1331
    const-string v1, "share_done_huawei_interstitial"

    .line 1332
    .line 1333
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1334
    .line 1335
    .line 1336
    const-string v1, "share_done_applovin_interstitial"

    .line 1337
    .line 1338
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339
    .line 1340
    .line 1341
    const-string v1, "share_done_pangle_interstitial"

    .line 1342
    .line 1343
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1344
    .line 1345
    .line 1346
    const-string v1, "share_done_huawei_splash"

    .line 1347
    .line 1348
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1349
    .line 1350
    .line 1351
    const-string v1, "share_done_tencent_splash"

    .line 1352
    .line 1353
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1354
    .line 1355
    .line 1356
    const-string v1, "share_done_cs_splash"

    .line 1357
    .line 1358
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1359
    .line 1360
    .line 1361
    const-string v1, "share_done_api_splash"

    .line 1362
    .line 1363
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1364
    .line 1365
    .line 1366
    const-string v1, "share_done_toutiao_splash"

    .line 1367
    .line 1368
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1369
    .line 1370
    .line 1371
    const-string v1, "share_done_meishu_splash"

    .line 1372
    .line 1373
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1374
    .line 1375
    .line 1376
    const-string v1, "share_done_tradplus_splash"

    .line 1377
    .line 1378
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1379
    .line 1380
    .line 1381
    const-string v1, "share_done_admob_rewardinters"

    .line 1382
    .line 1383
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1384
    .line 1385
    .line 1386
    const-string v1, "share_done_admob_banner"

    .line 1387
    .line 1388
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1389
    .line 1390
    .line 1391
    const-string v1, "share_done_facebook_banner"

    .line 1392
    .line 1393
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1394
    .line 1395
    .line 1396
    const-string v1, "share_done_applovin_banner"

    .line 1397
    .line 1398
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1399
    .line 1400
    .line 1401
    const-string v1, "share_done_vungle_banner"

    .line 1402
    .line 1403
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1404
    .line 1405
    .line 1406
    const-string v1, "shot_done_admob_interstitial"

    .line 1407
    .line 1408
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1409
    .line 1410
    .line 1411
    const-string v1, "shot_done_vungle_interstitial"

    .line 1412
    .line 1413
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1414
    .line 1415
    .line 1416
    const-string v1, "shot_done_toutiao_interstitial"

    .line 1417
    .line 1418
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1419
    .line 1420
    .line 1421
    const-string v1, "shot_done_applovin_interstitial"

    .line 1422
    .line 1423
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1424
    .line 1425
    .line 1426
    const-string v1, "shot_done_pangle_interstitial"

    .line 1427
    .line 1428
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1429
    .line 1430
    .line 1431
    const-string v1, "shot_done_api_native"

    .line 1432
    .line 1433
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1434
    .line 1435
    .line 1436
    const-string v1, "shot_done_admob_native"

    .line 1437
    .line 1438
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1439
    .line 1440
    .line 1441
    const-string v1, "shot_done_facebook_native"

    .line 1442
    .line 1443
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1444
    .line 1445
    .line 1446
    const-string v1, "shot_done_applovin_native"

    .line 1447
    .line 1448
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1449
    .line 1450
    .line 1451
    const-string v1, "shot_done_admob_banner"

    .line 1452
    .line 1453
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1454
    .line 1455
    .line 1456
    const-string v1, "shot_done_facebook_banner"

    .line 1457
    .line 1458
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1459
    .line 1460
    .line 1461
    const-string v1, "shot_done_applovin_banner"

    .line 1462
    .line 1463
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1464
    .line 1465
    .line 1466
    const-string v1, "shot_done_vungle_banner"

    .line 1467
    .line 1468
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1469
    .line 1470
    .line 1471
    const-string v1, "function_reward_toutiao_rewardvideo"

    .line 1472
    .line 1473
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1474
    .line 1475
    .line 1476
    const-string v1, "function_reward_admob_rewardvideo"

    .line 1477
    .line 1478
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1479
    .line 1480
    .line 1481
    const-string v1, "function_reward_vungle_rewardvideo"

    .line 1482
    .line 1483
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1484
    .line 1485
    .line 1486
    const-string v1, "function_reward_applovin_rewardvideo"

    .line 1487
    .line 1488
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1489
    .line 1490
    .line 1491
    const-string v1, "function_reward_pangle_rewardvideo"

    .line 1492
    .line 1493
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1494
    .line 1495
    .line 1496
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1497
    .line 1498
    .line 1499
    const-string v1, "function_reward_admob_interstitial"

    .line 1500
    .line 1501
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1502
    .line 1503
    .line 1504
    const-string v1, "function_reward_vungle_interstitial"

    .line 1505
    .line 1506
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1507
    .line 1508
    .line 1509
    const-string v1, "function_reward_applovin_interstitial"

    .line 1510
    .line 1511
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1512
    .line 1513
    .line 1514
    const-string v1, "purchase_exit_admob_rewardvideo"

    .line 1515
    .line 1516
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1517
    .line 1518
    .line 1519
    const-string v1, "purchase_exit_vungle_rewardvideo"

    .line 1520
    .line 1521
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522
    .line 1523
    .line 1524
    const-string v1, "purchase_exit_toutiao_rewardvideo"

    .line 1525
    .line 1526
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1527
    .line 1528
    .line 1529
    const-string v1, "purchase_exit_cs_rewardvideo"

    .line 1530
    .line 1531
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1532
    .line 1533
    .line 1534
    const-string v1, "purchase_exit_applovin_rewardvideo"

    .line 1535
    .line 1536
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537
    .line 1538
    .line 1539
    const-string v1, "purchase_exit_pangle_rewardvideo"

    .line 1540
    .line 1541
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1542
    .line 1543
    .line 1544
    const-string v1, "reward_video_admob_rewardvideo"

    .line 1545
    .line 1546
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1547
    .line 1548
    .line 1549
    const-string v1, "reward_video_vungle_rewardvideo"

    .line 1550
    .line 1551
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1552
    .line 1553
    .line 1554
    const-string v1, "reward_video_toutiao_rewardvideo"

    .line 1555
    .line 1556
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1557
    .line 1558
    .line 1559
    const-string v1, "reward_video_cs_rewardvideo"

    .line 1560
    .line 1561
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1562
    .line 1563
    .line 1564
    const-string v1, "reward_video_applovin_rewardvideo"

    .line 1565
    .line 1566
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1567
    .line 1568
    .line 1569
    const-string v1, "reward_video_pangle_rewardvideo"

    .line 1570
    .line 1571
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1572
    .line 1573
    .line 1574
    const-string v1, "ocr_reward_toutiao_rewardvideo"

    .line 1575
    .line 1576
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1577
    .line 1578
    .line 1579
    const-string v1, "space_lottery_admob_rewardvideo"

    .line 1580
    .line 1581
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1582
    .line 1583
    .line 1584
    const-string v1, "space_lottery_vungle_rewardvideo"

    .line 1585
    .line 1586
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1587
    .line 1588
    .line 1589
    const-string v1, "space_lottery_toutiao_rewardvideo"

    .line 1590
    .line 1591
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1592
    .line 1593
    .line 1594
    const-string v1, "space_lottery_cs_rewardvideo"

    .line 1595
    .line 1596
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1597
    .line 1598
    .line 1599
    const-string v1, "space_lottery_applovin_rewardvideo"

    .line 1600
    .line 1601
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1602
    .line 1603
    .line 1604
    const-string v1, "space_lottery_pangle_rewardvideo"

    .line 1605
    .line 1606
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1607
    .line 1608
    .line 1609
    sput-object v2, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O8〇:Ljava/util/Map;

    .line 1610
    .line 1611
    return-void
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
    .line 1825
    .line 1826
    .line 1827
    .line 1828
    .line 1829
    .line 1830
    .line 1831
    .line 1832
    .line 1833
    .line 1834
    .line 1835
    .line 1836
    .line 1837
    .line 1838
    .line 1839
    .line 1840
    .line 1841
    .line 1842
    .line 1843
    .line 1844
    .line 1845
    .line 1846
    .line 1847
    .line 1848
    .line 1849
    .line 1850
    .line 1851
    .line 1852
    .line 1853
    .line 1854
    .line 1855
    .line 1856
    .line 1857
    .line 1858
    .line 1859
    .line 1860
    .line 1861
    .line 1862
    .line 1863
    .line 1864
    .line 1865
    .line 1866
    .line 1867
    .line 1868
    .line 1869
    .line 1870
    .line 1871
    .line 1872
    .line 1873
    .line 1874
    .line 1875
    .line 1876
    .line 1877
    .line 1878
    .line 1879
    .line 1880
    .line 1881
    .line 1882
    .line 1883
    .line 1884
    .line 1885
    .line 1886
    .line 1887
    .line 1888
    .line 1889
    .line 1890
    .line 1891
    .line 1892
    .line 1893
    .line 1894
    .line 1895
    .line 1896
    .line 1897
    .line 1898
    .line 1899
    .line 1900
    .line 1901
    .line 1902
    .line 1903
    .line 1904
    .line 1905
    .line 1906
    .line 1907
    .line 1908
    .line 1909
    .line 1910
    .line 1911
    .line 1912
    .line 1913
    .line 1914
    .line 1915
    .line 1916
    .line 1917
    .line 1918
    .line 1919
    .line 1920
    .line 1921
    .line 1922
    .line 1923
    .line 1924
    .line 1925
    .line 1926
    .line 1927
    .line 1928
    .line 1929
    .line 1930
    .line 1931
    .line 1932
    .line 1933
    .line 1934
    .line 1935
    .line 1936
    .line 1937
    .line 1938
    .line 1939
    .line 1940
    .line 1941
    .line 1942
    .line 1943
    .line 1944
    .line 1945
    .line 1946
    .line 1947
    .line 1948
    .line 1949
    .line 1950
    .line 1951
    .line 1952
    .line 1953
    .line 1954
    .line 1955
    .line 1956
    .line 1957
    .line 1958
    .line 1959
    .line 1960
    .line 1961
    .line 1962
    .line 1963
    .line 1964
    .line 1965
    .line 1966
    .line 1967
    .line 1968
    .line 1969
    .line 1970
    .line 1971
    .line 1972
    .line 1973
    .line 1974
    .line 1975
    .line 1976
    .line 1977
    .line 1978
    .line 1979
    .line 1980
    .line 1981
    .line 1982
    .line 1983
    .line 1984
    .line 1985
    .line 1986
    .line 1987
    .line 1988
    .line 1989
    .line 1990
    .line 1991
    .line 1992
    .line 1993
    .line 1994
    .line 1995
    .line 1996
    .line 1997
    .line 1998
    .line 1999
    .line 2000
    .line 2001
    .line 2002
    .line 2003
    .line 2004
    .line 2005
    .line 2006
    .line 2007
    .line 2008
    .line 2009
    .line 2010
    .line 2011
    .line 2012
    .line 2013
    .line 2014
    .line 2015
    .line 2016
    .line 2017
    .line 2018
    .line 2019
    .line 2020
    .line 2021
    .line 2022
    .line 2023
    .line 2024
    .line 2025
    .line 2026
    .line 2027
    .line 2028
    .line 2029
    .line 2030
    .line 2031
    .line 2032
    .line 2033
    .line 2034
    .line 2035
    .line 2036
    .line 2037
    .line 2038
    .line 2039
    .line 2040
    .line 2041
    .line 2042
    .line 2043
    .line 2044
    .line 2045
    .line 2046
    .line 2047
    .line 2048
    .line 2049
    .line 2050
    .line 2051
    .line 2052
    .line 2053
    .line 2054
    .line 2055
    .line 2056
    .line 2057
    .line 2058
    .line 2059
    .line 2060
    .line 2061
    .line 2062
    .line 2063
    .line 2064
    .line 2065
    .line 2066
    .line 2067
    .line 2068
    .line 2069
    .line 2070
    .line 2071
    .line 2072
    .line 2073
    .line 2074
    .line 2075
    .line 2076
    .line 2077
    .line 2078
    .line 2079
    .line 2080
    .line 2081
    .line 2082
    .line 2083
    .line 2084
    .line 2085
    .line 2086
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, "AdConfigFragment"

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O8o08O8O:Ljava/lang/String;

    .line 7
    .line 8
    new-instance v0, Ljava/util/ArrayList;

    .line 9
    .line 10
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 14
    .line 15
    new-instance v0, Ljava/util/ArrayList;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0O:Ljava/util/ArrayList;

    .line 21
    .line 22
    sget-object v0, Lcom/intsig/advertisement/debug/AdDebugCfg;->〇080:Lcom/intsig/advertisement/debug/AdDebugCfg;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/advertisement/debug/AdDebugCfg;->〇080()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const-class v1, Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 29
    .line 30
    invoke-static {v0, v1}, Lcom/intsig/okgo/utils/GsonUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const-string v1, "fromJsonString(AdDebugCf\u2026nfigResponse::class.java)"

    .line 35
    .line 36
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    check-cast v0, Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 40
    .line 41
    iput-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 42
    .line 43
    const-string v0, ""

    .line 44
    .line 45
    iput-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 46
    .line 47
    iput-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇08O:Ljava/lang/String;

    .line 48
    .line 49
    iput-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O0O:Ljava/lang/String;

    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private static final O00OoO〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-interface {p0}, Landroid/content/DialogInterface;->cancel()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic O08〇()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O0O0〇()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0O〇O00O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O0〇()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->ooO:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final O88(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/content/DialogInterface;I)V
    .locals 9

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p2, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇08O:Ljava/lang/String;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O0O:Ljava/lang/String;

    .line 11
    .line 12
    new-instance v2, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string p2, "_"

    .line 21
    .line 22
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p2

    .line 38
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O8〇:Ljava/util/Map;

    .line 39
    .line 40
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    move-result-object p2

    .line 44
    check-cast p2, Ljava/lang/String;

    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 47
    .line 48
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    if-nez v0, :cond_2

    .line 53
    .line 54
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    if-eqz v0, :cond_0

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_0
    const-class v0, Lcom/intsig/advertisement/bean/SourceCfg;

    .line 62
    .line 63
    invoke-static {p2, v0}, Lcom/intsig/okgo/utils/GsonUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    move-result-object p2

    .line 67
    const-string v0, "fromJsonString(adString, SourceCfg::class.java)"

    .line 68
    .line 69
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    check-cast p2, Lcom/intsig/advertisement/bean/SourceCfg;

    .line 73
    .line 74
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 75
    .line 76
    new-instance v7, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 77
    .line 78
    iget-object v3, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 79
    .line 80
    const/4 v4, 0x0

    .line 81
    const/4 v5, 0x4

    .line 82
    const/4 v6, 0x0

    .line 83
    move-object v1, v7

    .line 84
    move-object v2, p2

    .line 85
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 86
    .line 87
    .line 88
    const/4 v1, 0x0

    .line 89
    invoke-virtual {v0, v1, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 90
    .line 91
    .line 92
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/test/docjson/AdConfigFragment$AdConfigAdapter;

    .line 93
    .line 94
    if-eqz v0, :cond_1

    .line 95
    .line 96
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 97
    .line 98
    .line 99
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0O:Ljava/util/ArrayList;

    .line 100
    .line 101
    new-instance v7, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$OpeSourceCfg;

    .line 102
    .line 103
    new-instance v8, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 104
    .line 105
    iget-object v3, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 106
    .line 107
    const/4 v4, 0x0

    .line 108
    const/4 v5, 0x4

    .line 109
    const/4 v6, 0x0

    .line 110
    move-object v1, v8

    .line 111
    move-object v2, p2

    .line 112
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 113
    .line 114
    .line 115
    sget-object p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$Operation;->ADD:Lcom/intsig/camscanner/test/docjson/AdConfigFragment$Operation;

    .line 116
    .line 117
    invoke-direct {v7, v8, p0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$OpeSourceCfg;-><init>(Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;Lcom/intsig/camscanner/test/docjson/AdConfigFragment$Operation;)V

    .line 118
    .line 119
    .line 120
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    .line 122
    .line 123
    goto :goto_1

    .line 124
    :cond_2
    :goto_0
    iget-object p0, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->OO:Lcom/intsig/camscanner/test/docjson/DocJsonTestActivity;

    .line 125
    .line 126
    const-string p2, "\u53c2\u6570\u4e0d\u5408\u6cd5\uff01"

    .line 127
    .line 128
    invoke-static {p0, p2}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    :goto_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 132
    .line 133
    .line 134
    return-void
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method public static final synthetic O880O〇()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇800OO〇0O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O8O(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic O8〇8〇O80()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->Ooo08:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic OO0O()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇o88o08〇:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final OO0o(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OO〇00〇8oO:Lcom/intsig/app/AlertDialog;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic OO〇〇o0oO()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O8o〇O0:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final OoO〇OOo8o(Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o〇()Lcom/intsig/advertisement/bean/SourceCfg;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o00〇〇Oo()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    sparse-switch v2, :sswitch_data_0

    .line 14
    .line 15
    .line 16
    goto/16 :goto_0

    .line 17
    .line 18
    :sswitch_0
    const-string p1, "function_reward"

    .line 19
    .line 20
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    if-nez p1, :cond_0

    .line 25
    .line 26
    goto/16 :goto_0

    .line 27
    .line 28
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getFunctionReward()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 35
    .line 36
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getFunctionReward()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    const-string v2, "mConfig.functionReward.source_info"

    .line 45
    .line 46
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    invoke-static {v1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 54
    .line 55
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 56
    .line 57
    .line 58
    goto/16 :goto_0

    .line 59
    .line 60
    :sswitch_1
    const-string p1, "reward_video"

    .line 61
    .line 62
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    if-nez p1, :cond_1

    .line 67
    .line 68
    goto/16 :goto_0

    .line 69
    .line 70
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 71
    .line 72
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getReward_video()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 77
    .line 78
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getReward_video()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 79
    .line 80
    .line 81
    move-result-object v1

    .line 82
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 83
    .line 84
    .line 85
    move-result-object v1

    .line 86
    const-string v2, "mConfig.reward_video.source_info"

    .line 87
    .line 88
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    invoke-static {v1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 96
    .line 97
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 98
    .line 99
    .line 100
    goto/16 :goto_0

    .line 101
    .line 102
    :sswitch_2
    const-string p1, "main_middle_banner"

    .line 103
    .line 104
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 105
    .line 106
    .line 107
    move-result p1

    .line 108
    if-nez p1, :cond_2

    .line 109
    .line 110
    goto/16 :goto_0

    .line 111
    .line 112
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 113
    .line 114
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getMainMiddleBanner()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 115
    .line 116
    .line 117
    move-result-object p1

    .line 118
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 119
    .line 120
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getMainMiddleBanner()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 121
    .line 122
    .line 123
    move-result-object v1

    .line 124
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 125
    .line 126
    .line 127
    move-result-object v1

    .line 128
    const-string v2, "mConfig.mainMiddleBanner.source_info"

    .line 129
    .line 130
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    invoke-static {v1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 134
    .line 135
    .line 136
    move-result-object v0

    .line 137
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 138
    .line 139
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 140
    .line 141
    .line 142
    goto/16 :goto_0

    .line 143
    .line 144
    :sswitch_3
    const-string p1, "app_launch"

    .line 145
    .line 146
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 147
    .line 148
    .line 149
    move-result p1

    .line 150
    if-nez p1, :cond_3

    .line 151
    .line 152
    goto/16 :goto_0

    .line 153
    .line 154
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 155
    .line 156
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getApp_launch()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 157
    .line 158
    .line 159
    move-result-object p1

    .line 160
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 161
    .line 162
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getApp_launch()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 163
    .line 164
    .line 165
    move-result-object v1

    .line 166
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 167
    .line 168
    .line 169
    move-result-object v1

    .line 170
    const-string v2, "mConfig.app_launch.source_info"

    .line 171
    .line 172
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    .line 174
    .line 175
    invoke-static {v1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 176
    .line 177
    .line 178
    move-result-object v0

    .line 179
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 180
    .line 181
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 182
    .line 183
    .line 184
    goto/16 :goto_0

    .line 185
    .line 186
    :sswitch_4
    const-string v2, "doc_list"

    .line 187
    .line 188
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 189
    .line 190
    .line 191
    move-result v1

    .line 192
    if-nez v1, :cond_4

    .line 193
    .line 194
    goto/16 :goto_0

    .line 195
    .line 196
    :cond_4
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 197
    .line 198
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getDoc_list()Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 199
    .line 200
    .line 201
    move-result-object v1

    .line 202
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getBanners()[Lcom/intsig/advertisement/bean/ItemConfig;

    .line 203
    .line 204
    .line 205
    move-result-object v1

    .line 206
    invoke-virtual {p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇080()I

    .line 207
    .line 208
    .line 209
    move-result v2

    .line 210
    aget-object v1, v1, v2

    .line 211
    .line 212
    iget-object v2, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 213
    .line 214
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ConfigResponse;->getDoc_list()Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 215
    .line 216
    .line 217
    move-result-object v2

    .line 218
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getBanners()[Lcom/intsig/advertisement/bean/ItemConfig;

    .line 219
    .line 220
    .line 221
    move-result-object v2

    .line 222
    invoke-virtual {p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇080()I

    .line 223
    .line 224
    .line 225
    move-result p1

    .line 226
    aget-object p1, v2, p1

    .line 227
    .line 228
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 229
    .line 230
    .line 231
    move-result-object p1

    .line 232
    const-string v2, "mConfig.doc_list.banners[cfg.index].source_info"

    .line 233
    .line 234
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 235
    .line 236
    .line 237
    invoke-static {p1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 238
    .line 239
    .line 240
    move-result-object p1

    .line 241
    check-cast p1, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 242
    .line 243
    invoke-virtual {v1, p1}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 244
    .line 245
    .line 246
    goto/16 :goto_0

    .line 247
    .line 248
    :sswitch_5
    const-string p1, "scan_done_reward"

    .line 249
    .line 250
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 251
    .line 252
    .line 253
    move-result p1

    .line 254
    if-nez p1, :cond_5

    .line 255
    .line 256
    goto/16 :goto_0

    .line 257
    .line 258
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 259
    .line 260
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getScandoneReward()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 261
    .line 262
    .line 263
    move-result-object p1

    .line 264
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 265
    .line 266
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getScandoneReward()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 267
    .line 268
    .line 269
    move-result-object v1

    .line 270
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 271
    .line 272
    .line 273
    move-result-object v1

    .line 274
    const-string v2, "mConfig.scandoneReward.source_info"

    .line 275
    .line 276
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 277
    .line 278
    .line 279
    invoke-static {v1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 280
    .line 281
    .line 282
    move-result-object v0

    .line 283
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 284
    .line 285
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 286
    .line 287
    .line 288
    goto/16 :goto_0

    .line 289
    .line 290
    :sswitch_6
    const-string p1, "doc_list_popup"

    .line 291
    .line 292
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 293
    .line 294
    .line 295
    move-result p1

    .line 296
    if-nez p1, :cond_6

    .line 297
    .line 298
    goto/16 :goto_0

    .line 299
    .line 300
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 301
    .line 302
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getDocListPopUp()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 303
    .line 304
    .line 305
    move-result-object p1

    .line 306
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 307
    .line 308
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getDocListPopUp()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 309
    .line 310
    .line 311
    move-result-object v1

    .line 312
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 313
    .line 314
    .line 315
    move-result-object v1

    .line 316
    const-string v2, "mConfig.docListPopUp.source_info"

    .line 317
    .line 318
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 319
    .line 320
    .line 321
    invoke-static {v1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 322
    .line 323
    .line 324
    move-result-object v0

    .line 325
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 326
    .line 327
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 328
    .line 329
    .line 330
    goto/16 :goto_0

    .line 331
    .line 332
    :sswitch_7
    const-string p1, "ocr_reward"

    .line 333
    .line 334
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 335
    .line 336
    .line 337
    move-result p1

    .line 338
    if-nez p1, :cond_7

    .line 339
    .line 340
    goto/16 :goto_0

    .line 341
    .line 342
    :cond_7
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 343
    .line 344
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getOcrReward()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 345
    .line 346
    .line 347
    move-result-object p1

    .line 348
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 349
    .line 350
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getOcrReward()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 351
    .line 352
    .line 353
    move-result-object v1

    .line 354
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 355
    .line 356
    .line 357
    move-result-object v1

    .line 358
    const-string v2, "mConfig.ocrReward.source_info"

    .line 359
    .line 360
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 361
    .line 362
    .line 363
    invoke-static {v1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 364
    .line 365
    .line 366
    move-result-object v0

    .line 367
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 368
    .line 369
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 370
    .line 371
    .line 372
    goto/16 :goto_0

    .line 373
    .line 374
    :sswitch_8
    const-string p1, "scan_done_top"

    .line 375
    .line 376
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 377
    .line 378
    .line 379
    move-result p1

    .line 380
    if-nez p1, :cond_8

    .line 381
    .line 382
    goto/16 :goto_0

    .line 383
    .line 384
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 385
    .line 386
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getScandoneTop()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 387
    .line 388
    .line 389
    move-result-object p1

    .line 390
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 391
    .line 392
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getScandoneTop()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 393
    .line 394
    .line 395
    move-result-object v1

    .line 396
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 397
    .line 398
    .line 399
    move-result-object v1

    .line 400
    const-string v2, "mConfig.scandoneTop.source_info"

    .line 401
    .line 402
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 403
    .line 404
    .line 405
    invoke-static {v1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 406
    .line 407
    .line 408
    move-result-object v0

    .line 409
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 410
    .line 411
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 412
    .line 413
    .line 414
    goto/16 :goto_0

    .line 415
    .line 416
    :sswitch_9
    const-string p1, "purchase_exit"

    .line 417
    .line 418
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 419
    .line 420
    .line 421
    move-result p1

    .line 422
    if-nez p1, :cond_9

    .line 423
    .line 424
    goto/16 :goto_0

    .line 425
    .line 426
    :cond_9
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 427
    .line 428
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getPurchase_exit()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 429
    .line 430
    .line 431
    move-result-object p1

    .line 432
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 433
    .line 434
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getPurchase_exit()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 435
    .line 436
    .line 437
    move-result-object v1

    .line 438
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 439
    .line 440
    .line 441
    move-result-object v1

    .line 442
    const-string v2, "mConfig.purchase_exit.source_info"

    .line 443
    .line 444
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 445
    .line 446
    .line 447
    invoke-static {v1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 448
    .line 449
    .line 450
    move-result-object v0

    .line 451
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 452
    .line 453
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 454
    .line 455
    .line 456
    goto/16 :goto_0

    .line 457
    .line 458
    :sswitch_a
    const-string p1, "pdf_watermark_video"

    .line 459
    .line 460
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 461
    .line 462
    .line 463
    move-result p1

    .line 464
    if-nez p1, :cond_a

    .line 465
    .line 466
    goto/16 :goto_0

    .line 467
    .line 468
    :cond_a
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 469
    .line 470
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getPdfWatermarkVideo()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 471
    .line 472
    .line 473
    move-result-object p1

    .line 474
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 475
    .line 476
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getPdfWatermarkVideo()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 477
    .line 478
    .line 479
    move-result-object v1

    .line 480
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 481
    .line 482
    .line 483
    move-result-object v1

    .line 484
    const-string v2, "mConfig.pdfWatermarkVideo.source_info"

    .line 485
    .line 486
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 487
    .line 488
    .line 489
    invoke-static {v1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 490
    .line 491
    .line 492
    move-result-object v0

    .line 493
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 494
    .line 495
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 496
    .line 497
    .line 498
    goto/16 :goto_0

    .line 499
    .line 500
    :sswitch_b
    const-string p1, "share_done"

    .line 501
    .line 502
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 503
    .line 504
    .line 505
    move-result p1

    .line 506
    if-nez p1, :cond_b

    .line 507
    .line 508
    goto/16 :goto_0

    .line 509
    .line 510
    :cond_b
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 511
    .line 512
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getShare_done()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 513
    .line 514
    .line 515
    move-result-object p1

    .line 516
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 517
    .line 518
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getShare_done()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 519
    .line 520
    .line 521
    move-result-object v1

    .line 522
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 523
    .line 524
    .line 525
    move-result-object v1

    .line 526
    const-string v2, "mConfig.share_done.source_info"

    .line 527
    .line 528
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 529
    .line 530
    .line 531
    invoke-static {v1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 532
    .line 533
    .line 534
    move-result-object v0

    .line 535
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 536
    .line 537
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 538
    .line 539
    .line 540
    goto/16 :goto_0

    .line 541
    .line 542
    :sswitch_c
    const-string v2, "scan_done"

    .line 543
    .line 544
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 545
    .line 546
    .line 547
    move-result v1

    .line 548
    if-nez v1, :cond_c

    .line 549
    .line 550
    goto/16 :goto_0

    .line 551
    .line 552
    :cond_c
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 553
    .line 554
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getScan_done()Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 555
    .line 556
    .line 557
    move-result-object v1

    .line 558
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getBanners()[Lcom/intsig/advertisement/bean/ItemConfig;

    .line 559
    .line 560
    .line 561
    move-result-object v1

    .line 562
    invoke-virtual {p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇080()I

    .line 563
    .line 564
    .line 565
    move-result v2

    .line 566
    aget-object v1, v1, v2

    .line 567
    .line 568
    iget-object v2, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 569
    .line 570
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ConfigResponse;->getScan_done()Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 571
    .line 572
    .line 573
    move-result-object v2

    .line 574
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getBanners()[Lcom/intsig/advertisement/bean/ItemConfig;

    .line 575
    .line 576
    .line 577
    move-result-object v2

    .line 578
    invoke-virtual {p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇080()I

    .line 579
    .line 580
    .line 581
    move-result p1

    .line 582
    aget-object p1, v2, p1

    .line 583
    .line 584
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 585
    .line 586
    .line 587
    move-result-object p1

    .line 588
    const-string v2, "mConfig.scan_done.banners[cfg.index].source_info"

    .line 589
    .line 590
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 591
    .line 592
    .line 593
    invoke-static {p1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 594
    .line 595
    .line 596
    move-result-object p1

    .line 597
    check-cast p1, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 598
    .line 599
    invoke-virtual {v1, p1}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 600
    .line 601
    .line 602
    goto/16 :goto_0

    .line 603
    .line 604
    :sswitch_d
    const-string p1, "page_list_banner"

    .line 605
    .line 606
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 607
    .line 608
    .line 609
    move-result p1

    .line 610
    if-nez p1, :cond_d

    .line 611
    .line 612
    goto :goto_0

    .line 613
    :cond_d
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 614
    .line 615
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getPage_list_banner()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 616
    .line 617
    .line 618
    move-result-object p1

    .line 619
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 620
    .line 621
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getPage_list_banner()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 622
    .line 623
    .line 624
    move-result-object v1

    .line 625
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 626
    .line 627
    .line 628
    move-result-object v1

    .line 629
    const-string v2, "mConfig.page_list_banner.source_info"

    .line 630
    .line 631
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 632
    .line 633
    .line 634
    invoke-static {v1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 635
    .line 636
    .line 637
    move-result-object v0

    .line 638
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 639
    .line 640
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 641
    .line 642
    .line 643
    goto :goto_0

    .line 644
    :sswitch_e
    const-string p1, "shot_done"

    .line 645
    .line 646
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 647
    .line 648
    .line 649
    move-result p1

    .line 650
    if-nez p1, :cond_e

    .line 651
    .line 652
    goto :goto_0

    .line 653
    :cond_e
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 654
    .line 655
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getShotDone()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 656
    .line 657
    .line 658
    move-result-object p1

    .line 659
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 660
    .line 661
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getShotDone()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 662
    .line 663
    .line 664
    move-result-object v1

    .line 665
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 666
    .line 667
    .line 668
    move-result-object v1

    .line 669
    const-string v2, "mConfig.shotDone.source_info"

    .line 670
    .line 671
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 672
    .line 673
    .line 674
    invoke-static {v1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 675
    .line 676
    .line 677
    move-result-object v0

    .line 678
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 679
    .line 680
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 681
    .line 682
    .line 683
    goto :goto_0

    .line 684
    :sswitch_f
    const-string p1, "space_lottery"

    .line 685
    .line 686
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 687
    .line 688
    .line 689
    move-result p1

    .line 690
    if-nez p1, :cond_f

    .line 691
    .line 692
    goto :goto_0

    .line 693
    :cond_f
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 694
    .line 695
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getSpace_lottery()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 696
    .line 697
    .line 698
    move-result-object p1

    .line 699
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 700
    .line 701
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getSpace_lottery()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 702
    .line 703
    .line 704
    move-result-object v1

    .line 705
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 706
    .line 707
    .line 708
    move-result-object v1

    .line 709
    const-string v2, "mConfig.space_lottery.source_info"

    .line 710
    .line 711
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 712
    .line 713
    .line 714
    invoke-static {v1, v0}, Lkotlin/collections/ArraysKt;->OO0o〇〇([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    .line 715
    .line 716
    .line 717
    move-result-object v0

    .line 718
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 719
    .line 720
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 721
    .line 722
    .line 723
    :goto_0
    return-void

    .line 724
    nop

    .line 725
    :sswitch_data_0
    .sparse-switch
        -0x79a9f6f0 -> :sswitch_f
        -0x76983259 -> :sswitch_e
        -0x719b1fc3 -> :sswitch_d
        -0x6cd263fc -> :sswitch_c
        -0x6a99601e -> :sswitch_b
        -0x66d4186d -> :sswitch_a
        -0x4eee9a64 -> :sswitch_9
        -0x349b1566 -> :sswitch_8
        -0x11abbe90 -> :sswitch_7
        0xa05f332 -> :sswitch_6
        0x30cee3ca -> :sswitch_5
        0x32266745 -> :sswitch_4
        0x3f912f91 -> :sswitch_3
        0x577b8110 -> :sswitch_2
        0x7c69676b -> :sswitch_1
        0x7db65576 -> :sswitch_0
    .end sparse-switch
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method public static synthetic Ooo8o(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o〇OoO0(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic OooO〇()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o〇oO:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O〇080〇o0()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇08oOOO0:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O〇0O〇Oo〇o()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇00O0:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O〇8〇008()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOoo80oO:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O〇〇O80o8()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o〇o〇Oo88:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O〇〇o8O()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->o0:Landroid/view/View;

    .line 2
    .line 3
    const v1, 0x7f0a1162

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Landroid/widget/Switch;

    .line 11
    .line 12
    invoke-static {}, Lcom/intsig/advertisement/util/SharePreferenceUtil;->〇o00〇〇Oo()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    xor-int/lit8 v1, v1, 0x1

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->o0:Landroid/view/View;

    .line 26
    .line 27
    const v1, 0x7f0a0847

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    check-cast v0, Landroid/widget/ImageView;

    .line 35
    .line 36
    new-instance v1, Lcom/intsig/camscanner/test/docjson/〇o〇;

    .line 37
    .line 38
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/test/docjson/〇o〇;-><init>(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->o0:Landroid/view/View;

    .line 45
    .line 46
    const v1, 0x7f0a1253

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    check-cast v0, Landroid/widget/TextView;

    .line 54
    .line 55
    new-instance v1, Lcom/intsig/camscanner/test/docjson/O8;

    .line 56
    .line 57
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/test/docjson/O8;-><init>(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->o0:Landroid/view/View;

    .line 64
    .line 65
    const v1, 0x7f0a18e9

    .line 66
    .line 67
    .line 68
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    check-cast v0, Landroid/widget/TextView;

    .line 73
    .line 74
    new-instance v1, Lcom/intsig/camscanner/test/docjson/Oo08;

    .line 75
    .line 76
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/test/docjson/Oo08;-><init>(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->o0:Landroid/view/View;

    .line 83
    .line 84
    const v1, 0x7f0a137c

    .line 85
    .line 86
    .line 87
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    check-cast v0, Landroid/widget/TextView;

    .line 92
    .line 93
    new-instance v1, Lcom/intsig/camscanner/test/docjson/o〇0;

    .line 94
    .line 95
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/test/docjson/o〇0;-><init>(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    .line 100
    .line 101
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->o0:Landroid/view/View;

    .line 102
    .line 103
    const v1, 0x7f0a1022

    .line 104
    .line 105
    .line 106
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 111
    .line 112
    iput-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o8〇OO0〇0o:Landroidx/recyclerview/widget/RecyclerView;

    .line 113
    .line 114
    new-instance v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$AdConfigAdapter;

    .line 115
    .line 116
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 117
    .line 118
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$AdConfigAdapter;-><init>(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Ljava/util/ArrayList;)V

    .line 119
    .line 120
    .line 121
    iput-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/test/docjson/AdConfigFragment$AdConfigAdapter;

    .line 122
    .line 123
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o8〇OO0〇0o:Landroidx/recyclerview/widget/RecyclerView;

    .line 124
    .line 125
    const/4 v1, 0x0

    .line 126
    if-eqz v0, :cond_0

    .line 127
    .line 128
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    goto :goto_0

    .line 133
    :cond_0
    move-object v0, v1

    .line 134
    :goto_0
    instance-of v2, v0, Landroidx/recyclerview/widget/SimpleItemAnimator;

    .line 135
    .line 136
    if-eqz v2, :cond_1

    .line 137
    .line 138
    move-object v1, v0

    .line 139
    check-cast v1, Landroidx/recyclerview/widget/SimpleItemAnimator;

    .line 140
    .line 141
    :cond_1
    if-eqz v1, :cond_2

    .line 142
    .line 143
    const/4 v0, 0x0

    .line 144
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/SimpleItemAnimator;->setSupportsChangeAnimations(Z)V

    .line 145
    .line 146
    .line 147
    const-wide/16 v2, 0x0

    .line 148
    .line 149
    invoke-virtual {v1, v2, v3}, Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;->setChangeDuration(J)V

    .line 150
    .line 151
    .line 152
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o8〇OO0〇0o:Landroidx/recyclerview/widget/RecyclerView;

    .line 153
    .line 154
    if-nez v0, :cond_3

    .line 155
    .line 156
    goto :goto_1

    .line 157
    :cond_3
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/test/docjson/AdConfigFragment$AdConfigAdapter;

    .line 158
    .line 159
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 160
    .line 161
    .line 162
    :goto_1
    return-void
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private final o0O0O〇〇〇0()V
    .locals 10

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getLayoutInflater()Landroid/view/LayoutInflater;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    const v3, 0x7f0d0170

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v3, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const v1, 0x7f0a10f4

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    check-cast v1, Landroid/widget/Spinner;

    .line 22
    .line 23
    const v2, 0x7f0a10f5

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    check-cast v2, Landroid/widget/Spinner;

    .line 31
    .line 32
    const v3, 0x7f0a10f6

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 36
    .line 37
    .line 38
    move-result-object v3

    .line 39
    move-object v9, v3

    .line 40
    check-cast v9, Landroid/widget/Spinner;

    .line 41
    .line 42
    new-instance v3, Lcom/intsig/app/AlertDialog$Builder;

    .line 43
    .line 44
    iget-object v4, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->OO:Lcom/intsig/camscanner/test/docjson/DocJsonTestActivity;

    .line 45
    .line 46
    invoke-direct {v3, v4}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 47
    .line 48
    .line 49
    const-string v4, "\u6dfb\u52a0\u5e7f\u544a"

    .line 50
    .line 51
    invoke-virtual {v3, v4}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 52
    .line 53
    .line 54
    move-result-object v3

    .line 55
    invoke-virtual {v3, v0}, Lcom/intsig/app/AlertDialog$Builder;->oO(Landroid/view/View;)Lcom/intsig/app/AlertDialog$Builder;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    new-instance v3, Lcom/intsig/camscanner/test/docjson/〇o00〇〇Oo;

    .line 60
    .line 61
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/test/docjson/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;)V

    .line 62
    .line 63
    .line 64
    const v4, 0x7f131e36

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v4, v3}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    iput-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo0:Lcom/intsig/app/AlertDialog;

    .line 76
    .line 77
    new-instance v0, Landroid/widget/ArrayAdapter;

    .line 78
    .line 79
    iget-object v3, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->OO:Lcom/intsig/camscanner/test/docjson/DocJsonTestActivity;

    .line 80
    .line 81
    const v4, 0x7f0d0741

    .line 82
    .line 83
    .line 84
    invoke-direct {v0, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 85
    .line 86
    .line 87
    new-instance v5, Landroid/widget/ArrayAdapter;

    .line 88
    .line 89
    iget-object v3, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->OO:Lcom/intsig/camscanner/test/docjson/DocJsonTestActivity;

    .line 90
    .line 91
    invoke-direct {v5, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 92
    .line 93
    .line 94
    new-instance v6, Landroid/widget/ArrayAdapter;

    .line 95
    .line 96
    iget-object v3, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->OO:Lcom/intsig/camscanner/test/docjson/DocJsonTestActivity;

    .line 97
    .line 98
    invoke-direct {v6, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 99
    .line 100
    .line 101
    sget-object v3, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇O〇〇O8:Ljava/util/ArrayList;

    .line 102
    .line 103
    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 107
    .line 108
    .line 109
    new-instance v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$initAddItemDialog$2;

    .line 110
    .line 111
    move-object v3, v0

    .line 112
    move-object v4, p0

    .line 113
    move-object v7, v2

    .line 114
    move-object v8, v9

    .line 115
    invoke-direct/range {v3 .. v8}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$initAddItemDialog$2;-><init>(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;Landroid/widget/Spinner;Landroid/widget/Spinner;)V

    .line 116
    .line 117
    .line 118
    invoke-virtual {v1, v0}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 119
    .line 120
    .line 121
    new-instance v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$initAddItemDialog$3;

    .line 122
    .line 123
    invoke-direct {v0, p0, v2}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$initAddItemDialog$3;-><init>(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/widget/Spinner;)V

    .line 124
    .line 125
    .line 126
    invoke-virtual {v2, v0}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 127
    .line 128
    .line 129
    new-instance v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$initAddItemDialog$4;

    .line 130
    .line 131
    invoke-direct {v0, p0, v9}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$initAddItemDialog$4;-><init>(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/widget/Spinner;)V

    .line 132
    .line 133
    .line 134
    invoke-virtual {v9, v0}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 135
    .line 136
    .line 137
    return-void
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private static final o0OO(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->OO:Lcom/intsig/camscanner/test/docjson/DocJsonTestActivity;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/test/docjson/DocJsonTestActivity;->onBackPressed()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic o0Oo()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oO00〇o:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o0〇〇00()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇O〇〇O8:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o808o8o08()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->Oo80:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o88()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOO0880O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final oOO8oo0(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->OO:Lcom/intsig/camscanner/test/docjson/DocJsonTestActivity;

    .line 7
    .line 8
    const-string p1, "\u5728\u505a\u4e86\u5728\u505a\u4e86"

    .line 9
    .line 10
    invoke-static {p0, p1}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic oOoO8OO〇(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o0OO(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic o〇08oO80o()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇o〇:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic o〇O8OO(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O88(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private static final o〇OoO0(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo0:Lcom/intsig/app/AlertDialog;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic o〇o08〇()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oo8ooo8O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o〇oo(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic 〇08O()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OO〇OOo:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇0o0oO〇〇0()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0O:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_2

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$OpeSourceCfg;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$OpeSourceCfg;->〇080()Lcom/intsig/camscanner/test/docjson/AdConfigFragment$Operation;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    sget-object v3, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$Operation;->DELETE:Lcom/intsig/camscanner/test/docjson/AdConfigFragment$Operation;

    .line 24
    .line 25
    if-ne v2, v3, :cond_1

    .line 26
    .line 27
    invoke-virtual {v1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$OpeSourceCfg;->〇o00〇〇Oo()Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇OoO0o0(Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;)V

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    invoke-virtual {v1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$OpeSourceCfg;->〇080()Lcom/intsig/camscanner/test/docjson/AdConfigFragment$Operation;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    sget-object v3, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$Operation;->ADD:Lcom/intsig/camscanner/test/docjson/AdConfigFragment$Operation;

    .line 40
    .line 41
    if-ne v2, v3, :cond_0

    .line 42
    .line 43
    invoke-virtual {v1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$OpeSourceCfg;->〇o00〇〇Oo()Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OoO〇OOo8o(Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_2
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private final 〇0o88Oo〇()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->OO:Lcom/intsig/camscanner/test/docjson/DocJsonTestActivity;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6
    .line 7
    .line 8
    const-string v1, "\u5e7f\u544a\u914d\u7f6e\u987b\u77e5"

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const-string v1, "\u8be5\u9875\u9762\u5c55\u793a\u4fe1\u606f\u4e3a\u672c\u5730\u914d\u7f6e\u5e7f\u544a\uff0c\u53f3\u4e0a\u89d2\u4e3a\u914d\u7f6e\u5f00\u5173\n1. \u6bcf\u6761\u5e7f\u544a\u4fe1\u606f\u53f3\u4fa7\u7ea2\u8272\u6309\u94ae\u4e3a\u300c\u5220\u9664\u6309\u94ae\u300d\uff0c\u70b9\u51fb\u4f1a\u5220\u9664\u5f53\u524d\u5e7f\u544a\u914d\u7f6e\uff1b\n2. \u300c\u6dfb\u52a0\u5e7f\u544a\u300d\u6309\u94ae\u70b9\u51fb\u540e\u4f1a\u5f39\u51fa\u9009\u62e9\u6846\uff0c\u9009\u62e9\u5e7f\u544a\u540e\u70b9\u51fb\u300c\u786e\u5b9a\u300d\u6309\u94ae\uff0c\u4f1a\u5c06\u9009\u62e9\u7684\u5e7f\u544a\u6dfb\u52a0\u8fdb\u672c\u5730\u914d\u7f6e\u3002\n\u6ce8\u610f\uff1a\n1. \u5f53\u914d\u7f6e\u5f00\u5173\u4e3a\u5173\u65f6\uff0c\u9000\u51fa\u9875\u9762\u4f1a\u91cd\u7f6e\u5e7f\u544a\u914d\u7f6e\uff1b\n2. \u5f53\u914d\u7f6e\u5f00\u5173\u4e3a\u5f00\u65f6\uff0c\u9000\u51fa\u9875\u9762\u4f1a\u81ea\u52a8\u4fdd\u5b58\u5f53\u524d\u914d\u7f6e\u3002"

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/camscanner/test/docjson/〇080;

    .line 21
    .line 22
    invoke-direct {v1}, Lcom/intsig/camscanner/test/docjson/〇080;-><init>()V

    .line 23
    .line 24
    .line 25
    const v2, 0x7f131e36

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    iput-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OO〇00〇8oO:Lcom/intsig/app/AlertDialog;

    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static final synthetic 〇0oO〇oo00()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇oo〇O〇80:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇0ooOOo(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->OO0o(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic 〇0〇0(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O00OoO〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic 〇8O0880()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->Oo0O0o8:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇O0o〇〇o()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o8〇OO:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇O8〇8000()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOO〇〇:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇O8〇8O0oO()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oO〇8O8oOo:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇OoO0o0(Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o〇()Lcom/intsig/advertisement/bean/SourceCfg;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇o00〇〇Oo()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    const-string v3, "it"

    .line 14
    .line 15
    const/4 v4, 0x0

    .line 16
    sparse-switch v2, :sswitch_data_0

    .line 17
    .line 18
    .line 19
    goto/16 :goto_10

    .line 20
    .line 21
    :sswitch_0
    const-string p1, "function_reward"

    .line 22
    .line 23
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    if-nez p1, :cond_0

    .line 28
    .line 29
    goto/16 :goto_10

    .line 30
    .line 31
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getFunctionReward()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 38
    .line 39
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getFunctionReward()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    const-string v2, "mConfig.functionReward.source_info"

    .line 48
    .line 49
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    new-instance v2, Ljava/util/ArrayList;

    .line 53
    .line 54
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .line 56
    .line 57
    array-length v5, v1

    .line 58
    const/4 v6, 0x0

    .line 59
    :goto_0
    if-ge v6, v5, :cond_2

    .line 60
    .line 61
    aget-object v7, v1, v6

    .line 62
    .line 63
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 67
    .line 68
    .line 69
    move-result v8

    .line 70
    xor-int/lit8 v8, v8, 0x1

    .line 71
    .line 72
    if-eqz v8, :cond_1

    .line 73
    .line 74
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 75
    .line 76
    .line 77
    :cond_1
    add-int/lit8 v6, v6, 0x1

    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_2
    new-array v0, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 81
    .line 82
    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 87
    .line 88
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 89
    .line 90
    .line 91
    goto/16 :goto_10

    .line 92
    .line 93
    :sswitch_1
    const-string p1, "reward_video"

    .line 94
    .line 95
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 96
    .line 97
    .line 98
    move-result p1

    .line 99
    if-nez p1, :cond_3

    .line 100
    .line 101
    goto/16 :goto_10

    .line 102
    .line 103
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 104
    .line 105
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getReward_video()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 106
    .line 107
    .line 108
    move-result-object p1

    .line 109
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 110
    .line 111
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getReward_video()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 112
    .line 113
    .line 114
    move-result-object v1

    .line 115
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 116
    .line 117
    .line 118
    move-result-object v1

    .line 119
    const-string v2, "mConfig.reward_video.source_info"

    .line 120
    .line 121
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    new-instance v2, Ljava/util/ArrayList;

    .line 125
    .line 126
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 127
    .line 128
    .line 129
    array-length v5, v1

    .line 130
    const/4 v6, 0x0

    .line 131
    :goto_1
    if-ge v6, v5, :cond_5

    .line 132
    .line 133
    aget-object v7, v1, v6

    .line 134
    .line 135
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 139
    .line 140
    .line 141
    move-result v8

    .line 142
    xor-int/lit8 v8, v8, 0x1

    .line 143
    .line 144
    if-eqz v8, :cond_4

    .line 145
    .line 146
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 147
    .line 148
    .line 149
    :cond_4
    add-int/lit8 v6, v6, 0x1

    .line 150
    .line 151
    goto :goto_1

    .line 152
    :cond_5
    new-array v0, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 153
    .line 154
    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 155
    .line 156
    .line 157
    move-result-object v0

    .line 158
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 159
    .line 160
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 161
    .line 162
    .line 163
    goto/16 :goto_10

    .line 164
    .line 165
    :sswitch_2
    const-string p1, "main_middle_banner"

    .line 166
    .line 167
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 168
    .line 169
    .line 170
    move-result p1

    .line 171
    if-nez p1, :cond_6

    .line 172
    .line 173
    goto/16 :goto_10

    .line 174
    .line 175
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 176
    .line 177
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getMainMiddleBanner()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 178
    .line 179
    .line 180
    move-result-object p1

    .line 181
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 182
    .line 183
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getMainMiddleBanner()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 184
    .line 185
    .line 186
    move-result-object v1

    .line 187
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 188
    .line 189
    .line 190
    move-result-object v1

    .line 191
    const-string v2, "mConfig.mainMiddleBanner.source_info"

    .line 192
    .line 193
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    .line 195
    .line 196
    new-instance v2, Ljava/util/ArrayList;

    .line 197
    .line 198
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .line 200
    .line 201
    array-length v5, v1

    .line 202
    const/4 v6, 0x0

    .line 203
    :goto_2
    if-ge v6, v5, :cond_8

    .line 204
    .line 205
    aget-object v7, v1, v6

    .line 206
    .line 207
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    .line 209
    .line 210
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 211
    .line 212
    .line 213
    move-result v8

    .line 214
    xor-int/lit8 v8, v8, 0x1

    .line 215
    .line 216
    if-eqz v8, :cond_7

    .line 217
    .line 218
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 219
    .line 220
    .line 221
    :cond_7
    add-int/lit8 v6, v6, 0x1

    .line 222
    .line 223
    goto :goto_2

    .line 224
    :cond_8
    new-array v0, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 225
    .line 226
    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 227
    .line 228
    .line 229
    move-result-object v0

    .line 230
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 231
    .line 232
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 233
    .line 234
    .line 235
    goto/16 :goto_10

    .line 236
    .line 237
    :sswitch_3
    const-string p1, "app_launch"

    .line 238
    .line 239
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 240
    .line 241
    .line 242
    move-result p1

    .line 243
    if-nez p1, :cond_9

    .line 244
    .line 245
    goto/16 :goto_10

    .line 246
    .line 247
    :cond_9
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 248
    .line 249
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getApp_launch()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 250
    .line 251
    .line 252
    move-result-object p1

    .line 253
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 254
    .line 255
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getApp_launch()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 256
    .line 257
    .line 258
    move-result-object v1

    .line 259
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 260
    .line 261
    .line 262
    move-result-object v1

    .line 263
    const-string v2, "mConfig.app_launch.source_info"

    .line 264
    .line 265
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    .line 267
    .line 268
    new-instance v2, Ljava/util/ArrayList;

    .line 269
    .line 270
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 271
    .line 272
    .line 273
    array-length v5, v1

    .line 274
    const/4 v6, 0x0

    .line 275
    :goto_3
    if-ge v6, v5, :cond_b

    .line 276
    .line 277
    aget-object v7, v1, v6

    .line 278
    .line 279
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 280
    .line 281
    .line 282
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 283
    .line 284
    .line 285
    move-result v8

    .line 286
    xor-int/lit8 v8, v8, 0x1

    .line 287
    .line 288
    if-eqz v8, :cond_a

    .line 289
    .line 290
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 291
    .line 292
    .line 293
    :cond_a
    add-int/lit8 v6, v6, 0x1

    .line 294
    .line 295
    goto :goto_3

    .line 296
    :cond_b
    new-array v0, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 297
    .line 298
    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 299
    .line 300
    .line 301
    move-result-object v0

    .line 302
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 303
    .line 304
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 305
    .line 306
    .line 307
    goto/16 :goto_10

    .line 308
    .line 309
    :sswitch_4
    const-string v2, "doc_list"

    .line 310
    .line 311
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 312
    .line 313
    .line 314
    move-result v1

    .line 315
    if-nez v1, :cond_c

    .line 316
    .line 317
    goto/16 :goto_10

    .line 318
    .line 319
    :cond_c
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 320
    .line 321
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getDoc_list()Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 322
    .line 323
    .line 324
    move-result-object v1

    .line 325
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getBanners()[Lcom/intsig/advertisement/bean/ItemConfig;

    .line 326
    .line 327
    .line 328
    move-result-object v1

    .line 329
    invoke-virtual {p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇080()I

    .line 330
    .line 331
    .line 332
    move-result v2

    .line 333
    aget-object v1, v1, v2

    .line 334
    .line 335
    iget-object v2, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 336
    .line 337
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ConfigResponse;->getDoc_list()Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 338
    .line 339
    .line 340
    move-result-object v2

    .line 341
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getBanners()[Lcom/intsig/advertisement/bean/ItemConfig;

    .line 342
    .line 343
    .line 344
    move-result-object v2

    .line 345
    invoke-virtual {p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇080()I

    .line 346
    .line 347
    .line 348
    move-result p1

    .line 349
    aget-object p1, v2, p1

    .line 350
    .line 351
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 352
    .line 353
    .line 354
    move-result-object p1

    .line 355
    const-string v2, "mConfig.doc_list.banners[cfg.index].source_info"

    .line 356
    .line 357
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 358
    .line 359
    .line 360
    new-instance v2, Ljava/util/ArrayList;

    .line 361
    .line 362
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 363
    .line 364
    .line 365
    array-length v5, p1

    .line 366
    const/4 v6, 0x0

    .line 367
    :goto_4
    if-ge v6, v5, :cond_e

    .line 368
    .line 369
    aget-object v7, p1, v6

    .line 370
    .line 371
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 372
    .line 373
    .line 374
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 375
    .line 376
    .line 377
    move-result v8

    .line 378
    xor-int/lit8 v8, v8, 0x1

    .line 379
    .line 380
    if-eqz v8, :cond_d

    .line 381
    .line 382
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 383
    .line 384
    .line 385
    :cond_d
    add-int/lit8 v6, v6, 0x1

    .line 386
    .line 387
    goto :goto_4

    .line 388
    :cond_e
    new-array p1, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 389
    .line 390
    invoke-interface {v2, p1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 391
    .line 392
    .line 393
    move-result-object p1

    .line 394
    check-cast p1, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 395
    .line 396
    invoke-virtual {v1, p1}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 397
    .line 398
    .line 399
    goto/16 :goto_10

    .line 400
    .line 401
    :sswitch_5
    const-string p1, "scan_done_reward"

    .line 402
    .line 403
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 404
    .line 405
    .line 406
    move-result p1

    .line 407
    if-nez p1, :cond_f

    .line 408
    .line 409
    goto/16 :goto_10

    .line 410
    .line 411
    :cond_f
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 412
    .line 413
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getScandoneReward()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 414
    .line 415
    .line 416
    move-result-object p1

    .line 417
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 418
    .line 419
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getScandoneReward()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 420
    .line 421
    .line 422
    move-result-object v1

    .line 423
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 424
    .line 425
    .line 426
    move-result-object v1

    .line 427
    const-string v2, "mConfig.scandoneReward.source_info"

    .line 428
    .line 429
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 430
    .line 431
    .line 432
    new-instance v2, Ljava/util/ArrayList;

    .line 433
    .line 434
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 435
    .line 436
    .line 437
    array-length v5, v1

    .line 438
    const/4 v6, 0x0

    .line 439
    :goto_5
    if-ge v6, v5, :cond_11

    .line 440
    .line 441
    aget-object v7, v1, v6

    .line 442
    .line 443
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 444
    .line 445
    .line 446
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 447
    .line 448
    .line 449
    move-result v8

    .line 450
    xor-int/lit8 v8, v8, 0x1

    .line 451
    .line 452
    if-eqz v8, :cond_10

    .line 453
    .line 454
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 455
    .line 456
    .line 457
    :cond_10
    add-int/lit8 v6, v6, 0x1

    .line 458
    .line 459
    goto :goto_5

    .line 460
    :cond_11
    new-array v0, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 461
    .line 462
    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 463
    .line 464
    .line 465
    move-result-object v0

    .line 466
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 467
    .line 468
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 469
    .line 470
    .line 471
    goto/16 :goto_10

    .line 472
    .line 473
    :sswitch_6
    const-string p1, "doc_list_popup"

    .line 474
    .line 475
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 476
    .line 477
    .line 478
    move-result p1

    .line 479
    if-nez p1, :cond_12

    .line 480
    .line 481
    goto/16 :goto_10

    .line 482
    .line 483
    :cond_12
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 484
    .line 485
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getDocListPopUp()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 486
    .line 487
    .line 488
    move-result-object p1

    .line 489
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 490
    .line 491
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getDocListPopUp()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 492
    .line 493
    .line 494
    move-result-object v1

    .line 495
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 496
    .line 497
    .line 498
    move-result-object v1

    .line 499
    const-string v2, "mConfig.docListPopUp.source_info"

    .line 500
    .line 501
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 502
    .line 503
    .line 504
    new-instance v2, Ljava/util/ArrayList;

    .line 505
    .line 506
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 507
    .line 508
    .line 509
    array-length v5, v1

    .line 510
    const/4 v6, 0x0

    .line 511
    :goto_6
    if-ge v6, v5, :cond_14

    .line 512
    .line 513
    aget-object v7, v1, v6

    .line 514
    .line 515
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 516
    .line 517
    .line 518
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 519
    .line 520
    .line 521
    move-result v8

    .line 522
    xor-int/lit8 v8, v8, 0x1

    .line 523
    .line 524
    if-eqz v8, :cond_13

    .line 525
    .line 526
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 527
    .line 528
    .line 529
    :cond_13
    add-int/lit8 v6, v6, 0x1

    .line 530
    .line 531
    goto :goto_6

    .line 532
    :cond_14
    new-array v0, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 533
    .line 534
    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 535
    .line 536
    .line 537
    move-result-object v0

    .line 538
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 539
    .line 540
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 541
    .line 542
    .line 543
    goto/16 :goto_10

    .line 544
    .line 545
    :sswitch_7
    const-string p1, "ocr_reward"

    .line 546
    .line 547
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 548
    .line 549
    .line 550
    move-result p1

    .line 551
    if-nez p1, :cond_15

    .line 552
    .line 553
    goto/16 :goto_10

    .line 554
    .line 555
    :cond_15
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 556
    .line 557
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getOcrReward()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 558
    .line 559
    .line 560
    move-result-object p1

    .line 561
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 562
    .line 563
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getOcrReward()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 564
    .line 565
    .line 566
    move-result-object v1

    .line 567
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 568
    .line 569
    .line 570
    move-result-object v1

    .line 571
    const-string v2, "mConfig.ocrReward.source_info"

    .line 572
    .line 573
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 574
    .line 575
    .line 576
    new-instance v2, Ljava/util/ArrayList;

    .line 577
    .line 578
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 579
    .line 580
    .line 581
    array-length v5, v1

    .line 582
    const/4 v6, 0x0

    .line 583
    :goto_7
    if-ge v6, v5, :cond_17

    .line 584
    .line 585
    aget-object v7, v1, v6

    .line 586
    .line 587
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 588
    .line 589
    .line 590
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 591
    .line 592
    .line 593
    move-result v8

    .line 594
    xor-int/lit8 v8, v8, 0x1

    .line 595
    .line 596
    if-eqz v8, :cond_16

    .line 597
    .line 598
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 599
    .line 600
    .line 601
    :cond_16
    add-int/lit8 v6, v6, 0x1

    .line 602
    .line 603
    goto :goto_7

    .line 604
    :cond_17
    new-array v0, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 605
    .line 606
    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 607
    .line 608
    .line 609
    move-result-object v0

    .line 610
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 611
    .line 612
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 613
    .line 614
    .line 615
    goto/16 :goto_10

    .line 616
    .line 617
    :sswitch_8
    const-string p1, "scan_done_top"

    .line 618
    .line 619
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 620
    .line 621
    .line 622
    move-result p1

    .line 623
    if-nez p1, :cond_18

    .line 624
    .line 625
    goto/16 :goto_10

    .line 626
    .line 627
    :cond_18
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 628
    .line 629
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getScandoneTop()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 630
    .line 631
    .line 632
    move-result-object p1

    .line 633
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 634
    .line 635
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getScandoneTop()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 636
    .line 637
    .line 638
    move-result-object v1

    .line 639
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 640
    .line 641
    .line 642
    move-result-object v1

    .line 643
    const-string v2, "mConfig.scandoneTop.source_info"

    .line 644
    .line 645
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 646
    .line 647
    .line 648
    new-instance v2, Ljava/util/ArrayList;

    .line 649
    .line 650
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 651
    .line 652
    .line 653
    array-length v5, v1

    .line 654
    const/4 v6, 0x0

    .line 655
    :goto_8
    if-ge v6, v5, :cond_1a

    .line 656
    .line 657
    aget-object v7, v1, v6

    .line 658
    .line 659
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 660
    .line 661
    .line 662
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 663
    .line 664
    .line 665
    move-result v8

    .line 666
    xor-int/lit8 v8, v8, 0x1

    .line 667
    .line 668
    if-eqz v8, :cond_19

    .line 669
    .line 670
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 671
    .line 672
    .line 673
    :cond_19
    add-int/lit8 v6, v6, 0x1

    .line 674
    .line 675
    goto :goto_8

    .line 676
    :cond_1a
    new-array v0, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 677
    .line 678
    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 679
    .line 680
    .line 681
    move-result-object v0

    .line 682
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 683
    .line 684
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 685
    .line 686
    .line 687
    goto/16 :goto_10

    .line 688
    .line 689
    :sswitch_9
    const-string p1, "purchase_exit"

    .line 690
    .line 691
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 692
    .line 693
    .line 694
    move-result p1

    .line 695
    if-nez p1, :cond_1b

    .line 696
    .line 697
    goto/16 :goto_10

    .line 698
    .line 699
    :cond_1b
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 700
    .line 701
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getPurchase_exit()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 702
    .line 703
    .line 704
    move-result-object p1

    .line 705
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 706
    .line 707
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getPurchase_exit()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 708
    .line 709
    .line 710
    move-result-object v1

    .line 711
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 712
    .line 713
    .line 714
    move-result-object v1

    .line 715
    const-string v2, "mConfig.purchase_exit.source_info"

    .line 716
    .line 717
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 718
    .line 719
    .line 720
    new-instance v2, Ljava/util/ArrayList;

    .line 721
    .line 722
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 723
    .line 724
    .line 725
    array-length v5, v1

    .line 726
    const/4 v6, 0x0

    .line 727
    :goto_9
    if-ge v6, v5, :cond_1d

    .line 728
    .line 729
    aget-object v7, v1, v6

    .line 730
    .line 731
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 732
    .line 733
    .line 734
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 735
    .line 736
    .line 737
    move-result v8

    .line 738
    xor-int/lit8 v8, v8, 0x1

    .line 739
    .line 740
    if-eqz v8, :cond_1c

    .line 741
    .line 742
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 743
    .line 744
    .line 745
    :cond_1c
    add-int/lit8 v6, v6, 0x1

    .line 746
    .line 747
    goto :goto_9

    .line 748
    :cond_1d
    new-array v0, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 749
    .line 750
    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 751
    .line 752
    .line 753
    move-result-object v0

    .line 754
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 755
    .line 756
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 757
    .line 758
    .line 759
    goto/16 :goto_10

    .line 760
    .line 761
    :sswitch_a
    const-string p1, "pdf_watermark_video"

    .line 762
    .line 763
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 764
    .line 765
    .line 766
    move-result p1

    .line 767
    if-nez p1, :cond_1e

    .line 768
    .line 769
    goto/16 :goto_10

    .line 770
    .line 771
    :cond_1e
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 772
    .line 773
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getPdfWatermarkVideo()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 774
    .line 775
    .line 776
    move-result-object p1

    .line 777
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 778
    .line 779
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getPdfWatermarkVideo()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 780
    .line 781
    .line 782
    move-result-object v1

    .line 783
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 784
    .line 785
    .line 786
    move-result-object v1

    .line 787
    const-string v2, "mConfig.pdfWatermarkVideo.source_info"

    .line 788
    .line 789
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 790
    .line 791
    .line 792
    new-instance v2, Ljava/util/ArrayList;

    .line 793
    .line 794
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 795
    .line 796
    .line 797
    array-length v5, v1

    .line 798
    const/4 v6, 0x0

    .line 799
    :goto_a
    if-ge v6, v5, :cond_20

    .line 800
    .line 801
    aget-object v7, v1, v6

    .line 802
    .line 803
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 804
    .line 805
    .line 806
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 807
    .line 808
    .line 809
    move-result v8

    .line 810
    xor-int/lit8 v8, v8, 0x1

    .line 811
    .line 812
    if-eqz v8, :cond_1f

    .line 813
    .line 814
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 815
    .line 816
    .line 817
    :cond_1f
    add-int/lit8 v6, v6, 0x1

    .line 818
    .line 819
    goto :goto_a

    .line 820
    :cond_20
    new-array v0, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 821
    .line 822
    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 823
    .line 824
    .line 825
    move-result-object v0

    .line 826
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 827
    .line 828
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 829
    .line 830
    .line 831
    goto/16 :goto_10

    .line 832
    .line 833
    :sswitch_b
    const-string p1, "share_done"

    .line 834
    .line 835
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 836
    .line 837
    .line 838
    move-result p1

    .line 839
    if-nez p1, :cond_21

    .line 840
    .line 841
    goto/16 :goto_10

    .line 842
    .line 843
    :cond_21
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 844
    .line 845
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getShare_done()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 846
    .line 847
    .line 848
    move-result-object p1

    .line 849
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 850
    .line 851
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getShare_done()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 852
    .line 853
    .line 854
    move-result-object v1

    .line 855
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 856
    .line 857
    .line 858
    move-result-object v1

    .line 859
    const-string v2, "mConfig.share_done.source_info"

    .line 860
    .line 861
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 862
    .line 863
    .line 864
    new-instance v2, Ljava/util/ArrayList;

    .line 865
    .line 866
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 867
    .line 868
    .line 869
    array-length v5, v1

    .line 870
    const/4 v6, 0x0

    .line 871
    :goto_b
    if-ge v6, v5, :cond_23

    .line 872
    .line 873
    aget-object v7, v1, v6

    .line 874
    .line 875
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 876
    .line 877
    .line 878
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 879
    .line 880
    .line 881
    move-result v8

    .line 882
    xor-int/lit8 v8, v8, 0x1

    .line 883
    .line 884
    if-eqz v8, :cond_22

    .line 885
    .line 886
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 887
    .line 888
    .line 889
    :cond_22
    add-int/lit8 v6, v6, 0x1

    .line 890
    .line 891
    goto :goto_b

    .line 892
    :cond_23
    new-array v0, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 893
    .line 894
    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 895
    .line 896
    .line 897
    move-result-object v0

    .line 898
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 899
    .line 900
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 901
    .line 902
    .line 903
    goto/16 :goto_10

    .line 904
    .line 905
    :sswitch_c
    const-string v2, "scan_done"

    .line 906
    .line 907
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 908
    .line 909
    .line 910
    move-result v1

    .line 911
    if-nez v1, :cond_24

    .line 912
    .line 913
    goto/16 :goto_10

    .line 914
    .line 915
    :cond_24
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 916
    .line 917
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getScan_done()Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 918
    .line 919
    .line 920
    move-result-object v1

    .line 921
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getBanners()[Lcom/intsig/advertisement/bean/ItemConfig;

    .line 922
    .line 923
    .line 924
    move-result-object v1

    .line 925
    invoke-virtual {p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇080()I

    .line 926
    .line 927
    .line 928
    move-result v2

    .line 929
    aget-object v1, v1, v2

    .line 930
    .line 931
    iget-object v2, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 932
    .line 933
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ConfigResponse;->getScan_done()Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 934
    .line 935
    .line 936
    move-result-object v2

    .line 937
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getBanners()[Lcom/intsig/advertisement/bean/ItemConfig;

    .line 938
    .line 939
    .line 940
    move-result-object v2

    .line 941
    invoke-virtual {p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;->〇080()I

    .line 942
    .line 943
    .line 944
    move-result p1

    .line 945
    aget-object p1, v2, p1

    .line 946
    .line 947
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 948
    .line 949
    .line 950
    move-result-object p1

    .line 951
    const-string v2, "mConfig.scan_done.banners[cfg.index].source_info"

    .line 952
    .line 953
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 954
    .line 955
    .line 956
    new-instance v2, Ljava/util/ArrayList;

    .line 957
    .line 958
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 959
    .line 960
    .line 961
    array-length v5, p1

    .line 962
    const/4 v6, 0x0

    .line 963
    :goto_c
    if-ge v6, v5, :cond_26

    .line 964
    .line 965
    aget-object v7, p1, v6

    .line 966
    .line 967
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 968
    .line 969
    .line 970
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 971
    .line 972
    .line 973
    move-result v8

    .line 974
    xor-int/lit8 v8, v8, 0x1

    .line 975
    .line 976
    if-eqz v8, :cond_25

    .line 977
    .line 978
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 979
    .line 980
    .line 981
    :cond_25
    add-int/lit8 v6, v6, 0x1

    .line 982
    .line 983
    goto :goto_c

    .line 984
    :cond_26
    new-array p1, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 985
    .line 986
    invoke-interface {v2, p1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 987
    .line 988
    .line 989
    move-result-object p1

    .line 990
    check-cast p1, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 991
    .line 992
    invoke-virtual {v1, p1}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 993
    .line 994
    .line 995
    goto/16 :goto_10

    .line 996
    .line 997
    :sswitch_d
    const-string p1, "page_list_banner"

    .line 998
    .line 999
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 1000
    .line 1001
    .line 1002
    move-result p1

    .line 1003
    if-nez p1, :cond_27

    .line 1004
    .line 1005
    goto/16 :goto_10

    .line 1006
    .line 1007
    :cond_27
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 1008
    .line 1009
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getPage_list_banner()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 1010
    .line 1011
    .line 1012
    move-result-object p1

    .line 1013
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 1014
    .line 1015
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getPage_list_banner()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 1016
    .line 1017
    .line 1018
    move-result-object v1

    .line 1019
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 1020
    .line 1021
    .line 1022
    move-result-object v1

    .line 1023
    const-string v2, "mConfig.page_list_banner.source_info"

    .line 1024
    .line 1025
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1026
    .line 1027
    .line 1028
    new-instance v2, Ljava/util/ArrayList;

    .line 1029
    .line 1030
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1031
    .line 1032
    .line 1033
    array-length v5, v1

    .line 1034
    const/4 v6, 0x0

    .line 1035
    :goto_d
    if-ge v6, v5, :cond_29

    .line 1036
    .line 1037
    aget-object v7, v1, v6

    .line 1038
    .line 1039
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1040
    .line 1041
    .line 1042
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 1043
    .line 1044
    .line 1045
    move-result v8

    .line 1046
    xor-int/lit8 v8, v8, 0x1

    .line 1047
    .line 1048
    if-eqz v8, :cond_28

    .line 1049
    .line 1050
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1051
    .line 1052
    .line 1053
    :cond_28
    add-int/lit8 v6, v6, 0x1

    .line 1054
    .line 1055
    goto :goto_d

    .line 1056
    :cond_29
    new-array v0, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 1057
    .line 1058
    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1059
    .line 1060
    .line 1061
    move-result-object v0

    .line 1062
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 1063
    .line 1064
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 1065
    .line 1066
    .line 1067
    goto/16 :goto_10

    .line 1068
    .line 1069
    :sswitch_e
    const-string p1, "shot_done"

    .line 1070
    .line 1071
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 1072
    .line 1073
    .line 1074
    move-result p1

    .line 1075
    if-nez p1, :cond_2a

    .line 1076
    .line 1077
    goto/16 :goto_10

    .line 1078
    .line 1079
    :cond_2a
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 1080
    .line 1081
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getShotDone()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 1082
    .line 1083
    .line 1084
    move-result-object p1

    .line 1085
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 1086
    .line 1087
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getShotDone()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 1088
    .line 1089
    .line 1090
    move-result-object v1

    .line 1091
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 1092
    .line 1093
    .line 1094
    move-result-object v1

    .line 1095
    const-string v2, "mConfig.shotDone.source_info"

    .line 1096
    .line 1097
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1098
    .line 1099
    .line 1100
    new-instance v2, Ljava/util/ArrayList;

    .line 1101
    .line 1102
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1103
    .line 1104
    .line 1105
    array-length v5, v1

    .line 1106
    const/4 v6, 0x0

    .line 1107
    :goto_e
    if-ge v6, v5, :cond_2c

    .line 1108
    .line 1109
    aget-object v7, v1, v6

    .line 1110
    .line 1111
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1112
    .line 1113
    .line 1114
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 1115
    .line 1116
    .line 1117
    move-result v8

    .line 1118
    xor-int/lit8 v8, v8, 0x1

    .line 1119
    .line 1120
    if-eqz v8, :cond_2b

    .line 1121
    .line 1122
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1123
    .line 1124
    .line 1125
    :cond_2b
    add-int/lit8 v6, v6, 0x1

    .line 1126
    .line 1127
    goto :goto_e

    .line 1128
    :cond_2c
    new-array v0, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 1129
    .line 1130
    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1131
    .line 1132
    .line 1133
    move-result-object v0

    .line 1134
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 1135
    .line 1136
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 1137
    .line 1138
    .line 1139
    goto :goto_10

    .line 1140
    :sswitch_f
    const-string p1, "space_lottery"

    .line 1141
    .line 1142
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 1143
    .line 1144
    .line 1145
    move-result p1

    .line 1146
    if-nez p1, :cond_2d

    .line 1147
    .line 1148
    goto :goto_10

    .line 1149
    :cond_2d
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 1150
    .line 1151
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getSpace_lottery()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 1152
    .line 1153
    .line 1154
    move-result-object p1

    .line 1155
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 1156
    .line 1157
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getSpace_lottery()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 1158
    .line 1159
    .line 1160
    move-result-object v1

    .line 1161
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 1162
    .line 1163
    .line 1164
    move-result-object v1

    .line 1165
    const-string v2, "mConfig.space_lottery.source_info"

    .line 1166
    .line 1167
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1168
    .line 1169
    .line 1170
    new-instance v2, Ljava/util/ArrayList;

    .line 1171
    .line 1172
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1173
    .line 1174
    .line 1175
    array-length v5, v1

    .line 1176
    const/4 v6, 0x0

    .line 1177
    :goto_f
    if-ge v6, v5, :cond_2f

    .line 1178
    .line 1179
    aget-object v7, v1, v6

    .line 1180
    .line 1181
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1182
    .line 1183
    .line 1184
    invoke-static {v7, v0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragmentKt;->〇080(Lcom/intsig/advertisement/bean/SourceCfg;Lcom/intsig/advertisement/bean/SourceCfg;)Z

    .line 1185
    .line 1186
    .line 1187
    move-result v8

    .line 1188
    xor-int/lit8 v8, v8, 0x1

    .line 1189
    .line 1190
    if-eqz v8, :cond_2e

    .line 1191
    .line 1192
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1193
    .line 1194
    .line 1195
    :cond_2e
    add-int/lit8 v6, v6, 0x1

    .line 1196
    .line 1197
    goto :goto_f

    .line 1198
    :cond_2f
    new-array v0, v4, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 1199
    .line 1200
    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1201
    .line 1202
    .line 1203
    move-result-object v0

    .line 1204
    check-cast v0, [Lcom/intsig/advertisement/bean/SourceCfg;

    .line 1205
    .line 1206
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/bean/ItemConfig;->setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V

    .line 1207
    .line 1208
    .line 1209
    :goto_10
    return-void

    .line 1210
    nop

    .line 1211
    :sswitch_data_0
    .sparse-switch
        -0x79a9f6f0 -> :sswitch_f
        -0x76983259 -> :sswitch_e
        -0x719b1fc3 -> :sswitch_d
        -0x6cd263fc -> :sswitch_c
        -0x6a99601e -> :sswitch_b
        -0x66d4186d -> :sswitch_a
        -0x4eee9a64 -> :sswitch_9
        -0x349b1566 -> :sswitch_8
        -0x11abbe90 -> :sswitch_7
        0xa05f332 -> :sswitch_6
        0x30cee3ca -> :sswitch_5
        0x32266745 -> :sswitch_4
        0x3f912f91 -> :sswitch_3
        0x577b8110 -> :sswitch_2
        0x7c69676b -> :sswitch_1
        0x7db65576 -> :sswitch_0
    .end sparse-switch
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method public static final synthetic 〇Oo〇O()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o0OoOOo0:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇o08(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOO8oo0(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic 〇oO88o()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇08〇o0O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇oOO80o()V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/advertisement/util/SharePreferenceUtil;->〇o00〇〇Oo()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-nez v1, :cond_0

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/advertisement/util/SharePreferenceUtil;->〇o00〇〇Oo()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const-class v2, Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 18
    .line 19
    invoke-static {v1, v2}, Lcom/intsig/okgo/utils/GsonUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    const-string v2, "fromJsonString(SharePref\u2026nfigResponse::class.java)"

    .line 24
    .line 25
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    check-cast v1, Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 29
    .line 30
    iput-object v1, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 31
    .line 32
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O8o08O8O:Ljava/lang/String;

    .line 33
    .line 34
    iget-object v2, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 35
    .line 36
    invoke-static {v2}, Lcom/intsig/okgo/utils/GsonUtils;->Oo08(Ljava/lang/Object;)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    new-instance v3, Ljava/lang/StringBuilder;

    .line 41
    .line 42
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    .line 44
    .line 45
    const-string v4, "after parse "

    .line 46
    .line 47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    iget-object v1, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 61
    .line 62
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getApp_launch()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 63
    .line 64
    .line 65
    move-result-object v2

    .line 66
    const-string v3, "it"

    .line 67
    .line 68
    const-string v4, "source_info"

    .line 69
    .line 70
    if-eqz v2, :cond_1

    .line 71
    .line 72
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 73
    .line 74
    .line 75
    move-result-object v2

    .line 76
    if-eqz v2, :cond_1

    .line 77
    .line 78
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    new-instance v6, Ljava/util/ArrayList;

    .line 82
    .line 83
    array-length v7, v2

    .line 84
    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 85
    .line 86
    .line 87
    array-length v7, v2

    .line 88
    const/4 v8, 0x0

    .line 89
    :goto_0
    if-ge v8, v7, :cond_1

    .line 90
    .line 91
    aget-object v10, v2, v8

    .line 92
    .line 93
    iget-object v15, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 94
    .line 95
    new-instance v14, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 96
    .line 97
    invoke-static {v10, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    const-string v11, "app_launch"

    .line 101
    .line 102
    const/4 v12, 0x0

    .line 103
    const/4 v13, 0x4

    .line 104
    const/16 v16, 0x0

    .line 105
    .line 106
    move-object v9, v14

    .line 107
    move-object v5, v14

    .line 108
    move-object/from16 v14, v16

    .line 109
    .line 110
    invoke-direct/range {v9 .. v14}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 111
    .line 112
    .line 113
    invoke-virtual {v15, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    .line 115
    .line 116
    move-result v5

    .line 117
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 118
    .line 119
    .line 120
    move-result-object v5

    .line 121
    invoke-interface {v6, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 122
    .line 123
    .line 124
    add-int/lit8 v8, v8, 0x1

    .line 125
    .line 126
    goto :goto_0

    .line 127
    :cond_1
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getShare_done()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 128
    .line 129
    .line 130
    move-result-object v2

    .line 131
    if-eqz v2, :cond_2

    .line 132
    .line 133
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 134
    .line 135
    .line 136
    move-result-object v2

    .line 137
    if-eqz v2, :cond_2

    .line 138
    .line 139
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    .line 141
    .line 142
    new-instance v5, Ljava/util/ArrayList;

    .line 143
    .line 144
    array-length v6, v2

    .line 145
    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 146
    .line 147
    .line 148
    array-length v6, v2

    .line 149
    const/4 v7, 0x0

    .line 150
    :goto_1
    if-ge v7, v6, :cond_2

    .line 151
    .line 152
    aget-object v9, v2, v7

    .line 153
    .line 154
    iget-object v14, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 155
    .line 156
    new-instance v15, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 157
    .line 158
    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    .line 160
    .line 161
    const-string v10, "share_done"

    .line 162
    .line 163
    const/4 v11, 0x0

    .line 164
    const/4 v12, 0x4

    .line 165
    const/4 v13, 0x0

    .line 166
    move-object v8, v15

    .line 167
    invoke-direct/range {v8 .. v13}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 168
    .line 169
    .line 170
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    .line 172
    .line 173
    move-result v8

    .line 174
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 175
    .line 176
    .line 177
    move-result-object v8

    .line 178
    invoke-interface {v5, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 179
    .line 180
    .line 181
    add-int/lit8 v7, v7, 0x1

    .line 182
    .line 183
    goto :goto_1

    .line 184
    :cond_2
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getReward_video()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 185
    .line 186
    .line 187
    move-result-object v2

    .line 188
    if-eqz v2, :cond_3

    .line 189
    .line 190
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 191
    .line 192
    .line 193
    move-result-object v2

    .line 194
    if-eqz v2, :cond_3

    .line 195
    .line 196
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    .line 198
    .line 199
    new-instance v5, Ljava/util/ArrayList;

    .line 200
    .line 201
    array-length v6, v2

    .line 202
    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 203
    .line 204
    .line 205
    array-length v6, v2

    .line 206
    const/4 v7, 0x0

    .line 207
    :goto_2
    if-ge v7, v6, :cond_3

    .line 208
    .line 209
    aget-object v9, v2, v7

    .line 210
    .line 211
    iget-object v14, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 212
    .line 213
    new-instance v15, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 214
    .line 215
    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    .line 217
    .line 218
    const-string v10, "reward_video"

    .line 219
    .line 220
    const/4 v11, 0x0

    .line 221
    const/4 v12, 0x4

    .line 222
    const/4 v13, 0x0

    .line 223
    move-object v8, v15

    .line 224
    invoke-direct/range {v8 .. v13}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 225
    .line 226
    .line 227
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 228
    .line 229
    .line 230
    move-result v8

    .line 231
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 232
    .line 233
    .line 234
    move-result-object v8

    .line 235
    invoke-interface {v5, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 236
    .line 237
    .line 238
    add-int/lit8 v7, v7, 0x1

    .line 239
    .line 240
    goto :goto_2

    .line 241
    :cond_3
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getPage_list_banner()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 242
    .line 243
    .line 244
    move-result-object v2

    .line 245
    if-eqz v2, :cond_4

    .line 246
    .line 247
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 248
    .line 249
    .line 250
    move-result-object v2

    .line 251
    if-eqz v2, :cond_4

    .line 252
    .line 253
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    .line 255
    .line 256
    new-instance v5, Ljava/util/ArrayList;

    .line 257
    .line 258
    array-length v6, v2

    .line 259
    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 260
    .line 261
    .line 262
    array-length v6, v2

    .line 263
    const/4 v7, 0x0

    .line 264
    :goto_3
    if-ge v7, v6, :cond_4

    .line 265
    .line 266
    aget-object v9, v2, v7

    .line 267
    .line 268
    iget-object v14, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 269
    .line 270
    new-instance v15, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 271
    .line 272
    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 273
    .line 274
    .line 275
    const-string v10, "page_list_banner"

    .line 276
    .line 277
    const/4 v11, 0x0

    .line 278
    const/4 v12, 0x4

    .line 279
    const/4 v13, 0x0

    .line 280
    move-object v8, v15

    .line 281
    invoke-direct/range {v8 .. v13}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 282
    .line 283
    .line 284
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    .line 286
    .line 287
    move-result v8

    .line 288
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 289
    .line 290
    .line 291
    move-result-object v8

    .line 292
    invoke-interface {v5, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 293
    .line 294
    .line 295
    add-int/lit8 v7, v7, 0x1

    .line 296
    .line 297
    goto :goto_3

    .line 298
    :cond_4
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getMainMiddleBanner()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 299
    .line 300
    .line 301
    move-result-object v2

    .line 302
    if-eqz v2, :cond_5

    .line 303
    .line 304
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 305
    .line 306
    .line 307
    move-result-object v2

    .line 308
    if-eqz v2, :cond_5

    .line 309
    .line 310
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 311
    .line 312
    .line 313
    new-instance v5, Ljava/util/ArrayList;

    .line 314
    .line 315
    array-length v6, v2

    .line 316
    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 317
    .line 318
    .line 319
    array-length v6, v2

    .line 320
    const/4 v7, 0x0

    .line 321
    :goto_4
    if-ge v7, v6, :cond_5

    .line 322
    .line 323
    aget-object v9, v2, v7

    .line 324
    .line 325
    iget-object v14, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 326
    .line 327
    new-instance v15, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 328
    .line 329
    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 330
    .line 331
    .line 332
    const-string v10, "main_middle_banner"

    .line 333
    .line 334
    const/4 v11, 0x0

    .line 335
    const/4 v12, 0x4

    .line 336
    const/4 v13, 0x0

    .line 337
    move-object v8, v15

    .line 338
    invoke-direct/range {v8 .. v13}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 339
    .line 340
    .line 341
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 342
    .line 343
    .line 344
    move-result v8

    .line 345
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 346
    .line 347
    .line 348
    move-result-object v8

    .line 349
    invoke-interface {v5, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 350
    .line 351
    .line 352
    add-int/lit8 v7, v7, 0x1

    .line 353
    .line 354
    goto :goto_4

    .line 355
    :cond_5
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getScandoneReward()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 356
    .line 357
    .line 358
    move-result-object v2

    .line 359
    if-eqz v2, :cond_6

    .line 360
    .line 361
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 362
    .line 363
    .line 364
    move-result-object v2

    .line 365
    if-eqz v2, :cond_6

    .line 366
    .line 367
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 368
    .line 369
    .line 370
    new-instance v5, Ljava/util/ArrayList;

    .line 371
    .line 372
    array-length v6, v2

    .line 373
    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 374
    .line 375
    .line 376
    array-length v6, v2

    .line 377
    const/4 v7, 0x0

    .line 378
    :goto_5
    if-ge v7, v6, :cond_6

    .line 379
    .line 380
    aget-object v9, v2, v7

    .line 381
    .line 382
    iget-object v14, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 383
    .line 384
    new-instance v15, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 385
    .line 386
    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 387
    .line 388
    .line 389
    const-string v10, "scan_done_reward"

    .line 390
    .line 391
    const/4 v11, 0x0

    .line 392
    const/4 v12, 0x4

    .line 393
    const/4 v13, 0x0

    .line 394
    move-object v8, v15

    .line 395
    invoke-direct/range {v8 .. v13}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 396
    .line 397
    .line 398
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 399
    .line 400
    .line 401
    move-result v8

    .line 402
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 403
    .line 404
    .line 405
    move-result-object v8

    .line 406
    invoke-interface {v5, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 407
    .line 408
    .line 409
    add-int/lit8 v7, v7, 0x1

    .line 410
    .line 411
    goto :goto_5

    .line 412
    :cond_6
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getOcrReward()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 413
    .line 414
    .line 415
    move-result-object v2

    .line 416
    if-eqz v2, :cond_7

    .line 417
    .line 418
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 419
    .line 420
    .line 421
    move-result-object v2

    .line 422
    if-eqz v2, :cond_7

    .line 423
    .line 424
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 425
    .line 426
    .line 427
    new-instance v5, Ljava/util/ArrayList;

    .line 428
    .line 429
    array-length v6, v2

    .line 430
    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 431
    .line 432
    .line 433
    array-length v6, v2

    .line 434
    const/4 v7, 0x0

    .line 435
    :goto_6
    if-ge v7, v6, :cond_7

    .line 436
    .line 437
    aget-object v9, v2, v7

    .line 438
    .line 439
    iget-object v14, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 440
    .line 441
    new-instance v15, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 442
    .line 443
    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 444
    .line 445
    .line 446
    const-string v10, "ocr_reward"

    .line 447
    .line 448
    const/4 v11, 0x0

    .line 449
    const/4 v12, 0x4

    .line 450
    const/4 v13, 0x0

    .line 451
    move-object v8, v15

    .line 452
    invoke-direct/range {v8 .. v13}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 453
    .line 454
    .line 455
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456
    .line 457
    .line 458
    move-result v8

    .line 459
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 460
    .line 461
    .line 462
    move-result-object v8

    .line 463
    invoke-interface {v5, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 464
    .line 465
    .line 466
    add-int/lit8 v7, v7, 0x1

    .line 467
    .line 468
    goto :goto_6

    .line 469
    :cond_7
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getShotDone()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 470
    .line 471
    .line 472
    move-result-object v2

    .line 473
    if-eqz v2, :cond_8

    .line 474
    .line 475
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 476
    .line 477
    .line 478
    move-result-object v2

    .line 479
    if-eqz v2, :cond_8

    .line 480
    .line 481
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 482
    .line 483
    .line 484
    new-instance v5, Ljava/util/ArrayList;

    .line 485
    .line 486
    array-length v6, v2

    .line 487
    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 488
    .line 489
    .line 490
    array-length v6, v2

    .line 491
    const/4 v7, 0x0

    .line 492
    :goto_7
    if-ge v7, v6, :cond_8

    .line 493
    .line 494
    aget-object v9, v2, v7

    .line 495
    .line 496
    iget-object v14, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 497
    .line 498
    new-instance v15, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 499
    .line 500
    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 501
    .line 502
    .line 503
    const-string v10, "shot_done"

    .line 504
    .line 505
    const/4 v11, 0x0

    .line 506
    const/4 v12, 0x4

    .line 507
    const/4 v13, 0x0

    .line 508
    move-object v8, v15

    .line 509
    invoke-direct/range {v8 .. v13}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 510
    .line 511
    .line 512
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 513
    .line 514
    .line 515
    move-result v8

    .line 516
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 517
    .line 518
    .line 519
    move-result-object v8

    .line 520
    invoke-interface {v5, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 521
    .line 522
    .line 523
    add-int/lit8 v7, v7, 0x1

    .line 524
    .line 525
    goto :goto_7

    .line 526
    :cond_8
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getScandoneTop()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 527
    .line 528
    .line 529
    move-result-object v2

    .line 530
    if-eqz v2, :cond_9

    .line 531
    .line 532
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 533
    .line 534
    .line 535
    move-result-object v2

    .line 536
    if-eqz v2, :cond_9

    .line 537
    .line 538
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 539
    .line 540
    .line 541
    new-instance v5, Ljava/util/ArrayList;

    .line 542
    .line 543
    array-length v6, v2

    .line 544
    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 545
    .line 546
    .line 547
    array-length v6, v2

    .line 548
    const/4 v7, 0x0

    .line 549
    :goto_8
    if-ge v7, v6, :cond_9

    .line 550
    .line 551
    aget-object v9, v2, v7

    .line 552
    .line 553
    iget-object v14, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 554
    .line 555
    new-instance v15, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 556
    .line 557
    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 558
    .line 559
    .line 560
    const-string v10, "scan_done_top"

    .line 561
    .line 562
    const/4 v11, 0x0

    .line 563
    const/4 v12, 0x4

    .line 564
    const/4 v13, 0x0

    .line 565
    move-object v8, v15

    .line 566
    invoke-direct/range {v8 .. v13}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 567
    .line 568
    .line 569
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570
    .line 571
    .line 572
    move-result v8

    .line 573
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 574
    .line 575
    .line 576
    move-result-object v8

    .line 577
    invoke-interface {v5, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 578
    .line 579
    .line 580
    add-int/lit8 v7, v7, 0x1

    .line 581
    .line 582
    goto :goto_8

    .line 583
    :cond_9
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getDocListPopUp()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 584
    .line 585
    .line 586
    move-result-object v2

    .line 587
    if-eqz v2, :cond_a

    .line 588
    .line 589
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 590
    .line 591
    .line 592
    move-result-object v2

    .line 593
    if-eqz v2, :cond_a

    .line 594
    .line 595
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 596
    .line 597
    .line 598
    new-instance v5, Ljava/util/ArrayList;

    .line 599
    .line 600
    array-length v6, v2

    .line 601
    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 602
    .line 603
    .line 604
    array-length v6, v2

    .line 605
    const/4 v7, 0x0

    .line 606
    :goto_9
    if-ge v7, v6, :cond_a

    .line 607
    .line 608
    aget-object v9, v2, v7

    .line 609
    .line 610
    iget-object v14, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 611
    .line 612
    new-instance v15, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 613
    .line 614
    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 615
    .line 616
    .line 617
    const-string v10, "doc_list_popup"

    .line 618
    .line 619
    const/4 v11, 0x0

    .line 620
    const/4 v12, 0x4

    .line 621
    const/4 v13, 0x0

    .line 622
    move-object v8, v15

    .line 623
    invoke-direct/range {v8 .. v13}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 624
    .line 625
    .line 626
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 627
    .line 628
    .line 629
    move-result v8

    .line 630
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 631
    .line 632
    .line 633
    move-result-object v8

    .line 634
    invoke-interface {v5, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 635
    .line 636
    .line 637
    add-int/lit8 v7, v7, 0x1

    .line 638
    .line 639
    goto :goto_9

    .line 640
    :cond_a
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getFunctionReward()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 641
    .line 642
    .line 643
    move-result-object v2

    .line 644
    if-eqz v2, :cond_b

    .line 645
    .line 646
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 647
    .line 648
    .line 649
    move-result-object v2

    .line 650
    if-eqz v2, :cond_b

    .line 651
    .line 652
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 653
    .line 654
    .line 655
    new-instance v5, Ljava/util/ArrayList;

    .line 656
    .line 657
    array-length v6, v2

    .line 658
    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 659
    .line 660
    .line 661
    array-length v6, v2

    .line 662
    const/4 v7, 0x0

    .line 663
    :goto_a
    if-ge v7, v6, :cond_b

    .line 664
    .line 665
    aget-object v9, v2, v7

    .line 666
    .line 667
    iget-object v14, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 668
    .line 669
    new-instance v15, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 670
    .line 671
    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 672
    .line 673
    .line 674
    const-string v10, "function_reward"

    .line 675
    .line 676
    const/4 v11, 0x0

    .line 677
    const/4 v12, 0x4

    .line 678
    const/4 v13, 0x0

    .line 679
    move-object v8, v15

    .line 680
    invoke-direct/range {v8 .. v13}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 681
    .line 682
    .line 683
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 684
    .line 685
    .line 686
    move-result v8

    .line 687
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 688
    .line 689
    .line 690
    move-result-object v8

    .line 691
    invoke-interface {v5, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 692
    .line 693
    .line 694
    add-int/lit8 v7, v7, 0x1

    .line 695
    .line 696
    goto :goto_a

    .line 697
    :cond_b
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getPurchase_exit()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 698
    .line 699
    .line 700
    move-result-object v2

    .line 701
    if-eqz v2, :cond_c

    .line 702
    .line 703
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 704
    .line 705
    .line 706
    move-result-object v2

    .line 707
    if-eqz v2, :cond_c

    .line 708
    .line 709
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 710
    .line 711
    .line 712
    new-instance v5, Ljava/util/ArrayList;

    .line 713
    .line 714
    array-length v6, v2

    .line 715
    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 716
    .line 717
    .line 718
    array-length v6, v2

    .line 719
    const/4 v7, 0x0

    .line 720
    :goto_b
    if-ge v7, v6, :cond_c

    .line 721
    .line 722
    aget-object v9, v2, v7

    .line 723
    .line 724
    iget-object v14, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 725
    .line 726
    new-instance v15, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 727
    .line 728
    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 729
    .line 730
    .line 731
    const-string v10, "purchase_exit"

    .line 732
    .line 733
    const/4 v11, 0x0

    .line 734
    const/4 v12, 0x4

    .line 735
    const/4 v13, 0x0

    .line 736
    move-object v8, v15

    .line 737
    invoke-direct/range {v8 .. v13}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 738
    .line 739
    .line 740
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 741
    .line 742
    .line 743
    move-result v8

    .line 744
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 745
    .line 746
    .line 747
    move-result-object v8

    .line 748
    invoke-interface {v5, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 749
    .line 750
    .line 751
    add-int/lit8 v7, v7, 0x1

    .line 752
    .line 753
    goto :goto_b

    .line 754
    :cond_c
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getSpace_lottery()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 755
    .line 756
    .line 757
    move-result-object v2

    .line 758
    if-eqz v2, :cond_d

    .line 759
    .line 760
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 761
    .line 762
    .line 763
    move-result-object v2

    .line 764
    if-eqz v2, :cond_d

    .line 765
    .line 766
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 767
    .line 768
    .line 769
    new-instance v5, Ljava/util/ArrayList;

    .line 770
    .line 771
    array-length v6, v2

    .line 772
    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 773
    .line 774
    .line 775
    array-length v6, v2

    .line 776
    const/4 v7, 0x0

    .line 777
    :goto_c
    if-ge v7, v6, :cond_d

    .line 778
    .line 779
    aget-object v9, v2, v7

    .line 780
    .line 781
    iget-object v14, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 782
    .line 783
    new-instance v15, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 784
    .line 785
    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 786
    .line 787
    .line 788
    const-string v10, "space_lottery"

    .line 789
    .line 790
    const/4 v11, 0x0

    .line 791
    const/4 v12, 0x4

    .line 792
    const/4 v13, 0x0

    .line 793
    move-object v8, v15

    .line 794
    invoke-direct/range {v8 .. v13}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 795
    .line 796
    .line 797
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 798
    .line 799
    .line 800
    move-result v8

    .line 801
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 802
    .line 803
    .line 804
    move-result-object v8

    .line 805
    invoke-interface {v5, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 806
    .line 807
    .line 808
    add-int/lit8 v7, v7, 0x1

    .line 809
    .line 810
    goto :goto_c

    .line 811
    :cond_d
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getPdfWatermarkVideo()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 812
    .line 813
    .line 814
    move-result-object v2

    .line 815
    if-eqz v2, :cond_e

    .line 816
    .line 817
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 818
    .line 819
    .line 820
    move-result-object v2

    .line 821
    if-eqz v2, :cond_e

    .line 822
    .line 823
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 824
    .line 825
    .line 826
    new-instance v5, Ljava/util/ArrayList;

    .line 827
    .line 828
    array-length v6, v2

    .line 829
    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 830
    .line 831
    .line 832
    array-length v6, v2

    .line 833
    const/4 v7, 0x0

    .line 834
    :goto_d
    if-ge v7, v6, :cond_e

    .line 835
    .line 836
    aget-object v9, v2, v7

    .line 837
    .line 838
    iget-object v14, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 839
    .line 840
    new-instance v15, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 841
    .line 842
    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 843
    .line 844
    .line 845
    const-string v10, "pdf_watermark_video"

    .line 846
    .line 847
    const/4 v11, 0x0

    .line 848
    const/4 v12, 0x4

    .line 849
    const/4 v13, 0x0

    .line 850
    move-object v8, v15

    .line 851
    invoke-direct/range {v8 .. v13}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 852
    .line 853
    .line 854
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 855
    .line 856
    .line 857
    move-result v8

    .line 858
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 859
    .line 860
    .line 861
    move-result-object v8

    .line 862
    invoke-interface {v5, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 863
    .line 864
    .line 865
    add-int/lit8 v7, v7, 0x1

    .line 866
    .line 867
    goto :goto_d

    .line 868
    :cond_e
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getDoc_list()Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 869
    .line 870
    .line 871
    move-result-object v2

    .line 872
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getBanners()[Lcom/intsig/advertisement/bean/ItemConfig;

    .line 873
    .line 874
    .line 875
    move-result-object v2

    .line 876
    const-string v3, "this.doc_list.banners"

    .line 877
    .line 878
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 879
    .line 880
    .line 881
    array-length v3, v2

    .line 882
    const/4 v5, 0x0

    .line 883
    const/4 v6, 0x0

    .line 884
    :goto_e
    const-string v7, "sourceCfg"

    .line 885
    .line 886
    if-ge v5, v3, :cond_10

    .line 887
    .line 888
    aget-object v8, v2, v5

    .line 889
    .line 890
    add-int/lit8 v9, v6, 0x1

    .line 891
    .line 892
    if-eqz v8, :cond_f

    .line 893
    .line 894
    invoke-virtual {v8}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 895
    .line 896
    .line 897
    move-result-object v8

    .line 898
    if-eqz v8, :cond_f

    .line 899
    .line 900
    invoke-static {v8, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 901
    .line 902
    .line 903
    new-instance v10, Ljava/util/ArrayList;

    .line 904
    .line 905
    array-length v11, v8

    .line 906
    invoke-direct {v10, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 907
    .line 908
    .line 909
    array-length v11, v8

    .line 910
    const/4 v12, 0x0

    .line 911
    :goto_f
    if-ge v12, v11, :cond_f

    .line 912
    .line 913
    aget-object v13, v8, v12

    .line 914
    .line 915
    iget-object v14, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 916
    .line 917
    new-instance v15, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 918
    .line 919
    invoke-static {v13, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 920
    .line 921
    .line 922
    move-object/from16 v16, v2

    .line 923
    .line 924
    const-string v2, "doc_list"

    .line 925
    .line 926
    invoke-direct {v15, v13, v2, v6}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;I)V

    .line 927
    .line 928
    .line 929
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 930
    .line 931
    .line 932
    move-result v2

    .line 933
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 934
    .line 935
    .line 936
    move-result-object v2

    .line 937
    invoke-interface {v10, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 938
    .line 939
    .line 940
    add-int/lit8 v12, v12, 0x1

    .line 941
    .line 942
    move-object/from16 v2, v16

    .line 943
    .line 944
    goto :goto_f

    .line 945
    :cond_f
    move-object/from16 v16, v2

    .line 946
    .line 947
    add-int/lit8 v5, v5, 0x1

    .line 948
    .line 949
    move v6, v9

    .line 950
    move-object/from16 v2, v16

    .line 951
    .line 952
    goto :goto_e

    .line 953
    :cond_10
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getScan_done()Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 954
    .line 955
    .line 956
    move-result-object v1

    .line 957
    invoke-virtual {v1}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getBanners()[Lcom/intsig/advertisement/bean/ItemConfig;

    .line 958
    .line 959
    .line 960
    move-result-object v1

    .line 961
    const-string v2, "this.scan_done.banners"

    .line 962
    .line 963
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 964
    .line 965
    .line 966
    array-length v2, v1

    .line 967
    const/4 v3, 0x0

    .line 968
    const/4 v5, 0x0

    .line 969
    :goto_10
    if-ge v3, v2, :cond_12

    .line 970
    .line 971
    aget-object v6, v1, v3

    .line 972
    .line 973
    add-int/lit8 v8, v5, 0x1

    .line 974
    .line 975
    if-eqz v6, :cond_11

    .line 976
    .line 977
    invoke-virtual {v6}, Lcom/intsig/advertisement/bean/ItemConfig;->getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 978
    .line 979
    .line 980
    move-result-object v6

    .line 981
    if-eqz v6, :cond_11

    .line 982
    .line 983
    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 984
    .line 985
    .line 986
    new-instance v9, Ljava/util/ArrayList;

    .line 987
    .line 988
    array-length v10, v6

    .line 989
    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 990
    .line 991
    .line 992
    array-length v10, v6

    .line 993
    const/4 v11, 0x0

    .line 994
    :goto_11
    if-ge v11, v10, :cond_11

    .line 995
    .line 996
    aget-object v12, v6, v11

    .line 997
    .line 998
    iget-object v13, v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 999
    .line 1000
    new-instance v14, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;

    .line 1001
    .line 1002
    invoke-static {v12, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1003
    .line 1004
    .line 1005
    const-string v15, "scan_done"

    .line 1006
    .line 1007
    invoke-direct {v14, v12, v15, v5}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment$SourceCfgWithPos;-><init>(Lcom/intsig/advertisement/bean/SourceCfg;Ljava/lang/String;I)V

    .line 1008
    .line 1009
    .line 1010
    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1011
    .line 1012
    .line 1013
    move-result v12

    .line 1014
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1015
    .line 1016
    .line 1017
    move-result-object v12

    .line 1018
    invoke-interface {v9, v12}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1019
    .line 1020
    .line 1021
    add-int/lit8 v11, v11, 0x1

    .line 1022
    .line 1023
    goto :goto_11

    .line 1024
    :cond_11
    add-int/lit8 v3, v3, 0x1

    .line 1025
    .line 1026
    move v5, v8

    .line 1027
    goto :goto_10

    .line 1028
    :cond_12
    return-void
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
    .line 1825
    .line 1826
    .line 1827
    .line 1828
    .line 1829
    .line 1830
    .line 1831
    .line 1832
    .line 1833
    .line 1834
    .line 1835
    .line 1836
    .line 1837
    .line 1838
    .line 1839
    .line 1840
    .line 1841
    .line 1842
    .line 1843
    .line 1844
    .line 1845
    .line 1846
    .line 1847
    .line 1848
    .line 1849
    .line 1850
    .line 1851
    .line 1852
    .line 1853
    .line 1854
    .line 1855
    .line 1856
    .line 1857
    .line 1858
    .line 1859
    .line 1860
    .line 1861
    .line 1862
    .line 1863
    .line 1864
    .line 1865
    .line 1866
    .line 1867
    .line 1868
    .line 1869
    .line 1870
    .line 1871
    .line 1872
    .line 1873
    .line 1874
    .line 1875
    .line 1876
    .line 1877
    .line 1878
    .line 1879
    .line 1880
    .line 1881
    .line 1882
    .line 1883
    .line 1884
    .line 1885
    .line 1886
    .line 1887
    .line 1888
    .line 1889
    .line 1890
    .line 1891
    .line 1892
    .line 1893
    .line 1894
    .line 1895
    .line 1896
    .line 1897
    .line 1898
    .line 1899
    .line 1900
    .line 1901
    .line 1902
    .line 1903
    .line 1904
    .line 1905
    .line 1906
    .line 1907
    .line 1908
    .line 1909
    .line 1910
    .line 1911
    .line 1912
    .line 1913
    .line 1914
    .line 1915
    .line 1916
    .line 1917
    .line 1918
    .line 1919
    .line 1920
    .line 1921
    .line 1922
    .line 1923
    .line 1924
    .line 1925
    .line 1926
    .line 1927
    .line 1928
    .line 1929
    .line 1930
    .line 1931
    .line 1932
    .line 1933
    .line 1934
    .line 1935
    .line 1936
    .line 1937
    .line 1938
    .line 1939
    .line 1940
    .line 1941
    .line 1942
    .line 1943
    .line 1944
    .line 1945
    .line 1946
    .line 1947
    .line 1948
    .line 1949
    .line 1950
    .line 1951
    .line 1952
    .line 1953
    .line 1954
    .line 1955
    .line 1956
    .line 1957
    .line 1958
    .line 1959
    .line 1960
    .line 1961
    .line 1962
    .line 1963
    .line 1964
    .line 1965
    .line 1966
    .line 1967
    .line 1968
    .line 1969
    .line 1970
    .line 1971
    .line 1972
    .line 1973
    .line 1974
    .line 1975
    .line 1976
    .line 1977
    .line 1978
    .line 1979
    .line 1980
    .line 1981
    .line 1982
    .line 1983
    .line 1984
    .line 1985
    .line 1986
    .line 1987
    .line 1988
    .line 1989
    .line 1990
    .line 1991
    .line 1992
    .line 1993
    .line 1994
    .line 1995
    .line 1996
    .line 1997
    .line 1998
    .line 1999
    .line 2000
    .line 2001
    .line 2002
    .line 2003
    .line 2004
    .line 2005
    .line 2006
    .line 2007
    .line 2008
    .line 2009
    .line 2010
    .line 2011
    .line 2012
    .line 2013
    .line 2014
    .line 2015
    .line 2016
    .line 2017
    .line 2018
    .line 2019
    .line 2020
    .line 2021
    .line 2022
    .line 2023
    .line 2024
    .line 2025
    .line 2026
    .line 2027
    .line 2028
    .line 2029
    .line 2030
    .line 2031
    .line 2032
    .line 2033
    .line 2034
    .line 2035
    .line 2036
    .line 2037
    .line 2038
    .line 2039
    .line 2040
    .line 2041
    .line 2042
    .line 2043
    .line 2044
    .line 2045
    .line 2046
    .line 2047
    .line 2048
    .line 2049
    .line 2050
    .line 2051
    .line 2052
    .line 2053
    .line 2054
    .line 2055
    .line 2056
    .line 2057
    .line 2058
    .line 2059
    .line 2060
    .line 2061
    .line 2062
    .line 2063
    .line 2064
    .line 2065
    .line 2066
    .line 2067
    .line 2068
    .line 2069
    .line 2070
    .line 2071
    .line 2072
    .line 2073
    .line 2074
    .line 2075
    .line 2076
    .line 2077
    .line 2078
    .line 2079
    .line 2080
    .line 2081
    .line 2082
    .line 2083
    .line 2084
    .line 2085
    .line 2086
.end method

.method public static final synthetic 〇oO〇08o()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇OO8ooO8〇:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O0O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic 〇ooO〇000(Lcom/intsig/camscanner/test/docjson/AdConfigFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇08O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic 〇〇()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇OO〇00〇0O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇〇O80〇0o()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOO8:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇〇〇0()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O88O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇〇〇00()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇o0O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇〇〇O〇()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇〇0o〇〇0:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p3, "inflater"

    .line 2
    .line 3
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const p3, 0x7f0d0294

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->o0:Landroid/view/View;

    .line 15
    .line 16
    const p2, 0x7f0a0665

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    check-cast p1, Lcom/intsig/view/FlowLayout;

    .line 24
    .line 25
    iput-object p1, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->〇OOo8〇0:Lcom/intsig/view/FlowLayout;

    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->o0:Landroid/view/View;

    .line 28
    .line 29
    return-object p1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public onStop()V
    .locals 6

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStop()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O8o08O8O:Ljava/lang/String;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 7
    .line 8
    iget-object v2, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇〇08O:Ljava/lang/String;

    .line 9
    .line 10
    iget-object v3, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O0O:Ljava/lang/String;

    .line 11
    .line 12
    new-instance v4, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v5, "mpos is "

    .line 18
    .line 19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const-string v1, ", msrc is "

    .line 26
    .line 27
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string v1, ", mtype is "

    .line 34
    .line 35
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/DocJsonBaseFragment;->o0:Landroid/view/View;

    .line 49
    .line 50
    const v1, 0x7f0a1162

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    check-cast v0, Landroid/widget/Switch;

    .line 58
    .line 59
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    if-eqz v0, :cond_0

    .line 64
    .line 65
    invoke-direct {p0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0o0oO〇〇0()V

    .line 66
    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 69
    .line 70
    invoke-static {v0}, Lcom/intsig/okgo/utils/GsonUtils;->Oo08(Ljava/lang/Object;)Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    invoke-static {v0}, Lcom/intsig/advertisement/util/SharePreferenceUtil;->〇〇888(Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->oOo〇8o008:Lcom/intsig/advertisement/bean/ConfigResponse;

    .line 78
    .line 79
    invoke-static {v0}, Lcom/intsig/advertisement/control/AdConfigManager;->〇0〇O0088o(Lcom/intsig/advertisement/bean/ConfigResponse;)V

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_0
    const-string v0, ""

    .line 84
    .line 85
    invoke-static {v0}, Lcom/intsig/advertisement/util/SharePreferenceUtil;->〇〇888(Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    const/4 v0, 0x0

    .line 89
    invoke-static {v0}, Lcom/intsig/advertisement/control/AdConfigManager;->〇0〇O0088o(Lcom/intsig/advertisement/bean/ConfigResponse;)V

    .line 90
    .line 91
    .line 92
    :goto_0
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇oOO80o()V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->O〇〇o8O()V

    .line 13
    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->o0O0O〇〇〇0()V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/test/docjson/AdConfigFragment;->〇0o88Oo〇()V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

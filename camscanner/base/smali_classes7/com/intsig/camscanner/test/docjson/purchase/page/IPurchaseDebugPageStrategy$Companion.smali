.class public final Lcom/intsig/camscanner/test/docjson/purchase/page/IPurchaseDebugPageStrategy$Companion;
.super Ljava/lang/Object;
.source "IPurchaseDebugPageStrategy.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/test/docjson/purchase/page/IPurchaseDebugPageStrategy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic 〇080:Lcom/intsig/camscanner/test/docjson/purchase/page/IPurchaseDebugPageStrategy$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/test/docjson/purchase/page/IPurchaseDebugPageStrategy$Companion;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/test/docjson/purchase/page/IPurchaseDebugPageStrategy$Companion;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/test/docjson/purchase/page/IPurchaseDebugPageStrategy$Companion;->〇080:Lcom/intsig/camscanner/test/docjson/purchase/page/IPurchaseDebugPageStrategy$Companion;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final 〇080(Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;)Lcom/intsig/camscanner/test/docjson/purchase/page/IPurchaseDebugPageStrategy;
    .locals 1
    .param p1    # Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "page"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;->getPage()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    if-eqz p1, :cond_5

    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    if-eq p1, v0, :cond_4

    .line 14
    .line 15
    const/4 v0, 0x2

    .line 16
    if-eq p1, v0, :cond_3

    .line 17
    .line 18
    const/4 v0, 0x3

    .line 19
    if-eq p1, v0, :cond_2

    .line 20
    .line 21
    const/4 v0, 0x4

    .line 22
    if-eq p1, v0, :cond_1

    .line 23
    .line 24
    const/4 v0, 0x5

    .line 25
    if-eq p1, v0, :cond_0

    .line 26
    .line 27
    const/4 p1, 0x0

    .line 28
    goto :goto_0

    .line 29
    :cond_0
    new-instance p1, Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugMarkStrategy;

    .line 30
    .line 31
    invoke-direct {p1}, Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugMarkStrategy;-><init>()V

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    new-instance p1, Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugAfterScanCNStrategy;

    .line 36
    .line 37
    invoke-direct {p1}, Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugAfterScanCNStrategy;-><init>()V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_2
    new-instance p1, Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugAfterScanStrategy;

    .line 42
    .line 43
    invoke-direct {p1}, Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugAfterScanStrategy;-><init>()V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_3
    new-instance p1, Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugActiveStrategy;

    .line 48
    .line 49
    invoke-direct {p1}, Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugActiveStrategy;-><init>()V

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_4
    new-instance p1, Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugPassiveStrategy;

    .line 54
    .line 55
    invoke-direct {p1}, Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugPassiveStrategy;-><init>()V

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_5
    new-instance p1, Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugGuideStrategy;

    .line 60
    .line 61
    invoke-direct {p1}, Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugGuideStrategy;-><init>()V

    .line 62
    .line 63
    .line 64
    :goto_0
    return-object p1
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

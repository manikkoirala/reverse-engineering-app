.class public final Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugMarkStrategy;
.super Ljava/lang/Object;
.source "PurchaseDebugMarkStrategy.kt"

# interfaces
.implements Lcom/intsig/camscanner/test/docjson/purchase/page/IPurchaseDebugPageStrategy;


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇o00〇〇Oo()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇80o〇o0()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇o〇()Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0o0〇8o()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->O880〇Oo(I)V

    .line 15
    .line 16
    .line 17
    const/4 v1, 0x1

    .line 18
    :cond_0
    return v1
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public O8(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;)V
    .locals 0
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/test/docjson/purchase/page/IPurchaseDebugPageStrategy$DefaultImpls;->〇080(Lcom/intsig/camscanner/test/docjson/purchase/page/IPurchaseDebugPageStrategy;Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇080(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;)V
    .locals 3
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "page"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/guide/markguide/CnGuideMarkControl;

    .line 12
    .line 13
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/guide/markguide/CnGuideMarkControl;-><init>(Landroid/app/Activity;)V

    .line 14
    .line 15
    .line 16
    new-instance v1, Lcom/intsig/camscanner/guide/markguide/GpGuideMarkControl;

    .line 17
    .line 18
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/guide/markguide/GpGuideMarkControl;-><init>(Landroid/app/Activity;)V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugMarkStrategy;->〇o00〇〇Oo()Z

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    if-eqz v2, :cond_0

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/markguide/CnGuideMarkControl;->〇80〇808〇O()V

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugMarkStrategy;->〇o〇()Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    invoke-virtual {v1}, Lcom/intsig/camscanner/guide/markguide/GpGuideMarkControl;->〇O8o08O()V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/test/docjson/purchase/page/PurchaseDebugMarkStrategy;->O8(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/test/docjson/purchase/type/PurchaseDebugPageType;)V

    .line 42
    .line 43
    .line 44
    :goto_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.class public Lcom/intsig/camscanner/share/type/ShareQQMiniProgram;
.super Lcom/intsig/camscanner/share/type/BaseShare;
.source "ShareQQMiniProgram.java"


# direct methods
.method private O0O8OO088(Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;Ljava/lang/String;)V
    .locals 8

    .line 1
    :try_start_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o88o88()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    sget-object p1, Lcom/intsig/camscanner/share/type/BaseShare;->〇00:Ljava/lang/String;

    .line 12
    .line 13
    const-string p2, "wxAppString == null"

    .line 14
    .line 15
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    new-instance v1, Lcom/intsig/tsapp/sync/AppConfigJson$WxApp;

    .line 20
    .line 21
    invoke-direct {v1, v0}, Lcom/intsig/tsapp/sync/AppConfigJson$WxApp;-><init>(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;->O8()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-eqz v2, :cond_1

    .line 33
    .line 34
    sget-object p1, Lcom/intsig/camscanner/share/type/BaseShare;->〇00:Ljava/lang/String;

    .line 35
    .line 36
    const-string p2, "webPageUrl == null"

    .line 37
    .line 38
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    return-void

    .line 42
    :cond_1
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/share/type/BaseShare;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v4

    .line 46
    invoke-virtual {p1}, Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;->〇80〇808〇O()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    if-eqz v2, :cond_2

    .line 55
    .line 56
    sget-object p1, Lcom/intsig/camscanner/share/type/BaseShare;->〇00:Ljava/lang/String;

    .line 57
    .line 58
    const-string p2, "sid == null"

    .line 59
    .line 60
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    return-void

    .line 64
    :cond_2
    new-instance v2, Lcom/intsig/tianshu/ParamsBuilder;

    .line 65
    .line 66
    invoke-direct {v2}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    const-string v3, "sid"

    .line 70
    .line 71
    invoke-virtual {v2, v3, v0}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {p1}, Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;->Oo08()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 79
    .line 80
    .line 81
    move-result v3

    .line 82
    const/4 v5, 0x1

    .line 83
    if-nez v3, :cond_4

    .line 84
    .line 85
    invoke-virtual {p1}, Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;->〇8o8o〇()I

    .line 86
    .line 87
    .line 88
    move-result v3

    .line 89
    if-ne v3, v5, :cond_3

    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_3
    const-string v3, "encrypt_id"

    .line 93
    .line 94
    invoke-virtual {v2, v3, v0}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 95
    .line 96
    .line 97
    goto :goto_1

    .line 98
    :cond_4
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/share/type/BaseShare;->〇00:Ljava/lang/String;

    .line 99
    .line 100
    const-string v3, "encrypt_id == null"

    .line 101
    .line 102
    invoke-static {v0, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    const-string v0, "device_id"

    .line 106
    .line 107
    invoke-virtual {p1}, Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;->〇o00〇〇Oo()Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v3

    .line 111
    invoke-virtual {v2, v0, v3}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 112
    .line 113
    .line 114
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;->Oooo8o0〇()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    if-nez v0, :cond_6

    .line 123
    .line 124
    invoke-virtual {p1}, Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;->〇O〇()Z

    .line 125
    .line 126
    .line 127
    move-result v0

    .line 128
    if-eqz v0, :cond_5

    .line 129
    .line 130
    const/4 v5, 0x2

    .line 131
    :cond_5
    const-string v0, "area"

    .line 132
    .line 133
    invoke-virtual {v2, v0, v5}, Lcom/intsig/tianshu/ParamsBuilder;->〇80〇808〇O(Ljava/lang/String;I)Lcom/intsig/tianshu/ParamsBuilder;

    .line 134
    .line 135
    .line 136
    :cond_6
    iget-object v0, v1, Lcom/intsig/tsapp/sync/AppConfigJson$WxApp;->share_doc:Lcom/intsig/tsapp/sync/AppConfigJson$ShareDoc;

    .line 137
    .line 138
    iget-object v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ShareDoc;->path:Ljava/lang/String;

    .line 139
    .line 140
    invoke-virtual {v2, v0}, Lcom/intsig/tianshu/ParamsBuilder;->o〇0(Ljava/lang/String;)Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object v5

    .line 144
    sget-object v0, Lcom/intsig/camscanner/share/type/BaseShare;->〇00:Ljava/lang/String;

    .line 145
    .line 146
    new-instance v1, Ljava/lang/StringBuilder;

    .line 147
    .line 148
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 149
    .line 150
    .line 151
    const-string v2, " path "

    .line 152
    .line 153
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    .line 155
    .line 156
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    .line 158
    .line 159
    const-string v2, " webPageUrl "

    .line 160
    .line 161
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    .line 163
    .line 164
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    .line 166
    .line 167
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 168
    .line 169
    .line 170
    move-result-object v1

    .line 171
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    .line 173
    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    .line 175
    .line 176
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 177
    .line 178
    .line 179
    iget-object v1, p0, Lcom/intsig/camscanner/share/type/BaseShare;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentActivity;

    .line 180
    .line 181
    const v2, 0x7f13046d

    .line 182
    .line 183
    .line 184
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 185
    .line 186
    .line 187
    move-result-object v1

    .line 188
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    .line 190
    .line 191
    const-string v1, ": "

    .line 192
    .line 193
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    .line 195
    .line 196
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    .line 198
    .line 199
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 200
    .line 201
    .line 202
    move-result-object v1

    .line 203
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇0〇O0088o()Z

    .line 204
    .line 205
    .line 206
    move-result v6

    .line 207
    iget-object v0, p0, Lcom/intsig/camscanner/share/type/BaseShare;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentActivity;

    .line 208
    .line 209
    invoke-virtual {p1}, Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;->〇〇888()Ljava/lang/String;

    .line 210
    .line 211
    .line 212
    move-result-object v3

    .line 213
    new-instance v7, Lcom/intsig/camscanner/share/type/ShareQQMiniProgram$2;

    .line 214
    .line 215
    invoke-direct {v7, p0}, Lcom/intsig/camscanner/share/type/ShareQQMiniProgram$2;-><init>(Lcom/intsig/camscanner/share/type/ShareQQMiniProgram;)V

    .line 216
    .line 217
    .line 218
    move-object v2, p2

    .line 219
    invoke-static/range {v0 .. v7}, Lcom/intsig/share/QQShareHelper;->O8(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/intsig/share/QQShareHelper$QQShareListener;)V

    .line 220
    .line 221
    .line 222
    iget-object p1, p0, Lcom/intsig/camscanner/share/type/BaseShare;->O8:Lcom/intsig/camscanner/share/listener/BaseShareListener;

    .line 223
    .line 224
    if-eqz p1, :cond_7

    .line 225
    .line 226
    const/4 p2, 0x0

    .line 227
    invoke-interface {p1, p2}, Lcom/intsig/camscanner/share/listener/BaseShareListener;->〇80〇808〇O(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    .line 229
    .line 230
    goto :goto_2

    .line 231
    :catchall_0
    move-exception p1

    .line 232
    sget-object p2, Lcom/intsig/camscanner/share/type/BaseShare;->〇00:Ljava/lang/String;

    .line 233
    .line 234
    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    .line 235
    .line 236
    .line 237
    move-result-object p1

    .line 238
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    .line 240
    .line 241
    :cond_7
    :goto_2
    return-void
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method static bridge synthetic O8O〇(Lcom/intsig/camscanner/share/type/ShareQQMiniProgram;Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/share/type/ShareQQMiniProgram;->O0O8OO088(Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public o8()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇O8〇〇o()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/type/BaseShare;->OO0o〇〇:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/share/type/BaseShare;->OO0o〇〇:Ljava/lang/String;

    .line 10
    .line 11
    return-object v0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/share/type/BaseShare;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentActivity;

    .line 13
    .line 14
    const v1, 0x7f1305e9

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    return-object v0
.end method

.method public 〇080()Landroid/content/Intent;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇80〇808〇O()Lcom/intsig/camscanner/share/LinkPanelShareType;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/LinkPanelShareType;->LINK_SHARE_GRID_ITEM:Lcom/intsig/camscanner/share/LinkPanelShareType;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O00()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o0O0O8(Lcom/intsig/camscanner/share/listener/BaseShareListener;)V
    .locals 11

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/type/BaseShare;->O8:Lcom/intsig/camscanner/share/listener/BaseShareListener;

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/share/type/BaseShare;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/share/type/BaseShare;->〇80〇808〇O:Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-static {p1, v0}, Lcom/intsig/camscanner/db/dao/ImageDao;->o0O0(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 8
    .line 9
    .line 10
    move-result-object v4

    .line 11
    sget-object p1, Lcom/intsig/camscanner/share/type/BaseShare;->〇00:Ljava/lang/String;

    .line 12
    .line 13
    const-string v0, "share to QQ mini  "

    .line 14
    .line 15
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/type/BaseShare;->o800o8O()Lcom/intsig/camscanner/share/ShareLinkLogger;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    if-eqz p1, :cond_0

    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/ShareLinkLogger;->〇0〇O0088o()V

    .line 25
    .line 26
    .line 27
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 28
    .line 29
    .line 30
    move-result-wide v0

    .line 31
    new-instance v10, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;

    .line 32
    .line 33
    iget-object v2, p0, Lcom/intsig/camscanner/share/type/BaseShare;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentActivity;

    .line 34
    .line 35
    iget-object v3, p0, Lcom/intsig/camscanner/share/type/BaseShare;->〇080:Ljava/util/ArrayList;

    .line 36
    .line 37
    const/4 v5, 0x0

    .line 38
    const-wide/16 v6, -0x1

    .line 39
    .line 40
    const/4 v8, 0x2

    .line 41
    new-instance v9, Lcom/intsig/camscanner/share/type/ShareQQMiniProgram$1;

    .line 42
    .line 43
    invoke-direct {v9, p0, v0, v1, p1}, Lcom/intsig/camscanner/share/type/ShareQQMiniProgram$1;-><init>(Lcom/intsig/camscanner/share/type/ShareQQMiniProgram;JLcom/intsig/camscanner/share/ShareLinkLogger;)V

    .line 44
    .line 45
    .line 46
    move-object v1, v10

    .line 47
    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;JILcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask$OnResultListener;)V

    .line 48
    .line 49
    .line 50
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    const/4 v0, 0x0

    .line 55
    new-array v0, v0, [Ljava/util/ArrayList;

    .line 56
    .line 57
    invoke-virtual {v10, p1, v0}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇oo〇()Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇8O0〇8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/share/type/BaseShare;->〇8o8o〇:I

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return v0

    .line 6
    :cond_0
    const v0, 0x7f080c3d

    .line 7
    .line 8
    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;
.super Lcom/intsig/app/BaseDialogFragment;
.source "ShareLinkCloudDocSyncDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic oOo〇8o008:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final 〇0O:Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/intsig/camscanner/share/bean/PdfLinkShareTrackData;

.field private o〇00O:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇080OO8〇0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/DialogShareLinkCloudDocSyncBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->oOo〇8o008:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->〇0O:Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/app/BaseDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/DialogShareLinkCloudDocSyncBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->〇080OO8〇0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public static final o00〇88〇08()Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->〇0O:Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog$Companion;->〇080()Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o880()Lcom/intsig/camscanner/databinding/DialogShareLinkCloudDocSyncBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->〇080OO8〇0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->oOo〇8o008:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/DialogShareLinkCloudDocSyncBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final O0〇0(Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->〇08O〇00〇o:Lkotlin/jvm/functions/Function0;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public dealClickAction(Landroid/view/View;)V
    .locals 13

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/app/BaseDialogFragment;->dealClickAction(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    if-nez p1, :cond_0

    .line 5
    .line 6
    return-void

    .line 7
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->o880()Lcom/intsig/camscanner/databinding/DialogShareLinkCloudDocSyncBinding;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/4 v1, 0x0

    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogShareLinkCloudDocSyncBinding;->OO:Landroid/widget/Button;

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_1
    move-object v0, v1

    .line 18
    :goto_0
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    const-string v2, "ShareLinkCloudDocSyncDialog"

    .line 23
    .line 24
    const/4 v3, 0x2

    .line 25
    const-string v4, "cs_share"

    .line 26
    .line 27
    const-string v5, "from_part"

    .line 28
    .line 29
    const/4 v6, 0x1

    .line 30
    const-string v7, "share_link"

    .line 31
    .line 32
    const-string v8, "from"

    .line 33
    .line 34
    const/4 v9, 0x0

    .line 35
    const-string v10, "CSCloundAuthorizePop"

    .line 36
    .line 37
    if-eqz v0, :cond_5

    .line 38
    .line 39
    const-string p1, "click sync"

    .line 40
    .line 41
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    iget-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->O8o08O8O:Lcom/intsig/camscanner/share/bean/PdfLinkShareTrackData;

    .line 45
    .line 46
    const-string v0, "open"

    .line 47
    .line 48
    if-eqz p1, :cond_2

    .line 49
    .line 50
    const/4 v1, 0x4

    .line 51
    new-array v1, v1, [Landroid/util/Pair;

    .line 52
    .line 53
    new-instance v2, Landroid/util/Pair;

    .line 54
    .line 55
    invoke-direct {v2, v8, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 56
    .line 57
    .line 58
    aput-object v2, v1, v9

    .line 59
    .line 60
    new-instance v2, Landroid/util/Pair;

    .line 61
    .line 62
    invoke-direct {v2, v5, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 63
    .line 64
    .line 65
    aput-object v2, v1, v6

    .line 66
    .line 67
    new-instance v2, Landroid/util/Pair;

    .line 68
    .line 69
    const-string v11, "doc_scheme"

    .line 70
    .line 71
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/bean/PdfLinkShareTrackData;->getDocScheme()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v12

    .line 75
    invoke-direct {v2, v11, v12}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 76
    .line 77
    .line 78
    aput-object v2, v1, v3

    .line 79
    .line 80
    new-instance v2, Landroid/util/Pair;

    .line 81
    .line 82
    const-string v11, "doc_num"

    .line 83
    .line 84
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/bean/PdfLinkShareTrackData;->getDocNum()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    invoke-direct {v2, v11, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 89
    .line 90
    .line 91
    const/4 p1, 0x3

    .line 92
    aput-object v2, v1, p1

    .line 93
    .line 94
    invoke-static {v10, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 95
    .line 96
    .line 97
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 98
    .line 99
    :cond_2
    if-nez v1, :cond_3

    .line 100
    .line 101
    new-array p1, v3, [Landroid/util/Pair;

    .line 102
    .line 103
    new-instance v1, Landroid/util/Pair;

    .line 104
    .line 105
    invoke-direct {v1, v8, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 106
    .line 107
    .line 108
    aput-object v1, p1, v9

    .line 109
    .line 110
    new-instance v1, Landroid/util/Pair;

    .line 111
    .line 112
    invoke-direct {v1, v5, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 113
    .line 114
    .line 115
    aput-object v1, p1, v6

    .line 116
    .line 117
    invoke-static {v10, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 118
    .line 119
    .line 120
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/ipo/IPOCheck;->O〇8O8〇008()V

    .line 121
    .line 122
    .line 123
    invoke-static {}, Lcom/intsig/camscanner/ipo/IPOCheck;->O8ooOoo〇()V

    .line 124
    .line 125
    .line 126
    iget-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->〇08O〇00〇o:Lkotlin/jvm/functions/Function0;

    .line 127
    .line 128
    if-eqz p1, :cond_4

    .line 129
    .line 130
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 131
    .line 132
    .line 133
    :cond_4
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 134
    .line 135
    .line 136
    goto :goto_1

    .line 137
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->o880()Lcom/intsig/camscanner/databinding/DialogShareLinkCloudDocSyncBinding;

    .line 138
    .line 139
    .line 140
    move-result-object v0

    .line 141
    if-eqz v0, :cond_6

    .line 142
    .line 143
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogShareLinkCloudDocSyncBinding;->〇OOo8〇0:Landroid/widget/Button;

    .line 144
    .line 145
    :cond_6
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 146
    .line 147
    .line 148
    move-result p1

    .line 149
    if-eqz p1, :cond_8

    .line 150
    .line 151
    const-string p1, "click cancel"

    .line 152
    .line 153
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    .line 155
    .line 156
    new-array p1, v3, [Landroid/util/Pair;

    .line 157
    .line 158
    new-instance v0, Landroid/util/Pair;

    .line 159
    .line 160
    invoke-direct {v0, v8, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 161
    .line 162
    .line 163
    aput-object v0, p1, v9

    .line 164
    .line 165
    new-instance v0, Landroid/util/Pair;

    .line 166
    .line 167
    invoke-direct {v0, v5, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 168
    .line 169
    .line 170
    aput-object v0, p1, v6

    .line 171
    .line 172
    const-string v0, "cancel"

    .line 173
    .line 174
    invoke-static {v10, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 175
    .line 176
    .line 177
    iget-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->o〇00O:Lkotlin/jvm/functions/Function0;

    .line 178
    .line 179
    if-eqz p1, :cond_7

    .line 180
    .line 181
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 182
    .line 183
    .line 184
    :cond_7
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 185
    .line 186
    .line 187
    :cond_8
    :goto_1
    return-void
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method protected init(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const/4 v0, 0x0

    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 15
    .line 16
    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    const/16 p1, 0x122

    .line 23
    .line 24
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    float-to-int p1, p1

    .line 29
    invoke-virtual {p0, p1}, Lcom/intsig/app/BaseDialogFragment;->oO〇oo(I)V

    .line 30
    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->o880()Lcom/intsig/camscanner/databinding/DialogShareLinkCloudDocSyncBinding;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    if-eqz p1, :cond_1

    .line 37
    .line 38
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogShareLinkCloudDocSyncBinding;->o〇00O:Lcom/google/android/material/imageview/ShapeableImageView;

    .line 39
    .line 40
    if-eqz p1, :cond_1

    .line 41
    .line 42
    new-instance v1, Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;

    .line 43
    .line 44
    invoke-direct {v1}, Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;-><init>()V

    .line 45
    .line 46
    .line 47
    const/4 v2, 0x4

    .line 48
    invoke-static {v2}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 49
    .line 50
    .line 51
    move-result v3

    .line 52
    invoke-virtual {v1, v0, v3}, Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;->setTopLeftCorner(IF)Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    invoke-static {v2}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    invoke-virtual {v1, v0, v2}, Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;->setTopRightCorner(IF)Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    invoke-virtual {v1}, Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;->build()Lcom/google/android/material/shape/ShapeAppearanceModel;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    invoke-virtual {p1, v1}, Lcom/google/android/material/imageview/ShapeableImageView;->setShapeAppearanceModel(Lcom/google/android/material/shape/ShapeAppearanceModel;)V

    .line 69
    .line 70
    .line 71
    :cond_1
    const/4 p1, 0x2

    .line 72
    new-array p1, p1, [Landroid/view/View;

    .line 73
    .line 74
    invoke-direct {p0}, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->o880()Lcom/intsig/camscanner/databinding/DialogShareLinkCloudDocSyncBinding;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    const/4 v2, 0x0

    .line 79
    if-eqz v1, :cond_2

    .line 80
    .line 81
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/DialogShareLinkCloudDocSyncBinding;->〇OOo8〇0:Landroid/widget/Button;

    .line 82
    .line 83
    goto :goto_0

    .line 84
    :cond_2
    move-object v1, v2

    .line 85
    :goto_0
    aput-object v1, p1, v0

    .line 86
    .line 87
    invoke-direct {p0}, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->o880()Lcom/intsig/camscanner/databinding/DialogShareLinkCloudDocSyncBinding;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    if-eqz v0, :cond_3

    .line 92
    .line 93
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/DialogShareLinkCloudDocSyncBinding;->OO:Landroid/widget/Button;

    .line 94
    .line 95
    :cond_3
    const/4 v0, 0x1

    .line 96
    aput-object v2, p1, v0

    .line 97
    .line 98
    invoke-virtual {p0, p1}, Lcom/intsig/app/BaseDialogFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 99
    .line 100
    .line 101
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public onDestroyView()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/app/BaseDialogFragment;->onDestroyView()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->〇08O〇00〇o:Lkotlin/jvm/functions/Function0;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onStart()V
    .locals 7

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->O8o08O8O:Lcom/intsig/camscanner/share/bean/PdfLinkShareTrackData;

    .line 5
    .line 6
    const-string v1, "cs_share"

    .line 7
    .line 8
    const-string v2, "from_part"

    .line 9
    .line 10
    const-string v3, "share_link"

    .line 11
    .line 12
    const-string v4, "from"

    .line 13
    .line 14
    const-string v5, "CSCloundAuthorizePop"

    .line 15
    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    :try_start_0
    sget-object v6, Lkotlin/Result;->Companion:Lkotlin/Result$Companion;

    .line 19
    .line 20
    new-instance v6, Lorg/json/JSONObject;

    .line 21
    .line 22
    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v6, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v6, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 29
    .line 30
    .line 31
    const-string v1, "doc_scheme"

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/bean/PdfLinkShareTrackData;->getDocScheme()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    invoke-virtual {v6, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 38
    .line 39
    .line 40
    const-string v1, "doc_num"

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/bean/PdfLinkShareTrackData;->getDocNum()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-virtual {v6, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 47
    .line 48
    .line 49
    invoke-static {v5, v6}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 50
    .line 51
    .line 52
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 53
    .line 54
    invoke-static {v0}, Lkotlin/Result;->constructor-impl(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    .line 56
    .line 57
    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    goto :goto_0

    .line 59
    :catchall_0
    move-exception v0

    .line 60
    sget-object v1, Lkotlin/Result;->Companion:Lkotlin/Result$Companion;

    .line 61
    .line 62
    invoke-static {v0}, Lkotlin/ResultKt;->〇080(Ljava/lang/Throwable;)Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-static {v0}, Lkotlin/Result;->constructor-impl(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    :goto_0
    invoke-static {v0}, Lkotlin/Result;->box-impl(Ljava/lang/Object;)Lkotlin/Result;

    .line 71
    .line 72
    .line 73
    goto :goto_1

    .line 74
    :cond_0
    invoke-static {v5, v4, v3, v2, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    :goto_1
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d022c

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8〇OOoooo(Lcom/intsig/camscanner/share/bean/PdfLinkShareTrackData;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->O8o08O8O:Lcom/intsig/camscanner/share/bean/PdfLinkShareTrackData;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇〇o0〇8(Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkCloudDocSyncDialog;->o〇00O:Lkotlin/jvm/functions/Function0;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

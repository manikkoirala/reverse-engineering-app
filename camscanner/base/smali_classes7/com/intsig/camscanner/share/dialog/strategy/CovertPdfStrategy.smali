.class public final Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;
.super Ljava/lang/Object;
.source "CovertPdfStrategy.kt"

# interfaces
.implements Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8:Landroid/os/Handler;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo08:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;

.field private final o〇0:Ljava/lang/Runnable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080:Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/share/ShareLinkLogger;

.field private 〇o〇:Lcom/intsig/app/BaseProgressDialog;


# direct methods
.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareLinkLogger;)V
    .locals 1
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mActivity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 10
    .line 11
    iput-object p2, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->〇o00〇〇Oo:Lcom/intsig/camscanner/share/ShareLinkLogger;

    .line 12
    .line 13
    new-instance p1, Landroid/os/Handler;

    .line 14
    .line 15
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    invoke-direct {p1, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 20
    .line 21
    .line 22
    iput-object p1, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->O8:Landroid/os/Handler;

    .line 23
    .line 24
    new-instance p1, LO0oO/〇o〇;

    .line 25
    .line 26
    invoke-direct {p1, p0}, LO0oO/〇o〇;-><init>(Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;)V

    .line 27
    .line 28
    .line 29
    iput-object p1, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->o〇0:Ljava/lang/Runnable;

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static final 〇80〇808〇O(Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->dismiss()V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->〇o00〇〇Oo:Lcom/intsig/camscanner/share/ShareLinkLogger;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const-string v1, "link_loading_auto"

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/ShareLinkLogger;->OoO8(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    :cond_0
    iget-object p0, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->Oo08:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;

    .line 19
    .line 20
    if-eqz p0, :cond_1

    .line 21
    .line 22
    invoke-interface {p0}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;->〇080()V

    .line 23
    .line 24
    .line 25
    :cond_1
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static synthetic 〇〇888(Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->〇80〇808〇O(Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public O8(Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "shareLoadingListener"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->Oo08:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public Oo08(II)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$DefaultImpls;->O8(Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public dismiss()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->O8:Landroid/os/Handler;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 12
    .line 13
    .line 14
    :cond_0
    iput-object v1, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isShowing()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO80()J
    .locals 2

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$DefaultImpls;->〇080(Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;)J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    return-wide v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onDestroy()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->dismiss()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->dismiss()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080()V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$DefaultImpls;->Oo08(Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->dismiss()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/AppUtil;->o〇〇0〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 15
    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    const v2, 0x7f13008a

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 30
    .line 31
    .line 32
    iput-object v0, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->〇o〇:Lcom/intsig/app/BaseProgressDialog;

    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->O8:Landroid/os/Handler;

    .line 35
    .line 36
    iget-object v1, p0, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->o〇0:Ljava/lang/Runnable;

    .line 37
    .line 38
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;->oO80()J

    .line 39
    .line 40
    .line 41
    move-result-wide v2

    .line 42
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇o〇(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$DefaultImpls;->〇o〇(Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

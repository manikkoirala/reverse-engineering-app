.class public final Lcom/intsig/camscanner/share/dialog/SyncDialogClient;
.super Ljava/lang/Object;
.source "SyncDialogClient.kt"

# interfaces
.implements Landroidx/lifecycle/DefaultLifecycleObserver;
.implements Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/share/dialog/SyncDialogClient$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇08O〇00〇o:Lcom/intsig/camscanner/share/dialog/SyncDialogClient$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private OO:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;

.field private final o0:Lcom/intsig/camscanner/share/ShareLinkLogger;

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/share/dialog/SyncDialogClient$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇08O〇00〇o:Lcom/intsig/camscanner/share/dialog/SyncDialogClient$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareLinkLogger;Z)V
    .locals 1
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p2, p0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->o0:Lcom/intsig/camscanner/share/ShareLinkLogger;

    .line 10
    .line 11
    if-eqz p3, :cond_1

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 14
    .line 15
    .line 16
    move-result-object p3

    .line 17
    iget p3, p3, Lcom/intsig/tsapp/sync/AppConfigJson;->share_page_style:I

    .line 18
    .line 19
    const/4 v0, 0x2

    .line 20
    if-lt p3, v0, :cond_1

    .line 21
    .line 22
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 23
    .line 24
    .line 25
    move-result-object p3

    .line 26
    iget p3, p3, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_share_type_switch:I

    .line 27
    .line 28
    const/4 v0, 0x1

    .line 29
    if-ne p3, v0, :cond_0

    .line 30
    .line 31
    new-instance p3, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;

    .line 32
    .line 33
    invoke-direct {p3, p1, p2}, Lcom/intsig/camscanner/share/dialog/strategy/CovertPdfStrategy;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareLinkLogger;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    new-instance p3, Lcom/intsig/camscanner/share/dialog/strategy/SelectStrategy;

    .line 38
    .line 39
    invoke-direct {p3, p1, p2}, Lcom/intsig/camscanner/share/dialog/strategy/SelectStrategy;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareLinkLogger;)V

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    new-instance p3, Lcom/intsig/camscanner/share/dialog/strategy/CancelStrategy;

    .line 44
    .line 45
    invoke-direct {p3, p1, p2}, Lcom/intsig/camscanner/share/dialog/strategy/CancelStrategy;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareLinkLogger;)V

    .line 46
    .line 47
    .line 48
    :goto_0
    iput-object p3, p0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇OOo8〇0:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;

    .line 49
    .line 50
    invoke-interface {p3, p0}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;->O8(Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {p1}, Landroidx/activity/ComponentActivity;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-virtual {p1, p0}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public final O8()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇OOo8〇0:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;->isShowing()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OO0o〇〇〇〇0()V
    .locals 2

    .line 1
    const-string v0, "SyncDialogClient"

    .line 2
    .line 3
    const-string v1, "uploadSuccess: "

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇OOo8〇0:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;->〇080()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oo08()V
    .locals 2

    .line 1
    const-string v0, "SyncDialogClient"

    .line 2
    .line 3
    const-string v1, "requestLinkSuccess: "

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇OOo8〇0:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;->o〇0()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO80(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "msg"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇OOo8〇0:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;

    .line 7
    .line 8
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;->〇o〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onCancel()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->OO:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;->onCancel()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public synthetic onCreate(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->〇080(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onDestroy(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 1
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "owner"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->〇o00〇〇Oo(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 7
    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇OOo8〇0:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;

    .line 10
    .line 11
    invoke-interface {p1}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;->onDestroy()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onDismiss()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->OO:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;->onDismiss()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public synthetic onPause(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->〇o〇(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public synthetic onResume(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->O8(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public synthetic onStart(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->Oo08(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public synthetic onStop(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->o〇0(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final o〇0(Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "shareLoadingListener"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->OO:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇080()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->OO:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;->〇080()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇80〇808〇O(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇OOo8〇0:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;

    .line 2
    .line 3
    invoke-interface {v0, p1, p2}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;->Oo08(II)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public final 〇o00〇〇Oo()V
    .locals 2

    .line 1
    const-string v0, "SyncDialogClient"

    .line 2
    .line 3
    const-string v1, "dismiss: "

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇OOo8〇0:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;->dismiss()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇()V
    .locals 2

    .line 1
    const-string v0, "SyncDialogClient"

    .line 2
    .line 3
    const-string v1, "fail: "

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇OOo8〇0:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;->dismiss()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇888()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇OOo8〇0:Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy;->〇o00〇〇Oo()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

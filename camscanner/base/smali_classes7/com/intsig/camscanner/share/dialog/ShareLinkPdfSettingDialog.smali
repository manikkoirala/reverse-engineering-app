.class public final Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;
.super Lcom/intsig/app/BaseBottomSheetDialogFragment;
.source "ShareLinkPdfSettingDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080OO8〇0:Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic 〇0O:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO:Lcom/intsig/camscanner/share/dialog/PdfSettingView;

.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDelegate;

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;

.field private 〇OOo8〇0:Lcom/intsig/camscanner/share/dialog/LinkSettingView;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇0O:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇080OO8〇0:Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    new-instance v0, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;

    .line 19
    .line 20
    invoke-direct {v0}, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;-><init>()V

    .line 21
    .line 22
    .line 23
    new-instance v1, LoO8o〇08〇/o〇0;

    .line 24
    .line 25
    invoke-direct {v1, p0}, LoO8o〇08〇/o〇0;-><init>(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0, v0, v1}, Landroidx/fragment/app/Fragment;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const-string v1, "registerForActivityResul\u2026_SETTING_DATA))\n        }"

    .line 33
    .line 34
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    iput-object v0, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->O8o08O8O:Landroidx/activity/result/ActivityResultLauncher;

    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static final synthetic O0〇0(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static final Ooo8o(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Landroidx/activity/result/ActivityResult;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    invoke-virtual {p0, v0}, Landroidx/fragment/app/DialogFragment;->setCancelable(Z)V

    .line 8
    .line 9
    .line 10
    iget-object p0, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->OO:Lcom/intsig/camscanner/share/dialog/PdfSettingView;

    .line 11
    .line 12
    if-eqz p0, :cond_1

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    invoke-virtual {p1}, Landroidx/activity/result/ActivityResult;->getData()Landroid/content/Intent;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    if-eqz p1, :cond_0

    .line 21
    .line 22
    const-string v0, "result_pdf_setting_data"

    .line 23
    .line 24
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    check-cast p1, Lcom/intsig/camscanner/settings/data/PdfSettingData;

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    const/4 p1, 0x0

    .line 32
    :goto_0
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/dialog/PdfSettingView;->〇O〇(Lcom/intsig/camscanner/settings/data/PdfSettingData;)V

    .line 33
    .line 34
    .line 35
    :cond_1
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic o00〇88〇08(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;)Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDelegate;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->o〇00O:Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDelegate;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇0〇0(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static final oOoO8OO〇(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->o〇00O:Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDelegate;

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    invoke-interface {p1}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDelegate;->〇080()V

    .line 11
    .line 12
    .line 13
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->〇080:Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->O8()I

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-nez p1, :cond_1

    .line 20
    .line 21
    const-string p1, "link"

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    const-string p1, "pdf"

    .line 25
    .line 26
    :goto_0
    new-instance v0, Lorg/json/JSONObject;

    .line 27
    .line 28
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v1, "scheme"

    .line 32
    .line 33
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;

    .line 38
    .line 39
    if-eqz v0, :cond_2

    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;->〇080()Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    if-eqz v0, :cond_2

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->o〇0()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    goto :goto_1

    .line 52
    :cond_2
    const/4 v0, 0x0

    .line 53
    :goto_1
    const-string v1, "type"

    .line 54
    .line 55
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    const-string v0, "CSShareLinkSetting"

    .line 60
    .line 61
    const-string v1, "link_share"

    .line 62
    .line 63
    invoke-static {v0, v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismiss()V

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static synthetic oOo〇08〇(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->Ooo8o(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->oOoO8OO〇(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇0ooOOo(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final o〇0〇o(Landroid/content/Context;)Lcom/intsig/camscanner/share/dialog/LinkSettingView;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇OOo8〇0:Lcom/intsig/camscanner/share/dialog/LinkSettingView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/share/dialog/LinkSettingView;

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    const/4 v4, 0x0

    .line 9
    const/4 v5, 0x6

    .line 10
    const/4 v6, 0x0

    .line 11
    move-object v1, v0

    .line 12
    move-object v2, p1

    .line 13
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/share/dialog/LinkSettingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇OOo8〇0:Lcom/intsig/camscanner/share/dialog/LinkSettingView;

    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;

    .line 19
    .line 20
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/dialog/LinkSettingView;->setData(Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;)V

    .line 21
    .line 22
    .line 23
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇OOo8〇0:Lcom/intsig/camscanner/share/dialog/LinkSettingView;

    .line 24
    .line 25
    return-object p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private static final o〇O8OO(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->〇080:Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->〇O〇(I)V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇〇〇00(I)V

    .line 13
    .line 14
    .line 15
    new-instance p1, Lorg/json/JSONObject;

    .line 16
    .line 17
    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v0, "from_part"

    .line 21
    .line 22
    const-string v1, "cs_share"

    .line 23
    .line 24
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const-string v0, "from"

    .line 29
    .line 30
    invoke-direct {p0}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇8〇OOoooo()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object p0

    .line 34
    invoke-virtual {p1, v0, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 35
    .line 36
    .line 37
    move-result-object p0

    .line 38
    const-string p1, "CSShareLinkSetting"

    .line 39
    .line 40
    const-string v0, "link"

    .line 41
    .line 42
    invoke-static {p1, v0, p0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final 〇088O()Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇0O:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇0ooOOo(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->o〇00O:Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDelegate;

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    invoke-interface {p1}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDelegate;->onClose()V

    .line 11
    .line 12
    .line 13
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismiss()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static final 〇0〇0(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;->〇o〇()Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 p1, 0x0

    .line 17
    :goto_0
    const/4 v1, 0x1

    .line 18
    if-nez p1, :cond_3

    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;

    .line 21
    .line 22
    if-eqz p1, :cond_1

    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;->〇080()Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    goto :goto_1

    .line 29
    :cond_1
    const/4 p1, 0x0

    .line 30
    :goto_1
    if-eqz p1, :cond_2

    .line 31
    .line 32
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    new-array v1, v1, [Ljava/lang/Object;

    .line 37
    .line 38
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->Oooo8o0〇()I

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    aput-object p1, v1, v0

    .line 47
    .line 48
    const p1, 0x7f131714

    .line 49
    .line 50
    .line 51
    invoke-virtual {p0, p1, v1}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object p0

    .line 55
    invoke-static {v2, p0}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    :cond_2
    return-void

    .line 59
    :cond_3
    sget-object p1, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->〇080:Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;

    .line 60
    .line 61
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->〇O〇(I)V

    .line 62
    .line 63
    .line 64
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇〇〇00(I)V

    .line 65
    .line 66
    .line 67
    new-instance p1, Lorg/json/JSONObject;

    .line 68
    .line 69
    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    .line 70
    .line 71
    .line 72
    const-string v0, "from_part"

    .line 73
    .line 74
    const-string v1, "cs_share"

    .line 75
    .line 76
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    const-string v0, "from"

    .line 81
    .line 82
    invoke-direct {p0}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇8〇OOoooo()Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object p0

    .line 86
    invoke-virtual {p1, v0, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 87
    .line 88
    .line 89
    move-result-object p0

    .line 90
    const-string p1, "CSShareLinkSetting"

    .line 91
    .line 92
    const-string v0, "pdf"

    .line 93
    .line 94
    invoke-static {p1, v0, p0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 95
    .line 96
    .line 97
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private final 〇8〇80o()V
    .locals 10

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇088O()Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    if-nez v1, :cond_1

    .line 13
    .line 14
    return-void

    .line 15
    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    const/4 v3, 0x0

    .line 20
    if-eqz v2, :cond_2

    .line 21
    .line 22
    const v4, 0x7f0a0512

    .line 23
    .line 24
    .line 25
    invoke-virtual {v2, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    goto :goto_0

    .line 30
    :cond_2
    move-object v2, v3

    .line 31
    :goto_0
    if-nez v2, :cond_3

    .line 32
    .line 33
    return-void

    .line 34
    :cond_3
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 35
    .line 36
    .line 37
    move-result-object v4

    .line 38
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 39
    .line 40
    .line 41
    move-result v5

    .line 42
    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 43
    .line 44
    invoke-static {v2}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->from(Landroid/view/View;)Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    const-string v4, "from(bottomSheet)"

    .line 49
    .line 50
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    const/4 v4, 0x1

    .line 54
    invoke-virtual {v2, v4}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setSkipCollapsed(Z)V

    .line 55
    .line 56
    .line 57
    new-instance v5, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog$initView$1;

    .line 58
    .line 59
    invoke-direct {v5, p0}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog$initView$1;-><init>(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v2, v5}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->addBottomSheetCallback(Lcom/google/android/material/bottomsheet/BottomSheetBehavior$BottomSheetCallback;)V

    .line 63
    .line 64
    .line 65
    const/high16 v2, 0x41400000    # 12.0f

    .line 66
    .line 67
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 68
    .line 69
    .line 70
    move-result v2

    .line 71
    int-to-float v2, v2

    .line 72
    invoke-virtual {v1}, Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 73
    .line 74
    .line 75
    move-result-object v5

    .line 76
    const-string v6, "vb.root"

    .line 77
    .line 78
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    const v6, 0x7f060209

    .line 82
    .line 83
    .line 84
    invoke-static {v0, v6}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 85
    .line 86
    .line 87
    move-result v0

    .line 88
    new-instance v6, Landroid/graphics/drawable/GradientDrawable;

    .line 89
    .line 90
    invoke-direct {v6}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 91
    .line 92
    .line 93
    invoke-virtual {v6, v0}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 94
    .line 95
    .line 96
    const/16 v0, 0x8

    .line 97
    .line 98
    new-array v0, v0, [Ljava/lang/Float;

    .line 99
    .line 100
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 101
    .line 102
    .line 103
    move-result-object v7

    .line 104
    const/4 v8, 0x0

    .line 105
    aput-object v7, v0, v8

    .line 106
    .line 107
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 108
    .line 109
    .line 110
    move-result-object v7

    .line 111
    aput-object v7, v0, v4

    .line 112
    .line 113
    const/4 v7, 0x2

    .line 114
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 115
    .line 116
    .line 117
    move-result-object v9

    .line 118
    aput-object v9, v0, v7

    .line 119
    .line 120
    const/4 v7, 0x3

    .line 121
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 122
    .line 123
    .line 124
    move-result-object v2

    .line 125
    aput-object v2, v0, v7

    .line 126
    .line 127
    const/4 v2, 0x4

    .line 128
    const/4 v7, 0x0

    .line 129
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 130
    .line 131
    .line 132
    move-result-object v9

    .line 133
    aput-object v9, v0, v2

    .line 134
    .line 135
    const/4 v2, 0x5

    .line 136
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 137
    .line 138
    .line 139
    move-result-object v9

    .line 140
    aput-object v9, v0, v2

    .line 141
    .line 142
    const/4 v2, 0x6

    .line 143
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 144
    .line 145
    .line 146
    move-result-object v9

    .line 147
    aput-object v9, v0, v2

    .line 148
    .line 149
    const/4 v2, 0x7

    .line 150
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 151
    .line 152
    .line 153
    move-result-object v7

    .line 154
    aput-object v7, v0, v2

    .line 155
    .line 156
    invoke-static {v0}, Lkotlin/collections/ArraysKt;->o〇O([Ljava/lang/Float;)[F

    .line 157
    .line 158
    .line 159
    move-result-object v0

    .line 160
    invoke-virtual {v6, v0}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadii([F)V

    .line 161
    .line 162
    .line 163
    invoke-virtual {v5, v6}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 164
    .line 165
    .line 166
    sget-object v0, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->〇080:Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;

    .line 167
    .line 168
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->O8()I

    .line 169
    .line 170
    .line 171
    move-result v0

    .line 172
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇〇〇00(I)V

    .line 173
    .line 174
    .line 175
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 176
    .line 177
    .line 178
    move-result v0

    .line 179
    if-nez v0, :cond_5

    .line 180
    .line 181
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇8o8O〇O()Z

    .line 182
    .line 183
    .line 184
    move-result v0

    .line 185
    if-eqz v0, :cond_4

    .line 186
    .line 187
    goto :goto_1

    .line 188
    :cond_4
    const/4 v0, 0x0

    .line 189
    goto :goto_2

    .line 190
    :cond_5
    :goto_1
    const/4 v0, 0x1

    .line 191
    :goto_2
    if-nez v0, :cond_6

    .line 192
    .line 193
    iget-object v0, v1, Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;->oOo0:Lcom/intsig/camscanner/share/widget/ShareTypeSelectView;

    .line 194
    .line 195
    const v2, 0x7f1316d0

    .line 196
    .line 197
    .line 198
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object v2

    .line 202
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/widget/ShareTypeSelectView;->setDesc(Ljava/lang/String;)V

    .line 203
    .line 204
    .line 205
    :cond_6
    iget-object v0, v1, Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;->oOo〇8o008:Lcom/intsig/camscanner/share/widget/ShareTypeSelectView;

    .line 206
    .line 207
    new-instance v2, LoO8o〇08〇/〇〇888;

    .line 208
    .line 209
    invoke-direct {v2, p0}, LoO8o〇08〇/〇〇888;-><init>(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;)V

    .line 210
    .line 211
    .line 212
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    .line 214
    .line 215
    iget-object v0, v1, Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;->oOo0:Lcom/intsig/camscanner/share/widget/ShareTypeSelectView;

    .line 216
    .line 217
    new-instance v2, LoO8o〇08〇/oO80;

    .line 218
    .line 219
    invoke-direct {v2, p0}, LoO8o〇08〇/oO80;-><init>(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;)V

    .line 220
    .line 221
    .line 222
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    .line 224
    .line 225
    iget-object v0, v1, Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 226
    .line 227
    new-instance v2, LoO8o〇08〇/〇80〇808〇O;

    .line 228
    .line 229
    invoke-direct {v2, p0}, LoO8o〇08〇/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;)V

    .line 230
    .line 231
    .line 232
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    .line 234
    .line 235
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;

    .line 236
    .line 237
    if-eqz v0, :cond_7

    .line 238
    .line 239
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;->〇080()Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 240
    .line 241
    .line 242
    move-result-object v3

    .line 243
    :cond_7
    iget-object v0, v1, Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;->OO:Landroidx/constraintlayout/widget/Group;

    .line 244
    .line 245
    const-string v2, "vb.groupBottom"

    .line 246
    .line 247
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    .line 249
    .line 250
    if-eqz v3, :cond_8

    .line 251
    .line 252
    const/4 v2, 0x1

    .line 253
    goto :goto_3

    .line 254
    :cond_8
    const/4 v2, 0x0

    .line 255
    :goto_3
    invoke-static {v0, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 256
    .line 257
    .line 258
    if-eqz v3, :cond_9

    .line 259
    .line 260
    iget-object v0, v1, Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 261
    .line 262
    new-array v2, v4, [Ljava/lang/Object;

    .line 263
    .line 264
    invoke-virtual {v3}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->Oooo8o0〇()I

    .line 265
    .line 266
    .line 267
    move-result v3

    .line 268
    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 269
    .line 270
    .line 271
    move-result-object v3

    .line 272
    aput-object v3, v2, v8

    .line 273
    .line 274
    const v3, 0x7f131712

    .line 275
    .line 276
    .line 277
    invoke-virtual {p0, v3, v2}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 278
    .line 279
    .line 280
    move-result-object v2

    .line 281
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    .line 283
    .line 284
    :cond_9
    iget-object v0, v1, Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 285
    .line 286
    new-instance v1, LoO8o〇08〇/OO0o〇〇〇〇0;

    .line 287
    .line 288
    invoke-direct {v1, p0}, LoO8o〇08〇/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;)V

    .line 289
    .line 290
    .line 291
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 292
    .line 293
    .line 294
    return-void
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private final 〇8〇OOoooo()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;->〇080()Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->o〇0()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    :cond_0
    const-string v0, "link_setting"

    .line 18
    .line 19
    :cond_1
    return-object v0
    .line 20
    .line 21
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->o〇O8OO(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final 〇o〇88〇8(Landroid/content/Context;)Lcom/intsig/camscanner/share/dialog/PdfSettingView;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->OO:Lcom/intsig/camscanner/share/dialog/PdfSettingView;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/share/dialog/PdfSettingView;

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    const/4 v4, 0x0

    .line 9
    const/4 v5, 0x6

    .line 10
    const/4 v6, 0x0

    .line 11
    move-object v1, v0

    .line 12
    move-object v2, p1

    .line 13
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/share/dialog/PdfSettingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->OO:Lcom/intsig/camscanner/share/dialog/PdfSettingView;

    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;

    .line 19
    .line 20
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/dialog/PdfSettingView;->setData(Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->OO:Lcom/intsig/camscanner/share/dialog/PdfSettingView;

    .line 24
    .line 25
    if-nez p1, :cond_0

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog$getPdfSettingView$1;

    .line 29
    .line 30
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog$getPdfSettingView$1;-><init>(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/dialog/PdfSettingView;->setMStartApp(Lkotlin/jvm/functions/Function1;)V

    .line 34
    .line 35
    .line 36
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->OO:Lcom/intsig/camscanner/share/dialog/PdfSettingView;

    .line 37
    .line 38
    return-object p1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static final synthetic 〇〇o0〇8(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;)Landroidx/activity/result/ActivityResultLauncher;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->O8o08O8O:Landroidx/activity/result/ActivityResultLauncher;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final 〇〇〇00(I)V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇088O()Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    if-nez v1, :cond_1

    .line 13
    .line 14
    return-void

    .line 15
    :cond_1
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;->〇OOo8〇0:Landroid/widget/FrameLayout;

    .line 16
    .line 17
    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 18
    .line 19
    .line 20
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;->oOo〇8o008:Lcom/intsig/camscanner/share/widget/ShareTypeSelectView;

    .line 21
    .line 22
    const/4 v3, 0x0

    .line 23
    const/4 v4, 0x1

    .line 24
    if-nez p1, :cond_2

    .line 25
    .line 26
    const/4 v5, 0x1

    .line 27
    goto :goto_0

    .line 28
    :cond_2
    const/4 v5, 0x0

    .line 29
    :goto_0
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/share/widget/ShareTypeSelectView;->setMSelect(Z)V

    .line 30
    .line 31
    .line 32
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;->oOo0:Lcom/intsig/camscanner/share/widget/ShareTypeSelectView;

    .line 33
    .line 34
    if-ne p1, v4, :cond_3

    .line 35
    .line 36
    const/4 v3, 0x1

    .line 37
    :cond_3
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/share/widget/ShareTypeSelectView;->setMSelect(Z)V

    .line 38
    .line 39
    .line 40
    if-nez p1, :cond_4

    .line 41
    .line 42
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->o〇0〇o(Landroid/content/Context;)Lcom/intsig/camscanner/share/dialog/LinkSettingView;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    if-eqz p1, :cond_5

    .line 47
    .line 48
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;->〇OOo8〇0:Landroid/widget/FrameLayout;

    .line 49
    .line 50
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 51
    .line 52
    .line 53
    goto :goto_1

    .line 54
    :cond_4
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇o〇88〇8(Landroid/content/Context;)Lcom/intsig/camscanner/share/dialog/PdfSettingView;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    if-eqz p1, :cond_5

    .line 59
    .line 60
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogShareLinkSettingBinding;->〇OOo8〇0:Landroid/widget/FrameLayout;

    .line 61
    .line 62
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 63
    .line 64
    .line 65
    :cond_5
    :goto_1
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method protected getNavigationBarColor()I
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const v1, 0x7f060209

    .line 8
    .line 9
    .line 10
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTheme()I
    .locals 1

    .line 1
    const v0, 0x7f140193

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected init(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/share/bean/ShareLinkPdfSettingData;

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    const-string p1, "ShareLinkSettingDialog"

    .line 6
    .line 7
    const-string v0, "init: reCreate but no data"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismiss()V

    .line 13
    .line 14
    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    invoke-virtual {p0, p1}, Landroidx/fragment/app/DialogFragment;->setShowsDialog(Z)V

    .line 17
    .line 18
    .line 19
    const/4 p1, 0x1

    .line 20
    invoke-virtual {p0, p1}, Landroidx/fragment/app/DialogFragment;->setCancelable(Z)V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇8〇80o()V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected isDefaultExpanded()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onStart()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lorg/json/JSONObject;

    .line 5
    .line 6
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 7
    .line 8
    .line 9
    const-string v1, "from_part"

    .line 10
    .line 11
    const-string v2, "cs_share"

    .line 12
    .line 13
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const-string v1, "from"

    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->〇8〇OOoooo()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const-string v1, "CSShareLinkSetting"

    .line 28
    .line 29
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d022e

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o08(Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDelegate;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDelegate;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "shareLinkPdfSettingDelegate"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDialog;->o〇00O:Lcom/intsig/camscanner/share/dialog/ShareLinkPdfSettingDelegate;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

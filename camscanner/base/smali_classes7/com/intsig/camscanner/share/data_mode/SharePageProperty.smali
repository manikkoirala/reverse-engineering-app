.class public Lcom/intsig/camscanner/share/data_mode/SharePageProperty;
.super Ljava/lang/Object;
.source "SharePageProperty.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/intsig/camscanner/share/data_mode/SharePageProperty;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public O8o08O8O:I

.field public OO:Ljava/lang/String;

.field public o0:Ljava/lang/String;

.field public o〇00O:J

.field public 〇08O〇00〇o:Ljava/lang/String;

.field public 〇OOo8〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/share/data_mode/SharePageProperty$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->O8o08O8O:I

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 4
    iput v0, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->O8o08O8O:I

    .line 5
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->o0:Ljava/lang/String;

    .line 6
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->〇OOo8〇0:I

    .line 7
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->OO:Ljava/lang/String;

    .line 8
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->〇08O〇00〇o:Ljava/lang/String;

    .line 9
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->o〇00O:J

    .line 10
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->O8o08O8O:I

    return-void
.end method

.method public static 〇o00〇〇Oo(Landroid/content/Context;JLjava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/share/data_mode/SharePageProperty;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-static {p1, p2}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 7
    .line 8
    .line 9
    move-result-object v2

    .line 10
    const-string v3, "image_titile"

    .line 11
    .line 12
    const-string v4, "page_num"

    .line 13
    .line 14
    const-string v5, "_data"

    .line 15
    .line 16
    const-string v6, "note"

    .line 17
    .line 18
    const-string v7, "_id"

    .line 19
    .line 20
    const-string v8, "folder_type"

    .line 21
    .line 22
    filled-new-array/range {v3 .. v8}, [Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    if-nez p1, :cond_0

    .line 31
    .line 32
    new-instance p1, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string p2, "_id in "

    .line 38
    .line 39
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    goto :goto_0

    .line 50
    :cond_0
    const/4 p1, 0x0

    .line 51
    :goto_0
    move-object v4, p1

    .line 52
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    const/4 v5, 0x0

    .line 57
    const-string v6, "page_num ASC"

    .line 58
    .line 59
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 60
    .line 61
    .line 62
    move-result-object p0

    .line 63
    if-eqz p0, :cond_2

    .line 64
    .line 65
    :goto_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    .line 66
    .line 67
    .line 68
    move-result p1

    .line 69
    if-eqz p1, :cond_1

    .line 70
    .line 71
    new-instance p1, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;

    .line 72
    .line 73
    invoke-direct {p1}, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;-><init>()V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    .line 78
    .line 79
    const/4 p2, 0x0

    .line 80
    invoke-interface {p0, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object p2

    .line 84
    iput-object p2, p1, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->OO:Ljava/lang/String;

    .line 85
    .line 86
    const/4 p2, 0x1

    .line 87
    invoke-interface {p0, p2}, Landroid/database/Cursor;->getInt(I)I

    .line 88
    .line 89
    .line 90
    move-result p2

    .line 91
    iput p2, p1, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->〇OOo8〇0:I

    .line 92
    .line 93
    const/4 p2, 0x2

    .line 94
    invoke-interface {p0, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object p2

    .line 98
    iput-object p2, p1, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->o0:Ljava/lang/String;

    .line 99
    .line 100
    const/4 p2, 0x3

    .line 101
    invoke-interface {p0, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object p2

    .line 105
    iput-object p2, p1, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->〇08O〇00〇o:Ljava/lang/String;

    .line 106
    .line 107
    const/4 p2, 0x4

    .line 108
    invoke-interface {p0, p2}, Landroid/database/Cursor;->getLong(I)J

    .line 109
    .line 110
    .line 111
    move-result-wide p2

    .line 112
    iput-wide p2, p1, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->o〇00O:J

    .line 113
    .line 114
    const/4 p2, 0x5

    .line 115
    invoke-interface {p0, p2}, Landroid/database/Cursor;->getInt(I)I

    .line 116
    .line 117
    .line 118
    move-result p2

    .line 119
    iput p2, p1, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->O8o08O8O:I

    .line 120
    .line 121
    goto :goto_1

    .line 122
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 123
    .line 124
    .line 125
    :cond_2
    return-object v0
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 1
    iget-object p2, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->o0:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget p2, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->〇OOo8〇0:I

    .line 7
    .line 8
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 9
    .line 10
    .line 11
    iget-object p2, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->OO:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object p2, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->〇08O〇00〇o:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    iget-wide v0, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->o〇00O:J

    .line 22
    .line 23
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 24
    .line 25
    .line 26
    iget p2, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->O8o08O8O:I

    .line 27
    .line 28
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇080()Lcom/intsig/camscanner/share/data_mode/SharePageProperty;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->o0:Ljava/lang/String;

    .line 7
    .line 8
    iput-object v1, v0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->o0:Ljava/lang/String;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->OO:Ljava/lang/String;

    .line 11
    .line 12
    iput-object v1, v0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->OO:Ljava/lang/String;

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->〇08O〇00〇o:Ljava/lang/String;

    .line 15
    .line 16
    iput-object v1, v0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->〇08O〇00〇o:Ljava/lang/String;

    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->〇OOo8〇0:I

    .line 19
    .line 20
    iput v1, v0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->〇OOo8〇0:I

    .line 21
    .line 22
    iget-wide v1, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->o〇00O:J

    .line 23
    .line 24
    iput-wide v1, v0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->o〇00O:J

    .line 25
    .line 26
    iget v1, p0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->O8o08O8O:I

    .line 27
    .line 28
    iput v1, v0, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->O8o08O8O:I

    .line 29
    .line 30
    return-object v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

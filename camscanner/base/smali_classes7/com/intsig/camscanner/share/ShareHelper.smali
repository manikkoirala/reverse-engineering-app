.class public Lcom/intsig/camscanner/share/ShareHelper;
.super Lcom/intsig/utils/activity/ActivityLifeCircleManager$LifeCircleListener;
.source "ShareHelper.java"

# interfaces
.implements Lcom/intsig/camscanner/share/listener/ShareTypeClickListener;
.implements Lcom/intsig/camscanner/share/listener/ShareCompressSelectListener;
.implements Lcom/intsig/camscanner/share/listener/ShareAppOnclickListener;
.implements Lcom/intsig/camscanner/share/listener/BaseShareListener;
.implements Landroidx/fragment/app/FragmentResultListener;
.implements Lcom/intsig/camscanner/share/listener/ShareLinkListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/share/ShareHelper$OnSyncDocUploadListenerImpl;,
        Lcom/intsig/camscanner/share/ShareHelper$ShareType;
    }
.end annotation


# static fields
.field private static Ooo08:Ljava/lang/String; = "ShareHelper"


# instance fields
.field private O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

.field private final O88O:Lcom/intsig/camscanner/share/channel/item/ShareChannelListener;

.field private O8o08O8O:Lcom/intsig/utils/activity/ActivityLifeCircleManager;

.field private OO:Landroidx/fragment/app/FragmentActivity;

.field private OO〇00〇8oO:Z

.field private Oo80:Lcom/intsig/camscanner/share/dialog/SyncDialogClient;

.field private O〇08oOOO0:Ljava/lang/String;

.field private O〇o88o08〇:Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareItemClickListener;

.field private o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

.field private o8o:Lcom/intsig/camscanner/share/ShareOptimization;

.field private o8oOOo:Landroid/content/pm/ActivityInfo;

.field private o8〇OO:Ljava/lang/String;

.field private o8〇OO0〇0o:I

.field private oOO〇〇:I

.field private oOo0:Lcom/intsig/app/BaseProgressDialog;

.field private oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

.field private oo8ooo8O:Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;

.field private ooo0〇〇O:Landroidx/fragment/app/DialogFragment;

.field private o〇00O:Lcom/intsig/camscanner/share/listener/ShareBackListener;

.field private o〇oO:J

.field private 〇00O0:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field private 〇080OO8〇0:Z

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

.field private 〇08〇o0O:Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;

.field private 〇0O:Lcom/intsig/callback/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/callback/Callback<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private 〇8〇oO〇〇8o:Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;

.field private 〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

.field private 〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

.field private 〇o0O:Z

.field private 〇〇08O:Z

.field private 〇〇o〇:Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>(Landroidx/fragment/app/FragmentActivity;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/utils/activity/ActivityLifeCircleManager$LifeCircleListener;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇080OO8〇0:Z

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    iput-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇0O:Lcom/intsig/callback/Callback;

    .line 9
    .line 10
    sget-object v2, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->DEFAULT:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 11
    .line 12
    iput-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 13
    .line 14
    iput-boolean v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO〇00〇8oO:Z

    .line 15
    .line 16
    iput v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO0〇0o:I

    .line 17
    .line 18
    iput-boolean v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇〇08O:Z

    .line 19
    .line 20
    new-instance v2, Lcom/intsig/camscanner/share/ShareHelper$4;

    .line 21
    .line 22
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/share/ShareHelper$4;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 23
    .line 24
    .line 25
    iput-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->O88O:Lcom/intsig/camscanner/share/channel/item/ShareChannelListener;

    .line 26
    .line 27
    iput v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOO〇〇:I

    .line 28
    .line 29
    new-instance v0, Lcom/intsig/camscanner/share/ShareHelper$OnSyncDocUploadListenerImpl;

    .line 30
    .line 31
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/share/ShareHelper$OnSyncDocUploadListenerImpl;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Lo〇o8〇〇O/o〇O;)V

    .line 32
    .line 33
    .line 34
    iput-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->oo8ooo8O:Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;

    .line 35
    .line 36
    const-wide/16 v0, 0x0

    .line 37
    .line 38
    iput-wide v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o〇oO:J

    .line 39
    .line 40
    new-instance v0, Lcom/intsig/camscanner/share/ShareHelper$12;

    .line 41
    .line 42
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper$12;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 43
    .line 44
    .line 45
    iput-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->O〇o88o08〇:Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareItemClickListener;

    .line 46
    .line 47
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 48
    .line 49
    iput-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇00O0:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 50
    .line 51
    const-string v0, ""

    .line 52
    .line 53
    iput-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->O〇08oOOO0:Ljava/lang/String;

    .line 54
    .line 55
    iput-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 56
    .line 57
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 58
    .line 59
    new-instance v0, Lcom/intsig/camscanner/share/ShareUiImplement;

    .line 60
    .line 61
    invoke-direct {v0}, Lcom/intsig/camscanner/share/ShareUiImplement;-><init>()V

    .line 62
    .line 63
    .line 64
    iput-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 65
    .line 66
    new-instance v0, Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 67
    .line 68
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/share/ShareDataPresenter;-><init>(Landroid/content/Context;)V

    .line 69
    .line 70
    .line 71
    iput-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 72
    .line 73
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 74
    .line 75
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareDataPresenter;->o800o8O()Z

    .line 76
    .line 77
    .line 78
    move-result v0

    .line 79
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/share/listener/ShareUiInterface;->Oo08(Z)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 83
    .line 84
    invoke-static {v0}, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->〇〇888(Landroid/app/Activity;)Lcom/intsig/utils/activity/ActivityLifeCircleManager;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    iput-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->O8o08O8O:Lcom/intsig/utils/activity/ActivityLifeCircleManager;

    .line 89
    .line 90
    invoke-virtual {v0, p0}, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->〇o00〇〇Oo(Lcom/intsig/utils/activity/ActivityLifeCircleManager$LifeCircleListener;)Lcom/intsig/utils/activity/ActivityLifeCircleManager;

    .line 91
    .line 92
    .line 93
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 94
    .line 95
    new-instance v1, Ljava/lang/StringBuilder;

    .line 96
    .line 97
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    .line 99
    .line 100
    const-string v2, "share from activity = "

    .line 101
    .line 102
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 106
    .line 107
    .line 108
    move-result-object p1

    .line 109
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object p1

    .line 120
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    return-void
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method static bridge synthetic O0(Lcom/intsig/camscanner/share/ShareHelper;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->o〇(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic O000(Lcom/intsig/camscanner/share/ShareHelper;)Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private synthetic O00O(Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/List;Ljava/util/List;Lcom/intsig/camscanner/share/type/BaseShare;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇〇o〇:Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->O08000(Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 10
    .line 11
    const-string p2, "prioritySync onSyncStopDon listener had removed"

    .line 12
    .line 13
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇〇o〇:Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;

    .line 18
    .line 19
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo〇O(Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;)V

    .line 20
    .line 21
    .line 22
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, p2, v0}, Lcom/intsig/camscanner/share/ShareHelper;->oO〇(Ljava/util/List;Z)Ljava/util/List;

    .line 24
    .line 25
    .line 26
    move-result-object p2

    .line 27
    invoke-interface {p3}, Ljava/util/List;->clear()V

    .line 28
    .line 29
    .line 30
    invoke-interface {p3, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 31
    .line 32
    .line 33
    sget-object v1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 34
    .line 35
    new-instance v2, Ljava/lang/StringBuilder;

    .line 36
    .line 37
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 38
    .line 39
    .line 40
    const-string v3, "prioritySync onSyncStopDone unSyncDocIdList: "

    .line 41
    .line 42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    const-string v3, ","

    .line 46
    .line 47
    invoke-static {v3, p3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v3

    .line 51
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    if-eqz v1, :cond_1

    .line 66
    .line 67
    invoke-virtual {p4}, Lcom/intsig/camscanner/share/type/BaseShare;->Oo8Oo00oo()Z

    .line 68
    .line 69
    .line 70
    move-result p1

    .line 71
    const-string p3, "onSyncStop"

    .line 72
    .line 73
    invoke-direct {p0, v0, p2, p1, p3}, Lcom/intsig/camscanner/share/ShareHelper;->〇〇0〇0o8(ILjava/util/List;ZLjava/lang/String;)V

    .line 74
    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_1
    invoke-virtual {p4}, Lcom/intsig/camscanner/share/type/BaseShare;->Oo8Oo00oo()Z

    .line 78
    .line 79
    .line 80
    move-result p2

    .line 81
    new-instance p4, Lo〇o8〇〇O/〇〇0o;

    .line 82
    .line 83
    invoke-direct {p4, p0, p1}, Lo〇o8〇〇O/〇〇0o;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/tsapp/sync/SyncThread;)V

    .line 84
    .line 85
    .line 86
    invoke-static {p3, p2, p4}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->OO0o〇〇〇〇0(Ljava/util/List;ZLandroidx/core/util/Consumer;)V

    .line 87
    .line 88
    .line 89
    :goto_0
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method static bridge synthetic O08000(Lcom/intsig/camscanner/share/ShareHelper;)Lcom/intsig/camscanner/share/type/BaseShare;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic O0O8OO088(Lcom/intsig/camscanner/share/ShareHelper;JZ)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/share/ShareHelper;->O0oO008(JZ)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private O0OO8〇0(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/intsig/camscanner/share/type/BaseShare;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareNormalLink;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    invoke-direct {v0, v1, p2}, Lcom/intsig/camscanner/share/type/ShareNormalLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 6
    .line 7
    .line 8
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 9
    .line 10
    invoke-static {p2, v0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->o08oOO(Landroid/content/Context;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getTitleLink()I

    .line 14
    .line 15
    .line 16
    move-result p2

    .line 17
    if-eqz p2, :cond_0

    .line 18
    .line 19
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getTitleLink()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p2

    .line 29
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/share/type/BaseShare;->OOO(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getActionStr()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p2

    .line 36
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 37
    .line 38
    .line 39
    move-result p2

    .line 40
    if-nez p2, :cond_1

    .line 41
    .line 42
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getActionStr()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/type/BaseShare;->oO(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    :cond_1
    return-object v0
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private O0o(Ljava/util/ArrayList;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_6

    .line 3
    .line 4
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    if-nez v1, :cond_0

    .line 9
    .line 10
    goto/16 :goto_2

    .line 11
    .line 12
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 13
    .line 14
    .line 15
    move-result-wide v1

    .line 16
    new-instance v3, Ljava/util/HashMap;

    .line 17
    .line 18
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 19
    .line 20
    .line 21
    invoke-static {p1}, Lcom/intsig/camscanner/app/DBUtil;->oO00OOO(Ljava/util/List;)Ljava/util/List;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 26
    .line 27
    .line 28
    move-result-object v4

    .line 29
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 30
    .line 31
    .line 32
    move-result v5

    .line 33
    if-eqz v5, :cond_4

    .line 34
    .line 35
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v5

    .line 39
    check-cast v5, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 40
    .line 41
    if-eqz v5, :cond_5

    .line 42
    .line 43
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->〇80()Z

    .line 44
    .line 45
    .line 46
    move-result v6

    .line 47
    if-nez v6, :cond_5

    .line 48
    .line 49
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->O000()Z

    .line 50
    .line 51
    .line 52
    move-result v6

    .line 53
    if-eqz v6, :cond_2

    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_2
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->Oo8Oo00oo()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v6

    .line 60
    invoke-interface {v3, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    move-result v6

    .line 64
    if-eqz v6, :cond_3

    .line 65
    .line 66
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->Oo8Oo00oo()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v5

    .line 70
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    .line 72
    .line 73
    move-result-object v5

    .line 74
    check-cast v5, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_3
    iget-object v6, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 78
    .line 79
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->Oo8Oo00oo()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v7

    .line 83
    invoke-static {v6, v7}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->OO0o〇〇(Landroid/content/Context;Ljava/lang/String;)Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

    .line 84
    .line 85
    .line 86
    move-result-object v6

    .line 87
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->Oo8Oo00oo()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v5

    .line 91
    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    .line 93
    .line 94
    move-object v5, v6

    .line 95
    :goto_0
    if-eqz v5, :cond_1

    .line 96
    .line 97
    invoke-virtual {v5}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v5

    .line 101
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 102
    .line 103
    .line 104
    move-result v5

    .line 105
    if-nez v5, :cond_1

    .line 106
    .line 107
    goto :goto_1

    .line 108
    :cond_4
    const/4 v0, 0x1

    .line 109
    :cond_5
    :goto_1
    sget-object v3, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 110
    .line 111
    new-instance v4, Ljava/lang/StringBuilder;

    .line 112
    .line 113
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    .line 115
    .line 116
    const-string v5, "isShareDoc costTime:"

    .line 117
    .line 118
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 122
    .line 123
    .line 124
    move-result-wide v5

    .line 125
    sub-long/2addr v5, v1

    .line 126
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    const-string v1, " docItemList size:"

    .line 130
    .line 131
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    .line 133
    .line 134
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 135
    .line 136
    .line 137
    move-result p1

    .line 138
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 139
    .line 140
    .line 141
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object p1

    .line 145
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    .line 147
    .line 148
    :cond_6
    :goto_2
    return v0
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private O0oO008(JZ)Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->optimize_share_request:I

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    if-eqz p3, :cond_0

    .line 11
    .line 12
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 13
    .line 14
    .line 15
    move-result-object p3

    .line 16
    invoke-static {p3, p1, p2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇8(Landroid/content/Context;J)Z

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    return p1

    .line 21
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 22
    .line 23
    .line 24
    move-result-object p3

    .line 25
    invoke-static {p3, p1, p2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇00(Landroid/content/Context;J)I

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    if-nez p1, :cond_1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    const/4 v1, 0x0

    .line 33
    :goto_0
    return v1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private synthetic O0oo0o0〇(Landroid/content/Intent;)V
    .locals 0

    .line 1
    const/4 p1, 0x1

    .line 2
    iput-boolean p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇080OO8〇0:Z

    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O0〇oo()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private O0o〇O0〇()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 2
    .line 3
    instance-of v0, v0, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 4
    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 8
    .line 9
    instance-of v1, v0, Lcom/intsig/camscanner/share/channel/item/SendToPcShareChannel;

    .line 10
    .line 11
    if-nez v1, :cond_0

    .line 12
    .line 13
    instance-of v0, v0, Lcom/intsig/camscanner/share/channel/item/LinkShareChannel;

    .line 14
    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    :cond_0
    const/4 v0, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_1
    const/4 v0, 0x0

    .line 20
    :goto_0
    return v0
    .line 21
.end method

.method static bridge synthetic O0o〇〇Oo(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/app/BaseProgressDialog;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo0:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static O0〇OO8(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackDataListener;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/intsig/camscanner/share/listener/ShareBackDataListener;",
            "Z)V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/ShareHelper;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 4
    .line 5
    .line 6
    sget-object p0, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->DEFAULT:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 7
    .line 8
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper;->o08〇〇0O(Lcom/intsig/camscanner/share/ShareHelper$ShareType;)V

    .line 9
    .line 10
    .line 11
    const/4 p0, 0x0

    .line 12
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper;->OOo88OOo(Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;)V

    .line 13
    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    const/4 v3, 0x0

    .line 17
    move-object v1, p1

    .line 18
    move-object v4, p2

    .line 19
    move v5, p3

    .line 20
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/share/ShareHelper;->O80〇O〇080(Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackListener;Lcom/intsig/camscanner/share/listener/ShareBackDataListener;Z)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private O0〇oo()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;-><init>(Landroid/content/Context;)V

    .line 6
    .line 7
    .line 8
    const/4 v1, 0x1

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->OO0o〇〇(I)Lcom/intsig/advertisement/params/AdRequestOptions$Builder;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇80〇808〇O()Lcom/intsig/advertisement/params/AdRequestOptions;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/ShareDoneManager;->o〇O()Lcom/intsig/advertisement/adapters/positions/ShareDoneManager;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {v1, v0}, Lcom/intsig/advertisement/adapters/positions/ShareDoneManager;->〇8〇0〇o〇O(Lcom/intsig/advertisement/params/AdRequestOptions;)V

    .line 22
    .line 23
    .line 24
    sget-object v0, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇080:Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;

    .line 25
    .line 26
    sget-object v1, Lcom/intsig/camscanner/gift/interval/IntervalTaskEnum;->TaskShare:Lcom/intsig/camscanner/gift/interval/IntervalTaskEnum;

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇o〇(Lcom/intsig/camscanner/gift/interval/IntervalTaskEnum;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private O80〇O〇080(Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackListener;Lcom/intsig/camscanner/share/listener/ShareBackDataListener;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;Z",
            "Lcom/intsig/camscanner/share/listener/ShareBackListener;",
            "Lcom/intsig/camscanner/share/listener/ShareBackDataListener;",
            "Z)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "share  share docs docIds = "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    new-instance v4, Ljava/lang/ref/WeakReference;

    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 26
    .line 27
    invoke-direct {v4, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 28
    .line 29
    .line 30
    new-instance v0, Lo〇o8〇〇O/o〇〇0〇;

    .line 31
    .line 32
    move-object v2, v0

    .line 33
    move-object v3, p0

    .line 34
    move-object v5, p1

    .line 35
    move v6, p2

    .line 36
    move-object v7, p3

    .line 37
    move-object v8, p4

    .line 38
    move v9, p5

    .line 39
    invoke-direct/range {v2 .. v9}, Lo〇o8〇〇O/o〇〇0〇;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Ljava/lang/ref/WeakReference;Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackListener;Lcom/intsig/camscanner/share/listener/ShareBackDataListener;Z)V

    .line 40
    .line 41
    .line 42
    sget-object v1, Lcom/intsig/camscanner/share/ShareLinkAlertHint;->〇〇888:Lcom/intsig/camscanner/share/ShareLinkAlertHint$Companion;

    .line 43
    .line 44
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/share/ShareLinkAlertHint$Companion;->OO0o〇〇(Lcom/intsig/callback/Callback0;)V

    .line 45
    .line 46
    .line 47
    if-eqz p4, :cond_1

    .line 48
    .line 49
    if-eqz p5, :cond_0

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    invoke-direct {p0, p1, p2, p4}, Lcom/intsig/camscanner/share/ShareHelper;->O88〇〇o0O(Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackDataListener;)V

    .line 53
    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_1
    :goto_0
    iput-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->o〇00O:Lcom/intsig/camscanner/share/listener/ShareBackListener;

    .line 57
    .line 58
    new-instance p3, Lcom/intsig/camscanner/control/DataChecker;

    .line 59
    .line 60
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 61
    .line 62
    const-wide/16 v5, -0x1

    .line 63
    .line 64
    const/4 v7, 0x0

    .line 65
    sget v8, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O:I

    .line 66
    .line 67
    new-instance v9, Lo〇o8〇〇O/OOO〇O0;

    .line 68
    .line 69
    invoke-direct {v9, p0, p1, p2, p4}, Lo〇o8〇〇O/OOO〇O0;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackDataListener;)V

    .line 70
    .line 71
    .line 72
    move-object v2, p3

    .line 73
    move-object v4, p1

    .line 74
    invoke-direct/range {v2 .. v9}, Lcom/intsig/camscanner/control/DataChecker;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;JLjava/lang/String;ILcom/intsig/camscanner/control/DataChecker$ActionListener;)V

    .line 75
    .line 76
    .line 77
    const/4 p1, 0x1

    .line 78
    invoke-virtual {p3, p1}, Lcom/intsig/camscanner/control/DataChecker;->〇0000OOO(Z)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {p3}, Lcom/intsig/camscanner/control/DataChecker;->o〇0()Z

    .line 82
    .line 83
    .line 84
    :goto_1
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method private O880oOO08(Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/ShareFeiShu;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/intsig/camscanner/share/type/ShareFeiShu;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->FEI_SHU:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareFeiShu;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 6
    .line 7
    invoke-direct {v1, v2, p1}, Lcom/intsig/camscanner/share/type/ShareFeiShu;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 8
    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    invoke-static {p1, v1, v0}, Lcom/intsig/camscanner/share/ShareHelper;->o08oOO(Landroid/content/Context;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getTitleLink()I

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-eqz p1, :cond_0

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getTitleLink()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/share/type/BaseShare;->OOO(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getActionStr()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    if-nez p1, :cond_1

    .line 43
    .line 44
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getActionStr()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/share/type/BaseShare;->oO(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    :cond_1
    return-object v1
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static O88o〇(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/ShareHelper$ShareType;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/intsig/camscanner/share/ShareHelper$ShareType;",
            "Lcom/intsig/camscanner/share/listener/ShareBackListener;",
            ")V"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-static {p0, p1, p2, v0, p3}, Lcom/intsig/camscanner/share/ShareHelper;->OoOOo8(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/ShareHelper$ShareType;Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private O88〇〇o0O(Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackDataListener;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;Z",
            "Lcom/intsig/camscanner/share/listener/ShareBackDataListener;",
            ")V"
        }
    .end annotation

    .line 1
    move-object v10, p0

    .line 2
    move-object v11, p1

    .line 3
    move-object/from16 v0, p3

    .line 4
    .line 5
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 6
    .line 7
    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 17
    .line 18
    .line 19
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 20
    .line 21
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->O8(Ljava/util/ArrayList;)Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    const/4 v12, 0x0

    .line 26
    const/4 v13, 0x1

    .line 27
    if-eqz v1, :cond_7

    .line 28
    .line 29
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareSecureLink;

    .line 30
    .line 31
    iget-object v3, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 32
    .line 33
    invoke-direct {v1, v3, p1}, Lcom/intsig/camscanner/share/type/ShareSecureLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 40
    .line 41
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    if-le v3, v13, :cond_1

    .line 46
    .line 47
    const/4 v3, 0x1

    .line 48
    goto :goto_0

    .line 49
    :cond_1
    const/4 v3, 0x0

    .line 50
    :goto_0
    invoke-static {v1, v3}, Lcom/intsig/camscanner/share/NormalLinkListUtil;->〇080(Landroid/content/Context;Z)Landroid/util/SparseArray;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    .line 55
    .line 56
    .line 57
    move-result v3

    .line 58
    const/4 v4, 0x2

    .line 59
    if-lt v3, v4, :cond_4

    .line 60
    .line 61
    const/4 v3, 0x0

    .line 62
    :goto_1
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    .line 63
    .line 64
    .line 65
    move-result v4

    .line 66
    if-ge v3, v4, :cond_5

    .line 67
    .line 68
    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    .line 69
    .line 70
    .line 71
    move-result-object v4

    .line 72
    check-cast v4, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 73
    .line 74
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->〇00O0O0(Ljava/util/ArrayList;)Z

    .line 75
    .line 76
    .line 77
    move-result v5

    .line 78
    if-eqz v5, :cond_2

    .line 79
    .line 80
    sget-object v5, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->WE_CHAT:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 81
    .line 82
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 83
    .line 84
    .line 85
    move-result v5

    .line 86
    if-eqz v5, :cond_2

    .line 87
    .line 88
    invoke-virtual {p0, v4, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O8O〇88oO0(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;

    .line 89
    .line 90
    .line 91
    move-result-object v4

    .line 92
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    .line 94
    .line 95
    goto :goto_2

    .line 96
    :cond_2
    invoke-direct {p0, v4, p1}, Lcom/intsig/camscanner/share/ShareHelper;->〇0O〇Oo(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Z

    .line 97
    .line 98
    .line 99
    move-result v5

    .line 100
    if-eqz v5, :cond_3

    .line 101
    .line 102
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O880oOO08(Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/ShareFeiShu;

    .line 103
    .line 104
    .line 105
    move-result-object v4

    .line 106
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    .line 108
    .line 109
    goto :goto_2

    .line 110
    :cond_3
    invoke-direct {p0, v4, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O0OO8〇0(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;

    .line 111
    .line 112
    .line 113
    move-result-object v4

    .line 114
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    .line 116
    .line 117
    :goto_2
    add-int/lit8 v3, v3, 0x1

    .line 118
    .line 119
    goto :goto_1

    .line 120
    :cond_4
    sget-object v1, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->WE_CHAT:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 121
    .line 122
    invoke-virtual {p0, v1, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O8O〇88oO0(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;

    .line 123
    .line 124
    .line 125
    move-result-object v1

    .line 126
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    .line 128
    .line 129
    sget-object v1, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->QQ:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 130
    .line 131
    invoke-direct {p0, v1, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O0OO8〇0(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;

    .line 132
    .line 133
    .line 134
    move-result-object v1

    .line 135
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    .line 137
    .line 138
    :cond_5
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 139
    .line 140
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇o〇(Ljava/util/ArrayList;)Z

    .line 141
    .line 142
    .line 143
    move-result v1

    .line 144
    if-eqz v1, :cond_6

    .line 145
    .line 146
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 147
    .line 148
    invoke-static {v1, p1}, Lcom/intsig/camscanner/share/type/SendToPc;->Oo〇O(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/SendToPc;

    .line 149
    .line 150
    .line 151
    move-result-object v1

    .line 152
    const v3, 0x7f080bfa

    .line 153
    .line 154
    .line 155
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 156
    .line 157
    .line 158
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    .line 160
    .line 161
    :cond_6
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareCopyLink;

    .line 162
    .line 163
    iget-object v3, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 164
    .line 165
    invoke-direct {v1, v3, p1}, Lcom/intsig/camscanner/share/type/ShareCopyLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 166
    .line 167
    .line 168
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    .line 170
    .line 171
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareNormalLink;

    .line 172
    .line 173
    iget-object v3, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 174
    .line 175
    invoke-direct {v1, v3, p1}, Lcom/intsig/camscanner/share/type/ShareNormalLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 176
    .line 177
    .line 178
    const v3, 0x7f080c2e

    .line 179
    .line 180
    .line 181
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 182
    .line 183
    .line 184
    iget-object v3, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 185
    .line 186
    const v4, 0x7f130848

    .line 187
    .line 188
    .line 189
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 190
    .line 191
    .line 192
    move-result-object v3

    .line 193
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 194
    .line 195
    .line 196
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/type/ShareNormalLink;->ooOO()V

    .line 197
    .line 198
    .line 199
    const-string v3, "more"

    .line 200
    .line 201
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->oO(Ljava/lang/String;)V

    .line 202
    .line 203
    .line 204
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    .line 206
    .line 207
    :cond_7
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 208
    .line 209
    .line 210
    move-result v1

    .line 211
    if-ne v1, v13, :cond_8

    .line 212
    .line 213
    const/4 v1, 0x1

    .line 214
    goto :goto_3

    .line 215
    :cond_8
    const/4 v1, 0x0

    .line 216
    :goto_3
    if-eqz v1, :cond_9

    .line 217
    .line 218
    invoke-virtual {p1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 219
    .line 220
    .line 221
    move-result-object v3

    .line 222
    check-cast v3, Ljava/lang/Long;

    .line 223
    .line 224
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    .line 225
    .line 226
    .line 227
    move-result-wide v3

    .line 228
    iget-object v5, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 229
    .line 230
    invoke-static {v5, v3, v4}, Lcom/intsig/camscanner/data/dao/DocDao;->〇o00〇〇Oo(Landroid/content/Context;J)I

    .line 231
    .line 232
    .line 233
    move-result v3

    .line 234
    invoke-static {v3}, Lcom/intsig/camscanner/certificate_package/util/CertificateDBUtil;->OO0o〇〇〇〇0(I)Z

    .line 235
    .line 236
    .line 237
    move-result v4

    .line 238
    goto :goto_4

    .line 239
    :cond_9
    const/4 v3, 0x0

    .line 240
    const/4 v4, 0x0

    .line 241
    :goto_4
    sget-object v5, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 242
    .line 243
    new-instance v6, Ljava/lang/StringBuilder;

    .line 244
    .line 245
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 246
    .line 247
    .line 248
    const-string v7, "isSingleDoc = "

    .line 249
    .line 250
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    .line 252
    .line 253
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 254
    .line 255
    .line 256
    const-string v7, "  isCertificateDocType = "

    .line 257
    .line 258
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    .line 260
    .line 261
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 262
    .line 263
    .line 264
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 265
    .line 266
    .line 267
    move-result-object v6

    .line 268
    invoke-static {v5, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    .line 270
    .line 271
    new-instance v5, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 272
    .line 273
    iget-object v6, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 274
    .line 275
    invoke-direct {v5, v6, p1}, Lcom/intsig/camscanner/share/type/SharePdf;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 276
    .line 277
    .line 278
    new-instance v6, Lo〇o8〇〇O/o〇8oOO88;

    .line 279
    .line 280
    invoke-direct {v6, p0}, Lo〇o8〇〇O/o〇8oOO88;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 281
    .line 282
    .line 283
    invoke-virtual {v5, v6}, Lcom/intsig/camscanner/share/type/SharePdf;->〇08〇0〇o〇8(Lcom/intsig/camscanner/share/type/SharePdf$OnTryDeductionListener;)V

    .line 284
    .line 285
    .line 286
    const v6, 0x7f080ab3

    .line 287
    .line 288
    .line 289
    invoke-virtual {v5, v6}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 290
    .line 291
    .line 292
    const v6, 0x7f080c35

    .line 293
    .line 294
    .line 295
    invoke-virtual {v5, v6}, Lcom/intsig/camscanner/share/type/BaseShare;->oO00OOO(I)V

    .line 296
    .line 297
    .line 298
    iget-object v6, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 299
    .line 300
    const v7, 0x7f13084e

    .line 301
    .line 302
    .line 303
    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 304
    .line 305
    .line 306
    move-result-object v6

    .line 307
    invoke-virtual {v5, v6}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 308
    .line 309
    .line 310
    invoke-static {}, Lcom/intsig/camscanner/share/view/ShareWatermarkUtil;->Oo08()I

    .line 311
    .line 312
    .line 313
    move-result v6

    .line 314
    const/4 v7, 0x3

    .line 315
    if-ne v6, v7, :cond_a

    .line 316
    .line 317
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 318
    .line 319
    .line 320
    move-result v6

    .line 321
    if-nez v6, :cond_a

    .line 322
    .line 323
    iget-object v6, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 324
    .line 325
    const v7, 0x7f131b2e

    .line 326
    .line 327
    .line 328
    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 329
    .line 330
    .line 331
    move-result-object v6

    .line 332
    invoke-virtual {v5, v6}, Lcom/intsig/camscanner/share/type/SharePdf;->Ooo(Ljava/lang/String;)V

    .line 333
    .line 334
    .line 335
    :cond_a
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 336
    .line 337
    .line 338
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇〇888()I

    .line 339
    .line 340
    .line 341
    move-result v5

    .line 342
    const/4 v6, 0x0

    .line 343
    if-ne v5, v13, :cond_b

    .line 344
    .line 345
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 346
    .line 347
    .line 348
    move-result-object v5

    .line 349
    invoke-virtual {v5}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 350
    .line 351
    .line 352
    move-result-object v5

    .line 353
    iget-object v5, v5, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->watermark_plus_list:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WatermarkPlusList;

    .line 354
    .line 355
    if-eqz v5, :cond_b

    .line 356
    .line 357
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇08〇o0O()Lcom/intsig/camscanner/pdf/preshare/PdfPlusWatchAdNoWatermarkStatus;

    .line 358
    .line 359
    .line 360
    move-result-object v7

    .line 361
    new-instance v8, Lcom/intsig/camscanner/share/type/ShareNoWatermark;

    .line 362
    .line 363
    iget-object v9, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 364
    .line 365
    invoke-direct {v8, v9, p1, v6}, Lcom/intsig/camscanner/share/type/ShareNoWatermark;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 366
    .line 367
    .line 368
    const v9, 0x7f080c02

    .line 369
    .line 370
    .line 371
    invoke-virtual {v8, v9}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 372
    .line 373
    .line 374
    iget v9, v5, Lcom/intsig/comm/purchase/entity/QueryProductsResult$WatermarkPlusList;->daily_ad_to_privileges:I

    .line 375
    .line 376
    invoke-virtual {v7}, Lcom/intsig/camscanner/pdf/preshare/PdfPlusWatchAdNoWatermarkStatus;->〇o00〇〇Oo()I

    .line 377
    .line 378
    .line 379
    move-result v7

    .line 380
    sub-int/2addr v9, v7

    .line 381
    invoke-static {v9, v12}, Ljava/lang/Math;->max(II)I

    .line 382
    .line 383
    .line 384
    move-result v7

    .line 385
    invoke-virtual {v8, v7}, Lcom/intsig/camscanner/share/type/ShareNoWatermark;->o88o0O(I)V

    .line 386
    .line 387
    .line 388
    iget v5, v5, Lcom/intsig/comm/purchase/entity/QueryProductsResult$WatermarkPlusList;->daily_ad_to_privileges:I

    .line 389
    .line 390
    invoke-virtual {v8, v5}, Lcom/intsig/camscanner/share/type/ShareNoWatermark;->〇oOo〇(I)V

    .line 391
    .line 392
    .line 393
    iget-object v5, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 394
    .line 395
    const v7, 0x7f131b9f

    .line 396
    .line 397
    .line 398
    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 399
    .line 400
    .line 401
    move-result-object v5

    .line 402
    invoke-virtual {v8, v5}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 403
    .line 404
    .line 405
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 406
    .line 407
    .line 408
    :cond_b
    if-eqz p2, :cond_c

    .line 409
    .line 410
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 411
    .line 412
    .line 413
    move-result v5

    .line 414
    if-ne v5, v13, :cond_c

    .line 415
    .line 416
    sget-object v5, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 417
    .line 418
    invoke-virtual {p1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 419
    .line 420
    .line 421
    move-result-object v7

    .line 422
    check-cast v7, Ljava/lang/Long;

    .line 423
    .line 424
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    .line 425
    .line 426
    .line 427
    move-result-wide v7

    .line 428
    invoke-static {v5, v7, v8}, Lcom/intsig/camscanner/db/dao/ImageDao;->oO(Landroid/content/Context;J)I

    .line 429
    .line 430
    .line 431
    move-result v5

    .line 432
    if-le v5, v13, :cond_c

    .line 433
    .line 434
    new-instance v5, Lcom/intsig/camscanner/share/type/ShareSeparatedPdf;

    .line 435
    .line 436
    iget-object v7, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 437
    .line 438
    invoke-direct {v5, v7, p1, v6}, Lcom/intsig/camscanner/share/type/ShareSeparatedPdf;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 439
    .line 440
    .line 441
    const v7, 0x7f080aa5

    .line 442
    .line 443
    .line 444
    invoke-virtual {v5, v7}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 445
    .line 446
    .line 447
    const v7, 0x7f080c45

    .line 448
    .line 449
    .line 450
    invoke-virtual {v5, v7}, Lcom/intsig/camscanner/share/type/BaseShare;->oO00OOO(I)V

    .line 451
    .line 452
    .line 453
    iget-object v7, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 454
    .line 455
    const v8, 0x7f130da1

    .line 456
    .line 457
    .line 458
    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 459
    .line 460
    .line 461
    move-result-object v7

    .line 462
    invoke-virtual {v5, v7}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 463
    .line 464
    .line 465
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 466
    .line 467
    .line 468
    :cond_c
    const-string v5, "single certificate doc can not be show word and long pic item"

    .line 469
    .line 470
    if-eqz v1, :cond_e

    .line 471
    .line 472
    invoke-virtual {p1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 473
    .line 474
    .line 475
    move-result-object v7

    .line 476
    check-cast v7, Ljava/lang/Long;

    .line 477
    .line 478
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    .line 479
    .line 480
    .line 481
    move-result-wide v7

    .line 482
    iget-object v9, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 483
    .line 484
    invoke-virtual {v9}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 485
    .line 486
    .line 487
    move-result-object v9

    .line 488
    invoke-static {v9, v7, v8}, Lcom/intsig/camscanner/tsapp/collaborate/CollaborateUtil;->O8(Landroid/content/Context;J)I

    .line 489
    .line 490
    .line 491
    move-result v9

    .line 492
    if-eq v9, v13, :cond_e

    .line 493
    .line 494
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇0〇o0O8()Z

    .line 495
    .line 496
    .line 497
    move-result v9

    .line 498
    if-eqz v9, :cond_e

    .line 499
    .line 500
    if-nez v4, :cond_e

    .line 501
    .line 502
    sget-object v9, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 503
    .line 504
    invoke-static {v9, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    .line 506
    .line 507
    iget-object v9, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 508
    .line 509
    invoke-virtual {v9}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 510
    .line 511
    .line 512
    move-result-object v9

    .line 513
    invoke-static {v9, v7, v8}, Lcom/intsig/camscanner/db/dao/ImageDao;->o8oO〇(Landroid/content/Context;J)Ljava/util/ArrayList;

    .line 514
    .line 515
    .line 516
    move-result-object v7

    .line 517
    new-instance v8, Lcom/intsig/camscanner/share/type/ShareToWord;

    .line 518
    .line 519
    iget-object v9, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 520
    .line 521
    invoke-direct {v8, v9, p1, v7}, Lcom/intsig/camscanner/share/type/ShareToWord;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 522
    .line 523
    .line 524
    const/16 v7, 0x7c

    .line 525
    .line 526
    if-ne v3, v7, :cond_d

    .line 527
    .line 528
    const v7, 0x7f08077e

    .line 529
    .line 530
    .line 531
    invoke-virtual {v8, v7}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 532
    .line 533
    .line 534
    iget-object v7, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 535
    .line 536
    const v9, 0x7f13112c

    .line 537
    .line 538
    .line 539
    invoke-virtual {v7, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 540
    .line 541
    .line 542
    move-result-object v7

    .line 543
    invoke-virtual {v8, v7}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 544
    .line 545
    .line 546
    goto :goto_5

    .line 547
    :cond_d
    const v7, 0x7f080e1b

    .line 548
    .line 549
    .line 550
    invoke-virtual {v8, v7}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 551
    .line 552
    .line 553
    const v7, 0x7f080c5d

    .line 554
    .line 555
    .line 556
    invoke-virtual {v8, v7}, Lcom/intsig/camscanner/share/type/BaseShare;->oO00OOO(I)V

    .line 557
    .line 558
    .line 559
    iget-object v7, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 560
    .line 561
    const v9, 0x7f13085c

    .line 562
    .line 563
    .line 564
    invoke-virtual {v7, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 565
    .line 566
    .line 567
    move-result-object v7

    .line 568
    invoke-virtual {v8, v7}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 569
    .line 570
    .line 571
    :goto_5
    invoke-virtual {v8, v3}, Lcom/intsig/camscanner/share/type/ShareToWord;->OoO〇(I)V

    .line 572
    .line 573
    .line 574
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 575
    .line 576
    .line 577
    :cond_e
    new-instance v3, Lcom/intsig/camscanner/share/type/ShareImage;

    .line 578
    .line 579
    iget-object v7, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 580
    .line 581
    invoke-direct {v3, v7, p1}, Lcom/intsig/camscanner/share/type/ShareImage;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 582
    .line 583
    .line 584
    const v7, 0x7f080902

    .line 585
    .line 586
    .line 587
    invoke-virtual {v3, v7}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 588
    .line 589
    .line 590
    const v7, 0x7f080c38

    .line 591
    .line 592
    .line 593
    invoke-virtual {v3, v7}, Lcom/intsig/camscanner/share/type/BaseShare;->oO00OOO(I)V

    .line 594
    .line 595
    .line 596
    iget-object v7, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 597
    .line 598
    const v8, 0x7f130844

    .line 599
    .line 600
    .line 601
    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 602
    .line 603
    .line 604
    move-result-object v7

    .line 605
    invoke-virtual {v3, v7}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 606
    .line 607
    .line 608
    if-eqz v1, :cond_f

    .line 609
    .line 610
    if-eqz v4, :cond_f

    .line 611
    .line 612
    const/4 v7, 0x1

    .line 613
    goto :goto_6

    .line 614
    :cond_f
    const/4 v7, 0x0

    .line 615
    :goto_6
    invoke-virtual {v3, v7}, Lcom/intsig/camscanner/share/type/ShareImage;->oO8o(Z)V

    .line 616
    .line 617
    .line 618
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 619
    .line 620
    .line 621
    if-eqz v1, :cond_10

    .line 622
    .line 623
    if-nez v4, :cond_11

    .line 624
    .line 625
    :cond_10
    sget-object v1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 626
    .line 627
    invoke-static {v1, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    .line 629
    .line 630
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OOo8o〇O(Ljava/util/List;)Lcom/intsig/camscanner/share/type/ShareLongImage;

    .line 631
    .line 632
    .line 633
    move-result-object v1

    .line 634
    const v3, 0x7f08098b

    .line 635
    .line 636
    .line 637
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 638
    .line 639
    .line 640
    const v3, 0x7f080988

    .line 641
    .line 642
    .line 643
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->oO00OOO(I)V

    .line 644
    .line 645
    .line 646
    iget-object v3, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 647
    .line 648
    const v4, 0x7f1307d1

    .line 649
    .line 650
    .line 651
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 652
    .line 653
    .line 654
    move-result-object v3

    .line 655
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 656
    .line 657
    .line 658
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 659
    .line 660
    .line 661
    :cond_11
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 662
    .line 663
    .line 664
    move-result-object v1

    .line 665
    iget v1, v1, Lcom/intsig/tsapp/sync/AppConfigJson;->share_preview_style:I

    .line 666
    .line 667
    invoke-static {v1}, Lcom/intsig/camscanner/share/view/share_type/ShareTypeLinkPanelNew;->〇o(I)Z

    .line 668
    .line 669
    .line 670
    move-result v3

    .line 671
    if-nez v3, :cond_12

    .line 672
    .line 673
    invoke-static {v1}, Lcom/intsig/camscanner/share/view/share_type/ShareTypeLinkPanelNew;->O8〇o(I)Z

    .line 674
    .line 675
    .line 676
    move-result v3

    .line 677
    if-eqz v3, :cond_13

    .line 678
    .line 679
    :cond_12
    new-instance v3, Lcom/intsig/camscanner/share/type/ShareAirPrint;

    .line 680
    .line 681
    iget-object v4, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 682
    .line 683
    invoke-direct {v3, v4, p1, v6}, Lcom/intsig/camscanner/share/type/ShareAirPrint;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 684
    .line 685
    .line 686
    const v4, 0x7f080c3b

    .line 687
    .line 688
    .line 689
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 690
    .line 691
    .line 692
    iget-object v4, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 693
    .line 694
    const v5, 0x7f13038b

    .line 695
    .line 696
    .line 697
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 698
    .line 699
    .line 700
    move-result-object v4

    .line 701
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 702
    .line 703
    .line 704
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 705
    .line 706
    .line 707
    :cond_13
    new-instance v3, Lcom/intsig/camscanner/share/type/ShareEmail;

    .line 708
    .line 709
    iget-object v4, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 710
    .line 711
    invoke-direct {v3, v4, p1}, Lcom/intsig/camscanner/share/type/ShareEmail;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 712
    .line 713
    .line 714
    const v4, 0x7f080c14

    .line 715
    .line 716
    .line 717
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 718
    .line 719
    .line 720
    iget-object v4, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 721
    .line 722
    const v5, 0x7f130136

    .line 723
    .line 724
    .line 725
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 726
    .line 727
    .line 728
    move-result-object v4

    .line 729
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 730
    .line 731
    .line 732
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 733
    .line 734
    .line 735
    invoke-static {v1}, Lcom/intsig/camscanner/share/view/share_type/ShareTypeLinkPanelNew;->〇o(I)Z

    .line 736
    .line 737
    .line 738
    move-result v1

    .line 739
    if-eqz v1, :cond_15

    .line 740
    .line 741
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 742
    .line 743
    invoke-static {v1, p1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->Oo08(Landroid/content/Context;Ljava/util/ArrayList;)Z

    .line 744
    .line 745
    .line 746
    move-result v1

    .line 747
    if-nez v1, :cond_15

    .line 748
    .line 749
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareNormalLink;

    .line 750
    .line 751
    iget-object v3, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 752
    .line 753
    invoke-direct {v1, v3, p1}, Lcom/intsig/camscanner/share/type/ShareNormalLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 754
    .line 755
    .line 756
    iget-object v3, v10, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 757
    .line 758
    iget-object v4, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 759
    .line 760
    iget-object v5, v10, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 761
    .line 762
    invoke-virtual {v3, v4, v5}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇〇808〇(Landroid/app/Activity;Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;)Ljava/lang/String;

    .line 763
    .line 764
    .line 765
    move-result-object v3

    .line 766
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/ShareNormalLink;->OOO8o〇〇(Ljava/lang/String;)V

    .line 767
    .line 768
    .line 769
    const v3, 0x7f080c26

    .line 770
    .line 771
    .line 772
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 773
    .line 774
    .line 775
    iget-object v3, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 776
    .line 777
    const v4, 0x7f130fb1

    .line 778
    .line 779
    .line 780
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 781
    .line 782
    .line 783
    move-result-object v3

    .line 784
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 785
    .line 786
    .line 787
    iget-object v3, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 788
    .line 789
    invoke-static {v3}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 790
    .line 791
    .line 792
    move-result v3

    .line 793
    if-nez v3, :cond_14

    .line 794
    .line 795
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->Oo08()Z

    .line 796
    .line 797
    .line 798
    move-result v3

    .line 799
    if-nez v3, :cond_14

    .line 800
    .line 801
    iget-object v3, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 802
    .line 803
    new-array v4, v13, [Ljava/lang/Object;

    .line 804
    .line 805
    const-string v5, "7"

    .line 806
    .line 807
    aput-object v5, v4, v12

    .line 808
    .line 809
    const v5, 0x7f131a32

    .line 810
    .line 811
    .line 812
    invoke-virtual {v3, v5, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 813
    .line 814
    .line 815
    move-result-object v3

    .line 816
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->Ooo(Ljava/lang/String;)V

    .line 817
    .line 818
    .line 819
    :cond_14
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 820
    .line 821
    .line 822
    :cond_15
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o088〇〇()Z

    .line 823
    .line 824
    .line 825
    move-result v1

    .line 826
    if-eqz v1, :cond_16

    .line 827
    .line 828
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareQrCode;

    .line 829
    .line 830
    iget-object v3, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 831
    .line 832
    invoke-direct {v1, v3, p1}, Lcom/intsig/camscanner/share/type/ShareQrCode;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 833
    .line 834
    .line 835
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 836
    .line 837
    .line 838
    :cond_16
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 839
    .line 840
    invoke-direct {p0, v1, v2, p1, v6}, Lcom/intsig/camscanner/share/ShareHelper;->Ooo8〇〇(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 841
    .line 842
    .line 843
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 844
    .line 845
    .line 846
    move-result v1

    .line 847
    const v3, 0x7f130858

    .line 848
    .line 849
    .line 850
    const v4, 0x7f080c48

    .line 851
    .line 852
    .line 853
    const v5, 0x7f080d5e

    .line 854
    .line 855
    .line 856
    if-ne v1, v13, :cond_17

    .line 857
    .line 858
    invoke-virtual {p1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 859
    .line 860
    .line 861
    move-result-object v1

    .line 862
    check-cast v1, Ljava/lang/Long;

    .line 863
    .line 864
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    .line 865
    .line 866
    .line 867
    move-result-wide v6

    .line 868
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 869
    .line 870
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 871
    .line 872
    .line 873
    move-result-object v1

    .line 874
    invoke-static {v1, v6, v7}, Lcom/intsig/camscanner/tsapp/collaborate/CollaborateUtil;->O8(Landroid/content/Context;J)I

    .line 875
    .line 876
    .line 877
    move-result v1

    .line 878
    if-eq v1, v13, :cond_18

    .line 879
    .line 880
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 881
    .line 882
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 883
    .line 884
    .line 885
    move-result-object v1

    .line 886
    invoke-static {v1, v6, v7}, Lcom/intsig/camscanner/db/dao/ImageDao;->o8oO〇(Landroid/content/Context;J)Ljava/util/ArrayList;

    .line 887
    .line 888
    .line 889
    move-result-object v1

    .line 890
    new-instance v8, Lcom/intsig/camscanner/share/type/ShareBatchOcr;

    .line 891
    .line 892
    iget-object v9, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 893
    .line 894
    invoke-direct {v8, v9, v6, v7, v1}, Lcom/intsig/camscanner/share/type/ShareBatchOcr;-><init>(Landroidx/fragment/app/FragmentActivity;JLjava/util/List;)V

    .line 895
    .line 896
    .line 897
    invoke-virtual {v8, v5}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 898
    .line 899
    .line 900
    invoke-virtual {v8, v4}, Lcom/intsig/camscanner/share/type/BaseShare;->oO00OOO(I)V

    .line 901
    .line 902
    .line 903
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 904
    .line 905
    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 906
    .line 907
    .line 908
    move-result-object v1

    .line 909
    invoke-virtual {v8, v1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 910
    .line 911
    .line 912
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 913
    .line 914
    .line 915
    goto :goto_7

    .line 916
    :cond_17
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 917
    .line 918
    .line 919
    move-result v1

    .line 920
    if-le v1, v13, :cond_18

    .line 921
    .line 922
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareOcrText;

    .line 923
    .line 924
    iget-object v6, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 925
    .line 926
    invoke-direct {v1, v6, p1}, Lcom/intsig/camscanner/share/type/ShareOcrText;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 927
    .line 928
    .line 929
    invoke-virtual {v1, v5}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 930
    .line 931
    .line 932
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/share/type/BaseShare;->oO00OOO(I)V

    .line 933
    .line 934
    .line 935
    iget-object v4, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 936
    .line 937
    invoke-virtual {v4, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 938
    .line 939
    .line 940
    move-result-object v3

    .line 941
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 942
    .line 943
    .line 944
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 945
    .line 946
    .line 947
    :cond_18
    :goto_7
    sget-object v1, Lcom/intsig/camscanner/ads/csAd/CollectAppListManager;->〇080:Lcom/intsig/camscanner/ads/csAd/CollectAppListManager;

    .line 948
    .line 949
    invoke-virtual {v1}, Lcom/intsig/camscanner/ads/csAd/CollectAppListManager;->Oo08()V

    .line 950
    .line 951
    .line 952
    if-nez v0, :cond_1a

    .line 953
    .line 954
    iget-object v0, v10, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 955
    .line 956
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 957
    .line 958
    iget-object v5, v10, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 959
    .line 960
    const/4 v6, 0x1

    .line 961
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO8〇(Ljava/util/ArrayList;)Z

    .line 962
    .line 963
    .line 964
    move-result v7

    .line 965
    iget-object v3, v10, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 966
    .line 967
    iget-object v4, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 968
    .line 969
    iget-object v8, v10, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 970
    .line 971
    invoke-virtual {v3, v4, v8}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇〇808〇(Landroid/app/Activity;Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;)Ljava/lang/String;

    .line 972
    .line 973
    .line 974
    move-result-object v8

    .line 975
    move/from16 v3, p2

    .line 976
    .line 977
    move-object v4, p0

    .line 978
    move-object v9, p1

    .line 979
    invoke-interface/range {v0 .. v9}, Lcom/intsig/camscanner/share/listener/ShareUiInterface;->〇080(Landroid/content/Context;Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareTypeClickListener;Lcom/intsig/camscanner/share/ShareHelper$ShareType;ZZLjava/lang/String;Ljava/util/ArrayList;)V

    .line 980
    .line 981
    .line 982
    iget-object v0, v10, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 983
    .line 984
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 985
    .line 986
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 987
    .line 988
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 989
    .line 990
    .line 991
    move-result v3

    .line 992
    if-ne v3, v13, :cond_19

    .line 993
    .line 994
    const/4 v12, 0x1

    .line 995
    :cond_19
    invoke-virtual {v0, v1, v2, v12}, Lcom/intsig/camscanner/share/ShareDataPresenter;->oo〇(Landroid/app/Activity;Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;Z)V

    .line 996
    .line 997
    .line 998
    goto :goto_8

    .line 999
    :cond_1a
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 1000
    .line 1001
    .line 1002
    move-result v1

    .line 1003
    if-lez v1, :cond_1b

    .line 1004
    .line 1005
    invoke-interface {v0, v2}, Lcom/intsig/camscanner/share/listener/ShareBackDataListener;->〇080(Ljava/util/ArrayList;)V

    .line 1006
    .line 1007
    .line 1008
    :cond_1b
    :goto_8
    return-void
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private O8OO08o()V
    .locals 9

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO〇00〇8oO:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    iput-boolean v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO〇00〇8oO:Z

    .line 7
    .line 8
    iget v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO0〇0o:I

    .line 9
    .line 10
    invoke-static {v0}, Lcom/intsig/camscanner/ocrapi/OcrStateSwitcher;->o〇0(I)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O8oOo80()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 23
    .line 24
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 25
    .line 26
    invoke-direct {v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 27
    .line 28
    .line 29
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_WORD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 30
    .line 31
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_WORD_PREVIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 36
    .line 37
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-static {v0, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇0〇O0088o(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 42
    .line 43
    .line 44
    return-void

    .line 45
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 46
    .line 47
    const-string v1, "onOcrBtnClick go to set lang for first time no matter local nor cloud"

    .line 48
    .line 49
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 53
    .line 54
    new-instance v1, Lcom/intsig/camscanner/share/ShareHelper$5;

    .line 55
    .line 56
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/share/ShareHelper$5;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 57
    .line 58
    .line 59
    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/DialogUtils;->O0o〇〇Oo(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V

    .line 60
    .line 61
    .line 62
    return-void

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 64
    .line 65
    instance-of v2, v0, Lcom/intsig/camscanner/share/type/ShareBatchOcr;

    .line 66
    .line 67
    if-eqz v2, :cond_3

    .line 68
    .line 69
    const-string v0, "CSShare"

    .line 70
    .line 71
    const-string v1, "batch_ocr"

    .line 72
    .line 73
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O8oOo80()Z

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    if-eqz v0, :cond_2

    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 83
    .line 84
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 85
    .line 86
    invoke-direct {v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 87
    .line 88
    .line 89
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_WORD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 90
    .line 91
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 92
    .line 93
    .line 94
    move-result-object v1

    .line 95
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_WORD_PREVIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 96
    .line 97
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 98
    .line 99
    .line 100
    move-result-object v1

    .line 101
    invoke-static {v0, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇0〇O0088o(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 102
    .line 103
    .line 104
    goto :goto_0

    .line 105
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 106
    .line 107
    check-cast v0, Lcom/intsig/camscanner/share/type/ShareBatchOcr;

    .line 108
    .line 109
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/ShareBatchOcr;->〇〇o8()V

    .line 110
    .line 111
    .line 112
    :goto_0
    return-void

    .line 113
    :cond_3
    instance-of v2, v0, Lcom/intsig/camscanner/share/type/ShareToWord;

    .line 114
    .line 115
    if-eqz v2, :cond_4

    .line 116
    .line 117
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 118
    .line 119
    const-string v1, " showShareApplicationView() 03 ShareToWord"

    .line 120
    .line 121
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 125
    .line 126
    check-cast v0, Lcom/intsig/camscanner/share/type/ShareToWord;

    .line 127
    .line 128
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/ShareToWord;->〇8o()V

    .line 129
    .line 130
    .line 131
    return-void

    .line 132
    :cond_4
    instance-of v2, v0, Lcom/intsig/camscanner/share/type/ShareAirPrint;

    .line 133
    .line 134
    if-eqz v2, :cond_5

    .line 135
    .line 136
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 137
    .line 138
    const-string v1, " showShareApplicationView() 04 ShareAirPrint"

    .line 139
    .line 140
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 144
    .line 145
    check-cast v0, Lcom/intsig/camscanner/share/type/ShareAirPrint;

    .line 146
    .line 147
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/ShareAirPrint;->OO〇0008O8()V

    .line 148
    .line 149
    .line 150
    return-void

    .line 151
    :cond_5
    instance-of v2, v0, Lcom/intsig/camscanner/share/type/ShareEmail;

    .line 152
    .line 153
    if-eqz v2, :cond_6

    .line 154
    .line 155
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 156
    .line 157
    const-string v1, " showShareApplicationView() 04 ShareEmail"

    .line 158
    .line 159
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    .line 161
    .line 162
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 163
    .line 164
    check-cast v0, Lcom/intsig/camscanner/share/type/ShareEmail;

    .line 165
    .line 166
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 167
    .line 168
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 169
    .line 170
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 171
    .line 172
    invoke-virtual {v1, v2, v3}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇〇808〇(Landroid/app/Activity;Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;)Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object v1

    .line 176
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/ShareEmail;->O8O〇(Ljava/lang/String;)V

    .line 177
    .line 178
    .line 179
    return-void

    .line 180
    :cond_6
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇o〇()Z

    .line 181
    .line 182
    .line 183
    move-result v0

    .line 184
    if-eqz v0, :cond_7

    .line 185
    .line 186
    return-void

    .line 187
    :cond_7
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 188
    .line 189
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇080()Landroid/content/Intent;

    .line 190
    .line 191
    .line 192
    move-result-object v0

    .line 193
    if-nez v0, :cond_8

    .line 194
    .line 195
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 196
    .line 197
    const-string v1, "mCurrentShare.buildIntent() is null"

    .line 198
    .line 199
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    .line 201
    .line 202
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->oO8o()V

    .line 203
    .line 204
    .line 205
    return-void

    .line 206
    :cond_8
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 207
    .line 208
    sget-object v3, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->EMAIL_MYSELF:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 209
    .line 210
    if-eq v2, v3, :cond_9

    .line 211
    .line 212
    sget-object v3, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->EMAIL_OTHER:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 213
    .line 214
    if-ne v2, v3, :cond_a

    .line 215
    .line 216
    :cond_9
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 217
    .line 218
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇o00〇〇Oo()Landroid/content/Intent;

    .line 219
    .line 220
    .line 221
    move-result-object v0

    .line 222
    :cond_a
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 223
    .line 224
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇80〇808〇O(Landroid/content/Intent;)Ljava/util/ArrayList;

    .line 225
    .line 226
    .line 227
    move-result-object v6

    .line 228
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 229
    .line 230
    sget-object v3, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->DEFAULT:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 231
    .line 232
    if-ne v2, v3, :cond_c

    .line 233
    .line 234
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 235
    .line 236
    invoke-virtual {v2}, Lcom/intsig/camscanner/share/type/BaseShare;->oO80()Ljava/util/ArrayList;

    .line 237
    .line 238
    .line 239
    move-result-object v2

    .line 240
    if-eqz v2, :cond_c

    .line 241
    .line 242
    invoke-static {}, Lcom/intsig/camscanner/pdf/PreferenceCsPdfHelper;->〇8o8o〇()Z

    .line 243
    .line 244
    .line 245
    move-result v3

    .line 246
    if-eqz v3, :cond_b

    .line 247
    .line 248
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 249
    .line 250
    invoke-virtual {v3}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O8o08O()Landroid/content/pm/ResolveInfo;

    .line 251
    .line 252
    .line 253
    move-result-object v3

    .line 254
    invoke-virtual {v2, v1, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 255
    .line 256
    .line 257
    :cond_b
    invoke-virtual {v6, v1, v2}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 258
    .line 259
    .line 260
    sget-object v3, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 261
    .line 262
    new-instance v4, Ljava/lang/StringBuilder;

    .line 263
    .line 264
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 265
    .line 266
    .line 267
    const-string v5, "insert special app size="

    .line 268
    .line 269
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    .line 271
    .line 272
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 273
    .line 274
    .line 275
    move-result v2

    .line 276
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 277
    .line 278
    .line 279
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 280
    .line 281
    .line 282
    move-result-object v2

    .line 283
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    .line 285
    .line 286
    :cond_c
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 287
    .line 288
    if-eqz v2, :cond_e

    .line 289
    .line 290
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 291
    .line 292
    instance-of v0, v0, Lcom/intsig/camscanner/share/type/ShareNormalLink;

    .line 293
    .line 294
    if-eqz v0, :cond_d

    .line 295
    .line 296
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 297
    .line 298
    .line 299
    move-result-object v0

    .line 300
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_share_type_switch:I

    .line 301
    .line 302
    if-nez v0, :cond_d

    .line 303
    .line 304
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 305
    .line 306
    check-cast v0, Lcom/intsig/camscanner/share/type/ShareNormalLink;

    .line 307
    .line 308
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/ShareNormalLink;->〇0O〇Oo(Z)V

    .line 309
    .line 310
    .line 311
    :cond_d
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 312
    .line 313
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->O88O:Lcom/intsig/camscanner/share/channel/item/ShareChannelListener;

    .line 314
    .line 315
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->O8ooOoo〇(Lcom/intsig/camscanner/share/channel/item/ShareChannelListener;)V

    .line 316
    .line 317
    .line 318
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 319
    .line 320
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->O8o08O8O:Lcom/intsig/utils/activity/ActivityLifeCircleManager;

    .line 321
    .line 322
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 323
    .line 324
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 325
    .line 326
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->o〇〇0〇(Lcom/intsig/utils/activity/ActivityLifeCircleManager;Lcom/intsig/camscanner/share/ShareDataPresenter;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 327
    .line 328
    .line 329
    const/4 v0, 0x0

    .line 330
    iput-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 331
    .line 332
    return-void

    .line 333
    :cond_e
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 334
    .line 335
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 336
    .line 337
    invoke-virtual {v1, v6, v0, v2}, Lcom/intsig/camscanner/share/ShareDataPresenter;->oO80(Ljava/util/List;Landroid/content/Intent;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 338
    .line 339
    .line 340
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 341
    .line 342
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 343
    .line 344
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O00()I

    .line 345
    .line 346
    .line 347
    move-result v1

    .line 348
    invoke-virtual {v0, v6, v1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇O〇(Ljava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 349
    .line 350
    .line 351
    move-result-object v5

    .line 352
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 353
    .line 354
    invoke-virtual {v0, v6}, Lcom/intsig/camscanner/share/ShareDataPresenter;->o〇〇0〇(Ljava/util/ArrayList;)V

    .line 355
    .line 356
    .line 357
    if-eqz v5, :cond_f

    .line 358
    .line 359
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    .line 360
    .line 361
    .line 362
    move-result v0

    .line 363
    if-gtz v0, :cond_10

    .line 364
    .line 365
    :cond_f
    if-eqz v6, :cond_12

    .line 366
    .line 367
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    .line 368
    .line 369
    .line 370
    move-result v0

    .line 371
    if-lez v0, :cond_12

    .line 372
    .line 373
    :cond_10
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 374
    .line 375
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇0000OOO()Z

    .line 376
    .line 377
    .line 378
    move-result v0

    .line 379
    if-eqz v0, :cond_11

    .line 380
    .line 381
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 382
    .line 383
    const-string v1, "OK. GO TO SHARE, NOW!"

    .line 384
    .line 385
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    .line 387
    .line 388
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 389
    .line 390
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 391
    .line 392
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/type/BaseShare;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 393
    .line 394
    .line 395
    move-result-object v1

    .line 396
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 397
    .line 398
    invoke-virtual {v2}, Lcom/intsig/camscanner/share/type/BaseShare;->OO0o〇〇()Ljava/lang/String;

    .line 399
    .line 400
    .line 401
    move-result-object v2

    .line 402
    invoke-virtual {v0, v1, v2, v6, v5}, Lcom/intsig/camscanner/share/ShareDataPresenter;->OO0o〇〇〇〇0(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Landroid/content/pm/ActivityInfo;

    .line 403
    .line 404
    .line 405
    move-result-object v0

    .line 406
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇〇〇0(Landroid/content/pm/ActivityInfo;)V

    .line 407
    .line 408
    .line 409
    return-void

    .line 410
    :cond_11
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 411
    .line 412
    iget-object v4, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 413
    .line 414
    iget-object v8, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 415
    .line 416
    move-object v7, p0

    .line 417
    invoke-interface/range {v3 .. v8}, Lcom/intsig/camscanner/share/listener/ShareUiInterface;->O8(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareAppOnclickListener;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 418
    .line 419
    .line 420
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 421
    .line 422
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 423
    .line 424
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 425
    .line 426
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇0000OOO(Landroid/app/Activity;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 427
    .line 428
    .line 429
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->Oo0oOo〇0()V

    .line 430
    .line 431
    .line 432
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 433
    .line 434
    .line 435
    move-result v0

    .line 436
    if-eqz v0, :cond_13

    .line 437
    .line 438
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O0〇oo()V

    .line 439
    .line 440
    .line 441
    goto :goto_1

    .line 442
    :cond_12
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->oO8o()V

    .line 443
    .line 444
    .line 445
    const-string v0, "no_share_app"

    .line 446
    .line 447
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/share/ShareHelper;->oO8008O(Ljava/lang/String;)V

    .line 448
    .line 449
    .line 450
    :cond_13
    :goto_1
    return-void
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
    .line 1825
    .line 1826
    .line 1827
    .line 1828
    .line 1829
    .line 1830
    .line 1831
    .line 1832
    .line 1833
    .line 1834
    .line 1835
    .line 1836
    .line 1837
    .line 1838
    .line 1839
    .line 1840
    .line 1841
    .line 1842
    .line 1843
    .line 1844
    .line 1845
    .line 1846
    .line 1847
    .line 1848
    .line 1849
    .line 1850
    .line 1851
    .line 1852
    .line 1853
    .line 1854
    .line 1855
    .line 1856
    .line 1857
    .line 1858
    .line 1859
    .line 1860
    .line 1861
    .line 1862
    .line 1863
    .line 1864
    .line 1865
    .line 1866
    .line 1867
    .line 1868
    .line 1869
    .line 1870
    .line 1871
    .line 1872
    .line 1873
    .line 1874
    .line 1875
    .line 1876
    .line 1877
    .line 1878
    .line 1879
    .line 1880
    .line 1881
    .line 1882
    .line 1883
    .line 1884
    .line 1885
    .line 1886
    .line 1887
    .line 1888
    .line 1889
    .line 1890
    .line 1891
    .line 1892
    .line 1893
    .line 1894
    .line 1895
    .line 1896
    .line 1897
    .line 1898
    .line 1899
    .line 1900
    .line 1901
    .line 1902
    .line 1903
    .line 1904
    .line 1905
    .line 1906
    .line 1907
    .line 1908
    .line 1909
    .line 1910
    .line 1911
    .line 1912
    .line 1913
    .line 1914
    .line 1915
    .line 1916
    .line 1917
    .line 1918
    .line 1919
    .line 1920
    .line 1921
    .line 1922
    .line 1923
    .line 1924
    .line 1925
    .line 1926
    .line 1927
    .line 1928
    .line 1929
    .line 1930
    .line 1931
    .line 1932
    .line 1933
    .line 1934
    .line 1935
    .line 1936
    .line 1937
    .line 1938
    .line 1939
    .line 1940
    .line 1941
    .line 1942
    .line 1943
    .line 1944
    .line 1945
    .line 1946
    .line 1947
    .line 1948
    .line 1949
    .line 1950
    .line 1951
    .line 1952
    .line 1953
    .line 1954
    .line 1955
    .line 1956
    .line 1957
    .line 1958
    .line 1959
    .line 1960
    .line 1961
    .line 1962
    .line 1963
    .line 1964
    .line 1965
    .line 1966
    .line 1967
    .line 1968
    .line 1969
    .line 1970
    .line 1971
    .line 1972
    .line 1973
    .line 1974
    .line 1975
    .line 1976
    .line 1977
    .line 1978
    .line 1979
    .line 1980
    .line 1981
    .line 1982
    .line 1983
    .line 1984
    .line 1985
    .line 1986
    .line 1987
    .line 1988
    .line 1989
    .line 1990
    .line 1991
    .line 1992
    .line 1993
    .line 1994
    .line 1995
    .line 1996
    .line 1997
    .line 1998
    .line 1999
    .line 2000
    .line 2001
    .line 2002
    .line 2003
    .line 2004
    .line 2005
    .line 2006
    .line 2007
    .line 2008
    .line 2009
    .line 2010
    .line 2011
    .line 2012
    .line 2013
    .line 2014
    .line 2015
    .line 2016
    .line 2017
    .line 2018
    .line 2019
    .line 2020
    .line 2021
    .line 2022
    .line 2023
    .line 2024
    .line 2025
    .line 2026
    .line 2027
    .line 2028
    .line 2029
    .line 2030
    .line 2031
    .line 2032
    .line 2033
    .line 2034
    .line 2035
    .line 2036
    .line 2037
    .line 2038
    .line 2039
    .line 2040
    .line 2041
    .line 2042
    .line 2043
    .line 2044
    .line 2045
    .line 2046
    .line 2047
    .line 2048
    .line 2049
    .line 2050
    .line 2051
    .line 2052
    .line 2053
    .line 2054
    .line 2055
    .line 2056
    .line 2057
    .line 2058
    .line 2059
    .line 2060
    .line 2061
    .line 2062
    .line 2063
    .line 2064
    .line 2065
    .line 2066
    .line 2067
    .line 2068
    .line 2069
    .line 2070
    .line 2071
    .line 2072
    .line 2073
    .line 2074
    .line 2075
    .line 2076
    .line 2077
    .line 2078
    .line 2079
    .line 2080
    .line 2081
    .line 2082
    .line 2083
    .line 2084
    .line 2085
    .line 2086
.end method

.method static bridge synthetic O8O〇(Lcom/intsig/camscanner/share/ShareHelper;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->o0O〇8o0O()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private O8O〇8oo08()V
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "tryToDeductionForSharePdf>>>"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 9
    .line 10
    if-eqz v0, :cond_2

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇oo()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_2

    .line 24
    .line 25
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇o〇OO80oO()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-gez v0, :cond_1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/intsig/utils/CommonLoadingTask;

    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 35
    .line 36
    new-instance v2, Lcom/intsig/camscanner/share/ShareHelper$1;

    .line 37
    .line 38
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/share/ShareHelper$1;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 39
    .line 40
    .line 41
    const/4 v3, 0x0

    .line 42
    const/4 v4, 0x0

    .line 43
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;Z)V

    .line 44
    .line 45
    .line 46
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->〇8o8o〇()Ljava/util/concurrent/ExecutorService;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    new-array v2, v4, [Ljava/lang/Void;

    .line 51
    .line 52
    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 53
    .line 54
    .line 55
    :cond_2
    :goto_0
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private O8oOo80()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/util/SwitchControl;->〇o00〇〇Oo()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 14
    .line 15
    instance-of v0, v0, Lcom/intsig/camscanner/share/type/ShareBatchOcr;

    .line 16
    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v0, 0x0

    .line 22
    :goto_0
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static synthetic O8ooOoo〇(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->〇o〇Oo0(Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static synthetic O8〇o(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->OO0〇〇8(Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private synthetic OO0〇〇8(Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/List;)V
    .locals 1

    .line 1
    sget-object p2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "prioritySync increase priority end, start sync:"

    .line 4
    .line 5
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08〇o0O:Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;

    .line 9
    .line 10
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00(Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;)V

    .line 11
    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O8ooOoo〇()Lcom/intsig/camscanner/tsapp/sync/SyncClient;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    const/4 p2, 0x0

    .line 18
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O0o〇〇Oo(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic OO8oO0o〇(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/dialog/SyncDialogClient;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->Oo80:Lcom/intsig/camscanner/share/dialog/SyncDialogClient;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private OO8〇(Ljava/util/ArrayList;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/intsig/utils/ListUtils;->〇o〇(Ljava/util/List;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->O8()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-eqz v0, :cond_1

    .line 34
    .line 35
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    check-cast v0, Ljava/lang/Long;

    .line 40
    .line 41
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 42
    .line 43
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 44
    .line 45
    .line 46
    move-result-wide v2

    .line 47
    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 52
    .line 53
    invoke-static {v1, v0}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇8(Landroid/content/Context;Landroid/net/Uri;)I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    if-lez v0, :cond_0

    .line 58
    .line 59
    const/4 p1, 0x1

    .line 60
    return p1

    .line 61
    :cond_1
    const/4 p1, 0x0

    .line 62
    return p1
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method static bridge synthetic OOO(Lcom/intsig/camscanner/share/ShareHelper;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o〇oO:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private OOO8o〇〇(Landroid/content/Intent;)V
    .locals 18

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    sget-object v2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 6
    .line 7
    new-instance v3, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v4, "getPackageName -> "

    .line 13
    .line 14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    iget-object v4, v0, Lcom/intsig/camscanner/share/ShareHelper;->o8oOOo:Landroid/content/pm/ActivityInfo;

    .line 18
    .line 19
    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 20
    .line 21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    const-string v2, "has_added_signature"

    .line 32
    .line 33
    const/4 v3, 0x0

    .line 34
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0ooOOo()Z

    .line 39
    .line 40
    .line 41
    move-result v4

    .line 42
    if-eqz v4, :cond_8

    .line 43
    .line 44
    const-string v4, "android.intent.extra.TEXT"

    .line 45
    .line 46
    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v5

    .line 50
    iget-object v6, v0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 51
    .line 52
    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    .line 53
    .line 54
    .line 55
    move-result-object v6

    .line 56
    iget-object v7, v0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 57
    .line 58
    const v8, 0x7f131d5c

    .line 59
    .line 60
    .line 61
    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v7

    .line 65
    invoke-interface {v6, v7, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    .line 66
    .line 67
    .line 68
    move-result v6

    .line 69
    sget-object v7, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 70
    .line 71
    new-instance v8, Ljava/lang/StringBuilder;

    .line 72
    .line 73
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    .line 75
    .line 76
    const-string v9, "addExtraForMail() mShareType: "

    .line 77
    .line 78
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    iget-object v9, v0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 82
    .line 83
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    const-string v9, " emailSignatureSwitch "

    .line 87
    .line 88
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    const-string v9, " needShowEmailSignature() "

    .line 95
    .line 96
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-static {}, Lcom/intsig/camscanner/share/ShareHelper;->o〇〇0〇88()Z

    .line 100
    .line 101
    .line 102
    move-result v9

    .line 103
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    const-string v9, " containsShortLink(text) "

    .line 107
    .line 108
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    invoke-static {v5}, Lcom/intsig/camscanner/share/ShareHelper;->oo(Ljava/lang/String;)Ljava/lang/Boolean;

    .line 112
    .line 113
    .line 114
    move-result-object v9

    .line 115
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    const-string v9, " emailContent\u5185\u5bb9\uff1a"

    .line 119
    .line 120
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    const-string v9, " hasAddedSignature\uff1a"

    .line 127
    .line 128
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 132
    .line 133
    .line 134
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 135
    .line 136
    .line 137
    move-result-object v8

    .line 138
    invoke-static {v7, v8}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    if-nez v6, :cond_0

    .line 142
    .line 143
    invoke-static {}, Lcom/intsig/camscanner/share/ShareHelper;->o〇〇0〇88()Z

    .line 144
    .line 145
    .line 146
    move-result v6

    .line 147
    if-eqz v6, :cond_8

    .line 148
    .line 149
    :cond_0
    invoke-static {v5}, Lcom/intsig/camscanner/share/ShareHelper;->oo(Ljava/lang/String;)Ljava/lang/Boolean;

    .line 150
    .line 151
    .line 152
    move-result-object v6

    .line 153
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    .line 154
    .line 155
    .line 156
    move-result v6

    .line 157
    if-nez v6, :cond_8

    .line 158
    .line 159
    if-nez v2, :cond_8

    .line 160
    .line 161
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇ooO8Ooo〇()Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object v2

    .line 165
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 166
    .line 167
    .line 168
    move-result v6

    .line 169
    const-string v7, "\n\n\n"

    .line 170
    .line 171
    const-string v8, ""

    .line 172
    .line 173
    if-eqz v6, :cond_6

    .line 174
    .line 175
    iget-object v2, v0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 176
    .line 177
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 178
    .line 179
    .line 180
    move-result-object v2

    .line 181
    const v6, 0x7f13178b

    .line 182
    .line 183
    .line 184
    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 185
    .line 186
    .line 187
    move-result-object v2

    .line 188
    new-instance v6, Ljava/lang/StringBuilder;

    .line 189
    .line 190
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 191
    .line 192
    .line 193
    const-string v9, "<a href=\'https://v3.camscanner.com/user/downloadOia\' style=\'margin-left:8px;\'>"

    .line 194
    .line 195
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    sget-object v9, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 199
    .line 200
    const v10, 0x7f13178a

    .line 201
    .line 202
    .line 203
    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 204
    .line 205
    .line 206
    move-result-object v9

    .line 207
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    .line 209
    .line 210
    const-string v9, "</a>\n"

    .line 211
    .line 212
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    .line 214
    .line 215
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 216
    .line 217
    .line 218
    move-result-object v6

    .line 219
    invoke-static {v5}, Lcom/intsig/camscanner/share/NormalLinkListUtil;->〇80〇808〇O(Ljava/lang/String;)Z

    .line 220
    .line 221
    .line 222
    move-result v9

    .line 223
    const-string v10, "</br></body></html>"

    .line 224
    .line 225
    const-string v11, "<html><body><br>"

    .line 226
    .line 227
    const-string v12, "</td> </tr> <tr> <td ><a href=https://v3.camscanner.com/user/downloadOia style=\'padding-left: 10px;\'>https://v3.camscanner.com/user/downloadOia</a></td> </tr> </table>"

    .line 228
    .line 229
    const-string v13, "<table border=0 cellpadding=0 cellspacing=0> <tr> <td rowspan=\'2\'> <img src=\'https://ss-static.intsig.net/10000_9c82fc88ded5b89a4078ead8318f27f4.png\' width=\'45px\'  height=\'45px\' /> </td> <td style=\'padding-left: 10px;\'>"

    .line 230
    .line 231
    const-string v14, "</p>"

    .line 232
    .line 233
    const-string v15, "</span><br>"

    .line 234
    .line 235
    const-string v3, "<span style=\'margin-left:8px;\'>"

    .line 236
    .line 237
    move-object/from16 v16, v8

    .line 238
    .line 239
    const-string v8, "\n\n\n<p>"

    .line 240
    .line 241
    const-string v1, "<img src=\'https://ss-static.intsig.net/10000_9c82fc88ded5b89a4078ead8318f27f4.png\' width=\'45px\'  height=\'45px\' style=\'float:left;vertical-align:middle;\' />"

    .line 242
    .line 243
    if-eqz v9, :cond_4

    .line 244
    .line 245
    new-instance v9, Ljava/lang/StringBuilder;

    .line 246
    .line 247
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 248
    .line 249
    .line 250
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 251
    .line 252
    .line 253
    move-result v17

    .line 254
    if-eqz v17, :cond_1

    .line 255
    .line 256
    move-object/from16 v17, v4

    .line 257
    .line 258
    move-object/from16 v4, v16

    .line 259
    .line 260
    goto :goto_0

    .line 261
    :cond_1
    move-object/from16 v17, v4

    .line 262
    .line 263
    move-object v4, v5

    .line 264
    :goto_0
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    .line 266
    .line 267
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    .line 269
    .line 270
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    .line 272
    .line 273
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    .line 275
    .line 276
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    .line 278
    .line 279
    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    .line 281
    .line 282
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    .line 284
    .line 285
    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    .line 287
    .line 288
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 289
    .line 290
    .line 291
    move-result-object v1

    .line 292
    iget-object v3, v0, Lcom/intsig/camscanner/share/ShareHelper;->o8oOOo:Landroid/content/pm/ActivityInfo;

    .line 293
    .line 294
    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 295
    .line 296
    invoke-static {v3}, Lcom/intsig/camscanner/share/NormalLinkListUtil;->OO0o〇〇〇〇0(Ljava/lang/String;)Z

    .line 297
    .line 298
    .line 299
    move-result v3

    .line 300
    if-eqz v3, :cond_3

    .line 301
    .line 302
    new-instance v1, Ljava/lang/StringBuilder;

    .line 303
    .line 304
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 305
    .line 306
    .line 307
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    .line 309
    .line 310
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    .line 312
    .line 313
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    .line 315
    .line 316
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 317
    .line 318
    .line 319
    move-result-object v1

    .line 320
    new-instance v2, Ljava/lang/StringBuilder;

    .line 321
    .line 322
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 323
    .line 324
    .line 325
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 326
    .line 327
    .line 328
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 329
    .line 330
    .line 331
    move-result v3

    .line 332
    if-eqz v3, :cond_2

    .line 333
    .line 334
    move-object/from16 v5, v16

    .line 335
    .line 336
    :cond_2
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    .line 338
    .line 339
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    .line 341
    .line 342
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    .line 344
    .line 345
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    .line 347
    .line 348
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 349
    .line 350
    .line 351
    move-result-object v1

    .line 352
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    .line 353
    .line 354
    .line 355
    move-result-object v1

    .line 356
    move-object/from16 v4, p1

    .line 357
    .line 358
    move-object/from16 v9, v17

    .line 359
    .line 360
    invoke-virtual {v4, v9, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 361
    .line 362
    .line 363
    sget-object v2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 364
    .line 365
    new-instance v3, Ljava/lang/StringBuilder;

    .line 366
    .line 367
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 368
    .line 369
    .line 370
    const-string v5, "content1: "

    .line 371
    .line 372
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    .line 374
    .line 375
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 376
    .line 377
    .line 378
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 379
    .line 380
    .line 381
    move-result-object v1

    .line 382
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    .line 384
    .line 385
    goto/16 :goto_1

    .line 386
    .line 387
    :cond_3
    move-object/from16 v4, p1

    .line 388
    .line 389
    move-object/from16 v9, v17

    .line 390
    .line 391
    invoke-virtual {v4, v9, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 392
    .line 393
    .line 394
    sget-object v2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 395
    .line 396
    new-instance v3, Ljava/lang/StringBuilder;

    .line 397
    .line 398
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 399
    .line 400
    .line 401
    const-string v5, "content2: "

    .line 402
    .line 403
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 404
    .line 405
    .line 406
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 407
    .line 408
    .line 409
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 410
    .line 411
    .line 412
    move-result-object v1

    .line 413
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    .line 415
    .line 416
    goto/16 :goto_1

    .line 417
    .line 418
    :cond_4
    move-object v9, v4

    .line 419
    move-object/from16 v4, p1

    .line 420
    .line 421
    iget-object v5, v0, Lcom/intsig/camscanner/share/ShareHelper;->o8oOOo:Landroid/content/pm/ActivityInfo;

    .line 422
    .line 423
    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 424
    .line 425
    invoke-static {v5}, Lcom/intsig/camscanner/share/NormalLinkListUtil;->OO0o〇〇〇〇0(Ljava/lang/String;)Z

    .line 426
    .line 427
    .line 428
    move-result v5

    .line 429
    if-eqz v5, :cond_5

    .line 430
    .line 431
    new-instance v1, Ljava/lang/StringBuilder;

    .line 432
    .line 433
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 434
    .line 435
    .line 436
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 437
    .line 438
    .line 439
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 440
    .line 441
    .line 442
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 443
    .line 444
    .line 445
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 446
    .line 447
    .line 448
    move-result-object v1

    .line 449
    new-instance v2, Ljava/lang/StringBuilder;

    .line 450
    .line 451
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 452
    .line 453
    .line 454
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 455
    .line 456
    .line 457
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 458
    .line 459
    .line 460
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 461
    .line 462
    .line 463
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 464
    .line 465
    .line 466
    move-result-object v1

    .line 467
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    .line 468
    .line 469
    .line 470
    move-result-object v1

    .line 471
    invoke-virtual {v4, v9, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 472
    .line 473
    .line 474
    sget-object v2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 475
    .line 476
    new-instance v3, Ljava/lang/StringBuilder;

    .line 477
    .line 478
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 479
    .line 480
    .line 481
    const-string v5, "content3: "

    .line 482
    .line 483
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 484
    .line 485
    .line 486
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 487
    .line 488
    .line 489
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 490
    .line 491
    .line 492
    move-result-object v1

    .line 493
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    .line 495
    .line 496
    goto/16 :goto_1

    .line 497
    .line 498
    :cond_5
    new-instance v5, Ljava/lang/StringBuilder;

    .line 499
    .line 500
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 501
    .line 502
    .line 503
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 504
    .line 505
    .line 506
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 507
    .line 508
    .line 509
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 510
    .line 511
    .line 512
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 513
    .line 514
    .line 515
    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 516
    .line 517
    .line 518
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 519
    .line 520
    .line 521
    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 522
    .line 523
    .line 524
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 525
    .line 526
    .line 527
    move-result-object v1

    .line 528
    sget-object v2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 529
    .line 530
    new-instance v3, Ljava/lang/StringBuilder;

    .line 531
    .line 532
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 533
    .line 534
    .line 535
    const-string v5, "displayInEmailContentZone"

    .line 536
    .line 537
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 538
    .line 539
    .line 540
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 541
    .line 542
    .line 543
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 544
    .line 545
    .line 546
    move-result-object v3

    .line 547
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    .line 549
    .line 550
    invoke-virtual {v4, v9, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 551
    .line 552
    .line 553
    sget-object v2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 554
    .line 555
    new-instance v3, Ljava/lang/StringBuilder;

    .line 556
    .line 557
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 558
    .line 559
    .line 560
    const-string v5, "content4: "

    .line 561
    .line 562
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 563
    .line 564
    .line 565
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566
    .line 567
    .line 568
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 569
    .line 570
    .line 571
    move-result-object v1

    .line 572
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    .line 574
    .line 575
    goto :goto_1

    .line 576
    :cond_6
    move-object v9, v4

    .line 577
    move-object/from16 v16, v8

    .line 578
    .line 579
    move-object v4, v1

    .line 580
    new-instance v1, Ljava/lang/StringBuilder;

    .line 581
    .line 582
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 583
    .line 584
    .line 585
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 586
    .line 587
    .line 588
    move-result v3

    .line 589
    if-eqz v3, :cond_7

    .line 590
    .line 591
    move-object/from16 v5, v16

    .line 592
    .line 593
    :cond_7
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 594
    .line 595
    .line 596
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 597
    .line 598
    .line 599
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 600
    .line 601
    .line 602
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 603
    .line 604
    .line 605
    move-result-object v1

    .line 606
    invoke-virtual {v4, v9, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 607
    .line 608
    .line 609
    sget-object v2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 610
    .line 611
    new-instance v3, Ljava/lang/StringBuilder;

    .line 612
    .line 613
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 614
    .line 615
    .line 616
    const-string v5, "content5: "

    .line 617
    .line 618
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 619
    .line 620
    .line 621
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 622
    .line 623
    .line 624
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 625
    .line 626
    .line 627
    move-result-object v1

    .line 628
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    .line 630
    .line 631
    goto :goto_1

    .line 632
    :cond_8
    move-object v4, v1

    .line 633
    :goto_1
    iget-object v1, v0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 634
    .line 635
    sget-object v2, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->EMAIL_MYSELF:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 636
    .line 637
    if-ne v1, v2, :cond_9

    .line 638
    .line 639
    const/4 v1, 0x1

    .line 640
    new-array v1, v1, [Ljava/lang/String;

    .line 641
    .line 642
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo08OO〇0()Ljava/lang/String;

    .line 643
    .line 644
    .line 645
    move-result-object v2

    .line 646
    const/4 v3, 0x0

    .line 647
    aput-object v2, v1, v3

    .line 648
    .line 649
    const-string v2, "android.intent.extra.EMAIL"

    .line 650
    .line 651
    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 652
    .line 653
    .line 654
    sget-object v2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 655
    .line 656
    new-instance v3, Ljava/lang/StringBuilder;

    .line 657
    .line 658
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 659
    .line 660
    .line 661
    const-string v4, "content6: "

    .line 662
    .line 663
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 664
    .line 665
    .line 666
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 667
    .line 668
    .line 669
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 670
    .line 671
    .line 672
    move-result-object v1

    .line 673
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    .line 675
    .line 676
    :cond_9
    return-void
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method public static synthetic OOO〇O0(Lcom/intsig/camscanner/share/ShareHelper;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/ShareHelper$ShareType;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/share/ShareHelper;->Oo08OO8oO(Ljava/util/ArrayList;Lcom/intsig/camscanner/share/ShareHelper$ShareType;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private OOo0O(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/ShareFeiShu;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/intsig/camscanner/share/type/ShareFeiShu;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->FEI_SHU:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareFeiShu;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 6
    .line 7
    invoke-direct {v1, v2, p1}, Lcom/intsig/camscanner/share/type/ShareFeiShu;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 8
    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    invoke-static {p1, v1, v0, p2}, Lcom/intsig/camscanner/share/ShareHelper;->o88O〇8(Landroid/content/Context;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getTitleLink()I

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-eqz p1, :cond_0

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getTitleLink()I

    .line 24
    .line 25
    .line 26
    move-result p2

    .line 27
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/share/type/BaseShare;->OOO(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getActionStr()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    if-nez p1, :cond_1

    .line 43
    .line 44
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getActionStr()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/share/type/BaseShare;->oO(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    :cond_1
    return-object v1
    .line 52
    .line 53
.end method

.method private OOo8o〇O(Ljava/util/List;)Lcom/intsig/camscanner/share/type/ShareLongImage;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/intsig/camscanner/share/type/ShareLongImage;"
        }
    .end annotation

    .line 1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 6
    .line 7
    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    const-string v3, "page_num ASC"

    .line 12
    .line 13
    invoke-static {v2, p1, v3}, Lcom/intsig/camscanner/db/dao/ImageDao;->OOO(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)Ljava/util/ArrayList;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    sget-object v3, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 18
    .line 19
    new-instance v4, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    const-string v5, "createShareLongImage costTime:"

    .line 25
    .line 26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 30
    .line 31
    .line 32
    move-result-wide v5

    .line 33
    sub-long/2addr v5, v0

    .line 34
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareLongImage;

    .line 45
    .line 46
    iget-object v5, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 47
    .line 48
    new-instance v6, Ljava/util/ArrayList;

    .line 49
    .line 50
    invoke-direct {v6, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 51
    .line 52
    .line 53
    const/4 v7, 0x0

    .line 54
    new-instance v8, Lcom/intsig/camscanner/share/data_mode/LongImageShareData;

    .line 55
    .line 56
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 57
    .line 58
    invoke-direct {v8, p1, v2}, Lcom/intsig/camscanner/share/data_mode/LongImageShareData;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    .line 59
    .line 60
    .line 61
    const/4 v9, 0x1

    .line 62
    move-object v4, v0

    .line 63
    invoke-direct/range {v4 .. v9}, Lcom/intsig/camscanner/share/type/ShareLongImage;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/data_mode/LongImageShareData;Z)V

    .line 64
    .line 65
    .line 66
    return-object v0
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private synthetic OOoo(Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/List;Ljava/util/List;Lcom/intsig/camscanner/share/type/BaseShare;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    new-instance v7, Lo〇o8〇〇O/〇〇〇0〇〇0;

    .line 4
    .line 5
    move-object v1, v7

    .line 6
    move-object v2, p0

    .line 7
    move-object v3, p1

    .line 8
    move-object v4, p2

    .line 9
    move-object v5, p3

    .line 10
    move-object v6, p4

    .line 11
    invoke-direct/range {v1 .. v6}, Lo〇o8〇〇O/〇〇〇0〇〇0;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/List;Ljava/util/List;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v7}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private OO〇(Landroid/content/Context;J)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/view/SharePdfCheckDialog;

    .line 2
    .line 3
    const v1, 0x7f140004

    .line 4
    .line 5
    .line 6
    invoke-direct {v0, p1, v1}, Lcom/intsig/camscanner/share/view/SharePdfCheckDialog;-><init>(Landroid/content/Context;I)V

    .line 7
    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/view/SharePdfCheckDialog;->〇8o8o〇(Z)Lcom/intsig/camscanner/share/view/SharePdfCheckDialog;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/view/SharePdfCheckDialog;->〇O8o08O(Z)Lcom/intsig/camscanner/share/view/SharePdfCheckDialog;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-virtual {p1, p2, p3}, Lcom/intsig/camscanner/share/view/SharePdfCheckDialog;->〇〇8O0〇8(J)Lcom/intsig/camscanner/share/view/SharePdfCheckDialog;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    new-instance p2, Lo〇o8〇〇O/〇8〇0〇o〇O;

    .line 23
    .line 24
    invoke-direct {p2, p0}, Lo〇o8〇〇O/〇8〇0〇o〇O;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/share/view/SharePdfCheckDialog;->〇0〇O0088o(Lcom/intsig/camscanner/share/view/SharePdfCheckDialog$ShareItemClickListener;)Lcom/intsig/camscanner/share/view/SharePdfCheckDialog;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :catch_0
    move-exception p1

    .line 36
    sget-object p2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 37
    .line 38
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 39
    .line 40
    .line 41
    :goto_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private synthetic OO〇0008O8(Lcom/intsig/camscanner/share/type/BaseShare;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->〇008〇oo(Lcom/intsig/camscanner/share/type/BaseShare;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private Oo(Z)Z
    .locals 6

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "covertPdfShareFromLinkShare: "

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return v1

    .line 14
    :cond_0
    instance-of v2, v0, Lcom/intsig/camscanner/share/channel/item/LinkShareChannel;

    .line 15
    .line 16
    if-nez v2, :cond_7

    .line 17
    .line 18
    instance-of v2, v0, Lcom/intsig/camscanner/share/channel/item/SendToPcShareChannel;

    .line 19
    .line 20
    if-eqz v2, :cond_1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->〇o〇()Landroid/content/pm/ActivityInfo;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    const/4 v3, 0x1

    .line 28
    if-eqz v2, :cond_3

    .line 29
    .line 30
    iget-object v4, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 31
    .line 32
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 33
    .line 34
    .line 35
    move-result v4

    .line 36
    if-nez v4, :cond_3

    .line 37
    .line 38
    sget-object v4, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 39
    .line 40
    iget-object v5, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 41
    .line 42
    invoke-static {v4, v5}, Lcom/intsig/camscanner/app/AppUtil;->〇8〇0〇o〇O(Landroid/content/Context;Ljava/lang/String;)Z

    .line 43
    .line 44
    .line 45
    move-result v4

    .line 46
    if-nez v4, :cond_3

    .line 47
    .line 48
    if-eqz p1, :cond_2

    .line 49
    .line 50
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 51
    .line 52
    new-array v2, v3, [Ljava/lang/Object;

    .line 53
    .line 54
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->Oooo8o0〇()I

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    aput-object v0, v2, v1

    .line 63
    .line 64
    const v0, 0x7f1316ba

    .line 65
    .line 66
    .line 67
    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    :cond_2
    return v1

    .line 75
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 76
    .line 77
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->ooo0〇O88O(Lcom/intsig/camscanner/share/type/BaseShare;)Lcom/intsig/camscanner/share/type/SharePdf;

    .line 78
    .line 79
    .line 80
    move-result-object p1

    .line 81
    if-eqz p1, :cond_6

    .line 82
    .line 83
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->OO0o〇〇()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 88
    .line 89
    .line 90
    move-result v0

    .line 91
    if-eqz v0, :cond_4

    .line 92
    .line 93
    if-eqz v2, :cond_4

    .line 94
    .line 95
    iget-object v0, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 96
    .line 97
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/BaseShare;->o8oO〇(Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    if-eqz v0, :cond_5

    .line 109
    .line 110
    if-eqz v2, :cond_5

    .line 111
    .line 112
    iget-object v0, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 113
    .line 114
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/BaseShare;->O〇O〇oO(Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    :cond_5
    const/4 v0, 0x0

    .line 118
    iput-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 119
    .line 120
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 121
    .line 122
    .line 123
    return v3

    .line 124
    :cond_6
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 125
    .line 126
    const-string v0, "covertPdfShareFromLinkShare pdfShare is null"

    .line 127
    .line 128
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    :cond_7
    :goto_0
    return v1
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private synthetic Oo08OO8oO(Ljava/util/ArrayList;Lcom/intsig/camscanner/share/ShareHelper$ShareType;I)V
    .locals 11

    .line 1
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    invoke-virtual {p3}, Landroid/app/Activity;->isFinishing()Z

    .line 4
    .line 5
    .line 6
    move-result p3

    .line 7
    if-eqz p3, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    invoke-direct {p0, p3, p1, v0}, Lcom/intsig/camscanner/share/ShareHelper;->O〇0(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    .line 14
    .line 15
    .line 16
    move-result p3

    .line 17
    if-eqz p3, :cond_1

    .line 18
    .line 19
    return-void

    .line 20
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    .line 21
    .line 22
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 23
    .line 24
    .line 25
    new-instance p3, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 28
    .line 29
    invoke-direct {p3, v1, p1}, Lcom/intsig/camscanner/share/type/SharePdf;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 30
    .line 31
    .line 32
    new-instance v1, Lo〇o8〇〇O/o〇8oOO88;

    .line 33
    .line 34
    invoke-direct {v1, p0}, Lo〇o8〇〇O/o〇8oOO88;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p3, v1}, Lcom/intsig/camscanner/share/type/SharePdf;->〇08〇0〇o〇8(Lcom/intsig/camscanner/share/type/SharePdf$OnTryDeductionListener;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    .line 42
    .line 43
    new-instance p3, Lcom/intsig/camscanner/share/type/ShareImage;

    .line 44
    .line 45
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 46
    .line 47
    invoke-direct {p3, v1, p1}, Lcom/intsig/camscanner/share/type/ShareImage;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OOo8o〇O(Ljava/util/List;)Lcom/intsig/camscanner/share/type/ShareLongImage;

    .line 54
    .line 55
    .line 56
    move-result-object p3

    .line 57
    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    .line 59
    .line 60
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 61
    .line 62
    .line 63
    move-result p3

    .line 64
    const/4 v9, 0x1

    .line 65
    const/4 v10, 0x0

    .line 66
    if-ne p3, v9, :cond_3

    .line 67
    .line 68
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 69
    .line 70
    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 71
    .line 72
    .line 73
    move-result-object p3

    .line 74
    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    check-cast v1, Ljava/lang/Long;

    .line 79
    .line 80
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    .line 81
    .line 82
    .line 83
    move-result-wide v1

    .line 84
    invoke-static {p3, v1, v2}, Lcom/intsig/camscanner/tsapp/collaborate/CollaborateUtil;->O8(Landroid/content/Context;J)I

    .line 85
    .line 86
    .line 87
    move-result p3

    .line 88
    if-eq p3, v9, :cond_3

    .line 89
    .line 90
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇8〇0〇o〇O()Z

    .line 91
    .line 92
    .line 93
    move-result p3

    .line 94
    if-eqz p3, :cond_2

    .line 95
    .line 96
    sget-object p3, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->EMAIL_MYSELF:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 97
    .line 98
    if-ne p2, p3, :cond_2

    .line 99
    .line 100
    new-instance p2, Lcom/intsig/camscanner/share/type/ShareToWord;

    .line 101
    .line 102
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 103
    .line 104
    invoke-direct {p2, p3, p1, v0}, Lcom/intsig/camscanner/share/type/ShareToWord;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 105
    .line 106
    .line 107
    const p3, 0x7f080c5f

    .line 108
    .line 109
    .line 110
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/share/type/BaseShare;->oO00OOO(I)V

    .line 111
    .line 112
    .line 113
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 114
    .line 115
    const v0, 0x7f130e3c

    .line 116
    .line 117
    .line 118
    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object p3

    .line 122
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    .line 127
    .line 128
    goto :goto_0

    .line 129
    :cond_2
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 130
    .line 131
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 132
    .line 133
    .line 134
    move-result-object p2

    .line 135
    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 136
    .line 137
    .line 138
    move-result-object p3

    .line 139
    check-cast p3, Ljava/lang/Long;

    .line 140
    .line 141
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    .line 142
    .line 143
    .line 144
    move-result-wide v0

    .line 145
    invoke-static {p2, v0, v1}, Lcom/intsig/camscanner/db/dao/ImageDao;->o8oO〇(Landroid/content/Context;J)Ljava/util/ArrayList;

    .line 146
    .line 147
    .line 148
    move-result-object p2

    .line 149
    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 150
    .line 151
    .line 152
    move-result-object p3

    .line 153
    check-cast p3, Ljava/lang/Long;

    .line 154
    .line 155
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    .line 156
    .line 157
    .line 158
    move-result-wide v0

    .line 159
    invoke-direct {p0, v3, v0, v1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->〇O(Ljava/util/List;JLjava/util/List;)V

    .line 160
    .line 161
    .line 162
    :cond_3
    :goto_0
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 163
    .line 164
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->O8(Ljava/util/ArrayList;)Z

    .line 165
    .line 166
    .line 167
    move-result p2

    .line 168
    if-eqz p2, :cond_c

    .line 169
    .line 170
    new-instance p2, Lcom/intsig/camscanner/share/type/ShareNormalLink;

    .line 171
    .line 172
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 173
    .line 174
    invoke-direct {p2, p3, p1}, Lcom/intsig/camscanner/share/type/ShareNormalLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 175
    .line 176
    .line 177
    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    .line 179
    .line 180
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 181
    .line 182
    sget-object p3, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->DEFAULT:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 183
    .line 184
    if-ne p2, p3, :cond_c

    .line 185
    .line 186
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 187
    .line 188
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 189
    .line 190
    .line 191
    move-result v0

    .line 192
    if-le v0, v9, :cond_4

    .line 193
    .line 194
    const/4 v0, 0x1

    .line 195
    goto :goto_1

    .line 196
    :cond_4
    const/4 v0, 0x0

    .line 197
    :goto_1
    invoke-static {p2, v0}, Lcom/intsig/camscanner/share/NormalLinkListUtil;->〇080(Landroid/content/Context;Z)Landroid/util/SparseArray;

    .line 198
    .line 199
    .line 200
    move-result-object p2

    .line 201
    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    .line 202
    .line 203
    .line 204
    move-result v0

    .line 205
    invoke-static {p2}, Lcom/intsig/camscanner/share/NormalLinkListUtil;->〇〇888(Landroid/util/SparseArray;)I

    .line 206
    .line 207
    .line 208
    move-result v1

    .line 209
    if-lt v0, v1, :cond_7

    .line 210
    .line 211
    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    .line 212
    .line 213
    .line 214
    move-result p3

    .line 215
    sub-int/2addr p3, v9

    .line 216
    :goto_2
    if-ltz p3, :cond_b

    .line 217
    .line 218
    invoke-virtual {p2, p3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    .line 219
    .line 220
    .line 221
    move-result-object v0

    .line 222
    check-cast v0, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 223
    .line 224
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->〇00O0O0(Ljava/util/ArrayList;)Z

    .line 225
    .line 226
    .line 227
    move-result v1

    .line 228
    if-eqz v1, :cond_5

    .line 229
    .line 230
    sget-object v1, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->WE_CHAT:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 231
    .line 232
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 233
    .line 234
    .line 235
    move-result v1

    .line 236
    if-eqz v1, :cond_5

    .line 237
    .line 238
    invoke-virtual {p0, v0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O8O〇88oO0(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;

    .line 239
    .line 240
    .line 241
    move-result-object v0

    .line 242
    invoke-virtual {v3, v10, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 243
    .line 244
    .line 245
    goto :goto_3

    .line 246
    :cond_5
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->〇0O〇Oo(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Z

    .line 247
    .line 248
    .line 249
    move-result v1

    .line 250
    if-eqz v1, :cond_6

    .line 251
    .line 252
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O880oOO08(Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/ShareFeiShu;

    .line 253
    .line 254
    .line 255
    move-result-object v0

    .line 256
    invoke-virtual {v3, v10, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 257
    .line 258
    .line 259
    goto :goto_3

    .line 260
    :cond_6
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O0OO8〇0(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;

    .line 261
    .line 262
    .line 263
    move-result-object v0

    .line 264
    invoke-virtual {v3, v10, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 265
    .line 266
    .line 267
    :goto_3
    add-int/lit8 p3, p3, -0x1

    .line 268
    .line 269
    goto :goto_2

    .line 270
    :cond_7
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 271
    .line 272
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareDataPresenter;->o〇0()Z

    .line 273
    .line 274
    .line 275
    move-result p2

    .line 276
    if-eqz p2, :cond_8

    .line 277
    .line 278
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareWhatsApp;

    .line 279
    .line 280
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 281
    .line 282
    invoke-direct {v0, v1, p1}, Lcom/intsig/camscanner/share/type/ShareWhatsApp;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 283
    .line 284
    .line 285
    invoke-virtual {v3, v10, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 286
    .line 287
    .line 288
    :cond_8
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 289
    .line 290
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->Oo08(Ljava/util/ArrayList;)Z

    .line 291
    .line 292
    .line 293
    move-result v0

    .line 294
    if-eqz v0, :cond_b

    .line 295
    .line 296
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 297
    .line 298
    if-ne v0, p3, :cond_b

    .line 299
    .line 300
    sget-object p3, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->WE_CHAT:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 301
    .line 302
    invoke-virtual {p0, p3, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O8O〇88oO0(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;

    .line 303
    .line 304
    .line 305
    move-result-object p3

    .line 306
    if-eqz p2, :cond_a

    .line 307
    .line 308
    invoke-static {}, Lcom/intsig/camscanner/app/AppUtil;->o〇8oOO88()Z

    .line 309
    .line 310
    .line 311
    move-result p2

    .line 312
    if-nez p2, :cond_a

    .line 313
    .line 314
    invoke-static {}, Lcom/intsig/camscanner/app/AppUtil;->o〇O()Z

    .line 315
    .line 316
    .line 317
    move-result p2

    .line 318
    if-eqz p2, :cond_9

    .line 319
    .line 320
    goto :goto_4

    .line 321
    :cond_9
    const/4 p2, 0x1

    .line 322
    goto :goto_5

    .line 323
    :cond_a
    :goto_4
    const/4 p2, 0x0

    .line 324
    :goto_5
    invoke-virtual {v3, p2, p3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 325
    .line 326
    .line 327
    :cond_b
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 328
    .line 329
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇o〇(Ljava/util/ArrayList;)Z

    .line 330
    .line 331
    .line 332
    move-result p2

    .line 333
    if-eqz p2, :cond_c

    .line 334
    .line 335
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 336
    .line 337
    invoke-static {p2, p1}, Lcom/intsig/camscanner/share/type/SendToPc;->Oo〇O(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/SendToPc;

    .line 338
    .line 339
    .line 340
    move-result-object p2

    .line 341
    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 342
    .line 343
    .line 344
    :cond_c
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 345
    .line 346
    .line 347
    move-result p2

    .line 348
    if-le p2, v9, :cond_d

    .line 349
    .line 350
    new-instance p2, Lcom/intsig/camscanner/share/type/ShareOcrText;

    .line 351
    .line 352
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 353
    .line 354
    invoke-direct {p2, p3, p1}, Lcom/intsig/camscanner/share/type/ShareOcrText;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 355
    .line 356
    .line 357
    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    .line 359
    .line 360
    :cond_d
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 361
    .line 362
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 363
    .line 364
    iget-object v5, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 365
    .line 366
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO8〇(Ljava/util/ArrayList;)Z

    .line 367
    .line 368
    .line 369
    move-result v6

    .line 370
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 371
    .line 372
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 373
    .line 374
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 375
    .line 376
    invoke-virtual {p2, p3, v0}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇〇808〇(Landroid/app/Activity;Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;)Ljava/lang/String;

    .line 377
    .line 378
    .line 379
    move-result-object v7

    .line 380
    move-object v4, p0

    .line 381
    move-object v8, p1

    .line 382
    invoke-interface/range {v1 .. v8}, Lcom/intsig/camscanner/share/listener/ShareUiInterface;->o〇0(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareTypeClickListener;Lcom/intsig/camscanner/share/ShareHelper$ShareType;ZLjava/lang/String;Ljava/util/ArrayList;)V

    .line 383
    .line 384
    .line 385
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 386
    .line 387
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 388
    .line 389
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 390
    .line 391
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 392
    .line 393
    .line 394
    move-result p1

    .line 395
    if-ne p1, v9, :cond_e

    .line 396
    .line 397
    goto :goto_6

    .line 398
    :cond_e
    const/4 v9, 0x0

    .line 399
    :goto_6
    invoke-virtual {p2, p3, v0, v9}, Lcom/intsig/camscanner/share/ShareDataPresenter;->oo〇(Landroid/app/Activity;Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;Z)V

    .line 400
    .line 401
    .line 402
    return-void
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private Oo0oOo〇0()V
    .locals 4

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    :try_start_0
    const-string v1, "from_part"

    .line 7
    .line 8
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 9
    .line 10
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 11
    .line 12
    .line 13
    const-string v1, "from"

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->O〇08oOOO0:Ljava/lang/String;

    .line 16
    .line 17
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 18
    .line 19
    .line 20
    const-string v1, "CSApplicationList"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :catch_0
    move-exception v1

    .line 27
    sget-object v2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 28
    .line 29
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 30
    .line 31
    .line 32
    :goto_0
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->〇8〇0〇o〇O()Z

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    if-eqz v1, :cond_0

    .line 37
    .line 38
    sget-object v1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 39
    .line 40
    new-instance v2, Ljava/lang/StringBuilder;

    .line 41
    .line 42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    .line 44
    .line 45
    const-string v3, "fromPartObject="

    .line 46
    .line 47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    :cond_0
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private Oo0oO〇O〇O()Z
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/ShareDoneManager;->o〇O()Lcom/intsig/advertisement/adapters/positions/ShareDoneManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->OOO〇O0(I)Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    if-eqz v2, :cond_0

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    invoke-virtual {v2}, Lcom/intsig/advertisement/params/RequestParam;->〇〇8O0〇8()Lcom/intsig/advertisement/enums/SourceType;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    sget-object v3, Lcom/intsig/advertisement/enums/SourceType;->Tencent:Lcom/intsig/advertisement/enums/SourceType;

    .line 27
    .line 28
    if-ne v2, v3, :cond_0

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/RequestParam;->〇o00〇〇Oo()Lcom/intsig/advertisement/enums/AdType;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    sget-object v2, Lcom/intsig/advertisement/enums/AdType;->Interstitial:Lcom/intsig/advertisement/enums/AdType;

    .line 39
    .line 40
    if-ne v0, v2, :cond_0

    .line 41
    .line 42
    const/4 v1, 0x1

    .line 43
    :cond_0
    return v1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static synthetic Oo8Oo00oo(Lcom/intsig/camscanner/share/ShareLinkLogger;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/share/ShareHelper;->o0(Lcom/intsig/camscanner/share/ShareLinkLogger;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic OoO8(Lcom/intsig/camscanner/share/ShareHelper;Ljava/lang/ref/WeakReference;JLjava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lcom/intsig/camscanner/share/ShareHelper;->〇o8OO0(Ljava/lang/ref/WeakReference;JLjava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method public static OoOOo8(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/ShareHelper$ShareType;Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/intsig/camscanner/share/ShareHelper$ShareType;",
            "Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;",
            "Lcom/intsig/camscanner/share/listener/ShareBackListener;",
            ")V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/ShareHelper;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/share/ShareHelper;->o08〇〇0O(Lcom/intsig/camscanner/share/ShareHelper$ShareType;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/share/ShareHelper;->OOo88OOo(Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;)V

    .line 10
    .line 11
    .line 12
    if-eqz p3, :cond_0

    .line 13
    .line 14
    invoke-interface {p3, v0}, Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;->〇o00〇〇Oo(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    invoke-direct {v0, p1, p4, p2}, Lcom/intsig/camscanner/share/ShareHelper;->〇080O0(Ljava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackListener;Lcom/intsig/camscanner/share/ShareHelper$ShareType;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method private synthetic OoO〇(Ljava/util/ArrayList;I)V
    .locals 8

    .line 1
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    invoke-virtual {p2}, Landroid/app/Activity;->isFinishing()Z

    .line 4
    .line 5
    .line 6
    move-result p2

    .line 7
    if-eqz p2, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    invoke-direct {p0, p2, p1, v0}, Lcom/intsig/camscanner/share/ShareHelper;->O〇0(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    .line 14
    .line 15
    .line 16
    move-result p2

    .line 17
    if-eqz p2, :cond_1

    .line 18
    .line 19
    return-void

    .line 20
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    .line 21
    .line 22
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 23
    .line 24
    .line 25
    new-instance p2, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 28
    .line 29
    invoke-direct {p2, v0, p1}, Lcom/intsig/camscanner/share/type/SharePdf;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    .line 34
    .line 35
    new-instance p2, Lcom/intsig/camscanner/share/type/ShareImage;

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 38
    .line 39
    invoke-direct {p2, v0, p1}, Lcom/intsig/camscanner/share/type/ShareImage;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 46
    .line 47
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 48
    .line 49
    iget-object v4, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 50
    .line 51
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO8〇(Ljava/util/ArrayList;)Z

    .line 52
    .line 53
    .line 54
    move-result v5

    .line 55
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 56
    .line 57
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 58
    .line 59
    iget-object v6, p0, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 60
    .line 61
    invoke-virtual {p2, v3, v6}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇〇808〇(Landroid/app/Activity;Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v6

    .line 65
    move-object v3, p0

    .line 66
    move-object v7, p1

    .line 67
    invoke-interface/range {v0 .. v7}, Lcom/intsig/camscanner/share/listener/ShareUiInterface;->o〇0(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareTypeClickListener;Lcom/intsig/camscanner/share/ShareHelper$ShareType;ZLjava/lang/String;Ljava/util/ArrayList;)V

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method static bridge synthetic Ooo(Lcom/intsig/camscanner/share/ShareHelper;)Lcom/intsig/camscanner/share/dialog/SyncDialogClient;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/share/ShareHelper;->Oo80:Lcom/intsig/camscanner/share/dialog/SyncDialogClient;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static Ooo8(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;Z",
            "Lcom/intsig/camscanner/share/listener/ShareBackListener;",
            ")V"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-static {p0, p1, p2, v0, p3}, Lcom/intsig/camscanner/share/ShareHelper;->Oo〇(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/SharePreviousInterceptor;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private Ooo8〇〇(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/share/type/BaseShare;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-eqz p2, :cond_6

    .line 2
    .line 3
    if-eqz p3, :cond_6

    .line 4
    .line 5
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iget-object v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->share_dir_v2:Lcom/intsig/tsapp/sync/AppConfigJson$ShareDirV2;

    .line 17
    .line 18
    if-nez v0, :cond_1

    .line 19
    .line 20
    return-void

    .line 21
    :cond_1
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/share/ShareHelper;->O0o(Ljava/util/ArrayList;)Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    if-nez v1, :cond_2

    .line 26
    .line 27
    return-void

    .line 28
    :cond_2
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    const/4 v2, 0x1

    .line 33
    if-ne v1, v2, :cond_3

    .line 34
    .line 35
    iget v1, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ShareDirV2;->show_single_share_entry:I

    .line 36
    .line 37
    if-eq v1, v2, :cond_4

    .line 38
    .line 39
    :cond_3
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    if-le v1, v2, :cond_6

    .line 44
    .line 45
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ShareDirV2;->show_multi_share_entry:I

    .line 46
    .line 47
    if-ne v0, v2, :cond_6

    .line 48
    .line 49
    :cond_4
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareInviteMembers;

    .line 50
    .line 51
    invoke-direct {v0, p1, p3}, Lcom/intsig/camscanner/share/type/ShareInviteMembers;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 52
    .line 53
    .line 54
    if-eqz p4, :cond_5

    .line 55
    .line 56
    invoke-virtual {v0, p4}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇8oOO88(Ljava/util/ArrayList;)V

    .line 57
    .line 58
    .line 59
    :cond_5
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    .line 61
    .line 62
    :cond_6
    :goto_0
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public static Oo〇(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/SharePreviousInterceptor;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;Z",
            "Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;",
            "Lcom/intsig/camscanner/share/listener/ShareBackListener;",
            ")V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/ShareHelper;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->DEFAULT:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/ShareHelper;->o08〇〇0O(Lcom/intsig/camscanner/share/ShareHelper$ShareType;)V

    .line 9
    .line 10
    .line 11
    if-eqz p1, :cond_0

    .line 12
    .line 13
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    const/4 v2, 0x1

    .line 18
    if-ne v1, v2, :cond_0

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    check-cast v1, Ljava/lang/Long;

    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    .line 28
    .line 29
    .line 30
    move-result-wide v1

    .line 31
    invoke-static {p0, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇O〇80o08O(Landroid/content/Context;J)I

    .line 32
    .line 33
    .line 34
    move-result p0

    .line 35
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper;->〇8O0O808〇(I)V

    .line 36
    .line 37
    .line 38
    :cond_0
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/share/ShareHelper;->OOo88OOo(Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;)V

    .line 39
    .line 40
    .line 41
    if-eqz p3, :cond_1

    .line 42
    .line 43
    invoke-interface {p3, v0}, Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;->〇o00〇〇Oo(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 44
    .line 45
    .line 46
    :cond_1
    const/4 v4, 0x0

    .line 47
    const/4 v5, 0x0

    .line 48
    move-object v1, p1

    .line 49
    move v2, p2

    .line 50
    move-object v3, p4

    .line 51
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/share/ShareHelper;->O80〇O〇080(Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackListener;Lcom/intsig/camscanner/share/listener/ShareBackDataListener;Z)V

    .line 52
    .line 53
    .line 54
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method static bridge synthetic Oo〇O(Lcom/intsig/camscanner/share/ShareHelper;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O0〇oo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private Oo〇o(Landroid/content/Context;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;

    .line 7
    .line 8
    invoke-direct {v1}, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;-><init>()V

    .line 9
    .line 10
    .line 11
    sget-object v2, Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;->IMAGE:Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;

    .line 12
    .line 13
    iput-object v2, v1, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇080:Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;

    .line 14
    .line 15
    const v2, 0x7f1301c7

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    iput-object v2, v1, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇o00〇〇Oo:Ljava/lang/String;

    .line 23
    .line 24
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    .line 26
    .line 27
    new-instance v1, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;

    .line 28
    .line 29
    invoke-direct {v1}, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;-><init>()V

    .line 30
    .line 31
    .line 32
    sget-object v2, Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;->IMAGE_SECURITY_MARK:Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;

    .line 33
    .line 34
    iput-object v2, v1, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇080:Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;

    .line 35
    .line 36
    const/4 v2, 0x1

    .line 37
    iput-boolean v2, v1, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇o〇:Z

    .line 38
    .line 39
    const v2, 0x7f13062e

    .line 40
    .line 41
    .line 42
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    iput-object p1, v1, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇o00〇〇Oo:Ljava/lang/String;

    .line 47
    .line 48
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    .line 50
    .line 51
    return-object v0
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static O〇(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/intsig/camscanner/share/listener/ShareBackListener;",
            ")V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/ShareHelper;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 4
    .line 5
    .line 6
    sget-object p0, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->DEFAULT:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 7
    .line 8
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper;->o08〇〇0O(Lcom/intsig/camscanner/share/ShareHelper$ShareType;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {v0, p1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->〇〇〇(Ljava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private O〇0(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 4
    .param p2    # Ljava/util/ArrayList;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/ArrayList;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-le v0, v1, :cond_0

    .line 9
    .line 10
    const/4 p1, 0x0

    .line 11
    return p1

    .line 12
    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 13
    .line 14
    if-eqz p3, :cond_1

    .line 15
    .line 16
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 17
    .line 18
    invoke-static {v0, p3}, Lcom/intsig/camscanner/db/dao/ImageDao;->o〇o(Landroid/content/Context;Ljava/util/ArrayList;)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    goto :goto_0

    .line 27
    :cond_1
    if-eqz p2, :cond_2

    .line 28
    .line 29
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 30
    .line 31
    invoke-static {v0, p2}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇000O0(Landroid/content/Context;Ljava/util/ArrayList;)Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    :cond_2
    :goto_0
    sget-object v1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 40
    .line 41
    new-instance v2, Ljava/lang/StringBuilder;

    .line 42
    .line 43
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 44
    .line 45
    .line 46
    const-string v3, "is share pad = "

    .line 47
    .line 48
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    if-eqz v1, :cond_3

    .line 66
    .line 67
    new-instance v1, Ljava/util/ArrayList;

    .line 68
    .line 69
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .line 71
    .line 72
    new-instance v2, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 73
    .line 74
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 75
    .line 76
    invoke-direct {v2, v3, p2, p3}, Lcom/intsig/camscanner/share/type/SharePdf;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    .line 81
    .line 82
    new-instance v2, Lcom/intsig/camscanner/share/type/ShareImage;

    .line 83
    .line 84
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 85
    .line 86
    invoke-direct {v2, v3, p2, p3}, Lcom/intsig/camscanner/share/type/ShareImage;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    .line 91
    .line 92
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 93
    .line 94
    invoke-interface {p2, p1, v1, p0}, Lcom/intsig/camscanner/share/listener/ShareUiInterface;->〇80〇808〇O(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareTypeClickListener;)V

    .line 95
    .line 96
    .line 97
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 98
    .line 99
    .line 100
    move-result p1

    .line 101
    return p1
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static O〇08(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/share/type/BaseShare;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/ShareHelper;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 4
    .line 5
    .line 6
    new-instance v1, Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    const/4 v3, 0x0

    .line 16
    const/4 v4, 0x1

    .line 17
    if-le v2, v4, :cond_0

    .line 18
    .line 19
    const/4 v2, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v2, 0x0

    .line 22
    :goto_0
    invoke-static {p0, v2}, Lcom/intsig/camscanner/share/NormalLinkListUtil;->〇080(Landroid/content/Context;Z)Landroid/util/SparseArray;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    .line 27
    .line 28
    .line 29
    move-result v5

    .line 30
    const/4 v6, 0x2

    .line 31
    const v7, 0x7f080c3e

    .line 32
    .line 33
    .line 34
    const v8, 0x7f080c52

    .line 35
    .line 36
    .line 37
    if-lt v5, v6, :cond_4

    .line 38
    .line 39
    :goto_1
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    .line 40
    .line 41
    .line 42
    move-result v5

    .line 43
    if-ge v3, v5, :cond_5

    .line 44
    .line 45
    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    .line 46
    .line 47
    .line 48
    move-result-object v5

    .line 49
    check-cast v5, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 50
    .line 51
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->〇00O0O0(Ljava/util/ArrayList;)Z

    .line 52
    .line 53
    .line 54
    move-result v6

    .line 55
    if-eqz v6, :cond_1

    .line 56
    .line 57
    sget-object v6, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->WE_CHAT:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 58
    .line 59
    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 60
    .line 61
    .line 62
    move-result v6

    .line 63
    if-eqz v6, :cond_1

    .line 64
    .line 65
    invoke-virtual {v0, v5, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O8O〇88oO0(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;

    .line 66
    .line 67
    .line 68
    move-result-object v5

    .line 69
    invoke-virtual {v5, v8}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    .line 74
    .line 75
    goto :goto_2

    .line 76
    :cond_1
    invoke-direct {v0, v5, p1}, Lcom/intsig/camscanner/share/ShareHelper;->〇0O〇Oo(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Z

    .line 77
    .line 78
    .line 79
    move-result v6

    .line 80
    if-eqz v6, :cond_2

    .line 81
    .line 82
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O880oOO08(Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/ShareFeiShu;

    .line 83
    .line 84
    .line 85
    move-result-object v5

    .line 86
    const v6, 0x7f080c1c

    .line 87
    .line 88
    .line 89
    invoke-virtual {v5, v6}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    .line 94
    .line 95
    goto :goto_2

    .line 96
    :cond_2
    invoke-direct {v0, v5, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O0OO8〇0(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;

    .line 97
    .line 98
    .line 99
    move-result-object v6

    .line 100
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 101
    .line 102
    .line 103
    move-result v9

    .line 104
    if-ne v9, v4, :cond_3

    .line 105
    .line 106
    sget-object v9, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->QQ:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 107
    .line 108
    if-ne v5, v9, :cond_3

    .line 109
    .line 110
    invoke-virtual {v6, v7}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 111
    .line 112
    .line 113
    :cond_3
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    .line 115
    .line 116
    :goto_2
    add-int/lit8 v3, v3, 0x1

    .line 117
    .line 118
    goto :goto_1

    .line 119
    :cond_4
    sget-object v2, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->WE_CHAT:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 120
    .line 121
    invoke-virtual {v0, v2, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O8O〇88oO0(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;

    .line 122
    .line 123
    .line 124
    move-result-object v2

    .line 125
    invoke-virtual {v2, v8}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 126
    .line 127
    .line 128
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    .line 130
    .line 131
    sget-object v2, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->QQ:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 132
    .line 133
    invoke-direct {v0, v2, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O0OO8〇0(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;

    .line 134
    .line 135
    .line 136
    move-result-object v2

    .line 137
    invoke-virtual {v2, v7}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    .line 142
    .line 143
    :cond_5
    iget-object v0, v0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 144
    .line 145
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇o〇(Ljava/util/ArrayList;)Z

    .line 146
    .line 147
    .line 148
    move-result v0

    .line 149
    if-eqz v0, :cond_6

    .line 150
    .line 151
    invoke-static {p0, p1}, Lcom/intsig/camscanner/share/type/SendToPc;->Oo〇O(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/SendToPc;

    .line 152
    .line 153
    .line 154
    move-result-object v0

    .line 155
    const v2, 0x7f080c43

    .line 156
    .line 157
    .line 158
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 159
    .line 160
    .line 161
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    .line 163
    .line 164
    :cond_6
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareCopyLink;

    .line 165
    .line 166
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/share/type/ShareCopyLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 167
    .line 168
    .line 169
    const v2, 0x7f080c07

    .line 170
    .line 171
    .line 172
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 173
    .line 174
    .line 175
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    .line 177
    .line 178
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareNormalLink;

    .line 179
    .line 180
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/share/type/ShareNormalLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 181
    .line 182
    .line 183
    const p1, 0x7f080c2f

    .line 184
    .line 185
    .line 186
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 187
    .line 188
    .line 189
    const p1, 0x7f130848

    .line 190
    .line 191
    .line 192
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 193
    .line 194
    .line 195
    move-result-object p0

    .line 196
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 197
    .line 198
    .line 199
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/ShareNormalLink;->ooOO()V

    .line 200
    .line 201
    .line 202
    const-string p0, "more"

    .line 203
    .line 204
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->oO(Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 208
    .line 209
    .line 210
    return-object v1
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private synthetic O〇0〇o808〇(Landroid/content/Intent;)V
    .locals 0

    .line 1
    const/4 p1, 0x1

    .line 2
    iput-boolean p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇080OO8〇0:Z

    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O0〇oo()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic O〇8O8〇008(Lcom/intsig/camscanner/share/ShareHelper;Landroid/content/Intent;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O0oo0o0〇(Landroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static synthetic O〇8oOo8O(Lcom/intsig/camscanner/share/ShareLinkLogger;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    if-eqz p0, :cond_0

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/ShareLinkLogger;->〇o〇()V

    .line 4
    .line 5
    .line 6
    :cond_0
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static O〇OO()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/utils/SharedPreferencesHelper;->O8()Lcom/intsig/utils/SharedPreferencesHelper;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "last_share_type"

    .line 6
    .line 7
    const-string v2, ""

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/utils/SharedPreferencesHelper;->〇〇888(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic O〇Oo(Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackDataListener;I)V
    .locals 1

    .line 1
    iget-object p4, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    invoke-direct {p0, p4, p1, v0}, Lcom/intsig/camscanner/share/ShareHelper;->O〇0(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    .line 5
    .line 6
    .line 7
    move-result p4

    .line 8
    if-eqz p4, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/share/ShareHelper;->O88〇〇o0O(Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackDataListener;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method static bridge synthetic O〇O〇oO(Lcom/intsig/camscanner/share/ShareHelper;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private O〇〇(Landroid/app/Activity;Lcom/intsig/camscanner/share/type/BaseShare;)V
    .locals 2

    .line 1
    iget-boolean p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇〇08O:Z

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    iput-boolean p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇〇08O:Z

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 14
    .line 15
    invoke-virtual {p1, v0, p2, v1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->O〇8O8〇008(Landroid/app/Activity;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;)V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static synthetic o0(Lcom/intsig/camscanner/share/ShareLinkLogger;)V
    .locals 1

    .line 1
    if-eqz p0, :cond_0

    .line 2
    .line 3
    const-string v0, "sync_compliance_not_confirm"

    .line 4
    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/share/ShareLinkLogger;->OO0o〇〇(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private o08O()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "back from shareAPP"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSShare"

    .line 9
    .line 10
    const-string v1, "share_and_stay"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    invoke-static {v0}, Lcom/intsig/camscanner/share/ShareRecorder;->O8(I)V

    .line 17
    .line 18
    .line 19
    new-instance v0, Lcom/intsig/camscanner/guide/markguide/CnGuideMarkControl;

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 22
    .line 23
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/guide/markguide/CnGuideMarkControl;-><init>(Landroid/app/Activity;)V

    .line 24
    .line 25
    .line 26
    new-instance v1, Lcom/intsig/camscanner/guide/markguide/GpGuideMarkControl;

    .line 27
    .line 28
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 29
    .line 30
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/guide/markguide/GpGuideMarkControl;-><init>(Landroid/app/Activity;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/markguide/CnGuideMarkControl;->O8()Z

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    if-eqz v2, :cond_0

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/markguide/CnGuideMarkControl;->〇80〇808〇O()V

    .line 40
    .line 41
    .line 42
    goto/16 :goto_1

    .line 43
    .line 44
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/camscanner/guide/markguide/GpGuideMarkControl;->Oo08()Z

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    if-eqz v0, :cond_1

    .line 49
    .line 50
    invoke-virtual {v1}, Lcom/intsig/camscanner/guide/markguide/GpGuideMarkControl;->〇O8o08O()V

    .line 51
    .line 52
    .line 53
    goto/16 :goto_1

    .line 54
    .line 55
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇o0O:Z

    .line 56
    .line 57
    if-eqz v0, :cond_2

    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o〇00O:Lcom/intsig/camscanner/share/listener/ShareBackListener;

    .line 60
    .line 61
    if-eqz v0, :cond_2

    .line 62
    .line 63
    invoke-interface {v0}, Lcom/intsig/camscanner/share/listener/ShareBackListener;->〇080()V

    .line 64
    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 68
    .line 69
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 70
    .line 71
    iget v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOO〇〇:I

    .line 72
    .line 73
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/paper/PaperUtil;->〇080(Landroidx/fragment/app/FragmentActivity;I)Z

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    if-eqz v0, :cond_3

    .line 78
    .line 79
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 80
    .line 81
    const/4 v1, 0x0

    .line 82
    invoke-static {v0, v1}, Lcom/intsig/camscanner/question/QuestionDialogUtil;->〇o00〇〇Oo(Landroid/app/Activity;Landroid/content/DialogInterface$OnDismissListener;)V

    .line 83
    .line 84
    .line 85
    goto :goto_1

    .line 86
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/purchase/vipmonth/share_done/ShareDoneVipMonthManager;->Oo08()Z

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    if-eqz v0, :cond_4

    .line 91
    .line 92
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 93
    .line 94
    invoke-static {v0}, Lcom/intsig/camscanner/purchase/vipmonth/share_done/ShareDoneVipMonthManager;->Oooo8o0〇(Landroidx/fragment/app/FragmentActivity;)V

    .line 95
    .line 96
    .line 97
    goto :goto_1

    .line 98
    :cond_4
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/ShareDoneManager;->o〇O()Lcom/intsig/advertisement/adapters/positions/ShareDoneManager;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/positions/ShareDoneManager;->o8oO〇()V

    .line 103
    .line 104
    .line 105
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8oOOo:Landroid/content/pm/ActivityInfo;

    .line 106
    .line 107
    if-eqz v0, :cond_5

    .line 108
    .line 109
    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 110
    .line 111
    invoke-static {v0}, Lcom/intsig/camscanner/app/IntentUtil;->〇8(Ljava/lang/String;)Z

    .line 112
    .line 113
    .line 114
    move-result v0

    .line 115
    if-eqz v0, :cond_5

    .line 116
    .line 117
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->Oo0oO〇O〇O()Z

    .line 118
    .line 119
    .line 120
    move-result v0

    .line 121
    if-nez v0, :cond_7

    .line 122
    .line 123
    :cond_5
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 124
    .line 125
    invoke-static {v0}, Lcom/intsig/camscanner/app/DialogUtils;->〇oOO8O8(Landroid/content/Context;)V

    .line 126
    .line 127
    .line 128
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    invoke-virtual {v0}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇O〇()Z

    .line 133
    .line 134
    .line 135
    move-result v0

    .line 136
    if-eqz v0, :cond_6

    .line 137
    .line 138
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 139
    .line 140
    .line 141
    move-result-object v0

    .line 142
    sget-object v1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->NOVICE_NEW_SHARE:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;

    .line 143
    .line 144
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V

    .line 145
    .line 146
    .line 147
    goto :goto_0

    .line 148
    :cond_6
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 149
    .line 150
    .line 151
    move-result-object v0

    .line 152
    sget-object v1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->NOVICE_SHARE:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;

    .line 153
    .line 154
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V

    .line 155
    .line 156
    .line 157
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o〇00O:Lcom/intsig/camscanner/share/listener/ShareBackListener;

    .line 158
    .line 159
    if-eqz v0, :cond_7

    .line 160
    .line 161
    invoke-interface {v0}, Lcom/intsig/camscanner/share/listener/ShareBackListener;->〇080()V

    .line 162
    .line 163
    .line 164
    :cond_7
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->O8o08O8O:Lcom/intsig/utils/activity/ActivityLifeCircleManager;

    .line 165
    .line 166
    invoke-virtual {v0}, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->O8()V

    .line 167
    .line 168
    .line 169
    return-void
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public static o08oOO(Landroid/content/Context;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;)V
    .locals 1

    .line 1
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getIconRes()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getTitleRes()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object p0

    .line 16
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getIntentName()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->O〇O〇oO(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getName()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p0

    .line 30
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇8(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getPkgName()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p0

    .line 37
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->o8oO〇(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getPkgName()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object p0

    .line 44
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->ooo〇8oO(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method static bridge synthetic o0O0(Lcom/intsig/camscanner/share/ShareHelper;J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o〇oO:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private o0O〇8o0O()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->share_page_style:I

    .line 10
    .line 11
    if-lez v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 14
    .line 15
    instance-of v1, v0, Lcom/intsig/camscanner/share/channel/item/LinkShareChannel;

    .line 16
    .line 17
    if-nez v1, :cond_0

    .line 18
    .line 19
    instance-of v0, v0, Lcom/intsig/camscanner/share/channel/item/SendToPcShareChannel;

    .line 20
    .line 21
    if-nez v0, :cond_0

    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 26
    :goto_0
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static synthetic o0ooO(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/type/BaseShare;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->OO〇0008O8(Lcom/intsig/camscanner/share/type/BaseShare;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static synthetic o8(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/type/BaseShare;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/share/ShareHelper;->ooo〇〇O〇(Lcom/intsig/camscanner/share/type/BaseShare;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public static synthetic o800o8O(Lcom/intsig/camscanner/share/ShareHelper;Ljava/util/ArrayList;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->OoO〇(Ljava/util/ArrayList;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private o80ooO(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/intsig/camscanner/share/type/BaseShare;"
        }
    .end annotation

    .line 1
    new-instance p1, Lcom/intsig/camscanner/share/type/ShareWeiXin;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    invoke-direct {p1, v0, p2}, Lcom/intsig/camscanner/share/type/ShareWeiXin;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇8oOO88(Ljava/util/ArrayList;)V

    .line 9
    .line 10
    .line 11
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 12
    .line 13
    const p3, 0x7f1305ed

    .line 14
    .line 15
    .line 16
    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p2

    .line 20
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 24
    .line 25
    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p2

    .line 29
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    return-object p1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private static o88O〇8(Landroid/content/Context;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/intsig/camscanner/share/type/BaseShare;",
            "Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getIconRes()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getTitleRes()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object p0

    .line 16
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getIntentName()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->O〇O〇oO(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getName()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p0

    .line 30
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇8(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getPkgName()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p0

    .line 37
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->o8oO〇(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getPkgName()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object p0

    .line 44
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->ooo〇8oO(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇8oOO88(Ljava/util/ArrayList;)V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method static bridge synthetic o88〇OO08〇(Lcom/intsig/camscanner/share/ShareHelper;Ljava/util/List;Z)Ljava/util/List;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->oO〇(Ljava/util/List;Z)Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static o8O0(Lcom/intsig/camscanner/share/type/BaseShare;)Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇08〇ooO00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    if-eqz p0, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/type/BaseShare;->O〇8O8〇008()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/type/BaseShare;->O〇8O8〇008()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    invoke-static {}, Lcom/intsig/camscanner/share/ShareHelper;->O〇OO()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 28
    .line 29
    .line 30
    move-result p0

    .line 31
    if-eqz p0, :cond_0

    .line 32
    .line 33
    const/4 p0, 0x1

    .line 34
    goto :goto_0

    .line 35
    :cond_0
    const/4 p0, 0x0

    .line 36
    :goto_0
    return p0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method static bridge synthetic o8O〇(Lcom/intsig/camscanner/share/ShareHelper;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->〇08〇0〇o〇8(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic o8oO〇(Lcom/intsig/camscanner/share/ShareHelper;)Lcom/intsig/utils/activity/ActivityLifeCircleManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/share/ShareHelper;->O8o08O8O:Lcom/intsig/utils/activity/ActivityLifeCircleManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private o8o〇〇0O(Ljava/lang/String;)V
    .locals 3

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    :try_start_0
    const-string v1, "from_part"

    .line 7
    .line 8
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 9
    .line 10
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 11
    .line 12
    .line 13
    const-string v1, "type"

    .line 14
    .line 15
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 16
    .line 17
    .line 18
    const-string p1, "from"

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->O〇08oOOO0:Ljava/lang/String;

    .line 21
    .line 22
    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 23
    .line 24
    .line 25
    const-string p1, "CSApplicationList"

    .line 26
    .line 27
    const-string v1, "click_apps"

    .line 28
    .line 29
    invoke-static {p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :catch_0
    move-exception p1

    .line 34
    sget-object v1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 35
    .line 36
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 37
    .line 38
    .line 39
    :goto_0
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->〇8〇0〇o〇O()Z

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    if-eqz p1, :cond_0

    .line 44
    .line 45
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 46
    .line 47
    new-instance v1, Ljava/lang/StringBuilder;

    .line 48
    .line 49
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    .line 51
    .line 52
    const-string v2, "fromPartObject="

    .line 53
    .line 54
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    :cond_0
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method static bridge synthetic oO(Lcom/intsig/camscanner/share/ShareHelper;)Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇00O0:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic oO00OOO(Lcom/intsig/camscanner/share/ShareHelper;)Lcom/intsig/app/BaseProgressDialog;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo0:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private oO8008O(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/BaseShare;->o800o8O()Lcom/intsig/camscanner/share/ShareLinkLogger;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    if-eqz v0, :cond_1

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/ShareLinkLogger;->OO0o〇〇(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    :cond_1
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private oO8o()V
    .locals 2

    .line 1
    const v0, 0x493f0

    .line 2
    .line 3
    .line 4
    invoke-static {v0}, Lcom/intsig/log/UserLogWriter;->oO80(I)V

    .line 5
    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 8
    .line 9
    const-string v1, "no app to share"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    const v1, 0x7f131f8f

    .line 17
    .line 18
    .line 19
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private synthetic oOo(Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareLinkLogger;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇〇888()Ljava/util/ArrayList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/share/ShareDataPresenter;->OO0o〇〇(Ljava/util/ArrayList;)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz p2, :cond_0

    .line 10
    .line 11
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareLinkLogger;->o800o8O()V

    .line 12
    .line 13
    .line 14
    :cond_0
    const/4 v1, 0x0

    .line 15
    if-nez v0, :cond_2

    .line 16
    .line 17
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 18
    .line 19
    const-string v2, " need sync and show dialog"

    .line 20
    .line 21
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 25
    .line 26
    invoke-static {v0}, Lcom/intsig/camscanner/app/AppUtil;->oO(Landroid/content/Context;)Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eqz v0, :cond_1

    .line 31
    .line 32
    const-string v0, "CSNoGenerateLinkPop"

    .line 33
    .line 34
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 38
    .line 39
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 40
    .line 41
    invoke-direct {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 42
    .line 43
    .line 44
    const v2, 0x7f1310d7

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    const v2, 0x7f1310b7

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    new-instance v2, Lo〇o8〇〇O/〇oOO8O8;

    .line 59
    .line 60
    invoke-direct {v2, p2}, Lo〇o8〇〇O/〇oOO8O8;-><init>(Lcom/intsig/camscanner/share/ShareLinkLogger;)V

    .line 61
    .line 62
    .line 63
    const p2, 0x7f13057e

    .line 64
    .line 65
    .line 66
    const v3, 0x7f060226

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, p2, v3, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇0〇O0088o(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 70
    .line 71
    .line 72
    move-result-object p2

    .line 73
    new-instance v0, Lo〇o8〇〇O/〇0000OOO;

    .line 74
    .line 75
    invoke-direct {v0, p0, p1}, Lo〇o8〇〇O/〇0000OOO;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 76
    .line 77
    .line 78
    const p1, 0x7f1310d5

    .line 79
    .line 80
    .line 81
    const v2, 0x7f060123

    .line 82
    .line 83
    .line 84
    invoke-virtual {p2, p1, v2, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇oOO8O8(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 93
    .line 94
    .line 95
    goto :goto_0

    .line 96
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->oo0O〇0〇〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 97
    .line 98
    .line 99
    :goto_0
    invoke-virtual {p3, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 100
    .line 101
    .line 102
    goto :goto_1

    .line 103
    :cond_2
    const/4 v2, 0x1

    .line 104
    if-ne v0, v2, :cond_3

    .line 105
    .line 106
    sget-object p2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 107
    .line 108
    const-string v0, "sync ing, need waiting"

    .line 109
    .line 110
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->oo0O〇0〇〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {p3, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 117
    .line 118
    .line 119
    goto :goto_1

    .line 120
    :cond_3
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 121
    .line 122
    const-string p3, "checkLoginAndSyncState had synced"

    .line 123
    .line 124
    invoke-static {p1, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    if-eqz p2, :cond_4

    .line 128
    .line 129
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareLinkLogger;->O8()V

    .line 130
    .line 131
    .line 132
    :cond_4
    :goto_1
    return-void
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private oO〇(Ljava/util/List;Z)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;Z)",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->optimize_share_request:I

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    if-eqz p2, :cond_0

    .line 11
    .line 12
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 13
    .line 14
    .line 15
    move-result-object p2

    .line 16
    invoke-static {p2, p1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇08O8o〇0(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    return-object p1

    .line 21
    :cond_0
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 22
    .line 23
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇0〇O0088o(Ljava/util/List;)Ljava/util/List;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    return-object p1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static oo(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 1

    .line 1
    if-nez p0, :cond_0

    .line 2
    .line 3
    sget-object p0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 4
    .line 5
    return-object p0

    .line 6
    :cond_0
    const-string v0, "https://cc.co/16YRy8"

    .line 7
    .line 8
    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_2

    .line 13
    .line 14
    const-string v0, "https://cc.co/16YRyq"

    .line 15
    .line 16
    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 17
    .line 18
    .line 19
    move-result p0

    .line 20
    if-eqz p0, :cond_1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    const/4 p0, 0x0

    .line 24
    goto :goto_1

    .line 25
    :cond_2
    :goto_0
    const/4 p0, 0x1

    .line 26
    :goto_1
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 27
    .line 28
    .line 29
    move-result-object p0

    .line 30
    return-object p0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private oo0O〇0〇〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V
    .locals 11

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇〇888()Ljava/util/ArrayList;

    .line 2
    .line 3
    .line 4
    move-result-object v6

    .line 5
    if-eqz v6, :cond_5

    .line 6
    .line 7
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto/16 :goto_1

    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 24
    .line 25
    const-string v0, "prioritySync network cannot use "

    .line 26
    .line 27
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 31
    .line 32
    const v0, 0x7f13008d

    .line 33
    .line 34
    .line 35
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 36
    .line 37
    .line 38
    return-void

    .line 39
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 40
    .line 41
    invoke-virtual {v0, v6}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇0〇O0088o(Ljava/util/List;)Ljava/util/List;

    .line 42
    .line 43
    .line 44
    move-result-object v7

    .line 45
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 46
    .line 47
    new-instance v1, Ljava/lang/StringBuilder;

    .line 48
    .line 49
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    .line 51
    .line 52
    const-string v2, "prioritySync docIdList:  "

    .line 53
    .line 54
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    const-string v2, ","

    .line 58
    .line 59
    invoke-static {v2, v6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v3

    .line 63
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    const-string v3, " , unSyncDocIdList: "

    .line 67
    .line 68
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-static {v2, v7}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v1

    .line 82
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    if-eqz v0, :cond_2

    .line 90
    .line 91
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 92
    .line 93
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 94
    .line 95
    .line 96
    return-void

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo0:Lcom/intsig/app/BaseProgressDialog;

    .line 98
    .line 99
    if-eqz v0, :cond_3

    .line 100
    .line 101
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 102
    .line 103
    .line 104
    :cond_3
    new-instance v0, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;

    .line 105
    .line 106
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 107
    .line 108
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o800o8O()Lcom/intsig/camscanner/share/ShareLinkLogger;

    .line 109
    .line 110
    .line 111
    move-result-object v2

    .line 112
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->o0O〇8o0O()Z

    .line 113
    .line 114
    .line 115
    move-result v3

    .line 116
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareLinkLogger;Z)V

    .line 117
    .line 118
    .line 119
    iput-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->Oo80:Lcom/intsig/camscanner/share/dialog/SyncDialogClient;

    .line 120
    .line 121
    new-instance v1, Lcom/intsig/camscanner/share/ShareHelper$10;

    .line 122
    .line 123
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/share/ShareHelper$10;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 124
    .line 125
    .line 126
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->o〇0(Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;)V

    .line 127
    .line 128
    .line 129
    sget-object v0, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;->〇O00:Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient$Companion;

    .line 130
    .line 131
    invoke-virtual {v0}, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient$Companion;->〇080()Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;

    .line 132
    .line 133
    .line 134
    move-result-object v0

    .line 135
    const/4 v8, 0x1

    .line 136
    invoke-virtual {v0, v8}, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;->oo〇(Z)V

    .line 137
    .line 138
    .line 139
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->Oo80:Lcom/intsig/camscanner/share/dialog/SyncDialogClient;

    .line 140
    .line 141
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇〇888()V

    .line 142
    .line 143
    .line 144
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->Oo80:Lcom/intsig/camscanner/share/dialog/SyncDialogClient;

    .line 145
    .line 146
    invoke-interface {v6}, Ljava/util/List;->size()I

    .line 147
    .line 148
    .line 149
    move-result v1

    .line 150
    invoke-interface {v7}, Ljava/util/List;->size()I

    .line 151
    .line 152
    .line 153
    move-result v2

    .line 154
    sub-int/2addr v1, v2

    .line 155
    invoke-interface {v6}, Ljava/util/List;->size()I

    .line 156
    .line 157
    .line 158
    move-result v2

    .line 159
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇80〇808〇O(II)V

    .line 160
    .line 161
    .line 162
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/ShareHelper;->〇000〇〇08()Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 163
    .line 164
    .line 165
    move-result-object v9

    .line 166
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08〇o0O:Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;

    .line 167
    .line 168
    invoke-virtual {v9, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->ooOO(Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;)V

    .line 169
    .line 170
    .line 171
    new-instance v10, Lcom/intsig/camscanner/share/ShareHelper$11;

    .line 172
    .line 173
    move-object v0, v10

    .line 174
    move-object v1, p0

    .line 175
    move-object v2, v6

    .line 176
    move-object v3, p1

    .line 177
    move-object v4, v7

    .line 178
    move-object v5, v9

    .line 179
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/share/ShareHelper$11;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Ljava/util/List;Lcom/intsig/camscanner/share/type/BaseShare;Ljava/util/List;Lcom/intsig/camscanner/tsapp/sync/SyncThread;)V

    .line 180
    .line 181
    .line 182
    iput-object v10, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08〇o0O:Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;

    .line 183
    .line 184
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->ooo〇8oO()Z

    .line 185
    .line 186
    .line 187
    move-result v0

    .line 188
    if-eqz v0, :cond_4

    .line 189
    .line 190
    new-instance v10, Lo〇o8〇〇O/〇00〇8;

    .line 191
    .line 192
    move-object v0, v10

    .line 193
    move-object v1, p0

    .line 194
    move-object v2, v9

    .line 195
    move-object v3, v6

    .line 196
    move-object v4, v7

    .line 197
    move-object v5, p1

    .line 198
    invoke-direct/range {v0 .. v5}, Lo〇o8〇〇O/〇00〇8;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/List;Ljava/util/List;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 199
    .line 200
    .line 201
    iput-object v10, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇〇o〇:Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;

    .line 202
    .line 203
    invoke-virtual {v9, v10}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇〇808〇(Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;)V

    .line 204
    .line 205
    .line 206
    invoke-static {v8}, Lcom/intsig/tianshu/TianShuAPI;->o〇OOo000(Z)V

    .line 207
    .line 208
    .line 209
    goto :goto_0

    .line 210
    :cond_4
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 211
    .line 212
    const-string v1, "prioritySync not sync"

    .line 213
    .line 214
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    .line 216
    .line 217
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->Oo8Oo00oo()Z

    .line 218
    .line 219
    .line 220
    move-result p1

    .line 221
    new-instance v0, Lo〇o8〇〇O/〇o;

    .line 222
    .line 223
    invoke-direct {v0, p0, v9}, Lo〇o8〇〇O/〇o;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/tsapp/sync/SyncThread;)V

    .line 224
    .line 225
    .line 226
    invoke-static {v7, p1, v0}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->OO0o〇〇〇〇0(Ljava/util/List;ZLandroidx/core/util/Consumer;)V

    .line 227
    .line 228
    .line 229
    :goto_0
    return-void

    .line 230
    :cond_5
    :goto_1
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 231
    .line 232
    const-string v0, "prioritySync param invalid"

    .line 233
    .line 234
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    .line 236
    .line 237
    return-void
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public static synthetic oo88o8O(Lcom/intsig/camscanner/share/ShareHelper;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->〇o〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic ooOO()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private ooO〇00O(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/intsig/camscanner/share/type/BaseShare;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareNormalLink;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    invoke-direct {v0, v1, p2}, Lcom/intsig/camscanner/share/type/ShareNormalLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 6
    .line 7
    .line 8
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 9
    .line 10
    invoke-static {p2, v0, p1, p3}, Lcom/intsig/camscanner/share/ShareHelper;->o88O〇8(Landroid/content/Context;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)V

    .line 11
    .line 12
    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private ooo0〇O88O(Lcom/intsig/camscanner/share/type/BaseShare;)Lcom/intsig/camscanner/share/type/SharePdf;
    .locals 4
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "createPdfShare share is null"

    .line 7
    .line 8
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-object v0

    .line 12
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇〇888()Ljava/util/ArrayList;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    if-nez v1, :cond_1

    .line 17
    .line 18
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 19
    .line 20
    const-string v1, "createPdfShare docIds is null"

    .line 21
    .line 22
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    return-object v0

    .line 26
    :cond_1
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/ShareCopyLink;

    .line 27
    .line 28
    if-eqz v1, :cond_2

    .line 29
    .line 30
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 31
    .line 32
    const-string v1, "createPdfShare "

    .line 33
    .line 34
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    return-object v0

    .line 38
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->Oooo8o0〇()Ljava/util/List;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    if-eqz v1, :cond_3

    .line 43
    .line 44
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    if-nez v2, :cond_3

    .line 49
    .line 50
    new-instance v0, Ljava/util/ArrayList;

    .line 51
    .line 52
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 53
    .line 54
    .line 55
    :cond_3
    new-instance v1, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 56
    .line 57
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇〇888()Ljava/util/ArrayList;

    .line 60
    .line 61
    .line 62
    move-result-object v3

    .line 63
    invoke-direct {v1, v2, v3, v0}, Lcom/intsig/camscanner/share/type/SharePdf;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->OO0o〇〇()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/share/type/BaseShare;->o8oO〇(Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/share/type/BaseShare;->O〇O〇oO(Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o800o8O()Lcom/intsig/camscanner/share/ShareLinkLogger;

    .line 81
    .line 82
    .line 83
    move-result-object p1

    .line 84
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇80(Lcom/intsig/camscanner/share/ShareLinkLogger;)V

    .line 85
    .line 86
    .line 87
    new-instance p1, Lo〇o8〇〇O/o〇8oOO88;

    .line 88
    .line 89
    invoke-direct {p1, p0}, Lo〇o8〇〇O/o〇8oOO88;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/share/type/SharePdf;->〇08〇0〇o〇8(Lcom/intsig/camscanner/share/type/SharePdf$OnTryDeductionListener;)V

    .line 93
    .line 94
    .line 95
    return-object v1
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public static ooo8o〇o〇(Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/ShareHelper;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/ShareHelper;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic ooo〇8oO(Lcom/intsig/camscanner/share/ShareHelper;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇080OO8〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private synthetic ooo〇〇O〇(Lcom/intsig/camscanner/share/type/BaseShare;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p2, "CSErrorInternetPop"

    .line 2
    .line 3
    const-string p3, "reconnect"

    .line 4
    .line 5
    invoke-static {p2, p3}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static synthetic oo〇(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/List;Ljava/util/List;Lcom/intsig/camscanner/share/type/BaseShare;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/share/ShareHelper;->O00O(Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/List;Ljava/util/List;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method private o〇(Ljava/lang/String;)V
    .locals 3
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/BaseShare;->o800o8O()Lcom/intsig/camscanner/share/ShareLinkLogger;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object v0, v1

    .line 14
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->〇o8oO()Z

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    if-eqz v2, :cond_2

    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 21
    .line 22
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->ooo0〇O88O(Lcom/intsig/camscanner/share/type/BaseShare;)Lcom/intsig/camscanner/share/type/SharePdf;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    if-eqz p1, :cond_3

    .line 27
    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    const-string v2, "link_fail"

    .line 31
    .line 32
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/ShareLinkLogger;->OoO8(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    :cond_1
    iput-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 36
    .line 37
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 38
    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 42
    .line 43
    invoke-static {v0, p1}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    :cond_3
    :goto_1
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static synthetic o〇0OOo〇0(Lcom/intsig/camscanner/share/ShareHelper;Landroid/content/Intent;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O〇0〇o808〇(Landroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static o〇0o〇〇(Landroidx/fragment/app/FragmentActivity;JLjava/util/ArrayList;Lcom/intsig/camscanner/share/ShareHelper$ShareType;Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "J",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/intsig/camscanner/share/ShareHelper$ShareType;",
            "Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;",
            "Lcom/intsig/camscanner/share/listener/ShareBackListener;",
            ")V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/ShareHelper;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇O〇80o08O(Landroid/content/Context;J)I

    .line 7
    .line 8
    .line 9
    move-result p0

    .line 10
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper;->〇8O0O808〇(I)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, p4}, Lcom/intsig/camscanner/share/ShareHelper;->o08〇〇0O(Lcom/intsig/camscanner/share/ShareHelper$ShareType;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, p5}, Lcom/intsig/camscanner/share/ShareHelper;->OOo88OOo(Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;)V

    .line 17
    .line 18
    .line 19
    if-eqz p5, :cond_0

    .line 20
    .line 21
    invoke-interface {p5, v0}, Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;->〇o00〇〇Oo(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 22
    .line 23
    .line 24
    :cond_0
    invoke-direct {v0, p1, p2, p3, p6}, Lcom/intsig/camscanner/share/ShareHelper;->〇〇〇0880(JLjava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
.end method

.method public static synthetic o〇8(Lcom/intsig/camscanner/share/ShareHelper;Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackDataListener;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/share/ShareHelper;->O〇Oo(Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackDataListener;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method static bridge synthetic o〇8oOO88(Lcom/intsig/camscanner/share/ShareHelper;)Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇〇o〇:Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private synthetic o〇8〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "disable_mobile_net"

    .line 2
    .line 3
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->oO8008O(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic o〇O(Lcom/intsig/camscanner/share/ShareHelper;)Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08〇o0O:Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic o〇O8〇〇o(Lcom/intsig/camscanner/share/ShareHelper;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->〇oo(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private o〇o(Lcom/intsig/camscanner/share/type/BaseShare;)Z
    .locals 7

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o800o8O()Lcom/intsig/camscanner/share/ShareLinkLogger;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 6
    .line 7
    const/4 v2, 0x1

    .line 8
    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 9
    .line 10
    .line 11
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 12
    .line 13
    invoke-static {v3}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 14
    .line 15
    .line 16
    move-result v3

    .line 17
    const/4 v4, 0x0

    .line 18
    if-nez v3, :cond_e

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8o:Lcom/intsig/camscanner/share/ShareOptimization;

    .line 21
    .line 22
    if-nez v1, :cond_0

    .line 23
    .line 24
    new-instance v1, Lcom/intsig/camscanner/share/ShareOptimization;

    .line 25
    .line 26
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 27
    .line 28
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇〇888()Ljava/util/ArrayList;

    .line 29
    .line 30
    .line 31
    move-result-object v5

    .line 32
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->Oooo8o0〇()Ljava/util/List;

    .line 33
    .line 34
    .line 35
    move-result-object v6

    .line 36
    invoke-direct {v1, v3, v5, v6}, Lcom/intsig/camscanner/share/ShareOptimization;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    .line 37
    .line 38
    .line 39
    iput-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8o:Lcom/intsig/camscanner/share/ShareOptimization;

    .line 40
    .line 41
    new-instance v3, Lcom/intsig/camscanner/share/ShareHelper$6;

    .line 42
    .line 43
    invoke-direct {v3, p0, v0}, Lcom/intsig/camscanner/share/ShareHelper$6;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/ShareLinkLogger;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/ShareOptimization;->〇o(Lcom/intsig/camscanner/share/ShareOptimization$OnUploadSuccessCallback;)V

    .line 47
    .line 48
    .line 49
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8o:Lcom/intsig/camscanner/share/ShareOptimization;

    .line 50
    .line 51
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/ShareOptimization;->o8()Z

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    if-eqz v1, :cond_2

    .line 56
    .line 57
    if-eqz v0, :cond_1

    .line 58
    .line 59
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareLinkLogger;->o800o8O()V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareLinkLogger;->O8()V

    .line 63
    .line 64
    .line 65
    :cond_1
    return v2

    .line 66
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8o:Lcom/intsig/camscanner/share/ShareOptimization;

    .line 67
    .line 68
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/share/ShareOptimization;->Oooo8o0〇(Lcom/intsig/camscanner/share/type/BaseShare;)I

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    sget-object v3, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 73
    .line 74
    new-instance v5, Ljava/lang/StringBuilder;

    .line 75
    .line 76
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .line 78
    .line 79
    const-string v6, "checkLoginAndSyncState canShare: "

    .line 80
    .line 81
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v5

    .line 91
    invoke-static {v3, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    if-nez v1, :cond_8

    .line 95
    .line 96
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->Oo08()Z

    .line 97
    .line 98
    .line 99
    move-result v1

    .line 100
    if-eqz v1, :cond_3

    .line 101
    .line 102
    new-instance v1, Lcom/intsig/camscanner/share/UnLoginShareRCNPromptDialog;

    .line 103
    .line 104
    invoke-direct {v1}, Lcom/intsig/camscanner/share/UnLoginShareRCNPromptDialog;-><init>()V

    .line 105
    .line 106
    .line 107
    new-instance v2, Lcom/intsig/camscanner/share/ShareHelper$7;

    .line 108
    .line 109
    invoke-direct {v2, p0, v0, p1}, Lcom/intsig/camscanner/share/ShareHelper$7;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/ShareLinkLogger;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 110
    .line 111
    .line 112
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/share/UnLoginShareRCNPromptDialog;->〇8〇OOoooo(Lcom/intsig/camscanner/share/UnLoginShareRCNPromptDialog$PromptListener;)V

    .line 113
    .line 114
    .line 115
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 116
    .line 117
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 118
    .line 119
    .line 120
    move-result-object p1

    .line 121
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/share/UnLoginShareRCNPromptDialog;->o〇0〇o(Landroidx/fragment/app/FragmentManager;)V

    .line 122
    .line 123
    .line 124
    goto :goto_0

    .line 125
    :cond_3
    if-eqz v0, :cond_4

    .line 126
    .line 127
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/ShareLinkLogger;->〇080(Z)V

    .line 128
    .line 129
    .line 130
    :cond_4
    invoke-static {}, Lcom/intsig/camscanner/ipo/IPOCheck;->o800o8O()Z

    .line 131
    .line 132
    .line 133
    move-result v1

    .line 134
    if-nez v1, :cond_5

    .line 135
    .line 136
    invoke-static {}, Lcom/intsig/camscanner/ipo/IPOCheck;->O〇8O8〇008()V

    .line 137
    .line 138
    .line 139
    invoke-static {}, Lcom/intsig/camscanner/ipo/IPOCheck;->O8ooOoo〇()V

    .line 140
    .line 141
    .line 142
    :cond_5
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->isPriorityQueueInShareLink()Z

    .line 147
    .line 148
    .line 149
    move-result v1

    .line 150
    xor-int/2addr v1, v2

    .line 151
    if-eqz v0, :cond_6

    .line 152
    .line 153
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareLinkLogger;->o800o8O()V

    .line 154
    .line 155
    .line 156
    :cond_6
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8o:Lcom/intsig/camscanner/share/ShareOptimization;

    .line 157
    .line 158
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/share/ShareOptimization;->Oo8Oo00oo(Z)V

    .line 159
    .line 160
    .line 161
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 162
    .line 163
    .line 164
    move-result-wide v1

    .line 165
    iput-wide v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o〇oO:J

    .line 166
    .line 167
    new-instance v1, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;

    .line 168
    .line 169
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 170
    .line 171
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o800o8O()Lcom/intsig/camscanner/share/ShareLinkLogger;

    .line 172
    .line 173
    .line 174
    move-result-object p1

    .line 175
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->o0O〇8o0O()Z

    .line 176
    .line 177
    .line 178
    move-result v3

    .line 179
    invoke-direct {v1, v2, p1, v3}, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareLinkLogger;Z)V

    .line 180
    .line 181
    .line 182
    iput-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->Oo80:Lcom/intsig/camscanner/share/dialog/SyncDialogClient;

    .line 183
    .line 184
    new-instance p1, Lcom/intsig/camscanner/share/ShareHelper$8;

    .line 185
    .line 186
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/share/ShareHelper$8;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 187
    .line 188
    .line 189
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->o〇0(Lcom/intsig/camscanner/share/dialog/strategy/ShareLoadingStrategy$ShareLoadingListener;)V

    .line 190
    .line 191
    .line 192
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->Oo80:Lcom/intsig/camscanner/share/dialog/SyncDialogClient;

    .line 193
    .line 194
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇〇888()V

    .line 195
    .line 196
    .line 197
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->Oo80:Lcom/intsig/camscanner/share/dialog/SyncDialogClient;

    .line 198
    .line 199
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 200
    .line 201
    const v2, 0x7f13008a

    .line 202
    .line 203
    .line 204
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 205
    .line 206
    .line 207
    move-result-object v1

    .line 208
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->oO80(Ljava/lang/String;)V

    .line 209
    .line 210
    .line 211
    :goto_0
    if-eqz v0, :cond_7

    .line 212
    .line 213
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareLinkLogger;->〇〇8O0〇8()V

    .line 214
    .line 215
    .line 216
    :cond_7
    return v4

    .line 217
    :cond_8
    const/4 v3, 0x2

    .line 218
    const/4 v5, 0x3

    .line 219
    if-ne v1, v3, :cond_9

    .line 220
    .line 221
    goto :goto_1

    .line 222
    :cond_9
    if-ne v1, v5, :cond_a

    .line 223
    .line 224
    if-eqz v0, :cond_a

    .line 225
    .line 226
    const-string v3, "pic_num_exceed_limit"

    .line 227
    .line 228
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/share/ShareLinkLogger;->OO0o〇〇(Ljava/lang/String;)V

    .line 229
    .line 230
    .line 231
    :cond_a
    :goto_1
    invoke-static {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->o〇0(Lcom/intsig/camscanner/share/type/BaseShare;)Z

    .line 232
    .line 233
    .line 234
    move-result p1

    .line 235
    if-eqz p1, :cond_b

    .line 236
    .line 237
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 238
    .line 239
    invoke-static {p1, v4}, Lcom/intsig/tsapp/account/login/LoginTranslucentActivity;->o880(Landroid/content/Context;Z)Landroid/content/Intent;

    .line 240
    .line 241
    .line 242
    move-result-object p1

    .line 243
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 244
    .line 245
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->O8o08O8O:Lcom/intsig/utils/activity/ActivityLifeCircleManager;

    .line 246
    .line 247
    const/16 v3, 0x2768

    .line 248
    .line 249
    invoke-virtual {v1, v2, p1, v3}, Lcom/intsig/camscanner/share/ShareDataPresenter;->startActivityForResult(Lcom/intsig/utils/activity/ActivityLifeCircleManager;Landroid/content/Intent;I)V

    .line 250
    .line 251
    .line 252
    if-eqz v0, :cond_d

    .line 253
    .line 254
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareLinkLogger;->Oooo8o0〇()V

    .line 255
    .line 256
    .line 257
    goto :goto_3

    .line 258
    :cond_b
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 259
    .line 260
    if-ne v1, v5, :cond_c

    .line 261
    .line 262
    new-array v1, v2, [Ljava/lang/Object;

    .line 263
    .line 264
    invoke-static {}, Lcom/intsig/camscanner/share/ShareOptimization;->oo88o8O()I

    .line 265
    .line 266
    .line 267
    move-result v2

    .line 268
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 269
    .line 270
    .line 271
    move-result-object v2

    .line 272
    aput-object v2, v1, v4

    .line 273
    .line 274
    const v2, 0x7f130927

    .line 275
    .line 276
    .line 277
    invoke-virtual {p1, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 278
    .line 279
    .line 280
    move-result-object p1

    .line 281
    goto :goto_2

    .line 282
    :cond_c
    const v1, 0x7f13038c

    .line 283
    .line 284
    .line 285
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 286
    .line 287
    .line 288
    move-result-object p1

    .line 289
    :goto_2
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/share/ShareHelper;->〇oO8O0〇〇O(Ljava/lang/String;Lcom/intsig/camscanner/share/ShareLinkLogger;)V

    .line 290
    .line 291
    .line 292
    :cond_d
    :goto_3
    return v4

    .line 293
    :cond_e
    invoke-static {}, Lcom/intsig/camscanner/ipo/IPOCheck;->o800o8O()Z

    .line 294
    .line 295
    .line 296
    move-result v3

    .line 297
    if-nez v3, :cond_11

    .line 298
    .line 299
    invoke-virtual {v1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 300
    .line 301
    .line 302
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 303
    .line 304
    .line 305
    move-result-object v3

    .line 306
    iget v3, v3, Lcom/intsig/tsapp/sync/AppConfigJson;->share_link_style:I

    .line 307
    .line 308
    if-lez v3, :cond_f

    .line 309
    .line 310
    goto :goto_4

    .line 311
    :cond_f
    const/4 v2, 0x0

    .line 312
    :goto_4
    if-eqz v2, :cond_11

    .line 313
    .line 314
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/share/ShareHelper;->Oo(Z)Z

    .line 315
    .line 316
    .line 317
    move-result v2

    .line 318
    sget-object v3, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 319
    .line 320
    new-instance v5, Ljava/lang/StringBuilder;

    .line 321
    .line 322
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 323
    .line 324
    .line 325
    const-string v6, "checkShareLoginAndSyncState canCovert: "

    .line 326
    .line 327
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    .line 329
    .line 330
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 331
    .line 332
    .line 333
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 334
    .line 335
    .line 336
    move-result-object v5

    .line 337
    invoke-static {v3, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    .line 339
    .line 340
    if-eqz v2, :cond_11

    .line 341
    .line 342
    if-eqz v0, :cond_10

    .line 343
    .line 344
    const-string p1, "sync_off"

    .line 345
    .line 346
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/ShareLinkLogger;->OoO8(Ljava/lang/String;)V

    .line 347
    .line 348
    .line 349
    :cond_10
    return v4

    .line 350
    :cond_11
    if-eqz v0, :cond_12

    .line 351
    .line 352
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareLinkLogger;->Oo08()Lcom/intsig/camscanner/share/bean/PdfLinkShareTrackData;

    .line 353
    .line 354
    .line 355
    move-result-object v2

    .line 356
    goto :goto_5

    .line 357
    :cond_12
    const/4 v2, 0x0

    .line 358
    :goto_5
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 359
    .line 360
    new-instance v4, Lo〇o8〇〇O/O〇O〇oO;

    .line 361
    .line 362
    invoke-direct {v4, p0, p1, v0, v1}, Lo〇o8〇〇O/O〇O〇oO;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareLinkLogger;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 363
    .line 364
    .line 365
    new-instance p1, Lo〇o8〇〇O/o8oO〇;

    .line 366
    .line 367
    invoke-direct {p1, v0}, Lo〇o8〇〇O/o8oO〇;-><init>(Lcom/intsig/camscanner/share/ShareLinkLogger;)V

    .line 368
    .line 369
    .line 370
    invoke-static {v3, v4, p1, v2}, Lcom/intsig/camscanner/ipo/IPOCheck;->OO0o〇〇〇〇0(Landroidx/fragment/app/FragmentActivity;Ljava/lang/Runnable;Ljava/lang/Runnable;Lcom/intsig/camscanner/share/bean/PdfLinkShareTrackData;)V

    .line 371
    .line 372
    .line 373
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 374
    .line 375
    .line 376
    move-result p1

    .line 377
    return p1
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method public static synthetic o〇〇0〇(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareLinkLogger;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/share/ShareHelper;->oOo(Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareLinkLogger;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public static o〇〇0〇88()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o8o〇Oo8Oo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇0(Lcom/intsig/camscanner/share/ShareHelper;Z)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->Oo(Z)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic 〇00(Lcom/intsig/camscanner/share/ShareHelper;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O8O〇8oo08()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic 〇0000OOO(Lcom/intsig/camscanner/share/ShareLinkLogger;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->O〇8oOo8O(Lcom/intsig/camscanner/share/ShareLinkLogger;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private 〇000O0(Lcom/intsig/camscanner/share/type/BaseShare;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇8〇o〇8(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 10
    .line 11
    const-string v1, "showTipsForDownLoadDataInMobileNetWork"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    new-instance v1, Lcom/intsig/camscanner/share/ShareHelper$9;

    .line 19
    .line 20
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/share/ShareHelper$9;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 21
    .line 22
    .line 23
    new-instance p1, Lo〇o8〇〇O/Oo8Oo00oo;

    .line 24
    .line 25
    invoke-direct {p1, p0}, Lo〇o8〇〇O/Oo8Oo00oo;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 26
    .line 27
    .line 28
    invoke-static {v0, v1, p1}, Lcom/intsig/camscanner/app/DialogUtils;->O0(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->oo0O〇0〇〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 33
    .line 34
    .line 35
    :goto_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static 〇008〇o0〇〇(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/intsig/camscanner/share/listener/ShareBackListener;",
            ")V"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    invoke-static {p0, p1, v0, v1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->Oo〇(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/SharePreviousInterceptor;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private 〇008〇oo(Lcom/intsig/camscanner/share/type/BaseShare;Z)V
    .locals 8

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇o()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇O8OO()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x1

    .line 12
    if-ne v0, v1, :cond_0

    .line 13
    .line 14
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->oo88o8O()J

    .line 17
    .line 18
    .line 19
    move-result-wide v4

    .line 20
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 21
    .line 22
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->Oo〇o(Landroid/content/Context;)Ljava/util/List;

    .line 23
    .line 24
    .line 25
    move-result-object v7

    .line 26
    move-object v2, p0

    .line 27
    move v6, p2

    .line 28
    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/share/ShareHelper;->〇8〇o〇8(Landroid/content/Context;JZLjava/util/List;)V

    .line 29
    .line 30
    .line 31
    return-void

    .line 32
    :cond_0
    if-eqz p2, :cond_1

    .line 33
    .line 34
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 37
    .line 38
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->oo88o8O()J

    .line 39
    .line 40
    .line 41
    move-result-wide v1

    .line 42
    invoke-interface {p2, v0, v1, v2, p0}, Lcom/intsig/camscanner/share/listener/ShareUiInterface;->OO0o〇〇〇〇0(Landroid/content/Context;JLcom/intsig/camscanner/share/listener/ShareCompressSelectListener;)V

    .line 43
    .line 44
    .line 45
    return-void

    .line 46
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O8OO08o()V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇00O0O0(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    if-lez p1, :cond_1

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    :cond_1
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic 〇00〇8(Lcom/intsig/camscanner/share/ShareHelper;Ljava/lang/ref/WeakReference;Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackListener;Lcom/intsig/camscanner/share/listener/ShareBackDataListener;Z)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p6}, Lcom/intsig/camscanner/share/ShareHelper;->〇Oo〇o8(Ljava/lang/ref/WeakReference;Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackListener;Lcom/intsig/camscanner/share/listener/ShareBackDataListener;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
.end method

.method private 〇080O0(Ljava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackListener;Lcom/intsig/camscanner/share/ShareHelper$ShareType;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/intsig/camscanner/share/listener/ShareBackListener;",
            "Lcom/intsig/camscanner/share/ShareHelper$ShareType;",
            ")V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "share  Docs  size = "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iput-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->o〇00O:Lcom/intsig/camscanner/share/listener/ShareBackListener;

    .line 28
    .line 29
    new-instance p2, Lcom/intsig/camscanner/control/DataChecker;

    .line 30
    .line 31
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 32
    .line 33
    const-wide/16 v5, -0x1

    .line 34
    .line 35
    const/4 v7, 0x0

    .line 36
    sget v8, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O:I

    .line 37
    .line 38
    new-instance v9, Lo〇o8〇〇O/o〇8;

    .line 39
    .line 40
    invoke-direct {v9, p0, p1, p3}, Lo〇o8〇〇O/o〇8;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/ShareHelper$ShareType;)V

    .line 41
    .line 42
    .line 43
    move-object v2, p2

    .line 44
    move-object v4, p1

    .line 45
    invoke-direct/range {v2 .. v9}, Lcom/intsig/camscanner/control/DataChecker;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;JLjava/lang/String;ILcom/intsig/camscanner/control/DataChecker$ActionListener;)V

    .line 46
    .line 47
    .line 48
    const/4 p1, 0x1

    .line 49
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/control/DataChecker;->〇0000OOO(Z)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p2}, Lcom/intsig/camscanner/control/DataChecker;->o〇0()Z

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static synthetic 〇08O8o〇0(Lcom/intsig/camscanner/share/ShareHelper;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->o〇8〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private 〇08〇0〇o〇8(I)V
    .locals 3
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/BaseShare;->o800o8O()Lcom/intsig/camscanner/share/ShareLinkLogger;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object v0, v1

    .line 14
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->〇o8oO()Z

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    if-eqz v2, :cond_2

    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 21
    .line 22
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->ooo0〇O88O(Lcom/intsig/camscanner/share/type/BaseShare;)Lcom/intsig/camscanner/share/type/SharePdf;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    if-eqz p1, :cond_3

    .line 27
    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    const-string v2, "link_fail"

    .line 31
    .line 32
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/ShareLinkLogger;->OoO8(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    :cond_1
    iput-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 36
    .line 37
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 38
    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->〇〇O00〇8(I)V

    .line 42
    .line 43
    .line 44
    :cond_3
    :goto_1
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private synthetic 〇0OO8(Lcom/intsig/camscanner/share/type/BaseShare;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    sget-object p2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    const-string p3, "User Operation: open sync"

    .line 4
    .line 5
    invoke-static {p2, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string p2, "CSNoGenerateLinkPop"

    .line 9
    .line 10
    const-string p3, "open_sync"

    .line 11
    .line 12
    invoke-static {p2, p3}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 16
    .line 17
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 18
    .line 19
    .line 20
    move-result-object p3

    .line 21
    const v0, 0x7f131e77

    .line 22
    .line 23
    .line 24
    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p3

    .line 28
    invoke-static {p2, p3}, Lcom/intsig/camscanner/app/AppUtil;->o8O〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->〇000O0(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private 〇0O〇Oo(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p2, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->FEI_SHU:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 6
    .line 7
    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    if-eqz p1, :cond_1

    .line 12
    .line 13
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    const/4 p2, 0x1

    .line 18
    if-ne p1, p2, :cond_1

    .line 19
    .line 20
    const/4 v0, 0x1

    .line 21
    :cond_1
    return v0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic 〇0〇O0088o(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/type/BaseShare;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/share/ShareHelper;->〇0OO8(Lcom/intsig/camscanner/share/type/BaseShare;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method static bridge synthetic 〇8(Lcom/intsig/camscanner/share/ShareHelper;)Landroidx/fragment/app/FragmentActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇80(Lcom/intsig/camscanner/share/ShareHelper;)Lcom/intsig/camscanner/share/ShareOptimization;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8o:Lcom/intsig/camscanner/share/ShareOptimization;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private synthetic 〇8o(Lcom/intsig/camscanner/share/type/BaseShare;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->〇008〇oo(Lcom/intsig/camscanner/share/type/BaseShare;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇8o8O〇O(Landroid/content/Context;ZLjava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;->PDF_SECURITY_MARK:Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;

    .line 7
    .line 8
    iput-object v1, v0, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇080:Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    iput-boolean v1, v0, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇o〇:Z

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o8oO〇()Z

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    const v3, 0x7f130e19

    .line 18
    .line 19
    .line 20
    if-eqz v2, :cond_0

    .line 21
    .line 22
    const v1, 0x7f130639

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    iput-object v1, v0, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇o00〇〇Oo:Ljava/lang/String;

    .line 30
    .line 31
    new-instance v1, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;

    .line 32
    .line 33
    invoke-direct {v1}, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;-><init>()V

    .line 34
    .line 35
    .line 36
    sget-object v2, Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;->PDF_NO_WATER_MARK:Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;

    .line 37
    .line 38
    iput-object v2, v1, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇080:Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;

    .line 39
    .line 40
    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    iput-object p1, v1, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇o00〇〇Oo:Ljava/lang/String;

    .line 45
    .line 46
    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_0
    const v2, 0x7f130638

    .line 51
    .line 52
    .line 53
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    iput-object v2, v0, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇o00〇〇Oo:Ljava/lang/String;

    .line 58
    .line 59
    new-instance v2, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;

    .line 60
    .line 61
    invoke-direct {v2}, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;-><init>()V

    .line 62
    .line 63
    .line 64
    sget-object v4, Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;->PDF:Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;

    .line 65
    .line 66
    iput-object v4, v2, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇080:Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;

    .line 67
    .line 68
    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v3

    .line 72
    iput-object v3, v2, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇o00〇〇Oo:Ljava/lang/String;

    .line 73
    .line 74
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    .line 76
    .line 77
    new-instance v2, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;

    .line 78
    .line 79
    invoke-direct {v2}, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;-><init>()V

    .line 80
    .line 81
    .line 82
    sget-object v3, Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;->PDF_NO_WATER_MARK:Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;

    .line 83
    .line 84
    iput-object v3, v2, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇080:Lcom/intsig/camscanner/share/view/ShareOptionDialog$OptionType;

    .line 85
    .line 86
    const v3, 0x7f130637

    .line 87
    .line 88
    .line 89
    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    iput-object p1, v2, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇o00〇〇Oo:Ljava/lang/String;

    .line 94
    .line 95
    iput-boolean v1, v2, Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;->〇o〇:Z

    .line 96
    .line 97
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    .line 99
    .line 100
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 101
    .line 102
    if-eqz p1, :cond_1

    .line 103
    .line 104
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇o()Z

    .line 105
    .line 106
    .line 107
    move-result p1

    .line 108
    if-eqz p1, :cond_1

    .line 109
    .line 110
    if-eqz p2, :cond_1

    .line 111
    .line 112
    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    .line 114
    .line 115
    :cond_1
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private synthetic 〇8o〇〇8080(Lcom/intsig/camscanner/share/ShareLinkLogger;Landroid/content/DialogInterface;I)V
    .locals 2

    .line 1
    sget-object p2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    const-string p3, "User Operation: go to login"

    .line 4
    .line 5
    invoke-static {p2, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 9
    .line 10
    const/4 p3, 0x0

    .line 11
    invoke-static {p2, p3}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->〇o00〇〇Oo(Landroid/content/Context;Lcom/intsig/tsapp/account/model/LoginMainArgs;)Landroid/content/Intent;

    .line 12
    .line 13
    .line 14
    move-result-object p2

    .line 15
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->O8o08O8O:Lcom/intsig/utils/activity/ActivityLifeCircleManager;

    .line 18
    .line 19
    const/16 v1, 0x2764

    .line 20
    .line 21
    invoke-virtual {p3, v0, p2, v1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->startActivityForResult(Lcom/intsig/utils/activity/ActivityLifeCircleManager;Landroid/content/Intent;I)V

    .line 22
    .line 23
    .line 24
    if-eqz p1, :cond_0

    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/ShareLinkLogger;->〇〇888()V

    .line 27
    .line 28
    .line 29
    :cond_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method static bridge synthetic 〇8〇0〇o〇O(Lcom/intsig/camscanner/share/ShareHelper;)Lcom/intsig/camscanner/share/ShareDataPresenter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇8〇o〇8(Landroid/content/Context;JZLjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JZ",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareMenuItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/view/ShareOptionDialog;

    .line 2
    .line 3
    const v1, 0x7f140004

    .line 4
    .line 5
    .line 6
    invoke-direct {v0, p1, v1}, Lcom/intsig/camscanner/share/view/ShareOptionDialog;-><init>(Landroid/content/Context;I)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, p4}, Lcom/intsig/camscanner/share/view/ShareOptionDialog;->O8ooOoo〇(Z)Lcom/intsig/camscanner/share/view/ShareOptionDialog;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    const/4 p4, 0x1

    .line 14
    invoke-virtual {p1, p4}, Lcom/intsig/camscanner/share/view/ShareOptionDialog;->〇0〇O0088o(Z)Lcom/intsig/camscanner/share/view/ShareOptionDialog;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-virtual {p1, p4}, Lcom/intsig/camscanner/share/view/ShareOptionDialog;->OoO8(Z)Lcom/intsig/camscanner/share/view/ShareOptionDialog;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-virtual {p1, p2, p3}, Lcom/intsig/camscanner/share/view/ShareOptionDialog;->O〇8O8〇008(J)Lcom/intsig/camscanner/share/view/ShareOptionDialog;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    invoke-virtual {p1, p5}, Lcom/intsig/camscanner/share/view/ShareOptionDialog;->〇0000OOO(Ljava/util/List;)Lcom/intsig/camscanner/share/view/ShareOptionDialog;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->O〇o88o08〇:Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareItemClickListener;

    .line 31
    .line 32
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/share/view/ShareOptionDialog;->〇oOO8O8(Lcom/intsig/camscanner/share/view/ShareOptionDialog$ShareItemClickListener;)Lcom/intsig/camscanner/share/view/ShareOptionDialog;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/view/ShareOptionDialog;->o〇O8〇〇o()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :catch_0
    move-exception p1

    .line 44
    sget-object p2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 45
    .line 46
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 47
    .line 48
    .line 49
    :goto_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private 〇O(Ljava/util/List;JLjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/share/type/BaseShare;",
            ">;J",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareBatchOcr;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    invoke-direct {v0, v1, p2, p3, p4}, Lcom/intsig/camscanner/share/type/ShareBatchOcr;-><init>(Landroidx/fragment/app/FragmentActivity;JLjava/util/List;)V

    .line 6
    .line 7
    .line 8
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static 〇O8〇OO〇(Landroidx/fragment/app/FragmentActivity;JLjava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "J",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/intsig/camscanner/share/listener/ShareBackListener;",
            ")V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/ShareHelper;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 4
    .line 5
    .line 6
    sget-object p0, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->DEFAULT:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 7
    .line 8
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper;->o08〇〇0O(Lcom/intsig/camscanner/share/ShareHelper$ShareType;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {v0, p1, p2, p3, p4}, Lcom/intsig/camscanner/share/ShareHelper;->〇〇〇0880(JLjava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public static 〇OO8Oo0〇(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/utils/SharedPreferencesHelper;->O8()Lcom/intsig/utils/SharedPreferencesHelper;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "last_share_type"

    .line 6
    .line 7
    invoke-virtual {v0, v1, p0}, Lcom/intsig/utils/SharedPreferencesHelper;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private synthetic 〇Oo〇o8(Ljava/lang/ref/WeakReference;Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackListener;Lcom/intsig/camscanner/share/listener/ShareBackDataListener;Z)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    check-cast p1, Landroidx/fragment/app/FragmentActivity;

    .line 12
    .line 13
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    if-nez p1, :cond_0

    .line 18
    .line 19
    move-object v0, p0

    .line 20
    move-object v1, p2

    .line 21
    move v2, p3

    .line 22
    move-object v3, p4

    .line 23
    move-object v4, p5

    .line 24
    move v5, p6

    .line 25
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/share/ShareHelper;->O80〇O〇080(Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackListener;Lcom/intsig/camscanner/share/listener/ShareBackDataListener;Z)V

    .line 26
    .line 27
    .line 28
    :cond_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
.end method

.method static bridge synthetic 〇O〇80o08O(Lcom/intsig/camscanner/share/ShareHelper;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO0〇0o:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic 〇o(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/List;Ljava/util/List;Lcom/intsig/camscanner/share/type/BaseShare;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/share/ShareHelper;->OOoo(Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/List;Ljava/util/List;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method static bridge synthetic 〇o0O0O8(Lcom/intsig/camscanner/share/ShareHelper;ILjava/util/List;ZLjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/share/ShareHelper;->〇〇0〇0o8(ILjava/util/List;ZLjava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method private synthetic 〇o8OO0(Ljava/lang/ref/WeakReference;JLjava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    check-cast p1, Landroidx/fragment/app/FragmentActivity;

    .line 12
    .line 13
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    if-nez p1, :cond_0

    .line 18
    .line 19
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/intsig/camscanner/share/ShareHelper;->〇〇〇0880(JLjava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private 〇o8oO()Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_3

    .line 5
    .line 6
    instance-of v0, v0, Lcom/intsig/camscanner/share/type/SendToPc;

    .line 7
    .line 8
    if-nez v0, :cond_3

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 11
    .line 12
    instance-of v2, v0, Lcom/intsig/camscanner/share/channel/item/SendToPcShareChannel;

    .line 13
    .line 14
    if-nez v2, :cond_3

    .line 15
    .line 16
    instance-of v0, v0, Lcom/intsig/camscanner/share/channel/item/LinkShareChannel;

    .line 17
    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->share_page_style:I

    .line 26
    .line 27
    sget-object v2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 28
    .line 29
    new-instance v3, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v4, "sharePdfWhenShareLinkFailure sharePageStyle: "

    .line 35
    .line 36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    const/4 v2, 0x2

    .line 50
    if-eq v0, v2, :cond_1

    .line 51
    .line 52
    const/4 v2, 0x3

    .line 53
    if-ne v0, v2, :cond_2

    .line 54
    .line 55
    :cond_1
    const/4 v1, 0x1

    .line 56
    :cond_2
    return v1

    .line 57
    :cond_3
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 58
    .line 59
    const-string v2, "sharePdfWhenShareLinkFailure is send to pc type, so ignored"

    .line 60
    .line 61
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    return v1
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private 〇oO8O0〇〇O(Ljava/lang/String;Lcom/intsig/camscanner/share/ShareLinkLogger;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const v2, 0x7f131d10

    .line 8
    .line 9
    .line 10
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    const v3, 0x7f13057e

    .line 17
    .line 18
    .line 19
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v3

    .line 23
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 24
    .line 25
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    const v4, 0x7f131d85

    .line 30
    .line 31
    .line 32
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    new-instance v5, Lo〇o8〇〇O/O〇8O8〇008;

    .line 37
    .line 38
    invoke-direct {v5, p2}, Lo〇o8〇〇O/O〇8O8〇008;-><init>(Lcom/intsig/camscanner/share/ShareLinkLogger;)V

    .line 39
    .line 40
    .line 41
    new-instance v6, Lo〇o8〇〇O/O8ooOoo〇;

    .line 42
    .line 43
    invoke-direct {v6, p0, p2}, Lo〇o8〇〇O/O8ooOoo〇;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/ShareLinkLogger;)V

    .line 44
    .line 45
    .line 46
    move-object v2, p1

    .line 47
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/app/DialogUtils;->O8〇o(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;)V

    .line 48
    .line 49
    .line 50
    if-eqz p2, :cond_0

    .line 51
    .line 52
    invoke-virtual {p2}, Lcom/intsig/camscanner/share/ShareLinkLogger;->〇O00()V

    .line 53
    .line 54
    .line 55
    :cond_0
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static synthetic 〇oOO8O8(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/ShareLinkLogger;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/share/ShareHelper;->〇8o〇〇8080(Lcom/intsig/camscanner/share/ShareLinkLogger;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private synthetic 〇oo(I)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    sget-object p1, Lcom/intsig/camscanner/share/type/BaseImagePdf$HandleType;->Original:Lcom/intsig/camscanner/share/type/BaseImagePdf$HandleType;

    .line 4
    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->〇o00〇〇Oo(Lcom/intsig/camscanner/share/type/BaseImagePdf$HandleType;)V

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x1

    .line 10
    if-ne p1, v0, :cond_1

    .line 11
    .line 12
    sget-object p1, Lcom/intsig/camscanner/share/type/BaseImagePdf$HandleType;->Medium:Lcom/intsig/camscanner/share/type/BaseImagePdf$HandleType;

    .line 13
    .line 14
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->〇o00〇〇Oo(Lcom/intsig/camscanner/share/type/BaseImagePdf$HandleType;)V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    const/4 v0, 0x2

    .line 19
    if-ne p1, v0, :cond_2

    .line 20
    .line 21
    sget-object p1, Lcom/intsig/camscanner/share/type/BaseImagePdf$HandleType;->Small:Lcom/intsig/camscanner/share/type/BaseImagePdf$HandleType;

    .line 22
    .line 23
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->〇o00〇〇Oo(Lcom/intsig/camscanner/share/type/BaseImagePdf$HandleType;)V

    .line 24
    .line 25
    .line 26
    :cond_2
    :goto_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static synthetic 〇oo〇(Lcom/intsig/camscanner/share/ShareLinkLogger;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->〇〇00OO(Lcom/intsig/camscanner/share/ShareLinkLogger;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private synthetic 〇o〇8()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O8OO08o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic 〇o〇Oo0(Lcom/intsig/camscanner/tsapp/sync/SyncThread;Ljava/util/List;)V
    .locals 1

    .line 1
    sget-object p2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "prioritySync increase priority end, start sync:"

    .line 4
    .line 5
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08〇o0O:Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;

    .line 9
    .line 10
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->〇O00(Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;)V

    .line 11
    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O8ooOoo〇()Lcom/intsig/camscanner/tsapp/sync/SyncClient;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    const/4 p2, 0x0

    .line 18
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O0o〇〇Oo(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static synthetic 〇〇00OO(Lcom/intsig/camscanner/share/ShareLinkLogger;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    const-string p2, "cancel"

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    const-string p1, "sync_close"

    .line 11
    .line 12
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/ShareLinkLogger;->OO0o〇〇(Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static synthetic 〇〇0o(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/type/BaseShare;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->〇8o(Lcom/intsig/camscanner/share/type/BaseShare;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private synthetic 〇〇0o8O〇〇(Ljava/util/ArrayList;Ljava/util/ArrayList;JI)V
    .locals 14

    .line 1
    move-object v10, p0

    .line 2
    move-object v9, p1

    .line 3
    move-object/from16 v6, p2

    .line 4
    .line 5
    move-wide/from16 v7, p3

    .line 6
    .line 7
    iget-object v0, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    iget-object v0, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    invoke-direct {p0, v0, p1, v6}, Lcom/intsig/camscanner/share/ShareHelper;->O〇0(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    return-void

    .line 25
    :cond_1
    new-instance v11, Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 28
    .line 29
    .line 30
    iget-object v0, v10, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 31
    .line 32
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->O8(Ljava/util/ArrayList;)Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    const/4 v1, 0x0

    .line 37
    const/4 v12, 0x1

    .line 38
    if-eqz v0, :cond_8

    .line 39
    .line 40
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareSecureLink;

    .line 41
    .line 42
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 43
    .line 44
    invoke-direct {v0, v2, p1}, Lcom/intsig/camscanner/share/type/ShareSecureLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, v6}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇8oOO88(Ljava/util/ArrayList;)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    iget-object v0, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 54
    .line 55
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 56
    .line 57
    .line 58
    move-result v2

    .line 59
    if-le v2, v12, :cond_2

    .line 60
    .line 61
    const/4 v2, 0x1

    .line 62
    goto :goto_0

    .line 63
    :cond_2
    const/4 v2, 0x0

    .line 64
    :goto_0
    invoke-static {v0, v2}, Lcom/intsig/camscanner/share/NormalLinkListUtil;->〇080(Landroid/content/Context;Z)Landroid/util/SparseArray;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    .line 69
    .line 70
    .line 71
    move-result v2

    .line 72
    const/4 v3, 0x2

    .line 73
    if-lt v2, v3, :cond_5

    .line 74
    .line 75
    const/4 v2, 0x0

    .line 76
    :goto_1
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    .line 77
    .line 78
    .line 79
    move-result v3

    .line 80
    if-ge v2, v3, :cond_6

    .line 81
    .line 82
    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    .line 83
    .line 84
    .line 85
    move-result-object v3

    .line 86
    check-cast v3, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 87
    .line 88
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 89
    .line 90
    .line 91
    move-result v4

    .line 92
    if-ne v4, v12, :cond_3

    .line 93
    .line 94
    sget-object v4, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->WE_CHAT:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 95
    .line 96
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 97
    .line 98
    .line 99
    move-result v4

    .line 100
    if-eqz v4, :cond_3

    .line 101
    .line 102
    invoke-direct {p0, v3, p1, v6}, Lcom/intsig/camscanner/share/ShareHelper;->o80ooO(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;

    .line 103
    .line 104
    .line 105
    move-result-object v3

    .line 106
    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    .line 108
    .line 109
    goto :goto_2

    .line 110
    :cond_3
    invoke-direct {p0, v3, p1}, Lcom/intsig/camscanner/share/ShareHelper;->〇0O〇Oo(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Z

    .line 111
    .line 112
    .line 113
    move-result v4

    .line 114
    if-eqz v4, :cond_4

    .line 115
    .line 116
    invoke-direct/range {p0 .. p2}, Lcom/intsig/camscanner/share/ShareHelper;->OOo0O(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/ShareFeiShu;

    .line 117
    .line 118
    .line 119
    move-result-object v3

    .line 120
    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    .line 122
    .line 123
    goto :goto_2

    .line 124
    :cond_4
    invoke-direct {p0, v3, p1, v6}, Lcom/intsig/camscanner/share/ShareHelper;->ooO〇00O(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;

    .line 125
    .line 126
    .line 127
    move-result-object v3

    .line 128
    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    .line 130
    .line 131
    :goto_2
    add-int/lit8 v2, v2, 0x1

    .line 132
    .line 133
    goto :goto_1

    .line 134
    :cond_5
    sget-object v0, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->WE_CHAT:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 135
    .line 136
    invoke-direct {p0, v0, p1, v6}, Lcom/intsig/camscanner/share/ShareHelper;->o80ooO(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 141
    .line 142
    const v3, 0x7f1305ed

    .line 143
    .line 144
    .line 145
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v2

    .line 149
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 150
    .line 151
    .line 152
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    .line 154
    .line 155
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareNormalLink;

    .line 156
    .line 157
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 158
    .line 159
    invoke-direct {v0, v2, p1}, Lcom/intsig/camscanner/share/type/ShareNormalLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 160
    .line 161
    .line 162
    invoke-virtual {v0, v6}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇8oOO88(Ljava/util/ArrayList;)V

    .line 163
    .line 164
    .line 165
    const v2, 0x7f080c3d

    .line 166
    .line 167
    .line 168
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 169
    .line 170
    .line 171
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 172
    .line 173
    const v3, 0x7f1305e9

    .line 174
    .line 175
    .line 176
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 177
    .line 178
    .line 179
    move-result-object v2

    .line 180
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 181
    .line 182
    .line 183
    sget-object v2, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->QQ:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 184
    .line 185
    invoke-virtual {v2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getIntentName()Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v3

    .line 189
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->O〇O〇oO(Ljava/lang/String;)V

    .line 190
    .line 191
    .line 192
    invoke-virtual {v2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getName()Ljava/lang/String;

    .line 193
    .line 194
    .line 195
    move-result-object v3

    .line 196
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->〇8(Ljava/lang/String;)V

    .line 197
    .line 198
    .line 199
    invoke-virtual {v2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getPkgName()Ljava/lang/String;

    .line 200
    .line 201
    .line 202
    move-result-object v3

    .line 203
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->o8oO〇(Ljava/lang/String;)V

    .line 204
    .line 205
    .line 206
    invoke-virtual {v2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getPkgName()Ljava/lang/String;

    .line 207
    .line 208
    .line 209
    move-result-object v3

    .line 210
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->ooo〇8oO(Ljava/lang/String;)V

    .line 211
    .line 212
    .line 213
    invoke-virtual {v2}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getActionStr()Ljava/lang/String;

    .line 214
    .line 215
    .line 216
    move-result-object v2

    .line 217
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->oO(Ljava/lang/String;)V

    .line 218
    .line 219
    .line 220
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 221
    .line 222
    .line 223
    :cond_6
    iget-object v0, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 224
    .line 225
    invoke-static {v0, p1, v6}, Lcom/intsig/camscanner/share/type/SendToPc;->ooOO(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/SendToPc;

    .line 226
    .line 227
    .line 228
    move-result-object v0

    .line 229
    const v2, 0x7f080bfa

    .line 230
    .line 231
    .line 232
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 233
    .line 234
    .line 235
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    .line 237
    .line 238
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareCopyLink;

    .line 239
    .line 240
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 241
    .line 242
    invoke-direct {v0, v2, p1, v6}, Lcom/intsig/camscanner/share/type/ShareCopyLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 243
    .line 244
    .line 245
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 246
    .line 247
    .line 248
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o088〇〇()Z

    .line 249
    .line 250
    .line 251
    move-result v0

    .line 252
    if-eqz v0, :cond_7

    .line 253
    .line 254
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareQrCode;

    .line 255
    .line 256
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 257
    .line 258
    invoke-direct {v0, v2, p1}, Lcom/intsig/camscanner/share/type/ShareQrCode;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 259
    .line 260
    .line 261
    invoke-virtual {v0, v6}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇8oOO88(Ljava/util/ArrayList;)V

    .line 262
    .line 263
    .line 264
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 265
    .line 266
    .line 267
    :cond_7
    iget-object v0, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 268
    .line 269
    invoke-direct {p0, v0, v11, p1, v6}, Lcom/intsig/camscanner/share/ShareHelper;->Ooo8〇〇(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 270
    .line 271
    .line 272
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareNormalLink;

    .line 273
    .line 274
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 275
    .line 276
    invoke-direct {v0, v2, p1}, Lcom/intsig/camscanner/share/type/ShareNormalLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 277
    .line 278
    .line 279
    invoke-virtual {v0, v6}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇8oOO88(Ljava/util/ArrayList;)V

    .line 280
    .line 281
    .line 282
    const v2, 0x7f080c2e

    .line 283
    .line 284
    .line 285
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 286
    .line 287
    .line 288
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 289
    .line 290
    const v3, 0x7f130848

    .line 291
    .line 292
    .line 293
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 294
    .line 295
    .line 296
    move-result-object v2

    .line 297
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 298
    .line 299
    .line 300
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/ShareNormalLink;->ooOO()V

    .line 301
    .line 302
    .line 303
    const-string v2, "more"

    .line 304
    .line 305
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->oO(Ljava/lang/String;)V

    .line 306
    .line 307
    .line 308
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    .line 310
    .line 311
    :cond_8
    new-instance v0, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 312
    .line 313
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 314
    .line 315
    invoke-direct {v0, v2, p1, v6}, Lcom/intsig/camscanner/share/type/SharePdf;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 316
    .line 317
    .line 318
    new-instance v2, Lo〇o8〇〇O/o〇8oOO88;

    .line 319
    .line 320
    invoke-direct {v2, p0}, Lo〇o8〇〇O/o〇8oOO88;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 321
    .line 322
    .line 323
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/type/SharePdf;->〇08〇0〇o〇8(Lcom/intsig/camscanner/share/type/SharePdf$OnTryDeductionListener;)V

    .line 324
    .line 325
    .line 326
    const v2, 0x7f080ab3

    .line 327
    .line 328
    .line 329
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 330
    .line 331
    .line 332
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 333
    .line 334
    const v3, 0x7f13084e

    .line 335
    .line 336
    .line 337
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 338
    .line 339
    .line 340
    move-result-object v2

    .line 341
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 342
    .line 343
    .line 344
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    .line 346
    .line 347
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 348
    .line 349
    .line 350
    move-result v0

    .line 351
    if-ne v0, v12, :cond_9

    .line 352
    .line 353
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 354
    .line 355
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 356
    .line 357
    .line 358
    move-result-object v1

    .line 359
    check-cast v1, Ljava/lang/Long;

    .line 360
    .line 361
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    .line 362
    .line 363
    .line 364
    move-result-wide v1

    .line 365
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/ImageDao;->oO(Landroid/content/Context;J)I

    .line 366
    .line 367
    .line 368
    move-result v0

    .line 369
    if-le v0, v12, :cond_9

    .line 370
    .line 371
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareSeparatedPdf;

    .line 372
    .line 373
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 374
    .line 375
    invoke-direct {v0, v1, p1, v6}, Lcom/intsig/camscanner/share/type/ShareSeparatedPdf;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 376
    .line 377
    .line 378
    const v1, 0x7f080aa5

    .line 379
    .line 380
    .line 381
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 382
    .line 383
    .line 384
    const v1, 0x7f080c45

    .line 385
    .line 386
    .line 387
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/BaseShare;->oO00OOO(I)V

    .line 388
    .line 389
    .line 390
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 391
    .line 392
    const v2, 0x7f130da1

    .line 393
    .line 394
    .line 395
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 396
    .line 397
    .line 398
    move-result-object v1

    .line 399
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 400
    .line 401
    .line 402
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 403
    .line 404
    .line 405
    :cond_9
    iget-object v0, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 406
    .line 407
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 408
    .line 409
    .line 410
    move-result-object v0

    .line 411
    invoke-static {v0, v7, v8}, Lcom/intsig/camscanner/tsapp/collaborate/CollaborateUtil;->O8(Landroid/content/Context;J)I

    .line 412
    .line 413
    .line 414
    move-result v0

    .line 415
    if-eq v0, v12, :cond_a

    .line 416
    .line 417
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇0〇o0O8()Z

    .line 418
    .line 419
    .line 420
    move-result v0

    .line 421
    if-eqz v0, :cond_a

    .line 422
    .line 423
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareToWord;

    .line 424
    .line 425
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 426
    .line 427
    invoke-direct {v0, v1, p1, v6}, Lcom/intsig/camscanner/share/type/ShareToWord;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 428
    .line 429
    .line 430
    const v1, 0x7f080e1b

    .line 431
    .line 432
    .line 433
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 434
    .line 435
    .line 436
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 437
    .line 438
    const v2, 0x7f13085c

    .line 439
    .line 440
    .line 441
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 442
    .line 443
    .line 444
    move-result-object v1

    .line 445
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 446
    .line 447
    .line 448
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    .line 450
    .line 451
    :cond_a
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareImage;

    .line 452
    .line 453
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 454
    .line 455
    invoke-direct {v0, v1, p1, v6}, Lcom/intsig/camscanner/share/type/ShareImage;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 456
    .line 457
    .line 458
    const v1, 0x7f080902

    .line 459
    .line 460
    .line 461
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 462
    .line 463
    .line 464
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 465
    .line 466
    const v2, 0x7f130844

    .line 467
    .line 468
    .line 469
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 470
    .line 471
    .line 472
    move-result-object v1

    .line 473
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 474
    .line 475
    .line 476
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 477
    .line 478
    .line 479
    iget-object v0, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 480
    .line 481
    invoke-static {v0, v6}, Lcom/intsig/camscanner/db/dao/ImageDao;->o0ooO(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    .line 482
    .line 483
    .line 484
    move-result-object v0

    .line 485
    new-instance v13, Lcom/intsig/camscanner/share/type/ShareLongImage;

    .line 486
    .line 487
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 488
    .line 489
    new-instance v4, Lcom/intsig/camscanner/share/data_mode/LongImageShareData;

    .line 490
    .line 491
    invoke-direct {v4, v1, v0}, Lcom/intsig/camscanner/share/data_mode/LongImageShareData;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    .line 492
    .line 493
    .line 494
    const/4 v5, 0x1

    .line 495
    move-object v0, v13

    .line 496
    move-object v2, p1

    .line 497
    move-object/from16 v3, p2

    .line 498
    .line 499
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/share/type/ShareLongImage;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/data_mode/LongImageShareData;Z)V

    .line 500
    .line 501
    .line 502
    const v0, 0x7f08098b

    .line 503
    .line 504
    .line 505
    invoke-virtual {v13, v0}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 506
    .line 507
    .line 508
    iget-object v0, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 509
    .line 510
    const v1, 0x7f1307d1

    .line 511
    .line 512
    .line 513
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 514
    .line 515
    .line 516
    move-result-object v0

    .line 517
    invoke-virtual {v13, v0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 518
    .line 519
    .line 520
    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 521
    .line 522
    .line 523
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 524
    .line 525
    .line 526
    move-result-object v0

    .line 527
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->share_preview_style:I

    .line 528
    .line 529
    invoke-static {v0}, Lcom/intsig/camscanner/share/view/share_type/ShareTypeLinkPanelNew;->〇o(I)Z

    .line 530
    .line 531
    .line 532
    move-result v1

    .line 533
    if-nez v1, :cond_b

    .line 534
    .line 535
    invoke-static {v0}, Lcom/intsig/camscanner/share/view/share_type/ShareTypeLinkPanelNew;->O8〇o(I)Z

    .line 536
    .line 537
    .line 538
    move-result v1

    .line 539
    if-eqz v1, :cond_c

    .line 540
    .line 541
    :cond_b
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareAirPrint;

    .line 542
    .line 543
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 544
    .line 545
    invoke-direct {v1, v2, p1, v6}, Lcom/intsig/camscanner/share/type/ShareAirPrint;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 546
    .line 547
    .line 548
    const v2, 0x7f080c3b

    .line 549
    .line 550
    .line 551
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 552
    .line 553
    .line 554
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 555
    .line 556
    const v3, 0x7f13038b

    .line 557
    .line 558
    .line 559
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 560
    .line 561
    .line 562
    move-result-object v2

    .line 563
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 564
    .line 565
    .line 566
    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 567
    .line 568
    .line 569
    :cond_c
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareEmail;

    .line 570
    .line 571
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 572
    .line 573
    invoke-direct {v1, v2, p1}, Lcom/intsig/camscanner/share/type/ShareEmail;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 574
    .line 575
    .line 576
    const v2, 0x7f080c14

    .line 577
    .line 578
    .line 579
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 580
    .line 581
    .line 582
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 583
    .line 584
    const v3, 0x7f130136

    .line 585
    .line 586
    .line 587
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 588
    .line 589
    .line 590
    move-result-object v2

    .line 591
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 592
    .line 593
    .line 594
    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 595
    .line 596
    .line 597
    invoke-static {v0}, Lcom/intsig/camscanner/share/view/share_type/ShareTypeLinkPanelNew;->〇o(I)Z

    .line 598
    .line 599
    .line 600
    move-result v0

    .line 601
    if-eqz v0, :cond_d

    .line 602
    .line 603
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 604
    .line 605
    invoke-static {v0, p1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->Oo08(Landroid/content/Context;Ljava/util/ArrayList;)Z

    .line 606
    .line 607
    .line 608
    move-result v0

    .line 609
    if-nez v0, :cond_d

    .line 610
    .line 611
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareNormalLink;

    .line 612
    .line 613
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 614
    .line 615
    invoke-direct {v0, v1, p1}, Lcom/intsig/camscanner/share/type/ShareNormalLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 616
    .line 617
    .line 618
    invoke-virtual {v0, v6}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇8oOO88(Ljava/util/ArrayList;)V

    .line 619
    .line 620
    .line 621
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 622
    .line 623
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 624
    .line 625
    iget-object v3, v10, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 626
    .line 627
    invoke-virtual {v1, v2, v3}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇〇808〇(Landroid/app/Activity;Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;)Ljava/lang/String;

    .line 628
    .line 629
    .line 630
    move-result-object v1

    .line 631
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/ShareNormalLink;->OOO8o〇〇(Ljava/lang/String;)V

    .line 632
    .line 633
    .line 634
    const v1, 0x7f080c26

    .line 635
    .line 636
    .line 637
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 638
    .line 639
    .line 640
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 641
    .line 642
    const v2, 0x7f130fb1

    .line 643
    .line 644
    .line 645
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 646
    .line 647
    .line 648
    move-result-object v1

    .line 649
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 650
    .line 651
    .line 652
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 653
    .line 654
    .line 655
    :cond_d
    iget-object v0, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 656
    .line 657
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 658
    .line 659
    .line 660
    move-result-object v0

    .line 661
    invoke-static {v0, v7, v8}, Lcom/intsig/camscanner/tsapp/collaborate/CollaborateUtil;->O8(Landroid/content/Context;J)I

    .line 662
    .line 663
    .line 664
    move-result v0

    .line 665
    if-eq v0, v12, :cond_e

    .line 666
    .line 667
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareBatchOcr;

    .line 668
    .line 669
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 670
    .line 671
    invoke-direct {v0, v1, v7, v8, v6}, Lcom/intsig/camscanner/share/type/ShareBatchOcr;-><init>(Landroidx/fragment/app/FragmentActivity;JLjava/util/List;)V

    .line 672
    .line 673
    .line 674
    const v1, 0x7f080d5e

    .line 675
    .line 676
    .line 677
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 678
    .line 679
    .line 680
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 681
    .line 682
    const v2, 0x7f130858

    .line 683
    .line 684
    .line 685
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 686
    .line 687
    .line 688
    move-result-object v1

    .line 689
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 690
    .line 691
    .line 692
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 693
    .line 694
    .line 695
    :cond_e
    iget-object v0, v10, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 696
    .line 697
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 698
    .line 699
    const/4 v3, 0x1

    .line 700
    iget-object v5, v10, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 701
    .line 702
    const/4 v6, 0x1

    .line 703
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO8〇(Ljava/util/ArrayList;)Z

    .line 704
    .line 705
    .line 706
    move-result v7

    .line 707
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 708
    .line 709
    iget-object v4, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 710
    .line 711
    iget-object v8, v10, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 712
    .line 713
    invoke-virtual {v2, v4, v8}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇〇808〇(Landroid/app/Activity;Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;)Ljava/lang/String;

    .line 714
    .line 715
    .line 716
    move-result-object v8

    .line 717
    move-object v2, v11

    .line 718
    move-object v4, p0

    .line 719
    move-object v9, p1

    .line 720
    invoke-interface/range {v0 .. v9}, Lcom/intsig/camscanner/share/listener/ShareUiInterface;->〇080(Landroid/content/Context;Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareTypeClickListener;Lcom/intsig/camscanner/share/ShareHelper$ShareType;ZZLjava/lang/String;Ljava/util/ArrayList;)V

    .line 721
    .line 722
    .line 723
    iget-object v0, v10, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 724
    .line 725
    iget-object v1, v10, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 726
    .line 727
    iget-object v2, v10, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 728
    .line 729
    invoke-virtual {v0, v1, v2, v12}, Lcom/intsig/camscanner/share/ShareDataPresenter;->oo〇(Landroid/app/Activity;Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;Z)V

    .line 730
    .line 731
    .line 732
    return-void
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
.end method

.method private 〇〇0〇0o8(ILjava/util/List;ZLjava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 4
    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, " isWholeDoc: "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string p3, " openCheckShareSpace():"

    .line 19
    .line 20
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8o8o()Z

    .line 24
    .line 25
    .line 26
    move-result p3

    .line 27
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object p3

    .line 34
    invoke-static {v0, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/ShareHelper;->〇000〇〇08()Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 38
    .line 39
    .line 40
    move-result-object p3

    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08〇o0O:Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;

    .line 42
    .line 43
    invoke-virtual {p3, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->ooOO(Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;)V

    .line 44
    .line 45
    .line 46
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 47
    .line 48
    if-eqz p3, :cond_1

    .line 49
    .line 50
    invoke-virtual {p3}, Lcom/intsig/camscanner/share/type/BaseShare;->o800o8O()Lcom/intsig/camscanner/share/ShareLinkLogger;

    .line 51
    .line 52
    .line 53
    move-result-object p3

    .line 54
    goto :goto_0

    .line 55
    :cond_1
    const/4 p3, 0x0

    .line 56
    :goto_0
    const/4 v0, 0x1

    .line 57
    const/4 v1, 0x0

    .line 58
    if-eqz p1, :cond_3

    .line 59
    .line 60
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    if-eqz v2, :cond_2

    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->〇08〇0〇o〇8(I)V

    .line 68
    .line 69
    .line 70
    goto :goto_2

    .line 71
    :cond_3
    :goto_1
    sget-object v2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 72
    .line 73
    new-instance v3, Ljava/lang/StringBuilder;

    .line 74
    .line 75
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    .line 77
    .line 78
    const-string v4, "prioritySync onFinishUploadAll unSyncDocIdList: "

    .line 79
    .line 80
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    const-string v4, ","

    .line 84
    .line 85
    invoke-static {v4, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v4

    .line 89
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v3

    .line 96
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    .line 100
    .line 101
    .line 102
    move-result p2

    .line 103
    if-eqz p2, :cond_4

    .line 104
    .line 105
    const/4 p2, 0x1

    .line 106
    goto :goto_3

    .line 107
    :cond_4
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 108
    .line 109
    const v2, 0x7f130555

    .line 110
    .line 111
    .line 112
    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object p2

    .line 116
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/share/ShareHelper;->o〇(Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    :goto_2
    const/4 p2, 0x0

    .line 120
    :goto_3
    if-eqz p2, :cond_6

    .line 121
    .line 122
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->Oo80:Lcom/intsig/camscanner/share/dialog/SyncDialogClient;

    .line 123
    .line 124
    if-eqz v2, :cond_7

    .line 125
    .line 126
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 127
    .line 128
    instance-of v3, v3, Lcom/intsig/camscanner/share/channel/item/EmailShareChannel;

    .line 129
    .line 130
    if-eqz v3, :cond_5

    .line 131
    .line 132
    invoke-virtual {v2}, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇o00〇〇Oo()V

    .line 133
    .line 134
    .line 135
    goto :goto_4

    .line 136
    :cond_5
    invoke-virtual {v2}, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->OO0o〇〇〇〇0()V

    .line 137
    .line 138
    .line 139
    goto :goto_4

    .line 140
    :cond_6
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->Oo80:Lcom/intsig/camscanner/share/dialog/SyncDialogClient;

    .line 141
    .line 142
    if-eqz v2, :cond_7

    .line 143
    .line 144
    invoke-virtual {v2}, Lcom/intsig/camscanner/share/dialog/SyncDialogClient;->〇o〇()V

    .line 145
    .line 146
    .line 147
    :cond_7
    :goto_4
    if-eqz p3, :cond_a

    .line 148
    .line 149
    if-eqz p2, :cond_8

    .line 150
    .line 151
    invoke-virtual {p3}, Lcom/intsig/camscanner/share/ShareLinkLogger;->O8()V

    .line 152
    .line 153
    .line 154
    goto :goto_5

    .line 155
    :cond_8
    const-string v2, "upload_fail"

    .line 156
    .line 157
    invoke-virtual {p3, v2}, Lcom/intsig/camscanner/share/ShareLinkLogger;->OO0o〇〇(Ljava/lang/String;)V

    .line 158
    .line 159
    .line 160
    new-instance v2, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;

    .line 161
    .line 162
    invoke-direct {v2, p1, v1, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;-><init>(III)V

    .line 163
    .line 164
    .line 165
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 166
    .line 167
    invoke-virtual {p3, v2, p4, v1}, Lcom/intsig/camscanner/share/ShareLinkLogger;->〇oo〇(Lcom/intsig/camscanner/tsapp/sync/SyncErrorCode;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    .line 169
    .line 170
    const/16 p3, 0x139

    .line 171
    .line 172
    if-ne p1, p3, :cond_9

    .line 173
    .line 174
    const-string p1, "\u4e91\u7a7a\u95f4\u8d85\u9650"

    .line 175
    .line 176
    invoke-static {p1}, Lcom/intsig/camscanner/share/LogReceiver;->〇o〇(Ljava/lang/String;)V

    .line 177
    .line 178
    .line 179
    goto :goto_5

    .line 180
    :cond_9
    if-nez p1, :cond_a

    .line 181
    .line 182
    const-string p1, "\u540c\u6b65\u72b6\u6001\u5f02\u5e38"

    .line 183
    .line 184
    invoke-static {p1}, Lcom/intsig/camscanner/share/LogReceiver;->〇o〇(Ljava/lang/String;)V

    .line 185
    .line 186
    .line 187
    :cond_a
    :goto_5
    if-eqz p2, :cond_c

    .line 188
    .line 189
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 190
    .line 191
    if-eqz p1, :cond_b

    .line 192
    .line 193
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/BaseShare;->OO8oO0o〇(Z)V

    .line 194
    .line 195
    .line 196
    :cond_b
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 197
    .line 198
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 199
    .line 200
    .line 201
    :cond_c
    return-void
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private 〇〇O00〇8(I)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const/4 v2, 0x0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto :goto_1

    .line 14
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 15
    .line 16
    new-instance v3, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v4, "showUploadErrorDialog errorCode="

    .line 22
    .line 23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    const-string v4, "; act is null="

    .line 30
    .line 31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    iget-object v4, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 35
    .line 36
    if-nez v4, :cond_1

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    const/4 v1, 0x0

    .line 40
    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    sget-object v0, Lcom/intsig/camscanner/share/ShareLinkAlertHint;->〇〇888:Lcom/intsig/camscanner/share/ShareLinkAlertHint$Companion;

    .line 51
    .line 52
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 53
    .line 54
    invoke-virtual {v0, v1, p1}, Lcom/intsig/camscanner/share/ShareLinkAlertHint$Companion;->Oooo8o0〇(Landroid/app/Activity;I)V

    .line 55
    .line 56
    .line 57
    return-void

    .line 58
    :cond_2
    :goto_1
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 59
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    .line 61
    .line 62
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 63
    .line 64
    .line 65
    const-string v3, "showUploadErrorDialog, but mActivity is null="

    .line 66
    .line 67
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 71
    .line 72
    if-nez v3, :cond_3

    .line 73
    .line 74
    goto :goto_2

    .line 75
    :cond_3
    const/4 v1, 0x0

    .line 76
    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    return-void
    .line 87
.end method

.method static bridge synthetic 〇〇o8(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/type/BaseShare;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->oo0O〇0〇〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇〇〇(Ljava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/intsig/camscanner/share/listener/ShareBackListener;",
            ")V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "share  Docs  size = "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iput-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->o〇00O:Lcom/intsig/camscanner/share/listener/ShareBackListener;

    .line 28
    .line 29
    new-instance p2, Lcom/intsig/camscanner/control/DataChecker;

    .line 30
    .line 31
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 32
    .line 33
    const-wide/16 v5, -0x1

    .line 34
    .line 35
    const/4 v7, 0x0

    .line 36
    sget v8, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O:I

    .line 37
    .line 38
    new-instance v9, Lo〇o8〇〇O/o〇0OOo〇0;

    .line 39
    .line 40
    invoke-direct {v9, p0, p1}, Lo〇o8〇〇O/o〇0OOo〇0;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Ljava/util/ArrayList;)V

    .line 41
    .line 42
    .line 43
    move-object v2, p2

    .line 44
    move-object v4, p1

    .line 45
    invoke-direct/range {v2 .. v9}, Lcom/intsig/camscanner/control/DataChecker;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;JLjava/lang/String;ILcom/intsig/camscanner/control/DataChecker$ActionListener;)V

    .line 46
    .line 47
    .line 48
    const/4 p1, 0x1

    .line 49
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/control/DataChecker;->〇0000OOO(Z)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p2}, Lcom/intsig/camscanner/control/DataChecker;->o〇0()Z

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private 〇〇〇0880(JLjava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/intsig/camscanner/share/listener/ShareBackListener;",
            ")V"
        }
    .end annotation

    .line 1
    move-object/from16 v7, p0

    .line 2
    .line 3
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 4
    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "share  share pages docId = "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    move-wide/from16 v8, p1

    .line 16
    .line 17
    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string v2, "   imageId = "

    .line 21
    .line 22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    move-object/from16 v11, p3

    .line 26
    .line 27
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    new-instance v2, Ljava/lang/ref/WeakReference;

    .line 38
    .line 39
    iget-object v0, v7, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 40
    .line 41
    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 42
    .line 43
    .line 44
    new-instance v10, Lo〇o8〇〇O/oo〇;

    .line 45
    .line 46
    move-object v0, v10

    .line 47
    move-object/from16 v1, p0

    .line 48
    .line 49
    move-wide/from16 v3, p1

    .line 50
    .line 51
    move-object/from16 v5, p3

    .line 52
    .line 53
    move-object/from16 v6, p4

    .line 54
    .line 55
    invoke-direct/range {v0 .. v6}, Lo〇o8〇〇O/oo〇;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Ljava/lang/ref/WeakReference;JLjava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 56
    .line 57
    .line 58
    sget-object v0, Lcom/intsig/camscanner/share/ShareLinkAlertHint;->〇〇888:Lcom/intsig/camscanner/share/ShareLinkAlertHint$Companion;

    .line 59
    .line 60
    invoke-virtual {v0, v10}, Lcom/intsig/camscanner/share/ShareLinkAlertHint$Companion;->OO0o〇〇(Lcom/intsig/callback/Callback0;)V

    .line 61
    .line 62
    .line 63
    move-object/from16 v0, p4

    .line 64
    .line 65
    iput-object v0, v7, Lcom/intsig/camscanner/share/ShareHelper;->o〇00O:Lcom/intsig/camscanner/share/listener/ShareBackListener;

    .line 66
    .line 67
    new-instance v2, Ljava/util/ArrayList;

    .line 68
    .line 69
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .line 71
    .line 72
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    .line 78
    .line 79
    new-instance v6, Lcom/intsig/camscanner/control/DataChecker;

    .line 80
    .line 81
    iget-object v10, v7, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 82
    .line 83
    const/4 v12, 0x0

    .line 84
    const/4 v13, 0x0

    .line 85
    sget v14, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O:I

    .line 86
    .line 87
    new-instance v15, Lo〇o8〇〇O/O8〇o;

    .line 88
    .line 89
    move-object v0, v15

    .line 90
    move-object/from16 v3, p3

    .line 91
    .line 92
    move-wide/from16 v4, p1

    .line 93
    .line 94
    invoke-direct/range {v0 .. v5}, Lo〇o8〇〇O/O8〇o;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Ljava/util/ArrayList;Ljava/util/ArrayList;J)V

    .line 95
    .line 96
    .line 97
    move-object v8, v6

    .line 98
    move-object v9, v10

    .line 99
    move-object v10, v12

    .line 100
    move-object v12, v13

    .line 101
    move v13, v14

    .line 102
    move-object v14, v15

    .line 103
    invoke-direct/range {v8 .. v14}, Lcom/intsig/camscanner/control/DataChecker;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;ILcom/intsig/camscanner/control/DataChecker$ActionListener;)V

    .line 104
    .line 105
    .line 106
    const/4 v0, 0x1

    .line 107
    invoke-virtual {v6, v0}, Lcom/intsig/camscanner/control/DataChecker;->〇0000OOO(Z)V

    .line 108
    .line 109
    .line 110
    invoke-virtual {v6}, Lcom/intsig/camscanner/control/DataChecker;->o〇0()Z

    .line 111
    .line 112
    .line 113
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static synthetic 〇〇〇0〇〇0(Lcom/intsig/camscanner/share/ShareHelper;Ljava/util/ArrayList;Ljava/util/ArrayList;JI)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lcom/intsig/camscanner/share/ShareHelper;->〇〇0o8O〇〇(Ljava/util/ArrayList;Ljava/util/ArrayList;JI)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method


# virtual methods
.method public O00()Lcom/intsig/camscanner/share/type/BaseShare;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8(Lcom/intsig/camscanner/share/type/BaseShare;Landroid/content/pm/ActivityInfo;)V
    .locals 0

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    if-nez p2, :cond_1

    .line 5
    .line 6
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 7
    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_1
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇080()Landroid/content/Intent;

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇〇〇0(Landroid/content/pm/ActivityInfo;)V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public O8888(Lcom/intsig/camscanner/share/listener/ShareBackListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o〇00O:Lcom/intsig/camscanner/share/listener/ShareBackListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public O8O〇88oO0(Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/BaseShare;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/intsig/camscanner/share/type/BaseShare;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareWeiXin;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    invoke-direct {v0, v1, p2}, Lcom/intsig/camscanner/share/type/ShareWeiXin;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)V

    .line 6
    .line 7
    .line 8
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 9
    .line 10
    const v1, 0x7f1305ed

    .line 11
    .line 12
    .line 13
    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object p2

    .line 17
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getTitleLink()I

    .line 21
    .line 22
    .line 23
    move-result p2

    .line 24
    if-eqz p2, :cond_0

    .line 25
    .line 26
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 27
    .line 28
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getTitleLink()I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p2

    .line 36
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/share/type/BaseShare;->OOO(Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getActionStr()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object p2

    .line 43
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 44
    .line 45
    .line 46
    move-result p2

    .line 47
    if-nez p2, :cond_1

    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getActionStr()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/type/BaseShare;->oO(Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    :cond_1
    return-object v0
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V
    .locals 14

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 5
    .line 6
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->O〇〇(Landroid/app/Activity;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o800o8O()Lcom/intsig/camscanner/share/ShareLinkLogger;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    sget-object v1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 14
    .line 15
    new-instance v2, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v3, "User Operation: onShareTypeItemClick = "

    .line 21
    .line 22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O8〇〇o()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    const-string v3, " ,share size = "

    .line 33
    .line 34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇oo〇()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    sget-object v1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 52
    .line 53
    new-instance v2, Ljava/lang/StringBuilder;

    .line 54
    .line 55
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .line 57
    .line 58
    const-string v3, "activity is "

    .line 59
    .line 60
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 64
    .line 65
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v2

    .line 72
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 76
    .line 77
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 78
    .line 79
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/share/ShareHelper;->O〇Oooo〇〇(Landroid/content/Context;)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇00〇8()Z

    .line 83
    .line 84
    .line 85
    move-result v1

    .line 86
    const/4 v2, 0x0

    .line 87
    const/4 v3, 0x1

    .line 88
    if-eqz v1, :cond_3

    .line 89
    .line 90
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 91
    .line 92
    invoke-static {v1}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    xor-int/2addr v1, v3

    .line 97
    if-eqz v1, :cond_1

    .line 98
    .line 99
    const-string v4, "CSErrorInternetPop"

    .line 100
    .line 101
    invoke-static {v4}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    new-instance v4, Lcom/intsig/app/AlertDialog$Builder;

    .line 105
    .line 106
    iget-object v5, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 107
    .line 108
    invoke-direct {v4, v5}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 109
    .line 110
    .line 111
    const v5, 0x7f130f5c

    .line 112
    .line 113
    .line 114
    invoke-virtual {v4, v5}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 115
    .line 116
    .line 117
    move-result-object v4

    .line 118
    const v5, 0x7f130f55

    .line 119
    .line 120
    .line 121
    invoke-virtual {v4, v5}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 122
    .line 123
    .line 124
    move-result-object v4

    .line 125
    const v5, 0x7f13057e

    .line 126
    .line 127
    .line 128
    invoke-virtual {v4, v5, v2}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 129
    .line 130
    .line 131
    move-result-object v4

    .line 132
    new-instance v5, Lo〇o8〇〇O/〇00;

    .line 133
    .line 134
    invoke-direct {v5, p0, p1}, Lo〇o8〇〇O/〇00;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 135
    .line 136
    .line 137
    const v6, 0x7f131db2

    .line 138
    .line 139
    .line 140
    invoke-virtual {v4, v6, v5}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 141
    .line 142
    .line 143
    move-result-object v4

    .line 144
    invoke-virtual {v4}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 145
    .line 146
    .line 147
    move-result-object v4

    .line 148
    invoke-virtual {v4}, Lcom/intsig/app/AlertDialog;->show()V

    .line 149
    .line 150
    .line 151
    :cond_1
    if-eqz v1, :cond_3

    .line 152
    .line 153
    if-eqz v0, :cond_2

    .line 154
    .line 155
    const-string p1, "no_network"

    .line 156
    .line 157
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/ShareLinkLogger;->OO0o〇〇(Ljava/lang/String;)V

    .line 158
    .line 159
    .line 160
    :cond_2
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 161
    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    .line 163
    .line 164
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    .line 166
    .line 167
    const-string v2, "onShareTypeItemClick, share link jump out!  isNetWorkUnavailable="

    .line 168
    .line 169
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    .line 171
    .line 172
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 173
    .line 174
    .line 175
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 176
    .line 177
    .line 178
    move-result-object v0

    .line 179
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    .line 181
    .line 182
    return-void

    .line 183
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇00〇8()Z

    .line 184
    .line 185
    .line 186
    move-result v1

    .line 187
    if-eqz v1, :cond_4

    .line 188
    .line 189
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->O000(Lcom/intsig/camscanner/share/listener/ShareLinkListener;)V

    .line 190
    .line 191
    .line 192
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o8()Z

    .line 193
    .line 194
    .line 195
    move-result v1

    .line 196
    const-string v4, "word"

    .line 197
    .line 198
    if-eqz v1, :cond_9

    .line 199
    .line 200
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 201
    .line 202
    instance-of v1, v1, Lcom/intsig/camscanner/share/type/ShareBatchOcr;

    .line 203
    .line 204
    if-nez v1, :cond_9

    .line 205
    .line 206
    sget-object v1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 207
    .line 208
    const-string v5, "normal user is not vip, show vip dialog"

    .line 209
    .line 210
    invoke-static {v1, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    .line 212
    .line 213
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 214
    .line 215
    .line 216
    move-result v1

    .line 217
    if-nez v1, :cond_8

    .line 218
    .line 219
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/ShareSecureLink;

    .line 220
    .line 221
    if-eqz v1, :cond_5

    .line 222
    .line 223
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 224
    .line 225
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 226
    .line 227
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_SHARE_ENCRYPTION_DOC_LINK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 228
    .line 229
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇00O0:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 230
    .line 231
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 232
    .line 233
    .line 234
    invoke-static {p1, v0}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇0〇O0088o(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 235
    .line 236
    .line 237
    return-void

    .line 238
    :cond_5
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/ShareOcrText;

    .line 239
    .line 240
    if-eqz v1, :cond_6

    .line 241
    .line 242
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 243
    .line 244
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 245
    .line 246
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_SHARE_TXT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 247
    .line 248
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇00O0:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 249
    .line 250
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 251
    .line 252
    .line 253
    invoke-static {p1, v0}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇0〇O0088o(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 254
    .line 255
    .line 256
    return-void

    .line 257
    :cond_6
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/ShareToWord;

    .line 258
    .line 259
    if-eqz v1, :cond_8

    .line 260
    .line 261
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 262
    .line 263
    const-string v0, " showShareApplicationView() 01 ShareToWord"

    .line 264
    .line 265
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    .line 267
    .line 268
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 269
    .line 270
    check-cast p1, Lcom/intsig/camscanner/share/type/ShareToWord;

    .line 271
    .line 272
    iput-object v4, p0, Lcom/intsig/camscanner/share/ShareHelper;->O〇08oOOO0:Ljava/lang/String;

    .line 273
    .line 274
    invoke-static {}, Lcom/intsig/camscanner/pagelist/presenter/WordListPresenter;->o0()Z

    .line 275
    .line 276
    .line 277
    move-result v0

    .line 278
    if-eqz v0, :cond_7

    .line 279
    .line 280
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/ShareToWord;->OO0〇〇8()Z

    .line 281
    .line 282
    .line 283
    move-result v0

    .line 284
    if-nez v0, :cond_7

    .line 285
    .line 286
    new-instance v0, Lcom/intsig/camscanner/share/ShareHelper$2;

    .line 287
    .line 288
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/share/ShareHelper$2;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/type/ShareToWord;)V

    .line 289
    .line 290
    .line 291
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->o〇0()V

    .line 292
    .line 293
    .line 294
    return-void

    .line 295
    :cond_7
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/ShareToWord;->〇8o()V

    .line 296
    .line 297
    .line 298
    return-void

    .line 299
    :cond_8
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇o0〇〇()Z

    .line 300
    .line 301
    .line 302
    move-result v1

    .line 303
    if-nez v1, :cond_9

    .line 304
    .line 305
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/ShareWord;

    .line 306
    .line 307
    if-eqz v1, :cond_9

    .line 308
    .line 309
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 310
    .line 311
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 312
    .line 313
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 314
    .line 315
    .line 316
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_WORD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 317
    .line 318
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 319
    .line 320
    .line 321
    move-result-object v0

    .line 322
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇00O0:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 323
    .line 324
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 325
    .line 326
    .line 327
    move-result-object v0

    .line 328
    invoke-static {p1, v0}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇0〇O0088o(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 329
    .line 330
    .line 331
    return-void

    .line 332
    :cond_9
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇00〇8()Z

    .line 333
    .line 334
    .line 335
    move-result v1

    .line 336
    if-nez v1, :cond_a

    .line 337
    .line 338
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O0o〇O0〇()Z

    .line 339
    .line 340
    .line 341
    move-result v1

    .line 342
    if-eqz v1, :cond_b

    .line 343
    .line 344
    :cond_a
    sget-object v1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 345
    .line 346
    new-instance v5, Ljava/lang/StringBuilder;

    .line 347
    .line 348
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 349
    .line 350
    .line 351
    const-string v6, "onShareTypeItemClick isSyncedSuccess: "

    .line 352
    .line 353
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 354
    .line 355
    .line 356
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇8()Z

    .line 357
    .line 358
    .line 359
    move-result v6

    .line 360
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 361
    .line 362
    .line 363
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 364
    .line 365
    .line 366
    move-result-object v5

    .line 367
    invoke-static {v1, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    .line 369
    .line 370
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇8()Z

    .line 371
    .line 372
    .line 373
    move-result v1

    .line 374
    if-nez v1, :cond_b

    .line 375
    .line 376
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->o〇o(Lcom/intsig/camscanner/share/type/BaseShare;)Z

    .line 377
    .line 378
    .line 379
    move-result v1

    .line 380
    if-nez v1, :cond_b

    .line 381
    .line 382
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 383
    .line 384
    const-string v0, "share stop as not login or is syncing"

    .line 385
    .line 386
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    .line 388
    .line 389
    return-void

    .line 390
    :cond_b
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/BaseImagePdf;

    .line 391
    .line 392
    const-string v5, "CSShare"

    .line 393
    .line 394
    const/4 v6, 0x0

    .line 395
    if-eqz v1, :cond_2d

    .line 396
    .line 397
    move-object v1, p1

    .line 398
    check-cast v1, Lcom/intsig/camscanner/share/type/BaseImagePdf;

    .line 399
    .line 400
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/type/BaseImagePdf;->O〇Oooo〇〇()Ljava/lang/Boolean;

    .line 401
    .line 402
    .line 403
    move-result-object v7

    .line 404
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    .line 405
    .line 406
    .line 407
    move-result v7

    .line 408
    if-nez v7, :cond_2d

    .line 409
    .line 410
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 411
    .line 412
    new-instance v7, Ljava/lang/StringBuilder;

    .line 413
    .line 414
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 415
    .line 416
    .line 417
    const-string v8, "show  "

    .line 418
    .line 419
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 420
    .line 421
    .line 422
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O8〇〇o()Ljava/lang/String;

    .line 423
    .line 424
    .line 425
    move-result-object v8

    .line 426
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 427
    .line 428
    .line 429
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 430
    .line 431
    .line 432
    move-result-object v7

    .line 433
    invoke-static {v0, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    .line 435
    .line 436
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->oo88o8O()J

    .line 437
    .line 438
    .line 439
    move-result-wide v7

    .line 440
    const-wide/32 v9, 0x100000

    .line 441
    .line 442
    .line 443
    cmp-long v0, v7, v9

    .line 444
    .line 445
    if-lez v0, :cond_c

    .line 446
    .line 447
    const/4 v11, 0x1

    .line 448
    goto :goto_0

    .line 449
    :cond_c
    const/4 v11, 0x0

    .line 450
    :goto_0
    instance-of v0, p1, Lcom/intsig/camscanner/share/type/ShareImage;

    .line 451
    .line 452
    if-eqz v0, :cond_10

    .line 453
    .line 454
    move-object v0, p1

    .line 455
    check-cast v0, Lcom/intsig/camscanner/share/type/ShareImage;

    .line 456
    .line 457
    const-string v1, "jpg"

    .line 458
    .line 459
    iput-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->O〇08oOOO0:Ljava/lang/String;

    .line 460
    .line 461
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/ShareImage;->O〇〇()Z

    .line 462
    .line 463
    .line 464
    move-result v1

    .line 465
    if-eqz v1, :cond_d

    .line 466
    .line 467
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/ShareImage;->〇080()Landroid/content/Intent;

    .line 468
    .line 469
    .line 470
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/ShareImage;->o〇〇0〇88()Landroid/content/pm/ActivityInfo;

    .line 471
    .line 472
    .line 473
    move-result-object p1

    .line 474
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇〇〇0(Landroid/content/pm/ActivityInfo;)V

    .line 475
    .line 476
    .line 477
    return-void

    .line 478
    :cond_d
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/ShareImage;->〇8o〇〇8080()Z

    .line 479
    .line 480
    .line 481
    move-result v1

    .line 482
    if-eqz v1, :cond_e

    .line 483
    .line 484
    new-instance v1, Lo〇o8〇〇O/o0ooO;

    .line 485
    .line 486
    invoke-direct {v1, p0, p1, v11}, Lo〇o8〇〇O/o0ooO;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/type/BaseShare;Z)V

    .line 487
    .line 488
    .line 489
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/ShareImage;->〇008〇oo(Lcom/intsig/camscanner/share/type/ShareImage$OnFreeTimesListener;)V

    .line 490
    .line 491
    .line 492
    return-void

    .line 493
    :cond_e
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/ShareImage;->〇oo()Z

    .line 494
    .line 495
    .line 496
    move-result v0

    .line 497
    if-eqz v0, :cond_f

    .line 498
    .line 499
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 500
    .line 501
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 502
    .line 503
    new-instance v2, Lo〇o8〇〇O/〇08O8o〇0;

    .line 504
    .line 505
    invoke-direct {v2, p0, p1, v11}, Lo〇o8〇〇O/〇08O8o〇0;-><init>(Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/type/BaseShare;Z)V

    .line 506
    .line 507
    .line 508
    invoke-interface {v0, v1, v2}, Lcom/intsig/camscanner/share/listener/ShareUiInterface;->〇〇888(Landroid/content/Context;Lcom/intsig/camscanner/share/listener/ShareImageWatermarkListener;)V

    .line 509
    .line 510
    .line 511
    return-void

    .line 512
    :cond_f
    invoke-direct {p0, p1, v11}, Lcom/intsig/camscanner/share/ShareHelper;->〇008〇oo(Lcom/intsig/camscanner/share/type/BaseShare;Z)V

    .line 513
    .line 514
    .line 515
    return-void

    .line 516
    :cond_10
    instance-of v0, p1, Lcom/intsig/camscanner/share/type/ShareToWord;

    .line 517
    .line 518
    if-eqz v0, :cond_11

    .line 519
    .line 520
    iput-object v4, p0, Lcom/intsig/camscanner/share/ShareHelper;->O〇08oOOO0:Ljava/lang/String;

    .line 521
    .line 522
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 523
    .line 524
    const-string v0, " showShareApplicationView() 02 ShareToWord"

    .line 525
    .line 526
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    .line 528
    .line 529
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 530
    .line 531
    check-cast p1, Lcom/intsig/camscanner/share/type/ShareToWord;

    .line 532
    .line 533
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/ShareToWord;->〇8o()V

    .line 534
    .line 535
    .line 536
    return-void

    .line 537
    :cond_11
    instance-of v0, p1, Lcom/intsig/camscanner/share/type/ShareNoWatermark;

    .line 538
    .line 539
    if-eqz v0, :cond_13

    .line 540
    .line 541
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 542
    .line 543
    const-string v1, " show video dialog"

    .line 544
    .line 545
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    .line 547
    .line 548
    move-object v0, p1

    .line 549
    check-cast v0, Lcom/intsig/camscanner/share/type/ShareNoWatermark;

    .line 550
    .line 551
    const-string v1, "pdf_watermark_free_click"

    .line 552
    .line 553
    invoke-static {v5, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    .line 555
    .line 556
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/ShareNoWatermark;->O0o8〇O()I

    .line 557
    .line 558
    .line 559
    move-result v0

    .line 560
    if-lez v0, :cond_12

    .line 561
    .line 562
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 563
    .line 564
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 565
    .line 566
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 567
    .line 568
    invoke-interface {v0, v1, v2, p1}, Lcom/intsig/camscanner/share/listener/ShareUiInterface;->oO80(Landroid/content/Context;Lcom/intsig/camscanner/share/ShareHelper$ShareType;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 569
    .line 570
    .line 571
    :cond_12
    return-void

    .line 572
    :cond_13
    instance-of v0, p1, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 573
    .line 574
    if-eqz v0, :cond_3e

    .line 575
    .line 576
    const-string v0, "pdf"

    .line 577
    .line 578
    iput-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->O〇08oOOO0:Ljava/lang/String;

    .line 579
    .line 580
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 581
    .line 582
    if-nez v0, :cond_14

    .line 583
    .line 584
    const-string v0, "share_type"

    .line 585
    .line 586
    const-string v4, "transfer_pdf"

    .line 587
    .line 588
    invoke-static {v5, v4, v0, v4}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    .line 590
    .line 591
    :cond_14
    move-object v12, p1

    .line 592
    check-cast v12, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 593
    .line 594
    invoke-virtual {v12}, Lcom/intsig/camscanner/share/type/SharePdf;->O88〇〇o0O()Z

    .line 595
    .line 596
    .line 597
    move-result v0

    .line 598
    if-eqz v0, :cond_1a

    .line 599
    .line 600
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 601
    .line 602
    const-string v2, "isPreviewPdf"

    .line 603
    .line 604
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    .line 606
    .line 607
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇〇888()Ljava/util/ArrayList;

    .line 608
    .line 609
    .line 610
    move-result-object p1

    .line 611
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 612
    .line 613
    .line 614
    move-result-object p1

    .line 615
    check-cast p1, Ljava/lang/Long;

    .line 616
    .line 617
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/type/BaseImagePdf;->o〇o()Ljava/util/ArrayList;

    .line 618
    .line 619
    .line 620
    move-result-object v0

    .line 621
    if-eqz v0, :cond_19

    .line 622
    .line 623
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    .line 624
    .line 625
    .line 626
    move-result v1

    .line 627
    if-eqz v1, :cond_15

    .line 628
    .line 629
    goto :goto_3

    .line 630
    :cond_15
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 631
    .line 632
    .line 633
    move-result v1

    .line 634
    new-array v2, v1, [J

    .line 635
    .line 636
    const/4 v4, 0x0

    .line 637
    :goto_1
    if-ge v4, v1, :cond_16

    .line 638
    .line 639
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 640
    .line 641
    .line 642
    move-result-object v5

    .line 643
    check-cast v5, Ljava/lang/Long;

    .line 644
    .line 645
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    .line 646
    .line 647
    .line 648
    move-result-wide v7

    .line 649
    aput-wide v7, v2, v4

    .line 650
    .line 651
    add-int/lit8 v4, v4, 0x1

    .line 652
    .line 653
    goto :goto_1

    .line 654
    :cond_16
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 655
    .line 656
    if-eqz v0, :cond_18

    .line 657
    .line 658
    sget-object v1, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->EMAIL_MYSELF:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 659
    .line 660
    if-eq v0, v1, :cond_17

    .line 661
    .line 662
    sget-object v1, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->EMAIL_OTHER:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 663
    .line 664
    if-ne v0, v1, :cond_18

    .line 665
    .line 666
    :cond_17
    invoke-virtual {v12, p1, v2, v3}, Lcom/intsig/camscanner/share/type/SharePdf;->〇OO8Oo0〇(Ljava/lang/Long;[JZ)V

    .line 667
    .line 668
    .line 669
    goto :goto_2

    .line 670
    :cond_18
    invoke-virtual {v12, p1, v2, v6}, Lcom/intsig/camscanner/share/type/SharePdf;->〇OO8Oo0〇(Ljava/lang/Long;[JZ)V

    .line 671
    .line 672
    .line 673
    :goto_2
    return-void

    .line 674
    :cond_19
    :goto_3
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O8OO08o()V

    .line 675
    .line 676
    .line 677
    return-void

    .line 678
    :cond_1a
    invoke-virtual {v12}, Lcom/intsig/camscanner/share/type/SharePdf;->o08〇〇0O()Z

    .line 679
    .line 680
    .line 681
    move-result v0

    .line 682
    if-nez v0, :cond_3e

    .line 683
    .line 684
    new-instance v0, Ljava/util/ArrayList;

    .line 685
    .line 686
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 687
    .line 688
    .line 689
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0〇0()I

    .line 690
    .line 691
    .line 692
    move-result v4

    .line 693
    const/4 v7, 0x2

    .line 694
    if-ne v4, v7, :cond_1b

    .line 695
    .line 696
    iget-object v4, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 697
    .line 698
    invoke-direct {p0, v4, v6, v0}, Lcom/intsig/camscanner/share/ShareHelper;->〇8o8O〇O(Landroid/content/Context;ZLjava/util/List;)V

    .line 699
    .line 700
    .line 701
    goto :goto_4

    .line 702
    :cond_1b
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0〇0()I

    .line 703
    .line 704
    .line 705
    move-result v4

    .line 706
    const/4 v7, 0x3

    .line 707
    if-ne v4, v7, :cond_1c

    .line 708
    .line 709
    iget-object v4, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 710
    .line 711
    invoke-direct {p0, v4, v3, v0}, Lcom/intsig/camscanner/share/ShareHelper;->〇8o8O〇O(Landroid/content/Context;ZLjava/util/List;)V

    .line 712
    .line 713
    .line 714
    :cond_1c
    :goto_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 715
    .line 716
    .line 717
    move-result v4

    .line 718
    if-le v4, v3, :cond_1d

    .line 719
    .line 720
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇o()Z

    .line 721
    .line 722
    .line 723
    move-result v4

    .line 724
    if-eqz v4, :cond_1e

    .line 725
    .line 726
    iget-object v8, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 727
    .line 728
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->oo88o8O()J

    .line 729
    .line 730
    .line 731
    move-result-wide v9

    .line 732
    move-object v7, p0

    .line 733
    move-object v12, v0

    .line 734
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/share/ShareHelper;->〇8〇o〇8(Landroid/content/Context;JZLjava/util/List;)V

    .line 735
    .line 736
    .line 737
    return-void

    .line 738
    :cond_1d
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 739
    .line 740
    .line 741
    move-result v0

    .line 742
    if-ne v0, v3, :cond_1f

    .line 743
    .line 744
    :cond_1e
    const/4 v0, 0x1

    .line 745
    goto :goto_5

    .line 746
    :cond_1f
    const/4 v0, 0x0

    .line 747
    :goto_5
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0〇0()I

    .line 748
    .line 749
    .line 750
    move-result v4

    .line 751
    if-eq v4, v3, :cond_20

    .line 752
    .line 753
    if-eqz v0, :cond_22

    .line 754
    .line 755
    :cond_20
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 756
    .line 757
    const-string v4, "SHARE_MODE_1"

    .line 758
    .line 759
    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    .line 761
    .line 762
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇o0〇〇()Z

    .line 763
    .line 764
    .line 765
    move-result v0

    .line 766
    if-nez v0, :cond_21

    .line 767
    .line 768
    iget-object v7, p0, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 769
    .line 770
    iget-object v8, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 771
    .line 772
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->oo88o8O()J

    .line 773
    .line 774
    .line 775
    move-result-wide v9

    .line 776
    move-object v13, p0

    .line 777
    invoke-interface/range {v7 .. v13}, Lcom/intsig/camscanner/share/listener/ShareUiInterface;->〇o〇(Landroid/content/Context;JZLcom/intsig/camscanner/share/type/SharePdf;Lcom/intsig/camscanner/share/listener/ShareCompressSelectListener;)V

    .line 778
    .line 779
    .line 780
    return-void

    .line 781
    :cond_21
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o8oO〇()Z

    .line 782
    .line 783
    .line 784
    move-result v0

    .line 785
    if-eqz v0, :cond_2c

    .line 786
    .line 787
    if-eqz v11, :cond_22

    .line 788
    .line 789
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 790
    .line 791
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 792
    .line 793
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->oo88o8O()J

    .line 794
    .line 795
    .line 796
    move-result-wide v2

    .line 797
    invoke-interface {v0, v1, v2, v3, p0}, Lcom/intsig/camscanner/share/listener/ShareUiInterface;->〇o00〇〇Oo(Landroid/content/Context;JLcom/intsig/camscanner/share/listener/ShareCompressSelectListener;)V

    .line 798
    .line 799
    .line 800
    return-void

    .line 801
    :cond_22
    invoke-virtual {v12}, Lcom/intsig/camscanner/share/type/SharePdf;->O〇oO〇oo8o()Z

    .line 802
    .line 803
    .line 804
    move-result v0

    .line 805
    if-nez v0, :cond_2a

    .line 806
    .line 807
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0〇0()I

    .line 808
    .line 809
    .line 810
    move-result v0

    .line 811
    const/4 v4, 0x4

    .line 812
    if-ne v0, v4, :cond_2a

    .line 813
    .line 814
    invoke-static {}, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇o〇()Lcom/intsig/camscanner/util/CurrentAppInfo;

    .line 815
    .line 816
    .line 817
    move-result-object v0

    .line 818
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/CurrentAppInfo;->oO80()Z

    .line 819
    .line 820
    .line 821
    move-result v0

    .line 822
    if-eqz v0, :cond_2a

    .line 823
    .line 824
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 825
    .line 826
    const-string v2, "SHARE_MODE_4"

    .line 827
    .line 828
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    .line 830
    .line 831
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;

    .line 832
    .line 833
    if-eqz v0, :cond_23

    .line 834
    .line 835
    invoke-interface {v0}, Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;->〇080()Z

    .line 836
    .line 837
    .line 838
    move-result v0

    .line 839
    if-eqz v0, :cond_23

    .line 840
    .line 841
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O8OO08o()V

    .line 842
    .line 843
    .line 844
    return-void

    .line 845
    :cond_23
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇〇888()Ljava/util/ArrayList;

    .line 846
    .line 847
    .line 848
    move-result-object v0

    .line 849
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 850
    .line 851
    .line 852
    move-result v0

    .line 853
    if-le v0, v3, :cond_24

    .line 854
    .line 855
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O8OO08o()V

    .line 856
    .line 857
    .line 858
    return-void

    .line 859
    :cond_24
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇〇888()Ljava/util/ArrayList;

    .line 860
    .line 861
    .line 862
    move-result-object p1

    .line 863
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 864
    .line 865
    .line 866
    move-result-object p1

    .line 867
    check-cast p1, Ljava/lang/Long;

    .line 868
    .line 869
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/type/BaseImagePdf;->o〇o()Ljava/util/ArrayList;

    .line 870
    .line 871
    .line 872
    move-result-object v0

    .line 873
    if-eqz v0, :cond_29

    .line 874
    .line 875
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    .line 876
    .line 877
    .line 878
    move-result v1

    .line 879
    if-eqz v1, :cond_25

    .line 880
    .line 881
    goto :goto_8

    .line 882
    :cond_25
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 883
    .line 884
    .line 885
    move-result v1

    .line 886
    new-array v2, v1, [J

    .line 887
    .line 888
    const/4 v4, 0x0

    .line 889
    :goto_6
    if-ge v4, v1, :cond_26

    .line 890
    .line 891
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 892
    .line 893
    .line 894
    move-result-object v5

    .line 895
    check-cast v5, Ljava/lang/Long;

    .line 896
    .line 897
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    .line 898
    .line 899
    .line 900
    move-result-wide v7

    .line 901
    aput-wide v7, v2, v4

    .line 902
    .line 903
    add-int/lit8 v4, v4, 0x1

    .line 904
    .line 905
    goto :goto_6

    .line 906
    :cond_26
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 907
    .line 908
    if-eqz v0, :cond_28

    .line 909
    .line 910
    sget-object v1, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->EMAIL_MYSELF:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 911
    .line 912
    if-eq v0, v1, :cond_27

    .line 913
    .line 914
    sget-object v1, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->EMAIL_OTHER:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 915
    .line 916
    if-ne v0, v1, :cond_28

    .line 917
    .line 918
    :cond_27
    invoke-virtual {v12, p1, v2, v3}, Lcom/intsig/camscanner/share/type/SharePdf;->〇OO8Oo0〇(Ljava/lang/Long;[JZ)V

    .line 919
    .line 920
    .line 921
    goto :goto_7

    .line 922
    :cond_28
    invoke-virtual {v12, p1, v2, v6}, Lcom/intsig/camscanner/share/type/SharePdf;->〇OO8Oo0〇(Ljava/lang/Long;[JZ)V

    .line 923
    .line 924
    .line 925
    :goto_7
    return-void

    .line 926
    :cond_29
    :goto_8
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O8OO08o()V

    .line 927
    .line 928
    .line 929
    return-void

    .line 930
    :cond_2a
    if-eqz v11, :cond_3e

    .line 931
    .line 932
    invoke-virtual {v12}, Lcom/intsig/camscanner/share/type/SharePdf;->O8888()Z

    .line 933
    .line 934
    .line 935
    move-result v0

    .line 936
    if-nez v0, :cond_3e

    .line 937
    .line 938
    invoke-virtual {v12}, Lcom/intsig/camscanner/share/type/SharePdf;->O〇oO〇oo8o()Z

    .line 939
    .line 940
    .line 941
    move-result v0

    .line 942
    if-nez v0, :cond_3e

    .line 943
    .line 944
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0〇0()I

    .line 945
    .line 946
    .line 947
    move-result v0

    .line 948
    if-nez v0, :cond_2b

    .line 949
    .line 950
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 951
    .line 952
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->oo88o8O()J

    .line 953
    .line 954
    .line 955
    move-result-wide v1

    .line 956
    invoke-direct {p0, v0, v1, v2}, Lcom/intsig/camscanner/share/ShareHelper;->OO〇(Landroid/content/Context;J)V

    .line 957
    .line 958
    .line 959
    goto :goto_9

    .line 960
    :cond_2b
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 961
    .line 962
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 963
    .line 964
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->oo88o8O()J

    .line 965
    .line 966
    .line 967
    move-result-wide v2

    .line 968
    invoke-interface {v0, v1, v2, v3, p0}, Lcom/intsig/camscanner/share/listener/ShareUiInterface;->OO0o〇〇〇〇0(Landroid/content/Context;JLcom/intsig/camscanner/share/listener/ShareCompressSelectListener;)V

    .line 969
    .line 970
    .line 971
    :goto_9
    return-void

    .line 972
    :cond_2c
    iget-object v7, p0, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 973
    .line 974
    iget-object v8, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 975
    .line 976
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->oo88o8O()J

    .line 977
    .line 978
    .line 979
    move-result-wide v9

    .line 980
    move-object v13, p0

    .line 981
    invoke-interface/range {v7 .. v13}, Lcom/intsig/camscanner/share/listener/ShareUiInterface;->〇o〇(Landroid/content/Context;JZLcom/intsig/camscanner/share/type/SharePdf;Lcom/intsig/camscanner/share/listener/ShareCompressSelectListener;)V

    .line 982
    .line 983
    .line 984
    return-void

    .line 985
    :cond_2d
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/ShareOcrText;

    .line 986
    .line 987
    if-eqz v1, :cond_2e

    .line 988
    .line 989
    const-string v0, "text"

    .line 990
    .line 991
    iput-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->O〇08oOOO0:Ljava/lang/String;

    .line 992
    .line 993
    iput-boolean v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO〇00〇8oO:Z

    .line 994
    .line 995
    iput v6, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO0〇0o:I

    .line 996
    .line 997
    goto/16 :goto_a

    .line 998
    .line 999
    :cond_2e
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/ShareSecureLink;

    .line 1000
    .line 1001
    const-string v4, "link"

    .line 1002
    .line 1003
    if-eqz v1, :cond_2f

    .line 1004
    .line 1005
    iput-object v4, p0, Lcom/intsig/camscanner/share/ShareHelper;->O〇08oOOO0:Ljava/lang/String;

    .line 1006
    .line 1007
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 1008
    .line 1009
    const-string v0, "go to edit secureLink password "

    .line 1010
    .line 1011
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 1012
    .line 1013
    .line 1014
    new-instance p1, Landroid/content/Intent;

    .line 1015
    .line 1016
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 1017
    .line 1018
    const-class v1, Lcom/intsig/camscanner/share/view/SecureLinkActivity;

    .line 1019
    .line 1020
    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1021
    .line 1022
    .line 1023
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 1024
    .line 1025
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->O8o08O8O:Lcom/intsig/utils/activity/ActivityLifeCircleManager;

    .line 1026
    .line 1027
    const/16 v2, 0x2763

    .line 1028
    .line 1029
    invoke-virtual {v0, v1, p1, v2}, Lcom/intsig/camscanner/share/ShareDataPresenter;->startActivityForResult(Lcom/intsig/utils/activity/ActivityLifeCircleManager;Landroid/content/Intent;I)V

    .line 1030
    .line 1031
    .line 1032
    return-void

    .line 1033
    :cond_2f
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/ShareWeiXin;

    .line 1034
    .line 1035
    const-string v7, "app_not_install"

    .line 1036
    .line 1037
    if-eqz v1, :cond_33

    .line 1038
    .line 1039
    iput-object v4, p0, Lcom/intsig/camscanner/share/ShareHelper;->O〇08oOOO0:Ljava/lang/String;

    .line 1040
    .line 1041
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 1042
    .line 1043
    invoke-static {v1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->o〇O8〇〇o(Landroid/content/Context;)Z

    .line 1044
    .line 1045
    .line 1046
    move-result v1

    .line 1047
    if-nez v1, :cond_32

    .line 1048
    .line 1049
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇80〇808〇O()Lcom/intsig/camscanner/share/LinkPanelShareType;

    .line 1050
    .line 1051
    .line 1052
    move-result-object p1

    .line 1053
    sget-object v1, Lcom/intsig/camscanner/share/LinkPanelShareType;->LINK_SHARE_GRID_ITEM:Lcom/intsig/camscanner/share/LinkPanelShareType;

    .line 1054
    .line 1055
    if-ne p1, v1, :cond_30

    .line 1056
    .line 1057
    invoke-static {v3, v3}, Lcom/intsig/camscanner/share/ShareRecorder;->Oo08(II)V

    .line 1058
    .line 1059
    .line 1060
    :cond_30
    if-eqz v0, :cond_31

    .line 1061
    .line 1062
    invoke-virtual {v0, v7}, Lcom/intsig/camscanner/share/ShareLinkLogger;->OO0o〇〇(Ljava/lang/String;)V

    .line 1063
    .line 1064
    .line 1065
    :cond_31
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 1066
    .line 1067
    const v0, 0x7f1305e5

    .line 1068
    .line 1069
    .line 1070
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 1071
    .line 1072
    .line 1073
    return-void

    .line 1074
    :cond_32
    check-cast p1, Lcom/intsig/camscanner/share/type/ShareWeiXin;

    .line 1075
    .line 1076
    new-instance v0, Lcom/intsig/camscanner/share/ShareHelper$3;

    .line 1077
    .line 1078
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/share/ShareHelper$3;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 1079
    .line 1080
    .line 1081
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/ShareWeiXin;->O0(Lcom/intsig/camscanner/share/listener/BaseShareListener;)V

    .line 1082
    .line 1083
    .line 1084
    return-void

    .line 1085
    :cond_33
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/ShareInviteMembers;

    .line 1086
    .line 1087
    if-eqz v1, :cond_34

    .line 1088
    .line 1089
    check-cast p1, Lcom/intsig/camscanner/share/type/ShareInviteMembers;

    .line 1090
    .line 1091
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/ShareInviteMembers;->O8O〇()V

    .line 1092
    .line 1093
    .line 1094
    return-void

    .line 1095
    :cond_34
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/ShareQQMiniProgram;

    .line 1096
    .line 1097
    if-eqz v1, :cond_35

    .line 1098
    .line 1099
    check-cast p1, Lcom/intsig/camscanner/share/type/ShareQQMiniProgram;

    .line 1100
    .line 1101
    new-instance v0, Lo〇o8〇〇O/oO;

    .line 1102
    .line 1103
    invoke-direct {v0, p0}, Lo〇o8〇〇O/oO;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 1104
    .line 1105
    .line 1106
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/ShareQQMiniProgram;->〇o0O0O8(Lcom/intsig/camscanner/share/listener/BaseShareListener;)V

    .line 1107
    .line 1108
    .line 1109
    return-void

    .line 1110
    :cond_35
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/ShareFeiShu;

    .line 1111
    .line 1112
    if-eqz v1, :cond_36

    .line 1113
    .line 1114
    check-cast p1, Lcom/intsig/camscanner/share/type/ShareFeiShu;

    .line 1115
    .line 1116
    new-instance v0, Lo〇o8〇〇O/〇8;

    .line 1117
    .line 1118
    invoke-direct {v0, p0}, Lo〇o8〇〇O/〇8;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 1119
    .line 1120
    .line 1121
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/ShareFeiShu;->〇o0O0O8(Lcom/intsig/camscanner/share/listener/BaseShareListener;)V

    .line 1122
    .line 1123
    .line 1124
    return-void

    .line 1125
    :cond_36
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/ShareWord;

    .line 1126
    .line 1127
    if-eqz v1, :cond_37

    .line 1128
    .line 1129
    move-object v1, p1

    .line 1130
    check-cast v1, Lcom/intsig/camscanner/share/type/ShareWord;

    .line 1131
    .line 1132
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/type/ShareWord;->o〇o()Z

    .line 1133
    .line 1134
    .line 1135
    move-result v1

    .line 1136
    if-eqz v1, :cond_37

    .line 1137
    .line 1138
    iput-boolean v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO〇00〇8oO:Z

    .line 1139
    .line 1140
    iput v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO0〇0o:I

    .line 1141
    .line 1142
    goto/16 :goto_a

    .line 1143
    .line 1144
    :cond_37
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/ShareBatchOcr;

    .line 1145
    .line 1146
    if-eqz v1, :cond_38

    .line 1147
    .line 1148
    iput-boolean v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO〇00〇8oO:Z

    .line 1149
    .line 1150
    iput v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO0〇0o:I

    .line 1151
    .line 1152
    goto/16 :goto_a

    .line 1153
    .line 1154
    :cond_38
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/ShareWhatsApp;

    .line 1155
    .line 1156
    if-eqz v1, :cond_3a

    .line 1157
    .line 1158
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 1159
    .line 1160
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇080()Landroid/content/Intent;

    .line 1161
    .line 1162
    .line 1163
    move-result-object p1

    .line 1164
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇80〇808〇O(Landroid/content/Intent;)Ljava/util/ArrayList;

    .line 1165
    .line 1166
    .line 1167
    move-result-object p1

    .line 1168
    if-eqz p1, :cond_39

    .line 1169
    .line 1170
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 1171
    .line 1172
    .line 1173
    move-result v0

    .line 1174
    if-lez v0, :cond_39

    .line 1175
    .line 1176
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 1177
    .line 1178
    .line 1179
    move-result-object p1

    .line 1180
    check-cast p1, Landroid/content/pm/ResolveInfo;

    .line 1181
    .line 1182
    iget-object p1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 1183
    .line 1184
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇〇〇0(Landroid/content/pm/ActivityInfo;)V

    .line 1185
    .line 1186
    .line 1187
    :cond_39
    return-void

    .line 1188
    :cond_3a
    instance-of v1, p1, Lcom/intsig/camscanner/share/type/ShareNormalLink;

    .line 1189
    .line 1190
    if-eqz v1, :cond_3d

    .line 1191
    .line 1192
    iput-object v4, p0, Lcom/intsig/camscanner/share/ShareHelper;->O〇08oOOO0:Ljava/lang/String;

    .line 1193
    .line 1194
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->OO0o〇〇()Ljava/lang/String;

    .line 1195
    .line 1196
    .line 1197
    move-result-object v1

    .line 1198
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 1199
    .line 1200
    .line 1201
    move-result v1

    .line 1202
    if-nez v1, :cond_3e

    .line 1203
    .line 1204
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 1205
    .line 1206
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->OO0o〇〇()Ljava/lang/String;

    .line 1207
    .line 1208
    .line 1209
    move-result-object v4

    .line 1210
    invoke-static {v1, v4}, Lcom/intsig/camscanner/app/AppUtil;->〇8〇0〇o〇O(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1211
    .line 1212
    .line 1213
    move-result v1

    .line 1214
    if-nez v1, :cond_3e

    .line 1215
    .line 1216
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇80〇808〇O()Lcom/intsig/camscanner/share/LinkPanelShareType;

    .line 1217
    .line 1218
    .line 1219
    move-result-object v1

    .line 1220
    sget-object v2, Lcom/intsig/camscanner/share/LinkPanelShareType;->LINK_SHARE_GRID_ITEM:Lcom/intsig/camscanner/share/LinkPanelShareType;

    .line 1221
    .line 1222
    if-ne v1, v2, :cond_3b

    .line 1223
    .line 1224
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->OO0o〇〇()Ljava/lang/String;

    .line 1225
    .line 1226
    .line 1227
    move-result-object v1

    .line 1228
    invoke-static {v1}, Lcom/intsig/camscanner/share/ShareRecorder;->〇〇888(Ljava/lang/String;)I

    .line 1229
    .line 1230
    .line 1231
    move-result v1

    .line 1232
    invoke-static {v3, v1}, Lcom/intsig/camscanner/share/ShareRecorder;->Oo08(II)V

    .line 1233
    .line 1234
    .line 1235
    :cond_3b
    if-eqz v0, :cond_3c

    .line 1236
    .line 1237
    invoke-virtual {v0, v7}, Lcom/intsig/camscanner/share/ShareLinkLogger;->OO0o〇〇(Ljava/lang/String;)V

    .line 1238
    .line 1239
    .line 1240
    :cond_3c
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 1241
    .line 1242
    new-array v1, v3, [Ljava/lang/Object;

    .line 1243
    .line 1244
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇0()Ljava/lang/String;

    .line 1245
    .line 1246
    .line 1247
    move-result-object p1

    .line 1248
    aput-object p1, v1, v6

    .line 1249
    .line 1250
    const p1, 0x7f130822

    .line 1251
    .line 1252
    .line 1253
    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 1254
    .line 1255
    .line 1256
    move-result-object p1

    .line 1257
    invoke-static {v0, p1}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 1258
    .line 1259
    .line 1260
    return-void

    .line 1261
    :cond_3d
    instance-of v0, p1, Lcom/intsig/camscanner/share/type/ShareLongImage;

    .line 1262
    .line 1263
    if-eqz v0, :cond_3e

    .line 1264
    .line 1265
    const-string v0, "long_pic"

    .line 1266
    .line 1267
    iput-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->O〇08oOOO0:Ljava/lang/String;

    .line 1268
    .line 1269
    move-object v0, p1

    .line 1270
    check-cast v0, Lcom/intsig/camscanner/share/type/ShareLongImage;

    .line 1271
    .line 1272
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/ShareLongImage;->o8O〇()Z

    .line 1273
    .line 1274
    .line 1275
    move-result v1

    .line 1276
    if-eqz v1, :cond_3e

    .line 1277
    .line 1278
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/ShareLongImage;->Oo〇O()V

    .line 1279
    .line 1280
    .line 1281
    return-void

    .line 1282
    :cond_3e
    :goto_a
    instance-of v0, p1, Lcom/intsig/camscanner/share/type/ShareCopyLink;

    .line 1283
    .line 1284
    if-eqz v0, :cond_3f

    .line 1285
    .line 1286
    const-string v0, "copy_link"

    .line 1287
    .line 1288
    invoke-static {v5, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 1289
    .line 1290
    .line 1291
    invoke-virtual {p1, v2, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇〇0o(Landroid/content/pm/ActivityInfo;Lcom/intsig/camscanner/share/listener/BaseShareListener;)V

    .line 1292
    .line 1293
    .line 1294
    return-void

    .line 1295
    :cond_3f
    instance-of v0, p1, Lcom/intsig/camscanner/share/type/ShareDirectLink;

    .line 1296
    .line 1297
    if-eqz v0, :cond_40

    .line 1298
    .line 1299
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 1300
    .line 1301
    const-string v1, "direct copy link, return"

    .line 1302
    .line 1303
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 1304
    .line 1305
    .line 1306
    invoke-virtual {p1, v2, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇〇0o(Landroid/content/pm/ActivityInfo;Lcom/intsig/camscanner/share/listener/BaseShareListener;)V

    .line 1307
    .line 1308
    .line 1309
    return-void

    .line 1310
    :cond_40
    instance-of v0, p1, Lcom/intsig/camscanner/share/type/ShareQrCode;

    .line 1311
    .line 1312
    if-eqz v0, :cond_41

    .line 1313
    .line 1314
    check-cast p1, Lcom/intsig/camscanner/share/type/ShareQrCode;

    .line 1315
    .line 1316
    new-instance v0, Lo〇o8〇〇O/O08000;

    .line 1317
    .line 1318
    invoke-direct {v0, p0}, Lo〇o8〇〇O/O08000;-><init>(Lcom/intsig/camscanner/share/ShareHelper;)V

    .line 1319
    .line 1320
    .line 1321
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/ShareQrCode;->OOO8o〇〇(Lcom/intsig/camscanner/share/listener/ActionListener;)V

    .line 1322
    .line 1323
    .line 1324
    return-void

    .line 1325
    :cond_41
    instance-of v0, p1, Lcom/intsig/camscanner/share/type/ShareSaveDCIM;

    .line 1326
    .line 1327
    if-eqz v0, :cond_42

    .line 1328
    .line 1329
    check-cast p1, Lcom/intsig/camscanner/share/type/ShareSaveDCIM;

    .line 1330
    .line 1331
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/ShareSaveDCIM;->O8O〇()V

    .line 1332
    .line 1333
    .line 1334
    return-void

    .line 1335
    :cond_42
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O8OO08o()V

    .line 1336
    .line 1337
    .line 1338
    return-void
.end method

.method public OO0o〇〇〇〇0(Landroid/content/pm/ActivityInfo;)V
    .locals 4

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 4
    .line 5
    const-string v0, "User Operation: OnShareAppItemClick  activityInfo = null"

    .line 6
    .line 7
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8oOOo:Landroid/content/pm/ActivityInfo;

    .line 12
    .line 13
    iget-object v0, p1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 14
    .line 15
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/share/ShareHelper;->o8o〇〇0O(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 19
    .line 20
    new-instance v1, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v2, "User Operation: OnShareAppItemClick  activityInfo  packageName = "

    .line 26
    .line 27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    iget-object v2, p1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 31
    .line 32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    const-string v2, ",name = "

    .line 36
    .line 37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    iget-object v2, p1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 41
    .line 42
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 53
    .line 54
    new-instance v1, Ljava/lang/StringBuilder;

    .line 55
    .line 56
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    .line 58
    .line 59
    const-string v2, "print"

    .line 60
    .line 61
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 65
    .line 66
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 77
    .line 78
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 79
    .line 80
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O00()I

    .line 81
    .line 82
    .line 83
    move-result v1

    .line 84
    iget-object v2, p1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 85
    .line 86
    iget-object v3, p1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 87
    .line 88
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/utils/provider/SharedApps;->〇o〇(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 92
    .line 93
    const-string v1, " start onPrepareData"

    .line 94
    .line 95
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 99
    .line 100
    invoke-virtual {v0, p1, p0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇〇0o(Landroid/content/pm/ActivityInfo;Lcom/intsig/camscanner/share/listener/BaseShareListener;)V

    .line 101
    .line 102
    .line 103
    return-void
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public OOo88OOo(Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public Oo08(Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇O〇〇O8:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected Oooo8o0〇(IILandroid/content/Intent;)V
    .locals 3

    .line 1
    invoke-super {p0, p1, p2, p3}, Lcom/intsig/utils/activity/ActivityLifeCircleManager$LifeCircleListener;->Oooo8o0〇(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 5
    .line 6
    new-instance v1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v2, " onActivityResult resultCode = "

    .line 12
    .line 13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    const-string v2, ", resultCode = "

    .line 20
    .line 21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const/16 v0, 0x2761

    .line 35
    .line 36
    if-ne p1, v0, :cond_2

    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8oOOo:Landroid/content/pm/ActivityInfo;

    .line 39
    .line 40
    const/4 p2, 0x1

    .line 41
    if-eqz p1, :cond_0

    .line 42
    .line 43
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 44
    .line 45
    iget-object p1, p1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 46
    .line 47
    invoke-virtual {p3, p1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇00(Ljava/lang/String;)Z

    .line 48
    .line 49
    .line 50
    move-result p1

    .line 51
    if-eqz p1, :cond_0

    .line 52
    .line 53
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 54
    .line 55
    .line 56
    move-result p1

    .line 57
    if-eqz p1, :cond_0

    .line 58
    .line 59
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 60
    .line 61
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇oo〇()Z

    .line 62
    .line 63
    .line 64
    move-result p1

    .line 65
    if-nez p1, :cond_0

    .line 66
    .line 67
    iput-boolean p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇080OO8〇0:Z

    .line 68
    .line 69
    return-void

    .line 70
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8oOOo:Landroid/content/pm/ActivityInfo;

    .line 71
    .line 72
    if-eqz p1, :cond_1

    .line 73
    .line 74
    const-string p3, "com.tencent.wework"

    .line 75
    .line 76
    iget-object p1, p1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 77
    .line 78
    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 79
    .line 80
    .line 81
    move-result p1

    .line 82
    if-eqz p1, :cond_1

    .line 83
    .line 84
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 85
    .line 86
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇oo〇()Z

    .line 87
    .line 88
    .line 89
    move-result p1

    .line 90
    if-nez p1, :cond_1

    .line 91
    .line 92
    iput-boolean p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇080OO8〇0:Z

    .line 93
    .line 94
    return-void

    .line 95
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->o08O()V

    .line 96
    .line 97
    .line 98
    goto/16 :goto_1

    .line 99
    .line 100
    :cond_2
    const/16 v0, 0x2762

    .line 101
    .line 102
    if-ne p1, v0, :cond_4

    .line 103
    .line 104
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 105
    .line 106
    if-eqz p1, :cond_b

    .line 107
    .line 108
    instance-of p2, p1, Lcom/intsig/camscanner/share/type/ShareOcrText;

    .line 109
    .line 110
    if-nez p2, :cond_3

    .line 111
    .line 112
    instance-of p2, p1, Lcom/intsig/camscanner/share/type/ShareWord;

    .line 113
    .line 114
    if-nez p2, :cond_3

    .line 115
    .line 116
    instance-of p1, p1, Lcom/intsig/camscanner/share/type/ShareBatchOcr;

    .line 117
    .line 118
    if-eqz p1, :cond_b

    .line 119
    .line 120
    :cond_3
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 121
    .line 122
    const-string p2, "back from setting language"

    .line 123
    .line 124
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O8OO08o()V

    .line 128
    .line 129
    .line 130
    goto/16 :goto_1

    .line 131
    .line 132
    :cond_4
    const/16 v0, 0x2763

    .line 133
    .line 134
    const/4 v1, -0x1

    .line 135
    if-ne p1, v0, :cond_6

    .line 136
    .line 137
    if-ne p2, v1, :cond_6

    .line 138
    .line 139
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 140
    .line 141
    if-eqz p1, :cond_b

    .line 142
    .line 143
    instance-of p1, p1, Lcom/intsig/camscanner/share/type/ShareSecureLink;

    .line 144
    .line 145
    if-eqz p1, :cond_b

    .line 146
    .line 147
    const-wide/16 p1, 0x0

    .line 148
    .line 149
    if-eqz p3, :cond_5

    .line 150
    .line 151
    const-string v0, "password"

    .line 152
    .line 153
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 154
    .line 155
    .line 156
    move-result-object v0

    .line 157
    const-string v1, "deadline_time"

    .line 158
    .line 159
    invoke-virtual {p3, v1, p1, p2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 160
    .line 161
    .line 162
    move-result-wide p1

    .line 163
    goto :goto_0

    .line 164
    :cond_5
    const-string v0, ""

    .line 165
    .line 166
    :goto_0
    sget-object p3, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 167
    .line 168
    new-instance v1, Ljava/lang/StringBuilder;

    .line 169
    .line 170
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 171
    .line 172
    .line 173
    const-string v2, "back from secure activity password="

    .line 174
    .line 175
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    .line 180
    .line 181
    const-string v2, ",deadLineTime = "

    .line 182
    .line 183
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    .line 185
    .line 186
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 187
    .line 188
    .line 189
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 190
    .line 191
    .line 192
    move-result-object v1

    .line 193
    invoke-static {p3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    .line 195
    .line 196
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 197
    .line 198
    check-cast p3, Lcom/intsig/camscanner/share/type/ShareSecureLink;

    .line 199
    .line 200
    invoke-virtual {p3, v0, p1, p2}, Lcom/intsig/camscanner/share/type/ShareSecureLink;->〇〇o8(Ljava/lang/String;J)V

    .line 201
    .line 202
    .line 203
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O8OO08o()V

    .line 204
    .line 205
    .line 206
    goto/16 :goto_1

    .line 207
    .line 208
    :cond_6
    const/16 p3, 0x2764

    .line 209
    .line 210
    const/16 v0, 0x2768

    .line 211
    .line 212
    if-eq p1, p3, :cond_7

    .line 213
    .line 214
    if-ne p1, v0, :cond_9

    .line 215
    .line 216
    :cond_7
    if-ne p2, v1, :cond_9

    .line 217
    .line 218
    sget-object p2, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 219
    .line 220
    const-string p3, "back from login"

    .line 221
    .line 222
    invoke-static {p2, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    .line 224
    .line 225
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 226
    .line 227
    if-eqz p2, :cond_b

    .line 228
    .line 229
    if-ne p1, v0, :cond_8

    .line 230
    .line 231
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 232
    .line 233
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 234
    .line 235
    .line 236
    move-result p1

    .line 237
    if-eqz p1, :cond_8

    .line 238
    .line 239
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 240
    .line 241
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o800o8O()Lcom/intsig/camscanner/share/ShareLinkLogger;

    .line 242
    .line 243
    .line 244
    move-result-object p1

    .line 245
    if-eqz p1, :cond_8

    .line 246
    .line 247
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 248
    .line 249
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o800o8O()Lcom/intsig/camscanner/share/ShareLinkLogger;

    .line 250
    .line 251
    .line 252
    move-result-object p1

    .line 253
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/ShareLinkLogger;->〇〇808〇()V

    .line 254
    .line 255
    .line 256
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 257
    .line 258
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 259
    .line 260
    .line 261
    goto :goto_1

    .line 262
    :cond_9
    const/16 p3, 0x2766

    .line 263
    .line 264
    if-ne p1, p3, :cond_a

    .line 265
    .line 266
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 267
    .line 268
    const-string p2, "buy point, when use word"

    .line 269
    .line 270
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    .line 272
    .line 273
    goto :goto_1

    .line 274
    :cond_a
    const/16 p3, 0x2767

    .line 275
    .line 276
    if-ne p1, p3, :cond_b

    .line 277
    .line 278
    if-nez p2, :cond_b

    .line 279
    .line 280
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->ooo0〇〇O:Landroidx/fragment/app/DialogFragment;

    .line 281
    .line 282
    if-eqz p1, :cond_b

    .line 283
    .line 284
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 285
    .line 286
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 287
    .line 288
    .line 289
    move-result-object p1

    .line 290
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->ooo0〇〇O:Landroidx/fragment/app/DialogFragment;

    .line 291
    .line 292
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 293
    .line 294
    .line 295
    move-result-object p2

    .line 296
    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 297
    .line 298
    .line 299
    move-result-object p2

    .line 300
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->ooo0〇〇O:Landroidx/fragment/app/DialogFragment;

    .line 301
    .line 302
    invoke-virtual {p3}, Landroidx/fragment/app/Fragment;->isAdded()Z

    .line 303
    .line 304
    .line 305
    move-result p3

    .line 306
    if-nez p3, :cond_b

    .line 307
    .line 308
    invoke-virtual {p1, p2}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    .line 309
    .line 310
    .line 311
    move-result-object p3

    .line 312
    if-nez p3, :cond_b

    .line 313
    .line 314
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    .line 315
    .line 316
    .line 317
    move-result-object p1

    .line 318
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->ooo0〇〇O:Landroidx/fragment/app/DialogFragment;

    .line 319
    .line 320
    invoke-virtual {p1, p3, p2}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 321
    .line 322
    .line 323
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 324
    .line 325
    .line 326
    :cond_b
    :goto_1
    return-void
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method public Oo〇O8o〇8()Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O〇Oooo〇〇(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/intsig/camscanner/DocumentActivity;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const-string p1, "cs_list"

    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 8
    .line 9
    goto/16 :goto_2

    .line 10
    .line 11
    :cond_0
    instance-of v0, p1, Lcom/intsig/camscanner/ImagePageViewActivity;

    .line 12
    .line 13
    if-nez v0, :cond_b

    .line 14
    .line 15
    instance-of v0, p1, Lcom/intsig/camscanner/pagedetail/PageDetailActivity;

    .line 16
    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_1
    instance-of v0, p1, Lcom/intsig/camscanner/ScanDoneActivity;

    .line 21
    .line 22
    if-nez v0, :cond_a

    .line 23
    .line 24
    instance-of v0, p1, Lcom/intsig/camscanner/scandone/ScanDoneNewActivity;

    .line 25
    .line 26
    if-eqz v0, :cond_2

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_2
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/WordPreviewActivity;

    .line 30
    .line 31
    if-eqz v0, :cond_3

    .line 32
    .line 33
    const-string p1, "cs_word_preview"

    .line 34
    .line 35
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 36
    .line 37
    const-string p1, "word"

    .line 38
    .line 39
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->O〇08oOOO0:Ljava/lang/String;

    .line 40
    .line 41
    goto :goto_2

    .line 42
    :cond_3
    instance-of v0, p1, Lcom/intsig/camscanner/imagestitch/LongImageStitchActivity;

    .line 43
    .line 44
    if-eqz v0, :cond_4

    .line 45
    .line 46
    const-string p1, "cs_long_pic_preview"

    .line 47
    .line 48
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 49
    .line 50
    goto :goto_2

    .line 51
    :cond_4
    instance-of v0, p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 52
    .line 53
    if-eqz v0, :cond_7

    .line 54
    .line 55
    check-cast p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 56
    .line 57
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->〇oO88o()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    if-nez p1, :cond_5

    .line 62
    .line 63
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 64
    .line 65
    return-object p1

    .line 66
    :cond_5
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇oO08〇o0()Z

    .line 67
    .line 68
    .line 69
    move-result p1

    .line 70
    if-eqz p1, :cond_6

    .line 71
    .line 72
    const-string p1, "cs_home"

    .line 73
    .line 74
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 75
    .line 76
    goto :goto_2

    .line 77
    :cond_6
    const-string p1, "cs_main"

    .line 78
    .line 79
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 80
    .line 81
    goto :goto_2

    .line 82
    :cond_7
    instance-of v0, p1, Lcom/intsig/camscanner/image_restore/ImageRestoreResultActivity;

    .line 83
    .line 84
    if-eqz v0, :cond_8

    .line 85
    .line 86
    const-string p1, "image_restore"

    .line 87
    .line 88
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 89
    .line 90
    goto :goto_2

    .line 91
    :cond_8
    instance-of v0, p1, Lcom/intsig/camscanner/pdf/PdfPreviewActivity;

    .line 92
    .line 93
    if-nez v0, :cond_9

    .line 94
    .line 95
    instance-of p1, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingActivity;

    .line 96
    .line 97
    if-eqz p1, :cond_c

    .line 98
    .line 99
    :cond_9
    const-string p1, "cs_pdf_preview"

    .line 100
    .line 101
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 102
    .line 103
    goto :goto_2

    .line 104
    :cond_a
    :goto_0
    const-string p1, "cs_scan_done"

    .line 105
    .line 106
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 107
    .line 108
    goto :goto_2

    .line 109
    :cond_b
    :goto_1
    const-string p1, "cs_detail"

    .line 110
    .line 111
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 112
    .line 113
    :cond_c
    :goto_2
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8〇OO:Ljava/lang/String;

    .line 114
    .line 115
    return-object p1
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public O〇oO〇oo8o(Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public o08〇〇0O(Lcom/intsig/camscanner/share/ShareHelper$ShareType;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "shareType = "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public o88O8()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇〇08O:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o8〇(Ljava/util/ArrayList;Lcom/intsig/camscanner/share/ShareHelper$ShareType;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/share/type/BaseShare;",
            ">;",
            "Lcom/intsig/camscanner/share/ShareHelper$ShareType;",
            ")V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o0:Lcom/intsig/camscanner/share/listener/ShareUiInterface;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    const/4 v5, 0x0

    .line 6
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 7
    .line 8
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 9
    .line 10
    invoke-virtual {v2, v1, v3}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇〇808〇(Landroid/app/Activity;Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v6

    .line 14
    const/4 v7, 0x0

    .line 15
    move-object v2, p1

    .line 16
    move-object v3, p0

    .line 17
    move-object v4, p2

    .line 18
    invoke-interface/range {v0 .. v7}, Lcom/intsig/camscanner/share/listener/ShareUiInterface;->o〇0(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/listener/ShareTypeClickListener;Lcom/intsig/camscanner/share/ShareHelper$ShareType;ZLjava/lang/String;Ljava/util/ArrayList;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public oO80(Lcom/intsig/camscanner/share/type/BaseShare;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/share/type/BaseShare;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onLinkFailure: "

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "\u94fe\u63a5\u83b7\u53d6\u5931\u8d25"

    .line 9
    .line 10
    invoke-static {v0}, Lcom/intsig/camscanner/share/LogReceiver;->〇o〇(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->〇o8oO()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->ooo0〇O88O(Lcom/intsig/camscanner/share/type/BaseShare;)Lcom/intsig/camscanner/share/type/SharePdf;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    if-eqz p1, :cond_0

    .line 24
    .line 25
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 26
    .line 27
    .line 28
    :cond_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected onDestroy()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/utils/activity/ActivityLifeCircleManager$LifeCircleListener;->onDestroy()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08〇o0O:Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/ShareHelper;->〇000〇〇08()Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08〇o0O:Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->ooOO(Lcom/intsig/camscanner/tsapp/sync/OnSyncDocUploadListener;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇〇o〇:Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;

    .line 18
    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/ShareHelper;->〇000〇〇08()Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇〇o〇:Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Oo〇O(Lcom/intsig/camscanner/tsapp/sync/SyncThread$OnSyncStopListener;)V

    .line 28
    .line 29
    .line 30
    :cond_1
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public onFragmentResult(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "onFragmentResult requestKey: "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v2, ", result: "

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    const-string v0, "share_channel_select_result_key"

    .line 32
    .line 33
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    if-eqz p1, :cond_0

    .line 38
    .line 39
    const-string p1, "data_key_channel"

    .line 40
    .line 41
    invoke-virtual {p2, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    check-cast p1, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 46
    .line 47
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->O88O:Lcom/intsig/camscanner/share/channel/item/ShareChannelListener;

    .line 48
    .line 49
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->O8ooOoo〇(Lcom/intsig/camscanner/share/channel/item/ShareChannelListener;)V

    .line 50
    .line 51
    .line 52
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->O8o08O8O:Lcom/intsig/utils/activity/ActivityLifeCircleManager;

    .line 53
    .line 54
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 55
    .line 56
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 57
    .line 58
    invoke-virtual {p1, p2, v0, v1}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->o〇〇0〇(Lcom/intsig/utils/activity/ActivityLifeCircleManager;Lcom/intsig/camscanner/share/ShareDataPresenter;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 59
    .line 60
    .line 61
    :cond_0
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public o〇0()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onShareAppClose"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "no_select_app"

    .line 9
    .line 10
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/share/ShareHelper;->oO8008O(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇000〇〇08()Lcom/intsig/camscanner/tsapp/sync/SyncThread;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oo〇(Landroid/content/Context;)Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/share/type/BaseShare;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_4

    .line 2
    .line 3
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    goto :goto_1

    .line 10
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    const/4 v1, 0x0

    .line 19
    if-eqz v0, :cond_2

    .line 20
    .line 21
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    check-cast v0, Lcom/intsig/camscanner/share/type/BaseShare;

    .line 26
    .line 27
    instance-of v2, v0, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 28
    .line 29
    if-eqz v2, :cond_1

    .line 30
    .line 31
    check-cast v0, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_2
    move-object v0, v1

    .line 35
    :goto_0
    if-nez v0, :cond_3

    .line 36
    .line 37
    return-void

    .line 38
    :cond_3
    const/4 p1, 0x1

    .line 39
    invoke-virtual {v0, v1, p0, p1}, Lcom/intsig/camscanner/share/type/SharePdf;->o〇0o〇〇(Landroid/content/pm/ActivityInfo;Lcom/intsig/camscanner/share/listener/BaseShareListener;Z)V

    .line 40
    .line 41
    .line 42
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 47
    .line 48
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 49
    .line 50
    iget-object v3, p0, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 51
    .line 52
    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/intsig/camscanner/share/ShareDataPresenter;->O8ooOoo〇(Landroid/app/Activity;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareHelper$ShareType;Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;)V

    .line 53
    .line 54
    .line 55
    :cond_4
    :goto_1
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇80〇808〇O(Landroid/content/Intent;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "data is ready to share and go to the app!"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇0O:Lcom/intsig/callback/Callback;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-interface {v0, p1}, Lcom/intsig/callback/Callback;->call(Ljava/lang/Object;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->o8oOOo:Landroid/content/pm/ActivityInfo;

    .line 16
    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 20
    .line 21
    invoke-static {v0}, Lcom/intsig/camscanner/app/IntentUtil;->〇8(Ljava/lang/String;)Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-nez v0, :cond_2

    .line 26
    .line 27
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-nez v0, :cond_2

    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O0〇oo()V

    .line 34
    .line 35
    .line 36
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 37
    .line 38
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o0O0(Landroid/content/Intent;)Z

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    if-nez v0, :cond_3

    .line 43
    .line 44
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OOO8o〇〇(Landroid/content/Intent;)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->O8o08O8O:Lcom/intsig/utils/activity/ActivityLifeCircleManager;

    .line 50
    .line 51
    const/16 v2, 0x2761

    .line 52
    .line 53
    invoke-virtual {v0, v1, p1, v2}, Lcom/intsig/camscanner/share/ShareDataPresenter;->OOO〇O0(Lcom/intsig/utils/activity/ActivityLifeCircleManager;Landroid/content/Intent;I)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_3
    if-eqz p1, :cond_4

    .line 58
    .line 59
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    if-eqz v0, :cond_4

    .line 64
    .line 65
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    const-string v0, "savetogallery"

    .line 74
    .line 75
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 76
    .line 77
    .line 78
    move-result p1

    .line 79
    if-eqz p1, :cond_4

    .line 80
    .line 81
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->o〇00O:Lcom/intsig/camscanner/share/listener/ShareBackListener;

    .line 82
    .line 83
    if-eqz p1, :cond_4

    .line 84
    .line 85
    invoke-interface {p1}, Lcom/intsig/camscanner/share/listener/ShareBackListener;->〇080()V

    .line 86
    .line 87
    .line 88
    :cond_4
    const/4 p1, 0x1

    .line 89
    iput-boolean p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇080OO8〇0:Z

    .line 90
    .line 91
    :goto_0
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public 〇8O0O808〇(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOO〇〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇8o8o〇()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected 〇O〇()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/utils/activity/ActivityLifeCircleManager$LifeCircleListener;->〇O〇()V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇080OO8〇0:Z

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇oo〇()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    iput-boolean v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇080OO8〇0:Z

    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->o08O()V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇o00〇〇Oo(Lcom/intsig/camscanner/share/type/BaseImagePdf$HandleType;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper;->Ooo08:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "User Operation: onCompressSelect = "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    sget-object v0, Lcom/intsig/camscanner/share/type/BaseImagePdf$HandleType;->Original:Lcom/intsig/camscanner/share/type/BaseImagePdf$HandleType;

    .line 28
    .line 29
    const-string v1, "CSChoiceSize"

    .line 30
    .line 31
    if-ne p1, v0, :cond_0

    .line 32
    .line 33
    const-string v0, "choice_large"

    .line 34
    .line 35
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/share/type/BaseImagePdf$HandleType;->Medium:Lcom/intsig/camscanner/share/type/BaseImagePdf$HandleType;

    .line 40
    .line 41
    if-ne p1, v0, :cond_1

    .line 42
    .line 43
    const-string v0, "choice_medium"

    .line 44
    .line 45
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_1
    const-string v0, "choice_small"

    .line 50
    .line 51
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 55
    .line 56
    instance-of v1, v0, Lcom/intsig/camscanner/share/type/BaseImagePdf;

    .line 57
    .line 58
    if-eqz v1, :cond_2

    .line 59
    .line 60
    check-cast v0, Lcom/intsig/camscanner/share/type/BaseImagePdf;

    .line 61
    .line 62
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/type/BaseImagePdf;->O0o〇O0〇(Lcom/intsig/camscanner/share/type/BaseImagePdf$HandleType;)V

    .line 63
    .line 64
    .line 65
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/share/ShareHelper;->O8OO08o()V

    .line 66
    .line 67
    .line 68
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇o〇(Landroidx/fragment/app/DialogFragment;Ljava/lang/Boolean;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->ooo0〇〇O:Landroidx/fragment/app/DialogFragment;

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 4
    .line 5
    if-eqz p1, :cond_1

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->EMAIL_MYSELF:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 8
    .line 9
    if-eq p1, v0, :cond_0

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->EMAIL_OTHER:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 12
    .line 13
    if-ne p1, v0, :cond_1

    .line 14
    .line 15
    :cond_0
    new-instance p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 16
    .line 17
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_NO_INK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 18
    .line 19
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_EMAIL_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 20
    .line 21
    invoke-direct {p1, v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    new-instance p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 26
    .line 27
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_NO_INK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 28
    .line 29
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 30
    .line 31
    invoke-direct {p1, v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 32
    .line 33
    .line 34
    :goto_0
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 35
    .line 36
    .line 37
    move-result p2

    .line 38
    if-eqz p2, :cond_2

    .line 39
    .line 40
    sget-object p2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_ADS_REWARD_PRE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 41
    .line 42
    iput-object p2, p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 43
    .line 44
    :cond_2
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->O8o08O8O:Lcom/intsig/utils/activity/ActivityLifeCircleManager;

    .line 45
    .line 46
    invoke-virtual {p2}, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->Oo08()Landroidx/fragment/app/Fragment;

    .line 47
    .line 48
    .line 49
    move-result-object p2

    .line 50
    const-string v0, "No_Watermark"

    .line 51
    .line 52
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseExtraData;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/camscanner/tsapp/purchase/PurchaseExtraData;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    const/16 v1, 0x2767

    .line 57
    .line 58
    invoke-static {p2, p1, v1, v0}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇O888o0o(Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;ILcom/intsig/camscanner/tsapp/purchase/PurchaseExtraData;)V

    .line 59
    .line 60
    .line 61
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 62
    .line 63
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 66
    .line 67
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 68
    .line 69
    iget-object v2, p0, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 70
    .line 71
    invoke-virtual {p1, p2, v0, v1, v2}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇oOO8O8(Landroid/app/Activity;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareHelper$ShareType;Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;)V

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public 〇〇00O〇0o(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇00O0:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇〇888(Landroidx/fragment/app/DialogFragment;Lcom/intsig/camscanner/purchase/entity/Function;Ljava/lang/Boolean;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->ooo0〇〇O:Landroidx/fragment/app/DialogFragment;

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 4
    .line 5
    if-eqz p1, :cond_1

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->EMAIL_MYSELF:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 8
    .line 9
    if-eq p1, v0, :cond_0

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->EMAIL_OTHER:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 12
    .line 13
    if-ne p1, v0, :cond_1

    .line 14
    .line 15
    :cond_0
    new-instance p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 16
    .line 17
    sget-object p2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_NO_INK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 18
    .line 19
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_EMAIL_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 20
    .line 21
    invoke-direct {p1, p2, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    new-instance p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 26
    .line 27
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 28
    .line 29
    invoke-direct {p1, p2, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 30
    .line 31
    .line 32
    :goto_0
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 33
    .line 34
    .line 35
    move-result p2

    .line 36
    if-eqz p2, :cond_2

    .line 37
    .line 38
    sget-object p2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_ADS_REWARD_PRE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 39
    .line 40
    iput-object p2, p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 41
    .line 42
    :cond_2
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->O8o08O8O:Lcom/intsig/utils/activity/ActivityLifeCircleManager;

    .line 43
    .line 44
    invoke-virtual {p2}, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->Oo08()Landroidx/fragment/app/Fragment;

    .line 45
    .line 46
    .line 47
    move-result-object p2

    .line 48
    const-string p3, "No_Watermark"

    .line 49
    .line 50
    invoke-static {p3}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseExtraData;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/camscanner/tsapp/purchase/PurchaseExtraData;

    .line 51
    .line 52
    .line 53
    move-result-object p3

    .line 54
    const/16 v0, 0x2767

    .line 55
    .line 56
    invoke-static {p2, p1, v0, p3}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇O888o0o(Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;ILcom/intsig/camscanner/tsapp/purchase/PurchaseExtraData;)V

    .line 57
    .line 58
    .line 59
    iget-object p1, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 60
    .line 61
    iget-object p2, p0, Lcom/intsig/camscanner/share/ShareHelper;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 62
    .line 63
    iget-object p3, p0, Lcom/intsig/camscanner/share/ShareHelper;->〇OOo8〇0:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/share/ShareHelper;->oOo〇8o008:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 66
    .line 67
    iget-object v1, p0, Lcom/intsig/camscanner/share/ShareHelper;->O0O:Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 68
    .line 69
    invoke-virtual {p1, p2, p3, v0, v1}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇oOO8O8(Landroid/app/Activity;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareHelper$ShareType;Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;)V

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

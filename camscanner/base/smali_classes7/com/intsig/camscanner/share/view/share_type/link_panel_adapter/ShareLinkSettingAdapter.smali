.class public Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;
.super Lcom/alibaba/android/vlayout/DelegateAdapter$Adapter;
.source "ShareLinkSettingAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/alibaba/android/vlayout/DelegateAdapter$Adapter<",
        "Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field public static final O8o08O8O:[I


# instance fields
.field private final OO:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private final o0:Landroid/content/Context;

.field private o〇00O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/share/bean/SecureLinkTimeItem;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇08O〇00〇o:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private 〇OOo8〇0:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [I

    .line 3
    .line 4
    fill-array-data v0, :array_0

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->O8o08O8O:[I

    .line 8
    .line 9
    return-void

    .line 10
    nop

    .line 11
    :array_0
    .array-data 4
        0x7
        0x1e
    .end array-data
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/alibaba/android/vlayout/DelegateAdapter$Adapter;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->〇OOo8〇0:Z

    .line 6
    .line 7
    new-instance v0, Looo008/O8;

    .line 8
    .line 9
    invoke-direct {v0}, Looo008/O8;-><init>()V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->OO:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 13
    .line 14
    new-instance v0, Looo008/Oo08;

    .line 15
    .line 16
    invoke-direct {v0, p0}, Looo008/Oo08;-><init>(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;)V

    .line 17
    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->〇08O〇00〇o:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    .line 20
    .line 21
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->o0:Landroid/content/Context;

    .line 22
    .line 23
    return-void
    .line 24
.end method

.method private O8ooOoo〇(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;)V
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0O〇OOo()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->o800o8O()Ljava/util/ArrayList;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    if-eqz v2, :cond_1

    .line 18
    .line 19
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    check-cast v2, Lcom/intsig/camscanner/share/bean/SecureLinkTimeItem;

    .line 24
    .line 25
    iget-object v3, p1, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;->o0:Landroid/widget/RadioGroup;

    .line 26
    .line 27
    invoke-virtual {v2}, Lcom/intsig/camscanner/share/bean/SecureLinkTimeItem;->O8()I

    .line 28
    .line 29
    .line 30
    move-result v4

    .line 31
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    check-cast v3, Landroid/widget/RadioButton;

    .line 36
    .line 37
    invoke-virtual {v2}, Lcom/intsig/camscanner/share/bean/SecureLinkTimeItem;->〇o00〇〇Oo()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v2}, Lcom/intsig/camscanner/share/bean/SecureLinkTimeItem;->〇o〇()I

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    if-ne v0, v2, :cond_0

    .line 49
    .line 50
    const/4 v2, 0x1

    .line 51
    goto :goto_1

    .line 52
    :cond_0
    const/4 v2, 0x0

    .line 53
    :goto_1
    invoke-virtual {v3, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 54
    .line 55
    .line 56
    invoke-direct {p0, v3, v2}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->〇00(Landroid/widget/RadioButton;Z)V

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_1
    iget-object p1, p1, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;->o0:Landroid/widget/RadioGroup;

    .line 61
    .line 62
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->〇08O〇00〇o:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    .line 63
    .line 64
    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 65
    .line 66
    .line 67
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static synthetic OoO8(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->〇O888o0o(Landroid/widget/CompoundButton;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private o800o8O()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/share/bean/SecureLinkTimeItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    const/4 v0, 0x2

    .line 7
    new-array v0, v0, [I

    .line 8
    .line 9
    fill-array-data v0, :array_0

    .line 10
    .line 11
    .line 12
    new-instance v1, Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 15
    .line 16
    .line 17
    iput-object v1, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->o0:Landroid/content/Context;

    .line 20
    .line 21
    const v2, 0x7f1308d5

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    const/4 v2, 0x0

    .line 29
    :goto_0
    sget-object v3, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->O8o08O8O:[I

    .line 30
    .line 31
    array-length v4, v3

    .line 32
    if-ge v2, v4, :cond_1

    .line 33
    .line 34
    iget-object v4, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 35
    .line 36
    new-instance v5, Lcom/intsig/camscanner/share/bean/SecureLinkTimeItem;

    .line 37
    .line 38
    aget v3, v3, v2

    .line 39
    .line 40
    aget v6, v0, v2

    .line 41
    .line 42
    invoke-direct {v5, v3, v1, v6}, Lcom/intsig/camscanner/share/bean/SecureLinkTimeItem;-><init>(ILjava/lang/String;I)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    add-int/lit8 v2, v2, 0x1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 52
    .line 53
    return-object v0

    .line 54
    nop

    .line 55
    :array_0
    .array-data 4
        0x7f0a0ef1
        0x7f0a0ee7
    .end array-data
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private synthetic oo88o8O(Landroid/widget/RadioGroup;I)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->o800o8O()Ljava/util/ArrayList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_2

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    check-cast v1, Lcom/intsig/camscanner/share/bean/SecureLinkTimeItem;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/bean/SecureLinkTimeItem;->O8()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    check-cast v2, Landroid/widget/RadioButton;

    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/bean/SecureLinkTimeItem;->O8()I

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    if-ne p2, v3, :cond_1

    .line 36
    .line 37
    const/4 v3, 0x1

    .line 38
    goto :goto_1

    .line 39
    :cond_1
    const/4 v3, 0x0

    .line 40
    :goto_1
    invoke-direct {p0, v2, v3}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->〇00(Landroid/widget/RadioButton;Z)V

    .line 41
    .line 42
    .line 43
    if-eqz v3, :cond_0

    .line 44
    .line 45
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/bean/SecureLinkTimeItem;->〇o〇()I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇oOoOO〇(I)V

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_2
    return-void
.end method

.method private 〇00(Landroid/widget/RadioButton;Z)V
    .locals 0

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    const p2, -0xe64364

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 7
    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/high16 p2, -0x67000000

    .line 11
    .line 12
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 13
    .line 14
    .line 15
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic 〇0〇O0088o(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;Landroid/widget/RadioGroup;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->oo88o8O(Landroid/widget/RadioGroup;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private static synthetic 〇O888o0o(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->Oo〇o〇Oo(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public O〇8O8〇008(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->〇OOo8〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getItemCount()I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getItemViewType(I)I
    .locals 0

    .line 1
    const/4 p1, 0x1

    .line 2
    return p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    check-cast p1, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->〇oo〇(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->o〇O8〇〇o(Landroid/view/ViewGroup;I)Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public o〇O8〇〇o(Landroid/view/ViewGroup;I)Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 6
    .line 7
    .line 8
    move-result-object p2

    .line 9
    const v0, 0x7f0d06ca

    .line 10
    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    new-instance p2, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;

    .line 18
    .line 19
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;-><init>(Landroid/view/View;)V

    .line 20
    .line 21
    .line 22
    return-object p2
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇oo〇(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;I)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    instance-of p2, p1, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;

    .line 2
    .line 3
    if-nez p2, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->O8ooOoo〇(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;)V

    .line 7
    .line 8
    .line 9
    iget-boolean p2, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->〇OOo8〇0:Z

    .line 10
    .line 11
    if-nez p2, :cond_1

    .line 12
    .line 13
    iget-object p1, p1, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;->o〇00O:Landroid/view/View;

    .line 14
    .line 15
    const/16 p2, 0x8

    .line 16
    .line 17
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_1
    iget-object p2, p1, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;->o〇00O:Landroid/view/View;

    .line 22
    .line 23
    const/4 v0, 0x0

    .line 24
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 25
    .line 26
    .line 27
    iget-object p2, p1, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;->〇08O〇00〇o:Landroid/widget/Switch;

    .line 28
    .line 29
    const/4 v0, 0x0

    .line 30
    invoke-virtual {p2, v0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 31
    .line 32
    .line 33
    iget-object p2, p1, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;->〇08O〇00〇o:Landroid/widget/Switch;

    .line 34
    .line 35
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo〇〇oO()Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    invoke-virtual {p2, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 40
    .line 41
    .line 42
    iget-object p1, p1, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter$ShareLinkSettingViewHolder;->〇08O〇00〇o:Landroid/widget/Switch;

    .line 43
    .line 44
    iget-object p2, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->OO:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 45
    .line 46
    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 47
    .line 48
    .line 49
    :goto_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇〇8O0〇8()Lcom/alibaba/android/vlayout/LayoutHelper;
    .locals 1

    .line 1
    new-instance v0, Lcom/alibaba/android/vlayout/layout/LinearLayoutHelper;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/alibaba/android/vlayout/layout/LinearLayoutHelper;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

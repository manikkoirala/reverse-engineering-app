.class public Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;
.super Lcom/intsig/app/BaseDialogFragment;
.source "ShareLinkSettingDialog.java"

# interfaces
.implements Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel$ShareTypeCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog$Factory;,
        Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog$DismissListener;
    }
.end annotation


# instance fields
.field private O8o08O8O:Lcom/intsig/behavior/BottomSheetBehavior2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/behavior/BottomSheetBehavior2<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private final oOo〇8o008:Lcom/intsig/behavior/BottomSheetBehavior2$BottomSheetCallback;

.field private final o〇00O:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

.field private 〇080OO8〇0:Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog$DismissListener;

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/share/listener/ShareTypeClickListener;

.field private 〇0O:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/app/BaseDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->DEFAULT:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->o〇00O:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->〇0O:Z

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog$1;

    .line 12
    .line 13
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog$1;-><init>(Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->oOo〇8o008:Lcom/intsig/behavior/BottomSheetBehavior2$BottomSheetCallback;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private o00〇88〇08(Landroid/view/View;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog$Factory;->〇o00〇〇Oo()Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog$Factory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-boolean v1, p0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->〇0O:Z

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog$Factory;->〇080(Z)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/share/listener/ShareTypeClickListener;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->OO0o〇〇(Lcom/intsig/camscanner/share/listener/ShareTypeClickListener;)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->OO0o〇〇〇〇0(Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->o〇00O:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->OoO8(Lcom/intsig/camscanner/share/ShareHelper$ShareType;)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->〇〇808〇(Landroid/view/View;)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    const-string v0, "ShareLinkSettingDialog"

    .line 36
    .line 37
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->〇0〇O0088o(Ljava/lang/String;)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->〇O〇(Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel$ShareTypeCallback;)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->Oo08()V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private o880(Landroid/view/View;)V
    .locals 1

    .line 1
    const v0, 0x7f0a0b8a

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    check-cast p1, Landroid/widget/LinearLayout;

    .line 9
    .line 10
    invoke-static {p1}, Lcom/intsig/behavior/BottomSheetBehavior2;->〇o〇(Landroid/view/View;)Lcom/intsig/behavior/BottomSheetBehavior2;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->O8o08O8O:Lcom/intsig/behavior/BottomSheetBehavior2;

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->oOo〇8o008:Lcom/intsig/behavior/BottomSheetBehavior2$BottomSheetCallback;

    .line 17
    .line 18
    invoke-virtual {p1, v0}, Lcom/intsig/behavior/BottomSheetBehavior2;->O8(Lcom/intsig/behavior/BottomSheetBehavior2$BottomSheetCallback;)V

    .line 19
    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->O8o08O8O:Lcom/intsig/behavior/BottomSheetBehavior2;

    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    invoke-virtual {p1, v0}, Lcom/intsig/behavior/BottomSheetBehavior2;->setHideable(Z)V

    .line 25
    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->O8o08O8O:Lcom/intsig/behavior/BottomSheetBehavior2;

    .line 28
    .line 29
    const/4 v0, 0x0

    .line 30
    invoke-virtual {p1, v0}, Lcom/intsig/behavior/BottomSheetBehavior2;->setPeekHeight(I)V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->O8o08O8O:Lcom/intsig/behavior/BottomSheetBehavior2;

    .line 34
    .line 35
    const/4 v0, 0x3

    .line 36
    invoke-virtual {p1, v0}, Lcom/intsig/behavior/BottomSheetBehavior2;->setState(I)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public O0〇0(Lcom/intsig/camscanner/share/listener/ShareTypeClickListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/share/listener/ShareTypeClickListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public Oo〇O()V
    .locals 2

    .line 1
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismissAllowingStateLoss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    goto :goto_0

    .line 5
    :catch_0
    move-exception v0

    .line 6
    const-string v1, "ShareLinkSettingDialog"

    .line 7
    .line 8
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 9
    .line 10
    .line 11
    :goto_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected init(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    const/4 p1, 0x0

    .line 2
    invoke-virtual {p0, p1}, Landroidx/fragment/app/DialogFragment;->setShowsDialog(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public o8o〇〇0O()V
    .locals 2

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    goto :goto_0

    .line 5
    :catch_0
    move-exception v0

    .line 6
    const-string v1, "ShareLinkSettingDialog"

    .line 7
    .line 8
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 9
    .line 10
    .line 11
    :goto_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    new-instance p1, Landroid/app/Dialog;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const v1, 0x7f140006

    .line 8
    .line 9
    .line 10
    invoke-direct {p1, v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 11
    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    const v2, 0x7f0d0734

    .line 26
    .line 27
    .line 28
    const/4 v3, 0x0

    .line 29
    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-virtual {p1, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 34
    .line 35
    .line 36
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->o00〇88〇08(Landroid/view/View;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    .line 47
    .line 48
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    const/16 v3, 0x50

    .line 53
    .line 54
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 55
    .line 56
    const/4 v3, -0x1

    .line 57
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 58
    .line 59
    const/4 v3, 0x0

    .line 60
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 61
    .line 62
    invoke-virtual {v0, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 63
    .line 64
    .line 65
    :cond_0
    const-string v0, "CSShareLinkSet"

    .line 66
    .line 67
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->o880(Landroid/view/View;)V

    .line 71
    .line 72
    .line 73
    return-object p1
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 5

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/app/BaseDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0O〇OOo()I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    const/4 v0, 0x0

    .line 9
    const/4 v1, 0x0

    .line 10
    :goto_0
    sget-object v2, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkSettingAdapter;->O8o08O8O:[I

    .line 11
    .line 12
    array-length v3, v2

    .line 13
    if-ge v1, v3, :cond_1

    .line 14
    .line 15
    aget v2, v2, v1

    .line 16
    .line 17
    if-ne v2, p1, :cond_0

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    const/4 v1, 0x0

    .line 24
    :goto_1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo〇〇oO()Z

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    const/4 v2, 0x2

    .line 29
    new-array v2, v2, [Landroid/util/Pair;

    .line 30
    .line 31
    new-instance v3, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    const-string v1, ""

    .line 40
    .line 41
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    const-string v4, "validity"

    .line 49
    .line 50
    invoke-static {v4, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 51
    .line 52
    .line 53
    move-result-object v3

    .line 54
    aput-object v3, v2, v0

    .line 55
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    .line 57
    .line 58
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    const-string v0, "extracted_code_status"

    .line 72
    .line 73
    invoke-static {v0, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    const/4 v0, 0x1

    .line 78
    aput-object p1, v2, v0

    .line 79
    .line 80
    const-string p1, "CSShareLinkSet"

    .line 81
    .line 82
    const-string v0, "code_status"

    .line 83
    .line 84
    invoke-static {p1, v0, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 85
    .line 86
    .line 87
    iget-object p1, p0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->〇080OO8〇0:Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog$DismissListener;

    .line 88
    .line 89
    if-eqz p1, :cond_2

    .line 90
    .line 91
    invoke-interface {p1}, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog$DismissListener;->onDismiss()V

    .line 92
    .line 93
    .line 94
    :cond_2
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public 〇8〇OOoooo(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->〇0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇〇o0〇8(Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog$DismissListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog;->〇080OO8〇0:Lcom/intsig/camscanner/share/view/ShareLinkSettingDialog$DismissListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public final Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareWorkflowAdapter$WorkflowViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "ShareWorkflowAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareWorkflowAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WorkflowViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final o0:Landroidx/fragment/app/FragmentActivity;

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/databinding/ViewItemShareWorkflowBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;Landroidx/fragment/app/FragmentActivity;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 7
    .line 8
    .line 9
    iput-object p2, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareWorkflowAdapter$WorkflowViewHolder;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 10
    .line 11
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ViewItemShareWorkflowBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ViewItemShareWorkflowBinding;

    .line 12
    .line 13
    .line 14
    move-result-object p2

    .line 15
    const-string v0, "bind(view)"

    .line 16
    .line 17
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    iput-object p2, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareWorkflowAdapter$WorkflowViewHolder;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/ViewItemShareWorkflowBinding;

    .line 21
    .line 22
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ViewItemShareWorkflowBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 23
    .line 24
    new-instance v0, Looo008/Oooo8o0〇;

    .line 25
    .line 26
    invoke-direct {v0, p1, p0}, Looo008/Oooo8o0〇;-><init>(Landroid/view/View;Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareWorkflowAdapter$WorkflowViewHolder;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static final O〇8O8〇008(Landroid/view/View;Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareWorkflowAdapter$WorkflowViewHolder;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p2, "$view"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "this$0"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    invoke-static {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 16
    .line 17
    .line 18
    move-result p2

    .line 19
    if-nez p2, :cond_1

    .line 20
    .line 21
    iget-object p1, p1, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareWorkflowAdapter$WorkflowViewHolder;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 22
    .line 23
    const/4 p2, 0x0

    .line 24
    if-eqz p1, :cond_0

    .line 25
    .line 26
    invoke-static {p1, p2}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->〇o00〇〇Oo(Landroid/content/Context;Lcom/intsig/tsapp/account/model/LoginMainArgs;)Landroid/content/Intent;

    .line 27
    .line 28
    .line 29
    move-result-object p2

    .line 30
    new-instance v0, Lcom/intsig/result/GetActivityResult;

    .line 31
    .line 32
    invoke-direct {v0, p1}, Lcom/intsig/result/GetActivityResult;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 33
    .line 34
    .line 35
    const v1, 0x85f021

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, p2, v1}, Lcom/intsig/result/GetActivityResult;->startActivityForResult(Landroid/content/Intent;I)Lcom/intsig/result/GetActivityResult;

    .line 39
    .line 40
    .line 41
    move-result-object p2

    .line 42
    new-instance v0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareWorkflowAdapter$WorkflowViewHolder$1$1$1;

    .line 43
    .line 44
    invoke-direct {v0, p1, p0}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareWorkflowAdapter$WorkflowViewHolder$1$1$1;-><init>(Landroidx/fragment/app/FragmentActivity;Landroid/content/Context;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {p2, v0}, Lcom/intsig/result/GetActivityResult;->〇8o8o〇(Lcom/intsig/result/OnForResultCallback;)Lcom/intsig/result/GetActivityResult;

    .line 48
    .line 49
    .line 50
    move-result-object p2

    .line 51
    :cond_0
    if-nez p2, :cond_3

    .line 52
    .line 53
    invoke-static {p0}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->oO80(Landroid/content/Context;)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_1
    const-string p1, "context"

    .line 58
    .line 59
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    const-string p1, "cs_share"

    .line 63
    .line 64
    invoke-static {p0, p1}, Lcom/intsig/camscanner/settings/workflow/WorkflowUtils;->〇080(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    instance-of p2, p0, Landroid/app/Activity;

    .line 69
    .line 70
    if-nez p2, :cond_2

    .line 71
    .line 72
    const/high16 p2, 0x10000000

    .line 73
    .line 74
    invoke-virtual {p1, p2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 75
    .line 76
    .line 77
    :cond_2
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 78
    .line 79
    .line 80
    :cond_3
    :goto_0
    const-string p0, "CSShare"

    .line 81
    .line 82
    const-string p1, "workflows"

    .line 83
    .line 84
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static synthetic 〇00(Landroid/view/View;Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareWorkflowAdapter$WorkflowViewHolder;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareWorkflowAdapter$WorkflowViewHolder;->O〇8O8〇008(Landroid/view/View;Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareWorkflowAdapter$WorkflowViewHolder;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public final O8ooOoo〇()Lcom/intsig/camscanner/databinding/ViewItemShareWorkflowBinding;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareWorkflowAdapter$WorkflowViewHolder;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/ViewItemShareWorkflowBinding;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

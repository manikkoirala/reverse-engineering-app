.class public final Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;
.super Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
.source "SharePadImageAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8o08O8O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 7
    .line 8
    .line 9
    new-instance p1, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder$rootView$2;

    .line 10
    .line 11
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder$rootView$2;-><init>(Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;)V

    .line 12
    .line 13
    .line 14
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;->o0:Lkotlin/Lazy;

    .line 19
    .line 20
    new-instance p1, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder$imageIv$2;

    .line 21
    .line 22
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder$imageIv$2;-><init>(Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;)V

    .line 23
    .line 24
    .line 25
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;->〇OOo8〇0:Lkotlin/Lazy;

    .line 30
    .line 31
    new-instance p1, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder$frameIv$2;

    .line 32
    .line 33
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder$frameIv$2;-><init>(Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;)V

    .line 34
    .line 35
    .line 36
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;->OO:Lkotlin/Lazy;

    .line 41
    .line 42
    new-instance p1, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder$ivSelect$2;

    .line 43
    .line 44
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder$ivSelect$2;-><init>(Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;)V

    .line 45
    .line 46
    .line 47
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;->〇08O〇00〇o:Lkotlin/Lazy;

    .line 52
    .line 53
    new-instance p1, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder$ivWaterMark$2;

    .line 54
    .line 55
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder$ivWaterMark$2;-><init>(Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;)V

    .line 56
    .line 57
    .line 58
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;->o〇00O:Lkotlin/Lazy;

    .line 63
    .line 64
    new-instance p1, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder$ivCloseWaterMark$2;

    .line 65
    .line 66
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder$ivCloseWaterMark$2;-><init>(Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;)V

    .line 67
    .line 68
    .line 69
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;->O8o08O8O:Lkotlin/Lazy;

    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public final O8ooOoo〇()Landroid/widget/ImageView;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;->O8o08O8O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "<get-ivCloseWaterMark>(...)"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    check-cast v0, Landroid/widget/ImageView;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇8O8〇008()Landroid/widget/ImageView;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;->〇OOo8〇0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "<get-imageIv>(...)"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    check-cast v0, Landroid/widget/ImageView;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇〇0〇()Landroid/view/View;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;->o0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/view/View;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇00()Landroid/widget/ImageView;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;->OO:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "<get-frameIv>(...)"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    check-cast v0, Landroid/widget/ImageView;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0000OOO()Landroid/widget/ImageView;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;->o〇00O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "<get-ivWaterMark>(...)"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    check-cast v0, Landroid/widget/ImageView;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇oOO8O8()Landroid/widget/ImageView;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/SharePadImageAdapter$ViewHolder;->〇08O〇00〇o:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "<get-ivSelect>(...)"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    check-cast v0, Landroid/widget/ImageView;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

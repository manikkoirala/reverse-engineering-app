.class public abstract Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;
.super Ljava/lang/Object;
.source "AbsShareTypePanel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel$ShareTypeCallback;
    }
.end annotation


# instance fields
.field protected O0O:Landroid/widget/GridView;

.field protected O88O:Z

.field protected O8o08O8O:Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel$ShareTypeCallback;

.field protected OO:Landroidx/fragment/app/FragmentActivity;

.field protected OO〇00〇8oO:Landroid/widget/TextView;

.field protected o0:Ljava/lang/String;

.field protected o8o:Z

.field protected o8oOOo:Landroid/widget/RelativeLayout;

.field protected o8〇OO0〇0o:Landroid/widget/LinearLayout;

.field protected oOO〇〇:Z

.field protected oOo0:Landroid/widget/Button;

.field protected oOo〇8o008:Landroid/widget/TextView;

.field protected oo8ooo8O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field protected ooo0〇〇O:Landroid/widget/TextView;

.field protected o〇00O:Landroid/view/View;

.field protected 〇080OO8〇0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/share/type/BaseShare;",
            ">;"
        }
    .end annotation
.end field

.field protected 〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

.field protected 〇0O:Landroid/widget/RelativeLayout;

.field protected 〇8〇oO〇〇8o:Landroid/widget/TextView;

.field protected 〇OOo8〇0:Lcom/intsig/camscanner/share/listener/ShareTypeClickListener;

.field protected 〇O〇〇O8:Landroid/widget/TextView;

.field protected 〇o0O:Landroid/widget/TextView;

.field protected 〇〇08O:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->o8o:Z

    .line 6
    .line 7
    new-instance v0, Ljava/util/ArrayList;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->oo8ooo8O:Ljava/util/ArrayList;

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o〇0(Landroid/view/View;)V
    .locals 1

    .line 1
    const v0, 0x7f0a17b1

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    check-cast v0, Landroid/widget/TextView;

    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 11
    .line 12
    const v0, 0x7f0a11cc

    .line 13
    .line 14
    .line 15
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->〇0O:Landroid/widget/RelativeLayout;

    .line 22
    .line 23
    const v0, 0x7f0a11cd

    .line 24
    .line 25
    .line 26
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    check-cast v0, Landroid/widget/TextView;

    .line 31
    .line 32
    iput-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->oOo〇8o008:Landroid/widget/TextView;

    .line 33
    .line 34
    const v0, 0x7f0a0465

    .line 35
    .line 36
    .line 37
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    check-cast v0, Landroid/widget/Button;

    .line 42
    .line 43
    iput-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->oOo0:Landroid/widget/Button;

    .line 44
    .line 45
    const v0, 0x7f0a0cdb

    .line 46
    .line 47
    .line 48
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    check-cast v0, Landroid/widget/LinearLayout;

    .line 53
    .line 54
    iput-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->o8〇OO0〇0o:Landroid/widget/LinearLayout;

    .line 55
    .line 56
    const v0, 0x7f0a17b0

    .line 57
    .line 58
    .line 59
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    check-cast v0, Landroid/widget/TextView;

    .line 64
    .line 65
    iput-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->〇8〇oO〇〇8o:Landroid/widget/TextView;

    .line 66
    .line 67
    const v0, 0x7f0a17af

    .line 68
    .line 69
    .line 70
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    check-cast v0, Landroid/widget/TextView;

    .line 75
    .line 76
    iput-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->ooo0〇〇O:Landroid/widget/TextView;

    .line 77
    .line 78
    const v0, 0x7f0a17b2

    .line 79
    .line 80
    .line 81
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    check-cast p1, Landroid/widget/TextView;

    .line 86
    .line 87
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->〇〇08O:Landroid/widget/TextView;

    .line 88
    .line 89
    iget-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->o〇00O:Landroid/view/View;

    .line 90
    .line 91
    const v0, 0x7f0a10cf

    .line 92
    .line 93
    .line 94
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    check-cast p1, Landroid/widget/GridView;

    .line 99
    .line 100
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->O0O:Landroid/widget/GridView;

    .line 101
    .line 102
    iget-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->o〇00O:Landroid/view/View;

    .line 103
    .line 104
    const v0, 0x7f0a17ae

    .line 105
    .line 106
    .line 107
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 108
    .line 109
    .line 110
    move-result-object p1

    .line 111
    check-cast p1, Landroid/widget/RelativeLayout;

    .line 112
    .line 113
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->o8oOOo:Landroid/widget/RelativeLayout;

    .line 114
    .line 115
    iget-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->o〇00O:Landroid/view/View;

    .line 116
    .line 117
    const v0, 0x7f0a17ad

    .line 118
    .line 119
    .line 120
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 121
    .line 122
    .line 123
    move-result-object p1

    .line 124
    check-cast p1, Landroid/widget/TextView;

    .line 125
    .line 126
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->〇o0O:Landroid/widget/TextView;

    .line 127
    .line 128
    iget-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->o〇00O:Landroid/view/View;

    .line 129
    .line 130
    const v0, 0x7f0a17ac

    .line 131
    .line 132
    .line 133
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 134
    .line 135
    .line 136
    move-result-object p1

    .line 137
    check-cast p1, Landroid/widget/TextView;

    .line 138
    .line 139
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->〇O〇〇O8:Landroid/widget/TextView;

    .line 140
    .line 141
    return-void
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method


# virtual methods
.method public OO0o〇〇(Lcom/intsig/camscanner/share/listener/ShareTypeClickListener;)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->〇OOo8〇0:Lcom/intsig/camscanner/share/listener/ShareTypeClickListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public OO0o〇〇〇〇0(Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->OO:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public Oo08()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->o〇00O:Landroid/view/View;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->o〇0(Landroid/view/View;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->〇〇888()V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->〇80〇808〇O()V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->oO80()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OoO8(Lcom/intsig/camscanner/share/ShareHelper$ShareType;)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->〇08O〇00〇o:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public Oooo8o0〇(Z)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->o8o:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method abstract oO80()V
.end method

.method public 〇0〇O0088o(Ljava/lang/String;)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method 〇80〇808〇O()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8o8o〇(Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 11
    .line 12
    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->oo8ooo8O:Ljava/util/ArrayList;

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->oo8ooo8O:Ljava/util/ArrayList;

    .line 19
    .line 20
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 21
    .line 22
    .line 23
    :goto_1
    return-object p0
    .line 24
.end method

.method public 〇O00(Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/share/type/BaseShare;",
            ">;)",
            "Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇O8o08O(Z)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->oOO〇〇:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇O〇(Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel$ShareTypeCallback;)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->O8o08O8O:Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel$ShareTypeCallback;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇〇808〇(Landroid/view/View;)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->o〇00O:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method abstract 〇〇888()V
.end method

.method public 〇〇8O0〇8(Z)Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/share/view/share_type/AbsShareTypePanel;->O88O:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

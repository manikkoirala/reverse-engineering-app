.class public final Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;
.super Lcom/alibaba/android/vlayout/DelegateAdapter$Adapter;
.source "ShareLinkPdfCardAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;,
        Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareChannelAdapter;,
        Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/alibaba/android/vlayout/DelegateAdapter$Adapter<",
        "Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO〇00〇8oO:Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

.field private OO:Lcom/intsig/camscanner/share/type/BaseShare;

.field private final o0:Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo〇8o008:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Lcom/intsig/camscanner/share/type/BaseShare;

.field private 〇080OO8〇0:I

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/share/type/BaseShare;

.field private 〇0O:Z

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/share/listener/IOnShareChannelCardListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->OO〇00〇8oO:Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/listener/IOnShareChannelCardListener;)V
    .locals 1
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/share/listener/IOnShareChannelCardListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "listener"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/alibaba/android/vlayout/DelegateAdapter$Adapter;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/share/listener/IOnShareChannelCardListener;

    .line 17
    .line 18
    const/4 p1, -0x1

    .line 19
    iput p1, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇080OO8〇0:I

    .line 20
    .line 21
    sget-object p1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 22
    .line 23
    new-instance p2, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$mShareLinkChannelList$2;

    .line 24
    .line 25
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$mShareLinkChannelList$2;-><init>(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;)V

    .line 26
    .line 27
    .line 28
    invoke-static {p1, p2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 29
    .line 30
    .line 31
    move-result-object p2

    .line 32
    iput-object p2, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->oOo〇8o008:Lkotlin/Lazy;

    .line 33
    .line 34
    new-instance p2, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$mSharePdfChannelList$2;

    .line 35
    .line 36
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$mSharePdfChannelList$2;-><init>(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;)V

    .line 37
    .line 38
    .line 39
    invoke-static {p1, p2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->oOo0:Lkotlin/Lazy;

    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final O8ooOoo〇(Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)V
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "onSettingClick, channel: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "ShareLinkPdfCardAdapter"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->OO:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 24
    .line 25
    instance-of v2, v0, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 26
    .line 27
    if-eqz v2, :cond_0

    .line 28
    .line 29
    move-object v2, v0

    .line 30
    check-cast v2, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    const/4 v2, 0x0

    .line 34
    :goto_0
    if-nez v2, :cond_1

    .line 35
    .line 36
    new-instance p1, Ljava/lang/StringBuilder;

    .line 37
    .line 38
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .line 40
    .line 41
    const-string v2, "click setting, mSharePdf: "

    .line 42
    .line 43
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    return-void

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/share/listener/IOnShareChannelCardListener;

    .line 58
    .line 59
    sget-object v1, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->〇080:Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;

    .line 60
    .line 61
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->o800o8O(Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)Z

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    invoke-interface {v0, v2, p1, v1}, Lcom/intsig/camscanner/share/listener/IOnShareChannelCardListener;->〇o00〇〇Oo(Lcom/intsig/camscanner/share/type/SharePdf;Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;Z)V

    .line 66
    .line 67
    .line 68
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static final synthetic OoO8(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇00(Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final o800o8O(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;->O〇8O8〇008()Lcom/intsig/camscanner/databinding/VlayoutItemShareLinkPdfCardItemBinding;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/VlayoutItemShareLinkPdfCardItemBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 8
    .line 9
    const v1, 0x7f131a3a

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final oo88o8O()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->oOo0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/util/List;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final oo〇(Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->o〇00O:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/share/listener/IOnShareChannelCardListener;

    .line 6
    .line 7
    invoke-interface {v1, v0, p1}, Lcom/intsig/camscanner/share/listener/IOnShareChannelCardListener;->〇o〇(Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)V

    .line 8
    .line 9
    .line 10
    :cond_0
    const/4 p1, 0x0

    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->O8o08O8O:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final 〇00(Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)V
    .locals 10

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "onChannelClick, channel: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "ShareLinkPdfCardAdapter"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sget-object v0, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->〇080:Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;

    .line 24
    .line 25
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->OoO8(Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    const-string v2, "type"

    .line 30
    .line 31
    const-string v3, "scheme"

    .line 32
    .line 33
    const/4 v4, 0x0

    .line 34
    const/4 v5, 0x2

    .line 35
    const-string v6, "link_share"

    .line 36
    .line 37
    const-string v7, "CSShare"

    .line 38
    .line 39
    const/4 v8, 0x1

    .line 40
    if-eqz v1, :cond_0

    .line 41
    .line 42
    new-array v1, v5, [Landroid/util/Pair;

    .line 43
    .line 44
    const-string v5, "link_setting"

    .line 45
    .line 46
    invoke-static {v3, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    aput-object v3, v1, v4

    .line 51
    .line 52
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->〇80〇808〇O(Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v3

    .line 56
    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    aput-object v2, v1, v8

    .line 61
    .line 62
    invoke-static {v7, v6, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 63
    .line 64
    .line 65
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->O8ooOoo〇(Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)V

    .line 66
    .line 67
    .line 68
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->O8o08O8O:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 69
    .line 70
    invoke-virtual {v0, v8}, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->Oooo8o0〇(Z)V

    .line 71
    .line 72
    .line 73
    goto :goto_2

    .line 74
    :cond_0
    iget v1, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇080OO8〇0:I

    .line 75
    .line 76
    if-eqz v1, :cond_2

    .line 77
    .line 78
    instance-of v1, p1, Lcom/intsig/camscanner/share/channel/item/SendToPcShareChannel;

    .line 79
    .line 80
    if-nez v1, :cond_2

    .line 81
    .line 82
    instance-of v1, p1, Lcom/intsig/camscanner/share/channel/item/LinkShareChannel;

    .line 83
    .line 84
    if-eqz v1, :cond_1

    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_1
    const-string v1, "pdf"

    .line 88
    .line 89
    goto :goto_1

    .line 90
    :cond_2
    :goto_0
    const-string v1, "link"

    .line 91
    .line 92
    :goto_1
    const/4 v9, 0x3

    .line 93
    new-array v9, v9, [Landroid/util/Pair;

    .line 94
    .line 95
    invoke-static {v3, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 96
    .line 97
    .line 98
    move-result-object v1

    .line 99
    aput-object v1, v9, v4

    .line 100
    .line 101
    invoke-static {}, Lcom/intsig/tsapp/sync/AppConfigJsonGet;->〇o00〇〇Oo()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    iget v1, v1, Lcom/intsig/tsapp/sync/AppConfigJson;->share_page_style:I

    .line 106
    .line 107
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v1

    .line 111
    const-string v3, "style"

    .line 112
    .line 113
    invoke-static {v3, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 114
    .line 115
    .line 116
    move-result-object v1

    .line 117
    aput-object v1, v9, v8

    .line 118
    .line 119
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->〇80〇808〇O(Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    invoke-static {v2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    aput-object v0, v9, v5

    .line 128
    .line 129
    invoke-static {v7, v6, v9}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 130
    .line 131
    .line 132
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->oo〇(Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)V

    .line 133
    .line 134
    .line 135
    :goto_2
    return-void
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private final 〇00〇8()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->o〇0()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇080OO8〇0:I

    .line 6
    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    if-eq v0, v1, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->OO:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->o〇00O:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇08O〇00〇o:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->o〇00O:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 21
    .line 22
    :goto_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static final synthetic 〇0〇O0088o(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;)Landroidx/fragment/app/FragmentActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final 〇O888o0o()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->oOo〇8o008:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/util/List;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇oo〇(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->Oo08()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    const/4 v2, 0x1

    .line 19
    new-array v2, v2, [Ljava/lang/Object;

    .line 20
    .line 21
    const-string v3, "7"

    .line 22
    .line 23
    aput-object v3, v2, v1

    .line 24
    .line 25
    const v3, 0x7f131a32

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const-string v2, "activity.getString(R.str\u2026651_share_link_date, \"7\")"

    .line 33
    .line 34
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;->O〇8O8〇008()Lcom/intsig/camscanner/databinding/VlayoutItemShareLinkPdfCardItemBinding;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/VlayoutItemShareLinkPdfCardItemBinding;->OO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 42
    .line 43
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;->O〇8O8〇008()Lcom/intsig/camscanner/databinding/VlayoutItemShareLinkPdfCardItemBinding;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/VlayoutItemShareLinkPdfCardItemBinding;->OO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 51
    .line 52
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    .line 54
    .line 55
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareChannelAdapter;

    .line 56
    .line 57
    iget-object v2, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 58
    .line 59
    new-instance v3, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$initHolder$1;

    .line 60
    .line 61
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$initHolder$1;-><init>(Ljava/lang/Object;)V

    .line 62
    .line 63
    .line 64
    invoke-direct {v0, v2, v3}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareChannelAdapter;-><init>(Landroidx/fragment/app/FragmentActivity;Lkotlin/jvm/functions/Function1;)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;->O8ooOoo〇(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareChannelAdapter;)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;->O〇8O8〇008()Lcom/intsig/camscanner/databinding/VlayoutItemShareLinkPdfCardItemBinding;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/VlayoutItemShareLinkPdfCardItemBinding;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 75
    .line 76
    new-instance v2, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 77
    .line 78
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 79
    .line 80
    .line 81
    move-result-object v3

    .line 82
    invoke-direct {v2, v3, v1, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;->〇00()Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareChannelAdapter;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 93
    .line 94
    .line 95
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method


# virtual methods
.method public final O8〇o()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇00〇8()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->O8o08O8O:Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->oo〇(Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OOO〇O0(Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/type/BaseShare;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->OO:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇08O〇00〇o:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public O〇8O8〇008(Landroid/view/ViewGroup;I)Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string p2, "parent"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p2, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;

    .line 7
    .line 8
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    const v1, 0x7f0d0796

    .line 17
    .line 18
    .line 19
    const/4 v2, 0x0

    .line 20
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    const-string v0, "from(parent.context)\n   \u2026card_item, parent, false)"

    .line 25
    .line 26
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;-><init>(Landroid/view/View;)V

    .line 30
    .line 31
    .line 32
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇oo〇(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;)V

    .line 33
    .line 34
    .line 35
    return-object p2
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public getItemCount()I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->o〇O8〇〇o(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->O〇8O8〇008(Landroid/view/ViewGroup;I)Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public o〇O8〇〇o(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;I)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p2, "holder"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇00〇8()V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->o800o8O(Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;)V

    .line 10
    .line 11
    .line 12
    const/4 p2, 0x1

    .line 13
    iget v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇080OO8〇0:I

    .line 14
    .line 15
    if-ne p2, v0, :cond_0

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->oo88o8O()Ljava/util/List;

    .line 18
    .line 19
    .line 20
    move-result-object p2

    .line 21
    goto :goto_0

    .line 22
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇O888o0o()Ljava/util/List;

    .line 23
    .line 24
    .line 25
    move-result-object p2

    .line 26
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareCardViewHolder;->〇00()Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareChannelAdapter;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    if-eqz p1, :cond_1

    .line 31
    .line 32
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter$ShareChannelAdapter;->〇O00(Ljava/util/List;)V

    .line 33
    .line 34
    .line 35
    :cond_1
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public final o〇〇0〇(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->OO:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇8oOO88(Ljava/util/ArrayList;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇08O〇00〇o:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇8oOO88(Ljava/util/ArrayList;)V

    .line 13
    .line 14
    .line 15
    :cond_1
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇0000OOO(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->OO:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/type/BaseShare;->O08000(Ljava/util/ArrayList;)V

    .line 7
    .line 8
    .line 9
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇08O〇00〇o:Lcom/intsig/camscanner/share/type/BaseShare;

    .line 10
    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    goto :goto_1

    .line 14
    :cond_1
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/type/BaseShare;->O08000(Ljava/util/ArrayList;)V

    .line 15
    .line 16
    .line 17
    :goto_1
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇oOO8O8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/share/view/share_type/link_panel_adapter/ShareLinkPdfCardAdapter;->〇0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇〇8O0〇8()Lcom/alibaba/android/vlayout/LayoutHelper;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/alibaba/android/vlayout/layout/LinearLayoutHelper;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/alibaba/android/vlayout/layout/LinearLayoutHelper;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

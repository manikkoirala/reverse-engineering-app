.class public Lcom/intsig/camscanner/settings/CacheManager;
.super Ljava/lang/Object;
.source "CacheManager.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static o8o:J = 0x240c8400L

.field private static oo8ooo8O:J = 0x0L

.field public static o〇oO:Z = false

.field public static 〇08〇o0O:I = 0x1f4


# instance fields
.field private O0O:Z

.field private O88O:Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;

.field private O8o08O8O:Landroid/widget/TextView;

.field private OO:Landroid/preference/PreferenceFragment;

.field private OO〇00〇8oO:Landroid/view/View;

.field private o0:Landroid/content/Context;

.field private o8oOOo:Z

.field private o8〇OO0〇0o:Landroid/view/View;

.field private oOO〇〇:Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;

.field private oOo0:Landroid/widget/TextView;

.field private oOo〇8o008:Landroid/widget/TextView;

.field private ooo0〇〇O:Z

.field private o〇00O:Landroid/view/View;

.field private 〇080OO8〇0:Landroid/widget/TextView;

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/view/CircleProgressBar;

.field private 〇0O:Landroid/widget/TextView;

.field private 〇8〇oO〇〇8o:Landroid/view/View;

.field private 〇OOo8〇0:Lcom/intsig/camscanner/settings/DocsSizeManager;

.field private 〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/camscanner/control/ProgressAnimHandler<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private 〇o0O:J

.field private 〇〇08O:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/settings/CacheManager;-><init>(Landroid/content/Context;Landroid/preference/PreferenceFragment;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/preference/PreferenceFragment;)V
    .locals 3

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->ooo0〇〇O:Z

    const/4 v1, 0x1

    .line 4
    iput-boolean v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇〇08O:Z

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->O0O:Z

    .line 6
    iput-boolean v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->o8oOOo:Z

    const-wide/16 v1, 0x0

    .line 7
    iput-wide v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇o0O:J

    .line 8
    new-instance v1, Lcom/intsig/camscanner/settings/CacheManager$2;

    invoke-direct {v1, p0}, Lcom/intsig/camscanner/settings/CacheManager$2;-><init>(Lcom/intsig/camscanner/settings/CacheManager;)V

    iput-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->O88O:Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;

    .line 9
    new-instance v1, Lcom/intsig/camscanner/settings/CacheManager$3;

    invoke-direct {v1, p0}, Lcom/intsig/camscanner/settings/CacheManager$3;-><init>(Lcom/intsig/camscanner/settings/CacheManager;)V

    iput-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->oOO〇〇:Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;

    .line 10
    invoke-static {}, Lcom/intsig/camscanner/settings/DocsSizeManager;->o〇0()Lcom/intsig/camscanner/settings/DocsSizeManager;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇OOo8〇0:Lcom/intsig/camscanner/settings/DocsSizeManager;

    .line 11
    iput-boolean v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->o8oOOo:Z

    .line 12
    iput-object p1, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 13
    iput-object p2, p0, Lcom/intsig/camscanner/settings/CacheManager;->OO:Landroid/preference/PreferenceFragment;

    .line 14
    new-instance p2, Lcom/intsig/camscanner/control/ProgressAnimHandler;

    invoke-direct {p2, p1}, Lcom/intsig/camscanner/control/ProgressAnimHandler;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    return-void
.end method

.method static bridge synthetic O8(Lcom/intsig/camscanner/settings/CacheManager;)Lcom/intsig/camscanner/view/CircleProgressBar;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇08O〇00〇o:Lcom/intsig/camscanner/view/CircleProgressBar;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private O8〇o()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/util/PermissionUtil;->〇O〇()[Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    new-instance v2, Lcom/intsig/camscanner/settings/CacheManager$9;

    .line 8
    .line 9
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/settings/CacheManager$9;-><init>(Lcom/intsig/camscanner/settings/CacheManager;)V

    .line 10
    .line 11
    .line 12
    invoke-static {v0, v1, v2}, Lcom/intsig/util/PermissionUtil;->Oo08(Landroid/content/Context;[Ljava/lang/String;Lcom/intsig/permission/PermissionCallback;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic OO0o〇〇(Lcom/intsig/camscanner/settings/CacheManager;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/settings/CacheManager;->o〇00O:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/settings/CacheManager;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇0O:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private synthetic OOO〇O0()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->O88O:Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇oOO8O8(Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o〇〇0〇()V

    .line 11
    .line 12
    .line 13
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 14
    .line 15
    .line 16
    move-result-wide v0

    .line 17
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇OOo8〇0:Lcom/intsig/camscanner/settings/DocsSizeManager;

    .line 18
    .line 19
    invoke-virtual {v2}, Lcom/intsig/camscanner/settings/DocsSizeManager;->〇o00〇〇Oo()V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/camscanner/settings/CacheManager;->o0ooO()V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/camscanner/settings/CacheManager;->o〇8()V

    .line 26
    .line 27
    .line 28
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 29
    .line 30
    invoke-virtual {v2}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o800o8O()V

    .line 31
    .line 32
    .line 33
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇OOo8〇0:Lcom/intsig/camscanner/settings/DocsSizeManager;

    .line 34
    .line 35
    invoke-virtual {v2}, Lcom/intsig/camscanner/settings/DocsSizeManager;->OO0o〇〇〇〇0()V

    .line 36
    .line 37
    .line 38
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 39
    .line 40
    .line 41
    move-result-wide v2

    .line 42
    sub-long/2addr v2, v0

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    const-string v1, "startSearchFile, costTime="

    .line 49
    .line 50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    const-string v1, "CacheManager"

    .line 61
    .line 62
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic Oo08(Lcom/intsig/camscanner/settings/CacheManager;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private Oo8Oo00oo()V
    .locals 3

    .line 1
    const-string v0, "CacheManager"

    .line 2
    .line 3
    const-string v1, "showSyncSettingTipsDialog"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 11
    .line 12
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 13
    .line 14
    .line 15
    const v1, 0x7f131d10

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const v1, 0x7f130357

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    new-instance v1, Lcom/intsig/camscanner/settings/CacheManager$6;

    .line 30
    .line 31
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/settings/CacheManager$6;-><init>(Lcom/intsig/camscanner/settings/CacheManager;)V

    .line 32
    .line 33
    .line 34
    const v2, 0x7f13057e

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/camscanner/settings/CacheManager$5;

    .line 42
    .line 43
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/settings/CacheManager$5;-><init>(Lcom/intsig/camscanner/settings/CacheManager;)V

    .line 44
    .line 45
    .line 46
    const v2, 0x7f131db3

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic OoO8(Lcom/intsig/camscanner/settings/CacheManager;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/CacheManager;->o〇0OOo〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic Oooo8o0〇(Lcom/intsig/camscanner/settings/CacheManager;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/settings/CacheManager;->o8〇OO0〇0o:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static o8(Landroid/content/Context;)V
    .locals 3

    .line 1
    sget-boolean v0, Lcom/intsig/camscanner/settings/CacheManager;->o〇oO:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const-string v0, "CacheManager"

    .line 6
    .line 7
    const-string v1, "showCachCleanSettingDialog"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 13
    .line 14
    invoke-direct {v0, p0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 15
    .line 16
    .line 17
    const v1, 0x7f131d10

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const v1, 0x7f130247

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    const/4 v1, 0x0

    .line 32
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->o〇0(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    new-instance v1, Lcom/intsig/camscanner/settings/CacheManager$8;

    .line 37
    .line 38
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/settings/CacheManager$8;-><init>(Landroid/content/Context;)V

    .line 39
    .line 40
    .line 41
    const v2, 0x7f13057e

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    new-instance v1, Lcom/intsig/camscanner/settings/CacheManager$7;

    .line 49
    .line 50
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/settings/CacheManager$7;-><init>(Landroid/content/Context;)V

    .line 51
    .line 52
    .line 53
    const p0, 0x7f13014b

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, p0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 57
    .line 58
    .line 59
    move-result-object p0

    .line 60
    invoke-virtual {p0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 61
    .line 62
    .line 63
    move-result-object p0

    .line 64
    invoke-virtual {p0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 65
    .line 66
    .line 67
    :cond_0
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static o800o8O(Landroid/content/Context;)V
    .locals 8

    .line 1
    const-string v0, "CacheManager"

    .line 2
    .line 3
    invoke-static {p0}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOO0o〇(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Image;->〇o〇:Landroid/net/Uri;

    .line 15
    .line 16
    const-string p0, "count(_id)"

    .line 17
    .line 18
    filled-new-array {p0}, [Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v4

    .line 22
    const-string v5, "belong_state >= -1 and sync_state = 0 and ( cache_state = 0 or ( sync_raw_jpg_state = 0 and raw_data IS NOT NULL  ))"

    .line 23
    .line 24
    const/4 v6, 0x0

    .line 25
    const/4 v7, 0x0

    .line 26
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 27
    .line 28
    .line 29
    move-result-object p0

    .line 30
    if-eqz p0, :cond_2

    .line 31
    .line 32
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    if-eqz v1, :cond_1

    .line 37
    .line 38
    const/4 v1, 0x0

    .line 39
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    new-instance v2, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    const-string v3, "cache image pageNumber ="

    .line 49
    .line 50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    sget v2, Lcom/intsig/camscanner/settings/CacheManager;->〇08〇o0O:I

    .line 64
    .line 65
    if-le v1, v2, :cond_1

    .line 66
    .line 67
    const/4 v1, 0x1

    .line 68
    sput-boolean v1, Lcom/intsig/camscanner/settings/CacheManager;->o〇oO:Z

    .line 69
    .line 70
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .line 72
    .line 73
    goto :goto_0

    .line 74
    :catch_0
    move-exception p0

    .line 75
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 76
    .line 77
    .line 78
    :cond_2
    :goto_0
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method static bridge synthetic oO80(Lcom/intsig/camscanner/settings/CacheManager;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/settings/CacheManager;->oOo〇8o008:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static oo88o8O(Landroid/content/Context;)V
    .locals 8

    .line 1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    if-eqz p0, :cond_1

    .line 10
    .line 11
    array-length v0, p0

    .line 12
    if-lez v0, :cond_1

    .line 13
    .line 14
    array-length v0, p0

    .line 15
    const/4 v1, 0x0

    .line 16
    :goto_0
    if-ge v1, v0, :cond_1

    .line 17
    .line 18
    aget-object v2, p0, v1

    .line 19
    .line 20
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    .line 21
    .line 22
    .line 23
    move-result v3

    .line 24
    if-nez v3, :cond_0

    .line 25
    .line 26
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    const-string v4, ".apk"

    .line 31
    .line 32
    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    if-eqz v3, :cond_0

    .line 37
    .line 38
    sget-wide v3, Lcom/intsig/camscanner/settings/CacheManager;->oo8ooo8O:J

    .line 39
    .line 40
    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    .line 41
    .line 42
    .line 43
    move-result-wide v5

    .line 44
    cmp-long v7, v3, v5

    .line 45
    .line 46
    if-lez v7, :cond_0

    .line 47
    .line 48
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    .line 50
    .line 51
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :catch_0
    move-exception p0

    .line 55
    const-string v0, "CacheManager"

    .line 56
    .line 57
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 58
    .line 59
    .line 60
    :cond_1
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method static bridge synthetic o〇0(Lcom/intsig/camscanner/settings/CacheManager;)Lcom/intsig/camscanner/settings/DocsSizeManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇OOo8〇0:Lcom/intsig/camscanner/settings/DocsSizeManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private o〇0OOo〇0()V
    .locals 1

    .line 1
    new-instance v0, Looooo0O/〇o〇;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Looooo0O/〇o〇;-><init>(Lcom/intsig/camscanner/settings/CacheManager;)V

    .line 4
    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o〇O8〇〇o()V
    .locals 19

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    const-string v0, "deletePdf"

    .line 4
    .line 5
    const-string v2, "CacheManager"

    .line 6
    .line 7
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    iget-boolean v0, v1, Lcom/intsig/camscanner/settings/CacheManager;->ooo0〇〇O:Z

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 16
    .line 17
    .line 18
    move-result-wide v3

    .line 19
    const-string v8, "_data IS NOT NULL and belong_state >= -1 and _id > 0 "

    .line 20
    .line 21
    iget-object v0, v1, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 22
    .line 23
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 24
    .line 25
    .line 26
    move-result-object v5

    .line 27
    sget-object v6, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 28
    .line 29
    const-string v0, "_id"

    .line 30
    .line 31
    const-string v11, "_data"

    .line 32
    .line 33
    filled-new-array {v0, v11}, [Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v7

    .line 37
    const/4 v9, 0x0

    .line 38
    const/4 v10, 0x0

    .line 39
    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    if-eqz v0, :cond_4

    .line 44
    .line 45
    new-instance v5, Ljava/util/ArrayList;

    .line 46
    .line 47
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .line 49
    .line 50
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 51
    .line 52
    .line 53
    move-result v6

    .line 54
    iget-object v7, v1, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 55
    .line 56
    invoke-virtual {v7}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->oo88o8O()I

    .line 57
    .line 58
    .line 59
    move-result v7

    .line 60
    const/4 v8, 0x0

    .line 61
    const/4 v9, 0x0

    .line 62
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 63
    .line 64
    .line 65
    move-result v10

    .line 66
    if-eqz v10, :cond_3

    .line 67
    .line 68
    iget-boolean v10, v1, Lcom/intsig/camscanner/settings/CacheManager;->ooo0〇〇O:Z

    .line 69
    .line 70
    if-eqz v10, :cond_1

    .line 71
    .line 72
    const-string v6, "deletePdf, mCancel = true"

    .line 73
    .line 74
    invoke-static {v2, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    goto :goto_1

    .line 78
    :cond_1
    const/4 v10, 0x1

    .line 79
    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v12

    .line 83
    invoke-static {v12}, Lcom/intsig/utils/FileUtil;->〇O888o0o(Ljava/lang/String;)J

    .line 84
    .line 85
    .line 86
    move-result-wide v16

    .line 87
    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v12

    .line 91
    invoke-static {v12}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 92
    .line 93
    .line 94
    move-result v12

    .line 95
    if-eqz v12, :cond_2

    .line 96
    .line 97
    iget-object v13, v1, Lcom/intsig/camscanner/settings/CacheManager;->〇OOo8〇0:Lcom/intsig/camscanner/settings/DocsSizeManager;

    .line 98
    .line 99
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    .line 100
    .line 101
    .line 102
    move-result-wide v14

    .line 103
    const/16 v18, 0x1

    .line 104
    .line 105
    invoke-virtual/range {v13 .. v18}, Lcom/intsig/camscanner/settings/DocsSizeManager;->〇8o8o〇(JJZ)V

    .line 106
    .line 107
    .line 108
    sget-object v12, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 109
    .line 110
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    .line 111
    .line 112
    .line 113
    move-result-wide v13

    .line 114
    invoke-static {v12, v13, v14}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 115
    .line 116
    .line 117
    move-result-object v12

    .line 118
    invoke-static {v12}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 119
    .line 120
    .line 121
    move-result-object v12

    .line 122
    const/4 v13, 0x0

    .line 123
    invoke-virtual {v12, v11, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 124
    .line 125
    .line 126
    invoke-virtual {v12}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 127
    .line 128
    .line 129
    move-result-object v12

    .line 130
    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    .line 132
    .line 133
    :cond_2
    sub-int v12, v6, v9

    .line 134
    .line 135
    int-to-float v12, v12

    .line 136
    const v13, 0x3dcccccd    # 0.1f

    .line 137
    .line 138
    .line 139
    mul-float v12, v12, v13

    .line 140
    .line 141
    int-to-float v13, v6

    .line 142
    div-float/2addr v12, v13

    .line 143
    int-to-float v13, v7

    .line 144
    mul-float v13, v13, v12

    .line 145
    .line 146
    float-to-int v12, v13

    .line 147
    iget-object v13, v1, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 148
    .line 149
    invoke-virtual {v13, v12}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->oo〇(I)V

    .line 150
    .line 151
    .line 152
    add-int/2addr v9, v10

    .line 153
    goto :goto_0

    .line 154
    :cond_3
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 155
    .line 156
    .line 157
    move-result-wide v6

    .line 158
    sub-long/2addr v6, v3

    .line 159
    new-instance v3, Ljava/lang/StringBuilder;

    .line 160
    .line 161
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 162
    .line 163
    .line 164
    const-string v4, "deletePdf,costTime="

    .line 165
    .line 166
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    .line 168
    .line 169
    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 170
    .line 171
    .line 172
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object v3

    .line 176
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    .line 178
    .line 179
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 180
    .line 181
    .line 182
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    .line 183
    .line 184
    .line 185
    move-result v0

    .line 186
    if-lez v0, :cond_4

    .line 187
    .line 188
    :try_start_0
    iget-object v0, v1, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 189
    .line 190
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 191
    .line 192
    .line 193
    move-result-object v0

    .line 194
    sget-object v3, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 195
    .line 196
    invoke-virtual {v0, v3, v5}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    .line 198
    .line 199
    goto :goto_2

    .line 200
    :catch_0
    move-exception v0

    .line 201
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 202
    .line 203
    .line 204
    :cond_4
    :goto_2
    return-void
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private synthetic o〇〇0〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->oOO〇〇:Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇oOO8O8(Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o〇〇0〇()V

    .line 11
    .line 12
    .line 13
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 14
    .line 15
    .line 16
    move-result-wide v0

    .line 17
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇OOo8〇0:Lcom/intsig/camscanner/settings/DocsSizeManager;

    .line 18
    .line 19
    invoke-virtual {v2}, Lcom/intsig/camscanner/settings/DocsSizeManager;->〇〇888()J

    .line 20
    .line 21
    .line 22
    move-result-wide v2

    .line 23
    iput-wide v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇o0O:J

    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/camscanner/settings/CacheManager;->〇00()V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/CacheManager;->o〇O8〇〇o()V

    .line 29
    .line 30
    .line 31
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 32
    .line 33
    .line 34
    move-result-wide v2

    .line 35
    sub-long/2addr v2, v0

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    .line 37
    .line 38
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .line 40
    .line 41
    const-string v1, "QuicklyDelete, costTime="

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    const-string v1, "CacheManager"

    .line 54
    .line 55
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 59
    .line 60
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o800o8O()V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇OOo8〇0:Lcom/intsig/camscanner/settings/DocsSizeManager;

    .line 64
    .line 65
    invoke-virtual {v0}, Lcom/intsig/camscanner/settings/DocsSizeManager;->OO0o〇〇〇〇0()V

    .line 66
    .line 67
    .line 68
    const/4 v0, 0x1

    .line 69
    iput-boolean v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇〇08O:Z

    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private synthetic 〇0000OOO(Landroid/content/Context;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇8〇oO〇〇8o:Landroid/view/View;

    .line 2
    .line 3
    const/16 v0, 0x8

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/settings/CacheManager;->O〇8O8〇008()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/settings/CacheManager;Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/CacheManager;->〇0000OOO(Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇0〇O0088o(Lcom/intsig/camscanner/settings/CacheManager;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/CacheManager;->〇〇〇0〇〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇80〇808〇O(Lcom/intsig/camscanner/settings/CacheManager;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/settings/CacheManager;->oOo0:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇8o8o〇(Lcom/intsig/camscanner/settings/CacheManager;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/settings/CacheManager;->O8o08O8O:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇O00(Lcom/intsig/camscanner/settings/CacheManager;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/settings/CacheManager;->o8oOOo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static 〇O888o0o(Landroid/content/Context;)V
    .locals 8

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    sget-wide v2, Lcom/intsig/camscanner/settings/CacheManager;->o8o:J

    .line 6
    .line 7
    sub-long v2, v0, v2

    .line 8
    .line 9
    sput-wide v2, Lcom/intsig/camscanner/settings/CacheManager;->oo8ooo8O:J

    .line 10
    .line 11
    const-wide/16 v4, 0x0

    .line 12
    .line 13
    cmp-long v6, v2, v4

    .line 14
    .line 15
    if-gez v6, :cond_0

    .line 16
    .line 17
    sput-wide v4, Lcom/intsig/camscanner/settings/CacheManager;->oo8ooo8O:J

    .line 18
    .line 19
    :cond_0
    sget-wide v2, Lcom/intsig/camscanner/settings/CacheManager;->oo8ooo8O:J

    .line 20
    .line 21
    invoke-static {p0}, Lcom/intsig/camscanner/util/PreferenceHelper;->o0oO(Landroid/content/Context;)J

    .line 22
    .line 23
    .line 24
    move-result-wide v4

    .line 25
    const-string v6, "CacheManager"

    .line 26
    .line 27
    cmp-long v7, v2, v4

    .line 28
    .line 29
    if-lez v7, :cond_3

    .line 30
    .line 31
    const-string v2, "over 7 days"

    .line 32
    .line 33
    invoke-static {v6, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    invoke-static {}, Lcom/intsig/utils/SDStorageUtil;->〇o〇()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    const-string v3, "CamScanner/.temp"

    .line 45
    .line 46
    if-nez v2, :cond_1

    .line 47
    .line 48
    new-instance v2, Ljava/io/File;

    .line 49
    .line 50
    invoke-static {}, Lcom/intsig/utils/SDStorageUtil;->〇o〇()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    invoke-direct {v2, v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    invoke-static {v2}, Lcom/intsig/camscanner/settings/CacheManager;->〇o(Ljava/io/File;)V

    .line 58
    .line 59
    .line 60
    :cond_1
    sget-object v2, Lcom/intsig/utils/SDStorageUtil;->〇080:Ljava/lang/String;

    .line 61
    .line 62
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    if-nez v2, :cond_2

    .line 67
    .line 68
    new-instance v2, Ljava/io/File;

    .line 69
    .line 70
    sget-object v4, Lcom/intsig/utils/SDStorageUtil;->〇080:Ljava/lang/String;

    .line 71
    .line 72
    invoke-direct {v2, v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    invoke-static {v2}, Lcom/intsig/camscanner/settings/CacheManager;->〇o(Ljava/io/File;)V

    .line 76
    .line 77
    .line 78
    :cond_2
    invoke-static {p0}, Lcom/intsig/camscanner/settings/CacheManager;->oo88o8O(Landroid/content/Context;)V

    .line 79
    .line 80
    .line 81
    invoke-static {p0, v0, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇Oo〇0O〇8(Landroid/content/Context;J)V

    .line 82
    .line 83
    .line 84
    invoke-static {p0}, Lcom/intsig/advertisement/cache/AdCacheManager;->〇080(Landroid/content/Context;)V

    .line 85
    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_3
    const-string p0, "no need to scan temp folder"

    .line 89
    .line 90
    invoke-static {v6, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    :goto_0
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method static bridge synthetic 〇O8o08O(Lcom/intsig/camscanner/settings/CacheManager;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇080OO8〇0:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇O〇(Lcom/intsig/camscanner/settings/CacheManager;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/settings/CacheManager;->O0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static 〇o(Ljava/io/File;)V
    .locals 13

    .line 1
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 9
    .line 10
    .line 11
    move-result-wide v0

    .line 12
    const-string v2, "scanAndCleanFiles start"

    .line 13
    .line 14
    const-string v3, "CacheManager"

    .line 15
    .line 16
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    new-instance v2, Ljava/util/LinkedList;

    .line 20
    .line 21
    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v2, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 25
    .line 26
    .line 27
    new-instance p0, Ljava/lang/StringBuilder;

    .line 28
    .line 29
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    .line 30
    .line 31
    .line 32
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    const-string v4, ".ad"

    .line 40
    .line 41
    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p0

    .line 48
    :cond_1
    :goto_0
    :try_start_0
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    .line 49
    .line 50
    .line 51
    move-result v4

    .line 52
    if-lez v4, :cond_7

    .line 53
    .line 54
    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 55
    .line 56
    .line 57
    move-result-object v4

    .line 58
    check-cast v4, Ljava/io/File;

    .line 59
    .line 60
    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    .line 61
    .line 62
    .line 63
    move-result-object v4

    .line 64
    if-eqz v4, :cond_1

    .line 65
    .line 66
    array-length v5, v4

    .line 67
    if-nez v5, :cond_2

    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_2
    array-length v5, v4

    .line 71
    const/4 v6, 0x0

    .line 72
    :goto_1
    if-ge v6, v5, :cond_1

    .line 73
    .line 74
    aget-object v7, v4, v6

    .line 75
    .line 76
    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    .line 77
    .line 78
    .line 79
    move-result v8

    .line 80
    if-eqz v8, :cond_5

    .line 81
    .line 82
    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    .line 83
    .line 84
    .line 85
    move-result-object v8

    .line 86
    if-eqz v8, :cond_4

    .line 87
    .line 88
    array-length v8, v8

    .line 89
    if-nez v8, :cond_3

    .line 90
    .line 91
    goto :goto_2

    .line 92
    :cond_3
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v8

    .line 96
    invoke-static {v8, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 97
    .line 98
    .line 99
    move-result v8

    .line 100
    if-nez v8, :cond_6

    .line 101
    .line 102
    invoke-virtual {v2, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 103
    .line 104
    .line 105
    goto :goto_3

    .line 106
    :cond_4
    :goto_2
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v8

    .line 110
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇o()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object v9

    .line 114
    invoke-static {v8, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 115
    .line 116
    .line 117
    move-result v8

    .line 118
    if-nez v8, :cond_6

    .line 119
    .line 120
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 121
    .line 122
    .line 123
    goto :goto_3

    .line 124
    :cond_5
    sget-wide v8, Lcom/intsig/camscanner/settings/CacheManager;->oo8ooo8O:J

    .line 125
    .line 126
    invoke-virtual {v7}, Ljava/io/File;->lastModified()J

    .line 127
    .line 128
    .line 129
    move-result-wide v10

    .line 130
    cmp-long v12, v8, v10

    .line 131
    .line 132
    if-lez v12, :cond_6

    .line 133
    .line 134
    invoke-virtual {v7}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    .line 136
    .line 137
    :cond_6
    :goto_3
    add-int/lit8 v6, v6, 0x1

    .line 138
    .line 139
    goto :goto_1

    .line 140
    :catch_0
    move-exception p0

    .line 141
    invoke-static {v3, p0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 142
    .line 143
    .line 144
    :cond_7
    new-instance p0, Ljava/lang/StringBuilder;

    .line 145
    .line 146
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    .line 147
    .line 148
    .line 149
    const-string v2, "scanAndCleanFiles end costTime="

    .line 150
    .line 151
    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    .line 153
    .line 154
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 155
    .line 156
    .line 157
    move-result-wide v4

    .line 158
    sub-long/2addr v4, v0

    .line 159
    invoke-virtual {p0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 160
    .line 161
    .line 162
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object p0

    .line 166
    invoke-static {v3, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    .line 168
    .line 169
    return-void
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/settings/CacheManager;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/CacheManager;->OOO〇O0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇oo〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O008oO0(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v0, "CacheManager"

    .line 10
    .line 11
    const-string v1, "go to sync setting"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/CacheManager;->Oo8Oo00oo()V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/settings/CacheManager;->O8ooOoo〇()V

    .line 21
    .line 22
    .line 23
    :goto_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static synthetic 〇o〇(Lcom/intsig/camscanner/settings/CacheManager;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/CacheManager;->o〇〇0〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇〇808〇(Lcom/intsig/camscanner/settings/CacheManager;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/settings/CacheManager;->OO〇00〇8oO:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/camscanner/settings/CacheManager;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇o0O:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/settings/CacheManager;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/CacheManager;->〇oo〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇〇〇0〇〇0()V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "startQuicklyDelete, mFinishDelete="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-boolean v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇〇08O:Z

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "CacheManager"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-boolean v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇〇08O:Z

    .line 26
    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    const/4 v0, 0x0

    .line 30
    iput-boolean v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇〇08O:Z

    .line 31
    .line 32
    new-instance v0, Looooo0O/O8;

    .line 33
    .line 34
    invoke-direct {v0, p0}, Looooo0O/O8;-><init>(Lcom/intsig/camscanner/settings/CacheManager;)V

    .line 35
    .line 36
    .line 37
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 38
    .line 39
    .line 40
    :cond_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method


# virtual methods
.method public O8ooOoo〇()V
    .locals 3

    .line 1
    const-string v0, "CacheManager"

    .line 2
    .line 3
    const-string v1, "go2DeepCacheClean"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Landroid/content/Intent;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 11
    .line 12
    const-class v2, Lcom/intsig/camscanner/settings/DeepCleanActivity;

    .line 13
    .line 14
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 15
    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->OO:Landroid/preference/PreferenceFragment;

    .line 18
    .line 19
    const/4 v2, 0x1

    .line 20
    if-nez v1, :cond_0

    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 23
    .line 24
    check-cast v1, Landroid/app/Activity;

    .line 25
    .line 26
    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    invoke-virtual {v1, v0, v2}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 31
    .line 32
    .line 33
    :goto_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public O〇8O8〇008()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->OO:Landroid/preference/PreferenceFragment;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 6
    .line 7
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 15
    .line 16
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8oo〇〇oO(Landroid/content/Context;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/CacheManager;->o〇0OOo〇0()V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_1
    const-string v0, "show cache clean tips"

    .line 27
    .line 28
    const-string v1, "CacheManager"

    .line 29
    .line 30
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 34
    .line 35
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 36
    .line 37
    invoke-direct {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 38
    .line 39
    .line 40
    const v2, 0x7f131d10

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    const v2, 0x7f13024e

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    const/4 v2, 0x0

    .line 55
    invoke-virtual {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->o〇0(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    new-instance v2, Lcom/intsig/camscanner/settings/CacheManager$4;

    .line 60
    .line 61
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/settings/CacheManager$4;-><init>(Lcom/intsig/camscanner/settings/CacheManager;)V

    .line 62
    .line 63
    .line 64
    const v3, 0x7f130019

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v3, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    :try_start_0
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    .line 77
    .line 78
    goto :goto_0

    .line 79
    :catch_0
    move-exception v0

    .line 80
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 81
    .line 82
    .line 83
    :goto_0
    return-void
    .line 84
    .line 85
    .line 86
.end method

.method public o0ooO()V
    .locals 15

    .line 1
    const-string v0, "searchALLPdf"

    .line 2
    .line 3
    const-string v1, "CacheManager"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 9
    .line 10
    .line 11
    move-result-wide v2

    .line 12
    const-string v0, "_id"

    .line 13
    .line 14
    const-string v4, "_data"

    .line 15
    .line 16
    filled-new-array {v0, v4}, [Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v7

    .line 20
    const-string v8, "_data IS NOT NULL and belong_state >= -1 and _id > 0 "

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 23
    .line 24
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 25
    .line 26
    .line 27
    move-result-object v5

    .line 28
    sget-object v6, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 29
    .line 30
    const/4 v9, 0x0

    .line 31
    const/4 v10, 0x0

    .line 32
    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 39
    .line 40
    .line 41
    move-result v4

    .line 42
    iget-object v5, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 43
    .line 44
    invoke-virtual {v5}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->oo88o8O()I

    .line 45
    .line 46
    .line 47
    move-result v5

    .line 48
    const/4 v6, 0x0

    .line 49
    const/4 v7, 0x0

    .line 50
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 51
    .line 52
    .line 53
    move-result v8

    .line 54
    if-eqz v8, :cond_1

    .line 55
    .line 56
    iget-boolean v8, p0, Lcom/intsig/camscanner/settings/CacheManager;->ooo0〇〇O:Z

    .line 57
    .line 58
    if-eqz v8, :cond_0

    .line 59
    .line 60
    const-string v4, "searchALLPdf, mCancel = true"

    .line 61
    .line 62
    invoke-static {v1, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_0
    const/4 v8, 0x1

    .line 67
    add-int/2addr v7, v8

    .line 68
    iget-object v9, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇OOo8〇0:Lcom/intsig/camscanner/settings/DocsSizeManager;

    .line 69
    .line 70
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    .line 71
    .line 72
    .line 73
    move-result-wide v10

    .line 74
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v8

    .line 78
    invoke-static {v8}, Lcom/intsig/utils/FileUtil;->〇O888o0o(Ljava/lang/String;)J

    .line 79
    .line 80
    .line 81
    move-result-wide v12

    .line 82
    const/4 v14, 0x1

    .line 83
    invoke-virtual/range {v9 .. v14}, Lcom/intsig/camscanner/settings/DocsSizeManager;->〇080(JJZ)V

    .line 84
    .line 85
    .line 86
    int-to-float v8, v5

    .line 87
    const v9, 0x3dcccccd    # 0.1f

    .line 88
    .line 89
    .line 90
    mul-float v8, v8, v9

    .line 91
    .line 92
    int-to-float v9, v7

    .line 93
    mul-float v8, v8, v9

    .line 94
    .line 95
    int-to-float v9, v4

    .line 96
    div-float/2addr v8, v9

    .line 97
    float-to-int v8, v8

    .line 98
    iget-object v9, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 99
    .line 100
    invoke-virtual {v9, v8}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->oo〇(I)V

    .line 101
    .line 102
    .line 103
    goto :goto_0

    .line 104
    :cond_1
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 105
    .line 106
    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    .line 108
    .line 109
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    .line 111
    .line 112
    const-string v4, "searchALLPdf, costTime="

    .line 113
    .line 114
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 118
    .line 119
    .line 120
    move-result-wide v4

    .line 121
    sub-long/2addr v4, v2

    .line 122
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v0

    .line 129
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    goto :goto_2

    .line 133
    :cond_2
    const-string v0, "cursor == null"

    .line 134
    .line 135
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    :goto_2
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 139
    .line 140
    const/16 v2, 0xa

    .line 141
    .line 142
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->oo〇(I)V

    .line 143
    .line 144
    .line 145
    const-string v0, "searchALLPdf end"

    .line 146
    .line 147
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    .line 149
    .line 150
    return-void
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const v0, 0x7f0a04c1

    .line 6
    .line 7
    .line 8
    const-string v1, "freeupmemory"

    .line 9
    .line 10
    const-string v2, "CSFreeupmemory"

    .line 11
    .line 12
    const-string v3, "CacheManager"

    .line 13
    .line 14
    if-ne p1, v0, :cond_0

    .line 15
    .line 16
    const-string p1, "click start quickly clean"

    .line 17
    .line 18
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/CacheManager;->O8〇o()V

    .line 22
    .line 23
    .line 24
    invoke-static {v2, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const v0, 0x7f0a0f84

    .line 29
    .line 30
    .line 31
    if-ne p1, v0, :cond_1

    .line 32
    .line 33
    invoke-static {v2, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    const-string p1, "click enter deep clean"

    .line 37
    .line 38
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 42
    .line 43
    invoke-static {}, Lcom/intsig/util/PermissionUtil;->〇O〇()[Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    new-instance v1, Lcom/intsig/camscanner/settings/CacheManager$1;

    .line 48
    .line 49
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/settings/CacheManager$1;-><init>(Lcom/intsig/camscanner/settings/CacheManager;)V

    .line 50
    .line 51
    .line 52
    invoke-static {p1, v0, v1}, Lcom/intsig/util/PermissionUtil;->Oo08(Landroid/content/Context;[Ljava/lang/String;Lcom/intsig/permission/PermissionCallback;)V

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_1
    const v0, 0x7f0a0d04

    .line 57
    .line 58
    .line 59
    if-ne p1, v0, :cond_2

    .line 60
    .line 61
    const-string p1, "click login"

    .line 62
    .line 63
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    new-instance p1, Looooo0O/〇o00〇〇Oo;

    .line 67
    .line 68
    invoke-direct {p1, p0}, Looooo0O/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/settings/CacheManager;)V

    .line 69
    .line 70
    .line 71
    sput-object p1, Lcom/intsig/tsapp/account/LoginMainActivity;->〇o0O:Lcom/intsig/tsapp/account/LoginMainActivity$OnLoginFinishListener;

    .line 72
    .line 73
    iget-object p1, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 74
    .line 75
    invoke-static {p1}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->oO80(Landroid/content/Context;)V

    .line 76
    .line 77
    .line 78
    :cond_2
    :goto_0
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public oo〇()V
    .locals 2

    .line 1
    const-string v0, "CacheManager"

    .line 2
    .line 3
    const-string v1, "onBackPressed"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇〇8O0〇8()V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇00()V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇OOo8〇0:Lcom/intsig/camscanner/settings/DocsSizeManager;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/settings/DocsSizeManager;->〇o00〇〇Oo()V

    .line 21
    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    iput-boolean v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->ooo0〇〇O:Z

    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public o〇8()V
    .locals 22

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const-string v1, "searchAllSyncImage"

    .line 4
    .line 5
    const-string v2, "CacheManager"

    .line 6
    .line 7
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 11
    .line 12
    .line 13
    move-result-wide v3

    .line 14
    iget-object v1, v0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 15
    .line 16
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 17
    .line 18
    .line 19
    move-result-object v5

    .line 20
    sget-object v6, Lcom/intsig/camscanner/provider/Documents$Image;->〇o〇:Landroid/net/Uri;

    .line 21
    .line 22
    const-string v1, "sync_raw_jpg_state"

    .line 23
    .line 24
    const-string v7, "document_id"

    .line 25
    .line 26
    const-string v8, "raw_data"

    .line 27
    .line 28
    const-string v9, "_data"

    .line 29
    .line 30
    const-string v10, "image_backup"

    .line 31
    .line 32
    filled-new-array {v8, v9, v10, v1, v7}, [Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v7

    .line 36
    const-string v8, "belong_state >= -1 and sync_state = 0 and ( cache_state = 0 or ( sync_raw_jpg_state = 0 and raw_data IS NOT NULL  ))"

    .line 37
    .line 38
    const/4 v9, 0x0

    .line 39
    const/4 v10, 0x0

    .line 40
    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    if-eqz v1, :cond_3

    .line 45
    .line 46
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    .line 47
    .line 48
    .line 49
    move-result v5

    .line 50
    iget-object v6, v0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 51
    .line 52
    invoke-virtual {v6}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->oo88o8O()I

    .line 53
    .line 54
    .line 55
    move-result v6

    .line 56
    int-to-float v6, v6

    .line 57
    const v7, 0x3dcccccd    # 0.1f

    .line 58
    .line 59
    .line 60
    mul-float v7, v7, v6

    .line 61
    .line 62
    const/4 v8, 0x0

    .line 63
    const/4 v9, 0x0

    .line 64
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 65
    .line 66
    .line 67
    move-result v10

    .line 68
    if-eqz v10, :cond_2

    .line 69
    .line 70
    iget-boolean v10, v0, Lcom/intsig/camscanner/settings/CacheManager;->ooo0〇〇O:Z

    .line 71
    .line 72
    if-eqz v10, :cond_0

    .line 73
    .line 74
    const-string v5, "searchAllSyncImage, mCancel = true"

    .line 75
    .line 76
    invoke-static {v2, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    goto :goto_1

    .line 80
    :cond_0
    const/4 v10, 0x3

    .line 81
    invoke-interface {v1, v10}, Landroid/database/Cursor;->getInt(I)I

    .line 82
    .line 83
    .line 84
    move-result v10

    .line 85
    const/4 v11, 0x4

    .line 86
    if-nez v10, :cond_1

    .line 87
    .line 88
    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v10

    .line 92
    invoke-static {v10}, Lcom/intsig/utils/FileUtil;->〇O888o0o(Ljava/lang/String;)J

    .line 93
    .line 94
    .line 95
    move-result-wide v15

    .line 96
    iget-object v12, v0, Lcom/intsig/camscanner/settings/CacheManager;->〇OOo8〇0:Lcom/intsig/camscanner/settings/DocsSizeManager;

    .line 97
    .line 98
    invoke-interface {v1, v11}, Landroid/database/Cursor;->getLong(I)J

    .line 99
    .line 100
    .line 101
    move-result-wide v13

    .line 102
    const/16 v17, 0x1

    .line 103
    .line 104
    invoke-virtual/range {v12 .. v17}, Lcom/intsig/camscanner/settings/DocsSizeManager;->〇080(JJZ)V

    .line 105
    .line 106
    .line 107
    :cond_1
    const/4 v10, 0x1

    .line 108
    invoke-interface {v1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object v12

    .line 112
    invoke-static {v12}, Lcom/intsig/utils/FileUtil;->〇O888o0o(Ljava/lang/String;)J

    .line 113
    .line 114
    .line 115
    move-result-wide v12

    .line 116
    const/4 v14, 0x2

    .line 117
    invoke-interface {v1, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object v14

    .line 121
    invoke-static {v14}, Lcom/intsig/utils/FileUtil;->〇O888o0o(Ljava/lang/String;)J

    .line 122
    .line 123
    .line 124
    move-result-wide v14

    .line 125
    add-long v19, v12, v14

    .line 126
    .line 127
    iget-object v12, v0, Lcom/intsig/camscanner/settings/CacheManager;->〇OOo8〇0:Lcom/intsig/camscanner/settings/DocsSizeManager;

    .line 128
    .line 129
    invoke-interface {v1, v11}, Landroid/database/Cursor;->getLong(I)J

    .line 130
    .line 131
    .line 132
    move-result-wide v17

    .line 133
    const/16 v21, 0x0

    .line 134
    .line 135
    move-object/from16 v16, v12

    .line 136
    .line 137
    invoke-virtual/range {v16 .. v21}, Lcom/intsig/camscanner/settings/DocsSizeManager;->〇080(JJZ)V

    .line 138
    .line 139
    .line 140
    add-int/2addr v9, v10

    .line 141
    const v10, 0x3f666666    # 0.9f

    .line 142
    .line 143
    .line 144
    mul-float v10, v10, v6

    .line 145
    .line 146
    int-to-float v11, v9

    .line 147
    mul-float v10, v10, v11

    .line 148
    .line 149
    int-to-float v11, v5

    .line 150
    div-float/2addr v10, v11

    .line 151
    add-float/2addr v10, v7

    .line 152
    float-to-int v10, v10

    .line 153
    iget-object v11, v0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 154
    .line 155
    invoke-virtual {v11, v10}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->oo〇(I)V

    .line 156
    .line 157
    .line 158
    goto :goto_0

    .line 159
    :cond_2
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 160
    .line 161
    .line 162
    goto :goto_2

    .line 163
    :cond_3
    const-string v1, "searchAllSyncImage cursor is null"

    .line 164
    .line 165
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    .line 167
    .line 168
    :goto_2
    iget-object v1, v0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 169
    .line 170
    invoke-virtual {v1}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o800o8O()V

    .line 171
    .line 172
    .line 173
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 174
    .line 175
    .line 176
    move-result-wide v5

    .line 177
    sub-long/2addr v5, v3

    .line 178
    new-instance v1, Ljava/lang/StringBuilder;

    .line 179
    .line 180
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 181
    .line 182
    .line 183
    const-string v3, "searchAllSyncImage, costTime="

    .line 184
    .line 185
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    .line 187
    .line 188
    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 189
    .line 190
    .line 191
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 192
    .line 193
    .line 194
    move-result-object v1

    .line 195
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    .line 197
    .line 198
    return-void
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public 〇00()V
    .locals 15

    .line 1
    const-string v0, "deleteSyncedRawJpg"

    .line 2
    .line 3
    const-string v1, "CacheManager"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->ooo0〇〇O:Z

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 14
    .line 15
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Image;->〇o〇:Landroid/net/Uri;

    .line 20
    .line 21
    const-string v0, "raw_data"

    .line 22
    .line 23
    const-string v4, "document_id"

    .line 24
    .line 25
    const-string v5, "_id"

    .line 26
    .line 27
    filled-new-array {v5, v0, v4}, [Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v4

    .line 31
    const-string v5, "belong_state >= -1 and sync_raw_jpg_state = 0 and raw_data IS NOT NULL "

    .line 32
    .line 33
    const/4 v6, 0x0

    .line 34
    const/4 v7, 0x0

    .line 35
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    if-eqz v0, :cond_5

    .line 40
    .line 41
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    iget-object v3, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 46
    .line 47
    invoke-virtual {v3}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->oo88o8O()I

    .line 48
    .line 49
    .line 50
    move-result v3

    .line 51
    new-instance v4, Ljava/util/ArrayList;

    .line 52
    .line 53
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .line 55
    .line 56
    const/4 v5, 0x0

    .line 57
    const/4 v6, 0x0

    .line 58
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 59
    .line 60
    .line 61
    move-result v7

    .line 62
    if-eqz v7, :cond_3

    .line 63
    .line 64
    iget-boolean v7, p0, Lcom/intsig/camscanner/settings/CacheManager;->ooo0〇〇O:Z

    .line 65
    .line 66
    if-eqz v7, :cond_1

    .line 67
    .line 68
    const-string v2, "deleteSyncedRawJpg, mCancel = true"

    .line 69
    .line 70
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    goto :goto_1

    .line 74
    :cond_1
    const/4 v7, 0x1

    .line 75
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v8

    .line 79
    invoke-static {v8}, Lcom/intsig/utils/FileUtil;->〇O888o0o(Ljava/lang/String;)J

    .line 80
    .line 81
    .line 82
    move-result-wide v12

    .line 83
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v8

    .line 87
    invoke-static {v8}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 88
    .line 89
    .line 90
    move-result v8

    .line 91
    if-eqz v8, :cond_2

    .line 92
    .line 93
    iget-object v9, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇OOo8〇0:Lcom/intsig/camscanner/settings/DocsSizeManager;

    .line 94
    .line 95
    const/4 v8, 0x2

    .line 96
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    .line 97
    .line 98
    .line 99
    move-result-wide v10

    .line 100
    const/4 v14, 0x1

    .line 101
    invoke-virtual/range {v9 .. v14}, Lcom/intsig/camscanner/settings/DocsSizeManager;->〇8o8o〇(JJZ)V

    .line 102
    .line 103
    .line 104
    sget-object v9, Lcom/intsig/camscanner/provider/Documents$Image;->Oo08:Landroid/net/Uri;

    .line 105
    .line 106
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    .line 107
    .line 108
    .line 109
    move-result-wide v10

    .line 110
    invoke-static {v9, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 111
    .line 112
    .line 113
    move-result-object v9

    .line 114
    invoke-static {v9}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 115
    .line 116
    .line 117
    move-result-object v9

    .line 118
    const-string v10, "sync_raw_jpg_state"

    .line 119
    .line 120
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 121
    .line 122
    .line 123
    move-result-object v8

    .line 124
    invoke-virtual {v9, v10, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v9}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 128
    .line 129
    .line 130
    move-result-object v8

    .line 131
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    .line 133
    .line 134
    :cond_2
    sub-int v8, v2, v6

    .line 135
    .line 136
    int-to-float v8, v8

    .line 137
    const v9, 0x3f666666    # 0.9f

    .line 138
    .line 139
    .line 140
    mul-float v8, v8, v9

    .line 141
    .line 142
    int-to-float v9, v2

    .line 143
    div-float/2addr v8, v9

    .line 144
    const v9, 0x3dcccccd    # 0.1f

    .line 145
    .line 146
    .line 147
    add-float/2addr v8, v9

    .line 148
    int-to-float v9, v3

    .line 149
    mul-float v9, v9, v8

    .line 150
    .line 151
    float-to-int v8, v9

    .line 152
    iget-object v9, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇O〇〇O8:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 153
    .line 154
    invoke-virtual {v9, v8}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->oo〇(I)V

    .line 155
    .line 156
    .line 157
    add-int/2addr v6, v7

    .line 158
    goto :goto_0

    .line 159
    :cond_3
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 160
    .line 161
    .line 162
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    .line 163
    .line 164
    .line 165
    move-result v0

    .line 166
    if-lez v0, :cond_4

    .line 167
    .line 168
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 169
    .line 170
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 171
    .line 172
    .line 173
    move-result-object v0

    .line 174
    sget-object v2, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 175
    .line 176
    invoke-virtual {v0, v2, v4}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    .line 178
    .line 179
    goto :goto_2

    .line 180
    :catch_0
    move-exception v0

    .line 181
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 182
    .line 183
    .line 184
    :cond_4
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    .line 185
    .line 186
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 187
    .line 188
    .line 189
    const-string v2, "deleteSyncedRawJpg size = "

    .line 190
    .line 191
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    .line 193
    .line 194
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    .line 195
    .line 196
    .line 197
    move-result v2

    .line 198
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 199
    .line 200
    .line 201
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 202
    .line 203
    .line 204
    move-result-object v0

    .line 205
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    .line 207
    .line 208
    :cond_5
    return-void
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public 〇00〇8()V
    .locals 7

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->O0O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->o8oOOo:Z

    .line 6
    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇OOo8〇0:Lcom/intsig/camscanner/settings/DocsSizeManager;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/settings/DocsSizeManager;->〇〇888()J

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    const-wide/16 v2, 0x0

    .line 16
    .line 17
    const/16 v4, 0x8

    .line 18
    .line 19
    const/4 v5, 0x0

    .line 20
    cmp-long v6, v0, v2

    .line 21
    .line 22
    if-lez v6, :cond_0

    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇08O〇00〇o:Lcom/intsig/camscanner/view/CircleProgressBar;

    .line 25
    .line 26
    const/4 v3, 0x1

    .line 27
    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 28
    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->oOo〇8o008:Landroid/widget/TextView;

    .line 31
    .line 32
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 33
    .line 34
    .line 35
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->oOo0:Landroid/widget/TextView;

    .line 36
    .line 37
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇08O〇00〇o:Lcom/intsig/camscanner/view/CircleProgressBar;

    .line 42
    .line 43
    invoke-virtual {v2, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 44
    .line 45
    .line 46
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇08O〇00〇o:Lcom/intsig/camscanner/view/CircleProgressBar;

    .line 47
    .line 48
    invoke-virtual {v2}, Lcom/intsig/camscanner/view/CircleProgressBar;->〇o〇()V

    .line 49
    .line 50
    .line 51
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->oOo〇8o008:Landroid/widget/TextView;

    .line 52
    .line 53
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 54
    .line 55
    .line 56
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->oOo0:Landroid/widget/TextView;

    .line 57
    .line 58
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 59
    .line 60
    .line 61
    :goto_0
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->O8o08O8O:Landroid/widget/TextView;

    .line 62
    .line 63
    invoke-static {v0, v1}, Lcom/intsig/utils/MemoryUtils;->〇o〇(J)Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    .line 69
    .line 70
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇080OO8〇0:Landroid/widget/TextView;

    .line 71
    .line 72
    iget-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇OOo8〇0:Lcom/intsig/camscanner/settings/DocsSizeManager;

    .line 73
    .line 74
    invoke-virtual {v1}, Lcom/intsig/camscanner/settings/DocsSizeManager;->oO80()J

    .line 75
    .line 76
    .line 77
    move-result-wide v1

    .line 78
    invoke-static {v1, v2}, Lcom/intsig/utils/MemoryUtils;->〇o〇(J)Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v1

    .line 82
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    .line 84
    .line 85
    :cond_2
    return-void
    .line 86
.end method

.method public 〇oOO8O8()Landroid/view/View;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const v1, 0x7f0d0028

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const v1, 0x7f0a04c1

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Lcom/intsig/camscanner/view/CircleProgressBar;

    .line 23
    .line 24
    iput-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇08O〇00〇o:Lcom/intsig/camscanner/view/CircleProgressBar;

    .line 25
    .line 26
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27
    .line 28
    .line 29
    const v1, 0x7f0a0f84

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    iput-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->o〇00O:Landroid/view/View;

    .line 37
    .line 38
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    .line 40
    .line 41
    const v1, 0x7f0a17e5

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    check-cast v1, Landroid/widget/TextView;

    .line 49
    .line 50
    iput-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->O8o08O8O:Landroid/widget/TextView;

    .line 51
    .line 52
    const v1, 0x7f0a176c

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    check-cast v1, Landroid/widget/TextView;

    .line 60
    .line 61
    iput-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇0O:Landroid/widget/TextView;

    .line 62
    .line 63
    const v1, 0x7f0a17e3

    .line 64
    .line 65
    .line 66
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    check-cast v1, Landroid/widget/TextView;

    .line 71
    .line 72
    iput-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇080OO8〇0:Landroid/widget/TextView;

    .line 73
    .line 74
    const v1, 0x7f0a12f7

    .line 75
    .line 76
    .line 77
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    check-cast v1, Landroid/widget/TextView;

    .line 82
    .line 83
    iput-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->oOo〇8o008:Landroid/widget/TextView;

    .line 84
    .line 85
    const v1, 0x7f0a160a

    .line 86
    .line 87
    .line 88
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 89
    .line 90
    .line 91
    move-result-object v1

    .line 92
    check-cast v1, Landroid/widget/TextView;

    .line 93
    .line 94
    iput-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->oOo0:Landroid/widget/TextView;

    .line 95
    .line 96
    const v1, 0x7f0a0d81

    .line 97
    .line 98
    .line 99
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 100
    .line 101
    .line 102
    move-result-object v1

    .line 103
    iput-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->OO〇00〇8oO:Landroid/view/View;

    .line 104
    .line 105
    const v1, 0x7f0a0bfe

    .line 106
    .line 107
    .line 108
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    iput-object v1, p0, Lcom/intsig/camscanner/settings/CacheManager;->o8〇OO0〇0o:Landroid/view/View;

    .line 113
    .line 114
    const v1, 0x7f0a0d04

    .line 115
    .line 116
    .line 117
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 118
    .line 119
    .line 120
    move-result-object v2

    .line 121
    iput-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇8〇oO〇〇8o:Landroid/view/View;

    .line 122
    .line 123
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->OO:Landroid/preference/PreferenceFragment;

    .line 124
    .line 125
    if-eqz v2, :cond_0

    .line 126
    .line 127
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->o0:Landroid/content/Context;

    .line 128
    .line 129
    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 130
    .line 131
    .line 132
    move-result v2

    .line 133
    if-nez v2, :cond_0

    .line 134
    .line 135
    iget-object v2, p0, Lcom/intsig/camscanner/settings/CacheManager;->〇8〇oO〇〇8o:Landroid/view/View;

    .line 136
    .line 137
    const/4 v3, 0x0

    .line 138
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 139
    .line 140
    .line 141
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 142
    .line 143
    .line 144
    move-result-object v1

    .line 145
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    .line 147
    .line 148
    :cond_0
    return-object v0
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

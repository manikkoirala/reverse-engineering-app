.class Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;
.super Ljava/lang/Object;
.source "OcrLanguagePresenter.java"

# interfaces
.implements Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguagePresenter;


# instance fields
.field private O8:I
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field private 〇080:Landroid/app/Activity;

.field private 〇o00〇〇Oo:Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;

.field private 〇o〇:Z


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇080:Landroid/app/Activity;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private oO80()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/nativelib/OcrLanguage;->getSystemLanguageKey()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    xor-int/lit8 v0, v0, 0x1

    .line 10
    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o〇0()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/ocrapi/OcrStateSwitcher;->〇o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->oO80()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/nativelib/OcrLanguage;->getSystemLanguageKey()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;->〇o〇(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private 〇〇888(Landroid/content/Intent;)V
    .locals 3

    .line 1
    const-string v0, "com.intsig.camscanner.settings.ocr.lang.OcrLanguageActivity.from_where"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const-string v0, "com.intsig.camscanner.settings.ocr.lang.OcrLanguageActivity.image_to_excel"

    .line 8
    .line 9
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v1, 0x1

    .line 14
    const-string v2, "OcrLanguagePresenter"

    .line 15
    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    const-string p1, "IMAGE_TO_EXCEL"

    .line 19
    .line 20
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iput-boolean v1, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇o〇:Z

    .line 24
    .line 25
    const p1, 0x7f160017

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;

    .line 29
    .line 30
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;->〇o00〇〇Oo(I)V

    .line 31
    .line 32
    .line 33
    const p1, 0x7f13061e

    .line 34
    .line 35
    .line 36
    iput p1, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->O8:I

    .line 37
    .line 38
    invoke-static {}, Lcom/intsig/camscanner/ocrapi/OcrStateSwitcher;->O8()Z

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    if-eqz p1, :cond_0

    .line 43
    .line 44
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->oO80()Z

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    if-eqz p1, :cond_0

    .line 49
    .line 50
    invoke-static {}, Lcom/intsig/camscanner/ocrapi/OcrStateSwitcher;->〇80〇808〇O()V

    .line 51
    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;

    .line 54
    .line 55
    invoke-static {}, Lcom/intsig/nativelib/OcrLanguage;->getSystemLanguageKey4Excel()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;->〇o〇(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;

    .line 63
    .line 64
    const-string v0, "KEY_EXCEL_LANG_ENG"

    .line 65
    .line 66
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;->〇080(Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_1
    const-string v0, "com.intsig.camscanner.settings.ocr.lang.OcrLanguageActivity.local_ocr"

    .line 71
    .line 72
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    const/4 v0, 0x0

    .line 77
    if-eqz p1, :cond_2

    .line 78
    .line 79
    const-string p1, "LOCAL_OCR"

    .line 80
    .line 81
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    iput-boolean v0, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇o〇:Z

    .line 85
    .line 86
    const p1, 0x7f16001b

    .line 87
    .line 88
    .line 89
    iget-object v1, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;

    .line 90
    .line 91
    invoke-interface {v1, p1}, Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;->〇o00〇〇Oo(I)V

    .line 92
    .line 93
    .line 94
    const p1, 0x7f130630

    .line 95
    .line 96
    .line 97
    iput p1, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->O8:I

    .line 98
    .line 99
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->o〇0()V

    .line 100
    .line 101
    .line 102
    invoke-static {v0}, Lcom/intsig/camscanner/ocrapi/OcrStateSwitcher;->OO0o〇〇〇〇0(I)V

    .line 103
    .line 104
    .line 105
    goto :goto_0

    .line 106
    :cond_2
    const-string p1, "CLOUD_OCR"

    .line 107
    .line 108
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    iput-boolean v0, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇o〇:Z

    .line 112
    .line 113
    const p1, 0x7f16001c

    .line 114
    .line 115
    .line 116
    iget-object v0, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;

    .line 117
    .line 118
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/settings/ocr/lang/IOcrLanguageView;->〇o00〇〇Oo(I)V

    .line 119
    .line 120
    .line 121
    const p1, 0x7f130616

    .line 122
    .line 123
    .line 124
    iput p1, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->O8:I

    .line 125
    .line 126
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->o〇0()V

    .line 127
    .line 128
    .line 129
    invoke-static {v1}, Lcom/intsig/camscanner/ocrapi/OcrStateSwitcher;->OO0o〇〇〇〇0(I)V

    .line 130
    .line 131
    .line 132
    :goto_0
    return-void
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method


# virtual methods
.method public O8(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇o〇:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {p1}, Lcom/intsig/nativelib/OcrLanguage;->getExcelLanguageByKey(Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1

    .line 10
    :cond_0
    invoke-static {p1}, Lcom/intsig/nativelib/OcrLanguage;->getLanguageByKey(Ljava/lang/String;)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public Oo08()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->O8:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public start()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇080:Landroid/app/Activity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Landroid/content/Intent;

    .line 10
    .line 11
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇080:Landroid/app/Activity;

    .line 16
    .line 17
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    :goto_0
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇〇888(Landroid/content/Intent;)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇080(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇o〇:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {p1}, Lcom/intsig/camscanner/mode_ocr/LanguageChoose;->〇080(Ljava/lang/String;)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1

    .line 10
    :cond_0
    invoke-static {p1}, Lcom/intsig/camscanner/mode_ocr/LanguageChoose;->〇o00〇〇Oo(Ljava/lang/String;)I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    return p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇o00〇〇Oo()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇o〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()I
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/settings/ocr/lang/OcrLanguagePresenter;->〇o〇:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/nativelib/OcrLanguage;->getExcelLanguageCount()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0

    .line 10
    :cond_0
    invoke-static {}, Lcom/intsig/nativelib/OcrLanguage;->getLanguageCount()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/settings/newsettings/viewmodel/MoreSettingsViewModel;
.super Landroidx/lifecycle/AndroidViewModel;
.source "MoreSettingsViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/settings/newsettings/viewmodel/MoreSettingsViewModel$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/MoreSettingsViewModel$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private o0:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MoreSettingsViewModel$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MoreSettingsViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MoreSettingsViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/MoreSettingsViewModel$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "application"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Landroidx/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V

    .line 7
    .line 8
    .line 9
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 10
    .line 11
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MoreSettingsViewModel;->o0:Landroidx/lifecycle/MutableLiveData;

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public final 〇80〇808〇O()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MoreSettingsViewModel;->o0:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8o8o〇()V
    .locals 14

    .line 1
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;-><init>(I)V

    .line 5
    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setItemType(I)V

    .line 9
    .line 10
    .line 11
    const v3, 0x7f1303b8

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setTitleRes(I)V

    .line 15
    .line 16
    .line 17
    new-instance v3, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;

    .line 18
    .line 19
    invoke-direct {v3, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;-><init>(I)V

    .line 20
    .line 21
    .line 22
    const/4 v4, 0x3

    .line 23
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setItemType(I)V

    .line 24
    .line 25
    .line 26
    const v5, 0x7f13064c

    .line 27
    .line 28
    .line 29
    invoke-virtual {v3, v5}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setTitleRes(I)V

    .line 30
    .line 31
    .line 32
    new-instance v5, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;

    .line 33
    .line 34
    invoke-direct {v5, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;-><init>(I)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v5, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setItemType(I)V

    .line 38
    .line 39
    .line 40
    const v6, 0x7f131ad1

    .line 41
    .line 42
    .line 43
    invoke-virtual {v5, v6}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setTitleRes(I)V

    .line 44
    .line 45
    .line 46
    new-instance v6, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;

    .line 47
    .line 48
    invoke-direct {v6, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;-><init>(I)V

    .line 49
    .line 50
    .line 51
    const/4 v7, 0x2

    .line 52
    invoke-virtual {v6, v7}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setItemType(I)V

    .line 53
    .line 54
    .line 55
    const v8, 0x7f13010a

    .line 56
    .line 57
    .line 58
    invoke-virtual {v6, v8}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setTitleRes(I)V

    .line 59
    .line 60
    .line 61
    new-instance v8, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;

    .line 62
    .line 63
    invoke-direct {v8, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;-><init>(I)V

    .line 64
    .line 65
    .line 66
    const/4 v9, 0x4

    .line 67
    invoke-virtual {v8, v9}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setItemType(I)V

    .line 68
    .line 69
    .line 70
    const v10, 0x7f131192

    .line 71
    .line 72
    .line 73
    invoke-virtual {v8, v10}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setTitleRes(I)V

    .line 74
    .line 75
    .line 76
    new-instance v10, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;

    .line 77
    .line 78
    invoke-direct {v10, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;-><init>(I)V

    .line 79
    .line 80
    .line 81
    const/4 v11, 0x5

    .line 82
    invoke-virtual {v10, v11}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setItemType(I)V

    .line 83
    .line 84
    .line 85
    const v11, 0x7f130406

    .line 86
    .line 87
    .line 88
    invoke-virtual {v10, v11}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setTitleRes(I)V

    .line 89
    .line 90
    .line 91
    new-instance v11, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;

    .line 92
    .line 93
    invoke-direct {v11, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;-><init>(I)V

    .line 94
    .line 95
    .line 96
    const/4 v12, 0x6

    .line 97
    invoke-virtual {v11, v12}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setItemType(I)V

    .line 98
    .line 99
    .line 100
    const v12, 0x7f130c5c

    .line 101
    .line 102
    .line 103
    invoke-virtual {v11, v12}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setTitleRes(I)V

    .line 104
    .line 105
    .line 106
    new-instance v12, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;

    .line 107
    .line 108
    invoke-direct {v12, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;-><init>(I)V

    .line 109
    .line 110
    .line 111
    const/4 v13, 0x7

    .line 112
    invoke-virtual {v12, v13}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setItemType(I)V

    .line 113
    .line 114
    .line 115
    const v13, 0x7f131bf1

    .line 116
    .line 117
    .line 118
    invoke-virtual {v12, v13}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setTitleRes(I)V

    .line 119
    .line 120
    .line 121
    new-array v9, v9, [Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;

    .line 122
    .line 123
    aput-object v0, v9, v2

    .line 124
    .line 125
    aput-object v5, v9, v1

    .line 126
    .line 127
    aput-object v6, v9, v7

    .line 128
    .line 129
    aput-object v3, v9, v4

    .line 130
    .line 131
    invoke-static {v9}, Lkotlin/collections/CollectionsKt;->Oooo8o0〇([Ljava/lang/Object;)Ljava/util/List;

    .line 132
    .line 133
    .line 134
    move-result-object v0

    .line 135
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->o〇0()Z

    .line 136
    .line 137
    .line 138
    move-result v1

    .line 139
    if-eqz v1, :cond_0

    .line 140
    .line 141
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->〇〇888()Z

    .line 142
    .line 143
    .line 144
    move-result v1

    .line 145
    if-nez v1, :cond_0

    .line 146
    .line 147
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    .line 149
    .line 150
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 151
    .line 152
    invoke-virtual {v1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇O8〇〇o()Z

    .line 153
    .line 154
    .line 155
    move-result v1

    .line 156
    if-nez v1, :cond_1

    .line 157
    .line 158
    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    .line 160
    .line 161
    :cond_1
    invoke-static {}, Lcom/intsig/base/ToolbarThemeGet;->Oo08()Z

    .line 162
    .line 163
    .line 164
    move-result v1

    .line 165
    if-eqz v1, :cond_2

    .line 166
    .line 167
    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    .line 169
    .line 170
    :cond_2
    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    .line 172
    .line 173
    iget-object v1, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MoreSettingsViewModel;->o0:Landroidx/lifecycle/MutableLiveData;

    .line 174
    .line 175
    invoke-virtual {v1, v0}, Landroidx/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 176
    .line 177
    .line 178
    return-void
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

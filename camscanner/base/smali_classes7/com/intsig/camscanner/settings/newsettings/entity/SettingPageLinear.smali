.class public final Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;
.super Ljava/lang/Object;
.source "SettingPageLinear.kt"

# interfaces
.implements Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private itemType:I

.field private showArrow:Z

.field private subtitleColorRes:I
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field private subtitleRes:I

.field private titleRes:I
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field private final type:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput p1, p0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->type:I

    .line 5
    .line 6
    const/4 p1, 0x1

    .line 7
    iput-boolean p1, p0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->showArrow:Z

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public final getItemType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->itemType:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getShowArrow()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->showArrow:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getSubtitleColorRes()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->subtitleColorRes:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getSubtitleRes()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->subtitleRes:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getTitleRes()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->titleRes:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->type:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final setItemType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->itemType:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setShowArrow(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->showArrow:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setSubtitleColorRes(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->subtitleColorRes:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setSubtitleRes(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->subtitleRes:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setTitleRes(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->titleRes:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

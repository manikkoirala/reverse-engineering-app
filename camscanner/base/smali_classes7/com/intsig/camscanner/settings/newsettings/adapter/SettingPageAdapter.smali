.class public final Lcom/intsig/camscanner/settings/newsettings/adapter/SettingPageAdapter;
.super Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;
.source "SettingPageAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/BaseProviderMultiAdapter<",
        "Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/intsig/camscanner/settings/newsettings/adapter/SettingPageAdapter;-><init>(Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;",
            ">;)V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;-><init>(Ljava/util/List;)V

    .line 4
    new-instance p1, Lcom/intsig/camscanner/settings/newsettings/adapter/provider/SettingTitleProvider;

    const/4 v0, 0x0

    const v1, 0x7f0d04af

    invoke-direct {p1, v0, v1}, Lcom/intsig/camscanner/settings/newsettings/adapter/provider/SettingTitleProvider;-><init>(II)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 5
    new-instance p1, Lcom/intsig/camscanner/settings/newsettings/adapter/provider/SettingRightArrowLineProvider;

    const/4 v0, 0x1

    const v1, 0x7f0d04ac

    invoke-direct {p1, v0, v1}, Lcom/intsig/camscanner/settings/newsettings/adapter/provider/SettingRightArrowLineProvider;-><init>(II)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 6
    new-instance p1, Lcom/intsig/camscanner/settings/newsettings/adapter/provider/SettingRightTxtLineProvider;

    const/4 v0, 0x2

    const v1, 0x7f0d04ae

    invoke-direct {p1, v0, v1}, Lcom/intsig/camscanner/settings/newsettings/adapter/provider/SettingRightTxtLineProvider;-><init>(II)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 7
    new-instance p1, Lcom/intsig/camscanner/settings/newsettings/adapter/provider/SettingLogInOrOutProvider;

    const/4 v0, 0x3

    const v1, 0x7f0d04ad

    invoke-direct {p1, v0, v1}, Lcom/intsig/camscanner/settings/newsettings/adapter/provider/SettingLogInOrOutProvider;-><init>(II)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 8
    new-instance p1, Lcom/intsig/camscanner/settings/newsettings/adapter/provider/SettingCsPdfVipProvider;

    invoke-direct {p1}, Lcom/intsig/camscanner/settings/newsettings/adapter/provider/SettingCsPdfVipProvider;-><init>()V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 2
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/adapter/SettingPageAdapter;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method protected oO〇(Ljava/util/List;I)I
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;",
            ">;I)I"
        }
    .end annotation

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    check-cast p1, Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;

    .line 11
    .line 12
    invoke-interface {p1}, Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;->getType()I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    return p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

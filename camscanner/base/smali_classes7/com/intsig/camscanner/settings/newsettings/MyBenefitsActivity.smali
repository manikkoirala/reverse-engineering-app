.class public final Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "MyBenefitsActivity.kt"


# annotations
.annotation build Lcom/alibaba/android/arouter/facade/annotation/Route;
    name = "\u6211\u7684\u8d44\u4ea7\u9875\u9762"
    path = "/me/benefits"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇O〇〇O8:Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic 〇o0O:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final O0O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8oOOo:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private ooo0〇〇O:Ljava/lang/String;

.field private 〇〇08O:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "binding"

    .line 7
    .line 8
    const-string v3, "getBinding()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇o0O:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇O〇〇O8:Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, ""

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 7
    .line 8
    iput-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇〇08O:Ljava/lang/String;

    .line 9
    .line 10
    new-instance v0, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 11
    .line 12
    const-class v1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 13
    .line 14
    invoke-direct {v0, v1, p0}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;-><init>(Ljava/lang/Class;Landroid/app/Activity;)V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->O0O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 18
    .line 19
    sget-object v0, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 20
    .line 21
    new-instance v1, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity$mActiveFlag$2;

    .line 22
    .line 23
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity$mActiveFlag$2;-><init>(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;)V

    .line 24
    .line 25
    .line 26
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    iput-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->o8oOOo:Lkotlin/Lazy;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private final O00OoO〇(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    move-object v0, v1

    .line 12
    :goto_0
    if-nez v0, :cond_1

    .line 13
    .line 14
    goto :goto_1

    .line 15
    :cond_1
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 16
    .line 17
    .line 18
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    if-eqz p1, :cond_2

    .line 23
    .line 24
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 25
    .line 26
    :cond_2
    if-nez v1, :cond_3

    .line 27
    .line 28
    goto :goto_2

    .line 29
    :cond_3
    const p1, 0x7f080238

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0, p1}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    invoke-virtual {v1, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 37
    .line 38
    .line 39
    :goto_2
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    if-eqz p1, :cond_4

    .line 44
    .line 45
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 46
    .line 47
    if-eqz p1, :cond_4

    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 50
    .line 51
    const v1, 0x7f060230

    .line 52
    .line 53
    .line 54
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 59
    .line 60
    .line 61
    :cond_4
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static synthetic O0〇(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇0o0oO〇〇0(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static final O80OO(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    invoke-virtual {p1}, Landroidx/appcompat/widget/AppCompatTextView;->getText()Ljava/lang/CharSequence;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 p1, 0x0

    .line 22
    :goto_0
    const v0, 0x7f130fab

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    if-eqz p1, :cond_2

    .line 34
    .line 35
    invoke-static {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    if-eqz p1, :cond_1

    .line 40
    .line 41
    const-string p1, "cs_privilege_storage"

    .line 42
    .line 43
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->OO〇〇o0oO(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_1
    const/16 p1, 0x64

    .line 48
    .line 49
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->OO0o〇〇〇〇0(Landroid/app/Activity;I)V

    .line 50
    .line 51
    .line 52
    :goto_1
    const-string p1, "100gb_clould_storage"

    .line 53
    .line 54
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇o〇OO80oO(Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    :cond_2
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private final O88(Ljava/lang/String;Lcom/intsig/okgo/callback/CustomStringCallback;)V
    .locals 2

    .line 1
    const-string v0, "MyBenefitsActivity"

    .line 2
    .line 3
    const-string v1, "queryGiftTask"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "cs_privilege"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/tianshu/TianShuAPI;->ooOO(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-static {p1}, Lcom/lzy/okgo/OkGo;->get(Ljava/lang/String;)Lcom/lzy/okgo/request/GetRequest;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity$queryGiftTask$1;

    .line 19
    .line 20
    invoke-direct {v0, p2}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity$queryGiftTask$1;-><init>(Lcom/intsig/okgo/callback/CustomStringCallback;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1, v0}, Lcom/lzy/okgo/request/base/Request;->execute(Lcom/lzy/okgo/callback/Callback;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic O880O〇(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇o〇OO80oO(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final O8O()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->OoO8()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "getSyncAccount()"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final O8〇o0〇〇8(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    invoke-virtual {p1}, Landroidx/appcompat/widget/AppCompatTextView;->getText()Ljava/lang/CharSequence;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 p1, 0x0

    .line 22
    :goto_0
    const v0, 0x7f130fab

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    if-eqz p1, :cond_2

    .line 34
    .line 35
    invoke-static {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    if-eqz p1, :cond_1

    .line 40
    .line 41
    const-string p1, "cs_privilege_pdf"

    .line 42
    .line 43
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->OO〇〇o0oO(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_1
    const/16 p1, 0x64

    .line 48
    .line 49
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->OO0o〇〇〇〇0(Landroid/app/Activity;I)V

    .line 50
    .line 51
    .line 52
    :goto_1
    const-string p1, "extra_tool_full_pdf_editor"

    .line 53
    .line 54
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇o〇OO80oO(Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    :cond_2
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static final synthetic OO0O(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->O00OoO〇(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final OO〇〇o0oO(Ljava/lang/String;)V
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    new-instance v2, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity$addGiftAction$1;

    .line 10
    .line 11
    invoke-direct {v2, p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity$addGiftAction$1;-><init>(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    const-string v3, "cs_privilege"

    .line 15
    .line 16
    invoke-static {v0, v3, p1, v1, v2}, Lcom/intsig/tianshu/TianShuAPI;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/okgo/callback/CustomStringCallback;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final OoO〇OOo8o()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇OoO0o0()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eq v0, v1, :cond_1

    .line 7
    .line 8
    const/4 v1, 0x2

    .line 9
    if-eq v0, v1, :cond_0

    .line 10
    .line 11
    const-string v0, ""

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const-string v0, "purchase_time"

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_1
    const-string v0, "trial_time"

    .line 18
    .line 19
    :goto_0
    return-object v0
    .line 20
    .line 21
.end method

.method public static final synthetic OooO〇(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Lcom/intsig/camscanner/data/GiftTaskJson;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->o〇OoO0(Lcom/intsig/camscanner/data/GiftTaskJson;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static synthetic O〇080〇o0(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->O80OO(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->O8〇o0〇〇8(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final synthetic O〇〇O80o8(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;)Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final O〇〇o8O(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    move-object v0, v1

    .line 12
    :goto_0
    if-nez v0, :cond_1

    .line 13
    .line 14
    goto :goto_1

    .line 15
    :cond_1
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 16
    .line 17
    .line 18
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    if-eqz p1, :cond_2

    .line 23
    .line 24
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 25
    .line 26
    :cond_2
    if-nez v1, :cond_3

    .line 27
    .line 28
    goto :goto_2

    .line 29
    :cond_3
    const p1, 0x7f080238

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0, p1}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    invoke-virtual {v1, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 37
    .line 38
    .line 39
    :goto_2
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    if-eqz p1, :cond_4

    .line 44
    .line 45
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 46
    .line 47
    if-eqz p1, :cond_4

    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 50
    .line 51
    const v1, 0x7f060230

    .line 52
    .line 53
    .line 54
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 59
    .line 60
    .line 61
    :cond_4
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private final o0O0O〇〇〇0()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity$initData$1;

    .line 6
    .line 7
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity$initData$1;-><init>(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;)V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->O88(Ljava/lang/String;Lcom/intsig/okgo/callback/CustomStringCallback;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o0OO(Lcom/intsig/camscanner/data/GiftTaskJson;)V
    .locals 13

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    iget-object v1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->status:Ljava/lang/Integer;

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    move-object v1, v0

    .line 8
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    const-string v3, "refreshPdfView"

    .line 14
    .line 15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    const-string v2, "MyBenefitsActivity"

    .line 26
    .line 27
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    if-eqz p1, :cond_1

    .line 31
    .line 32
    iget-object v1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->status:Ljava/lang/Integer;

    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_1
    move-object v1, v0

    .line 36
    :goto_1
    const/4 v2, 0x1

    .line 37
    const/4 v3, 0x0

    .line 38
    if-eqz v1, :cond_1a

    .line 39
    .line 40
    const/4 v4, -0x1

    .line 41
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 42
    .line 43
    .line 44
    move-result v5

    .line 45
    if-ne v5, v4, :cond_2

    .line 46
    .line 47
    goto/16 :goto_10

    .line 48
    .line 49
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 50
    .line 51
    .line 52
    move-result-object v4

    .line 53
    if-eqz v4, :cond_3

    .line 54
    .line 55
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇o0O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 56
    .line 57
    if-eqz v4, :cond_3

    .line 58
    .line 59
    invoke-static {v4, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 60
    .line 61
    .line 62
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 63
    .line 64
    .line 65
    move-result-object v4

    .line 66
    if-eqz v4, :cond_4

    .line 67
    .line 68
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8o:Landroid/widget/LinearLayout;

    .line 69
    .line 70
    if-eqz v4, :cond_4

    .line 71
    .line 72
    invoke-static {v4, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 73
    .line 74
    .line 75
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 76
    .line 77
    .line 78
    move-result v4

    .line 79
    const v5, 0x7f130faf

    .line 80
    .line 81
    .line 82
    const/4 v6, -0x2

    .line 83
    const-string v7, ": "

    .line 84
    .line 85
    const/16 v8, 0x3e8

    .line 86
    .line 87
    if-eq v4, v6, :cond_e

    .line 88
    .line 89
    const/4 v9, 0x3

    .line 90
    if-eq v4, v9, :cond_9

    .line 91
    .line 92
    if-eqz v4, :cond_e

    .line 93
    .line 94
    if-eq v4, v2, :cond_5

    .line 95
    .line 96
    goto/16 :goto_11

    .line 97
    .line 98
    :cond_5
    if-eqz p1, :cond_6

    .line 99
    .line 100
    iget-object p1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->create_time:Ljava/lang/String;

    .line 101
    .line 102
    goto :goto_2

    .line 103
    :cond_6
    move-object p1, v0

    .line 104
    :goto_2
    invoke-static {p1}, Lcom/intsig/utils/NumberUtils;->〇080(Ljava/lang/String;)J

    .line 105
    .line 106
    .line 107
    move-result-wide v1

    .line 108
    int-to-long v3, v8

    .line 109
    mul-long v1, v1, v3

    .line 110
    .line 111
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 112
    .line 113
    .line 114
    move-result-object p1

    .line 115
    if-eqz p1, :cond_7

    .line 116
    .line 117
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 118
    .line 119
    :cond_7
    const p1, 0x7f130fac

    .line 120
    .line 121
    .line 122
    if-nez v0, :cond_8

    .line 123
    .line 124
    goto :goto_3

    .line 125
    :cond_8
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v3

    .line 129
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO8Ooo〇(J)Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object v1

    .line 133
    new-instance v2, Ljava/lang/StringBuilder;

    .line 134
    .line 135
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 136
    .line 137
    .line 138
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    .line 140
    .line 141
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    .line 143
    .line 144
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    .line 146
    .line 147
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 148
    .line 149
    .line 150
    move-result-object v1

    .line 151
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    .line 153
    .line 154
    :goto_3
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object p1

    .line 158
    const-string v0, "getString(R.string.cs_613_gift_10)"

    .line 159
    .line 160
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->O〇〇o8O(Ljava/lang/String;)V

    .line 164
    .line 165
    .line 166
    goto/16 :goto_11

    .line 167
    .line 168
    :cond_9
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 169
    .line 170
    .line 171
    move-result-object v1

    .line 172
    if-eqz v1, :cond_a

    .line 173
    .line 174
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 175
    .line 176
    if-eqz v1, :cond_a

    .line 177
    .line 178
    const v2, 0x7f080836

    .line 179
    .line 180
    .line 181
    invoke-virtual {v1, v2}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 182
    .line 183
    .line 184
    :cond_a
    if-eqz p1, :cond_b

    .line 185
    .line 186
    iget-object p1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->expiry_time:Ljava/lang/String;

    .line 187
    .line 188
    goto :goto_4

    .line 189
    :cond_b
    move-object p1, v0

    .line 190
    :goto_4
    invoke-static {p1}, Lcom/intsig/utils/NumberUtils;->〇080(Ljava/lang/String;)J

    .line 191
    .line 192
    .line 193
    move-result-wide v1

    .line 194
    int-to-long v3, v8

    .line 195
    mul-long v1, v1, v3

    .line 196
    .line 197
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 198
    .line 199
    .line 200
    move-result-object p1

    .line 201
    if-eqz p1, :cond_c

    .line 202
    .line 203
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 204
    .line 205
    :cond_c
    if-nez v0, :cond_d

    .line 206
    .line 207
    goto :goto_5

    .line 208
    :cond_d
    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 209
    .line 210
    .line 211
    move-result-object p1

    .line 212
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO8Ooo〇(J)Ljava/lang/String;

    .line 213
    .line 214
    .line 215
    move-result-object v1

    .line 216
    new-instance v2, Ljava/lang/StringBuilder;

    .line 217
    .line 218
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 219
    .line 220
    .line 221
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    .line 223
    .line 224
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    .line 226
    .line 227
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    .line 229
    .line 230
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 231
    .line 232
    .line 233
    move-result-object p1

    .line 234
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    .line 236
    .line 237
    :goto_5
    const p1, 0x7f130ae2

    .line 238
    .line 239
    .line 240
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 241
    .line 242
    .line 243
    move-result-object p1

    .line 244
    const-string v0, "getString(R.string.cs_536_svip_02)"

    .line 245
    .line 246
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    .line 248
    .line 249
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->O〇〇o8O(Ljava/lang/String;)V

    .line 250
    .line 251
    .line 252
    goto/16 :goto_11

    .line 253
    .line 254
    :cond_e
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 255
    .line 256
    .line 257
    move-result-object v4

    .line 258
    if-eqz v4, :cond_f

    .line 259
    .line 260
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 261
    .line 262
    goto :goto_6

    .line 263
    :cond_f
    move-object v4, v0

    .line 264
    :goto_6
    if-nez v4, :cond_10

    .line 265
    .line 266
    goto :goto_7

    .line 267
    :cond_10
    const v9, 0x7f130fab

    .line 268
    .line 269
    .line 270
    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 271
    .line 272
    .line 273
    move-result-object v9

    .line 274
    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    .line 276
    .line 277
    :goto_7
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 278
    .line 279
    .line 280
    move-result-object v4

    .line 281
    if-eqz v4, :cond_11

    .line 282
    .line 283
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 284
    .line 285
    goto :goto_8

    .line 286
    :cond_11
    move-object v4, v0

    .line 287
    :goto_8
    if-nez v4, :cond_12

    .line 288
    .line 289
    goto :goto_9

    .line 290
    :cond_12
    const v9, 0x7f08018d

    .line 291
    .line 292
    .line 293
    invoke-virtual {p0, v9}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 294
    .line 295
    .line 296
    move-result-object v9

    .line 297
    invoke-virtual {v4, v9}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 298
    .line 299
    .line 300
    :goto_9
    if-eqz p1, :cond_13

    .line 301
    .line 302
    iget-object p1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->expiry_time:Ljava/lang/String;

    .line 303
    .line 304
    goto :goto_a

    .line 305
    :cond_13
    move-object p1, v0

    .line 306
    :goto_a
    invoke-static {p1}, Lcom/intsig/utils/NumberUtils;->〇080(Ljava/lang/String;)J

    .line 307
    .line 308
    .line 309
    move-result-wide v9

    .line 310
    int-to-long v11, v8

    .line 311
    mul-long v9, v9, v11

    .line 312
    .line 313
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 314
    .line 315
    .line 316
    move-result-object p1

    .line 317
    if-eqz p1, :cond_14

    .line 318
    .line 319
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 320
    .line 321
    goto :goto_b

    .line 322
    :cond_14
    move-object p1, v0

    .line 323
    :goto_b
    if-nez p1, :cond_15

    .line 324
    .line 325
    goto :goto_c

    .line 326
    :cond_15
    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 327
    .line 328
    .line 329
    move-result-object v4

    .line 330
    invoke-direct {p0, v9, v10}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO8Ooo〇(J)Ljava/lang/String;

    .line 331
    .line 332
    .line 333
    move-result-object v5

    .line 334
    new-instance v8, Ljava/lang/StringBuilder;

    .line 335
    .line 336
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 337
    .line 338
    .line 339
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    .line 341
    .line 342
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    .line 344
    .line 345
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    .line 347
    .line 348
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 349
    .line 350
    .line 351
    move-result-object v4

    .line 352
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 353
    .line 354
    .line 355
    :goto_c
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 356
    .line 357
    .line 358
    move-result p1

    .line 359
    if-ne p1, v6, :cond_1d

    .line 360
    .line 361
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 362
    .line 363
    .line 364
    move-result-object p1

    .line 365
    if-eqz p1, :cond_16

    .line 366
    .line 367
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 368
    .line 369
    goto :goto_d

    .line 370
    :cond_16
    move-object p1, v0

    .line 371
    :goto_d
    if-nez p1, :cond_17

    .line 372
    .line 373
    goto :goto_e

    .line 374
    :cond_17
    const v1, 0x3ecccccd    # 0.4f

    .line 375
    .line 376
    .line 377
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 378
    .line 379
    .line 380
    :goto_e
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 381
    .line 382
    .line 383
    move-result-object p1

    .line 384
    if-eqz p1, :cond_18

    .line 385
    .line 386
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 387
    .line 388
    :cond_18
    if-nez v0, :cond_19

    .line 389
    .line 390
    goto :goto_f

    .line 391
    :cond_19
    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 392
    .line 393
    .line 394
    :goto_f
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 395
    .line 396
    .line 397
    move-result-object p1

    .line 398
    if-eqz p1, :cond_1d

    .line 399
    .line 400
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->oo8ooo8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 401
    .line 402
    if-eqz p1, :cond_1d

    .line 403
    .line 404
    invoke-static {p1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 405
    .line 406
    .line 407
    goto :goto_11

    .line 408
    :cond_1a
    :goto_10
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 409
    .line 410
    .line 411
    move-result-object p1

    .line 412
    if-eqz p1, :cond_1b

    .line 413
    .line 414
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇o0O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 415
    .line 416
    if-eqz p1, :cond_1b

    .line 417
    .line 418
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 419
    .line 420
    .line 421
    :cond_1b
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 422
    .line 423
    .line 424
    move-result-object p1

    .line 425
    if-eqz p1, :cond_1c

    .line 426
    .line 427
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8o:Landroid/widget/LinearLayout;

    .line 428
    .line 429
    if-eqz p1, :cond_1c

    .line 430
    .line 431
    invoke-static {p1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 432
    .line 433
    .line 434
    :cond_1c
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 435
    .line 436
    .line 437
    move-result-object p1

    .line 438
    if-eqz p1, :cond_1d

    .line 439
    .line 440
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 441
    .line 442
    if-eqz p1, :cond_1d

    .line 443
    .line 444
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 445
    .line 446
    .line 447
    :cond_1d
    :goto_11
    return-void
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method public static final synthetic o0Oo(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static final synthetic o808o8o08(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Lcom/intsig/camscanner/data/GiftTaskJson;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇0o88Oo〇(Lcom/intsig/camscanner/data/GiftTaskJson;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final oOO8oo0()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v1, L〇0888/〇080;

    .line 12
    .line 13
    invoke-direct {v1, p0}, L〇0888/〇080;-><init>(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 17
    .line 18
    .line 19
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 26
    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    new-instance v1, L〇0888/〇o00〇〇Oo;

    .line 30
    .line 31
    invoke-direct {v1, p0}, L〇0888/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    .line 36
    .line 37
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    if-eqz v0, :cond_2

    .line 42
    .line 43
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 44
    .line 45
    if-eqz v0, :cond_2

    .line 46
    .line 47
    new-instance v1, L〇0888/〇o〇;

    .line 48
    .line 49
    invoke-direct {v1, p0}, L〇0888/〇o〇;-><init>(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    .line 54
    .line 55
    :cond_2
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static final synthetic o〇08oO80o(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->O〇〇o8O(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final o〇OoO0(Lcom/intsig/camscanner/data/GiftTaskJson;Z)V
    .locals 16
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n",
            "StringFormatMatches"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    iget-object v3, v1, Lcom/intsig/camscanner/data/GiftTaskJson;->status:Ljava/lang/Integer;

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    move-object v3, v2

    .line 12
    :goto_0
    if-eqz v3, :cond_25

    .line 13
    .line 14
    const/4 v4, -0x1

    .line 15
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 16
    .line 17
    .line 18
    move-result v5

    .line 19
    if-ne v5, v4, :cond_1

    .line 20
    .line 21
    goto/16 :goto_15

    .line 22
    .line 23
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 24
    .line 25
    .line 26
    move-result-object v4

    .line 27
    const/4 v5, 0x1

    .line 28
    if-eqz v4, :cond_2

    .line 29
    .line 30
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->O88O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 31
    .line 32
    if-eqz v4, :cond_2

    .line 33
    .line 34
    invoke-static {v4, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 35
    .line 36
    .line 37
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    const/4 v6, 0x0

    .line 42
    if-eqz v4, :cond_3

    .line 43
    .line 44
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8o:Landroid/widget/LinearLayout;

    .line 45
    .line 46
    if-eqz v4, :cond_3

    .line 47
    .line 48
    invoke-static {v4, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 49
    .line 50
    .line 51
    :cond_3
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 52
    .line 53
    .line 54
    move-result v4

    .line 55
    const v7, 0x7f130faf

    .line 56
    .line 57
    .line 58
    const/4 v8, -0x2

    .line 59
    const/16 v9, 0x3e8

    .line 60
    .line 61
    const/4 v10, 0x3

    .line 62
    const-string v11, ": "

    .line 63
    .line 64
    if-eq v4, v8, :cond_11

    .line 65
    .line 66
    if-eq v4, v10, :cond_b

    .line 67
    .line 68
    if-eqz v4, :cond_11

    .line 69
    .line 70
    if-eq v4, v5, :cond_4

    .line 71
    .line 72
    goto/16 :goto_14

    .line 73
    .line 74
    :cond_4
    if-eqz v1, :cond_5

    .line 75
    .line 76
    iget-object v1, v1, Lcom/intsig/camscanner/data/GiftTaskJson;->create_time:Ljava/lang/String;

    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_5
    move-object v1, v2

    .line 80
    :goto_1
    invoke-static {v1}, Lcom/intsig/utils/NumberUtils;->〇080(Ljava/lang/String;)J

    .line 81
    .line 82
    .line 83
    move-result-wide v7

    .line 84
    int-to-long v12, v9

    .line 85
    mul-long v7, v7, v12

    .line 86
    .line 87
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    if-eqz v1, :cond_6

    .line 92
    .line 93
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->O0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 94
    .line 95
    goto :goto_2

    .line 96
    :cond_6
    move-object v1, v2

    .line 97
    :goto_2
    if-nez v1, :cond_7

    .line 98
    .line 99
    goto :goto_3

    .line 100
    :cond_7
    const v4, 0x7f130fa9

    .line 101
    .line 102
    .line 103
    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v4

    .line 107
    invoke-direct {v0, v7, v8}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO8Ooo〇(J)Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v7

    .line 111
    new-instance v8, Ljava/lang/StringBuilder;

    .line 112
    .line 113
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    .line 115
    .line 116
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v4

    .line 129
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    .line 131
    .line 132
    :goto_3
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 133
    .line 134
    .line 135
    move-result-object v1

    .line 136
    if-eqz v1, :cond_8

    .line 137
    .line 138
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇〇08O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 139
    .line 140
    if-eqz v1, :cond_8

    .line 141
    .line 142
    invoke-static {v1, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 143
    .line 144
    .line 145
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 146
    .line 147
    .line 148
    move-result-object v1

    .line 149
    if-eqz v1, :cond_9

    .line 150
    .line 151
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇〇08O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 152
    .line 153
    goto :goto_4

    .line 154
    :cond_9
    move-object v1, v2

    .line 155
    :goto_4
    if-nez v1, :cond_a

    .line 156
    .line 157
    goto :goto_5

    .line 158
    :cond_a
    const v4, 0x7f130faa

    .line 159
    .line 160
    .line 161
    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object v4

    .line 165
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->O8O()Ljava/lang/String;

    .line 166
    .line 167
    .line 168
    move-result-object v7

    .line 169
    new-instance v8, Ljava/lang/StringBuilder;

    .line 170
    .line 171
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 172
    .line 173
    .line 174
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    .line 176
    .line 177
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    .line 179
    .line 180
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    .line 182
    .line 183
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 184
    .line 185
    .line 186
    move-result-object v4

    .line 187
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    .line 189
    .line 190
    :goto_5
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->O8O()Ljava/lang/String;

    .line 191
    .line 192
    .line 193
    move-result-object v1

    .line 194
    new-instance v4, Ljava/lang/StringBuilder;

    .line 195
    .line 196
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 197
    .line 198
    .line 199
    const-string v7, "Account atvTextVipAccount:"

    .line 200
    .line 201
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    .line 203
    .line 204
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    .line 206
    .line 207
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 208
    .line 209
    .line 210
    move-result-object v1

    .line 211
    const-string v4, "MyBenefitsActivity"

    .line 212
    .line 213
    invoke-static {v4, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    .line 215
    .line 216
    const v1, 0x7f130fac

    .line 217
    .line 218
    .line 219
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 220
    .line 221
    .line 222
    move-result-object v1

    .line 223
    const-string v4, "getString(R.string.cs_613_gift_10)"

    .line 224
    .line 225
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 226
    .line 227
    .line 228
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->O00OoO〇(Ljava/lang/String;)V

    .line 229
    .line 230
    .line 231
    goto/16 :goto_14

    .line 232
    .line 233
    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 234
    .line 235
    .line 236
    move-result-object v4

    .line 237
    if-eqz v4, :cond_c

    .line 238
    .line 239
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 240
    .line 241
    if-eqz v4, :cond_c

    .line 242
    .line 243
    const v8, 0x7f080ba8

    .line 244
    .line 245
    .line 246
    invoke-virtual {v4, v8}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 247
    .line 248
    .line 249
    :cond_c
    if-eqz v1, :cond_d

    .line 250
    .line 251
    iget-object v1, v1, Lcom/intsig/camscanner/data/GiftTaskJson;->expiry_time:Ljava/lang/String;

    .line 252
    .line 253
    goto :goto_6

    .line 254
    :cond_d
    move-object v1, v2

    .line 255
    :goto_6
    invoke-static {v1}, Lcom/intsig/utils/NumberUtils;->〇080(Ljava/lang/String;)J

    .line 256
    .line 257
    .line 258
    move-result-wide v12

    .line 259
    int-to-long v8, v9

    .line 260
    mul-long v12, v12, v8

    .line 261
    .line 262
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 263
    .line 264
    .line 265
    move-result-object v1

    .line 266
    if-eqz v1, :cond_e

    .line 267
    .line 268
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->O0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 269
    .line 270
    goto :goto_7

    .line 271
    :cond_e
    move-object v1, v2

    .line 272
    :goto_7
    if-nez v1, :cond_f

    .line 273
    .line 274
    goto :goto_8

    .line 275
    :cond_f
    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 276
    .line 277
    .line 278
    move-result-object v4

    .line 279
    invoke-direct {v0, v12, v13}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO8Ooo〇(J)Ljava/lang/String;

    .line 280
    .line 281
    .line 282
    move-result-object v7

    .line 283
    new-instance v8, Ljava/lang/StringBuilder;

    .line 284
    .line 285
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 286
    .line 287
    .line 288
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    .line 290
    .line 291
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292
    .line 293
    .line 294
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    .line 296
    .line 297
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 298
    .line 299
    .line 300
    move-result-object v4

    .line 301
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 302
    .line 303
    .line 304
    :goto_8
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 305
    .line 306
    .line 307
    move-result-object v1

    .line 308
    if-eqz v1, :cond_10

    .line 309
    .line 310
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇〇08O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 311
    .line 312
    if-eqz v1, :cond_10

    .line 313
    .line 314
    const v4, 0x7f130fad

    .line 315
    .line 316
    .line 317
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 318
    .line 319
    .line 320
    :cond_10
    const v1, 0x7f130ae2

    .line 321
    .line 322
    .line 323
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 324
    .line 325
    .line 326
    move-result-object v1

    .line 327
    const-string v4, "getString(R.string.cs_536_svip_02)"

    .line 328
    .line 329
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 330
    .line 331
    .line 332
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->O00OoO〇(Ljava/lang/String;)V

    .line 333
    .line 334
    .line 335
    goto/16 :goto_14

    .line 336
    .line 337
    :cond_11
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 338
    .line 339
    .line 340
    move-result-object v4

    .line 341
    if-eqz v4, :cond_12

    .line 342
    .line 343
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 344
    .line 345
    goto :goto_9

    .line 346
    :cond_12
    move-object v4, v2

    .line 347
    :goto_9
    if-nez v4, :cond_13

    .line 348
    .line 349
    goto :goto_a

    .line 350
    :cond_13
    const v12, 0x7f130fab

    .line 351
    .line 352
    .line 353
    invoke-virtual {v0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 354
    .line 355
    .line 356
    move-result-object v12

    .line 357
    invoke-virtual {v4, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 358
    .line 359
    .line 360
    :goto_a
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 361
    .line 362
    .line 363
    move-result-object v4

    .line 364
    if-eqz v4, :cond_14

    .line 365
    .line 366
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 367
    .line 368
    goto :goto_b

    .line 369
    :cond_14
    move-object v4, v2

    .line 370
    :goto_b
    if-nez v4, :cond_15

    .line 371
    .line 372
    goto :goto_c

    .line 373
    :cond_15
    const v12, 0x7f08018d

    .line 374
    .line 375
    .line 376
    invoke-virtual {v0, v12}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 377
    .line 378
    .line 379
    move-result-object v12

    .line 380
    invoke-virtual {v4, v12}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 381
    .line 382
    .line 383
    :goto_c
    if-eqz v1, :cond_16

    .line 384
    .line 385
    iget-object v1, v1, Lcom/intsig/camscanner/data/GiftTaskJson;->expiry_time:Ljava/lang/String;

    .line 386
    .line 387
    goto :goto_d

    .line 388
    :cond_16
    move-object v1, v2

    .line 389
    :goto_d
    invoke-static {v1}, Lcom/intsig/utils/NumberUtils;->〇080(Ljava/lang/String;)J

    .line 390
    .line 391
    .line 392
    move-result-wide v12

    .line 393
    int-to-long v14, v9

    .line 394
    mul-long v12, v12, v14

    .line 395
    .line 396
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 397
    .line 398
    .line 399
    move-result-object v1

    .line 400
    if-eqz v1, :cond_17

    .line 401
    .line 402
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->O0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 403
    .line 404
    goto :goto_e

    .line 405
    :cond_17
    move-object v1, v2

    .line 406
    :goto_e
    if-nez v1, :cond_18

    .line 407
    .line 408
    goto :goto_f

    .line 409
    :cond_18
    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 410
    .line 411
    .line 412
    move-result-object v4

    .line 413
    invoke-direct {v0, v12, v13}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO8Ooo〇(J)Ljava/lang/String;

    .line 414
    .line 415
    .line 416
    move-result-object v7

    .line 417
    new-instance v9, Ljava/lang/StringBuilder;

    .line 418
    .line 419
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 420
    .line 421
    .line 422
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 423
    .line 424
    .line 425
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    .line 427
    .line 428
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    .line 430
    .line 431
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 432
    .line 433
    .line 434
    move-result-object v4

    .line 435
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 436
    .line 437
    .line 438
    :goto_f
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 439
    .line 440
    .line 441
    move-result v1

    .line 442
    if-ne v1, v8, :cond_1d

    .line 443
    .line 444
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 445
    .line 446
    .line 447
    move-result-object v1

    .line 448
    if-eqz v1, :cond_19

    .line 449
    .line 450
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 451
    .line 452
    goto :goto_10

    .line 453
    :cond_19
    move-object v1, v2

    .line 454
    :goto_10
    if-nez v1, :cond_1a

    .line 455
    .line 456
    goto :goto_11

    .line 457
    :cond_1a
    const v4, 0x3ecccccd    # 0.4f

    .line 458
    .line 459
    .line 460
    invoke-virtual {v1, v4}, Landroid/view/View;->setAlpha(F)V

    .line 461
    .line 462
    .line 463
    :goto_11
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 464
    .line 465
    .line 466
    move-result-object v1

    .line 467
    if-eqz v1, :cond_1b

    .line 468
    .line 469
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 470
    .line 471
    goto :goto_12

    .line 472
    :cond_1b
    move-object v1, v2

    .line 473
    :goto_12
    if-nez v1, :cond_1c

    .line 474
    .line 475
    goto :goto_13

    .line 476
    :cond_1c
    invoke-virtual {v1, v6}, Landroid/view/View;->setEnabled(Z)V

    .line 477
    .line 478
    .line 479
    :goto_13
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 480
    .line 481
    .line 482
    move-result-object v1

    .line 483
    if-eqz v1, :cond_1d

    .line 484
    .line 485
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->oo8ooo8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 486
    .line 487
    if-eqz v1, :cond_1d

    .line 488
    .line 489
    invoke-static {v1, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 490
    .line 491
    .line 492
    :cond_1d
    :goto_14
    const-string v1, "getString(R.string.cs_61\u2026gift_02, \"3\", vipTypeStr)"

    .line 493
    .line 494
    const-string v4, "3"

    .line 495
    .line 496
    const/4 v7, 0x2

    .line 497
    const v8, 0x7f130fa4

    .line 498
    .line 499
    .line 500
    if-eqz p2, :cond_21

    .line 501
    .line 502
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 503
    .line 504
    .line 505
    move-result v3

    .line 506
    if-eq v3, v10, :cond_1e

    .line 507
    .line 508
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 509
    .line 510
    .line 511
    move-result-object v3

    .line 512
    if-eqz v3, :cond_1e

    .line 513
    .line 514
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 515
    .line 516
    if-eqz v3, :cond_1e

    .line 517
    .line 518
    const v9, 0x7f080ba7

    .line 519
    .line 520
    .line 521
    invoke-virtual {v3, v9}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 522
    .line 523
    .line 524
    :cond_1e
    const v3, 0x7f130b7d

    .line 525
    .line 526
    .line 527
    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 528
    .line 529
    .line 530
    move-result-object v3

    .line 531
    const-string v9, "getString(R.string.cs_542_renew_182)"

    .line 532
    .line 533
    invoke-static {v3, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 534
    .line 535
    .line 536
    new-array v7, v7, [Ljava/lang/Object;

    .line 537
    .line 538
    aput-object v4, v7, v6

    .line 539
    .line 540
    aput-object v3, v7, v5

    .line 541
    .line 542
    invoke-virtual {v0, v8, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 543
    .line 544
    .line 545
    move-result-object v3

    .line 546
    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 547
    .line 548
    .line 549
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 550
    .line 551
    .line 552
    move-result-object v1

    .line 553
    if-eqz v1, :cond_1f

    .line 554
    .line 555
    iget-object v2, v1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 556
    .line 557
    :cond_1f
    if-nez v2, :cond_20

    .line 558
    .line 559
    goto :goto_15

    .line 560
    :cond_20
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 561
    .line 562
    .line 563
    goto :goto_15

    .line 564
    :cond_21
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 565
    .line 566
    .line 567
    move-result v3

    .line 568
    if-eq v3, v10, :cond_22

    .line 569
    .line 570
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 571
    .line 572
    .line 573
    move-result-object v3

    .line 574
    if-eqz v3, :cond_22

    .line 575
    .line 576
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 577
    .line 578
    if-eqz v3, :cond_22

    .line 579
    .line 580
    const v9, 0x7f080ba6

    .line 581
    .line 582
    .line 583
    invoke-virtual {v3, v9}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 584
    .line 585
    .line 586
    :cond_22
    const v3, 0x7f1303d8

    .line 587
    .line 588
    .line 589
    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 590
    .line 591
    .line 592
    move-result-object v3

    .line 593
    const-string v9, "getString(R.string.a_summary_vip_account)"

    .line 594
    .line 595
    invoke-static {v3, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 596
    .line 597
    .line 598
    new-array v7, v7, [Ljava/lang/Object;

    .line 599
    .line 600
    aput-object v4, v7, v6

    .line 601
    .line 602
    aput-object v3, v7, v5

    .line 603
    .line 604
    invoke-virtual {v0, v8, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 605
    .line 606
    .line 607
    move-result-object v3

    .line 608
    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 609
    .line 610
    .line 611
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 612
    .line 613
    .line 614
    move-result-object v1

    .line 615
    if-eqz v1, :cond_23

    .line 616
    .line 617
    iget-object v2, v1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 618
    .line 619
    :cond_23
    if-nez v2, :cond_24

    .line 620
    .line 621
    goto :goto_15

    .line 622
    :cond_24
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 623
    .line 624
    .line 625
    :cond_25
    :goto_15
    return-void
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method public static final synthetic o〇o08〇(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Lcom/intsig/camscanner/data/GiftTaskJson;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->o0OO(Lcom/intsig/camscanner/data/GiftTaskJson;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final startActivity(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇O〇〇O8:Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity$Companion;->startActivity(Landroid/content/Context;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static final 〇0o0oO〇〇0(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    invoke-virtual {p1}, Landroidx/appcompat/widget/AppCompatTextView;->getText()Ljava/lang/CharSequence;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 p1, 0x0

    .line 22
    :goto_0
    const v0, 0x7f130fab

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    if-eqz p1, :cond_3

    .line 34
    .line 35
    invoke-static {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    if-eqz p1, :cond_1

    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇〇08O:Ljava/lang/String;

    .line 42
    .line 43
    if-eqz p1, :cond_2

    .line 44
    .line 45
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->OO〇〇o0oO(Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    goto :goto_1

    .line 49
    :cond_1
    const/16 p1, 0x64

    .line 50
    .line 51
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->OO0o〇〇〇〇0(Landroid/app/Activity;I)V

    .line 52
    .line 53
    .line 54
    :cond_2
    :goto_1
    const-string p1, "3month_gold"

    .line 55
    .line 56
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇o〇OO80oO(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    :cond_3
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private final 〇0o88Oo〇(Lcom/intsig/camscanner/data/GiftTaskJson;)V
    .locals 13

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    iget-object v1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->status:Ljava/lang/Integer;

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    move-object v1, v0

    .line 8
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    const-string v3, "refreshCloudView"

    .line 14
    .line 15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    const-string v2, "MyBenefitsActivity"

    .line 26
    .line 27
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    if-eqz v1, :cond_1

    .line 35
    .line 36
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_1
    move-object v1, v0

    .line 40
    :goto_1
    const/4 v2, 0x0

    .line 41
    const/4 v3, 0x1

    .line 42
    if-nez v1, :cond_2

    .line 43
    .line 44
    goto :goto_2

    .line 45
    :cond_2
    new-array v4, v3, [Ljava/lang/Object;

    .line 46
    .line 47
    const-string v5, "100"

    .line 48
    .line 49
    aput-object v5, v4, v2

    .line 50
    .line 51
    const v5, 0x7f130fa5

    .line 52
    .line 53
    .line 54
    invoke-virtual {p0, v5, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v4

    .line 58
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    .line 60
    .line 61
    :goto_2
    if-eqz p1, :cond_3

    .line 62
    .line 63
    iget-object v1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->status:Ljava/lang/Integer;

    .line 64
    .line 65
    goto :goto_3

    .line 66
    :cond_3
    move-object v1, v0

    .line 67
    :goto_3
    if-eqz v1, :cond_1c

    .line 68
    .line 69
    const/4 v4, -0x1

    .line 70
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 71
    .line 72
    .line 73
    move-result v5

    .line 74
    if-ne v5, v4, :cond_4

    .line 75
    .line 76
    goto/16 :goto_12

    .line 77
    .line 78
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 79
    .line 80
    .line 81
    move-result-object v4

    .line 82
    if-eqz v4, :cond_5

    .line 83
    .line 84
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇O〇〇O8:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 85
    .line 86
    if-eqz v4, :cond_5

    .line 87
    .line 88
    invoke-static {v4, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 89
    .line 90
    .line 91
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 92
    .line 93
    .line 94
    move-result-object v4

    .line 95
    if-eqz v4, :cond_6

    .line 96
    .line 97
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8o:Landroid/widget/LinearLayout;

    .line 98
    .line 99
    if-eqz v4, :cond_6

    .line 100
    .line 101
    invoke-static {v4, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 102
    .line 103
    .line 104
    :cond_6
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 105
    .line 106
    .line 107
    move-result v4

    .line 108
    const v5, 0x7f130faf

    .line 109
    .line 110
    .line 111
    const/4 v6, -0x2

    .line 112
    const-string v7, ": "

    .line 113
    .line 114
    const/16 v8, 0x3e8

    .line 115
    .line 116
    if-eq v4, v6, :cond_10

    .line 117
    .line 118
    const/4 v9, 0x3

    .line 119
    if-eq v4, v9, :cond_b

    .line 120
    .line 121
    if-eqz v4, :cond_10

    .line 122
    .line 123
    if-eq v4, v3, :cond_7

    .line 124
    .line 125
    goto/16 :goto_13

    .line 126
    .line 127
    :cond_7
    if-eqz p1, :cond_8

    .line 128
    .line 129
    iget-object p1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->create_time:Ljava/lang/String;

    .line 130
    .line 131
    goto :goto_4

    .line 132
    :cond_8
    move-object p1, v0

    .line 133
    :goto_4
    invoke-static {p1}, Lcom/intsig/utils/NumberUtils;->〇080(Ljava/lang/String;)J

    .line 134
    .line 135
    .line 136
    move-result-wide v1

    .line 137
    int-to-long v3, v8

    .line 138
    mul-long v1, v1, v3

    .line 139
    .line 140
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 141
    .line 142
    .line 143
    move-result-object p1

    .line 144
    if-eqz p1, :cond_9

    .line 145
    .line 146
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatTextView;

    .line 147
    .line 148
    :cond_9
    const p1, 0x7f130fac

    .line 149
    .line 150
    .line 151
    if-nez v0, :cond_a

    .line 152
    .line 153
    goto :goto_5

    .line 154
    :cond_a
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v3

    .line 158
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO8Ooo〇(J)Ljava/lang/String;

    .line 159
    .line 160
    .line 161
    move-result-object v1

    .line 162
    new-instance v2, Ljava/lang/StringBuilder;

    .line 163
    .line 164
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    .line 166
    .line 167
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    .line 169
    .line 170
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    .line 172
    .line 173
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    .line 175
    .line 176
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 177
    .line 178
    .line 179
    move-result-object v1

    .line 180
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    .line 182
    .line 183
    :goto_5
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 184
    .line 185
    .line 186
    move-result-object p1

    .line 187
    const-string v0, "getString(R.string.cs_613_gift_10)"

    .line 188
    .line 189
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    .line 191
    .line 192
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇oOO80o(Ljava/lang/String;)V

    .line 193
    .line 194
    .line 195
    goto/16 :goto_13

    .line 196
    .line 197
    :cond_b
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 198
    .line 199
    .line 200
    move-result-object v1

    .line 201
    if-eqz v1, :cond_c

    .line 202
    .line 203
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 204
    .line 205
    if-eqz v1, :cond_c

    .line 206
    .line 207
    const v2, 0x7f080ba5

    .line 208
    .line 209
    .line 210
    invoke-virtual {v1, v2}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 211
    .line 212
    .line 213
    :cond_c
    if-eqz p1, :cond_d

    .line 214
    .line 215
    iget-object p1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->expiry_time:Ljava/lang/String;

    .line 216
    .line 217
    goto :goto_6

    .line 218
    :cond_d
    move-object p1, v0

    .line 219
    :goto_6
    invoke-static {p1}, Lcom/intsig/utils/NumberUtils;->〇080(Ljava/lang/String;)J

    .line 220
    .line 221
    .line 222
    move-result-wide v1

    .line 223
    int-to-long v3, v8

    .line 224
    mul-long v1, v1, v3

    .line 225
    .line 226
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 227
    .line 228
    .line 229
    move-result-object p1

    .line 230
    if-eqz p1, :cond_e

    .line 231
    .line 232
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatTextView;

    .line 233
    .line 234
    :cond_e
    if-nez v0, :cond_f

    .line 235
    .line 236
    goto :goto_7

    .line 237
    :cond_f
    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 238
    .line 239
    .line 240
    move-result-object p1

    .line 241
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO8Ooo〇(J)Ljava/lang/String;

    .line 242
    .line 243
    .line 244
    move-result-object v1

    .line 245
    new-instance v2, Ljava/lang/StringBuilder;

    .line 246
    .line 247
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 248
    .line 249
    .line 250
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    .line 252
    .line 253
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    .line 255
    .line 256
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    .line 258
    .line 259
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 260
    .line 261
    .line 262
    move-result-object p1

    .line 263
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    .line 265
    .line 266
    :goto_7
    const p1, 0x7f130ae2

    .line 267
    .line 268
    .line 269
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 270
    .line 271
    .line 272
    move-result-object p1

    .line 273
    const-string v0, "getString(R.string.cs_536_svip_02)"

    .line 274
    .line 275
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    .line 277
    .line 278
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇oOO80o(Ljava/lang/String;)V

    .line 279
    .line 280
    .line 281
    goto/16 :goto_13

    .line 282
    .line 283
    :cond_10
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 284
    .line 285
    .line 286
    move-result-object v4

    .line 287
    if-eqz v4, :cond_11

    .line 288
    .line 289
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 290
    .line 291
    goto :goto_8

    .line 292
    :cond_11
    move-object v4, v0

    .line 293
    :goto_8
    if-nez v4, :cond_12

    .line 294
    .line 295
    goto :goto_9

    .line 296
    :cond_12
    const v9, 0x7f130fab

    .line 297
    .line 298
    .line 299
    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 300
    .line 301
    .line 302
    move-result-object v9

    .line 303
    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    .line 305
    .line 306
    :goto_9
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 307
    .line 308
    .line 309
    move-result-object v4

    .line 310
    if-eqz v4, :cond_13

    .line 311
    .line 312
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 313
    .line 314
    goto :goto_a

    .line 315
    :cond_13
    move-object v4, v0

    .line 316
    :goto_a
    if-nez v4, :cond_14

    .line 317
    .line 318
    goto :goto_b

    .line 319
    :cond_14
    const v9, 0x7f08018d

    .line 320
    .line 321
    .line 322
    invoke-virtual {p0, v9}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 323
    .line 324
    .line 325
    move-result-object v9

    .line 326
    invoke-virtual {v4, v9}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 327
    .line 328
    .line 329
    :goto_b
    if-eqz p1, :cond_15

    .line 330
    .line 331
    iget-object p1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->expiry_time:Ljava/lang/String;

    .line 332
    .line 333
    goto :goto_c

    .line 334
    :cond_15
    move-object p1, v0

    .line 335
    :goto_c
    invoke-static {p1}, Lcom/intsig/utils/NumberUtils;->〇080(Ljava/lang/String;)J

    .line 336
    .line 337
    .line 338
    move-result-wide v9

    .line 339
    int-to-long v11, v8

    .line 340
    mul-long v9, v9, v11

    .line 341
    .line 342
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 343
    .line 344
    .line 345
    move-result-object p1

    .line 346
    if-eqz p1, :cond_16

    .line 347
    .line 348
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatTextView;

    .line 349
    .line 350
    goto :goto_d

    .line 351
    :cond_16
    move-object p1, v0

    .line 352
    :goto_d
    if-nez p1, :cond_17

    .line 353
    .line 354
    goto :goto_e

    .line 355
    :cond_17
    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 356
    .line 357
    .line 358
    move-result-object v4

    .line 359
    invoke-direct {p0, v9, v10}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO8Ooo〇(J)Ljava/lang/String;

    .line 360
    .line 361
    .line 362
    move-result-object v5

    .line 363
    new-instance v8, Ljava/lang/StringBuilder;

    .line 364
    .line 365
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 366
    .line 367
    .line 368
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    .line 370
    .line 371
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372
    .line 373
    .line 374
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    .line 376
    .line 377
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 378
    .line 379
    .line 380
    move-result-object v4

    .line 381
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 382
    .line 383
    .line 384
    :goto_e
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 385
    .line 386
    .line 387
    move-result p1

    .line 388
    if-ne p1, v6, :cond_1f

    .line 389
    .line 390
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 391
    .line 392
    .line 393
    move-result-object p1

    .line 394
    if-eqz p1, :cond_18

    .line 395
    .line 396
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 397
    .line 398
    goto :goto_f

    .line 399
    :cond_18
    move-object p1, v0

    .line 400
    :goto_f
    if-nez p1, :cond_19

    .line 401
    .line 402
    goto :goto_10

    .line 403
    :cond_19
    const v1, 0x3ecccccd    # 0.4f

    .line 404
    .line 405
    .line 406
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 407
    .line 408
    .line 409
    :goto_10
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 410
    .line 411
    .line 412
    move-result-object p1

    .line 413
    if-eqz p1, :cond_1a

    .line 414
    .line 415
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 416
    .line 417
    :cond_1a
    if-nez v0, :cond_1b

    .line 418
    .line 419
    goto :goto_11

    .line 420
    :cond_1b
    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 421
    .line 422
    .line 423
    :goto_11
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 424
    .line 425
    .line 426
    move-result-object p1

    .line 427
    if-eqz p1, :cond_1f

    .line 428
    .line 429
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->oo8ooo8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 430
    .line 431
    if-eqz p1, :cond_1f

    .line 432
    .line 433
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 434
    .line 435
    .line 436
    goto :goto_13

    .line 437
    :cond_1c
    :goto_12
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 438
    .line 439
    .line 440
    move-result-object p1

    .line 441
    if-eqz p1, :cond_1d

    .line 442
    .line 443
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->〇O〇〇O8:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 444
    .line 445
    if-eqz p1, :cond_1d

    .line 446
    .line 447
    invoke-static {p1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 448
    .line 449
    .line 450
    :cond_1d
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 451
    .line 452
    .line 453
    move-result-object p1

    .line 454
    if-eqz p1, :cond_1e

    .line 455
    .line 456
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->o8o:Landroid/widget/LinearLayout;

    .line 457
    .line 458
    if-eqz p1, :cond_1e

    .line 459
    .line 460
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 461
    .line 462
    .line 463
    :cond_1e
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 464
    .line 465
    .line 466
    move-result-object p1

    .line 467
    if-eqz p1, :cond_1f

    .line 468
    .line 469
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatTextView;

    .line 470
    .line 471
    if-eqz p1, :cond_1f

    .line 472
    .line 473
    invoke-static {p1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 474
    .line 475
    .line 476
    :cond_1f
    :goto_13
    return-void
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method private final 〇OoO0o0()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->o8oOOo:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Number;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇oO88o(Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇oOO80o(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final 〇oOO80o(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    move-object v0, v1

    .line 12
    :goto_0
    if-nez v0, :cond_1

    .line 13
    .line 14
    goto :goto_1

    .line 15
    :cond_1
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 16
    .line 17
    .line 18
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    if-eqz p1, :cond_2

    .line 23
    .line 24
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 25
    .line 26
    :cond_2
    if-nez v1, :cond_3

    .line 27
    .line 28
    goto :goto_2

    .line 29
    :cond_3
    const p1, 0x7f080238

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0, p1}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    invoke-virtual {v1, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 37
    .line 38
    .line 39
    :goto_2
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    if-eqz p1, :cond_4

    .line 44
    .line 45
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 46
    .line 47
    if-eqz p1, :cond_4

    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 50
    .line 51
    const v1, 0x7f060230

    .line 52
    .line 53
    .line 54
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 59
    .line 60
    .line 61
    :cond_4
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private final 〇ooO8Ooo〇(J)Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "yyyy.MM.dd"

    .line 2
    .line 3
    invoke-static {p1, p2, v0}, Lcom/intsig/utils/DateTimeUtil;->Oo08(JLjava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const-string p2, "getFormattedDateBySpecSt\u2026YYYYMMDDWithDot\n        )"

    .line 8
    .line 9
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-object p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final 〇ooO〇000()Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->O0O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇o0O:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;->〇〇888(Landroid/app/Activity;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/ActivityMyBenefitsBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇o〇OO80oO(Ljava/lang/String;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇OoO0o0()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "trackAction: actionId: "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v2, ", mActiveFlag: "

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    const-string v1, "MyBenefitsActivity"

    .line 31
    .line 32
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇OoO0o0()I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    const-string v1, "CSMyPrivilege"

    .line 40
    .line 41
    if-lez v0, :cond_0

    .line 42
    .line 43
    const/4 v0, 0x2

    .line 44
    new-array v0, v0, [Landroid/util/Pair;

    .line 45
    .line 46
    new-instance v2, Landroid/util/Pair;

    .line 47
    .line 48
    const-string v3, "scheme"

    .line 49
    .line 50
    const-string v4, "passive_pop"

    .line 51
    .line 52
    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 53
    .line 54
    .line 55
    const/4 v3, 0x0

    .line 56
    aput-object v2, v0, v3

    .line 57
    .line 58
    new-instance v2, Landroid/util/Pair;

    .line 59
    .line 60
    const-string v3, "type"

    .line 61
    .line 62
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->OoO〇OOo8o()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v4

    .line 66
    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 67
    .line 68
    .line 69
    const/4 v3, 0x1

    .line 70
    aput-object v2, v0, v3

    .line 71
    .line 72
    invoke-static {v1, p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 73
    .line 74
    .line 75
    goto :goto_0

    .line 76
    :cond_0
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    :goto_0
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public final OO0o(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇〇08O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/app/AppUtil;->〇〇o8(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->o0O0O〇〇〇0()V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->oOO8oo0()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .line 1
    const/4 v0, 0x4

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    const-string v0, "MyBenefitsActivity"

    .line 5
    .line 6
    const-string v1, "onKeyDown, go back"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "back"

    .line 12
    .line 13
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇o〇OO80oO(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/intsig/mvp/activity/BaseChangeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    return p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "item"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const v1, 0x102002c

    .line 11
    .line 12
    .line 13
    if-ne v1, v0, :cond_0

    .line 14
    .line 15
    const-string v0, "MyBenefitsActivity"

    .line 16
    .line 17
    const-string v1, "onOptionsItemSelected, go back"

    .line 18
    .line 19
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    const-string v0, "back"

    .line 23
    .line 24
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇o〇OO80oO(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    :cond_0
    invoke-super {p0, p1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    return p1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected onStart()V
    .locals 5

    .line 1
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStart()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇OoO0o0()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    new-instance v1, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    const-string v2, "onStart: mActiveFlag: "

    .line 14
    .line 15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const-string v1, "MyBenefitsActivity"

    .line 26
    .line 27
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->〇OoO0o0()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    const-string v1, "CSMyPrivilege"

    .line 35
    .line 36
    if-lez v0, :cond_0

    .line 37
    .line 38
    const-string v0, "type"

    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->OoO〇OOo8o()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v2

    .line 44
    const-string v3, "scheme"

    .line 45
    .line 46
    const-string v4, "passive_pop"

    .line 47
    .line 48
    invoke-static {v1, v3, v4, v0, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    invoke-static {v1}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    :goto_0
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    const v0, 0x7f0d008c

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0ooOOo(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇0ooOOo(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    const-string v0, "fromPart"

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    const-string v0, "null cannot be cast to non-null type kotlin.String"

    .line 13
    .line 14
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    iput-object p1, p0, Lcom/intsig/camscanner/settings/newsettings/MyBenefitsActivity;->ooo0〇〇O:Ljava/lang/String;

    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

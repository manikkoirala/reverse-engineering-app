.class public final Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;
.super Landroidx/lifecycle/AndroidViewModel;
.source "MyAccountViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080OO8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:I

.field private final OO:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Landroid/app/Application;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Z

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇080OO8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "app"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Landroidx/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->o0:Landroid/app/Application;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 12
    .line 13
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;-><init>(Landroid/app/Application;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 17
    .line 18
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 19
    .line 20
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 24
    .line 25
    invoke-static {}, Lcom/intsig/wechat/WeChatApi;->Oo08()Lcom/intsig/wechat/WeChatApi;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-virtual {p1}, Lcom/intsig/wechat/WeChatApi;->〇8o8o〇()Z

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    iput-boolean p1, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇08O〇00〇o:Z

    .line 34
    .line 35
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 36
    .line 37
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 38
    .line 39
    .line 40
    iput-object p1, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->o〇00O:Landroidx/lifecycle/MutableLiveData;

    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private final O8ooOoo〇([Lcom/intsig/comm/purchase/entity/Coupon;)I
    .locals 5

    .line 1
    array-length v0, p1

    .line 2
    const/4 v1, 0x0

    .line 3
    const/4 v2, 0x0

    .line 4
    :goto_0
    if-ge v1, v0, :cond_1

    .line 5
    .line 6
    aget-object v3, p1, v1

    .line 7
    .line 8
    iget v3, v3, Lcom/intsig/comm/purchase/entity/Coupon;->type:I

    .line 9
    .line 10
    const/4 v4, 0x7

    .line 11
    if-eq v3, v4, :cond_0

    .line 12
    .line 13
    add-int/lit8 v2, v2, 0x1

    .line 14
    .line 15
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    return v2
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic Oooo8o0〇(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇8o〇〇8080(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static final O〇O〇oO(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 7
    .line 8
    new-instance v1, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel$queryCoupon$1$1;

    .line 9
    .line 10
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel$queryCoupon$1$1;-><init>(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->oo88o8O(Lcom/intsig/okgo/callback/CustomStringCallback;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final o0ooO()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->o0:Landroid/app/Application;

    .line 7
    .line 8
    invoke-static {v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 13
    .line 14
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇808〇(Z)Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 22
    .line 23
    invoke-virtual {v2}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇o00〇〇Oo()Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 31
    .line 32
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->o800o8O(Z)Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    if-eqz v2, :cond_0

    .line 37
    .line 38
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    :cond_0
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇80()Z

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    if-nez v2, :cond_1

    .line 46
    .line 47
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->o0ooO()Z

    .line 48
    .line 49
    .line 50
    move-result v2

    .line 51
    if-nez v2, :cond_1

    .line 52
    .line 53
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 54
    .line 55
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->Oooo8o0〇(Z)Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    if-eqz v2, :cond_1

    .line 60
    .line 61
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    :cond_1
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 65
    .line 66
    invoke-virtual {v2}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇O8o08O()Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    if-eqz v2, :cond_2

    .line 71
    .line 72
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    .line 74
    .line 75
    :cond_2
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 76
    .line 77
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇888(Z)Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    if-eqz v2, :cond_3

    .line 82
    .line 83
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    .line 85
    .line 86
    :cond_3
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 87
    .line 88
    invoke-virtual {v2}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->oO80()Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageCsPdfVip;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    if-eqz v2, :cond_4

    .line 93
    .line 94
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    .line 96
    .line 97
    :cond_4
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->〇O〇80o08O(Ljava/util/List;)Ljava/lang/Object;

    .line 98
    .line 99
    .line 100
    move-result-object v2

    .line 101
    check-cast v2, Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;

    .line 102
    .line 103
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇oo〇(Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;)V

    .line 104
    .line 105
    .line 106
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 107
    .line 108
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->o〇0(Z)Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 109
    .line 110
    .line 111
    move-result-object v2

    .line 112
    if-eqz v2, :cond_5

    .line 113
    .line 114
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    .line 116
    .line 117
    :cond_5
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 118
    .line 119
    invoke-virtual {v2}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇O00()Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 120
    .line 121
    .line 122
    move-result-object v2

    .line 123
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    .line 125
    .line 126
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 127
    .line 128
    iget v3, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->O8o08O8O:I

    .line 129
    .line 130
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇8o8o〇(I)Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 131
    .line 132
    .line 133
    move-result-object v2

    .line 134
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    .line 136
    .line 137
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 138
    .line 139
    invoke-virtual {v2}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇80〇808〇O()Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 140
    .line 141
    .line 142
    move-result-object v2

    .line 143
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    .line 145
    .line 146
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->〇O〇80o08O(Ljava/util/List;)Ljava/lang/Object;

    .line 147
    .line 148
    .line 149
    move-result-object v2

    .line 150
    check-cast v2, Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;

    .line 151
    .line 152
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇oo〇(Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;)V

    .line 153
    .line 154
    .line 155
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 156
    .line 157
    invoke-virtual {v2}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->OoO8()Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 158
    .line 159
    .line 160
    move-result-object v2

    .line 161
    if-eqz v2, :cond_6

    .line 162
    .line 163
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    .line 165
    .line 166
    :cond_6
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->〇O〇80o08O(Ljava/util/List;)Ljava/lang/Object;

    .line 167
    .line 168
    .line 169
    move-result-object v2

    .line 170
    check-cast v2, Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;

    .line 171
    .line 172
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇oo〇(Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;)V

    .line 173
    .line 174
    .line 175
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 176
    .line 177
    invoke-virtual {v2}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇0〇O0088o()Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 178
    .line 179
    .line 180
    move-result-object v2

    .line 181
    if-eqz v2, :cond_7

    .line 182
    .line 183
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    .line 185
    .line 186
    :cond_7
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 187
    .line 188
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->OO0o〇〇(Z)Lcom/intsig/camscanner/settings/newsettings/entity/SettingLogInOrOut;

    .line 189
    .line 190
    .line 191
    move-result-object v1

    .line 192
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    .line 194
    .line 195
    return-object v0
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public static final synthetic oo88o8O(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;[Lcom/intsig/comm/purchase/entity/Coupon;)I
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->O8ooOoo〇([Lcom/intsig/comm/purchase/entity/Coupon;)I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final o〇0OOo〇0()V
    .locals 2

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->o0ooO()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :catch_0
    move-exception v0

    .line 12
    const-string v1, "MyAccountViewModel"

    .line 13
    .line 14
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 15
    .line 16
    .line 17
    :goto_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final o〇O(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇oo〇()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    iput v0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->O8o08O8O:I

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->o〇0OOo〇0()V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->O〇O〇oO(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static final 〇8o8O〇O(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇O888o0o()V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->o〇0OOo〇0()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic 〇8o8o〇(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->o〇O(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static final 〇8o〇〇8080(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->o〇O8〇〇o()V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->o〇0OOo〇0()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic 〇O00(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇8o8O〇O(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final 〇oo〇(Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;)V
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;->getType()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x2

    .line 6
    const/4 v2, 0x1

    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    const-string v0, "null cannot be cast to non-null type com.intsig.camscanner.settings.newsettings.entity.SettingPageRightTxtLinear"

    .line 10
    .line 11
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    check-cast p1, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 15
    .line 16
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setBottomDivider(Z)V

    .line 17
    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_0
    invoke-interface {p1}, Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;->getType()I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    const/4 v1, 0x4

    .line 25
    if-ne v0, v1, :cond_3

    .line 26
    .line 27
    instance-of v0, p1, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageCsPdfVip;

    .line 28
    .line 29
    if-eqz v0, :cond_1

    .line 30
    .line 31
    check-cast p1, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageCsPdfVip;

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_1
    const/4 p1, 0x0

    .line 35
    :goto_0
    if-nez p1, :cond_2

    .line 36
    .line 37
    goto :goto_1

    .line 38
    :cond_2
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageCsPdfVip;->setBottomDivider(Z)V

    .line 39
    .line 40
    .line 41
    :cond_3
    :goto_1
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public final O8〇o()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/settings/newsettings/entity/ISettingPageType;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇8oOo8O()V
    .locals 2

    .line 1
    const-string v0, "MyAccountViewModel"

    .line 2
    .line 3
    const-string v1, "queryPoint"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    new-instance v1, L〇〇0/〇o00〇〇Oo;

    .line 13
    .line 14
    invoke-direct {v1, p0}, L〇〇0/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
.end method

.method public final o8o〇〇0O()Lkotlinx/coroutines/flow/Flow;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/flow/Flow<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel$queryStripeSubscription$1;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel$queryStripeSubscription$1;-><init>(Lkotlin/coroutines/Continuation;)V

    .line 5
    .line 6
    .line 7
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->〇O00(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/flow/Flow;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    new-instance v2, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel$queryStripeSubscription$2;

    .line 12
    .line 13
    invoke-direct {v2, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel$queryStripeSubscription$2;-><init>(Lkotlin/coroutines/Continuation;)V

    .line 14
    .line 15
    .line 16
    invoke-static {v0, v2}, Lkotlinx/coroutines/flow/FlowKt;->o〇0(Lkotlinx/coroutines/flow/Flow;Lkotlin/jvm/functions/Function3;)Lkotlinx/coroutines/flow/Flow;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-static {v0, v1}, Lkotlinx/coroutines/flow/FlowKt;->〇〇8O0〇8(Lkotlinx/coroutines/flow/Flow;Lkotlin/coroutines/CoroutineContext;)Lkotlinx/coroutines/flow/Flow;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    return-object v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public final oO()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->〇00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "queryCoupon>>> isNeedQuery = "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const-string v2, "MyAccountViewModel"

    .line 23
    .line 24
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    if-nez v0, :cond_0

    .line 28
    .line 29
    return-void

    .line 30
    :cond_0
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    new-instance v1, L〇〇0/O8;

    .line 35
    .line 36
    invoke-direct {v1, p0}, L〇〇0/O8;-><init>(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public final oo〇()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->o〇00O:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇8oOO88()V
    .locals 2

    .line 1
    const-string v0, "MyAccountViewModel"

    .line 2
    .line 3
    const-string v1, "queryFaxBalance"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    new-instance v1, L〇〇0/〇080;

    .line 13
    .line 14
    invoke-direct {v1, p0}, L〇〇0/〇080;-><init>(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
.end method

.method public final 〇00()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->o〇0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->〇〇888()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇oo()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->o0:Landroid/app/Application;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    new-instance v1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v2, "queryStorage>>>  isLogin = "

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    const-string v2, "MyAccountViewModel"

    .line 25
    .line 26
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    if-nez v0, :cond_0

    .line 30
    .line 31
    return-void

    .line 32
    :cond_0
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    new-instance v1, L〇〇0/〇o〇;

    .line 37
    .line 38
    invoke-direct {v1, p0}, L〇〇0/〇o〇;-><init>(Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public final 〇〇〇0〇〇0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/MyAccountViewModel;->o0ooO()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

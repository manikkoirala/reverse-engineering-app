.class public final Lcom/intsig/camscanner/settings/newsettings/SettingsActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "SettingsActivity.kt"


# annotations
.annotation build Lcom/alibaba/android/arouter/facade/annotation/Route;
    name = "\u8bbe\u7f6e\u9875"
    path = "/me/setting_main"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/settings/newsettings/SettingsActivity$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o8oOOo:Lcom/intsig/camscanner/settings/newsettings/SettingsActivity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O0O:Z

.field private ooo0〇〇O:I

.field private 〇〇08O:Lcom/intsig/mvp/fragment/BaseChangeFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/SettingsActivity$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/SettingsActivity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/settings/newsettings/SettingsActivity;->o8oOOo:Lcom/intsig/camscanner/settings/newsettings/SettingsActivity$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, -0x1

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/settings/newsettings/SettingsActivity;->ooo0〇〇O:I

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    iput-boolean v0, p0, Lcom/intsig/camscanner/settings/newsettings/SettingsActivity;->O0O:Z

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O0〇(I)Lcom/intsig/mvp/fragment/BaseChangeFragment;
    .locals 2

    .line 1
    const/4 v0, -0x1

    .line 2
    const/4 v1, 0x0

    .line 3
    if-eq p1, v0, :cond_3

    .line 4
    .line 5
    if-eqz p1, :cond_2

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    if-eq p1, v0, :cond_1

    .line 9
    .line 10
    const/4 v0, 0x2

    .line 11
    if-eq p1, v0, :cond_1

    .line 12
    .line 13
    const/4 v0, 0x3

    .line 14
    if-eq p1, v0, :cond_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/settings/newsettings/fragment/CloudServiceSettingFragment;->O8o08O8O:Lcom/intsig/camscanner/settings/newsettings/fragment/CloudServiceSettingFragment$Companion;

    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/newsettings/fragment/CloudServiceSettingFragment$Companion;->〇080()Lcom/intsig/camscanner/settings/newsettings/fragment/CloudServiceSettingFragment;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    goto :goto_0

    .line 24
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/settings/newsettings/fragment/MyAccountFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/settings/newsettings/fragment/MyAccountFragment$Companion;

    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/newsettings/fragment/MyAccountFragment$Companion;->〇080()Lcom/intsig/camscanner/settings/newsettings/fragment/MyAccountFragment;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    goto :goto_0

    .line 31
    :cond_2
    sget-object p1, Lcom/intsig/camscanner/settings/newsettings/fragment/MoreSettingFragment;->o〇00O:Lcom/intsig/camscanner/settings/newsettings/fragment/MoreSettingFragment$Companion;

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/newsettings/fragment/MoreSettingFragment$Companion;->〇080()Lcom/intsig/camscanner/settings/newsettings/fragment/MoreSettingFragment;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    :cond_3
    :goto_0
    return-object v1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/app/AppUtil;->〇〇o8(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    iget p1, p0, Lcom/intsig/camscanner/settings/newsettings/SettingsActivity;->ooo0〇〇O:I

    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/SettingsActivity;->O0〇(I)Lcom/intsig/mvp/fragment/BaseChangeFragment;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    iput-object p1, p0, Lcom/intsig/camscanner/settings/newsettings/SettingsActivity;->〇〇08O:Lcom/intsig/mvp/fragment/BaseChangeFragment;

    .line 11
    .line 12
    if-nez p1, :cond_0

    .line 13
    .line 14
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 15
    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    const v0, 0x7f0a0650

    .line 19
    .line 20
    .line 21
    const/4 v1, 0x1

    .line 22
    invoke-virtual {p0, v0, p1, v1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->O〇8〇008(ILandroidx/fragment/app/Fragment;Z)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 2
    .line 3
    .line 4
    iget v0, p0, Lcom/intsig/camscanner/settings/newsettings/SettingsActivity;->ooo0〇〇O:I

    .line 5
    .line 6
    const/4 v1, 0x2

    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/SettingsActivity;->〇〇08O:Lcom/intsig/mvp/fragment/BaseChangeFragment;

    .line 10
    .line 11
    instance-of v1, v0, Lcom/intsig/camscanner/settings/newsettings/fragment/MyAccountFragment;

    .line 12
    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    iget-boolean p1, p0, Lcom/intsig/camscanner/settings/newsettings/SettingsActivity;->O0O:Z

    .line 18
    .line 19
    if-eqz p1, :cond_0

    .line 20
    .line 21
    const/4 p1, 0x0

    .line 22
    iput-boolean p1, p0, Lcom/intsig/camscanner/settings/newsettings/SettingsActivity;->O0O:Z

    .line 23
    .line 24
    const-string p1, "null cannot be cast to non-null type com.intsig.camscanner.settings.newsettings.fragment.MyAccountFragment"

    .line 25
    .line 26
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    check-cast v0, Lcom/intsig/camscanner/settings/newsettings/fragment/MyAccountFragment;

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/camscanner/settings/newsettings/fragment/MyAccountFragment;->OO〇〇o0oO()V

    .line 32
    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    const v0, 0x7f0d00b4

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0ooOOo(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇0ooOOo(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    const-string v0, "extra_which_page"

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    iput p1, p0, Lcom/intsig/camscanner/settings/newsettings/SettingsActivity;->ooo0〇〇O:I

    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public final Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;
.super Ljava/lang/Object;
.source "MyAccountRepo.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇o00〇〇Oo:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final 〇080:Landroid/app/Application;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇o00〇〇Oo:Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "app"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇080:Landroid/app/Application;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final O8(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 1
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/AppUtil;->Oo08(J)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    const-string v0, "byte2MB(count.toLong())"

    .line 10
    .line 11
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :catch_0
    move-exception p1

    .line 16
    const-string v0, "MyAccountRepo"

    .line 17
    .line 18
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 19
    .line 20
    .line 21
    const-string p1, "0"

    .line 22
    .line 23
    :goto_0
    return-object p1
    .line 24
.end method

.method private final OO0o〇〇〇〇0()Ljava/lang/String;
    .locals 8

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->ooo〇〇O〇()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    const-wide/16 v2, 0x0

    .line 6
    .line 7
    const/4 v4, 0x1

    .line 8
    const/4 v5, 0x0

    .line 9
    cmp-long v6, v2, v0

    .line 10
    .line 11
    if-gtz v6, :cond_0

    .line 12
    .line 13
    const-wide/16 v2, 0x7

    .line 14
    .line 15
    cmp-long v6, v0, v2

    .line 16
    .line 17
    if-gez v6, :cond_0

    .line 18
    .line 19
    const/4 v2, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v2, 0x0

    .line 22
    :goto_0
    const-string v3, "\uff09"

    .line 23
    .line 24
    const-string v6, "\uff08"

    .line 25
    .line 26
    if-ne v2, v4, :cond_1

    .line 27
    .line 28
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇080:Landroid/app/Application;

    .line 29
    .line 30
    new-array v4, v4, [Ljava/lang/Object;

    .line 31
    .line 32
    new-instance v7, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    aput-object v0, v4, v5

    .line 45
    .line 46
    const v0, 0x7f131c69

    .line 47
    .line 48
    .line 49
    invoke-virtual {v2, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    const-string v1, "app.getString(\n         \u2026ays\n                    )"

    .line 54
    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    new-instance v1, Ljava/lang/StringBuilder;

    .line 59
    .line 60
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    return-object v0

    .line 77
    :cond_1
    if-nez v2, :cond_2

    .line 78
    .line 79
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇8o()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    const v1, 0x7f131c68

    .line 84
    .line 85
    .line 86
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v1

    .line 90
    new-instance v2, Ljava/lang/StringBuilder;

    .line 91
    .line 92
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    .line 94
    .line 95
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    new-instance v1, Ljava/lang/StringBuilder;

    .line 106
    .line 107
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 108
    .line 109
    .line 110
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    return-object v0

    .line 124
    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    .line 125
    .line 126
    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 127
    .line 128
    .line 129
    throw v0
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private final Oo08()Ljava/lang/String;
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/app/AppUtil;->O〇8O8〇008()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    const-string v1, "content"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    new-instance v1, Lkotlin/text/Regex;

    .line 17
    .line 18
    const-string v2, "/"

    .line 19
    .line 20
    invoke-direct {v1, v2}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/4 v3, 0x0

    .line 24
    invoke-virtual {v1, v0, v3}, Lkotlin/text/Regex;->split(Ljava/lang/CharSequence;I)Ljava/util/List;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    check-cast v0, Ljava/util/Collection;

    .line 29
    .line 30
    new-array v1, v3, [Ljava/lang/String;

    .line 31
    .line 32
    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    check-cast v0, [Ljava/lang/String;

    .line 37
    .line 38
    array-length v1, v0

    .line 39
    const/4 v4, 0x2

    .line 40
    if-ne v1, v4, :cond_0

    .line 41
    .line 42
    aget-object v1, v0, v3

    .line 43
    .line 44
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    const/4 v3, 0x1

    .line 49
    aget-object v0, v0, v3

    .line 50
    .line 51
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    new-instance v3, Ljava/lang/StringBuilder;

    .line 56
    .line 57
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    .line 59
    .line 60
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    goto :goto_0

    .line 74
    :cond_0
    const-string v0, "--"

    .line 75
    .line 76
    :goto_0
    return-object v0
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;JLjava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇o〇(Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;JLjava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private final 〇O〇()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->〇80〇808〇O()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->〇80〇808〇O()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->O8〇o()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇80()Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-eqz v0, :cond_2

    .line 27
    .line 28
    :cond_1
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->〇0000OOO()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-nez v0, :cond_2

    .line 37
    .line 38
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->〇0000OOO()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    goto :goto_0

    .line 43
    :cond_2
    const/4 v0, 0x0

    .line 44
    :goto_0
    return-object v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private static final 〇o〇(Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;JLjava/lang/String;)V
    .locals 6

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$myAccount"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-wide/16 v0, 0x7

    .line 12
    .line 13
    const-string v2, "\uff09"

    .line 14
    .line 15
    const-string v3, "\uff08"

    .line 16
    .line 17
    const-string v4, "MyAccountRepo"

    .line 18
    .line 19
    cmp-long v5, p2, v0

    .line 20
    .line 21
    if-gez v5, :cond_0

    .line 22
    .line 23
    new-instance p4, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string v0, "user team account will be expired in "

    .line 29
    .line 30
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {p4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string v0, " days"

    .line 37
    .line 38
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object p4

    .line 45
    invoke-static {v4, p4}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    iget-object p0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇080:Landroid/app/Application;

    .line 49
    .line 50
    const/4 p4, 0x1

    .line 51
    new-array p4, p4, [Ljava/lang/Object;

    .line 52
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    .line 54
    .line 55
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object p2

    .line 65
    const/4 p3, 0x0

    .line 66
    aput-object p2, p4, p3

    .line 67
    .line 68
    const p2, 0x7f131c69

    .line 69
    .line 70
    .line 71
    invoke-virtual {p0, p2, p4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object p0

    .line 75
    const-string p2, "app.getString(R.string.c\u2026ion_account_03, \"\" + day)"

    .line 76
    .line 77
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->getSubtitle()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object p2

    .line 84
    new-instance p3, Ljava/lang/StringBuilder;

    .line 85
    .line 86
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    .line 88
    .line 89
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    invoke-virtual {p3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object p0

    .line 105
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    goto :goto_0

    .line 109
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    .line 110
    .line 111
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    .line 113
    .line 114
    const-string p3, "user team account expire time:"

    .line 115
    .line 116
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object p2

    .line 126
    invoke-static {v4, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    const p2, 0x7f131c68

    .line 130
    .line 131
    .line 132
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 133
    .line 134
    .line 135
    move-result-object p0

    .line 136
    new-instance p2, Ljava/lang/StringBuilder;

    .line 137
    .line 138
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    .line 140
    .line 141
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    .line 143
    .line 144
    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    .line 146
    .line 147
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 148
    .line 149
    .line 150
    move-result-object p0

    .line 151
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->getSubtitle()Ljava/lang/String;

    .line 152
    .line 153
    .line 154
    move-result-object p2

    .line 155
    new-instance p3, Ljava/lang/StringBuilder;

    .line 156
    .line 157
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 158
    .line 159
    .line 160
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    .line 162
    .line 163
    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    .line 165
    .line 166
    invoke-virtual {p3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    .line 168
    .line 169
    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    .line 171
    .line 172
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object p0

    .line 176
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 177
    .line 178
    .line 179
    :goto_0
    return-void
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private final 〇〇8O0〇8(I)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇080:Landroid/app/Application;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const-string v0, "app.getString(strId)"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-object p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public final OO0o〇〇(Z)Lcom/intsig/camscanner/settings/newsettings/entity/SettingLogInOrOut;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingLogInOrOut;

    .line 2
    .line 3
    const/4 v1, 0x3

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingLogInOrOut;-><init>(I)V

    .line 5
    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    if-ne p1, v1, :cond_0

    .line 9
    .line 10
    const/16 p1, 0xa

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    if-nez p1, :cond_1

    .line 14
    .line 15
    const/16 p1, 0xb

    .line 16
    .line 17
    :goto_0
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingLogInOrOut;->setItemType(I)V

    .line 18
    .line 19
    .line 20
    return-object v0

    .line 21
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    .line 22
    .line 23
    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 24
    .line 25
    .line 26
    throw p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public final OoO8()Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇080:Landroid/app/Application;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/CommonUtil;->O8(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 11
    .line 12
    const/4 v2, 0x2

    .line 13
    invoke-direct {v0, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;-><init>(I)V

    .line 14
    .line 15
    .line 16
    const/16 v2, 0x9

    .line 17
    .line 18
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setItemType(I)V

    .line 19
    .line 20
    .line 21
    const v2, 0x7f13040f

    .line 22
    .line 23
    .line 24
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitle(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    const-string v2, ""

    .line 32
    .line 33
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitle(Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    new-instance v2, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear$Label;

    .line 40
    .line 41
    invoke-direct {v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear$Label;-><init>()V

    .line 42
    .line 43
    .line 44
    const v3, 0x7f0805f5

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear$Label;->〇O8o08O(I)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitleLabel(Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear$Label;)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setBottomDivider(Z)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_0
    if-nez v0, :cond_1

    .line 58
    .line 59
    const/4 v0, 0x0

    .line 60
    :goto_0
    return-object v0

    .line 61
    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    .line 62
    .line 63
    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 64
    .line 65
    .line 66
    throw v0
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public final Oooo8o0〇(Z)Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    new-instance p1, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 5
    .line 6
    const/4 v0, 0x2

    .line 7
    invoke-direct {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;-><init>(I)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x3

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setItemType(I)V

    .line 12
    .line 13
    .line 14
    const v0, 0x7f13018b

    .line 15
    .line 16
    .line 17
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitle(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    const-string v0, ""

    .line 25
    .line 26
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitle(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    if-nez p1, :cond_1

    .line 34
    .line 35
    const/4 p1, 0x0

    .line 36
    :goto_0
    return-object p1

    .line 37
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    .line 38
    .line 39
    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 40
    .line 41
    .line 42
    throw p1
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public final o800o8O(Z)Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->〇8()Z

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/wechat/WeChatApi;->Oo08()Lcom/intsig/wechat/WeChatApi;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-virtual {p1}, Lcom/intsig/wechat/WeChatApi;->〇8o8o〇()Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    if-eqz p1, :cond_0

    .line 19
    .line 20
    invoke-static {}, Lcom/intsig/tsapp/account/exp/UserBindOptExp;->〇080()Z

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    if-nez p1, :cond_0

    .line 25
    .line 26
    const/4 p1, 0x1

    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const/4 p1, 0x0

    .line 29
    :goto_0
    if-ne p1, v0, :cond_4

    .line 30
    .line 31
    new-instance p1, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 32
    .line 33
    const/4 v0, 0x2

    .line 34
    invoke-direct {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;-><init>(I)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setItemType(I)V

    .line 38
    .line 39
    .line 40
    const v0, 0x7f1306a6

    .line 41
    .line 42
    .line 43
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitle(Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    const-string v0, ""

    .line 51
    .line 52
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇80()Z

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    if-eqz v0, :cond_1

    .line 60
    .line 61
    const v0, 0x7f131190

    .line 62
    .line 63
    .line 64
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    goto :goto_1

    .line 69
    :cond_1
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->O8〇o()Z

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    if-eqz v0, :cond_2

    .line 74
    .line 75
    const v0, 0x7f130727

    .line 76
    .line 77
    .line 78
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    goto :goto_1

    .line 83
    :cond_2
    const v0, 0x7f130730

    .line 84
    .line 85
    .line 86
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    :goto_1
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitle(Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇80()Z

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    if-eqz v0, :cond_3

    .line 98
    .line 99
    const v0, 0x7f060205

    .line 100
    .line 101
    .line 102
    goto :goto_2

    .line 103
    :cond_3
    const v0, 0x7f0601ee

    .line 104
    .line 105
    .line 106
    :goto_2
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitleColorRes(I)V

    .line 107
    .line 108
    .line 109
    goto :goto_3

    .line 110
    :cond_4
    if-nez p1, :cond_5

    .line 111
    .line 112
    const/4 p1, 0x0

    .line 113
    :goto_3
    return-object p1

    .line 114
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    .line 115
    .line 116
    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 117
    .line 118
    .line 119
    throw p1
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public final oO80()Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageCsPdfVip;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipManager;->〇〇808〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageCsPdfVip;

    .line 14
    .line 15
    invoke-direct {v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageCsPdfVip;-><init>()V

    .line 16
    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v0, 0x0

    .line 20
    :goto_0
    return-object v0
    .line 21
.end method

.method public final oo88o8O(Lcom/intsig/okgo/callback/CustomStringCallback;)V
    .locals 3
    .param p1    # Lcom/intsig/okgo/callback/CustomStringCallback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .line 1
    const-string v0, "callback"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇080:Landroid/app/Application;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/purchase/CouponManager;->〇o00〇〇Oo(Landroid/content/Context;)Lcom/intsig/tianshu/base/CouponRequest;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v1, "getCouponRequest(app)"

    .line 13
    .line 14
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const-string v1, "CouponManager"

    .line 18
    .line 19
    const-string v2, "query coupon list"

    .line 20
    .line 21
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-static {v0, p1}, Lcom/intsig/tianshu/TianShuAPI;->o〇〇0〇88(Lcom/intsig/tianshu/base/CouponRequest;Lcom/intsig/okgo/callback/CustomStringCallback;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public final o〇0(Z)Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    new-instance p1, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 5
    .line 6
    const/4 v0, 0x2

    .line 7
    invoke-direct {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;-><init>(I)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x5

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setItemType(I)V

    .line 12
    .line 13
    .line 14
    const v0, 0x7f1303b3

    .line 15
    .line 16
    .line 17
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitle(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->Oo08()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    const v0, 0x7f131f7f

    .line 32
    .line 33
    .line 34
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitle(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    const v0, 0x7f0601ee

    .line 42
    .line 43
    .line 44
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitleColorRes(I)V

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    if-nez p1, :cond_1

    .line 49
    .line 50
    const/4 p1, 0x0

    .line 51
    :goto_0
    return-object p1

    .line 52
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    .line 53
    .line 54
    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 55
    .line 56
    .line 57
    throw p1
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public final o〇O8〇〇o()V
    .locals 0
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/https/account/UserPropertyAPI;->〇O〇()Lcom/intsig/camscanner/https/account/UserPropertyAPI$PointsInfo;

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0〇O0088o()Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-nez v0, :cond_1

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->oO80()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    goto :goto_1

    .line 17
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 18
    :goto_1
    if-nez v0, :cond_2

    .line 19
    .line 20
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 21
    .line 22
    const/4 v2, 0x2

    .line 23
    invoke-direct {v0, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;-><init>(I)V

    .line 24
    .line 25
    .line 26
    const/16 v2, 0xc

    .line 27
    .line 28
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setItemType(I)V

    .line 29
    .line 30
    .line 31
    const v2, 0x7f130fd0

    .line 32
    .line 33
    .line 34
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitle(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    const-string v2, ""

    .line 42
    .line 43
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitle(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setBottomDivider(Z)V

    .line 50
    .line 51
    .line 52
    goto :goto_2

    .line 53
    :cond_2
    if-ne v0, v1, :cond_3

    .line 54
    .line 55
    const/4 v0, 0x0

    .line 56
    :goto_2
    return-object v0

    .line 57
    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    .line 58
    .line 59
    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 60
    .line 61
    .line 62
    throw v0
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public final 〇80〇808〇O()Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;-><init>(I)V

    .line 5
    .line 6
    .line 7
    const/16 v1, 0x8

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setItemType(I)V

    .line 10
    .line 11
    .line 12
    const v1, 0x7f131bd4

    .line 13
    .line 14
    .line 15
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitle(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/util/CnUnsubscribeScaffoldConfig;->〇080:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/util/CnUnsubscribeScaffoldConfig;

    .line 23
    .line 24
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/util/CnUnsubscribeScaffoldConfig;->〇o00〇〇Oo()Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-eqz v1, :cond_0

    .line 29
    .line 30
    const/4 v1, 0x1

    .line 31
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitleShowRedDot(Z)V

    .line 32
    .line 33
    .line 34
    :cond_0
    return-object v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public final 〇8o8o〇(I)Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;-><init>(I)V

    .line 5
    .line 6
    .line 7
    const/4 v1, 0x7

    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setItemType(I)V

    .line 9
    .line 10
    .line 11
    const v1, 0x7f131c6a

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitle(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇080:Landroid/app/Application;

    .line 22
    .line 23
    const/4 v2, 0x1

    .line 24
    new-array v2, v2, [Ljava/lang/Object;

    .line 25
    .line 26
    const/4 v3, 0x0

    .line 27
    if-gez p1, :cond_0

    .line 28
    .line 29
    const/4 p1, 0x0

    .line 30
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    aput-object p1, v2, v3

    .line 35
    .line 36
    const p1, 0x7f130311

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    const-string v1, "app.getString(R.string.a\u2026ance < 0) 0 else balance)"

    .line 44
    .line 45
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    const-string p1, ""

    .line 52
    .line 53
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitle(Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 57
    .line 58
    .line 59
    move-result p1

    .line 60
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRootClickable(Z)V

    .line 61
    .line 62
    .line 63
    return-object v0
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public final 〇O00()Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;-><init>(I)V

    .line 5
    .line 6
    .line 7
    const/4 v1, 0x6

    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setItemType(I)V

    .line 9
    .line 10
    .line 11
    const v1, 0x7f131c86

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitle(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0o〇()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    iget-object v2, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇080:Landroid/app/Application;

    .line 26
    .line 27
    const/4 v3, 0x1

    .line 28
    new-array v3, v3, [Ljava/lang/Object;

    .line 29
    .line 30
    const/4 v4, 0x0

    .line 31
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    aput-object v1, v3, v4

    .line 36
    .line 37
    const v1, 0x7f13031f

    .line 38
    .line 39
    .line 40
    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    const-string v2, "app.getString(R.string.a\u2026points_number, leftPoint)"

    .line 45
    .line 46
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->〇O〇()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    const-string v2, "getPointDesc()"

    .line 57
    .line 58
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitle(Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    const v1, 0x7f0601ee

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitleColorRes(I)V

    .line 68
    .line 69
    .line 70
    return-object v0
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public final 〇O888o0o()V
    .locals 0
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇08〇0〇o〇8()[J

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O8o08O()Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/advertisement/control/AdConfigManager;->〇〇808〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->O〇8O8〇008()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;-><init>(I)V

    .line 17
    .line 18
    .line 19
    const/16 v1, 0xc

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setItemType(I)V

    .line 22
    .line 23
    .line 24
    const v1, 0x7f131223

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setTitleRes(I)V

    .line 28
    .line 29
    .line 30
    const/4 v1, 0x0

    .line 31
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageLinear;->setShowArrow(Z)V

    .line 32
    .line 33
    .line 34
    return-object v0

    .line 35
    :cond_0
    const/4 v0, 0x0

    .line 36
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public final 〇o00〇〇Oo()Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;-><init>(I)V

    .line 5
    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setItemType(I)V

    .line 9
    .line 10
    .line 11
    const v2, 0x7f13036e

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitle(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const v2, 0x7f0601ee

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitleColorRes(I)V

    .line 25
    .line 26
    .line 27
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OoOOo8()Z

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    if-eqz v2, :cond_0

    .line 32
    .line 33
    const v1, 0x7f131ed6

    .line 34
    .line 35
    .line 36
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇080:Landroid/app/Application;

    .line 44
    .line 45
    new-instance v2, LO888Oo/〇080;

    .line 46
    .line 47
    invoke-direct {v2, p0, v0}, LO888Oo/〇080;-><init>(Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;)V

    .line 48
    .line 49
    .line 50
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/TeamUtil;->〇080(Landroid/content/Context;Lcom/intsig/camscanner/util/TeamUtil$TeamExpireListener;)V

    .line 51
    .line 52
    .line 53
    goto/16 :goto_2

    .line 54
    .line 55
    :cond_0
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->o0ooO()Ljava/lang/Boolean;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    const-string v3, "isCropAccount()"

    .line 60
    .line 61
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 65
    .line 66
    .line 67
    move-result v2

    .line 68
    if-eqz v2, :cond_1

    .line 69
    .line 70
    const v1, 0x7f131217

    .line 71
    .line 72
    .line 73
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    goto/16 :goto_2

    .line 81
    .line 82
    :cond_1
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->〇8〇0〇o〇O()Z

    .line 83
    .line 84
    .line 85
    move-result v2

    .line 86
    if-eqz v2, :cond_2

    .line 87
    .line 88
    const v1, 0x7f131c7a

    .line 89
    .line 90
    .line 91
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v1

    .line 95
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    const-string v1, ""

    .line 99
    .line 100
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitle(Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    goto/16 :goto_2

    .line 104
    .line 105
    :cond_2
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->o8oO〇()Z

    .line 106
    .line 107
    .line 108
    move-result v2

    .line 109
    const v3, 0x7f13019e

    .line 110
    .line 111
    .line 112
    if-eqz v2, :cond_5

    .line 113
    .line 114
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->O08000()Z

    .line 115
    .line 116
    .line 117
    move-result v2

    .line 118
    if-ne v2, v1, :cond_3

    .line 119
    .line 120
    const v1, 0x7f131c51

    .line 121
    .line 122
    .line 123
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 124
    .line 125
    .line 126
    move-result-object v1

    .line 127
    goto :goto_0

    .line 128
    :cond_3
    if-nez v2, :cond_4

    .line 129
    .line 130
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 131
    .line 132
    .line 133
    move-result-object v1

    .line 134
    :goto_0
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitle(Ljava/lang/String;)V

    .line 135
    .line 136
    .line 137
    const v1, 0x7f1303d8

    .line 138
    .line 139
    .line 140
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object v1

    .line 144
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 145
    .line 146
    .line 147
    invoke-virtual {v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->getSubtitle()Ljava/lang/String;

    .line 148
    .line 149
    .line 150
    move-result-object v1

    .line 151
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 152
    .line 153
    .line 154
    move-result-object v2

    .line 155
    new-instance v3, Ljava/lang/StringBuilder;

    .line 156
    .line 157
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 158
    .line 159
    .line 160
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    .line 162
    .line 163
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    .line 165
    .line 166
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 167
    .line 168
    .line 169
    move-result-object v1

    .line 170
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 171
    .line 172
    .line 173
    goto :goto_2

    .line 174
    :cond_4
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    .line 175
    .line 176
    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 177
    .line 178
    .line 179
    throw v0

    .line 180
    :cond_5
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->Oo8Oo00oo()Z

    .line 181
    .line 182
    .line 183
    move-result v2

    .line 184
    if-eqz v2, :cond_6

    .line 185
    .line 186
    const v1, 0x7f131226

    .line 187
    .line 188
    .line 189
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 190
    .line 191
    .line 192
    move-result-object v1

    .line 193
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 194
    .line 195
    .line 196
    move-result-object v2

    .line 197
    new-instance v4, Ljava/lang/StringBuilder;

    .line 198
    .line 199
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 200
    .line 201
    .line 202
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    .line 204
    .line 205
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    .line 207
    .line 208
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 209
    .line 210
    .line 211
    move-result-object v1

    .line 212
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 213
    .line 214
    .line 215
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 216
    .line 217
    .line 218
    move-result-object v1

    .line 219
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitle(Ljava/lang/String;)V

    .line 220
    .line 221
    .line 222
    goto :goto_2

    .line 223
    :cond_6
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->oo〇()Z

    .line 224
    .line 225
    .line 226
    move-result v2

    .line 227
    if-ne v2, v1, :cond_7

    .line 228
    .line 229
    const v1, 0x7f1308a2

    .line 230
    .line 231
    .line 232
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 233
    .line 234
    .line 235
    move-result-object v1

    .line 236
    goto :goto_1

    .line 237
    :cond_7
    if-nez v2, :cond_8

    .line 238
    .line 239
    const v1, 0x7f130356

    .line 240
    .line 241
    .line 242
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 243
    .line 244
    .line 245
    move-result-object v1

    .line 246
    :goto_1
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 247
    .line 248
    .line 249
    const v1, 0x7f131c65

    .line 250
    .line 251
    .line 252
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 253
    .line 254
    .line 255
    move-result-object v1

    .line 256
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitle(Ljava/lang/String;)V

    .line 257
    .line 258
    .line 259
    const v1, 0x7f0601d7

    .line 260
    .line 261
    .line 262
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitleColorRes(I)V

    .line 263
    .line 264
    .line 265
    :goto_2
    return-object v0

    .line 266
    :cond_8
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    .line 267
    .line 268
    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 269
    .line 270
    .line 271
    throw v0
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public final 〇oo〇()I
    .locals 1
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/https/account/UserPropertyAPI;->〇〇888()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇808〇(Z)Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;
    .locals 11
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇O〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 6
    .line 7
    const/4 v2, 0x2

    .line 8
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;-><init>(I)V

    .line 9
    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setItemType(I)V

    .line 13
    .line 14
    .line 15
    const v3, 0x7f0601ee

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitleColorRes(I)V

    .line 19
    .line 20
    .line 21
    const/4 v4, 0x1

    .line 22
    if-ne p1, v4, :cond_7

    .line 23
    .line 24
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    if-ne p1, v4, :cond_0

    .line 29
    .line 30
    const p1, 0x7f13067e

    .line 31
    .line 32
    .line 33
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    .line 38
    .line 39
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    .line 41
    .line 42
    const-string v3, "\uff08"

    .line 43
    .line 44
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    const-string p1, "\uff09"

    .line 51
    .line 52
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitle(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    const p1, 0x7f060206

    .line 63
    .line 64
    .line 65
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitleColorRes(I)V

    .line 66
    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_0
    if-nez p1, :cond_1

    .line 70
    .line 71
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitle(Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    const p1, 0x7f060208

    .line 78
    .line 79
    .line 80
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitleColorRes(I)V

    .line 81
    .line 82
    .line 83
    :cond_1
    :goto_0
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇()Z

    .line 84
    .line 85
    .line 86
    move-result p1

    .line 87
    const/4 v0, 0x0

    .line 88
    if-ne p1, v4, :cond_2

    .line 89
    .line 90
    invoke-static {v0, v4, v0}, Lcom/intsig/comm/account_data/AccountHelper;->〇o00〇〇Oo(Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    goto :goto_1

    .line 95
    :cond_2
    if-nez p1, :cond_6

    .line 96
    .line 97
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇80()Z

    .line 98
    .line 99
    .line 100
    move-result p1

    .line 101
    if-eqz p1, :cond_3

    .line 102
    .line 103
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->OoO8()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v5

    .line 107
    const-string p1, "getSyncAccount()"

    .line 108
    .line 109
    invoke-static {v5, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    const-string p1, "@"

    .line 113
    .line 114
    filled-new-array {p1}, [Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v6

    .line 118
    const/4 v7, 0x0

    .line 119
    const/4 v8, 0x0

    .line 120
    const/4 v9, 0x6

    .line 121
    const/4 v10, 0x0

    .line 122
    invoke-static/range {v5 .. v10}, Lkotlin/text/StringsKt;->Oo(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    .line 123
    .line 124
    .line 125
    move-result-object p1

    .line 126
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 127
    .line 128
    .line 129
    move-result-object p1

    .line 130
    check-cast p1, Ljava/lang/String;

    .line 131
    .line 132
    goto :goto_1

    .line 133
    :cond_3
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->o0ooO()Z

    .line 134
    .line 135
    .line 136
    move-result p1

    .line 137
    if-eqz p1, :cond_4

    .line 138
    .line 139
    const-string p1, ""

    .line 140
    .line 141
    goto :goto_1

    .line 142
    :cond_4
    invoke-static {v0, v4, v0}, Lcom/intsig/comm/account_data/AccountHelper;->〇o00〇〇Oo(Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object p1

    .line 146
    :goto_1
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 147
    .line 148
    .line 149
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇80()Z

    .line 150
    .line 151
    .line 152
    move-result p1

    .line 153
    if-eqz p1, :cond_5

    .line 154
    .line 155
    const p1, 0x7f131187

    .line 156
    .line 157
    .line 158
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 159
    .line 160
    .line 161
    move-result-object p1

    .line 162
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitle(Ljava/lang/String;)V

    .line 163
    .line 164
    .line 165
    goto :goto_2

    .line 166
    :cond_5
    const p1, 0x7f131d1f

    .line 167
    .line 168
    .line 169
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 170
    .line 171
    .line 172
    move-result-object p1

    .line 173
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitle(Ljava/lang/String;)V

    .line 174
    .line 175
    .line 176
    :goto_2
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->o0ooO()Z

    .line 177
    .line 178
    .line 179
    move-result p1

    .line 180
    if-eqz p1, :cond_8

    .line 181
    .line 182
    const p1, 0x7f080984

    .line 183
    .line 184
    .line 185
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitleIcon(I)V

    .line 186
    .line 187
    .line 188
    goto :goto_3

    .line 189
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    .line 190
    .line 191
    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 192
    .line 193
    .line 194
    throw p1

    .line 195
    :cond_7
    if-nez p1, :cond_8

    .line 196
    .line 197
    const p1, 0x7f130813

    .line 198
    .line 199
    .line 200
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 201
    .line 202
    .line 203
    move-result-object p1

    .line 204
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitle(Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    const p1, 0x7f0600ef

    .line 208
    .line 209
    .line 210
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitleColorRes(I)V

    .line 211
    .line 212
    .line 213
    const p1, 0x7f131199

    .line 214
    .line 215
    .line 216
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 217
    .line 218
    .line 219
    move-result-object p1

    .line 220
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 221
    .line 222
    .line 223
    const p1, 0x7f130175

    .line 224
    .line 225
    .line 226
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 227
    .line 228
    .line 229
    move-result-object p1

    .line 230
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitle(Ljava/lang/String;)V

    .line 231
    .line 232
    .line 233
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitleColorRes(I)V

    .line 234
    .line 235
    .line 236
    :cond_8
    :goto_3
    return-object v1
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public final 〇〇888(Z)Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇080:Landroid/app/Application;

    .line 5
    .line 6
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/account/util/AccountUtil;->Oooo8o0〇(Landroid/content/Context;)Z

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    const/4 p1, 0x1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 p1, 0x0

    .line 15
    :goto_0
    if-ne p1, v0, :cond_1

    .line 16
    .line 17
    new-instance p1, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;

    .line 18
    .line 19
    const/4 v1, 0x2

    .line 20
    invoke-direct {p1, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;-><init>(I)V

    .line 21
    .line 22
    .line 23
    const/4 v1, 0x4

    .line 24
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setItemType(I)V

    .line 25
    .line 26
    .line 27
    const v1, 0x7f130d9b

    .line 28
    .line 29
    .line 30
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitle(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    const-string v1, ""

    .line 38
    .line 39
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setSubtitle(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setRightTitle(Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇〇〇o0〇O()Z

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    if-eqz v1, :cond_2

    .line 50
    .line 51
    new-instance v1, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear$Label;

    .line 52
    .line 53
    invoke-direct {v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear$Label;-><init>()V

    .line 54
    .line 55
    .line 56
    const v2, 0x7f130d9c

    .line 57
    .line 58
    .line 59
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/settings/newsettings/viewmodel/repo/MyAccountRepo;->〇〇8O0〇8(I)Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear$Label;->Oooo8o0〇(Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    const v2, 0x7f060204

    .line 67
    .line 68
    .line 69
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear$Label;->〇8o8o〇(I)V

    .line 70
    .line 71
    .line 72
    const/4 v2, 0x6

    .line 73
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear$Label;->〇O00(I)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear$Label;->〇O〇(I)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear$Label;->〇〇8O0〇8(I)V

    .line 80
    .line 81
    .line 82
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear$Label;->〇〇808〇(I)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear$Label;->OO0o〇〇(Z)V

    .line 86
    .line 87
    .line 88
    new-instance v0, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 89
    .line 90
    invoke-direct {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 91
    .line 92
    .line 93
    const-string v2, "#FD765A"

    .line 94
    .line 95
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 96
    .line 97
    .line 98
    move-result v2

    .line 99
    invoke-virtual {v0, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 100
    .line 101
    .line 102
    const-string v2, "#FF5860"

    .line 103
    .line 104
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 105
    .line 106
    .line 107
    move-result v2

    .line 108
    invoke-virtual {v0, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇oo〇(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 109
    .line 110
    .line 111
    const/high16 v2, 0x41000000    # 8.0f

    .line 112
    .line 113
    invoke-virtual {v0, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇oOO8O8(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v0, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇0000OOO(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {v0, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇〇8O0〇8(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 120
    .line 121
    .line 122
    invoke-virtual {v0, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇0〇O0088o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 123
    .line 124
    .line 125
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear$Label;->OO0o〇〇〇〇0(Lcom/intsig/utils/GradientDrawableBuilder$Builder;)V

    .line 126
    .line 127
    .line 128
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear;->setTitleLabel(Lcom/intsig/camscanner/settings/newsettings/entity/SettingPageRightTxtLinear$Label;)V

    .line 129
    .line 130
    .line 131
    goto :goto_1

    .line 132
    :cond_1
    if-nez p1, :cond_3

    .line 133
    .line 134
    const/4 p1, 0x0

    .line 135
    :cond_2
    :goto_1
    return-object p1

    .line 136
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    .line 137
    .line 138
    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 139
    .line 140
    .line 141
    throw p1
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

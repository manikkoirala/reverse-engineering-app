.class public final Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "WorkFlowEmailAddOrEditProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider$EmailAddOrEditHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/camscanner/settings/workflow/data/BaseWorkFlowEmailItem;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8o08O8O:I

.field private final o〇00O:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 2
    .line 3
    .line 4
    const v0, 0x7f0d04ed

    .line 5
    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider;->O8o08O8O:I

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string p2, "parent"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p2, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider$EmailAddOrEditHolder;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider;->oO80()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-static {p1, v0}, Lcom/chad/library/adapter/base/util/AdapterUtilsKt;->〇080(Landroid/view/ViewGroup;I)Landroid/view/View;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider$EmailAddOrEditHolder;-><init>(Landroid/view/View;)V

    .line 17
    .line 18
    .line 19
    return-object p2
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/settings/workflow/data/BaseWorkFlowEmailItem;)V
    .locals 8
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/settings/workflow/data/BaseWorkFlowEmailItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    check-cast p1, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider$EmailAddOrEditHolder;

    .line 12
    .line 13
    check-cast p2, Lcom/intsig/camscanner/settings/workflow/data/WorkFlowEmailAddOrEditItem;

    .line 14
    .line 15
    invoke-virtual {p2}, Lcom/intsig/camscanner/settings/workflow/data/WorkFlowEmailAddOrEditItem;->〇o〇()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    const-string v1, "helper.mBinding.tvWorkflowDone"

    .line 20
    .line 21
    const-string v2, "helper.mBinding.tvWorkflowEdit"

    .line 22
    .line 23
    const-string v3, "helper.mBinding.tvWorkflowAddEmail"

    .line 24
    .line 25
    const/4 v4, 0x0

    .line 26
    const/16 v5, 0x8

    .line 27
    .line 28
    const/4 v6, 0x1

    .line 29
    if-ne v0, v6, :cond_0

    .line 30
    .line 31
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider$EmailAddOrEditHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemWorkFlowAutoSendEmailContentFooterBinding;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemWorkFlowAutoSendEmailContentFooterBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 36
    .line 37
    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider$EmailAddOrEditHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemWorkFlowAutoSendEmailContentFooterBinding;

    .line 44
    .line 45
    .line 46
    move-result-object p2

    .line 47
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemWorkFlowAutoSendEmailContentFooterBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 48
    .line 49
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider$EmailAddOrEditHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemWorkFlowAutoSendEmailContentFooterBinding;

    .line 56
    .line 57
    .line 58
    move-result-object p2

    .line 59
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemWorkFlowAutoSendEmailContentFooterBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 60
    .line 61
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {p2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 65
    .line 66
    .line 67
    goto :goto_4

    .line 68
    :cond_0
    if-nez v0, :cond_5

    .line 69
    .line 70
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider$EmailAddOrEditHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemWorkFlowAutoSendEmailContentFooterBinding;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemWorkFlowAutoSendEmailContentFooterBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 75
    .line 76
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {p2}, Lcom/intsig/camscanner/settings/workflow/data/WorkFlowEmailAddOrEditItem;->〇o00〇〇Oo()I

    .line 80
    .line 81
    .line 82
    move-result v3

    .line 83
    const/4 v7, 0x3

    .line 84
    if-ge v3, v7, :cond_1

    .line 85
    .line 86
    const/4 v3, 0x1

    .line 87
    goto :goto_0

    .line 88
    :cond_1
    const/4 v3, 0x0

    .line 89
    :goto_0
    if-eqz v3, :cond_2

    .line 90
    .line 91
    const/4 v3, 0x0

    .line 92
    goto :goto_1

    .line 93
    :cond_2
    const/16 v3, 0x8

    .line 94
    .line 95
    :goto_1
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider$EmailAddOrEditHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemWorkFlowAutoSendEmailContentFooterBinding;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemWorkFlowAutoSendEmailContentFooterBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 103
    .line 104
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    invoke-virtual {p2}, Lcom/intsig/camscanner/settings/workflow/data/WorkFlowEmailAddOrEditItem;->〇o00〇〇Oo()I

    .line 108
    .line 109
    .line 110
    move-result p2

    .line 111
    if-lez p2, :cond_3

    .line 112
    .line 113
    goto :goto_2

    .line 114
    :cond_3
    const/4 v6, 0x0

    .line 115
    :goto_2
    if-eqz v6, :cond_4

    .line 116
    .line 117
    goto :goto_3

    .line 118
    :cond_4
    const/16 v4, 0x8

    .line 119
    .line 120
    :goto_3
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 121
    .line 122
    .line 123
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider$EmailAddOrEditHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemWorkFlowAutoSendEmailContentFooterBinding;

    .line 124
    .line 125
    .line 126
    move-result-object p2

    .line 127
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemWorkFlowAutoSendEmailContentFooterBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 128
    .line 129
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    invoke-virtual {p2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 133
    .line 134
    .line 135
    :cond_5
    :goto_4
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider$EmailAddOrEditHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemWorkFlowAutoSendEmailContentFooterBinding;

    .line 136
    .line 137
    .line 138
    move-result-object p1

    .line 139
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemWorkFlowAutoSendEmailContentFooterBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 140
    .line 141
    const p2, 0x7f131a13

    .line 142
    .line 143
    .line 144
    invoke-static {p2}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object p2

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    .line 149
    .line 150
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 151
    .line 152
    .line 153
    const-string v1, "+ "

    .line 154
    .line 155
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    .line 157
    .line 158
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    .line 160
    .line 161
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object p2

    .line 165
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    .line 167
    .line 168
    return-void
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public oO80()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider;->O8o08O8O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/settings/workflow/data/BaseWorkFlowEmailItem;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider;->o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/settings/workflow/data/BaseWorkFlowEmailItem;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowEmailAddOrEditProvider;->o〇00O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowNetDiskAdapter;
.super Lcom/chad/library/adapter/base/BaseQuickAdapter;
.source "WorkFlowNetDiskAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowNetDiskAdapter$NetDiskLinearProvider;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/BaseQuickAdapter<",
        "Lcom/intsig/camscanner/settings/workflow/data/NetDiscConfigData;",
        "Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowNetDiskAdapter$NetDiskLinearProvider;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/settings/workflow/data/NetDiscConfigData;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const v0, 0x7f0d04f3

    .line 7
    .line 8
    .line 9
    invoke-direct {p0, v0, p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;-><init>(ILjava/util/List;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method protected O0OO8〇0(Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowNetDiskAdapter$NetDiskLinearProvider;Lcom/intsig/camscanner/settings/workflow/data/NetDiscConfigData;)V
    .locals 3
    .param p1    # Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowNetDiskAdapter$NetDiskLinearProvider;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/settings/workflow/data/NetDiscConfigData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "holder"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowNetDiskAdapter$NetDiskLinearProvider;->〇00()Lcom/intsig/camscanner/databinding/ItemWorkFlowThirdNetDiskItemBinding;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemWorkFlowThirdNetDiskItemBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 16
    .line 17
    invoke-virtual {p2}, Lcom/intsig/camscanner/settings/workflow/data/NetDiscConfigData;->〇080()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowNetDiskAdapter$NetDiskLinearProvider;->〇00()Lcom/intsig/camscanner/databinding/ItemWorkFlowThirdNetDiskItemBinding;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemWorkFlowThirdNetDiskItemBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 29
    .line 30
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 31
    .line 32
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-virtual {p2}, Lcom/intsig/camscanner/settings/workflow/data/NetDiscConfigData;->〇o00〇〇Oo()I

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowNetDiskAdapter$NetDiskLinearProvider;->〇00()Lcom/intsig/camscanner/databinding/ItemWorkFlowThirdNetDiskItemBinding;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemWorkFlowThirdNetDiskItemBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 52
    .line 53
    invoke-virtual {p2}, Lcom/intsig/camscanner/settings/workflow/data/NetDiscConfigData;->〇o〇()Z

    .line 54
    .line 55
    .line 56
    move-result v1

    .line 57
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p0, p2}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->oO(Ljava/lang/Object;)I

    .line 61
    .line 62
    .line 63
    move-result p2

    .line 64
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->〇00〇8()I

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    const/4 v1, 0x0

    .line 69
    const/4 v2, 0x1

    .line 70
    if-ltz p2, :cond_0

    .line 71
    .line 72
    sub-int/2addr v0, v2

    .line 73
    if-ne p2, v0, :cond_0

    .line 74
    .line 75
    const/4 p2, 0x1

    .line 76
    goto :goto_0

    .line 77
    :cond_0
    const/4 p2, 0x0

    .line 78
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowNetDiskAdapter$NetDiskLinearProvider;->〇00()Lcom/intsig/camscanner/databinding/ItemWorkFlowThirdNetDiskItemBinding;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemWorkFlowThirdNetDiskItemBinding;->o〇00O:Landroid/view/View;

    .line 83
    .line 84
    const-string v0, "holder.binding.vThirdNetDiskBottomLine"

    .line 85
    .line 86
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    xor-int/2addr p2, v2

    .line 90
    if-eqz p2, :cond_1

    .line 91
    .line 92
    goto :goto_1

    .line 93
    :cond_1
    const/16 v1, 0x8

    .line 94
    .line 95
    :goto_1
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 96
    .line 97
    .line 98
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public bridge synthetic O〇8O8〇008(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowNetDiskAdapter$NetDiskLinearProvider;

    .line 2
    .line 3
    check-cast p2, Lcom/intsig/camscanner/settings/workflow/data/NetDiscConfigData;

    .line 4
    .line 5
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowNetDiskAdapter;->O0OO8〇0(Lcom/intsig/camscanner/settings/workflow/adapter/WorkFlowNetDiskAdapter$NetDiskLinearProvider;Lcom/intsig/camscanner/settings/workflow/data/NetDiscConfigData;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

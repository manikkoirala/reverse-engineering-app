.class public final Lcom/intsig/camscanner/settings/PdfImportTypeFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "PdfImportTypeFragment.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic 〇OOo8〇0:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/settings/PdfImportTypeFragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/settings/PdfImportTypeFragment;->〇OOo8〇0:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/settings/PdfImportTypeFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private final O0〇0()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇80〇808〇O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/PdfImportTypeFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const/4 v2, 0x0

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;->OO:Lcom/intsig/camscanner/view/SelectItemView;

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    move-object v1, v2

    .line 16
    :goto_0
    if-nez v1, :cond_1

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_1
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/view/SelectItemView;->setMSelect(Z)V

    .line 20
    .line 21
    .line 22
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/PdfImportTypeFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    if-eqz v1, :cond_2

    .line 27
    .line 28
    iget-object v2, v1, Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;->〇OOo8〇0:Lcom/intsig/camscanner/view/SelectItemView;

    .line 29
    .line 30
    :cond_2
    if-nez v2, :cond_3

    .line 31
    .line 32
    goto :goto_2

    .line 33
    :cond_3
    xor-int/lit8 v0, v0, 0x1

    .line 34
    .line 35
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/view/SelectItemView;->setMSelect(Z)V

    .line 36
    .line 37
    .line 38
    :goto_2
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private static final o00〇88〇08(Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p1, "$binding"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "type"

    .line 7
    .line 8
    const-string v0, "pdf"

    .line 9
    .line 10
    const-string v1, "CSDefaultPdfImportFormat"

    .line 11
    .line 12
    const-string v2, "format_select"

    .line 13
    .line 14
    invoke-static {v1, v2, p1, v0}, Lcom/intsig/log/LogAgentHelper;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    sget-object p1, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;

    .line 18
    .line 19
    const/4 v0, 0x0

    .line 20
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇〇0o(Z)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;->OO:Lcom/intsig/camscanner/view/SelectItemView;

    .line 24
    .line 25
    const/4 v1, 0x1

    .line 26
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/view/SelectItemView;->setMSelect(Z)V

    .line 27
    .line 28
    .line 29
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;->〇OOo8〇0:Lcom/intsig/camscanner/view/SelectItemView;

    .line 30
    .line 31
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/view/SelectItemView;->setMSelect(Z)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final o880()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/PdfImportTypeFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;->OO:Lcom/intsig/camscanner/view/SelectItemView;

    .line 9
    .line 10
    new-instance v2, Looooo0O/o〇8oOO88;

    .line 11
    .line 12
    invoke-direct {v2, v0}, Looooo0O/o〇8oOO88;-><init>(Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 16
    .line 17
    .line 18
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;->〇OOo8〇0:Lcom/intsig/camscanner/view/SelectItemView;

    .line 19
    .line 20
    new-instance v2, Looooo0O/o〇O;

    .line 21
    .line 22
    invoke-direct {v2, v0}, Looooo0O/o〇O;-><init>(Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/settings/PdfImportTypeFragment;->o00〇88〇08(Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private final oooO888()Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/settings/PdfImportTypeFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/settings/PdfImportTypeFragment;->〇OOo8〇0:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/settings/PdfImportTypeFragment;->〇〇o0〇8(Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static final 〇〇o0〇8(Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p1, "$binding"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "type"

    .line 7
    .line 8
    const-string v0, "convert_to_pic"

    .line 9
    .line 10
    const-string v1, "CSDefaultPdfImportFormat"

    .line 11
    .line 12
    const-string v2, "format_select"

    .line 13
    .line 14
    invoke-static {v1, v2, p1, v0}, Lcom/intsig/log/LogAgentHelper;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    sget-object p1, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇〇0o(Z)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;->〇OOo8〇0:Lcom/intsig/camscanner/view/SelectItemView;

    .line 24
    .line 25
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/SelectItemView;->setMSelect(Z)V

    .line 26
    .line 27
    .line 28
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/FragmentPdfImportTypeSetBinding;->OO:Lcom/intsig/camscanner/view/SelectItemView;

    .line 29
    .line 30
    const/4 p1, 0x0

    .line 31
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/SelectItemView;->setMSelect(Z)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public initialize(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    const v0, 0x7f131b77

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {p1, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 11
    .line 12
    .line 13
    const-string p1, "CSDefaultPdfImportFormat"

    .line 14
    .line 15
    invoke-static {p1}, Lcom/intsig/log/LogAgentHelper;->〇0000OOO(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/PdfImportTypeFragment;->O0〇0()V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/settings/PdfImportTypeFragment;->o880()V

    .line 22
    .line 23
    .line 24
    return-void
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d031e

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

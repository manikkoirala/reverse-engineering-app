.class public Lcom/intsig/camscanner/watermark/WaterMarkView;
.super Ljava/lang/Object;
.source "WaterMarkView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/watermark/WaterMarkView$Mode;,
        Lcom/intsig/camscanner/watermark/WaterMarkView$AlignModeV;
    }
.end annotation


# instance fields
.field private final O8:Lcom/intsig/camscanner/watermark/TextInterface;

.field private final OO0o〇〇:[F

.field private OO0o〇〇〇〇0:Landroid/graphics/Paint;

.field private final Oo08:Lcom/intsig/camscanner/watermark/EditableInterface;

.field private OoO8:I

.field private Oooo8o0〇:Lcom/intsig/camscanner/watermark/WaterMarkView$AlignModeV;

.field private o800o8O:F

.field private oO80:Landroid/graphics/Matrix;

.field private oo88o8O:F

.field private o〇0:Landroid/graphics/RectF;

.field private o〇O8〇〇o:Z

.field private 〇00:I

.field private 〇080:Landroid/graphics/drawable/Drawable;

.field private 〇0〇O0088o:I

.field private 〇80〇808〇O:Landroid/graphics/Matrix;

.field private 〇8o8o〇:Landroid/graphics/Paint;

.field private 〇O00:Z

.field private 〇O888o0o:Z

.field private 〇O8o08O:Landroid/graphics/Path;

.field private 〇O〇:I

.field private 〇o00〇〇Oo:Landroid/graphics/drawable/Drawable;

.field private 〇oo〇:Z

.field private 〇o〇:Landroid/view/View;

.field private 〇〇808〇:I

.field private 〇〇888:Landroid/graphics/RectF;

.field private 〇〇8O0〇8:Lcom/intsig/camscanner/watermark/WaterMarkView$Mode;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/intsig/camscanner/watermark/TextInterface;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x2

    .line 5
    new-array v0, v0, [F

    .line 6
    .line 7
    fill-array-data v0, :array_0

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OO0o〇〇:[F

    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    iput v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇00:I

    .line 14
    .line 15
    iput-boolean v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇O8〇〇o:Z

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    iput v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oo88o8O:F

    .line 19
    .line 20
    const/high16 v1, 0x3f800000    # 1.0f

    .line 21
    .line 22
    iput v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o800o8O:F

    .line 23
    .line 24
    new-instance v1, Landroid/graphics/Matrix;

    .line 25
    .line 26
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 27
    .line 28
    .line 29
    iput-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 30
    .line 31
    const/16 v1, 0xa

    .line 32
    .line 33
    iput v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇0〇O0088o:I

    .line 34
    .line 35
    const/16 v1, 0x1e

    .line 36
    .line 37
    iput v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OoO8:I

    .line 38
    .line 39
    iput-boolean v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇oo〇:Z

    .line 40
    .line 41
    sget-object v0, Lcom/intsig/camscanner/watermark/WaterMarkView$AlignModeV;->Center:Lcom/intsig/camscanner/watermark/WaterMarkView$AlignModeV;

    .line 42
    .line 43
    iput-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->Oooo8o0〇:Lcom/intsig/camscanner/watermark/WaterMarkView$AlignModeV;

    .line 44
    .line 45
    new-instance v0, Landroid/graphics/Path;

    .line 46
    .line 47
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 48
    .line 49
    .line 50
    iput-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O8o08O:Landroid/graphics/Path;

    .line 51
    .line 52
    iput-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 53
    .line 54
    iput-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->O8:Lcom/intsig/camscanner/watermark/TextInterface;

    .line 55
    .line 56
    instance-of p1, p2, Lcom/intsig/camscanner/watermark/EditableInterface;

    .line 57
    .line 58
    if-eqz p1, :cond_0

    .line 59
    .line 60
    check-cast p2, Lcom/intsig/camscanner/watermark/EditableInterface;

    .line 61
    .line 62
    iput-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->Oo08:Lcom/intsig/camscanner/watermark/EditableInterface;

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_0
    const/4 p1, 0x0

    .line 66
    iput-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->Oo08:Lcom/intsig/camscanner/watermark/EditableInterface;

    .line 67
    .line 68
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->OOO〇O0()V

    .line 69
    .line 70
    .line 71
    const/high16 p1, 0x41a00000    # 20.0f

    .line 72
    .line 73
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇O8〇〇o(F)V

    .line 74
    .line 75
    .line 76
    return-void

    .line 77
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private O8(Landroid/graphics/Canvas;Z)V
    .locals 2

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    iget-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O8o08O:Landroid/graphics/Path;

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇8o8o〇:Landroid/graphics/Paint;

    .line 6
    .line 7
    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 8
    .line 9
    .line 10
    iget-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OO0o〇〇〇〇0:Landroid/graphics/Paint;

    .line 11
    .line 12
    invoke-virtual {p2}, Landroid/graphics/Paint;->reset()V

    .line 13
    .line 14
    .line 15
    iget-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OO0o〇〇〇〇0:Landroid/graphics/Paint;

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->O8:Lcom/intsig/camscanner/watermark/TextInterface;

    .line 18
    .line 19
    check-cast v0, Lcom/intsig/camscanner/watermark/WaterMarkTextView;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/watermark/WaterMarkTextView;->OoO8()Landroid/graphics/Paint;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 26
    .line 27
    .line 28
    iget-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OO0o〇〇〇〇0:Landroid/graphics/Paint;

    .line 29
    .line 30
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 31
    .line 32
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 33
    .line 34
    .line 35
    iget-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OO0o〇〇〇〇0:Landroid/graphics/Paint;

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->O8:Lcom/intsig/camscanner/watermark/TextInterface;

    .line 38
    .line 39
    check-cast v0, Lcom/intsig/camscanner/watermark/WaterMarkTextView;

    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/camscanner/watermark/WaterMarkTextView;->o800o8O()F

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    const/high16 v1, 0x41800000    # 16.0f

    .line 46
    .line 47
    div-float/2addr v0, v1

    .line 48
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 49
    .line 50
    .line 51
    iget-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O8o08O:Landroid/graphics/Path;

    .line 52
    .line 53
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OO0o〇〇〇〇0:Landroid/graphics/Paint;

    .line 54
    .line 55
    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 56
    .line 57
    .line 58
    :cond_0
    iget-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->O8:Lcom/intsig/camscanner/watermark/TextInterface;

    .line 59
    .line 60
    invoke-interface {p2, p1}, Lcom/intsig/camscanner/watermark/TextInterface;->draw(Landroid/graphics/Canvas;)V

    .line 61
    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private OOO〇O0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->O8:Lcom/intsig/camscanner/watermark/TextInterface;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/watermark/TextInterface;->getIntrinsicWidth()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->O8:Lcom/intsig/camscanner/watermark/TextInterface;

    .line 8
    .line 9
    invoke-interface {v1}, Lcom/intsig/camscanner/watermark/TextInterface;->getIntrinsicHeight()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    int-to-float v0, v0

    .line 14
    int-to-float v1, v1

    .line 15
    div-float/2addr v0, v1

    .line 16
    iput v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o800o8O:F

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private 〇o00〇〇Oo()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->O8:Lcom/intsig/camscanner/watermark/TextInterface;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/camscanner/watermark/WaterMarkTextView;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/watermark/WaterMarkTextView;->o800o8O()F

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/high16 v1, 0x40000000    # 2.0f

    .line 10
    .line 11
    div-float/2addr v0, v1

    .line 12
    float-to-int v0, v0

    .line 13
    iput v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OoO8:I

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇〇8O0〇8(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const v0, 0x7f0812fd

    .line 6
    .line 7
    .line 8
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o00〇〇Oo:Landroid/graphics/drawable/Drawable;

    .line 13
    .line 14
    const v0, 0x7f0812fc

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    iput-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇080:Landroid/graphics/drawable/Drawable;

    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o00〇〇Oo:Landroid/graphics/drawable/Drawable;

    .line 24
    .line 25
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    div-int/lit8 p1, p1, 0x2

    .line 30
    .line 31
    iput p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O〇:I

    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o00〇〇Oo:Landroid/graphics/drawable/Drawable;

    .line 34
    .line 35
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    div-int/lit8 p1, p1, 0x2

    .line 40
    .line 41
    iput p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇808〇:I

    .line 42
    .line 43
    new-instance p1, Landroid/graphics/Paint;

    .line 44
    .line 45
    const/4 v0, 0x1

    .line 46
    invoke-direct {p1, v0}, Landroid/graphics/Paint;-><init>(I)V

    .line 47
    .line 48
    .line 49
    iput-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇8o8o〇:Landroid/graphics/Paint;

    .line 50
    .line 51
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 52
    .line 53
    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 54
    .line 55
    .line 56
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇8o8o〇:Landroid/graphics/Paint;

    .line 57
    .line 58
    const v1, -0x73000001

    .line 59
    .line 60
    .line 61
    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 62
    .line 63
    .line 64
    new-instance p1, Landroid/graphics/Paint;

    .line 65
    .line 66
    invoke-direct {p1, v0}, Landroid/graphics/Paint;-><init>(I)V

    .line 67
    .line 68
    .line 69
    iput-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OO0o〇〇〇〇0:Landroid/graphics/Paint;

    .line 70
    .line 71
    sget-object p1, Lcom/intsig/camscanner/watermark/WaterMarkView$Mode;->None:Lcom/intsig/camscanner/watermark/WaterMarkView$Mode;

    .line 72
    .line 73
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇00(Lcom/intsig/camscanner/watermark/WaterMarkView$Mode;)V

    .line 74
    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public O8ooOoo〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O888o0o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public OO0o〇〇()Landroid/graphics/Matrix;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oO80:Landroid/graphics/Matrix;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO0o〇〇〇〇0()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇00:I

    .line 2
    .line 3
    const/4 v1, 0x4

    .line 4
    and-int/2addr v0, v1

    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oo08()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888()Landroid/graphics/RectF;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇80〇808〇O()Landroid/graphics/RectF;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->Oo08:Lcom/intsig/camscanner/watermark/EditableInterface;

    .line 14
    .line 15
    if-eqz v2, :cond_2

    .line 16
    .line 17
    iget-object v2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->O8:Lcom/intsig/camscanner/watermark/TextInterface;

    .line 18
    .line 19
    invoke-interface {v2}, Lcom/intsig/camscanner/watermark/TextInterface;->getIntrinsicWidth()I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    iget-object v3, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->O8:Lcom/intsig/camscanner/watermark/TextInterface;

    .line 24
    .line 25
    invoke-interface {v3}, Lcom/intsig/camscanner/watermark/TextInterface;->getIntrinsicHeight()I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->OOO〇O0()V

    .line 30
    .line 31
    .line 32
    new-instance v4, Landroid/graphics/RectF;

    .line 33
    .line 34
    invoke-direct {v4, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->OO0o〇〇()Landroid/graphics/Matrix;

    .line 38
    .line 39
    .line 40
    move-result-object v5

    .line 41
    invoke-virtual {v5, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 42
    .line 43
    .line 44
    const/4 v5, 0x2

    .line 45
    new-array v5, v5, [F

    .line 46
    .line 47
    int-to-float v2, v2

    .line 48
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    .line 49
    .line 50
    .line 51
    move-result v6

    .line 52
    sub-float/2addr v2, v6

    .line 53
    const/4 v6, 0x0

    .line 54
    aput v2, v5, v6

    .line 55
    .line 56
    int-to-float v2, v3

    .line 57
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    sub-float/2addr v2, v3

    .line 62
    const/4 v3, 0x1

    .line 63
    aput v2, v5, v3

    .line 64
    .line 65
    new-instance v2, Landroid/graphics/Matrix;

    .line 66
    .line 67
    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 68
    .line 69
    .line 70
    iget v4, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oo88o8O:F

    .line 71
    .line 72
    neg-float v4, v4

    .line 73
    invoke-virtual {v2, v4}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 74
    .line 75
    .line 76
    aget v2, v5, v6

    .line 77
    .line 78
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 79
    .line 80
    .line 81
    move-result v4

    .line 82
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    .line 83
    .line 84
    .line 85
    move-result v7

    .line 86
    div-float/2addr v4, v7

    .line 87
    mul-float v2, v2, v4

    .line 88
    .line 89
    aget v3, v5, v3

    .line 90
    .line 91
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 92
    .line 93
    .line 94
    move-result v0

    .line 95
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    .line 96
    .line 97
    .line 98
    move-result v1

    .line 99
    div-float/2addr v0, v1

    .line 100
    mul-float v3, v3, v0

    .line 101
    .line 102
    const/4 v0, 0x0

    .line 103
    cmpl-float v1, v2, v0

    .line 104
    .line 105
    if-nez v1, :cond_0

    .line 106
    .line 107
    cmpl-float v0, v3, v0

    .line 108
    .line 109
    if-eqz v0, :cond_1

    .line 110
    .line 111
    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    .line 112
    .line 113
    div-float/2addr v2, v0

    .line 114
    div-float/2addr v3, v0

    .line 115
    invoke-virtual {p0, v2, v3, v6}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O00(FFZ)V

    .line 116
    .line 117
    .line 118
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇0〇O0088o()V

    .line 119
    .line 120
    .line 121
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 122
    .line 123
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O8o08O()Landroid/graphics/Rect;

    .line 124
    .line 125
    .line 126
    move-result-object v1

    .line 127
    invoke-virtual {v0, v1}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    .line 128
    .line 129
    .line 130
    :cond_2
    return-void
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method OoO8(FF)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇0:Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->offset(FF)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇0〇O0088o()V

    .line 7
    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public Oooo8o0〇()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oo88o8O:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O〇8O8〇008(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oo88o8O:F

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->Oo08()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public o800o8O(ILandroid/view/MotionEvent;FF)V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eq p1, v0, :cond_5

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 5
    .line 6
    if-nez v1, :cond_0

    .line 7
    .line 8
    goto/16 :goto_0

    .line 9
    .line 10
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OO0o〇〇:[F

    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    aput p3, v1, v2

    .line 14
    .line 15
    aput p4, v1, v0

    .line 16
    .line 17
    const/16 v1, 0x40

    .line 18
    .line 19
    if-ne p1, v1, :cond_1

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇0:Landroid/graphics/RectF;

    .line 22
    .line 23
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    iget-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 28
    .line 29
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    .line 30
    .line 31
    .line 32
    move-result p2

    .line 33
    div-float/2addr p1, p2

    .line 34
    mul-float p3, p3, p1

    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇0:Landroid/graphics/RectF;

    .line 37
    .line 38
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    iget-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 43
    .line 44
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    .line 45
    .line 46
    .line 47
    move-result p2

    .line 48
    div-float/2addr p1, p2

    .line 49
    mul-float p4, p4, p1

    .line 50
    .line 51
    invoke-virtual {p0, p3, p4}, Lcom/intsig/camscanner/watermark/WaterMarkView;->OoO8(FF)V

    .line 52
    .line 53
    .line 54
    return-void

    .line 55
    :cond_1
    const/16 p4, 0x20

    .line 56
    .line 57
    if-ne p1, p4, :cond_2

    .line 58
    .line 59
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇0:Landroid/graphics/RectF;

    .line 60
    .line 61
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    .line 62
    .line 63
    .line 64
    move-result p1

    .line 65
    iget-object p4, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 66
    .line 67
    invoke-virtual {p4}, Landroid/graphics/RectF;->width()F

    .line 68
    .line 69
    .line 70
    move-result p4

    .line 71
    div-float/2addr p1, p4

    .line 72
    mul-float p3, p3, p1

    .line 73
    .line 74
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OO0o〇〇:[F

    .line 75
    .line 76
    aget p1, p1, v0

    .line 77
    .line 78
    iget-object p4, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇0:Landroid/graphics/RectF;

    .line 79
    .line 80
    invoke-virtual {p4}, Landroid/graphics/RectF;->height()F

    .line 81
    .line 82
    .line 83
    move-result p4

    .line 84
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 85
    .line 86
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    div-float/2addr p4, v0

    .line 91
    mul-float p1, p1, p4

    .line 92
    .line 93
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    .line 94
    .line 95
    .line 96
    move-result p4

    .line 97
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    .line 98
    .line 99
    .line 100
    move-result p2

    .line 101
    invoke-virtual {p0, p4, p2, p3, p1}, Lcom/intsig/camscanner/watermark/WaterMarkView;->oo88o8O(FFFF)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇0〇O0088o()V

    .line 105
    .line 106
    .line 107
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 108
    .line 109
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O8o08O()Landroid/graphics/Rect;

    .line 110
    .line 111
    .line 112
    move-result-object p2

    .line 113
    invoke-virtual {p1, p2}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    .line 114
    .line 115
    .line 116
    return-void

    .line 117
    :cond_2
    new-instance p2, Landroid/graphics/Matrix;

    .line 118
    .line 119
    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    .line 120
    .line 121
    .line 122
    iget p3, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oo88o8O:F

    .line 123
    .line 124
    neg-float p3, p3

    .line 125
    invoke-virtual {p2, p3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 126
    .line 127
    .line 128
    iget-object p3, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OO0o〇〇:[F

    .line 129
    .line 130
    invoke-virtual {p2, p3}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 131
    .line 132
    .line 133
    iget-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OO0o〇〇:[F

    .line 134
    .line 135
    aget p2, p2, v2

    .line 136
    .line 137
    and-int/lit8 p3, p1, 0x6

    .line 138
    .line 139
    if-nez p3, :cond_3

    .line 140
    .line 141
    const/4 p2, 0x0

    .line 142
    :cond_3
    iget-object p3, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇0:Landroid/graphics/RectF;

    .line 143
    .line 144
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    .line 145
    .line 146
    .line 147
    move-result p3

    .line 148
    iget-object p4, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 149
    .line 150
    invoke-virtual {p4}, Landroid/graphics/RectF;->width()F

    .line 151
    .line 152
    .line 153
    move-result p4

    .line 154
    div-float/2addr p3, p4

    .line 155
    mul-float p2, p2, p3

    .line 156
    .line 157
    and-int/lit8 p1, p1, 0x2

    .line 158
    .line 159
    if-eqz p1, :cond_4

    .line 160
    .line 161
    const/4 v0, -0x1

    .line 162
    :cond_4
    int-to-float p1, v0

    .line 163
    mul-float p2, p2, p1

    .line 164
    .line 165
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O〇(F)V

    .line 166
    .line 167
    .line 168
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇0〇O0088o()V

    .line 169
    .line 170
    .line 171
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 172
    .line 173
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O8o08O()Landroid/graphics/Rect;

    .line 174
    .line 175
    .line 176
    move-result-object p2

    .line 177
    invoke-virtual {p1, p2}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    .line 178
    .line 179
    .line 180
    :cond_5
    :goto_0
    return-void
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method protected oO80(Landroid/graphics/Matrix;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-direct {v0, p2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method oo88o8O(FFFF)V
    .locals 9

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v1, v0, [F

    .line 3
    .line 4
    iget-object v2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 5
    .line 6
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    const/4 v3, 0x0

    .line 11
    aput v2, v1, v3

    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 14
    .line 15
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    const/4 v4, 0x1

    .line 20
    aput v2, v1, v4

    .line 21
    .line 22
    new-array v2, v0, [F

    .line 23
    .line 24
    iget-object v5, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 25
    .line 26
    iget v6, v5, Landroid/graphics/RectF;->right:F

    .line 27
    .line 28
    aput v6, v2, v3

    .line 29
    .line 30
    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    .line 31
    .line 32
    aput v5, v2, v4

    .line 33
    .line 34
    new-array v5, v0, [F

    .line 35
    .line 36
    aput p1, v5, v3

    .line 37
    .line 38
    aput p2, v5, v4

    .line 39
    .line 40
    invoke-static {v2, v1}, Lcom/intsig/camscanner/watermark/WaterMarkUtil;->Oo08([F[F)D

    .line 41
    .line 42
    .line 43
    move-result-wide p1

    .line 44
    invoke-static {v5, v1}, Lcom/intsig/camscanner/watermark/WaterMarkUtil;->Oo08([F[F)D

    .line 45
    .line 46
    .line 47
    move-result-wide v5

    .line 48
    iget-boolean v7, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O888o0o:Z

    .line 49
    .line 50
    if-eqz v7, :cond_1

    .line 51
    .line 52
    new-instance v7, Landroid/graphics/Matrix;

    .line 53
    .line 54
    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 55
    .line 56
    .line 57
    iget v8, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oo88o8O:F

    .line 58
    .line 59
    neg-float v8, v8

    .line 60
    invoke-virtual {v7, v8}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 61
    .line 62
    .line 63
    new-array v8, v0, [F

    .line 64
    .line 65
    aput p3, v8, v3

    .line 66
    .line 67
    aput p4, v8, v4

    .line 68
    .line 69
    invoke-virtual {v7, v8}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 70
    .line 71
    .line 72
    aget p3, v8, v3

    .line 73
    .line 74
    iget-object p4, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇0:Landroid/graphics/RectF;

    .line 75
    .line 76
    invoke-virtual {p4}, Landroid/graphics/RectF;->width()F

    .line 77
    .line 78
    .line 79
    move-result p4

    .line 80
    iget-object v7, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 81
    .line 82
    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    .line 83
    .line 84
    .line 85
    move-result v7

    .line 86
    div-float/2addr p4, v7

    .line 87
    mul-float p3, p3, p4

    .line 88
    .line 89
    aget p4, v8, v4

    .line 90
    .line 91
    iget-object v7, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇0:Landroid/graphics/RectF;

    .line 92
    .line 93
    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    .line 94
    .line 95
    .line 96
    move-result v7

    .line 97
    iget-object v8, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 98
    .line 99
    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    .line 100
    .line 101
    .line 102
    move-result v8

    .line 103
    div-float/2addr v7, v8

    .line 104
    mul-float p4, p4, v7

    .line 105
    .line 106
    new-array v0, v0, [F

    .line 107
    .line 108
    iget-object v7, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 109
    .line 110
    iget v8, v7, Landroid/graphics/RectF;->right:F

    .line 111
    .line 112
    add-float/2addr p3, v8

    .line 113
    aput p3, v0, v3

    .line 114
    .line 115
    iget p3, v7, Landroid/graphics/RectF;->bottom:F

    .line 116
    .line 117
    add-float/2addr p4, p3

    .line 118
    aput p4, v0, v4

    .line 119
    .line 120
    invoke-static {v1, v0}, Lcom/intsig/camscanner/watermark/WaterMarkUtil;->OO0o〇〇〇〇0([F[F)D

    .line 121
    .line 122
    .line 123
    move-result-wide p3

    .line 124
    invoke-static {v1, v2}, Lcom/intsig/camscanner/watermark/WaterMarkUtil;->OO0o〇〇〇〇0([F[F)D

    .line 125
    .line 126
    .line 127
    move-result-wide v0

    .line 128
    sub-double/2addr p3, v0

    .line 129
    double-to-float p3, p3

    .line 130
    sub-double/2addr v5, p1

    .line 131
    double-to-float p1, v5

    .line 132
    neg-float p1, p1

    .line 133
    iput p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oo88o8O:F

    .line 134
    .line 135
    iget p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o800o8O:F

    .line 136
    .line 137
    float-to-double p1, p1

    .line 138
    const-wide/high16 v0, 0x4004000000000000L    # 2.5

    .line 139
    .line 140
    cmpg-double p4, p1, v0

    .line 141
    .line 142
    if-gtz p4, :cond_0

    .line 143
    .line 144
    const/high16 p1, 0x40800000    # 4.0f

    .line 145
    .line 146
    div-float/2addr p3, p1

    .line 147
    :cond_0
    invoke-virtual {p0, p3}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O〇(F)V

    .line 148
    .line 149
    .line 150
    goto :goto_0

    .line 151
    :cond_1
    sub-double/2addr v5, p1

    .line 152
    double-to-float p1, v5

    .line 153
    neg-float p1, p1

    .line 154
    iput p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oo88o8O:F

    .line 155
    .line 156
    :goto_0
    return-void
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public o〇0()Lcom/intsig/camscanner/watermark/TextInterface;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->O8:Lcom/intsig/camscanner/watermark/TextInterface;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇O8〇〇o(F)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o800o8O:F

    .line 2
    .line 3
    const/high16 v1, 0x3f800000    # 1.0f

    .line 4
    .line 5
    cmpl-float v1, v0, v1

    .line 6
    .line 7
    if-ltz v1, :cond_0

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->O8:Lcom/intsig/camscanner/watermark/TextInterface;

    .line 10
    .line 11
    div-float v0, p1, v0

    .line 12
    .line 13
    invoke-interface {v1, p1, v0}, Lcom/intsig/camscanner/watermark/TextInterface;->oO80(FF)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->O8:Lcom/intsig/camscanner/watermark/TextInterface;

    .line 18
    .line 19
    mul-float v0, v0, p1

    .line 20
    .line 21
    invoke-interface {v1, v0, p1}, Lcom/intsig/camscanner/watermark/TextInterface;->oO80(FF)V

    .line 22
    .line 23
    .line 24
    return-void
.end method

.method public o〇〇0〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇O8〇〇o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public update(Landroid/graphics/Matrix;)V
    .locals 1

    .line 6
    sget-object v0, Lcom/intsig/camscanner/watermark/WaterMarkView$Mode;->None:Lcom/intsig/camscanner/watermark/WaterMarkView$Mode;

    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇00(Lcom/intsig/camscanner/watermark/WaterMarkView$Mode;)V

    .line 7
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0, p1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oO80:Landroid/graphics/Matrix;

    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇0〇O0088o()V

    return-void
.end method

.method public update(Landroid/graphics/Matrix;Landroid/graphics/Rect;)V
    .locals 0

    .line 1
    sget-object p2, Lcom/intsig/camscanner/watermark/WaterMarkView$Mode;->None:Lcom/intsig/camscanner/watermark/WaterMarkView$Mode;

    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇00(Lcom/intsig/camscanner/watermark/WaterMarkView$Mode;)V

    .line 2
    new-instance p2, Landroid/graphics/Matrix;

    invoke-direct {p2, p1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oO80:Landroid/graphics/Matrix;

    const/4 p1, 0x0

    .line 3
    iput p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oo88o8O:F

    .line 4
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇0〇O0088o()V

    return-void
.end method

.method public 〇00(Lcom/intsig/camscanner/watermark/WaterMarkView$Mode;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇8O0〇8:Lcom/intsig/camscanner/watermark/WaterMarkView$Mode;

    .line 2
    .line 3
    if-eq p1, v0, :cond_0

    .line 4
    .line 5
    iput-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇8O0〇8:Lcom/intsig/camscanner/watermark/WaterMarkView$Mode;

    .line 6
    .line 7
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 8
    .line 9
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇0000OOO(Landroid/content/Context;Landroid/graphics/Matrix;Landroid/graphics/Rect;Landroid/graphics/RectF;Z)V
    .locals 0

    .line 1
    new-instance p3, Landroid/graphics/Matrix;

    .line 2
    .line 3
    invoke-direct {p3, p2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 4
    .line 5
    .line 6
    iput-object p3, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oO80:Landroid/graphics/Matrix;

    .line 7
    .line 8
    const/4 p2, 0x0

    .line 9
    iput p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oo88o8O:F

    .line 10
    .line 11
    new-instance p2, Landroid/graphics/Matrix;

    .line 12
    .line 13
    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 17
    .line 18
    iput-object p4, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇0:Landroid/graphics/RectF;

    .line 19
    .line 20
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇8O0〇8(Landroid/content/Context;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇0〇O0088o()V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method protected 〇080()Landroid/graphics/RectF;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oO80:Landroid/graphics/Matrix;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇0:Landroid/graphics/RectF;

    .line 4
    .line 5
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/watermark/WaterMarkView;->oO80(Landroid/graphics/Matrix;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0〇O0088o()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇080()Landroid/graphics/RectF;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 15
    .line 16
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    neg-float v1, v1

    .line 21
    iget-object v2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 22
    .line 23
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    neg-float v2, v2

    .line 28
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 32
    .line 33
    iget v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oo88o8O:F

    .line 34
    .line 35
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 41
    .line 42
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    iget-object v2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 47
    .line 48
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇80〇808〇O()Landroid/graphics/RectF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8o8o〇(FF)I
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 p1, -0x1

    .line 6
    goto/16 :goto_4

    .line 7
    .line 8
    :cond_0
    new-instance v0, Landroid/graphics/RectF;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 11
    .line 12
    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 13
    .line 14
    .line 15
    iget v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OoO8:I

    .line 16
    .line 17
    neg-int v2, v1

    .line 18
    int-to-float v2, v2

    .line 19
    neg-int v1, v1

    .line 20
    int-to-float v1, v1

    .line 21
    invoke-virtual {v0, v2, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 22
    .line 23
    .line 24
    const/4 v1, 0x2

    .line 25
    new-array v1, v1, [F

    .line 26
    .line 27
    const/4 v2, 0x0

    .line 28
    aput p1, v1, v2

    .line 29
    .line 30
    const/4 p1, 0x1

    .line 31
    aput p2, v1, p1

    .line 32
    .line 33
    new-instance p2, Landroid/graphics/Matrix;

    .line 34
    .line 35
    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    neg-float v3, v3

    .line 43
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    .line 44
    .line 45
    .line 46
    move-result v4

    .line 47
    neg-float v4, v4

    .line 48
    invoke-virtual {p2, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 49
    .line 50
    .line 51
    iget v3, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oo88o8O:F

    .line 52
    .line 53
    neg-float v3, v3

    .line 54
    invoke-virtual {p2, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    .line 62
    .line 63
    .line 64
    move-result v4

    .line 65
    invoke-virtual {p2, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 66
    .line 67
    .line 68
    invoke-virtual {p2, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 69
    .line 70
    .line 71
    iget-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 72
    .line 73
    invoke-virtual {p2}, Landroid/view/View;->invalidate()V

    .line 74
    .line 75
    .line 76
    aget p2, v1, p1

    .line 77
    .line 78
    iget v3, v0, Landroid/graphics/RectF;->top:F

    .line 79
    .line 80
    const/high16 v4, 0x42200000    # 40.0f

    .line 81
    .line 82
    sub-float/2addr v3, v4

    .line 83
    cmpl-float v3, p2, v3

    .line 84
    .line 85
    if-ltz v3, :cond_1

    .line 86
    .line 87
    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    .line 88
    .line 89
    add-float/2addr v3, v4

    .line 90
    cmpg-float p2, p2, v3

    .line 91
    .line 92
    if-gez p2, :cond_1

    .line 93
    .line 94
    const/4 p2, 0x1

    .line 95
    goto :goto_0

    .line 96
    :cond_1
    const/4 p2, 0x0

    .line 97
    :goto_0
    aget v3, v1, v2

    .line 98
    .line 99
    iget v5, v0, Landroid/graphics/RectF;->left:F

    .line 100
    .line 101
    sub-float v6, v5, v4

    .line 102
    .line 103
    cmpl-float v6, v3, v6

    .line 104
    .line 105
    if-ltz v6, :cond_2

    .line 106
    .line 107
    iget v6, v0, Landroid/graphics/RectF;->right:F

    .line 108
    .line 109
    add-float/2addr v6, v4

    .line 110
    cmpg-float v6, v3, v6

    .line 111
    .line 112
    if-gez v6, :cond_2

    .line 113
    .line 114
    const/4 v6, 0x1

    .line 115
    goto :goto_1

    .line 116
    :cond_2
    const/4 v6, 0x0

    .line 117
    :goto_1
    const/16 v7, 0x40

    .line 118
    .line 119
    if-eqz p2, :cond_3

    .line 120
    .line 121
    if-eqz v6, :cond_3

    .line 122
    .line 123
    const/16 v8, 0x40

    .line 124
    .line 125
    goto :goto_2

    .line 126
    :cond_3
    const/4 v8, 0x1

    .line 127
    :goto_2
    iget-boolean v9, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O888o0o:Z

    .line 128
    .line 129
    if-nez v9, :cond_7

    .line 130
    .line 131
    sub-float/2addr v5, v3

    .line 132
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    .line 133
    .line 134
    .line 135
    move-result v3

    .line 136
    cmpg-float v3, v3, v4

    .line 137
    .line 138
    if-gez v3, :cond_4

    .line 139
    .line 140
    if-eqz p2, :cond_4

    .line 141
    .line 142
    or-int/lit8 v8, v8, 0x2

    .line 143
    .line 144
    :cond_4
    iget v3, v0, Landroid/graphics/RectF;->right:F

    .line 145
    .line 146
    aget v5, v1, v2

    .line 147
    .line 148
    sub-float/2addr v3, v5

    .line 149
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    .line 150
    .line 151
    .line 152
    move-result v3

    .line 153
    cmpg-float v3, v3, v4

    .line 154
    .line 155
    if-gez v3, :cond_5

    .line 156
    .line 157
    if-eqz p2, :cond_5

    .line 158
    .line 159
    or-int/lit8 v8, v8, 0x4

    .line 160
    .line 161
    :cond_5
    iget v3, v0, Landroid/graphics/RectF;->top:F

    .line 162
    .line 163
    aget v5, v1, p1

    .line 164
    .line 165
    sub-float/2addr v3, v5

    .line 166
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    .line 167
    .line 168
    .line 169
    move-result v3

    .line 170
    cmpg-float v3, v3, v4

    .line 171
    .line 172
    if-gez v3, :cond_6

    .line 173
    .line 174
    if-eqz v6, :cond_6

    .line 175
    .line 176
    or-int/lit8 v3, v8, 0x8

    .line 177
    .line 178
    move v8, v3

    .line 179
    :cond_6
    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    .line 180
    .line 181
    aget v5, v1, p1

    .line 182
    .line 183
    sub-float/2addr v3, v5

    .line 184
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    .line 185
    .line 186
    .line 187
    move-result v3

    .line 188
    cmpg-float v3, v3, v4

    .line 189
    .line 190
    if-gez v3, :cond_7

    .line 191
    .line 192
    if-eqz v6, :cond_7

    .line 193
    .line 194
    or-int/lit8 v8, v8, 0x10

    .line 195
    .line 196
    :cond_7
    iget v3, v0, Landroid/graphics/RectF;->right:F

    .line 197
    .line 198
    aget v5, v1, v2

    .line 199
    .line 200
    sub-float/2addr v3, v5

    .line 201
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    .line 202
    .line 203
    .line 204
    move-result v3

    .line 205
    cmpg-float v3, v3, v4

    .line 206
    .line 207
    if-gez v3, :cond_8

    .line 208
    .line 209
    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    .line 210
    .line 211
    aget v5, v1, p1

    .line 212
    .line 213
    sub-float/2addr v3, v5

    .line 214
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    .line 215
    .line 216
    .line 217
    move-result v3

    .line 218
    cmpg-float v3, v3, v4

    .line 219
    .line 220
    if-gez v3, :cond_8

    .line 221
    .line 222
    if-eqz p2, :cond_8

    .line 223
    .line 224
    if-eqz v6, :cond_8

    .line 225
    .line 226
    const/16 p2, 0x20

    .line 227
    .line 228
    goto :goto_3

    .line 229
    :cond_8
    move p2, v8

    .line 230
    :goto_3
    if-ne p2, p1, :cond_9

    .line 231
    .line 232
    aget v2, v1, v2

    .line 233
    .line 234
    float-to-int v2, v2

    .line 235
    int-to-float v2, v2

    .line 236
    aget p1, v1, p1

    .line 237
    .line 238
    float-to-int p1, p1

    .line 239
    int-to-float p1, p1

    .line 240
    invoke-virtual {v0, v2, p1}, Landroid/graphics/RectF;->contains(FF)Z

    .line 241
    .line 242
    .line 243
    move-result p1

    .line 244
    if-eqz p1, :cond_9

    .line 245
    .line 246
    return v7

    .line 247
    :cond_9
    move p1, p2

    .line 248
    :goto_4
    return p1
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method protected 〇O00(FFZ)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    new-instance v0, Landroid/graphics/RectF;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇0:Landroid/graphics/RectF;

    .line 8
    .line 9
    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 10
    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->Oooo8o0〇:Lcom/intsig/camscanner/watermark/WaterMarkView$AlignModeV;

    .line 13
    .line 14
    sget-object v2, Lcom/intsig/camscanner/watermark/WaterMarkView$AlignModeV;->Center:Lcom/intsig/camscanner/watermark/WaterMarkView$AlignModeV;

    .line 15
    .line 16
    if-ne v1, v2, :cond_0

    .line 17
    .line 18
    neg-float p1, p1

    .line 19
    neg-float p2, p2

    .line 20
    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->inset(FF)V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    sget-object v2, Lcom/intsig/camscanner/watermark/WaterMarkView$AlignModeV;->Top:Lcom/intsig/camscanner/watermark/WaterMarkView$AlignModeV;

    .line 25
    .line 26
    const/high16 v3, 0x40000000    # 2.0f

    .line 27
    .line 28
    const/4 v4, 0x0

    .line 29
    if-ne v1, v2, :cond_1

    .line 30
    .line 31
    neg-float p1, p1

    .line 32
    invoke-virtual {v0, p1, v4}, Landroid/graphics/RectF;->inset(FF)V

    .line 33
    .line 34
    .line 35
    iget p1, v0, Landroid/graphics/RectF;->bottom:F

    .line 36
    .line 37
    mul-float p2, p2, v3

    .line 38
    .line 39
    add-float/2addr p1, p2

    .line 40
    iput p1, v0, Landroid/graphics/RectF;->bottom:F

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    neg-float p1, p1

    .line 44
    invoke-virtual {v0, p1, v4}, Landroid/graphics/RectF;->inset(FF)V

    .line 45
    .line 46
    .line 47
    iget p1, v0, Landroid/graphics/RectF;->top:F

    .line 48
    .line 49
    mul-float p2, p2, v3

    .line 50
    .line 51
    sub-float/2addr p1, p2

    .line 52
    iput p1, v0, Landroid/graphics/RectF;->top:F

    .line 53
    .line 54
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->O8:Lcom/intsig/camscanner/watermark/TextInterface;

    .line 55
    .line 56
    iget-object p2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oO80:Landroid/graphics/Matrix;

    .line 57
    .line 58
    invoke-virtual {p0, p2, v0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->oO80(Landroid/graphics/Matrix;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    .line 59
    .line 60
    .line 61
    move-result-object p2

    .line 62
    invoke-interface {p1, p2}, Lcom/intsig/camscanner/watermark/TextInterface;->o〇0(Landroid/graphics/RectF;)Z

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    if-nez p1, :cond_2

    .line 67
    .line 68
    if-nez p3, :cond_3

    .line 69
    .line 70
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇0:Landroid/graphics/RectF;

    .line 71
    .line 72
    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇0〇O0088o()V

    .line 76
    .line 77
    .line 78
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 79
    .line 80
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 81
    .line 82
    .line 83
    :cond_3
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public 〇O888o0o(FF)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    new-instance v0, Landroid/graphics/RectF;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 8
    .line 9
    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 10
    .line 11
    .line 12
    iget v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OoO8:I

    .line 13
    .line 14
    neg-int v2, v1

    .line 15
    int-to-float v2, v2

    .line 16
    neg-int v1, v1

    .line 17
    int-to-float v1, v1

    .line 18
    invoke-virtual {v0, v2, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 19
    .line 20
    .line 21
    const/4 v1, 0x2

    .line 22
    new-array v1, v1, [F

    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    aput p1, v1, v2

    .line 26
    .line 27
    const/4 p1, 0x1

    .line 28
    aput p2, v1, p1

    .line 29
    .line 30
    new-instance p2, Landroid/graphics/Matrix;

    .line 31
    .line 32
    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    neg-float v3, v3

    .line 40
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    .line 41
    .line 42
    .line 43
    move-result v4

    .line 44
    neg-float v4, v4

    .line 45
    invoke-virtual {p2, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 46
    .line 47
    .line 48
    iget v3, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->oo88o8O:F

    .line 49
    .line 50
    neg-float v3, v3

    .line 51
    invoke-virtual {p2, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    .line 55
    .line 56
    .line 57
    move-result v3

    .line 58
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    .line 59
    .line 60
    .line 61
    move-result v4

    .line 62
    invoke-virtual {p2, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 63
    .line 64
    .line 65
    invoke-virtual {p2, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 66
    .line 67
    .line 68
    aget p2, v1, v2

    .line 69
    .line 70
    aget p1, v1, p1

    .line 71
    .line 72
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 73
    .line 74
    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 75
    .line 76
    .line 77
    iget v1, v0, Landroid/graphics/RectF;->top:F

    .line 78
    .line 79
    const/high16 v2, 0x42200000    # 40.0f

    .line 80
    .line 81
    sub-float/2addr v1, v2

    .line 82
    cmpl-float v1, p1, v1

    .line 83
    .line 84
    if-ltz v1, :cond_0

    .line 85
    .line 86
    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 87
    .line 88
    add-float/2addr v1, v2

    .line 89
    cmpg-float v1, p1, v1

    .line 90
    .line 91
    :cond_0
    iget v1, v0, Landroid/graphics/RectF;->left:F

    .line 92
    .line 93
    sub-float v3, v1, v2

    .line 94
    .line 95
    cmpl-float v3, p2, v3

    .line 96
    .line 97
    if-ltz v3, :cond_1

    .line 98
    .line 99
    iget v3, v0, Landroid/graphics/RectF;->right:F

    .line 100
    .line 101
    add-float/2addr v3, v2

    .line 102
    cmpg-float v3, p2, v3

    .line 103
    .line 104
    :cond_1
    iget-boolean v3, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇O8〇〇o:Z

    .line 105
    .line 106
    if-eqz v3, :cond_2

    .line 107
    .line 108
    sub-float/2addr v1, p2

    .line 109
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 110
    .line 111
    .line 112
    move-result p2

    .line 113
    cmpg-float p2, p2, v2

    .line 114
    .line 115
    if-gez p2, :cond_2

    .line 116
    .line 117
    iget p2, v0, Landroid/graphics/RectF;->top:F

    .line 118
    .line 119
    sub-float/2addr p2, p1

    .line 120
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    .line 121
    .line 122
    .line 123
    :cond_2
    return-void
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method protected 〇O8o08O()Landroid/graphics/Rect;
    .locals 5

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 6
    .line 7
    .line 8
    iget v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OoO8:I

    .line 9
    .line 10
    neg-int v2, v1

    .line 11
    int-to-float v2, v2

    .line 12
    neg-int v1, v1

    .line 13
    int-to-float v1, v1

    .line 14
    invoke-virtual {v0, v2, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 15
    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 18
    .line 19
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 20
    .line 21
    .line 22
    new-instance v1, Landroid/graphics/Rect;

    .line 23
    .line 24
    iget v2, v0, Landroid/graphics/RectF;->left:F

    .line 25
    .line 26
    float-to-int v2, v2

    .line 27
    iget v3, v0, Landroid/graphics/RectF;->top:F

    .line 28
    .line 29
    float-to-int v3, v3

    .line 30
    iget v4, v0, Landroid/graphics/RectF;->right:F

    .line 31
    .line 32
    float-to-int v4, v4

    .line 33
    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    .line 34
    .line 35
    float-to-int v0, v0

    .line 36
    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 37
    .line 38
    .line 39
    iget v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O〇:I

    .line 40
    .line 41
    neg-int v0, v0

    .line 42
    mul-int/lit8 v0, v0, 0x2

    .line 43
    .line 44
    iget v2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇808〇:I

    .line 45
    .line 46
    neg-int v2, v2

    .line 47
    mul-int/lit8 v2, v2, 0x2

    .line 48
    .line 49
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 50
    .line 51
    .line 52
    return-object v1
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected 〇O〇(F)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o800o8O:F

    .line 2
    .line 3
    div-float v0, p1, v0

    .line 4
    .line 5
    const/4 v1, 0x1

    .line 6
    invoke-virtual {p0, p1, v0, v1}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O00(FFZ)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇oOO8O8(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇808〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eq v0, p1, :cond_0

    .line 6
    .line 7
    iget p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇00:I

    .line 8
    .line 9
    xor-int/lit8 p1, p1, 0x2

    .line 10
    .line 11
    iput p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇00:I

    .line 12
    .line 13
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 14
    .line 15
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇oo〇(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->OO0o〇〇〇〇0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eq v0, p1, :cond_1

    .line 6
    .line 7
    iget v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇00:I

    .line 8
    .line 9
    xor-int/lit8 v0, v0, 0x4

    .line 10
    .line 11
    iput v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇00:I

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->Oo08:Lcom/intsig/camscanner/watermark/EditableInterface;

    .line 14
    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    if-eqz p1, :cond_0

    .line 18
    .line 19
    invoke-interface {v0}, Lcom/intsig/camscanner/watermark/EditableInterface;->Oo08()V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-interface {v0}, Lcom/intsig/camscanner/watermark/EditableInterface;->O8()V

    .line 24
    .line 25
    .line 26
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 27
    .line 28
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected 〇o〇(Landroid/graphics/Canvas;)V
    .locals 11

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O00:Z

    .line 2
    .line 3
    if-nez v0, :cond_3

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 6
    .line 7
    if-eqz v0, :cond_3

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->Oo08:Lcom/intsig/camscanner/watermark/EditableInterface;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 14
    .line 15
    iget v2, v1, Landroid/graphics/RectF;->left:F

    .line 16
    .line 17
    iget v3, v1, Landroid/graphics/RectF;->top:F

    .line 18
    .line 19
    iget v4, v1, Landroid/graphics/RectF;->right:F

    .line 20
    .line 21
    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    .line 22
    .line 23
    invoke-interface {v0, v2, v3, v4, v1}, Lcom/intsig/camscanner/watermark/EditableInterface;->〇o00〇〇Oo(FFFF)V

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->O8:Lcom/intsig/camscanner/watermark/TextInterface;

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 30
    .line 31
    iget v2, v1, Landroid/graphics/RectF;->left:F

    .line 32
    .line 33
    float-to-int v2, v2

    .line 34
    iget v3, v1, Landroid/graphics/RectF;->top:F

    .line 35
    .line 36
    float-to-int v3, v3

    .line 37
    iget v4, v1, Landroid/graphics/RectF;->right:F

    .line 38
    .line 39
    float-to-int v4, v4

    .line 40
    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    .line 41
    .line 42
    float-to-int v1, v1

    .line 43
    invoke-interface {v0, v2, v3, v4, v1}, Lcom/intsig/camscanner/watermark/TextInterface;->setBounds(IIII)V

    .line 44
    .line 45
    .line 46
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o00〇〇Oo()V

    .line 47
    .line 48
    .line 49
    new-instance v0, Landroid/graphics/RectF;

    .line 50
    .line 51
    iget-object v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇888:Landroid/graphics/RectF;

    .line 52
    .line 53
    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 54
    .line 55
    .line 56
    iget v1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->OoO8:I

    .line 57
    .line 58
    neg-int v2, v1

    .line 59
    int-to-float v2, v2

    .line 60
    neg-int v1, v1

    .line 61
    int-to-float v1, v1

    .line 62
    invoke-virtual {v0, v2, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    iget-object v2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 70
    .line 71
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 72
    .line 73
    .line 74
    iget-object v2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O8o08O:Landroid/graphics/Path;

    .line 75
    .line 76
    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 77
    .line 78
    .line 79
    iget-object v2, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O8o08O:Landroid/graphics/Path;

    .line 80
    .line 81
    iget v3, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇0〇O0088o:I

    .line 82
    .line 83
    int-to-float v4, v3

    .line 84
    int-to-float v3, v3

    .line 85
    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 86
    .line 87
    invoke-virtual {v2, v0, v4, v3, v5}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {p0}, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇808〇()Z

    .line 91
    .line 92
    .line 93
    move-result v2

    .line 94
    invoke-direct {p0, p1, v2}, Lcom/intsig/camscanner/watermark/WaterMarkView;->O8(Landroid/graphics/Canvas;Z)V

    .line 95
    .line 96
    .line 97
    if-eqz v2, :cond_2

    .line 98
    .line 99
    iget-boolean v3, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇oo〇:Z

    .line 100
    .line 101
    if-eqz v3, :cond_2

    .line 102
    .line 103
    iget v3, v0, Landroid/graphics/RectF;->left:F

    .line 104
    .line 105
    float-to-int v3, v3

    .line 106
    iget v4, v0, Landroid/graphics/RectF;->right:F

    .line 107
    .line 108
    float-to-int v4, v4

    .line 109
    iget v5, v0, Landroid/graphics/RectF;->top:F

    .line 110
    .line 111
    float-to-int v5, v5

    .line 112
    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    .line 113
    .line 114
    float-to-int v0, v0

    .line 115
    iget-object v6, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o00〇〇Oo:Landroid/graphics/drawable/Drawable;

    .line 116
    .line 117
    if-eqz v6, :cond_1

    .line 118
    .line 119
    iget v7, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O〇:I

    .line 120
    .line 121
    sub-int v8, v4, v7

    .line 122
    .line 123
    iget v9, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇808〇:I

    .line 124
    .line 125
    sub-int v10, v0, v9

    .line 126
    .line 127
    add-int/2addr v4, v7

    .line 128
    add-int/2addr v0, v9

    .line 129
    invoke-virtual {v6, v8, v10, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 130
    .line 131
    .line 132
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o00〇〇Oo:Landroid/graphics/drawable/Drawable;

    .line 133
    .line 134
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 135
    .line 136
    .line 137
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇080:Landroid/graphics/drawable/Drawable;

    .line 138
    .line 139
    if-eqz v0, :cond_2

    .line 140
    .line 141
    iget-boolean v4, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇O8〇〇o:Z

    .line 142
    .line 143
    if-eqz v4, :cond_2

    .line 144
    .line 145
    iget v4, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇O〇:I

    .line 146
    .line 147
    sub-int v6, v3, v4

    .line 148
    .line 149
    iget v7, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇〇808〇:I

    .line 150
    .line 151
    sub-int v8, v5, v7

    .line 152
    .line 153
    add-int/2addr v3, v4

    .line 154
    add-int/2addr v5, v7

    .line 155
    invoke-virtual {v0, v6, v8, v3, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 156
    .line 157
    .line 158
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇080:Landroid/graphics/drawable/Drawable;

    .line 159
    .line 160
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 161
    .line 162
    .line 163
    :cond_2
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 164
    .line 165
    .line 166
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->Oo08:Lcom/intsig/camscanner/watermark/EditableInterface;

    .line 167
    .line 168
    if-eqz p1, :cond_3

    .line 169
    .line 170
    if-eqz v2, :cond_3

    .line 171
    .line 172
    invoke-interface {p1}, Lcom/intsig/camscanner/watermark/EditableInterface;->〇080()Z

    .line 173
    .line 174
    .line 175
    move-result p1

    .line 176
    if-eqz p1, :cond_3

    .line 177
    .line 178
    iget-object p1, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇o〇:Landroid/view/View;

    .line 179
    .line 180
    const-wide/16 v0, 0x12c

    .line 181
    .line 182
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->postInvalidateDelayed(J)V

    .line 183
    .line 184
    .line 185
    :cond_3
    return-void
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public 〇〇808〇()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->〇00:I

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    and-int/2addr v0, v1

    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888()Landroid/graphics/RectF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/watermark/WaterMarkView;->o〇0:Landroid/graphics/RectF;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

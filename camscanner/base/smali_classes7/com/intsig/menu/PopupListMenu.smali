.class public Lcom/intsig/menu/PopupListMenu;
.super Ljava/lang/Object;
.source "PopupListMenu.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/menu/PopupListMenu$MenuItemClickListener;,
        Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;
    }
.end annotation


# instance fields
.field private O8:Landroid/view/View;

.field private OO0o〇〇:F

.field private OO0o〇〇〇〇0:Z

.field private Oo08:Lcom/intsig/menu/PopupMenuItems;

.field private OoO8:I

.field private Oooo8o0〇:Landroid/text/TextPaint;

.field private o800o8O:Z

.field private oO80:Landroid/content/Context;

.field private o〇0:Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;

.field public 〇080:Landroid/widget/PopupWindow;

.field private 〇0〇O0088o:I

.field private 〇80〇808〇O:Z

.field private 〇8o8o〇:I

.field private 〇O00:I

.field private 〇O888o0o:Lcom/intsig/menu/PopupListMenu$MenuItemClickListener;

.field private 〇O8o08O:F

.field private 〇O〇:F

.field private 〇o00〇〇Oo:Landroid/graphics/drawable/Drawable;

.field private 〇o〇:Lcom/intsig/view/PopupListView;

.field private 〇〇808〇:F

.field private 〇〇888:Z

.field private 〇〇8O0〇8:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/intsig/menu/PopupMenuItems;ZZ)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    .line 2
    invoke-direct/range {v0 .. v5}, Lcom/intsig/menu/PopupListMenu;-><init>(Landroid/content/Context;Lcom/intsig/menu/PopupMenuItems;ZZLandroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/intsig/menu/PopupMenuItems;ZZLandroid/view/View;)V
    .locals 2

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/intsig/menu/PopupListMenu;->〇〇888:Z

    const/high16 v1, -0x1000000

    .line 5
    iput v1, p0, Lcom/intsig/menu/PopupListMenu;->〇8o8o〇:I

    const/4 v1, 0x0

    .line 6
    iput v1, p0, Lcom/intsig/menu/PopupListMenu;->〇0〇O0088o:I

    .line 7
    iput v1, p0, Lcom/intsig/menu/PopupListMenu;->OoO8:I

    .line 8
    iput-boolean v0, p0, Lcom/intsig/menu/PopupListMenu;->o800o8O:Z

    .line 9
    iput-object p1, p0, Lcom/intsig/menu/PopupListMenu;->oO80:Landroid/content/Context;

    .line 10
    iput-object p5, p0, Lcom/intsig/menu/PopupListMenu;->O8:Landroid/view/View;

    .line 11
    iput-boolean p3, p0, Lcom/intsig/menu/PopupListMenu;->〇80〇808〇O:Z

    .line 12
    iput-boolean p4, p0, Lcom/intsig/menu/PopupListMenu;->OO0o〇〇〇〇0:Z

    .line 13
    iput-object p2, p0, Lcom/intsig/menu/PopupListMenu;->Oo08:Lcom/intsig/menu/PopupMenuItems;

    .line 14
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 15
    iget-object p2, p0, Lcom/intsig/menu/PopupListMenu;->Oo08:Lcom/intsig/menu/PopupMenuItems;

    invoke-virtual {p2}, Lcom/intsig/menu/PopupMenuItems;->OO0o〇〇()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 16
    sget p2, Lcom/intsig/comm/R$dimen;->popup_menu_icon_size:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/intsig/menu/PopupListMenu;->〇0〇O0088o:I

    .line 17
    sget p3, Lcom/intsig/comm/R$dimen;->popup_menu_icon_start_margin:I

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    add-int/2addr p2, p3

    iput p2, p0, Lcom/intsig/menu/PopupListMenu;->〇0〇O0088o:I

    .line 18
    sget p3, Lcom/intsig/comm/R$dimen;->popup_menu_icon_end_margin:I

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    add-int/2addr p2, p3

    iput p2, p0, Lcom/intsig/menu/PopupListMenu;->〇0〇O0088o:I

    .line 19
    :cond_0
    sget p2, Lcom/intsig/comm/R$dimen;->popup_menu_text_padding:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    mul-int/lit8 p2, p2, 0x2

    sget p3, Lcom/intsig/comm/R$dimen;->popup_menu_item_text_extra_padding:I

    .line 20
    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    add-int/2addr p2, p1

    iput p2, p0, Lcom/intsig/menu/PopupListMenu;->OoO8:I

    .line 21
    invoke-direct {p0}, Lcom/intsig/menu/PopupListMenu;->Oooo8o0〇()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZ)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/menu/PopupMenuItems;

    invoke-direct {v0, p1}, Lcom/intsig/menu/PopupMenuItems;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/intsig/menu/PopupListMenu;-><init>(Landroid/content/Context;Lcom/intsig/menu/PopupMenuItems;ZZ)V

    return-void
.end method

.method static bridge synthetic O8(Lcom/intsig/menu/PopupListMenu;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/menu/PopupListMenu;->oO80:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private OO0o〇〇(IF)F
    .locals 0

    .line 1
    packed-switch p1, :pswitch_data_0

    .line 2
    .line 3
    .line 4
    :pswitch_0
    iget p1, p0, Lcom/intsig/menu/PopupListMenu;->〇〇808〇:F

    .line 5
    .line 6
    neg-float p1, p1

    .line 7
    return p1

    .line 8
    :pswitch_1
    neg-float p1, p2

    .line 9
    const/high16 p2, 0x40800000    # 4.0f

    .line 10
    .line 11
    mul-float p1, p1, p2

    .line 12
    .line 13
    const/high16 p2, 0x40400000    # 3.0f

    .line 14
    .line 15
    div-float/2addr p1, p2

    .line 16
    return p1

    .line 17
    :pswitch_2
    iget p1, p0, Lcom/intsig/menu/PopupListMenu;->〇O8o08O:F

    .line 18
    .line 19
    neg-float p1, p1

    .line 20
    iget p2, p0, Lcom/intsig/menu/PopupListMenu;->〇〇8O0〇8:I

    .line 21
    .line 22
    int-to-float p2, p2

    .line 23
    sub-float/2addr p1, p2

    .line 24
    return p1

    .line 25
    :pswitch_3
    neg-float p1, p2

    .line 26
    return p1

    .line 27
    :pswitch_4
    iget p1, p0, Lcom/intsig/menu/PopupListMenu;->〇O8o08O:F

    .line 28
    .line 29
    neg-float p1, p1

    .line 30
    sub-float/2addr p1, p2

    .line 31
    return p1

    .line 32
    :pswitch_5
    iget p1, p0, Lcom/intsig/menu/PopupListMenu;->〇〇808〇:F

    .line 33
    .line 34
    neg-float p1, p1

    .line 35
    return p1

    .line 36
    :pswitch_6
    iget p1, p0, Lcom/intsig/menu/PopupListMenu;->〇〇808〇:F

    .line 37
    .line 38
    neg-float p1, p1

    .line 39
    return p1

    .line 40
    :pswitch_7
    iget p1, p0, Lcom/intsig/menu/PopupListMenu;->〇O8o08O:F

    .line 41
    .line 42
    neg-float p1, p1

    .line 43
    sub-float/2addr p1, p2

    .line 44
    return p1

    .line 45
    :pswitch_8
    iget p1, p0, Lcom/intsig/menu/PopupListMenu;->〇O8o08O:F

    .line 46
    .line 47
    neg-float p1, p1

    .line 48
    sub-float/2addr p1, p2

    .line 49
    return p1

    .line 50
    nop

    .line 51
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 52
    .line 53
.end method

.method static bridge synthetic Oo08(Lcom/intsig/menu/PopupListMenu;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/menu/PopupListMenu;->〇〇888:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private Oooo8o0〇()V
    .locals 5

    .line 1
    const/4 v0, 0x4

    .line 2
    iput v0, p0, Lcom/intsig/menu/PopupListMenu;->〇O00:I

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->oO80:Landroid/content/Context;

    .line 5
    .line 6
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    sget v1, Lcom/intsig/comm/R$dimen;->popup_menu_offset:I

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    int-to-float v0, v0

    .line 17
    iput v0, p0, Lcom/intsig/menu/PopupListMenu;->〇〇808〇:F

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->oO80:Landroid/content/Context;

    .line 20
    .line 21
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    sget v1, Lcom/intsig/comm/R$dimen;->popup_menu_bottom_offset:I

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    iput v0, p0, Lcom/intsig/menu/PopupListMenu;->〇〇8O0〇8:I

    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->oO80:Landroid/content/Context;

    .line 34
    .line 35
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    sget v1, Lcom/intsig/comm/R$dimen;->popup_menu_min_width:I

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    int-to-float v0, v0

    .line 46
    iput v0, p0, Lcom/intsig/menu/PopupListMenu;->〇O〇:F

    .line 47
    .line 48
    iget-boolean v0, p0, Lcom/intsig/menu/PopupListMenu;->OO0o〇〇〇〇0:Z

    .line 49
    .line 50
    if-eqz v0, :cond_0

    .line 51
    .line 52
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    .line 53
    .line 54
    iget-object v1, p0, Lcom/intsig/menu/PopupListMenu;->oO80:Landroid/content/Context;

    .line 55
    .line 56
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    sget v2, Lcom/intsig/comm/R$color;->bg_menu_black_style:I

    .line 61
    .line 62
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 67
    .line 68
    .line 69
    iput-object v0, p0, Lcom/intsig/menu/PopupListMenu;->〇o00〇〇Oo:Landroid/graphics/drawable/Drawable;

    .line 70
    .line 71
    const/4 v0, -0x1

    .line 72
    iput v0, p0, Lcom/intsig/menu/PopupListMenu;->〇8o8o〇:I

    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/menu/PopupListMenu;->〇80〇808〇O:Z

    .line 76
    .line 77
    if-eqz v0, :cond_1

    .line 78
    .line 79
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->oO80:Landroid/content/Context;

    .line 80
    .line 81
    sget v1, Lcom/intsig/comm/R$drawable;->bg_alert_dialog_common:I

    .line 82
    .line 83
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    iput-object v0, p0, Lcom/intsig/menu/PopupListMenu;->〇o00〇〇Oo:Landroid/graphics/drawable/Drawable;

    .line 88
    .line 89
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->oO80:Landroid/content/Context;

    .line 90
    .line 91
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    sget v1, Lcom/intsig/comm/R$color;->cs_color_text_4:I

    .line 96
    .line 97
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 98
    .line 99
    .line 100
    move-result v0

    .line 101
    iput v0, p0, Lcom/intsig/menu/PopupListMenu;->〇8o8o〇:I

    .line 102
    .line 103
    goto :goto_0

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->oO80:Landroid/content/Context;

    .line 105
    .line 106
    sget v1, Lcom/intsig/comm/R$drawable;->bg_alert_dialog_common:I

    .line 107
    .line 108
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    iput-object v0, p0, Lcom/intsig/menu/PopupListMenu;->〇o00〇〇Oo:Landroid/graphics/drawable/Drawable;

    .line 113
    .line 114
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->oO80:Landroid/content/Context;

    .line 115
    .line 116
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    sget v1, Lcom/intsig/comm/R$color;->pop_tags_item_no_selected:I

    .line 121
    .line 122
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 123
    .line 124
    .line 125
    move-result v0

    .line 126
    iput v0, p0, Lcom/intsig/menu/PopupListMenu;->〇8o8o〇:I

    .line 127
    .line 128
    :goto_0
    invoke-direct {p0}, Lcom/intsig/menu/PopupListMenu;->oo88o8O()V

    .line 129
    .line 130
    .line 131
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->oO80:Landroid/content/Context;

    .line 132
    .line 133
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 134
    .line 135
    .line 136
    move-result-object v0

    .line 137
    sget v1, Lcom/intsig/comm/R$layout;->popup_list_menu_listview:I

    .line 138
    .line 139
    const/4 v2, 0x0

    .line 140
    const/4 v3, 0x0

    .line 141
    invoke-virtual {v0, v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 142
    .line 143
    .line 144
    move-result-object v0

    .line 145
    new-instance v1, Landroid/widget/PopupWindow;

    .line 146
    .line 147
    const/4 v2, -0x2

    .line 148
    const/4 v4, 0x1

    .line 149
    invoke-direct {v1, v0, v2, v2, v4}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    .line 150
    .line 151
    .line 152
    iput-object v1, p0, Lcom/intsig/menu/PopupListMenu;->〇080:Landroid/widget/PopupWindow;

    .line 153
    .line 154
    iget-object v2, p0, Lcom/intsig/menu/PopupListMenu;->〇o00〇〇Oo:Landroid/graphics/drawable/Drawable;

    .line 155
    .line 156
    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 157
    .line 158
    .line 159
    iget-object v1, p0, Lcom/intsig/menu/PopupListMenu;->〇080:Landroid/widget/PopupWindow;

    .line 160
    .line 161
    invoke-virtual {v1, v4}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 162
    .line 163
    .line 164
    iget-object v1, p0, Lcom/intsig/menu/PopupListMenu;->〇080:Landroid/widget/PopupWindow;

    .line 165
    .line 166
    invoke-virtual {v1, v4}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 167
    .line 168
    .line 169
    iget-object v1, p0, Lcom/intsig/menu/PopupListMenu;->〇080:Landroid/widget/PopupWindow;

    .line 170
    .line 171
    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    .line 172
    .line 173
    .line 174
    move-result-object v1

    .line 175
    new-instance v2, Lcom/intsig/menu/PopupListMenu$1;

    .line 176
    .line 177
    invoke-direct {v2, p0}, Lcom/intsig/menu/PopupListMenu$1;-><init>(Lcom/intsig/menu/PopupListMenu;)V

    .line 178
    .line 179
    .line 180
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 181
    .line 182
    .line 183
    sget v1, Lcom/intsig/comm/R$id;->popup_menu_listview:I

    .line 184
    .line 185
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 186
    .line 187
    .line 188
    move-result-object v0

    .line 189
    check-cast v0, Lcom/intsig/view/PopupListView;

    .line 190
    .line 191
    iput-object v0, p0, Lcom/intsig/menu/PopupListMenu;->〇o〇:Lcom/intsig/view/PopupListView;

    .line 192
    .line 193
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 194
    .line 195
    .line 196
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->O8:Landroid/view/View;

    .line 197
    .line 198
    if-eqz v0, :cond_2

    .line 199
    .line 200
    iget-object v1, p0, Lcom/intsig/menu/PopupListMenu;->〇o〇:Lcom/intsig/view/PopupListView;

    .line 201
    .line 202
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 203
    .line 204
    .line 205
    :cond_2
    new-instance v0, Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;

    .line 206
    .line 207
    invoke-direct {v0, p0}, Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;-><init>(Lcom/intsig/menu/PopupListMenu;)V

    .line 208
    .line 209
    .line 210
    iput-object v0, p0, Lcom/intsig/menu/PopupListMenu;->o〇0:Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;

    .line 211
    .line 212
    iget-object v1, p0, Lcom/intsig/menu/PopupListMenu;->〇o〇:Lcom/intsig/view/PopupListView;

    .line 213
    .line 214
    invoke-virtual {v1, v0}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 215
    .line 216
    .line 217
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->〇o〇:Lcom/intsig/view/PopupListView;

    .line 218
    .line 219
    new-instance v1, Lcom/intsig/menu/PopupListMenu$2;

    .line 220
    .line 221
    invoke-direct {v1, p0}, Lcom/intsig/menu/PopupListMenu$2;-><init>(Lcom/intsig/menu/PopupListMenu;)V

    .line 222
    .line 223
    .line 224
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 225
    .line 226
    .line 227
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->〇o〇:Lcom/intsig/view/PopupListView;

    .line 228
    .line 229
    new-instance v1, Lcom/intsig/menu/PopupListMenu$3;

    .line 230
    .line 231
    invoke-direct {v1, p0}, Lcom/intsig/menu/PopupListMenu$3;-><init>(Lcom/intsig/menu/PopupListMenu;)V

    .line 232
    .line 233
    .line 234
    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 235
    .line 236
    .line 237
    return-void
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method static bridge synthetic oO80(Lcom/intsig/menu/PopupListMenu;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/menu/PopupListMenu;->〇〇888:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private oo88o8O()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->oO80:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sget v1, Lcom/intsig/comm/R$layout;->popup_list_menu_item:I

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    const/4 v3, 0x0

    .line 11
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sget v1, Lcom/intsig/comm/R$id;->title:I

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Landroid/widget/TextView;

    .line 22
    .line 23
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    iput-object v0, p0, Lcom/intsig/menu/PopupListMenu;->Oooo8o0〇:Landroid/text/TextPaint;

    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic o〇0(Lcom/intsig/menu/PopupListMenu;)Lcom/intsig/menu/PopupMenuItems;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/menu/PopupListMenu;->Oo08:Lcom/intsig/menu/PopupMenuItems;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇080(Lcom/intsig/menu/PopupListMenu;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/menu/PopupListMenu;->OO0o〇〇〇〇0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇8o8o〇()Ljava/lang/String;
    .locals 5

    .line 1
    const-string v0, ""

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    :goto_0
    iget-object v2, p0, Lcom/intsig/menu/PopupListMenu;->o〇0:Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;

    .line 5
    .line 6
    invoke-virtual {v2}, Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;->getCount()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    if-ge v1, v2, :cond_1

    .line 11
    .line 12
    iget-object v2, p0, Lcom/intsig/menu/PopupListMenu;->o〇0:Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;

    .line 13
    .line 14
    invoke-virtual {v2, v1}, Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;->getItem(I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    check-cast v2, Lcom/intsig/menu/MenuItem;

    .line 19
    .line 20
    invoke-virtual {v2}, Lcom/intsig/menu/MenuItem;->oO80()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 29
    .line 30
    .line 31
    move-result v4

    .line 32
    if-le v3, v4, :cond_0

    .line 33
    .line 34
    invoke-virtual {v2}, Lcom/intsig/menu/MenuItem;->oO80()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    return-object v0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private 〇O8o08O(IF)F
    .locals 3

    .line 1
    const v0, 0x3fb33333    # 1.4f

    .line 2
    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    const/high16 v2, 0x40000000    # 2.0f

    .line 6
    .line 7
    packed-switch p1, :pswitch_data_0

    .line 8
    .line 9
    .line 10
    :pswitch_0
    iget p1, p0, Lcom/intsig/menu/PopupListMenu;->OO0o〇〇:F

    .line 11
    .line 12
    neg-float p1, p1

    .line 13
    add-float/2addr p1, p2

    .line 14
    return p1

    .line 15
    :pswitch_1
    iget p1, p0, Lcom/intsig/menu/PopupListMenu;->OO0o〇〇:F

    .line 16
    .line 17
    neg-float p1, p1

    .line 18
    div-float/2addr p1, v0

    .line 19
    return p1

    .line 20
    :pswitch_2
    return v1

    .line 21
    :pswitch_3
    iget p1, p0, Lcom/intsig/menu/PopupListMenu;->OO0o〇〇:F

    .line 22
    .line 23
    neg-float p1, p1

    .line 24
    div-float/2addr p1, v0

    .line 25
    return p1

    .line 26
    :pswitch_4
    iget p1, p0, Lcom/intsig/menu/PopupListMenu;->OO0o〇〇:F

    .line 27
    .line 28
    neg-float p1, p1

    .line 29
    div-float/2addr p1, v2

    .line 30
    div-float/2addr p2, v2

    .line 31
    add-float/2addr p1, p2

    .line 32
    return p1

    .line 33
    :pswitch_5
    iget p1, p0, Lcom/intsig/menu/PopupListMenu;->OO0o〇〇:F

    .line 34
    .line 35
    neg-float p1, p1

    .line 36
    div-float/2addr p1, v2

    .line 37
    div-float/2addr p2, v2

    .line 38
    add-float/2addr p1, p2

    .line 39
    return p1

    .line 40
    :pswitch_6
    return v1

    .line 41
    :pswitch_7
    iget p1, p0, Lcom/intsig/menu/PopupListMenu;->OO0o〇〇:F

    .line 42
    .line 43
    neg-float p1, p1

    .line 44
    add-float/2addr p1, p2

    .line 45
    return p1

    .line 46
    nop

    .line 47
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇O〇(ILandroid/view/View;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->oO80:Landroid/content/Context;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sget v1, Lcom/intsig/comm/R$dimen;->popup_menu_item_height:I

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iget-object v1, p0, Lcom/intsig/menu/PopupListMenu;->〇o〇:Lcom/intsig/view/PopupListView;

    .line 14
    .line 15
    invoke-virtual {v1}, Landroid/widget/ListView;->getDividerHeight()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    add-int/2addr v0, v1

    .line 20
    iget-object v1, p0, Lcom/intsig/menu/PopupListMenu;->o〇0:Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;

    .line 21
    .line 22
    invoke-virtual {v1}, Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;->getCount()I

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    mul-int v1, v1, v0

    .line 27
    .line 28
    int-to-float v1, v1

    .line 29
    iget v2, p0, Lcom/intsig/menu/PopupListMenu;->〇〇808〇:F

    .line 30
    .line 31
    add-float/2addr v1, v2

    .line 32
    iput v1, p0, Lcom/intsig/menu/PopupListMenu;->〇O8o08O:F

    .line 33
    .line 34
    const/4 v1, 0x2

    .line 35
    const/4 v2, 0x1

    .line 36
    if-eq p1, v2, :cond_0

    .line 37
    .line 38
    if-ne p1, v1, :cond_1

    .line 39
    .line 40
    :cond_0
    new-array p1, v1, [I

    .line 41
    .line 42
    invoke-virtual {p2, p1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 43
    .line 44
    .line 45
    aget p1, p1, v2

    .line 46
    .line 47
    int-to-float p1, p1

    .line 48
    iget p2, p0, Lcom/intsig/menu/PopupListMenu;->〇O8o08O:F

    .line 49
    .line 50
    cmpl-float p2, p2, p1

    .line 51
    .line 52
    if-lez p2, :cond_1

    .line 53
    .line 54
    int-to-float p2, v0

    .line 55
    sub-float/2addr p1, p2

    .line 56
    iput p1, p0, Lcom/intsig/menu/PopupListMenu;->〇O8o08O:F

    .line 57
    .line 58
    iget-object p2, p0, Lcom/intsig/menu/PopupListMenu;->〇080:Landroid/widget/PopupWindow;

    .line 59
    .line 60
    float-to-int p1, p1

    .line 61
    invoke-virtual {p2, p1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 62
    .line 63
    .line 64
    :cond_1
    invoke-direct {p0}, Lcom/intsig/menu/PopupListMenu;->〇8o8o〇()Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    iget-object p2, p0, Lcom/intsig/menu/PopupListMenu;->Oooo8o0〇:Landroid/text/TextPaint;

    .line 69
    .line 70
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 71
    .line 72
    .line 73
    move-result p1

    .line 74
    float-to-int p1, p1

    .line 75
    iget p2, p0, Lcom/intsig/menu/PopupListMenu;->〇0〇O0088o:I

    .line 76
    .line 77
    add-int/2addr p2, p1

    .line 78
    iget p1, p0, Lcom/intsig/menu/PopupListMenu;->OoO8:I

    .line 79
    .line 80
    add-int/2addr p2, p1

    .line 81
    int-to-float p1, p2

    .line 82
    iput p1, p0, Lcom/intsig/menu/PopupListMenu;->OO0o〇〇:F

    .line 83
    .line 84
    iget p2, p0, Lcom/intsig/menu/PopupListMenu;->〇O〇:F

    .line 85
    .line 86
    cmpg-float p1, p1, p2

    .line 87
    .line 88
    if-gez p1, :cond_2

    .line 89
    .line 90
    iput p2, p0, Lcom/intsig/menu/PopupListMenu;->OO0o〇〇:F

    .line 91
    .line 92
    :cond_2
    iget-object p1, p0, Lcom/intsig/menu/PopupListMenu;->〇o〇:Lcom/intsig/view/PopupListView;

    .line 93
    .line 94
    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 95
    .line 96
    iget v0, p0, Lcom/intsig/menu/PopupListMenu;->OO0o〇〇:F

    .line 97
    .line 98
    float-to-int v0, v0

    .line 99
    const/4 v1, -0x2

    .line 100
    invoke-direct {p2, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 101
    .line 102
    .line 103
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 104
    .line 105
    .line 106
    return-void
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/menu/PopupListMenu;)Lcom/intsig/menu/PopupListMenu$MenuItemClickListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/menu/PopupListMenu;->〇O888o0o:Lcom/intsig/menu/PopupListMenu$MenuItemClickListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/menu/PopupListMenu;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/menu/PopupListMenu;->〇8o8o〇:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/menu/PopupListMenu;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/menu/PopupListMenu;->o800o8O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇〇8O0〇8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/menu/PopupListMenu;->〇〇888:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public O8ooOoo〇(Landroid/view/View;I)V
    .locals 0

    .line 1
    iput p2, p0, Lcom/intsig/menu/PopupListMenu;->〇O00:I

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/menu/PopupListMenu;->O〇8O8〇008(Landroid/view/View;I)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x0

    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/menu/PopupListMenu;->〇〇8O0〇8(Z)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public OO0o〇〇〇〇0()Z
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/intsig/menu/PopupListMenu;->〇080:Landroid/widget/PopupWindow;

    .line 3
    .line 4
    if-eqz v1, :cond_0

    .line 5
    .line 6
    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/menu/PopupListMenu;->〇080:Landroid/widget/PopupWindow;

    .line 13
    .line 14
    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 15
    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/menu/PopupListMenu;->〇080:Landroid/widget/PopupWindow;

    .line 18
    .line 19
    invoke-virtual {v1, v0}, Landroid/widget/PopupWindow;->setFocusable(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    goto :goto_0

    .line 24
    :catch_0
    move-exception v1

    .line 25
    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    :cond_0
    :goto_0
    return v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public OoO8(I)V
    .locals 2

    .line 1
    iput p1, p0, Lcom/intsig/menu/PopupListMenu;->〇O00:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->〇080:Landroid/widget/PopupWindow;

    .line 4
    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    const/4 v1, 0x7

    .line 8
    if-ne p1, v1, :cond_0

    .line 9
    .line 10
    sget p1, Lcom/intsig/comm/R$style;->head_popwin_anim_style:I

    .line 11
    .line 12
    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 13
    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/16 v1, 0x8

    .line 17
    .line 18
    if-ne p1, v1, :cond_1

    .line 19
    .line 20
    sget p1, Lcom/intsig/comm/R$style;->bottom_popwin_anim_style:I

    .line 21
    .line 22
    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 23
    .line 24
    .line 25
    :cond_1
    :goto_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public O〇8O8〇008(Landroid/view/View;I)V
    .locals 6

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->o〇0:Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;->notifyDataSetChanged()V

    .line 7
    .line 8
    .line 9
    iput p2, p0, Lcom/intsig/menu/PopupListMenu;->〇O00:I

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/menu/PopupListMenu;->OO0o〇〇〇〇0()Z

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    if-eqz p2, :cond_1

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    iget-object p2, p0, Lcom/intsig/menu/PopupListMenu;->o〇0:Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;

    .line 19
    .line 20
    invoke-virtual {p2}, Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;->getCount()I

    .line 21
    .line 22
    .line 23
    move-result p2

    .line 24
    if-lez p2, :cond_2

    .line 25
    .line 26
    new-instance p2, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v0, "mAnchor.getTop() ="

    .line 32
    .line 33
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    iget p2, p0, Lcom/intsig/menu/PopupListMenu;->〇O00:I

    .line 44
    .line 45
    invoke-direct {p0, p2, p1}, Lcom/intsig/menu/PopupListMenu;->〇O〇(ILandroid/view/View;)V

    .line 46
    .line 47
    .line 48
    iget p2, p0, Lcom/intsig/menu/PopupListMenu;->〇O00:I

    .line 49
    .line 50
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    int-to-float v0, v0

    .line 55
    invoke-direct {p0, p2, v0}, Lcom/intsig/menu/PopupListMenu;->〇O8o08O(IF)F

    .line 56
    .line 57
    .line 58
    move-result p2

    .line 59
    iget v0, p0, Lcom/intsig/menu/PopupListMenu;->〇O00:I

    .line 60
    .line 61
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    int-to-float v1, v1

    .line 66
    invoke-direct {p0, v0, v1}, Lcom/intsig/menu/PopupListMenu;->OO0o〇〇(IF)F

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    const/4 v1, 0x2

    .line 71
    new-array v1, v1, [I

    .line 72
    .line 73
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 74
    .line 75
    .line 76
    const/4 v2, 0x1

    .line 77
    aget v3, v1, v2

    .line 78
    .line 79
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 80
    .line 81
    .line 82
    move-result v4

    .line 83
    add-int/2addr v3, v4

    .line 84
    aput v3, v1, v2

    .line 85
    .line 86
    const/4 v4, 0x0

    .line 87
    aget v5, v1, v4

    .line 88
    .line 89
    float-to-int p2, p2

    .line 90
    add-int/2addr v5, p2

    .line 91
    aput v5, v1, v4

    .line 92
    .line 93
    float-to-int p2, v0

    .line 94
    add-int/2addr v3, p2

    .line 95
    aput v3, v1, v2

    .line 96
    .line 97
    :try_start_0
    iget-object p2, p0, Lcom/intsig/menu/PopupListMenu;->〇080:Landroid/widget/PopupWindow;

    .line 98
    .line 99
    invoke-virtual {p2, p1, v4, v5, v3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 100
    .line 101
    .line 102
    iget-object p2, p0, Lcom/intsig/menu/PopupListMenu;->〇080:Landroid/widget/PopupWindow;

    .line 103
    .line 104
    invoke-virtual {p2, v2}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 105
    .line 106
    .line 107
    iget-object p2, p0, Lcom/intsig/menu/PopupListMenu;->〇080:Landroid/widget/PopupWindow;

    .line 108
    .line 109
    invoke-virtual {p2}, Landroid/widget/PopupWindow;->update()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    .line 111
    .line 112
    :catch_0
    new-instance p2, Ljava/lang/StringBuilder;

    .line 113
    .line 114
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    .line 116
    .line 117
    const-string v1, "mAnchor.offsetY ="

    .line 118
    .line 119
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    const-string v0, ", "

    .line 126
    .line 127
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    .line 129
    .line 130
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 131
    .line 132
    .line 133
    move-result p1

    .line 134
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    :cond_2
    :goto_0
    return-void
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public o800o8O(Lcom/intsig/menu/PopupListMenu$MenuItemClickListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/menu/PopupListMenu;->〇O888o0o:Lcom/intsig/menu/PopupListMenu$MenuItemClickListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public o〇O8〇〇o(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/menu/PopupListMenu;->o800o8O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇00(Landroid/view/View;)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/menu/PopupListMenu;->〇O00:I

    .line 2
    .line 3
    invoke-virtual {p0, p1, v0}, Lcom/intsig/menu/PopupListMenu;->O〇8O8〇008(Landroid/view/View;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇0〇O0088o(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/menu/PopupListMenu;->〇8o8o〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇80〇808〇O(ILjava/lang/String;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->Oo08:Lcom/intsig/menu/PopupMenuItems;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/menu/PopupMenuItems;->〇o〇(ILjava/lang/String;I)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/menu/PopupListMenu;->o〇0:Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;->notifyDataSetChanged()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public 〇O00()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->oO80:Landroid/content/Context;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/menu/PopupListMenu;->Oo08:Lcom/intsig/menu/PopupMenuItems;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/menu/PopupMenuItems;->OO0o〇〇()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    sget v1, Lcom/intsig/comm/R$dimen;->popup_menu_icon_size:I

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    iput v1, p0, Lcom/intsig/menu/PopupListMenu;->〇0〇O0088o:I

    .line 22
    .line 23
    sget v2, Lcom/intsig/comm/R$dimen;->popup_menu_icon_start_margin:I

    .line 24
    .line 25
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    add-int/2addr v1, v2

    .line 30
    iput v1, p0, Lcom/intsig/menu/PopupListMenu;->〇0〇O0088o:I

    .line 31
    .line 32
    sget v2, Lcom/intsig/comm/R$dimen;->popup_menu_icon_end_margin:I

    .line 33
    .line 34
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    add-int/2addr v1, v0

    .line 39
    iput v1, p0, Lcom/intsig/menu/PopupListMenu;->〇0〇O0088o:I

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    const/4 v0, 0x0

    .line 43
    iput v0, p0, Lcom/intsig/menu/PopupListMenu;->〇0〇O0088o:I

    .line 44
    .line 45
    :goto_0
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇O888o0o(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/menu/PopupListMenu;->〇O〇:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇oo〇(Lcom/intsig/menu/PopupMenuItems;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/menu/PopupListMenu;->Oo08:Lcom/intsig/menu/PopupMenuItems;

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/menu/PopupListMenu;->o〇0:Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/menu/PopupListMenu$MenuItemAdapter;->notifyDataSetChanged()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇〇808〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/menu/PopupListMenu;->〇080:Landroid/widget/PopupWindow;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

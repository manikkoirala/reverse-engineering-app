.class public interface abstract Lcom/intsig/issocket/ISSocketAndroidCallback;
.super Ljava/lang/Object;
.source "ISSocketAndroidCallback.java"


# virtual methods
.method public abstract socketAckError(Lcom/intsig/issocket/ISSocketAndroid;II)V
.end method

.method public abstract socketDidConnectedToHost(Lcom/intsig/issocket/ISSocketAndroid;)V
.end method

.method public abstract socketDidFailedWithError(Lcom/intsig/issocket/ISSocketAndroid;I)V
.end method

.method public abstract socketDidFinishLoading(Lcom/intsig/issocket/ISSocketAndroid;)V
.end method

.method public abstract socketDidPingTestToHost(Lcom/intsig/issocket/ISSocketAndroid;Ljava/lang/String;JJ)V
.end method

.method public abstract socketDidReadTimeout(Lcom/intsig/issocket/ISSocketAndroid;)V
.end method

.method public abstract socketDidReceiveAuthenticationChallenge(Lcom/intsig/issocket/ISSocketAndroid;)V
.end method

.method public abstract socketDidReceiveData(Lcom/intsig/issocket/ISSocketAndroid;[BIZ)V
.end method

.method public abstract socketDidResolvedDNS(Lcom/intsig/issocket/ISSocketAndroid;[B)V
.end method

.method public abstract socketDidSendBytes(Lcom/intsig/issocket/ISSocketAndroid;III)V
.end method

.method public abstract socketIsReadyToWriteData(Lcom/intsig/issocket/ISSocketAndroid;)V
.end method

.method public abstract socketWillSendAuthenticationChallenge(Lcom/intsig/issocket/ISSocketAndroid;)V
.end method

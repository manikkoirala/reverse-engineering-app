.class public interface abstract Lcom/intsig/issocket/ISSocketMessagePolicy;
.super Ljava/lang/Object;
.source "ISSocketMessagePolicy.java"


# virtual methods
.method public abstract appVersion()Ljava/lang/String;
.end method

.method public abstract channelDidConnect(Ljava/lang/String;)V
.end method

.method public abstract channelDidDisconnect(Ljava/lang/String;IZ)V
.end method

.method public abstract channelDidReadTimeout(Ljava/lang/String;I)V
.end method

.method public abstract channelDidReceiveJSON(Lorg/json/JSONObject;ILjava/lang/String;)V
.end method

.method public abstract channelDidResolveDNS(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract channelDidStartConnect(Ljava/lang/String;)V
.end method

.method public abstract channelTokenDidExpired(Ljava/lang/String;)V
.end method

.method public abstract channels()[Ljava/lang/String;
.end method

.method public abstract clientApp()Ljava/lang/String;
.end method

.method public abstract country()Ljava/lang/String;
.end method

.method public abstract deviceIDForChannel()Ljava/lang/String;
.end method

.method public abstract hostForChannel(Ljava/lang/String;)[Ljava/lang/String;
.end method

.method public abstract isChannelAnonymous(Ljava/lang/String;)Z
.end method

.method public abstract isChannelMonopolize(Ljava/lang/String;)Z
.end method

.method public abstract isInternetConnectionAvailable()Z
.end method

.method public abstract language()Ljava/lang/String;
.end method

.method public abstract platform()I
.end method

.method public abstract product(Ljava/lang/String;)I
.end method

.method public abstract productProtocolVersion(Ljava/lang/String;)I
.end method

.method public abstract userIDForChannel()Ljava/lang/String;
.end method

.method public abstract userTokenForChannel()Ljava/lang/String;
.end method

.class public Lcom/intsig/issocket/ISSocketAndroid;
.super Ljava/lang/Object;
.source "ISSocketAndroid.java"

# interfaces
.implements Lcom/intsig/issocket/ISSocketAndroidCallback;


# static fields
.field public static ISSOCKET_AES_EXCHANGE_ERROR:I = -0x2

.field public static ISSOCKET_CONNECT_FAILED:I = -0x1

.field public static ISSOCKET_DATA_MALFORM:I = -0x9

.field public static ISSOCKET_DECRYPT_FAILED:I = -0x8

.field public static ISSOCKET_DEFAULT_ACK_TIMEOUT:J = 0x0L

.field public static ISSOCKET_DNS_RESOLVE_FAILED:I = -0x5

.field public static ISSOCKET_EOF_REACHED:I = -0x10

.field public static ISSOCKET_LOGIN_AT_ANOTHER_PLACE:I = -0xd

.field public static ISSOCKET_NETWORK_DOWN:I = -0xa

.field public static ISSOCKET_NETWORK_UNREACH:I = -0xb

.field public static ISSOCKET_NO_CONTEXT_ERROR:I = -0xf

.field public static ISSOCKET_NO_ERROR:I = 0x0

.field public static ISSOCKET_READ_TIMEOUT:I = -0x7

.field public static ISSOCKET_SERVER_BUSY:I = -0xc

.field public static ISSOCKET_TOKEN_EXPIRED:I = -0x3

.field public static ISSOCKET_UID_TOKEN_INVALID:I = -0x4

.field public static ISSOCKET_UNKNOWN_ERROR:I = -0xe

.field public static ISSOCKET_WRITE_TIMEOUT:I = -0x6

.field private static final cb_thread:Lcom/intsig/issocket/ISSocketCallbackLooperThread;


# instance fields
.field private SO_ANONYMOUS:I

.field private SO_MONOPOLIZE:I

.field private SO_PINGTEST:I

.field private cb:Lcom/intsig/issocket/ISSocketAndroidCallback;

.field private closedByClient:Z

.field private context:J

.field private cycleRetain:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/intsig/issocket/ISSocketAndroid;",
            ">;"
        }
    .end annotation
.end field

.field private host:Ljava/lang/String;

.field private port:I

.field private readTimeout:J

.field public server:Ljava/lang/String;

.field private uuid:Ljava/lang/String;

.field private writeTimeout:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const-string v0, "issocket_android"

    .line 2
    .line 3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/intsig/issocket/ISSocketCallbackLooperThread;

    .line 7
    .line 8
    invoke-direct {v0}, Lcom/intsig/issocket/ISSocketCallbackLooperThread;-><init>()V

    .line 9
    .line 10
    .line 11
    sput-object v0, Lcom/intsig/issocket/ISSocketAndroid;->cb_thread:Lcom/intsig/issocket/ISSocketCallbackLooperThread;

    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 14
    .line 15
    .line 16
    const-wide/16 v0, 0x3c

    .line 17
    .line 18
    sput-wide v0, Lcom/intsig/issocket/ISSocketAndroid;->ISSOCKET_DEFAULT_ACK_TIMEOUT:J

    .line 19
    .line 20
    return-void
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x2

    .line 5
    iput v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->SO_ANONYMOUS:I

    .line 6
    .line 7
    const/4 v0, 0x4

    .line 8
    iput v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->SO_MONOPOLIZE:I

    .line 9
    .line 10
    const/16 v0, 0x8

    .line 11
    .line 12
    iput v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->SO_PINGTEST:I

    .line 13
    .line 14
    new-instance v0, Ljava/util/HashSet;

    .line 15
    .line 16
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 17
    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->cycleRetain:Ljava/util/Set;

    .line 20
    .line 21
    const/4 v0, 0x0

    .line 22
    iput-boolean v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->closedByClient:Z

    .line 23
    .line 24
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    iput-object v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->uuid:Ljava/lang/String;

    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private native clearSocketCallback(J)V
.end method

.method private native closeSocket(J)V
.end method

.method private static native errorDesc(I)[B
.end method

.method public static errorDescription(I)Ljava/lang/String;
    .locals 1

    .line 1
    new-instance v0, Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {p0}, Lcom/intsig/issocket/ISSocketAndroid;->errorDesc(I)[B

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    invoke-direct {v0, p0}, Ljava/lang/String;-><init>([B)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private native flushSocket(J)V
.end method

.method private handlerPost(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/issocket/ISSocketAndroid;->cb_thread:Lcom/intsig/issocket/ISSocketCallbackLooperThread;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/issocket/ISSocketCallbackLooperThread;->mHandler:Landroid/os/Handler;

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static initISSocketSettings(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1
    new-instance v0, Ljava/io/File;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 11
    .line 12
    .line 13
    invoke-static {p0, p1}, Lcom/intsig/issocket/ISSocketAndroid;->initSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static native initSettings(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native isClosed(J)Z
.end method

.method private native isConnected(J)Z
.end method

.method private native isConnecting(J)Z
.end method

.method private native isReadyToWriteData(J)Z
.end method

.method private static native networkTrafficCountDown()J
.end method

.method private static native networkTrafficCountUp()J
.end method

.method public static pingTimeForAPI(Ljava/lang/String;)J
    .locals 2

    .line 1
    invoke-static {p0}, Lcom/intsig/issocket/ISSocketAndroid;->pingTimeToApi(Ljava/lang/String;)J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    return-wide v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static native pingTimeToApi(Ljava/lang/String;)J
.end method

.method private native releaseContext(J)V
.end method

.method private socketConnectToHost(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/issocket/ISSocketAndroidCallback;)Z
    .locals 7

    .line 1
    move-object v0, p0

    .line 2
    move-object v1, p1

    .line 3
    move v2, p2

    .line 4
    move-object/from16 v3, p14

    .line 5
    .line 6
    const/4 v4, 0x0

    .line 7
    if-eqz v1, :cond_2

    .line 8
    .line 9
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 10
    .line 11
    .line 12
    move-result v5

    .line 13
    if-eqz v5, :cond_2

    .line 14
    .line 15
    if-lez v2, :cond_2

    .line 16
    .line 17
    if-eqz p3, :cond_2

    .line 18
    .line 19
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    .line 20
    .line 21
    .line 22
    move-result v5

    .line 23
    if-eqz v5, :cond_2

    .line 24
    .line 25
    if-eqz p4, :cond_2

    .line 26
    .line 27
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    .line 28
    .line 29
    .line 30
    move-result v5

    .line 31
    if-eqz v5, :cond_2

    .line 32
    .line 33
    if-eqz p5, :cond_2

    .line 34
    .line 35
    invoke-virtual {p5}, Ljava/lang/String;->length()I

    .line 36
    .line 37
    .line 38
    move-result v5

    .line 39
    if-eqz v5, :cond_2

    .line 40
    .line 41
    if-nez v3, :cond_0

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_0
    iput-object v1, v0, Lcom/intsig/issocket/ISSocketAndroid;->host:Ljava/lang/String;

    .line 45
    .line 46
    iput v2, v0, Lcom/intsig/issocket/ISSocketAndroid;->port:I

    .line 47
    .line 48
    iput-object v3, v0, Lcom/intsig/issocket/ISSocketAndroid;->cb:Lcom/intsig/issocket/ISSocketAndroidCallback;

    .line 49
    .line 50
    invoke-direct/range {p0 .. p13}, Lcom/intsig/issocket/ISSocketAndroid;->socketConnectionToHost(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    .line 51
    .line 52
    .line 53
    move-result-wide v1

    .line 54
    iput-wide v1, v0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 55
    .line 56
    const-wide/16 v5, 0x0

    .line 57
    .line 58
    cmp-long v3, v1, v5

    .line 59
    .line 60
    if-nez v3, :cond_1

    .line 61
    .line 62
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 63
    .line 64
    const-string v2, "Init socket connection failed."

    .line 65
    .line 66
    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    return v4

    .line 70
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/issocket/ISSocketAndroid;->retainSelf()V

    .line 71
    .line 72
    .line 73
    const/4 v1, 0x1

    .line 74
    return v1

    .line 75
    :cond_2
    :goto_0
    return v4
.end method

.method private native socketConnectionToHost(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
.end method

.method private native socketDisableRead(J)V
.end method

.method private native socketDisableWrite(J)V
.end method

.method private native socketEnableRead(J)V
.end method

.method private native socketEnableWrite(J)V
.end method

.method public static socketNetworkTrafficCountDown()J
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/issocket/ISSocketAndroid;->networkTrafficCountDown()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    return-wide v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static socketNetworkTrafficCountUp()J
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/issocket/ISSocketAndroid;->networkTrafficCountUp()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    return-wide v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private native socketSetTimeout(JJJ)V
.end method

.method private writeJson(Lorg/json/JSONObject;)I
    .locals 6

    .line 1
    sget-wide v2, Lcom/intsig/issocket/ISSocketAndroid;->ISSOCKET_DEFAULT_ACK_TIMEOUT:J

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/intsig/issocket/ISSocketAndroid;->writeJson(Lorg/json/JSONObject;JZI)I

    move-result p1

    return p1
.end method

.method private native writeSocket(J[BJZI)I
.end method

.method private writeString(Ljava/lang/String;)I
    .locals 6

    .line 1
    sget-wide v2, Lcom/intsig/issocket/ISSocketAndroid;->ISSOCKET_DEFAULT_ACK_TIMEOUT:J

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/intsig/issocket/ISSocketAndroid;->writeString(Ljava/lang/String;JZI)I

    move-result p1

    return p1
.end method


# virtual methods
.method public anonymousMonopolizedSocketToHost(Ljava/lang/String;ILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/issocket/ISSocketAndroidCallback;)Z
    .locals 16

    .line 1
    move-object/from16 v15, p0

    .line 2
    .line 3
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v4

    .line 11
    iget v0, v15, Lcom/intsig/issocket/ISSocketAndroid;->SO_ANONYMOUS:I

    .line 12
    .line 13
    iget v1, v15, Lcom/intsig/issocket/ISSocketAndroid;->SO_MONOPOLIZE:I

    .line 14
    .line 15
    or-int v6, v0, v1

    .line 16
    .line 17
    move-object/from16 v0, p0

    .line 18
    .line 19
    move-object/from16 v1, p1

    .line 20
    .line 21
    move/from16 v2, p2

    .line 22
    .line 23
    move-object/from16 v3, p3

    .line 24
    .line 25
    move-object/from16 v5, p3

    .line 26
    .line 27
    move/from16 v7, p4

    .line 28
    .line 29
    move/from16 v8, p5

    .line 30
    .line 31
    move/from16 v9, p6

    .line 32
    .line 33
    move-object/from16 v10, p7

    .line 34
    .line 35
    move-object/from16 v11, p8

    .line 36
    .line 37
    move-object/from16 v12, p9

    .line 38
    .line 39
    move-object/from16 v13, p10

    .line 40
    .line 41
    move-object/from16 v14, p11

    .line 42
    .line 43
    invoke-direct/range {v0 .. v14}, Lcom/intsig/issocket/ISSocketAndroid;->socketConnectToHost(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/issocket/ISSocketAndroidCallback;)Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    return v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
.end method

.method public anonymousSharedSocketToHost(Ljava/lang/String;ILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/issocket/ISSocketAndroidCallback;)Z
    .locals 16

    .line 1
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v5

    .line 9
    move-object/from16 v0, p0

    .line 10
    .line 11
    iget v7, v0, Lcom/intsig/issocket/ISSocketAndroid;->SO_ANONYMOUS:I

    .line 12
    .line 13
    move-object/from16 v1, p0

    .line 14
    .line 15
    move-object/from16 v2, p1

    .line 16
    .line 17
    move/from16 v3, p2

    .line 18
    .line 19
    move-object/from16 v4, p3

    .line 20
    .line 21
    move-object/from16 v6, p3

    .line 22
    .line 23
    move/from16 v8, p4

    .line 24
    .line 25
    move/from16 v9, p5

    .line 26
    .line 27
    move/from16 v10, p6

    .line 28
    .line 29
    move-object/from16 v11, p7

    .line 30
    .line 31
    move-object/from16 v12, p8

    .line 32
    .line 33
    move-object/from16 v13, p9

    .line 34
    .line 35
    move-object/from16 v14, p10

    .line 36
    .line 37
    move-object/from16 v15, p11

    .line 38
    .line 39
    invoke-direct/range {v1 .. v15}, Lcom/intsig/issocket/ISSocketAndroid;->socketConnectToHost(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/issocket/ISSocketAndroidCallback;)Z

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    return v1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
.end method

.method public certifyMonopolizedSocketToHost(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/issocket/ISSocketAndroidCallback;)Z
    .locals 16

    move-object/from16 v15, p0

    .line 1
    iget v6, v15, Lcom/intsig/issocket/ISSocketAndroid;->SO_MONOPOLIZE:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/intsig/issocket/ISSocketAndroid;->socketConnectToHost(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/issocket/ISSocketAndroidCallback;)Z

    move-result v0

    return v0
.end method

.method public certifySharedSocketToHost(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/issocket/ISSocketAndroidCallback;)Z
    .locals 15

    const/4 v6, 0x0

    move-object v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    .line 1
    invoke-direct/range {v0 .. v14}, Lcom/intsig/issocket/ISSocketAndroid;->socketConnectToHost(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/issocket/ISSocketAndroidCallback;)Z

    move-result v0

    return v0
.end method

.method public close()V
    .locals 5

    .line 1
    iget-wide v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    cmp-long v4, v0, v2

    .line 6
    .line 7
    if-eqz v4, :cond_0

    .line 8
    .line 9
    iget-boolean v2, p0, Lcom/intsig/issocket/ISSocketAndroid;->closedByClient:Z

    .line 10
    .line 11
    if-nez v2, :cond_0

    .line 12
    .line 13
    invoke-direct {p0, v0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->closeSocket(J)V

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x1

    .line 17
    iput-boolean v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->closedByClient:Z

    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
.end method

.method public disableRead()V
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 2
    .line 3
    invoke-direct {p0, v0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->socketDisableRead(J)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public disableWrite()V
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 2
    .line 3
    invoke-direct {p0, v0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->socketDisableWrite(J)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public enableRead()V
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 2
    .line 3
    invoke-direct {p0, v0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->socketEnableRead(J)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public enableWrite()V
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 2
    .line 3
    invoke-direct {p0, v0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->socketEnableWrite(J)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected finalize()V
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->closedByClient:Z

    .line 2
    .line 3
    const-wide/16 v1, 0x0

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    iget-wide v3, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 8
    .line 9
    cmp-long v0, v3, v1

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-direct {p0, v3, v4}, Lcom/intsig/issocket/ISSocketAndroid;->isConnected(J)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    iget-wide v3, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 20
    .line 21
    invoke-direct {p0, v3, v4}, Lcom/intsig/issocket/ISSocketAndroid;->closeSocket(J)V

    .line 22
    .line 23
    .line 24
    :cond_0
    iget-wide v3, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 25
    .line 26
    iput-wide v1, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 27
    .line 28
    cmp-long v0, v3, v1

    .line 29
    .line 30
    if-eqz v0, :cond_1

    .line 31
    .line 32
    invoke-direct {p0, v3, v4}, Lcom/intsig/issocket/ISSocketAndroid;->releaseContext(J)V

    .line 33
    .line 34
    .line 35
    :cond_1
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public flushMsg()V
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 2
    .line 3
    invoke-direct {p0, v0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->flushSocket(J)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->host:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPort()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->port:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getReadTimeout()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->readTimeout:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->uuid:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getWriteTimeout()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->writeTimeout:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isClosed()Z
    .locals 6

    .line 1
    iget-wide v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    const/4 v4, 0x1

    .line 6
    cmp-long v5, v0, v2

    .line 7
    .line 8
    if-nez v5, :cond_0

    .line 9
    .line 10
    return v4

    .line 11
    :cond_0
    iget-boolean v2, p0, Lcom/intsig/issocket/ISSocketAndroid;->closedByClient:Z

    .line 12
    .line 13
    if-ne v2, v4, :cond_1

    .line 14
    .line 15
    return v4

    .line 16
    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->isClosed(J)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    return v0
    .line 21
.end method

.method public isConnected()Z
    .locals 6

    .line 1
    iget-wide v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    const/4 v4, 0x0

    .line 6
    cmp-long v5, v0, v2

    .line 7
    .line 8
    if-nez v5, :cond_0

    .line 9
    .line 10
    return v4

    .line 11
    :cond_0
    iget-boolean v2, p0, Lcom/intsig/issocket/ISSocketAndroid;->closedByClient:Z

    .line 12
    .line 13
    const/4 v3, 0x1

    .line 14
    if-ne v2, v3, :cond_1

    .line 15
    .line 16
    return v4

    .line 17
    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->isConnected(J)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    return v0
.end method

.method public isConnecting()Z
    .locals 6

    .line 1
    iget-wide v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    const/4 v4, 0x0

    .line 6
    cmp-long v5, v0, v2

    .line 7
    .line 8
    if-nez v5, :cond_0

    .line 9
    .line 10
    return v4

    .line 11
    :cond_0
    iget-boolean v2, p0, Lcom/intsig/issocket/ISSocketAndroid;->closedByClient:Z

    .line 12
    .line 13
    const/4 v3, 0x1

    .line 14
    if-ne v2, v3, :cond_1

    .line 15
    .line 16
    return v4

    .line 17
    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->isConnecting(J)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    return v0
.end method

.method public isReadyToWriteData()Z
    .locals 6

    .line 1
    iget-wide v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    const/4 v4, 0x0

    .line 6
    cmp-long v5, v0, v2

    .line 7
    .line 8
    if-nez v5, :cond_0

    .line 9
    .line 10
    return v4

    .line 11
    :cond_0
    iget-boolean v2, p0, Lcom/intsig/issocket/ISSocketAndroid;->closedByClient:Z

    .line 12
    .line 13
    const/4 v3, 0x1

    .line 14
    if-ne v2, v3, :cond_1

    .line 15
    .line 16
    return v4

    .line 17
    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->isReadyToWriteData(J)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    return v0
.end method

.method public releaseSelf()V
    .locals 5

    .line 1
    iget-wide v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    iput-wide v2, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 6
    .line 7
    cmp-long v4, v0, v2

    .line 8
    .line 9
    if-eqz v4, :cond_0

    .line 10
    .line 11
    invoke-direct {p0, v0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->releaseContext(J)V

    .line 12
    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->cycleRetain:Ljava/util/Set;

    .line 15
    .line 16
    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
.end method

.method public retainSelf()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->cycleRetain:Ljava/util/Set;

    .line 2
    .line 3
    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setReadTimeout(J)V
    .locals 7

    .line 1
    iput-wide p1, p0, Lcom/intsig/issocket/ISSocketAndroid;->readTimeout:J

    .line 2
    .line 3
    iget-wide v1, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 4
    .line 5
    const-wide/16 v3, 0x0

    .line 6
    .line 7
    cmp-long v0, v1, v3

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-wide v5, p0, Lcom/intsig/issocket/ISSocketAndroid;->writeTimeout:J

    .line 12
    .line 13
    move-object v0, p0

    .line 14
    move-wide v3, p1

    .line 15
    invoke-direct/range {v0 .. v6}, Lcom/intsig/issocket/ISSocketAndroid;->socketSetTimeout(JJJ)V

    .line 16
    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setWriteTimeout(J)V
    .locals 7

    .line 1
    iput-wide p1, p0, Lcom/intsig/issocket/ISSocketAndroid;->writeTimeout:J

    .line 2
    .line 3
    iget-wide v1, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    .line 4
    .line 5
    const-wide/16 v3, 0x0

    .line 6
    .line 7
    cmp-long v0, v1, v3

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-wide v3, p0, Lcom/intsig/issocket/ISSocketAndroid;->readTimeout:J

    .line 12
    .line 13
    move-object v0, p0

    .line 14
    move-wide v5, p1

    .line 15
    invoke-direct/range {v0 .. v6}, Lcom/intsig/issocket/ISSocketAndroid;->socketSetTimeout(JJJ)V

    .line 16
    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public socketAckError(Lcom/intsig/issocket/ISSocketAndroid;II)V
    .locals 7

    .line 1
    iget-object v2, p0, Lcom/intsig/issocket/ISSocketAndroid;->cb:Lcom/intsig/issocket/ISSocketAndroidCallback;

    .line 2
    .line 3
    new-instance v6, Lcom/intsig/issocket/ISSocketAndroid$11;

    .line 4
    .line 5
    move-object v0, v6

    .line 6
    move-object v1, p0

    .line 7
    move-object v3, p1

    .line 8
    move v4, p2

    .line 9
    move v5, p3

    .line 10
    invoke-direct/range {v0 .. v5}, Lcom/intsig/issocket/ISSocketAndroid$11;-><init>(Lcom/intsig/issocket/ISSocketAndroid;Lcom/intsig/issocket/ISSocketAndroidCallback;Lcom/intsig/issocket/ISSocketAndroid;II)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0, v6}, Lcom/intsig/issocket/ISSocketAndroid;->handlerPost(Ljava/lang/Runnable;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public socketDidConnectedToHost(Lcom/intsig/issocket/ISSocketAndroid;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->cb:Lcom/intsig/issocket/ISSocketAndroidCallback;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/issocket/ISSocketAndroid$1;

    .line 4
    .line 5
    invoke-direct {v1, p0, v0, p1}, Lcom/intsig/issocket/ISSocketAndroid$1;-><init>(Lcom/intsig/issocket/ISSocketAndroid;Lcom/intsig/issocket/ISSocketAndroidCallback;Lcom/intsig/issocket/ISSocketAndroid;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->handlerPost(Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public socketDidFailedWithError(Lcom/intsig/issocket/ISSocketAndroid;I)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/issocket/ISSocketAndroid;->releaseSelf()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->cb:Lcom/intsig/issocket/ISSocketAndroidCallback;

    .line 5
    .line 6
    new-instance v1, Lcom/intsig/issocket/ISSocketAndroid$8;

    .line 7
    .line 8
    invoke-direct {v1, p0, v0, p1, p2}, Lcom/intsig/issocket/ISSocketAndroid$8;-><init>(Lcom/intsig/issocket/ISSocketAndroid;Lcom/intsig/issocket/ISSocketAndroidCallback;Lcom/intsig/issocket/ISSocketAndroid;I)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->handlerPost(Ljava/lang/Runnable;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public socketDidFinishLoading(Lcom/intsig/issocket/ISSocketAndroid;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/issocket/ISSocketAndroid;->releaseSelf()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->cb:Lcom/intsig/issocket/ISSocketAndroidCallback;

    .line 5
    .line 6
    new-instance v1, Lcom/intsig/issocket/ISSocketAndroid$9;

    .line 7
    .line 8
    invoke-direct {v1, p0, v0, p1}, Lcom/intsig/issocket/ISSocketAndroid$9;-><init>(Lcom/intsig/issocket/ISSocketAndroid;Lcom/intsig/issocket/ISSocketAndroidCallback;Lcom/intsig/issocket/ISSocketAndroid;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->handlerPost(Ljava/lang/Runnable;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public socketDidPingTestToHost(Lcom/intsig/issocket/ISSocketAndroid;Ljava/lang/String;JJ)V
    .locals 11

    .line 1
    move-object v9, p0

    .line 2
    iget-object v2, v9, Lcom/intsig/issocket/ISSocketAndroid;->cb:Lcom/intsig/issocket/ISSocketAndroidCallback;

    .line 3
    .line 4
    new-instance v10, Lcom/intsig/issocket/ISSocketAndroid$12;

    .line 5
    .line 6
    move-object v0, v10

    .line 7
    move-object v1, p0

    .line 8
    move-object v3, p1

    .line 9
    move-object v4, p2

    .line 10
    move-wide v5, p3

    .line 11
    move-wide/from16 v7, p5

    .line 12
    .line 13
    invoke-direct/range {v0 .. v8}, Lcom/intsig/issocket/ISSocketAndroid$12;-><init>(Lcom/intsig/issocket/ISSocketAndroid;Lcom/intsig/issocket/ISSocketAndroidCallback;Lcom/intsig/issocket/ISSocketAndroid;Ljava/lang/String;JJ)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0, v10}, Lcom/intsig/issocket/ISSocketAndroid;->handlerPost(Ljava/lang/Runnable;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public socketDidReadTimeout(Lcom/intsig/issocket/ISSocketAndroid;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->cb:Lcom/intsig/issocket/ISSocketAndroidCallback;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/issocket/ISSocketAndroid$6;

    .line 4
    .line 5
    invoke-direct {v1, p0, v0, p1}, Lcom/intsig/issocket/ISSocketAndroid$6;-><init>(Lcom/intsig/issocket/ISSocketAndroid;Lcom/intsig/issocket/ISSocketAndroidCallback;Lcom/intsig/issocket/ISSocketAndroid;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->handlerPost(Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public socketDidReceiveAuthenticationChallenge(Lcom/intsig/issocket/ISSocketAndroid;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->cb:Lcom/intsig/issocket/ISSocketAndroidCallback;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/issocket/ISSocketAndroid$3;

    .line 4
    .line 5
    invoke-direct {v1, p0, v0, p1}, Lcom/intsig/issocket/ISSocketAndroid$3;-><init>(Lcom/intsig/issocket/ISSocketAndroid;Lcom/intsig/issocket/ISSocketAndroidCallback;Lcom/intsig/issocket/ISSocketAndroid;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->handlerPost(Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public socketDidReceiveData(Lcom/intsig/issocket/ISSocketAndroid;[BIZ)V
    .locals 8

    .line 1
    iget-object v2, p0, Lcom/intsig/issocket/ISSocketAndroid;->cb:Lcom/intsig/issocket/ISSocketAndroidCallback;

    .line 2
    .line 3
    new-instance v7, Lcom/intsig/issocket/ISSocketAndroid$5;

    .line 4
    .line 5
    move-object v0, v7

    .line 6
    move-object v1, p0

    .line 7
    move-object v3, p1

    .line 8
    move-object v4, p2

    .line 9
    move v5, p3

    .line 10
    move v6, p4

    .line 11
    invoke-direct/range {v0 .. v6}, Lcom/intsig/issocket/ISSocketAndroid$5;-><init>(Lcom/intsig/issocket/ISSocketAndroid;Lcom/intsig/issocket/ISSocketAndroidCallback;Lcom/intsig/issocket/ISSocketAndroid;[BIZ)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, v7}, Lcom/intsig/issocket/ISSocketAndroid;->handlerPost(Ljava/lang/Runnable;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public socketDidResolvedDNS(Lcom/intsig/issocket/ISSocketAndroid;[B)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->cb:Lcom/intsig/issocket/ISSocketAndroidCallback;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/issocket/ISSocketAndroid$10;

    .line 4
    .line 5
    invoke-direct {v1, p0, v0, p1, p2}, Lcom/intsig/issocket/ISSocketAndroid$10;-><init>(Lcom/intsig/issocket/ISSocketAndroid;Lcom/intsig/issocket/ISSocketAndroidCallback;Lcom/intsig/issocket/ISSocketAndroid;[B)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->handlerPost(Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public socketDidSendBytes(Lcom/intsig/issocket/ISSocketAndroid;III)V
    .locals 8

    .line 1
    iget-object v2, p0, Lcom/intsig/issocket/ISSocketAndroid;->cb:Lcom/intsig/issocket/ISSocketAndroidCallback;

    .line 2
    .line 3
    new-instance v7, Lcom/intsig/issocket/ISSocketAndroid$7;

    .line 4
    .line 5
    move-object v0, v7

    .line 6
    move-object v1, p0

    .line 7
    move-object v3, p1

    .line 8
    move v4, p2

    .line 9
    move v5, p3

    .line 10
    move v6, p4

    .line 11
    invoke-direct/range {v0 .. v6}, Lcom/intsig/issocket/ISSocketAndroid$7;-><init>(Lcom/intsig/issocket/ISSocketAndroid;Lcom/intsig/issocket/ISSocketAndroidCallback;Lcom/intsig/issocket/ISSocketAndroid;III)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, v7}, Lcom/intsig/issocket/ISSocketAndroid;->handlerPost(Ljava/lang/Runnable;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public socketIsReadyToWriteData(Lcom/intsig/issocket/ISSocketAndroid;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->cb:Lcom/intsig/issocket/ISSocketAndroidCallback;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/issocket/ISSocketAndroid$4;

    .line 4
    .line 5
    invoke-direct {v1, p0, v0, p1}, Lcom/intsig/issocket/ISSocketAndroid$4;-><init>(Lcom/intsig/issocket/ISSocketAndroid;Lcom/intsig/issocket/ISSocketAndroidCallback;Lcom/intsig/issocket/ISSocketAndroid;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->handlerPost(Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public socketWillSendAuthenticationChallenge(Lcom/intsig/issocket/ISSocketAndroid;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/issocket/ISSocketAndroid;->cb:Lcom/intsig/issocket/ISSocketAndroidCallback;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/issocket/ISSocketAndroid$2;

    .line 4
    .line 5
    invoke-direct {v1, p0, v0, p1}, Lcom/intsig/issocket/ISSocketAndroid$2;-><init>(Lcom/intsig/issocket/ISSocketAndroid;Lcom/intsig/issocket/ISSocketAndroidCallback;Lcom/intsig/issocket/ISSocketAndroid;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0, v1}, Lcom/intsig/issocket/ISSocketAndroid;->handlerPost(Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public writeBytes([BJ)I
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    .line 1
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/issocket/ISSocketAndroid;->writeBytes([BJZI)I

    move-result p1

    return p1
.end method

.method public writeBytes([BJZI)I
    .locals 9

    .line 2
    array-length v0, p1

    if-lez v0, :cond_0

    iget-wide v2, p0, Lcom/intsig/issocket/ISSocketAndroid;->context:J

    const-wide/16 v0, 0x0

    cmp-long v4, v2, v0

    if-eqz v4, :cond_0

    if-ltz p5, :cond_0

    move-object v1, p0

    move-object v4, p1

    move-wide v5, p2

    move v7, p4

    move v8, p5

    .line 3
    invoke-direct/range {v1 .. v8}, Lcom/intsig/issocket/ISSocketAndroid;->writeSocket(J[BJZI)I

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public writeJson(Lorg/json/JSONObject;I)I
    .locals 6

    .line 2
    sget-wide v2, Lcom/intsig/issocket/ISSocketAndroid;->ISSOCKET_DEFAULT_ACK_TIMEOUT:J

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/intsig/issocket/ISSocketAndroid;->writeJson(Lorg/json/JSONObject;JZI)I

    move-result p1

    return p1
.end method

.method public writeJson(Lorg/json/JSONObject;JZI)I
    .locals 6

    .line 3
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/intsig/issocket/ISSocketAndroid;->writeString(Ljava/lang/String;JZI)I

    move-result p1

    return p1
.end method

.method public writeString(Ljava/lang/String;I)I
    .locals 6

    .line 2
    sget-wide v2, Lcom/intsig/issocket/ISSocketAndroid;->ISSOCKET_DEFAULT_ACK_TIMEOUT:J

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/intsig/issocket/ISSocketAndroid;->writeString(Ljava/lang/String;JZI)I

    move-result p1

    return p1
.end method

.method public writeString(Ljava/lang/String;JZI)I
    .locals 6

    .line 3
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/intsig/issocket/ISSocketAndroid;->writeBytes([BJZI)I

    move-result p1

    return p1
.end method

.class public Lcom/intsig/issocket/ISSocketSDKLoger;
.super Ljava/lang/Object;
.source "ISSocketSDKLoger.java"


# static fields
.field private static isSocketErrorCallback:Lcom/intsig/issocket/ISSocketErrorCallback;

.field private static logerListener:Lcom/intsig/logagent/LogAgent$ISSocketSDKLogerListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static issocketsdklog(ILjava/lang/String;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/issocket/ISSocketSDKLoger;->logerListener:Lcom/intsig/logagent/LogAgent$ISSocketSDKLogerListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p0, p1}, Lcom/intsig/logagent/LogAgent$ISSocketSDKLogerListener;->issocketsdklog(ILjava/lang/String;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static setISSocketSDKLogerListener(Lcom/intsig/logagent/LogAgent$ISSocketSDKLogerListener;)V
    .locals 0

    .line 1
    sput-object p0, Lcom/intsig/issocket/ISSocketSDKLoger;->logerListener:Lcom/intsig/logagent/LogAgent$ISSocketSDKLogerListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static setIsSocketErrorCallback(Lcom/intsig/issocket/ISSocketErrorCallback;)V
    .locals 0

    .line 1
    sput-object p0, Lcom/intsig/issocket/ISSocketSDKLoger;->isSocketErrorCallback:Lcom/intsig/issocket/ISSocketErrorCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static socketAckError(I)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/issocket/ISSocketSDKLoger;->isSocketErrorCallback:Lcom/intsig/issocket/ISSocketErrorCallback;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p0}, Lcom/intsig/issocket/ISSocketErrorCallback;->socketAckError(I)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static socketDidFailedWithError(I)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/issocket/ISSocketSDKLoger;->isSocketErrorCallback:Lcom/intsig/issocket/ISSocketErrorCallback;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p0}, Lcom/intsig/issocket/ISSocketErrorCallback;->socketDidFailedWithError(I)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static socketDidReadTimeout()V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/issocket/ISSocketSDKLoger;->isSocketErrorCallback:Lcom/intsig/issocket/ISSocketErrorCallback;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/issocket/ISSocketErrorCallback;->socketDidReadTimeout()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

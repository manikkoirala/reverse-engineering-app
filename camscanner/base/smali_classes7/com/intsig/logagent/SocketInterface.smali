.class public interface abstract Lcom/intsig/logagent/SocketInterface;
.super Ljava/lang/Object;
.source "SocketInterface.java"


# virtual methods
.method public abstract getInfo()Lorg/json/JSONObject;
.end method

.method public abstract sendMsg(Lorg/json/JSONObject;II)I
.end method

.method public abstract setCallback(Lcom/intsig/logagent/channel/ChannelSDK$ChannelStatusCallback;)V
.end method

.method public abstract setDeviceId(Ljava/lang/String;)V
.end method

.method public abstract setEnableConnect(Z)V
.end method

.method public abstract setLogLevel(I)V
.end method

.method public abstract tryConnectChannel()Z
.end method

.class public Lcom/intsig/nativelib/BookSplitter;
.super Ljava/lang/Object;
.source "BookSplitter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/nativelib/BookSplitter$OcrLine;,
        Lcom/intsig/nativelib/BookSplitter$SceneResult;
    }
.end annotation


# static fields
.field public static final PAGE_SPLIT_AUTO:I = 0x2

.field public static final PAGE_SPLIT_DOUBLE:I = 0x1

.field public static final PAGE_SPLIT_SINGLE:I = 0x0

.field public static final SHADOW_TYPE_COLOR:I = 0x2

.field public static final SHADOW_TYPE_COMPLEX_BACKGROUND_ADJUST:I = 0x4

.field public static final SHADOW_TYPE_COMPLEX_BACKGROUND_REMOVE:I = 0x3

.field public static final SHADOW_TYPE_DEFAULT:I = 0x1

.field public static final SHADOW_TYPE_DESHADOW:I = 0x1

.field public static final SHADOW_TYPE_MAGIC:I = 0x3

.field public static final SHADOW_TYPE_PAGE_BACKGROUND:I = 0x2

.field public static final SHADOW_TYPE_STRONG_REMOVE_SHADOW:I = 0x5


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-string v0, "booksplitter"

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/NativeLibLoader;->〇080(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static native CropDewarpImagePos(Landroid/graphics/Bitmap;[IIZ)I
.end method

.method public static native CropDewarpImagePosFile(Ljava/lang/String;[ILjava/lang/String;IZ)I
.end method

.method public static native CropDewarpImagePosPtr(J[IJIZ)I
.end method

.method public static native CropDewarpImagePosPtrInplace(J[IIZ)I
.end method

.method public static native DetectBorder(J[II)I
.end method

.method public static native DetectBorderYUV([BII[II)I
.end method

.method public static native DetectFrameBorder([BII[II)I
.end method

.method public static native DewarpImagePos(Landroid/graphics/Bitmap;[IZ)I
.end method

.method public static native DewarpImagePosFile(Ljava/lang/String;[ILjava/lang/String;Z)I
.end method

.method public static native DewarpImagePosPtr(J[IJZ)I
.end method

.method public static native DewarpImagePosPtrInplace(J[IZ)I
.end method

.method public static native DewarpImgBitmap(Landroid/graphics/Bitmap;[IIFZZ)I
.end method

.method public static native DewarpImgBitmapNew(Landroid/graphics/Bitmap;I)I
.end method

.method public static native DewarpImgFile(Ljava/lang/String;[ILjava/lang/String;IFZZ)I
.end method

.method public static native DewarpImgFileNew(Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method public static native DewarpImgP(J[IJIFZZ)I
.end method

.method public static native DewarpImgPtrInplace(J[IIFZZ)I
.end method

.method public static native DewarpImgPtrInplaceNew(JI)I
.end method

.method public static native DewarpImgPtrNew(JJI)I
.end method

.method public static native DrawCropDewarpProcessImagePos(Landroid/graphics/Bitmap;[I[IIII)I
.end method

.method public static native DrawDewarpFrames(Landroid/graphics/Bitmap;II)I
.end method

.method public static native DrawDewarpProcessImagePos(Landroid/graphics/Bitmap;[I[III)I
.end method

.method public static native DrawDewarpProcessImagePosNew(Landroid/graphics/Bitmap;[I[IIIF)I
.end method

.method public static native DrawDewarpRotateProcessImagePos(Landroid/graphics/Bitmap;[I[IIIIIII)I
.end method

.method public static native GetDewarpImageWH([I[I[IZ)I
.end method

.method public static native GetSceneName(I)Ljava/lang/String;
.end method

.method public static native GetVersion()Ljava/lang/String;
.end method

.method public static native ImageSilverBulletBitmap(Landroid/graphics/Bitmap;III)I
.end method

.method public static native ImageSilverBulletFile(Ljava/lang/String;Ljava/lang/String;III)I
.end method

.method public static native ImageSilverBulletPtr(JIII)I
.end method

.method public static native InitDetectFrameBorder(I)I
.end method

.method public static native InitDewarpRotateProcess()I
.end method

.method public static native InitDrawCropDewarpProcess(Landroid/graphics/Bitmap;[I)I
.end method

.method public static native InitDrawDewarpFrames(Landroid/graphics/Bitmap;)I
.end method

.method public static native ReflectBinaryColorShiftBitmap(Landroid/graphics/Bitmap;I)I
.end method

.method public static native ReflectBinaryColorShiftBitmapBG(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;I)I
.end method

.method public static native ReflectBinaryColorShiftImgFile(Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method public static native ReflectBinaryColorShiftImgFileBG(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method public static native ReflectBinaryColorShiftImgPtr(JJI)I
.end method

.method public static native ReflectBinaryColorShiftImgPtrBG(JJJI)I
.end method

.method public static native ReflectBinaryColorShiftPtrInplace(JI)I
.end method

.method public static native ReflectBinaryColorShiftPtrInplaceBG(JJI)I
.end method

.method public static native ReleaseDewarpFrames()I
.end method

.method public static native ReleaseDrawCropDewarpProcess()I
.end method

.method public static native ScanImgP(J[II)I
.end method

.method public static native adjustRect([I[I[II[I[I)I
.end method

.method public static native classifyShadowTypeBitmap(Landroid/graphics/Bitmap;)I
.end method

.method public static native classifyShadowTypeImagePtr(J)I
.end method

.method public static native cropAndgetDocDirectionBitmap(Landroid/graphics/Bitmap;[I)I
.end method

.method public static native cropAndgetDocDirectionPath(Ljava/lang/String;[I)I
.end method

.method public static native cropAndgetDocDirectionPtr(J[I)I
.end method

.method public static native deblurBitmap(Landroid/graphics/Bitmap;I)I
.end method

.method public static native deblurFile(Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method public static native deblurPtr(JI)I
.end method

.method public static native deshadowBitmap(Landroid/graphics/Bitmap;I)I
.end method

.method public static native deshadowBitmap2(Landroid/graphics/Bitmap;II)I
.end method

.method public static native deshadowBitmapNew(Landroid/graphics/Bitmap;II)I
.end method

.method public static native deshadowFile(Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method public static native deshadowFile2(Ljava/lang/String;Ljava/lang/String;II)I
.end method

.method public static native deshadowFileNew(Ljava/lang/String;Ljava/lang/String;II)I
.end method

.method public static native deshadowImagePtr(JI)I
.end method

.method public static native deshadowImagePtr2(JII)I
.end method

.method public static native deshadowImagePtrNew(JII)I
.end method

.method public static native destroyResource4Lines()I
.end method

.method public static native detectInvoiceFrameBorder([BIII)F
.end method

.method public static native detectInvoicePath(Ljava/lang/String;I)F
.end method

.method public static native detectInvoicePtr(JI)F
.end method

.method public static native documentRadarBitmap(Landroid/graphics/Bitmap;I)F
.end method

.method public static native documentRadarPath(Ljava/lang/String;I)F
.end method

.method public static native documentRadarPtr(JI)F
.end method

.method public static native findCandidateLines(J[I[I)I
.end method

.method public static native fingerDetectBitmap(Landroid/graphics/Bitmap;I)I
.end method

.method public static native fingerDetectPath(Ljava/lang/String;I)I
.end method

.method public static native fingerDetectPtr(JI)I
.end method

.method public static native getClarityClassifyBitmap(Landroid/graphics/Bitmap;I)I
.end method

.method public static native getClarityClassifyPath(Ljava/lang/String;I)I
.end method

.method public static native getClarityClassifyPtr(JI)I
.end method

.method public static native getDocDirectionBitmap(Landroid/graphics/Bitmap;I)I
.end method

.method public static native getDocDirectionPath(Ljava/lang/String;I)I
.end method

.method public static native getDocDirectionPtr(JI)I
.end method

.method public static native getEdgeBitmap(Landroid/graphics/Bitmap;II[I[I)I
.end method

.method public static native getEdgeFile(Ljava/lang/String;Ljava/lang/String;II[I[I[I)I
.end method

.method public static native getImageFormat(J)I
.end method

.method public static native getMoireClassifyBitmap(Landroid/graphics/Bitmap;I)I
.end method

.method public static native getMoireClassifyPath(Ljava/lang/String;I)I
.end method

.method public static native getMoireClassifyPtr(JI)I
.end method

.method public static native getMultimodalClassifyBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;ILjava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/nativelib/BookSplitter$SceneResult;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native getMultimodalClassifyPath(Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/nativelib/BookSplitter$SceneResult;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native getMultimodalClassifyPtr(JLjava/lang/String;ILjava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/nativelib/BookSplitter$SceneResult;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native getNameProposal(Ljava/util/ArrayList;ILjava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/nativelib/BookSplitter$OcrLine;",
            ">;I",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method public static native getNewMultimodalClassifyBitmap(Landroid/graphics/Bitmap;Ljava/util/ArrayList;ILjava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/nativelib/BookSplitter$OcrLine;",
            ">;I",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/nativelib/BookSplitter$SceneResult;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native getNewMultimodalClassifyPath(Ljava/lang/String;Ljava/util/ArrayList;ILjava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/nativelib/BookSplitter$OcrLine;",
            ">;I",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/nativelib/BookSplitter$SceneResult;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native getNewMultimodalClassifyPtr(JLjava/util/ArrayList;ILjava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/nativelib/BookSplitter$OcrLine;",
            ">;I",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/nativelib/BookSplitter$SceneResult;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native getNewSceneClassifyBitmap(Landroid/graphics/Bitmap;ILjava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "I",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/nativelib/BookSplitter$SceneResult;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native getNewSceneClassifyPath(Ljava/lang/String;ILjava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/nativelib/BookSplitter$SceneResult;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native getNewSceneClassifyPtr(JILjava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/nativelib/BookSplitter$SceneResult;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native getSceneClassifyBitmap(Landroid/graphics/Bitmap;I)I
.end method

.method public static native getSceneClassifyPath(Ljava/lang/String;I)I
.end method

.method public static native getSceneClassifyPtr(JI)I
.end method

.method public static native initClarityClassify(I)I
.end method

.method public static native initCropDewarp(I)I
.end method

.method public static native initCropDewarpOld(I)I
.end method

.method public static native initDeblur()I
.end method

.method public static native initDeshadowModel2(I)I
.end method

.method public static native initDeshadowModelNew(I)I
.end method

.method public static native initDetectBorder(I)I
.end method

.method public static native initDocDirection()I
.end method

.method public static native initFingerDetect(I)I
.end method

.method public static native initInvoiceDetect(I)I
.end method

.method public static native initMoireClassify(I)I
.end method

.method public static native initMultimodalClassify(Ljava/lang/String;I)I
.end method

.method public static native initRadar(I)I
.end method

.method public static native initResource4Lines()I
.end method

.method public static native initSceneClassify(I)I
.end method

.method public static native refineEraseAny(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public static native releaseClarityClassify(I)V
.end method

.method public static native releaseCropDewarp(I)I
.end method

.method public static native releaseCropDewarpOld(I)I
.end method

.method public static native releaseDeblur(I)I
.end method

.method public static native releaseDeshadowModel2(I)V
.end method

.method public static native releaseDeshadowModelNew(I)V
.end method

.method public static native releaseDetectBorder(I)I
.end method

.method public static native releaseDocDirection(I)V
.end method

.method public static native releaseFingerDetect(I)I
.end method

.method public static native releaseInvoiceDetect(I)V
.end method

.method public static native releaseMoireClassify(I)V
.end method

.method public static native releaseMultimodalClassify(I)V
.end method

.method public static native releaseRadar(I)V
.end method

.method public static native releaseSceneClassify(I)V
.end method

.class public Lcom/intsig/nativelib/FocusAreaUtil;
.super Ljava/lang/Object;
.source "FocusAreaUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/nativelib/FocusAreaUtil$Point;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "FocusAreaUtil"

.field private static tag:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    :try_start_0
    const-string v0, "FocusArea"

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/NativeLibLoader;->〇080(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 4
    .line 5
    .line 6
    goto :goto_0

    .line 7
    :catch_0
    move-exception v0

    .line 8
    const-string v1, "FocusAreaUtil"

    .line 9
    .line 10
    invoke-static {v1, v0}, Lcom/intsig/utils/LogMessage;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 11
    .line 12
    .line 13
    :goto_0
    const/4 v0, 0x4

    .line 14
    sput v0, Lcom/intsig/nativelib/FocusAreaUtil;->tag:I

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static native FindFocusPoints([BII[I)I
.end method

.method public static findBestPoint([III)[I
    .locals 17

    move-object/from16 v0, p0

    if-eqz v0, :cond_5

    .line 1
    array-length v1, v0

    .line 2
    div-int/lit8 v2, v1, 0x3

    sub-int v2, v1, v2

    const/4 v3, 0x2

    .line 3
    div-int/lit8 v4, p1, 0x2

    .line 4
    div-int/lit8 v5, p2, 0x2

    const v6, 0x7fffffff

    const/4 v7, 0x0

    move v8, v2

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const v12, 0x7fffffff

    :goto_0
    const/4 v13, 0x1

    if-ge v8, v1, :cond_4

    .line 5
    aget v14, v0, v8

    if-lez v14, :cond_3

    if-ge v14, v11, :cond_0

    goto :goto_1

    :cond_0
    if-le v14, v11, :cond_1

    const v12, 0x7fffffff

    :cond_1
    sub-int v11, v8, v2

    mul-int/lit8 v11, v11, 0x2

    .line 6
    aget v15, v0, v11

    add-int/2addr v11, v13

    .line 7
    aget v11, v0, v11

    sub-int v13, v15, v4

    mul-int v13, v13, v13

    sub-int v16, v11, v5

    mul-int v16, v16, v16

    add-int v13, v13, v16

    if-ge v13, v12, :cond_2

    move v10, v11

    move v12, v13

    move v9, v15

    :cond_2
    move v11, v14

    :cond_3
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_4
    new-array v0, v3, [I

    aput v9, v0, v7

    aput v10, v0, v13

    return-object v0

    :cond_5
    const/4 v0, 0x0

    return-object v0
.end method

.method public static findBestPoint([IIIII)[I
    .locals 15

    move-object v0, p0

    .line 8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    if-eqz v0, :cond_a

    .line 9
    array-length v3, v0

    .line 10
    div-int/lit8 v4, v3, 0x3

    sub-int v5, v3, v4

    .line 11
    new-array v6, v4, [Lcom/intsig/nativelib/FocusAreaUtil$Point;

    const/4 v7, 0x0

    move v8, v5

    const/4 v9, 0x0

    :goto_0
    const/4 v10, 0x1

    if-ge v8, v3, :cond_1

    sub-int v11, v8, v5

    mul-int/lit8 v12, v11, 0x2

    .line 12
    aget v13, v0, v12

    add-int/2addr v12, v10

    .line 13
    aget v10, v0, v12

    .line 14
    new-instance v12, Lcom/intsig/nativelib/FocusAreaUtil$Point;

    aget v14, v0, v8

    invoke-direct {v12, v13, v10, v14}, Lcom/intsig/nativelib/FocusAreaUtil$Point;-><init>(III)V

    aput-object v12, v6, v11

    .line 15
    aget v10, v0, v8

    if-lez v10, :cond_0

    add-int/lit8 v9, v9, 0x1

    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 16
    :cond_1
    invoke-static {v6}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 17
    sget v0, Lcom/intsig/nativelib/FocusAreaUtil;->tag:I

    div-int v0, v9, v0

    sub-int v0, v4, v0

    sub-int/2addr v0, v10

    move v3, v0

    :goto_1
    if-ge v3, v4, :cond_4

    sub-int v5, v4, v9

    :goto_2
    if-ge v5, v4, :cond_3

    .line 18
    aget-object v8, v6, v3

    iget v11, v8, Lcom/intsig/nativelib/FocusAreaUtil$Point;->x:I

    sub-int v12, v11, p1

    aget-object v13, v6, v5

    iget v14, v13, Lcom/intsig/nativelib/FocusAreaUtil$Point;->x:I

    if-ge v12, v14, :cond_2

    add-int v11, v11, p1

    if-ge v14, v11, :cond_2

    iget v11, v8, Lcom/intsig/nativelib/FocusAreaUtil$Point;->y:I

    sub-int v12, v11, p2

    iget v13, v13, Lcom/intsig/nativelib/FocusAreaUtil$Point;->y:I

    if-ge v12, v13, :cond_2

    add-int v11, v11, p2

    if-ge v13, v11, :cond_2

    .line 19
    iget v11, v8, Lcom/intsig/nativelib/FocusAreaUtil$Point;->range:I

    add-int/2addr v11, v10

    iput v11, v8, Lcom/intsig/nativelib/FocusAreaUtil$Point;->range:I

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    const/high16 v3, -0x80000000

    const/high16 v5, -0x80000000

    const/4 v8, 0x0

    const/4 v9, 0x0

    :goto_3
    if-ge v0, v4, :cond_7

    .line 20
    aget-object v11, v6, v0

    iget v12, v11, Lcom/intsig/nativelib/FocusAreaUtil$Point;->range:I

    if-le v12, v3, :cond_5

    .line 21
    iget v8, v11, Lcom/intsig/nativelib/FocusAreaUtil$Point;->x:I

    .line 22
    iget v9, v11, Lcom/intsig/nativelib/FocusAreaUtil$Point;->y:I

    .line 23
    iget v3, v11, Lcom/intsig/nativelib/FocusAreaUtil$Point;->value:I

    move v5, v3

    move v3, v12

    goto :goto_4

    :cond_5
    if-ne v12, v3, :cond_6

    .line 24
    iget v12, v11, Lcom/intsig/nativelib/FocusAreaUtil$Point;->value:I

    if-le v12, v5, :cond_6

    .line 25
    iget v8, v11, Lcom/intsig/nativelib/FocusAreaUtil$Point;->x:I

    .line 26
    iget v9, v11, Lcom/intsig/nativelib/FocusAreaUtil$Point;->y:I

    move v5, v12

    :cond_6
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 27
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "findBestPoint cost time="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v1

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FocusAreaUtil"

    invoke-static {v1, v0}, Lcom/intsig/utils/LogMessage;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x2

    if-gtz v8, :cond_9

    if-lez v9, :cond_8

    goto :goto_5

    :cond_8
    new-array v1, v0, [I

    .line 28
    div-int/lit8 v2, p3, 0x2

    aput v2, v1, v7

    div-int/lit8 v0, p4, 0x2

    aput v0, v1, v10

    return-object v1

    .line 29
    :cond_9
    :goto_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "lastCx:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ",lastCy:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "findBestPoint"

    invoke-static {v2, v1}, Lcom/intsig/utils/LogMessage;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    new-array v0, v0, [I

    aput v8, v0, v7

    aput v9, v0, v10

    return-object v0

    :cond_a
    const/4 v0, 0x0

    return-object v0
.end method

.method public static generatePoints(II)[I
    .locals 10

    .line 1
    const/16 v0, 0x14

    .line 2
    .line 3
    div-int/2addr p0, v0

    .line 4
    div-int v1, p1, p0

    .line 5
    .line 6
    mul-int/lit8 v2, v1, 0x14

    .line 7
    .line 8
    mul-int/lit8 v2, v2, 0x3

    .line 9
    .line 10
    new-array v2, v2, [I

    .line 11
    .line 12
    mul-int v3, v1, p0

    .line 13
    .line 14
    sub-int/2addr p1, v3

    .line 15
    div-int/lit8 p1, p1, 0x2

    .line 16
    .line 17
    const/4 v3, 0x0

    .line 18
    const/4 v4, 0x0

    .line 19
    const/4 v5, 0x0

    .line 20
    :goto_0
    if-ge v4, v1, :cond_1

    .line 21
    .line 22
    div-int/lit8 v6, p0, 0x2

    .line 23
    .line 24
    mul-int v7, v4, p0

    .line 25
    .line 26
    add-int/2addr v7, v6

    .line 27
    add-int/2addr v7, p1

    .line 28
    const/4 v8, 0x0

    .line 29
    :goto_1
    if-ge v8, v0, :cond_0

    .line 30
    .line 31
    add-int/lit8 v9, v5, 0x1

    .line 32
    .line 33
    aput v6, v2, v5

    .line 34
    .line 35
    add-int/lit8 v5, v9, 0x1

    .line 36
    .line 37
    aput v7, v2, v9

    .line 38
    .line 39
    add-int/2addr v6, p0

    .line 40
    add-int/lit8 v8, v8, 0x1

    .line 41
    .line 42
    goto :goto_1

    .line 43
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_1
    return-object v2
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

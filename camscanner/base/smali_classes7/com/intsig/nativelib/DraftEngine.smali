.class public Lcom/intsig/nativelib/DraftEngine;
.super Ljava/lang/Object;
.source "DraftEngine.java"


# static fields
.field public static final COMPOSE_STYLE_START:I = 0x1

.field public static final COMPOSE_STYLE_TILED:I = 0x0

.field public static final COMPOSE_STYLE_XY:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-string v0, "native-lib"

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/NativeLibLoader;->〇080(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static native BackToState(ILandroid/graphics/Bitmap;I)I
.end method

.method public static native CleanImage(Ljava/lang/String;Landroid/graphics/Bitmap;II)I
.end method

.method public static native CleanImageKeepColor(Ljava/lang/String;Landroid/graphics/Bitmap;II)I
.end method

.method public static native ClipRegion(ILandroid/graphics/Bitmap;[BIIFFLandroid/graphics/Bitmap;)I
.end method

.method public static native Compose(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;FFFF)V
.end method

.method public static native ComposeBackground(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
.end method

.method public static native EraseColor(ILandroid/graphics/Bitmap;[BIIFF)V
.end method

.method public static native FreeContext(I)V
.end method

.method public static native SaveState(I)I
.end method

.method public static native StrokeColor(ILandroid/graphics/Bitmap;I)V
.end method

.method public static native StrokeSize(ILandroid/graphics/Bitmap;F)V
.end method

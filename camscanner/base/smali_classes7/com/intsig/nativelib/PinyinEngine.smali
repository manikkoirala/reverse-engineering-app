.class public Lcom/intsig/nativelib/PinyinEngine;
.super Ljava/lang/Object;
.source "PinyinEngine.java"


# static fields
.field static final COUNTRY_CHINA:I = 0x2

.field static final COUNTRY_EN:I = -0x1

.field static final COUNTRY_JAPAN:I = 0x0

.field static final COUNTRY_KOREAN:I = 0x1

.field private static final TAG:Ljava/lang/String; = "PinyinEngine"

.field private static initSuccess:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    :try_start_0
    const-string v0, "pinyinEngine"

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/NativeLibLoader;->〇080(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    sput-boolean v0, Lcom/intsig/nativelib/PinyinEngine;->initSuccess:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :catch_0
    move-exception v0

    .line 11
    const-string v1, "PinyinEngine"

    .line 12
    .line 13
    invoke-static {v1, v0}, Lcom/intsig/utils/LogMessage;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 14
    .line 15
    .line 16
    :goto_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static native getPinyin(Ljava/lang/String;I)[Ljava/lang/String;
.end method

.method static native initEngine(Ljava/io/FileDescriptor;JJ)I
.end method

.method static native releaseEngine()I
.end method

.class public Lcom/intsig/nativelib/OCREngine;
.super Ljava/lang/Object;
.source "OCREngine.java"


# static fields
.field public static final ISLangOpt_Afrikaans:I = 0x11

.field public static final ISLangOpt_Albanian:I = 0x12

.field public static final ISLangOpt_Armenian:I = 0x34

.field public static final ISLangOpt_Azerbaijani:I = 0x2b

.field public static final ISLangOpt_Basque:I = 0x13

.field public static final ISLangOpt_Belarussian:I = 0x32

.field public static final ISLangOpt_Bulgarian:I = 0x2e

.field public static final ISLangOpt_Catalan:I = 0x14

.field public static final ISLangOpt_Chinese_Simp:I = 0x1

.field public static final ISLangOpt_Chinese_Trad:I = 0x2

.field public static final ISLangOpt_Creole:I = 0x26

.field public static final ISLangOpt_Croatian:I = 0x15

.field public static final ISLangOpt_Czech:I = 0x16

.field public static final ISLangOpt_Danish:I = 0xd

.field public static final ISLangOpt_Dutch:I = 0xa

.field public static final ISLangOpt_English:I = 0x5

.field public static final ISLangOpt_Esperanto:I = 0x28

.field public static final ISLangOpt_Estobnian:I = 0x17

.field public static final ISLangOpt_Filipino:I = 0x29

.field public static final ISLangOpt_Finnish:I = 0xc

.field public static final ISLangOpt_French:I = 0x6

.field public static final ISLangOpt_Galician:I = 0x27

.field public static final ISLangOpt_German:I = 0x8

.field public static final ISLangOpt_Greek:I = 0x33

.field public static final ISLangOpt_Hungarian:I = 0xf

.field public static final ISLangOpt_Icelandic:I = 0x18

.field public static final ISLangOpt_Indonesian:I = 0x2a

.field public static final ISLangOpt_Irish:I = 0x19

.field public static final ISLangOpt_Italian:I = 0x9

.field public static final ISLangOpt_Japanese:I = 0x3

.field public static final ISLangOpt_Korean:I = 0x4

.field public static final ISLangOpt_Latin:I = 0x1a

.field public static final ISLangOpt_Latvian:I = 0x1b

.field public static final ISLangOpt_Lithuanian:I = 0x1c

.field public static final ISLangOpt_Macedonian:I = 0x2f

.field public static final ISLangOpt_Malay:I = 0x1d

.field public static final ISLangOpt_Maltese:I = 0x25

.field public static final ISLangOpt_Norwegian:I = 0xe

.field public static final ISLangOpt_Polish:I = 0x1e

.field public static final ISLangOpt_Portuguese:I = 0x7

.field public static final ISLangOpt_Romanian:I = 0x1f

.field public static final ISLangOpt_Russia:I = 0x2d

.field public static final ISLangOpt_Serbian:I = 0x31

.field public static final ISLangOpt_Slovak:I = 0x20

.field public static final ISLangOpt_Slovenian:I = 0x21

.field public static final ISLangOpt_Spanish:I = 0x2c

.field public static final ISLangOpt_Swanhili:I = 0x22

.field public static final ISLangOpt_Swedish:I = 0xb

.field public static final ISLangOpt_Turkish:I = 0x23

.field public static final ISLangOpt_Ukrainian:I = 0x30

.field public static final ISLangOpt_Vietnamese:I = 0x10

.field public static final ISLangOpt_Welsh:I = 0x24

.field public static final ISLangOpt_World_Wide:I = 0x0

.field public static final ISUserOpt_DetAndRec:I = 0x0

.field public static final ISUserOpt_DetOnly:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-string v0, "OCREngine"

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/NativeLibLoader;->〇080(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static native GetVersion()Ljava/lang/String;
.end method

.method public static native LoadModel(Ljava/lang/String;I)I
.end method

.method public static RecognizePage(J[I)Lcom/intsig/nativelib/OCROutput;
    .locals 7

    const/4 v0, 0x0

    new-array v3, v0, [Lcom/intsig/nativelib/Polygon;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-wide v1, p0

    move-object v4, p2

    .line 2
    invoke-static/range {v1 .. v6}, Lcom/intsig/nativelib/OCREngine;->RecognizePage(J[Lcom/intsig/nativelib/Polygon;[IIZ)Lcom/intsig/nativelib/OCROutput;

    move-result-object p0

    return-object p0
.end method

.method public static RecognizePage(J[Lcom/intsig/nativelib/Polygon;[I)Lcom/intsig/nativelib/OCROutput;
    .locals 1

    const/4 v0, 0x0

    .line 3
    invoke-static {p0, p1, p2, p3, v0}, Lcom/intsig/nativelib/OCREngine;->RecognizePage(J[Lcom/intsig/nativelib/Polygon;[II)Lcom/intsig/nativelib/OCROutput;

    move-result-object p0

    return-object p0
.end method

.method public static RecognizePage(J[Lcom/intsig/nativelib/Polygon;[II)Lcom/intsig/nativelib/OCROutput;
    .locals 6

    const/4 v5, 0x0

    move-wide v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    .line 1
    invoke-static/range {v0 .. v5}, Lcom/intsig/nativelib/OCREngine;->RecognizePage(J[Lcom/intsig/nativelib/Polygon;[IIZ)Lcom/intsig/nativelib/OCROutput;

    move-result-object p0

    return-object p0
.end method

.method public static native RecognizePage(J[Lcom/intsig/nativelib/Polygon;[IIZ)Lcom/intsig/nativelib/OCROutput;
.end method

.method public static native ReleaseModel()V
.end method

.class public Lcom/intsig/notes/fragment/EditNoteFragment;
.super Landroidx/fragment/app/Fragment;
.source "EditNoteFragment.java"

# interfaces
.implements Lcom/intsig/notes/activity/NoteActivity$FragmentCallback;
.implements Lcom/intsig/note/engine/entity/Document$OnCurrentPageChangedListener;


# static fields
.field public static ooO:Z = false


# instance fields
.field private O0O:Z

.field private O88O:I

.field private O8o08O8O:Ljava/lang/String;

.field private OO:Lcom/intsig/note/engine/view/DrawBoard;

.field private OO〇00〇8oO:Ljava/lang/String;

.field private Oo80:Z

.field private Ooo08:Lcom/intsig/view/InkSettingLayout;

.field private final O〇08oOOO0:Landroid/os/Handler;

.field private O〇o88o08〇:Z

.field private o0:Landroid/app/Activity;

.field private o8o:Lcom/intsig/note/engine/draw/DrawToolManager;

.field private o8oOOo:Lcom/intsig/note/engine/entity/Document;

.field private o8〇OO:Ljava/lang/String;

.field private o8〇OO0〇0o:I

.field private oOO〇〇:Lcom/intsig/notes/pen/PenSetting;

.field private oOo0:F

.field private oOo〇8o008:Z

.field private oo8ooo8O:Lcom/hciilab/digitalink/core/PenType$OnPenRangeChangeListener;

.field private final ooo0〇〇O:Lcom/samsung/spen/lib/input/SPenEventLibrary;

.field private o〇00O:Ljava/lang/Runnable;

.field private o〇oO:Landroid/widget/ProgressBar;

.field private 〇00O0:Z

.field private 〇080OO8〇0:I

.field private 〇08O〇00〇o:I

.field private 〇08〇o0O:Z

.field private 〇0O:Ljava/lang/String;

.field private 〇8〇oO〇〇8o:I

.field private final 〇OO8ooO8〇:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation
.end field

.field private 〇OOo8〇0:Landroid/view/View;

.field private 〇O〇〇O8:Lcom/intsig/note/engine/entity/Page;

.field private 〇o0O:I

.field private 〇〇08O:Z

.field private 〇〇o〇:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const/high16 v0, 0x3f800000    # 1.0f

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOo0:F

    .line 7
    .line 8
    new-instance v0, Lcom/samsung/spen/lib/input/SPenEventLibrary;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/samsung/spen/lib/input/SPenEventLibrary;-><init>()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->ooo0〇〇O:Lcom/samsung/spen/lib/input/SPenEventLibrary;

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    iput-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O0O:Z

    .line 17
    .line 18
    iput-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇08〇o0O:Z

    .line 19
    .line 20
    iput-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇〇o〇:Z

    .line 21
    .line 22
    iput-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->Oo80:Z

    .line 23
    .line 24
    iput-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇o88o08〇:Z

    .line 25
    .line 26
    new-instance v0, Landroid/os/Handler;

    .line 27
    .line 28
    new-instance v1, Lcom/intsig/notes/fragment/EditNoteFragment$1;

    .line 29
    .line 30
    invoke-direct {v1, p0}, Lcom/intsig/notes/fragment/EditNoteFragment$1;-><init>(Lcom/intsig/notes/fragment/EditNoteFragment;)V

    .line 31
    .line 32
    .line 33
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    .line 34
    .line 35
    .line 36
    iput-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇08oOOO0:Landroid/os/Handler;

    .line 37
    .line 38
    new-instance v0, Landroid/util/SparseArray;

    .line 39
    .line 40
    const/4 v1, 0x3

    .line 41
    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    .line 42
    .line 43
    .line 44
    iput-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇OO8ooO8〇:Landroid/util/SparseArray;

    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private O00OoO〇()V
    .locals 1

    .line 1
    new-instance v0, L〇0oO/O8;

    .line 2
    .line 3
    invoke-direct {v0, p0}, L〇0oO/O8;-><init>(Lcom/intsig/notes/fragment/EditNoteFragment;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO8oo0(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private O08〇()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/view/InkSettingLayout$ColorItemData;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/view/InkSettingLayout$ColorItemData;

    .line 7
    .line 8
    sget-object v2, Lcom/intsig/view/color/ColorItemView$Status;->normal:Lcom/intsig/view/color/ColorItemView$Status;

    .line 9
    .line 10
    const/high16 v3, -0x1000000

    .line 11
    .line 12
    const v4, -0x79797a

    .line 13
    .line 14
    .line 15
    invoke-direct {v1, v2, v3, v4}, Lcom/intsig/view/InkSettingLayout$ColorItemData;-><init>(Lcom/intsig/view/color/ColorItemView$Status;II)V

    .line 16
    .line 17
    .line 18
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    new-instance v1, Lcom/intsig/view/InkSettingLayout$ColorItemData;

    .line 22
    .line 23
    const/4 v3, -0x1

    .line 24
    const/4 v4, 0x0

    .line 25
    invoke-direct {v1, v2, v3, v4}, Lcom/intsig/view/InkSettingLayout$ColorItemData;-><init>(Lcom/intsig/view/color/ColorItemView$Status;II)V

    .line 26
    .line 27
    .line 28
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    new-instance v1, Lcom/intsig/view/InkSettingLayout$ColorItemData;

    .line 32
    .line 33
    const v3, -0x4a8b3

    .line 34
    .line 35
    .line 36
    invoke-direct {v1, v2, v3, v4}, Lcom/intsig/view/InkSettingLayout$ColorItemData;-><init>(Lcom/intsig/view/color/ColorItemView$Status;II)V

    .line 37
    .line 38
    .line 39
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    .line 41
    .line 42
    new-instance v1, Lcom/intsig/view/InkSettingLayout$ColorItemData;

    .line 43
    .line 44
    const v3, -0xa46bc

    .line 45
    .line 46
    .line 47
    invoke-direct {v1, v2, v3, v4}, Lcom/intsig/view/InkSettingLayout$ColorItemData;-><init>(Lcom/intsig/view/color/ColorItemView$Status;II)V

    .line 48
    .line 49
    .line 50
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    new-instance v1, Lcom/intsig/view/InkSettingLayout$ColorItemData;

    .line 54
    .line 55
    const v3, -0xb44c01

    .line 56
    .line 57
    .line 58
    invoke-direct {v1, v2, v3, v4}, Lcom/intsig/view/InkSettingLayout$ColorItemData;-><init>(Lcom/intsig/view/color/ColorItemView$Status;II)V

    .line 59
    .line 60
    .line 61
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    new-instance v1, Lcom/intsig/view/InkSettingLayout$ColorItemData;

    .line 65
    .line 66
    const v3, -0xe64364

    .line 67
    .line 68
    .line 69
    invoke-direct {v1, v2, v3, v4}, Lcom/intsig/view/InkSettingLayout$ColorItemData;-><init>(Lcom/intsig/view/color/ColorItemView$Status;II)V

    .line 70
    .line 71
    .line 72
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    .line 74
    .line 75
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO〇〇:Lcom/intsig/notes/pen/PenSetting;

    .line 76
    .line 77
    invoke-virtual {v1}, Lcom/intsig/notes/pen/PenSetting;->〇080()Lcom/intsig/notes/pen/PenState;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 82
    .line 83
    .line 84
    move-result-object v2

    .line 85
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 86
    .line 87
    .line 88
    move-result v3

    .line 89
    if-eqz v3, :cond_1

    .line 90
    .line 91
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 92
    .line 93
    .line 94
    move-result-object v3

    .line 95
    check-cast v3, Lcom/intsig/view/InkSettingLayout$ColorItemData;

    .line 96
    .line 97
    iget v4, v1, Lcom/intsig/notes/pen/PenState;->〇o00〇〇Oo:I

    .line 98
    .line 99
    iget v5, v3, Lcom/intsig/view/InkSettingLayout$ColorItemData;->〇o00〇〇Oo:I

    .line 100
    .line 101
    if-ne v4, v5, :cond_0

    .line 102
    .line 103
    sget-object v1, Lcom/intsig/view/color/ColorItemView$Status;->selected:Lcom/intsig/view/color/ColorItemView$Status;

    .line 104
    .line 105
    iput-object v1, v3, Lcom/intsig/view/InkSettingLayout$ColorItemData;->〇080:Lcom/intsig/view/color/ColorItemView$Status;

    .line 106
    .line 107
    :cond_1
    return-object v0
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method static bridge synthetic O0O0〇(Lcom/intsig/notes/fragment/EditNoteFragment;Ljava/lang/Runnable;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO8oo0(Ljava/lang/Runnable;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private O0o0()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8o:Lcom/intsig/note/engine/draw/DrawToolManager;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v1, 0x3

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o〇(I)Lcom/intsig/note/engine/draw/DrawTool;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Lcom/intsig/note/engine/draw/EraserTool;

    .line 11
    .line 12
    const/4 v1, 0x2

    .line 13
    new-array v2, v1, [F

    .line 14
    .line 15
    invoke-static {v1, v2}, Lcom/hciilab/digitalink/core/PenType;->getPenWidthRange(I[F)V

    .line 16
    .line 17
    .line 18
    const/4 v3, 0x0

    .line 19
    aget v3, v2, v3

    .line 20
    .line 21
    const/4 v4, 0x1

    .line 22
    aget v2, v2, v4

    .line 23
    .line 24
    sub-float/2addr v2, v3

    .line 25
    iget-object v4, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO〇〇:Lcom/intsig/notes/pen/PenSetting;

    .line 26
    .line 27
    invoke-virtual {v4, v1}, Lcom/intsig/notes/pen/PenSetting;->〇o00〇〇Oo(I)Lcom/intsig/notes/pen/PenState;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    iget v1, v1, Lcom/intsig/notes/pen/PenState;->〇080:I

    .line 32
    .line 33
    int-to-float v1, v1

    .line 34
    mul-float v2, v2, v1

    .line 35
    .line 36
    const/high16 v1, 0x42c80000    # 100.0f

    .line 37
    .line 38
    div-float/2addr v2, v1

    .line 39
    add-float/2addr v2, v3

    .line 40
    float-to-int v1, v2

    .line 41
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/draw/EraserTool;->〇o00〇〇Oo(I)V

    .line 42
    .line 43
    .line 44
    :cond_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private O0〇([FLcom/intsig/note/engine/draw/DrawToolManager;)V
    .locals 1

    .line 1
    if-nez p2, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    const/4 v0, 0x0

    .line 5
    invoke-virtual {p2, v0}, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o〇(I)Lcom/intsig/note/engine/draw/DrawTool;

    .line 6
    .line 7
    .line 8
    move-result-object p2

    .line 9
    check-cast p2, Lcom/intsig/note/engine/draw/InkTool;

    .line 10
    .line 11
    invoke-virtual {p2}, Lcom/intsig/note/engine/draw/InkTool;->〇o00〇〇Oo()I

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    invoke-static {p2, p1}, Lcom/hciilab/digitalink/core/PenType;->getPenWidthRange(I[F)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic O0〇0(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->〇OoO0o0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private O80OO(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "bitmap_path"

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    iput-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O8o08O8O:Ljava/lang/String;

    .line 14
    .line 15
    const-string v1, "file_path"

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    iput-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO〇00〇8oO:Ljava/lang/String;

    .line 22
    .line 23
    const-string v1, "bitmap_rotation"

    .line 24
    .line 25
    const/4 v2, 0x0

    .line 26
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    iput v3, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇080OO8〇0:I

    .line 31
    .line 32
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    iput-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇0O:Ljava/lang/String;

    .line 37
    .line 38
    const-string v1, "extra_key_page_id"

    .line 39
    .line 40
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    iput-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8〇OO:Ljava/lang/String;

    .line 45
    .line 46
    const/4 v0, 0x1

    .line 47
    if-eqz p1, :cond_0

    .line 48
    .line 49
    const-string v1, "scanner_bmp_rotated"

    .line 50
    .line 51
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    if-eqz p1, :cond_0

    .line 56
    .line 57
    const/4 p1, 0x1

    .line 58
    goto :goto_0

    .line 59
    :cond_0
    const/4 p1, 0x0

    .line 60
    :goto_0
    iput-boolean p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOo〇8o008:Z

    .line 61
    .line 62
    new-instance p1, Landroid/graphics/BitmapFactory$Options;

    .line 63
    .line 64
    invoke-direct {p1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 65
    .line 66
    .line 67
    iput-boolean v0, p1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 68
    .line 69
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O8o08O8O:Ljava/lang/String;

    .line 70
    .line 71
    invoke-static {v1, p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 72
    .line 73
    .line 74
    iget-boolean v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOo〇8o008:Z

    .line 75
    .line 76
    if-nez v1, :cond_2

    .line 77
    .line 78
    iget v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇080OO8〇0:I

    .line 79
    .line 80
    rem-int/lit16 v1, v1, 0xb4

    .line 81
    .line 82
    if-nez v1, :cond_1

    .line 83
    .line 84
    goto :goto_1

    .line 85
    :cond_1
    iget v1, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 86
    .line 87
    iput v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8〇OO0〇0o:I

    .line 88
    .line 89
    iget p1, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 90
    .line 91
    iput p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇8〇oO〇〇8o:I

    .line 92
    .line 93
    goto :goto_2

    .line 94
    :cond_2
    :goto_1
    iget v1, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 95
    .line 96
    iput v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8〇OO0〇0o:I

    .line 97
    .line 98
    iget p1, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 99
    .line 100
    iput p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇8〇oO〇〇8o:I

    .line 101
    .line 102
    :goto_2
    iget-object p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 103
    .line 104
    sget v1, Lcom/intsig/res/InkSettingResIds;->〇o00〇〇Oo:I

    .line 105
    .line 106
    invoke-static {p1, v1}, Lcom/intsig/res/InkSettingResIds;->〇080(Landroid/content/Context;I)Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object p1

    .line 110
    const/4 v1, 0x6

    .line 111
    invoke-direct {p0, p1, v2, v1}, Lcom/intsig/notes/fragment/EditNoteFragment;->o〇OoO0(Ljava/lang/String;II)V

    .line 112
    .line 113
    .line 114
    iput-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O0O:Z

    .line 115
    .line 116
    iput-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇08〇o0O:Z

    .line 117
    .line 118
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private static synthetic O88(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p0, "EditNoteFragment"

    .line 2
    .line 3
    const-string p1, "cancel"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private synthetic O880O〇()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->o8O〇008()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->O0o0()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic O8O(II)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇08〇o0O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->Oo80:Z

    .line 6
    .line 7
    if-nez v0, :cond_2

    .line 8
    .line 9
    int-to-float p1, p1

    .line 10
    const/high16 v0, 0x3f800000    # 1.0f

    .line 11
    .line 12
    mul-float p1, p1, v0

    .line 13
    .line 14
    iget v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8〇OO0〇0o:I

    .line 15
    .line 16
    int-to-float v1, v1

    .line 17
    div-float/2addr p1, v1

    .line 18
    int-to-float p2, p2

    .line 19
    mul-float p2, p2, v0

    .line 20
    .line 21
    iget v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇8〇oO〇〇8o:I

    .line 22
    .line 23
    int-to-float v0, v0

    .line 24
    div-float/2addr p2, v0

    .line 25
    invoke-static {p1, p2}, Ljava/lang/Math;->min(FF)F

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    iput p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOo0:F

    .line 30
    .line 31
    iget p2, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8〇OO0〇0o:I

    .line 32
    .line 33
    int-to-float p2, p2

    .line 34
    mul-float p2, p2, p1

    .line 35
    .line 36
    float-to-int p2, p2

    .line 37
    iput p2, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇o0O:I

    .line 38
    .line 39
    iget v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇8〇oO〇〇8o:I

    .line 40
    .line 41
    int-to-float v0, v0

    .line 42
    mul-float v0, v0, p1

    .line 43
    .line 44
    float-to-int p1, v0

    .line 45
    iput p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O88O:I

    .line 46
    .line 47
    if-lez p2, :cond_1

    .line 48
    .line 49
    if-gtz p1, :cond_0

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    const/4 p1, 0x1

    .line 53
    iput-boolean p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->Oo80:Z

    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_1
    :goto_0
    return-void

    .line 57
    :cond_2
    :goto_1
    new-instance p1, L〇0oO/〇o〇;

    .line 58
    .line 59
    invoke-direct {p1, p0}, L〇0oO/〇o〇;-><init>(Lcom/intsig/notes/fragment/EditNoteFragment;)V

    .line 60
    .line 61
    .line 62
    invoke-direct {p0, p1}, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO8oo0(Ljava/lang/Runnable;)V

    .line 63
    .line 64
    .line 65
    :cond_3
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method static bridge synthetic O8〇8〇O80(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->o0OO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private O8〇o0〇〇8()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇00O0:Z

    .line 2
    .line 3
    const-string v1, "EditNoteFragment"

    .line 4
    .line 5
    if-nez v0, :cond_2

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8oOOo:Lcom/intsig/note/engine/entity/Document;

    .line 8
    .line 9
    if-eqz v0, :cond_2

    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇O〇〇O8:Lcom/intsig/note/engine/entity/Page;

    .line 12
    .line 13
    if-nez v2, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/note/engine/entity/Document;->〇80〇808〇O()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇08oOOO0:Landroid/os/Handler;

    .line 23
    .line 24
    const/16 v2, 0x3ea

    .line 25
    .line 26
    invoke-virtual {v0, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 31
    .line 32
    .line 33
    const-string v0, "MSG_DISMISS_AND_BACK"

    .line 34
    .line 35
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    return-void

    .line 39
    :cond_1
    const-string v0, "save2Scanner"

    .line 40
    .line 41
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    const/16 v0, 0x66

    .line 45
    .line 46
    invoke-direct {p0, v0}, Lcom/intsig/notes/fragment/EditNoteFragment;->showDialog(I)V

    .line 47
    .line 48
    .line 49
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->〇〇()V

    .line 50
    .line 51
    .line 52
    new-instance v0, L〇0oO/〇o00〇〇Oo;

    .line 53
    .line 54
    invoke-direct {v0, p0}, L〇0oO/〇o00〇〇Oo;-><init>(Lcom/intsig/notes/fragment/EditNoteFragment;)V

    .line 55
    .line 56
    .line 57
    invoke-direct {p0, v0}, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO8oo0(Ljava/lang/Runnable;)V

    .line 58
    .line 59
    .line 60
    return-void

    .line 61
    :cond_2
    :goto_0
    const-string v0, "save2Scanner mIsLoading || mDocument == null || mPage == null"

    .line 62
    .line 63
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private OO0O()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇OOo8〇0:Landroid/view/View;

    .line 2
    .line 3
    sget v1, Lcom/intsig/innote/R$id;->tv_top_hint:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇0O:Ljava/lang/String;

    .line 12
    .line 13
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    const/4 v2, 0x0

    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    const/16 v1, 0x8

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v1, 0x0

    .line 24
    :goto_0
    const/4 v3, 0x1

    .line 25
    new-array v3, v3, [Landroid/view/View;

    .line 26
    .line 27
    aput-object v0, v3, v2

    .line 28
    .line 29
    invoke-static {v1, v3}, Lcom/intsig/utils/CustomViewUtils;->〇o〇(I[Landroid/view/View;)V

    .line 30
    .line 31
    .line 32
    if-eqz v0, :cond_1

    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇0O:Ljava/lang/String;

    .line 35
    .line 36
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    .line 38
    .line 39
    :cond_1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇OOo8〇0:Landroid/view/View;

    .line 40
    .line 41
    sget v1, Lcom/intsig/innote/R$id;->loadProgress:I

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    check-cast v0, Landroid/widget/ProgressBar;

    .line 48
    .line 49
    iput-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o〇oO:Landroid/widget/ProgressBar;

    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇OOo8〇0:Landroid/view/View;

    .line 52
    .line 53
    sget v1, Lcom/intsig/innote/R$id;->drawViewGroup:I

    .line 54
    .line 55
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    check-cast v0, Lcom/intsig/note/engine/view/DrawBoard;

    .line 60
    .line 61
    iput-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 62
    .line 63
    new-instance v1, L〇0oO/oO80;

    .line 64
    .line 65
    invoke-direct {v1, p0}, L〇0oO/oO80;-><init>(Lcom/intsig/notes/fragment/EditNoteFragment;)V

    .line 66
    .line 67
    .line 68
    invoke-interface {v0, v1}, Lcom/intsig/note/engine/view/DrawBoard;->setOnSizeChangedListener(Lcom/intsig/note/engine/view/DrawBoard$OnSizeChangedListener;)V

    .line 69
    .line 70
    .line 71
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 72
    .line 73
    new-instance v1, Lcom/intsig/notes/fragment/EditNoteFragment$3;

    .line 74
    .line 75
    invoke-direct {v1, p0}, Lcom/intsig/notes/fragment/EditNoteFragment$3;-><init>(Lcom/intsig/notes/fragment/EditNoteFragment;)V

    .line 76
    .line 77
    .line 78
    invoke-interface {v0, v1}, Lcom/intsig/note/engine/view/DrawBoard;->setCallback(Lcom/intsig/note/engine/view/DrawBoard$Callback;)V

    .line 79
    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 82
    .line 83
    invoke-interface {v0}, Lcom/intsig/note/engine/view/DrawBoard;->getDrawToolManager()Lcom/intsig/note/engine/draw/DrawToolManager;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    iput-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8o:Lcom/intsig/note/engine/draw/DrawToolManager;

    .line 88
    .line 89
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->o808o8o08()V

    .line 90
    .line 91
    .line 92
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 93
    .line 94
    invoke-interface {v0}, Lcom/intsig/note/engine/view/DrawBoard;->Oo08()V

    .line 95
    .line 96
    .line 97
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private synthetic OO〇〇o0oO()V
    .locals 6

    .line 1
    iget-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇〇o〇:Z

    .line 2
    .line 3
    if-nez v0, :cond_2

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇00O0:Z

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇08oOOO0:Landroid/os/Handler;

    .line 9
    .line 10
    const/16 v2, 0x3ee

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 13
    .line 14
    .line 15
    iget-boolean v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOo〇8o008:Z

    .line 16
    .line 17
    if-nez v1, :cond_1

    .line 18
    .line 19
    iget v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇080OO8〇0:I

    .line 20
    .line 21
    if-lez v1, :cond_1

    .line 22
    .line 23
    iget-object v2, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O8o08O8O:Ljava/lang/String;

    .line 24
    .line 25
    const/16 v3, 0x64

    .line 26
    .line 27
    const/4 v4, 0x0

    .line 28
    const/high16 v5, 0x3f800000    # 1.0f

    .line 29
    .line 30
    invoke-static {v2, v1, v5, v3, v4}, Lcom/intsig/scanner/ScannerEngine;->scaleImage(Ljava/lang/String;IFILjava/lang/String;)I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    if-ltz v1, :cond_0

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const/4 v0, 0x0

    .line 38
    :goto_0
    iput-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOo〇8o008:Z

    .line 39
    .line 40
    :cond_1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 41
    .line 42
    iget v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇o0O:I

    .line 43
    .line 44
    iget v2, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O88O:I

    .line 45
    .line 46
    invoke-interface {v0, v1, v2}, Lcom/intsig/note/engine/view/DrawBoard;->〇〇888(II)V

    .line 47
    .line 48
    .line 49
    :cond_2
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private synthetic OoO〇OOo8o()V
    .locals 3

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇O〇〇O8:Lcom/intsig/note/engine/entity/Page;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO〇00〇8oO:Ljava/lang/String;

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOo0:F

    .line 6
    .line 7
    invoke-static {v0, v1, v2}, Lcom/intsig/scanner/ScannerFormat;->loadData(Lcom/intsig/note/engine/entity/Page;Ljava/lang/String;F)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇08oOOO0:Landroid/os/Handler;

    .line 11
    .line 12
    new-instance v1, L〇0oO/o〇0;

    .line 13
    .line 14
    invoke-direct {v1, p0}, L〇0oO/o〇0;-><init>(Lcom/intsig/notes/fragment/EditNoteFragment;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 18
    .line 19
    .line 20
    return-void

    .line 21
    :catch_0
    move-exception v0

    .line 22
    const-string v1, "EditNoteFragment"

    .line 23
    .line 24
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 28
    .line 29
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic Ooo8o(Lcom/intsig/notes/fragment/EditNoteFragment;)Ljava/lang/Runnable;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o〇00O:Ljava/lang/Runnable;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private synthetic OooO〇(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇00O0:Z

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8oOOo:Lcom/intsig/note/engine/entity/Document;

    .line 6
    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇O〇〇O8:Lcom/intsig/note/engine/entity/Page;

    .line 10
    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 16
    .line 17
    invoke-interface {p1}, Lcom/intsig/note/engine/view/DrawBoard;->o〇0()V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    iget-object p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 22
    .line 23
    invoke-interface {p1}, Lcom/intsig/note/engine/view/DrawBoard;->OO0o〇〇〇〇0()V

    .line 24
    .line 25
    .line 26
    :cond_1
    :goto_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private O〇00O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o〇oO:Landroid/widget/ProgressBar;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 7
    .line 8
    .line 9
    :cond_0
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private O〇080〇o0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 2
    .line 3
    new-instance v1, L〇0oO/Oo08;

    .line 4
    .line 5
    invoke-direct {v1, p0}, L〇0oO/Oo08;-><init>(Lcom/intsig/notes/fragment/EditNoteFragment;)V

    .line 6
    .line 7
    .line 8
    invoke-interface {v0, v1}, Lcom/intsig/note/engine/view/DrawBoard;->oO80(Lcom/intsig/note/engine/history/HistoryActionStack$OnChangedListener;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private O〇0O〇Oo〇o(Landroid/content/Context;Ljava/lang/String;ZI)Lcom/intsig/app/BaseProgressDialog;
    .locals 0

    .line 1
    invoke-static {p1, p4}, Lcom/intsig/utils/DialogUtils;->〇o〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-eqz p2, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1, p2}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 8
    .line 9
    .line 10
    :cond_0
    invoke-virtual {p1, p3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 11
    .line 12
    .line 13
    return-object p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private O〇0o8o8〇()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6
    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 9
    .line 10
    sget v2, Lcom/intsig/res/InkSettingResIds;->〇o〇:I

    .line 11
    .line 12
    invoke-static {v1, v2}, Lcom/intsig/res/InkSettingResIds;->〇080(Landroid/content/Context;I)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 21
    .line 22
    sget v2, Lcom/intsig/res/InkSettingResIds;->O8:I

    .line 23
    .line 24
    invoke-static {v1, v2}, Lcom/intsig/res/InkSettingResIds;->〇080(Landroid/content/Context;I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 33
    .line 34
    sget v2, Lcom/intsig/res/InkSettingResIds;->Oo08:I

    .line 35
    .line 36
    invoke-static {v1, v2}, Lcom/intsig/res/InkSettingResIds;->〇080(Landroid/content/Context;I)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    new-instance v2, L〇0oO/OO0o〇〇〇〇0;

    .line 41
    .line 42
    invoke-direct {v2}, L〇0oO/OO0o〇〇〇〇0;-><init>()V

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->o800o8O(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 50
    .line 51
    sget v2, Lcom/intsig/res/InkSettingResIds;->o〇0:I

    .line 52
    .line 53
    invoke-static {v1, v2}, Lcom/intsig/res/InkSettingResIds;->〇080(Landroid/content/Context;I)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    new-instance v2, L〇0oO/〇8o8o〇;

    .line 58
    .line 59
    invoke-direct {v2, p0}, L〇0oO/〇8o8o〇;-><init>(Lcom/intsig/notes/fragment/EditNoteFragment;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->oo〇(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 71
    .line 72
    .line 73
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic O〇8〇008(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->O0o0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private O〇〇O80o8()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇OOo8〇0:Landroid/view/View;

    .line 2
    .line 3
    sget v1, Lcom/intsig/innote/R$id;->ink_setting:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/view/InkSettingLayout;

    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->Ooo08:Lcom/intsig/view/InkSettingLayout;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8〇OO:Ljava/lang/String;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/view/InkSettingLayout;->setCurrentPageId(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->Ooo08:Lcom/intsig/view/InkSettingLayout;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/view/InkSettingLayout;->〇〇808〇()V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->Ooo08:Lcom/intsig/view/InkSettingLayout;

    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO〇〇:Lcom/intsig/notes/pen/PenSetting;

    .line 26
    .line 27
    const/4 v2, 0x1

    .line 28
    invoke-virtual {v1, v2}, Lcom/intsig/notes/pen/PenSetting;->〇o00〇〇Oo(I)Lcom/intsig/notes/pen/PenState;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-virtual {v0, v2, v1}, Lcom/intsig/view/InkSettingLayout;->〇80〇808〇O(ILcom/intsig/notes/pen/PenState;)V

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->Ooo08:Lcom/intsig/view/InkSettingLayout;

    .line 36
    .line 37
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO〇〇:Lcom/intsig/notes/pen/PenSetting;

    .line 38
    .line 39
    const/4 v2, 0x3

    .line 40
    invoke-virtual {v1, v2}, Lcom/intsig/notes/pen/PenSetting;->〇o00〇〇Oo(I)Lcom/intsig/notes/pen/PenState;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    invoke-virtual {v0, v2, v1}, Lcom/intsig/view/InkSettingLayout;->〇80〇808〇O(ILcom/intsig/notes/pen/PenState;)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->Ooo08:Lcom/intsig/view/InkSettingLayout;

    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO〇〇:Lcom/intsig/notes/pen/PenSetting;

    .line 50
    .line 51
    const/4 v2, 0x2

    .line 52
    invoke-virtual {v1, v2}, Lcom/intsig/notes/pen/PenSetting;->〇o00〇〇Oo(I)Lcom/intsig/notes/pen/PenState;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    invoke-virtual {v0, v2, v1}, Lcom/intsig/view/InkSettingLayout;->〇80〇808〇O(ILcom/intsig/notes/pen/PenState;)V

    .line 57
    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->Ooo08:Lcom/intsig/view/InkSettingLayout;

    .line 60
    .line 61
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->O08〇()Ljava/util/List;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    invoke-virtual {v0, v1}, Lcom/intsig/view/InkSettingLayout;->OO0o〇〇(Ljava/util/List;)V

    .line 66
    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->Ooo08:Lcom/intsig/view/InkSettingLayout;

    .line 69
    .line 70
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO〇〇:Lcom/intsig/notes/pen/PenSetting;

    .line 71
    .line 72
    iget v1, v1, Lcom/intsig/notes/pen/PenSetting;->〇o00〇〇Oo:I

    .line 73
    .line 74
    const/4 v2, 0x0

    .line 75
    invoke-virtual {v0, v1, v2}, Lcom/intsig/view/InkSettingLayout;->Oooo8o0〇(IZ)V

    .line 76
    .line 77
    .line 78
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->Ooo08:Lcom/intsig/view/InkSettingLayout;

    .line 79
    .line 80
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇OOo8〇0:Landroid/view/View;

    .line 81
    .line 82
    sget v2, Lcom/intsig/innote/R$id;->v_pen_size:I

    .line 83
    .line 84
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    invoke-virtual {v0, v1}, Lcom/intsig/view/InkSettingLayout;->setViewSizeIndicator(Landroid/view/View;)V

    .line 89
    .line 90
    .line 91
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->Ooo08:Lcom/intsig/view/InkSettingLayout;

    .line 92
    .line 93
    new-instance v1, Lcom/intsig/notes/fragment/EditNoteFragment$2;

    .line 94
    .line 95
    invoke-direct {v1, p0}, Lcom/intsig/notes/fragment/EditNoteFragment$2;-><init>(Lcom/intsig/notes/fragment/EditNoteFragment;)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {v0, v1}, Lcom/intsig/view/InkSettingLayout;->setInkSettingLayoutListener(Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;)V

    .line 99
    .line 100
    .line 101
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->〇ooO8Ooo〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private synthetic o0O0O〇〇〇0()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O8o08O8O:Ljava/lang/String;

    .line 2
    .line 3
    const/4 v1, 0x3

    .line 4
    invoke-static {v0, v1}, Lcom/intsig/scanner/ScannerEngine;->decodeImageS(Ljava/lang/String;I)I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇O〇〇O8:Lcom/intsig/note/engine/entity/Page;

    .line 9
    .line 10
    const-class v2, Lcom/intsig/note/engine/draw/InkElement;

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Lcom/intsig/note/engine/entity/Page;->oO80(Ljava/lang/Class;)Ljava/util/List;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    if-lez v2, :cond_1

    .line 21
    .line 22
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 23
    .line 24
    .line 25
    move-result v6

    .line 26
    new-array v5, v6, [Lcom/hciilab/digitalink/core/PenRecord;

    .line 27
    .line 28
    const/4 v2, 0x0

    .line 29
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 30
    .line 31
    .line 32
    move-result v3

    .line 33
    if-ge v2, v3, :cond_0

    .line 34
    .line 35
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v3

    .line 39
    check-cast v3, Lcom/intsig/note/engine/draw/InkElement;

    .line 40
    .line 41
    const/high16 v4, 0x3f800000    # 1.0f

    .line 42
    .line 43
    iget v7, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOo0:F

    .line 44
    .line 45
    div-float/2addr v4, v7

    .line 46
    invoke-virtual {v3, v4}, Lcom/intsig/note/engine/draw/InkElement;->o0ooO(F)Lcom/hciilab/digitalink/core/PenRecord;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    aput-object v3, v5, v2

    .line 51
    .line 52
    add-int/lit8 v2, v2, 0x1

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    int-to-long v3, v0

    .line 56
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8oOOo:Lcom/intsig/note/engine/entity/Document;

    .line 57
    .line 58
    invoke-virtual {v1}, Lcom/intsig/note/engine/entity/Document;->Oo08()F

    .line 59
    .line 60
    .line 61
    move-result v7

    .line 62
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8oOOo:Lcom/intsig/note/engine/entity/Document;

    .line 63
    .line 64
    invoke-virtual {v1}, Lcom/intsig/note/engine/entity/Document;->Oo08()F

    .line 65
    .line 66
    .line 67
    move-result v8

    .line 68
    iget v9, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOo0:F

    .line 69
    .line 70
    invoke-static/range {v3 .. v9}, Lcom/intsig/utils/InkCanvasUtil;->〇080(J[Lcom/hciilab/digitalink/core/PenRecord;IFFF)Z

    .line 71
    .line 72
    .line 73
    :cond_1
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O8o08O8O:Ljava/lang/String;

    .line 74
    .line 75
    const/16 v2, 0x50

    .line 76
    .line 77
    invoke-static {v0, v1, v2}, Lcom/intsig/scanner/ScannerEngine;->encodeImageS(ILjava/lang/String;I)I

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    const-string v1, "EditNoteFragment"

    .line 82
    .line 83
    if-gez v0, :cond_2

    .line 84
    .line 85
    const-string v0, "ScannerEngine encodeImageS failed"

    .line 86
    .line 87
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇O〇〇O8:Lcom/intsig/note/engine/entity/Page;

    .line 91
    .line 92
    iget-object v2, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO〇00〇8oO:Ljava/lang/String;

    .line 93
    .line 94
    iget v3, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOo0:F

    .line 95
    .line 96
    iget v4, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8〇OO0〇0o:I

    .line 97
    .line 98
    iget v5, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇8〇oO〇〇8o:I

    .line 99
    .line 100
    invoke-static {v0, v2, v3, v4, v5}, Lcom/intsig/scanner/ScannerFormat;->saveData(Lcom/intsig/note/engine/entity/Page;Ljava/lang/String;FII)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    .line 102
    .line 103
    goto :goto_1

    .line 104
    :catch_0
    move-exception v0

    .line 105
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 106
    .line 107
    .line 108
    :goto_1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇08oOOO0:Landroid/os/Handler;

    .line 109
    .line 110
    const/16 v1, 0x3f0

    .line 111
    .line 112
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 117
    .line 118
    .line 119
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇08oOOO0:Landroid/os/Handler;

    .line 120
    .line 121
    const/16 v1, 0x3ea

    .line 122
    .line 123
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 128
    .line 129
    .line 130
    return-void
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private o0OO()V
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/note/engine/resource/NewShading;

    .line 2
    .line 3
    new-instance v1, Ljava/io/File;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O8o08O8O:Ljava/lang/String;

    .line 6
    .line 7
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v2, -0x1

    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/note/engine/resource/NewShading;-><init>(Ljava/io/File;I)V

    .line 12
    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8oOOo:Lcom/intsig/note/engine/entity/Document;

    .line 15
    .line 16
    iget v2, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇o0O:I

    .line 17
    .line 18
    iget v3, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O88O:I

    .line 19
    .line 20
    const/4 v4, 0x0

    .line 21
    invoke-virtual {v1, v2, v3, v4}, Lcom/intsig/note/engine/entity/Document;->〇080(IIZ)Lcom/intsig/note/engine/entity/Page;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    iput-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇O〇〇O8:Lcom/intsig/note/engine/entity/Page;

    .line 26
    .line 27
    invoke-virtual {v1, v0}, Lcom/intsig/note/engine/entity/Page;->OOO〇O0(Lcom/intsig/note/engine/resource/Shading;)V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇080〇o0()V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private o0Oo()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {v0}, Lcom/intsig/notes/pen/PenSetting;->〇o〇(Landroid/content/Context;)Lcom/intsig/notes/pen/PenSetting;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO〇〇:Lcom/intsig/notes/pen/PenSetting;

    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o0〇〇00(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇OO8ooO8〇:Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Landroid/app/Dialog;

    .line 8
    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :catch_0
    move-exception p1

    .line 16
    const-string v0, "EditNoteFragment"

    .line 17
    .line 18
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    :goto_0
    return-void
    .line 22
    .line 23
    .line 24
.end method

.method private o808o8o08()V
    .locals 2

    .line 1
    new-instance v0, L〇0oO/〇O8o08O;

    .line 2
    .line 3
    invoke-direct {v0, p0}, L〇0oO/〇O8o08O;-><init>(Lcom/intsig/notes/fragment/EditNoteFragment;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oo8ooo8O:Lcom/hciilab/digitalink/core/PenType$OnPenRangeChangeListener;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/hciilab/digitalink/core/PenType;->registerOnPenRangeChangeListener(Lcom/hciilab/digitalink/core/PenType$OnPenRangeChangeListener;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8o:Lcom/intsig/note/engine/draw/DrawToolManager;

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/draw/DrawToolManager;->〇080(I)Lcom/intsig/note/engine/draw/DrawTool;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    check-cast v0, Lcom/intsig/note/engine/draw/InkTool;

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO〇〇:Lcom/intsig/notes/pen/PenSetting;

    .line 21
    .line 22
    iget v1, v1, Lcom/intsig/notes/pen/PenSetting;->〇o00〇〇Oo:I

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/draw/InkTool;->Oo08(I)V

    .line 25
    .line 26
    .line 27
    const/16 v1, 0xff

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/draw/InkTool;->〇o〇(I)V

    .line 30
    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->o8O〇008()V

    .line 33
    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO〇〇:Lcom/intsig/notes/pen/PenSetting;

    .line 36
    .line 37
    invoke-virtual {v1}, Lcom/intsig/notes/pen/PenSetting;->〇080()Lcom/intsig/notes/pen/PenState;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    iget v1, v1, Lcom/intsig/notes/pen/PenState;->〇o00〇〇Oo:I

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/draw/InkTool;->O8(I)V

    .line 44
    .line 45
    .line 46
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->O0o0()V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic o88(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇0o8o8〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic o880(Lcom/intsig/notes/fragment/EditNoteFragment;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/notes/fragment/EditNoteFragment;->OooO〇(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private o8O〇008()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8o:Lcom/intsig/note/engine/draw/DrawToolManager;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o〇(I)Lcom/intsig/note/engine/draw/DrawTool;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Lcom/intsig/note/engine/draw/InkTool;

    .line 11
    .line 12
    const/4 v2, 0x2

    .line 13
    new-array v2, v2, [F

    .line 14
    .line 15
    iget-object v3, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8o:Lcom/intsig/note/engine/draw/DrawToolManager;

    .line 16
    .line 17
    invoke-direct {p0, v2, v3}, Lcom/intsig/notes/fragment/EditNoteFragment;->O0〇([FLcom/intsig/note/engine/draw/DrawToolManager;)V

    .line 18
    .line 19
    .line 20
    aget v1, v2, v1

    .line 21
    .line 22
    const/4 v3, 0x1

    .line 23
    aget v2, v2, v3

    .line 24
    .line 25
    iget-object v3, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO〇〇:Lcom/intsig/notes/pen/PenSetting;

    .line 26
    .line 27
    invoke-virtual {v3}, Lcom/intsig/notes/pen/PenSetting;->〇080()Lcom/intsig/notes/pen/PenState;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    iget v3, v3, Lcom/intsig/notes/pen/PenState;->〇080:I

    .line 32
    .line 33
    sub-float/2addr v2, v1

    .line 34
    int-to-float v3, v3

    .line 35
    mul-float v2, v2, v3

    .line 36
    .line 37
    const/high16 v3, 0x42c80000    # 100.0f

    .line 38
    .line 39
    div-float/2addr v2, v3

    .line 40
    add-float/2addr v2, v1

    .line 41
    float-to-int v1, v2

    .line 42
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/draw/InkTool;->o〇0(I)V

    .line 43
    .line 44
    .line 45
    :cond_0
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private oOO8oo0(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic oOoO8OO〇(Lcom/intsig/notes/fragment/EditNoteFragment;)Landroid/os/Handler;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇08oOOO0:Landroid/os/Handler;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic oOo〇08〇(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->O880O〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic oO〇oo(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/notes/fragment/EditNoteFragment;->O88(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic oooO888(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->OoO〇OOo8o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private o〇08oO80o(I)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇OO8ooO8〇:Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Landroid/app/Dialog;

    .line 8
    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/app/Dialog;->isShowing()Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    const/4 p1, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    :goto_0
    return p1
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic o〇0〇o(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->OO〇〇o0oO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic o〇O8OO(Lcom/intsig/notes/fragment/EditNoteFragment;)Lcom/intsig/note/engine/view/DrawBoard;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private o〇OoO0(Ljava/lang/String;II)V
    .locals 4

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    new-instance v2, Lcom/intsig/note/engine/entity/Document;

    .line 6
    .line 7
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v3

    .line 11
    invoke-direct {v2, p1, v3, v0, v1}, Lcom/intsig/note/engine/entity/Document;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 12
    .line 13
    .line 14
    iput-object v2, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8oOOo:Lcom/intsig/note/engine/entity/Document;

    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 17
    .line 18
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    iget p1, p1, Landroid/util/DisplayMetrics;->xdpi:F

    .line 27
    .line 28
    invoke-virtual {v2, p1}, Lcom/intsig/note/engine/entity/Document;->OO0o〇〇(F)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8oOOo:Lcom/intsig/note/engine/entity/Document;

    .line 32
    .line 33
    invoke-virtual {p1, p0}, Lcom/intsig/note/engine/entity/Document;->Oooo8o0〇(Lcom/intsig/note/engine/entity/Document$OnCurrentPageChangedListener;)V

    .line 34
    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8oOOo:Lcom/intsig/note/engine/entity/Document;

    .line 37
    .line 38
    invoke-virtual {p1, p2}, Lcom/intsig/note/engine/entity/Document;->〇8o8o〇(I)V

    .line 39
    .line 40
    .line 41
    iput p3, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇08O〇00〇o:I

    .line 42
    .line 43
    const/4 p1, 0x1

    .line 44
    iput-boolean p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇o88o08〇:Z

    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private synthetic o〇o08〇(ZZ)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->Ooo08:Lcom/intsig/view/InkSettingLayout;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/view/InkSettingLayout;->〇O〇(ZZ)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic o〇oo(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇00O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private showDialog(I)V
    .locals 2

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇OO8ooO8〇:Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/app/Dialog;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-nez p1, :cond_1

    .line 16
    .line 17
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/notes/fragment/EditNoteFragment;->OO0o(I)Landroid/app/Dialog;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇OO8ooO8〇:Landroid/util/SparseArray;

    .line 26
    .line 27
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :catch_0
    move-exception p1

    .line 35
    const-string v0, "EditNoteFragment"

    .line 36
    .line 37
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 38
    .line 39
    .line 40
    :cond_1
    :goto_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static synthetic 〇088O(Lcom/intsig/notes/fragment/EditNoteFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/notes/fragment/EditNoteFragment;->〇oOO80o(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method static bridge synthetic 〇08O(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->〇oO〇08o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇0o0oO〇〇0()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇〇08O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    :try_start_0
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->ooo0〇〇O:Lcom/samsung/spen/lib/input/SPenEventLibrary;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/samsung/spen/lib/input/SPenEventLibrary;->O8(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :catch_0
    move-exception v0

    .line 14
    const-string v1, "EditNoteFragment"

    .line 15
    .line 16
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 17
    .line 18
    .line 19
    :goto_0
    const/4 v0, 0x0

    .line 20
    iput-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇〇08O:Z

    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private 〇0o88Oo〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇O〇〇O8:Lcom/intsig/note/engine/entity/Page;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇00O0:Z

    .line 7
    .line 8
    const/16 v0, 0x65

    .line 9
    .line 10
    invoke-direct {p0, v0}, Lcom/intsig/notes/fragment/EditNoteFragment;->o〇08oO80o(I)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇08oOOO0:Landroid/os/Handler;

    .line 17
    .line 18
    const/16 v1, 0x3f5

    .line 19
    .line 20
    const-wide/16 v2, 0x1c2

    .line 21
    .line 22
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 23
    .line 24
    .line 25
    :cond_0
    new-instance v0, L〇0oO/〇〇888;

    .line 26
    .line 27
    invoke-direct {v0, p0}, L〇0oO/〇〇888;-><init>(Lcom/intsig/notes/fragment/EditNoteFragment;)V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0, v0}, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO8oo0(Ljava/lang/Runnable;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method static bridge synthetic 〇0oO〇oo00(Lcom/intsig/notes/fragment/EditNoteFragment;Ljava/lang/Runnable;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o〇00O:Ljava/lang/Runnable;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇0ooOOo(Lcom/intsig/notes/fragment/EditNoteFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O0O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇0〇0(Lcom/intsig/notes/fragment/EditNoteFragment;)Lcom/intsig/note/engine/draw/DrawToolManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8o:Lcom/intsig/note/engine/draw/DrawToolManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic 〇80O8o8O〇(Lcom/intsig/notes/fragment/EditNoteFragment;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/notes/fragment/EditNoteFragment;->O8O(II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method static bridge synthetic 〇8O0880(Lcom/intsig/notes/fragment/EditNoteFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/notes/fragment/EditNoteFragment;->o0〇〇00(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇8〇80o(Lcom/intsig/notes/fragment/EditNoteFragment;)Lcom/intsig/note/engine/entity/Document;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8oOOo:Lcom/intsig/note/engine/entity/Document;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic 〇8〇OOoooo(Lcom/intsig/notes/fragment/EditNoteFragment;ZZ)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/notes/fragment/EditNoteFragment;->o〇o08〇(ZZ)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method static bridge synthetic 〇O0o〇〇o(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->O00OoO〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->o0O0O〇〇〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇O8〇8000(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->〇O8〇8O0oO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇O8〇8O0oO()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->getBackStackEntryCount()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 18
    .line 19
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->popBackStack()V

    .line 28
    .line 29
    .line 30
    :cond_1
    :goto_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private synthetic 〇OoO0o0()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇〇o〇:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 6
    .line 7
    invoke-interface {v0}, Lcom/intsig/note/engine/view/DrawBoard;->recycle()V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    iput-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇〇o〇:Z

    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇Oo〇O(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->O8〇o0〇〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o08(Lcom/intsig/notes/fragment/EditNoteFragment;)Lcom/intsig/notes/pen/PenSetting;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO〇〇:Lcom/intsig/notes/pen/PenSetting;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇oO88o()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇〇08O:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    :try_start_0
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->ooo0〇〇O:Lcom/samsung/spen/lib/input/SPenEventLibrary;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 8
    .line 9
    new-instance v2, L〇0oO/〇80〇808〇O;

    .line 10
    .line 11
    invoke-direct {v2, p0}, L〇0oO/〇80〇808〇O;-><init>(Lcom/intsig/notes/fragment/EditNoteFragment;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1, v2}, Lcom/samsung/spen/lib/input/SPenEventLibrary;->〇o〇(Landroid/content/Context;Lcom/samsung/spensdk/applistener/SPenDetachmentListener;)Z

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x1

    .line 18
    iput-boolean v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇〇08O:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :catch_0
    move-exception v0

    .line 22
    const-string v1, "EditNoteFragment"

    .line 23
    .line 24
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 25
    .line 26
    .line 27
    :cond_0
    :goto_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private synthetic 〇oOO80o(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇oO〇08o()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o〇oO:Landroid/widget/ProgressBar;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/16 v1, 0x8

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic 〇ooO8Ooo〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8oOOo:Lcom/intsig/note/engine/entity/Document;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/entity/Document;->〇O8o08O(I)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic 〇ooO〇000()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/note/engine/view/DrawBoard;->O8()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇O〇〇O8:Lcom/intsig/note/engine/entity/Page;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/note/engine/entity/Page;->〇oOO8O8()V

    .line 9
    .line 10
    .line 11
    :try_start_0
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇O〇〇O8:Lcom/intsig/note/engine/entity/Page;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/note/engine/entity/Page;->〇0〇O0088o()Lcom/intsig/note/engine/resource/Shading;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇O〇〇O8:Lcom/intsig/note/engine/entity/Page;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/note/engine/entity/Page;->〇〇808〇()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    if-eqz v1, :cond_0

    .line 24
    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    .line 26
    .line 27
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8oOOo:Lcom/intsig/note/engine/entity/Document;

    .line 31
    .line 32
    invoke-virtual {v2}, Lcom/intsig/note/engine/entity/Document;->O8()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    invoke-static {v2}, Lcom/intsig/note/engine/io/FileUtil;->〇o00〇〇Oo(Ljava/lang/String;)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 51
    .line 52
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    .line 53
    .line 54
    iget-object v3, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 55
    .line 56
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 57
    .line 58
    .line 59
    move-result-object v3

    .line 60
    iget v4, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇o0O:I

    .line 61
    .line 62
    div-int/lit8 v4, v4, 0x2

    .line 63
    .line 64
    iget v5, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O88O:I

    .line 65
    .line 66
    div-int/lit8 v5, v5, 0x2

    .line 67
    .line 68
    invoke-static {v0, v4, v5}, Lcom/intsig/util/InnoteUtil;->〇o00〇〇Oo(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    invoke-direct {v2, v3, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 73
    .line 74
    .line 75
    invoke-interface {v1, v2}, Lcom/intsig/note/engine/view/DrawBoard;->setLoadingDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 76
    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/note/engine/resource/Shading;->oO80()Z

    .line 80
    .line 81
    .line 82
    move-result v1

    .line 83
    if-eqz v1, :cond_1

    .line 84
    .line 85
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 86
    .line 87
    invoke-virtual {v0}, Lcom/intsig/note/engine/resource/Shading;->〇〇888()Landroid/graphics/drawable/Drawable;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    invoke-interface {v1, v0}, Lcom/intsig/note/engine/view/DrawBoard;->setLoadingDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 92
    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_1
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 96
    .line 97
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    .line 98
    .line 99
    invoke-virtual {v0}, Lcom/intsig/note/engine/resource/Shading;->o〇0()I

    .line 100
    .line 101
    .line 102
    move-result v0

    .line 103
    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 104
    .line 105
    .line 106
    invoke-interface {v1, v2}, Lcom/intsig/note/engine/view/DrawBoard;->setLoadingDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 107
    .line 108
    .line 109
    :goto_0
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 110
    .line 111
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇O〇〇O8:Lcom/intsig/note/engine/entity/Page;

    .line 112
    .line 113
    invoke-interface {v0, v1}, Lcom/intsig/note/engine/view/DrawBoard;->setPage(Lcom/intsig/note/engine/entity/Page;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    .line 115
    .line 116
    goto :goto_1

    .line 117
    :catch_0
    move-exception v0

    .line 118
    const-string v1, "EditNoteFragment"

    .line 119
    .line 120
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 121
    .line 122
    .line 123
    :goto_1
    return-void
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method static bridge synthetic 〇o〇88〇8(Lcom/intsig/notes/fragment/EditNoteFragment;)Landroid/app/Activity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇o〇OO80oO()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO〇〇:Lcom/intsig/notes/pen/PenSetting;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 6
    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/notes/pen/PenSetting;->O8(Landroid/content/Context;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const-string v1, "note_saved"

    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 19
    .line 20
    .line 21
    return-void
.end method

.method static bridge synthetic 〇〇O80〇0o(Lcom/intsig/notes/fragment/EditNoteFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->O0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic 〇〇o0〇8(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->〇ooO〇000()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇〇〇0(Lcom/intsig/notes/fragment/EditNoteFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇00O0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇〇〇00(Lcom/intsig/notes/fragment/EditNoteFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇〇o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇〇〇O〇(Lcom/intsig/notes/fragment/EditNoteFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->o8O〇008()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public OO0o(I)Landroid/app/Dialog;
    .locals 2

    .line 1
    const/16 v0, 0x65

    .line 2
    .line 3
    if-eq p1, v0, :cond_0

    .line 4
    .line 5
    const/16 v0, 0x66

    .line 6
    .line 7
    if-eq p1, v0, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    return-object p1

    .line 11
    :cond_0
    iget-object p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 12
    .line 13
    sget v0, Lcom/intsig/res/InkSettingResIds;->〇〇888:I

    .line 14
    .line 15
    invoke-static {p1, v0}, Lcom/intsig/res/InkSettingResIds;->〇080(Landroid/content/Context;I)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0, p1, v0, v1, v1}, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇0O〇Oo〇o(Landroid/content/Context;Ljava/lang/String;ZI)Lcom/intsig/app/BaseProgressDialog;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    return-object p1
.end method

.method public O〇〇o8O()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8oOOo:Lcom/intsig/note/engine/entity/Document;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/note/engine/entity/Document;->〇80〇808〇O()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO00OOO(III)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/note/engine/view/DrawBoard;->〇o00〇〇Oo()Z

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8oOOo:Lcom/intsig/note/engine/entity/Document;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/note/engine/entity/Document;->〇o〇()Lcom/intsig/note/engine/entity/Page;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    iput-object p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇O〇〇O8:Lcom/intsig/note/engine/entity/Page;

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->〇0o88Oo〇()V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇080〇o0()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onBackPressed()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-interface {v0}, Lcom/intsig/note/engine/view/DrawBoard;->〇o00〇〇Oo()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    return v1

    .line 13
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇〇o8O()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇0o8o8〇()V

    .line 20
    .line 21
    .line 22
    return v1

    .line 23
    :cond_1
    const/4 v0, 0x0

    .line 24
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o0:Landroid/app/Activity;

    .line 5
    .line 6
    if-nez v0, :cond_1

    .line 7
    .line 8
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 19
    .line 20
    .line 21
    :cond_0
    return-void

    .line 22
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/notes/fragment/EditNoteFragment;->O80OO(Landroid/os/Bundle;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .line 1
    sget p3, Lcom/intsig/innote/R$layout;->ink_note_fragment_edit_note:I

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    iput-object p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇OOo8〇0:Landroid/view/View;

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->o0Oo()V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->OO0O()V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->〇oO88o()V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->O〇〇O80o8()V

    .line 20
    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇OOo8〇0:Landroid/view/View;

    .line 23
    .line 24
    return-object p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public onDestroy()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->o8oOOo:Lcom/intsig/note/engine/entity/Document;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/note/engine/entity/Document;->OO0o〇〇〇〇0()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    sput-boolean v1, Lcom/intsig/notes/fragment/EditNoteFragment;->ooO:Z

    .line 13
    .line 14
    :cond_0
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    .line 15
    .line 16
    .line 17
    invoke-static {}, Lcom/intsig/note/engine/draw/BitmapLoader;->〇o00〇〇Oo()Lcom/intsig/note/engine/draw/BitmapLoader;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lcom/intsig/note/engine/draw/BitmapLoader;->〇080()V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->〇0o0oO〇〇0()V

    .line 25
    .line 26
    .line 27
    iput-boolean v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇08〇o0O:Z

    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public onDestroyView()V
    .locals 1

    .line 1
    new-instance v0, L〇0oO/〇080;

    .line 2
    .line 3
    invoke-direct {v0, p0}, L〇0oO/〇080;-><init>(Lcom/intsig/notes/fragment/EditNoteFragment;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/notes/fragment/EditNoteFragment;->oOO8oo0(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oo8ooo8O:Lcom/hciilab/digitalink/core/PenType$OnPenRangeChangeListener;

    .line 10
    .line 11
    invoke-static {v0}, Lcom/hciilab/digitalink/core/PenType;->unregisterOnPenRangeChangeListener(Lcom/hciilab/digitalink/core/PenType$OnPenRangeChangeListener;)V

    .line 12
    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    iput-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->〇OOo8〇0:Landroid/view/View;

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->〇0o0oO〇〇0()V

    .line 18
    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->〇o〇OO80oO()V

    .line 21
    .line 22
    .line 23
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroyView()V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public onDetach()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDetach()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onPause()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-interface {v0}, Lcom/intsig/note/engine/view/DrawBoard;->onPause()V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onResume()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->OO:Lcom/intsig/note/engine/view/DrawBoard;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-interface {v0}, Lcom/intsig/note/engine/view/DrawBoard;->onResume()V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/notes/fragment/EditNoteFragment;->〇o〇OO80oO()V

    .line 5
    .line 6
    .line 7
    const-string v0, "scanner_bmp_rotated"

    .line 8
    .line 9
    iget-boolean v1, p0, Lcom/intsig/notes/fragment/EditNoteFragment;->oOo〇8o008:Z

    .line 10
    .line 11
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

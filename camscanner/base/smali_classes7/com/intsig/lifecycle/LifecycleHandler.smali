.class public Lcom/intsig/lifecycle/LifecycleHandler;
.super Landroid/os/Handler;
.source "LifecycleHandler.java"

# interfaces
.implements Landroidx/lifecycle/LifecycleObserver;


# instance fields
.field private o0:Landroidx/lifecycle/LifecycleOwner;


# direct methods
.method public constructor <init>(Landroid/os/Handler$Callback;Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    .line 2
    iput-object p2, p0, Lcom/intsig/lifecycle/LifecycleHandler;->o0:Landroidx/lifecycle/LifecycleOwner;

    .line 3
    invoke-direct {p0}, Lcom/intsig/lifecycle/LifecycleHandler;->〇080()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 4
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 5
    iput-object p2, p0, Lcom/intsig/lifecycle/LifecycleHandler;->o0:Landroidx/lifecycle/LifecycleOwner;

    .line 6
    invoke-direct {p0}, Lcom/intsig/lifecycle/LifecycleHandler;->〇080()V

    return-void
.end method

.method private onDestroy()V
    .locals 1
    .annotation runtime Landroidx/lifecycle/OnLifecycleEvent;
        value = .enum Landroidx/lifecycle/Lifecycle$Event;->ON_DESTROY:Landroidx/lifecycle/Lifecycle$Event;
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 3
    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/lifecycle/LifecycleHandler;->o0:Landroidx/lifecycle/LifecycleOwner;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-interface {v0}, Landroidx/lifecycle/LifecycleOwner;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0, p0}, Landroidx/lifecycle/Lifecycle;->removeObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇080()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/lifecycle/LifecycleHandler;->o0:Landroidx/lifecycle/LifecycleOwner;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Landroidx/lifecycle/LifecycleOwner;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0, p0}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public Lcom/intsig/note/engine/entity/Page;
.super Ljava/lang/Object;
.source "Page.java"

# interfaces
.implements Lcom/intsig/note/engine/io/FileUtil$FileIO;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/note/engine/entity/Page$OnChangeShadingListener;,
        Lcom/intsig/note/engine/entity/Page$PreviewGenerator;
    }
.end annotation


# instance fields
.field protected O0O:Ljava/lang/String;

.field protected O88O:Z

.field protected O8o08O8O:Lcom/intsig/note/engine/draw/DrawList;

.field protected OO:I

.field private OO〇00〇8oO:Ljava/lang/String;

.field protected o0:Lcom/intsig/note/engine/entity/Document;

.field private o8o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/note/engine/entity/Page$OnChangeShadingListener;",
            ">;"
        }
    .end annotation
.end field

.field private o8oOOo:Z

.field protected o8〇OO0〇0o:Ljava/lang/String;

.field protected oOO〇〇:F

.field protected oOo0:Z

.field protected oOo〇8o008:Lcom/intsig/note/engine/draw/CacheLayer;

.field private ooo0〇〇O:Z

.field protected o〇00O:Lcom/intsig/note/engine/draw/ThumbGenerator;

.field protected 〇080OO8〇0:Lcom/intsig/note/engine/entity/Attachment;

.field protected 〇08O〇00〇o:I

.field protected 〇0O:Lcom/intsig/note/engine/resource/Shading;

.field protected 〇8〇oO〇〇8o:Ljava/lang/String;

.field private 〇OOo8〇0:J

.field protected 〇O〇〇O8:Z

.field private 〇o0O:Z

.field protected 〇〇08O:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/intsig/note/engine/entity/Document;II)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/note/engine/entity/Page;->o8oOOo:Z

    .line 6
    .line 7
    new-instance v0, Ljava/util/ArrayList;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/note/engine/entity/Page;->o8o:Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/note/engine/entity/Page;->o800o8O()V

    .line 15
    .line 16
    .line 17
    iput-object p1, p0, Lcom/intsig/note/engine/entity/Page;->o0:Lcom/intsig/note/engine/entity/Document;

    .line 18
    .line 19
    invoke-virtual {p0, p2, p3}, Lcom/intsig/note/engine/entity/Page;->oo〇(II)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/note/engine/entity/Page;->〇00()Lcom/intsig/note/engine/draw/ThumbGenerator;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    iput-object p1, p0, Lcom/intsig/note/engine/entity/Page;->o〇00O:Lcom/intsig/note/engine/draw/ThumbGenerator;

    .line 27
    .line 28
    const/4 p1, 0x1

    .line 29
    iput-boolean p1, p0, Lcom/intsig/note/engine/entity/Page;->〇O〇〇O8:Z

    .line 30
    .line 31
    iput-boolean p1, p0, Lcom/intsig/note/engine/entity/Page;->〇o0O:Z

    .line 32
    .line 33
    iput-boolean p1, p0, Lcom/intsig/note/engine/entity/Page;->ooo0〇〇O:Z

    .line 34
    .line 35
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 36
    .line 37
    .line 38
    move-result-wide p2

    .line 39
    iput-wide p2, p0, Lcom/intsig/note/engine/entity/Page;->〇OOo8〇0:J

    .line 40
    .line 41
    new-instance p2, Ljava/lang/StringBuilder;

    .line 42
    .line 43
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 44
    .line 45
    .line 46
    const-string p3, ""

    .line 47
    .line 48
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    iget-wide v0, p0, Lcom/intsig/note/engine/entity/Page;->〇OOo8〇0:J

    .line 52
    .line 53
    invoke-virtual {p2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object p2

    .line 60
    iput-object p2, p0, Lcom/intsig/note/engine/entity/Page;->OO〇00〇8oO:Ljava/lang/String;

    .line 61
    .line 62
    new-instance p2, Ljava/lang/StringBuilder;

    .line 63
    .line 64
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .line 66
    .line 67
    const-string p3, "/pages/"

    .line 68
    .line 69
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    iget-object p3, p0, Lcom/intsig/note/engine/entity/Page;->OO〇00〇8oO:Ljava/lang/String;

    .line 73
    .line 74
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object p2

    .line 81
    iput-object p2, p0, Lcom/intsig/note/engine/entity/Page;->o8〇OO0〇0o:Ljava/lang/String;

    .line 82
    .line 83
    iget-object p2, p0, Lcom/intsig/note/engine/entity/Page;->o0:Lcom/intsig/note/engine/entity/Document;

    .line 84
    .line 85
    invoke-virtual {p2}, Lcom/intsig/note/engine/entity/Document;->o〇0()Lcom/intsig/note/engine/entity/Document$PageLoader;

    .line 86
    .line 87
    .line 88
    move-result-object p2

    .line 89
    invoke-virtual {p2, p0, p1}, Lcom/intsig/note/engine/entity/Document$PageLoader;->O8(Lcom/intsig/note/engine/entity/Page;Z)V

    .line 90
    .line 91
    .line 92
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public O8(II)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/note/engine/entity/Page;->O88O:Z

    .line 3
    .line 4
    invoke-virtual {p0, p1, p2}, Lcom/intsig/note/engine/entity/Page;->oo〇(II)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected O8ooOoo〇()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO0o〇〇()Landroid/graphics/Rect;
    .locals 4

    .line 1
    new-instance v0, Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/note/engine/entity/Page;->〇〇8O0〇8()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p0}, Lcom/intsig/note/engine/entity/Page;->〇O00()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    const/4 v3, 0x0

    .line 12
    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 13
    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO0o〇〇〇〇0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/note/engine/entity/Page;->〇08O〇00〇o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OOO〇O0(Lcom/intsig/note/engine/resource/Shading;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->〇0O:Lcom/intsig/note/engine/resource/Shading;

    .line 4
    .line 5
    if-eq v0, p1, :cond_0

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0, p0}, Lcom/intsig/note/engine/resource/Shading;->O8(Lcom/intsig/note/engine/entity/Page;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    iput-object p1, p0, Lcom/intsig/note/engine/entity/Page;->〇0O:Lcom/intsig/note/engine/resource/Shading;

    .line 13
    .line 14
    invoke-virtual {p1, p0}, Lcom/intsig/note/engine/resource/Shading;->〇o〇(Lcom/intsig/note/engine/entity/Page;)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->O8o08O8O:Lcom/intsig/note/engine/draw/DrawList;

    .line 18
    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/note/engine/entity/Page;->〇0O:Lcom/intsig/note/engine/resource/Shading;

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/draw/DrawList;->〇〇8O0〇8(Lcom/intsig/note/engine/resource/Shading;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->o8o:Ljava/util/ArrayList;

    .line 27
    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    if-eqz v1, :cond_1

    .line 39
    .line 40
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    check-cast v1, Lcom/intsig/note/engine/entity/Page$OnChangeShadingListener;

    .line 45
    .line 46
    invoke-interface {v1, p1}, Lcom/intsig/note/engine/entity/Page$OnChangeShadingListener;->〇080(Lcom/intsig/note/engine/resource/Shading;)V

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_1
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public Oo08()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/note/engine/entity/Page;->oOO〇〇:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OoO8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/note/engine/entity/Page;->OO:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oooo8o0〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->〇〇08O:Ljava/lang/String;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->o8〇OO0〇0o:Ljava/lang/String;

    .line 6
    .line 7
    :cond_0
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected O〇8O8〇008(Lcom/intsig/note/engine/draw/DrawList;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/intsig/note/engine/entity/Page;->O8o08O8O:Lcom/intsig/note/engine/draw/DrawList;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->oOo〇8o008:Lcom/intsig/note/engine/draw/CacheLayer;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    check-cast v0, Lcom/intsig/note/engine/draw/InkCacheLayer;

    .line 8
    .line 9
    invoke-virtual {v0, p1}, Lcom/intsig/note/engine/draw/InkCacheLayer;->〇0000OOO(Lcom/intsig/note/engine/draw/DrawList;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/note/engine/draw/InkCacheLayer;->o〇〇0〇()I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    if-nez p1, :cond_0

    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/note/engine/entity/Page;->O8o08O8O:Lcom/intsig/note/engine/draw/DrawList;

    .line 19
    .line 20
    const-class v1, Lcom/intsig/note/engine/draw/InkElement;

    .line 21
    .line 22
    invoke-virtual {p1, v1}, Lcom/intsig/note/engine/draw/DrawList;->Oooo8o0〇(Ljava/lang/Class;)I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    invoke-virtual {v0, p1}, Lcom/intsig/note/engine/draw/InkCacheLayer;->OOO〇O0(I)V

    .line 27
    .line 28
    .line 29
    :cond_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public o800o8O()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/note/engine/entity/Attachment;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/note/engine/entity/Attachment;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/note/engine/entity/Page;->〇080OO8〇0:Lcom/intsig/note/engine/entity/Attachment;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO80(Ljava/lang/Class;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/note/engine/draw/DrawElement;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->O8o08O8O:Lcom/intsig/note/engine/draw/DrawList;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/note/engine/draw/DrawList;->OO0o〇〇(Ljava/lang/Class;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public oo88o8O()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/note/engine/entity/Page;->ooo0〇〇O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oo〇(II)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/note/engine/entity/Page;->O88O:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iput p1, p0, Lcom/intsig/note/engine/entity/Page;->OO:I

    .line 6
    .line 7
    iput p2, p0, Lcom/intsig/note/engine/entity/Page;->〇08O〇00〇o:I

    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    iput-boolean p1, p0, Lcom/intsig/note/engine/entity/Page;->O88O:Z

    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public o〇0()Lcom/intsig/note/engine/draw/CacheLayer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->oOo〇8o008:Lcom/intsig/note/engine/draw/CacheLayer;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected o〇O8〇〇o()Lcom/intsig/note/engine/draw/DrawList;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/note/engine/draw/DrawList;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/note/engine/draw/DrawList;-><init>(Lcom/intsig/note/engine/entity/Page;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇〇0〇(F)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->o0:Lcom/intsig/note/engine/entity/Document;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/note/engine/entity/Document;->〇〇808〇(F)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected 〇00()Lcom/intsig/note/engine/draw/ThumbGenerator;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0000OOO(Lcom/intsig/note/engine/entity/Page$PreviewGenerator;)V
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iput-object p1, p0, Lcom/intsig/note/engine/entity/Page;->o〇00O:Lcom/intsig/note/engine/draw/ThumbGenerator;

    .line 4
    .line 5
    :cond_0
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇0〇O0088o()Lcom/intsig/note/engine/resource/Shading;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->〇0O:Lcom/intsig/note/engine/resource/Shading;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇80〇808〇O()Lcom/intsig/note/engine/draw/DrawList;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->O8o08O8O:Lcom/intsig/note/engine/draw/DrawList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8o8o〇()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->〇0O:Lcom/intsig/note/engine/resource/Shading;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/note/engine/resource/Shading;->〇80〇808〇O()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O00()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->o0:Lcom/intsig/note/engine/entity/Document;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/note/engine/entity/Document;->〇〇888()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget v1, p0, Lcom/intsig/note/engine/entity/Page;->〇08O〇00〇o:I

    .line 8
    .line 9
    int-to-float v1, v1

    .line 10
    mul-float v0, v0, v1

    .line 11
    .line 12
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O888o0o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/note/engine/entity/Page;->〇O〇〇O8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O8o08O()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->〇0O:Lcom/intsig/note/engine/resource/Shading;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/note/engine/resource/Shading;->〇8o8o〇()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O〇()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->o0:Lcom/intsig/note/engine/entity/Document;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/note/engine/entity/Document;->〇〇888()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇oOO8O8()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/note/engine/entity/Page;->oOo0:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/intsig/note/engine/entity/Page;->oOo0:Z

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->o0:Lcom/intsig/note/engine/entity/Document;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/note/engine/entity/Document;->o〇0()Lcom/intsig/note/engine/entity/Document$PageLoader;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0, p0}, Lcom/intsig/note/engine/entity/Document$PageLoader;->〇o〇(Lcom/intsig/note/engine/entity/Page;)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->〇0O:Lcom/intsig/note/engine/resource/Shading;

    .line 19
    .line 20
    invoke-virtual {p0, v0}, Lcom/intsig/note/engine/entity/Page;->OOO〇O0(Lcom/intsig/note/engine/resource/Shading;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇oo〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/note/engine/entity/Page;->〇O〇〇O8:Z

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    iget-boolean p1, p0, Lcom/intsig/note/engine/entity/Page;->o8oOOo:Z

    .line 6
    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    iput-boolean p1, p0, Lcom/intsig/note/engine/entity/Page;->o8oOOo:Z

    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇o〇(Lcom/intsig/note/engine/entity/Page$OnChangeShadingListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->o8o:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇〇808〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->O0O:Ljava/lang/String;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 6
    .line 7
    :cond_0
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888()Lcom/intsig/note/engine/entity/Document;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->o0:Lcom/intsig/note/engine/entity/Document;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇8O0〇8()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/entity/Page;->o0:Lcom/intsig/note/engine/entity/Document;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/note/engine/entity/Document;->〇〇888()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget v1, p0, Lcom/intsig/note/engine/entity/Page;->OO:I

    .line 8
    .line 9
    int-to-float v1, v1

    .line 10
    mul-float v0, v0, v1

    .line 11
    .line 12
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

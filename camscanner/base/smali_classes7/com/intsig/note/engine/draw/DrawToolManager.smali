.class public final Lcom/intsig/note/engine/draw/DrawToolManager;
.super Ljava/lang/Object;
.source "DrawToolManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/note/engine/draw/DrawToolManager$NoneTool;
    }
.end annotation


# instance fields
.field private 〇080:Lcom/intsig/note/engine/draw/DrawTool;

.field private 〇o00〇〇Oo:Lcom/intsig/note/engine/draw/DrawTool;

.field private 〇o〇:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/intsig/note/engine/draw/DrawTool;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Landroid/util/SparseArray;

    .line 5
    .line 6
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o〇:Landroid/util/SparseArray;

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/note/engine/draw/DrawToolManager;->Oo08()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public O8(Lcom/intsig/note/engine/draw/DrawTool;)I
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, -0x1

    .line 4
    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o〇:Landroid/util/SparseArray;

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->keyAt(I)I

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    :goto_0
    return p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public Oo08()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o〇:Landroid/util/SparseArray;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/note/engine/draw/DrawToolManager$NoneTool;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/note/engine/draw/DrawToolManager$NoneTool;-><init>(Lcom/intsig/note/engine/draw/DrawToolManager;)V

    .line 6
    .line 7
    .line 8
    const/4 v2, 0x4

    .line 9
    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o〇:Landroid/util/SparseArray;

    .line 13
    .line 14
    new-instance v1, Lcom/intsig/note/engine/draw/InkTool;

    .line 15
    .line 16
    invoke-direct {v1}, Lcom/intsig/note/engine/draw/InkTool;-><init>()V

    .line 17
    .line 18
    .line 19
    const/4 v2, 0x0

    .line 20
    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o〇:Landroid/util/SparseArray;

    .line 24
    .line 25
    new-instance v1, Lcom/intsig/note/engine/draw/EraserTool;

    .line 26
    .line 27
    invoke-direct {v1}, Lcom/intsig/note/engine/draw/EraserTool;-><init>()V

    .line 28
    .line 29
    .line 30
    const/4 v2, 0x3

    .line 31
    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public o〇0()Lcom/intsig/note/engine/draw/DrawTool;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o00〇〇Oo:Lcom/intsig/note/engine/draw/DrawTool;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇080:Lcom/intsig/note/engine/draw/DrawTool;

    .line 6
    .line 7
    return-object v0

    .line 8
    :cond_0
    iget-object v1, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇080:Lcom/intsig/note/engine/draw/DrawTool;

    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇080:Lcom/intsig/note/engine/draw/DrawTool;

    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    iput-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o00〇〇Oo:Lcom/intsig/note/engine/draw/DrawTool;

    .line 14
    .line 15
    return-object v1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080(I)Lcom/intsig/note/engine/draw/DrawTool;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇080:Lcom/intsig/note/engine/draw/DrawTool;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o〇:Landroid/util/SparseArray;

    .line 4
    .line 5
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇080:Lcom/intsig/note/engine/draw/DrawTool;

    .line 12
    .line 13
    return-object p1

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇080:Lcom/intsig/note/engine/draw/DrawTool;

    .line 15
    .line 16
    invoke-virtual {p0, v0}, Lcom/intsig/note/engine/draw/DrawToolManager;->O8(Lcom/intsig/note/engine/draw/DrawTool;)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    const/4 v1, 0x3

    .line 23
    if-ne v0, v1, :cond_2

    .line 24
    .line 25
    :cond_1
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇080:Lcom/intsig/note/engine/draw/DrawTool;

    .line 26
    .line 27
    iput-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o00〇〇Oo:Lcom/intsig/note/engine/draw/DrawTool;

    .line 28
    .line 29
    :cond_2
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o〇:Landroid/util/SparseArray;

    .line 30
    .line 31
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    check-cast p1, Lcom/intsig/note/engine/draw/DrawTool;

    .line 36
    .line 37
    iput-object p1, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇080:Lcom/intsig/note/engine/draw/DrawTool;

    .line 38
    .line 39
    return-object p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇o00〇〇Oo()Lcom/intsig/note/engine/draw/DrawTool;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇080:Lcom/intsig/note/engine/draw/DrawTool;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o〇:Landroid/util/SparseArray;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/note/engine/draw/DrawTool;

    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇080:Lcom/intsig/note/engine/draw/DrawTool;

    .line 15
    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇080:Lcom/intsig/note/engine/draw/DrawTool;

    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇(I)Lcom/intsig/note/engine/draw/DrawTool;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o〇:Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/note/engine/draw/DrawTool;

    .line 8
    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public abstract Lcom/intsig/note/engine/draw/DrawElement;
.super Lcom/intsig/note/engine/draw/Element;
.source "DrawElement.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;
    }
.end annotation


# instance fields
.field protected O8o08O8O:Landroid/graphics/RectF;

.field protected OO〇00〇8oO:Lcom/intsig/note/engine/draw/BitmapLoader;

.field protected o8〇OO0〇0o:Z

.field private oOo0:Landroid/graphics/Matrix;

.field private oOo〇8o008:Landroid/graphics/Matrix;

.field private ooo0〇〇O:[F

.field protected o〇00O:Landroid/graphics/Rect;

.field protected 〇080OO8〇0:Z

.field private 〇0O:Landroid/graphics/RectF;

.field protected 〇8〇oO〇〇8o:Lcom/intsig/note/engine/draw/DrawList;


# direct methods
.method public constructor <init>(Lcom/google/gson/stream/JsonReader;Lcom/intsig/note/engine/draw/DrawList;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/note/engine/draw/Element;-><init>(Lcom/google/gson/stream/JsonReader;Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x2

    new-array p1, p1, [F

    .line 6
    iput-object p1, p0, Lcom/intsig/note/engine/draw/DrawElement;->ooo0〇〇O:[F

    .line 7
    iput-object p2, p0, Lcom/intsig/note/engine/draw/DrawElement;->〇8〇oO〇〇8o:Lcom/intsig/note/engine/draw/DrawList;

    .line 8
    invoke-direct {p0}, Lcom/intsig/note/engine/draw/DrawElement;->o〇0()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/note/engine/draw/DrawList;)V
    .locals 1

    .line 9
    invoke-direct {p0}, Lcom/intsig/note/engine/draw/Element;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [F

    .line 10
    iput-object v0, p0, Lcom/intsig/note/engine/draw/DrawElement;->ooo0〇〇O:[F

    .line 11
    iput-object p1, p0, Lcom/intsig/note/engine/draw/DrawElement;->〇8〇oO〇〇8o:Lcom/intsig/note/engine/draw/DrawList;

    .line 12
    invoke-direct {p0}, Lcom/intsig/note/engine/draw/DrawElement;->o〇0()V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;Lcom/intsig/note/engine/draw/DrawList;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/note/engine/draw/Element;-><init>(Lorg/json/JSONObject;Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x2

    new-array p1, p1, [F

    .line 2
    iput-object p1, p0, Lcom/intsig/note/engine/draw/DrawElement;->ooo0〇〇O:[F

    .line 3
    iput-object p2, p0, Lcom/intsig/note/engine/draw/DrawElement;->〇8〇oO〇〇8o:Lcom/intsig/note/engine/draw/DrawList;

    .line 4
    invoke-direct {p0}, Lcom/intsig/note/engine/draw/DrawElement;->o〇0()V

    return-void
.end method

.method private o〇0()V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/note/engine/draw/DrawElement;->O8o08O8O:Landroid/graphics/RectF;

    .line 7
    .line 8
    new-instance v0, Landroid/graphics/Rect;

    .line 9
    .line 10
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/note/engine/draw/DrawElement;->o〇00O:Landroid/graphics/Rect;

    .line 14
    .line 15
    new-instance v0, Landroid/graphics/RectF;

    .line 16
    .line 17
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/note/engine/draw/DrawElement;->〇0O:Landroid/graphics/RectF;

    .line 21
    .line 22
    new-instance v0, Landroid/graphics/Matrix;

    .line 23
    .line 24
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 25
    .line 26
    .line 27
    iput-object v0, p0, Lcom/intsig/note/engine/draw/DrawElement;->oOo〇8o008:Landroid/graphics/Matrix;

    .line 28
    .line 29
    new-instance v0, Landroid/graphics/Matrix;

    .line 30
    .line 31
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 32
    .line 33
    .line 34
    iput-object v0, p0, Lcom/intsig/note/engine/draw/DrawElement;->oOo0:Landroid/graphics/Matrix;

    .line 35
    .line 36
    invoke-static {}, Lcom/intsig/note/engine/draw/BitmapLoader;->〇o00〇〇Oo()Lcom/intsig/note/engine/draw/BitmapLoader;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    iput-object v0, p0, Lcom/intsig/note/engine/draw/DrawElement;->OO〇00〇8oO:Lcom/intsig/note/engine/draw/BitmapLoader;

    .line 41
    .line 42
    const-string v0, "Rect"

    .line 43
    .line 44
    invoke-virtual {p0, v0}, Lcom/intsig/note/engine/draw/Element;->Oo08(Ljava/lang/String;)Lcom/intsig/note/engine/draw/Param;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    if-nez v0, :cond_0

    .line 49
    .line 50
    new-instance v0, Lcom/intsig/note/engine/draw/RectParam;

    .line 51
    .line 52
    new-instance v1, Landroid/graphics/Rect;

    .line 53
    .line 54
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 55
    .line 56
    .line 57
    invoke-direct {v0, v1}, Lcom/intsig/note/engine/draw/RectParam;-><init>(Landroid/graphics/Rect;)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p0, v0}, Lcom/intsig/note/engine/draw/Element;->OO0o〇〇〇〇0(Lcom/intsig/note/engine/draw/Param;)V

    .line 61
    .line 62
    .line 63
    :cond_0
    const-string v0, "Rotate"

    .line 64
    .line 65
    invoke-virtual {p0, v0}, Lcom/intsig/note/engine/draw/Element;->Oo08(Ljava/lang/String;)Lcom/intsig/note/engine/draw/Param;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    if-nez v0, :cond_1

    .line 70
    .line 71
    new-instance v0, Lcom/intsig/note/engine/draw/RotateParam;

    .line 72
    .line 73
    const/4 v1, 0x0

    .line 74
    invoke-direct {v0, v1}, Lcom/intsig/note/engine/draw/RotateParam;-><init>(F)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {p0, v0}, Lcom/intsig/note/engine/draw/Element;->OO0o〇〇〇〇0(Lcom/intsig/note/engine/draw/Param;)V

    .line 78
    .line 79
    .line 80
    :cond_1
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method


# virtual methods
.method public O8ooOoo〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/note/engine/draw/DrawElement;->o8〇OO0〇0o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public OO0o〇〇(Landroid/graphics/Canvas;Lcom/hciilab/digitalink/core/InkCanvas;Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;)V
    .locals 3

    .line 1
    const-string v0, "Rect"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/note/engine/draw/Element;->Oo08(Ljava/lang/String;)Lcom/intsig/note/engine/draw/Param;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/note/engine/draw/DrawElement;->〇8〇oO〇〇8o:Lcom/intsig/note/engine/draw/DrawList;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/note/engine/draw/DrawList;->〇〇808〇()Lcom/intsig/note/engine/entity/Page;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {v1}, Lcom/intsig/note/engine/entity/Page;->〇O〇()F

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1, v1, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 21
    .line 22
    .line 23
    if-eqz v0, :cond_0

    .line 24
    .line 25
    new-instance v1, Landroid/graphics/RectF;

    .line 26
    .line 27
    iget-object v0, v0, Lcom/intsig/note/engine/draw/Param;->〇OOo8〇0:Ljava/lang/Object;

    .line 28
    .line 29
    check-cast v0, Landroid/graphics/Rect;

    .line 30
    .line 31
    invoke-direct {v1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/intsig/note/engine/draw/DrawElement;->〇〇8O0〇8()F

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    invoke-virtual {p1, v0, v2, v1}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/note/engine/draw/DrawElement;->〇00(Landroid/graphics/Canvas;Lcom/hciilab/digitalink/core/InkCanvas;Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/note/engine/draw/DrawElement;->〇00(Landroid/graphics/Canvas;Lcom/hciilab/digitalink/core/InkCanvas;Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;)V

    .line 60
    .line 61
    .line 62
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 63
    .line 64
    .line 65
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public OoO8(IILcom/intsig/note/engine/draw/DrawElement$MatrixInfo;)Z
    .locals 2

    .line 1
    const-string v0, "Rect"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/note/engine/draw/Element;->Oo08(Ljava/lang/String;)Lcom/intsig/note/engine/draw/Param;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v0, v0, Lcom/intsig/note/engine/draw/Param;->〇OOo8〇0:Ljava/lang/Object;

    .line 8
    .line 9
    check-cast v0, Landroid/graphics/Rect;

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/note/engine/draw/DrawElement;->O8o08O8O:Landroid/graphics/RectF;

    .line 12
    .line 13
    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawElement;->oOo0:Landroid/graphics/Matrix;

    .line 17
    .line 18
    iget-object p3, p3, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇080:Landroid/graphics/Matrix;

    .line 19
    .line 20
    invoke-virtual {v0, p3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/note/engine/draw/DrawElement;->Oooo8o0〇()Lcom/intsig/note/engine/draw/DrawList;

    .line 24
    .line 25
    .line 26
    move-result-object p3

    .line 27
    invoke-virtual {p3}, Lcom/intsig/note/engine/draw/DrawList;->〇〇808〇()Lcom/intsig/note/engine/entity/Page;

    .line 28
    .line 29
    .line 30
    move-result-object p3

    .line 31
    invoke-virtual {p3}, Lcom/intsig/note/engine/entity/Page;->〇O〇()F

    .line 32
    .line 33
    .line 34
    move-result p3

    .line 35
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawElement;->oOo0:Landroid/graphics/Matrix;

    .line 36
    .line 37
    invoke-virtual {v0, p3, p3}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 38
    .line 39
    .line 40
    iget-object p3, p0, Lcom/intsig/note/engine/draw/DrawElement;->oOo0:Landroid/graphics/Matrix;

    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawElement;->O8o08O8O:Landroid/graphics/RectF;

    .line 43
    .line 44
    invoke-virtual {p3, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 45
    .line 46
    .line 47
    iget-object p3, p0, Lcom/intsig/note/engine/draw/DrawElement;->O8o08O8O:Landroid/graphics/RectF;

    .line 48
    .line 49
    int-to-float p1, p1

    .line 50
    int-to-float p2, p2

    .line 51
    invoke-virtual {p3, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    return p1
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public Oooo8o0〇()Lcom/intsig/note/engine/draw/DrawList;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawElement;->〇8〇oO〇〇8o:Lcom/intsig/note/engine/draw/DrawList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public abstract O〇8O8〇008(Landroid/view/MotionEvent;Lcom/hciilab/digitalink/core/InkCanvas;Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;)Z
.end method

.method protected o800o8O()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO80()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawElement;->〇8〇oO〇〇8o:Lcom/intsig/note/engine/draw/DrawList;

    .line 2
    .line 3
    invoke-virtual {v0, p0}, Lcom/intsig/note/engine/draw/DrawList;->〇O〇(Lcom/intsig/note/engine/draw/DrawElement;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public abstract oo88o8O()Z
.end method

.method public abstract o〇O8〇〇o()Z
.end method

.method public abstract 〇00(Landroid/graphics/Canvas;Lcom/hciilab/digitalink/core/InkCanvas;Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;)V
.end method

.method public 〇0〇O0088o()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/note/engine/draw/DrawElement;->〇O〇()Landroid/graphics/Rect;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 6
    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O00()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/note/engine/draw/DrawElement;->〇O〇()Landroid/graphics/Rect;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 6
    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O888o0o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/note/engine/draw/DrawElement;->〇080OO8〇0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method 〇O〇()Landroid/graphics/Rect;
    .locals 1

    .line 1
    const-string v0, "Rect"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/note/engine/draw/Element;->Oo08(Ljava/lang/String;)Lcom/intsig/note/engine/draw/Param;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v0, v0, Lcom/intsig/note/engine/draw/Param;->〇OOo8〇0:Ljava/lang/Object;

    .line 8
    .line 9
    check-cast v0, Landroid/graphics/Rect;

    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇oo〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/note/engine/draw/DrawElement;->o8〇OO0〇0o:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇808〇()Landroid/graphics/RectF;
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    const-string v1, "Rect"

    .line 4
    .line 5
    invoke-virtual {p0, v1}, Lcom/intsig/note/engine/draw/Element;->Oo08(Ljava/lang/String;)Lcom/intsig/note/engine/draw/Param;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    iget-object v1, v1, Lcom/intsig/note/engine/draw/Param;->〇OOo8〇0:Ljava/lang/Object;

    .line 10
    .line 11
    check-cast v1, Landroid/graphics/Rect;

    .line 12
    .line 13
    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/note/engine/draw/Element;->〇〇888()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/note/engine/draw/DrawElement;->〇8〇oO〇〇8o:Lcom/intsig/note/engine/draw/DrawList;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/note/engine/draw/DrawList;->〇〇808〇()Lcom/intsig/note/engine/entity/Page;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const/4 v1, 0x1

    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/entity/Page;->〇oo〇(Z)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇8O0〇8()F
    .locals 1

    .line 1
    const-string v0, "Rotate"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/note/engine/draw/Element;->Oo08(Ljava/lang/String;)Lcom/intsig/note/engine/draw/Param;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/note/engine/draw/RotateParam;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/note/engine/draw/RotateParam;->o〇0()F

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

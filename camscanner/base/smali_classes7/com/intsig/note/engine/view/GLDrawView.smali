.class public Lcom/intsig/note/engine/view/GLDrawView;
.super Landroid/opengl/GLSurfaceView;
.source "GLDrawView.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/note/engine/view/GLDrawView$OnSelectedChangeListener;,
        Lcom/intsig/note/engine/view/GLDrawView$MyRender;,
        Lcom/intsig/note/engine/view/GLDrawView$ScaleGestureListener;
    }
.end annotation


# instance fields
.field private O0O:I

.field private O88O:Lcom/intsig/note/engine/view/MatrixManager;

.field private O8o08O8O:Z

.field private O8o〇O0:J

.field private O8〇o〇88:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private OO:Landroid/graphics/Bitmap;

.field private OO〇00〇8oO:F

.field private OO〇OOo:F

.field private Oo0O0o8:Z

.field protected Oo0〇Ooo:[F

.field protected Oo80:Lcom/intsig/note/engine/draw/DrawToolManager;

.field private Ooo08:Z

.field private O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

.field protected O〇O:Lcom/intsig/note/engine/view/DrawCanvas$OnMatrixChangeListener;

.field protected O〇o88o08〇:Lcom/intsig/note/engine/history/HistoryActionStack;

.field protected o0:Lcom/hciilab/digitalink/core/InkCanvas;

.field protected o0OoOOo0:Z

.field private o8O:Lcom/intsig/note/engine/view/DrawCanvas$OnInitListener;

.field protected o8o:Landroid/graphics/Matrix;

.field private o8oOOo:I

.field protected o8〇OO:Z

.field protected o8〇OO0〇0o:F

.field private oO00〇o:Z

.field private oOO0880O:Z

.field private oOO8:J

.field protected oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

.field private oOo0:Landroid/graphics/drawable/Drawable;

.field private oOoo80oO:Z

.field private oOo〇08〇:Lcom/intsig/note/engine/view/GLDrawView$OnSelectedChangeListener;

.field private oOo〇8o008:I

.field private oO〇8O8oOo:Z

.field private oO〇oo:Lcom/intsig/note/engine/view/DrawCanvas$OnPageLoadedListener;

.field protected oo8ooo8O:Landroid/graphics/RectF;

.field protected ooO:Landroid/os/Handler;

.field private ooo0〇〇O:F

.field protected o〇00O:Landroid/util/DisplayMetrics;

.field protected o〇oO:Landroid/graphics/RectF;

.field protected o〇o〇Oo88:Z

.field protected 〇00O0:Lcom/intsig/note/engine/draw/DrawList;

.field private 〇080OO8〇0:Landroid/view/GestureDetector$OnGestureListener;

.field private 〇08O〇00〇o:Landroid/graphics/drawable/BitmapDrawable;

.field private 〇08〇o0O:I

.field private 〇0O:Lcom/intsig/note/engine/entity/Page$PreviewGenerator;

.field private 〇0O〇O00O:Z

.field private 〇800OO〇0O:F

.field private 〇80O8o8O〇:Lcom/intsig/note/engine/view/DrawCanvas$OnScaleListener;

.field private 〇8〇o88:Lcom/intsig/note/engine/view/DrawCanvas$OnSizeChangeListener;

.field private 〇8〇oO〇〇8o:F

.field protected 〇O8oOo0:Lcom/intsig/note/engine/view/DrawCanvas$OnLoadListener;

.field private 〇OO8ooO8〇:Landroid/os/HandlerThread;

.field private 〇OOo8〇0:Landroid/graphics/Canvas;

.field protected 〇OO〇00〇0O:Landroid/os/Handler;

.field private 〇O〇〇O8:Lcom/intsig/note/engine/view/CustomScaleGestureDetector;

.field private 〇o0O:Landroid/view/GestureDetector;

.field protected 〇oo〇O〇80:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private 〇〇08O:F

.field private 〇〇o〇:I

.field private 〇〇〇0o〇〇0:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    const/4 p2, 0x1

    .line 5
    iput-boolean p2, p0, Lcom/intsig/note/engine/view/GLDrawView;->O8o08O8O:Z

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/note/engine/view/GLDrawView$1;

    .line 8
    .line 9
    invoke-direct {v0, p0}, Lcom/intsig/note/engine/view/GLDrawView$1;-><init>(Lcom/intsig/note/engine/view/GLDrawView;)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇080OO8〇0:Landroid/view/GestureDetector$OnGestureListener;

    .line 13
    .line 14
    new-instance v0, Lcom/intsig/note/engine/view/GLDrawView$2;

    .line 15
    .line 16
    invoke-direct {v0, p0}, Lcom/intsig/note/engine/view/GLDrawView$2;-><init>(Lcom/intsig/note/engine/view/GLDrawView;)V

    .line 17
    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇0O:Lcom/intsig/note/engine/entity/Page$PreviewGenerator;

    .line 20
    .line 21
    const/high16 v0, 0x40a00000    # 5.0f

    .line 22
    .line 23
    iput v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->OO〇00〇8oO:F

    .line 24
    .line 25
    const/high16 v0, 0x3f800000    # 1.0f

    .line 26
    .line 27
    iput v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8〇OO0〇0o:F

    .line 28
    .line 29
    const/16 v0, 0x9

    .line 30
    .line 31
    new-array v0, v0, [F

    .line 32
    .line 33
    iput-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0〇Ooo:[F

    .line 34
    .line 35
    iput-boolean p2, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOoo80oO:Z

    .line 36
    .line 37
    new-instance p2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 38
    .line 39
    invoke-direct {p2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    .line 40
    .line 41
    .line 42
    iput-object p2, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇oo〇O〇80:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 43
    .line 44
    new-instance p2, Ljava/util/ArrayList;

    .line 45
    .line 46
    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 47
    .line 48
    .line 49
    iput-object p2, p0, Lcom/intsig/note/engine/view/GLDrawView;->O8〇o〇88:Ljava/util/ArrayList;

    .line 50
    .line 51
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    .line 52
    .line 53
    .line 54
    move-result-object p2

    .line 55
    const/4 v0, 0x0

    .line 56
    if-eqz p2, :cond_1

    .line 57
    .line 58
    instance-of v1, p2, Landroid/graphics/drawable/BitmapDrawable;

    .line 59
    .line 60
    if-eqz v1, :cond_1

    .line 61
    .line 62
    check-cast p2, Landroid/graphics/drawable/BitmapDrawable;

    .line 63
    .line 64
    invoke-virtual {p2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 73
    .line 74
    if-ne v1, v2, :cond_0

    .line 75
    .line 76
    iput-object p2, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇08O〇00〇o:Landroid/graphics/drawable/BitmapDrawable;

    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_0
    invoke-virtual {p2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 84
    .line 85
    .line 86
    move-result v2

    .line 87
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 88
    .line 89
    .line 90
    move-result v3

    .line 91
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 92
    .line 93
    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 94
    .line 95
    .line 96
    move-result-object v2

    .line 97
    new-instance v3, Landroid/graphics/Canvas;

    .line 98
    .line 99
    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 100
    .line 101
    .line 102
    const/4 v4, 0x0

    .line 103
    invoke-virtual {v3, v1, v4, v4, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 104
    .line 105
    .line 106
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 107
    .line 108
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 109
    .line 110
    .line 111
    move-result-object v3

    .line 112
    invoke-direct {v1, v3, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 113
    .line 114
    .line 115
    iput-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇08O〇00〇o:Landroid/graphics/drawable/BitmapDrawable;

    .line 116
    .line 117
    invoke-virtual {p2}, Landroid/graphics/drawable/BitmapDrawable;->getTileModeX()Landroid/graphics/Shader$TileMode;

    .line 118
    .line 119
    .line 120
    move-result-object v2

    .line 121
    invoke-virtual {p2}, Landroid/graphics/drawable/BitmapDrawable;->getTileModeY()Landroid/graphics/Shader$TileMode;

    .line 122
    .line 123
    .line 124
    move-result-object p2

    .line 125
    invoke-virtual {v1, v2, p2}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 126
    .line 127
    .line 128
    :cond_1
    :goto_0
    new-instance p2, Lcom/hciilab/digitalink/core/InkCanvas;

    .line 129
    .line 130
    invoke-direct {p2}, Lcom/hciilab/digitalink/core/InkCanvas;-><init>()V

    .line 131
    .line 132
    .line 133
    iput-object p2, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 134
    .line 135
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 136
    .line 137
    .line 138
    move-result-object p2

    .line 139
    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 140
    .line 141
    .line 142
    move-result-object p2

    .line 143
    iput-object p2, p0, Lcom/intsig/note/engine/view/GLDrawView;->o〇00O:Landroid/util/DisplayMetrics;

    .line 144
    .line 145
    const-string p2, "activity"

    .line 146
    .line 147
    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 148
    .line 149
    .line 150
    move-result-object p2

    .line 151
    check-cast p2, Landroid/app/ActivityManager;

    .line 152
    .line 153
    new-instance v1, Ljava/lang/StringBuilder;

    .line 154
    .line 155
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 156
    .line 157
    .line 158
    const-string v2, "OpenGL ES v"

    .line 159
    .line 160
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    .line 162
    .line 163
    invoke-virtual {p2}, Landroid/app/ActivityManager;->getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;

    .line 164
    .line 165
    .line 166
    move-result-object p2

    .line 167
    iget p2, p2, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    .line 168
    .line 169
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 170
    .line 171
    .line 172
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object p2

    .line 176
    const-string v1, "GLDrawView"

    .line 177
    .line 178
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    .line 180
    .line 181
    const/4 p2, 0x2

    .line 182
    invoke-virtual {p0, p2}, Landroid/opengl/GLSurfaceView;->setEGLContextClientVersion(I)V

    .line 183
    .line 184
    .line 185
    const/16 v3, 0x8

    .line 186
    .line 187
    const/16 v4, 0x8

    .line 188
    .line 189
    const/16 v5, 0x8

    .line 190
    .line 191
    const/16 v6, 0x8

    .line 192
    .line 193
    const/4 v7, 0x0

    .line 194
    const/16 v8, 0x8

    .line 195
    .line 196
    move-object v2, p0

    .line 197
    invoke-virtual/range {v2 .. v8}, Landroid/opengl/GLSurfaceView;->setEGLConfigChooser(IIIIII)V

    .line 198
    .line 199
    .line 200
    new-instance p2, Lcom/intsig/note/engine/view/GLDrawView$MyRender;

    .line 201
    .line 202
    invoke-direct {p2, p0}, Lcom/intsig/note/engine/view/GLDrawView$MyRender;-><init>(Lcom/intsig/note/engine/view/GLDrawView;)V

    .line 203
    .line 204
    .line 205
    invoke-virtual {p0, p2}, Landroid/opengl/GLSurfaceView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 206
    .line 207
    .line 208
    const/4 p2, 0x0

    .line 209
    invoke-virtual {p0, p2}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    .line 210
    .line 211
    .line 212
    new-instance p2, Landroid/view/GestureDetector;

    .line 213
    .line 214
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇080OO8〇0:Landroid/view/GestureDetector$OnGestureListener;

    .line 215
    .line 216
    invoke-direct {p2, p1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 217
    .line 218
    .line 219
    iput-object p2, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇o0O:Landroid/view/GestureDetector;

    .line 220
    .line 221
    new-instance p1, Lcom/intsig/note/engine/view/CustomScaleGestureDetector;

    .line 222
    .line 223
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 224
    .line 225
    .line 226
    move-result-object p2

    .line 227
    new-instance v2, Lcom/intsig/note/engine/view/GLDrawView$ScaleGestureListener;

    .line 228
    .line 229
    invoke-direct {v2, p0, v0}, Lcom/intsig/note/engine/view/GLDrawView$ScaleGestureListener;-><init>(Lcom/intsig/note/engine/view/GLDrawView;Lo〇Oo/o800o8O;)V

    .line 230
    .line 231
    .line 232
    invoke-direct {p1, p2, v2}, Lcom/intsig/note/engine/view/CustomScaleGestureDetector;-><init>(Landroid/content/Context;Lcom/intsig/note/engine/view/CustomScaleGestureDetector$OnScaleGestureListener;)V

    .line 233
    .line 234
    .line 235
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇O〇〇O8:Lcom/intsig/note/engine/view/CustomScaleGestureDetector;

    .line 236
    .line 237
    invoke-static {}, Lcom/intsig/note/engine/view/MatrixManager;->〇080()Lcom/intsig/note/engine/view/MatrixManager;

    .line 238
    .line 239
    .line 240
    move-result-object p1

    .line 241
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->O88O:Lcom/intsig/note/engine/view/MatrixManager;

    .line 242
    .line 243
    new-instance p1, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 244
    .line 245
    invoke-direct {p1}, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;-><init>()V

    .line 246
    .line 247
    .line 248
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 249
    .line 250
    new-instance p1, Landroid/graphics/Matrix;

    .line 251
    .line 252
    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    .line 253
    .line 254
    .line 255
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 256
    .line 257
    new-instance p1, Landroid/graphics/RectF;

    .line 258
    .line 259
    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    .line 260
    .line 261
    .line 262
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oo8ooo8O:Landroid/graphics/RectF;

    .line 263
    .line 264
    new-instance p1, Landroid/graphics/RectF;

    .line 265
    .line 266
    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    .line 267
    .line 268
    .line 269
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o〇oO:Landroid/graphics/RectF;

    .line 270
    .line 271
    new-instance p1, Lcom/intsig/note/engine/draw/DrawToolManager;

    .line 272
    .line 273
    invoke-direct {p1}, Lcom/intsig/note/engine/draw/DrawToolManager;-><init>()V

    .line 274
    .line 275
    .line 276
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo80:Lcom/intsig/note/engine/draw/DrawToolManager;

    .line 277
    .line 278
    new-instance p1, Lcom/intsig/note/engine/history/HistoryActionStack;

    .line 279
    .line 280
    invoke-direct {p1}, Lcom/intsig/note/engine/history/HistoryActionStack;-><init>()V

    .line 281
    .line 282
    .line 283
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇o88o08〇:Lcom/intsig/note/engine/history/HistoryActionStack;

    .line 284
    .line 285
    new-instance p1, Landroid/os/Handler;

    .line 286
    .line 287
    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    .line 288
    .line 289
    .line 290
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OO〇00〇0O:Landroid/os/Handler;

    .line 291
    .line 292
    new-instance p1, Landroid/os/HandlerThread;

    .line 293
    .line 294
    invoke-direct {p1, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 295
    .line 296
    .line 297
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OO8ooO8〇:Landroid/os/HandlerThread;

    .line 298
    .line 299
    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    .line 300
    .line 301
    .line 302
    new-instance p1, Landroid/os/Handler;

    .line 303
    .line 304
    iget-object p2, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OO8ooO8〇:Landroid/os/HandlerThread;

    .line 305
    .line 306
    invoke-virtual {p2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    .line 307
    .line 308
    .line 309
    move-result-object p2

    .line 310
    new-instance v0, Lcom/intsig/note/engine/view/GLDrawView$3;

    .line 311
    .line 312
    invoke-direct {v0, p0}, Lcom/intsig/note/engine/view/GLDrawView$3;-><init>(Lcom/intsig/note/engine/view/GLDrawView;)V

    .line 313
    .line 314
    .line 315
    invoke-direct {p1, p2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 316
    .line 317
    .line 318
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->ooO:Landroid/os/Handler;

    .line 319
    .line 320
    return-void
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method static bridge synthetic O8(Lcom/intsig/note/engine/view/GLDrawView;)Landroid/graphics/Canvas;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OOo8〇0:Landroid/graphics/Canvas;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic O8ooOoo〇(Lcom/intsig/note/engine/view/GLDrawView;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOo0:Landroid/graphics/drawable/Drawable;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic O8〇o(Lcom/intsig/note/engine/view/GLDrawView;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇〇〇0o〇〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic OO0o〇〇(Lcom/intsig/note/engine/view/GLDrawView;)Lcom/intsig/note/engine/view/DrawCanvas$OnPageLoadedListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oO〇oo:Lcom/intsig/note/engine/view/DrawCanvas$OnPageLoadedListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic OO0o〇〇〇〇0(Lcom/intsig/note/engine/view/GLDrawView;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O0O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic OOO〇O0(Lcom/intsig/note/engine/view/GLDrawView;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇〇o〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic Oo08(Lcom/intsig/note/engine/view/GLDrawView;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O8o08O8O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic OoO8(Lcom/intsig/note/engine/view/GLDrawView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->ooo0〇〇O:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic Oooo8o0〇(Lcom/intsig/note/engine/view/GLDrawView;)Lcom/intsig/note/engine/view/DrawCanvas$OnScaleListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇80O8o8O〇:Lcom/intsig/note/engine/view/DrawCanvas$OnScaleListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic O〇8O8〇008(Lcom/intsig/note/engine/view/GLDrawView;J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO8:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic o0ooO(Lcom/intsig/note/engine/view/GLDrawView;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇8〇oO〇〇8o:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic o800o8O(Lcom/intsig/note/engine/view/GLDrawView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇〇08O:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private oO()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇oo〇O〇80:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/note/engine/view/GLDrawView$8;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/note/engine/view/GLDrawView$8;-><init>(Lcom/intsig/note/engine/view/GLDrawView;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic oO80(Lcom/intsig/note/engine/view/GLDrawView;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOo〇8o008:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic oo88o8O(Lcom/intsig/note/engine/view/GLDrawView;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O8〇o〇88:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic oo〇(Lcom/intsig/note/engine/view/GLDrawView;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇08〇o0O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic o〇0(Lcom/intsig/note/engine/view/GLDrawView;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO8:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic o〇8(Lcom/intsig/note/engine/view/GLDrawView;Landroid/graphics/Canvas;Lcom/hciilab/digitalink/core/InkCanvas;ZZLcom/intsig/note/engine/draw/DrawElement$MatrixInfo;)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lcom/intsig/note/engine/view/GLDrawView;->o〇8oOO88(Landroid/graphics/Canvas;Lcom/hciilab/digitalink/core/InkCanvas;ZZLcom/intsig/note/engine/draw/DrawElement$MatrixInfo;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
.end method

.method private o〇8oOO88(Landroid/graphics/Canvas;Lcom/hciilab/digitalink/core/InkCanvas;ZZLcom/intsig/note/engine/draw/DrawElement$MatrixInfo;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->OO:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 5
    .line 6
    .line 7
    if-nez p4, :cond_0

    .line 8
    .line 9
    iget-object p4, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 p4, 0x0

    .line 13
    :goto_0
    if-eqz p3, :cond_1

    .line 14
    .line 15
    iget-object p3, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 16
    .line 17
    invoke-virtual {p3, p1, p2, p5, p4}, Lcom/intsig/note/engine/draw/DrawList;->〇80〇808〇O(Landroid/graphics/Canvas;Lcom/hciilab/digitalink/core/InkCanvas;Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;Lcom/intsig/note/engine/draw/DrawElement;)V

    .line 18
    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_1
    iget-object p3, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 22
    .line 23
    invoke-virtual {p3, p1, p2, p5, p4}, Lcom/intsig/note/engine/draw/DrawList;->〇〇888(Landroid/graphics/Canvas;Lcom/hciilab/digitalink/core/InkCanvas;Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;Lcom/intsig/note/engine/draw/DrawElement;)V

    .line 24
    .line 25
    .line 26
    :goto_1
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method static bridge synthetic o〇O8〇〇o(Lcom/intsig/note/engine/view/GLDrawView;Landroid/graphics/Canvas;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OOo8〇0:Landroid/graphics/Canvas;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic o〇〇0〇(Lcom/intsig/note/engine/view/GLDrawView;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->OO:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇00(Lcom/intsig/note/engine/view/GLDrawView;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oO00〇o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇0000OOO(Lcom/intsig/note/engine/view/GLDrawView;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8oOOo:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇00〇8(Lcom/intsig/note/engine/view/GLDrawView;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->ooo0〇〇O:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇0〇O0088o(Lcom/intsig/note/engine/view/GLDrawView;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇〇〇0o〇〇0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇oo〇O〇80:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/note/engine/view/GLDrawView$7;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/note/engine/view/GLDrawView$7;-><init>(Lcom/intsig/note/engine/view/GLDrawView;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇80〇808〇O(Lcom/intsig/note/engine/view/GLDrawView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->OO〇00〇8oO:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇8o8o〇(Lcom/intsig/note/engine/view/GLDrawView;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8oOOo:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇O00(Lcom/intsig/note/engine/view/GLDrawView;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇〇o〇:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇O888o0o(Lcom/intsig/note/engine/view/GLDrawView;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->Ooo08:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇O8o08O(Lcom/intsig/note/engine/view/GLDrawView;)Lcom/intsig/note/engine/view/DrawCanvas$OnInitListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8O:Lcom/intsig/note/engine/view/DrawCanvas$OnInitListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇O〇(Lcom/intsig/note/engine/view/GLDrawView;)Landroid/graphics/Bitmap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->OO:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o(Lcom/intsig/note/engine/view/GLDrawView;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇〇08O:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/note/engine/view/GLDrawView;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇08O〇00〇o:Landroid/graphics/drawable/BitmapDrawable;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇oOO8O8(Lcom/intsig/note/engine/view/GLDrawView;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->O0O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇oo〇(Lcom/intsig/note/engine/view/GLDrawView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇8〇oO〇〇8o:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/note/engine/view/GLDrawView;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO0880O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇〇808〇(Lcom/intsig/note/engine/view/GLDrawView;)Lcom/intsig/note/engine/view/DrawCanvas$OnSizeChangeListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇8〇o88:Lcom/intsig/note/engine/view/DrawCanvas$OnSizeChangeListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/note/engine/view/GLDrawView;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOo0:Landroid/graphics/drawable/Drawable;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇〇8O0〇8(Lcom/intsig/note/engine/view/GLDrawView;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇08〇o0O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public O000(IFF)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇080:Landroid/graphics/Matrix;

    .line 4
    .line 5
    invoke-virtual {v0, p2, p3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 9
    .line 10
    iget-object v0, v0, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇080:Landroid/graphics/Matrix;

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    invoke-virtual {p0, v0, v1, v1}, Lcom/intsig/note/engine/view/GLDrawView;->Oo8Oo00oo(Landroid/graphics/Matrix;ZZ)Landroid/graphics/PointF;

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x2

    .line 17
    if-ne p1, v0, :cond_0

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇O8oOo0:Lcom/intsig/note/engine/view/DrawCanvas$OnLoadListener;

    .line 20
    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    invoke-interface {v0}, Lcom/intsig/note/engine/view/DrawCanvas$OnLoadListener;->〇o00〇〇Oo()V

    .line 24
    .line 25
    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇oo〇O〇80:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 27
    .line 28
    new-instance v1, Lcom/intsig/note/engine/view/GLDrawView$6;

    .line 29
    .line 30
    invoke-direct {v1, p0, p1, p2, p3}, Lcom/intsig/note/engine/view/GLDrawView$6;-><init>(Lcom/intsig/note/engine/view/GLDrawView;IFF)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public O08000()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->Ooo08:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected Oo8Oo00oo(Landroid/graphics/Matrix;ZZ)Landroid/graphics/PointF;
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_6

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o〇oO:Landroid/graphics/RectF;

    .line 7
    .line 8
    iget v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇08〇o0O:I

    .line 9
    .line 10
    int-to-float v2, v2

    .line 11
    iget v3, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇〇o〇:I

    .line 12
    .line 13
    int-to-float v3, v3

    .line 14
    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o〇oO:Landroid/graphics/RectF;

    .line 18
    .line 19
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o〇oO:Landroid/graphics/RectF;

    .line 23
    .line 24
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->o〇oO:Landroid/graphics/RectF;

    .line 29
    .line 30
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    const/high16 v3, 0x40000000    # 2.0f

    .line 35
    .line 36
    if-eqz p3, :cond_2

    .line 37
    .line 38
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 39
    .line 40
    .line 41
    move-result p3

    .line 42
    int-to-float p3, p3

    .line 43
    cmpg-float v4, v0, p3

    .line 44
    .line 45
    if-gez v4, :cond_0

    .line 46
    .line 47
    sub-float/2addr p3, v0

    .line 48
    div-float/2addr p3, v3

    .line 49
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o〇oO:Landroid/graphics/RectF;

    .line 50
    .line 51
    iget v0, v0, Landroid/graphics/RectF;->top:F

    .line 52
    .line 53
    :goto_0
    sub-float/2addr p3, v0

    .line 54
    goto :goto_1

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o〇oO:Landroid/graphics/RectF;

    .line 56
    .line 57
    iget v4, v0, Landroid/graphics/RectF;->top:F

    .line 58
    .line 59
    cmpl-float v5, v4, v1

    .line 60
    .line 61
    if-lez v5, :cond_1

    .line 62
    .line 63
    neg-float p3, v4

    .line 64
    goto :goto_1

    .line 65
    :cond_1
    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    .line 66
    .line 67
    cmpg-float p3, v0, p3

    .line 68
    .line 69
    if-gez p3, :cond_2

    .line 70
    .line 71
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 72
    .line 73
    .line 74
    move-result p3

    .line 75
    int-to-float p3, p3

    .line 76
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o〇oO:Landroid/graphics/RectF;

    .line 77
    .line 78
    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_2
    const/4 p3, 0x0

    .line 82
    :goto_1
    if-eqz p2, :cond_5

    .line 83
    .line 84
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 85
    .line 86
    .line 87
    move-result p2

    .line 88
    int-to-float p2, p2

    .line 89
    cmpg-float v0, v2, p2

    .line 90
    .line 91
    if-gez v0, :cond_3

    .line 92
    .line 93
    sub-float/2addr p2, v2

    .line 94
    div-float/2addr p2, v3

    .line 95
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o〇oO:Landroid/graphics/RectF;

    .line 96
    .line 97
    iget v0, v0, Landroid/graphics/RectF;->left:F

    .line 98
    .line 99
    :goto_2
    sub-float/2addr p2, v0

    .line 100
    :goto_3
    move v1, p2

    .line 101
    goto :goto_4

    .line 102
    :cond_3
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o〇oO:Landroid/graphics/RectF;

    .line 103
    .line 104
    iget v2, v0, Landroid/graphics/RectF;->left:F

    .line 105
    .line 106
    cmpl-float v3, v2, v1

    .line 107
    .line 108
    if-lez v3, :cond_4

    .line 109
    .line 110
    neg-float p2, v2

    .line 111
    goto :goto_3

    .line 112
    :cond_4
    iget v0, v0, Landroid/graphics/RectF;->right:F

    .line 113
    .line 114
    cmpg-float v2, v0, p2

    .line 115
    .line 116
    if-gez v2, :cond_5

    .line 117
    .line 118
    goto :goto_2

    .line 119
    :cond_5
    :goto_4
    invoke-virtual {p1, v1, p3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 120
    .line 121
    .line 122
    goto :goto_5

    .line 123
    :cond_6
    const/4 p3, 0x0

    .line 124
    :goto_5
    new-instance p1, Landroid/graphics/PointF;

    .line 125
    .line 126
    invoke-direct {p1, v1, p3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 127
    .line 128
    .line 129
    return-object p1
.end method

.method public Ooo()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇o88o08〇:Lcom/intsig/note/engine/history/HistoryActionStack;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/note/engine/history/HistoryActionStack;->OO0o〇〇〇〇0()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O〇O〇oO()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/hciilab/digitalink/core/InkCanvas;->recycleCanvas()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->OO:Landroid/graphics/Bitmap;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->OO:Landroid/graphics/Bitmap;

    .line 17
    .line 18
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 19
    .line 20
    .line 21
    :cond_0
    return-void
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->finalize()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OO8ooO8〇:Landroid/os/HandlerThread;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OO8ooO8〇:Landroid/os/HandlerThread;

    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDrawToolManager()Lcom/intsig/note/engine/draw/DrawToolManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo80:Lcom/intsig/note/engine/draw/DrawToolManager;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHistoryActionStack()Lcom/intsig/note/engine/history/HistoryActionStack;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇o88o08〇:Lcom/intsig/note/engine/history/HistoryActionStack;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPageRect()Landroid/graphics/RectF;
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oo8ooo8O:Landroid/graphics/RectF;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o8()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇0O〇O00O:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o8oO〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇o88o08〇:Lcom/intsig/note/engine/history/HistoryActionStack;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/note/engine/history/HistoryActionStack;->〇〇888()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO00OOO(Lcom/intsig/note/engine/draw/DrawElement;)V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    if-ne v0, p1, :cond_0

    .line 6
    .line 7
    iget-boolean v3, p0, Lcom/intsig/note/engine/view/GLDrawView;->Ooo08:Z

    .line 8
    .line 9
    if-nez v3, :cond_4

    .line 10
    .line 11
    :cond_0
    if-eqz v0, :cond_2

    .line 12
    .line 13
    iget-object v3, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOo〇08〇:Lcom/intsig/note/engine/view/GLDrawView$OnSelectedChangeListener;

    .line 14
    .line 15
    if-eqz v3, :cond_1

    .line 16
    .line 17
    invoke-interface {v3, v1, v0}, Lcom/intsig/note/engine/view/GLDrawView$OnSelectedChangeListener;->〇o00〇〇Oo(ZLcom/intsig/note/engine/draw/DrawElement;)V

    .line 18
    .line 19
    .line 20
    :cond_1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/draw/DrawElement;->O8ooOoo〇(Z)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 26
    .line 27
    iget-object v3, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇o88o08〇:Lcom/intsig/note/engine/history/HistoryActionStack;

    .line 28
    .line 29
    invoke-virtual {v0, v3}, Lcom/intsig/note/engine/draw/Element;->O8(Lcom/intsig/note/engine/history/HistoryActionStack;)V

    .line 30
    .line 31
    .line 32
    :cond_2
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOo〇08〇:Lcom/intsig/note/engine/view/GLDrawView$OnSelectedChangeListener;

    .line 33
    .line 34
    if-eqz v0, :cond_3

    .line 35
    .line 36
    invoke-interface {v0, v2, p1}, Lcom/intsig/note/engine/view/GLDrawView$OnSelectedChangeListener;->〇o00〇〇Oo(ZLcom/intsig/note/engine/draw/DrawElement;)V

    .line 37
    .line 38
    .line 39
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/note/engine/draw/Element;->〇O8o08O()V

    .line 40
    .line 41
    .line 42
    :cond_4
    invoke-virtual {p1, v2}, Lcom/intsig/note/engine/draw/DrawElement;->O8ooOoo〇(Z)V

    .line 43
    .line 44
    .line 45
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 46
    .line 47
    iput-boolean v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->Ooo08:Z

    .line 48
    .line 49
    iput-boolean v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOoo80oO:Z

    .line 50
    .line 51
    iget-object v4, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OOo8〇0:Landroid/graphics/Canvas;

    .line 52
    .line 53
    iget-object v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 54
    .line 55
    const/4 v6, 0x0

    .line 56
    const/4 v7, 0x0

    .line 57
    iget-object v8, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 58
    .line 59
    move-object v3, p0

    .line 60
    invoke-direct/range {v3 .. v8}, Lcom/intsig/note/engine/view/GLDrawView;->o〇8oOO88(Landroid/graphics/Canvas;Lcom/hciilab/digitalink/core/InkCanvas;ZZLcom/intsig/note/engine/draw/DrawElement$MatrixInfo;)V

    .line 61
    .line 62
    .line 63
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->OO:Landroid/graphics/Bitmap;

    .line 66
    .line 67
    invoke-virtual {p1, v0}, Lcom/hciilab/digitalink/core/InkCanvas;->uploadTexture(Landroid/graphics/Bitmap;)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 71
    .line 72
    .line 73
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public onPause()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/note/engine/view/GLDrawView$9;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/note/engine/view/GLDrawView$9;-><init>(Lcom/intsig/note/engine/view/GLDrawView;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v0}, Landroid/opengl/GLSurfaceView;->queueEvent(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onPause()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iput-wide v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O8o〇O0:J

    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    const/4 v3, 0x0

    .line 20
    const/4 v4, 0x1

    .line 21
    if-ne v2, v4, :cond_0

    .line 22
    .line 23
    const/4 v2, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v2, 0x0

    .line 26
    :goto_0
    iget-object v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 27
    .line 28
    if-eqz v5, :cond_17

    .line 29
    .line 30
    iget-boolean v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇0O〇O00O:Z

    .line 31
    .line 32
    if-eqz v5, :cond_1

    .line 33
    .line 34
    if-nez v2, :cond_1

    .line 35
    .line 36
    goto/16 :goto_5

    .line 37
    .line 38
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 39
    .line 40
    .line 41
    move-result v5

    .line 42
    if-nez v5, :cond_2

    .line 43
    .line 44
    iget-object v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->oo8ooo8O:Landroid/graphics/RectF;

    .line 45
    .line 46
    invoke-virtual {v5, v0, v1}, Landroid/graphics/RectF;->contains(FF)Z

    .line 47
    .line 48
    .line 49
    move-result v5

    .line 50
    iput-boolean v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->oO〇8O8oOo:Z

    .line 51
    .line 52
    :cond_2
    iget-boolean v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->oO〇8O8oOo:Z

    .line 53
    .line 54
    if-nez v5, :cond_3

    .line 55
    .line 56
    return v3

    .line 57
    :cond_3
    iput-boolean v3, p0, Lcom/intsig/note/engine/view/GLDrawView;->o〇o〇Oo88:Z

    .line 58
    .line 59
    iget-boolean v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOoo80oO:Z

    .line 60
    .line 61
    if-eqz v5, :cond_4

    .line 62
    .line 63
    iget-object v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇O〇〇O8:Lcom/intsig/note/engine/view/CustomScaleGestureDetector;

    .line 64
    .line 65
    invoke-virtual {v5, p1}, Lcom/intsig/note/engine/view/CustomScaleGestureDetector;->〇80〇808〇O(Landroid/view/MotionEvent;)Z

    .line 66
    .line 67
    .line 68
    :cond_4
    iget-object v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇O〇〇O8:Lcom/intsig/note/engine/view/CustomScaleGestureDetector;

    .line 69
    .line 70
    invoke-virtual {v5}, Lcom/intsig/note/engine/view/CustomScaleGestureDetector;->oO80()Z

    .line 71
    .line 72
    .line 73
    move-result v5

    .line 74
    if-eqz v5, :cond_5

    .line 75
    .line 76
    iput-boolean v4, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0O0o8:Z

    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_5
    iget-boolean v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0OoOOo0:Z

    .line 80
    .line 81
    if-eqz v5, :cond_6

    .line 82
    .line 83
    iput-boolean v3, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0O0o8:Z

    .line 84
    .line 85
    :cond_6
    :goto_1
    iget-boolean v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0O0o8:Z

    .line 86
    .line 87
    if-nez v5, :cond_7

    .line 88
    .line 89
    iget-object v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇o0O:Landroid/view/GestureDetector;

    .line 90
    .line 91
    invoke-virtual {v5, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 92
    .line 93
    .line 94
    move-result v5

    .line 95
    if-eqz v5, :cond_8

    .line 96
    .line 97
    return v4

    .line 98
    :cond_7
    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    .line 99
    .line 100
    .line 101
    move-result-object v5

    .line 102
    const/4 v6, 0x3

    .line 103
    invoke-virtual {v5, v6}, Landroid/view/MotionEvent;->setAction(I)V

    .line 104
    .line 105
    .line 106
    iget-object v6, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇o0O:Landroid/view/GestureDetector;

    .line 107
    .line 108
    invoke-virtual {v6, v5}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 109
    .line 110
    .line 111
    invoke-virtual {v5}, Landroid/view/MotionEvent;->recycle()V

    .line 112
    .line 113
    .line 114
    :cond_8
    iget-boolean v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0O0o8:Z

    .line 115
    .line 116
    if-nez v5, :cond_9

    .line 117
    .line 118
    iget-boolean v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->Ooo08:Z

    .line 119
    .line 120
    if-eqz v5, :cond_a

    .line 121
    .line 122
    :cond_9
    iget-boolean v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->oO00〇o:Z

    .line 123
    .line 124
    if-eqz v5, :cond_15

    .line 125
    .line 126
    :cond_a
    iget-object v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo80:Lcom/intsig/note/engine/draw/DrawToolManager;

    .line 127
    .line 128
    invoke-virtual {v5}, Lcom/intsig/note/engine/draw/DrawToolManager;->〇o00〇〇Oo()Lcom/intsig/note/engine/draw/DrawTool;

    .line 129
    .line 130
    .line 131
    move-result-object v5

    .line 132
    instance-of v6, v5, Lcom/intsig/note/engine/draw/InkTool;

    .line 133
    .line 134
    if-nez v6, :cond_b

    .line 135
    .line 136
    instance-of v6, v5, Lcom/intsig/note/engine/draw/EraserTool;

    .line 137
    .line 138
    if-eqz v6, :cond_15

    .line 139
    .line 140
    :cond_b
    iget-boolean v6, p0, Lcom/intsig/note/engine/view/GLDrawView;->oO00〇o:Z

    .line 141
    .line 142
    if-eqz v6, :cond_c

    .line 143
    .line 144
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->setAction(I)V

    .line 145
    .line 146
    .line 147
    :cond_c
    invoke-static {p1}, Lcom/intsig/note/engine/CompabilityHelper;->〇080(Landroid/view/MotionEvent;)Z

    .line 148
    .line 149
    .line 150
    move-result v6

    .line 151
    iget-object v7, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 152
    .line 153
    if-eqz v7, :cond_d

    .line 154
    .line 155
    const/4 v8, 0x1

    .line 156
    goto :goto_2

    .line 157
    :cond_d
    const/4 v8, 0x0

    .line 158
    :goto_2
    if-eqz v8, :cond_e

    .line 159
    .line 160
    instance-of v7, v7, Lcom/intsig/note/engine/draw/InkElement;

    .line 161
    .line 162
    if-eqz v7, :cond_e

    .line 163
    .line 164
    const/4 v7, 0x1

    .line 165
    goto :goto_3

    .line 166
    :cond_e
    const/4 v7, 0x0

    .line 167
    :goto_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 168
    .line 169
    .line 170
    move-result v9

    .line 171
    if-nez v9, :cond_f

    .line 172
    .line 173
    iget-object v8, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 174
    .line 175
    invoke-virtual {v5, v8}, Lcom/intsig/note/engine/draw/DrawTool;->〇080(Lcom/intsig/note/engine/draw/DrawList;)Lcom/intsig/note/engine/draw/DrawElement;

    .line 176
    .line 177
    .line 178
    move-result-object v5

    .line 179
    iput-object v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 180
    .line 181
    iput v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->OO〇OOo:F

    .line 182
    .line 183
    iput v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇800OO〇0O:F

    .line 184
    .line 185
    const/4 v8, 0x1

    .line 186
    goto :goto_4

    .line 187
    :cond_f
    if-eqz v7, :cond_11

    .line 188
    .line 189
    iget-object v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 190
    .line 191
    instance-of v5, v5, Lcom/intsig/note/engine/draw/EngineEraserElement;

    .line 192
    .line 193
    if-nez v5, :cond_10

    .line 194
    .line 195
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 196
    .line 197
    .line 198
    move-result v5

    .line 199
    if-ne v4, v5, :cond_11

    .line 200
    .line 201
    :cond_10
    iget-object v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 202
    .line 203
    check-cast v5, Lcom/intsig/note/engine/draw/InkElement;

    .line 204
    .line 205
    invoke-virtual {v5}, Lcom/intsig/note/engine/draw/InkElement;->〇0000OOO()V

    .line 206
    .line 207
    .line 208
    :cond_11
    :goto_4
    if-eqz v8, :cond_14

    .line 209
    .line 210
    iget-object v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 211
    .line 212
    iget-object v9, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 213
    .line 214
    iget-object v10, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 215
    .line 216
    invoke-virtual {v5, p1, v9, v10}, Lcom/intsig/note/engine/draw/DrawElement;->O〇8O8〇008(Landroid/view/MotionEvent;Lcom/hciilab/digitalink/core/InkCanvas;Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;)Z

    .line 217
    .line 218
    .line 219
    move-result v5

    .line 220
    if-eqz v5, :cond_14

    .line 221
    .line 222
    if-eqz v7, :cond_13

    .line 223
    .line 224
    iget-object v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 225
    .line 226
    check-cast v5, Lcom/intsig/note/engine/draw/InkElement;

    .line 227
    .line 228
    invoke-virtual {v5}, Lcom/intsig/note/engine/draw/InkElement;->〇o()Z

    .line 229
    .line 230
    .line 231
    move-result v5

    .line 232
    if-eqz v5, :cond_13

    .line 233
    .line 234
    iget-boolean v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0OoOOo0:Z

    .line 235
    .line 236
    if-nez v5, :cond_13

    .line 237
    .line 238
    const/4 v5, 0x2

    .line 239
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 240
    .line 241
    .line 242
    move-result p1

    .line 243
    if-ne v5, p1, :cond_13

    .line 244
    .line 245
    if-nez v6, :cond_12

    .line 246
    .line 247
    iget p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->OO〇OOo:F

    .line 248
    .line 249
    sub-float/2addr v0, p1

    .line 250
    float-to-double v5, v0

    .line 251
    iget p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇800OO〇0O:F

    .line 252
    .line 253
    sub-float/2addr v1, p1

    .line 254
    float-to-double v0, v1

    .line 255
    invoke-static {v5, v6, v0, v1}, Ljava/lang/Math;->hypot(DD)D

    .line 256
    .line 257
    .line 258
    move-result-wide v0

    .line 259
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o〇00O:Landroid/util/DisplayMetrics;

    .line 260
    .line 261
    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    .line 262
    .line 263
    const/high16 v5, 0x41400000    # 12.0f

    .line 264
    .line 265
    mul-float p1, p1, v5

    .line 266
    .line 267
    float-to-double v5, p1

    .line 268
    cmpl-double p1, v0, v5

    .line 269
    .line 270
    if-lez p1, :cond_13

    .line 271
    .line 272
    :cond_12
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 273
    .line 274
    check-cast p1, Lcom/intsig/note/engine/draw/InkElement;

    .line 275
    .line 276
    invoke-virtual {p1}, Lcom/intsig/note/engine/draw/InkElement;->〇0000OOO()V

    .line 277
    .line 278
    .line 279
    iput-boolean v4, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0OoOOo0:Z

    .line 280
    .line 281
    iput-boolean v4, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO0880O:Z

    .line 282
    .line 283
    :cond_13
    iput-boolean v4, p0, Lcom/intsig/note/engine/view/GLDrawView;->o〇o〇Oo88:Z

    .line 284
    .line 285
    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 286
    .line 287
    .line 288
    :cond_14
    if-eqz v8, :cond_15

    .line 289
    .line 290
    if-eqz v2, :cond_15

    .line 291
    .line 292
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 293
    .line 294
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 295
    .line 296
    invoke-virtual {p1, v0, v3}, Lcom/intsig/note/engine/draw/DrawList;->O8(Lcom/intsig/note/engine/draw/DrawElement;Z)V

    .line 297
    .line 298
    .line 299
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇o88o08〇:Lcom/intsig/note/engine/history/HistoryActionStack;

    .line 300
    .line 301
    new-instance v0, Lcom/intsig/note/engine/history/AddAction;

    .line 302
    .line 303
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 304
    .line 305
    invoke-direct {v0, v1}, Lcom/intsig/note/engine/history/AddAction;-><init>(Lcom/intsig/note/engine/draw/DrawElement;)V

    .line 306
    .line 307
    .line 308
    invoke-virtual {p1, v0}, Lcom/intsig/note/engine/history/HistoryActionStack;->o〇0(Lcom/intsig/note/engine/history/Action;)V

    .line 309
    .line 310
    .line 311
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 312
    .line 313
    invoke-virtual {p0, p1, v0}, Lcom/intsig/note/engine/view/GLDrawView;->〇8〇0〇o〇O(Lcom/intsig/note/engine/draw/DrawElement;Lcom/intsig/note/engine/history/AddAction;)V

    .line 314
    .line 315
    .line 316
    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 317
    .line 318
    .line 319
    :cond_15
    if-eqz v2, :cond_16

    .line 320
    .line 321
    iput-boolean v3, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0O0o8:Z

    .line 322
    .line 323
    iput-boolean v3, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0OoOOo0:Z

    .line 324
    .line 325
    iput-boolean v3, p0, Lcom/intsig/note/engine/view/GLDrawView;->oO00〇o:Z

    .line 326
    .line 327
    iput-boolean v3, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO0880O:Z

    .line 328
    .line 329
    iput-boolean v3, p0, Lcom/intsig/note/engine/view/GLDrawView;->oO〇8O8oOo:Z

    .line 330
    .line 331
    :cond_16
    return v4

    .line 332
    :cond_17
    :goto_5
    return v3
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method o〇0OOo〇0(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O8o08O8O:Z

    .line 2
    .line 3
    if-eq v0, p1, :cond_0

    .line 4
    .line 5
    iput-boolean p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->O8o08O8O:Z

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 8
    .line 9
    invoke-virtual {v0, p1}, Lcom/hciilab/digitalink/core/InkCanvas;->enableVectorStroke(Z)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected o〇O()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0〇Ooo:[F

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/hciilab/digitalink/core/InkCanvas;->getMatrix([F)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 9
    .line 10
    iget-object v0, v0, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇080:Landroid/graphics/Matrix;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0〇Ooo:[F

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setValues([F)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0〇Ooo:[F

    .line 20
    .line 21
    const/4 v2, 0x0

    .line 22
    aget v1, v1, v2

    .line 23
    .line 24
    iput v1, v0, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇o00〇〇Oo:F

    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oo8ooo8O:Landroid/graphics/RectF;

    .line 27
    .line 28
    iget v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇08〇o0O:I

    .line 29
    .line 30
    int-to-float v1, v1

    .line 31
    iget v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇〇o〇:I

    .line 32
    .line 33
    int-to-float v2, v2

    .line 34
    const/4 v3, 0x0

    .line 35
    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 39
    .line 40
    iget-object v0, v0, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇080:Landroid/graphics/Matrix;

    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oo8ooo8O:Landroid/graphics/RectF;

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O88O:Lcom/intsig/note/engine/view/MatrixManager;

    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 50
    .line 51
    iget-object v1, v1, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇080:Landroid/graphics/Matrix;

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/view/MatrixManager;->〇o〇(Landroid/graphics/Matrix;)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O88O:Lcom/intsig/note/engine/view/MatrixManager;

    .line 57
    .line 58
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 59
    .line 60
    iget v1, v1, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇o00〇〇Oo:F

    .line 61
    .line 62
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/view/MatrixManager;->O8(F)V

    .line 63
    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OO〇00〇0O:Landroid/os/Handler;

    .line 66
    .line 67
    new-instance v1, Lcom/intsig/note/engine/view/GLDrawView$5;

    .line 68
    .line 69
    invoke-direct {v1, p0}, Lcom/intsig/note/engine/view/GLDrawView$5;-><init>(Lcom/intsig/note/engine/view/GLDrawView;)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 73
    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public setBackgroundResource(I)V
    .locals 6

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_3

    .line 10
    .line 11
    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 12
    .line 13
    if-eqz v1, :cond_3

    .line 14
    .line 15
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 16
    .line 17
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 26
    .line 27
    if-ne v1, v2, :cond_0

    .line 28
    .line 29
    iput-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇08O〇00〇o:Landroid/graphics/drawable/BitmapDrawable;

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 45
    .line 46
    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    new-instance v3, Landroid/graphics/Canvas;

    .line 51
    .line 52
    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 53
    .line 54
    .line 55
    const/4 v4, 0x0

    .line 56
    const/4 v5, 0x0

    .line 57
    invoke-virtual {v3, v1, v5, v5, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 58
    .line 59
    .line 60
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 61
    .line 62
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 63
    .line 64
    .line 65
    move-result-object v3

    .line 66
    invoke-direct {v1, v3, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 67
    .line 68
    .line 69
    iput-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇08O〇00〇o:Landroid/graphics/drawable/BitmapDrawable;

    .line 70
    .line 71
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getTileModeX()Landroid/graphics/Shader$TileMode;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getTileModeY()Landroid/graphics/Shader$TileMode;

    .line 76
    .line 77
    .line 78
    move-result-object v0

    .line 79
    invoke-virtual {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 80
    .line 81
    .line 82
    :goto_0
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 83
    .line 84
    invoke-virtual {v0}, Lcom/hciilab/digitalink/core/InkCanvas;->isInited()Z

    .line 85
    .line 86
    .line 87
    move-result v0

    .line 88
    if-eqz v0, :cond_2

    .line 89
    .line 90
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 91
    .line 92
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇08O〇00〇o:Landroid/graphics/drawable/BitmapDrawable;

    .line 93
    .line 94
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇08O〇00〇o:Landroid/graphics/drawable/BitmapDrawable;

    .line 99
    .line 100
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getTileModeX()Landroid/graphics/Shader$TileMode;

    .line 101
    .line 102
    .line 103
    move-result-object v1

    .line 104
    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    .line 105
    .line 106
    if-ne v1, v2, :cond_1

    .line 107
    .line 108
    const/4 v1, 0x1

    .line 109
    goto :goto_1

    .line 110
    :cond_1
    const/4 v1, 0x0

    .line 111
    :goto_1
    invoke-virtual {p1, v0, v1}, Lcom/hciilab/digitalink/core/InkCanvas;->setBackground(Landroid/graphics/Bitmap;Z)V

    .line 112
    .line 113
    .line 114
    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 115
    .line 116
    .line 117
    goto :goto_2

    .line 118
    :cond_2
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->setBackgroundResource(I)V

    .line 119
    .line 120
    .line 121
    :cond_3
    :goto_2
    return-void
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public setDrawList(Lcom/intsig/note/engine/draw/DrawList;)V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8〇OO:Z

    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 5
    .line 6
    invoke-virtual {p1, p0}, Ljava/util/Observable;->addObserver(Ljava/util/Observer;)V

    .line 7
    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    .line 12
    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 15
    .line 16
    iget v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8〇OO0〇0o:F

    .line 17
    .line 18
    invoke-virtual {p1, v1, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 19
    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 22
    .line 23
    invoke-virtual {p0, p1, v0, v0}, Lcom/intsig/note/engine/view/GLDrawView;->Oo8Oo00oo(Landroid/graphics/Matrix;ZZ)Landroid/graphics/PointF;

    .line 24
    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 29
    .line 30
    invoke-virtual {p1, v0}, Lcom/hciilab/digitalink/core/InkCanvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/note/engine/view/GLDrawView;->o〇O()V

    .line 34
    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 37
    .line 38
    invoke-virtual {p1}, Lcom/intsig/note/engine/draw/DrawList;->〇〇808〇()Lcom/intsig/note/engine/entity/Page;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇0O:Lcom/intsig/note/engine/entity/Page$PreviewGenerator;

    .line 43
    .line 44
    invoke-virtual {p1, v0}, Lcom/intsig/note/engine/entity/Page;->〇0000OOO(Lcom/intsig/note/engine/entity/Page$PreviewGenerator;)V

    .line 45
    .line 46
    .line 47
    iget v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇08〇o0O:I

    .line 48
    .line 49
    int-to-float v0, v0

    .line 50
    invoke-virtual {p1}, Lcom/intsig/note/engine/entity/Page;->OoO8()I

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    int-to-float v1, v1

    .line 55
    div-float/2addr v0, v1

    .line 56
    invoke-virtual {p1, v0}, Lcom/intsig/note/engine/entity/Page;->o〇〇0〇(F)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/note/engine/entity/Page;->〇〇888()Lcom/intsig/note/engine/entity/Document;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/intsig/note/engine/entity/Document;->Oo08()F

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    invoke-virtual {p1}, Lcom/intsig/note/engine/entity/Page;->〇O〇()F

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 72
    .line 73
    invoke-virtual {v2, v0, v0, v1}, Lcom/hciilab/digitalink/core/InkCanvas;->setDpi(FFF)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {p1}, Lcom/intsig/note/engine/entity/Page;->oo88o8O()Z

    .line 77
    .line 78
    .line 79
    move-result p1

    .line 80
    invoke-virtual {p0, p1}, Lcom/intsig/note/engine/view/GLDrawView;->o〇0OOo〇0(Z)V

    .line 81
    .line 82
    .line 83
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->ooO:Landroid/os/Handler;

    .line 84
    .line 85
    const/4 v0, 0x0

    .line 86
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 87
    .line 88
    .line 89
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->ooO:Landroid/os/Handler;

    .line 90
    .line 91
    const/16 v0, 0x65

    .line 92
    .line 93
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 94
    .line 95
    .line 96
    return-void
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public setLoadingDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOo0:Landroid/graphics/drawable/Drawable;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLoadingRes(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOo〇8o008:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOnInitListener(Lcom/intsig/note/engine/view/DrawCanvas$OnInitListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8O:Lcom/intsig/note/engine/view/DrawCanvas$OnInitListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOnLoadListener(Lcom/intsig/note/engine/view/DrawCanvas$OnLoadListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇O8oOo0:Lcom/intsig/note/engine/view/DrawCanvas$OnLoadListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOnMatrixChangeListener(Lcom/intsig/note/engine/view/DrawCanvas$OnMatrixChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇O:Lcom/intsig/note/engine/view/DrawCanvas$OnMatrixChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOnPageLoadedListener(Lcom/intsig/note/engine/view/DrawCanvas$OnPageLoadedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oO〇oo:Lcom/intsig/note/engine/view/DrawCanvas$OnPageLoadedListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOnScaleListener(Lcom/intsig/note/engine/view/DrawCanvas$OnScaleListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇80O8o8O〇:Lcom/intsig/note/engine/view/DrawCanvas$OnScaleListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOnSelectedChangeListener(Lcom/intsig/note/engine/view/GLDrawView$OnSelectedChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOo〇08〇:Lcom/intsig/note/engine/view/GLDrawView$OnSelectedChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOnSizeChangeListener(Lcom/intsig/note/engine/view/DrawCanvas$OnSizeChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇8〇o88:Lcom/intsig/note/engine/view/DrawCanvas$OnSizeChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 6

    .line 1
    if-nez p2, :cond_1

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OOo8〇0:Landroid/graphics/Canvas;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    const/4 v4, 0x1

    .line 9
    iget-object v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 10
    .line 11
    move-object v0, p0

    .line 12
    invoke-direct/range {v0 .. v5}, Lcom/intsig/note/engine/view/GLDrawView;->o〇8oOO88(Landroid/graphics/Canvas;Lcom/hciilab/digitalink/core/InkCanvas;ZZLcom/intsig/note/engine/draw/DrawElement$MatrixInfo;)V

    .line 13
    .line 14
    .line 15
    iget-object p2, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->OO:Landroid/graphics/Bitmap;

    .line 18
    .line 19
    invoke-virtual {p2, v0}, Lcom/hciilab/digitalink/core/InkCanvas;->uploadTexture(Landroid/graphics/Bitmap;)V

    .line 20
    .line 21
    .line 22
    check-cast p1, Lcom/intsig/note/engine/draw/DrawList;

    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/intsig/note/engine/draw/DrawList;->〇8o8o〇()I

    .line 25
    .line 26
    .line 27
    move-result p2

    .line 28
    invoke-virtual {p1}, Lcom/intsig/note/engine/draw/DrawList;->OO0o〇〇〇〇0()I

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    if-le p2, p1, :cond_0

    .line 33
    .line 34
    invoke-direct {p0}, Lcom/intsig/note/engine/view/GLDrawView;->〇8()V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    invoke-direct {p0}, Lcom/intsig/note/engine/view/GLDrawView;->oO()V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    instance-of p1, p2, Lcom/intsig/note/engine/draw/InkElement;

    .line 43
    .line 44
    if-eqz p1, :cond_3

    .line 45
    .line 46
    check-cast p2, Lcom/intsig/note/engine/draw/InkElement;

    .line 47
    .line 48
    invoke-virtual {p2}, Lcom/intsig/note/engine/draw/DrawElement;->〇O888o0o()Z

    .line 49
    .line 50
    .line 51
    move-result p1

    .line 52
    if-eqz p1, :cond_2

    .line 53
    .line 54
    invoke-direct {p0}, Lcom/intsig/note/engine/view/GLDrawView;->oO()V

    .line 55
    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_2
    invoke-direct {p0}, Lcom/intsig/note/engine/view/GLDrawView;->〇8()V

    .line 59
    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_3
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OOo8〇0:Landroid/graphics/Canvas;

    .line 63
    .line 64
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 65
    .line 66
    const/4 v3, 0x0

    .line 67
    const/4 v4, 0x1

    .line 68
    iget-object v5, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 69
    .line 70
    move-object v0, p0

    .line 71
    invoke-direct/range {v0 .. v5}, Lcom/intsig/note/engine/view/GLDrawView;->o〇8oOO88(Landroid/graphics/Canvas;Lcom/hciilab/digitalink/core/InkCanvas;ZZLcom/intsig/note/engine/draw/DrawElement$MatrixInfo;)V

    .line 72
    .line 73
    .line 74
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 75
    .line 76
    iget-object p2, p0, Lcom/intsig/note/engine/view/GLDrawView;->OO:Landroid/graphics/Bitmap;

    .line 77
    .line 78
    invoke-virtual {p1, p2}, Lcom/hciilab/digitalink/core/InkCanvas;->uploadTexture(Landroid/graphics/Bitmap;)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 82
    .line 83
    .line 84
    :goto_0
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public 〇08O8o〇0(II)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/note/engine/view/GLDrawView$4;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1, p2}, Lcom/intsig/note/engine/view/GLDrawView$4;-><init>(Lcom/intsig/note/engine/view/GLDrawView;II)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v0}, Landroid/opengl/GLSurfaceView;->queueEvent(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇80()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇0O〇O00O:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected 〇8〇0〇o〇O(Lcom/intsig/note/engine/draw/DrawElement;Lcom/intsig/note/engine/history/AddAction;)V
    .locals 0

    .line 1
    const/4 p1, 0x1

    .line 2
    iput-boolean p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇〇〇0o〇〇0:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected 〇〇0o(II)Lcom/intsig/note/engine/draw/DrawElement;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/note/engine/draw/DrawList;->〇8o8o〇()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    add-int/lit8 v0, v0, -0x1

    .line 8
    .line 9
    :goto_0
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/note/engine/draw/DrawList;->OO0o〇〇〇〇0()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-lt v0, v1, :cond_1

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 18
    .line 19
    invoke-virtual {v1, v0}, Lcom/intsig/note/engine/draw/DrawList;->〇O8o08O(I)Lcom/intsig/note/engine/draw/DrawElement;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 24
    .line 25
    invoke-virtual {v1, p1, p2, v2}, Lcom/intsig/note/engine/draw/DrawElement;->OoO8(IILcom/intsig/note/engine/draw/DrawElement$MatrixInfo;)Z

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    if-eqz v2, :cond_0

    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/note/engine/draw/DrawElement;->o〇O8〇〇o()Z

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    if-eqz v1, :cond_0

    .line 36
    .line 37
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 38
    .line 39
    invoke-virtual {p1, v0}, Lcom/intsig/note/engine/draw/DrawList;->〇O8o08O(I)Lcom/intsig/note/engine/draw/DrawElement;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    return-object p1

    .line 44
    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_1
    const/4 p1, 0x0

    .line 48
    return-object p1
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇〇〇0〇〇0()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    iget-boolean v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->Ooo08:Z

    .line 7
    .line 8
    if-eqz v2, :cond_0

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOo〇08〇:Lcom/intsig/note/engine/view/GLDrawView$OnSelectedChangeListener;

    .line 11
    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    invoke-interface {v2, v1, v0}, Lcom/intsig/note/engine/view/GLDrawView$OnSelectedChangeListener;->〇o00〇〇Oo(ZLcom/intsig/note/engine/draw/DrawElement;)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOo〇08〇:Lcom/intsig/note/engine/view/GLDrawView$OnSelectedChangeListener;

    .line 18
    .line 19
    invoke-interface {v0}, Lcom/intsig/note/engine/view/GLDrawView$OnSelectedChangeListener;->〇080()V

    .line 20
    .line 21
    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/draw/DrawElement;->O8ooOoo〇(Z)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 28
    .line 29
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇o88o08〇:Lcom/intsig/note/engine/history/HistoryActionStack;

    .line 30
    .line 31
    invoke-virtual {v0, v2}, Lcom/intsig/note/engine/draw/Element;->O8(Lcom/intsig/note/engine/history/HistoryActionStack;)V

    .line 32
    .line 33
    .line 34
    :cond_1
    iput-boolean v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->Ooo08:Z

    .line 35
    .line 36
    const/4 v0, 0x0

    .line 37
    iput-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇08oOOO0:Lcom/intsig/note/engine/draw/DrawElement;

    .line 38
    .line 39
    const/4 v0, 0x1

    .line 40
    iput-boolean v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOoo80oO:Z

    .line 41
    .line 42
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OOo8〇0:Landroid/graphics/Canvas;

    .line 43
    .line 44
    iget-object v3, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 45
    .line 46
    const/4 v4, 0x0

    .line 47
    const/4 v5, 0x1

    .line 48
    iget-object v6, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 49
    .line 50
    move-object v1, p0

    .line 51
    invoke-direct/range {v1 .. v6}, Lcom/intsig/note/engine/view/GLDrawView;->o〇8oOO88(Landroid/graphics/Canvas;Lcom/hciilab/digitalink/core/InkCanvas;ZZLcom/intsig/note/engine/draw/DrawElement$MatrixInfo;)V

    .line 52
    .line 53
    .line 54
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0:Lcom/hciilab/digitalink/core/InkCanvas;

    .line 55
    .line 56
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->OO:Landroid/graphics/Bitmap;

    .line 57
    .line 58
    invoke-virtual {v0, v1}, Lcom/hciilab/digitalink/core/InkCanvas;->uploadTexture(Landroid/graphics/Bitmap;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

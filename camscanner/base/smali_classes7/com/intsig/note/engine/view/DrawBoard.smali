.class public interface abstract Lcom/intsig/note/engine/view/DrawBoard;
.super Ljava/lang/Object;
.source "DrawBoard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/note/engine/view/DrawBoard$Callback;,
        Lcom/intsig/note/engine/view/DrawBoard$OnSizeChangedListener;
    }
.end annotation


# virtual methods
.method public abstract O8()V
.end method

.method public abstract OO0o〇〇〇〇0()V
.end method

.method public abstract Oo08()V
.end method

.method public abstract getDrawToolManager()Lcom/intsig/note/engine/draw/DrawToolManager;
.end method

.method public abstract oO80(Lcom/intsig/note/engine/history/HistoryActionStack$OnChangedListener;)V
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method

.method public abstract o〇0()V
.end method

.method public abstract recycle()V
.end method

.method public abstract setCallback(Lcom/intsig/note/engine/view/DrawBoard$Callback;)V
.end method

.method public abstract setLoadingDrawable(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract setOnSizeChangedListener(Lcom/intsig/note/engine/view/DrawBoard$OnSizeChangedListener;)V
.end method

.method public abstract setPage(Lcom/intsig/note/engine/entity/Page;)V
.end method

.method public abstract 〇080()V
.end method

.method public abstract 〇80〇808〇O()V
.end method

.method public abstract 〇o00〇〇Oo()Z
.end method

.method public abstract 〇o〇()V
.end method

.method public abstract 〇〇888(II)V
.end method

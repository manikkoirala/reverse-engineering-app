.class public Lcom/intsig/note/engine/view/GLMoveableDrawView;
.super Lcom/intsig/note/engine/view/GLDrawView;
.source "GLMoveableDrawView.java"

# interfaces
.implements Lcom/intsig/note/engine/entity/Page$OnChangeShadingListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/note/engine/view/GLMoveableDrawView$MoveAnimAction;,
        Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;
    }
.end annotation


# static fields
.field private static O〇8〇008:Landroid/graphics/Paint;


# instance fields
.field private O0O0〇:Ljava/lang/Runnable;

.field protected O0〇0:Z

.field private O8〇8〇O80:Ljava/lang/Runnable;

.field private Ooo8o:Z

.field private o00〇88〇08:Z

.field o88:Z

.field private o880:Landroid/graphics/Matrix;

.field private oOoO8OO〇:Z

.field private oooO888:Landroid/graphics/RectF;

.field private o〇0〇o:I

.field private o〇O8OO:Lcom/intsig/note/engine/history/AddAction;

.field private o〇oo:Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;

.field private 〇088O:Landroid/graphics/Rect;

.field private 〇08O:Ljava/lang/Runnable;

.field private 〇0oO〇oo00:F

.field private 〇0ooOOo:J

.field private 〇0〇0:F

.field private 〇8O0880:F

.field private 〇8〇80o:F

.field private 〇8〇OOoooo:F

.field protected 〇O0o〇〇o:Z

.field private 〇O8〇8000:F

.field private 〇Oo〇O:Ljava/lang/Runnable;

.field private 〇o08:Z

.field private 〇o〇88〇8:F

.field private 〇〇O80〇0o:F

.field private 〇〇o0〇8:F

.field private 〇〇〇0:I

.field private 〇〇〇00:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O〇8〇008:Landroid/graphics/Paint;

    .line 8
    .line 9
    const v1, 0x50444444

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 13
    .line 14
    .line 15
    sget-object v0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O〇8〇008:Landroid/graphics/Paint;

    .line 16
    .line 17
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 20
    .line 21
    .line 22
    sget-object v0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O〇8〇008:Landroid/graphics/Paint;

    .line 23
    .line 24
    const/high16 v1, 0x40a00000    # 5.0f

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/note/engine/view/GLDrawView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    new-instance p1, Landroid/graphics/Matrix;

    .line 5
    .line 6
    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o880:Landroid/graphics/Matrix;

    .line 10
    .line 11
    const/high16 p1, 0x42a00000    # 80.0f

    .line 12
    .line 13
    iput p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇o0〇8:F

    .line 14
    .line 15
    const/4 p1, 0x0

    .line 16
    iput-boolean p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O0〇0:Z

    .line 17
    .line 18
    new-instance p1, Lcom/intsig/note/engine/view/GLMoveableDrawView$1;

    .line 19
    .line 20
    invoke-direct {p1, p0}, Lcom/intsig/note/engine/view/GLMoveableDrawView$1;-><init>(Lcom/intsig/note/engine/view/GLMoveableDrawView;)V

    .line 21
    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O8〇8〇O80:Ljava/lang/Runnable;

    .line 24
    .line 25
    new-instance p1, Lcom/intsig/note/engine/view/GLMoveableDrawView$2;

    .line 26
    .line 27
    invoke-direct {p1, p0}, Lcom/intsig/note/engine/view/GLMoveableDrawView$2;-><init>(Lcom/intsig/note/engine/view/GLMoveableDrawView;)V

    .line 28
    .line 29
    .line 30
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O0O0〇:Ljava/lang/Runnable;

    .line 31
    .line 32
    new-instance p1, Lcom/intsig/note/engine/view/GLMoveableDrawView$3;

    .line 33
    .line 34
    invoke-direct {p1, p0}, Lcom/intsig/note/engine/view/GLMoveableDrawView$3;-><init>(Lcom/intsig/note/engine/view/GLMoveableDrawView;)V

    .line 35
    .line 36
    .line 37
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇Oo〇O:Ljava/lang/Runnable;

    .line 38
    .line 39
    new-instance p1, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;

    .line 40
    .line 41
    invoke-direct {p1, p0}, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;-><init>(Lcom/intsig/note/engine/view/GLMoveableDrawView;)V

    .line 42
    .line 43
    .line 44
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o〇oo:Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;

    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic O0(Lcom/intsig/note/engine/view/GLMoveableDrawView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇0oO〇oo00:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic O0O8OO088(Lcom/intsig/note/engine/view/GLMoveableDrawView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇O8〇8000:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic O0o〇〇Oo(Lcom/intsig/note/engine/view/GLMoveableDrawView;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇〇00:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic O8O〇(Lcom/intsig/note/engine/view/GLMoveableDrawView;)Ljava/lang/Runnable;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O0O0〇:Ljava/lang/Runnable;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic OO8oO0o〇(Lcom/intsig/note/engine/view/GLMoveableDrawView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇o0〇8:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic OOO(Lcom/intsig/note/engine/view/GLMoveableDrawView;)Ljava/lang/Runnable;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇08O:Ljava/lang/Runnable;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic OOO8o〇〇(Lcom/intsig/note/engine/view/GLMoveableDrawView;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇OOoooo:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private Oo(Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇088O:Landroid/graphics/Rect;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroid/graphics/Rect;

    .line 6
    .line 7
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇088O:Landroid/graphics/Rect;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 13
    .line 14
    iget-object v0, v0, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇080:Landroid/graphics/Matrix;

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0〇Ooo:[F

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 19
    .line 20
    .line 21
    iget v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇OOoooo:F

    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 24
    .line 25
    iget v1, v1, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇o00〇〇Oo:F

    .line 26
    .line 27
    mul-float v0, v0, v1

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0〇Ooo:[F

    .line 30
    .line 31
    const/4 v2, 0x5

    .line 32
    aget v1, v1, v2

    .line 33
    .line 34
    add-float/2addr v0, v1

    .line 35
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 40
    .line 41
    iget v1, v1, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇o00〇〇Oo:F

    .line 42
    .line 43
    iget v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇o0〇8:F

    .line 44
    .line 45
    mul-float v1, v1, v2

    .line 46
    .line 47
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇088O:Landroid/graphics/Rect;

    .line 52
    .line 53
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    const/4 v4, 0x0

    .line 58
    invoke-virtual {v2, v4, v4, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 59
    .line 60
    .line 61
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇088O:Landroid/graphics/Rect;

    .line 62
    .line 63
    sget-object v3, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O〇8〇008:Landroid/graphics/Paint;

    .line 64
    .line 65
    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 66
    .line 67
    .line 68
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇088O:Landroid/graphics/Rect;

    .line 69
    .line 70
    add-int/2addr v0, v1

    .line 71
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 72
    .line 73
    .line 74
    move-result v1

    .line 75
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 76
    .line 77
    .line 78
    move-result v3

    .line 79
    invoke-virtual {v2, v4, v0, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇088O:Landroid/graphics/Rect;

    .line 83
    .line 84
    sget-object v1, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O〇8〇008:Landroid/graphics/Paint;

    .line 85
    .line 86
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 87
    .line 88
    .line 89
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method static bridge synthetic Ooo8〇〇(Lcom/intsig/note/engine/view/GLMoveableDrawView;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇〇0:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic Oo〇O(Lcom/intsig/note/engine/view/GLMoveableDrawView;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇〇0:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic o0O0(Lcom/intsig/note/engine/view/GLMoveableDrawView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇OOoooo:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic o88〇OO08〇(Lcom/intsig/note/engine/view/GLMoveableDrawView;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o00〇88〇08:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic o8O〇(Lcom/intsig/note/engine/view/GLMoveableDrawView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇O80〇0o:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic ooOO(Lcom/intsig/note/engine/view/GLMoveableDrawView;Ljava/lang/Runnable;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇08O:Ljava/lang/Runnable;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic ooo〇8oO(Lcom/intsig/note/engine/view/GLMoveableDrawView;)Ljava/lang/Runnable;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O8〇8〇O80:Ljava/lang/Runnable;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static synthetic o〇o(Lcom/intsig/note/engine/view/GLMoveableDrawView;Lcom/intsig/note/engine/draw/DrawElement;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/note/engine/view/GLDrawView;->oO00OOO(Lcom/intsig/note/engine/draw/DrawElement;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇0(Lcom/intsig/note/engine/view/GLMoveableDrawView;)Landroid/graphics/RectF;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->oooO888:Landroid/graphics/RectF;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇00O0O0(Lcom/intsig/note/engine/view/GLMoveableDrawView;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇o08:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇0O〇Oo(Lcom/intsig/note/engine/view/GLMoveableDrawView;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->oOoO8OO〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇O(Lcom/intsig/note/engine/view/GLMoveableDrawView;J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇0ooOOo:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇O〇80o08O(Lcom/intsig/note/engine/view/GLMoveableDrawView;)Lcom/intsig/note/engine/history/AddAction;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o〇O8OO:Lcom/intsig/note/engine/history/AddAction;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o0O0O8(Lcom/intsig/note/engine/view/GLMoveableDrawView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8O0880:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇〇o8(Lcom/intsig/note/engine/view/GLMoveableDrawView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇80o:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public O0OO8〇0(FLjava/lang/Runnable;Z)V
    .locals 5

    .line 1
    const/16 v0, 0xc8

    .line 2
    .line 3
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇〇00:I

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    iput v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇〇0:I

    .line 7
    .line 8
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 9
    .line 10
    iput p1, v2, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇o00〇〇Oo:F

    .line 11
    .line 12
    iget v3, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇OOoooo:F

    .line 13
    .line 14
    iput v3, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇80o:F

    .line 15
    .line 16
    div-int/lit8 v0, v0, 0x12

    .line 17
    .line 18
    iget-object v2, v2, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇080:Landroid/graphics/Matrix;

    .line 19
    .line 20
    iget-object v3, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0〇Ooo:[F

    .line 21
    .line 22
    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->getValues([F)V

    .line 23
    .line 24
    .line 25
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0〇Ooo:[F

    .line 26
    .line 27
    aget v3, v2, v1

    .line 28
    .line 29
    const/4 v4, 0x4

    .line 30
    aget v2, v2, v4

    .line 31
    .line 32
    sub-float v3, p1, v3

    .line 33
    .line 34
    int-to-float v0, v0

    .line 35
    div-float/2addr v3, v0

    .line 36
    iput v3, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇O8〇8000:F

    .line 37
    .line 38
    sub-float v2, p1, v2

    .line 39
    .line 40
    div-float/2addr v2, v0

    .line 41
    iput v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8O0880:F

    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/note/engine/view/GLDrawView;->o8()V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0〇Ooo:[F

    .line 47
    .line 48
    aput p1, v0, v1

    .line 49
    .line 50
    aput p1, v0, v4

    .line 51
    .line 52
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 53
    .line 54
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->setValues([F)V

    .line 55
    .line 56
    .line 57
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 58
    .line 59
    const/4 v0, 0x1

    .line 60
    invoke-virtual {p0, p1, v0, v0}, Lcom/intsig/note/engine/view/GLDrawView;->Oo8Oo00oo(Landroid/graphics/Matrix;ZZ)Landroid/graphics/PointF;

    .line 61
    .line 62
    .line 63
    iput-object p2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇08O:Ljava/lang/Runnable;

    .line 64
    .line 65
    iput-boolean p3, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇O0o〇〇o:Z

    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OO〇00〇0O:Landroid/os/Handler;

    .line 68
    .line 69
    iget-object p2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O0O0〇:Ljava/lang/Runnable;

    .line 70
    .line 71
    invoke-virtual {p1, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public O880oOO08()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/note/engine/view/GLMoveableDrawView;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const/16 v0, 0xc8

    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇〇00:I

    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇〇0:I

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 18
    .line 19
    iget-object v1, v1, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇080:Landroid/graphics/Matrix;

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 22
    .line 23
    .line 24
    iget v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇o0〇8:F

    .line 25
    .line 26
    neg-float v0, v0

    .line 27
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 28
    .line 29
    iget v1, v1, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇o00〇〇Oo:F

    .line 30
    .line 31
    mul-float v0, v0, v1

    .line 32
    .line 33
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇0oO〇oo00:F

    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 36
    .line 37
    const/4 v2, 0x0

    .line 38
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 39
    .line 40
    .line 41
    iget v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇0oO〇oo00:F

    .line 42
    .line 43
    iget v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇〇00:I

    .line 44
    .line 45
    div-int/lit8 v1, v1, 0x12

    .line 46
    .line 47
    int-to-float v1, v1

    .line 48
    div-float/2addr v0, v1

    .line 49
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇0oO〇oo00:F

    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/intsig/note/engine/draw/DrawList;->〇〇808〇()Lcom/intsig/note/engine/entity/Page;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lcom/intsig/note/engine/entity/Page;->〇〇8O0〇8()I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    int-to-float v0, v0

    .line 62
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 63
    .line 64
    iget v1, v1, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇o00〇〇Oo:F

    .line 65
    .line 66
    mul-float v0, v0, v1

    .line 67
    .line 68
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇O80〇0o:F

    .line 69
    .line 70
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 71
    .line 72
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 73
    .line 74
    .line 75
    iget v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇O80〇0o:F

    .line 76
    .line 77
    iget v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇〇00:I

    .line 78
    .line 79
    div-int/lit8 v1, v1, 0x12

    .line 80
    .line 81
    int-to-float v1, v1

    .line 82
    div-float/2addr v0, v1

    .line 83
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇O80〇0o:F

    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 86
    .line 87
    const/4 v1, 0x1

    .line 88
    invoke-virtual {p0, v0, v1, v1}, Lcom/intsig/note/engine/view/GLDrawView;->Oo8Oo00oo(Landroid/graphics/Matrix;ZZ)Landroid/graphics/PointF;

    .line 89
    .line 90
    .line 91
    invoke-virtual {p0}, Lcom/intsig/note/engine/view/GLDrawView;->o8()V

    .line 92
    .line 93
    .line 94
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OO〇00〇0O:Landroid/os/Handler;

    .line 95
    .line 96
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O8〇8〇O80:Ljava/lang/Runnable;

    .line 97
    .line 98
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 99
    .line 100
    .line 101
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public OOo0O(I)V
    .locals 3

    .line 1
    const/16 v0, 0xc8

    .line 2
    .line 3
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇〇00:I

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    iput v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇〇0:I

    .line 7
    .line 8
    neg-int p1, p1

    .line 9
    int-to-float p1, p1

    .line 10
    iput p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇O80〇0o:F

    .line 11
    .line 12
    div-int/lit8 v0, v0, 0x12

    .line 13
    .line 14
    int-to-float v0, v0

    .line 15
    div-float v0, p1, v0

    .line 16
    .line 17
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇O80〇0o:F

    .line 18
    .line 19
    const/4 v0, 0x0

    .line 20
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇0oO〇oo00:F

    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/note/engine/view/GLDrawView;->o8()V

    .line 23
    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 26
    .line 27
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 28
    .line 29
    iget-object v2, v2, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇080:Landroid/graphics/Matrix;

    .line 30
    .line 31
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 32
    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 35
    .line 36
    invoke-virtual {v1, p1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 37
    .line 38
    .line 39
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 40
    .line 41
    const/4 v0, 0x1

    .line 42
    invoke-virtual {p0, p1, v0, v0}, Lcom/intsig/note/engine/view/GLDrawView;->Oo8Oo00oo(Landroid/graphics/Matrix;ZZ)Landroid/graphics/PointF;

    .line 43
    .line 44
    .line 45
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OO〇00〇0O:Landroid/os/Handler;

    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O8〇8〇O80:Ljava/lang/Runnable;

    .line 48
    .line 49
    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public OOo8o〇O()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o00〇88〇08:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oo〇o(Landroid/graphics/Canvas;)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O0〇0:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o00〇88〇08:Z

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0, p1}, Lcom/intsig/note/engine/view/GLMoveableDrawView;->Oo(Landroid/graphics/Canvas;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method O〇0()V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o88:Z

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o〇oo:Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;

    .line 5
    .line 6
    iget-object v0, v0, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇080:Landroid/graphics/Matrix;

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o〇oo:Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    iput v1, v0, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇o00〇〇Oo:F

    .line 15
    .line 16
    const/high16 v2, 0x3f800000    # 1.0f

    .line 17
    .line 18
    iput v2, v0, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇o〇:F

    .line 19
    .line 20
    iput v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇O80〇0o:F

    .line 21
    .line 22
    iput v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇0oO〇oo00:F

    .line 23
    .line 24
    iput v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇O8〇8000:F

    .line 25
    .line 26
    iput v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8O0880:F

    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public O〇08(F)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    invoke-virtual {p0, p1, v0, v1}, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O0OO8〇0(FLjava/lang/Runnable;Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public O〇OO()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o〇oo:Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇080:Landroid/graphics/Matrix;

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 6
    .line 7
    iget-object v1, v1, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇080:Landroid/graphics/Matrix;

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o〇oo:Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;

    .line 13
    .line 14
    iget v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇OOoooo:F

    .line 15
    .line 16
    iput v1, v0, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇o00〇〇Oo:F

    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 19
    .line 20
    iget v1, v1, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇o00〇〇Oo:F

    .line 21
    .line 22
    iput v1, v0, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇o〇:F

    .line 23
    .line 24
    const/4 v0, 0x1

    .line 25
    iput-boolean v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o88:Z

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method O〇Oooo〇〇(Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;I)V
    .locals 6

    .line 1
    iput p2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇〇00:I

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇〇0:I

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 7
    .line 8
    iget v2, p1, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇o〇:F

    .line 9
    .line 10
    iput v2, v1, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇o00〇〇Oo:F

    .line 11
    .line 12
    iget v2, p1, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇o00〇〇Oo:F

    .line 13
    .line 14
    iput v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇80o:F

    .line 15
    .line 16
    div-int/lit8 p2, p2, 0x12

    .line 17
    .line 18
    iget-object v1, v1, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇080:Landroid/graphics/Matrix;

    .line 19
    .line 20
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0〇Ooo:[F

    .line 21
    .line 22
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 23
    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0〇Ooo:[F

    .line 26
    .line 27
    const/4 v2, 0x2

    .line 28
    aget v3, v1, v2

    .line 29
    .line 30
    iput v3, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇O80〇0o:F

    .line 31
    .line 32
    const/4 v3, 0x5

    .line 33
    aget v4, v1, v3

    .line 34
    .line 35
    iput v4, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇0oO〇oo00:F

    .line 36
    .line 37
    aget v4, v1, v0

    .line 38
    .line 39
    iput v4, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇O8〇8000:F

    .line 40
    .line 41
    const/4 v4, 0x4

    .line 42
    aget v5, v1, v4

    .line 43
    .line 44
    iput v5, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8O0880:F

    .line 45
    .line 46
    iget-object v5, p1, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇080:Landroid/graphics/Matrix;

    .line 47
    .line 48
    invoke-virtual {v5, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 49
    .line 50
    .line 51
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->Oo0〇Ooo:[F

    .line 52
    .line 53
    aget v2, v1, v2

    .line 54
    .line 55
    iget v5, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇O80〇0o:F

    .line 56
    .line 57
    sub-float/2addr v2, v5

    .line 58
    int-to-float p2, p2

    .line 59
    div-float/2addr v2, p2

    .line 60
    iput v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇O80〇0o:F

    .line 61
    .line 62
    aget v2, v1, v3

    .line 63
    .line 64
    iget v3, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇0oO〇oo00:F

    .line 65
    .line 66
    sub-float/2addr v2, v3

    .line 67
    div-float/2addr v2, p2

    .line 68
    iput v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇0oO〇oo00:F

    .line 69
    .line 70
    aget v0, v1, v0

    .line 71
    .line 72
    iget v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇O8〇8000:F

    .line 73
    .line 74
    sub-float/2addr v0, v2

    .line 75
    div-float/2addr v0, p2

    .line 76
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇O8〇8000:F

    .line 77
    .line 78
    aget v0, v1, v4

    .line 79
    .line 80
    iget v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8O0880:F

    .line 81
    .line 82
    sub-float/2addr v0, v1

    .line 83
    div-float/2addr v0, p2

    .line 84
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8O0880:F

    .line 85
    .line 86
    iget-object p2, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 87
    .line 88
    iget-object p1, p1, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇080:Landroid/graphics/Matrix;

    .line 89
    .line 90
    invoke-virtual {p2, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 91
    .line 92
    .line 93
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8o:Landroid/graphics/Matrix;

    .line 94
    .line 95
    const/4 p2, 0x1

    .line 96
    invoke-virtual {p0, p1, p2, p2}, Lcom/intsig/note/engine/view/GLDrawView;->Oo8Oo00oo(Landroid/graphics/Matrix;ZZ)Landroid/graphics/PointF;

    .line 97
    .line 98
    .line 99
    invoke-virtual {p0}, Lcom/intsig/note/engine/view/GLDrawView;->o8()V

    .line 100
    .line 101
    .line 102
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OO〇00〇0O:Landroid/os/Handler;

    .line 103
    .line 104
    iget-object p2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O0O0〇:Ljava/lang/Runnable;

    .line 105
    .line 106
    invoke-virtual {p1, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 107
    .line 108
    .line 109
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public oO00OOO(Lcom/intsig/note/engine/draw/DrawElement;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/note/engine/view/GLMoveableDrawView$4;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lcom/intsig/note/engine/view/GLMoveableDrawView$4;-><init>(Lcom/intsig/note/engine/view/GLMoveableDrawView;Lcom/intsig/note/engine/draw/DrawElement;)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x0

    .line 7
    invoke-virtual {p0, v0, p1}, Lcom/intsig/note/engine/view/GLMoveableDrawView;->ooO〇00O(Ljava/lang/Runnable;Z)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/note/engine/view/GLDrawView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    const/4 v3, 0x1

    .line 14
    if-nez v2, :cond_1

    .line 15
    .line 16
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    iput v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇0〇0:F

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-eq v2, v3, :cond_2

    .line 28
    .line 29
    iget-object v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OO〇00〇0O:Landroid/os/Handler;

    .line 30
    .line 31
    iget-object v4, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇Oo〇O:Ljava/lang/Runnable;

    .line 32
    .line 33
    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 34
    .line 35
    .line 36
    :cond_2
    :goto_0
    iget-boolean v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o00〇88〇08:Z

    .line 37
    .line 38
    if-eqz v2, :cond_4

    .line 39
    .line 40
    iget-boolean v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->o0OoOOo0:Z

    .line 41
    .line 42
    if-eqz v2, :cond_4

    .line 43
    .line 44
    iget-boolean v2, p0, Lcom/intsig/note/engine/view/GLDrawView;->o〇o〇Oo88:Z

    .line 45
    .line 46
    if-eqz v2, :cond_4

    .line 47
    .line 48
    const/4 v2, 0x2

    .line 49
    new-array v2, v2, [F

    .line 50
    .line 51
    const/high16 v4, 0x3f800000    # 1.0f

    .line 52
    .line 53
    aput v4, v2, v1

    .line 54
    .line 55
    iget v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇0〇0:F

    .line 56
    .line 57
    aput v1, v2, v3

    .line 58
    .line 59
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 60
    .line 61
    iget-object v1, v1, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇080:Landroid/graphics/Matrix;

    .line 62
    .line 63
    iget-object v4, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o880:Landroid/graphics/Matrix;

    .line 64
    .line 65
    invoke-virtual {v1, v4}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 66
    .line 67
    .line 68
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o880:Landroid/graphics/Matrix;

    .line 69
    .line 70
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 71
    .line 72
    .line 73
    aget v1, v2, v3

    .line 74
    .line 75
    invoke-virtual {p0, v1}, Lcom/intsig/note/engine/view/GLMoveableDrawView;->oo(F)I

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    int-to-float v1, v1

    .line 80
    iget v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇OOoooo:F

    .line 81
    .line 82
    cmpl-float v4, v1, v2

    .line 83
    .line 84
    if-eqz v4, :cond_3

    .line 85
    .line 86
    iput v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇o〇88〇8:F

    .line 87
    .line 88
    iput v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇OOoooo:F

    .line 89
    .line 90
    iput-boolean v3, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->Ooo8o:Z

    .line 91
    .line 92
    :cond_3
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->oooO888:Landroid/graphics/RectF;

    .line 93
    .line 94
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 95
    .line 96
    .line 97
    move-result v2

    .line 98
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 99
    .line 100
    .line 101
    move-result p1

    .line 102
    invoke-virtual {v1, v2, p1}, Landroid/graphics/RectF;->contains(FF)Z

    .line 103
    .line 104
    .line 105
    move-result p1

    .line 106
    if-eqz p1, :cond_4

    .line 107
    .line 108
    iput-boolean v3, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->oOoO8OO〇:Z

    .line 109
    .line 110
    :cond_4
    return v0
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public oo(F)I
    .locals 5

    .line 1
    iget v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o〇0〇o:I

    .line 2
    .line 3
    int-to-float v1, v0

    .line 4
    sub-float/2addr p1, v1

    .line 5
    iget v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇o0〇8:F

    .line 6
    .line 7
    div-float/2addr p1, v1

    .line 8
    float-to-int p1, p1

    .line 9
    int-to-float v0, v0

    .line 10
    int-to-float v2, p1

    .line 11
    mul-float v1, v1, v2

    .line 12
    .line 13
    add-float/2addr v0, v1

    .line 14
    float-to-int v0, v0

    .line 15
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/intsig/note/engine/draw/DrawList;->〇〇808〇()Lcom/intsig/note/engine/entity/Page;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    int-to-float v2, v0

    .line 22
    invoke-virtual {v1}, Lcom/intsig/note/engine/entity/Page;->OO0o〇〇〇〇0()I

    .line 23
    .line 24
    .line 25
    move-result v3

    .line 26
    invoke-virtual {v1}, Lcom/intsig/note/engine/entity/Page;->〇0〇O0088o()Lcom/intsig/note/engine/resource/Shading;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v1}, Lcom/intsig/note/engine/resource/Shading;->OO0o〇〇〇〇0()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    sub-int/2addr v3, v1

    .line 35
    int-to-float v1, v3

    .line 36
    iget v3, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇o0〇8:F

    .line 37
    .line 38
    sub-float/2addr v1, v3

    .line 39
    const/high16 v4, 0x40a00000    # 5.0f

    .line 40
    .line 41
    add-float/2addr v1, v4

    .line 42
    cmpl-float v1, v2, v1

    .line 43
    .line 44
    if-lez v1, :cond_0

    .line 45
    .line 46
    iget v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o〇0〇o:I

    .line 47
    .line 48
    int-to-float v0, v0

    .line 49
    add-int/lit8 p1, p1, -0x1

    .line 50
    .line 51
    int-to-float p1, p1

    .line 52
    mul-float v3, v3, p1

    .line 53
    .line 54
    add-float/2addr v0, v3

    .line 55
    float-to-int v0, v0

    .line 56
    :cond_0
    return v0
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public ooO〇00O(Ljava/lang/Runnable;Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇o00〇〇Oo:F

    .line 4
    .line 5
    iget v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->o8〇OO0〇0o:F

    .line 6
    .line 7
    cmpl-float v0, v0, v1

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    if-eqz p1, :cond_1

    .line 12
    .line 13
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    invoke-virtual {p0, v1, p1, p2}, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O0OO8〇0(FLjava/lang/Runnable;Z)V

    .line 18
    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->O〇O:Lcom/intsig/note/engine/view/DrawCanvas$OnMatrixChangeListener;

    .line 21
    .line 22
    if-eqz p1, :cond_1

    .line 23
    .line 24
    invoke-interface {p1}, Lcom/intsig/note/engine/view/DrawCanvas$OnMatrixChangeListener;->〇080()V

    .line 25
    .line 26
    .line 27
    :cond_1
    :goto_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public ooo0〇O88O()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/note/engine/draw/DrawList;->〇〇808〇()Lcom/intsig/note/engine/entity/Page;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇OOoooo:F

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/note/engine/entity/Page;->OO0o〇〇〇〇0()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    invoke-virtual {v0}, Lcom/intsig/note/engine/entity/Page;->〇0〇O0088o()Lcom/intsig/note/engine/resource/Shading;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lcom/intsig/note/engine/resource/Shading;->OO0o〇〇〇〇0()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    sub-int/2addr v2, v0

    .line 22
    int-to-float v0, v2

    .line 23
    iget v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇o0〇8:F

    .line 24
    .line 25
    sub-float/2addr v0, v2

    .line 26
    const/high16 v2, 0x40a00000    # 5.0f

    .line 27
    .line 28
    sub-float/2addr v0, v2

    .line 29
    cmpl-float v0, v1, v0

    .line 30
    .line 31
    if-lez v0, :cond_0

    .line 32
    .line 33
    const/4 v0, 0x1

    .line 34
    goto :goto_0

    .line 35
    :cond_0
    const/4 v0, 0x0

    .line 36
    :goto_0
    return v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public setDrawList(Lcom/intsig/note/engine/draw/DrawList;)V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o00〇88〇08:Z

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 4
    .line 5
    iget v1, v1, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇o00〇〇Oo:F

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇000O0()V

    .line 8
    .line 9
    .line 10
    invoke-super {p0, p1}, Lcom/intsig/note/engine/view/GLDrawView;->setDrawList(Lcom/intsig/note/engine/draw/DrawList;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O〇0()V

    .line 14
    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇00O0:Lcom/intsig/note/engine/draw/DrawList;

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/note/engine/draw/DrawList;->〇〇808〇()Lcom/intsig/note/engine/entity/Page;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-virtual {p1, p0}, Lcom/intsig/note/engine/entity/Page;->〇o〇(Lcom/intsig/note/engine/entity/Page$OnChangeShadingListener;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/note/engine/entity/Page;->〇8o8o〇()F

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    iput-boolean v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o00〇88〇08:Z

    .line 30
    .line 31
    const/4 v0, 0x0

    .line 32
    cmpl-float v0, v2, v0

    .line 33
    .line 34
    if-lez v0, :cond_0

    .line 35
    .line 36
    iput v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇o0〇8:F

    .line 37
    .line 38
    const/4 v0, 0x1

    .line 39
    iput-boolean v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O0〇0:Z

    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/note/engine/entity/Page;->〇O8o08O()I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    int-to-float v0, v0

    .line 46
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇OOoooo:F

    .line 47
    .line 48
    invoke-virtual {p1}, Lcom/intsig/note/engine/entity/Page;->〇O8o08O()I

    .line 49
    .line 50
    .line 51
    move-result p1

    .line 52
    iput p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o〇0〇o:I

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    const/4 p1, 0x0

    .line 56
    iput-boolean p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O0〇0:Z

    .line 57
    .line 58
    const/high16 v0, 0x42a00000    # 80.0f

    .line 59
    .line 60
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇o0〇8:F

    .line 61
    .line 62
    iput p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o〇0〇o:I

    .line 63
    .line 64
    int-to-float p1, p1

    .line 65
    iput p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇OOoooo:F

    .line 66
    .line 67
    :goto_0
    iget-boolean p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o00〇88〇08:Z

    .line 68
    .line 69
    if-eqz p1, :cond_1

    .line 70
    .line 71
    invoke-virtual {p0, v1}, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O〇08(F)V

    .line 72
    .line 73
    .line 74
    :cond_1
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setMoveX(Landroid/graphics/RectF;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->oooO888:Landroid/graphics/RectF;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMoveable(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o00〇88〇08:Z

    .line 2
    .line 3
    if-ne v0, p1, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iput-boolean p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o00〇88〇08:Z

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected 〇000O0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OO〇00〇0O:Landroid/os/Handler;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O8〇8〇O80:Ljava/lang/Runnable;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OO〇00〇0O:Landroid/os/Handler;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O0O0〇:Ljava/lang/Runnable;

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 13
    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    iput-boolean v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇o08:Z

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080(Lcom/intsig/note/engine/resource/Shading;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/note/engine/resource/Shading;->〇80〇808〇O()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    cmpl-float v1, v0, v1

    .line 7
    .line 8
    if-lez v1, :cond_0

    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇o0〇8:F

    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    iput-boolean v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O0〇0:Z

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/note/engine/resource/Shading;->〇8o8o〇()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    int-to-float v0, v0

    .line 20
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇OOoooo:F

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/note/engine/resource/Shading;->〇8o8o〇()I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    iput p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o〇0〇o:I

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    const/high16 p1, 0x42a00000    # 80.0f

    .line 30
    .line 31
    iput p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇〇o0〇8:F

    .line 32
    .line 33
    const/4 p1, 0x0

    .line 34
    iput p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o〇0〇o:I

    .line 35
    .line 36
    int-to-float v0, p1

    .line 37
    iput v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇OOoooo:F

    .line 38
    .line 39
    iput-boolean p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->O0〇0:Z

    .line 40
    .line 41
    :goto_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected 〇8〇0〇o〇O(Lcom/intsig/note/engine/draw/DrawElement;Lcom/intsig/note/engine/history/AddAction;)V
    .locals 5

    .line 1
    iput-object p2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o〇O8OO:Lcom/intsig/note/engine/history/AddAction;

    .line 2
    .line 3
    iget-boolean v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->oOoO8OO〇:Z

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 8
    .line 9
    .line 10
    move-result-wide v0

    .line 11
    iget-wide v2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇0ooOOo:J

    .line 12
    .line 13
    sub-long/2addr v0, v2

    .line 14
    const-wide/16 v2, 0x1f4

    .line 15
    .line 16
    cmp-long v4, v0, v2

    .line 17
    .line 18
    if-lez v4, :cond_0

    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLDrawView;->〇OO〇00〇0O:Landroid/os/Handler;

    .line 21
    .line 22
    iget-object p2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇Oo〇O:Ljava/lang/Runnable;

    .line 23
    .line 24
    const-wide/16 v0, 0x12c

    .line 25
    .line 26
    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/intsig/note/engine/view/GLDrawView;->〇8〇0〇o〇O(Lcom/intsig/note/engine/draw/DrawElement;Lcom/intsig/note/engine/history/AddAction;)V

    .line 31
    .line 32
    .line 33
    :goto_0
    iget-boolean p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->Ooo8o:Z

    .line 34
    .line 35
    if-eqz p1, :cond_1

    .line 36
    .line 37
    iget-boolean p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->oOoO8OO〇:Z

    .line 38
    .line 39
    if-nez p1, :cond_1

    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o〇O8OO:Lcom/intsig/note/engine/history/AddAction;

    .line 42
    .line 43
    if-eqz p1, :cond_1

    .line 44
    .line 45
    new-instance p1, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;

    .line 46
    .line 47
    invoke-direct {p1, p0}, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;-><init>(Lcom/intsig/note/engine/view/GLMoveableDrawView;)V

    .line 48
    .line 49
    .line 50
    iget p2, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇o〇88〇8:F

    .line 51
    .line 52
    iput p2, p1, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇o00〇〇Oo:F

    .line 53
    .line 54
    iget-object p2, p1, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇080:Landroid/graphics/Matrix;

    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 57
    .line 58
    iget-object v0, v0, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇080:Landroid/graphics/Matrix;

    .line 59
    .line 60
    invoke-virtual {p2, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 61
    .line 62
    .line 63
    iget-object p2, p0, Lcom/intsig/note/engine/view/GLDrawView;->oOO〇〇:Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;

    .line 64
    .line 65
    iget p2, p2, Lcom/intsig/note/engine/draw/DrawElement$MatrixInfo;->〇o00〇〇Oo:F

    .line 66
    .line 67
    iput p2, p1, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇o〇:F

    .line 68
    .line 69
    new-instance p2, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;

    .line 70
    .line 71
    invoke-direct {p2, p0}, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;-><init>(Lcom/intsig/note/engine/view/GLMoveableDrawView;)V

    .line 72
    .line 73
    .line 74
    iget-object v0, p2, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇080:Landroid/graphics/Matrix;

    .line 75
    .line 76
    iget-object v1, p1, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇080:Landroid/graphics/Matrix;

    .line 77
    .line 78
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 79
    .line 80
    .line 81
    iget v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->〇8〇OOoooo:F

    .line 82
    .line 83
    iput v0, p2, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇o00〇〇Oo:F

    .line 84
    .line 85
    iget v0, p1, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇o〇:F

    .line 86
    .line 87
    iput v0, p2, Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;->〇o〇:F

    .line 88
    .line 89
    iget-object v0, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->o〇O8OO:Lcom/intsig/note/engine/history/AddAction;

    .line 90
    .line 91
    new-instance v1, Lcom/intsig/note/engine/view/GLMoveableDrawView$MoveAnimAction;

    .line 92
    .line 93
    const/16 v2, 0x50

    .line 94
    .line 95
    invoke-direct {v1, p0, p1, p2, v2}, Lcom/intsig/note/engine/view/GLMoveableDrawView$MoveAnimAction;-><init>(Lcom/intsig/note/engine/view/GLMoveableDrawView;Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;Lcom/intsig/note/engine/view/GLMoveableDrawView$SavedState;I)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {v0, v1}, Lcom/intsig/note/engine/history/AddAction;->〇o00〇〇Oo(Lcom/intsig/note/engine/history/Action;)V

    .line 99
    .line 100
    .line 101
    const/4 p1, 0x0

    .line 102
    iput-boolean p1, p0, Lcom/intsig/note/engine/view/GLMoveableDrawView;->Ooo8o:Z

    .line 103
    .line 104
    :cond_1
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

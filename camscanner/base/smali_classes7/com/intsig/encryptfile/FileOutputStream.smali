.class public Lcom/intsig/encryptfile/FileOutputStream;
.super Ljava/io/FileOutputStream;
.source "FileOutputStream.java"


# instance fields
.field private mHandle:J


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/intsig/encryptfile/FileOutputStream;-><init>(Ljava/io/File;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    .line 3
    :try_start_0
    invoke-static {}, Lcom/intsig/encryptfile/ISEncryptFile;->ISCrypterHeader()[B

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    .line 4
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/intsig/encryptfile/ISEncryptFile;->SizeOfFile(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long p2, v2, v4

    if-lez p2, :cond_0

    .line 5
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/intsig/encryptfile/ISEncryptFile;->FileEncryptedByISCrypter(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 6
    iput-wide v4, p0, Lcom/intsig/encryptfile/FileOutputStream;->mHandle:J

    goto :goto_0

    .line 7
    :cond_0
    invoke-static {}, Lcom/intsig/encryptfile/ISEncryptFile;->InitHandle()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/intsig/encryptfile/FileOutputStream;->mHandle:J

    .line 8
    invoke-static {v4, v5}, Lcom/intsig/encryptfile/ISEncryptFile;->BeginEncryptionProcess(J)Z

    if-lez p2, :cond_1

    .line 9
    iget-wide p1, p0, Lcom/intsig/encryptfile/FileOutputStream;->mHandle:J

    invoke-static {p1, p2, v2, v3, v1}, Lcom/intsig/encryptfile/ISEncryptFile;->SeekOffset(JJZ)Z

    goto :goto_0

    .line 10
    :cond_1
    array-length p1, v0

    invoke-super {p0, v0, v1, p1}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_0

    .line 11
    :cond_2
    invoke-static {}, Lcom/intsig/encryptfile/ISEncryptFile;->InitHandle()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/intsig/encryptfile/FileOutputStream;->mHandle:J

    .line 12
    invoke-static {p1, p2}, Lcom/intsig/encryptfile/ISEncryptFile;->BeginEncryptionProcess(J)Z

    .line 13
    array-length p1, v0

    invoke-super {p0, v0, v1, p1}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 14
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .line 15
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/encryptfile/FileOutputStream;-><init>(Ljava/io/File;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .line 16
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lcom/intsig/encryptfile/FileOutputStream;-><init>(Ljava/io/File;Z)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Ljava/io/FileOutputStream;->close()V

    .line 2
    .line 3
    .line 4
    iget-wide v0, p0, Lcom/intsig/encryptfile/FileOutputStream;->mHandle:J

    .line 5
    .line 6
    const-wide/16 v2, 0x0

    .line 7
    .line 8
    cmp-long v4, v0, v2

    .line 9
    .line 10
    if-eqz v4, :cond_0

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/encryptfile/ISEncryptFile;->EndEncryptionProcess(J)Z

    .line 13
    .line 14
    .line 15
    iget-wide v0, p0, Lcom/intsig/encryptfile/FileOutputStream;->mHandle:J

    .line 16
    .line 17
    invoke-static {v0, v1}, Lcom/intsig/encryptfile/ISEncryptFile;->CloseHandle(J)Z

    .line 18
    .line 19
    .line 20
    iput-wide v2, p0, Lcom/intsig/encryptfile/FileOutputStream;->mHandle:J

    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public write(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [B

    int-to-byte p1, p1

    const/4 v2, 0x0

    aput-byte p1, v1, v2

    .line 1
    invoke-virtual {p0, v1, v2, v0}, Lcom/intsig/encryptfile/FileOutputStream;->write([BII)V

    return-void
.end method

.method public write([BII)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    iget-wide v0, p0, Lcom/intsig/encryptfile/FileOutputStream;->mHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    .line 3
    invoke-static {v0, v1, p1, p2, p3}, Lcom/intsig/encryptfile/ISEncryptFile;->EncryptDataToData(J[BII)[B

    move-result-object p1

    const/4 p2, 0x0

    .line 4
    invoke-super {p0, p1, p2, p3}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_0

    .line 5
    :cond_0
    invoke-super {p0, p1, p2, p3}, Ljava/io/FileOutputStream;->write([BII)V

    :goto_0
    return-void
.end method

.class public Lcom/intsig/ocr/ResultPage$ResultLine;
.super Ljava/lang/Object;
.source "ResultPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/ocr/ResultPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ResultLine"
.end annotation


# instance fields
.field public O8:[I

.field public Oo08:Landroid/graphics/Rect;

.field public 〇080:[Lcom/intsig/ocr/ResultPage$ResultWord;

.field public 〇o00〇〇Oo:I

.field public 〇o〇:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x5

    .line 5
    new-array v0, v0, [I

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/ocr/ResultPage$ResultLine;->〇o〇:[I

    .line 8
    .line 9
    const/4 v0, 0x4

    .line 10
    new-array v0, v0, [I

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/ocr/ResultPage$ResultLine;->O8:[I

    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    iput-object v0, p0, Lcom/intsig/ocr/ResultPage$ResultLine;->Oo08:Landroid/graphics/Rect;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/ocr/ResultPage$ResultLine;->〇080()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080()Ljava/lang/String;
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    :goto_0
    iget v2, p0, Lcom/intsig/ocr/ResultPage$ResultLine;->〇o00〇〇Oo:I

    .line 8
    .line 9
    if-ge v1, v2, :cond_1

    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/ocr/ResultPage$ResultLine;->〇080:[Lcom/intsig/ocr/ResultPage$ResultWord;

    .line 12
    .line 13
    aget-object v2, v2, v1

    .line 14
    .line 15
    invoke-virtual {v2}, Lcom/intsig/ocr/ResultPage$ResultWord;->〇080()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    iget-object v2, p0, Lcom/intsig/ocr/ResultPage$ResultLine;->〇080:[Lcom/intsig/ocr/ResultPage$ResultWord;

    .line 23
    .line 24
    aget-object v2, v2, v1

    .line 25
    .line 26
    iget v2, v2, Lcom/intsig/ocr/ResultPage$ResultWord;->O8:I

    .line 27
    .line 28
    const/4 v3, 0x1

    .line 29
    if-ne v2, v3, :cond_0

    .line 30
    .line 31
    const-string v2, " "

    .line 32
    .line 33
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    return-object v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method 〇o00〇〇Oo(Lcom/intsig/nativelib/LineItem;I)V
    .locals 4

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/nativelib/LineItem;->getCharItems()[Lcom/intsig/nativelib/CharItem;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/nativelib/LineItem;->getCharItems()[Lcom/intsig/nativelib/CharItem;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    array-length v0, v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    goto :goto_1

    .line 17
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/nativelib/LineItem;->getCharItems()[Lcom/intsig/nativelib/CharItem;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    array-length v1, v0

    .line 22
    iput v1, p0, Lcom/intsig/ocr/ResultPage$ResultLine;->〇o00〇〇Oo:I

    .line 23
    .line 24
    new-array v1, v1, [Lcom/intsig/ocr/ResultPage$ResultWord;

    .line 25
    .line 26
    iput-object v1, p0, Lcom/intsig/ocr/ResultPage$ResultLine;->〇080:[Lcom/intsig/ocr/ResultPage$ResultWord;

    .line 27
    .line 28
    const/4 v1, 0x0

    .line 29
    :goto_0
    iget v2, p0, Lcom/intsig/ocr/ResultPage$ResultLine;->〇o00〇〇Oo:I

    .line 30
    .line 31
    if-ge v1, v2, :cond_1

    .line 32
    .line 33
    iget-object v2, p0, Lcom/intsig/ocr/ResultPage$ResultLine;->〇080:[Lcom/intsig/ocr/ResultPage$ResultWord;

    .line 34
    .line 35
    new-instance v3, Lcom/intsig/ocr/ResultPage$ResultWord;

    .line 36
    .line 37
    invoke-direct {v3}, Lcom/intsig/ocr/ResultPage$ResultWord;-><init>()V

    .line 38
    .line 39
    .line 40
    aput-object v3, v2, v1

    .line 41
    .line 42
    iget-object v2, p0, Lcom/intsig/ocr/ResultPage$ResultLine;->〇080:[Lcom/intsig/ocr/ResultPage$ResultWord;

    .line 43
    .line 44
    aget-object v2, v2, v1

    .line 45
    .line 46
    aget-object v3, v0, v1

    .line 47
    .line 48
    invoke-virtual {v2, v3, p2}, Lcom/intsig/ocr/ResultPage$ResultWord;->〇o00〇〇Oo(Lcom/intsig/nativelib/CharItem;I)V

    .line 49
    .line 50
    .line 51
    add-int/lit8 v1, v1, 0x1

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/nativelib/LineItem;->getLineCoords()[I

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    invoke-static {p1}, Lcom/intsig/ocr/ResultPage;->〇080([I)Landroid/graphics/Rect;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    iput-object p1, p0, Lcom/intsig/ocr/ResultPage$ResultLine;->Oo08:Landroid/graphics/Rect;

    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/ocr/ResultPage$ResultLine;->O8:[I

    .line 65
    .line 66
    invoke-static {p1, v0, p2}, Lcom/intsig/ocr/ResultPage;->〇o00〇〇Oo(Landroid/graphics/Rect;[II)V

    .line 67
    .line 68
    .line 69
    :cond_2
    :goto_1
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

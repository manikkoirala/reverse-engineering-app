.class public Lcom/intsig/log4a/FileAppender;
.super Lcom/intsig/log4a/Appender;
.source "FileAppender.java"


# instance fields
.field O8o08O8O:Ljava/io/OutputStream;

.field oOo〇8o008:[B

.field 〇080OO8〇0:Z

.field 〇0O:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/intsig/log4a/PropertyConfigure;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/log4a/Appender;-><init>(Lcom/intsig/log4a/PropertyConfigure;I)V

    .line 2
    .line 3
    .line 4
    const/4 p2, 0x0

    .line 5
    iput-object p2, p0, Lcom/intsig/log4a/FileAppender;->O8o08O8O:Ljava/io/OutputStream;

    .line 6
    .line 7
    const/4 p2, 0x0

    .line 8
    iput-boolean p2, p0, Lcom/intsig/log4a/FileAppender;->〇080OO8〇0:Z

    .line 9
    .line 10
    const/4 p2, 0x2

    .line 11
    new-array p2, p2, [B

    .line 12
    .line 13
    fill-array-data p2, :array_0

    .line 14
    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/log4a/FileAppender;->oOo〇8o008:[B

    .line 17
    .line 18
    invoke-virtual {p0, p1}, Lcom/intsig/log4a/FileAppender;->〇O8o08O(Lcom/intsig/log4a/PropertyConfigure;)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    nop

    .line 23
    :array_0
    .array-data 1
        0xdt
        0xat
    .end array-data
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public declared-synchronized 〇080(Lcom/intsig/log4a/LogEvent;)V
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p1, Lcom/intsig/log4a/LogEvent;->O8:Lcom/intsig/log4a/Level;

    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/intsig/log4a/Appender;->O8(Lcom/intsig/log4a/Level;)Z

    .line 5
    .line 6
    .line 7
    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    :try_start_1
    iget-object v0, p0, Lcom/intsig/log4a/FileAppender;->O8o08O8O:Ljava/io/OutputStream;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/log4a/Appender;->o0:Lcom/intsig/log4a/PropertyConfigure;

    .line 13
    .line 14
    invoke-virtual {p1, v1}, Lcom/intsig/log4a/LogEvent;->〇080(Lcom/intsig/log4a/PropertyConfigure;)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 23
    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/log4a/FileAppender;->O8o08O8O:Ljava/io/OutputStream;

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/log4a/FileAppender;->oOo〇8o008:[B

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 30
    .line 31
    .line 32
    iget-boolean p1, p0, Lcom/intsig/log4a/FileAppender;->〇080OO8〇0:Z

    .line 33
    .line 34
    if-eqz p1, :cond_0

    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/log4a/FileAppender;->O8o08O8O:Ljava/io/OutputStream;

    .line 37
    .line 38
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 39
    .line 40
    .line 41
    goto :goto_1

    .line 42
    :catch_0
    move-exception p1

    .line 43
    goto :goto_0

    .line 44
    :catch_1
    move-exception p1

    .line 45
    :goto_0
    :try_start_2
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 46
    .line 47
    .line 48
    :cond_0
    :goto_1
    monitor-exit p0

    .line 49
    return-void

    .line 50
    :catchall_0
    move-exception p1

    .line 51
    monitor-exit p0

    .line 52
    throw p1
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇80〇808〇O(Lcom/intsig/log4a/PropertyConfigure;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/log4a/Appender;->〇80〇808〇O(Lcom/intsig/log4a/PropertyConfigure;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, p1}, Lcom/intsig/log4a/FileAppender;->〇O8o08O(Lcom/intsig/log4a/PropertyConfigure;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method 〇8o8o〇(Ljava/io/File;)Ljava/io/OutputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/text/SimpleDateFormat;

    .line 2
    .line 3
    const-string v1, "yyyy-MM-dd_HH-mm-ss"

    .line 4
    .line 5
    sget-object v2, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 8
    .line 9
    .line 10
    new-instance v1, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v2, "log-"

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    new-instance v2, Ljava/util/Date;

    .line 21
    .line 22
    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/intsig/log4a/Appender;->o〇0()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    const-string v0, ".log"

    .line 40
    .line 41
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    new-instance v1, Ljava/io/File;

    .line 49
    .line 50
    invoke-direct {v1, p1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    iput-object p1, p0, Lcom/intsig/log4a/FileAppender;->〇0O:Ljava/lang/String;

    .line 58
    .line 59
    new-instance p1, Ljava/io/FileOutputStream;

    .line 60
    .line 61
    invoke-direct {p1, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 62
    .line 63
    .line 64
    return-object p1
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method 〇O8o08O(Lcom/intsig/log4a/PropertyConfigure;)V
    .locals 10

    .line 1
    invoke-virtual {p1}, Lcom/intsig/log4a/PropertyConfigure;->oO80()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    :try_start_0
    new-instance v1, Ljava/io/File;

    .line 6
    .line 7
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    const/4 v2, 0x1

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 v0, 0x1

    .line 23
    :goto_0
    if-nez v0, :cond_1

    .line 24
    .line 25
    return-void

    .line 26
    :cond_1
    new-instance v0, Lcom/intsig/log4a/FileAppender$1;

    .line 27
    .line 28
    invoke-direct {v0, p0}, Lcom/intsig/log4a/FileAppender$1;-><init>(Lcom/intsig/log4a/FileAppender;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1, v0}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    if-eqz v0, :cond_4

    .line 36
    .line 37
    array-length v3, v0

    .line 38
    if-lt v3, v2, :cond_4

    .line 39
    .line 40
    array-length v3, v0

    .line 41
    new-instance v4, Lcom/intsig/log4a/FileAppender$2;

    .line 42
    .line 43
    invoke-direct {v4, p0}, Lcom/intsig/log4a/FileAppender$2;-><init>(Lcom/intsig/log4a/FileAppender;)V

    .line 44
    .line 45
    .line 46
    invoke-static {v0, v4}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 47
    .line 48
    .line 49
    add-int/lit8 v4, v3, -0x1

    .line 50
    .line 51
    aget-object v4, v0, v4

    .line 52
    .line 53
    new-instance v5, Ljava/io/File;

    .line 54
    .line 55
    invoke-direct {v5, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v5}, Ljava/io/File;->length()J

    .line 59
    .line 60
    .line 61
    move-result-wide v6

    .line 62
    invoke-virtual {p1}, Lcom/intsig/log4a/PropertyConfigure;->o〇0()J

    .line 63
    .line 64
    .line 65
    move-result-wide v8

    .line 66
    cmp-long v4, v6, v8

    .line 67
    .line 68
    if-gtz v4, :cond_2

    .line 69
    .line 70
    new-instance v0, Ljava/io/FileOutputStream;

    .line 71
    .line 72
    invoke-direct {v0, v5, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    .line 73
    .line 74
    .line 75
    iput-object v0, p0, Lcom/intsig/log4a/FileAppender;->O8o08O8O:Ljava/io/OutputStream;

    .line 76
    .line 77
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    iput-object v0, p0, Lcom/intsig/log4a/FileAppender;->〇0O:Ljava/lang/String;

    .line 82
    .line 83
    goto :goto_2

    .line 84
    :cond_2
    move v2, v3

    .line 85
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/log4a/PropertyConfigure;->Oo08()I

    .line 86
    .line 87
    .line 88
    move-result v4

    .line 89
    if-lt v2, v4, :cond_3

    .line 90
    .line 91
    new-instance v4, Ljava/io/File;

    .line 92
    .line 93
    sub-int v5, v3, v2

    .line 94
    .line 95
    aget-object v5, v0, v5

    .line 96
    .line 97
    invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 101
    .line 102
    .line 103
    add-int/lit8 v2, v2, -0x1

    .line 104
    .line 105
    goto :goto_1

    .line 106
    :cond_3
    invoke-virtual {p0, v1}, Lcom/intsig/log4a/FileAppender;->〇8o8o〇(Ljava/io/File;)Ljava/io/OutputStream;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    iput-object v0, p0, Lcom/intsig/log4a/FileAppender;->O8o08O8O:Ljava/io/OutputStream;

    .line 111
    .line 112
    goto :goto_2

    .line 113
    :cond_4
    invoke-virtual {p0, v1}, Lcom/intsig/log4a/FileAppender;->〇8o8o〇(Ljava/io/File;)Ljava/io/OutputStream;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    iput-object v0, p0, Lcom/intsig/log4a/FileAppender;->O8o08O8O:Ljava/io/OutputStream;

    .line 118
    .line 119
    :goto_2
    invoke-virtual {p1}, Lcom/intsig/log4a/PropertyConfigure;->〇8o8o〇()Z

    .line 120
    .line 121
    .line 122
    move-result v0

    .line 123
    if-eqz v0, :cond_5

    .line 124
    .line 125
    new-instance v0, Ljava/util/zip/GZIPOutputStream;

    .line 126
    .line 127
    iget-object v1, p0, Lcom/intsig/log4a/FileAppender;->O8o08O8O:Ljava/io/OutputStream;

    .line 128
    .line 129
    invoke-direct {v0, v1}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 130
    .line 131
    .line 132
    iput-object v0, p0, Lcom/intsig/log4a/FileAppender;->O8o08O8O:Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    .line 134
    goto :goto_3

    .line 135
    :catch_0
    move-exception v0

    .line 136
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 137
    .line 138
    .line 139
    :cond_5
    :goto_3
    invoke-virtual {p1}, Lcom/intsig/log4a/PropertyConfigure;->〇080()Z

    .line 140
    .line 141
    .line 142
    move-result p1

    .line 143
    iput-boolean p1, p0, Lcom/intsig/log4a/FileAppender;->〇080OO8〇0:Z

    .line 144
    .line 145
    return-void
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public 〇o〇()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/log4a/Appender;->〇o〇()V

    .line 2
    .line 3
    .line 4
    :try_start_0
    iget-object v0, p0, Lcom/intsig/log4a/FileAppender;->O8o08O8O:Ljava/io/OutputStream;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/log4a/FileAppender;->O8o08O8O:Ljava/io/OutputStream;

    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 14
    .line 15
    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lcom/intsig/log4a/FileAppender;->O8o08O8O:Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :catch_0
    move-exception v0

    .line 21
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 22
    .line 23
    .line 24
    :goto_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

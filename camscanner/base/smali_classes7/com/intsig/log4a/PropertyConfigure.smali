.class public Lcom/intsig/log4a/PropertyConfigure;
.super Ljava/lang/Object;
.source "PropertyConfigure.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/log4a/PropertyConfigure$Layout;
    }
.end annotation


# instance fields
.field O8:J

.field Oo08:J

.field o〇0:I

.field 〇080:Ljava/util/Properties;

.field 〇o00〇〇Oo:Lcom/intsig/log4a/Level;

.field 〇o〇:Ljava/lang/String;

.field 〇〇888:Lcom/intsig/log4a/PropertyConfigure$Layout;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0, p1}, Lcom/intsig/log4a/PropertyConfigure;-><init>(Ljava/util/Properties;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Properties;Ljava/lang/String;)V
    .locals 4

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    iput-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    .line 4
    sget-object v0, Lcom/intsig/log4a/Level;->O8:Lcom/intsig/log4a/Level;

    iput-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇o00〇〇Oo:Lcom/intsig/log4a/Level;

    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇o〇:Ljava/lang/String;

    const-wide/16 v0, -0x1

    .line 6
    iput-wide v0, p0, Lcom/intsig/log4a/PropertyConfigure;->O8:J

    .line 7
    iput-wide v0, p0, Lcom/intsig/log4a/PropertyConfigure;->Oo08:J

    const/4 v0, -0x1

    .line 8
    iput v0, p0, Lcom/intsig/log4a/PropertyConfigure;->o〇0:I

    if-eqz p1, :cond_0

    .line 9
    invoke-virtual {p1}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v0

    .line 10
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    .line 12
    iget-object v2, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    invoke-virtual {p1, v1}, Ljava/util/Dictionary;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/Dictionary;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 13
    new-instance p1, Ljava/io/File;

    invoke-direct {p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 14
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 15
    :try_start_0
    new-instance p2, Ljava/io/FileInputStream;

    invoke-direct {p2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 16
    iget-object p1, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    invoke-virtual {p1, p2}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 17
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 18
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/log4a/PropertyConfigure;->OO0o〇〇〇〇0()V

    return-void
.end method


# virtual methods
.method public O8()Lcom/intsig/log4a/Appender;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    .line 2
    .line 3
    const-string v1, "log4a.extra.appender"

    .line 4
    .line 5
    const-string v2, "fast_file"

    .line 6
    .line 7
    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    const/16 v2, 0x14

    .line 16
    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    new-instance v0, Lcom/intsig/log4a/FastFileAppender;

    .line 20
    .line 21
    invoke-direct {v0, p0, v2}, Lcom/intsig/log4a/FastFileAppender;-><init>(Lcom/intsig/log4a/PropertyConfigure;I)V

    .line 22
    .line 23
    .line 24
    return-object v0

    .line 25
    :cond_0
    const-string v1, "file"

    .line 26
    .line 27
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    new-instance v0, Lcom/intsig/log4a/FileAppender;

    .line 34
    .line 35
    invoke-direct {v0, p0, v2}, Lcom/intsig/log4a/FileAppender;-><init>(Lcom/intsig/log4a/PropertyConfigure;I)V

    .line 36
    .line 37
    .line 38
    return-object v0

    .line 39
    :cond_1
    new-instance v0, Lcom/intsig/log4a/EncFileAppender;

    .line 40
    .line 41
    invoke-direct {v0, p0, v2}, Lcom/intsig/log4a/EncFileAppender;-><init>(Lcom/intsig/log4a/PropertyConfigure;I)V

    .line 42
    .line 43
    .line 44
    return-object v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method OO0o〇〇〇〇0()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    .line 2
    .line 3
    const-string v1, "log4a.level"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const-string v1, "off"

    .line 10
    .line 11
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    sget-object v0, Lcom/intsig/log4a/Level;->〇o〇:Lcom/intsig/log4a/Level;

    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇o00〇〇Oo:Lcom/intsig/log4a/Level;

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const-string v1, "error"

    .line 23
    .line 24
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-eqz v1, :cond_1

    .line 29
    .line 30
    sget-object v0, Lcom/intsig/log4a/Level;->O8:Lcom/intsig/log4a/Level;

    .line 31
    .line 32
    iput-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇o00〇〇Oo:Lcom/intsig/log4a/Level;

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    const-string v1, "warn"

    .line 36
    .line 37
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    if-eqz v1, :cond_2

    .line 42
    .line 43
    sget-object v0, Lcom/intsig/log4a/Level;->Oo08:Lcom/intsig/log4a/Level;

    .line 44
    .line 45
    iput-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇o00〇〇Oo:Lcom/intsig/log4a/Level;

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_2
    const-string v1, "info"

    .line 49
    .line 50
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    if-eqz v1, :cond_3

    .line 55
    .line 56
    sget-object v0, Lcom/intsig/log4a/Level;->o〇0:Lcom/intsig/log4a/Level;

    .line 57
    .line 58
    iput-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇o00〇〇Oo:Lcom/intsig/log4a/Level;

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_3
    const-string v1, "debug"

    .line 62
    .line 63
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    if-eqz v0, :cond_4

    .line 68
    .line 69
    sget-object v0, Lcom/intsig/log4a/Level;->〇〇888:Lcom/intsig/log4a/Level;

    .line 70
    .line 71
    iput-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇o00〇〇Oo:Lcom/intsig/log4a/Level;

    .line 72
    .line 73
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    .line 74
    .line 75
    const-string v1, "log4a.format"

    .line 76
    .line 77
    const-string v2, "%l\t%d %t\t%g\t%m"

    .line 78
    .line 79
    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    .line 84
    .line 85
    const-string v2, "log4a.time.format"

    .line 86
    .line 87
    const-string v3, "MM-dd HH:mm:ss.SSS"

    .line 88
    .line 89
    invoke-virtual {v1, v2, v3}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v1

    .line 93
    iget-object v2, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    .line 94
    .line 95
    const-string v3, "log4a.thread"

    .line 96
    .line 97
    const-string v4, "name"

    .line 98
    .line 99
    invoke-virtual {v2, v3, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object v2

    .line 103
    new-instance v3, Lcom/intsig/log4a/PropertyConfigure$Layout;

    .line 104
    .line 105
    invoke-direct {v3, p0, v0, v1, v2}, Lcom/intsig/log4a/PropertyConfigure$Layout;-><init>(Lcom/intsig/log4a/PropertyConfigure;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    iput-object v3, p0, Lcom/intsig/log4a/PropertyConfigure;->〇〇888:Lcom/intsig/log4a/PropertyConfigure$Layout;

    .line 109
    .line 110
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public Oo08()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/log4a/PropertyConfigure;->o〇0:I

    .line 2
    .line 3
    if-gez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    .line 6
    .line 7
    const-string v1, "log4a.appender.file.maxnumbers"

    .line 8
    .line 9
    const-string v2, "1"

    .line 10
    .line 11
    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    iput v0, p0, Lcom/intsig/log4a/PropertyConfigure;->o〇0:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :catch_0
    move-exception v0

    .line 23
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 24
    .line 25
    .line 26
    const/4 v0, 0x1

    .line 27
    iput v0, p0, Lcom/intsig/log4a/PropertyConfigure;->o〇0:I

    .line 28
    .line 29
    :cond_0
    :goto_0
    iget v0, p0, Lcom/intsig/log4a/PropertyConfigure;->o〇0:I

    .line 30
    .line 31
    return v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public oO80()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇o〇:Ljava/lang/String;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    .line 6
    .line 7
    const-string v1, "log4a.appender.file.dir"

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iput-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇o〇:Ljava/lang/String;

    .line 15
    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇o〇:Ljava/lang/String;

    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0()J
    .locals 8

    .line 1
    iget-wide v0, p0, Lcom/intsig/log4a/PropertyConfigure;->O8:J

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    cmp-long v4, v0, v2

    .line 6
    .line 7
    if-gez v4, :cond_4

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    .line 10
    .line 11
    const-string v1, "log4a.appender.file.maxsize"

    .line 12
    .line 13
    const-string v2, "1M"

    .line 14
    .line 15
    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const-wide/32 v1, 0x100000

    .line 20
    .line 21
    .line 22
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    add-int/lit8 v3, v3, -0x1

    .line 35
    .line 36
    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    const/16 v4, 0x4b

    .line 41
    .line 42
    const/16 v5, 0x4d

    .line 43
    .line 44
    if-eq v3, v5, :cond_0

    .line 45
    .line 46
    if-ne v3, v4, :cond_1

    .line 47
    .line 48
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 49
    .line 50
    .line 51
    move-result v6

    .line 52
    add-int/lit8 v6, v6, -0x1

    .line 53
    .line 54
    const/4 v7, 0x0

    .line 55
    invoke-virtual {v0, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    :cond_1
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 60
    .line 61
    .line 62
    move-result-wide v6

    .line 63
    if-ne v3, v5, :cond_2

    .line 64
    .line 65
    mul-long v6, v6, v1

    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_2
    if-ne v3, v4, :cond_3

    .line 69
    .line 70
    const-wide/16 v3, 0x400

    .line 71
    .line 72
    mul-long v6, v6, v3

    .line 73
    .line 74
    :cond_3
    :goto_0
    iput-wide v6, p0, Lcom/intsig/log4a/PropertyConfigure;->O8:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    .line 76
    goto :goto_1

    .line 77
    :catch_0
    move-exception v0

    .line 78
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 79
    .line 80
    .line 81
    iput-wide v1, p0, Lcom/intsig/log4a/PropertyConfigure;->O8:J

    .line 82
    .line 83
    :cond_4
    :goto_1
    iget-wide v0, p0, Lcom/intsig/log4a/PropertyConfigure;->O8:J

    .line 84
    .line 85
    return-wide v0
    .line 86
.end method

.method 〇080()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    .line 2
    .line 3
    const-string v1, "log4a.appender.file.flush.immediately"

    .line 4
    .line 5
    const-string v2, "false"

    .line 6
    .line 7
    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    :try_start_0
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    .line 12
    .line 13
    .line 14
    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 15
    return v0

    .line 16
    :catch_0
    move-exception v0

    .line 17
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 18
    .line 19
    .line 20
    const/4 v0, 0x0

    .line 21
    return v0
.end method

.method public 〇80〇808〇O()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    .line 2
    .line 3
    const-string v1, "process_name"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method 〇8o8o〇()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    .line 2
    .line 3
    const-string v1, "log4a.appender.file.zip"

    .line 4
    .line 5
    const-string v2, "false"

    .line 6
    .line 7
    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    :try_start_0
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    .line 12
    .line 13
    .line 14
    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 15
    return v0

    .line 16
    :catch_0
    move-exception v0

    .line 17
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 18
    .line 19
    .line 20
    const/4 v0, 0x1

    .line 21
    return v0
.end method

.method public 〇O8o08O(Lcom/intsig/log4a/LogEvent;)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇〇888:Lcom/intsig/log4a/PropertyConfigure$Layout;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/log4a/PropertyConfigure$Layout;->〇080(Lcom/intsig/log4a/LogEvent;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇o00〇〇Oo()Lcom/intsig/log4a/Appender;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    .line 2
    .line 3
    const-string v1, "log4a.appender"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const-string v1, "logcat"

    .line 10
    .line 11
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    new-instance v0, Lcom/intsig/log4a/LogcatAppender;

    .line 18
    .line 19
    invoke-direct {v0, p0}, Lcom/intsig/log4a/LogcatAppender;-><init>(Lcom/intsig/log4a/PropertyConfigure;)V

    .line 20
    .line 21
    .line 22
    return-object v0

    .line 23
    :cond_0
    const-string v1, "console"

    .line 24
    .line 25
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    if-eqz v1, :cond_1

    .line 30
    .line 31
    new-instance v0, Lcom/intsig/log4a/ConsoleAppender;

    .line 32
    .line 33
    invoke-direct {v0, p0}, Lcom/intsig/log4a/ConsoleAppender;-><init>(Lcom/intsig/log4a/PropertyConfigure;)V

    .line 34
    .line 35
    .line 36
    return-object v0

    .line 37
    :cond_1
    const-string v1, "enc_file"

    .line 38
    .line 39
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    const/16 v2, 0x14

    .line 44
    .line 45
    if-eqz v1, :cond_2

    .line 46
    .line 47
    new-instance v0, Lcom/intsig/log4a/EncFileAppender;

    .line 48
    .line 49
    invoke-direct {v0, p0, v2}, Lcom/intsig/log4a/EncFileAppender;-><init>(Lcom/intsig/log4a/PropertyConfigure;I)V

    .line 50
    .line 51
    .line 52
    return-object v0

    .line 53
    :cond_2
    const-string v1, "fast_file"

    .line 54
    .line 55
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    if-eqz v0, :cond_3

    .line 60
    .line 61
    new-instance v0, Lcom/intsig/log4a/FastFileAppender;

    .line 62
    .line 63
    invoke-direct {v0, p0, v2}, Lcom/intsig/log4a/FastFileAppender;-><init>(Lcom/intsig/log4a/PropertyConfigure;I)V

    .line 64
    .line 65
    .line 66
    return-object v0

    .line 67
    :cond_3
    new-instance v0, Lcom/intsig/log4a/FileAppender;

    .line 68
    .line 69
    invoke-direct {v0, p0, v2}, Lcom/intsig/log4a/FileAppender;-><init>(Lcom/intsig/log4a/PropertyConfigure;I)V

    .line 70
    .line 71
    .line 72
    return-object v0
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇o〇()J
    .locals 8

    .line 1
    iget-wide v0, p0, Lcom/intsig/log4a/PropertyConfigure;->Oo08:J

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    const-wide/32 v4, 0x100000

    .line 6
    .line 7
    .line 8
    cmp-long v6, v0, v2

    .line 9
    .line 10
    if-gez v6, :cond_4

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇080:Ljava/util/Properties;

    .line 13
    .line 14
    const-string v1, "log4a.appender.cache.maxsize"

    .line 15
    .line 16
    const-string v2, "1M"

    .line 17
    .line 18
    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    add-int/lit8 v1, v1, -0x1

    .line 35
    .line 36
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    const/16 v2, 0x4b

    .line 41
    .line 42
    const/16 v3, 0x4d

    .line 43
    .line 44
    if-eq v1, v3, :cond_0

    .line 45
    .line 46
    if-ne v1, v2, :cond_1

    .line 47
    .line 48
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 49
    .line 50
    .line 51
    move-result v6

    .line 52
    add-int/lit8 v6, v6, -0x1

    .line 53
    .line 54
    const/4 v7, 0x0

    .line 55
    invoke-virtual {v0, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    :cond_1
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 60
    .line 61
    .line 62
    move-result-wide v6

    .line 63
    if-ne v1, v3, :cond_2

    .line 64
    .line 65
    mul-long v6, v6, v4

    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_2
    if-ne v1, v2, :cond_3

    .line 69
    .line 70
    const-wide/16 v0, 0x400

    .line 71
    .line 72
    mul-long v6, v6, v0

    .line 73
    .line 74
    :cond_3
    :goto_0
    iput-wide v6, p0, Lcom/intsig/log4a/PropertyConfigure;->Oo08:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    .line 76
    goto :goto_1

    .line 77
    :catch_0
    move-exception v0

    .line 78
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 79
    .line 80
    .line 81
    iput-wide v4, p0, Lcom/intsig/log4a/PropertyConfigure;->Oo08:J

    .line 82
    .line 83
    :cond_4
    :goto_1
    iget-wide v0, p0, Lcom/intsig/log4a/PropertyConfigure;->Oo08:J

    .line 84
    .line 85
    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    .line 86
    .line 87
    .line 88
    move-result-wide v0

    .line 89
    return-wide v0
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public 〇〇888()Lcom/intsig/log4a/Level;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/log4a/PropertyConfigure;->〇o00〇〇Oo:Lcom/intsig/log4a/Level;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

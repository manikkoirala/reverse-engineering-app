.class public final Lcom/intsig/innote/R$string;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/innote/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final a_about_app_copyright:I = 0x7f130001

.field public static final a_about_app_version:I = 0x7f130002

.field public static final a_account_btn_keep_secret:I = 0x7f130005

.field public static final a_account_btn_quit_hint:I = 0x7f130006

.field public static final a_account_msg_password_expired:I = 0x7f130007

.field public static final a_autocomposite_document_rename:I = 0x7f130009

.field public static final a_backup_summary:I = 0x7f13000a

.field public static final a_btn_accurate_ocr:I = 0x7f13000b

.field public static final a_btn_add_tag:I = 0x7f13000c

.field public static final a_btn_bound_phone_or_email:I = 0x7f13000d

.field public static final a_btn_change_email:I = 0x7f13000e

.field public static final a_btn_christmas:I = 0x7f13000f

.field public static final a_btn_contact_us:I = 0x7f130010

.field public static final a_btn_copy_text:I = 0x7f130011

.field public static final a_btn_document_finish_continue:I = 0x7f130012

.field public static final a_btn_document_finish_id_card:I = 0x7f130013

.field public static final a_btn_dont_show_anymore:I = 0x7f130014

.field public static final a_btn_get_it_right_now:I = 0x7f130015

.field public static final a_btn_give_up:I = 0x7f130016

.field public static final a_btn_go_open_sync:I = 0x7f130017

.field public static final a_btn_go_to_login:I = 0x7f130018

.field public static final a_btn_i_know:I = 0x7f130019

.field public static final a_btn_insert:I = 0x7f13001a

.field public static final a_btn_know_more:I = 0x7f13001b

.field public static final a_btn_konw_more:I = 0x7f13001c

.field public static final a_btn_mark_all_readed:I = 0x7f13001d

.field public static final a_btn_month_price:I = 0x7f13001e

.field public static final a_btn_ocr_select_language:I = 0x7f13001f

.field public static final a_btn_ocr_translation:I = 0x7f130020

.field public static final a_btn_open:I = 0x7f130021

.field public static final a_btn_open_sync:I = 0x7f130022

.field public static final a_btn_open_website:I = 0x7f130023

.field public static final a_btn_purchase_now:I = 0x7f130024

.field public static final a_btn_redeem_invite_code:I = 0x7f130025

.field public static final a_btn_redo_ocr:I = 0x7f130026

.field public static final a_btn_repeat_try:I = 0x7f130027

.field public static final a_btn_replace:I = 0x7f130028

.field public static final a_btn_save_topic:I = 0x7f130029

.field public static final a_btn_save_topic_double:I = 0x7f13002a

.field public static final a_btn_start_free_trial:I = 0x7f13002b

.field public static final a_btn_sync_now:I = 0x7f13002c

.field public static final a_btn_tip_assist:I = 0x7f13002d

.field public static final a_btn_tip_comment:I = 0x7f13002e

.field public static final a_btn_tip_note:I = 0x7f13002f

.field public static final a_btn_unlock_reward:I = 0x7f130030

.field public static final a_btn_upgrade_now:I = 0x7f130031

.field public static final a_btn_use_now:I = 0x7f130032

.field public static final a_btn_view_note:I = 0x7f130033

.field public static final a_btn_year_price:I = 0x7f130034

.field public static final a_button_start_shoot_a_picture:I = 0x7f130035

.field public static final a_button_text_get_now:I = 0x7f130036

.field public static final a_camcard_ad_tip:I = 0x7f130037

.field public static final a_capture_label_flash_torch:I = 0x7f130038

.field public static final a_cloud_msg_relogin:I = 0x7f130039

.field public static final a_default_month_price:I = 0x7f13003a

.field public static final a_desc_tips_jigsaw_edit:I = 0x7f13003b

.field public static final a_desc_tips_topic_edit:I = 0x7f13003c

.field public static final a_descrption_pay_version_feature:I = 0x7f13003d

.field public static final a_descrption_premium_feature:I = 0x7f13003e

.field public static final a_dialog_title_error:I = 0x7f13003f

.field public static final a_dlg_msg_verify_failed:I = 0x7f130040

.field public static final a_dlg_msg_verifying:I = 0x7f130041

.field public static final a_doc_menu_clear_pdf_password:I = 0x7f130042

.field public static final a_document_msg_copying:I = 0x7f130043

.field public static final a_document_msg_moving:I = 0x7f130044

.field public static final a_document_title_select_goal:I = 0x7f130045

.field public static final a_fax_btn_recharge:I = 0x7f130046

.field public static final a_fax_btn_send:I = 0x7f130047

.field public static final a_fax_hint_country:I = 0x7f130048

.field public static final a_fax_hint_number:I = 0x7f130049

.field public static final a_fax_hint_sample:I = 0x7f13004a

.field public static final a_fax_label_number1:I = 0x7f13004b

.field public static final a_fax_label_pages:I = 0x7f13004c

.field public static final a_fax_label_task_state:I = 0x7f13004d

.field public static final a_fax_msg_balance_not_enough:I = 0x7f13004e

.field public static final a_fax_msg_check_faxnumber:I = 0x7f13004f

.field public static final a_fax_msg_comfirm_desc:I = 0x7f130050

.field public static final a_fax_msg_deny:I = 0x7f130051

.field public static final a_fax_msg_error_send_empty:I = 0x7f130052

.field public static final a_fax_msg_no_task:I = 0x7f130053

.field public static final a_fax_msg_obtain:I = 0x7f130054

.field public static final a_fax_msg_permission:I = 0x7f130055

.field public static final a_fax_msg_query_balance:I = 0x7f130056

.field public static final a_fax_msg_query_failure:I = 0x7f130057

.field public static final a_fax_msg_start_with_zero:I = 0x7f130058

.field public static final a_fax_msg_state_send_failure:I = 0x7f130059

.field public static final a_fax_msg_state_send_success:I = 0x7f13005a

.field public static final a_fax_msg_state_sending:I = 0x7f13005b

.field public static final a_fax_msg_state_wait:I = 0x7f13005c

.field public static final a_fax_title_comfirm:I = 0x7f13005d

.field public static final a_fax_title_country:I = 0x7f13005e

.field public static final a_fax_title_notification:I = 0x7f13005f

.field public static final a_global_btn_cancel_task:I = 0x7f130060

.field public static final a_global_btn_close:I = 0x7f130061

.field public static final a_global_btn_later:I = 0x7f130062

.field public static final a_global_btn_not_share:I = 0x7f130063

.field public static final a_global_btn_recommend:I = 0x7f130064

.field public static final a_global_btn_refresh:I = 0x7f130065

.field public static final a_global_btn_remove_task:I = 0x7f130066

.field public static final a_global_btn_resend_task:I = 0x7f130067

.field public static final a_global_cs_service_email:I = 0x7f130068

.field public static final a_global_cs_url:I = 0x7f130069

.field public static final a_global_hint_add_ink:I = 0x7f13006a

.field public static final a_global_hint_input_password:I = 0x7f13006b

.field public static final a_global_hint_input_password_again:I = 0x7f13006c

.field public static final a_global_hint_sign_out:I = 0x7f13006d

.field public static final a_global_label_adjust_bright:I = 0x7f13006e

.field public static final a_global_label_adjust_contrast:I = 0x7f13006f

.field public static final a_global_label_adjust_detail:I = 0x7f130070

.field public static final a_global_label_doc_name:I = 0x7f130071

.field public static final a_global_label_facebook:I = 0x7f130072

.field public static final a_global_label_fax:I = 0x7f130073

.field public static final a_global_label_login:I = 0x7f130074

.field public static final a_global_label_print:I = 0x7f130075

.field public static final a_global_label_privce_policy:I = 0x7f130076

.field public static final a_global_label_save:I = 0x7f130077

.field public static final a_global_label_select_none:I = 0x7f130078

.field public static final a_global_label_task_start_time:I = 0x7f130079

.field public static final a_global_label_tell_friend:I = 0x7f13007a

.field public static final a_global_label_twitter:I = 0x7f13007b

.field public static final a_global_label_upgrade:I = 0x7f13007c

.field public static final a_global_label_weibo:I = 0x7f13007d

.field public static final a_global_msg_auth_error:I = 0x7f13007e

.field public static final a_global_msg_auth_ok:I = 0x7f13007f

.field public static final a_global_msg_battery_low:I = 0x7f130080

.field public static final a_global_msg_clean_task_recode:I = 0x7f130081

.field public static final a_global_msg_commit_to_server_fail:I = 0x7f130082

.field public static final a_global_msg_fail:I = 0x7f130083

.field public static final a_global_msg_gps_prompt:I = 0x7f130084

.field public static final a_global_msg_image_missing:I = 0x7f130085

.field public static final a_global_msg_image_not_exist:I = 0x7f130086

.field public static final a_global_msg_input_password:I = 0x7f130087

.field public static final a_global_msg_invite_content:I = 0x7f130088

.field public static final a_global_msg_load_failed:I = 0x7f130089

.field public static final a_global_msg_loading:I = 0x7f13008a

.field public static final a_global_msg_loging_out:I = 0x7f13008b

.field public static final a_global_msg_net_timeout:I = 0x7f13008c

.field public static final a_global_msg_network_not_available:I = 0x7f13008d

.field public static final a_global_msg_no_docs_in_tag:I = 0x7f13008e

.field public static final a_global_msg_openapi_error:I = 0x7f13008f

.field public static final a_global_msg_openapi_must_login:I = 0x7f130090

.field public static final a_global_msg_password_error:I = 0x7f130091

.field public static final a_global_msg_password_not_same:I = 0x7f130092

.field public static final a_global_msg_password_null:I = 0x7f130093

.field public static final a_global_msg_pdf_size_limit:I = 0x7f130094

.field public static final a_global_msg_recommend_complete:I = 0x7f130095

.field public static final a_global_msg_recommend_content_long:I = 0x7f130096

.field public static final a_global_msg_recommend_content_short_new:I = 0x7f130097

.field public static final a_global_msg_recommend_content_short_new_assist:I = 0x7f130098

.field public static final a_global_msg_recommend_failed:I = 0x7f130099

.field public static final a_global_msg_recommend_fb_new:I = 0x7f13009a

.field public static final a_global_msg_recommend_tw_new:I = 0x7f13009b

.field public static final a_global_msg_recommend_tw_new_assist:I = 0x7f13009c

.field public static final a_global_msg_recommend_wb_new:I = 0x7f13009d

.field public static final a_global_msg_recommend_wb_new_assist:I = 0x7f13009e

.field public static final a_global_msg_recommend_wx_new:I = 0x7f13009f

.field public static final a_global_msg_recommend_wx_new_assist:I = 0x7f1300a0

.field public static final a_global_msg_sign_out:I = 0x7f1300a1

.field public static final a_global_msg_success:I = 0x7f1300a2

.field public static final a_global_msg_task_process:I = 0x7f1300a3

.field public static final a_global_share_link_subject:I = 0x7f1300a4

.field public static final a_global_sum_purchase_premium_now:I = 0x7f1300a5

.field public static final a_global_title_access_doc:I = 0x7f1300a6

.field public static final a_global_title_activate_protect:I = 0x7f1300a7

.field public static final a_global_title_cs_vip_account:I = 0x7f1300a8

.field public static final a_global_title_notification:I = 0x7f1300a9

.field public static final a_global_title_openapi_error:I = 0x7f1300aa

.field public static final a_global_title_orientation_auto:I = 0x7f1300ab

.field public static final a_global_title_orientation_landscape:I = 0x7f1300ac

.field public static final a_global_title_orientation_portrait:I = 0x7f1300ad

.field public static final a_global_title_refer_to_earn:I = 0x7f1300ae

.field public static final a_global_title_reward_edu_login:I = 0x7f1300af

.field public static final a_global_title_reward_job:I = 0x7f1300b0

.field public static final a_global_title_reward_normal_login:I = 0x7f1300b1

.field public static final a_global_title_set_pdf_password:I = 0x7f1300b2

.field public static final a_global_title_tips:I = 0x7f1300b3

.field public static final a_global_upgrade_share_msg_new:I = 0x7f1300b4

.field public static final a_global_upgrade_share_msg_new_assist:I = 0x7f1300b5

.field public static final a_global_upgrade_share_title:I = 0x7f1300b6

.field public static final a_global_upgrade_show_msg:I = 0x7f1300b7

.field public static final a_global_upgrade_show_title:I = 0x7f1300b8

.field public static final a_hint_add_note:I = 0x7f1300b9

.field public static final a_hint_buy_on_pc:I = 0x7f1300ba

.field public static final a_hint_input_email:I = 0x7f1300bb

.field public static final a_hint_input_phone:I = 0x7f1300bc

.field public static final a_hint_input_validated_code:I = 0x7f1300bd

.field public static final a_hint_input_water_mark:I = 0x7f1300be

.field public static final a_hint_ocr_export:I = 0x7f1300bf

.field public static final a_hint_page_name_input:I = 0x7f1300c0

.field public static final a_image_share_default_txt:I = 0x7f1300c1

.field public static final a_image_share_default_txt_link:I = 0x7f1300c2

.field public static final a_img_btn_text_add_page:I = 0x7f1300c3

.field public static final a_img_btn_text_comment:I = 0x7f1300c4

.field public static final a_img_btn_text_mark:I = 0x7f1300c5

.field public static final a_img_btn_text_recognize:I = 0x7f1300c6

.field public static final a_img_btn_text_rotate:I = 0x7f1300c7

.field public static final a_label_12_month:I = 0x7f1300e2

.field public static final a_label_1_month:I = 0x7f1300e3

.field public static final a_label_accept:I = 0x7f1300e4

.field public static final a_label_ad_exit:I = 0x7f1300e5

.field public static final a_label_add_by_capture:I = 0x7f1300e6

.field public static final a_label_add_cloud_hint:I = 0x7f1300e7

.field public static final a_label_authority_explain_title:I = 0x7f1300e8

.field public static final a_label_authority_title:I = 0x7f1300e9

.field public static final a_label_autoupload_jpg:I = 0x7f1300ea

.field public static final a_label_autoupload_open:I = 0x7f1300eb

.field public static final a_label_autoupload_pdf:I = 0x7f1300ec

.field public static final a_label_autoupload_tryloginagain:I = 0x7f1300ed

.field public static final a_label_bind_right_now:I = 0x7f1300ee

.field public static final a_label_browse_ocr_result:I = 0x7f1300ef

.field public static final a_label_btn_ignore:I = 0x7f1300f0

.field public static final a_label_business_card:I = 0x7f1300f1

.field public static final a_label_buy_points:I = 0x7f1300f2

.field public static final a_label_call_phone:I = 0x7f1300f3

.field public static final a_label_camcard_doc_banner:I = 0x7f1300f4

.field public static final a_label_camera_hint:I = 0x7f1300f5

.field public static final a_label_cancel_offline_psw:I = 0x7f1300f7

.field public static final a_label_cancel_select_all:I = 0x7f1300f8

.field public static final a_label_capture_cn_driver:I = 0x7f1300f9

.field public static final a_label_capture_cn_driver_car:I = 0x7f1300fa

.field public static final a_label_capture_cn_driver_person:I = 0x7f1300fb

.field public static final a_label_capture_driver:I = 0x7f1300fc

.field public static final a_label_capture_id_back:I = 0x7f1300fd

.field public static final a_label_capture_id_front:I = 0x7f1300fe

.field public static final a_label_capture_idcard:I = 0x7f1300ff

.field public static final a_label_capture_mode_certificate:I = 0x7f130100

.field public static final a_label_capture_mode_greet_card:I = 0x7f130101

.field public static final a_label_capture_mode_namecard:I = 0x7f130102

.field public static final a_label_capture_mode_normal:I = 0x7f130103

.field public static final a_label_capture_mode_ocr:I = 0x7f130104

.field public static final a_label_capture_mode_ppt:I = 0x7f130105

.field public static final a_label_capture_mode_topic:I = 0x7f130106

.field public static final a_label_capture_passport:I = 0x7f130107

.field public static final a_label_capture_residence_booklet:I = 0x7f130108

.field public static final a_label_check_privilege:I = 0x7f130109

.field public static final a_label_clean_space:I = 0x7f13010a

.field public static final a_label_clear_cache_size:I = 0x7f13010b

.field public static final a_label_click_clean:I = 0x7f13010c

.field public static final a_label_close_panel:I = 0x7f13010d

.field public static final a_label_collaborate_pull_can_refresh:I = 0x7f13010e

.field public static final a_label_collaborate_release_to_refresh:I = 0x7f13010f

.field public static final a_label_collaborating:I = 0x7f130110

.field public static final a_label_color:I = 0x7f130111

.field public static final a_label_comments:I = 0x7f130112

.field public static final a_label_composite:I = 0x7f130113

.field public static final a_label_composite_preview:I = 0x7f130114

.field public static final a_label_content_delete:I = 0x7f130115

.field public static final a_label_continue:I = 0x7f130116

.field public static final a_label_cs_developer:I = 0x7f130117

.field public static final a_label_cs_developer_fix_data:I = 0x7f130118

.field public static final a_label_cs_developer_fix_datebase:I = 0x7f130119

.field public static final a_label_cs_developer_fix_doc_thumb:I = 0x7f13011a

.field public static final a_label_cs_or:I = 0x7f13011b

.field public static final a_label_cs_vip_right:I = 0x7f13011c

.field public static final a_label_cutom_gallery:I = 0x7f13011d

.field public static final a_label_day:I = 0x7f13011e

.field public static final a_label_deep_clean:I = 0x7f13011f

.field public static final a_label_deep_clean_btn_enable:I = 0x7f130120

.field public static final a_label_deep_clean_msg:I = 0x7f130121

.field public static final a_label_delete:I = 0x7f130122

.field public static final a_label_delete_in_offline_folder:I = 0x7f130123

.field public static final a_label_deny:I = 0x7f130124

.field public static final a_label_description_in_vip_purchase_dialog:I = 0x7f130125

.field public static final a_label_detail:I = 0x7f130126

.field public static final a_label_dir_storage_root_change:I = 0x7f130127

.field public static final a_label_discard:I = 0x7f130128

.field public static final a_label_doc_like_accounts:I = 0x7f130129

.field public static final a_label_doc_search_no_match_curtag:I = 0x7f13012a

.field public static final a_label_doc_show_order_asc:I = 0x7f13012b

.field public static final a_label_doc_show_order_desc:I = 0x7f13012c

.field public static final a_label_doc_sort_name_atoz:I = 0x7f13012d

.field public static final a_label_doc_sort_name_ztoa:I = 0x7f13012e

.field public static final a_label_done_scan:I = 0x7f13012f

.field public static final a_label_download_free_genuine_app:I = 0x7f130130

.field public static final a_label_drawer_menu_doc:I = 0x7f130131

.field public static final a_label_drawer_menu_messagecentre:I = 0x7f130132

.field public static final a_label_drawer_menu_tag:I = 0x7f130133

.field public static final a_label_drawer_my_doc:I = 0x7f130134

.field public static final a_label_e_evidence_list:I = 0x7f130135

.field public static final a_label_email:I = 0x7f130136

.field public static final a_label_enter_account_psw:I = 0x7f130137

.field public static final a_label_enter_offline_psw:I = 0x7f130138

.field public static final a_label_exit:I = 0x7f130139

.field public static final a_label_experience_expire_time:I = 0x7f13013a

.field public static final a_label_export_image:I = 0x7f13013b

.field public static final a_label_export_image_unlimited:I = 0x7f13013c

.field public static final a_label_external_storage_available:I = 0x7f13013d

.field public static final a_label_failed_purchase_7_day_msg:I = 0x7f13013e

.field public static final a_label_fax_buy:I = 0x7f13013f

.field public static final a_label_feedback_email:I = 0x7f130140

.field public static final a_label_first_enter_offline_tips:I = 0x7f130141

.field public static final a_label_follow:I = 0x7f130142

.field public static final a_label_follow_us_fb:I = 0x7f130143

.field public static final a_label_follow_us_twitter:I = 0x7f130144

.field public static final a_label_follow_us_weibo:I = 0x7f130145

.field public static final a_label_free:I = 0x7f130146

.field public static final a_label_free_cloud:I = 0x7f130147

.field public static final a_label_free_quota_tips:I = 0x7f130148

.field public static final a_label_gallery_perference:I = 0x7f130149

.field public static final a_label_give_up:I = 0x7f13014a

.field public static final a_label_go2_cache_clean:I = 0x7f13014b

.field public static final a_label_go_set:I = 0x7f13014c

.field public static final a_label_go_to_gp_dialog_paytm_recharge:I = 0x7f13014d

.field public static final a_label_go_to_see_offline_folder:I = 0x7f13014e

.field public static final a_label_goto_facebook:I = 0x7f13014f

.field public static final a_label_greetcard_guide_1:I = 0x7f130150

.field public static final a_label_greetcard_guide_2:I = 0x7f130151

.field public static final a_label_greetcard_guide_3:I = 0x7f130152

.field public static final a_label_greetcard_guide_4:I = 0x7f130153

.field public static final a_label_guide_desc_1:I = 0x7f130154

.field public static final a_label_guide_desc_2:I = 0x7f130155

.field public static final a_label_guide_desc_3:I = 0x7f130156

.field public static final a_label_guide_desc_4:I = 0x7f130157

.field public static final a_label_guide_desc_5:I = 0x7f130158

.field public static final a_label_guide_jump2lastpage:I = 0x7f130159

.field public static final a_label_guide_login_in_right_now:I = 0x7f13015a

.field public static final a_label_guide_start_using:I = 0x7f13015b

.field public static final a_label_guide_title_1:I = 0x7f13015c

.field public static final a_label_guide_title_2:I = 0x7f13015d

.field public static final a_label_guide_title_3:I = 0x7f13015e

.field public static final a_label_guide_title_4:I = 0x7f13015f

.field public static final a_label_guide_title_5:I = 0x7f130160

.field public static final a_label_handingwrite:I = 0x7f130161

.field public static final a_label_has_delete_size:I = 0x7f130162

.field public static final a_label_have_a_try_for_two_time:I = 0x7f130163

.field public static final a_label_have_selected:I = 0x7f130164

.field public static final a_label_hour:I = 0x7f130165

.field public static final a_label_house_property:I = 0x7f130166

.field public static final a_label_huawei_full_version:I = 0x7f130167

.field public static final a_label_identify_doing:I = 0x7f130168

.field public static final a_label_import_from_gallery:I = 0x7f130169

.field public static final a_label_import_pdf:I = 0x7f13016a

.field public static final a_label_internal_storage_available:I = 0x7f13016b

.field public static final a_label_invit_link:I = 0x7f13016c

.field public static final a_label_invite_code:I = 0x7f13016d

.field public static final a_label_invite_join_desc:I = 0x7f13016e

.field public static final a_label_language:I = 0x7f13016f

.field public static final a_label_lasso:I = 0x7f130170

.field public static final a_label_like_facebook:I = 0x7f130171

.field public static final a_label_login_now:I = 0x7f130172

.field public static final a_label_mail_login:I = 0x7f130173

.field public static final a_label_mail_to_me:I = 0x7f130174

.field public static final a_label_main_left_sign_in:I = 0x7f130175

.field public static final a_label_manul_sort:I = 0x7f130176

.field public static final a_label_member_title:I = 0x7f130177

.field public static final a_label_menu_doc_show_order:I = 0x7f130178

.field public static final a_label_mimute:I = 0x7f130179

.field public static final a_label_mode_translate:I = 0x7f13017a

.field public static final a_label_modify_offline_psw_tips:I = 0x7f13017b

.field public static final a_label_month:I = 0x7f13017c

.field public static final a_label_new_psw:I = 0x7f13017d

.field public static final a_label_next_time:I = 0x7f13017e

.field public static final a_label_no_cache:I = 0x7f13017f

.field public static final a_label_no_cost_after_experience:I = 0x7f130180

.field public static final a_label_no_duty_explain:I = 0x7f130181

.field public static final a_label_no_local_cache:I = 0x7f130182

.field public static final a_label_not_available:I = 0x7f130183

.field public static final a_label_ocr_result:I = 0x7f130184

.field public static final a_label_ocr_result_and_note:I = 0x7f130185

.field public static final a_label_ocr_txt_export:I = 0x7f130186

.field public static final a_label_off:I = 0x7f130187

.field public static final a_label_offline_folder_dialog_content:I = 0x7f130188

.field public static final a_label_offline_folder_pre_account_1:I = 0x7f130189

.field public static final a_label_offline_folder_pre_account_2:I = 0x7f13018a

.field public static final a_label_offline_folder_psw_modify:I = 0x7f13018b

.field public static final a_label_offline_folder_psw_summary:I = 0x7f13018c

.field public static final a_label_offline_folder_psw_title:I = 0x7f13018d

.field public static final a_label_once_sevenday_vip:I = 0x7f13018e

.field public static final a_label_one_year_desc:I = 0x7f13018f

.field public static final a_label_only_one_second:I = 0x7f130190

.field public static final a_label_other_account_login:I = 0x7f130191

.field public static final a_label_other_tag:I = 0x7f130192

.field public static final a_label_out_storage_card:I = 0x7f130193

.field public static final a_label_page_add_by_collaborator:I = 0x7f130194

.field public static final a_label_page_rename:I = 0x7f130195

.field public static final a_label_paper_inch:I = 0x7f130196

.field public static final a_label_pdf_watermark:I = 0x7f130197

.field public static final a_label_phone_login:I = 0x7f130198

.field public static final a_label_points_expire_content:I = 0x7f130199

.field public static final a_label_premium_description:I = 0x7f13019a

.field public static final a_label_premium_free_trial:I = 0x7f13019b

.field public static final a_label_processing:I = 0x7f13019c

.field public static final a_label_purchase_now:I = 0x7f13019d

.field public static final a_label_purchase_renewal:I = 0x7f13019e

.field public static final a_label_purchase_rmb:I = 0x7f13019f

.field public static final a_label_purchase_type_bill:I = 0x7f1301a0

.field public static final a_label_qrcode_tips:I = 0x7f1301a1

.field public static final a_label_quick_register:I = 0x7f1301a2

.field public static final a_label_read_more:I = 0x7f1301a3

.field public static final a_label_read_more_function_desc:I = 0x7f1301a4

.field public static final a_label_recognition_literacy:I = 0x7f1301a5

.field public static final a_label_recognition_mode_local:I = 0x7f1301a6

.field public static final a_label_redeem_desc:I = 0x7f1301a7

.field public static final a_label_redeem_title:I = 0x7f1301a8

.field public static final a_label_reget_verifycode:I = 0x7f1301a9

.field public static final a_label_reject:I = 0x7f1301aa

.field public static final a_label_remind_net_error:I = 0x7f1301ab

.field public static final a_label_remove_ad:I = 0x7f1301ac

.field public static final a_label_rewarded_video_tips_available:I = 0x7f1301ad

.field public static final a_label_rewarded_video_tips_down:I = 0x7f1301ae

.field public static final a_label_rewarded_video_tips_up:I = 0x7f1301af

.field public static final a_label_scanning:I = 0x7f1301b0

.field public static final a_label_search:I = 0x7f1301b1

.field public static final a_label_search_result_in_folder:I = 0x7f1301b2

.field public static final a_label_seconds:I = 0x7f1301b3

.field public static final a_label_select_all:I = 0x7f1301b4

.field public static final a_label_select_from_gallery:I = 0x7f1301b5

.field public static final a_label_select_position:I = 0x7f1301b6

.field public static final a_label_select_size:I = 0x7f1301b7

.field public static final a_label_select_size_medium:I = 0x7f1301b8

.field public static final a_label_select_size_ori:I = 0x7f1301b9

.field public static final a_label_select_size_small:I = 0x7f1301ba

.field public static final a_label_send_to:I = 0x7f1301bb

.field public static final a_label_send_to_we_chat_guys:I = 0x7f1301bc

.field public static final a_label_set_offline_psw_success:I = 0x7f1301bd

.field public static final a_label_setting_about_and_feedback:I = 0x7f1301be

.field public static final a_label_setting_help:I = 0x7f1301bf

.field public static final a_label_setting_show_doc_details:I = 0x7f1301c0

.field public static final a_label_setting_tip_offline_folder:I = 0x7f1301c1

.field public static final a_label_sevenday_hint:I = 0x7f1301c2

.field public static final a_label_share:I = 0x7f1301c3

.field public static final a_label_share_by_email:I = 0x7f1301c4

.field public static final a_label_share_by_sms:I = 0x7f1301c5

.field public static final a_label_share_file_secure_link:I = 0x7f1301c6

.field public static final a_label_share_jpg_file:I = 0x7f1301c7

.field public static final a_label_share_pdf_file:I = 0x7f1301c8

.field public static final a_label_share_pdf_link:I = 0x7f1301c9

.field public static final a_label_share_to_fb:I = 0x7f1301ca

.field public static final a_label_share_to_googleplus:I = 0x7f1301cb

.field public static final a_label_share_to_twitter:I = 0x7f1301cc

.field public static final a_label_share_to_weibo:I = 0x7f1301cd

.field public static final a_label_share_txt_file:I = 0x7f1301ce

.field public static final a_label_share_weixin_circle:I = 0x7f1301cf

.field public static final a_label_sharesecurelink_setting:I = 0x7f1301d0

.field public static final a_label_signature_title:I = 0x7f1301d1

.field public static final a_label_sms:I = 0x7f1301d2

.field public static final a_label_sorry:I = 0x7f1301d3

.field public static final a_label_sort_creation_n2o:I = 0x7f1301d4

.field public static final a_label_sort_creation_o2n:I = 0x7f1301d5

.field public static final a_label_sort_modified_n2o:I = 0x7f1301d6

.field public static final a_label_sort_modified_o2n:I = 0x7f1301d7

.field public static final a_label_special_offer:I = 0x7f1301d8

.field public static final a_label_start_immediately:I = 0x7f1301d9

.field public static final a_label_support_sixty_language:I = 0x7f1301da

.field public static final a_label_sync_complete:I = 0x7f1301db

.field public static final a_label_syncing:I = 0x7f1301dc

.field public static final a_label_tag_bar_lock:I = 0x7f1301dd

.field public static final a_label_tag_input:I = 0x7f1301de

.field public static final a_label_tag_setting_donnot_save:I = 0x7f1301df

.field public static final a_label_template_settings:I = 0x7f1301e0

.field public static final a_label_template_settings_tips:I = 0x7f1301e1

.field public static final a_label_text_direction_detection:I = 0x7f1301e2

.field public static final a_label_third_service:I = 0x7f1301e3

.field public static final a_label_tip_copy_out_of_offline:I = 0x7f1301e4

.field public static final a_label_tip_copying_synced_document_into_offline_folder:I = 0x7f1301e5

.field public static final a_label_tip_moving_collaborate_document_into_offline_folder:I = 0x7f1301e6

.field public static final a_label_tip_moving_out_of_offline:I = 0x7f1301e7

.field public static final a_label_tips_doc_long_press:I = 0x7f1301e8

.field public static final a_label_tips_doc_tablet_manual_sort:I = 0x7f1301e9

.field public static final a_label_tips_topic_edit:I = 0x7f1301ea

.field public static final a_label_tips_when_close_offline_folder:I = 0x7f1301eb

.field public static final a_label_title_eraser:I = 0x7f1301ec

.field public static final a_label_title_offline_folder:I = 0x7f1301ed

.field public static final a_label_title_undo:I = 0x7f1301ee

.field public static final a_label_try_count_out:I = 0x7f1301ef

.field public static final a_label_undifined:I = 0x7f1301f0

.field public static final a_label_upgrade_1:I = 0x7f1301f1

.field public static final a_label_upgrade_to_premium:I = 0x7f1301f2

.field public static final a_label_upload_fail:I = 0x7f1301f3

.field public static final a_label_use_instructions:I = 0x7f1301f4

.field public static final a_label_use_voucher_to_purchase_other:I = 0x7f1301f5

.field public static final a_label_verify_new_psw:I = 0x7f1301f6

.field public static final a_label_vip_10_floder:I = 0x7f1301f7

.field public static final a_label_vip_11_link:I = 0x7f1301f8

.field public static final a_label_vip_12_nosync:I = 0x7f1301f9

.field public static final a_label_vip_13_upload:I = 0x7f1301fa

.field public static final a_label_vip_14_pdf:I = 0x7f1301fb

.field public static final a_label_vip_16_sharing:I = 0x7f1301fc

.field public static final a_label_vip_1_idcard:I = 0x7f1301fd

.field public static final a_label_vip_2_watermark:I = 0x7f1301fe

.field public static final a_label_vip_3_ocrexport:I = 0x7f1301ff

.field public static final a_label_vip_4_puzzle:I = 0x7f130200

.field public static final a_label_vip_5_ocr:I = 0x7f130201

.field public static final a_label_vip_6_ad:I = 0x7f130202

.field public static final a_label_vip_7_translation:I = 0x7f130203

.field public static final a_label_vip_8_cloudocr:I = 0x7f130204

.field public static final a_label_vip_9_10g:I = 0x7f130205

.field public static final a_label_vip_for_greetcard:I = 0x7f130206

.field public static final a_label_vip_for_icon:I = 0x7f130207

.field public static final a_label_vip_for_topic:I = 0x7f130208

.field public static final a_label_vip_function_guid:I = 0x7f130209

.field public static final a_label_vip_purchase_dialog_offline:I = 0x7f13020a

.field public static final a_label_wechat_circle:I = 0x7f13020b

.field public static final a_label_wechat_friend:I = 0x7f13020c

.field public static final a_label_year:I = 0x7f13020d

.field public static final a_lable_collaborator_unavailable:I = 0x7f13020e

.field public static final a_like_msg_introduce_share_reward:I = 0x7f13020f

.field public static final a_like_title_congratulations:I = 0x7f130210

.field public static final a_mag_ocr_and_translate:I = 0x7f130211

.field public static final a_main_doc_confirm_delete_msg:I = 0x7f130212

.field public static final a_main_lable_select_sort_mode:I = 0x7f130213

.field public static final a_main_merge_keep_old:I = 0x7f130214

.field public static final a_main_merge_method:I = 0x7f130215

.field public static final a_main_merge_no_keep_old:I = 0x7f130216

.field public static final a_main_move_docs_out:I = 0x7f130217

.field public static final a_main_search_no_data:I = 0x7f130218

.field public static final a_max_signature_count:I = 0x7f13021b

.field public static final a_max_signature_style:I = 0x7f13021c

.field public static final a_menu_add_signature:I = 0x7f13021d

.field public static final a_menu_create_folder:I = 0x7f13021e

.field public static final a_menu_create_team_folder:I = 0x7f13021f

.field public static final a_menu_delete_signature:I = 0x7f130220

.field public static final a_menu_e_evidence:I = 0x7f130221

.field public static final a_menu_e_evidence_mode:I = 0x7f130222

.field public static final a_menu_import_images:I = 0x7f130223

.field public static final a_menu_pdf_orientation_auto:I = 0x7f130224

.field public static final a_menu_pdf_orientation_land:I = 0x7f130225

.field public static final a_menu_pdf_orientation_port:I = 0x7f130226

.field public static final a_menu_select:I = 0x7f130227

.field public static final a_menu_show_more:I = 0x7f130228

.field public static final a_menu_sort_way:I = 0x7f130229

.field public static final a_menu_title_send:I = 0x7f13022a

.field public static final a_menu_title_share_collaborate_page_uploadfax:I = 0x7f13022b

.field public static final a_menu_verify:I = 0x7f13022c

.field public static final a_menu_view_member:I = 0x7f13022d

.field public static final a_message_open_sync_first:I = 0x7f13022e

.field public static final a_message_sync_first:I = 0x7f13022f

.field public static final a_msg_account_already_bound:I = 0x7f130230

.field public static final a_msg_account_not_activate:I = 0x7f130231

.field public static final a_msg_activation_code_empty:I = 0x7f130232

.field public static final a_msg_activation_fail:I = 0x7f130233

.field public static final a_msg_activite_to_pay_version_success:I = 0x7f130234

.field public static final a_msg_add_comment:I = 0x7f130235

.field public static final a_msg_alipay_uninstall_prompt:I = 0x7f130236

.field public static final a_msg_already_register_by_camcard:I = 0x7f130237

.field public static final a_msg_already_register_by_camscanner:I = 0x7f130238

.field public static final a_msg_application_storage_low:I = 0x7f130239

.field public static final a_msg_auto_sync_in_mobile_net:I = 0x7f13023a

.field public static final a_msg_autoupload_account_change:I = 0x7f13023b

.field public static final a_msg_autoupload_exituploadaccount:I = 0x7f13023c

.field public static final a_msg_autoupload_full_storgae_error:I = 0x7f13023d

.field public static final a_msg_autoupload_login_unknown_error:I = 0x7f13023e

.field public static final a_msg_autoupload_openaccount:I = 0x7f13023f

.field public static final a_msg_autoupload_webstorage_error:I = 0x7f130240

.field public static final a_msg_been_save_failed:I = 0x7f130241

.field public static final a_msg_buy_fax_success:I = 0x7f130242

.field public static final a_msg_buy_points_3000_success_unlogin:I = 0x7f130243

.field public static final a_msg_buy_vip_fail:I = 0x7f130244

.field public static final a_msg_buy_vip_success:I = 0x7f130245

.field public static final a_msg_buy_vip_success_unlogin:I = 0x7f130246

.field public static final a_msg_cache_clean_guide:I = 0x7f130247

.field public static final a_msg_camscanner_vip_product:I = 0x7f130248

.field public static final a_msg_capture_detected_idcard_2_a4_page:I = 0x7f130249

.field public static final a_msg_change_account:I = 0x7f13024a

.field public static final a_msg_change_mail_to_me:I = 0x7f13024b

.field public static final a_msg_check_parameter_not_acceptable:I = 0x7f13024c

.field public static final a_msg_checking_account:I = 0x7f13024d

.field public static final a_msg_clean_tips:I = 0x7f13024e

.field public static final a_msg_cloud_ocr_fail_tips:I = 0x7f13024f

.field public static final a_msg_comment_mark:I = 0x7f130250

.field public static final a_msg_comment_satisfy:I = 0x7f130251

.field public static final a_msg_comment_satisfy_tip:I = 0x7f130252

.field public static final a_msg_composite_document_create_fail:I = 0x7f130253

.field public static final a_msg_composite_function:I = 0x7f130254

.field public static final a_msg_composite_processing:I = 0x7f130255

.field public static final a_msg_confirm_changeto_external_storage:I = 0x7f130256

.field public static final a_msg_confirm_doc_password:I = 0x7f130257

.field public static final a_msg_confirm_email:I = 0x7f130258

.field public static final a_msg_copy_to_clipboard:I = 0x7f130259

.field public static final a_msg_copy_url_success:I = 0x7f13025a

.field public static final a_msg_creat_txt:I = 0x7f13025b

.field public static final a_msg_db_restore_need_upgrade_app:I = 0x7f13025c

.field public static final a_msg_db_upgrading:I = 0x7f13025d

.field public static final a_msg_deep_clean_warnning:I = 0x7f13025e

.field public static final a_msg_delete_copperation_docs_contain_unsync:I = 0x7f13025f

.field public static final a_msg_delete_copperation_docs_synced:I = 0x7f130260

.field public static final a_msg_delete_data_upgrade_hint:I = 0x7f130261

.field public static final a_msg_delete_docs_contain_unsync:I = 0x7f130262

.field public static final a_msg_delete_docs_synced:I = 0x7f130263

.field public static final a_msg_delete_folder_contain_unsync:I = 0x7f130264

.field public static final a_msg_delete_folder_synced:I = 0x7f130265

.field public static final a_msg_delete_page_unsynced:I = 0x7f130266

.field public static final a_msg_dir_max_limit:I = 0x7f130267

.field public static final a_msg_dlg_pdf_import_hint:I = 0x7f130268

.field public static final a_msg_doc_add:I = 0x7f130269

.field public static final a_msg_doc_comment_add:I = 0x7f13026a

.field public static final a_msg_doc_delete:I = 0x7f13026b

.field public static final a_msg_doc_like:I = 0x7f13026c

.field public static final a_msg_doc_lost_picture:I = 0x7f13026d

.field public static final a_msg_doc_title_invalid_empty:I = 0x7f13026e

.field public static final a_msg_doc_update:I = 0x7f13026f

.field public static final a_msg_document_add_page_guide:I = 0x7f130270

.field public static final a_msg_document_finish_doc:I = 0x7f130271

.field public static final a_msg_doing_cloud_ocr:I = 0x7f130272

.field public static final a_msg_download_image_data_need_login:I = 0x7f130273

.field public static final a_msg_downloading_image_data:I = 0x7f130274

.field public static final a_msg_downloading_jpg:I = 0x7f130275

.field public static final a_msg_drop_cur_image:I = 0x7f130276

.field public static final a_msg_e_evidence_auth_ing:I = 0x7f130277

.field public static final a_msg_e_evidence_duty_explain_msg:I = 0x7f130278

.field public static final a_msg_e_evidence_explain:I = 0x7f130279

.field public static final a_msg_e_evidence_explain_vivo_market:I = 0x7f13027a

.field public static final a_msg_e_evidence_gps:I = 0x7f13027b

.field public static final a_msg_e_evidence_known_usage:I = 0x7f13027c

.field public static final a_msg_e_evidence_known_usage_1:I = 0x7f13027d

.field public static final a_msg_e_evidence_known_usage_2:I = 0x7f13027e

.field public static final a_msg_e_evidence_known_usage_3:I = 0x7f13027f

.field public static final a_msg_e_evidence_known_usage_4:I = 0x7f130280

.field public static final a_msg_e_evidence_not_login_open_gps:I = 0x7f130281

.field public static final a_msg_e_evidence_not_login_setting_page_see:I = 0x7f130282

.field public static final a_msg_e_evidence_not_try_any_more:I = 0x7f130283

.field public static final a_msg_e_evidence_over_max_mb_size_upload_prompt:I = 0x7f130284

.field public static final a_msg_e_evidence_over_max_upload_prompt:I = 0x7f130285

.field public static final a_msg_e_evidence_retry_upload_prompt:I = 0x7f130286

.field public static final a_msg_e_evidence_upload_back_after_pay:I = 0x7f130287

.field public static final a_msg_e_evidence_upload_back_before_pay:I = 0x7f130288

.field public static final a_msg_e_evidence_upload_fail:I = 0x7f130289

.field public static final a_msg_e_evidence_upload_just_for_auth_suc:I = 0x7f13028a

.field public static final a_msg_e_evidence_upload_right_now:I = 0x7f13028b

.field public static final a_msg_e_evidence_uploading:I = 0x7f13028c

.field public static final a_msg_edit_without_raw_image:I = 0x7f13028d

.field public static final a_msg_err_cannot_connect_to_camera:I = 0x7f13028e

.field public static final a_msg_err_need_syncing_complete:I = 0x7f13028f

.field public static final a_msg_err_no_back_camera:I = 0x7f130290

.field public static final a_msg_err_not_complete_doc:I = 0x7f130291

.field public static final a_msg_err_not_complete_image:I = 0x7f130292

.field public static final a_msg_error_area_code:I = 0x7f130293

.field public static final a_msg_error_assist_when_not_login:I = 0x7f130294

.field public static final a_msg_error_collaborate_when_not_login:I = 0x7f130295

.field public static final a_msg_error_empty_comment:I = 0x7f130296

.field public static final a_msg_error_nick_name_illegal:I = 0x7f130297

.field public static final a_msg_error_no_sns_enable:I = 0x7f130298

.field public static final a_msg_error_send_empty:I = 0x7f130299

.field public static final a_msg_exit_certificate_composite:I = 0x7f13029a

.field public static final a_msg_exit_cs:I = 0x7f13029b

.field public static final a_msg_exit_ppt_preview:I = 0x7f13029c

.field public static final a_msg_fail_create_link:I = 0x7f13029d

.field public static final a_msg_fax_if_go_to_register:I = 0x7f13029e

.field public static final a_msg_fax_send_no_answer:I = 0x7f13029f

.field public static final a_msg_fax_send_not_receive:I = 0x7f1302a0

.field public static final a_msg_fax_send_wrong_number:I = 0x7f1302a1

.field public static final a_msg_fax_storgae_error:I = 0x7f1302a2

.field public static final a_msg_fax_transfer_success:I = 0x7f1302a3

.field public static final a_msg_feedback_content_hint:I = 0x7f1302a4

.field public static final a_msg_feedback_ok:I = 0x7f1302a5

.field public static final a_msg_feedback_send_failed:I = 0x7f1302a6

.field public static final a_msg_folder_be_delete:I = 0x7f1302a7

.field public static final a_msg_folder_been_delete_hint:I = 0x7f1302a8

.field public static final a_msg_folder_title_invalid_empty:I = 0x7f1302a9

.field public static final a_msg_for_sync_in_mobile_network:I = 0x7f1302aa

.field public static final a_msg_forced_logout:I = 0x7f1302ab

.field public static final a_msg_get_quality_picture_with_net:I = 0x7f1302ac

.field public static final a_msg_get_unlimit_folder:I = 0x7f1302ad

.field public static final a_msg_global_no_syscamera:I = 0x7f1302ae

.field public static final a_msg_goto_discuss:I = 0x7f1302af

.field public static final a_msg_guid_fix_bug:I = 0x7f1302b0

.field public static final a_msg_hint_account_login_abnormal:I = 0x7f1302b1

.field public static final a_msg_hint_oporate_limit:I = 0x7f1302b2

.field public static final a_msg_human_translate_chinese:I = 0x7f1302b3

.field public static final a_msg_idcard_check_statement_part_1:I = 0x7f1302b4

.field public static final a_msg_if_save_watermark:I = 0x7f1302b5

.field public static final a_msg_image_format_error:I = 0x7f1302b6

.field public static final a_msg_image_open_api_progress:I = 0x7f1302b7

.field public static final a_msg_import_pdf_encrypt:I = 0x7f1302b8

.field public static final a_msg_import_pdf_exceed_limit_file:I = 0x7f1302b9

.field public static final a_msg_import_pdf_exceed_limit_files:I = 0x7f1302ba

.field public static final a_msg_import_pdf_exceed_limit_noname:I = 0x7f1302bb

.field public static final a_msg_import_pdf_fail:I = 0x7f1302bc

.field public static final a_msg_import_pdf_failed:I = 0x7f1302bd

.field public static final a_msg_import_pdf_illegal_file:I = 0x7f1302be

.field public static final a_msg_import_pdf_illegal_file_noname:I = 0x7f1302bf

.field public static final a_msg_import_pdf_illegal_files:I = 0x7f1302c0

.field public static final a_msg_import_pdf_max_size:I = 0x7f1302c1

.field public static final a_msg_import_pdf_need_login:I = 0x7f1302c2

.field public static final a_msg_import_pdf_retry_need_login:I = 0x7f1302c3

.field public static final a_msg_ink_and_ocr_tip:I = 0x7f1302c4

.field public static final a_msg_input_activation_code:I = 0x7f1302c5

.field public static final a_msg_input_doc_password:I = 0x7f1302c6

.field public static final a_msg_input_word_num_exceed_limit:I = 0x7f1302c7

.field public static final a_msg_insert_backup_data_to_camscanner:I = 0x7f1302c8

.field public static final a_msg_internal_storage_full:I = 0x7f1302c9

.field public static final a_msg_joined_team:I = 0x7f1302ca

.field public static final a_msg_last_collaborate_time:I = 0x7f1302cb

.field public static final a_msg_last_sync_time:I = 0x7f1302cc

.field public static final a_msg_left_ink_times:I = 0x7f1302cd

.field public static final a_msg_login_account:I = 0x7f1302ce

.field public static final a_msg_login_cs_account:I = 0x7f1302cf

.field public static final a_msg_login_info_error:I = 0x7f1302d0

.field public static final a_msg_login_information_expired:I = 0x7f1302d1

.field public static final a_msg_login_out_clean_data_ok:I = 0x7f1302d2

.field public static final a_msg_login_out_confirm_warm_message:I = 0x7f1302d3

.field public static final a_msg_login_to_download_jpg:I = 0x7f1302d4

.field public static final a_msg_login_too_many_client:I = 0x7f1302d5

.field public static final a_msg_logined_user2open_sync:I = 0x7f1302d6

.field public static final a_msg_logout_keep_document:I = 0x7f1302d7

.field public static final a_msg_logout_normal:I = 0x7f1302d8

.field public static final a_msg_logout_when_has_faxbalance:I = 0x7f1302d9

.field public static final a_msg_logout_when_pdf_uploading:I = 0x7f1302da

.field public static final a_msg_logout_when_syncing:I = 0x7f1302db

.field public static final a_msg_long_click_appstar:I = 0x7f1302dc

.field public static final a_msg_long_click_auto_trim_zone:I = 0x7f1302dd

.field public static final a_msg_long_click_camera_flash:I = 0x7f1302de

.field public static final a_msg_long_click_camera_grid_switch:I = 0x7f1302df

.field public static final a_msg_long_click_camera_rotate:I = 0x7f1302e0

.field public static final a_msg_long_click_camera_size:I = 0x7f1302e1

.field public static final a_msg_long_click_camera_sound:I = 0x7f1302e2

.field public static final a_msg_long_click_camera_sprit_switch:I = 0x7f1302e3

.field public static final a_msg_long_click_delete:I = 0x7f1302e4

.field public static final a_msg_long_click_gallery:I = 0x7f1302e5

.field public static final a_msg_long_click_lock1:I = 0x7f1302e6

.field public static final a_msg_long_click_merge:I = 0x7f1302e7

.field public static final a_msg_long_click_no_trim:I = 0x7f1302e8

.field public static final a_msg_long_click_save:I = 0x7f1302e9

.field public static final a_msg_long_click_unlock:I = 0x7f1302ea

.field public static final a_msg_low_storage:I = 0x7f1302eb

.field public static final a_msg_low_storage_clean_cache:I = 0x7f1302ec

.field public static final a_msg_low_storage_fail_save:I = 0x7f1302ed

.field public static final a_msg_manager:I = 0x7f1302ee

.field public static final a_msg_manager_desc:I = 0x7f1302ef

.field public static final a_msg_merge_docs_err:I = 0x7f1302f0

.field public static final a_msg_month_vip_pay_fail:I = 0x7f1302f1

.field public static final a_msg_msgcenter_doc_delete:I = 0x7f1302f2

.field public static final a_msg_multi_capture_hint:I = 0x7f1302f3

.field public static final a_msg_multidocs_lost_picture:I = 0x7f1302f4

.field public static final a_msg_net_error_notification:I = 0x7f1302f5

.field public static final a_msg_new_get_sevenday_try_success:I = 0x7f1302f6

.field public static final a_msg_new_register_guide:I = 0x7f1302f7

.field public static final a_msg_no_back_camera_go_gallery:I = 0x7f1302f8

.field public static final a_msg_no_email_app_installed:I = 0x7f1302f9

.field public static final a_msg_no_network:I = 0x7f1302fa

.field public static final a_msg_no_third_share_app:I = 0x7f1302fb

.field public static final a_msg_normal_download_failed:I = 0x7f1302fc

.field public static final a_msg_not_logout:I = 0x7f1302fd

.field public static final a_msg_not_support_purchase:I = 0x7f1302fe

.field public static final a_msg_note_do_other_mission:I = 0x7f1302ff

.field public static final a_msg_note_login:I = 0x7f130300

.field public static final a_msg_note_recommend_facebook_success:I = 0x7f130301

.field public static final a_msg_ocr_detail:I = 0x7f130302

.field public static final a_msg_ocr_failed:I = 0x7f130303

.field public static final a_msg_ocr_producess:I = 0x7f130304

.field public static final a_msg_ocr_result_export:I = 0x7f130305

.field public static final a_msg_ocr_user_cant_see:I = 0x7f130306

.field public static final a_msg_only_addition:I = 0x7f130307

.field public static final a_msg_only_addition_desc:I = 0x7f130308

.field public static final a_msg_only_read:I = 0x7f130309

.field public static final a_msg_only_read_desc:I = 0x7f13030a

.field public static final a_msg_op_need_download_first:I = 0x7f13030b

.field public static final a_msg_op_need_image_data:I = 0x7f13030c

.field public static final a_msg_op_to_clear_ocruser:I = 0x7f13030d

.field public static final a_msg_over_verify_times:I = 0x7f13030e

.field public static final a_msg_page_be_deleted:I = 0x7f13030f

.field public static final a_msg_page_not_exist:I = 0x7f130310

.field public static final a_msg_page_number:I = 0x7f130311

.field public static final a_msg_pay_fail_need_check:I = 0x7f130312

.field public static final a_msg_pdf_default_setting_tips:I = 0x7f130313

.field public static final a_msg_pdf_file_missing:I = 0x7f130314

.field public static final a_msg_permission_camera:I = 0x7f130315

.field public static final a_msg_permission_location:I = 0x7f130316

.field public static final a_msg_permission_not_granted:I = 0x7f130317

.field public static final a_msg_permission_storage:I = 0x7f130318

.field public static final a_msg_permission_telephone:I = 0x7f130319

.field public static final a_msg_picture_are_lost:I = 0x7f13031b

.field public static final a_msg_picture_to_pdf:I = 0x7f13031c

.field public static final a_msg_pirate_app_prompt:I = 0x7f13031d

.field public static final a_msg_please_input_email_address:I = 0x7f13031e

.field public static final a_msg_points_number:I = 0x7f13031f

.field public static final a_msg_premium_noad:I = 0x7f130320

.field public static final a_msg_premium_ocr:I = 0x7f130321

.field public static final a_msg_premium_watermark:I = 0x7f130322

.field public static final a_msg_prepare_ocr:I = 0x7f130323

.field public static final a_msg_produce_failed:I = 0x7f130324

.field public static final a_msg_pull_to_refresh_release:I = 0x7f130325

.field public static final a_msg_purchase_premium_premotion:I = 0x7f130326

.field public static final a_msg_purchase_vip_desc:I = 0x7f130327

.field public static final a_msg_pwd_contain_blank:I = 0x7f130328

.field public static final a_msg_qr_login_need_login_first:I = 0x7f130329

.field public static final a_msg_qr_login_need_network:I = 0x7f13032a

.field public static final a_msg_qr_login_success:I = 0x7f13032b

.field public static final a_msg_qr_web_login_scann_fail:I = 0x7f13032c

.field public static final a_msg_raw_image_too_large:I = 0x7f13032d

.field public static final a_msg_raw_image_unavailable:I = 0x7f13032e

.field public static final a_msg_read_and_addition:I = 0x7f13032f

.field public static final a_msg_read_and_addition_desc:I = 0x7f130330

.field public static final a_msg_refer_to_earn_old_user_tip:I = 0x7f130331

.field public static final a_msg_register_guide_for_special_country:I = 0x7f130332

.field public static final a_msg_register_to_gallery_fail:I = 0x7f130333

.field public static final a_msg_register_to_gallery_success:I = 0x7f130334

.field public static final a_msg_reinstall_app:I = 0x7f130335

.field public static final a_msg_remove_collaborator_fail:I = 0x7f130336

.field public static final a_msg_replace_backup_data_to_camscanner:I = 0x7f130337

.field public static final a_msg_replace_or_insert:I = 0x7f130338

.field public static final a_msg_save_topic:I = 0x7f130339

.field public static final a_msg_send_title:I = 0x7f13033a

.field public static final a_msg_send_to_wechat_error:I = 0x7f13033b

.field public static final a_msg_set_license_hide_hint:I = 0x7f13033c

.field public static final a_msg_set_permission:I = 0x7f13033d

.field public static final a_msg_share_save_to_gallery:I = 0x7f13033e

.field public static final a_msg_share_sns:I = 0x7f13033f

.field public static final a_msg_share_to_facebook:I = 0x7f130340

.field public static final a_msg_share_to_otherapp:I = 0x7f130341

.field public static final a_msg_share_to_whatsapp:I = 0x7f130343

.field public static final a_msg_share_to_whatsapp_and_facebook:I = 0x7f130344

.field public static final a_msg_sharesecurelink_deadline:I = 0x7f130345

.field public static final a_msg_sign_get_space:I = 0x7f130346

.field public static final a_msg_sign_get_year_vip:I = 0x7f130347

.field public static final a_msg_sign_today_finished:I = 0x7f130348

.field public static final a_msg_sns_share_fail:I = 0x7f130349

.field public static final a_msg_sns_share_success:I = 0x7f13034a

.field public static final a_msg_storage_data_losed:I = 0x7f13034c

.field public static final a_msg_storage_limit_message:I = 0x7f13034d

.field public static final a_msg_storage_limit_notification:I = 0x7f13034e

.field public static final a_msg_storage_limit_notification_for_vip:I = 0x7f13034f

.field public static final a_msg_storage_limit_title:I = 0x7f130350

.field public static final a_msg_storage_pulled_out:I = 0x7f130351

.field public static final a_msg_storage_pulled_out_no_page:I = 0x7f130352

.field public static final a_msg_storage_pulled_out_when_work_internal:I = 0x7f130353

.field public static final a_msg_submit_fax_hint:I = 0x7f130354

.field public static final a_msg_successive_sign:I = 0x7f130355

.field public static final a_msg_summary_camscanner_account:I = 0x7f130356

.field public static final a_msg_sync_setting_for_deep_clean:I = 0x7f130357

.field public static final a_msg_tap_to_fullscreen_mode:I = 0x7f130358

.field public static final a_msg_team_login:I = 0x7f130359

.field public static final a_msg_team_members_limit:I = 0x7f13035a

.field public static final a_msg_team_not_exit:I = 0x7f13035b

.field public static final a_msg_ten_G_cloud_space:I = 0x7f13035c

.field public static final a_msg_throw_points_get_storage:I = 0x7f13035d

.field public static final a_msg_tip_set_nick_name_exp:I = 0x7f13035e

.field public static final a_msg_tip_set_nick_name_fail:I = 0x7f13035f

.field public static final a_msg_tips_blackwhite_mode:I = 0x7f130360

.field public static final a_msg_tips_change_mode:I = 0x7f130361

.field public static final a_msg_tips_for_multichoice:I = 0x7f130362

.field public static final a_msg_tips_gray_mode:I = 0x7f130363

.field public static final a_msg_tips_magic_mode:I = 0x7f130364

.field public static final a_msg_tips_set_ocr_language:I = 0x7f130365

.field public static final a_msg_title_back_up_cancel:I = 0x7f130366

.field public static final a_msg_title_back_up_doing:I = 0x7f130367

.field public static final a_msg_title_back_up_net_work_not_available:I = 0x7f130368

.field public static final a_msg_title_back_up_remind_open_sync:I = 0x7f130369

.field public static final a_msg_title_back_up_sync_now:I = 0x7f13036a

.field public static final a_msg_title_back_up_undercarriage:I = 0x7f13036b

.field public static final a_msg_title_back_up_use_cloud_sync:I = 0x7f13036c

.field public static final a_msg_title_back_up_use_mobile_net:I = 0x7f13036d

.field public static final a_msg_title_camscanner_account:I = 0x7f13036e

.field public static final a_msg_title_to_senior_account:I = 0x7f13036f

.field public static final a_msg_translate:I = 0x7f130370

.field public static final a_msg_translate_summary:I = 0x7f130372

.field public static final a_msg_under_mobile_network:I = 0x7f130373

.field public static final a_msg_upgrade_db:I = 0x7f130374

.field public static final a_msg_upgrade_to_full_version:I = 0x7f130375

.field public static final a_msg_upgrade_vip_fail:I = 0x7f130376

.field public static final a_msg_upgrade_vip_success:I = 0x7f130377

.field public static final a_msg_upload_pdf_doc_fail:I = 0x7f130378

.field public static final a_msg_vip_auto_upload:I = 0x7f130379

.field public static final a_msg_vip_bind_account:I = 0x7f13037a

.field public static final a_msg_vip_composite:I = 0x7f13037b

.field public static final a_msg_vip_download_failed:I = 0x7f13037c

.field public static final a_msg_vip_higth_quality_picture:I = 0x7f13037d

.field public static final a_msg_vip_pdf_no_ads:I = 0x7f13037e

.field public static final a_msg_vip_share_txt:I = 0x7f13037f

.field public static final a_msg_vip_subscription_month:I = 0x7f130380

.field public static final a_msg_vip_subscription_year:I = 0x7f130381

.field public static final a_msg_we_chat_uninstall_prompt:I = 0x7f130382

.field public static final a_msg_weixi_activity_tips:I = 0x7f130383

.field public static final a_msg_year_vip_pay_fail:I = 0x7f130384

.field public static final a_no_page_selected:I = 0x7f130385

.field public static final a_one_fax_price:I = 0x7f130386

.field public static final a_per_point:I = 0x7f130387

.field public static final a_per_storage:I = 0x7f130388

.field public static final a_preview_guide_batch:I = 0x7f130389

.field public static final a_preview_guide_single:I = 0x7f13038a

.field public static final a_print_label_printers_info:I = 0x7f13038b

.field public static final a_print_msg_login_first:I = 0x7f13038c

.field public static final a_privacy_policy_agree:I = 0x7f13038d

.field public static final a_privacy_policy_disagree:I = 0x7f13038e

.field public static final a_purchase_desc_certificate:I = 0x7f13038f

.field public static final a_purchase_desc_cloud_limit:I = 0x7f130390

.field public static final a_purchase_desc_cloud_ocr:I = 0x7f130391

.field public static final a_purchase_desc_cloud_ocr_translate:I = 0x7f130392

.field public static final a_purchase_desc_ocr_check_edit:I = 0x7f130393

.field public static final a_purchase_desc_ocr_export:I = 0x7f130394

.field public static final a_purchase_desc_translate:I = 0x7f130395

.field public static final a_purchase_desc_upload:I = 0x7f130396

.field public static final a_purchase_desc_vip_10G_clound:I = 0x7f130397

.field public static final a_purchase_points:I = 0x7f130398

.field public static final a_restore_summary:I = 0x7f13039a

.field public static final a_return_url_hosts:I = 0x7f13039b

.field public static final a_scan_auto_mode:I = 0x7f13039c

.field public static final a_scan_bw_mode_new:I = 0x7f13039d

.field public static final a_scan_gray_mode:I = 0x7f13039e

.field public static final a_scan_highenhance_mode:I = 0x7f13039f

.field public static final a_scan_lowenhance_mode:I = 0x7f1303a0

.field public static final a_scan_noenhance_mode:I = 0x7f1303a1

.field public static final a_scan_wb_mode:I = 0x7f1303a2

.field public static final a_select_task_list:I = 0x7f1303a3

.field public static final a_send_select_multi_docs:I = 0x7f1303a4

.field public static final a_send_select_one_doc_info:I = 0x7f1303a5

.field public static final a_send_select_one_doc_size:I = 0x7f1303a6

.field public static final a_send_select_one_doc_title:I = 0x7f1303a7

.field public static final a_set_hint_input_new_password:I = 0x7f1303a8

.field public static final a_set_hint_input_new_password_again:I = 0x7f1303a9

.field public static final a_set_msg_clear_password:I = 0x7f1303aa

.field public static final a_set_msg_gallery:I = 0x7f1303ab

.field public static final a_set_msg_pdf_padding:I = 0x7f1303ac

.field public static final a_set_title_backup:I = 0x7f1303ad

.field public static final a_set_title_backup_restore:I = 0x7f1303ae

.field public static final a_set_title_clear_password:I = 0x7f1303af

.field public static final a_set_title_create_password:I = 0x7f1303b0

.field public static final a_set_title_pdf_padding:I = 0x7f1303b1

.field public static final a_set_title_restore:I = 0x7f1303b2

.field public static final a_setting_account_cloud_storage:I = 0x7f1303b3

.field public static final a_setting_auto_save_gallery:I = 0x7f1303b4

.field public static final a_setting_autoupload_description:I = 0x7f1303b5

.field public static final a_setting_category_security_document:I = 0x7f1303b6

.field public static final a_setting_document_manager:I = 0x7f1303b7

.field public static final a_setting_export_document:I = 0x7f1303b8

.field public static final a_setting_help_protocalandprivce:I = 0x7f1303b9

.field public static final a_setting_help_protocol:I = 0x7f1303ba

.field public static final a_setting_hightquality_scan:I = 0x7f1303bb

.field public static final a_setting_image_scan:I = 0x7f1303bc

.field public static final a_setting_msg_license:I = 0x7f1303bd

.field public static final a_setting_msg_update_pdf_comfirm:I = 0x7f1303be

.field public static final a_setting_msg_update_pdf_finish:I = 0x7f1303bf

.field public static final a_setting_register_protocol:I = 0x7f1303c1

.field public static final a_setting_security_protect:I = 0x7f1303c2

.field public static final a_setting_summary_document_manager:I = 0x7f1303c3

.field public static final a_setting_summary_export_pdf:I = 0x7f1303c4

.field public static final a_setting_summary_security_doc_password:I = 0x7f1303c5

.field public static final a_setting_title_export_pdf:I = 0x7f1303c6

.field public static final a_setting_title_license:I = 0x7f1303c7

.field public static final a_setting_title_update_pdf:I = 0x7f1303c8

.field public static final a_setting_upload_record:I = 0x7f1303ca

.field public static final a_share_image_miss:I = 0x7f1303cb

.field public static final a_sharesecurelink_limt_one_month:I = 0x7f1303cc

.field public static final a_sharesecurelink_limt_one_week:I = 0x7f1303cd

.field public static final a_sharesecurelink_limt_three_months:I = 0x7f1303ce

.field public static final a_sharesecurelink_limt_two_week:I = 0x7f1303cf

.field public static final a_state_unreviewed:I = 0x7f1303d0

.field public static final a_state_unreviewed_desc:I = 0x7f1303d1

.field public static final a_str_space:I = 0x7f1303d2

.field public static final a_subject_email_share_multi_docs:I = 0x7f1303d3

.field public static final a_subject_share_one_page_paid:I = 0x7f1303d4

.field public static final a_summary_gallery_perference:I = 0x7f1303d5

.field public static final a_summary_msg_receive_switch:I = 0x7f1303d6

.field public static final a_summary_upload_manage:I = 0x7f1303d7

.field public static final a_summary_vip_account:I = 0x7f1303d8

.field public static final a_super_vip_desc:I = 0x7f1303d9

.field public static final a_sync_setting_close_sync:I = 0x7f1303da

.field public static final a_sync_setting_network:I = 0x7f1303db

.field public static final a_sync_setting_network_all:I = 0x7f1303dc

.field public static final a_sync_setting_network_wifi:I = 0x7f1303dd

.field public static final a_sync_setting_sumary_network_all:I = 0x7f1303de

.field public static final a_sync_setting_sumary_network_wifi:I = 0x7f1303df

.field public static final a_sync_tip_go_to_private_folder:I = 0x7f1303e0

.field public static final a_sync_tip_sync_open:I = 0x7f1303e1

.field public static final a_sync_tip_sync_open_detail:I = 0x7f1303e2

.field public static final a_tag_label_add:I = 0x7f1303e3

.field public static final a_tag_label_black:I = 0x7f1303e4

.field public static final a_tag_label_card:I = 0x7f1303e5

.field public static final a_tag_label_certificate:I = 0x7f1303e6

.field public static final a_tag_label_remark:I = 0x7f1303e7

.field public static final a_tag_label_ungroup:I = 0x7f1303e8

.field public static final a_tag_label_white:I = 0x7f1303e9

.field public static final a_tag_lock_dlg_msg:I = 0x7f1303ea

.field public static final a_tag_tilte_add:I = 0x7f1303eb

.field public static final a_ten_fax_price:I = 0x7f1303ec

.field public static final a_text_go_to_do:I = 0x7f1303ed

.field public static final a_tip_ad_facebook:I = 0x7f1303ee

.field public static final a_tip_ad_google:I = 0x7f1303ef

.field public static final a_tip_experience_trim:I = 0x7f1303f0

.field public static final a_tip_normal_share_has_ink:I = 0x7f1303f1

.field public static final a_tip_vip_price_after_trial:I = 0x7f1303f2

.field public static final a_tip_vip_share_no_ink:I = 0x7f1303f3

.field public static final a_tips_certificate_menu:I = 0x7f1303f4

.field public static final a_tips_check_prestige:I = 0x7f1303f5

.field public static final a_tips_i_have_agree:I = 0x7f1303f6

.field public static final a_tips_image_break:I = 0x7f1303f7

.field public static final a_tips_know_introduce:I = 0x7f1303f8

.field public static final a_tips_max_e_evidence:I = 0x7f1303f9

.field public static final a_tips_mode_topic:I = 0x7f1303fa

.field public static final a_tips_signature_capture:I = 0x7f1303fb

.field public static final a_tips_team_expire:I = 0x7f1303fc

.field public static final a_tips_team_expired:I = 0x7f1303fd

.field public static final a_tips_team_guide:I = 0x7f1303fe

.field public static final a_tips_team_guide_1:I = 0x7f1303ff

.field public static final a_tips_team_guide_2:I = 0x7f130400

.field public static final a_tips_team_welcome:I = 0x7f130401

.field public static final a_tips_this_only_support_read:I = 0x7f130402

.field public static final a_tips_topic_preview_back:I = 0x7f130403

.field public static final a_tips_wait_for_upload:I = 0x7f130404

.field public static final a_tips_when_save_empty_topic:I = 0x7f130405

.field public static final a_title_activate:I = 0x7f130406

.field public static final a_title_activate_ok:I = 0x7f130407

.field public static final a_title_add_to_shortcut:I = 0x7f130408

.field public static final a_title_app_config:I = 0x7f130409

.field public static final a_title_auto_upload:I = 0x7f13040a

.field public static final a_title_autoupload_account_change:I = 0x7f13040b

.field public static final a_title_autoupload_format:I = 0x7f13040c

.field public static final a_title_camcard_download_webview:I = 0x7f13040d

.field public static final a_title_certificate_menu:I = 0x7f13040e

.field public static final a_title_change_icon:I = 0x7f13040f

.field public static final a_title_change_net_hint:I = 0x7f130410

.field public static final a_title_cloud_ocr_fail:I = 0x7f130411

.field public static final a_title_common_faq:I = 0x7f130412

.field public static final a_title_confirm_doc_password:I = 0x7f130413

.field public static final a_title_congratulations:I = 0x7f130414

.field public static final a_title_copy:I = 0x7f130415

.field public static final a_title_cutom_gallery_all:I = 0x7f130416

.field public static final a_title_default_camcard_doc_title:I = 0x7f130417

.field public static final a_title_default_topic:I = 0x7f130418

.field public static final a_title_default_topic_image:I = 0x7f130419

.field public static final a_title_delete_screenshot_image:I = 0x7f13041a

.field public static final a_title_delete_warter_mark:I = 0x7f13041b

.field public static final a_title_dir_storage_display_show:I = 0x7f13041c

.field public static final a_title_dir_storage_root:I = 0x7f13041d

.field public static final a_title_dlg_error_title:I = 0x7f13041e

.field public static final a_title_dlg_import_pdf_fail:I = 0x7f13041f

.field public static final a_title_dlg_rename_doc_title:I = 0x7f130420

.field public static final a_title_dlg_send_fail:I = 0x7f130421

.field public static final a_title_doc_pdf_attribute:I = 0x7f130422

.field public static final a_title_doc_show_order:I = 0x7f130423

.field public static final a_title_edit_not_supported:I = 0x7f130424

.field public static final a_title_empty_shared_docs:I = 0x7f130425

.field public static final a_title_example_name:I = 0x7f130426

.field public static final a_title_fax_charge:I = 0x7f130427

.field public static final a_title_files_save:I = 0x7f130428

.field public static final a_title_find_new_screenshot:I = 0x7f130429

.field public static final a_title_get_privilege_successfully:I = 0x7f13042a

.field public static final a_title_get_vip_by_refer_to_earn:I = 0x7f13042b

.field public static final a_title_give_you_seven_days:I = 0x7f13042c

.field public static final a_title_know_cs_quickly:I = 0x7f13042d

.field public static final a_title_local_ocr_fail:I = 0x7f13042e

.field public static final a_title_low_storage:I = 0x7f13042f

.field public static final a_title_manual_batch:I = 0x7f130430

.field public static final a_title_msg_receive_switch:I = 0x7f130431

.field public static final a_title_net_error_notification:I = 0x7f130432

.field public static final a_title_no_more_cloud_ocr_try:I = 0x7f130433

.field public static final a_title_no_water_ink:I = 0x7f130434

.field public static final a_title_ocr_completed:I = 0x7f130435

.field public static final a_title_ocr_lang_af:I = 0x7f130436

.field public static final a_title_ocr_lang_ar:I = 0x7f130437

.field public static final a_title_ocr_lang_bn:I = 0x7f130438

.field public static final a_title_ocr_lang_ca:I = 0x7f130439

.field public static final a_title_ocr_lang_chs:I = 0x7f13043a

.field public static final a_title_ocr_lang_cht:I = 0x7f13043b

.field public static final a_title_ocr_lang_cs:I = 0x7f13043c

.field public static final a_title_ocr_lang_dan:I = 0x7f13043d

.field public static final a_title_ocr_lang_de:I = 0x7f13043e

.field public static final a_title_ocr_lang_du:I = 0x7f13043f

.field public static final a_title_ocr_lang_el:I = 0x7f130440

.field public static final a_title_ocr_lang_eng:I = 0x7f130441

.field public static final a_title_ocr_lang_es:I = 0x7f130442

.field public static final a_title_ocr_lang_fa:I = 0x7f130443

.field public static final a_title_ocr_lang_fil:I = 0x7f130444

.field public static final a_title_ocr_lang_fin:I = 0x7f130445

.field public static final a_title_ocr_lang_fr:I = 0x7f130446

.field public static final a_title_ocr_lang_he:I = 0x7f130447

.field public static final a_title_ocr_lang_hi:I = 0x7f130448

.field public static final a_title_ocr_lang_hun:I = 0x7f130449

.field public static final a_title_ocr_lang_id:I = 0x7f13044a

.field public static final a_title_ocr_lang_it:I = 0x7f13044b

.field public static final a_title_ocr_lang_jp:I = 0x7f13044c

.field public static final a_title_ocr_lang_ko:I = 0x7f13044d

.field public static final a_title_ocr_lang_ne:I = 0x7f13044e

.field public static final a_title_ocr_lang_no:I = 0x7f13044f

.field public static final a_title_ocr_lang_pl:I = 0x7f130450

.field public static final a_title_ocr_lang_po:I = 0x7f130451

.field public static final a_title_ocr_lang_russian:I = 0x7f130452

.field public static final a_title_ocr_lang_sa:I = 0x7f130453

.field public static final a_title_ocr_lang_sk:I = 0x7f130454

.field public static final a_title_ocr_lang_sl:I = 0x7f130455

.field public static final a_title_ocr_lang_sr:I = 0x7f130456

.field public static final a_title_ocr_lang_sw:I = 0x7f130457

.field public static final a_title_ocr_lang_ta:I = 0x7f130458

.field public static final a_title_ocr_lang_th:I = 0x7f130459

.field public static final a_title_ocr_lang_tr:I = 0x7f13045a

.field public static final a_title_ocr_lang_uk:I = 0x7f13045b

.field public static final a_title_ocr_lang_ur:I = 0x7f13045c

.field public static final a_title_ocr_lang_uz:I = 0x7f13045d

.field public static final a_title_ocr_lang_vi:I = 0x7f13045e

.field public static final a_title_ocr_result:I = 0x7f13045f

.field public static final a_title_ocr_support_lang:I = 0x7f130460

.field public static final a_title_page_rename:I = 0x7f130461

.field public static final a_title_pdf_preview:I = 0x7f130462

.field public static final a_title_prepare_document:I = 0x7f130463

.field public static final a_title_privacy_policy:I = 0x7f130464

.field public static final a_title_qrcode:I = 0x7f130465

.field public static final a_title_reward_register:I = 0x7f130466

.field public static final a_title_save_water_mark:I = 0x7f130467

.field public static final a_title_scan_done:I = 0x7f130468

.field public static final a_title_scan_qr_for_vip:I = 0x7f130469

.field public static final a_title_scanner_everyone_use:I = 0x7f13046a

.field public static final a_title_screenshot:I = 0x7f13046b

.field public static final a_title_security_and_backup:I = 0x7f13046c

.field public static final a_title_send_to_wx_mini:I = 0x7f13046d

.field public static final a_title_setting_default_doc_name:I = 0x7f13046e

.field public static final a_title_setting_show_tagbar:I = 0x7f13046f

.field public static final a_title_share_ocr:I = 0x7f130470

.field public static final a_title_sharesecurelink_deadlinename:I = 0x7f130471

.field public static final a_title_sharesecurelink_unlimt:I = 0x7f130472

.field public static final a_title_show_fax_state:I = 0x7f130473

.field public static final a_title_sign_in_for_vip:I = 0x7f130474

.field public static final a_title_simulate_name:I = 0x7f130475

.field public static final a_title_sns_result:I = 0x7f130476

.field public static final a_title_start_do_camera:I = 0x7f130477

.field public static final a_title_storage_pulled_out:I = 0x7f130478

.field public static final a_title_system_message:I = 0x7f130479

.field public static final a_title_tag_setting_save_change:I = 0x7f13047a

.field public static final a_title_ten_million:I = 0x7f13047b

.field public static final a_title_time_to:I = 0x7f13047c

.field public static final a_title_upgrade:I = 0x7f13047d

.field public static final a_title_upload_format:I = 0x7f13047e

.field public static final a_title_use_cs:I = 0x7f13047f

.field public static final a_title_verifycode_login:I = 0x7f130480

.field public static final a_title_vip_for_free:I = 0x7f130481

.field public static final a_title_warter_mark:I = 0x7f130482

.field public static final a_title_webstorage_baidu:I = 0x7f130483

.field public static final a_txt_fax_balance:I = 0x7f130485

.field public static final a_upload_upload_option_doc:I = 0x7f130486

.field public static final a_upload_upload_option_image:I = 0x7f130487

.field public static final a_view_msg_empty_doc:I = 0x7f130490

.field public static final a_vip_msg_certificate:I = 0x7f130491

.field public static final a_warn_enable_five_million_pixels_plus:I = 0x7f130492

.field public static final abc_action_bar_home_description:I = 0x7f130493

.field public static final abc_action_bar_up_description:I = 0x7f130494

.field public static final abc_action_menu_overflow_description:I = 0x7f130495

.field public static final abc_action_mode_done:I = 0x7f130496

.field public static final abc_activity_chooser_view_see_all:I = 0x7f130497

.field public static final abc_activitychooserview_choose_application:I = 0x7f130498

.field public static final abc_capital_off:I = 0x7f130499

.field public static final abc_capital_on:I = 0x7f13049a

.field public static final abc_menu_alt_shortcut_label:I = 0x7f13049b

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f13049c

.field public static final abc_menu_delete_shortcut_label:I = 0x7f13049d

.field public static final abc_menu_enter_shortcut_label:I = 0x7f13049e

.field public static final abc_menu_function_shortcut_label:I = 0x7f13049f

.field public static final abc_menu_meta_shortcut_label:I = 0x7f1304a0

.field public static final abc_menu_shift_shortcut_label:I = 0x7f1304a1

.field public static final abc_menu_space_shortcut_label:I = 0x7f1304a2

.field public static final abc_menu_sym_shortcut_label:I = 0x7f1304a3

.field public static final abc_prepend_shortcut_label:I = 0x7f1304a4

.field public static final abc_search_hint:I = 0x7f1304a5

.field public static final abc_searchview_description_clear:I = 0x7f1304a6

.field public static final abc_searchview_description_query:I = 0x7f1304a7

.field public static final abc_searchview_description_search:I = 0x7f1304a8

.field public static final abc_searchview_description_submit:I = 0x7f1304a9

.field public static final abc_searchview_description_voice:I = 0x7f1304aa

.field public static final abc_shareactionprovider_share_with:I = 0x7f1304ab

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f1304ac

.field public static final abc_toolbar_collapse_description:I = 0x7f1304ad

.field public static final about_dialog_title:I = 0x7f1304ae

.field public static final about_version:I = 0x7f1304b0

.field public static final account_set_sign_in:I = 0x7f1304b1

.field public static final action_batch_title:I = 0x7f1304b2

.field public static final action_send_title:I = 0x7f1304b3

.field public static final androidx_startup:I = 0x7f1304ea

.field public static final app_about:I = 0x7f1304eb

.field public static final app_name:I = 0x7f1304ed

.field public static final app_version:I = 0x7f1304ee

.field public static final appbar_scrolling_view_behavior:I = 0x7f1304ef

.field public static final ask_to_delete:I = 0x7f130511

.field public static final authorizing:I = 0x7f130512

.field public static final auto_composite_template_idcard:I = 0x7f13051b

.field public static final auto_composite_template_passport:I = 0x7f13051c

.field public static final auto_composite_template_us_driver_liscence:I = 0x7f13051f

.field public static final auto_trimming:I = 0x7f130520

.field public static final auto_trimming_summary:I = 0x7f130521

.field public static final bottom_sheet_behavior:I = 0x7f130522

.field public static final bottom_sheet_behavior2:I = 0x7f130523

.field public static final bottomsheet_action_collapse:I = 0x7f130524

.field public static final bottomsheet_action_expand:I = 0x7f130525

.field public static final bottomsheet_action_expand_halfway:I = 0x7f130526

.field public static final bottomsheet_drag_handle_clicked:I = 0x7f130527

.field public static final bottomsheet_drag_handle_content_description:I = 0x7f130528

.field public static final bound_trim_error:I = 0x7f130529

.field public static final btn_add_water_maker:I = 0x7f13052f

.field public static final btn_camera_title:I = 0x7f130530

.field public static final btn_comment:I = 0x7f130531

.field public static final btn_continue_photo:I = 0x7f130532

.field public static final btn_delete_title:I = 0x7f130533

.field public static final btn_done_title:I = 0x7f130534

.field public static final btn_edit_title:I = 0x7f130535

.field public static final btn_gallery_title:I = 0x7f130536

.field public static final btn_grid_mode_title:I = 0x7f130537

.field public static final btn_list_mode_title:I = 0x7f130538

.field public static final btn_recommend:I = 0x7f130539

.field public static final btn_remove_water_maker:I = 0x7f13053a

.field public static final btn_scan_back_title:I = 0x7f13053b

.field public static final btn_scan_bound_title:I = 0x7f13053c

.field public static final btn_scan_turn_right_title:I = 0x7f13053d

.field public static final btn_send_title:I = 0x7f13053e

.field public static final btn_share_title:I = 0x7f13053f

.field public static final btn_tag_title:I = 0x7f130540

.field public static final btn_upload_title:I = 0x7f130541

.field public static final button_no:I = 0x7f130542

.field public static final button_yes:I = 0x7f130543

.field public static final c_add_pdf_size_height_label:I = 0x7f130544

.field public static final c_add_pdf_size_inch:I = 0x7f130545

.field public static final c_add_pdf_size_milli:I = 0x7f130546

.field public static final c_add_pdf_size_name_hint:I = 0x7f130547

.field public static final c_add_pdf_size_name_label:I = 0x7f130548

.field public static final c_add_pdf_size_point:I = 0x7f130549

.field public static final c_add_pdf_size_width_label:I = 0x7f13054d

.field public static final c_btn_confirm:I = 0x7f13054e

.field public static final c_btn_verify_email_next:I = 0x7f13054f

.field public static final c_changepwd_hint_old_pwd:I = 0x7f130550

.field public static final c_changepwd_label_confirm:I = 0x7f130551

.field public static final c_changepwd_msg_changing:I = 0x7f130552

.field public static final c_changepwd_title:I = 0x7f130553

.field public static final c_changepwd_toast_success:I = 0x7f130554

.field public static final c_common_sync_failed:I = 0x7f130555

.field public static final c_global_msg_have_same_pdfsize:I = 0x7f130556

.field public static final c_global_msg_if_delete_pdfsize:I = 0x7f130557

.field public static final c_global_msg_pdfsize_name_empty:I = 0x7f130558

.field public static final c_global_msg_width_height_invial:I = 0x7f130559

.field public static final c_global_msg_width_height_outofbound:I = 0x7f13055a

.field public static final c_global_pdfsize_adjust_name:I = 0x7f13055b

.field public static final c_global_register_toast_register_fail:I = 0x7f13055c

.field public static final c_global_toast_network_error:I = 0x7f13055d

.field public static final c_globat_email_not_reg:I = 0x7f13055e

.field public static final c_gmail_msg_email_login_conflict_error_1:I = 0x7f13055f

.field public static final c_info_get_extra_by_review:I = 0x7f130560

.field public static final c_info_reward_activate:I = 0x7f130561

.field public static final c_label_account_logout:I = 0x7f130562

.field public static final c_label_send_doc_title:I = 0x7f130563

.field public static final c_label_try_later:I = 0x7f130564

.field public static final c_msg_account_disable:I = 0x7f130565

.field public static final c_msg_error_phone:I = 0x7f130566

.field public static final c_msg_error_validate_number:I = 0x7f130567

.field public static final c_msg_login_to_sync:I = 0x7f130568

.field public static final c_msg_old_pwd_error:I = 0x7f130569

.field public static final c_msg_phone_pwd_not_match:I = 0x7f13056a

.field public static final c_msg_pwd_error:I = 0x7f13056b

.field public static final c_msg_request_verify_code_fail:I = 0x7f13056c

.field public static final c_msg_reset_pass_failed:I = 0x7f13056d

.field public static final c_msg_send_sms_error_211:I = 0x7f13056e

.field public static final c_register_send_validation:I = 0x7f13056f

.field public static final c_sign_pwd_hint:I = 0x7f130570

.field public static final c_sync_msg_server_unavail:I = 0x7f130571

.field public static final c_sync_warning_cloudspace_upgrade:I = 0x7f130572

.field public static final c_text_phone_number:I = 0x7f130573

.field public static final c_tianshu_error_email_reg:I = 0x7f130574

.field public static final c_tips_mi_disable_camera_disable_msg_not_v6:I = 0x7f130575

.field public static final c_tips_mi_disable_camera_disable_msg_v6:I = 0x7f130576

.field public static final c_tips_mi_disable_camera_disable_ok_button:I = 0x7f130577

.field public static final c_tips_mi_disable_camera_disable_title:I = 0x7f130578

.field public static final c_title_reset_password:I = 0x7f130579

.field public static final c_title_select_country:I = 0x7f13057a

.field public static final c_title_set_pwd:I = 0x7f13057b

.field public static final camera_btn_hint:I = 0x7f13057c

.field public static final camera_error_title:I = 0x7f13057d

.field public static final cancel:I = 0x7f13057e

.field public static final ce_648_quizgo_safe1:I = 0x7f13057f

.field public static final ce_648_quizgo_safe2:I = 0x7f130580

.field public static final ce_648_quizgo_safe3:I = 0x7f130581

.field public static final ce_648_quizgo_safe4:I = 0x7f130582

.field public static final ce_648_quizgo_safe6:I = 0x7f130583

.field public static final change_size_realign_content:I = 0x7f130584

.field public static final character_counter_content_description:I = 0x7f130585

.field public static final character_counter_overflowed_content_description:I = 0x7f130586

.field public static final character_counter_pattern:I = 0x7f130587

.field public static final check_auth:I = 0x7f130588

.field public static final check_email:I = 0x7f130589

.field public static final check_license:I = 0x7f13058a

.field public static final chose_model:I = 0x7f13058b

.field public static final chose_template_accorded_file_type:I = 0x7f13058c

.field public static final clear_text_end_icon_content_description:I = 0x7f13058d

.field public static final cn_login_first_login_create_account:I = 0x7f13058e

.field public static final common_google_play_services_enable_button:I = 0x7f1305a7

.field public static final common_google_play_services_enable_text:I = 0x7f1305a8

.field public static final common_google_play_services_enable_title:I = 0x7f1305a9

.field public static final common_google_play_services_install_button:I = 0x7f1305aa

.field public static final common_google_play_services_install_text:I = 0x7f1305ab

.field public static final common_google_play_services_install_title:I = 0x7f1305ac

.field public static final common_google_play_services_notification_channel_name:I = 0x7f1305ad

.field public static final common_google_play_services_notification_ticker:I = 0x7f1305ae

.field public static final common_google_play_services_unknown_issue:I = 0x7f1305af

.field public static final common_google_play_services_unsupported_text:I = 0x7f1305b0

.field public static final common_google_play_services_update_button:I = 0x7f1305b1

.field public static final common_google_play_services_update_text:I = 0x7f1305b2

.field public static final common_google_play_services_update_title:I = 0x7f1305b3

.field public static final common_google_play_services_updating_text:I = 0x7f1305b4

.field public static final common_google_play_services_wear_update_text:I = 0x7f1305b5

.field public static final common_open_on_phone:I = 0x7f1305b6

.field public static final common_signin_button_text:I = 0x7f1305b7

.field public static final common_signin_button_text_long:I = 0x7f1305b8

.field public static final create_pdf__dialog_title:I = 0x7f1305ba

.field public static final cs590_noads:I = 0x7f1305bb

.field public static final cs_16_capon_pop3_normal_first:I = 0x7f1305bc

.field public static final cs_16_capon_pop3_normal_top_open:I = 0x7f1305bd

.field public static final cs_16_capon_pop3_normal_top_second:I = 0x7f1305be

.field public static final cs_16_capon_pop3_package_mouth:I = 0x7f1305bf

.field public static final cs_16_capon_pop3_package_mouthly:I = 0x7f1305c0

.field public static final cs_16_capon_pop3_package_year:I = 0x7f1305c1

.field public static final cs_16_capon_pop3_package_yearly:I = 0x7f1305c2

.field public static final cs_16_capon_pop_left:I = 0x7f1305c3

.field public static final cs_17_pay_immediate:I = 0x7f1305c4

.field public static final cs_17_pay_type:I = 0x7f1305c5

.field public static final cs_18_certificates_title:I = 0x7f1305c6

.field public static final cs_20_list_docad_collage:I = 0x7f1305c7

.field public static final cs_21_main_idcard_folder:I = 0x7f1305c8

.field public static final cs_21_pay_more:I = 0x7f1305c9

.field public static final cs_21main_idcard_folder_tips:I = 0x7f1305ca

.field public static final cs_22_coupon_month:I = 0x7f1305cb

.field public static final cs_22_coupon_year:I = 0x7f1305cc

.field public static final cs_22_new_privileges:I = 0x7f1305cd

.field public static final cs_22_receiving_Preferences:I = 0x7f1305ce

.field public static final cs_22_receiving_Preferences_detil:I = 0x7f1305cf

.field public static final cs_22_store:I = 0x7f1305d0

.field public static final cs_29_blur_form:I = 0x7f1305d1

.field public static final cs_29_bottom_of_main:I = 0x7f1305d2

.field public static final cs_29_clear_form:I = 0x7f1305d3

.field public static final cs_29_id_quit_pop:I = 0x7f1305d4

.field public static final cs_29_id_quit_pop_continue:I = 0x7f1305d5

.field public static final cs_29_id_quit_pop_out:I = 0x7f1305d6

.field public static final cs_29_id_quit_pop_title:I = 0x7f1305d7

.field public static final cs_29_list_operation_word:I = 0x7f1305d8

.field public static final cs_29_tips_title:I = 0x7f1305d9

.field public static final cs_31_ad_label:I = 0x7f1305da

.field public static final cs_31_coupon_pop_1:I = 0x7f1305db

.field public static final cs_31_coupon_pop_2:I = 0x7f1305dc

.field public static final cs_31_coupon_pop_countdown:I = 0x7f1305dd

.field public static final cs_31_coupon_pop_day:I = 0x7f1305de

.field public static final cs_31_coupon_pop_nomal:I = 0x7f1305df

.field public static final cs_32_task_idcard_comfirm:I = 0x7f1305e0

.field public static final cs_33_scandone_transfer_pop:I = 0x7f1305e1

.field public static final cs_34_scan_multi_shot_pop:I = 0x7f1305e2

.field public static final cs_34_scan_scandone_pop1:I = 0x7f1305e3

.field public static final cs_35_download_qq:I = 0x7f1305e4

.field public static final cs_35_download_wechat:I = 0x7f1305e5

.field public static final cs_35_idcard_mode_addmark_pop_content:I = 0x7f1305e6

.field public static final cs_35_idcard_mode_addmark_pop_tittle:I = 0x7f1305e7

.field public static final cs_35_more:I = 0x7f1305e8

.field public static final cs_35_qq:I = 0x7f1305e9

.field public static final cs_35_watermark_pop_advanced_account:I = 0x7f1305ea

.field public static final cs_35_watermark_pop_button_noremind:I = 0x7f1305eb

.field public static final cs_35_watermark_pop_button_watermark:I = 0x7f1305ec

.field public static final cs_35_weixin:I = 0x7f1305ed

.field public static final cs_36_banner_monthly_user_special_scandone:I = 0x7f1305ee

.field public static final cs_36_popup_monthly_user_special:I = 0x7f1305ef

.field public static final cs_36_popup_monthly_user_special_content1:I = 0x7f1305f0

.field public static final cs_36_popup_monthly_user_special_content2:I = 0x7f1305f1

.field public static final cs_36_popup_monthly_user_special_link:I = 0x7f1305f2

.field public static final cs_36_privacy_protocol:I = 0x7f1305f3

.field public static final cs_36_user_protocol:I = 0x7f1305f4

.field public static final cs_36_user_protocol_full:I = 0x7f1305f5

.field public static final cs_37_failed_discount:I = 0x7f1305f6

.field public static final cs_37_failed_discount_20_privileges:I = 0x7f1305f7

.field public static final cs_37_failed_discount_first_month_price:I = 0x7f1305f8

.field public static final cs_37_failed_discount_first_year_price:I = 0x7f1305f9

.field public static final cs_37_failed_discount_give_up:I = 0x7f1305fa

.field public static final cs_37_failed_discount_no_watermarks:I = 0x7f1305fb

.field public static final cs_37_failed_discount_original_price:I = 0x7f1305fc

.field public static final cs_37_failed_discount_time_limit:I = 0x7f1305fd

.field public static final cs_38_new_user_title:I = 0x7f1305fe

.field public static final cs_39_old_user_coupon_name:I = 0x7f1305ff

.field public static final cs_39_old_user_discount_discription:I = 0x7f130600

.field public static final cs_39_old_user_discount_title:I = 0x7f130601

.field public static final cs_40_collage_operation_vision:I = 0x7f130602

.field public static final cs_41_china_id_vision_12:I = 0x7f130603

.field public static final cs_41_china_id_vision_3:I = 0x7f130604

.field public static final cs_42_id_watermark_description:I = 0x7f130605

.field public static final cs_42_id_watermark_example:I = 0x7f130606

.field public static final cs_42_id_watermark_guide:I = 0x7f130607

.field public static final cs_42_id_watermark_tips:I = 0x7f130608

.field public static final cs_50_doclist_id_guide_discribe:I = 0x7f130609

.field public static final cs_5100_again_share:I = 0x7f13060a

.field public static final cs_5100_alarm_book_reach_limit:I = 0x7f13060b

.field public static final cs_5100_alarm_signature_free_credit:I = 0x7f13060c

.field public static final cs_5100_bubble_view:I = 0x7f13060d

.field public static final cs_5100_button_book_cancel_limit:I = 0x7f13060e

.field public static final cs_5100_button_book_continue_to_capture:I = 0x7f13060f

.field public static final cs_5100_button_book_save_pictures:I = 0x7f130610

.field public static final cs_5100_button_book_start:I = 0x7f130611

.field public static final cs_5100_button_book_upgrade:I = 0x7f130612

.field public static final cs_5100_button_book_watch_ad:I = 0x7f130613

.field public static final cs_5100_button_signature_discard:I = 0x7f130614

.field public static final cs_5100_button_signature_upgrade:I = 0x7f130615

.field public static final cs_5100_cloud_ocr_language:I = 0x7f130616

.field public static final cs_5100_confirm_back:I = 0x7f130617

.field public static final cs_5100_confirm_discard:I = 0x7f130618

.field public static final cs_5100_confirm_no_records:I = 0x7f130619

.field public static final cs_5100_crop_tip:I = 0x7f13061a

.field public static final cs_5100_drag_square:I = 0x7f13061b

.field public static final cs_5100_excel_doc_tip:I = 0x7f13061c

.field public static final cs_5100_excel_guide:I = 0x7f13061d

.field public static final cs_5100_excel_language:I = 0x7f13061e

.field public static final cs_5100_excel_pro_pop:I = 0x7f13061f

.field public static final cs_5100_excel_records:I = 0x7f130620

.field public static final cs_5100_excel_save:I = 0x7f130621

.field public static final cs_5100_excel_scan:I = 0x7f130622

.field public static final cs_5100_guide_book:I = 0x7f130623

.field public static final cs_5100_guide_book2:I = 0x7f130624

.field public static final cs_5100_guide_book_align:I = 0x7f130625

.field public static final cs_5100_guide_book_reedit:I = 0x7f130626

.field public static final cs_5100_guide_book_retry:I = 0x7f130627

.field public static final cs_5100_guide_import_gallery_bank_card:I = 0x7f130628

.field public static final cs_5100_guide_import_gallery_company_id:I = 0x7f130629

.field public static final cs_5100_guide_import_gallery_house_property:I = 0x7f13062a

.field public static final cs_5100_guide_import_gallery_hukou:I = 0x7f13062b

.field public static final cs_5100_guide_import_gallery_idcard:I = 0x7f13062c

.field public static final cs_5100_guide_import_gallery_passport:I = 0x7f13062d

.field public static final cs_5100_jpg_security:I = 0x7f13062e

.field public static final cs_5100_local_ocr:I = 0x7f13062f

.field public static final cs_5100_local_ocr_language:I = 0x7f130630

.field public static final cs_5100_no_excel:I = 0x7f130631

.field public static final cs_5100_no_tip:I = 0x7f130632

.field public static final cs_5100_no_water:I = 0x7f130633

.field public static final cs_5100_nothing:I = 0x7f130634

.field public static final cs_5100_ocr_update:I = 0x7f130635

.field public static final cs_5100_ok:I = 0x7f130636

.field public static final cs_5100_pdf_no_water:I = 0x7f130637

.field public static final cs_5100_pdf_no_water_security:I = 0x7f130638

.field public static final cs_5100_pdf_security:I = 0x7f130639

.field public static final cs_5100_popup_book_no_credit:I = 0x7f13063a

.field public static final cs_5100_popup_book_no_network:I = 0x7f13063b

.field public static final cs_5100_popup_book_premium:I = 0x7f13063c

.field public static final cs_5100_popup_folder_limit_reached:I = 0x7f13063d

.field public static final cs_5100_popup_signature_leave:I = 0x7f13063e

.field public static final cs_5100_popup_signature_premium:I = 0x7f13063f

.field public static final cs_5100_recognize_again:I = 0x7f130640

.field public static final cs_5100_record:I = 0x7f130641

.field public static final cs_5100_security_could_waste_time:I = 0x7f130642

.field public static final cs_5100_see_example:I = 0x7f130643

.field public static final cs_5100_sorry_pay_excel:I = 0x7f130644

.field public static final cs_5100_sync_drop_down:I = 0x7f130645

.field public static final cs_5100_sync_keep_in_app:I = 0x7f130646

.field public static final cs_5100_sync_not_operate:I = 0x7f130647

.field public static final cs_5100_sync_release:I = 0x7f130648

.field public static final cs_5100_sync_success:I = 0x7f130649

.field public static final cs_5100_sync_tip:I = 0x7f13064a

.field public static final cs_5100_syncing:I = 0x7f13064b

.field public static final cs_5100_text_recognition_ocr:I = 0x7f13064c

.field public static final cs_5100_toast_auto_crop_off:I = 0x7f13064d

.field public static final cs_5100_toast_auto_crop_on:I = 0x7f13064e

.field public static final cs_5100_toast_book_ad_watched:I = 0x7f13064f

.field public static final cs_5100_toast_book_upgraded:I = 0x7f130650

.field public static final cs_5100_water_tip:I = 0x7f130651

.field public static final cs_5110_after_trial:I = 0x7f130652

.field public static final cs_5110_copy:I = 0x7f130653

.field public static final cs_511_1000:I = 0x7f130654

.field public static final cs_511_10GB:I = 0x7f130655

.field public static final cs_511_24hour_countdown:I = 0x7f130656

.field public static final cs_511_4_credit:I = 0x7f130657

.field public static final cs_511_add_protect:I = 0x7f130659

.field public static final cs_511_cancel_pdf_password:I = 0x7f13065a

.field public static final cs_511_clear_protect_water:I = 0x7f13065b

.field public static final cs_511_cloud:I = 0x7f13065c

.field public static final cs_511_compression:I = 0x7f13065d

.field public static final cs_511_compression_size:I = 0x7f13065e

.field public static final cs_511_coupon:I = 0x7f13065f

.field public static final cs_511_delete_water:I = 0x7f130660

.field public static final cs_511_electronic_signature:I = 0x7f130661

.field public static final cs_511_file_protect:I = 0x7f130662

.field public static final cs_511_file_size:I = 0x7f130663

.field public static final cs_511_fix_protect:I = 0x7f130664

.field public static final cs_511_generating_link:I = 0x7f130665

.field public static final cs_511_immediately_restore:I = 0x7f130666

.field public static final cs_511_immediately_to:I = 0x7f130667

.field public static final cs_511_keep_account_status:I = 0x7f130668

.field public static final cs_511_limmediately_open:I = 0x7f130669

.field public static final cs_511_logo_water:I = 0x7f13066a

.field public static final cs_511_more:I = 0x7f13066b

.field public static final cs_511_newbie_task1_toast:I = 0x7f13066c

.field public static final cs_511_newbie_task2_toast:I = 0x7f13066d

.field public static final cs_511_newbie_task3_toast:I = 0x7f13066e

.field public static final cs_511_newbie_task4_toast:I = 0x7f13066f

.field public static final cs_511_pdf_password:I = 0x7f130670

.field public static final cs_511_pdf_password_cancel_toast:I = 0x7f130671

.field public static final cs_511_pdf_password_set_toast:I = 0x7f130672

.field public static final cs_511_pdf_photo:I = 0x7f130673

.field public static final cs_511_protect:I = 0x7f130674

.field public static final cs_511_protect_page:I = 0x7f130675

.field public static final cs_511_reset_password:I = 0x7f130676

.field public static final cs_511_set_pdf_premium_pop:I = 0x7f130677

.field public static final cs_511_share_link:I = 0x7f130678

.field public static final cs_511_three_day_free:I = 0x7f130679

.field public static final cs_511_word_document:I = 0x7f13067a

.field public static final cs_512_4_6_photo_print:I = 0x7f13067b

.field public static final cs_512_HD_unit:I = 0x7f13067c

.field public static final cs_512_ad_report:I = 0x7f13067d

.field public static final cs_512_button_add_nickname:I = 0x7f13067e

.field public static final cs_512_button_auto_crop:I = 0x7f13067f

.field public static final cs_512_button_book2:I = 0x7f130680

.field public static final cs_512_button_rating:I = 0x7f130681

.field public static final cs_512_button_reset:I = 0x7f130682

.field public static final cs_512_congratulations_pop_content:I = 0x7f130683

.field public static final cs_512_copy:I = 0x7f130684

.field public static final cs_512_dialog_rating:I = 0x7f130685

.field public static final cs_512_dialog_rating_content:I = 0x7f130686

.field public static final cs_512_id_photo_prin_notes_preview:I = 0x7f130687

.field public static final cs_512_invite_code_invalid:I = 0x7f130688

.field public static final cs_512_limited_time:I = 0x7f130689

.field public static final cs_512_make_a_copy:I = 0x7f13068a

.field public static final cs_512_message_filter:I = 0x7f13068b

.field public static final cs_512_message_invitee:I = 0x7f13068c

.field public static final cs_512_open_HD:I = 0x7f13068d

.field public static final cs_512_redeem_code_guideline:I = 0x7f13068e

.field public static final cs_512_redeem_to_earn:I = 0x7f13068f

.field public static final cs_512_save_card:I = 0x7f130690

.field public static final cs_512_save_my_doc:I = 0x7f130691

.field public static final cs_512_save_success:I = 0x7f130692

.field public static final cs_512_save_to_view:I = 0x7f130693

.field public static final cs_512_welcome_pop_button:I = 0x7f130694

.field public static final cs_512_welcome_pop_content:I = 0x7f130695

.field public static final cs_513_ID_card:I = 0x7f130696

.field public static final cs_513_QS_engineer:I = 0x7f130697

.field public static final cs_513_UrbanRural_planner:I = 0x7f130698

.field public static final cs_513_ad_rewarded_video:I = 0x7f130699

.field public static final cs_513_ad_rewarded_video_fail:I = 0x7f13069a

.field public static final cs_513_admission_letter:I = 0x7f13069b

.field public static final cs_513_aircrew:I = 0x7f13069c

.field public static final cs_513_architect_certificate:I = 0x7f13069d

.field public static final cs_513_auctioneer:I = 0x7f13069e

.field public static final cs_513_audit_professional:I = 0x7f13069f

.field public static final cs_513_aviation_intelligence_officer:I = 0x7f1306a0

.field public static final cs_513_aviation_security_officer:I = 0x7f1306a1

.field public static final cs_513_bankbook:I = 0x7f1306a2

.field public static final cs_513_banking_professional:I = 0x7f1306a3

.field public static final cs_513_batch_ocr:I = 0x7f1306a4

.field public static final cs_513_batch_ocr_describe:I = 0x7f1306a5

.field public static final cs_513_bind:I = 0x7f1306a6

.field public static final cs_513_bind_fail:I = 0x7f1306a7

.field public static final cs_513_bind_fail_tips:I = 0x7f1306a8

.field public static final cs_513_bind_no_wechat_tip:I = 0x7f1306a9

.field public static final cs_513_bind_success:I = 0x7f1306aa

.field public static final cs_513_bind_success_tips:I = 0x7f1306ab

.field public static final cs_513_birth_certificate:I = 0x7f1306ac

.field public static final cs_513_business_license:I = 0x7f1306ad

.field public static final cs_513_buy_cpoint:I = 0x7f1306ae

.field public static final cs_513_certificate_completion:I = 0x7f1306af

.field public static final cs_513_certificate_search:I = 0x7f1306b0

.field public static final cs_513_certified_personnel:I = 0x7f1306b1

.field public static final cs_513_cet_certificate:I = 0x7f1306b2

.field public static final cs_513_chemical_engineer:I = 0x7f1306b3

.field public static final cs_513_child_certificate:I = 0x7f1306b4

.field public static final cs_513_child_health_care:I = 0x7f1306b5

.field public static final cs_513_child_health_handbook:I = 0x7f1306b6

.field public static final cs_513_citizen_card:I = 0x7f1306b7

.field public static final cs_513_civil_engineer:I = 0x7f1306b8

.field public static final cs_513_cloud_100:I = 0x7f1306b9

.field public static final cs_513_cloud_100_pop:I = 0x7f1306ba

.field public static final cs_513_communication_personnel:I = 0x7f1306bb

.field public static final cs_513_compere:I = 0x7f1306bc

.field public static final cs_513_computer_rank:I = 0x7f1306bd

.field public static final cs_513_computer_software:I = 0x7f1306be

.field public static final cs_513_constructor_certificate:I = 0x7f1306bf

.field public static final cs_513_country_doctor:I = 0x7f1306c0

.field public static final cs_513_cpa:I = 0x7f1306c1

.field public static final cs_513_cpa_certificate:I = 0x7f1306c2

.field public static final cs_513_cpv:I = 0x7f1306c3

.field public static final cs_513_crew:I = 0x7f1306c4

.field public static final cs_513_cs_submit:I = 0x7f1306c5

.field public static final cs_513_cultural_relics_protection:I = 0x7f1306c6

.field public static final cs_513_death_certificate:I = 0x7f1306c7

.field public static final cs_513_degree_certificate:I = 0x7f1306c8

.field public static final cs_513_divorce_certificate_card:I = 0x7f1306c9

.field public static final cs_513_doctor:I = 0x7f1306ca

.field public static final cs_513_economic_professional:I = 0x7f1306cb

.field public static final cs_513_eia:I = 0x7f1306cc

.field public static final cs_513_electrical_engineer:I = 0x7f1306cd

.field public static final cs_513_entry_exit_personnel:I = 0x7f1306ce

.field public static final cs_513_environmental_engineer:I = 0x7f1306cf

.field public static final cs_513_equipment_supervisor:I = 0x7f1306d0

.field public static final cs_513_faq_account:I = 0x7f1306d1

.field public static final cs_513_faq_choose_type:I = 0x7f1306d2

.field public static final cs_513_faq_cloud:I = 0x7f1306d3

.field public static final cs_513_faq_crash:I = 0x7f1306d4

.field public static final cs_513_faq_duplicate:I = 0x7f1306d5

.field public static final cs_513_faq_email:I = 0x7f1306d6

.field public static final cs_513_faq_fax:I = 0x7f1306d7

.field public static final cs_513_faq_input_email:I = 0x7f1306d8

.field public static final cs_513_faq_ocr:I = 0x7f1306d9

.field public static final cs_513_faq_photo:I = 0x7f1306da

.field public static final cs_513_faq_points:I = 0x7f1306db

.field public static final cs_513_faq_process:I = 0x7f1306dc

.field public static final cs_513_faq_purchase:I = 0x7f1306dd

.field public static final cs_513_faq_share:I = 0x7f1306de

.field public static final cs_513_faq_sync:I = 0x7f1306df

.field public static final cs_513_faq_ui:I = 0x7f1306e0

.field public static final cs_513_faq_write_suggestion:I = 0x7f1306e1

.field public static final cs_513_firefighter_certificate:I = 0x7f1306e2

.field public static final cs_513_first_sysc_cloud:I = 0x7f1306e3

.field public static final cs_513_highway_waterway_engineerin:I = 0x7f1306e6

.field public static final cs_513_hk_macao_passport:I = 0x7f1306e7

.field public static final cs_513_id_card:I = 0x7f1306e8

.field public static final cs_513_internet_bind_fail:I = 0x7f1306e9

.field public static final cs_513_internet_unbind_fail:I = 0x7f1306ea

.field public static final cs_513_journalist:I = 0x7f1306eb

.field public static final cs_513_land_registration_agents:I = 0x7f1306ec

.field public static final cs_513_lawyer_certificate:I = 0x7f1306ed

.field public static final cs_513_licensed_veterinarian:I = 0x7f1306ee

.field public static final cs_513_marine_surveyor:I = 0x7f1306ef

.field public static final cs_513_marriage_certificate_card:I = 0x7f1306f0

.field public static final cs_513_mechanical_engineers:I = 0x7f1306f1

.field public static final cs_513_medical_insurance_card:I = 0x7f1306f2

.field public static final cs_513_medical_professional:I = 0x7f1306f3

.field public static final cs_513_medical_record_card:I = 0x7f1306f4

.field public static final cs_513_message_capture:I = 0x7f1306f5

.field public static final cs_513_metallurgical_enginee:I = 0x7f1306f6

.field public static final cs_513_metrologist:I = 0x7f1306f7

.field public static final cs_513_military_office:I = 0x7f1306f8

.field public static final cs_513_mineral_engineer:I = 0x7f1306f9

.field public static final cs_513_more_certificate:I = 0x7f1306fa

.field public static final cs_513_motor_vehicle_testing:I = 0x7f1306fb

.field public static final cs_513_natural_gas_enginee:I = 0x7f1306fc

.field public static final cs_513_navigators:I = 0x7f1306fd

.field public static final cs_513_normal_certificate:I = 0x7f1306fe

.field public static final cs_513_notary_certificate:I = 0x7f1306ff

.field public static final cs_513_nuclear_operator_certificate:I = 0x7f130700

.field public static final cs_513_nuclear_safety_certificate:I = 0x7f130701

.field public static final cs_513_nuclear_safety_engineer:I = 0x7f130702

.field public static final cs_513_nurse:I = 0x7f130703

.field public static final cs_513_ocr_Recognizing:I = 0x7f130704

.field public static final cs_513_ocr_not_full_premium:I = 0x7f130705

.field public static final cs_513_ocr_registered_given:I = 0x7f130706

.field public static final cs_513_ocr_used:I = 0x7f130707

.field public static final cs_513_organ_transplantation_doctor:I = 0x7f130708

.field public static final cs_513_patent_agent:I = 0x7f130709

.field public static final cs_513_pdf_signature:I = 0x7f13070a

.field public static final cs_513_performance_brokers:I = 0x7f13070b

.field public static final cs_513_pharmacist:I = 0x7f13070c

.field public static final cs_513_premises_permit:I = 0x7f13070d

.field public static final cs_513_project_consultancy:I = 0x7f13070e

.field public static final cs_513_public_equipment_engineer:I = 0x7f13070f

.field public static final cs_513_publishing_industry:I = 0x7f130710

.field public static final cs_513_real_estate:I = 0x7f130711

.field public static final cs_513_real_estate_brokers:I = 0x7f130712

.field public static final cs_513_recognition_limit:I = 0x7f130713

.field public static final cs_513_registered_surveyor:I = 0x7f130714

.field public static final cs_513_registration_certificate:I = 0x7f130715

.field public static final cs_513_remaining_free:I = 0x7f130716

.field public static final cs_513_residence_permit:I = 0x7f130717

.field public static final cs_513_safety_engineer:I = 0x7f130718

.field public static final cs_513_securities_certification:I = 0x7f130719

.field public static final cs_513_social_security_card:I = 0x7f13071a

.field public static final cs_513_social_worker:I = 0x7f13071b

.field public static final cs_513_special_equipment:I = 0x7f13071c

.field public static final cs_513_statistical_profession:I = 0x7f13071d

.field public static final cs_513_structural_engineer:I = 0x7f13071e

.field public static final cs_513_student_ID:I = 0x7f13071f

.field public static final cs_513_student_certificate:I = 0x7f130720

.field public static final cs_513_supervising_engineer_certificate:I = 0x7f130721

.field public static final cs_513_taiwan_passport:I = 0x7f130722

.field public static final cs_513_tax_accountant:I = 0x7f130723

.field public static final cs_513_teacher_certificate:I = 0x7f130724

.field public static final cs_513_translators_qualification:I = 0x7f130725

.field public static final cs_513_undergraduate_diploma:I = 0x7f130726

.field public static final cs_513_untied:I = 0x7f130727

.field public static final cs_513_untied_fail:I = 0x7f130728

.field public static final cs_513_untied_tips:I = 0x7f130729

.field public static final cs_513_vaccination_certificate:I = 0x7f13072a

.field public static final cs_513_vehicle_message_page:I = 0x7f13072b

.field public static final cs_513_village_veterinarian:I = 0x7f13072c

.field public static final cs_513_vocational_qualification:I = 0x7f13072d

.field public static final cs_513_water_conservancy:I = 0x7f13072e

.field public static final cs_513_whatsnew_tips:I = 0x7f13072f

.field public static final cs_513c_bind_go:I = 0x7f130730

.field public static final cs_514_bank_card:I = 0x7f130731

.field public static final cs_514_bind_account:I = 0x7f130732

.field public static final cs_514_bind_phone_introduce:I = 0x7f130733

.field public static final cs_514_bind_phone_number:I = 0x7f130734

.field public static final cs_514_bind_two_wechat_tip:I = 0x7f130735

.field public static final cs_514_bind_wechat_fail:I = 0x7f130736

.field public static final cs_514_binded_tips:I = 0x7f130737

.field public static final cs_514_brand_model:I = 0x7f130738

.field public static final cs_514_change_phone:I = 0x7f130739

.field public static final cs_514_change_phone_or_rebind:I = 0x7f13073a

.field public static final cs_514_check_the_details_of_the_document:I = 0x7f13073b

.field public static final cs_514_create_account:I = 0x7f13073c

.field public static final cs_514_delete_excel_tips:I = 0x7f13073d

.field public static final cs_514_driving_license_number:I = 0x7f13073e

.field public static final cs_514_giude_three_try:I = 0x7f13073f

.field public static final cs_514_id_effective_start_date:I = 0x7f130740

.field public static final cs_514_id_pake_add:I = 0x7f130741

.field public static final cs_514_id_pake_add_empty:I = 0x7f130742

.field public static final cs_514_id_pake_add_guide:I = 0x7f130743

.field public static final cs_514_id_pake_address:I = 0x7f130744

.field public static final cs_514_id_pake_collage:I = 0x7f130745

.field public static final cs_514_id_pake_delete:I = 0x7f130746

.field public static final cs_514_id_pake_delete_pop:I = 0x7f130747

.field public static final cs_514_id_pake_edit:I = 0x7f130748

.field public static final cs_514_id_pake_file_number:I = 0x7f130749

.field public static final cs_514_id_pake_holder:I = 0x7f13074a

.field public static final cs_514_id_pake_issue_date:I = 0x7f13074b

.field public static final cs_514_id_pake_name:I = 0x7f13074c

.field public static final cs_514_id_pake_ocr:I = 0x7f13074d

.field public static final cs_514_id_pake_ocr_detil:I = 0x7f13074e

.field public static final cs_514_id_pake_ocr_number:I = 0x7f13074f

.field public static final cs_514_id_pake_pack_up:I = 0x7f130750

.field public static final cs_514_id_pake_period_of_validity:I = 0x7f130751

.field public static final cs_514_id_pake_quasi_drivie_type:I = 0x7f130752

.field public static final cs_514_id_pake_regist_date:I = 0x7f130753

.field public static final cs_514_id_pake_release_date:I = 0x7f130754

.field public static final cs_514_id_pake_vehicle:I = 0x7f130755

.field public static final cs_514_idcard_security:I = 0x7f130756

.field public static final cs_514_identity_card:I = 0x7f130757

.field public static final cs_514_invite_left:I = 0x7f130758

.field public static final cs_514_invite_left_entrance:I = 0x7f130759

.field public static final cs_514_invite_vip_pop:I = 0x7f13075a

.field public static final cs_514_invite_vip_tips:I = 0x7f13075b

.field public static final cs_514_license_plate_number:I = 0x7f13075c

.field public static final cs_514_life_member:I = 0x7f13075d

.field public static final cs_514_more_features:I = 0x7f13075e

.field public static final cs_514_my_id_package:I = 0x7f13075f

.field public static final cs_514_ocr_lose_edit:I = 0x7f130760

.field public static final cs_514_ocr_update_content:I = 0x7f130761

.field public static final cs_514_others_login:I = 0x7f130762

.field public static final cs_514_passport_card:I = 0x7f130763

.field public static final cs_514_pdf:I = 0x7f130764

.field public static final cs_514_pdf_extract:I = 0x7f130765

.field public static final cs_514_pdf_import_delete_fail:I = 0x7f130766

.field public static final cs_514_pdf_select:I = 0x7f130767

.field public static final cs_514_pdf_to_word:I = 0x7f130768

.field public static final cs_514_pdf_view:I = 0x7f130769

.field public static final cs_514_pdf_word:I = 0x7f13076a

.field public static final cs_514_pinyin:I = 0x7f13076b

.field public static final cs_514_premium_close_improve:I = 0x7f13076c

.field public static final cs_514_premium_close_one_burger:I = 0x7f13076d

.field public static final cs_514_premium_close_one_step:I = 0x7f13076e

.field public static final cs_514_premium_no_thank:I = 0x7f13076f

.field public static final cs_514_reload_login:I = 0x7f130770

.field public static final cs_514_save_fail:I = 0x7f130771

.field public static final cs_514_save_phone:I = 0x7f130772

.field public static final cs_514_three_billed:I = 0x7f130773

.field public static final cs_514_to_be_added:I = 0x7f130774

.field public static final cs_514_transfer_file:I = 0x7f130775

.field public static final cs_514_transfer_save_tips:I = 0x7f130776

.field public static final cs_514_transfer_word_fail_toast:I = 0x7f130777

.field public static final cs_514_verification_login:I = 0x7f130778

.field public static final cs_514_wechat_login:I = 0x7f130779

.field public static final cs_514_wechat_login_network_fail:I = 0x7f13077a

.field public static final cs_515_buy_setting_othters_07:I = 0x7f13077b

.field public static final cs_515_christmas_02:I = 0x7f13077c

.field public static final cs_515_eco_ok:I = 0x7f13077d

.field public static final cs_515_guide_video_05:I = 0x7f13077e

.field public static final cs_515_hint_hold_phone:I = 0x7f13077f

.field public static final cs_515_import_cs:I = 0x7f130780

.field public static final cs_515_input_url:I = 0x7f130781

.field public static final cs_515_login_get_free_quota:I = 0x7f130782

.field public static final cs_515_now_scan:I = 0x7f130783

.field public static final cs_515_pdf_signature_limit_tips:I = 0x7f130784

.field public static final cs_515_premiumpage_folder:I = 0x7f130785

.field public static final cs_515_rename_picture:I = 0x7f130786

.field public static final cs_515_scan_ercode:I = 0x7f130787

.field public static final cs_515_send_success:I = 0x7f130788

.field public static final cs_515_send_success_pc:I = 0x7f130789

.field public static final cs_515_send_to_pc:I = 0x7f13078a

.field public static final cs_515_send_to_pc_url:I = 0x7f13078b

.field public static final cs_515_share_document_green_points:I = 0x7f13078c

.field public static final cs_515_sync_latter:I = 0x7f13078d

.field public static final cs_515_sync_little:I = 0x7f13078e

.field public static final cs_515_sync_login:I = 0x7f13078f

.field public static final cs_515_sync_message:I = 0x7f130790

.field public static final cs_515_title_auto_rotate:I = 0x7f130791

.field public static final cs_515_warning_delete_pictures:I = 0x7f130792

.field public static final cs_516_24hdiscountpop_01:I = 0x7f130793

.field public static final cs_516_24hdiscountpop_02:I = 0x7f130794

.field public static final cs_516_24hdiscountpop_03:I = 0x7f130795

.field public static final cs_516_24hdiscountpop_04:I = 0x7f130796

.field public static final cs_516_24hdiscountpop_05:I = 0x7f130797

.field public static final cs_516_24hdiscountpop_06:I = 0x7f130798

.field public static final cs_516_24hdiscountpop_07:I = 0x7f130799

.field public static final cs_516_24hdiscountpop_08:I = 0x7f13079a

.field public static final cs_516_24hdiscountpop_09:I = 0x7f13079b

.field public static final cs_516_capacity_setting_export:I = 0x7f13079c

.field public static final cs_516_giftcard_07:I = 0x7f13079d

.field public static final cs_516_giftcard_10:I = 0x7f13079e

.field public static final cs_516_giftcard_25:I = 0x7f13079f

.field public static final cs_516_guidenew_01:I = 0x7f1307a0

.field public static final cs_516_id_photo_policy1:I = 0x7f1307a1

.field public static final cs_516_id_photo_policy2:I = 0x7f1307a2

.field public static final cs_516_left_premium_lable_type_others:I = 0x7f1307a3

.field public static final cs_516_link_share:I = 0x7f1307a4

.field public static final cs_516_message_order_illegal:I = 0x7f1307a5

.field public static final cs_516_not_paid_for_exclusive_order:I = 0x7f1307a6

.field public static final cs_516_ocr_auto_segment:I = 0x7f1307a7

.field public static final cs_516_ocr_expot_to_txt:I = 0x7f1307a8

.field public static final cs_516_ocr_expot_to_word:I = 0x7f1307a9

.field public static final cs_516_ocr_send_text:I = 0x7f1307aa

.field public static final cs_516_other_export_method:I = 0x7f1307ab

.field public static final cs_516_premium_failed_googlePlay:I = 0x7f1307ac

.field public static final cs_516_share_as_txt:I = 0x7f1307ad

.field public static final cs_516_share_as_word:I = 0x7f1307ae

.field public static final cs_516_silm_down_link:I = 0x7f1307af

.field public static final cs_516_tasklist_00:I = 0x7f1307b0

.field public static final cs_516_tasklist_11:I = 0x7f1307b1

.field public static final cs_516_tasklist_13:I = 0x7f1307b2

.field public static final cs_516_tasklist_14:I = 0x7f1307b3

.field public static final cs_516_tasklist_15:I = 0x7f1307b4

.field public static final cs_516_tasklist_16:I = 0x7f1307b5

.field public static final cs_516_tasklist_17:I = 0x7f1307b6

.field public static final cs_516_tasklist_mi:I = 0x7f1307b7

.field public static final cs_516_tasklist_pre:I = 0x7f1307b8

.field public static final cs_516_tasklist_share:I = 0x7f1307b9

.field public static final cs_516_tasklist_um:I = 0x7f1307ba

.field public static final cs_516_toast_ocr_area:I = 0x7f1307bb

.field public static final cs_516_whatsnew_01:I = 0x7f1307bc

.field public static final cs_5175c_all_pages:I = 0x7f1307bd

.field public static final cs_5175c_current_page:I = 0x7f1307be

.field public static final cs_5175c_tap_to_edit_text:I = 0x7f1307bf

.field public static final cs_517_bottom_words:I = 0x7f1307c0

.field public static final cs_517_bottom_words_modify:I = 0x7f1307c1

.field public static final cs_517_change_area_code:I = 0x7f1307c2

.field public static final cs_517_clarify_double_focus:I = 0x7f1307c3

.field public static final cs_517_compressionl:I = 0x7f1307c4

.field public static final cs_517_compressionl_popup:I = 0x7f1307c5

.field public static final cs_517_compressionm:I = 0x7f1307c6

.field public static final cs_517_compressions:I = 0x7f1307c7

.field public static final cs_517_double_focus:I = 0x7f1307c8

.field public static final cs_517_files_manager:I = 0x7f1307c9

.field public static final cs_517_hms_buy_success:I = 0x7f1307ca

.field public static final cs_517_household_page:I = 0x7f1307cb

.field public static final cs_517_household_page_tip:I = 0x7f1307cc

.field public static final cs_517_hukou_collage:I = 0x7f1307cd

.field public static final cs_517_long_photo:I = 0x7f1307ce

.field public static final cs_517_long_photo_fail_tips:I = 0x7f1307cf

.field public static final cs_517_long_photo_premium_title:I = 0x7f1307d0

.field public static final cs_517_long_photo_share:I = 0x7f1307d1

.field public static final cs_517_multipledocs_button:I = 0x7f1307d2

.field public static final cs_517_multipledocs_popup1:I = 0x7f1307d3

.field public static final cs_517_multipledocs_popup2:I = 0x7f1307d4

.field public static final cs_517_personal_detail_page:I = 0x7f1307d5

.field public static final cs_517_send_code:I = 0x7f1307d6

.field public static final cs_517_share_success_pop_continue:I = 0x7f1307d7

.field public static final cs_517_sponsor_scan_pop1:I = 0x7f1307d8

.field public static final cs_517_verify_area_code:I = 0x7f1307d9

.field public static final cs_517_verify_area_code_2:I = 0x7f1307da

.field public static final cs_517_verify_area_code_3:I = 0x7f1307db

.field public static final cs_517_water_color:I = 0x7f1307dc

.field public static final cs_517_water_size:I = 0x7f1307dd

.field public static final cs_517_whats_new_pdf_import_edit_content:I = 0x7f1307de

.field public static final cs_518_ad_close_ad:I = 0x7f1307df

.field public static final cs_518_import_files_intrance:I = 0x7f1307e0

.field public static final cs_518_ppt_fail:I = 0x7f1307e1

.field public static final cs_518_recommend:I = 0x7f1307e2

.field public static final cs_518a_download_vk:I = 0x7f1307e3

.field public static final cs_518a_email_share_signature:I = 0x7f1307e4

.field public static final cs_518a_free_cloud_vk:I = 0x7f1307e5

.field public static final cs_518a_guide_resubscribe_01:I = 0x7f1307e6

.field public static final cs_518a_guide_resubscribe_02:I = 0x7f1307e7

.field public static final cs_518a_guide_resubscribe_04:I = 0x7f1307e8

.field public static final cs_518a_share_link_to_vk_content:I = 0x7f1307e9

.field public static final cs_518a_user_active_sharing_to_vk_android:I = 0x7f1307ea

.field public static final cs_518a_whatsapp:I = 0x7f1307eb

.field public static final cs_518b_add_page:I = 0x7f1307ec

.field public static final cs_518b_add_team:I = 0x7f1307ed

.field public static final cs_518b_adjust_order:I = 0x7f1307ee

.field public static final cs_518b_cancel_transfer:I = 0x7f1307ef

.field public static final cs_518b_compression_done:I = 0x7f1307f0

.field public static final cs_518b_extract_new_doc:I = 0x7f1307f1

.field public static final cs_518b_extract_page:I = 0x7f1307f2

.field public static final cs_518b_feedback:I = 0x7f1307f3

.field public static final cs_518b_invite_team_member:I = 0x7f1307f4

.field public static final cs_518b_invite_to_join:I = 0x7f1307f5

.field public static final cs_518b_last_login:I = 0x7f1307f6

.field public static final cs_518b_login_error_area_and_number_not_match:I = 0x7f1307f7

.field public static final cs_518b_login_error_area_code_not_supported:I = 0x7f1307f8

.field public static final cs_518b_meber_collaboration:I = 0x7f1307f9

.field public static final cs_518b_merge:I = 0x7f1307fa

.field public static final cs_518b_new_merge_doc:I = 0x7f1307fb

.field public static final cs_518b_pc:I = 0x7f1307fc

.field public static final cs_518b_pdf_compression:I = 0x7f1307fd

.field public static final cs_518b_pdf_extract:I = 0x7f1307fe

.field public static final cs_518b_pdf_image:I = 0x7f1307ff

.field public static final cs_518b_pdf_long_picture:I = 0x7f130800

.field public static final cs_518b_pdf_merge:I = 0x7f130801

.field public static final cs_518b_pdf_password_again:I = 0x7f130802

.field public static final cs_518b_pdf_privileges:I = 0x7f130803

.field public static final cs_518b_pdf_pro:I = 0x7f130804

.field public static final cs_518b_pdf_signature:I = 0x7f130805

.field public static final cs_518b_pdf_tools:I = 0x7f130806

.field public static final cs_518b_pdf_transfer_word_pop:I = 0x7f130807

.field public static final cs_518b_pdf_water:I = 0x7f130808

.field public static final cs_518b_select_document:I = 0x7f130809

.field public static final cs_518b_transfer_success:I = 0x7f13080a

.field public static final cs_518b_ugprade_renewal:I = 0x7f13080b

.field public static final cs_518b_user_manual:I = 0x7f13080c

.field public static final cs_518b_welcome_team:I = 0x7f13080d

.field public static final cs_518c_apply_to_all:I = 0x7f13080e

.field public static final cs_518c_batch_process:I = 0x7f13080f

.field public static final cs_518c_batch_process_warning:I = 0x7f130810

.field public static final cs_518c_deleted:I = 0x7f130811

.field public static final cs_518c_move_close:I = 0x7f130812

.field public static final cs_518c_my_account:I = 0x7f130813

.field public static final cs_518c_no_doc:I = 0x7f130814

.field public static final cs_518c_not_move:I = 0x7f130815

.field public static final cs_518c_ocr_clarify:I = 0x7f130816

.field public static final cs_518c_off:I = 0x7f130817

.field public static final cs_518c_on:I = 0x7f130818

.field public static final cs_518c_photo_missing_1:I = 0x7f130819

.field public static final cs_518c_photo_missing_2:I = 0x7f13081a

.field public static final cs_518c_quit_warning:I = 0x7f13081b

.field public static final cs_518c_quit_without_save:I = 0x7f13081c

.field public static final cs_518c_search_docs:I = 0x7f13081d

.field public static final cs_518c_select_filter:I = 0x7f13081e

.field public static final cs_519_pc_edit_ocr_url:I = 0x7f13081f

.field public static final cs_519_web_login_to_pc_url:I = 0x7f130820

.field public static final cs_519a_cancelanytime:I = 0x7f130821

.field public static final cs_519a_download_app_first:I = 0x7f130822

.field public static final cs_519a_kakao_talk_title:I = 0x7f130823

.field public static final cs_519a_line_title:I = 0x7f130824

.field public static final cs_519a_messenger_lite_title:I = 0x7f130825

.field public static final cs_519a_messenger_title:I = 0x7f130826

.field public static final cs_519a_resubscribe_01:I = 0x7f130827

.field public static final cs_519a_resubscribe_02:I = 0x7f130828

.field public static final cs_519a_viber_title:I = 0x7f130829

.field public static final cs_519a_whatsapp_business_title:I = 0x7f13082a

.field public static final cs_519a_zalo_title:I = 0x7f13082b

.field public static final cs_519b_2_factor:I = 0x7f13082c

.field public static final cs_519b_2_factor_explanation:I = 0x7f13082d

.field public static final cs_519b_2_factor_note:I = 0x7f13082e

.field public static final cs_519b_2_factor_once:I = 0x7f13082f

.field public static final cs_519b_2_factor_open:I = 0x7f130830

.field public static final cs_519b_account_forzen:I = 0x7f130831

.field public static final cs_519b_auto_register:I = 0x7f130832

.field public static final cs_519b_code_erro:I = 0x7f130833

.field public static final cs_519b_code_erro_3:I = 0x7f130834

.field public static final cs_519b_code_erro_31:I = 0x7f130835

.field public static final cs_519b_code_erro_5:I = 0x7f130836

.field public static final cs_519b_code_erro_51:I = 0x7f130837

.field public static final cs_519b_code_login:I = 0x7f130838

.field public static final cs_519b_confirm_country:I = 0x7f130839

.field public static final cs_519b_contact_service:I = 0x7f13083a

.field public static final cs_519b_encryption:I = 0x7f13083b

.field public static final cs_519b_find_account:I = 0x7f13083c

.field public static final cs_519b_find_passcode:I = 0x7f13083d

.field public static final cs_519b_forget_recovery_code:I = 0x7f13083e

.field public static final cs_519b_freeze:I = 0x7f13083f

.field public static final cs_519b_huawei_unavailable:I = 0x7f130840

.field public static final cs_519b_identify_verification:I = 0x7f130841

.field public static final cs_519b_invalid_number:I = 0x7f130842

.field public static final cs_519b_invalid_super_verification_code:I = 0x7f130843

.field public static final cs_519b_jpg_share:I = 0x7f130844

.field public static final cs_519b_link_share:I = 0x7f130845

.field public static final cs_519b_login_pc:I = 0x7f130846

.field public static final cs_519b_login_register:I = 0x7f130847

.field public static final cs_519b_more:I = 0x7f130848

.field public static final cs_519b_network_delay:I = 0x7f130849

.field public static final cs_519b_no_verify_code:I = 0x7f13084a

.field public static final cs_519b_noaccess_account:I = 0x7f13084b

.field public static final cs_519b_others_share:I = 0x7f13084c

.field public static final cs_519b_pc_edit:I = 0x7f13084d

.field public static final cs_519b_pdf_share:I = 0x7f13084e

.field public static final cs_519b_recovery_wrong_3:I = 0x7f13084f

.field public static final cs_519b_register_verify:I = 0x7f130850

.field public static final cs_519b_screencut:I = 0x7f130851

.field public static final cs_519b_security_warning:I = 0x7f130852

.field public static final cs_519b_super_verification_code:I = 0x7f130853

.field public static final cs_519b_super_verification_code_note:I = 0x7f130854

.field public static final cs_519b_sync_unbind_tips:I = 0x7f130855

.field public static final cs_519b_try_pc_ocr:I = 0x7f130856

.field public static final cs_519b_txt_pc_sync:I = 0x7f130857

.field public static final cs_519b_txt_share:I = 0x7f130858

.field public static final cs_519b_use_super_verification_code:I = 0x7f130859

.field public static final cs_519b_vcode_sendto:I = 0x7f13085a

.field public static final cs_519b_verify_identity:I = 0x7f13085b

.field public static final cs_519b_word_share:I = 0x7f13085c

.field public static final cs_519c_auto_capture_answer1:I = 0x7f13085d

.field public static final cs_519c_auto_capture_answer2:I = 0x7f13085e

.field public static final cs_519c_auto_capture_answer3:I = 0x7f13085f

.field public static final cs_519c_auto_capture_answer4:I = 0x7f130860

.field public static final cs_519c_auto_capture_answer5:I = 0x7f130861

.field public static final cs_519c_auto_capture_question:I = 0x7f130862

.field public static final cs_519c_auto_capture_question2:I = 0x7f130863

.field public static final cs_519c_capture_3_items:I = 0x7f130864

.field public static final cs_519c_clear_success:I = 0x7f130865

.field public static final cs_519c_fill:I = 0x7f130866

.field public static final cs_519c_network_fail:I = 0x7f130867

.field public static final cs_519c_ocr_credit_not_enough1:I = 0x7f130868

.field public static final cs_519c_ocr_credit_not_enough2:I = 0x7f130869

.field public static final cs_519c_ocr_horizonal_print:I = 0x7f13086a

.field public static final cs_519c_ocr_purchase_credit:I = 0x7f13086b

.field public static final cs_519c_ocr_re_crop:I = 0x7f13086c

.field public static final cs_519c_ocr_re_crop2:I = 0x7f13086d

.field public static final cs_519c_ocr_select:I = 0x7f13086e

.field public static final cs_519c_ocr_upgrade:I = 0x7f13086f

.field public static final cs_519c_others:I = 0x7f130870

.field public static final cs_519c_pop_archive_file:I = 0x7f130871

.field public static final cs_519c_pop_close_archive_page:I = 0x7f130872

.field public static final cs_519c_pop_remain_archive_page:I = 0x7f130873

.field public static final cs_519c_purchase_c_succeed:I = 0x7f130874

.field public static final cs_519c_question_great:I = 0x7f130875

.field public static final cs_519c_question_not_good:I = 0x7f130876

.field public static final cs_519c_recommedation_task:I = 0x7f130877

.field public static final cs_519c_thanks:I = 0x7f130878

.field public static final cs_5205c_auto_capture:I = 0x7f130879

.field public static final cs_5207_once_vip_pop_cancel:I = 0x7f13087a

.field public static final cs_5207_once_vip_pop_msg:I = 0x7f13087b

.field public static final cs_5207_once_vip_pop_ok:I = 0x7f13087c

.field public static final cs_520_a_key_login:I = 0x7f13087d

.field public static final cs_520_b_cloud_download_app:I = 0x7f13087e

.field public static final cs_520_b_cloud_files:I = 0x7f13087f

.field public static final cs_520_b_ppt_net_tips:I = 0x7f130880

.field public static final cs_520_b_transfer_success_tips:I = 0x7f130881

.field public static final cs_520_edu_unlocked:I = 0x7f130882

.field public static final cs_520_folder_empty_tip:I = 0x7f130883

.field public static final cs_520_folder_limit:I = 0x7f130884

.field public static final cs_520_guide_new01:I = 0x7f130885

.field public static final cs_520_guide_new02:I = 0x7f130886

.field public static final cs_520_guide_new03:I = 0x7f130887

.field public static final cs_520_import_ppt_toast:I = 0x7f130888

.field public static final cs_520_move_folder:I = 0x7f130889

.field public static final cs_520_new_folder:I = 0x7f13088a

.field public static final cs_520_no_folder:I = 0x7f13088b

.field public static final cs_520_title_edu_unlocked:I = 0x7f13088c

.field public static final cs_520_title_failed_to_change:I = 0x7f13088d

.field public static final cs_520_upload_box:I = 0x7f13088e

.field public static final cs_520_upload_dropbox:I = 0x7f13088f

.field public static final cs_520_upload_evernote:I = 0x7f130890

.field public static final cs_520_upload_google_drive:I = 0x7f130891

.field public static final cs_520_upload_one_drive:I = 0x7f130892

.field public static final cs_520_upload_one_note:I = 0x7f130893

.field public static final cs_520_vip_guide01:I = 0x7f130894

.field public static final cs_520_vip_guide02:I = 0x7f130895

.field public static final cs_520a_change_email:I = 0x7f130896

.field public static final cs_520a_change_email_hint:I = 0x7f130897

.field public static final cs_520a_change_email_hint2:I = 0x7f130898

.field public static final cs_520a_change_number_hint:I = 0x7f130899

.field public static final cs_520a_change_phone_number:I = 0x7f13089a

.field public static final cs_520a_error_vcode_expired:I = 0x7f13089b

.field public static final cs_520a_input_phone_number:I = 0x7f13089c

.field public static final cs_520a_pop_detail:I = 0x7f13089d

.field public static final cs_520b_last_share:I = 0x7f13089e

.field public static final cs_520b_ppt_hide:I = 0x7f13089f

.field public static final cs_520b_ppt_up_tisp:I = 0x7f1308a0

.field public static final cs_520b_processing_tips:I = 0x7f1308a1

.field public static final cs_520c_edu:I = 0x7f1308a2

.field public static final cs_520c_edu_benefit1:I = 0x7f1308a3

.field public static final cs_520c_edu_benefit2:I = 0x7f1308a4

.field public static final cs_520c_edu_benefit3:I = 0x7f1308a5

.field public static final cs_520c_edu_benefit4:I = 0x7f1308a6

.field public static final cs_520c_edu_benefit5:I = 0x7f1308a7

.field public static final cs_520c_edu_benefit6:I = 0x7f1308a8

.field public static final cs_520c_edu_collect:I = 0x7f1308a9

.field public static final cs_520c_edu_duration:I = 0x7f1308aa

.field public static final cs_520c_edu_share_msg_body:I = 0x7f1308ab

.field public static final cs_520c_edu_share_msg_title:I = 0x7f1308ac

.field public static final cs_520c_edu_success1:I = 0x7f1308ad

.field public static final cs_520c_edu_success2:I = 0x7f1308ae

.field public static final cs_520c_ocr_guide:I = 0x7f1308af

.field public static final cs_520c_picture_limit_free:I = 0x7f1308b0

.field public static final cs_5213_album:I = 0x7f1308b1

.field public static final cs_5213_hint_modify_ocr_language:I = 0x7f1308b2

.field public static final cs_5213_reminder:I = 0x7f1308b3

.field public static final cs_521_b_refer:I = 0x7f1308b4

.field public static final cs_521_b_transfer_fail:I = 0x7f1308b5

.field public static final cs_521_b_transfer_free_trial:I = 0x7f1308b6

.field public static final cs_521_b_transfer_guide:I = 0x7f1308b7

.field public static final cs_521_b_transfer_loading:I = 0x7f1308b8

.field public static final cs_521_b_transfer_notify:I = 0x7f1308b9

.field public static final cs_521_b_transfer_success:I = 0x7f1308ba

.field public static final cs_521_b_transfer_view:I = 0x7f1308bb

.field public static final cs_521_b_transfering:I = 0x7f1308bc

.field public static final cs_521_b_word_waiting:I = 0x7f1308bd

.field public static final cs_521_batch_tips:I = 0x7f1308be

.field public static final cs_521_birth_certificate_new:I = 0x7f1308bf

.field public static final cs_521_button_next:I = 0x7f1308c0

.field public static final cs_521_button_smudge:I = 0x7f1308c1

.field public static final cs_521_button_use_color:I = 0x7f1308c2

.field public static final cs_521_collage_01:I = 0x7f1308c3

.field public static final cs_521_date:I = 0x7f1308c4

.field public static final cs_521_disability_certificate:I = 0x7f1308c5

.field public static final cs_521_download_layer_tip1:I = 0x7f1308c6

.field public static final cs_521_download_layer_tip2:I = 0x7f1308c7

.field public static final cs_521_download_layer_tip3:I = 0x7f1308c8

.field public static final cs_521_download_layer_tip4:I = 0x7f1308c9

.field public static final cs_521_download_layer_tip5:I = 0x7f1308ca

.field public static final cs_521_download_layer_tip6:I = 0x7f1308cb

.field public static final cs_521_download_layer_tip7:I = 0x7f1308cc

.field public static final cs_521_guide_move_circle:I = 0x7f1308cd

.field public static final cs_521_id_mode_hint:I = 0x7f1308ce

.field public static final cs_521_id_mode_hint2:I = 0x7f1308cf

.field public static final cs_521_id_mode_tips2:I = 0x7f1308d0

.field public static final cs_521_id_mode_tips3:I = 0x7f1308d1

.field public static final cs_521_id_mode_tips4:I = 0x7f1308d2

.field public static final cs_521_id_tips:I = 0x7f1308d3

.field public static final cs_521_lawyer_license:I = 0x7f1308d4

.field public static final cs_521_link_date:I = 0x7f1308d5

.field public static final cs_521_link_setting:I = 0x7f1308d6

.field public static final cs_521_modify_pdf_auto:I = 0x7f1308d7

.field public static final cs_521_ocr_layer_tip1:I = 0x7f1308d8

.field public static final cs_521_ocr_layer_tip2:I = 0x7f1308d9

.field public static final cs_521_ocr_layer_tip3:I = 0x7f1308da

.field public static final cs_521_ocr_layer_tip4:I = 0x7f1308db

.field public static final cs_521_ocr_layer_tip5:I = 0x7f1308dc

.field public static final cs_521_process_layer_tip1:I = 0x7f1308dd

.field public static final cs_521_process_layer_tip2:I = 0x7f1308de

.field public static final cs_521_process_layer_tip3:I = 0x7f1308df

.field public static final cs_521_process_layer_tip4:I = 0x7f1308e0

.field public static final cs_521_resubscription_reason:I = 0x7f1308e1

.field public static final cs_521_resubscription_reason01:I = 0x7f1308e2

.field public static final cs_521_resubscription_reason02:I = 0x7f1308e3

.field public static final cs_521_resubscription_reason03:I = 0x7f1308e4

.field public static final cs_521_resubscription_reteach1_1:I = 0x7f1308e5

.field public static final cs_521_resubscription_reteach1_2:I = 0x7f1308e6

.field public static final cs_521_resubscription_reteach2_1:I = 0x7f1308e7

.field public static final cs_521_resubscription_reteach2_2:I = 0x7f1308e8

.field public static final cs_521_resubscription_reteach3_1:I = 0x7f1308e9

.field public static final cs_521_resubscription_reteach3_2:I = 0x7f1308ea

.field public static final cs_521_resubscription_reteach4_1:I = 0x7f1308eb

.field public static final cs_521_resubscription_reteach4_2:I = 0x7f1308ec

.field public static final cs_521_resubscription_reteach5_1:I = 0x7f1308ed

.field public static final cs_521_resubscription_reteach5_2:I = 0x7f1308ee

.field public static final cs_521_resubscription_reteach6:I = 0x7f1308ef

.field public static final cs_521_save_files:I = 0x7f1308f0

.field public static final cs_521_share_link_link:I = 0x7f1308f1

.field public static final cs_521_sign_cancel:I = 0x7f1308f2

.field public static final cs_521_sign_comfirm:I = 0x7f1308f3

.field public static final cs_521_use_now:I = 0x7f1308f4

.field public static final cs_521_web_log_in:I = 0x7f1308f5

.field public static final cs_521_web_sign_tip:I = 0x7f1308f6

.field public static final cs_5223_button_auto_crop:I = 0x7f1308f7

.field public static final cs_5223_content_auto_crop:I = 0x7f1308f8

.field public static final cs_5223_pc_login_failed:I = 0x7f1308f9

.field public static final cs_5223_pc_send_failed:I = 0x7f1308fa

.field public static final cs_5223_signature_description_01:I = 0x7f1308fb

.field public static final cs_5223_title_auto_crop:I = 0x7f1308fc

.field public static final cs_5223_title_nps:I = 0x7f1308fd

.field public static final cs_5225_newguide1:I = 0x7f1308fe

.field public static final cs_5225_newguide10:I = 0x7f1308ff

.field public static final cs_5225_newguide11:I = 0x7f130900

.field public static final cs_5225_newguide12:I = 0x7f130901

.field public static final cs_5225_newguide13:I = 0x7f130902

.field public static final cs_5225_newguide14:I = 0x7f130903

.field public static final cs_5225_newguide15:I = 0x7f130904

.field public static final cs_5225_newguide16:I = 0x7f130905

.field public static final cs_5225_newguide17:I = 0x7f130906

.field public static final cs_5225_newguide18:I = 0x7f130907

.field public static final cs_5225_newguide19:I = 0x7f130908

.field public static final cs_5225_newguide2:I = 0x7f130909

.field public static final cs_5225_newguide20:I = 0x7f13090a

.field public static final cs_5225_newguide21:I = 0x7f13090b

.field public static final cs_5225_newguide3:I = 0x7f13090c

.field public static final cs_5225_newguide4:I = 0x7f13090d

.field public static final cs_5225_newguide5:I = 0x7f13090e

.field public static final cs_5225_newguide6:I = 0x7f13090f

.field public static final cs_5225_newguide7:I = 0x7f130910

.field public static final cs_5225_newguide8:I = 0x7f130911

.field public static final cs_5225_newguide9:I = 0x7f130912

.field public static final cs_5225_premiumpop1:I = 0x7f130913

.field public static final cs_5225_premiumpop2:I = 0x7f130914

.field public static final cs_5225_premiumpop3:I = 0x7f130915

.field public static final cs_5225_premiumpop5:I = 0x7f130916

.field public static final cs_5225_premiumpop6:I = 0x7f130917

.field public static final cs_522_add:I = 0x7f130918

.field public static final cs_522_add_text:I = 0x7f130919

.field public static final cs_522_analysis:I = 0x7f13091a

.field public static final cs_522_easter_egg1:I = 0x7f13091b

.field public static final cs_522_easter_egg2:I = 0x7f13091c

.field public static final cs_522_easter_egg3:I = 0x7f13091d

.field public static final cs_522_easter_egg4:I = 0x7f13091e

.field public static final cs_522_email_signature:I = 0x7f13091f

.field public static final cs_522_email_signature_edit:I = 0x7f130920

.field public static final cs_522_email_signature_explanation:I = 0x7f130921

.field public static final cs_522_excel_ocr:I = 0x7f130922

.field public static final cs_522_giftcard_popup:I = 0x7f130923

.field public static final cs_522_giftcard_premium:I = 0x7f130924

.field public static final cs_522_guide_add_text:I = 0x7f130925

.field public static final cs_522_manual_sort_title:I = 0x7f130926

.field public static final cs_522_merge_limit:I = 0x7f130927

.field public static final cs_522_not_log_link_tip:I = 0x7f130928

.field public static final cs_522_open_analysis:I = 0x7f130929

.field public static final cs_522_open_analysis_detail:I = 0x7f13092a

.field public static final cs_522_ppt_nom:I = 0x7f13092b

.field public static final cs_522_privacy_settings:I = 0x7f13092c

.field public static final cs_522_renewal_reminder1:I = 0x7f13092d

.field public static final cs_522_renewal_reminder2:I = 0x7f13092e

.field public static final cs_522_renewal_reminder5:I = 0x7f13092f

.field public static final cs_522_text_ocr:I = 0x7f130930

.field public static final cs_522_web_01:I = 0x7f130931

.field public static final cs_522_web_02:I = 0x7f130932

.field public static final cs_522_web_03:I = 0x7f130933

.field public static final cs_522_web_04:I = 0x7f130934

.field public static final cs_522_web_05:I = 0x7f130935

.field public static final cs_522_web_06:I = 0x7f130936

.field public static final cs_522_web_07:I = 0x7f130937

.field public static final cs_522_web_08:I = 0x7f130938

.field public static final cs_522_web_09:I = 0x7f130939

.field public static final cs_522_web_10:I = 0x7f13093a

.field public static final cs_522_web_11:I = 0x7f13093b

.field public static final cs_522_web_12:I = 0x7f13093c

.field public static final cs_522_web_13:I = 0x7f13093d

.field public static final cs_522_web_14:I = 0x7f13093e

.field public static final cs_522_web_15:I = 0x7f13093f

.field public static final cs_522_web_16:I = 0x7f130940

.field public static final cs_522_web_17:I = 0x7f130941

.field public static final cs_522_web_18:I = 0x7f130942

.field public static final cs_522_web_19:I = 0x7f130943

.field public static final cs_522_web_20:I = 0x7f130944

.field public static final cs_522_web_21:I = 0x7f130945

.field public static final cs_522_web_22:I = 0x7f130946

.field public static final cs_522_web_23:I = 0x7f130947

.field public static final cs_522_web_24:I = 0x7f130948

.field public static final cs_522_web_26:I = 0x7f130949

.field public static final cs_522_web_27:I = 0x7f13094a

.field public static final cs_522_web_28:I = 0x7f13094b

.field public static final cs_522_web_29:I = 0x7f13094c

.field public static final cs_522_web_30:I = 0x7f13094d

.field public static final cs_522_web_32:I = 0x7f13094e

.field public static final cs_522_web_33:I = 0x7f13094f

.field public static final cs_522_web_34:I = 0x7f130950

.field public static final cs_522_web_35:I = 0x7f130951

.field public static final cs_522_web_36:I = 0x7f130952

.field public static final cs_522_web_37:I = 0x7f130953

.field public static final cs_522_web_38:I = 0x7f130954

.field public static final cs_522_web_39:I = 0x7f130955

.field public static final cs_522_web_40:I = 0x7f130956

.field public static final cs_522_web_41:I = 0x7f130957

.field public static final cs_522_web_44:I = 0x7f130958

.field public static final cs_522_web_45:I = 0x7f130959

.field public static final cs_522a_comment:I = 0x7f13095a

.field public static final cs_522a_comment_details:I = 0x7f13095b

.field public static final cs_522a_comment_guide:I = 0x7f13095c

.field public static final cs_522a_comment_hr01:I = 0x7f13095d

.field public static final cs_522a_comment_hr02:I = 0x7f13095e

.field public static final cs_522a_comment_hr03:I = 0x7f13095f

.field public static final cs_522a_comment_manager01:I = 0x7f130960

.field public static final cs_522a_comment_manager02:I = 0x7f130961

.field public static final cs_522a_comment_manager03:I = 0x7f130962

.field public static final cs_522a_comment_previliages:I = 0x7f130963

.field public static final cs_522a_comment_sales01:I = 0x7f130964

.field public static final cs_522a_comment_sales03:I = 0x7f130965

.field public static final cs_522a_comment_sales04:I = 0x7f130966

.field public static final cs_522a_comment_student01:I = 0x7f130967

.field public static final cs_522a_comment_student02:I = 0x7f130968

.field public static final cs_522a_comment_student03:I = 0x7f130969

.field public static final cs_522a_comment_worker01:I = 0x7f13096a

.field public static final cs_522a_comment_worker02:I = 0x7f13096b

.field public static final cs_522b_import_fail:I = 0x7f13096c

.field public static final cs_522b_noads:I = 0x7f13096d

.field public static final cs_522b_picture_txt:I = 0x7f13096e

.field public static final cs_522b_qa_filter:I = 0x7f13096f

.field public static final cs_522b_qa_filter_tips:I = 0x7f130970

.field public static final cs_522b_skip:I = 0x7f130971

.field public static final cs_522b_view:I = 0x7f130972

.field public static final cs_5235_month:I = 0x7f130973

.field public static final cs_5235_nowatermark:I = 0x7f130974

.field public static final cs_5235_single_id:I = 0x7f130975

.field public static final cs_5235_toexcel:I = 0x7f130976

.field public static final cs_5235_topdf:I = 0x7f130977

.field public static final cs_523_body_switch_filter:I = 0x7f130978

.field public static final cs_523_button_smudge:I = 0x7f130979

.field public static final cs_523_clear:I = 0x7f13097a

.field public static final cs_523_college_click:I = 0x7f13097b

.field public static final cs_523_college_click2:I = 0x7f13097c

.field public static final cs_523_copy_fail:I = 0x7f13097d

.field public static final cs_523_copy_success:I = 0x7f13097e

.field public static final cs_523_excel_contents:I = 0x7f13097f

.field public static final cs_523_friends_link:I = 0x7f130980

.field public static final cs_523_label_authentic:I = 0x7f130981

.field public static final cs_523_label_button:I = 0x7f130982

.field public static final cs_523_label_cerk:I = 0x7f130983

.field public static final cs_523_label_certificate:I = 0x7f130984

.field public static final cs_523_label_certificate_01:I = 0x7f130985

.field public static final cs_523_label_certificate_02:I = 0x7f130986

.field public static final cs_523_label_college_student:I = 0x7f130987

.field public static final cs_523_label_details:I = 0x7f130988

.field public static final cs_523_label_doctor:I = 0x7f130989

.field public static final cs_523_label_engineer:I = 0x7f13098a

.field public static final cs_523_label_finance:I = 0x7f13098b

.field public static final cs_523_label_government:I = 0x7f13098c

.field public static final cs_523_label_jpegtopdf:I = 0x7f13098d

.field public static final cs_523_label_jpegtopdf_01:I = 0x7f13098e

.field public static final cs_523_label_jpegtopdf_02:I = 0x7f13098f

.field public static final cs_523_label_jpegtopdf_03:I = 0x7f130990

.field public static final cs_523_label_jpegtopdf_04:I = 0x7f130991

.field public static final cs_523_label_lawyer:I = 0x7f130992

.field public static final cs_523_label_manager:I = 0x7f130993

.field public static final cs_523_label_others:I = 0x7f130994

.field public static final cs_523_label_recommend:I = 0x7f130995

.field public static final cs_523_label_register_tips:I = 0x7f130996

.field public static final cs_523_label_registration_success:I = 0x7f130997

.field public static final cs_523_label_sales:I = 0x7f130998

.field public static final cs_523_label_scanning:I = 0x7f130999

.field public static final cs_523_label_signature:I = 0x7f13099a

.field public static final cs_523_label_signature_01:I = 0x7f13099b

.field public static final cs_523_label_signature_02:I = 0x7f13099c

.field public static final cs_523_label_speediness:I = 0x7f13099d

.field public static final cs_523_label_student:I = 0x7f13099e

.field public static final cs_523_label_teacher:I = 0x7f13099f

.field public static final cs_523_label_tips:I = 0x7f1309a0

.field public static final cs_523_newtab_app_head2:I = 0x7f1309a1

.field public static final cs_523_other_app:I = 0x7f1309a2

.field public static final cs_523_page_number:I = 0x7f1309a3

.field public static final cs_523_page_number_center:I = 0x7f1309a4

.field public static final cs_523_page_number_left:I = 0x7f1309a5

.field public static final cs_523_page_number_right:I = 0x7f1309a6

.field public static final cs_523_pdf_convert:I = 0x7f1309a7

.field public static final cs_523_pdf_excel:I = 0x7f1309a8

.field public static final cs_523_pdf_ppt:I = 0x7f1309a9

.field public static final cs_523_pdf_ppt_contents:I = 0x7f1309aa

.field public static final cs_523_scan_qr_tips1:I = 0x7f1309ab

.field public static final cs_523_scan_qr_tips2:I = 0x7f1309ac

.field public static final cs_523_title_not_saved:I = 0x7f1309ad

.field public static final cs_523_title_switch_filter:I = 0x7f1309ae

.field public static final cs_523_unlimited:I = 0x7f1309af

.field public static final cs_5245_access_code:I = 0x7f1309b0

.field public static final cs_5245_access_code2:I = 0x7f1309b1

.field public static final cs_5245_resubscribe_1:I = 0x7f1309b2

.field public static final cs_5245_resubscribe_10:I = 0x7f1309b3

.field public static final cs_5245_resubscribe_2:I = 0x7f1309b4

.field public static final cs_5245_resubscribe_3:I = 0x7f1309b5

.field public static final cs_5245_resubscribe_4:I = 0x7f1309b6

.field public static final cs_5245_resubscribe_5:I = 0x7f1309b7

.field public static final cs_5245_resubscribe_6:I = 0x7f1309b8

.field public static final cs_5245_resubscribe_7:I = 0x7f1309b9

.field public static final cs_5245_resubscribe_8:I = 0x7f1309ba

.field public static final cs_5245_resubscribe_9:I = 0x7f1309bb

.field public static final cs_524_cloud_link_fail:I = 0x7f1309bc

.field public static final cs_524_cloudspace_warning1:I = 0x7f1309bd

.field public static final cs_524_cloudspace_warning2:I = 0x7f1309be

.field public static final cs_524_cloudspace_warning2_1:I = 0x7f1309bf

.field public static final cs_524_cloudspace_warning3:I = 0x7f1309c0

.field public static final cs_524_cloudspace_warning4:I = 0x7f1309c1

.field public static final cs_524_cloudspace_warning5:I = 0x7f1309c2

.field public static final cs_524_cloudspace_warning6:I = 0x7f1309c3

.field public static final cs_524_edit_text:I = 0x7f1309c4

.field public static final cs_524_protocol_desc_0:I = 0x7f1309c5

.field public static final cs_524_protocol_desc_1:I = 0x7f1309c6

.field public static final cs_524_protocol_desc_2:I = 0x7f1309c7

.field public static final cs_524_protocol_desc_3:I = 0x7f1309c8

.field public static final cs_524_protocol_desc_4:I = 0x7f1309c9

.field public static final cs_524_protocol_desc_5:I = 0x7f1309ca

.field public static final cs_524_protocol_desc_6:I = 0x7f1309cb

.field public static final cs_524_protocol_desc_7:I = 0x7f1309cc

.field public static final cs_524_protocol_desc_8:I = 0x7f1309cd

.field public static final cs_524_text:I = 0x7f1309ce

.field public static final cs_524_whats_new_1:I = 0x7f1309cf

.field public static final cs_524_whats_new_2:I = 0x7f1309d0

.field public static final cs_5250_cloudspace_web_01:I = 0x7f1309d1

.field public static final cs_5250_cloudspace_web_02:I = 0x7f1309d2

.field public static final cs_5250_cloudspace_web_03:I = 0x7f1309d3

.field public static final cs_5250_cloudspace_web_04:I = 0x7f1309d4

.field public static final cs_5250_cloudspace_web_05:I = 0x7f1309d5

.field public static final cs_5250_cloudspace_web_06:I = 0x7f1309d6

.field public static final cs_5250_cloudspace_web_07:I = 0x7f1309d7

.field public static final cs_5250_label_edu_01:I = 0x7f1309d8

.field public static final cs_5250_label_edu_02:I = 0x7f1309d9

.field public static final cs_5250_label_edu_03:I = 0x7f1309da

.field public static final cs_5250_label_edu_05:I = 0x7f1309db

.field public static final cs_5250_label_edu_06:I = 0x7f1309dc

.field public static final cs_5250_label_edu_08:I = 0x7f1309dd

.field public static final cs_5250_label_edu_09:I = 0x7f1309de

.field public static final cs_5255_nps_negative:I = 0x7f1309df

.field public static final cs_5255_nps_neutral:I = 0x7f1309e0

.field public static final cs_5255_nps_no_recommend:I = 0x7f1309e1

.field public static final cs_5255_nps_positive:I = 0x7f1309e2

.field public static final cs_5255_nps_will_recommend:I = 0x7f1309e3

.field public static final cs_5255_reback_pop_01:I = 0x7f1309e4

.field public static final cs_5255_reback_pop_02:I = 0x7f1309e5

.field public static final cs_525_newweb_bubble2:I = 0x7f1309e6

.field public static final cs_525_newweb_content1:I = 0x7f1309e7

.field public static final cs_525_newweb_content2:I = 0x7f1309e8

.field public static final cs_525_newweb_title:I = 0x7f1309e9

.field public static final cs_525_ocr_02:I = 0x7f1309ea

.field public static final cs_525_ocr_03:I = 0x7f1309eb

.field public static final cs_525_ocr_04:I = 0x7f1309ec

.field public static final cs_525_ocr_05:I = 0x7f1309ed

.field public static final cs_525_ocr_06:I = 0x7f1309ee

.field public static final cs_525_ocr_07:I = 0x7f1309ef

.field public static final cs_525_ocr_08:I = 0x7f1309f0

.field public static final cs_525_ocr_14:I = 0x7f1309f1

.field public static final cs_525_to_log_in:I = 0x7f1309f2

.field public static final cs_526_button_correction:I = 0x7f1309f3

.field public static final cs_526_checkbox_no_remind:I = 0x7f1309f4

.field public static final cs_526_copytip:I = 0x7f1309f5

.field public static final cs_526_cut:I = 0x7f1309f6

.field public static final cs_526_edu_got:I = 0x7f1309f7

.field public static final cs_526_export_image:I = 0x7f1309f8

.field public static final cs_526_guide_correction1:I = 0x7f1309f9

.field public static final cs_526_guide_correction2:I = 0x7f1309fa

.field public static final cs_526_login_hinttip:I = 0x7f1309fb

.field public static final cs_526_paste:I = 0x7f1309fc

.field public static final cs_526_toast_correction_off:I = 0x7f1309fd

.field public static final cs_526_toast_correction_on:I = 0x7f1309fe

.field public static final cs_526_toword_tip:I = 0x7f1309ff

.field public static final cs_526_translatetip:I = 0x7f130a00

.field public static final cs_526_word:I = 0x7f130a01

.field public static final cs_527_button_add:I = 0x7f130a02

.field public static final cs_527_dialog_body_delete:I = 0x7f130a03

.field public static final cs_527_dialog_title_delete:I = 0x7f130a04

.field public static final cs_527_filter_apply_all:I = 0x7f130a05

.field public static final cs_527_guide_batch_crop:I = 0x7f130a06

.field public static final cs_527_guide_crop:I = 0x7f130a07

.field public static final cs_527_guide_crop2:I = 0x7f130a08

.field public static final cs_527_icon_add:I = 0x7f130a09

.field public static final cs_527_label_foreign_01:I = 0x7f130a0a

.field public static final cs_527_label_foreign_02:I = 0x7f130a0b

.field public static final cs_527_label_foreign_03:I = 0x7f130a0c

.field public static final cs_527_label_foreign_04:I = 0x7f130a0d

.field public static final cs_527_label_foreign_05:I = 0x7f130a0e

.field public static final cs_527_label_foreign_06:I = 0x7f130a0f

.field public static final cs_527_label_foreign_07:I = 0x7f130a10

.field public static final cs_527_label_foreign_08:I = 0x7f130a11

.field public static final cs_527_label_foreign_09:I = 0x7f130a12

.field public static final cs_527_label_foreign_10:I = 0x7f130a13

.field public static final cs_527_label_foreign_11:I = 0x7f130a14

.field public static final cs_527_label_foreign_12:I = 0x7f130a15

.field public static final cs_527_label_foreign_13:I = 0x7f130a16

.field public static final cs_527_label_foreign_14:I = 0x7f130a17

.field public static final cs_527_label_foreign_15:I = 0x7f130a18

.field public static final cs_527_option_crop1:I = 0x7f130a19

.field public static final cs_527_option_crop2:I = 0x7f130a1a

.field public static final cs_527_replace:I = 0x7f130a1b

.field public static final cs_527_settings_gallery_crop:I = 0x7f130a1c

.field public static final cs_527_settings_title_crop:I = 0x7f130a1d

.field public static final cs_527_sign_mail_illegal_tip:I = 0x7f130a1e

.field public static final cs_527_title_crop:I = 0x7f130a1f

.field public static final cs_528_Application_category1:I = 0x7f130a20

.field public static final cs_528_Application_title3:I = 0x7f130a21

.field public static final cs_528_Application_title4:I = 0x7f130a22

.field public static final cs_528_Application_title5:I = 0x7f130a23

.field public static final cs_528_coupon_01:I = 0x7f130a24

.field public static final cs_528_coupon_02:I = 0x7f130a25

.field public static final cs_528_coupon_03:I = 0x7f130a26

.field public static final cs_528_coupon_04:I = 0x7f130a27

.field public static final cs_528_coupon_05:I = 0x7f130a28

.field public static final cs_528_coupon_06:I = 0x7f130a29

.field public static final cs_528_disclaimer_korea1:I = 0x7f130a2a

.field public static final cs_528_disclaimer_korea2:I = 0x7f130a2b

.field public static final cs_528_label_02:I = 0x7f130a2c

.field public static final cs_528_label_03:I = 0x7f130a2d

.field public static final cs_528_label_04:I = 0x7f130a2e

.field public static final cs_528_label_05:I = 0x7f130a2f

.field public static final cs_528_label_06:I = 0x7f130a30

.field public static final cs_528_label_06_1:I = 0x7f130a31

.field public static final cs_528_label_07:I = 0x7f130a32

.field public static final cs_528_label_08:I = 0x7f130a33

.field public static final cs_528_label_16:I = 0x7f130a34

.field public static final cs_528_label_17:I = 0x7f130a35

.field public static final cs_528_label_20:I = 0x7f130a36

.field public static final cs_528_needs1:I = 0x7f130a37

.field public static final cs_528_needs2:I = 0x7f130a38

.field public static final cs_528_needs3:I = 0x7f130a39

.field public static final cs_528_needs4:I = 0x7f130a3a

.field public static final cs_528_needs5:I = 0x7f130a3b

.field public static final cs_528_needs_category1:I = 0x7f130a3c

.field public static final cs_528_pclogin_tip:I = 0x7f130a3d

.field public static final cs_528_title_share_panel:I = 0x7f130a3e

.field public static final cs_528_title_share_panel_nothing_selected:I = 0x7f130a3f

.field public static final cs_528_toexcel_failtip:I = 0x7f130a40

.field public static final cs_529_pdftoword_rerecognize:I = 0x7f130a41

.field public static final cs_529_pdftoword_scope:I = 0x7f130a42

.field public static final cs_529_pdftoword_tip_cuttext:I = 0x7f130a43

.field public static final cs_529_pdftoword_tip_edittext:I = 0x7f130a44

.field public static final cs_52_ocr_edit:I = 0x7f130a45

.field public static final cs_52_search_zero:I = 0x7f130a46

.field public static final cs_530_label_test_01:I = 0x7f130a47

.field public static final cs_530_label_test_02:I = 0x7f130a48

.field public static final cs_530_label_test_03:I = 0x7f130a49

.field public static final cs_530_label_test_04:I = 0x7f130a4a

.field public static final cs_530_label_test_05:I = 0x7f130a4b

.field public static final cs_530_label_test_06:I = 0x7f130a4c

.field public static final cs_530_label_test_07:I = 0x7f130a4d

.field public static final cs_530_label_test_08:I = 0x7f130a4e

.field public static final cs_530_pdftoword_tip_fast_a:I = 0x7f130a4f

.field public static final cs_530_pdftoword_tip_fast_b:I = 0x7f130a50

.field public static final cs_530_pdftoword_tip_fast_c:I = 0x7f130a51

.field public static final cs_530_scandone_share:I = 0x7f130a52

.field public static final cs_530_scandone_share_wechat:I = 0x7f130a53

.field public static final cs_530_signature_othres_signarea:I = 0x7f130a54

.field public static final cs_531_album_desc:I = 0x7f130a55

.field public static final cs_531_album_title:I = 0x7f130a56

.field public static final cs_531_guide_banner_desc01:I = 0x7f130a57

.field public static final cs_531_guide_banner_desc02:I = 0x7f130a58

.field public static final cs_531_guide_banner_desc03:I = 0x7f130a59

.field public static final cs_531_guide_banner_desc04:I = 0x7f130a5a

.field public static final cs_531_guide_banner_desc05:I = 0x7f130a5b

.field public static final cs_531_guide_banner_title03:I = 0x7f130a5c

.field public static final cs_531_guide_banner_title05:I = 0x7f130a5d

.field public static final cs_531_guide_first_year_price_desc:I = 0x7f130a5e

.field public static final cs_531_guide_lisense_ali:I = 0x7f130a5f

.field public static final cs_531_guide_lisense_weixin:I = 0x7f130a60

.field public static final cs_531_guide_second_year_price_desc:I = 0x7f130a61

.field public static final cs_531_guide_trial_01:I = 0x7f130a62

.field public static final cs_531_guide_trial_02:I = 0x7f130a63

.field public static final cs_531_id_photo_01:I = 0x7f130a64

.field public static final cs_531_id_photo_02:I = 0x7f130a65

.field public static final cs_531_id_photo_03:I = 0x7f130a66

.field public static final cs_531_id_photo_04:I = 0x7f130a67

.field public static final cs_531_id_photo_05:I = 0x7f130a68

.field public static final cs_531_idscan_tip:I = 0x7f130a69

.field public static final cs_531_imei_desc:I = 0x7f130a6a

.field public static final cs_531_imei_title:I = 0x7f130a6b

.field public static final cs_531_label_style_01:I = 0x7f130a6c

.field public static final cs_531_label_style_02:I = 0x7f130a6d

.field public static final cs_531_label_style_03:I = 0x7f130a6e

.field public static final cs_531_label_style_04:I = 0x7f130a6f

.field public static final cs_531_label_style_05:I = 0x7f130a70

.field public static final cs_531_label_style_06:I = 0x7f130a71

.field public static final cs_531_label_style_07:I = 0x7f130a72

.field public static final cs_531_label_style_08:I = 0x7f130a73

.field public static final cs_531_label_style_09:I = 0x7f130a74

.field public static final cs_531_label_style_10:I = 0x7f130a75

.field public static final cs_531_label_style_11:I = 0x7f130a76

.field public static final cs_531_label_style_12:I = 0x7f130a77

.field public static final cs_531_label_style_13:I = 0x7f130a78

.field public static final cs_531_label_style_14:I = 0x7f130a79

.field public static final cs_531_label_style_15:I = 0x7f130a7a

.field public static final cs_531_label_style_16:I = 0x7f130a7b

.field public static final cs_531_label_style_17:I = 0x7f130a7c

.field public static final cs_531_label_style_18:I = 0x7f130a7d

.field public static final cs_531_label_style_19:I = 0x7f130a7e

.field public static final cs_531_label_style_20:I = 0x7f130a7f

.field public static final cs_531_label_style_21:I = 0x7f130a80

.field public static final cs_531_location_desc:I = 0x7f130a81

.field public static final cs_531_location_title:I = 0x7f130a82

.field public static final cs_531_protocol_agree:I = 0x7f130a83

.field public static final cs_531_protocol_title:I = 0x7f130a84

.field public static final cs_531_reminder:I = 0x7f130a85

.field public static final cs_531_reminder_desc:I = 0x7f130a86

.field public static final cs_531_smear_brush:I = 0x7f130a87

.field public static final cs_531_smear_rectangle:I = 0x7f130a88

.field public static final cs_531_storage_desc1:I = 0x7f130a89

.field public static final cs_531_storage_desc2:I = 0x7f130a8a

.field public static final cs_531_storage_title:I = 0x7f130a8b

.field public static final cs_531_year_guide_sub_title:I = 0x7f130a8c

.field public static final cs_532_discard_images:I = 0x7f130a8d

.field public static final cs_532_label_foreign_03:I = 0x7f130a8e

.field public static final cs_532_label_foreign_04:I = 0x7f130a8f

.field public static final cs_532_label_foreign_05:I = 0x7f130a90

.field public static final cs_533_label_01:I = 0x7f130a91

.field public static final cs_533_label_02:I = 0x7f130a92

.field public static final cs_533_label_03:I = 0x7f130a93

.field public static final cs_533_label_04:I = 0x7f130a94

.field public static final cs_533_label_05:I = 0x7f130a95

.field public static final cs_533_label_06:I = 0x7f130a96

.field public static final cs_533_label_07:I = 0x7f130a97

.field public static final cs_533_label_08:I = 0x7f130a98

.field public static final cs_533_label_09:I = 0x7f130a99

.field public static final cs_533_label_10:I = 0x7f130a9a

.field public static final cs_533_needs6:I = 0x7f130a9b

.field public static final cs_533_needs7:I = 0x7f130a9c

.field public static final cs_534_add_card_template:I = 0x7f130a9d

.field public static final cs_534_gp_installtip:I = 0x7f130a9e

.field public static final cs_534_gp_pay:I = 0x7f130a9f

.field public static final cs_534_hw_pay:I = 0x7f130aa0

.field public static final cs_534_hwpay_installtip:I = 0x7f130aa1

.field public static final cs_534_noads:I = 0x7f130aa2

.field public static final cs_534_nps_feature_score:I = 0x7f130aa3

.field public static final cs_534_nps_feature_score_1:I = 0x7f130aa4

.field public static final cs_534_nps_feature_score_2:I = 0x7f130aa5

.field public static final cs_534_nps_feature_score_3:I = 0x7f130aa6

.field public static final cs_534_nps_feature_score_4:I = 0x7f130aa7

.field public static final cs_534_nps_feature_score_5:I = 0x7f130aa8

.field public static final cs_534_ocr:I = 0x7f130aa9

.field public static final cs_534_paynow:I = 0x7f130aaa

.field public static final cs_534_select_payment:I = 0x7f130aab

.field public static final cs_535_account_error:I = 0x7f130aac

.field public static final cs_535_delete_account:I = 0x7f130aad

.field public static final cs_535_delete_account_clarify:I = 0x7f130aae

.field public static final cs_535_delete_account_clarify_content:I = 0x7f130aaf

.field public static final cs_535_delete_account_clarify_content10:I = 0x7f130ab0

.field public static final cs_535_delete_account_clarify_content2:I = 0x7f130ab1

.field public static final cs_535_delete_account_clarify_content3:I = 0x7f130ab2

.field public static final cs_535_delete_account_clarify_content4:I = 0x7f130ab3

.field public static final cs_535_delete_account_clarify_content5:I = 0x7f130ab4

.field public static final cs_535_delete_account_clarify_content6:I = 0x7f130ab5

.field public static final cs_535_delete_account_clarify_content7:I = 0x7f130ab6

.field public static final cs_535_delete_account_clarify_content8:I = 0x7f130ab7

.field public static final cs_535_delete_account_clarify_content9:I = 0x7f130ab8

.field public static final cs_535_delete_account_clarify_title:I = 0x7f130ab9

.field public static final cs_535_delete_account_confirm_button:I = 0x7f130aba

.field public static final cs_535_delete_account_contact_us:I = 0x7f130abb

.field public static final cs_535_delete_account_error1:I = 0x7f130abc

.field public static final cs_535_delete_account_error1_1:I = 0x7f130abd

.field public static final cs_535_delete_account_error_detail:I = 0x7f130abe

.field public static final cs_535_delete_account_error_popup:I = 0x7f130abf

.field public static final cs_535_delete_account_menu_title:I = 0x7f130ac0

.field public static final cs_535_delete_account_title:I = 0x7f130ac1

.field public static final cs_535_guidetest_1:I = 0x7f130ac2

.field public static final cs_535_guidetest_10:I = 0x7f130ac3

.field public static final cs_535_guidetest_11:I = 0x7f130ac4

.field public static final cs_535_guidetest_2:I = 0x7f130ac5

.field public static final cs_535_guidetest_3:I = 0x7f130ac6

.field public static final cs_535_guidetest_3_1:I = 0x7f130ac7

.field public static final cs_535_guidetest_5:I = 0x7f130ac8

.field public static final cs_535_guidetest_6:I = 0x7f130ac9

.field public static final cs_535_guidetest_8:I = 0x7f130aca

.field public static final cs_535_guidetest_9:I = 0x7f130acb

.field public static final cs_535_popup_markup_merged:I = 0x7f130acc

.field public static final cs_536_agree_continue:I = 0x7f130acd

.field public static final cs_536_button_start_photo_restoration:I = 0x7f130ace

.field public static final cs_536_content_photo_restoration:I = 0x7f130acf

.field public static final cs_536_go_restore:I = 0x7f130ad0

.field public static final cs_536_guide3_photo_restoration:I = 0x7f130ad1

.field public static final cs_536_guide4_photo_restoration:I = 0x7f130ad2

.field public static final cs_536_hint_loading_photo_restoration:I = 0x7f130ad3

.field public static final cs_536_multidoc_share1:I = 0x7f130ad4

.field public static final cs_536_multidoc_share4:I = 0x7f130ad5

.field public static final cs_536_no_credit:I = 0x7f130ad6

.field public static final cs_536_photo_restoration_after:I = 0x7f130ad7

.field public static final cs_536_photo_restoration_before:I = 0x7f130ad8

.field public static final cs_536_photo_restoration_example:I = 0x7f130ad9

.field public static final cs_536_photo_restoration_result1:I = 0x7f130ada

.field public static final cs_536_photo_restoration_result2:I = 0x7f130adb

.field public static final cs_536_photo_restoration_result3:I = 0x7f130adc

.field public static final cs_536_photo_restoration_view_result:I = 0x7f130add

.field public static final cs_536_picture_too_small:I = 0x7f130ade

.field public static final cs_536_save_share:I = 0x7f130adf

.field public static final cs_536_selected_docs_label:I = 0x7f130ae0

.field public static final cs_536_server_error:I = 0x7f130ae1

.field public static final cs_536_svip_02:I = 0x7f130ae2

.field public static final cs_536_title_photo_restoration:I = 0x7f130ae3

.field public static final cs_536_toast_no_moire_off:I = 0x7f130ae4

.field public static final cs_537_background_transfering_pop:I = 0x7f130ae5

.field public static final cs_537_devicemuch:I = 0x7f130ae6

.field public static final cs_537_doclink_01:I = 0x7f130ae7

.field public static final cs_537_doclink_02:I = 0x7f130ae8

.field public static final cs_537_doclink_03:I = 0x7f130ae9

.field public static final cs_537_docshare1:I = 0x7f130aea

.field public static final cs_537_file_transfering_pop:I = 0x7f130aeb

.field public static final cs_537_incentive_19:I = 0x7f130aec

.field public static final cs_537_moire_no_credit:I = 0x7f130aed

.field public static final cs_537_multidoc_share5:I = 0x7f130aee

.field public static final cs_537_toast_transfer_background_guide:I = 0x7f130aef

.field public static final cs_537_toast_transfer_wait_guide:I = 0x7f130af0

.field public static final cs_537_transfer_fail_guide:I = 0x7f130af1

.field public static final cs_537_transfer_preview:I = 0x7f130af2

.field public static final cs_537_transfer_speed:I = 0x7f130af3

.field public static final cs_537_transfer_speed_unit:I = 0x7f130af4

.field public static final cs_537_transfering:I = 0x7f130af5

.field public static final cs_538_coupon_01:I = 0x7f130af6

.field public static final cs_538_coupon_02:I = 0x7f130af7

.field public static final cs_538_coupon_04:I = 0x7f130af8

.field public static final cs_538_guide_07:I = 0x7f130af9

.field public static final cs_538_needs:I = 0x7f130afa

.field public static final cs_538_needs3:I = 0x7f130afb

.field public static final cs_538_needs4:I = 0x7f130afc

.field public static final cs_538_pdftools_01:I = 0x7f130afd

.field public static final cs_538_upgrade_05:I = 0x7f130afe

.field public static final cs_538_upgrade_06:I = 0x7f130aff

.field public static final cs_538_vip_moire_no_credit:I = 0x7f130b00

.field public static final cs_539_edit_pdf:I = 0x7f130b01

.field public static final cs_539_purchase_points:I = 0x7f130b02

.field public static final cs_539_scandone_share1:I = 0x7f130b03

.field public static final cs_539_scandone_share2:I = 0x7f130b04

.field public static final cs_539_scandone_share3:I = 0x7f130b05

.field public static final cs_539_scandone_share4:I = 0x7f130b06

.field public static final cs_539_scandone_share5:I = 0x7f130b07

.field public static final cs_539_scandone_share6:I = 0x7f130b08

.field public static final cs_539_scandone_share7:I = 0x7f130b09

.field public static final cs_539_scandone_share8:I = 0x7f130b0a

.field public static final cs_539_scandone_share9:I = 0x7f130b0b

.field public static final cs_539_setting_dark:I = 0x7f130b0c

.field public static final cs_539_setting_dark_confirm:I = 0x7f130b0d

.field public static final cs_539_setting_light:I = 0x7f130b0e

.field public static final cs_539_setting_select:I = 0x7f130b0f

.field public static final cs_539_setting_system:I = 0x7f130b10

.field public static final cs_539_setting_system_tip:I = 0x7f130b11

.field public static final cs_539_share_copy6:I = 0x7f130b12

.field public static final cs_540_ad_applaunch_skip:I = 0x7f130b13

.field public static final cs_540_ad_dp_popup_title:I = 0x7f130b14

.field public static final cs_540_colorize_after:I = 0x7f130b15

.field public static final cs_540_colorize_before:I = 0x7f130b16

.field public static final cs_540_image_restore_select_mode:I = 0x7f130b17

.field public static final cs_540_image_restore_tip:I = 0x7f130b18

.field public static final cs_540_labe_02:I = 0x7f130b19

.field public static final cs_540_lifetime_01:I = 0x7f130b1a

.field public static final cs_540_no_credit_day:I = 0x7f130b1b

.field public static final cs_540_photo_colorize_guide:I = 0x7f130b1c

.field public static final cs_540_repair_credit_day:I = 0x7f130b1d

.field public static final cs_540_repair_credit_month:I = 0x7f130b1e

.field public static final cs_540_shar_lark:I = 0x7f130b1f

.field public static final cs_540_title_photo_colorize:I = 0x7f130b20

.field public static final cs_541_edu_desc:I = 0x7f130b21

.field public static final cs_541_edu_title_v2:I = 0x7f130b22

.field public static final cs_541_fix_page_number:I = 0x7f130b23

.field public static final cs_541_storage_permission_pop:I = 0x7f130b24

.field public static final cs_542_document_access_01:I = 0x7f130b25

.field public static final cs_542_document_access_02:I = 0x7f130b26

.field public static final cs_542_document_access_03:I = 0x7f130b27

.field public static final cs_542_download:I = 0x7f130b28

.field public static final cs_542_guidead_02:I = 0x7f130b29

.field public static final cs_542_guidead_03:I = 0x7f130b2a

.field public static final cs_542_pdftools_04:I = 0x7f130b2b

.field public static final cs_542_pdftools_05:I = 0x7f130b2c

.field public static final cs_542_pdftools_06:I = 0x7f130b2d

.field public static final cs_542_pdfwatermarkfree_01:I = 0x7f130b2e

.field public static final cs_542_pdfwatermarkfree_02:I = 0x7f130b2f

.field public static final cs_542_pdfwatermarkfree_03:I = 0x7f130b30

.field public static final cs_542_pdfwatermarkfree_06:I = 0x7f130b31

.field public static final cs_542_pdfwatermarkfree_08:I = 0x7f130b32

.field public static final cs_542_renew_1:I = 0x7f130b33

.field public static final cs_542_renew_10:I = 0x7f130b34

.field public static final cs_542_renew_102:I = 0x7f130b35

.field public static final cs_542_renew_103:I = 0x7f130b36

.field public static final cs_542_renew_104:I = 0x7f130b37

.field public static final cs_542_renew_105:I = 0x7f130b38

.field public static final cs_542_renew_106:I = 0x7f130b39

.field public static final cs_542_renew_107:I = 0x7f130b3a

.field public static final cs_542_renew_108:I = 0x7f130b3b

.field public static final cs_542_renew_109:I = 0x7f130b3c

.field public static final cs_542_renew_110:I = 0x7f130b3d

.field public static final cs_542_renew_111:I = 0x7f130b3e

.field public static final cs_542_renew_112:I = 0x7f130b3f

.field public static final cs_542_renew_115:I = 0x7f130b40

.field public static final cs_542_renew_116:I = 0x7f130b41

.field public static final cs_542_renew_117:I = 0x7f130b42

.field public static final cs_542_renew_118:I = 0x7f130b43

.field public static final cs_542_renew_119:I = 0x7f130b44

.field public static final cs_542_renew_120:I = 0x7f130b45

.field public static final cs_542_renew_121:I = 0x7f130b46

.field public static final cs_542_renew_122:I = 0x7f130b47

.field public static final cs_542_renew_123:I = 0x7f130b48

.field public static final cs_542_renew_124:I = 0x7f130b49

.field public static final cs_542_renew_125:I = 0x7f130b4a

.field public static final cs_542_renew_126:I = 0x7f130b4b

.field public static final cs_542_renew_127:I = 0x7f130b4c

.field public static final cs_542_renew_128:I = 0x7f130b4d

.field public static final cs_542_renew_13:I = 0x7f130b4e

.field public static final cs_542_renew_130:I = 0x7f130b4f

.field public static final cs_542_renew_132:I = 0x7f130b50

.field public static final cs_542_renew_133:I = 0x7f130b51

.field public static final cs_542_renew_134:I = 0x7f130b52

.field public static final cs_542_renew_135:I = 0x7f130b53

.field public static final cs_542_renew_136:I = 0x7f130b54

.field public static final cs_542_renew_137:I = 0x7f130b55

.field public static final cs_542_renew_138:I = 0x7f130b56

.field public static final cs_542_renew_139:I = 0x7f130b57

.field public static final cs_542_renew_14:I = 0x7f130b58

.field public static final cs_542_renew_140:I = 0x7f130b59

.field public static final cs_542_renew_141:I = 0x7f130b5a

.field public static final cs_542_renew_142:I = 0x7f130b5b

.field public static final cs_542_renew_143:I = 0x7f130b5c

.field public static final cs_542_renew_145:I = 0x7f130b5d

.field public static final cs_542_renew_146:I = 0x7f130b5e

.field public static final cs_542_renew_148:I = 0x7f130b5f

.field public static final cs_542_renew_150:I = 0x7f130b60

.field public static final cs_542_renew_151:I = 0x7f130b61

.field public static final cs_542_renew_152:I = 0x7f130b62

.field public static final cs_542_renew_154:I = 0x7f130b63

.field public static final cs_542_renew_157:I = 0x7f130b64

.field public static final cs_542_renew_158:I = 0x7f130b65

.field public static final cs_542_renew_159:I = 0x7f130b66

.field public static final cs_542_renew_16:I = 0x7f130b67

.field public static final cs_542_renew_160:I = 0x7f130b68

.field public static final cs_542_renew_161:I = 0x7f130b69

.field public static final cs_542_renew_162:I = 0x7f130b6a

.field public static final cs_542_renew_163:I = 0x7f130b6b

.field public static final cs_542_renew_164:I = 0x7f130b6c

.field public static final cs_542_renew_165:I = 0x7f130b6d

.field public static final cs_542_renew_167:I = 0x7f130b6e

.field public static final cs_542_renew_168:I = 0x7f130b6f

.field public static final cs_542_renew_169:I = 0x7f130b70

.field public static final cs_542_renew_17:I = 0x7f130b71

.field public static final cs_542_renew_170:I = 0x7f130b72

.field public static final cs_542_renew_171:I = 0x7f130b73

.field public static final cs_542_renew_172:I = 0x7f130b74

.field public static final cs_542_renew_173:I = 0x7f130b75

.field public static final cs_542_renew_175:I = 0x7f130b76

.field public static final cs_542_renew_176:I = 0x7f130b77

.field public static final cs_542_renew_177:I = 0x7f130b78

.field public static final cs_542_renew_178:I = 0x7f130b79

.field public static final cs_542_renew_179:I = 0x7f130b7a

.field public static final cs_542_renew_180:I = 0x7f130b7b

.field public static final cs_542_renew_181:I = 0x7f130b7c

.field public static final cs_542_renew_182:I = 0x7f130b7d

.field public static final cs_542_renew_183:I = 0x7f130b7e

.field public static final cs_542_renew_184:I = 0x7f130b7f

.field public static final cs_542_renew_185:I = 0x7f130b80

.field public static final cs_542_renew_186:I = 0x7f130b81

.field public static final cs_542_renew_189:I = 0x7f130b82

.field public static final cs_542_renew_19:I = 0x7f130b83

.field public static final cs_542_renew_190:I = 0x7f130b84

.field public static final cs_542_renew_191:I = 0x7f130b85

.field public static final cs_542_renew_192:I = 0x7f130b86

.field public static final cs_542_renew_193:I = 0x7f130b87

.field public static final cs_542_renew_194:I = 0x7f130b88

.field public static final cs_542_renew_195:I = 0x7f130b89

.field public static final cs_542_renew_196:I = 0x7f130b8a

.field public static final cs_542_renew_197:I = 0x7f130b8b

.field public static final cs_542_renew_198:I = 0x7f130b8c

.field public static final cs_542_renew_199:I = 0x7f130b8d

.field public static final cs_542_renew_2:I = 0x7f130b8e

.field public static final cs_542_renew_20:I = 0x7f130b8f

.field public static final cs_542_renew_200:I = 0x7f130b90

.field public static final cs_542_renew_201:I = 0x7f130b91

.field public static final cs_542_renew_202:I = 0x7f130b92

.field public static final cs_542_renew_203:I = 0x7f130b93

.field public static final cs_542_renew_204:I = 0x7f130b94

.field public static final cs_542_renew_205:I = 0x7f130b95

.field public static final cs_542_renew_206:I = 0x7f130b96

.field public static final cs_542_renew_207:I = 0x7f130b97

.field public static final cs_542_renew_208:I = 0x7f130b98

.field public static final cs_542_renew_209:I = 0x7f130b99

.field public static final cs_542_renew_210:I = 0x7f130b9a

.field public static final cs_542_renew_211:I = 0x7f130b9b

.field public static final cs_542_renew_212:I = 0x7f130b9c

.field public static final cs_542_renew_213:I = 0x7f130b9d

.field public static final cs_542_renew_214:I = 0x7f130b9e

.field public static final cs_542_renew_215:I = 0x7f130b9f

.field public static final cs_542_renew_216:I = 0x7f130ba0

.field public static final cs_542_renew_217:I = 0x7f130ba1

.field public static final cs_542_renew_218:I = 0x7f130ba2

.field public static final cs_542_renew_219:I = 0x7f130ba3

.field public static final cs_542_renew_220:I = 0x7f130ba4

.field public static final cs_542_renew_221:I = 0x7f130ba5

.field public static final cs_542_renew_222:I = 0x7f130ba6

.field public static final cs_542_renew_223:I = 0x7f130ba7

.field public static final cs_542_renew_224:I = 0x7f130ba8

.field public static final cs_542_renew_225:I = 0x7f130ba9

.field public static final cs_542_renew_226:I = 0x7f130baa

.field public static final cs_542_renew_227:I = 0x7f130bab

.field public static final cs_542_renew_228:I = 0x7f130bac

.field public static final cs_542_renew_229:I = 0x7f130bad

.field public static final cs_542_renew_230:I = 0x7f130bae

.field public static final cs_542_renew_231:I = 0x7f130baf

.field public static final cs_542_renew_232:I = 0x7f130bb0

.field public static final cs_542_renew_233:I = 0x7f130bb1

.field public static final cs_542_renew_234:I = 0x7f130bb2

.field public static final cs_542_renew_235:I = 0x7f130bb3

.field public static final cs_542_renew_236:I = 0x7f130bb4

.field public static final cs_542_renew_237:I = 0x7f130bb5

.field public static final cs_542_renew_238:I = 0x7f130bb6

.field public static final cs_542_renew_239:I = 0x7f130bb7

.field public static final cs_542_renew_24:I = 0x7f130bb8

.field public static final cs_542_renew_240:I = 0x7f130bb9

.field public static final cs_542_renew_241:I = 0x7f130bba

.field public static final cs_542_renew_242:I = 0x7f130bbb

.field public static final cs_542_renew_243:I = 0x7f130bbc

.field public static final cs_542_renew_244:I = 0x7f130bbd

.field public static final cs_542_renew_245:I = 0x7f130bbe

.field public static final cs_542_renew_246:I = 0x7f130bbf

.field public static final cs_542_renew_247:I = 0x7f130bc0

.field public static final cs_542_renew_248:I = 0x7f130bc1

.field public static final cs_542_renew_249:I = 0x7f130bc2

.field public static final cs_542_renew_25:I = 0x7f130bc3

.field public static final cs_542_renew_250:I = 0x7f130bc4

.field public static final cs_542_renew_251:I = 0x7f130bc5

.field public static final cs_542_renew_252:I = 0x7f130bc6

.field public static final cs_542_renew_254:I = 0x7f130bc7

.field public static final cs_542_renew_256:I = 0x7f130bc8

.field public static final cs_542_renew_257:I = 0x7f130bc9

.field public static final cs_542_renew_258:I = 0x7f130bca

.field public static final cs_542_renew_26:I = 0x7f130bcb

.field public static final cs_542_renew_260:I = 0x7f130bcc

.field public static final cs_542_renew_261:I = 0x7f130bcd

.field public static final cs_542_renew_262:I = 0x7f130bce

.field public static final cs_542_renew_263:I = 0x7f130bcf

.field public static final cs_542_renew_264:I = 0x7f130bd0

.field public static final cs_542_renew_265:I = 0x7f130bd1

.field public static final cs_542_renew_266:I = 0x7f130bd2

.field public static final cs_542_renew_267:I = 0x7f130bd3

.field public static final cs_542_renew_268:I = 0x7f130bd4

.field public static final cs_542_renew_269:I = 0x7f130bd5

.field public static final cs_542_renew_27:I = 0x7f130bd6

.field public static final cs_542_renew_271:I = 0x7f130bd7

.field public static final cs_542_renew_272:I = 0x7f130bd8

.field public static final cs_542_renew_273:I = 0x7f130bd9

.field public static final cs_542_renew_274:I = 0x7f130bda

.field public static final cs_542_renew_275:I = 0x7f130bdb

.field public static final cs_542_renew_276:I = 0x7f130bdc

.field public static final cs_542_renew_28:I = 0x7f130bdd

.field public static final cs_542_renew_29:I = 0x7f130bde

.field public static final cs_542_renew_3:I = 0x7f130bdf

.field public static final cs_542_renew_30:I = 0x7f130be0

.field public static final cs_542_renew_31:I = 0x7f130be1

.field public static final cs_542_renew_32:I = 0x7f130be2

.field public static final cs_542_renew_33:I = 0x7f130be3

.field public static final cs_542_renew_34:I = 0x7f130be4

.field public static final cs_542_renew_35:I = 0x7f130be5

.field public static final cs_542_renew_36:I = 0x7f130be6

.field public static final cs_542_renew_37:I = 0x7f130be7

.field public static final cs_542_renew_38:I = 0x7f130be8

.field public static final cs_542_renew_39:I = 0x7f130be9

.field public static final cs_542_renew_40:I = 0x7f130bea

.field public static final cs_542_renew_41:I = 0x7f130beb

.field public static final cs_542_renew_42:I = 0x7f130bec

.field public static final cs_542_renew_43:I = 0x7f130bed

.field public static final cs_542_renew_44:I = 0x7f130bee

.field public static final cs_542_renew_45:I = 0x7f130bef

.field public static final cs_542_renew_46:I = 0x7f130bf0

.field public static final cs_542_renew_47:I = 0x7f130bf1

.field public static final cs_542_renew_49:I = 0x7f130bf2

.field public static final cs_542_renew_5:I = 0x7f130bf3

.field public static final cs_542_renew_50:I = 0x7f130bf4

.field public static final cs_542_renew_55:I = 0x7f130bf5

.field public static final cs_542_renew_58:I = 0x7f130bf6

.field public static final cs_542_renew_59:I = 0x7f130bf7

.field public static final cs_542_renew_6:I = 0x7f130bf8

.field public static final cs_542_renew_60:I = 0x7f130bf9

.field public static final cs_542_renew_61:I = 0x7f130bfa

.field public static final cs_542_renew_62:I = 0x7f130bfb

.field public static final cs_542_renew_63:I = 0x7f130bfc

.field public static final cs_542_renew_65:I = 0x7f130bfd

.field public static final cs_542_renew_66:I = 0x7f130bfe

.field public static final cs_542_renew_67:I = 0x7f130bff

.field public static final cs_542_renew_68:I = 0x7f130c00

.field public static final cs_542_renew_69:I = 0x7f130c01

.field public static final cs_542_renew_7:I = 0x7f130c02

.field public static final cs_542_renew_70:I = 0x7f130c03

.field public static final cs_542_renew_71:I = 0x7f130c04

.field public static final cs_542_renew_72:I = 0x7f130c05

.field public static final cs_542_renew_77:I = 0x7f130c06

.field public static final cs_542_renew_79:I = 0x7f130c07

.field public static final cs_542_renew_8:I = 0x7f130c08

.field public static final cs_542_renew_80:I = 0x7f130c09

.field public static final cs_542_renew_82:I = 0x7f130c0a

.field public static final cs_542_renew_83:I = 0x7f130c0b

.field public static final cs_542_renew_86:I = 0x7f130c0c

.field public static final cs_542_renew_87:I = 0x7f130c0d

.field public static final cs_542_renew_88:I = 0x7f130c0e

.field public static final cs_542_renew_89:I = 0x7f130c0f

.field public static final cs_542_renew_9:I = 0x7f130c10

.field public static final cs_542_renew_90:I = 0x7f130c11

.field public static final cs_542_renew_91:I = 0x7f130c12

.field public static final cs_542_renew_92:I = 0x7f130c13

.field public static final cs_542_renew_93:I = 0x7f130c14

.field public static final cs_542_renew_94:I = 0x7f130c15

.field public static final cs_542_renew_95:I = 0x7f130c16

.field public static final cs_542_renew_96:I = 0x7f130c17

.field public static final cs_542_renew_97:I = 0x7f130c18

.field public static final cs_542_renew_98:I = 0x7f130c19

.field public static final cs_542_renew_99:I = 0x7f130c1a

.field public static final cs_542_username_blank:I = 0x7f130c1b

.field public static final cs_542_username_new:I = 0x7f130c1c

.field public static final cs_542_watermark_01:I = 0x7f130c1d

.field public static final cs_543_LoginRegister_04:I = 0x7f130c1e

.field public static final cs_543_LoginRegister_06:I = 0x7f130c1f

.field public static final cs_543_LoginRegister_08:I = 0x7f130c20

.field public static final cs_543_account_01:I = 0x7f130c21

.field public static final cs_543_account_02:I = 0x7f130c22

.field public static final cs_543_count_dowm_01:I = 0x7f130c23

.field public static final cs_543_count_dowm_02:I = 0x7f130c24

.field public static final cs_543_count_dowm_03:I = 0x7f130c25

.field public static final cs_543_count_dowm_04:I = 0x7f130c26

.field public static final cs_543_count_dowm_05:I = 0x7f130c27

.field public static final cs_543_count_dowm_06:I = 0x7f130c28

.field public static final cs_543_count_dowm_07:I = 0x7f130c29

.field public static final cs_543_create_jigsaw:I = 0x7f130c2a

.field public static final cs_543_export_word:I = 0x7f130c2b

.field public static final cs_543_label_01:I = 0x7f130c2c

.field public static final cs_543_label_02:I = 0x7f130c2d

.field public static final cs_543_label_03:I = 0x7f130c2e

.field public static final cs_543_label_04:I = 0x7f130c2f

.field public static final cs_543_label_05:I = 0x7f130c30

.field public static final cs_544_new:I = 0x7f130c31

.field public static final cs_544_new_app:I = 0x7f130c32

.field public static final cs_544_new_app_guide:I = 0x7f130c33

.field public static final cs_544_new_app_view:I = 0x7f130c34

.field public static final cs_544_retain_02:I = 0x7f130c35

.field public static final cs_544_retain_03:I = 0x7f130c36

.field public static final cs_544_retain_04:I = 0x7f130c37

.field public static final cs_544_retain_05:I = 0x7f130c38

.field public static final cs_544_wanliu_01:I = 0x7f130c39

.field public static final cs_544_wanliu_02:I = 0x7f130c3a

.field public static final cs_544_wanliu_03:I = 0x7f130c3b

.field public static final cs_544_wanliu_05:I = 0x7f130c3c

.field public static final cs_544_wanliu_06:I = 0x7f130c3d

.field public static final cs_545_newuser_04:I = 0x7f130c3e

.field public static final cs_545_pop_01:I = 0x7f130c3f

.field public static final cs_545_pop_02:I = 0x7f130c40

.field public static final cs_545_pop_03:I = 0x7f130c41

.field public static final cs_545_retainpop_01:I = 0x7f130c42

.field public static final cs_545_retainpop_02:I = 0x7f130c43

.field public static final cs_545_retainpop_04:I = 0x7f130c44

.field public static final cs_545_scandone_rewardsuccess:I = 0x7f130c45

.field public static final cs_545_scandonereward_01:I = 0x7f130c46

.field public static final cs_545_scandonereward_02:I = 0x7f130c47

.field public static final cs_545_scandonereward_03:I = 0x7f130c48

.field public static final cs_546_label_10:I = 0x7f130c49

.field public static final cs_546_label_33:I = 0x7f130c4a

.field public static final cs_546_message_activity:I = 0x7f130c4b

.field public static final cs_546_message_business:I = 0x7f130c4c

.field public static final cs_546_message_empty:I = 0x7f130c4d

.field public static final cs_546_message_readed:I = 0x7f130c4e

.field public static final cs_546_message_reload:I = 0x7f130c4f

.field public static final cs_546_message_reload_setting:I = 0x7f130c50

.field public static final cs_546_message_reload_tip:I = 0x7f130c51

.field public static final cs_546_message_vip:I = 0x7f130c52

.field public static final cs_546_phototoword_01:I = 0x7f130c53

.field public static final cs_546_phototoword_02:I = 0x7f130c54

.field public static final cs_546_phototoword_03:I = 0x7f130c55

.field public static final cs_546_phototoword_04:I = 0x7f130c56

.field public static final cs_546_phototoword_05:I = 0x7f130c57

.field public static final cs_546_push_btn:I = 0x7f130c58

.field public static final cs_546_push_btn2:I = 0x7f130c59

.field public static final cs_546_push_personal_btn:I = 0x7f130c5a

.field public static final cs_546_push_personal_tip:I = 0x7f130c5b

.field public static final cs_546_push_setting:I = 0x7f130c5c

.field public static final cs_546_push_time_tip1:I = 0x7f130c5d

.field public static final cs_546_push_timesetting:I = 0x7f130c5e

.field public static final cs_546_system_message:I = 0x7f130c5f

.field public static final cs_546_word_export_create:I = 0x7f130c60

.field public static final cs_546_word_fail_recon:I = 0x7f130c61

.field public static final cs_547_android11_01:I = 0x7f130c62

.field public static final cs_547_android11_02:I = 0x7f130c63

.field public static final cs_547_extract_text:I = 0x7f130c64

.field public static final cs_547_pdf_12:I = 0x7f130c65

.field public static final cs_547_pdf_21:I = 0x7f130c66

.field public static final cs_547_pdfwatermarkfree_01:I = 0x7f130c67

.field public static final cs_547_phototoword_04:I = 0x7f130c68

.field public static final cs_547_phototoword_05:I = 0x7f130c69

.field public static final cs_547_phototoword_06:I = 0x7f130c6a

.field public static final cs_547_phototoword_07:I = 0x7f130c6b

.field public static final cs_547_phototoword_08:I = 0x7f130c6c

.field public static final cs_547_toolbox_01:I = 0x7f130c6d

.field public static final cs_547_toolbox_02:I = 0x7f130c6e

.field public static final cs_547_toolbox_05:I = 0x7f130c6f

.field public static final cs_547_toolbox_06:I = 0x7f130c70

.field public static final cs_548_assistgetvip_05:I = 0x7f130c71

.field public static final cs_548_cannot_process:I = 0x7f130c72

.field public static final cs_548_cannot_process_content:I = 0x7f130c73

.field public static final cs_548_cloud_to_upgrade:I = 0x7f130c75

.field public static final cs_548_external_ocr:I = 0x7f130c76

.field public static final cs_548_retain_01:I = 0x7f130c77

.field public static final cs_548_retain_02:I = 0x7f130c78

.field public static final cs_548_retain_03:I = 0x7f130c79

.field public static final cs_548_retain_04:I = 0x7f130c7a

.field public static final cs_548_safe_child:I = 0x7f130c7b

.field public static final cs_549_label_01:I = 0x7f130c7c

.field public static final cs_549_label_09:I = 0x7f130c7d

.field public static final cs_549_log_in_due_to_no_image:I = 0x7f130c7e

.field public static final cs_549_message_end:I = 0x7f130c7f

.field public static final cs_549_message_reload_failed:I = 0x7f130c80

.field public static final cs_549_no_image:I = 0x7f130c81

.field public static final cs_549_ocr_freebuttom:I = 0x7f130c82

.field public static final cs_549_retain_01:I = 0x7f130c83

.field public static final cs_549_retain_02:I = 0x7f130c84

.field public static final cs_549_search_08:I = 0x7f130c85

.field public static final cs_549_usinvite_14:I = 0x7f130c86

.field public static final cs_549_usinvite_17:I = 0x7f130c87

.field public static final cs_549_usinvite_18:I = 0x7f130c88

.field public static final cs_549_usinvite_21:I = 0x7f130c89

.field public static final cs_549_usinvite_22:I = 0x7f130c8a

.field public static final cs_549_usinvite_38:I = 0x7f130c8b

.field public static final cs_550_biology:I = 0x7f130c8c

.field public static final cs_550_camelpdf_tools_016:I = 0x7f130c8d

.field public static final cs_550_cannot_download:I = 0x7f130c8e

.field public static final cs_550_cannot_process:I = 0x7f130c8f

.field public static final cs_550_capture_question:I = 0x7f130c90

.field public static final cs_550_capture_test_paper:I = 0x7f130c91

.field public static final cs_550_check_network:I = 0x7f130c92

.field public static final cs_550_chemistry:I = 0x7f130c93

.field public static final cs_550_chinese:I = 0x7f130c94

.field public static final cs_550_compare:I = 0x7f130c95

.field public static final cs_550_create_qbook:I = 0x7f130c96

.field public static final cs_550_download:I = 0x7f130c97

.field public static final cs_550_download2:I = 0x7f130c98

.field public static final cs_550_english:I = 0x7f130c99

.field public static final cs_550_final_exam:I = 0x7f130c9a

.field public static final cs_550_geography:I = 0x7f130c9b

.field public static final cs_550_gift_02:I = 0x7f130c9c

.field public static final cs_550_grade:I = 0x7f130c9d

.field public static final cs_550_grade1:I = 0x7f130c9e

.field public static final cs_550_grade10:I = 0x7f130c9f

.field public static final cs_550_grade11:I = 0x7f130ca0

.field public static final cs_550_grade12:I = 0x7f130ca1

.field public static final cs_550_grade2:I = 0x7f130ca2

.field public static final cs_550_grade3:I = 0x7f130ca3

.field public static final cs_550_grade4:I = 0x7f130ca4

.field public static final cs_550_grade5:I = 0x7f130ca5

.field public static final cs_550_grade6:I = 0x7f130ca6

.field public static final cs_550_grade7:I = 0x7f130ca7

.field public static final cs_550_grade8:I = 0x7f130ca8

.field public static final cs_550_grade9:I = 0x7f130ca9

.field public static final cs_550_graduate:I = 0x7f130caa

.field public static final cs_550_hide_origin:I = 0x7f130cab

.field public static final cs_550_history:I = 0x7f130cac

.field public static final cs_550_law:I = 0x7f130cad

.field public static final cs_550_liberal:I = 0x7f130cae

.field public static final cs_550_login_02:I = 0x7f130caf

.field public static final cs_550_maths:I = 0x7f130cb0

.field public static final cs_550_message_accept:I = 0x7f130cb1

.field public static final cs_550_message_deny:I = 0x7f130cb2

.field public static final cs_550_middle_test:I = 0x7f130cb3

.field public static final cs_550_monthly_test:I = 0x7f130cb4

.field public static final cs_550_no_network:I = 0x7f130cb5

.field public static final cs_550_off_campus:I = 0x7f130cb6

.field public static final cs_550_other_grade:I = 0x7f130cb7

.field public static final cs_550_paper_type:I = 0x7f130cb8

.field public static final cs_550_physics:I = 0x7f130cb9

.field public static final cs_550_politics:I = 0x7f130cba

.field public static final cs_550_practice_test:I = 0x7f130cbb

.field public static final cs_550_process_failed:I = 0x7f130cbc

.field public static final cs_550_processing:I = 0x7f130cbd

.field public static final cs_550_qbook_answer1:I = 0x7f130cbe

.field public static final cs_550_qbook_answer2:I = 0x7f130cbf

.field public static final cs_550_qbook_answer3:I = 0x7f130cc0

.field public static final cs_550_qbook_answer4:I = 0x7f130cc1

.field public static final cs_550_qbook_answer5:I = 0x7f130cc2

.field public static final cs_550_qbook_answer6:I = 0x7f130cc3

.field public static final cs_550_qbook_guide2:I = 0x7f130cc4

.field public static final cs_550_qbook_question1:I = 0x7f130cc5

.field public static final cs_550_qbook_question2:I = 0x7f130cc6

.field public static final cs_550_qbook_quide:I = 0x7f130cc7

.field public static final cs_550_qbook_quide3:I = 0x7f130cc8

.field public static final cs_550_quiz:I = 0x7f130cc9

.field public static final cs_550_retry:I = 0x7f130cca

.field public static final cs_550_science:I = 0x7f130ccb

.field public static final cs_550_science2:I = 0x7f130ccc

.field public static final cs_550_search_10:I = 0x7f130ccd

.field public static final cs_550_search_24:I = 0x7f130cce

.field public static final cs_550_search_27:I = 0x7f130ccf

.field public static final cs_550_select_wrong_answer:I = 0x7f130cd0

.field public static final cs_550_semester:I = 0x7f130cd1

.field public static final cs_550_semester1:I = 0x7f130cd2

.field public static final cs_550_semester2:I = 0x7f130cd3

.field public static final cs_550_servant:I = 0x7f130cd4

.field public static final cs_550_student:I = 0x7f130cd5

.field public static final cs_550_subject:I = 0x7f130cd6

.field public static final cs_550_teacher:I = 0x7f130cd7

.field public static final cs_550_test_paper:I = 0x7f130cd8

.field public static final cs_550_test_paper_reach_limit:I = 0x7f130cd9

.field public static final cs_550_test_paper_reach_limit2:I = 0x7f130cda

.field public static final cs_550_test_paper_save:I = 0x7f130cdb

.field public static final cs_550_test_paper_stay:I = 0x7f130cdc

.field public static final cs_550_translate_01:I = 0x7f130cdd

.field public static final cs_550_translate_02:I = 0x7f130cde

.field public static final cs_550_translate_03:I = 0x7f130cdf

.field public static final cs_550_translate_04:I = 0x7f130ce0

.field public static final cs_550_translate_05:I = 0x7f130ce1

.field public static final cs_550_translate_06:I = 0x7f130ce2

.field public static final cs_550_translate_07:I = 0x7f130ce3

.field public static final cs_550_translate_08:I = 0x7f130ce4

.field public static final cs_550_unit_test:I = 0x7f130ce5

.field public static final cs_550_unsubscribe_03:I = 0x7f130ce6

.field public static final cs_550_unsubscribe_04:I = 0x7f130ce7

.field public static final cs_550_unsubscribe_05:I = 0x7f130ce8

.field public static final cs_550_unsubscribe_06:I = 0x7f130ce9

.field public static final cs_550_unsubscribe_07:I = 0x7f130cea

.field public static final cs_550_view_original:I = 0x7f130ceb

.field public static final cs_551_guide_01:I = 0x7f130cec

.field public static final cs_551_guide_02:I = 0x7f130ced

.field public static final cs_551_guide_03:I = 0x7f130cee

.field public static final cs_551_guide_04:I = 0x7f130cef

.field public static final cs_551_guide_05:I = 0x7f130cf0

.field public static final cs_551_guide_06:I = 0x7f130cf1

.field public static final cs_551_guide_07:I = 0x7f130cf2

.field public static final cs_551_guide_08:I = 0x7f130cf3

.field public static final cs_551_inviteentrance_01:I = 0x7f130cf4

.field public static final cs_551_page_number_04:I = 0x7f130cf5

.field public static final cs_551_please_select:I = 0x7f130cf6

.field public static final cs_551_premium_02:I = 0x7f130cf7

.field public static final cs_551_premium_04:I = 0x7f130cf8

.field public static final cs_551_premium_05:I = 0x7f130cf9

.field public static final cs_551_premium_06:I = 0x7f130cfa

.field public static final cs_551_premium_07:I = 0x7f130cfb

.field public static final cs_551_premium_08:I = 0x7f130cfc

.field public static final cs_551_premium_09:I = 0x7f130cfd

.field public static final cs_551_premium_10:I = 0x7f130cfe

.field public static final cs_551_premium_11:I = 0x7f130cff

.field public static final cs_551_premium_12:I = 0x7f130d00

.field public static final cs_551_premium_13:I = 0x7f130d01

.field public static final cs_551_premium_14:I = 0x7f130d02

.field public static final cs_551_premium_15:I = 0x7f130d03

.field public static final cs_551_premium_16:I = 0x7f130d04

.field public static final cs_551_premium_17:I = 0x7f130d05

.field public static final cs_551_premium_18:I = 0x7f130d06

.field public static final cs_551_premium_19:I = 0x7f130d07

.field public static final cs_551_premium_20:I = 0x7f130d08

.field public static final cs_551_premium_21:I = 0x7f130d09

.field public static final cs_551_scenario_05:I = 0x7f130d0a

.field public static final cs_551_scenario_06:I = 0x7f130d0b

.field public static final cs_551_scenario_07:I = 0x7f130d0c

.field public static final cs_551_scenario_08:I = 0x7f130d0d

.field public static final cs_551_scenario_09:I = 0x7f130d0e

.field public static final cs_551_scenario_10:I = 0x7f130d0f

.field public static final cs_551_scenario_11:I = 0x7f130d10

.field public static final cs_551_scenario_12:I = 0x7f130d11

.field public static final cs_551_scenario_13:I = 0x7f130d12

.field public static final cs_551_scenario_14:I = 0x7f130d13

.field public static final cs_551_scenario_15:I = 0x7f130d14

.field public static final cs_551_scenario_16:I = 0x7f130d15

.field public static final cs_551_scenario_17:I = 0x7f130d16

.field public static final cs_551_scenario_18:I = 0x7f130d17

.field public static final cs_551_scenario_19:I = 0x7f130d18

.field public static final cs_551_scenario_20:I = 0x7f130d19

.field public static final cs_551_scenario_21:I = 0x7f130d1a

.field public static final cs_551_scenario_22:I = 0x7f130d1b

.field public static final cs_551_scenario_23:I = 0x7f130d1c

.field public static final cs_551_scenario_24:I = 0x7f130d1d

.field public static final cs_551_scenario_25:I = 0x7f130d1e

.field public static final cs_551_scenario_26:I = 0x7f130d1f

.field public static final cs_551_scenario_27:I = 0x7f130d20

.field public static final cs_551_scenario_32:I = 0x7f130d21

.field public static final cs_551_scenario_33:I = 0x7f130d22

.field public static final cs_551_title_test1:I = 0x7f130d23

.field public static final cs_551_title_test3:I = 0x7f130d24

.field public static final cs_551_wordupdate_09:I = 0x7f130d25

.field public static final cs_551_wordupdate_10:I = 0x7f130d26

.field public static final cs_552_item_limit:I = 0x7f130d27

.field public static final cs_552_new_note:I = 0x7f130d28

.field public static final cs_552_vipreward_01:I = 0x7f130d29

.field public static final cs_552_vipreward_02:I = 0x7f130d2a

.field public static final cs_552_vipreward_03:I = 0x7f130d2b

.field public static final cs_552_vipreward_04:I = 0x7f130d2c

.field public static final cs_552_vipreward_05:I = 0x7f130d2d

.field public static final cs_552_vipreward_06:I = 0x7f130d2e

.field public static final cs_552_vipreward_07:I = 0x7f130d2f

.field public static final cs_552_vipreward_08:I = 0x7f130d30

.field public static final cs_552_vipreward_09:I = 0x7f130d31

.field public static final cs_552_vipreward_10:I = 0x7f130d32

.field public static final cs_552_vipreward_11:I = 0x7f130d33

.field public static final cs_552_vipreward_12:I = 0x7f130d34

.field public static final cs_552_vipreward_13:I = 0x7f130d35

.field public static final cs_552_vipreward_14:I = 0x7f130d36

.field public static final cs_552_vipreward_15:I = 0x7f130d37

.field public static final cs_552_vipreward_16:I = 0x7f130d38

.field public static final cs_552_vipreward_17:I = 0x7f130d39

.field public static final cs_552_vipreward_18:I = 0x7f130d3a

.field public static final cs_552_vipreward_19:I = 0x7f130d3b

.field public static final cs_552_vipreward_20:I = 0x7f130d3c

.field public static final cs_552_vipreward_21:I = 0x7f130d3d

.field public static final cs_552_vipreward_22:I = 0x7f130d3e

.field public static final cs_552_vipreward_23:I = 0x7f130d3f

.field public static final cs_552_vipreward_24:I = 0x7f130d40

.field public static final cs_552_vipreward_25:I = 0x7f130d41

.field public static final cs_552_watermark_buyonce_01:I = 0x7f130d42

.field public static final cs_552_watermark_buyonce_02:I = 0x7f130d43

.field public static final cs_552_watermark_buyonce_10:I = 0x7f130d44

.field public static final cs_553_link_10:I = 0x7f130d45

.field public static final cs_553_link_12:I = 0x7f130d46

.field public static final cs_553_printer_01:I = 0x7f130d47

.field public static final cs_553_printer_02:I = 0x7f130d48

.field public static final cs_553_printer_03:I = 0x7f130d49

.field public static final cs_553_printer_04:I = 0x7f130d4a

.field public static final cs_553_printer_06:I = 0x7f130d4b

.field public static final cs_553_printer_07:I = 0x7f130d4c

.field public static final cs_553_printer_08:I = 0x7f130d4d

.field public static final cs_553_printer_09:I = 0x7f130d4e

.field public static final cs_553_printer_10:I = 0x7f130d4f

.field public static final cs_553_printer_11:I = 0x7f130d50

.field public static final cs_553_printer_13:I = 0x7f130d51

.field public static final cs_553_printer_14:I = 0x7f130d52

.field public static final cs_553_printer_15:I = 0x7f130d53

.field public static final cs_553_printer_16:I = 0x7f130d54

.field public static final cs_553_printer_17:I = 0x7f130d55

.field public static final cs_553_printer_18:I = 0x7f130d56

.field public static final cs_553_printer_20:I = 0x7f130d57

.field public static final cs_553_printer_21:I = 0x7f130d58

.field public static final cs_553_printer_22:I = 0x7f130d59

.field public static final cs_553_printer_23:I = 0x7f130d5a

.field public static final cs_553_printer_24:I = 0x7f130d5b

.field public static final cs_553_printer_25:I = 0x7f130d5c

.field public static final cs_553_printer_26:I = 0x7f130d5d

.field public static final cs_553_printer_27:I = 0x7f130d5e

.field public static final cs_553_printer_28:I = 0x7f130d5f

.field public static final cs_553_printer_29:I = 0x7f130d60

.field public static final cs_553_printer_30:I = 0x7f130d61

.field public static final cs_553_printer_31:I = 0x7f130d62

.field public static final cs_553_printer_32:I = 0x7f130d63

.field public static final cs_553_printer_33:I = 0x7f130d64

.field public static final cs_553_printer_34:I = 0x7f130d65

.field public static final cs_553_printer_35:I = 0x7f130d66

.field public static final cs_553_printer_36:I = 0x7f130d67

.field public static final cs_553_printer_37:I = 0x7f130d68

.field public static final cs_553_printer_38:I = 0x7f130d69

.field public static final cs_553_printer_39:I = 0x7f130d6a

.field public static final cs_553_printer_40:I = 0x7f130d6b

.field public static final cs_553_printer_41:I = 0x7f130d6c

.field public static final cs_553_printer_42:I = 0x7f130d6d

.field public static final cs_553_printer_43:I = 0x7f130d6e

.field public static final cs_553_printer_45:I = 0x7f130d6f

.field public static final cs_553_printer_46:I = 0x7f130d70

.field public static final cs_553_printer_47:I = 0x7f130d71

.field public static final cs_553_printer_48:I = 0x7f130d72

.field public static final cs_553_printer_49:I = 0x7f130d73

.field public static final cs_553_printer_51:I = 0x7f130d74

.field public static final cs_553_printer_53:I = 0x7f130d75

.field public static final cs_553_printer_54:I = 0x7f130d76

.field public static final cs_553_printer_55:I = 0x7f130d77

.field public static final cs_553_printer_57:I = 0x7f130d78

.field public static final cs_553_printer_59:I = 0x7f130d79

.field public static final cs_553_printer_60:I = 0x7f130d7a

.field public static final cs_553_printer_61:I = 0x7f130d7b

.field public static final cs_553_printer_62:I = 0x7f130d7c

.field public static final cs_553_printer_63:I = 0x7f130d7d

.field public static final cs_553_printer_64:I = 0x7f130d7e

.field public static final cs_553_printer_65:I = 0x7f130d7f

.field public static final cs_553_printer_66:I = 0x7f130d80

.field public static final cs_553_printer_67:I = 0x7f130d81

.field public static final cs_553_printer_68:I = 0x7f130d82

.field public static final cs_553_printer_69:I = 0x7f130d83

.field public static final cs_553_printer_72:I = 0x7f130d84

.field public static final cs_553_printer_73:I = 0x7f130d85

.field public static final cs_553_printer_74:I = 0x7f130d86

.field public static final cs_553_printer_75:I = 0x7f130d87

.field public static final cs_553_printer_76:I = 0x7f130d88

.field public static final cs_553_printer_81:I = 0x7f130d89

.field public static final cs_553_printer_82:I = 0x7f130d8a

.field public static final cs_553_printer_83:I = 0x7f130d8b

.field public static final cs_553_printer_84:I = 0x7f130d8c

.field public static final cs_553_rate_1:I = 0x7f130d8d

.field public static final cs_553_rate_2:I = 0x7f130d8e

.field public static final cs_554_create_qbook:I = 0x7f130d8f

.field public static final cs_554_edu1:I = 0x7f130d90

.field public static final cs_554_edu10:I = 0x7f130d91

.field public static final cs_554_edu12:I = 0x7f130d92

.field public static final cs_554_edu13_svip:I = 0x7f130d93

.field public static final cs_554_edu14:I = 0x7f130d94

.field public static final cs_554_edu2:I = 0x7f130d95

.field public static final cs_554_edu3_svip:I = 0x7f130d96

.field public static final cs_554_edu4:I = 0x7f130d97

.field public static final cs_554_edu5:I = 0x7f130d98

.field public static final cs_554_edu6:I = 0x7f130d99

.field public static final cs_554_edu7:I = 0x7f130d9a

.field public static final cs_554_edu_1:I = 0x7f130d9b

.field public static final cs_554_edu_2:I = 0x7f130d9c

.field public static final cs_554_export_failed:I = 0x7f130d9d

.field public static final cs_554_export_pdf:I = 0x7f130d9e

.field public static final cs_554_exporting_pdf:I = 0x7f130d9f

.field public static final cs_554_failed_other_reason:I = 0x7f130da0

.field public static final cs_554_split_pdf:I = 0x7f130da1

.field public static final cs_554_split_pdf1:I = 0x7f130da2

.field public static final cs_554_split_pdf2:I = 0x7f130da3

.field public static final cs_595_10_14_oversea:I = 0x7f130da5

.field public static final cs_595_14_18_oversea:I = 0x7f130da6

.field public static final cs_595_14_20_oversea:I = 0x7f130da7

.field public static final cs_595_15_buypage_coupon:I = 0x7f130da8

.field public static final cs_595_15_main_retrievepop:I = 0x7f130da9

.field public static final cs_595_15_newpop_limit:I = 0x7f130daa

.field public static final cs_595_16_20_oversea:I = 0x7f130dab

.field public static final cs_595_16_22_oversea:I = 0x7f130dac

.field public static final cs_595_ID_color_all:I = 0x7f130dad

.field public static final cs_595_ID_color_blue_red:I = 0x7f130dae

.field public static final cs_595_ID_color_blue_white:I = 0x7f130daf

.field public static final cs_595_ID_color_blue_white_red:I = 0x7f130db0

.field public static final cs_595_accountant_test:I = 0x7f130db1

.field public static final cs_595_afganistan_visa:I = 0x7f130db2

.field public static final cs_595_album:I = 0x7f130db3

.field public static final cs_595_australia_visa:I = 0x7f130db4

.field public static final cs_595_australian_visa_oversea:I = 0x7f130db5

.field public static final cs_595_austria_visa:I = 0x7f130db6

.field public static final cs_595_austrian_visa_oversea:I = 0x7f130db7

.field public static final cs_595_bangladesh_visa:I = 0x7f130db8

.field public static final cs_595_bangladesh_visa_oversea:I = 0x7f130db9

.field public static final cs_595_bank_card:I = 0x7f130dba

.field public static final cs_595_belgium_visa:I = 0x7f130dbb

.field public static final cs_595_belgium_visa_oversea:I = 0x7f130dbc

.field public static final cs_595_blue:I = 0x7f130dbd

.field public static final cs_595_brunei_visa:I = 0x7f130dbe

.field public static final cs_595_burma_visa:I = 0x7f130dbf

.field public static final cs_595_burma_visa_oversea:I = 0x7f130dc0

.field public static final cs_595_buy_points:I = 0x7f130dc1

.field public static final cs_595_cambodia_visa:I = 0x7f130dc2

.field public static final cs_595_camera:I = 0x7f130dc3

.field public static final cs_595_certificates_A4:I = 0x7f130dc4

.field public static final cs_595_certificates_drive_license:I = 0x7f130dc5

.field public static final cs_595_certificates_go_try:I = 0x7f130dc6

.field public static final cs_595_certificates_no_watermark_file_size:I = 0x7f130dc7

.field public static final cs_595_certificates_pattern:I = 0x7f130dc8

.field public static final cs_595_check_account_five_minutes:I = 0x7f130dc9

.field public static final cs_595_check_file:I = 0x7f130dca

.field public static final cs_595_china_government_test:I = 0x7f130dcb

.field public static final cs_595_china_visa_oversea:I = 0x7f130dcc

.field public static final cs_595_collage_recommend:I = 0x7f130dcd

.field public static final cs_595_collage_tips:I = 0x7f130dce

.field public static final cs_595_common_size:I = 0x7f130dcf

.field public static final cs_595_computer_test:I = 0x7f130dd0

.field public static final cs_595_continue_ocr:I = 0x7f130dd1

.field public static final cs_595_create_photo:I = 0x7f130dd2

.field public static final cs_595_czech_visa:I = 0x7f130dd3

.field public static final cs_595_czech_visa_oversea:I = 0x7f130dd4

.field public static final cs_595_disclaimer:I = 0x7f130dd5

.field public static final cs_595_divorce_certificate:I = 0x7f130dd6

.field public static final cs_595_doc_transfer_word:I = 0x7f130dd7

.field public static final cs_595_driver_license:I = 0x7f130dd8

.field public static final cs_595_dubai_visa:I = 0x7f130dd9

.field public static final cs_595_english_four_six:I = 0x7f130dda

.field public static final cs_595_finland_visa:I = 0x7f130ddb

.field public static final cs_595_first_level_constructor:I = 0x7f130ddc

.field public static final cs_595_five_inch:I = 0x7f130ddd

.field public static final cs_595_france_visa:I = 0x7f130dde

.field public static final cs_595_french_visa_oversea:I = 0x7f130ddf

.field public static final cs_595_go_taiwan_certificate:I = 0x7f130de0

.field public static final cs_595_guide_features_cloud_space:I = 0x7f130de1

.field public static final cs_595_guide_features_column_title_basic:I = 0x7f130de2

.field public static final cs_595_guide_features_column_title_premium:I = 0x7f130de3

.field public static final cs_595_guide_features_column_title_privilege:I = 0x7f130de4

.field public static final cs_595_guide_features_id:I = 0x7f130de5

.field public static final cs_595_guide_features_more:I = 0x7f130de6

.field public static final cs_595_guide_features_ocr:I = 0x7f130de7

.field public static final cs_595_guide_features_pdf:I = 0x7f130de8

.field public static final cs_595_guide_features_premium:I = 0x7f130de9

.field public static final cs_595_guide_features_scan:I = 0x7f130dea

.field public static final cs_595_guide_pay_premium_classified_folder:I = 0x7f130deb

.field public static final cs_595_guide_pay_premium_collage:I = 0x7f130dec

.field public static final cs_595_guide_pay_premium_free_trial:I = 0x7f130ded

.field public static final cs_595_guide_pay_premium_new_user_only:I = 0x7f130dee

.field public static final cs_595_health_card:I = 0x7f130def

.field public static final cs_595_hk_macao_pass:I = 0x7f130df0

.field public static final cs_595_iceland_visa:I = 0x7f130df1

.field public static final cs_595_iceland_visa_oversea:I = 0x7f130df2

.field public static final cs_595_id_card_in_bank:I = 0x7f130df3

.field public static final cs_595_id_photo:I = 0x7f130df4

.field public static final cs_595_id_photo_premium_pop:I = 0x7f130df5

.field public static final cs_595_id_photo_provider:I = 0x7f130df6

.field public static final cs_595_id_recognize:I = 0x7f130df7

.field public static final cs_595_indian_visa:I = 0x7f130df8

.field public static final cs_595_indian_visa_oversea:I = 0x7f130df9

.field public static final cs_595_iran_visa:I = 0x7f130dfa

.field public static final cs_595_japan_visa:I = 0x7f130dfb

.field public static final cs_595_japan_visa_oversea:I = 0x7f130dfc

.field public static final cs_595_kazakhstan_visa:I = 0x7f130dfd

.field public static final cs_595_kenya_visa:I = 0x7f130dfe

.field public static final cs_595_kenya_visa_oversea:I = 0x7f130dff

.field public static final cs_595_korea_visa_oversea:I = 0x7f130e00

.field public static final cs_595_korean_visa:I = 0x7f130e01

.field public static final cs_595_kyrghyzstan:I = 0x7f130e02

.field public static final cs_595_laos_visa:I = 0x7f130e03

.field public static final cs_595_laos_visa_oversea:I = 0x7f130e04

.field public static final cs_595_larger_one_inch:I = 0x7f130e05

.field public static final cs_595_larger_two_inch:I = 0x7f130e06

.field public static final cs_595_law_test:I = 0x7f130e07

.field public static final cs_595_malaysia_visa:I = 0x7f130e08

.field public static final cs_595_malaysia_visa_oversea:I = 0x7f130e09

.field public static final cs_595_mandarin_test:I = 0x7f130e0a

.field public static final cs_595_medical_care_certificate:I = 0x7f130e0b

.field public static final cs_595_mongolia_visa:I = 0x7f130e0c

.field public static final cs_595_mongolian_visa_oversea:I = 0x7f130e0d

.field public static final cs_595_nepal_visa:I = 0x7f130e0e

.field public static final cs_595_nepal_visa_oversea:I = 0x7f130e0f

.field public static final cs_595_new_zealand_visa:I = 0x7f130e10

.field public static final cs_595_new_zealand_visa_oversea:I = 0x7f130e11

.field public static final cs_595_no_watermark:I = 0x7f130e12

.field public static final cs_595_no_watermark_change:I = 0x7f130e13

.field public static final cs_595_ocr_new_feature:I = 0x7f130e14

.field public static final cs_595_one_inch:I = 0x7f130e15

.field public static final cs_595_pakistan_visa:I = 0x7f130e16

.field public static final cs_595_pakistan_visa_oversea:I = 0x7f130e17

.field public static final cs_595_passport_no_receipt:I = 0x7f130e18

.field public static final cs_595_pdf:I = 0x7f130e19

.field public static final cs_595_philippine_visa:I = 0x7f130e1a

.field public static final cs_595_philippine_visa_oversea:I = 0x7f130e1b

.field public static final cs_595_points_number:I = 0x7f130e1c

.field public static final cs_595_points_pop_one:I = 0x7f130e1d

.field public static final cs_595_points_pop_two:I = 0x7f130e1e

.field public static final cs_595_points_used_pop:I = 0x7f130e1f

.field public static final cs_595_postgraduate_test:I = 0x7f130e20

.field public static final cs_595_preview:I = 0x7f130e21

.field public static final cs_595_processing:I = 0x7f130e22

.field public static final cs_595_processing_cloud_pop:I = 0x7f130e23

.field public static final cs_595_recharged:I = 0x7f130e24

.field public static final cs_595_residence_certificate:I = 0x7f130e25

.field public static final cs_595_russia_visa:I = 0x7f130e26

.field public static final cs_595_russia_visa_oversea:I = 0x7f130e27

.field public static final cs_595_save_to_local_only_folder:I = 0x7f130e28

.field public static final cs_595_saved_in_local_only_folder:I = 0x7f130e29

.field public static final cs_595_second_china_id_card:I = 0x7f130e2a

.field public static final cs_595_second_generation_id_no_receipt:I = 0x7f130e2b

.field public static final cs_595_second_level_constructor:I = 0x7f130e2c

.field public static final cs_595_select_photo_size:I = 0x7f130e2d

.field public static final cs_595_singapore_visa:I = 0x7f130e2e

.field public static final cs_595_singapore_visa_oversea:I = 0x7f130e2f

.field public static final cs_595_smaller_one_inch:I = 0x7f130e30

.field public static final cs_595_smaller_two_inch:I = 0x7f130e31

.field public static final cs_595_social_security_card_no_receipt:I = 0x7f130e32

.field public static final cs_595_syria_visa:I = 0x7f130e33

.field public static final cs_595_teacher_test:I = 0x7f130e34

.field public static final cs_595_thai_visa_oversea:I = 0x7f130e35

.field public static final cs_595_thailand_visa:I = 0x7f130e36

.field public static final cs_595_three_inch:I = 0x7f130e37

.field public static final cs_595_timely_recharge:I = 0x7f130e38

.field public static final cs_595_tips:I = 0x7f130e39

.field public static final cs_595_tips_two:I = 0x7f130e3a

.field public static final cs_595_tour_guide_test:I = 0x7f130e3b

.field public static final cs_595_transfer_word:I = 0x7f130e3c

.field public static final cs_595_transfer_word_premium_pop:I = 0x7f130e3d

.field public static final cs_595_turkmenistan:I = 0x7f130e3e

.field public static final cs_595_two_inch:I = 0x7f130e3f

.field public static final cs_595_upgrade_premium:I = 0x7f130e40

.field public static final cs_595_us_passport_photo:I = 0x7f130e41

.field public static final cs_595_usa_visa:I = 0x7f130e42

.field public static final cs_595_use_points:I = 0x7f130e43

.field public static final cs_595_various_certificate:I = 0x7f130e44

.field public static final cs_595_various_visa:I = 0x7f130e45

.field public static final cs_595_vietnam_visa_forty_sixty:I = 0x7f130e46

.field public static final cs_595_vietnam_visa_oversea:I = 0x7f130e47

.field public static final cs_595_vietnam_visa_thirty_five:I = 0x7f130e48

.field public static final cs_595_watermark_mode2_1:I = 0x7f130e49

.field public static final cs_595_watermark_mode2_2:I = 0x7f130e4a

.field public static final cs_595_watermark_preview:I = 0x7f130e4b

.field public static final cs_595_watermark_remove:I = 0x7f130e4c

.field public static final cs_595_white:I = 0x7f130e4d

.field public static final cs_596_afganistan:I = 0x7f130e4e

.field public static final cs_596_albania:I = 0x7f130e4f

.field public static final cs_596_algieria:I = 0x7f130e50

.field public static final cs_596_american_samoa:I = 0x7f130e51

.field public static final cs_596_andorra:I = 0x7f130e52

.field public static final cs_596_angola:I = 0x7f130e53

.field public static final cs_596_anguilla:I = 0x7f130e54

.field public static final cs_596_antigua_barbuda:I = 0x7f130e55

.field public static final cs_596_argentina:I = 0x7f130e56

.field public static final cs_596_armenia:I = 0x7f130e57

.field public static final cs_596_aruba:I = 0x7f130e58

.field public static final cs_596_ascension:I = 0x7f130e59

.field public static final cs_596_australia:I = 0x7f130e5a

.field public static final cs_596_austria:I = 0x7f130e5b

.field public static final cs_596_azerbaijan:I = 0x7f130e5c

.field public static final cs_596_bahamas:I = 0x7f130e5d

.field public static final cs_596_bahrain:I = 0x7f130e5e

.field public static final cs_596_bangladesh:I = 0x7f130e5f

.field public static final cs_596_barbados:I = 0x7f130e60

.field public static final cs_596_belarus:I = 0x7f130e61

.field public static final cs_596_belgium:I = 0x7f130e62

.field public static final cs_596_belize:I = 0x7f130e63

.field public static final cs_596_benin:I = 0x7f130e64

.field public static final cs_596_bermuda:I = 0x7f130e65

.field public static final cs_596_bhutark_bhutan:I = 0x7f130e66

.field public static final cs_596_bolivia:I = 0x7f130e67

.field public static final cs_596_bosnia_and_herzegovina:I = 0x7f130e68

.field public static final cs_596_botswana:I = 0x7f130e69

.field public static final cs_596_brazil:I = 0x7f130e6a

.field public static final cs_596_british_virgin_islands:I = 0x7f130e6b

.field public static final cs_596_brunei:I = 0x7f130e6c

.field public static final cs_596_bulgaria:I = 0x7f130e6d

.field public static final cs_596_burkina_faso:I = 0x7f130e6e

.field public static final cs_596_burma:I = 0x7f130e6f

.field public static final cs_596_burundi:I = 0x7f130e70

.field public static final cs_596_cambodia:I = 0x7f130e71

.field public static final cs_596_cameroon:I = 0x7f130e72

.field public static final cs_596_canada:I = 0x7f130e73

.field public static final cs_596_cape_verde:I = 0x7f130e74

.field public static final cs_596_cayman_islands:I = 0x7f130e75

.field public static final cs_596_central_republic:I = 0x7f130e76

.field public static final cs_596_chad:I = 0x7f130e77

.field public static final cs_596_chile:I = 0x7f130e78

.field public static final cs_596_china:I = 0x7f130e79

.field public static final cs_596_colombia:I = 0x7f130e7a

.field public static final cs_596_comorrk_comoros:I = 0x7f130e7b

.field public static final cs_596_congo:I = 0x7f130e7c

.field public static final cs_596_cook_islands:I = 0x7f130e7d

.field public static final cs_596_costa_rica:I = 0x7f130e7e

.field public static final cs_596_croatrk_croatia:I = 0x7f130e7f

.field public static final cs_596_cuba:I = 0x7f130e80

.field public static final cs_596_cyprus:I = 0x7f130e81

.field public static final cs_596_czech_republic:I = 0x7f130e82

.field public static final cs_596_denmark:I = 0x7f130e83

.field public static final cs_596_djibouti:I = 0x7f130e84

.field public static final cs_596_dominican_republic:I = 0x7f130e85

.field public static final cs_596_dominrk_dominica:I = 0x7f130e86

.field public static final cs_596_ecuador:I = 0x7f130e87

.field public static final cs_596_egypt:I = 0x7f130e88

.field public static final cs_596_ei_salvador:I = 0x7f130e89

.field public static final cs_596_equatrk_guinea:I = 0x7f130e8a

.field public static final cs_596_eritrrk_eritrea:I = 0x7f130e8b

.field public static final cs_596_estonia:I = 0x7f130e8c

.field public static final cs_596_ethiopia:I = 0x7f130e8d

.field public static final cs_596_falklrk_islands:I = 0x7f130e8e

.field public static final cs_596_faroerk_islands:I = 0x7f130e8f

.field public static final cs_596_fiji:I = 0x7f130e90

.field public static final cs_596_finland:I = 0x7f130e91

.field public static final cs_596_france:I = 0x7f130e92

.field public static final cs_596_french_guiana:I = 0x7f130e93

.field public static final cs_596_french_polynesia:I = 0x7f130e94

.field public static final cs_596_gabon:I = 0x7f130e95

.field public static final cs_596_gambia:I = 0x7f130e96

.field public static final cs_596_georgia:I = 0x7f130e97

.field public static final cs_596_germany:I = 0x7f130e98

.field public static final cs_596_ghana:I = 0x7f130e99

.field public static final cs_596_gibraltar:I = 0x7f130e9a

.field public static final cs_596_greece:I = 0x7f130e9b

.field public static final cs_596_greenrk_greenland:I = 0x7f130e9c

.field public static final cs_596_grenada:I = 0x7f130e9d

.field public static final cs_596_guam:I = 0x7f130e9e

.field public static final cs_596_guatemala:I = 0x7f130e9f

.field public static final cs_596_guinea:I = 0x7f130ea0

.field public static final cs_596_guinea_bissau:I = 0x7f130ea1

.field public static final cs_596_guyana:I = 0x7f130ea2

.field public static final cs_596_haiti:I = 0x7f130ea3

.field public static final cs_596_honduras:I = 0x7f130ea4

.field public static final cs_596_hong_kong:I = 0x7f130ea5

.field public static final cs_596_hungary:I = 0x7f130ea6

.field public static final cs_596_iceland:I = 0x7f130ea7

.field public static final cs_596_india:I = 0x7f130ea8

.field public static final cs_596_indonesia:I = 0x7f130ea9

.field public static final cs_596_indyk:I = 0x7f130eaa

.field public static final cs_596_iran:I = 0x7f130eab

.field public static final cs_596_iraq:I = 0x7f130eac

.field public static final cs_596_ireland:I = 0x7f130ead

.field public static final cs_596_israel:I = 0x7f130eae

.field public static final cs_596_italy:I = 0x7f130eaf

.field public static final cs_596_ivory_coast:I = 0x7f130eb0

.field public static final cs_596_jamaica:I = 0x7f130eb1

.field public static final cs_596_japan:I = 0x7f130eb2

.field public static final cs_596_jemen:I = 0x7f130eb3

.field public static final cs_596_jordan:I = 0x7f130eb4

.field public static final cs_596_kazakstan:I = 0x7f130eb5

.field public static final cs_596_kenya:I = 0x7f130eb6

.field public static final cs_596_kiribrk_kiribati:I = 0x7f130eb7

.field public static final cs_596_korea:I = 0x7f130eb8

.field public static final cs_596_kuwait:I = 0x7f130eb9

.field public static final cs_596_kyrgyzstan:I = 0x7f130eba

.field public static final cs_596_laos:I = 0x7f130ebb

.field public static final cs_596_latvia:I = 0x7f130ebc

.field public static final cs_596_lebanon:I = 0x7f130ebd

.field public static final cs_596_lesotho:I = 0x7f130ebe

.field public static final cs_596_liberia:I = 0x7f130ebf

.field public static final cs_596_libya:I = 0x7f130ec0

.field public static final cs_596_liechtenstein:I = 0x7f130ec1

.field public static final cs_596_lithuania:I = 0x7f130ec2

.field public static final cs_596_luxembourg:I = 0x7f130ec3

.field public static final cs_596_macao:I = 0x7f130ec4

.field public static final cs_596_macedrk_macedonia:I = 0x7f130ec5

.field public static final cs_596_madagascar:I = 0x7f130ec6

.field public static final cs_596_malawi:I = 0x7f130ec7

.field public static final cs_596_malaysia:I = 0x7f130ec8

.field public static final cs_596_maldives:I = 0x7f130ec9

.field public static final cs_596_mali:I = 0x7f130eca

.field public static final cs_596_malta:I = 0x7f130ecb

.field public static final cs_596_mariana_islands:I = 0x7f130ecc

.field public static final cs_596_marshrk_islands:I = 0x7f130ecd

.field public static final cs_596_martinique:I = 0x7f130ece

.field public static final cs_596_maurirk_mauritania:I = 0x7f130ecf

.field public static final cs_596_mauritius:I = 0x7f130ed0

.field public static final cs_596_mexico:I = 0x7f130ed1

.field public static final cs_596_micrork_micronesia:I = 0x7f130ed2

.field public static final cs_596_moldova:I = 0x7f130ed3

.field public static final cs_596_monaco:I = 0x7f130ed4

.field public static final cs_596_mongolia:I = 0x7f130ed5

.field public static final cs_596_monterk_montenegro:I = 0x7f130ed6

.field public static final cs_596_montserrat:I = 0x7f130ed7

.field public static final cs_596_morocco:I = 0x7f130ed8

.field public static final cs_596_mozambique:I = 0x7f130ed9

.field public static final cs_596_namibia:I = 0x7f130eda

.field public static final cs_596_nauru:I = 0x7f130edb

.field public static final cs_596_nepal:I = 0x7f130edc

.field public static final cs_596_netherlands_antilles:I = 0x7f130edd

.field public static final cs_596_netherlands_netherlands:I = 0x7f130ede

.field public static final cs_596_new_crk_caledonia:I = 0x7f130edf

.field public static final cs_596_new_zealand:I = 0x7f130ee0

.field public static final cs_596_nicaragua:I = 0x7f130ee1

.field public static final cs_596_niger:I = 0x7f130ee2

.field public static final cs_596_nigeria:I = 0x7f130ee3

.field public static final cs_596_niue:I = 0x7f130ee4

.field public static final cs_596_norfork_island:I = 0x7f130ee5

.field public static final cs_596_north_korea:I = 0x7f130ee6

.field public static final cs_596_norway:I = 0x7f130ee7

.field public static final cs_596_ocean_territory:I = 0x7f130ee8

.field public static final cs_596_oman:I = 0x7f130ee9

.field public static final cs_596_pakistan:I = 0x7f130eea

.field public static final cs_596_palaurk_palau:I = 0x7f130eeb

.field public static final cs_596_palesrk_palestine:I = 0x7f130eec

.field public static final cs_596_panama:I = 0x7f130eed

.field public static final cs_596_papua_cuinea:I = 0x7f130eee

.field public static final cs_596_paraguay:I = 0x7f130eef

.field public static final cs_596_peru:I = 0x7f130ef0

.field public static final cs_596_philippines:I = 0x7f130ef1

.field public static final cs_596_poland:I = 0x7f130ef2

.field public static final cs_596_portugal:I = 0x7f130ef3

.field public static final cs_596_puerto_rico:I = 0x7f130ef4

.field public static final cs_596_qatar:I = 0x7f130ef5

.field public static final cs_596_reguilla:I = 0x7f130ef6

.field public static final cs_596_romania:I = 0x7f130ef7

.field public static final cs_596_russia:I = 0x7f130ef8

.field public static final cs_596_rwandrk_rwanda:I = 0x7f130ef9

.field public static final cs_596_saint_barthelemy:I = 0x7f130efa

.field public static final cs_596_saint_helena:I = 0x7f130efb

.field public static final cs_596_saint_lueia:I = 0x7f130efc

.field public static final cs_596_saint_martin:I = 0x7f130efd

.field public static final cs_596_saint_vincent:I = 0x7f130efe

.field public static final cs_596_saintrk_miquelon:I = 0x7f130eff

.field public static final cs_596_saintrk_nevis:I = 0x7f130f00

.field public static final cs_596_samoa_eastern:I = 0x7f130f01

.field public static final cs_596_samoa_western:I = 0x7f130f02

.field public static final cs_596_san_marino:I = 0x7f130f03

.field public static final cs_596_sao_principe:I = 0x7f130f04

.field public static final cs_596_saudi_arabia:I = 0x7f130f05

.field public static final cs_596_senegal:I = 0x7f130f06

.field public static final cs_596_serbirk_serbia:I = 0x7f130f07

.field public static final cs_596_seychelles:I = 0x7f130f08

.field public static final cs_596_sierra_leone:I = 0x7f130f09

.field public static final cs_596_singapore:I = 0x7f130f0a

.field public static final cs_596_sint_maarten:I = 0x7f130f0b

.field public static final cs_596_slovakia:I = 0x7f130f0c

.field public static final cs_596_slovenia:I = 0x7f130f0d

.field public static final cs_596_solomon_islands:I = 0x7f130f0e

.field public static final cs_596_somalijski:I = 0x7f130f0f

.field public static final cs_596_south_africa:I = 0x7f130f10

.field public static final cs_596_southrk_sudan:I = 0x7f130f11

.field public static final cs_596_spain:I = 0x7f130f12

.field public static final cs_596_sri_lanka:I = 0x7f130f13

.field public static final cs_596_sudan:I = 0x7f130f14

.field public static final cs_596_suriname:I = 0x7f130f15

.field public static final cs_596_swaziland:I = 0x7f130f16

.field public static final cs_596_sweden:I = 0x7f130f17

.field public static final cs_596_switzerland:I = 0x7f130f18

.field public static final cs_596_syria:I = 0x7f130f19

.field public static final cs_596_taiwan:I = 0x7f130f1a

.field public static final cs_596_tajikstan:I = 0x7f130f1b

.field public static final cs_596_tanzania:I = 0x7f130f1c

.field public static final cs_596_thailand:I = 0x7f130f1d

.field public static final cs_596_togo:I = 0x7f130f1e

.field public static final cs_596_tokelrk_tokelau:I = 0x7f130f1f

.field public static final cs_596_tonga:I = 0x7f130f20

.field public static final cs_596_trinidad_tobago:I = 0x7f130f21

.field public static final cs_596_tunisia:I = 0x7f130f22

.field public static final cs_596_turkmenistan:I = 0x7f130f23

.field public static final cs_596_turks_and_caicos_islands:I = 0x7f130f24

.field public static final cs_596_tuvalrk_tuvalu:I = 0x7f130f25

.field public static final cs_596_uganda:I = 0x7f130f26

.field public static final cs_596_ukraine:I = 0x7f130f27

.field public static final cs_596_united_america:I = 0x7f130f28

.field public static final cs_596_united_emirates:I = 0x7f130f29

.field public static final cs_596_united_kingdom:I = 0x7f130f2a

.field public static final cs_596_uruguay:I = 0x7f130f2b

.field public static final cs_596_us_virgin_islands:I = 0x7f130f2c

.field public static final cs_596_uzbekistan:I = 0x7f130f2d

.field public static final cs_596_vanuark_vanuatu:I = 0x7f130f2e

.field public static final cs_596_vaticrk_vatican:I = 0x7f130f2f

.field public static final cs_596_venezuela:I = 0x7f130f30

.field public static final cs_596_vietnam:I = 0x7f130f31

.field public static final cs_596_yugoslavia:I = 0x7f130f32

.field public static final cs_596_zaire:I = 0x7f130f33

.field public static final cs_596_zambia:I = 0x7f130f34

.field public static final cs_596_zimbabwe:I = 0x7f130f35

.field public static final cs_600_android11_01:I = 0x7f130f36

.field public static final cs_600_android11_02:I = 0x7f130f37

.field public static final cs_600_android11_03:I = 0x7f130f38

.field public static final cs_600_android11_04:I = 0x7f130f39

.field public static final cs_600_android11_05:I = 0x7f130f3a

.field public static final cs_604_demoire:I = 0x7f130f3b

.field public static final cs_604_demoire_loading1:I = 0x7f130f3c

.field public static final cs_604_demoire_loading2:I = 0x7f130f3d

.field public static final cs_604_demoire_loading3:I = 0x7f130f3e

.field public static final cs_604_demoire_more:I = 0x7f130f3f

.field public static final cs_604_demoire_now:I = 0x7f130f40

.field public static final cs_604_detect_moire:I = 0x7f130f41

.field public static final cs_605_feedback:I = 0x7f130f42

.field public static final cs_605_feedback_guide:I = 0x7f130f43

.field public static final cs_605_feedback_guide2:I = 0x7f130f44

.field public static final cs_605_feedback_guide4:I = 0x7f130f45

.field public static final cs_605_feedback_guide5:I = 0x7f130f46

.field public static final cs_605_feedback_guide7:I = 0x7f130f47

.field public static final cs_608_learn_more:I = 0x7f130f48

.field public static final cs_609_hint_authorization2_photo_restoration:I = 0x7f130f49

.field public static final cs_609_hint_move_slider:I = 0x7f130f4a

.field public static final cs_609_refer:I = 0x7f130f4b

.field public static final cs_609_refer_content1:I = 0x7f130f4c

.field public static final cs_609_refer_content2:I = 0x7f130f4d

.field public static final cs_609_refer_content3:I = 0x7f130f4e

.field public static final cs_609_refer_content4:I = 0x7f130f4f

.field public static final cs_609_title_refer:I = 0x7f130f50

.field public static final cs_609_title_share:I = 0x7f130f51

.field public static final cs_610_claim_03:I = 0x7f130f52

.field public static final cs_610_delete_confirm:I = 0x7f130f53

.field public static final cs_610_error_image_corrupted:I = 0x7f130f54

.field public static final cs_610_error_network:I = 0x7f130f55

.field public static final cs_610_error_no_folder:I = 0x7f130f56

.field public static final cs_610_error_no_space:I = 0x7f130f57

.field public static final cs_610_error_server:I = 0x7f130f58

.field public static final cs_610_error_vip_no_folder:I = 0x7f130f59

.field public static final cs_610_error_vip_no_space:I = 0x7f130f5a

.field public static final cs_610_excelupdate_03:I = 0x7f130f5b

.field public static final cs_610_link_create_failed:I = 0x7f130f5c

.field public static final cs_610_login_security:I = 0x7f130f5d

.field public static final cs_610_login_security02:I = 0x7f130f5e

.field public static final cs_610_login_security03:I = 0x7f130f5f

.field public static final cs_610_protocol_desc_2:I = 0x7f130f60

.field public static final cs_610_search_commend_title:I = 0x7f130f61

.field public static final cs_610_security_browse:I = 0x7f130f62

.field public static final cs_610_security_logout:I = 0x7f130f63

.field public static final cs_610_share_pdf:I = 0x7f130f64

.field public static final cs_611_book:I = 0x7f130f65

.field public static final cs_611_camera_01:I = 0x7f130f66

.field public static final cs_611_camera_02:I = 0x7f130f67

.field public static final cs_611_camera_03:I = 0x7f130f68

.field public static final cs_611_camera_04:I = 0x7f130f69

.field public static final cs_611_camera_05:I = 0x7f130f6a

.field public static final cs_611_camera_06:I = 0x7f130f6b

.field public static final cs_611_camera_07:I = 0x7f130f6c

.field public static final cs_611_camera_08:I = 0x7f130f6d

.field public static final cs_611_camera_10:I = 0x7f130f6e

.field public static final cs_611_camera_12:I = 0x7f130f6f

.field public static final cs_611_camera_15:I = 0x7f130f70

.field public static final cs_611_camera_16:I = 0x7f130f71

.field public static final cs_611_camera_17:I = 0x7f130f72

.field public static final cs_611_cancellation02:I = 0x7f130f73

.field public static final cs_611_cancellation03:I = 0x7f130f74

.field public static final cs_611_cancellation04:I = 0x7f130f75

.field public static final cs_611_cancellation05:I = 0x7f130f76

.field public static final cs_611_cancellation06:I = 0x7f130f77

.field public static final cs_611_cancellation07:I = 0x7f130f78

.field public static final cs_611_cancellation11:I = 0x7f130f79

.field public static final cs_611_cancellation13:I = 0x7f130f7a

.field public static final cs_611_cslist02:I = 0x7f130f7b

.field public static final cs_611_cslist06:I = 0x7f130f7c

.field public static final cs_611_cslist09:I = 0x7f130f7d

.field public static final cs_611_cslist10:I = 0x7f130f7e

.field public static final cs_611_newguide_01:I = 0x7f130f7f

.field public static final cs_611_newguide_02:I = 0x7f130f80

.field public static final cs_611_newguide_03:I = 0x7f130f81

.field public static final cs_611_newguide_04:I = 0x7f130f82

.field public static final cs_611_newguide_05:I = 0x7f130f83

.field public static final cs_611_tag01:I = 0x7f130f84

.field public static final cs_611_tag02:I = 0x7f130f85

.field public static final cs_611_tag03:I = 0x7f130f86

.field public static final cs_611_tag05:I = 0x7f130f87

.field public static final cs_611_tag06:I = 0x7f130f88

.field public static final cs_611_tag07:I = 0x7f130f89

.field public static final cs_611_tag08:I = 0x7f130f8a

.field public static final cs_612_camera_01:I = 0x7f130f8b

.field public static final cs_612_camera_02:I = 0x7f130f8c

.field public static final cs_612_identification_01:I = 0x7f130f8d

.field public static final cs_612_identification_02:I = 0x7f130f8e

.field public static final cs_612_identification_03:I = 0x7f130f8f

.field public static final cs_612_identification_04:I = 0x7f130f90

.field public static final cs_612_identification_05:I = 0x7f130f91

.field public static final cs_612_identification_06:I = 0x7f130f92

.field public static final cs_612_identification_07:I = 0x7f130f93

.field public static final cs_612_identification_08:I = 0x7f130f94

.field public static final cs_612_identification_09:I = 0x7f130f95

.field public static final cs_612_identification_10:I = 0x7f130f96

.field public static final cs_612_superPDF_01:I = 0x7f130f97

.field public static final cs_612_superPDF_02:I = 0x7f130f98

.field public static final cs_612_superPDF_04:I = 0x7f130f99

.field public static final cs_612_superPDF_06:I = 0x7f130f9a

.field public static final cs_612_superPDF_07:I = 0x7f130f9b

.field public static final cs_612_superPDF_08:I = 0x7f130f9c

.field public static final cs_612_superPDF_09:I = 0x7f130f9d

.field public static final cs_612_superPDF_10:I = 0x7f130f9e

.field public static final cs_612_superPDF_11:I = 0x7f130f9f

.field public static final cs_612_watermark_01:I = 0x7f130fa0

.field public static final cs_613_crop_01:I = 0x7f130fa1

.field public static final cs_613_crop_02:I = 0x7f130fa2

.field public static final cs_613_document_share:I = 0x7f130fa3

.field public static final cs_613_gift_02:I = 0x7f130fa4

.field public static final cs_613_gift_03:I = 0x7f130fa5

.field public static final cs_613_gift_04:I = 0x7f130fa6

.field public static final cs_613_gift_05:I = 0x7f130fa7

.field public static final cs_613_gift_06:I = 0x7f130fa8

.field public static final cs_613_gift_07:I = 0x7f130fa9

.field public static final cs_613_gift_08:I = 0x7f130faa

.field public static final cs_613_gift_09:I = 0x7f130fab

.field public static final cs_613_gift_10:I = 0x7f130fac

.field public static final cs_613_gift_11:I = 0x7f130fad

.field public static final cs_613_gift_12:I = 0x7f130fae

.field public static final cs_613_gift_13:I = 0x7f130faf

.field public static final cs_613_gift_16:I = 0x7f130fb0

.field public static final cs_613_link_share:I = 0x7f130fb1

.field public static final cs_613_link_tips:I = 0x7f130fb2

.field public static final cs_613_ocrguide_01:I = 0x7f130fb3

.field public static final cs_613_ocrguide_02:I = 0x7f130fb4

.field public static final cs_613_watermark_tips:I = 0x7f130fb5

.field public static final cs_614_button_colorize:I = 0x7f130fb6

.field public static final cs_614_button_system_import:I = 0x7f130fb7

.field public static final cs_614_content_android11_permission:I = 0x7f130fb8

.field public static final cs_614_content_system_import:I = 0x7f130fb9

.field public static final cs_614_file_01:I = 0x7f130fba

.field public static final cs_614_file_02:I = 0x7f130fbb

.field public static final cs_614_file_03:I = 0x7f130fbc

.field public static final cs_614_file_08:I = 0x7f130fbd

.field public static final cs_614_guide_01:I = 0x7f130fbe

.field public static final cs_614_guide_02:I = 0x7f130fbf

.field public static final cs_614_guide_03:I = 0x7f130fc0

.field public static final cs_614_guide_04:I = 0x7f130fc1

.field public static final cs_614_guide_05:I = 0x7f130fc2

.field public static final cs_614_guide_06:I = 0x7f130fc3

.field public static final cs_614_guide_07:I = 0x7f130fc4

.field public static final cs_614_guide_08:I = 0x7f130fc5

.field public static final cs_614_guide_09:I = 0x7f130fc6

.field public static final cs_614_guide_10:I = 0x7f130fc7

.field public static final cs_614_guide_11:I = 0x7f130fc8

.field public static final cs_614_privacy_setting01:I = 0x7f130fc9

.field public static final cs_614_privacy_setting02:I = 0x7f130fca

.field public static final cs_614_privacy_setting03:I = 0x7f130fcb

.field public static final cs_614_privacy_setting04:I = 0x7f130fcc

.field public static final cs_614_privacy_setting05:I = 0x7f130fcd

.field public static final cs_614_privacy_setting06:I = 0x7f130fce

.field public static final cs_614_privacy_setting_title:I = 0x7f130fcf

.field public static final cs_614_submanagement:I = 0x7f130fd0

.field public static final cs_614_tab_colorize:I = 0x7f130fd1

.field public static final cs_614_tab_enhance:I = 0x7f130fd2

.field public static final cs_614_title_android11_permission:I = 0x7f130fd3

.field public static final cs_614_title_colorize:I = 0x7f130fd4

.field public static final cs_614_title_enhance:I = 0x7f130fd5

.field public static final cs_614_title_system_import:I = 0x7f130fd6

.field public static final cs_614_watermark_01:I = 0x7f130fd7

.field public static final cs_614_watermark_03:I = 0x7f130fd8

.field public static final cs_614_watermark_04:I = 0x7f130fd9

.field public static final cs_614_watermark_05:I = 0x7f130fda

.field public static final cs_614_watermark_06:I = 0x7f130fdb

.field public static final cs_614_watermark_08:I = 0x7f130fdc

.field public static final cs_614_watermark_09:I = 0x7f130fdd

.field public static final cs_615_camera_03:I = 0x7f130fde

.field public static final cs_615_camera_04:I = 0x7f130fdf

.field public static final cs_615_camera_05:I = 0x7f130fe0

.field public static final cs_615_csreward_01:I = 0x7f130fe1

.field public static final cs_615_csreward_02:I = 0x7f130fe2

.field public static final cs_615_csreward_03:I = 0x7f130fe3

.field public static final cs_616_guide_closetip:I = 0x7f130fe4

.field public static final cs_616_new_user_share_guid01:I = 0x7f130fe5

.field public static final cs_616_new_user_share_guid02:I = 0x7f130fe6

.field public static final cs_616_new_user_share_guid03:I = 0x7f130fe7

.field public static final cs_617_gift_01:I = 0x7f130fe8

.field public static final cs_617_gift_03:I = 0x7f130fe9

.field public static final cs_617_gift_04:I = 0x7f130fea

.field public static final cs_617_gift_05:I = 0x7f130feb

.field public static final cs_617_gift_06:I = 0x7f130fec

.field public static final cs_617_share01:I = 0x7f130fed

.field public static final cs_617_share02:I = 0x7f130fee

.field public static final cs_617_share03:I = 0x7f130fef

.field public static final cs_617_share04:I = 0x7f130ff0

.field public static final cs_617_share08:I = 0x7f130ff1

.field public static final cs_617_share09:I = 0x7f130ff2

.field public static final cs_617_share100:I = 0x7f130ff3

.field public static final cs_617_share102:I = 0x7f130ff4

.field public static final cs_617_share13:I = 0x7f130ff5

.field public static final cs_617_share14:I = 0x7f130ff6

.field public static final cs_617_share22:I = 0x7f130ff7

.field public static final cs_617_share23:I = 0x7f130ff8

.field public static final cs_617_share24:I = 0x7f130ff9

.field public static final cs_617_share25:I = 0x7f130ffa

.field public static final cs_617_share26:I = 0x7f130ffb

.field public static final cs_617_share28:I = 0x7f130ffc

.field public static final cs_617_share29:I = 0x7f130ffd

.field public static final cs_617_share32:I = 0x7f130ffe

.field public static final cs_617_share36:I = 0x7f130fff

.field public static final cs_617_share39:I = 0x7f131000

.field public static final cs_617_share40:I = 0x7f131001

.field public static final cs_617_share41:I = 0x7f131002

.field public static final cs_617_share43:I = 0x7f131003

.field public static final cs_617_share44:I = 0x7f131004

.field public static final cs_617_share59:I = 0x7f131005

.field public static final cs_617_share61:I = 0x7f131006

.field public static final cs_617_share66:I = 0x7f131007

.field public static final cs_617_share67:I = 0x7f131008

.field public static final cs_617_share68:I = 0x7f131009

.field public static final cs_617_share74:I = 0x7f13100a

.field public static final cs_617_share75:I = 0x7f13100b

.field public static final cs_617_share77:I = 0x7f13100c

.field public static final cs_617_share78:I = 0x7f13100d

.field public static final cs_617_share80:I = 0x7f13100e

.field public static final cs_617_share81:I = 0x7f13100f

.field public static final cs_617_share82:I = 0x7f131010

.field public static final cs_617_share83:I = 0x7f131011

.field public static final cs_617_share85:I = 0x7f131012

.field public static final cs_617_share87:I = 0x7f131013

.field public static final cs_617_share88:I = 0x7f131014

.field public static final cs_617_share89:I = 0x7f131015

.field public static final cs_617_share90:I = 0x7f131016

.field public static final cs_617_share93:I = 0x7f131017

.field public static final cs_617_share94:I = 0x7f131018

.field public static final cs_617_share96:I = 0x7f131019

.field public static final cs_617_share98:I = 0x7f13101a

.field public static final cs_617_share99:I = 0x7f13101b

.field public static final cs_617_watch_video_01:I = 0x7f13101c

.field public static final cs_617_watch_video_02:I = 0x7f13101d

.field public static final cs_617_watch_video_03:I = 0x7f13101e

.field public static final cs_617_watch_video_04:I = 0x7f13101f

.field public static final cs_617_watch_video_05:I = 0x7f131020

.field public static final cs_617_watch_video_06:I = 0x7f131021

.field public static final cs_617_watch_video_07:I = 0x7f131022

.field public static final cs_617_watch_video_08:I = 0x7f131023

.field public static final cs_618_a4copy:I = 0x7f131024

.field public static final cs_618_add_credentials:I = 0x7f131025

.field public static final cs_618_car_info_id:I = 0x7f131026

.field public static final cs_618_car_info_num:I = 0x7f131027

.field public static final cs_618_company_info_id:I = 0x7f131028

.field public static final cs_618_company_info_name:I = 0x7f131029

.field public static final cs_618_delete_watermark:I = 0x7f13102a

.field public static final cs_618_drive_info_dt:I = 0x7f13102b

.field public static final cs_618_entrance_doclist:I = 0x7f13102c

.field public static final cs_618_export_pic:I = 0x7f13102d

.field public static final cs_618_export_pic_ing:I = 0x7f13102e

.field public static final cs_618_export_pic_success:I = 0x7f13102f

.field public static final cs_618_family_empty01:I = 0x7f131030

.field public static final cs_618_family_empty02:I = 0x7f131031

.field public static final cs_618_family_empty03:I = 0x7f131032

.field public static final cs_618_family_empty04:I = 0x7f131033

.field public static final cs_618_familyfolder_content:I = 0x7f131034

.field public static final cs_618_familyfolder_title:I = 0x7f131035

.field public static final cs_618_folder_title01:I = 0x7f131036

.field public static final cs_618_foldermore_delete:I = 0x7f131037

.field public static final cs_618_foldermore_move:I = 0x7f131038

.field public static final cs_618_foldermore_view:I = 0x7f131039

.field public static final cs_618_folderview_card:I = 0x7f13103a

.field public static final cs_618_folderview_time:I = 0x7f13103b

.field public static final cs_618_house_info_address:I = 0x7f13103c

.field public static final cs_618_house_info_hold:I = 0x7f13103d

.field public static final cs_618_huawei_installtip:I = 0x7f13103e

.field public static final cs_618_hukou_card:I = 0x7f13103f

.field public static final cs_618_hukou_info_hold:I = 0x7f131040

.field public static final cs_618_hukou_info_type:I = 0x7f131041

.field public static final cs_618_id_recognized_btn:I = 0x7f131042

.field public static final cs_618_id_recognized_tip:I = 0x7f131043

.field public static final cs_618_idcard_identity:I = 0x7f131044

.field public static final cs_618_idcard_info_addre:I = 0x7f131045

.field public static final cs_618_idcard_info_num:I = 0x7f131046

.field public static final cs_618_idcard_pay:I = 0x7f131047

.field public static final cs_618_idcard_recognizing:I = 0x7f131048

.field public static final cs_618_idcard_safe_btn:I = 0x7f131049

.field public static final cs_618_idcard_safe_desc:I = 0x7f13104a

.field public static final cs_618_idcard_safe_pop:I = 0x7f13104b

.field public static final cs_618_idcard_safe_set:I = 0x7f13104c

.field public static final cs_618_idcard_safe_tip01:I = 0x7f13104d

.field public static final cs_618_idcard_set:I = 0x7f13104e

.field public static final cs_618_idcard_set_content:I = 0x7f13104f

.field public static final cs_618_idcard_set_title:I = 0x7f131050

.field public static final cs_618_idcard_show:I = 0x7f131051

.field public static final cs_618_idcard_show_tip:I = 0x7f131052

.field public static final cs_618_idcard_wififail:I = 0x7f131053

.field public static final cs_618_idcard_wififail_tip:I = 0x7f131054

.field public static final cs_618_idcard_wififail_title:I = 0x7f131055

.field public static final cs_618_idcardfolder_content:I = 0x7f131056

.field public static final cs_618_idcardfolder_title:I = 0x7f131057

.field public static final cs_618_idea_empty01:I = 0x7f131058

.field public static final cs_618_idea_empty02:I = 0x7f131059

.field public static final cs_618_idea_empty03:I = 0x7f13105a

.field public static final cs_618_ideafolder_content:I = 0x7f13105b

.field public static final cs_618_ideafolder_title:I = 0x7f13105c

.field public static final cs_618_idimport_btn:I = 0x7f13105d

.field public static final cs_618_idimport_cancel:I = 0x7f13105e

.field public static final cs_618_idimport_finish:I = 0x7f13105f

.field public static final cs_618_idimport_finish_btn:I = 0x7f131060

.field public static final cs_618_idimport_haved:I = 0x7f131061

.field public static final cs_618_idimport_identifing:I = 0x7f131062

.field public static final cs_618_idimport_warn:I = 0x7f131063

.field public static final cs_618_idimport_warn_content:I = 0x7f131064

.field public static final cs_618_invoice:I = 0x7f131065

.field public static final cs_618_invoice_billtime:I = 0x7f131066

.field public static final cs_618_invoice_checktips:I = 0x7f131067

.field public static final cs_618_invoice_deleteone:I = 0x7f131068

.field public static final cs_618_invoice_duplicate:I = 0x7f131069

.field public static final cs_618_invoice_duplicate_btn_no:I = 0x7f13106a

.field public static final cs_618_invoice_duplicate_btn_yes:I = 0x7f13106b

.field public static final cs_618_invoice_duplicate_info:I = 0x7f13106c

.field public static final cs_618_invoice_face_delete:I = 0x7f13106d

.field public static final cs_618_invoice_face_delete_info:I = 0x7f13106e

.field public static final cs_618_invoice_face_head:I = 0x7f13106f

.field public static final cs_618_invoice_folder:I = 0x7f131070

.field public static final cs_618_invoice_guide:I = 0x7f131071

.field public static final cs_618_invoice_guide_info:I = 0x7f131072

.field public static final cs_618_invoice_guidetips:I = 0x7f131073

.field public static final cs_618_invoice_import_jpg:I = 0x7f131074

.field public static final cs_618_invoice_import_xlsx:I = 0x7f131075

.field public static final cs_618_invoice_import_zip:I = 0x7f131076

.field public static final cs_618_invoice_noresult:I = 0x7f131077

.field public static final cs_618_invoice_noresult_btn:I = 0x7f131078

.field public static final cs_618_invoice_noresult_info:I = 0x7f131079

.field public static final cs_618_invoice_noresult_tips:I = 0x7f13107a

.field public static final cs_618_invoice_retake_all:I = 0x7f13107b

.field public static final cs_618_invoice_sum:I = 0x7f13107c

.field public static final cs_618_invoice_twice:I = 0x7f13107d

.field public static final cs_618_me_reward_1:I = 0x7f13107e

.field public static final cs_618_me_reward_2:I = 0x7f13107f

.field public static final cs_618_page_watermark:I = 0x7f131080

.field public static final cs_618_passport_info_dt:I = 0x7f131081

.field public static final cs_618_passport_info_id:I = 0x7f131082

.field public static final cs_618_press_tip:I = 0x7f131083

.field public static final cs_618_saved_idcard01:I = 0x7f131084

.field public static final cs_618_saved_idcard02:I = 0x7f131085

.field public static final cs_618_saved_suggest:I = 0x7f131086

.field public static final cs_618_saved_to:I = 0x7f131087

.field public static final cs_618_saveto:I = 0x7f131088

.field public static final cs_618_saveto_btn:I = 0x7f131089

.field public static final cs_618_saveto_local01:I = 0x7f13108a

.field public static final cs_618_saveto_success:I = 0x7f13108b

.field public static final cs_618_scandone_reward_1:I = 0x7f13108c

.field public static final cs_618_scandone_reward_2:I = 0x7f13108d

.field public static final cs_618_share_long_image:I = 0x7f13108e

.field public static final cs_618_share_pdf:I = 0x7f13108f

.field public static final cs_618_share_pic:I = 0x7f131090

.field public static final cs_618_share_word:I = 0x7f131091

.field public static final cs_618_single_watermark:I = 0x7f131092

.field public static final cs_618_time_demo_title01:I = 0x7f131093

.field public static final cs_618_time_demo_title02:I = 0x7f131094

.field public static final cs_618_time_recordsmth:I = 0x7f131095

.field public static final cs_618_time_rename_tip:I = 0x7f131096

.field public static final cs_618_time_today:I = 0x7f131097

.field public static final cs_618_timefolder_content:I = 0x7f131098

.field public static final cs_618_timefolder_title:I = 0x7f131099

.field public static final cs_618_to_view:I = 0x7f13109a

.field public static final cs_618_try_now:I = 0x7f13109b

.field public static final cs_618_watch_video_01:I = 0x7f13109c

.field public static final cs_618_watch_video_02:I = 0x7f13109d

.field public static final cs_618_watch_video_03:I = 0x7f13109e

.field public static final cs_618_watch_video_04:I = 0x7f13109f

.field public static final cs_618_watch_video_05:I = 0x7f1310a0

.field public static final cs_618_watch_video_06:I = 0x7f1310a1

.field public static final cs_618_watch_video_07:I = 0x7f1310a2

.field public static final cs_618_watch_video_08:I = 0x7f1310a3

.field public static final cs_618_watch_video_09:I = 0x7f1310a4

.field public static final cs_618_watch_video_10:I = 0x7f1310a5

.field public static final cs_618_watch_video_11:I = 0x7f1310a6

.field public static final cs_618_watch_video_12:I = 0x7f1310a7

.field public static final cs_618_watch_video_13:I = 0x7f1310a8

.field public static final cs_618_watch_video_14:I = 0x7f1310a9

.field public static final cs_618_watch_video_15:I = 0x7f1310aa

.field public static final cs_618_watch_video_16:I = 0x7f1310ab

.field public static final cs_618_work_empty01:I = 0x7f1310ac

.field public static final cs_618_work_empty02:I = 0x7f1310ad

.field public static final cs_618_work_empty03:I = 0x7f1310ae

.field public static final cs_618_workfolder_content:I = 0x7f1310af

.field public static final cs_618_workfolder_title:I = 0x7f1310b0

.field public static final cs_619_basicvip_1:I = 0x7f1310b1

.field public static final cs_619_basicvip_2:I = 0x7f1310b2

.field public static final cs_619_body_check_network2:I = 0x7f1310b3

.field public static final cs_619_body_close_sync:I = 0x7f1310b4

.field public static final cs_619_body_invite_fail:I = 0x7f1310b5

.field public static final cs_619_body_invite_fail2:I = 0x7f1310b6

.field public static final cs_619_body_link_failed:I = 0x7f1310b7

.field public static final cs_619_body_sync_failed:I = 0x7f1310b8

.field public static final cs_619_body_sync_now:I = 0x7f1310b9

.field public static final cs_619_body_sync_now2:I = 0x7f1310ba

.field public static final cs_619_body_sync_now3:I = 0x7f1310bb

.field public static final cs_619_button_about_sync:I = 0x7f1310bc

.field public static final cs_619_button_home_sync:I = 0x7f1310bd

.field public static final cs_619_button_learn_account:I = 0x7f1310be

.field public static final cs_619_button_learn_more:I = 0x7f1310bf

.field public static final cs_619_button_ocr_more:I = 0x7f1310c0

.field public static final cs_619_button_sync_now2:I = 0x7f1310c1

.field public static final cs_619_grid_mode:I = 0x7f1310c2

.field public static final cs_619_guide__freemon:I = 0x7f1310c3

.field public static final cs_619_guide__freeyear:I = 0x7f1310c4

.field public static final cs_619_hint_security1:I = 0x7f1310c5

.field public static final cs_619_hint_security2:I = 0x7f1310c6

.field public static final cs_619_hint_sync:I = 0x7f1310c7

.field public static final cs_619_hint_sync_done:I = 0x7f1310c8

.field public static final cs_619_idphoto_01:I = 0x7f1310c9

.field public static final cs_619_large_mode:I = 0x7f1310ca

.field public static final cs_619_list_mode:I = 0x7f1310cb

.field public static final cs_619_radio_use_data:I = 0x7f1310cc

.field public static final cs_619_reward_01:I = 0x7f1310cd

.field public static final cs_619_reward_02:I = 0x7f1310ce

.field public static final cs_619_reward_03:I = 0x7f1310cf

.field public static final cs_619_reward_04:I = 0x7f1310d0

.field public static final cs_619_title_close_sync:I = 0x7f1310d1

.field public static final cs_619_title_invite_fail:I = 0x7f1310d2

.field public static final cs_619_title_note:I = 0x7f1310d3

.field public static final cs_619_title_ocr:I = 0x7f1310d4

.field public static final cs_619_title_open_sync:I = 0x7f1310d5

.field public static final cs_619_title_setting_security:I = 0x7f1310d6

.field public static final cs_619_title_sync_closed:I = 0x7f1310d7

.field public static final cs_620_bigimage_01:I = 0x7f1310d8

.field public static final cs_620_demoire_loading:I = 0x7f1310d9

.field public static final cs_620_folder_nom:I = 0x7f1310da

.field public static final cs_620_korea_01:I = 0x7f1310db

.field public static final cs_620_korea_14:I = 0x7f1310dc

.field public static final cs_620_label_02:I = 0x7f1310dd

.field public static final cs_620_label_03:I = 0x7f1310de

.field public static final cs_620_label_04:I = 0x7f1310df

.field public static final cs_620_label_05:I = 0x7f1310e0

.field public static final cs_620_label_06:I = 0x7f1310e1

.field public static final cs_620_label_07:I = 0x7f1310e2

.field public static final cs_620_label_08:I = 0x7f1310e3

.field public static final cs_620_month_apr:I = 0x7f1310e4

.field public static final cs_620_month_aug:I = 0x7f1310e5

.field public static final cs_620_month_feb:I = 0x7f1310e6

.field public static final cs_620_month_jan:I = 0x7f1310e7

.field public static final cs_620_month_july:I = 0x7f1310e8

.field public static final cs_620_month_june:I = 0x7f1310e9

.field public static final cs_620_month_mar:I = 0x7f1310ea

.field public static final cs_620_month_may:I = 0x7f1310eb

.field public static final cs_620_month_nov:I = 0x7f1310ec

.field public static final cs_620_month_oct:I = 0x7f1310ed

.field public static final cs_620_month_sep:I = 0x7f1310ee

.field public static final cs_620_sale_promtion_web_001:I = 0x7f1310ef

.field public static final cs_620_sale_promtion_web_002:I = 0x7f1310f0

.field public static final cs_620_sale_promtion_web_003:I = 0x7f1310f1

.field public static final cs_620_sale_promtion_web_008:I = 0x7f1310f2

.field public static final cs_620_sale_promtion_web_009:I = 0x7f1310f3

.field public static final cs_620_sale_promtion_web_015:I = 0x7f1310f4

.field public static final cs_620_sale_promtion_web_017:I = 0x7f1310f5

.field public static final cs_620_sale_promtion_web_018:I = 0x7f1310f6

.field public static final cs_620_sale_promtion_web_019:I = 0x7f1310f7

.field public static final cs_620_wechat_01:I = 0x7f1310f8

.field public static final cs_620_wechat_02:I = 0x7f1310f9

.field public static final cs_620_wechat_03:I = 0x7f1310fa

.field public static final cs_620_wechat_04:I = 0x7f1310fb

.field public static final cs_620_wechat_05:I = 0x7f1310fc

.field public static final cs_620_wechat_06:I = 0x7f1310fd

.field public static final cs_620_wechat_07:I = 0x7f1310fe

.field public static final cs_620_wechat_08:I = 0x7f1310ff

.field public static final cs_620_wechat_09:I = 0x7f131100

.field public static final cs_620_wechat_10:I = 0x7f131101

.field public static final cs_620_wechat_12:I = 0x7f131102

.field public static final cs_620_wechat_13:I = 0x7f131103

.field public static final cs_620_wechat_14:I = 0x7f131104

.field public static final cs_620_wechat_15:I = 0x7f131105

.field public static final cs_620_wechat_16:I = 0x7f131106

.field public static final cs_620_wechat_17:I = 0x7f131107

.field public static final cs_620_wechat_18:I = 0x7f131108

.field public static final cs_620_wechat_19:I = 0x7f131109

.field public static final cs_620_wechat_20:I = 0x7f13110a

.field public static final cs_620_wechat_21:I = 0x7f13110b

.field public static final cs_620_wechat_22:I = 0x7f13110c

.field public static final cs_620_wechat_23:I = 0x7f13110d

.field public static final cs_620_wechat_24:I = 0x7f13110e

.field public static final cs_620_wechat_25:I = 0x7f13110f

.field public static final cs_620_wechat_34:I = 0x7f131110

.field public static final cs_620_wechat_35:I = 0x7f131111

.field public static final cs_620_wechat_36:I = 0x7f131112

.field public static final cs_620_wechat_40:I = 0x7f131113

.field public static final cs_620_wechat_41:I = 0x7f131114

.field public static final cs_621_guide_free_disabled:I = 0x7f131115

.field public static final cs_621_guide_free_enabled:I = 0x7f131116

.field public static final cs_621_guide_trial_30_description:I = 0x7f131117

.field public static final cs_621_idcard_fail_tip01:I = 0x7f131118

.field public static final cs_621_idcard_fail_tip02:I = 0x7f131119

.field public static final cs_621_price_first_discount:I = 0x7f13111a

.field public static final cs_621_saved_to_all_documents:I = 0x7f13111b

.field public static final cs_621_wifi_tips_01:I = 0x7f13111c

.field public static final cs_621_wifi_tips_02:I = 0x7f13111d

.field public static final cs_621_wifi_tips_03:I = 0x7f13111e

.field public static final cs_621_wifi_tips_04:I = 0x7f13111f

.field public static final cs_622_account_google_signin:I = 0x7f131120

.field public static final cs_622_album_01:I = 0x7f131121

.field public static final cs_622_album_02:I = 0x7f131122

.field public static final cs_622_album_03:I = 0x7f131123

.field public static final cs_622_album_04:I = 0x7f131124

.field public static final cs_622_album_05:I = 0x7f131125

.field public static final cs_622_album_06:I = 0x7f131126

.field public static final cs_622_docstructure_01:I = 0x7f131127

.field public static final cs_622_docstructure_02:I = 0x7f131128

.field public static final cs_622_docstructure_03:I = 0x7f131129

.field public static final cs_622_docstructure_04:I = 0x7f13112a

.field public static final cs_622_docstructure_05:I = 0x7f13112b

.field public static final cs_622_docstructure_06:I = 0x7f13112c

.field public static final cs_622_docstructure_08:I = 0x7f13112d

.field public static final cs_622_docstructure_09:I = 0x7f13112e

.field public static final cs_622_folder_export_empty:I = 0x7f13112f

.field public static final cs_622_folder_exportall:I = 0x7f131130

.field public static final cs_622_free_word_06:I = 0x7f131131

.field public static final cs_622_free_word_07:I = 0x7f131132

.field public static final cs_622_free_word_08:I = 0x7f131133

.field public static final cs_622_free_word_09:I = 0x7f131134

.field public static final cs_622_free_word_10:I = 0x7f131135

.field public static final cs_622_free_word_11:I = 0x7f131136

.field public static final cs_622_free_word_12:I = 0x7f131137

.field public static final cs_622_free_word_13:I = 0x7f131138

.field public static final cs_622_free_word_14:I = 0x7f131139

.field public static final cs_622_free_word_17:I = 0x7f13113a

.field public static final cs_622_linkswitch_1:I = 0x7f13113b

.field public static final cs_622_original_mage_missing_01:I = 0x7f13113c

.field public static final cs_622_original_mage_missing_02:I = 0x7f13113d

.field public static final cs_622_reputation_1:I = 0x7f13113e

.field public static final cs_622_reputation_2:I = 0x7f13113f

.field public static final cs_623_certificate_01:I = 0x7f131140

.field public static final cs_623_certificate_02:I = 0x7f131141

.field public static final cs_623_certificate_03:I = 0x7f131142

.field public static final cs_623_certificate_04:I = 0x7f131143

.field public static final cs_623_certificate_05:I = 0x7f131144

.field public static final cs_623_certificate_06:I = 0x7f131145

.field public static final cs_623_certificate_07:I = 0x7f131146

.field public static final cs_623_cspdf_download_btn:I = 0x7f131147

.field public static final cs_623_cspdf_icon:I = 0x7f131148

.field public static final cs_623_cspdf_import_dialogue_intro:I = 0x7f131149

.field public static final cs_623_cspdf_import_dialogue_title:I = 0x7f13114a

.field public static final cs_623_cspdf_import_top_intro:I = 0x7f13114b

.field public static final cs_623_cspdf_import_top_intro2:I = 0x7f13114c

.field public static final cs_623_cspdf_landingpage_cs:I = 0x7f13114d

.field public static final cs_623_cspdf_landingpage_pdf:I = 0x7f13114e

.field public static final cs_623_cspdf_pdftools_intro:I = 0x7f13114f

.field public static final cs_623_experience_ocr_for_free:I = 0x7f131150

.field public static final cs_623_feature_sheet:I = 0x7f131151

.field public static final cs_623_idpremiumpage_1:I = 0x7f131152

.field public static final cs_623_idpremiumpage_2:I = 0x7f131153

.field public static final cs_623_original_drawing_of_certificate:I = 0x7f131154

.field public static final cs_623_print_tip:I = 0x7f131155

.field public static final cs_623_privacy_desc01:I = 0x7f131156

.field public static final cs_623_privacy_desc02:I = 0x7f131157

.field public static final cs_623_privacy_desc03:I = 0x7f131158

.field public static final cs_623_privacy_desc04:I = 0x7f131159

.field public static final cs_623_recall_001:I = 0x7f13115a

.field public static final cs_623_recognize_01:I = 0x7f13115b

.field public static final cs_623_recognize_02:I = 0x7f13115c

.field public static final cs_623_recognize_03:I = 0x7f13115d

.field public static final cs_623_recognize_edge:I = 0x7f13115e

.field public static final cs_623_repeat_01:I = 0x7f13115f

.field public static final cs_623_repeat_02:I = 0x7f131160

.field public static final cs_623_repeat_03:I = 0x7f131161

.field public static final cs_623_repeat_04:I = 0x7f131162

.field public static final cs_623_repeat_05:I = 0x7f131163

.field public static final cs_623_repeat_06:I = 0x7f131164

.field public static final cs_623_repeat_07:I = 0x7f131165

.field public static final cs_623_repeat_08:I = 0x7f131166

.field public static final cs_623_repeat_09:I = 0x7f131167

.field public static final cs_623_repeat_10:I = 0x7f131168

.field public static final cs_623_repeat_11:I = 0x7f131169

.field public static final cs_623_repeat_12:I = 0x7f13116a

.field public static final cs_623_repeat_13:I = 0x7f13116b

.field public static final cs_623_repeat_14:I = 0x7f13116c

.field public static final cs_623_repeat_15:I = 0x7f13116d

.field public static final cs_623_upgrade:I = 0x7f13116e

.field public static final cs_623_upgrade_02:I = 0x7f13116f

.field public static final cs_623_vip_001:I = 0x7f131170

.field public static final cs_623_vip_002:I = 0x7f131171

.field public static final cs_623_vip_003:I = 0x7f131172

.field public static final cs_623_vip_004:I = 0x7f131173

.field public static final cs_623_word_01:I = 0x7f131174

.field public static final cs_624_add_phone_num:I = 0x7f131175

.field public static final cs_624_bind_phone:I = 0x7f131176

.field public static final cs_624_camscanner_privacy_policy:I = 0x7f131177

.field public static final cs_624_huawei_appgallery_link:I = 0x7f131178

.field public static final cs_624_new_scan_btn:I = 0x7f131179

.field public static final cs_624_new_scan_buy_desc:I = 0x7f13117a

.field public static final cs_624_new_scan_complete:I = 0x7f13117b

.field public static final cs_624_new_scan_feature01:I = 0x7f13117c

.field public static final cs_624_new_scan_feature02:I = 0x7f13117d

.field public static final cs_624_new_scan_feature03:I = 0x7f13117e

.field public static final cs_624_new_scan_feature04:I = 0x7f13117f

.field public static final cs_624_new_scan_feature05:I = 0x7f131180

.field public static final cs_624_new_scan_feature06:I = 0x7f131181

.field public static final cs_624_new_scan_tip:I = 0x7f131182

.field public static final cs_624_new_scan_vip:I = 0x7f131183

.field public static final cs_624_no_more_cloud_ocr_try:I = 0x7f131184

.field public static final cs_624_pay_erro:I = 0x7f131185

.field public static final cs_624_phone_bind_tip:I = 0x7f131186

.field public static final cs_624_phone_binding_title:I = 0x7f131187

.field public static final cs_624_phone_registered:I = 0x7f131188

.field public static final cs_624_style_01:I = 0x7f131189

.field public static final cs_624_style_02:I = 0x7f13118a

.field public static final cs_624_style_03:I = 0x7f13118b

.field public static final cs_624_style_04:I = 0x7f13118c

.field public static final cs_624_style_05:I = 0x7f13118d

.field public static final cs_624_style_06:I = 0x7f13118e

.field public static final cs_624_style_07:I = 0x7f13118f

.field public static final cs_624_weixin_bind:I = 0x7f131190

.field public static final cs_625_authorization_information01:I = 0x7f131191

.field public static final cs_625_authorization_management01:I = 0x7f131192

.field public static final cs_625_authorization_management02:I = 0x7f131193

.field public static final cs_625_authorization_management03:I = 0x7f131194

.field public static final cs_625_authorization_management04:I = 0x7f131195

.field public static final cs_625_authorization_management05:I = 0x7f131196

.field public static final cs_625_authorization_management06:I = 0x7f131197

.field public static final cs_625_authorization_management07:I = 0x7f131198

.field public static final cs_625_authorization_management08:I = 0x7f131199

.field public static final cs_625_authorization_management09:I = 0x7f13119a

.field public static final cs_625_authorization_tip:I = 0x7f13119b

.field public static final cs_625_export_as_word_text_only:I = 0x7f13119c

.field public static final cs_625_extracted:I = 0x7f13119d

.field public static final cs_625_id_free_sign:I = 0x7f13119e

.field public static final cs_625_new_vip_button1:I = 0x7f13119f

.field public static final cs_625_new_vip_button2:I = 0x7f1311a0

.field public static final cs_625_new_vip_button3:I = 0x7f1311a1

.field public static final cs_625_new_vip_button4:I = 0x7f1311a2

.field public static final cs_625_new_vip_button5:I = 0x7f1311a3

.field public static final cs_625_new_vip_button6:I = 0x7f1311a4

.field public static final cs_625_new_vip_button7:I = 0x7f1311a5

.field public static final cs_625_new_vip_button8:I = 0x7f1311a6

.field public static final cs_625_new_vip_gift1:I = 0x7f1311a7

.field public static final cs_625_new_vip_gift2:I = 0x7f1311a8

.field public static final cs_625_new_vip_gift3:I = 0x7f1311a9

.field public static final cs_625_new_vip_pop1_txt1:I = 0x7f1311aa

.field public static final cs_625_new_vip_pop1_txt2:I = 0x7f1311ab

.field public static final cs_625_new_vip_pop2_txt1:I = 0x7f1311ac

.field public static final cs_625_new_vip_pop2_txt2:I = 0x7f1311ad

.field public static final cs_625_new_vip_pop3_txt1:I = 0x7f1311ae

.field public static final cs_625_new_vip_pop3_txt2:I = 0x7f1311af

.field public static final cs_625_new_vip_pop4_title:I = 0x7f1311b0

.field public static final cs_625_new_vip_subtitle1:I = 0x7f1311b1

.field public static final cs_625_new_vip_subtitle2:I = 0x7f1311b2

.field public static final cs_625_new_vip_tip:I = 0x7f1311b3

.field public static final cs_625_new_vip_title1:I = 0x7f1311b4

.field public static final cs_625_new_vip_title2:I = 0x7f1311b5

.field public static final cs_625_new_vip_toast:I = 0x7f1311b6

.field public static final cs_625_new_vip_txt1:I = 0x7f1311b7

.field public static final cs_625_privacy_agree_first:I = 0x7f1311b8

.field public static final cs_625_readexp_01:I = 0x7f1311b9

.field public static final cs_625_readexp_02:I = 0x7f1311ba

.field public static final cs_625_readexp_03:I = 0x7f1311bb

.field public static final cs_625_readexp_04:I = 0x7f1311bc

.field public static final cs_625_readexp_05:I = 0x7f1311bd

.field public static final cs_625_readexp_06:I = 0x7f1311be

.field public static final cs_625_readexp_07:I = 0x7f1311bf

.field public static final cs_625_tag_01:I = 0x7f1311c0

.field public static final cs_626_bluetooth_content:I = 0x7f1311c1

.field public static final cs_626_bluetooth_pop:I = 0x7f1311c2

.field public static final cs_626_bluetooth_pop_12later:I = 0x7f1311c3

.field public static final cs_626_bluetooth_title:I = 0x7f1311c4

.field public static final cs_626_cancel_anyitme:I = 0x7f1311c5

.field public static final cs_626_complelte:I = 0x7f1311c6

.field public static final cs_626_congratulation_premium:I = 0x7f1311c7

.field public static final cs_626_congratulations:I = 0x7f1311c8

.field public static final cs_626_give_up:I = 0x7f1311c9

.field public static final cs_626_link_share:I = 0x7f1311ca

.field public static final cs_626_nearbydevice_content:I = 0x7f1311cb

.field public static final cs_626_nearbydevice_title:I = 0x7f1311cc

.field public static final cs_626_newcomer_gift:I = 0x7f1311cd

.field public static final cs_626_newcomer_gift_content:I = 0x7f1311ce

.field public static final cs_626_newcomer_gift_gold:I = 0x7f1311cf

.field public static final cs_626_premium:I = 0x7f1311d0

.field public static final cs_626_printer_01:I = 0x7f1311d1

.field public static final cs_626_printer_02:I = 0x7f1311d2

.field public static final cs_626_printer_03:I = 0x7f1311d3

.field public static final cs_626_printer_04:I = 0x7f1311d4

.field public static final cs_626_radar_select_file_num:I = 0x7f1311d5

.field public static final cs_626_recurring_billing:I = 0x7f1311d6

.field public static final cs_626_refund_button1:I = 0x7f1311d7

.field public static final cs_626_refund_button2:I = 0x7f1311d8

.field public static final cs_626_refund_button3:I = 0x7f1311d9

.field public static final cs_626_refund_button4:I = 0x7f1311da

.field public static final cs_626_refund_condition1:I = 0x7f1311db

.field public static final cs_626_refund_condition2:I = 0x7f1311dc

.field public static final cs_626_refund_condition2_1:I = 0x7f1311dd

.field public static final cs_626_refund_condition3:I = 0x7f1311de

.field public static final cs_626_refund_condition4:I = 0x7f1311df

.field public static final cs_626_refund_condition4_1:I = 0x7f1311e0

.field public static final cs_626_refund_condition5:I = 0x7f1311e1

.field public static final cs_626_refund_condition6:I = 0x7f1311e2

.field public static final cs_626_refund_pop:I = 0x7f1311e3

.field public static final cs_626_refund_tips1:I = 0x7f1311e4

.field public static final cs_626_refund_tips2:I = 0x7f1311e5

.field public static final cs_626_refund_tips3:I = 0x7f1311e6

.field public static final cs_626_refund_tips4:I = 0x7f1311e7

.field public static final cs_626_refund_tips5:I = 0x7f1311e8

.field public static final cs_626_refund_tips6:I = 0x7f1311e9

.field public static final cs_626_refund_tips6_1:I = 0x7f1311ea

.field public static final cs_626_refund_title1:I = 0x7f1311eb

.field public static final cs_626_refund_title2:I = 0x7f1311ec

.field public static final cs_626_refund_title3:I = 0x7f1311ed

.field public static final cs_626_refund_title4:I = 0x7f1311ee

.field public static final cs_626_refund_title5:I = 0x7f1311ef

.field public static final cs_626_refund_title6:I = 0x7f1311f0

.field public static final cs_626_refund_title6_1:I = 0x7f1311f1

.field public static final cs_626_refund_toast1:I = 0x7f1311f2

.field public static final cs_626_refund_toast2:I = 0x7f1311f3

.field public static final cs_626_refund_txt:I = 0x7f1311f4

.field public static final cs_626_refund_vip_1year:I = 0x7f1311f5

.field public static final cs_626_refund_vip_moon:I = 0x7f1311f6

.field public static final cs_626_refund_vip_season:I = 0x7f1311f7

.field public static final cs_626_refund_vip_year:I = 0x7f1311f8

.field public static final cs_626_shot_01:I = 0x7f1311f9

.field public static final cs_626_shot_02:I = 0x7f1311fa

.field public static final cs_626_shot_03:I = 0x7f1311fb

.field public static final cs_626_shot_04:I = 0x7f1311fc

.field public static final cs_626_shot_05:I = 0x7f1311fd

.field public static final cs_626_shot_06:I = 0x7f1311fe

.field public static final cs_626_shot_07:I = 0x7f1311ff

.field public static final cs_626_shot_08:I = 0x7f131200

.field public static final cs_626_shot_09:I = 0x7f131201

.field public static final cs_626_shot_10:I = 0x7f131202

.field public static final cs_626_shot_11:I = 0x7f131203

.field public static final cs_626_shot_12:I = 0x7f131204

.field public static final cs_626_shot_13:I = 0x7f131205

.field public static final cs_626_shot_14:I = 0x7f131206

.field public static final cs_626_shot_15:I = 0x7f131207

.field public static final cs_626_shot_16:I = 0x7f131208

.field public static final cs_626_shot_18:I = 0x7f131209

.field public static final cs_626_shot_19:I = 0x7f13120a

.field public static final cs_626_start_trial:I = 0x7f13120b

.field public static final cs_626_toexcel_mid_cam:I = 0x7f13120c

.field public static final cs_626_view_all_features:I = 0x7f13120d

.field public static final cs_627_add_02:I = 0x7f13120e

.field public static final cs_627_add_03:I = 0x7f13120f

.field public static final cs_627_add_id_doc:I = 0x7f131210

.field public static final cs_627_add_id_pohoto:I = 0x7f131211

.field public static final cs_627_back_confirm:I = 0x7f131212

.field public static final cs_627_delete_id:I = 0x7f131213

.field public static final cs_627_edit_idinfo:I = 0x7f131214

.field public static final cs_627_edit_tip:I = 0x7f131215

.field public static final cs_627_enterprise_01:I = 0x7f131216

.field public static final cs_627_enterprise_02:I = 0x7f131217

.field public static final cs_627_enterprise_03:I = 0x7f131218

.field public static final cs_627_enterprise_04:I = 0x7f131219

.field public static final cs_627_google_01:I = 0x7f13121b

.field public static final cs_627_limit_01:I = 0x7f13121c

.field public static final cs_627_limit_02:I = 0x7f13121d

.field public static final cs_627_limit_03:I = 0x7f13121e

.field public static final cs_627_logo:I = 0x7f13121f

.field public static final cs_627_logout:I = 0x7f131220

.field public static final cs_627_name:I = 0x7f131221

.field public static final cs_627_noad_01:I = 0x7f131222

.field public static final cs_627_noad_02:I = 0x7f131223

.field public static final cs_627_noad_03:I = 0x7f131224

.field public static final cs_627_noad_04:I = 0x7f131225

.field public static final cs_627_noad_07:I = 0x7f131226

.field public static final cs_627_qifengzhang_01:I = 0x7f131227

.field public static final cs_627_qifengzhang_02:I = 0x7f131228

.field public static final cs_627_qifengzhang_limit:I = 0x7f131229

.field public static final cs_627_quit:I = 0x7f13122a

.field public static final cs_627_recognize_fail_pic:I = 0x7f13122b

.field public static final cs_627_recognize_fail_type:I = 0x7f13122c

.field public static final cs_627_recognize_id:I = 0x7f13122d

.field public static final cs_627_share_01:I = 0x7f13122e

.field public static final cs_627_sign_adjust_01:I = 0x7f13122f

.field public static final cs_627_sign_adjust_02:I = 0x7f131230

.field public static final cs_627_sign_limit:I = 0x7f131231

.field public static final cs_627_single_page:I = 0x7f131232

.field public static final cs_627_start_time:I = 0x7f131233

.field public static final cs_627_tips:I = 0x7f131234

.field public static final cs_627_use_all:I = 0x7f131235

.field public static final cs_627_widget_01:I = 0x7f131236

.field public static final cs_627_widget_02:I = 0x7f131237

.field public static final cs_627_widget_03:I = 0x7f131238

.field public static final cs_627_widget_04:I = 0x7f131239

.field public static final cs_627_widget_05:I = 0x7f13123a

.field public static final cs_627_widget_06:I = 0x7f13123b

.field public static final cs_627_widget_07:I = 0x7f13123c

.field public static final cs_627_widget_08:I = 0x7f13123d

.field public static final cs_627_widget_09:I = 0x7f13123e

.field public static final cs_627_widget_10:I = 0x7f13123f

.field public static final cs_627_widget_11:I = 0x7f131240

.field public static final cs_627_widget_12:I = 0x7f131241

.field public static final cs_627_widget_13:I = 0x7f131242

.field public static final cs_627_widget_14:I = 0x7f131243

.field public static final cs_627_widget_15:I = 0x7f131244

.field public static final cs_627_widget_16:I = 0x7f131245

.field public static final cs_627_widget_17:I = 0x7f131246

.field public static final cs_627_widget_18:I = 0x7f131247

.field public static final cs_627_widget_19:I = 0x7f131248

.field public static final cs_627_widget_20:I = 0x7f131249

.field public static final cs_627_widget_21:I = 0x7f13124a

.field public static final cs_627_widget_22:I = 0x7f13124b

.field public static final cs_627_widget_23:I = 0x7f13124c

.field public static final cs_627_widget_24:I = 0x7f13124d

.field public static final cs_627_widget_25:I = 0x7f13124e

.field public static final cs_627_widget_26:I = 0x7f13124f

.field public static final cs_627_widget_27:I = 0x7f131250

.field public static final cs_627_widget_28:I = 0x7f131251

.field public static final cs_627_widget_29:I = 0x7f131252

.field public static final cs_627_widget_30:I = 0x7f131253

.field public static final cs_627_widget_32:I = 0x7f131254

.field public static final cs_627_widget_33:I = 0x7f131255

.field public static final cs_627_widget_34:I = 0x7f131256

.field public static final cs_627_widget_35:I = 0x7f131257

.field public static final cs_627_widget_36:I = 0x7f131258

.field public static final cs_627_widget_37:I = 0x7f131259

.field public static final cs_627_yinzhang:I = 0x7f13125a

.field public static final cs_628_add_success:I = 0x7f13125b

.field public static final cs_628_additional_vip_package_01:I = 0x7f13125c

.field public static final cs_628_additional_vip_package_02:I = 0x7f13125d

.field public static final cs_628_additional_vip_package_03:I = 0x7f13125e

.field public static final cs_628_additional_vip_package_04:I = 0x7f13125f

.field public static final cs_628_additional_vip_package_05:I = 0x7f131260

.field public static final cs_628_additional_vip_package_06:I = 0x7f131261

.field public static final cs_628_additional_vip_package_07:I = 0x7f131262

.field public static final cs_628_additional_vip_package_08:I = 0x7f131263

.field public static final cs_628_additional_vip_package_09:I = 0x7f131264

.field public static final cs_628_additional_vip_package_10:I = 0x7f131265

.field public static final cs_628_additional_vip_package_11:I = 0x7f131266

.field public static final cs_628_additional_vip_package_12:I = 0x7f131267

.field public static final cs_628_additional_vip_package_13:I = 0x7f131268

.field public static final cs_628_additional_vip_package_14:I = 0x7f131269

.field public static final cs_628_additional_vip_package_15:I = 0x7f13126a

.field public static final cs_628_additional_vip_package_16:I = 0x7f13126b

.field public static final cs_628_algorithm01:I = 0x7f13126c

.field public static final cs_628_algorithm02:I = 0x7f13126d

.field public static final cs_628_algorithm03:I = 0x7f13126e

.field public static final cs_628_algorithm04:I = 0x7f13126f

.field public static final cs_628_algorithm05:I = 0x7f131270

.field public static final cs_628_apply_content02:I = 0x7f131271

.field public static final cs_628_apply_title:I = 0x7f131272

.field public static final cs_628_batch_scan_settings:I = 0x7f131273

.field public static final cs_628_card_info_tip:I = 0x7f131274

.field public static final cs_628_card_info_title:I = 0x7f131275

.field public static final cs_628_card_name_tip:I = 0x7f131276

.field public static final cs_628_card_no_tip:I = 0x7f131277

.field public static final cs_628_card_othet_tip:I = 0x7f131278

.field public static final cs_628_card_type:I = 0x7f131279

.field public static final cs_628_card_type_tip:I = 0x7f13127a

.field public static final cs_628_certification_tip:I = 0x7f13127b

.field public static final cs_628_change_language01:I = 0x7f13127c

.field public static final cs_628_change_language02:I = 0x7f13127d

.field public static final cs_628_fill_in_later:I = 0x7f13127e

.field public static final cs_628_fill_in_now:I = 0x7f13127f

.field public static final cs_628_fill_in_tips:I = 0x7f131280

.field public static final cs_628_filter_limt:I = 0x7f131281

.field public static final cs_628_free_qrcode:I = 0x7f131282

.field public static final cs_628_howtouse:I = 0x7f131283

.field public static final cs_628_idcardfolder_content02:I = 0x7f131284

.field public static final cs_628_magic:I = 0x7f131285

.field public static final cs_628_magic_02:I = 0x7f131286

.field public static final cs_628_main_qrcode:I = 0x7f131287

.field public static final cs_628_new_vip_button1:I = 0x7f131288

.field public static final cs_628_new_vip_button2:I = 0x7f131289

.field public static final cs_628_new_vip_gift1:I = 0x7f13128a

.field public static final cs_628_new_vip_toast1:I = 0x7f13128b

.field public static final cs_628_new_vip_toast2:I = 0x7f13128c

.field public static final cs_628_new_vip_txt1:I = 0x7f13128d

.field public static final cs_628_new_vip_txt2:I = 0x7f13128e

.field public static final cs_628_new_vip_txt3:I = 0x7f13128f

.field public static final cs_628_new_vip_txt4:I = 0x7f131290

.field public static final cs_628_new_vip_txt5:I = 0x7f131291

.field public static final cs_628_new_vip_txt6:I = 0x7f131292

.field public static final cs_628_new_vip_txt7:I = 0x7f131293

.field public static final cs_628_new_vip_txt8:I = 0x7f131294

.field public static final cs_628_new_vip_txt9:I = 0x7f131295

.field public static final cs_628_other_cards:I = 0x7f131296

.field public static final cs_628_please_fill_in:I = 0x7f131297

.field public static final cs_628_qrcode_doc_webtips:I = 0x7f131298

.field public static final cs_628_qrcode_guide_01:I = 0x7f131299

.field public static final cs_628_qrcode_guide_02:I = 0x7f13129a

.field public static final cs_628_qrcode_guide_03:I = 0x7f13129b

.field public static final cs_628_qrcode_limit:I = 0x7f13129c

.field public static final cs_628_qrcode_limit_02:I = 0x7f13129d

.field public static final cs_628_qrcode_safe:I = 0x7f13129e

.field public static final cs_628_qrcode_share:I = 0x7f13129f

.field public static final cs_628_qrcode_shixiao:I = 0x7f1312a0

.field public static final cs_628_qrcode_tips:I = 0x7f1312a1

.field public static final cs_628_recognize_fail:I = 0x7f1312a2

.field public static final cs_628_sever_timeout:I = 0x7f1312a3

.field public static final cs_628_sever_wrong:I = 0x7f1312a4

.field public static final cs_628_share_to_qrcode:I = 0x7f1312a5

.field public static final cs_628_share_via_qrcode:I = 0x7f1312a6

.field public static final cs_628_single_scan_settings:I = 0x7f1312a7

.field public static final cs_628_super_filter:I = 0x7f1312a8

.field public static final cs_628_super_filter_tips:I = 0x7f1312a9

.field public static final cs_628_super_filter_tips2:I = 0x7f1312aa

.field public static final cs_628_tag_01:I = 0x7f1312ab

.field public static final cs_628_tag_02:I = 0x7f1312ac

.field public static final cs_628_tag_03:I = 0x7f1312ad

.field public static final cs_628_tag_04:I = 0x7f1312ae

.field public static final cs_628_tag_05:I = 0x7f1312af

.field public static final cs_628_tag_06:I = 0x7f1312b0

.field public static final cs_628_tag_07:I = 0x7f1312b1

.field public static final cs_628_tag_08:I = 0x7f1312b2

.field public static final cs_628_tag_09:I = 0x7f1312b3

.field public static final cs_628_tag_10:I = 0x7f1312b4

.field public static final cs_628_tag_11:I = 0x7f1312b5

.field public static final cs_628_tag_12:I = 0x7f1312b6

.field public static final cs_628_tag_13:I = 0x7f1312b7

.field public static final cs_628_tag_14:I = 0x7f1312b8

.field public static final cs_628_tag_15:I = 0x7f1312b9

.field public static final cs_628_tag_16:I = 0x7f1312ba

.field public static final cs_628_tag_17:I = 0x7f1312bb

.field public static final cs_628_tag_18:I = 0x7f1312bc

.field public static final cs_628_tag_19:I = 0x7f1312bd

.field public static final cs_628_tag_20:I = 0x7f1312be

.field public static final cs_628_tag_21:I = 0x7f1312bf

.field public static final cs_628_tag_22:I = 0x7f1312c0

.field public static final cs_628_tag_23:I = 0x7f1312c1

.field public static final cs_628_tag_24:I = 0x7f1312c2

.field public static final cs_628_tag_25:I = 0x7f1312c3

.field public static final cs_628_tag_26:I = 0x7f1312c4

.field public static final cs_628_tag_27:I = 0x7f1312c5

.field public static final cs_628_tag_28:I = 0x7f1312c6

.field public static final cs_628_timefolder_content02:I = 0x7f1312c7

.field public static final cs_629_activegift_01:I = 0x7f1312c8

.field public static final cs_629_activegift_02:I = 0x7f1312c9

.field public static final cs_629_activegift_02_premium:I = 0x7f1312ca

.field public static final cs_629_activegift_03:I = 0x7f1312cb

.field public static final cs_629_activegift_04:I = 0x7f1312cc

.field public static final cs_629_activegift_25:I = 0x7f1312cd

.field public static final cs_629_activegift_25_premium:I = 0x7f1312ce

.field public static final cs_629_activegift_26:I = 0x7f1312cf

.field public static final cs_629_activegift_27:I = 0x7f1312d0

.field public static final cs_629_activegift_28:I = 0x7f1312d1

.field public static final cs_629_activegift_29:I = 0x7f1312d2

.field public static final cs_629_activegift_30:I = 0x7f1312d3

.field public static final cs_629_activegift_31:I = 0x7f1312d4

.field public static final cs_629_activegift_32:I = 0x7f1312d5

.field public static final cs_629_activegift_33:I = 0x7f1312d6

.field public static final cs_629_erase_01:I = 0x7f1312d7

.field public static final cs_629_erase_02:I = 0x7f1312d8

.field public static final cs_629_erase_03:I = 0x7f1312d9

.field public static final cs_629_erase_04:I = 0x7f1312da

.field public static final cs_629_erase_05:I = 0x7f1312db

.field public static final cs_629_erase_06:I = 0x7f1312dc

.field public static final cs_629_erase_07:I = 0x7f1312dd

.field public static final cs_629_erase_08:I = 0x7f1312de

.field public static final cs_629_erase_09:I = 0x7f1312df

.field public static final cs_629_erase_10:I = 0x7f1312e0

.field public static final cs_629_erase_11:I = 0x7f1312e1

.field public static final cs_629_erase_12:I = 0x7f1312e2

.field public static final cs_629_erase_13:I = 0x7f1312e3

.field public static final cs_629_erase_14:I = 0x7f1312e4

.field public static final cs_629_erase_15:I = 0x7f1312e5

.field public static final cs_629_erase_16:I = 0x7f1312e6

.field public static final cs_629_erase_17:I = 0x7f1312e7

.field public static final cs_629_erase_18:I = 0x7f1312e8

.field public static final cs_629_erase_19:I = 0x7f1312e9

.field public static final cs_629_erase_20:I = 0x7f1312ea

.field public static final cs_629_erase_21:I = 0x7f1312eb

.field public static final cs_629_erase_22:I = 0x7f1312ec

.field public static final cs_629_erase_23:I = 0x7f1312ed

.field public static final cs_629_noad_01:I = 0x7f1312ee

.field public static final cs_629_noad_02:I = 0x7f1312ef

.field public static final cs_629_noad_03:I = 0x7f1312f0

.field public static final cs_629_noad_04:I = 0x7f1312f1

.field public static final cs_629_nosave:I = 0x7f1312f2

.field public static final cs_629_passivity_50:I = 0x7f1312f3

.field public static final cs_629_passivity_51:I = 0x7f1312f4

.field public static final cs_629_passivity_52:I = 0x7f1312f5

.field public static final cs_629_reward_01:I = 0x7f1312f6

.field public static final cs_629_reward_02:I = 0x7f1312f7

.field public static final cs_629_reward_03:I = 0x7f1312f8

.field public static final cs_629_reward_04:I = 0x7f1312f9

.field public static final cs_629_reward_05:I = 0x7f1312fa

.field public static final cs_629_reward_06:I = 0x7f1312fb

.field public static final cs_629_reward_07:I = 0x7f1312fc

.field public static final cs_629_reward_08:I = 0x7f1312fd

.field public static final cs_629_reward_09:I = 0x7f1312fe

.field public static final cs_629_save:I = 0x7f1312ff

.field public static final cs_629_save_album:I = 0x7f131300

.field public static final cs_629_scandone_02:I = 0x7f131301

.field public static final cs_629_selected_function:I = 0x7f131302

.field public static final cs_629_share:I = 0x7f131303

.field public static final cs_629_shared_folder_01:I = 0x7f131304

.field public static final cs_629_shared_folder_02:I = 0x7f131305

.field public static final cs_629_shared_folder_03:I = 0x7f131306

.field public static final cs_629_shared_folder_04:I = 0x7f131307

.field public static final cs_629_shared_folder_05:I = 0x7f131308

.field public static final cs_629_shared_folder_06:I = 0x7f131309

.field public static final cs_629_shared_folder_07:I = 0x7f13130a

.field public static final cs_629_shared_folder_08:I = 0x7f13130b

.field public static final cs_629_shared_folder_09:I = 0x7f13130c

.field public static final cs_629_shared_folder_10:I = 0x7f13130d

.field public static final cs_629_shared_folder_11:I = 0x7f13130e

.field public static final cs_629_shared_folder_12:I = 0x7f13130f

.field public static final cs_629_shared_folder_13:I = 0x7f131310

.field public static final cs_629_shared_folder_14:I = 0x7f131311

.field public static final cs_629_shared_folder_15:I = 0x7f131312

.field public static final cs_629_shared_folder_16:I = 0x7f131313

.field public static final cs_629_shared_folder_17:I = 0x7f131314

.field public static final cs_629_shared_folder_18:I = 0x7f131315

.field public static final cs_629_tablist_1:I = 0x7f131316

.field public static final cs_629_tablist_2:I = 0x7f131317

.field public static final cs_629_tablist_3:I = 0x7f131318

.field public static final cs_629_wechat_logout_01:I = 0x7f131319

.field public static final cs_630_associated_label:I = 0x7f13131a

.field public static final cs_630_barcode_01:I = 0x7f13131b

.field public static final cs_630_barcode_02:I = 0x7f13131c

.field public static final cs_630_barcode_03:I = 0x7f13131d

.field public static final cs_630_barcode_04:I = 0x7f13131e

.field public static final cs_630_barcode_05:I = 0x7f13131f

.field public static final cs_630_barcode_06:I = 0x7f131320

.field public static final cs_630_barcode_07:I = 0x7f131321

.field public static final cs_630_barcode_08:I = 0x7f131322

.field public static final cs_630_barcode_09:I = 0x7f131323

.field public static final cs_630_barcode_10:I = 0x7f131324

.field public static final cs_630_barcode_11:I = 0x7f131325

.field public static final cs_630_barcode_12:I = 0x7f131326

.field public static final cs_630_barcode_13:I = 0x7f131327

.field public static final cs_630_barcode_14:I = 0x7f131328

.field public static final cs_630_barcode_15:I = 0x7f131329

.field public static final cs_630_barcode_16:I = 0x7f13132a

.field public static final cs_630_barcode_17:I = 0x7f13132b

.field public static final cs_630_barcode_18:I = 0x7f13132c

.field public static final cs_630_barcode_19:I = 0x7f13132d

.field public static final cs_630_barcode_20:I = 0x7f13132e

.field public static final cs_630_barcode_21:I = 0x7f13132f

.field public static final cs_630_barcode_22:I = 0x7f131330

.field public static final cs_630_cancel_convert:I = 0x7f131331

.field public static final cs_630_capacity_expansion:I = 0x7f131332

.field public static final cs_630_confirm_cance_conversion:I = 0x7f131333

.field public static final cs_630_converted_save:I = 0x7f131334

.field public static final cs_630_converting:I = 0x7f131335

.field public static final cs_630_convertion_suceed:I = 0x7f131336

.field public static final cs_630_copy_suceed:I = 0x7f131337

.field public static final cs_630_custom:I = 0x7f131338

.field public static final cs_630_custom_title:I = 0x7f131339

.field public static final cs_630_desktop_shortcut_confirmation:I = 0x7f13133a

.field public static final cs_630_detailed_information:I = 0x7f13133b

.field public static final cs_630_document_classification:I = 0x7f13133c

.field public static final cs_630_document_type:I = 0x7f13133d

.field public static final cs_630_extract_desc:I = 0x7f13133e

.field public static final cs_630_filter:I = 0x7f13133f

.field public static final cs_630_filter_doc:I = 0x7f131340

.field public static final cs_630_first:I = 0x7f131341

.field public static final cs_630_free_code_scan_02:I = 0x7f131342

.field public static final cs_630_func_toast:I = 0x7f131343

.field public static final cs_630_history_01:I = 0x7f131344

.field public static final cs_630_history_02:I = 0x7f131345

.field public static final cs_630_history_03:I = 0x7f131346

.field public static final cs_630_history_04:I = 0x7f131347

.field public static final cs_630_history_05:I = 0x7f131348

.field public static final cs_630_home_func_title:I = 0x7f131349

.field public static final cs_630_idcard_desc:I = 0x7f13134a

.field public static final cs_630_idphoto_desc:I = 0x7f13134b

.field public static final cs_630_import_doc_desc:I = 0x7f13134c

.field public static final cs_630_import_first_doc:I = 0x7f13134d

.field public static final cs_630_import_pic_desc:I = 0x7f13134e

.field public static final cs_630_import_succeded:I = 0x7f13134f

.field public static final cs_630_importing:I = 0x7f131350

.field public static final cs_630_incentive_01:I = 0x7f131351

.field public static final cs_630_incentive_02:I = 0x7f131352

.field public static final cs_630_incentive_03:I = 0x7f131353

.field public static final cs_630_incentive_04:I = 0x7f131354

.field public static final cs_630_incentive_05:I = 0x7f131355

.field public static final cs_630_incentive_07:I = 0x7f131356

.field public static final cs_630_incentive_08:I = 0x7f131357

.field public static final cs_630_incentive_09:I = 0x7f131358

.field public static final cs_630_incentive_10:I = 0x7f131359

.field public static final cs_630_incentive_11:I = 0x7f13135a

.field public static final cs_630_incentive_12:I = 0x7f13135b

.field public static final cs_630_incentive_13:I = 0x7f13135c

.field public static final cs_630_keyword_search:I = 0x7f13135d

.field public static final cs_630_last:I = 0x7f13135e

.field public static final cs_630_lock_desc:I = 0x7f13135f

.field public static final cs_630_manage:I = 0x7f131360

.field public static final cs_630_merge_desc:I = 0x7f131361

.field public static final cs_630_new_function:I = 0x7f131362

.field public static final cs_630_no_doc_01:I = 0x7f131363

.field public static final cs_630_no_doc_02:I = 0x7f131364

.field public static final cs_630_no_matches_searched:I = 0x7f131365

.field public static final cs_630_no_support_file:I = 0x7f131366

.field public static final cs_630_no_support_import_01:I = 0x7f131367

.field public static final cs_630_no_support_import_02:I = 0x7f131368

.field public static final cs_630_ocr_desc:I = 0x7f131369

.field public static final cs_630_open_doc:I = 0x7f13136a

.field public static final cs_630_paper_desc:I = 0x7f13136b

.field public static final cs_630_pass_conditional01:I = 0x7f13136c

.field public static final cs_630_pass_conditional02:I = 0x7f13136d

.field public static final cs_630_pass_conditional03:I = 0x7f13136e

.field public static final cs_630_photo_desc:I = 0x7f13136f

.field public static final cs_630_play:I = 0x7f131370

.field public static final cs_630_quit_play:I = 0x7f131371

.field public static final cs_630_reorder_desc:I = 0x7f131372

.field public static final cs_630_route:I = 0x7f131373

.field public static final cs_630_save_as_pdf:I = 0x7f131374

.field public static final cs_630_scan_desc:I = 0x7f131375

.field public static final cs_630_scan_new_documents:I = 0x7f131376

.field public static final cs_630_scanppt_desc:I = 0x7f131377

.field public static final cs_630_searc_in_web:I = 0x7f131378

.field public static final cs_630_search:I = 0x7f131379

.field public static final cs_630_shop_01:I = 0x7f13137a

.field public static final cs_630_shop_02:I = 0x7f13137b

.field public static final cs_630_sign_desc:I = 0x7f13137c

.field public static final cs_630_sign_in:I = 0x7f13137d

.field public static final cs_630_sign_signed:I = 0x7f13137e

.field public static final cs_630_support_import:I = 0x7f13137f

.field public static final cs_630_swich_page:I = 0x7f131380

.field public static final cs_630_synchronize:I = 0x7f131381

.field public static final cs_630_to_import:I = 0x7f131382

.field public static final cs_630_toexcel_desc:I = 0x7f131383

.field public static final cs_630_tolongpic_desc:I = 0x7f131384

.field public static final cs_630_topic_desc:I = 0x7f131385

.field public static final cs_630_toppt_desc:I = 0x7f131386

.field public static final cs_630_toword_desc:I = 0x7f131387

.field public static final cs_630_upgrade_cloud_space:I = 0x7f131388

.field public static final cs_630_watermark_desc:I = 0x7f131389

.field public static final cs_630_wiget_01:I = 0x7f13138a

.field public static final cs_631_cloud:I = 0x7f13138b

.field public static final cs_631_cspdf_intro:I = 0x7f13138c

.field public static final cs_631_cspdf_not:I = 0x7f13138d

.field public static final cs_631_cspdf_open:I = 0x7f13138e

.field public static final cs_631_cspdf_powerby:I = 0x7f13138f

.field public static final cs_631_facebook:I = 0x7f131390

.field public static final cs_631_file_01:I = 0x7f131391

.field public static final cs_631_file_02:I = 0x7f131392

.field public static final cs_631_file_03:I = 0x7f131393

.field public static final cs_631_file_05:I = 0x7f131394

.field public static final cs_631_file_06:I = 0x7f131395

.field public static final cs_631_file_07:I = 0x7f131396

.field public static final cs_631_file_08:I = 0x7f131397

.field public static final cs_631_file_09:I = 0x7f131398

.field public static final cs_631_file_10:I = 0x7f131399

.field public static final cs_631_file_11:I = 0x7f13139a

.field public static final cs_631_file_12:I = 0x7f13139b

.field public static final cs_631_file_14:I = 0x7f13139c

.field public static final cs_631_file_15:I = 0x7f13139d

.field public static final cs_631_file_16:I = 0x7f13139e

.field public static final cs_631_file_17:I = 0x7f13139f

.field public static final cs_631_file_18:I = 0x7f1313a0

.field public static final cs_631_file_19:I = 0x7f1313a1

.field public static final cs_631_file_20:I = 0x7f1313a2

.field public static final cs_631_file_21:I = 0x7f1313a3

.field public static final cs_631_file_22:I = 0x7f1313a4

.field public static final cs_631_import_01:I = 0x7f1313a5

.field public static final cs_631_import_02:I = 0x7f1313a6

.field public static final cs_631_import_03:I = 0x7f1313a7

.field public static final cs_631_import_04:I = 0x7f1313a8

.field public static final cs_631_import_05:I = 0x7f1313a9

.field public static final cs_631_import_06:I = 0x7f1313aa

.field public static final cs_631_newmore_01:I = 0x7f1313ab

.field public static final cs_631_newmore_02:I = 0x7f1313ac

.field public static final cs_631_newmore_03:I = 0x7f1313ad

.field public static final cs_631_newmore_04:I = 0x7f1313ae

.field public static final cs_631_newmore_05:I = 0x7f1313af

.field public static final cs_631_newmore_06:I = 0x7f1313b0

.field public static final cs_631_newmore_07:I = 0x7f1313b1

.field public static final cs_631_newmore_08:I = 0x7f1313b2

.field public static final cs_631_newmore_09:I = 0x7f1313b3

.field public static final cs_631_newmore_10:I = 0x7f1313b4

.field public static final cs_631_newmore_11:I = 0x7f1313b5

.field public static final cs_631_newmore_12:I = 0x7f1313b6

.field public static final cs_631_newmore_13:I = 0x7f1313b7

.field public static final cs_631_newmore_14:I = 0x7f1313b8

.field public static final cs_631_newmore_15:I = 0x7f1313b9

.field public static final cs_631_newmore_16:I = 0x7f1313ba

.field public static final cs_631_newmore_17:I = 0x7f1313bb

.field public static final cs_631_newmore_18:I = 0x7f1313bc

.field public static final cs_631_newmore_19:I = 0x7f1313bd

.field public static final cs_631_newmore_20:I = 0x7f1313be

.field public static final cs_631_newmore_21:I = 0x7f1313bf

.field public static final cs_631_newmore_22:I = 0x7f1313c0

.field public static final cs_631_newmore_23:I = 0x7f1313c1

.field public static final cs_631_newmore_24:I = 0x7f1313c2

.field public static final cs_631_newmore_25:I = 0x7f1313c3

.field public static final cs_631_newmore_26:I = 0x7f1313c4

.field public static final cs_631_newmore_27:I = 0x7f1313c5

.field public static final cs_631_newmore_28:I = 0x7f1313c6

.field public static final cs_631_newmore_29:I = 0x7f1313c7

.field public static final cs_631_newmore_30:I = 0x7f1313c8

.field public static final cs_631_newmore_31:I = 0x7f1313c9

.field public static final cs_631_newmore_32:I = 0x7f1313ca

.field public static final cs_631_scan_doc:I = 0x7f1313cb

.field public static final cs_631_sign_01:I = 0x7f1313cc

.field public static final cs_631_sign_add_contact:I = 0x7f1313cd

.field public static final cs_631_sign_add_doc_001:I = 0x7f1313ce

.field public static final cs_631_sign_added_contacts:I = 0x7f1313cf

.field public static final cs_631_sign_all:I = 0x7f1313d0

.field public static final cs_631_sign_all_contacts:I = 0x7f1313d1

.field public static final cs_631_sign_all_sign_001:I = 0x7f1313d2

.field public static final cs_631_sign_camera_tips_01:I = 0x7f1313d3

.field public static final cs_631_sign_camera_tips_02:I = 0x7f1313d4

.field public static final cs_631_sign_camera_tips_03:I = 0x7f1313d5

.field public static final cs_631_sign_camera_tips_04:I = 0x7f1313d6

.field public static final cs_631_sign_choosesign:I = 0x7f1313d7

.field public static final cs_631_sign_complete:I = 0x7f1313d8

.field public static final cs_631_sign_consent:I = 0x7f1313d9

.field public static final cs_631_sign_continue:I = 0x7f1313da

.field public static final cs_631_sign_continue_invite:I = 0x7f1313db

.field public static final cs_631_sign_coose:I = 0x7f1313dc

.field public static final cs_631_sign_copylink:I = 0x7f1313dd

.field public static final cs_631_sign_cs_01:I = 0x7f1313de

.field public static final cs_631_sign_cssign:I = 0x7f1313df

.field public static final cs_631_sign_cssign_01:I = 0x7f1313e0

.field public static final cs_631_sign_date:I = 0x7f1313e1

.field public static final cs_631_sign_delete:I = 0x7f1313e2

.field public static final cs_631_sign_delete_01:I = 0x7f1313e3

.field public static final cs_631_sign_detail:I = 0x7f1313e4

.field public static final cs_631_sign_documents:I = 0x7f1313e5

.field public static final cs_631_sign_draft:I = 0x7f1313e6

.field public static final cs_631_sign_draft00:I = 0x7f1313e7

.field public static final cs_631_sign_draft_01:I = 0x7f1313e8

.field public static final cs_631_sign_email:I = 0x7f1313e9

.field public static final cs_631_sign_expired:I = 0x7f1313ea

.field public static final cs_631_sign_expired_01:I = 0x7f1313eb

.field public static final cs_631_sign_gosign:I = 0x7f1313ec

.field public static final cs_631_sign_have_been_sign:I = 0x7f1313ed

.field public static final cs_631_sign_here:I = 0x7f1313ee

.field public static final cs_631_sign_home:I = 0x7f1313ef

.field public static final cs_631_sign_images:I = 0x7f1313f0

.field public static final cs_631_sign_import_document:I = 0x7f1313f1

.field public static final cs_631_sign_initialtime:I = 0x7f1313f2

.field public static final cs_631_sign_initiator:I = 0x7f1313f3

.field public static final cs_631_sign_initiator00:I = 0x7f1313f4

.field public static final cs_631_sign_initiator_01:I = 0x7f1313f5

.field public static final cs_631_sign_invalid_users:I = 0x7f1313f6

.field public static final cs_631_sign_invite_email:I = 0x7f1313f7

.field public static final cs_631_sign_invite_phone:I = 0x7f1313f8

.field public static final cs_631_sign_limit_num:I = 0x7f1313f9

.field public static final cs_631_sign_limit_num_02:I = 0x7f1313fa

.field public static final cs_631_sign_limit_tips:I = 0x7f1313fb

.field public static final cs_631_sign_line_1:I = 0x7f1313fc

.field public static final cs_631_sign_line_2:I = 0x7f1313fd

.field public static final cs_631_sign_line_3:I = 0x7f1313fe

.field public static final cs_631_sign_local:I = 0x7f1313ff

.field public static final cs_631_sign_more:I = 0x7f131400

.field public static final cs_631_sign_moreinfo:I = 0x7f131401

.field public static final cs_631_sign_my:I = 0x7f131402

.field public static final cs_631_sign_my_sign:I = 0x7f131403

.field public static final cs_631_sign_my_sign01:I = 0x7f131404

.field public static final cs_631_sign_my_sign_01:I = 0x7f131405

.field public static final cs_631_sign_myinfo:I = 0x7f131406

.field public static final cs_631_sign_name:I = 0x7f131407

.field public static final cs_631_sign_new_doc:I = 0x7f131408

.field public static final cs_631_sign_nocontacts:I = 0x7f131409

.field public static final cs_631_sign_nosign:I = 0x7f13140a

.field public static final cs_631_sign_notify:I = 0x7f13140b

.field public static final cs_631_sign_now:I = 0x7f13140c

.field public static final cs_631_sign_num_02:I = 0x7f13140d

.field public static final cs_631_sign_number:I = 0x7f13140e

.field public static final cs_631_sign_order:I = 0x7f13140f

.field public static final cs_631_sign_order_02:I = 0x7f131410

.field public static final cs_631_sign_other_sign:I = 0x7f131411

.field public static final cs_631_sign_over_choice:I = 0x7f131412

.field public static final cs_631_sign_panel:I = 0x7f131413

.field public static final cs_631_sign_phone:I = 0x7f131414

.field public static final cs_631_sign_pleasesign:I = 0x7f131415

.field public static final cs_631_sign_refresh:I = 0x7f131416

.field public static final cs_631_sign_refresh_01:I = 0x7f131417

.field public static final cs_631_sign_reminder:I = 0x7f131418

.field public static final cs_631_sign_revolve_01:I = 0x7f131419

.field public static final cs_631_sign_revolve_02:I = 0x7f13141a

.field public static final cs_631_sign_risk_title:I = 0x7f13141b

.field public static final cs_631_sign_scan:I = 0x7f13141c

.field public static final cs_631_sign_seal:I = 0x7f13141d

.field public static final cs_631_sign_selected:I = 0x7f13141e

.field public static final cs_631_sign_self:I = 0x7f13141f

.field public static final cs_631_sign_send_over:I = 0x7f131420

.field public static final cs_631_sign_send_over_choice:I = 0x7f131421

.field public static final cs_631_sign_send_waiting:I = 0x7f131422

.field public static final cs_631_sign_sendto:I = 0x7f131423

.field public static final cs_631_sign_share:I = 0x7f131424

.field public static final cs_631_sign_shareby:I = 0x7f131425

.field public static final cs_631_sign_signatory:I = 0x7f131426

.field public static final cs_631_sign_signature:I = 0x7f131427

.field public static final cs_631_sign_signed:I = 0x7f131428

.field public static final cs_631_sign_signed00:I = 0x7f131429

.field public static final cs_631_sign_signed_01:I = 0x7f13142a

.field public static final cs_631_sign_signsuccess:I = 0x7f13142b

.field public static final cs_631_sign_slgan:I = 0x7f13142c

.field public static final cs_631_sign_stamp:I = 0x7f13142d

.field public static final cs_631_sign_status_status:I = 0x7f13142e

.field public static final cs_631_sign_tips_fall_01:I = 0x7f13142f

.field public static final cs_631_sign_tips_fall_02:I = 0x7f131430

.field public static final cs_631_sign_tips_fall_03:I = 0x7f131431

.field public static final cs_631_sign_tips_fall_04:I = 0x7f131432

.field public static final cs_631_sign_title:I = 0x7f131433

.field public static final cs_631_sign_to_other:I = 0x7f131434

.field public static final cs_631_sign_tutorial:I = 0x7f131435

.field public static final cs_631_sign_uninvited:I = 0x7f131436

.field public static final cs_631_sign_unsigned:I = 0x7f131437

.field public static final cs_631_sign_unsigned00:I = 0x7f131438

.field public static final cs_631_sign_unsigned_01:I = 0x7f131439

.field public static final cs_631_sign_usual_qa:I = 0x7f13143a

.field public static final cs_631_sign_validtill:I = 0x7f13143b

.field public static final cs_631_sign_validtime:I = 0x7f13143c

.field public static final cs_631_sign_wechat:I = 0x7f13143d

.field public static final cs_631_sign_wechat_tips_receive:I = 0x7f13143e

.field public static final cs_631_sign_who:I = 0x7f13143f

.field public static final cs_631_sign_who_to:I = 0x7f131440

.field public static final cs_631_to_image:I = 0x7f131441

.field public static final cs_631_type:I = 0x7f131442

.field public static final cs_631_viplevel_01:I = 0x7f131443

.field public static final cs_631_viplevel_02:I = 0x7f131444

.field public static final cs_631_viplevel_03:I = 0x7f131445

.field public static final cs_631_viplevel_04:I = 0x7f131446

.field public static final cs_631_viplevel_05:I = 0x7f131447

.field public static final cs_631_viplevel_06:I = 0x7f131448

.field public static final cs_631_viplevel_07:I = 0x7f131449

.field public static final cs_631_viplevel_08:I = 0x7f13144a

.field public static final cs_631_viplevel_09:I = 0x7f13144b

.field public static final cs_631_viplevel_10:I = 0x7f13144c

.field public static final cs_631_viplevel_11:I = 0x7f13144d

.field public static final cs_631_viplevel_12:I = 0x7f13144e

.field public static final cs_631_viplevel_13:I = 0x7f13144f

.field public static final cs_631_viplevel_14:I = 0x7f131450

.field public static final cs_631_viplevel_15:I = 0x7f131451

.field public static final cs_631_viplevel_16:I = 0x7f131452

.field public static final cs_631_viplevel_17:I = 0x7f131453

.field public static final cs_631_viplevel_18:I = 0x7f131454

.field public static final cs_631_viplevel_19:I = 0x7f131455

.field public static final cs_631_viplevel_20:I = 0x7f131456

.field public static final cs_631_viplevel_21:I = 0x7f131457

.field public static final cs_631_viplevel_22:I = 0x7f131458

.field public static final cs_631_viplevel_23:I = 0x7f131459

.field public static final cs_631_viplevel_24:I = 0x7f13145a

.field public static final cs_631_viplevel_25:I = 0x7f13145b

.field public static final cs_631_viplevel_26:I = 0x7f13145c

.field public static final cs_631_viplevel_27:I = 0x7f13145d

.field public static final cs_631_viplevel_28:I = 0x7f13145e

.field public static final cs_631_viplevel_29:I = 0x7f13145f

.field public static final cs_631_viplevel_30:I = 0x7f131460

.field public static final cs_631_viplevel_31:I = 0x7f131461

.field public static final cs_631_viplevel_32:I = 0x7f131462

.field public static final cs_631_viplevel_33:I = 0x7f131463

.field public static final cs_631_viplevel_34:I = 0x7f131464

.field public static final cs_631_viplevel_35:I = 0x7f131465

.field public static final cs_631_viplevel_36:I = 0x7f131466

.field public static final cs_631_whatsapp:I = 0x7f131467

.field public static final cs_631_workplan_01:I = 0x7f131468

.field public static final cs_631_workplan_02:I = 0x7f131469

.field public static final cs_631_workplan_03:I = 0x7f13146a

.field public static final cs_631_workplan_04:I = 0x7f13146b

.field public static final cs_631_workplan_05:I = 0x7f13146c

.field public static final cs_631_workplan_06:I = 0x7f13146d

.field public static final cs_631_workplan_07:I = 0x7f13146e

.field public static final cs_631_workplan_08:I = 0x7f13146f

.field public static final cs_631_workplan_09:I = 0x7f131470

.field public static final cs_631_workplan_10:I = 0x7f131471

.field public static final cs_631_workplan_11:I = 0x7f131472

.field public static final cs_631_workplan_12:I = 0x7f131473

.field public static final cs_631_workplan_13:I = 0x7f131474

.field public static final cs_631_workplan_15:I = 0x7f131475

.field public static final cs_631_workplan_16:I = 0x7f131476

.field public static final cs_631_workplan_17:I = 0x7f131477

.field public static final cs_631_workplan_18:I = 0x7f131478

.field public static final cs_631_workplan_19:I = 0x7f131479

.field public static final cs_631_workplan_20:I = 0x7f13147a

.field public static final cs_631_workplan_21:I = 0x7f13147b

.field public static final cs_631_workplan_22:I = 0x7f13147c

.field public static final cs_631_workplan_23:I = 0x7f13147d

.field public static final cs_631_workplan_24:I = 0x7f13147e

.field public static final cs_631_workplan_25:I = 0x7f13147f

.field public static final cs_631_workplan_26:I = 0x7f131480

.field public static final cs_631_workplan_27:I = 0x7f131481

.field public static final cs_631_workplan_28:I = 0x7f131482

.field public static final cs_631_workplan_29:I = 0x7f131483

.field public static final cs_631_workplan_30:I = 0x7f131484

.field public static final cs_631_workplan_31:I = 0x7f131485

.field public static final cs_631_workplan_32:I = 0x7f131486

.field public static final cs_631_workplan_33:I = 0x7f131487

.field public static final cs_631_workplan_34:I = 0x7f131488

.field public static final cs_631_workplan_35:I = 0x7f131489

.field public static final cs_631_workplan_36:I = 0x7f13148a

.field public static final cs_631_workplan_37:I = 0x7f13148b

.field public static final cs_631_workplan_38:I = 0x7f13148c

.field public static final cs_631_workplan_39:I = 0x7f13148d

.field public static final cs_631_workplan_40:I = 0x7f13148e

.field public static final cs_631_workplan_41:I = 0x7f13148f

.field public static final cs_632_cloud_05:I = 0x7f131490

.field public static final cs_632_cloud_08:I = 0x7f131491

.field public static final cs_632_cloud_09:I = 0x7f131492

.field public static final cs_632_cloud_10:I = 0x7f131493

.field public static final cs_632_cloud_11:I = 0x7f131494

.field public static final cs_632_cloud_12:I = 0x7f131495

.field public static final cs_632_cloud_13:I = 0x7f131496

.field public static final cs_632_floder_01:I = 0x7f131497

.field public static final cs_632_floder_02:I = 0x7f131498

.field public static final cs_632_floder_03:I = 0x7f131499

.field public static final cs_632_floder_04:I = 0x7f13149a

.field public static final cs_632_floder_05:I = 0x7f13149b

.field public static final cs_632_floder_06:I = 0x7f13149c

.field public static final cs_632_floder_07:I = 0x7f13149d

.field public static final cs_632_floder_08:I = 0x7f13149e

.field public static final cs_632_floder_09:I = 0x7f13149f

.field public static final cs_632_floder_10:I = 0x7f1314a0

.field public static final cs_632_floder_11:I = 0x7f1314a1

.field public static final cs_632_floder_12:I = 0x7f1314a2

.field public static final cs_632_floder_13:I = 0x7f1314a3

.field public static final cs_632_floder_14:I = 0x7f1314a4

.field public static final cs_632_floder_15:I = 0x7f1314a5

.field public static final cs_632_floder_16:I = 0x7f1314a6

.field public static final cs_632_floder_17:I = 0x7f1314a7

.field public static final cs_632_floder_18:I = 0x7f1314a8

.field public static final cs_632_floder_19:I = 0x7f1314a9

.field public static final cs_632_floder_20:I = 0x7f1314aa

.field public static final cs_632_floder_21:I = 0x7f1314ab

.field public static final cs_632_floder_22:I = 0x7f1314ac

.field public static final cs_632_floder_23:I = 0x7f1314ad

.field public static final cs_632_floder_24:I = 0x7f1314ae

.field public static final cs_632_floder_25:I = 0x7f1314af

.field public static final cs_632_floder_26:I = 0x7f1314b0

.field public static final cs_632_floder_27:I = 0x7f1314b1

.field public static final cs_632_floder_28:I = 0x7f1314b2

.field public static final cs_632_floder_29:I = 0x7f1314b3

.field public static final cs_632_floder_30:I = 0x7f1314b4

.field public static final cs_632_floder_31:I = 0x7f1314b5

.field public static final cs_632_floder_32:I = 0x7f1314b6

.field public static final cs_632_floder_33:I = 0x7f1314b7

.field public static final cs_632_floder_34:I = 0x7f1314b8

.field public static final cs_632_floder_35:I = 0x7f1314b9

.field public static final cs_632_floder_36:I = 0x7f1314ba

.field public static final cs_632_floder_37:I = 0x7f1314bb

.field public static final cs_632_floder_38:I = 0x7f1314bc

.field public static final cs_632_floder_39:I = 0x7f1314bd

.field public static final cs_632_floder_40:I = 0x7f1314be

.field public static final cs_632_mall_01:I = 0x7f1314bf

.field public static final cs_632_mall_02:I = 0x7f1314c0

.field public static final cs_632_mall_03:I = 0x7f1314c1

.field public static final cs_632_mall_04:I = 0x7f1314c2

.field public static final cs_632_mall_05:I = 0x7f1314c3

.field public static final cs_632_mall_06:I = 0x7f1314c4

.field public static final cs_632_mall_privacy_policy:I = 0x7f1314c5

.field public static final cs_632_newmore_32:I = 0x7f1314c6

.field public static final cs_632_newmore_35:I = 0x7f1314c7

.field public static final cs_632_newmore_38:I = 0x7f1314c8

.field public static final cs_632_newmore_39:I = 0x7f1314c9

.field public static final cs_632_newmore_40:I = 0x7f1314ca

.field public static final cs_632_newmore_add_signature:I = 0x7f1314cb

.field public static final cs_632_newmore_add_text:I = 0x7f1314cc

.field public static final cs_632_newmore_add_title:I = 0x7f1314cd

.field public static final cs_632_newmore_add_to_siri:I = 0x7f1314ce

.field public static final cs_632_newmore_add_watermark:I = 0x7f1314cf

.field public static final cs_632_newmore_airprint:I = 0x7f1314d0

.field public static final cs_632_newmore_annotate:I = 0x7f1314d1

.field public static final cs_632_newmore_annotation:I = 0x7f1314d2

.field public static final cs_632_newmore_antitheft:I = 0x7f1314d3

.field public static final cs_632_newmore_clear_passsword_inapp:I = 0x7f1314d4

.field public static final cs_632_newmore_collage:I = 0x7f1314d5

.field public static final cs_632_newmore_continue_takephoto:I = 0x7f1314d6

.field public static final cs_632_newmore_copy:I = 0x7f1314d7

.field public static final cs_632_newmore_copy_move:I = 0x7f1314d8

.field public static final cs_632_newmore_crop:I = 0x7f1314d9

.field public static final cs_632_newmore_edit:I = 0x7f1314da

.field public static final cs_632_newmore_edit_on_pc:I = 0x7f1314db

.field public static final cs_632_newmore_edit_pdf:I = 0x7f1314dc

.field public static final cs_632_newmore_email:I = 0x7f1314dd

.field public static final cs_632_newmore_email_to_mysellf:I = 0x7f1314de

.field public static final cs_632_newmore_evidence:I = 0x7f1314df

.field public static final cs_632_newmore_fax:I = 0x7f1314e0

.field public static final cs_632_newmore_file_password:I = 0x7f1314e1

.field public static final cs_632_newmore_file_security:I = 0x7f1314e2

.field public static final cs_632_newmore_filter:I = 0x7f1314e3

.field public static final cs_632_newmore_import_image:I = 0x7f1314e4

.field public static final cs_632_newmore_move:I = 0x7f1314e5

.field public static final cs_632_newmore_notes:I = 0x7f1314e6

.field public static final cs_632_newmore_other:I = 0x7f1314e7

.field public static final cs_632_newmore_passsword_inapp:I = 0x7f1314e8

.field public static final cs_632_newmore_print:I = 0x7f1314e9

.field public static final cs_632_newmore_properties:I = 0x7f1314ea

.field public static final cs_632_newmore_protect_pdf:I = 0x7f1314eb

.field public static final cs_632_newmore_rotate:I = 0x7f1314ec

.field public static final cs_632_newmore_save_to_gallery:I = 0x7f1314ed

.field public static final cs_632_newmore_send_share:I = 0x7f1314ee

.field public static final cs_632_newmore_smart_recognition:I = 0x7f1314ef

.field public static final cs_632_newmore_smudge:I = 0x7f1314f0

.field public static final cs_632_newmore_sort:I = 0x7f1314f1

.field public static final cs_632_newmore_text_recognition:I = 0x7f1314f2

.field public static final cs_632_newmore_toexcel:I = 0x7f1314f3

.field public static final cs_632_newmore_toppt:I = 0x7f1314f4

.field public static final cs_632_newmore_toword:I = 0x7f1314f5

.field public static final cs_632_newmore_translate:I = 0x7f1314f6

.field public static final cs_632_newmore_upload:I = 0x7f1314f7

.field public static final cs_632_ocrerror_01:I = 0x7f1314f8

.field public static final cs_632_ocrerror_02:I = 0x7f1314f9

.field public static final cs_632_scangift_01:I = 0x7f1314fa

.field public static final cs_632_scangift_02:I = 0x7f1314fb

.field public static final cs_632_scangift_03:I = 0x7f1314fc

.field public static final cs_632_scangift_04:I = 0x7f1314fd

.field public static final cs_632_scangift_05:I = 0x7f1314fe

.field public static final cs_632_scangift_06:I = 0x7f1314ff

.field public static final cs_632_scangift_07:I = 0x7f131500

.field public static final cs_632_scangift_08:I = 0x7f131501

.field public static final cs_632_scangift_09:I = 0x7f131502

.field public static final cs_632_scangift_10:I = 0x7f131503

.field public static final cs_632_scangift_11:I = 0x7f131504

.field public static final cs_632_scangift_12:I = 0x7f131505

.field public static final cs_633_cloud_01:I = 0x7f131506

.field public static final cs_633_cloud_02:I = 0x7f131507

.field public static final cs_633_cloud_03:I = 0x7f131508

.field public static final cs_633_cloud_share_tip:I = 0x7f131509

.field public static final cs_633_edu_02:I = 0x7f13150a

.field public static final cs_633_edu_03:I = 0x7f13150b

.field public static final cs_633_edu_26:I = 0x7f13150c

.field public static final cs_633_lab_01:I = 0x7f13150d

.field public static final cs_633_lab_02:I = 0x7f13150e

.field public static final cs_633_lab_03:I = 0x7f13150f

.field public static final cs_633_lab_04:I = 0x7f131510

.field public static final cs_633_lab_05:I = 0x7f131511

.field public static final cs_633_lab_06:I = 0x7f131512

.field public static final cs_633_lab_07:I = 0x7f131513

.field public static final cs_633_lab_08:I = 0x7f131514

.field public static final cs_633_lab_09:I = 0x7f131515

.field public static final cs_633_lab_10:I = 0x7f131516

.field public static final cs_633_lab_11:I = 0x7f131517

.field public static final cs_633_lab_12:I = 0x7f131518

.field public static final cs_633_lab_13:I = 0x7f131519

.field public static final cs_633_lab_14:I = 0x7f13151a

.field public static final cs_633_lab_15:I = 0x7f13151b

.field public static final cs_633_lab_16:I = 0x7f13151c

.field public static final cs_633_lab_17:I = 0x7f13151d

.field public static final cs_633_lab_18:I = 0x7f13151e

.field public static final cs_633_lab_19:I = 0x7f13151f

.field public static final cs_633_lab_20:I = 0x7f131520

.field public static final cs_633_lab_21:I = 0x7f131521

.field public static final cs_633_lab_22:I = 0x7f131522

.field public static final cs_633_lab_23:I = 0x7f131523

.field public static final cs_633_lab_24:I = 0x7f131524

.field public static final cs_633_lab_25:I = 0x7f131525

.field public static final cs_633_life_info:I = 0x7f131526

.field public static final cs_633_sign_capture:I = 0x7f131527

.field public static final cs_633_sign_tips_01:I = 0x7f131528

.field public static final cs_633_study_info:I = 0x7f131529

.field public static final cs_633_study_info_desc:I = 0x7f13152a

.field public static final cs_633_sync_1gb:I = 0x7f13152b

.field public static final cs_633_sync_auto:I = 0x7f13152c

.field public static final cs_633_sync_bt_descr:I = 0x7f13152d

.field public static final cs_633_sync_content01:I = 0x7f13152e

.field public static final cs_633_sync_content02:I = 0x7f13152f

.field public static final cs_633_sync_content03:I = 0x7f131530

.field public static final cs_633_sync_content04:I = 0x7f131531

.field public static final cs_633_sync_content05:I = 0x7f131532

.field public static final cs_633_sync_content06:I = 0x7f131533

.field public static final cs_633_sync_content07:I = 0x7f131534

.field public static final cs_633_sync_content08:I = 0x7f131535

.field public static final cs_633_sync_content09:I = 0x7f131536

.field public static final cs_633_sync_ocr:I = 0x7f131537

.field public static final cs_633_sync_off_btn:I = 0x7f131538

.field public static final cs_633_sync_off_pop:I = 0x7f131539

.field public static final cs_633_sync_off_pop01:I = 0x7f13153a

.field public static final cs_633_sync_platform:I = 0x7f13153b

.field public static final cs_633_sync_pop:I = 0x7f13153c

.field public static final cs_633_sync_testpaper:I = 0x7f13153d

.field public static final cs_633_sync_title:I = 0x7f13153e

.field public static final cs_633_sync_turn_off:I = 0x7f13153f

.field public static final cs_633_sync_turn_on:I = 0x7f131540

.field public static final cs_633_work_info:I = 0x7f131541

.field public static final cs_634_cloud_01:I = 0x7f131542

.field public static final cs_634_cloud_02:I = 0x7f131543

.field public static final cs_634_cloud_03:I = 0x7f131544

.field public static final cs_634_cloud_04:I = 0x7f131545

.field public static final cs_634_cloud_05:I = 0x7f131546

.field public static final cs_634_cloud_06:I = 0x7f131547

.field public static final cs_634_cloud_07:I = 0x7f131548

.field public static final cs_634_cloud_08:I = 0x7f131549

.field public static final cs_634_cloud_09:I = 0x7f13154a

.field public static final cs_634_cloud_10:I = 0x7f13154b

.field public static final cs_634_cloud_open:I = 0x7f13154c

.field public static final cs_634_cloud_setting:I = 0x7f13154d

.field public static final cs_634_invoice_2b:I = 0x7f13154e

.field public static final cs_634_pdf_share_coppressed_01:I = 0x7f13154f

.field public static final cs_634_pdf_share_coppressed_02:I = 0x7f131550

.field public static final cs_634_pdf_share_coppressed_03:I = 0x7f131551

.field public static final cs_634_pdf_share_coppressed_04:I = 0x7f131552

.field public static final cs_634_pdf_share_coppressed_05:I = 0x7f131553

.field public static final cs_634_pdf_share_coppressed_06:I = 0x7f131554

.field public static final cs_634_pdf_share_coppressed_07:I = 0x7f131555

.field public static final cs_634_pdf_share_coppressed_08:I = 0x7f131556

.field public static final cs_634_pdf_share_coppressed_09:I = 0x7f131557

.field public static final cs_634_pdf_share_coppressed_10:I = 0x7f131558

.field public static final cs_634_read_add:I = 0x7f131559

.field public static final cs_634_saveto_t1:I = 0x7f13155a

.field public static final cs_634_saveto_t2:I = 0x7f13155b

.field public static final cs_634_saveto_t3:I = 0x7f13155c

.field public static final cs_634_scan_01:I = 0x7f13155d

.field public static final cs_634_scan_tip:I = 0x7f13155e

.field public static final cs_634_scangift_03:I = 0x7f13155f

.field public static final cs_634_share_flow_free_01:I = 0x7f131560

.field public static final cs_634_share_flow_free_02:I = 0x7f131561

.field public static final cs_634_share_flow_free_03:I = 0x7f131562

.field public static final cs_634_share_flow_free_04:I = 0x7f131563

.field public static final cs_634_share_flow_free_05:I = 0x7f131564

.field public static final cs_634_share_flow_free_06:I = 0x7f131565

.field public static final cs_634_share_q_01:I = 0x7f131566

.field public static final cs_634_share_q_02:I = 0x7f131567

.field public static final cs_634_share_q_03:I = 0x7f131568

.field public static final cs_634_sign_title:I = 0x7f131569

.field public static final cs_634_tablet_01:I = 0x7f13156a

.field public static final cs_634_tablet_02:I = 0x7f13156b

.field public static final cs_634_tablet_03:I = 0x7f13156c

.field public static final cs_634_tablet_04:I = 0x7f13156d

.field public static final cs_634_tablet_05:I = 0x7f13156e

.field public static final cs_634_tablet_06:I = 0x7f13156f

.field public static final cs_635_account_01:I = 0x7f131570

.field public static final cs_635_account_02:I = 0x7f131571

.field public static final cs_635_account_apple:I = 0x7f131572

.field public static final cs_635_account_cloud:I = 0x7f131573

.field public static final cs_635_account_google:I = 0x7f131574

.field public static final cs_635_account_log0:I = 0x7f131575

.field public static final cs_635_account_log1:I = 0x7f131576

.field public static final cs_635_account_log2:I = 0x7f131577

.field public static final cs_635_account_log3:I = 0x7f131578

.field public static final cs_635_account_log4:I = 0x7f131579

.field public static final cs_635_account_log5:I = 0x7f13157a

.field public static final cs_635_account_log6:I = 0x7f13157b

.field public static final cs_635_account_log7:I = 0x7f13157c

.field public static final cs_635_account_log8:I = 0x7f13157d

.field public static final cs_635_account_lognow:I = 0x7f13157e

.field public static final cs_635_account_tip0:I = 0x7f13157f

.field public static final cs_635_account_tip1:I = 0x7f131580

.field public static final cs_635_account_tip10:I = 0x7f131581

.field public static final cs_635_account_tip11:I = 0x7f131582

.field public static final cs_635_account_tip12:I = 0x7f131583

.field public static final cs_635_account_tip13:I = 0x7f131584

.field public static final cs_635_account_tip14:I = 0x7f131585

.field public static final cs_635_account_tip15:I = 0x7f131586

.field public static final cs_635_account_tip16:I = 0x7f131587

.field public static final cs_635_account_tip17:I = 0x7f131588

.field public static final cs_635_account_tip18:I = 0x7f131589

.field public static final cs_635_account_tip19:I = 0x7f13158a

.field public static final cs_635_account_tip2:I = 0x7f13158b

.field public static final cs_635_account_tip20:I = 0x7f13158c

.field public static final cs_635_account_tip21:I = 0x7f13158d

.field public static final cs_635_account_tip22:I = 0x7f13158e

.field public static final cs_635_account_tip23:I = 0x7f13158f

.field public static final cs_635_account_tip24:I = 0x7f131590

.field public static final cs_635_account_tip25:I = 0x7f131591

.field public static final cs_635_account_tip26:I = 0x7f131592

.field public static final cs_635_account_tip27:I = 0x7f131593

.field public static final cs_635_account_tip28:I = 0x7f131594

.field public static final cs_635_account_tip29:I = 0x7f131595

.field public static final cs_635_account_tip3:I = 0x7f131596

.field public static final cs_635_account_tip30:I = 0x7f131597

.field public static final cs_635_account_tip31:I = 0x7f131598

.field public static final cs_635_account_tip32:I = 0x7f131599

.field public static final cs_635_account_tip33:I = 0x7f13159a

.field public static final cs_635_account_tip34:I = 0x7f13159b

.field public static final cs_635_account_tip35:I = 0x7f13159c

.field public static final cs_635_account_tip36:I = 0x7f13159d

.field public static final cs_635_account_tip37:I = 0x7f13159e

.field public static final cs_635_account_tip38:I = 0x7f13159f

.field public static final cs_635_account_tip39:I = 0x7f1315a0

.field public static final cs_635_account_tip4:I = 0x7f1315a1

.field public static final cs_635_account_tip40:I = 0x7f1315a2

.field public static final cs_635_account_tip5:I = 0x7f1315a3

.field public static final cs_635_account_tip6:I = 0x7f1315a4

.field public static final cs_635_account_tip7:I = 0x7f1315a5

.field public static final cs_635_account_tip8:I = 0x7f1315a6

.field public static final cs_635_account_tip9:I = 0x7f1315a7

.field public static final cs_635_dir_person:I = 0x7f1315a8

.field public static final cs_635_dir_share:I = 0x7f1315a9

.field public static final cs_635_dir_title:I = 0x7f1315aa

.field public static final cs_635_log_pop_button1:I = 0x7f1315ab

.field public static final cs_635_log_pop_button2:I = 0x7f1315ac

.field public static final cs_635_log_pop_txt1:I = 0x7f1315ad

.field public static final cs_635_mobile_change:I = 0x7f1315ae

.field public static final cs_635_mobile_now:I = 0x7f1315af

.field public static final cs_635_pay_01:I = 0x7f1315b0

.field public static final cs_635_pay_02:I = 0x7f1315b1

.field public static final cs_635_pay_03:I = 0x7f1315b2

.field public static final cs_635_pay_04:I = 0x7f1315b3

.field public static final cs_635_pay_05:I = 0x7f1315b4

.field public static final cs_635_pay_06:I = 0x7f1315b5

.field public static final cs_635_pay_07:I = 0x7f1315b6

.field public static final cs_635_pay_08:I = 0x7f1315b7

.field public static final cs_635_pay_09:I = 0x7f1315b8

.field public static final cs_635_pay_10:I = 0x7f1315b9

.field public static final cs_635_pay_11:I = 0x7f1315ba

.field public static final cs_635_pay_12:I = 0x7f1315bb

.field public static final cs_635_pay_13:I = 0x7f1315bc

.field public static final cs_635_pay_14:I = 0x7f1315bd

.field public static final cs_635_pay_15:I = 0x7f1315be

.field public static final cs_635_pay_16:I = 0x7f1315bf

.field public static final cs_635_pay_17:I = 0x7f1315c0

.field public static final cs_635_recent_use:I = 0x7f1315c1

.field public static final cs_636_account_delete_1:I = 0x7f1315c2

.field public static final cs_636_account_delete_10:I = 0x7f1315c3

.field public static final cs_636_account_delete_9:I = 0x7f1315c4

.field public static final cs_636_account_email:I = 0x7f1315c5

.field public static final cs_636_account_email2:I = 0x7f1315c6

.field public static final cs_636_account_mobile:I = 0x7f1315c7

.field public static final cs_636_account_note1:I = 0x7f1315c8

.field public static final cs_636_account_note1_1:I = 0x7f1315c9

.field public static final cs_636_account_note1_2:I = 0x7f1315ca

.field public static final cs_636_account_note1_3:I = 0x7f1315cb

.field public static final cs_636_account_note2:I = 0x7f1315cc

.field public static final cs_636_account_note3:I = 0x7f1315cd

.field public static final cs_636_account_note4:I = 0x7f1315ce

.field public static final cs_636_account_wx:I = 0x7f1315cf

.field public static final cs_636_app_copyright:I = 0x7f1315d0

.field public static final cs_636_camera_01:I = 0x7f1315d1

.field public static final cs_636_camera_deny01:I = 0x7f1315d2

.field public static final cs_636_camera_deny02:I = 0x7f1315d3

.field public static final cs_636_camera_deny03:I = 0x7f1315d4

.field public static final cs_636_camera_deny04:I = 0x7f1315d5

.field public static final cs_636_enterprise_01:I = 0x7f1315d6

.field public static final cs_636_id_color_all:I = 0x7f1315d7

.field public static final cs_636_id_color_yellow:I = 0x7f1315d8

.field public static final cs_636_indonesia_photo_01:I = 0x7f1315d9

.field public static final cs_636_indonesia_photo_02:I = 0x7f1315da

.field public static final cs_636_pdf_01:I = 0x7f1315db

.field public static final cs_636_pdf_02:I = 0x7f1315dc

.field public static final cs_636_pdf_03:I = 0x7f1315dd

.field public static final cs_636_pdf_04:I = 0x7f1315de

.field public static final cs_636_pdf_05:I = 0x7f1315df

.field public static final cs_636_pdf_06:I = 0x7f1315e0

.field public static final cs_636_pdf_07:I = 0x7f1315e1

.field public static final cs_636_pdf_08:I = 0x7f1315e2

.field public static final cs_636_pdf_09:I = 0x7f1315e3

.field public static final cs_636_pdf_10:I = 0x7f1315e4

.field public static final cs_636_pdf_11:I = 0x7f1315e5

.field public static final cs_636_pdf_12:I = 0x7f1315e6

.field public static final cs_636_pdf_13:I = 0x7f1315e7

.field public static final cs_636_save_label_share:I = 0x7f1315e8

.field public static final cs_636_table_01:I = 0x7f1315e9

.field public static final cs_636_table_02:I = 0x7f1315ea

.field public static final cs_636_table_03:I = 0x7f1315eb

.field public static final cs_636_table_04:I = 0x7f1315ec

.field public static final cs_636_use_rate_2:I = 0x7f1315ed

.field public static final cs_636_use_rate_3:I = 0x7f1315ee

.field public static final cs_636_use_rate_4:I = 0x7f1315ef

.field public static final cs_636_use_rate_5:I = 0x7f1315f0

.field public static final cs_636_yellow_id_card:I = 0x7f1315f1

.field public static final cs_637_check_detail_01:I = 0x7f1315f2

.field public static final cs_637_delete_sign:I = 0x7f1315f3

.field public static final cs_637_docradar_01:I = 0x7f1315f4

.field public static final cs_637_docradar_02:I = 0x7f1315f5

.field public static final cs_637_docradar_03:I = 0x7f1315f6

.field public static final cs_637_docradar_04:I = 0x7f1315f7

.field public static final cs_637_docradar_05:I = 0x7f1315f8

.field public static final cs_637_docradar_06:I = 0x7f1315f9

.field public static final cs_637_docradar_07:I = 0x7f1315fa

.field public static final cs_637_docradar_08:I = 0x7f1315fb

.field public static final cs_637_docradar_09:I = 0x7f1315fc

.field public static final cs_637_docradar_10:I = 0x7f1315fd

.field public static final cs_637_docradar_11:I = 0x7f1315fe

.field public static final cs_637_docradar_12:I = 0x7f1315ff

.field public static final cs_637_docradar_13:I = 0x7f131600

.field public static final cs_637_done_sub_01:I = 0x7f131601

.field public static final cs_637_edu_01:I = 0x7f131602

.field public static final cs_637_edu_02:I = 0x7f131603

.field public static final cs_637_edu_03:I = 0x7f131604

.field public static final cs_637_edu_04:I = 0x7f131605

.field public static final cs_637_edu_05:I = 0x7f131606

.field public static final cs_637_edu_06:I = 0x7f131607

.field public static final cs_637_edu_07:I = 0x7f131608

.field public static final cs_637_edu_08:I = 0x7f131609

.field public static final cs_637_edu_09:I = 0x7f13160a

.field public static final cs_637_edu_09_a:I = 0x7f13160b

.field public static final cs_637_edu_10:I = 0x7f13160c

.field public static final cs_637_edu_10_a:I = 0x7f13160d

.field public static final cs_637_edu_10_b:I = 0x7f13160e

.field public static final cs_637_edu_11:I = 0x7f13160f

.field public static final cs_637_edu_12:I = 0x7f131610

.field public static final cs_637_edu_13:I = 0x7f131611

.field public static final cs_637_edu_14:I = 0x7f131612

.field public static final cs_637_edu_15:I = 0x7f131613

.field public static final cs_637_edu_16:I = 0x7f131614

.field public static final cs_637_edu_17:I = 0x7f131615

.field public static final cs_637_edu_18:I = 0x7f131616

.field public static final cs_637_edu_19:I = 0x7f131617

.field public static final cs_637_edu_20:I = 0x7f131618

.field public static final cs_637_edu_21:I = 0x7f131619

.field public static final cs_637_edu_22:I = 0x7f13161a

.field public static final cs_637_edu_23:I = 0x7f13161b

.field public static final cs_637_edu_24:I = 0x7f13161c

.field public static final cs_637_edu_25:I = 0x7f13161d

.field public static final cs_637_edu_26:I = 0x7f13161e

.field public static final cs_637_edu_27:I = 0x7f13161f

.field public static final cs_637_edu_28:I = 0x7f131620

.field public static final cs_637_edu_29:I = 0x7f131621

.field public static final cs_637_edu_30:I = 0x7f131622

.field public static final cs_637_edu_31:I = 0x7f131623

.field public static final cs_637_edu_32:I = 0x7f131624

.field public static final cs_637_edu_33:I = 0x7f131625

.field public static final cs_637_edu_34:I = 0x7f131626

.field public static final cs_637_edu_35:I = 0x7f131627

.field public static final cs_637_edu_36:I = 0x7f131628

.field public static final cs_637_edu_37:I = 0x7f131629

.field public static final cs_637_edu_38:I = 0x7f13162a

.field public static final cs_637_edu_39:I = 0x7f13162b

.field public static final cs_637_edu_40:I = 0x7f13162c

.field public static final cs_637_edu_41:I = 0x7f13162d

.field public static final cs_637_edu_42:I = 0x7f13162e

.field public static final cs_637_edu_43:I = 0x7f13162f

.field public static final cs_637_edu_44:I = 0x7f131630

.field public static final cs_637_edu_45:I = 0x7f131631

.field public static final cs_637_edu_46:I = 0x7f131632

.field public static final cs_637_edu_47:I = 0x7f131633

.field public static final cs_637_edu_48:I = 0x7f131634

.field public static final cs_637_edu_49:I = 0x7f131635

.field public static final cs_637_incentive_01:I = 0x7f131636

.field public static final cs_637_incentive_02:I = 0x7f131637

.field public static final cs_637_incentive_03:I = 0x7f131638

.field public static final cs_637_incentive_04:I = 0x7f131639

.field public static final cs_637_incentive_05:I = 0x7f13163a

.field public static final cs_637_incentive_06:I = 0x7f13163b

.field public static final cs_637_incentive_07:I = 0x7f13163c

.field public static final cs_637_incentive_08:I = 0x7f13163d

.field public static final cs_637_incentive_09:I = 0x7f13163e

.field public static final cs_637_incentive_10:I = 0x7f13163f

.field public static final cs_637_incentive_11:I = 0x7f131640

.field public static final cs_637_incentive_13:I = 0x7f131641

.field public static final cs_637_incentive_14:I = 0x7f131642

.field public static final cs_637_incentive_15:I = 0x7f131643

.field public static final cs_637_incentive_16:I = 0x7f131644

.field public static final cs_637_incentive_17:I = 0x7f131645

.field public static final cs_637_incentive_18:I = 0x7f131646

.field public static final cs_637_incentive_19:I = 0x7f131647

.field public static final cs_637_incentive_20:I = 0x7f131648

.field public static final cs_637_incentive_21:I = 0x7f131649

.field public static final cs_637_incentive_22:I = 0x7f13164a

.field public static final cs_637_incentive_23:I = 0x7f13164b

.field public static final cs_637_optimize_01:I = 0x7f13164c

.field public static final cs_637_optimize_02:I = 0x7f13164d

.field public static final cs_637_optimize_03:I = 0x7f13164e

.field public static final cs_637_optimize_04:I = 0x7f13164f

.field public static final cs_637_optimize_05:I = 0x7f131650

.field public static final cs_637_optimize_06:I = 0x7f131651

.field public static final cs_637_optimize_07:I = 0x7f131652

.field public static final cs_637_optimize_08:I = 0x7f131653

.field public static final cs_637_optimize_09:I = 0x7f131654

.field public static final cs_637_optimize_10:I = 0x7f131655

.field public static final cs_637_preparedoc_btn:I = 0x7f131656

.field public static final cs_637_preparedoc_ready:I = 0x7f131657

.field public static final cs_637_preparedoc_ready_btn:I = 0x7f131658

.field public static final cs_637_preparedoc_toast:I = 0x7f131659

.field public static final cs_637_preparedoc_txt1:I = 0x7f13165a

.field public static final cs_637_reboot_01:I = 0x7f13165b

.field public static final cs_637_reboot_02:I = 0x7f13165c

.field public static final cs_637_save_as:I = 0x7f13165d

.field public static final cs_637_send_sign_01:I = 0x7f13165e

.field public static final cs_637_sign_organize:I = 0x7f13165f

.field public static final cs_637_vip_button1:I = 0x7f131660

.field public static final cs_637_vip_button2:I = 0x7f131661

.field public static final cs_637_vip_button3:I = 0x7f131662

.field public static final cs_637_vip_title1:I = 0x7f131663

.field public static final cs_637_vip_title2:I = 0x7f131664

.field public static final cs_637_vip_txt1:I = 0x7f131665

.field public static final cs_637_vip_txt2:I = 0x7f131666

.field public static final cs_637_vip_txt3:I = 0x7f131667

.field public static final cs_637_vip_txt5:I = 0x7f131668

.field public static final cs_637_vip_txt6:I = 0x7f131669

.field public static final cs_637_wait:I = 0x7f13166a

.field public static final cs_637_word_01:I = 0x7f13166b

.field public static final cs_637_word_02:I = 0x7f13166c

.field public static final cs_637_word_03:I = 0x7f13166d

.field public static final cs_637_word_04:I = 0x7f13166e

.field public static final cs_637_word_05:I = 0x7f13166f

.field public static final cs_637_word_06:I = 0x7f131670

.field public static final cs_637_word_07:I = 0x7f131671

.field public static final cs_637_word_08:I = 0x7f131672

.field public static final cs_637_word_09:I = 0x7f131673

.field public static final cs_637_word_new0:I = 0x7f131674

.field public static final cs_637_word_new1:I = 0x7f131675

.field public static final cs_637_word_new2:I = 0x7f131676

.field public static final cs_638_bind_quick_btn:I = 0x7f131677

.field public static final cs_638_camera_02:I = 0x7f131678

.field public static final cs_638_camera_03:I = 0x7f131679

.field public static final cs_638_floder_view:I = 0x7f13167a

.field public static final cs_638_license_offline:I = 0x7f13167b

.field public static final cs_638_license_offline_1:I = 0x7f13167c

.field public static final cs_638_license_offline_2:I = 0x7f13167d

.field public static final cs_638_license_offline_3:I = 0x7f13167e

.field public static final cs_638_login_other_phone:I = 0x7f13167f

.field public static final cs_638_login_quick_btn:I = 0x7f131680

.field public static final cs_638_login_title:I = 0x7f131681

.field public static final cs_638_login_tost:I = 0x7f131682

.field public static final cs_638_phone_error:I = 0x7f131683

.field public static final cs_638_phone_verifycode:I = 0x7f131684

.field public static final cs_638_phone_verifycode_resend:I = 0x7f131685

.field public static final cs_638_recommend:I = 0x7f131686

.field public static final cs_638_vip_title1:I = 0x7f131687

.field public static final cs_638_vip_toast1:I = 0x7f131688

.field public static final cs_638_vip_toast2:I = 0x7f131689

.field public static final cs_638_vip_toast3:I = 0x7f13168a

.field public static final cs_638_vip_toast4:I = 0x7f13168b

.field public static final cs_638_vip_txt1:I = 0x7f13168c

.field public static final cs_639_enter_tips_01:I = 0x7f13168d

.field public static final cs_639_fefer_banner_01:I = 0x7f13168e

.field public static final cs_639_fefer_banner_02:I = 0x7f13168f

.field public static final cs_639_fefer_banner_03:I = 0x7f131690

.field public static final cs_639_fefer_fail_01:I = 0x7f131691

.field public static final cs_639_fefer_icon_01:I = 0x7f131692

.field public static final cs_639_fefer_popup_01:I = 0x7f131693

.field public static final cs_639_fefer_popup_02:I = 0x7f131694

.field public static final cs_639_fefer_popup_03:I = 0x7f131695

.field public static final cs_639_fefer_popup_04:I = 0x7f131696

.field public static final cs_639_fefer_popup_05:I = 0x7f131697

.field public static final cs_639_fefer_popup_06:I = 0x7f131698

.field public static final cs_639_fefer_popup_07:I = 0x7f131699

.field public static final cs_639_fefer_popup_08:I = 0x7f13169a

.field public static final cs_639_fefer_success_01:I = 0x7f13169b

.field public static final cs_639_fefer_success_02:I = 0x7f13169c

.field public static final cs_639_fefer_success_03:I = 0x7f13169d

.field public static final cs_639_guide_free1month:I = 0x7f13169e

.field public static final cs_639_guide_free3month:I = 0x7f13169f

.field public static final cs_639_guide_free6month:I = 0x7f1316a0

.field public static final cs_639_help1:I = 0x7f1316a1

.field public static final cs_639_help10:I = 0x7f1316a2

.field public static final cs_639_help11:I = 0x7f1316a3

.field public static final cs_639_help12:I = 0x7f1316a4

.field public static final cs_639_help13:I = 0x7f1316a5

.field public static final cs_639_help14:I = 0x7f1316a6

.field public static final cs_639_help15:I = 0x7f1316a7

.field public static final cs_639_help2:I = 0x7f1316a8

.field public static final cs_639_help3:I = 0x7f1316a9

.field public static final cs_639_help4:I = 0x7f1316aa

.field public static final cs_639_help5:I = 0x7f1316ab

.field public static final cs_639_help6:I = 0x7f1316ac

.field public static final cs_639_help7:I = 0x7f1316ad

.field public static final cs_639_help8:I = 0x7f1316ae

.field public static final cs_639_help9:I = 0x7f1316af

.field public static final cs_639_import_clear:I = 0x7f1316b0

.field public static final cs_639_nofree_notice_free1month:I = 0x7f1316b1

.field public static final cs_639_nofree_notice_free3month:I = 0x7f1316b2

.field public static final cs_639_nofree_notice_free6month:I = 0x7f1316b3

.field public static final cs_639_questionset_tutorial_body:I = 0x7f1316b4

.field public static final cs_639_questionset_tutorial_title:I = 0x7f1316b5

.field public static final cs_639_read_in_cs:I = 0x7f1316b6

.field public static final cs_639_retention_01:I = 0x7f1316b7

.field public static final cs_639_save_sign_pic:I = 0x7f1316b8

.field public static final cs_639_share_dingding:I = 0x7f1316b9

.field public static final cs_639_share_install_tip:I = 0x7f1316ba

.field public static final cs_639_share_limit:I = 0x7f1316bb

.field public static final cs_639_share_to:I = 0x7f1316bc

.field public static final cs_639_share_wecom:I = 0x7f1316bd

.field public static final cs_639_transfer_help:I = 0x7f1316be

.field public static final cs_639_transfer_tip:I = 0x7f1316bf

.field public static final cs_639_wechat_pdf_1:I = 0x7f1316c0

.field public static final cs_639_wechat_pdf_2:I = 0x7f1316c1

.field public static final cs_639_wechat_pdf_3:I = 0x7f1316c2

.field public static final cs_639_wechat_pdf_4:I = 0x7f1316c3

.field public static final cs_640_add_text:I = 0x7f1316c4

.field public static final cs_640_adjust_alpha:I = 0x7f1316c5

.field public static final cs_640_adjust_color:I = 0x7f1316c6

.field public static final cs_640_adjust_pen:I = 0x7f1316c7

.field public static final cs_640_adjust_size:I = 0x7f1316c8

.field public static final cs_640_adjust_stroke:I = 0x7f1316c9

.field public static final cs_640_adjust_style:I = 0x7f1316ca

.field public static final cs_640_baotian_7:I = 0x7f1316cb

.field public static final cs_640_bind_account:I = 0x7f1316cc

.field public static final cs_640_brush_tool:I = 0x7f1316cd

.field public static final cs_640_clear_watermark:I = 0x7f1316ce

.field public static final cs_640_code_tip:I = 0x7f1316cf

.field public static final cs_640_cs_logo:I = 0x7f1316d0

.field public static final cs_640_delete_text:I = 0x7f1316d1

.field public static final cs_640_edit_text:I = 0x7f1316d2

.field public static final cs_640_erase_all:I = 0x7f1316d3

.field public static final cs_640_eraser_tool:I = 0x7f1316d4

.field public static final cs_640_extraction_failed:I = 0x7f1316d5

.field public static final cs_640_file_conflict:I = 0x7f1316d6

.field public static final cs_640_file_conflict_btn_1:I = 0x7f1316d7

.field public static final cs_640_file_conflict_btn_2:I = 0x7f1316d8

.field public static final cs_640_file_conflict_intro:I = 0x7f1316d9

.field public static final cs_640_file_size:I = 0x7f1316da

.field public static final cs_640_insert_text_t1:I = 0x7f1316db

.field public static final cs_640_link_expires:I = 0x7f1316dc

.field public static final cs_640_link_valid:I = 0x7f1316dd

.field public static final cs_640_login_1:I = 0x7f1316de

.field public static final cs_640_login_10:I = 0x7f1316df

.field public static final cs_640_login_11:I = 0x7f1316e0

.field public static final cs_640_login_12:I = 0x7f1316e1

.field public static final cs_640_login_13:I = 0x7f1316e2

.field public static final cs_640_login_14:I = 0x7f1316e3

.field public static final cs_640_login_15:I = 0x7f1316e4

.field public static final cs_640_login_16:I = 0x7f1316e5

.field public static final cs_640_login_17:I = 0x7f1316e6

.field public static final cs_640_login_18:I = 0x7f1316e7

.field public static final cs_640_login_19:I = 0x7f1316e8

.field public static final cs_640_login_2:I = 0x7f1316e9

.field public static final cs_640_login_20:I = 0x7f1316ea

.field public static final cs_640_login_21:I = 0x7f1316eb

.field public static final cs_640_login_22:I = 0x7f1316ec

.field public static final cs_640_login_23:I = 0x7f1316ed

.field public static final cs_640_login_24:I = 0x7f1316ee

.field public static final cs_640_login_25:I = 0x7f1316ef

.field public static final cs_640_login_26:I = 0x7f1316f0

.field public static final cs_640_login_27:I = 0x7f1316f1

.field public static final cs_640_login_3:I = 0x7f1316f3

.field public static final cs_640_login_4:I = 0x7f1316f4

.field public static final cs_640_login_5:I = 0x7f1316f5

.field public static final cs_640_login_6:I = 0x7f1316f6

.field public static final cs_640_login_7:I = 0x7f1316f7

.field public static final cs_640_login_8:I = 0x7f1316f8

.field public static final cs_640_login_9:I = 0x7f1316f9

.field public static final cs_640_more_share:I = 0x7f1316fa

.field public static final cs_640_ocrresult_01:I = 0x7f1316fb

.field public static final cs_640_ocrresult_02:I = 0x7f1316fc

.field public static final cs_640_ocrresult_03:I = 0x7f1316fd

.field public static final cs_640_ocrresult_04:I = 0x7f1316fe

.field public static final cs_640_ocrresult_05:I = 0x7f1316ff

.field public static final cs_640_ocrresult_delete1:I = 0x7f131700

.field public static final cs_640_ocrresult_delete2:I = 0x7f131701

.field public static final cs_640_ocrresult_export_word1:I = 0x7f131702

.field public static final cs_640_ocrresult_share:I = 0x7f131703

.field public static final cs_640_ocrresult_tip1:I = 0x7f131704

.field public static final cs_640_ocrresult_tip2:I = 0x7f131705

.field public static final cs_640_pager_orientation:I = 0x7f131706

.field public static final cs_640_pager_size:I = 0x7f131707

.field public static final cs_640_password_settings:I = 0x7f131708

.field public static final cs_640_pdf_size:I = 0x7f131709

.field public static final cs_640_pdf_size01:I = 0x7f13170a

.field public static final cs_640_pdf_size02:I = 0x7f13170b

.field public static final cs_640_pdf_size03:I = 0x7f13170c

.field public static final cs_640_pixel_eraser:I = 0x7f13170d

.field public static final cs_640_redo:I = 0x7f13170e

.field public static final cs_640_remove_pdf_encryption:I = 0x7f13170f

.field public static final cs_640_remove_watermark_prompt:I = 0x7f131710

.field public static final cs_640_remove_watermark_prompt_bold:I = 0x7f131711

.field public static final cs_640_send_to_friend:I = 0x7f131712

.field public static final cs_640_send_wxmp:I = 0x7f131713

.field public static final cs_640_share_not_support:I = 0x7f131714

.field public static final cs_640_share_setting:I = 0x7f131715

.field public static final cs_640_stroke_eraser:I = 0x7f131716

.field public static final cs_640_stroke_eraser_intro:I = 0x7f131717

.field public static final cs_640_twitter:I = 0x7f131718

.field public static final cs_640_undo:I = 0x7f131719

.field public static final cs_640_usergrowth_01:I = 0x7f13171a

.field public static final cs_640_usergrowth_02:I = 0x7f13171b

.field public static final cs_640_usergrowth_03:I = 0x7f13171c

.field public static final cs_640_usergrowth_04:I = 0x7f13171d

.field public static final cs_640_usergrowth_05:I = 0x7f13171e

.field public static final cs_640_usergrowth_06:I = 0x7f13171f

.field public static final cs_640_usergrowth_07:I = 0x7f131720

.field public static final cs_640_usergrowth_08:I = 0x7f131721

.field public static final cs_640_usergrowth_09:I = 0x7f131722

.field public static final cs_640_usergrowth_10:I = 0x7f131723

.field public static final cs_640_usergrowth_11:I = 0x7f131724

.field public static final cs_640_usergrowth_12:I = 0x7f131725

.field public static final cs_640_usergrowth_13:I = 0x7f131726

.field public static final cs_640_usergrowth_14:I = 0x7f131727

.field public static final cs_640_usergrowth_15:I = 0x7f131728

.field public static final cs_640_usergrowth_16:I = 0x7f131729

.field public static final cs_640_usergrowth_17:I = 0x7f13172a

.field public static final cs_640_usergrowth_18:I = 0x7f13172b

.field public static final cs_640_usergrowth_19:I = 0x7f13172c

.field public static final cs_640_usergrowth_21:I = 0x7f13172d

.field public static final cs_640_usergrowth_22:I = 0x7f13172e

.field public static final cs_640_usergrowth_23:I = 0x7f13172f

.field public static final cs_640_usergrowth_24:I = 0x7f131730

.field public static final cs_640_usergrowth_26:I = 0x7f131731

.field public static final cs_640_usergrowth_27:I = 0x7f131732

.field public static final cs_640_usergrowth_28:I = 0x7f131733

.field public static final cs_640_usergrowth_29:I = 0x7f131734

.field public static final cs_640_usergrowth_30:I = 0x7f131735

.field public static final cs_640_usergrowth_31:I = 0x7f131736

.field public static final cs_640_usergrowth_32:I = 0x7f131737

.field public static final cs_640_usergrowth_33:I = 0x7f131738

.field public static final cs_640_vk:I = 0x7f131739

.field public static final cs_640_wendang_1:I = 0x7f13173a

.field public static final cs_640_wendang_10:I = 0x7f13173b

.field public static final cs_640_wendang_11:I = 0x7f13173c

.field public static final cs_640_wendang_12:I = 0x7f13173d

.field public static final cs_640_wendang_13:I = 0x7f13173e

.field public static final cs_640_wendang_14:I = 0x7f13173f

.field public static final cs_640_wendang_15:I = 0x7f131740

.field public static final cs_640_wendang_16:I = 0x7f131741

.field public static final cs_640_wendang_17:I = 0x7f131742

.field public static final cs_640_wendang_18:I = 0x7f131743

.field public static final cs_640_wendang_19:I = 0x7f131744

.field public static final cs_640_wendang_2:I = 0x7f131745

.field public static final cs_640_wendang_20:I = 0x7f131746

.field public static final cs_640_wendang_21:I = 0x7f131747

.field public static final cs_640_wendang_22:I = 0x7f131748

.field public static final cs_640_wendang_23:I = 0x7f131749

.field public static final cs_640_wendang_24:I = 0x7f13174a

.field public static final cs_640_wendang_25:I = 0x7f13174b

.field public static final cs_640_wendang_26:I = 0x7f13174c

.field public static final cs_640_wendang_3:I = 0x7f13174d

.field public static final cs_640_wendang_4:I = 0x7f13174e

.field public static final cs_640_wendang_5:I = 0x7f13174f

.field public static final cs_640_wendang_6:I = 0x7f131750

.field public static final cs_640_wendang_7:I = 0x7f131751

.field public static final cs_640_wendang_8:I = 0x7f131752

.field public static final cs_640_wendang_9:I = 0x7f131753

.field public static final cs_640_wxmp_tip:I = 0x7f131754

.field public static final cs_641_all_doc:I = 0x7f131755

.field public static final cs_641_bank_01:I = 0x7f131756

.field public static final cs_641_bank_02:I = 0x7f131757

.field public static final cs_641_bank_03:I = 0x7f131758

.field public static final cs_641_bank_04:I = 0x7f131759

.field public static final cs_641_bank_05:I = 0x7f13175a

.field public static final cs_641_bank_06:I = 0x7f13175b

.field public static final cs_641_bank_07:I = 0x7f13175c

.field public static final cs_641_bank_08:I = 0x7f13175d

.field public static final cs_641_bank_09:I = 0x7f13175e

.field public static final cs_641_bank_10:I = 0x7f13175f

.field public static final cs_641_bank_11:I = 0x7f131760

.field public static final cs_641_bank_12:I = 0x7f131761

.field public static final cs_641_bank_13:I = 0x7f131762

.field public static final cs_641_bank_14:I = 0x7f131763

.field public static final cs_641_bank_15:I = 0x7f131764

.field public static final cs_641_bank_16:I = 0x7f131765

.field public static final cs_641_bank_17:I = 0x7f131766

.field public static final cs_641_bank_18:I = 0x7f131767

.field public static final cs_641_bank_19:I = 0x7f131768

.field public static final cs_641_bank_20:I = 0x7f131769

.field public static final cs_641_bank_21:I = 0x7f13176a

.field public static final cs_641_bank_22:I = 0x7f13176b

.field public static final cs_641_bank_23:I = 0x7f13176c

.field public static final cs_641_bank_24:I = 0x7f13176d

.field public static final cs_641_bank_25:I = 0x7f13176e

.field public static final cs_641_bank_26:I = 0x7f13176f

.field public static final cs_641_bank_27:I = 0x7f131770

.field public static final cs_641_bank_28:I = 0x7f131771

.field public static final cs_641_bank_29:I = 0x7f131772

.field public static final cs_641_bank_30:I = 0x7f131773

.field public static final cs_641_bank_31:I = 0x7f131774

.field public static final cs_641_bank_32:I = 0x7f131775

.field public static final cs_641_bank_33:I = 0x7f131776

.field public static final cs_641_bank_34:I = 0x7f131777

.field public static final cs_641_button_ask_ai:I = 0x7f131778

.field public static final cs_641_classify_background:I = 0x7f131779

.field public static final cs_641_classify_bill:I = 0x7f13177a

.field public static final cs_641_classify_cases:I = 0x7f13177b

.field public static final cs_641_classify_govdoc:I = 0x7f13177c

.field public static final cs_641_classify_homewrok:I = 0x7f13177d

.field public static final cs_641_classify_landbill:I = 0x7f13177e

.field public static final cs_641_classify_legaldoc:I = 0x7f13177f

.field public static final cs_641_classify_orderbill:I = 0x7f131780

.field public static final cs_641_classify_others:I = 0x7f131781

.field public static final cs_641_classify_policybill:I = 0x7f131782

.field public static final cs_641_clipbord:I = 0x7f131783

.field public static final cs_641_clipbord_open:I = 0x7f131784

.field public static final cs_641_clipbord_open_link:I = 0x7f131785

.field public static final cs_641_doc_open_title:I = 0x7f131786

.field public static final cs_641_doc_openl_content:I = 0x7f131787

.field public static final cs_641_doc_personal_content:I = 0x7f131788

.field public static final cs_641_doc_personal_title:I = 0x7f131789

.field public static final cs_641_email_content_content:I = 0x7f13178a

.field public static final cs_641_email_content_title:I = 0x7f13178b

.field public static final cs_641_excel_2:I = 0x7f13178c

.field public static final cs_641_from_friends:I = 0x7f13178d

.field public static final cs_641_from_friends_default:I = 0x7f13178e

.field public static final cs_641_h5game_1:I = 0x7f13178f

.field public static final cs_641_h5game_10:I = 0x7f131790

.field public static final cs_641_h5game_2:I = 0x7f131791

.field public static final cs_641_h5game_3:I = 0x7f131792

.field public static final cs_641_h5game_4:I = 0x7f131793

.field public static final cs_641_h5game_5:I = 0x7f131794

.field public static final cs_641_h5game_6:I = 0x7f131795

.field public static final cs_641_h5game_7:I = 0x7f131796

.field public static final cs_641_h5game_8:I = 0x7f131797

.field public static final cs_641_h5game_9:I = 0x7f131798

.field public static final cs_641_link_public:I = 0x7f131799

.field public static final cs_641_link_public_pop:I = 0x7f13179a

.field public static final cs_641_log_1:I = 0x7f13179b

.field public static final cs_641_log_10:I = 0x7f13179c

.field public static final cs_641_log_2:I = 0x7f13179d

.field public static final cs_641_log_3:I = 0x7f13179e

.field public static final cs_641_log_4:I = 0x7f13179f

.field public static final cs_641_log_5:I = 0x7f1317a0

.field public static final cs_641_log_6:I = 0x7f1317a1

.field public static final cs_641_log_7:I = 0x7f1317a2

.field public static final cs_641_log_8:I = 0x7f1317a3

.field public static final cs_641_log_9:I = 0x7f1317a4

.field public static final cs_641_open_btn:I = 0x7f1317a5

.field public static final cs_641_open_excel:I = 0x7f1317a6

.field public static final cs_641_open_excel_open:I = 0x7f1317a7

.field public static final cs_641_open_ppt:I = 0x7f1317a8

.field public static final cs_641_open_ppt_open:I = 0x7f1317a9

.field public static final cs_641_open_word:I = 0x7f1317aa

.field public static final cs_641_open_word_open:I = 0x7f1317ab

.field public static final cs_641_save_to_cs_1:I = 0x7f1317ac

.field public static final cs_641_see_all:I = 0x7f1317ad

.field public static final cs_641_send_to:I = 0x7f1317ae

.field public static final cs_642_bluetooth_guide_01:I = 0x7f1317af

.field public static final cs_642_bluetooth_guide_02:I = 0x7f1317b0

.field public static final cs_642_bluetooth_guide_03:I = 0x7f1317b1

.field public static final cs_642_clarity_1:I = 0x7f1317b2

.field public static final cs_642_clarity_2:I = 0x7f1317b3

.field public static final cs_642_clarity_3:I = 0x7f1317b4

.field public static final cs_642_clarity_4:I = 0x7f1317b5

.field public static final cs_642_clarity_5:I = 0x7f1317b6

.field public static final cs_642_clarity_agree:I = 0x7f1317b7

.field public static final cs_642_clarity_button:I = 0x7f1317b8

.field public static final cs_642_clarity_content:I = 0x7f1317b9

.field public static final cs_642_clarity_restore:I = 0x7f1317ba

.field public static final cs_642_clarity_title:I = 0x7f1317bb

.field public static final cs_642_earse_01:I = 0x7f1317bc

.field public static final cs_642_earse_02:I = 0x7f1317bd

.field public static final cs_642_earse_03:I = 0x7f1317be

.field public static final cs_642_earse_04:I = 0x7f1317bf

.field public static final cs_642_earse_05:I = 0x7f1317c0

.field public static final cs_642_guidesale_01:I = 0x7f1317c1

.field public static final cs_642_guidesale_02:I = 0x7f1317c2

.field public static final cs_642_guidesale_03:I = 0x7f1317c3

.field public static final cs_642_guidesale_04:I = 0x7f1317c4

.field public static final cs_642_guidesale_05:I = 0x7f1317c5

.field public static final cs_642_guidesale_06:I = 0x7f1317c6

.field public static final cs_642_guidesale_07:I = 0x7f1317c7

.field public static final cs_642_guidesale_08:I = 0x7f1317c8

.field public static final cs_642_guidesale_09:I = 0x7f1317c9

.field public static final cs_642_guidesale_10:I = 0x7f1317ca

.field public static final cs_642_guidesale_11:I = 0x7f1317cb

.field public static final cs_642_guidesale_12:I = 0x7f1317cc

.field public static final cs_642_guidesale_13:I = 0x7f1317cd

.field public static final cs_642_ocr_01:I = 0x7f1317ce

.field public static final cs_642_refer_banner_01:I = 0x7f1317cf

.field public static final cs_642_refer_popup_01:I = 0x7f1317d0

.field public static final cs_642_refer_popup_02:I = 0x7f1317d1

.field public static final cs_642_refer_popup_03:I = 0x7f1317d2

.field public static final cs_642_refer_poster_01:I = 0x7f1317d3

.field public static final cs_642_refer_poster_02:I = 0x7f1317d4

.field public static final cs_642_refer_toast_01:I = 0x7f1317d5

.field public static final cs_642_refer_toast_02:I = 0x7f1317d6

.field public static final cs_643_camera_edit_retake:I = 0x7f1317d7

.field public static final cs_643_camera_retake:I = 0x7f1317d8

.field public static final cs_643_me_crop_each_time:I = 0x7f1317d9

.field public static final cs_643_points_01:I = 0x7f1317da

.field public static final cs_643_share_pdf_doc:I = 0x7f1317db

.field public static final cs_643_share_pdf_link:I = 0x7f1317dc

.field public static final cs_643_wechat_01:I = 0x7f1317dd

.field public static final cs_643_wechat_02:I = 0x7f1317de

.field public static final cs_643_wechat_03:I = 0x7f1317df

.field public static final cs_643_wechat_04:I = 0x7f1317e0

.field public static final cs_643_wechat_05:I = 0x7f1317e1

.field public static final cs_643_worldwide_sharedone01:I = 0x7f1317e2

.field public static final cs_643_worldwide_sharedone02:I = 0x7f1317e3

.field public static final cs_643_worldwide_sharedone03:I = 0x7f1317e4

.field public static final cs_643_worldwide_sharedone04:I = 0x7f1317e5

.field public static final cs_643_worldwide_sharedone05:I = 0x7f1317e6

.field public static final cs_643_worldwide_sharedone06:I = 0x7f1317e7

.field public static final cs_643_worldwide_sharedone07:I = 0x7f1317e8

.field public static final cs_643_worldwide_sharedone08:I = 0x7f1317e9

.field public static final cs_644_ai_01:I = 0x7f1317ea

.field public static final cs_644_barcode01:I = 0x7f1317eb

.field public static final cs_644_mifeng_popup_close:I = 0x7f1317ec

.field public static final cs_644_mifeng_popup_download:I = 0x7f1317ed

.field public static final cs_644_mifeng_popup_goon:I = 0x7f1317ee

.field public static final cs_644_mifeng_popup_title:I = 0x7f1317ef

.field public static final cs_644_mifeng_popup_title1:I = 0x7f1317f0

.field public static final cs_644_mifeng_popup_title2:I = 0x7f1317f1

.field public static final cs_644_mifeng_popup_title3:I = 0x7f1317f2

.field public static final cs_644_mifeng_popup_title4:I = 0x7f1317f3

.field public static final cs_644_mifeng_popup_title5:I = 0x7f1317f4

.field public static final cs_644_mifeng_recommend_1:I = 0x7f1317f5

.field public static final cs_644_mifeng_recommend_2:I = 0x7f1317f6

.field public static final cs_644_mifeng_recommend_3:I = 0x7f1317f7

.field public static final cs_644_mifeng_recommend_4:I = 0x7f1317f8

.field public static final cs_644_mifeng_recommend_5:I = 0x7f1317f9

.field public static final cs_644_mifeng_shot_popup:I = 0x7f1317fa

.field public static final cs_644_mifeng_shot_popup_download:I = 0x7f1317fb

.field public static final cs_644_mifeng_shot_popup_text:I = 0x7f1317fc

.field public static final cs_644_mifeng_toword_print:I = 0x7f1317fd

.field public static final cs_644_mifeng_toword_wrong:I = 0x7f1317fe

.field public static final cs_644_name_01:I = 0x7f1317ff

.field public static final cs_644_name_02:I = 0x7f131800

.field public static final cs_644_name_03:I = 0x7f131801

.field public static final cs_644_name_04:I = 0x7f131802

.field public static final cs_644_name_05:I = 0x7f131803

.field public static final cs_644_skip_demo:I = 0x7f131804

.field public static final cs_644_whiteboard_1:I = 0x7f131805

.field public static final cs_644_whiteboard_2:I = 0x7f131806

.field public static final cs_644_whiteboard_3:I = 0x7f131807

.field public static final cs_644_whiteboard_4:I = 0x7f131808

.field public static final cs_644_whiteboard_5:I = 0x7f131809

.field public static final cs_644_workflow_1:I = 0x7f13180a

.field public static final cs_644_workflow_10:I = 0x7f13180b

.field public static final cs_644_workflow_11:I = 0x7f13180c

.field public static final cs_644_workflow_12:I = 0x7f13180d

.field public static final cs_644_workflow_13:I = 0x7f13180e

.field public static final cs_644_workflow_14:I = 0x7f13180f

.field public static final cs_644_workflow_15:I = 0x7f131810

.field public static final cs_644_workflow_16:I = 0x7f131811

.field public static final cs_644_workflow_17:I = 0x7f131812

.field public static final cs_644_workflow_18:I = 0x7f131813

.field public static final cs_644_workflow_19:I = 0x7f131814

.field public static final cs_644_workflow_2:I = 0x7f131815

.field public static final cs_644_workflow_20:I = 0x7f131816

.field public static final cs_644_workflow_21:I = 0x7f131817

.field public static final cs_644_workflow_22:I = 0x7f131818

.field public static final cs_644_workflow_23:I = 0x7f131819

.field public static final cs_644_workflow_24:I = 0x7f13181a

.field public static final cs_644_workflow_25:I = 0x7f13181b

.field public static final cs_644_workflow_26:I = 0x7f13181c

.field public static final cs_644_workflow_27:I = 0x7f13181d

.field public static final cs_644_workflow_28:I = 0x7f13181e

.field public static final cs_644_workflow_29:I = 0x7f13181f

.field public static final cs_644_workflow_3:I = 0x7f131820

.field public static final cs_644_workflow_30:I = 0x7f131821

.field public static final cs_644_workflow_31:I = 0x7f131822

.field public static final cs_644_workflow_4:I = 0x7f131823

.field public static final cs_644_workflow_5:I = 0x7f131824

.field public static final cs_644_workflow_6:I = 0x7f131825

.field public static final cs_644_workflow_7:I = 0x7f131826

.field public static final cs_644_workflow_8:I = 0x7f131827

.field public static final cs_644_workflow_9:I = 0x7f131828

.field public static final cs_645_count_01:I = 0x7f131829

.field public static final cs_645_count_02:I = 0x7f13182a

.field public static final cs_645_count_03:I = 0x7f13182b

.field public static final cs_645_count_04:I = 0x7f13182c

.field public static final cs_645_count_05:I = 0x7f13182d

.field public static final cs_645_count_06:I = 0x7f13182e

.field public static final cs_645_count_07:I = 0x7f13182f

.field public static final cs_645_count_08:I = 0x7f131830

.field public static final cs_645_count_09:I = 0x7f131831

.field public static final cs_645_count_10:I = 0x7f131832

.field public static final cs_645_count_11:I = 0x7f131833

.field public static final cs_645_count_12:I = 0x7f131834

.field public static final cs_645_count_13:I = 0x7f131835

.field public static final cs_645_count_14:I = 0x7f131836

.field public static final cs_645_count_15:I = 0x7f131837

.field public static final cs_645_count_16:I = 0x7f131838

.field public static final cs_645_count_17:I = 0x7f131839

.field public static final cs_645_count_18:I = 0x7f13183a

.field public static final cs_645_count_19:I = 0x7f13183b

.field public static final cs_645_count_20:I = 0x7f13183c

.field public static final cs_645_count_21:I = 0x7f13183d

.field public static final cs_645_count_22:I = 0x7f13183e

.field public static final cs_645_count_23:I = 0x7f13183f

.field public static final cs_645_count_24:I = 0x7f131840

.field public static final cs_645_recall_01:I = 0x7f131841

.field public static final cs_645_recall_02:I = 0x7f131842

.field public static final cs_645_recall_03:I = 0x7f131843

.field public static final cs_645_recall_04:I = 0x7f131844

.field public static final cs_645_recall_05:I = 0x7f131845

.field public static final cs_645_recall_06:I = 0x7f131846

.field public static final cs_645_recall_07:I = 0x7f131847

.field public static final cs_645_recall_08:I = 0x7f131848

.field public static final cs_645_recall_09:I = 0x7f131849

.field public static final cs_645_recall_10:I = 0x7f13184a

.field public static final cs_645_recall_11:I = 0x7f13184b

.field public static final cs_645_recall_12:I = 0x7f13184c

.field public static final cs_645_recall_13:I = 0x7f13184d

.field public static final cs_645_stripe_01:I = 0x7f13184e

.field public static final cs_645_stripe_02:I = 0x7f13184f

.field public static final cs_645_stripe_03:I = 0x7f131850

.field public static final cs_645_stripe_04:I = 0x7f131851

.field public static final cs_645_whiteboard_6:I = 0x7f131852

.field public static final cs_645_whiteboard_7:I = 0x7f131853

.field public static final cs_645_whiteboard_8:I = 0x7f131854

.field public static final cs_645_widget_01:I = 0x7f131855

.field public static final cs_645_widget_02:I = 0x7f131856

.field public static final cs_645_widget_03:I = 0x7f131857

.field public static final cs_645_widget_04:I = 0x7f131858

.field public static final cs_645_widget_05:I = 0x7f131859

.field public static final cs_645_widget_06:I = 0x7f13185a

.field public static final cs_645_widget_07:I = 0x7f13185b

.field public static final cs_645_widget_08:I = 0x7f13185c

.field public static final cs_645_widget_09:I = 0x7f13185d

.field public static final cs_645_widget_10:I = 0x7f13185e

.field public static final cs_645_widget_11:I = 0x7f13185f

.field public static final cs_645_widget_12:I = 0x7f131860

.field public static final cs_645_widget_13:I = 0x7f131861

.field public static final cs_645_widget_14:I = 0x7f131862

.field public static final cs_645_widget_all_files:I = 0x7f131863

.field public static final cs_645_widget_batch_scan:I = 0x7f131864

.field public static final cs_645_widget_recent_files:I = 0x7f131865

.field public static final cs_645_widget_single_scan:I = 0x7f131866

.field public static final cs_646_adfeedback_1:I = 0x7f131867

.field public static final cs_646_adfeedback_3:I = 0x7f131868

.field public static final cs_646_adfeedback_4:I = 0x7f131869

.field public static final cs_646_banks_invoice_body:I = 0x7f13186a

.field public static final cs_646_banks_invoice_title:I = 0x7f13186b

.field public static final cs_646_camera_count_title:I = 0x7f13186c

.field public static final cs_646_camera_to_excel_description:I = 0x7f13186d

.field public static final cs_646_camera_to_excel_title:I = 0x7f13186e

.field public static final cs_646_camera_to_word_description:I = 0x7f13186f

.field public static final cs_646_camera_to_word_title:I = 0x7f131870

.field public static final cs_646_doc_merge_more:I = 0x7f131871

.field public static final cs_646_invoice_01:I = 0x7f131872

.field public static final cs_646_invoice_02:I = 0x7f131873

.field public static final cs_646_invoice_03:I = 0x7f131874

.field public static final cs_646_invoice_05:I = 0x7f131875

.field public static final cs_646_invoice_06:I = 0x7f131876

.field public static final cs_646_invoice_07:I = 0x7f131877

.field public static final cs_646_invoice_08:I = 0x7f131878

.field public static final cs_646_invoice_09:I = 0x7f131879

.field public static final cs_646_invoice_11:I = 0x7f13187a

.field public static final cs_646_invoice_12:I = 0x7f13187b

.field public static final cs_646_invoice_13:I = 0x7f13187c

.field public static final cs_646_invoice_14:I = 0x7f13187d

.field public static final cs_646_invoice_18:I = 0x7f13187e

.field public static final cs_646_invoice_20:I = 0x7f13187f

.field public static final cs_646_invoice_21:I = 0x7f131880

.field public static final cs_646_invoice_22:I = 0x7f131881

.field public static final cs_646_invoice_26:I = 0x7f131882

.field public static final cs_646_invoice_27:I = 0x7f131883

.field public static final cs_646_invoice_33:I = 0x7f131884

.field public static final cs_646_invoice_34:I = 0x7f131885

.field public static final cs_646_invoice_35:I = 0x7f131886

.field public static final cs_646_invoice_36:I = 0x7f131887

.field public static final cs_646_invoice_37:I = 0x7f131888

.field public static final cs_646_invoice_38:I = 0x7f131889

.field public static final cs_646_invoice_40:I = 0x7f13188a

.field public static final cs_646_invoice_42:I = 0x7f13188b

.field public static final cs_646_notification_1:I = 0x7f13188c

.field public static final cs_646_notification_2:I = 0x7f13188d

.field public static final cs_646_scan_book_more:I = 0x7f13188e

.field public static final cs_646_scan_id_description:I = 0x7f13188f

.field public static final cs_646_share_45:I = 0x7f131890

.field public static final cs_646_share_46:I = 0x7f131891

.field public static final cs_646_share_47:I = 0x7f131892

.field public static final cs_646_share_48:I = 0x7f131893

.field public static final cs_646_share_49:I = 0x7f131894

.field public static final cs_646_sign_ddl:I = 0x7f131895

.field public static final cs_646_sign_delete_contact:I = 0x7f131896

.field public static final cs_646_sign_delete_info1:I = 0x7f131897

.field public static final cs_646_sign_delete_info2:I = 0x7f131898

.field public static final cs_646_sign_discard_reminder:I = 0x7f131899

.field public static final cs_646_sign_handwrite:I = 0x7f13189a

.field public static final cs_646_sign_import:I = 0x7f13189b

.field public static final cs_646_sign_import_logo:I = 0x7f13189c

.field public static final cs_646_sign_import_pagingseal:I = 0x7f13189d

.field public static final cs_646_sign_import_stamp:I = 0x7f13189e

.field public static final cs_646_sign_logo_required:I = 0x7f13189f

.field public static final cs_646_sign_pagingseal_required:I = 0x7f1318a0

.field public static final cs_646_sign_reinvite:I = 0x7f1318a1

.field public static final cs_646_sign_scan:I = 0x7f1318a2

.field public static final cs_646_sign_scan_logo:I = 0x7f1318a3

.field public static final cs_646_sign_scan_pagingseal:I = 0x7f1318a4

.field public static final cs_646_sign_scan_stamp:I = 0x7f1318a5

.field public static final cs_646_sign_sent_reminder:I = 0x7f1318a6

.field public static final cs_646_sign_signature_options:I = 0x7f1318a7

.field public static final cs_646_sign_signature_required:I = 0x7f1318a8

.field public static final cs_646_sign_signature_time:I = 0x7f1318a9

.field public static final cs_646_sign_stamp_required:I = 0x7f1318aa

.field public static final cs_646_start_scan_home:I = 0x7f1318ab

.field public static final cs_647_adrule_01:I = 0x7f1318ac

.field public static final cs_647_adrule_02:I = 0x7f1318ad

.field public static final cs_647_adrule_03:I = 0x7f1318ae

.field public static final cs_647_adrule_04:I = 0x7f1318af

.field public static final cs_647_adrule_06:I = 0x7f1318b0

.field public static final cs_647_bank_safe_tip:I = 0x7f1318b1

.field public static final cs_647_datu:I = 0x7f1318b2

.field public static final cs_647_datu_remind:I = 0x7f1318b3

.field public static final cs_647_gongge_remind:I = 0x7f1318b4

.field public static final cs_647_icon3:I = 0x7f1318b5

.field public static final cs_647_ppl_01:I = 0x7f1318b6

.field public static final cs_647_ppl_02:I = 0x7f1318b7

.field public static final cs_647_quizgo_benefit1:I = 0x7f1318b8

.field public static final cs_647_quizgo_benefit2:I = 0x7f1318b9

.field public static final cs_647_quizgo_benefit3:I = 0x7f1318ba

.field public static final cs_647_quizgo_benefit4:I = 0x7f1318bb

.field public static final cs_647_quizgo_benefit5:I = 0x7f1318bc

.field public static final cs_647_quizgo_benefit6:I = 0x7f1318bd

.field public static final cs_647_quizgo_benefit7:I = 0x7f1318be

.field public static final cs_647_quizgo_benefit8:I = 0x7f1318bf

.field public static final cs_647_quizgo_button:I = 0x7f1318c0

.field public static final cs_647_quizgo_download:I = 0x7f1318c1

.field public static final cs_647_quizgo_title1:I = 0x7f1318c2

.field public static final cs_647_quizgo_title2:I = 0x7f1318c3

.field public static final cs_647_quizgo_title3:I = 0x7f1318c4

.field public static final cs_647_suolue:I = 0x7f1318c5

.field public static final cs_647_web2pdf_2pdf:I = 0x7f1318c6

.field public static final cs_647_web2pdf_2pic:I = 0x7f1318c7

.field public static final cs_647_web2pdf_btn1:I = 0x7f1318c8

.field public static final cs_647_web2pdf_btn3:I = 0x7f1318c9

.field public static final cs_647_web2pdf_btn4:I = 0x7f1318ca

.field public static final cs_647_web2pdf_btn5:I = 0x7f1318cb

.field public static final cs_647_web2pdf_favorites:I = 0x7f1318cc

.field public static final cs_647_web2pdf_tag:I = 0x7f1318cd

.field public static final cs_647_web2pdf_title1:I = 0x7f1318ce

.field public static final cs_647_web2pdf_title2:I = 0x7f1318cf

.field public static final cs_647_web2pdf_title3:I = 0x7f1318d0

.field public static final cs_647_web2pdf_toast1:I = 0x7f1318d1

.field public static final cs_647_web2pdf_toast2:I = 0x7f1318d2

.field public static final cs_647_web2pdf_toast3:I = 0x7f1318d3

.field public static final cs_647_web2pdf_toast4:I = 0x7f1318d4

.field public static final cs_647_web2pdf_toast5:I = 0x7f1318d5

.field public static final cs_647_word_01:I = 0x7f1318d6

.field public static final cs_647_word_02:I = 0x7f1318d7

.field public static final cs_647_word_03:I = 0x7f1318d8

.field public static final cs_647_word_04:I = 0x7f1318d9

.field public static final cs_647_word_05:I = 0x7f1318da

.field public static final cs_647_word_06:I = 0x7f1318db

.field public static final cs_648_add_count_mode:I = 0x7f1318dc

.field public static final cs_648_add_count_mode02:I = 0x7f1318dd

.field public static final cs_648_add_photo:I = 0x7f1318de

.field public static final cs_648_add_photo02:I = 0x7f1318df

.field public static final cs_648_default_description:I = 0x7f1318e0

.field public static final cs_648_defult_01:I = 0x7f1318e1

.field public static final cs_648_defult_02:I = 0x7f1318e2

.field public static final cs_648_defult_03:I = 0x7f1318e3

.field public static final cs_648_invoice_export_excel:I = 0x7f1318e4

.field public static final cs_648_invoice_export_pdf:I = 0x7f1318e5

.field public static final cs_648_office_saveaspdf:I = 0x7f1318e6

.field public static final cs_648_pdf_done_03:I = 0x7f1318e7

.field public static final cs_648_pdf_vipshare_01:I = 0x7f1318e8

.field public static final cs_648_pdf_vipshare_02:I = 0x7f1318e9

.field public static final cs_648_pdf_waiting_01:I = 0x7f1318ea

.field public static final cs_648_pdf_waiting_03:I = 0x7f1318eb

.field public static final cs_648_pdf_waiting_06:I = 0x7f1318ec

.field public static final cs_648_print_icon:I = 0x7f1318ed

.field public static final cs_648_retention_01:I = 0x7f1318ee

.field public static final cs_648_retention_02:I = 0x7f1318ef

.field public static final cs_648_retention_03:I = 0x7f1318f0

.field public static final cs_648_retention_04:I = 0x7f1318f1

.field public static final cs_648_retention_05:I = 0x7f1318f2

.field public static final cs_648_retention_07:I = 0x7f1318f3

.field public static final cs_648_retention_08:I = 0x7f1318f4

.field public static final cs_648_retention_09:I = 0x7f1318f5

.field public static final cs_648_retention_10:I = 0x7f1318f6

.field public static final cs_648_retention_11:I = 0x7f1318f7

.field public static final cs_648_retention_12:I = 0x7f1318f8

.field public static final cs_648_retention_13:I = 0x7f1318f9

.field public static final cs_648_retention_14:I = 0x7f1318fa

.field public static final cs_648_single_tip0:I = 0x7f1318fb

.field public static final cs_648_single_tip1:I = 0x7f1318fc

.field public static final cs_648_translate_1:I = 0x7f1318fd

.field public static final cs_648_translate_2:I = 0x7f1318fe

.field public static final cs_648_translate_3:I = 0x7f1318ff

.field public static final cs_648_translate_4:I = 0x7f131900

.field public static final cs_648_translate_5:I = 0x7f131901

.field public static final cs_648_worldwide_1:I = 0x7f131902

.field public static final cs_649_drive_photo_tip:I = 0x7f131903

.field public static final cs_649_drive_tip:I = 0x7f131904

.field public static final cs_649_feedback_1:I = 0x7f131905

.field public static final cs_649_flacs_1:I = 0x7f131906

.field public static final cs_649_flacs_10:I = 0x7f131907

.field public static final cs_649_flacs_11:I = 0x7f131908

.field public static final cs_649_flacs_12:I = 0x7f131909

.field public static final cs_649_flacs_13:I = 0x7f13190a

.field public static final cs_649_flacs_14:I = 0x7f13190b

.field public static final cs_649_flacs_15:I = 0x7f13190c

.field public static final cs_649_flacs_16:I = 0x7f13190d

.field public static final cs_649_flacs_17:I = 0x7f13190e

.field public static final cs_649_flacs_18:I = 0x7f13190f

.field public static final cs_649_flacs_19:I = 0x7f131910

.field public static final cs_649_flacs_2:I = 0x7f131911

.field public static final cs_649_flacs_20:I = 0x7f131912

.field public static final cs_649_flacs_21:I = 0x7f131913

.field public static final cs_649_flacs_22:I = 0x7f131914

.field public static final cs_649_flacs_23:I = 0x7f131915

.field public static final cs_649_flacs_24:I = 0x7f131916

.field public static final cs_649_flacs_25:I = 0x7f131917

.field public static final cs_649_flacs_26:I = 0x7f131918

.field public static final cs_649_flacs_27:I = 0x7f131919

.field public static final cs_649_flacs_28:I = 0x7f13191a

.field public static final cs_649_flacs_29:I = 0x7f13191b

.field public static final cs_649_flacs_3:I = 0x7f13191c

.field public static final cs_649_flacs_30:I = 0x7f13191d

.field public static final cs_649_flacs_31:I = 0x7f13191e

.field public static final cs_649_flacs_32:I = 0x7f13191f

.field public static final cs_649_flacs_33:I = 0x7f131920

.field public static final cs_649_flacs_34:I = 0x7f131921

.field public static final cs_649_flacs_35:I = 0x7f131922

.field public static final cs_649_flacs_36:I = 0x7f131923

.field public static final cs_649_flacs_4:I = 0x7f131924

.field public static final cs_649_flacs_5:I = 0x7f131925

.field public static final cs_649_flacs_6:I = 0x7f131926

.field public static final cs_649_flacs_7:I = 0x7f131927

.field public static final cs_649_flacs_8:I = 0x7f131928

.field public static final cs_649_flacs_9:I = 0x7f131929

.field public static final cs_649_flacs_info:I = 0x7f13192a

.field public static final cs_649_flacs_no_thanks:I = 0x7f13192b

.field public static final cs_649_idcard_btn:I = 0x7f13192c

.field public static final cs_649_notification_1:I = 0x7f13192d

.field public static final cs_649_passport_tip1:I = 0x7f13192e

.field public static final cs_649_passport_tip2:I = 0x7f13192f

.field public static final cs_649_passport_tip3:I = 0x7f131930

.field public static final cs_649_passport_tip4:I = 0x7f131931

.field public static final cs_649_print_1:I = 0x7f131932

.field public static final cs_649_print_10:I = 0x7f131933

.field public static final cs_649_print_100:I = 0x7f131934

.field public static final cs_649_print_101:I = 0x7f131935

.field public static final cs_649_print_10_1:I = 0x7f131936

.field public static final cs_649_print_11:I = 0x7f131937

.field public static final cs_649_print_12:I = 0x7f131938

.field public static final cs_649_print_13:I = 0x7f131939

.field public static final cs_649_print_14:I = 0x7f13193a

.field public static final cs_649_print_15:I = 0x7f13193b

.field public static final cs_649_print_16:I = 0x7f13193c

.field public static final cs_649_print_17:I = 0x7f13193d

.field public static final cs_649_print_18:I = 0x7f13193e

.field public static final cs_649_print_19:I = 0x7f13193f

.field public static final cs_649_print_2:I = 0x7f131940

.field public static final cs_649_print_20:I = 0x7f131941

.field public static final cs_649_print_21:I = 0x7f131942

.field public static final cs_649_print_22:I = 0x7f131943

.field public static final cs_649_print_23:I = 0x7f131944

.field public static final cs_649_print_24:I = 0x7f131945

.field public static final cs_649_print_25:I = 0x7f131946

.field public static final cs_649_print_26:I = 0x7f131947

.field public static final cs_649_print_27:I = 0x7f131948

.field public static final cs_649_print_28:I = 0x7f131949

.field public static final cs_649_print_29:I = 0x7f13194a

.field public static final cs_649_print_3:I = 0x7f13194b

.field public static final cs_649_print_30:I = 0x7f13194c

.field public static final cs_649_print_31:I = 0x7f13194d

.field public static final cs_649_print_32:I = 0x7f13194e

.field public static final cs_649_print_33:I = 0x7f13194f

.field public static final cs_649_print_34:I = 0x7f131950

.field public static final cs_649_print_35:I = 0x7f131951

.field public static final cs_649_print_36:I = 0x7f131952

.field public static final cs_649_print_37:I = 0x7f131953

.field public static final cs_649_print_38:I = 0x7f131954

.field public static final cs_649_print_39:I = 0x7f131955

.field public static final cs_649_print_4:I = 0x7f131956

.field public static final cs_649_print_40:I = 0x7f131957

.field public static final cs_649_print_41:I = 0x7f131958

.field public static final cs_649_print_42:I = 0x7f131959

.field public static final cs_649_print_43:I = 0x7f13195a

.field public static final cs_649_print_44:I = 0x7f13195b

.field public static final cs_649_print_45:I = 0x7f13195c

.field public static final cs_649_print_46:I = 0x7f13195d

.field public static final cs_649_print_47:I = 0x7f13195e

.field public static final cs_649_print_48:I = 0x7f13195f

.field public static final cs_649_print_49:I = 0x7f131960

.field public static final cs_649_print_5:I = 0x7f131961

.field public static final cs_649_print_50:I = 0x7f131962

.field public static final cs_649_print_51:I = 0x7f131963

.field public static final cs_649_print_52:I = 0x7f131964

.field public static final cs_649_print_53:I = 0x7f131965

.field public static final cs_649_print_54:I = 0x7f131966

.field public static final cs_649_print_55:I = 0x7f131967

.field public static final cs_649_print_56:I = 0x7f131968

.field public static final cs_649_print_57:I = 0x7f131969

.field public static final cs_649_print_58:I = 0x7f13196a

.field public static final cs_649_print_59:I = 0x7f13196b

.field public static final cs_649_print_6:I = 0x7f13196c

.field public static final cs_649_print_60:I = 0x7f13196d

.field public static final cs_649_print_61:I = 0x7f13196e

.field public static final cs_649_print_62:I = 0x7f13196f

.field public static final cs_649_print_63:I = 0x7f131970

.field public static final cs_649_print_64:I = 0x7f131971

.field public static final cs_649_print_65:I = 0x7f131972

.field public static final cs_649_print_66:I = 0x7f131973

.field public static final cs_649_print_67:I = 0x7f131974

.field public static final cs_649_print_68:I = 0x7f131975

.field public static final cs_649_print_69:I = 0x7f131976

.field public static final cs_649_print_7:I = 0x7f131977

.field public static final cs_649_print_70:I = 0x7f131978

.field public static final cs_649_print_71:I = 0x7f131979

.field public static final cs_649_print_72:I = 0x7f13197a

.field public static final cs_649_print_73:I = 0x7f13197b

.field public static final cs_649_print_74:I = 0x7f13197c

.field public static final cs_649_print_75:I = 0x7f13197d

.field public static final cs_649_print_76:I = 0x7f13197e

.field public static final cs_649_print_77:I = 0x7f13197f

.field public static final cs_649_print_78:I = 0x7f131980

.field public static final cs_649_print_79:I = 0x7f131981

.field public static final cs_649_print_8:I = 0x7f131982

.field public static final cs_649_print_81:I = 0x7f131983

.field public static final cs_649_print_82:I = 0x7f131984

.field public static final cs_649_print_83:I = 0x7f131985

.field public static final cs_649_print_84:I = 0x7f131986

.field public static final cs_649_print_85:I = 0x7f131987

.field public static final cs_649_print_86:I = 0x7f131988

.field public static final cs_649_print_87:I = 0x7f131989

.field public static final cs_649_print_88:I = 0x7f13198a

.field public static final cs_649_print_89:I = 0x7f13198b

.field public static final cs_649_print_9:I = 0x7f13198c

.field public static final cs_649_print_90:I = 0x7f13198d

.field public static final cs_649_print_91:I = 0x7f13198e

.field public static final cs_649_print_92:I = 0x7f13198f

.field public static final cs_649_print_93:I = 0x7f131990

.field public static final cs_649_print_94:I = 0x7f131991

.field public static final cs_649_print_95:I = 0x7f131992

.field public static final cs_649_print_96:I = 0x7f131993

.field public static final cs_649_print_97:I = 0x7f131994

.field public static final cs_649_print_98:I = 0x7f131995

.field public static final cs_649_print_99:I = 0x7f131996

.field public static final cs_649_quding_1:I = 0x7f131997

.field public static final cs_649_quding_2:I = 0x7f131998

.field public static final cs_649_quding_3:I = 0x7f131999

.field public static final cs_649_quding_4:I = 0x7f13199a

.field public static final cs_649_quding_5:I = 0x7f13199b

.field public static final cs_649_quding_6:I = 0x7f13199c

.field public static final cs_649_quding_7:I = 0x7f13199d

.field public static final cs_649_quding_8:I = 0x7f13199e

.field public static final cs_649_refer3_1:I = 0x7f13199f

.field public static final cs_649_refer3_10:I = 0x7f1319a0

.field public static final cs_649_refer3_11:I = 0x7f1319a1

.field public static final cs_649_refer3_12:I = 0x7f1319a2

.field public static final cs_649_refer3_13:I = 0x7f1319a3

.field public static final cs_649_refer3_2:I = 0x7f1319a4

.field public static final cs_649_refer3_3:I = 0x7f1319a5

.field public static final cs_649_refer3_4:I = 0x7f1319a6

.field public static final cs_649_refer3_5:I = 0x7f1319a7

.field public static final cs_649_refer3_6:I = 0x7f1319a8

.field public static final cs_649_refer3_7:I = 0x7f1319a9

.field public static final cs_649_refer3_8:I = 0x7f1319aa

.field public static final cs_649_refer3_9:I = 0x7f1319ab

.field public static final cs_649_share_print_tip:I = 0x7f1319ac

.field public static final cs_649_single_translate_char_limit:I = 0x7f1319ad

.field public static final cs_649_translate_2:I = 0x7f1319ae

.field public static final cs_649_upload_waitingpop:I = 0x7f1319af

.field public static final cs_650_ai_recommned:I = 0x7f1319b0

.field public static final cs_650_android14_authority:I = 0x7f1319b1

.field public static final cs_650_android14_authority_tip1:I = 0x7f1319b2

.field public static final cs_650_android14_authority_tip2:I = 0x7f1319b3

.field public static final cs_650_android14_authority_tip3:I = 0x7f1319b4

.field public static final cs_650_android14_authority_tip4:I = 0x7f1319b5

.field public static final cs_650_connvert_cancel:I = 0x7f1319b6

.field public static final cs_650_connvert_confirm:I = 0x7f1319b7

.field public static final cs_650_connvert_pop:I = 0x7f1319b8

.field public static final cs_650_connvert_toast:I = 0x7f1319b9

.field public static final cs_650_convert_toast:I = 0x7f1319ba

.field public static final cs_650_exit_toast:I = 0x7f1319bb

.field public static final cs_650_filter_fix:I = 0x7f1319bc

.field public static final cs_650_filter_handwirting:I = 0x7f1319bd

.field public static final cs_650_filter_hd:I = 0x7f1319be

.field public static final cs_650_filter_vip_pop:I = 0x7f1319bf

.field public static final cs_650_filter_vip_pop_btn:I = 0x7f1319c0

.field public static final cs_650_filter_watermark:I = 0x7f1319c1

.field public static final cs_650_guide_title:I = 0x7f1319c2

.field public static final cs_650_guide_title_01:I = 0x7f1319c3

.field public static final cs_650_guide_title_02:I = 0x7f1319c4

.field public static final cs_650_idcard_tips:I = 0x7f1319c5

.field public static final cs_650_log_01:I = 0x7f1319c6

.field public static final cs_650_log_02:I = 0x7f1319c7

.field public static final cs_650_log_03:I = 0x7f1319c8

.field public static final cs_650_name:I = 0x7f1319c9

.field public static final cs_650_ocrresult_delete3:I = 0x7f1319ca

.field public static final cs_650_paper_toady_limit:I = 0x7f1319cb

.field public static final cs_650_photo_01:I = 0x7f1319cc

.field public static final cs_650_photo_02:I = 0x7f1319cd

.field public static final cs_650_photo_03:I = 0x7f1319ce

.field public static final cs_650_photo_04:I = 0x7f1319cf

.field public static final cs_650_photo_05:I = 0x7f1319d0

.field public static final cs_650_photo_06:I = 0x7f1319d1

.field public static final cs_650_photo_07:I = 0x7f1319d2

.field public static final cs_650_photo_08:I = 0x7f1319d3

.field public static final cs_650_photo_09:I = 0x7f1319d4

.field public static final cs_650_photo_10:I = 0x7f1319d5

.field public static final cs_650_photo_11:I = 0x7f1319d6

.field public static final cs_650_photo_12:I = 0x7f1319d7

.field public static final cs_650_photo_13:I = 0x7f1319d8

.field public static final cs_650_photo_14:I = 0x7f1319d9

.field public static final cs_650_photo_15:I = 0x7f1319da

.field public static final cs_650_priner_01:I = 0x7f1319db

.field public static final cs_650_priner_02:I = 0x7f1319dc

.field public static final cs_650_scene_import_001:I = 0x7f1319dd

.field public static final cs_650_scene_import_002:I = 0x7f1319de

.field public static final cs_650_scene_import_003:I = 0x7f1319df

.field public static final cs_650_scene_import_004:I = 0x7f1319e0

.field public static final cs_650_scene_import_005:I = 0x7f1319e1

.field public static final cs_650_scene_import_006:I = 0x7f1319e2

.field public static final cs_650_scene_import_007:I = 0x7f1319e3

.field public static final cs_650_scene_import_008:I = 0x7f1319e4

.field public static final cs_650_scene_import_009:I = 0x7f1319e5

.field public static final cs_650_scene_import_010:I = 0x7f1319e6

.field public static final cs_650_scene_import_011:I = 0x7f1319e7

.field public static final cs_650_scene_import_012:I = 0x7f1319e8

.field public static final cs_650_scene_import_btn:I = 0x7f1319e9

.field public static final cs_650_share_print_content:I = 0x7f1319ea

.field public static final cs_650_share_print_tiltle:I = 0x7f1319eb

.field public static final cs_650_suggeations:I = 0x7f1319ec

.field public static final cs_650_tag_01:I = 0x7f1319ed

.field public static final cs_650_tag_02:I = 0x7f1319ee

.field public static final cs_650_tag_03:I = 0x7f1319ef

.field public static final cs_650_tag_04:I = 0x7f1319f0

.field public static final cs_650_tag_05:I = 0x7f1319f1

.field public static final cs_650_tag_06:I = 0x7f1319f2

.field public static final cs_650_tag_07:I = 0x7f1319f3

.field public static final cs_650_tag_08:I = 0x7f1319f4

.field public static final cs_650_tag_10:I = 0x7f1319f5

.field public static final cs_650_tag_11:I = 0x7f1319f6

.field public static final cs_650_tag_12:I = 0x7f1319f7

.field public static final cs_650_tag_13:I = 0x7f1319f8

.field public static final cs_650_tag_14:I = 0x7f1319f9

.field public static final cs_650_tag_15:I = 0x7f1319fa

.field public static final cs_650_tag_16:I = 0x7f1319fb

.field public static final cs_650_tag_17:I = 0x7f1319fc

.field public static final cs_650_tag_18:I = 0x7f1319fd

.field public static final cs_650_tag_19:I = 0x7f1319fe

.field public static final cs_650_tag_20:I = 0x7f1319ff

.field public static final cs_650_tag_21:I = 0x7f131a00

.field public static final cs_650_tag_23:I = 0x7f131a01

.field public static final cs_650_tag_24:I = 0x7f131a02

.field public static final cs_650_tag_25:I = 0x7f131a03

.field public static final cs_650_tag_26:I = 0x7f131a04

.field public static final cs_650_tag_27:I = 0x7f131a05

.field public static final cs_650_tag_28:I = 0x7f131a06

.field public static final cs_650_tag_29:I = 0x7f131a07

.field public static final cs_650_tag_30:I = 0x7f131a08

.field public static final cs_650_translate_1:I = 0x7f131a09

.field public static final cs_650_translate_2:I = 0x7f131a0a

.field public static final cs_650_view_paper:I = 0x7f131a0b

.field public static final cs_650_workflow_1:I = 0x7f131a0c

.field public static final cs_650_workflow_10:I = 0x7f131a0d

.field public static final cs_650_workflow_11:I = 0x7f131a0e

.field public static final cs_650_workflow_12:I = 0x7f131a0f

.field public static final cs_650_workflow_13:I = 0x7f131a10

.field public static final cs_650_workflow_14:I = 0x7f131a11

.field public static final cs_650_workflow_15:I = 0x7f131a12

.field public static final cs_650_workflow_16:I = 0x7f131a13

.field public static final cs_650_workflow_17:I = 0x7f131a14

.field public static final cs_650_workflow_2:I = 0x7f131a15

.field public static final cs_650_workflow_3:I = 0x7f131a16

.field public static final cs_650_workflow_4:I = 0x7f131a17

.field public static final cs_650_workflow_5:I = 0x7f131a18

.field public static final cs_650_workflow_6:I = 0x7f131a19

.field public static final cs_650_workflow_7:I = 0x7f131a1a

.field public static final cs_650_workflow_7_title:I = 0x7f131a1b

.field public static final cs_650_workflow_8:I = 0x7f131a1c

.field public static final cs_650_workflow_8_title:I = 0x7f131a1d

.field public static final cs_650_workflow_9:I = 0x7f131a1e

.field public static final cs_651_ai_tip:I = 0x7f131a1f

.field public static final cs_651_email_guide_01:I = 0x7f131a20

.field public static final cs_651_email_guide_02:I = 0x7f131a21

.field public static final cs_651_email_guide_03:I = 0x7f131a22

.field public static final cs_651_email_guide_04:I = 0x7f131a23

.field public static final cs_651_email_guide_05:I = 0x7f131a24

.field public static final cs_651_filter_cancel:I = 0x7f131a25

.field public static final cs_651_filter_vippop:I = 0x7f131a26

.field public static final cs_651_handwrite_01:I = 0x7f131a27

.field public static final cs_651_handwrite_02:I = 0x7f131a28

.field public static final cs_651_handwrite_03:I = 0x7f131a29

.field public static final cs_651_printer_1:I = 0x7f131a2a

.field public static final cs_651_printer_2:I = 0x7f131a2b

.field public static final cs_651_printer_3:I = 0x7f131a2c

.field public static final cs_651_printer_4:I = 0x7f131a2d

.field public static final cs_651_printer_5:I = 0x7f131a2e

.field public static final cs_651_printer_6:I = 0x7f131a2f

.field public static final cs_651_printer_7:I = 0x7f131a30

.field public static final cs_651_printer_8:I = 0x7f131a31

.field public static final cs_651_share_link_date:I = 0x7f131a32

.field public static final cs_651_share_link_pop_confirm:I = 0x7f131a33

.field public static final cs_651_share_link_pop_content01:I = 0x7f131a34

.field public static final cs_651_share_link_pop_content02:I = 0x7f131a35

.field public static final cs_651_share_link_pop_content03:I = 0x7f131a36

.field public static final cs_651_share_link_pop_content04:I = 0x7f131a37

.field public static final cs_651_share_link_pop_deney:I = 0x7f131a38

.field public static final cs_651_share_link_pop_title:I = 0x7f131a39

.field public static final cs_651_share_link_title:I = 0x7f131a3a

.field public static final cs_651_to_longpic:I = 0x7f131a3b

.field public static final cs_651_upgrade_btn:I = 0x7f131a3c

.field public static final cs_652_count_01:I = 0x7f131a3d

.field public static final cs_652_count_02:I = 0x7f131a3e

.field public static final cs_652_count_03:I = 0x7f131a3f

.field public static final cs_652_count_04:I = 0x7f131a40

.field public static final cs_652_count_05:I = 0x7f131a41

.field public static final cs_652_count_06:I = 0x7f131a42

.field public static final cs_652_count_07:I = 0x7f131a43

.field public static final cs_652_count_08:I = 0x7f131a44

.field public static final cs_652_count_09:I = 0x7f131a45

.field public static final cs_652_edit_01:I = 0x7f131a46

.field public static final cs_652_excel_01:I = 0x7f131a47

.field public static final cs_652_excel_02:I = 0x7f131a48

.field public static final cs_652_excel_03:I = 0x7f131a49

.field public static final cs_652_excel_04:I = 0x7f131a4a

.field public static final cs_652_import_01:I = 0x7f131a4b

.field public static final cs_652_import_02:I = 0x7f131a4c

.field public static final cs_652_import_03:I = 0x7f131a4d

.field public static final cs_652_import_04:I = 0x7f131a4e

.field public static final cs_652_new_loading:I = 0x7f131a4f

.field public static final cs_652_share_multi_title:I = 0x7f131a50

.field public static final cs_652_superfilter_01:I = 0x7f131a51

.field public static final cs_652_superfilter_02:I = 0x7f131a52

.field public static final cs_652_superfilter_03:I = 0x7f131a53

.field public static final cs_652_superfilter_04:I = 0x7f131a54

.field public static final cs_653_cmp_01:I = 0x7f131a55

.field public static final cs_653_count_01:I = 0x7f131a56

.field public static final cs_653_count_02:I = 0x7f131a57

.field public static final cs_653_count_03:I = 0x7f131a58

.field public static final cs_653_count_04:I = 0x7f131a59

.field public static final cs_653_count_05:I = 0x7f131a5a

.field public static final cs_653_count_06:I = 0x7f131a5b

.field public static final cs_653_count_07:I = 0x7f131a5c

.field public static final cs_653_cs_ai_07:I = 0x7f131a5d

.field public static final cs_653_cs_ai_08:I = 0x7f131a5e

.field public static final cs_653_cs_ai_09:I = 0x7f131a5f

.field public static final cs_653_cs_ai_10:I = 0x7f131a60

.field public static final cs_653_cs_ai_11:I = 0x7f131a61

.field public static final cs_653_cs_ai_12:I = 0x7f131a62

.field public static final cs_653_cs_ai_31:I = 0x7f131a63

.field public static final cs_653_cs_ai_32:I = 0x7f131a64

.field public static final cs_653_docyh_01:I = 0x7f131a65

.field public static final cs_653_super_01:I = 0x7f131a66

.field public static final cs_654_discard_scan_title:I = 0x7f131a67

.field public static final cs_654_edit_doc_01:I = 0x7f131a68

.field public static final cs_654_edit_doc_02:I = 0x7f131a69

.field public static final cs_654_edit_doc_03:I = 0x7f131a6a

.field public static final cs_654_edit_doc_04_body:I = 0x7f131a6b

.field public static final cs_654_edit_doc_04_title:I = 0x7f131a6c

.field public static final cs_654_edit_doc_05:I = 0x7f131a6d

.field public static final cs_654_edit_doc_06:I = 0x7f131a6e

.field public static final cs_654_edit_doc_07:I = 0x7f131a6f

.field public static final cs_654_edit_doc_08:I = 0x7f131a70

.field public static final cs_654_edit_doc_09:I = 0x7f131a71

.field public static final cs_654_edit_doc_10:I = 0x7f131a72

.field public static final cs_654_edit_doc_11:I = 0x7f131a73

.field public static final cs_654_edit_doc_12:I = 0x7f131a74

.field public static final cs_654_icon_banner_free:I = 0x7f131a75

.field public static final cs_654_idpt_01:I = 0x7f131a76

.field public static final cs_654_idpt_02:I = 0x7f131a77

.field public static final cs_654_idpt_03:I = 0x7f131a78

.field public static final cs_654_idpt_04:I = 0x7f131a79

.field public static final cs_654_newvip_01:I = 0x7f131a7a

.field public static final cs_654_newvip_02:I = 0x7f131a7b

.field public static final cs_654_newvip_03:I = 0x7f131a7c

.field public static final cs_654_newvip_04:I = 0x7f131a7d

.field public static final cs_654_newvip_06:I = 0x7f131a7e

.field public static final cs_654_newvip_07:I = 0x7f131a7f

.field public static final cs_654_newvip_08:I = 0x7f131a80

.field public static final cs_654_newvip_09:I = 0x7f131a81

.field public static final cs_654_newvip_10:I = 0x7f131a82

.field public static final cs_654_newvip_12:I = 0x7f131a83

.field public static final cs_654_newvip_13:I = 0x7f131a84

.field public static final cs_654_newvip_14:I = 0x7f131a85

.field public static final cs_654_newvip_16:I = 0x7f131a86

.field public static final cs_654_newvip_17:I = 0x7f131a87

.field public static final cs_654_newvip_18:I = 0x7f131a88

.field public static final cs_654_newvip_19:I = 0x7f131a89

.field public static final cs_654_newvip_21:I = 0x7f131a8a

.field public static final cs_654_printer_1:I = 0x7f131a8b

.field public static final cs_654_printer_10:I = 0x7f131a8c

.field public static final cs_654_printer_2:I = 0x7f131a8d

.field public static final cs_654_printer_3:I = 0x7f131a8e

.field public static final cs_654_printer_4:I = 0x7f131a8f

.field public static final cs_654_printer_5:I = 0x7f131a90

.field public static final cs_654_printer_6:I = 0x7f131a91

.field public static final cs_654_printer_7:I = 0x7f131a92

.field public static final cs_654_printer_8:I = 0x7f131a93

.field public static final cs_654_printer_8_link:I = 0x7f131a94

.field public static final cs_654_printer_9:I = 0x7f131a95

.field public static final cs_655_about_beian:I = 0x7f131a96

.field public static final cs_655_add_json01:I = 0x7f131a97

.field public static final cs_655_add_json02:I = 0x7f131a98

.field public static final cs_655_add_json03:I = 0x7f131a99

.field public static final cs_655_add_json04:I = 0x7f131a9a

.field public static final cs_655_add_json05:I = 0x7f131a9b

.field public static final cs_655_add_json06:I = 0x7f131a9c

.field public static final cs_655_add_json07:I = 0x7f131a9d

.field public static final cs_655_add_json08:I = 0x7f131a9e

.field public static final cs_655_add_json09:I = 0x7f131a9f

.field public static final cs_655_add_json10:I = 0x7f131aa0

.field public static final cs_655_add_json11:I = 0x7f131aa1

.field public static final cs_655_add_json12:I = 0x7f131aa2

.field public static final cs_655_add_json13:I = 0x7f131aa3

.field public static final cs_655_add_json14:I = 0x7f131aa4

.field public static final cs_655_add_json15:I = 0x7f131aa5

.field public static final cs_655_doc_backup_01:I = 0x7f131aa6

.field public static final cs_655_doc_backup_02:I = 0x7f131aa7

.field public static final cs_655_doc_backup_03:I = 0x7f131aa8

.field public static final cs_655_doc_backup_04:I = 0x7f131aa9

.field public static final cs_655_doc_backup_06:I = 0x7f131aaa

.field public static final cs_655_doc_backup_08:I = 0x7f131aab

.field public static final cs_655_doc_backup_09:I = 0x7f131aac

.field public static final cs_655_doc_backup_10:I = 0x7f131aad

.field public static final cs_655_doc_backup_11:I = 0x7f131aae

.field public static final cs_655_doc_backup_12:I = 0x7f131aaf

.field public static final cs_655_doc_backup_13:I = 0x7f131ab0

.field public static final cs_655_doc_backup_14:I = 0x7f131ab1

.field public static final cs_655_doc_backup_15:I = 0x7f131ab2

.field public static final cs_655_doc_backup_16:I = 0x7f131ab3

.field public static final cs_655_doc_backup_17:I = 0x7f131ab4

.field public static final cs_655_doc_backup_18:I = 0x7f131ab5

.field public static final cs_655_doc_backup_19:I = 0x7f131ab6

.field public static final cs_655_doc_backup_20:I = 0x7f131ab7

.field public static final cs_655_doc_backup_21:I = 0x7f131ab8

.field public static final cs_655_doc_backup_22:I = 0x7f131ab9

.field public static final cs_655_doc_backup_23:I = 0x7f131aba

.field public static final cs_655_doc_backup_24:I = 0x7f131abb

.field public static final cs_655_doc_backup_25:I = 0x7f131abc

.field public static final cs_655_doc_backup_26:I = 0x7f131abd

.field public static final cs_655_doc_backup_28:I = 0x7f131abe

.field public static final cs_655_doc_backup_29:I = 0x7f131abf

.field public static final cs_655_doc_backup_30:I = 0x7f131ac0

.field public static final cs_655_doc_backup_31:I = 0x7f131ac1

.field public static final cs_655_doc_backup_32:I = 0x7f131ac2

.field public static final cs_655_doc_backup_34:I = 0x7f131ac3

.field public static final cs_655_doc_backup_35:I = 0x7f131ac4

.field public static final cs_655_doc_backup_36:I = 0x7f131ac5

.field public static final cs_655_doc_backup_37:I = 0x7f131ac6

.field public static final cs_655_doc_backup_38:I = 0x7f131ac7

.field public static final cs_655_doc_backup_39:I = 0x7f131ac8

.field public static final cs_655_doc_backup_40:I = 0x7f131ac9

.field public static final cs_655_doc_backup_41:I = 0x7f131aca

.field public static final cs_655_doc_backup_42:I = 0x7f131acb

.field public static final cs_655_doc_backup_44:I = 0x7f131acc

.field public static final cs_655_doc_backup_45:I = 0x7f131acd

.field public static final cs_655_doc_backup_46:I = 0x7f131ace

.field public static final cs_655_doc_backup_47:I = 0x7f131acf

.field public static final cs_655_doc_backup_48:I = 0x7f131ad0

.field public static final cs_655_doc_backup_49:I = 0x7f131ad1

.field public static final cs_655_doc_backup_50:I = 0x7f131ad2

.field public static final cs_655_doc_backup_51:I = 0x7f131ad3

.field public static final cs_655_doc_backup_52:I = 0x7f131ad4

.field public static final cs_655_doc_backup_53:I = 0x7f131ad5

.field public static final cs_655_doc_backup_54:I = 0x7f131ad6

.field public static final cs_655_doc_backup_55:I = 0x7f131ad7

.field public static final cs_655_doc_backup_56:I = 0x7f131ad8

.field public static final cs_655_doc_backup_57:I = 0x7f131ad9

.field public static final cs_655_doc_backup_58:I = 0x7f131ada

.field public static final cs_655_doc_backup_59:I = 0x7f131adb

.field public static final cs_655_doc_backup_60:I = 0x7f131adc

.field public static final cs_655_doc_backup_type:I = 0x7f131add

.field public static final cs_655_from_00:I = 0x7f131ade

.field public static final cs_655_from_01:I = 0x7f131adf

.field public static final cs_655_from_02:I = 0x7f131ae0

.field public static final cs_655_from_03:I = 0x7f131ae1

.field public static final cs_655_from_04:I = 0x7f131ae2

.field public static final cs_655_from_05:I = 0x7f131ae3

.field public static final cs_655_from_06:I = 0x7f131ae4

.field public static final cs_655_from_07:I = 0x7f131ae5

.field public static final cs_655_from_08:I = 0x7f131ae6

.field public static final cs_655_from_09:I = 0x7f131ae7

.field public static final cs_655_from_10:I = 0x7f131ae8

.field public static final cs_655_from_11:I = 0x7f131ae9

.field public static final cs_655_from_12:I = 0x7f131aea

.field public static final cs_655_from_13:I = 0x7f131aeb

.field public static final cs_655_from_14:I = 0x7f131aec

.field public static final cs_655_from_15:I = 0x7f131aed

.field public static final cs_655_pdfpassword_t1:I = 0x7f131aee

.field public static final cs_655_pdfpassword_t2:I = 0x7f131aef

.field public static final cs_655_pdfpw_btn1:I = 0x7f131af0

.field public static final cs_655_pdfpw_btn2:I = 0x7f131af1

.field public static final cs_655_pdfpw_btn3:I = 0x7f131af2

.field public static final cs_655_pdfpw_t1:I = 0x7f131af3

.field public static final cs_655_pdfpw_t2:I = 0x7f131af4

.field public static final cs_655_premium_monthly_01:I = 0x7f131af5

.field public static final cs_655_premium_monthly_02:I = 0x7f131af6

.field public static final cs_655_premium_monthly_03:I = 0x7f131af7

.field public static final cs_655_premium_monthly_04:I = 0x7f131af8

.field public static final cs_655_premium_monthly_06:I = 0x7f131af9

.field public static final cs_655_premium_monthly_08:I = 0x7f131afa

.field public static final cs_655_premium_monthly_09:I = 0x7f131afb

.field public static final cs_655_premium_monthly_10:I = 0x7f131afc

.field public static final cs_655_premium_monthly_11:I = 0x7f131afd

.field public static final cs_655_premium_monthly_12:I = 0x7f131afe

.field public static final cs_655_premium_monthly_13:I = 0x7f131aff

.field public static final cs_655_premium_monthly_14:I = 0x7f131b00

.field public static final cs_655_premium_monthly_15:I = 0x7f131b01

.field public static final cs_655_premium_monthly_16:I = 0x7f131b02

.field public static final cs_655_premium_monthly_17:I = 0x7f131b03

.field public static final cs_655_premium_monthly_19:I = 0x7f131b04

.field public static final cs_655_premium_monthly_21:I = 0x7f131b05

.field public static final cs_655_premium_monthly_22:I = 0x7f131b06

.field public static final cs_655_premium_monthly_23:I = 0x7f131b07

.field public static final cs_655_premium_monthly_24:I = 0x7f131b08

.field public static final cs_655_premium_monthly_25:I = 0x7f131b09

.field public static final cs_655_premium_monthly_26:I = 0x7f131b0a

.field public static final cs_655_premium_monthly_27:I = 0x7f131b0b

.field public static final cs_655_premium_monthly_28:I = 0x7f131b0c

.field public static final cs_655_premium_monthly_29:I = 0x7f131b0d

.field public static final cs_655_premium_monthly_30:I = 0x7f131b0e

.field public static final cs_655_premium_monthly_31:I = 0x7f131b0f

.field public static final cs_655_premium_monthly_32:I = 0x7f131b10

.field public static final cs_655_premium_monthly_33:I = 0x7f131b11

.field public static final cs_655_premium_monthly_34:I = 0x7f131b12

.field public static final cs_655_printer_1:I = 0x7f131b13

.field public static final cs_655_printer_2:I = 0x7f131b14

.field public static final cs_655_printer_3:I = 0x7f131b15

.field public static final cs_655_printer_4:I = 0x7f131b16

.field public static final cs_655_printer_5:I = 0x7f131b17

.field public static final cs_655_sysprinter_1:I = 0x7f131b18

.field public static final cs_655_sysprinter_2:I = 0x7f131b19

.field public static final cs_655_sysprinter_3:I = 0x7f131b1a

.field public static final cs_655_vip_activity1:I = 0x7f131b1b

.field public static final cs_655_vip_activity10:I = 0x7f131b1c

.field public static final cs_655_vip_activity12:I = 0x7f131b1d

.field public static final cs_655_vip_activity13:I = 0x7f131b1e

.field public static final cs_655_vip_activity14:I = 0x7f131b1f

.field public static final cs_655_vip_activity15:I = 0x7f131b20

.field public static final cs_655_vip_activity16:I = 0x7f131b21

.field public static final cs_655_vip_activity17:I = 0x7f131b22

.field public static final cs_655_vip_activity18:I = 0x7f131b23

.field public static final cs_655_vip_activity19:I = 0x7f131b24

.field public static final cs_655_vip_activity20:I = 0x7f131b25

.field public static final cs_655_vip_activity6:I = 0x7f131b26

.field public static final cs_655_vip_activity7:I = 0x7f131b27

.field public static final cs_655_vip_activity8:I = 0x7f131b28

.field public static final cs_655_vip_activity9:I = 0x7f131b29

.field public static final cs_655_watermark1:I = 0x7f131b2a

.field public static final cs_655_watermark10:I = 0x7f131b2b

.field public static final cs_655_watermark3:I = 0x7f131b2c

.field public static final cs_655_watermark4:I = 0x7f131b2d

.field public static final cs_655_watermark5:I = 0x7f131b2e

.field public static final cs_655_watermark7:I = 0x7f131b2f

.field public static final cs_655_watermark8:I = 0x7f131b30

.field public static final cs_655_watermark9:I = 0x7f131b31

.field public static final cs_655_word_04:I = 0x7f131b32

.field public static final cs_655_word_07:I = 0x7f131b33

.field public static final cs_656_30_premium:I = 0x7f131b34

.field public static final cs_656_choose_word_01:I = 0x7f131b35

.field public static final cs_656_choose_word_02:I = 0x7f131b36

.field public static final cs_656_choose_word_03:I = 0x7f131b37

.field public static final cs_656_choose_word_04:I = 0x7f131b38

.field public static final cs_656_choose_word_05:I = 0x7f131b39

.field public static final cs_656_choose_word_06:I = 0x7f131b3a

.field public static final cs_656_choose_word_07:I = 0x7f131b3b

.field public static final cs_656_choose_word_08:I = 0x7f131b3c

.field public static final cs_656_choose_word_09:I = 0x7f131b3d

.field public static final cs_656_choose_word_10:I = 0x7f131b3e

.field public static final cs_656_choose_word_11:I = 0x7f131b3f

.field public static final cs_656_choose_word_12:I = 0x7f131b40

.field public static final cs_656_choose_word_13:I = 0x7f131b41

.field public static final cs_656_choose_word_14:I = 0x7f131b42

.field public static final cs_656_choose_word_14_1:I = 0x7f131b43

.field public static final cs_656_choose_word_15:I = 0x7f131b44

.field public static final cs_656_choose_word_16:I = 0x7f131b45

.field public static final cs_656_choose_word_17:I = 0x7f131b46

.field public static final cs_656_choose_word_18:I = 0x7f131b47

.field public static final cs_656_choose_word_18_1:I = 0x7f131b48

.field public static final cs_656_choose_word_19:I = 0x7f131b49

.field public static final cs_656_choose_word_20:I = 0x7f131b4a

.field public static final cs_656_choose_word_21:I = 0x7f131b4b

.field public static final cs_656_choose_word_22:I = 0x7f131b4c

.field public static final cs_656_choose_word_24:I = 0x7f131b4d

.field public static final cs_656_choose_word_26:I = 0x7f131b4e

.field public static final cs_656_choose_word_27:I = 0x7f131b4f

.field public static final cs_656_choose_word_28:I = 0x7f131b50

.field public static final cs_656_choose_word_29:I = 0x7f131b51

.field public static final cs_656_choose_word_30:I = 0x7f131b52

.field public static final cs_656_choose_word_31:I = 0x7f131b53

.field public static final cs_656_choose_word_32:I = 0x7f131b54

.field public static final cs_656_edit_text_01:I = 0x7f131b55

.field public static final cs_656_edit_text_02:I = 0x7f131b56

.field public static final cs_656_edit_text_03:I = 0x7f131b57

.field public static final cs_656_edit_text_04:I = 0x7f131b58

.field public static final cs_656_link_loding_stp1:I = 0x7f131b59

.field public static final cs_656_link_loding_stp1_ok:I = 0x7f131b5a

.field public static final cs_656_link_loding_stp2:I = 0x7f131b5b

.field public static final cs_656_link_loding_stp2_ok:I = 0x7f131b5c

.field public static final cs_656_link_loding_stp3:I = 0x7f131b5d

.field public static final cs_656_link_loding_stp3_ok:I = 0x7f131b5e

.field public static final cs_656_open_saved_01:I = 0x7f131b5f

.field public static final cs_656_open_saved_02:I = 0x7f131b60

.field public static final cs_656_open_saved_03:I = 0x7f131b61

.field public static final cs_656_open_saved_04:I = 0x7f131b62

.field public static final cs_656_open_saved_05:I = 0x7f131b63

.field public static final cs_656_open_saved_06:I = 0x7f131b64

.field public static final cs_656_open_saved_07:I = 0x7f131b65

.field public static final cs_656_open_saved_08:I = 0x7f131b66

.field public static final cs_656_open_saved_09:I = 0x7f131b67

.field public static final cs_656_open_saved_10:I = 0x7f131b68

.field public static final cs_656_printer_1:I = 0x7f131b69

.field public static final cs_656_printer_2:I = 0x7f131b6a

.field public static final cs_656_printer_3:I = 0x7f131b6b

.field public static final cs_656_printer_4:I = 0x7f131b6c

.field public static final cs_656_printer_5:I = 0x7f131b6d

.field public static final cs_656_printer_6:I = 0x7f131b6e

.field public static final cs_656_printer_7:I = 0x7f131b6f

.field public static final cs_656_select_text:I = 0x7f131b70

.field public static final cs_656_transfer_pic_document_loading:I = 0x7f131b71

.field public static final cs_656_transfer_pic_document_loading_noquit:I = 0x7f131b72

.field public static final cs_656_transfer_pic_guide:I = 0x7f131b73

.field public static final cs_656_transfer_pic_import_setting:I = 0x7f131b74

.field public static final cs_656_transfer_pic_import_transfer:I = 0x7f131b75

.field public static final cs_656_transfer_pic_pdf:I = 0x7f131b76

.field public static final cs_656_transfer_pic_pdf_default_format:I = 0x7f131b77

.field public static final cs_656_transfer_pic_pdf_setting:I = 0x7f131b78

.field public static final cs_656_transfer_pic_pdf_viewtype:I = 0x7f131b79

.field public static final cs_656_transfer_pic_transfer:I = 0x7f131b7a

.field public static final cs_656_welcomeback_01:I = 0x7f131b7b

.field public static final cs_656_welcomeback_03:I = 0x7f131b7c

.field public static final cs_656_welcomeback_04:I = 0x7f131b7d

.field public static final cs_656_welcomeback_05:I = 0x7f131b7e

.field public static final cs_656_welcomeback_06:I = 0x7f131b7f

.field public static final cs_656_welcomeback_09:I = 0x7f131b80

.field public static final cs_656_welcomeback_10:I = 0x7f131b81

.field public static final cs_656_welcomeback_11:I = 0x7f131b82

.field public static final cs_656_welcomeback_12:I = 0x7f131b83

.field public static final cs_656_welcomeback_29:I = 0x7f131b84

.field public static final cs_656_welcomeback_30:I = 0x7f131b85

.field public static final cs_656_welcomeback_31:I = 0x7f131b86

.field public static final cs_656_welcomeback_32:I = 0x7f131b87

.field public static final cs_656_welcomeback_33:I = 0x7f131b88

.field public static final cs_656_welcomeback_34:I = 0x7f131b89

.field public static final cs_656_welcomeback_35:I = 0x7f131b8a

.field public static final cs_656_welcomeback_36:I = 0x7f131b8b

.field public static final cs_656_welcomeback_37:I = 0x7f131b8c

.field public static final cs_656_welcomeback_38:I = 0x7f131b8d

.field public static final cs_656_welcomeback_39:I = 0x7f131b8e

.field public static final cs_656_welcomeback_40:I = 0x7f131b8f

.field public static final cs_656_word_1:I = 0x7f131b90

.field public static final cs_660_back_01:I = 0x7f131b91

.field public static final cs_660_growth01:I = 0x7f131b92

.field public static final cs_660_growth02:I = 0x7f131b93

.field public static final cs_660_growth03:I = 0x7f131b94

.field public static final cs_660_growth04:I = 0x7f131b95

.field public static final cs_660_growth05:I = 0x7f131b96

.field public static final cs_660_nps_reward:I = 0x7f131b97

.field public static final cs_660_privacy_update:I = 0x7f131b98

.field public static final cs_660_privacy_update_btn01:I = 0x7f131b99

.field public static final cs_660_privacy_update_content:I = 0x7f131b9a

.field public static final cs_660_remove_watermark_001:I = 0x7f131b9b

.field public static final cs_660_remove_watermark_002:I = 0x7f131b9c

.field public static final cs_660_remove_watermark_003:I = 0x7f131b9d

.field public static final cs_660_remove_watermark_004:I = 0x7f131b9e

.field public static final cs_660_remove_watermark_005:I = 0x7f131b9f

.field public static final cs_660_remove_watermark_006:I = 0x7f131ba0

.field public static final cs_660_remove_watermark_008:I = 0x7f131ba1

.field public static final cs_660_remove_watermark_009:I = 0x7f131ba2

.field public static final cs_660_remove_watermark_012:I = 0x7f131ba3

.field public static final cs_660_remove_watermark_013:I = 0x7f131ba4

.field public static final cs_660_remove_watermark_014:I = 0x7f131ba5

.field public static final cs_660_remove_watermark_015:I = 0x7f131ba6

.field public static final cs_660_remove_watermark_016:I = 0x7f131ba7

.field public static final cs_660_study:I = 0x7f131ba8

.field public static final cs_670_feel_01:I = 0x7f131baa

.field public static final cs_670_feel_02:I = 0x7f131bab

.field public static final cs_670_feel_03:I = 0x7f131bac

.field public static final cs_670_feel_04:I = 0x7f131bad

.field public static final cs_670_feel_05:I = 0x7f131bae

.field public static final cs_670_feel_06:I = 0x7f131baf

.field public static final cs_670_feel_07:I = 0x7f131bb0

.field public static final cs_670_feel_08:I = 0x7f131bb1

.field public static final cs_670_feel_09:I = 0x7f131bb2

.field public static final cs_670_feel_10:I = 0x7f131bb3

.field public static final cs_670_feel_11:I = 0x7f131bb4

.field public static final cs_670_feel_12:I = 0x7f131bb5

.field public static final cs_670_feel_13:I = 0x7f131bb6

.field public static final cs_670_feel_14:I = 0x7f131bb7

.field public static final cs_670_feel_15:I = 0x7f131bb8

.field public static final cs_670_feel_16:I = 0x7f131bb9

.field public static final cs_670_feel_17:I = 0x7f131bba

.field public static final cs_670_feel_18:I = 0x7f131bbb

.field public static final cs_670_feel_19:I = 0x7f131bbc

.field public static final cs_670_feel_20:I = 0x7f131bbd

.field public static final cs_670_feel_21:I = 0x7f131bbe

.field public static final cs_670_feel_22:I = 0x7f131bbf

.field public static final cs_670_feel_23:I = 0x7f131bc0

.field public static final cs_670_feel_24:I = 0x7f131bc1

.field public static final cs_670_feel_25:I = 0x7f131bc2

.field public static final cs_670_feel_26:I = 0x7f131bc3

.field public static final cs_670_feel_27:I = 0x7f131bc4

.field public static final cs_670_feel_28:I = 0x7f131bc5

.field public static final cs_670_feel_30:I = 0x7f131bc6

.field public static final cs_670_feel_31:I = 0x7f131bc7

.field public static final cs_670_guide_1:I = 0x7f131bc8

.field public static final cs_670_ocr_01:I = 0x7f131bc9

.field public static final cs_670_ocr_02:I = 0x7f131bca

.field public static final cs_670_ocr_03:I = 0x7f131bcb

.field public static final cs_670_ocr_04:I = 0x7f131bcc

.field public static final cs_670_ocr_05:I = 0x7f131bcd

.field public static final cs_670_ocr_06:I = 0x7f131bce

.field public static final cs_670_ocr_07:I = 0x7f131bcf

.field public static final cs_670_responsibility_03:I = 0x7f131bd0

.field public static final cs_670_responsibility_04:I = 0x7f131bd1

.field public static final cs_670_responsibility_05:I = 0x7f131bd2

.field public static final cs_680_alipay:I = 0x7f131bd3

.field public static final cs_680_gift_01:I = 0x7f131bd4

.field public static final cs_680_local_ocr_tips_renew:I = 0x7f131bd5

.field public static final cs_680_new_scan_tip01:I = 0x7f131bd6

.field public static final cs_680_new_scan_tip02:I = 0x7f131bd7

.field public static final cs_680_new_scan_tip03:I = 0x7f131bd8

.field public static final cs_680_new_scan_tip04:I = 0x7f131bd9

.field public static final cs_680_new_scan_tip06:I = 0x7f131bda

.field public static final cs_680_new_scan_tip07:I = 0x7f131bdb

.field public static final cs_680_new_scan_tip08:I = 0x7f131bdc

.field public static final cs_680_new_scan_tip09:I = 0x7f131bdd

.field public static final cs_680_new_scan_tip10:I = 0x7f131bde

.field public static final cs_680_new_scan_tip11:I = 0x7f131bdf

.field public static final cs_680_new_scan_tip12:I = 0x7f131be0

.field public static final cs_680_new_scan_tip13:I = 0x7f131be1

.field public static final cs_680_ocr_01:I = 0x7f131be2

.field public static final cs_680_ocr_02:I = 0x7f131be3

.field public static final cs_680_permission01:I = 0x7f131be4

.field public static final cs_680_permission02:I = 0x7f131be5

.field public static final cs_680_permission03:I = 0x7f131be6

.field public static final cs_680_permission04:I = 0x7f131be7

.field public static final cs_680_permission05:I = 0x7f131be8

.field public static final cs_680_permission06:I = 0x7f131be9

.field public static final cs_680_permission07:I = 0x7f131bea

.field public static final cs_680_permission08:I = 0x7f131beb

.field public static final cs_680_permission09:I = 0x7f131bec

.field public static final cs_680_permission10:I = 0x7f131bed

.field public static final cs_680_permission11:I = 0x7f131bee

.field public static final cs_680_permission12:I = 0x7f131bef

.field public static final cs_680_permission13:I = 0x7f131bf0

.field public static final cs_680_permission14:I = 0x7f131bf1

.field public static final cs_680_permission15:I = 0x7f131bf2

.field public static final cs_680_permission16:I = 0x7f131bf3

.field public static final cs_680_permission17:I = 0x7f131bf4

.field public static final cs_680_privacy_list:I = 0x7f131bf5

.field public static final cs_680_privacy_premission:I = 0x7f131bf6

.field public static final cs_680_privacy_summary:I = 0x7f131bf7

.field public static final cs_680_privacy_third:I = 0x7f131bf8

.field public static final cs_690_ocr_01:I = 0x7f131bf9

.field public static final cs_690_register01:I = 0x7f131bfa

.field public static final cs_690_register02:I = 0x7f131bfb

.field public static final cs_690_register03:I = 0x7f131bfc

.field public static final cs_690_register04:I = 0x7f131bfd

.field public static final cs_690_register05:I = 0x7f131bfe

.field public static final cs_690_register05_1:I = 0x7f131bff

.field public static final cs_690_register06:I = 0x7f131c00

.field public static final cs_690_register07:I = 0x7f131c01

.field public static final cs_690_register08:I = 0x7f131c02

.field public static final cs_ad_546_doclistpopup_close:I = 0x7f131c03

.field public static final cs_ali_pay_success:I = 0x7f131c04

.field public static final cs_confirmbox_btn_exit:I = 0x7f131c05

.field public static final cs_confirmbox_btn_remove:I = 0x7f131c06

.field public static final cs_import_pdf_word:I = 0x7f131c07

.field public static final cs_import_sign_01:I = 0x7f131c08

.field public static final cs_import_sign_02:I = 0x7f131c09

.field public static final cs_import_sign_03:I = 0x7f131c0a

.field public static final cs_import_sign_04:I = 0x7f131c0b

.field public static final cs_import_sign_05:I = 0x7f131c0c

.field public static final cs_invited_id_001:I = 0x7f131c0d

.field public static final cs_invited_id_0010:I = 0x7f131c0e

.field public static final cs_invited_id_0011:I = 0x7f131c0f

.field public static final cs_invited_id_0012:I = 0x7f131c10

.field public static final cs_invited_id_0013:I = 0x7f131c11

.field public static final cs_invited_id_0014:I = 0x7f131c12

.field public static final cs_invited_id_0015:I = 0x7f131c13

.field public static final cs_invited_id_0016:I = 0x7f131c14

.field public static final cs_invited_id_0017:I = 0x7f131c15

.field public static final cs_invited_id_0018:I = 0x7f131c16

.field public static final cs_invited_id_0019:I = 0x7f131c17

.field public static final cs_invited_id_002:I = 0x7f131c18

.field public static final cs_invited_id_0020:I = 0x7f131c19

.field public static final cs_invited_id_0021:I = 0x7f131c1a

.field public static final cs_invited_id_0023:I = 0x7f131c1b

.field public static final cs_invited_id_0024:I = 0x7f131c1c

.field public static final cs_invited_id_0025:I = 0x7f131c1d

.field public static final cs_invited_id_0026:I = 0x7f131c1e

.field public static final cs_invited_id_0027:I = 0x7f131c1f

.field public static final cs_invited_id_0028:I = 0x7f131c20

.field public static final cs_invited_id_0029:I = 0x7f131c21

.field public static final cs_invited_id_003:I = 0x7f131c22

.field public static final cs_invited_id_0030:I = 0x7f131c23

.field public static final cs_invited_id_0031:I = 0x7f131c24

.field public static final cs_invited_id_0032:I = 0x7f131c25

.field public static final cs_invited_id_0033:I = 0x7f131c26

.field public static final cs_invited_id_0034:I = 0x7f131c27

.field public static final cs_invited_id_0035:I = 0x7f131c28

.field public static final cs_invited_id_0036:I = 0x7f131c29

.field public static final cs_invited_id_0037:I = 0x7f131c2a

.field public static final cs_invited_id_004:I = 0x7f131c2b

.field public static final cs_invited_id_0042:I = 0x7f131c2c

.field public static final cs_invited_id_0043:I = 0x7f131c2d

.field public static final cs_invited_id_0044:I = 0x7f131c2e

.field public static final cs_invited_id_0045:I = 0x7f131c2f

.field public static final cs_invited_id_0046:I = 0x7f131c30

.field public static final cs_invited_id_0047:I = 0x7f131c31

.field public static final cs_invited_id_0048:I = 0x7f131c32

.field public static final cs_invited_id_0049:I = 0x7f131c33

.field public static final cs_invited_id_005:I = 0x7f131c34

.field public static final cs_invited_id_0051:I = 0x7f131c35

.field public static final cs_invited_id_0052:I = 0x7f131c36

.field public static final cs_invited_id_0053:I = 0x7f131c37

.field public static final cs_invited_id_0054:I = 0x7f131c38

.field public static final cs_invited_id_0055:I = 0x7f131c39

.field public static final cs_invited_id_0056:I = 0x7f131c3a

.field public static final cs_invited_id_0057:I = 0x7f131c3b

.field public static final cs_invited_id_0058:I = 0x7f131c3c

.field public static final cs_invited_id_0059:I = 0x7f131c3d

.field public static final cs_invited_id_006:I = 0x7f131c3e

.field public static final cs_invited_id_0060:I = 0x7f131c3f

.field public static final cs_invited_id_0061:I = 0x7f131c40

.field public static final cs_invited_id_0062:I = 0x7f131c41

.field public static final cs_invited_id_0063:I = 0x7f131c42

.field public static final cs_invited_id_0064:I = 0x7f131c43

.field public static final cs_invited_id_0066:I = 0x7f131c44

.field public static final cs_invited_id_0068:I = 0x7f131c45

.field public static final cs_invited_id_0069:I = 0x7f131c46

.field public static final cs_invited_id_007:I = 0x7f131c47

.field public static final cs_invited_id_0070:I = 0x7f131c48

.field public static final cs_invited_id_0071:I = 0x7f131c49

.field public static final cs_invited_id_0073:I = 0x7f131c4a

.field public static final cs_invited_id_0074:I = 0x7f131c4b

.field public static final cs_invited_id_0075:I = 0x7f131c4c

.field public static final cs_invited_id_0076:I = 0x7f131c4d

.field public static final cs_invited_id_0078:I = 0x7f131c4e

.field public static final cs_invited_id_008:I = 0x7f131c4f

.field public static final cs_invited_id_009:I = 0x7f131c50

.field public static final cs_k44_left_premium_Label:I = 0x7f131c51

.field public static final cs_main_menu_btn_tip_note:I = 0x7f131c52

.field public static final cs_no526_10years:I = 0x7f131c53

.field public static final cs_no526_chosen:I = 0x7f131c54

.field public static final cs_no526_ingenuity:I = 0x7f131c55

.field public static final cs_no527_label_guide_03:I = 0x7f131c56

.field public static final cs_no527_label_guide_05:I = 0x7f131c57

.field public static final cs_no528_svip_15:I = 0x7f131c58

.field public static final cs_no528_svip_17:I = 0x7f131c59

.field public static final cs_no528_svip_18:I = 0x7f131c5a

.field public static final cs_no528_svip_19:I = 0x7f131c5b

.field public static final cs_no528_svip_20:I = 0x7f131c5c

.field public static final cs_no528_svip_22:I = 0x7f131c5d

.field public static final cs_no528_svip_24:I = 0x7f131c5e

.field public static final cs_no528_svip_27:I = 0x7f131c5f

.field public static final cs_no528_svip_32:I = 0x7f131c60

.field public static final cs_no528_svip_34:I = 0x7f131c61

.field public static final cs_no528_svip_35:I = 0x7f131c62

.field public static final cs_no528_svip_37:I = 0x7f131c63

.field public static final cs_no528_svip_39:I = 0x7f131c64

.field public static final cs_no528_svip_41:I = 0x7f131c65

.field public static final cs_no528_svip_8:I = 0x7f131c66

.field public static final cs_ocr_TrailPop_tip:I = 0x7f131c67

.field public static final cs_revision_account_01:I = 0x7f131c68

.field public static final cs_revision_account_03:I = 0x7f131c69

.field public static final cs_revision_account_04:I = 0x7f131c6a

.field public static final cs_revision_bottom_01:I = 0x7f131c6b

.field public static final cs_revision_bottom_02:I = 0x7f131c6c

.field public static final cs_revision_docs_03:I = 0x7f131c6d

.field public static final cs_revision_guide_01:I = 0x7f131c6e

.field public static final cs_revision_guide_02:I = 0x7f131c6f

.field public static final cs_revision_guide_03:I = 0x7f131c70

.field public static final cs_revision_guide_04:I = 0x7f131c71

.field public static final cs_revision_guide_05:I = 0x7f131c72

.field public static final cs_revision_guide_06:I = 0x7f131c73

.field public static final cs_revision_home_01:I = 0x7f131c74

.field public static final cs_revision_home_02:I = 0x7f131c75

.field public static final cs_revision_me_01:I = 0x7f131c76

.field public static final cs_revision_me_02:I = 0x7f131c77

.field public static final cs_revision_me_03:I = 0x7f131c78

.field public static final cs_revision_me_04:I = 0x7f131c79

.field public static final cs_revision_me_05:I = 0x7f131c7a

.field public static final cs_revision_me_07:I = 0x7f131c7b

.field public static final cs_revision_me_12:I = 0x7f131c7c

.field public static final cs_revision_me_13:I = 0x7f131c7d

.field public static final cs_revision_me_19:I = 0x7f131c7e

.field public static final cs_revision_me_20:I = 0x7f131c7f

.field public static final cs_revision_me_21:I = 0x7f131c80

.field public static final cs_revision_me_22:I = 0x7f131c81

.field public static final cs_revision_me_24:I = 0x7f131c82

.field public static final cs_revision_me_25:I = 0x7f131c83

.field public static final cs_revision_me_26:I = 0x7f131c84

.field public static final cs_revision_me_27:I = 0x7f131c85

.field public static final cs_revision_me_29:I = 0x7f131c86

.field public static final cs_revision_me_32:I = 0x7f131c87

.field public static final cs_revision_me_33:I = 0x7f131c88

.field public static final cs_revision_recent_01:I = 0x7f131c89

.field public static final cs_revision_recent_02:I = 0x7f131c8a

.field public static final cs_revision_tools_01:I = 0x7f131c8b

.field public static final cs_revision_tools_02:I = 0x7f131c8c

.field public static final cs_rnsign_confirm_01:I = 0x7f131c8d

.field public static final cs_rnsign_confirm_02:I = 0x7f131c8e

.field public static final cs_rnsign_confirm_03:I = 0x7f131c8f

.field public static final cs_rnsign_confirm_04:I = 0x7f131c90

.field public static final cs_rnsign_confirm_06:I = 0x7f131c91

.field public static final cs_rnsign_confirm_07:I = 0x7f131c92

.field public static final cs_rnsign_confirm_08:I = 0x7f131c93

.field public static final cs_rnsign_confirm_09:I = 0x7f131c94

.field public static final cs_rnsign_confirm_10:I = 0x7f131c95

.field public static final cs_rnsign_confirm_11:I = 0x7f131c96

.field public static final cs_rnsign_confirm_12:I = 0x7f131c97

.field public static final cs_rnsign_confirm_14:I = 0x7f131c98

.field public static final cs_rnsign_confirm_15:I = 0x7f131c99

.field public static final cs_rnsign_confirm_16:I = 0x7f131c9a

.field public static final cs_rnsign_confirm_17:I = 0x7f131c9b

.field public static final cs_rnsign_confirm_20:I = 0x7f131c9c

.field public static final cs_rnsign_confirm_21:I = 0x7f131c9d

.field public static final cs_rnsign_confirm_22:I = 0x7f131c9e

.field public static final cs_rnsign_confirm_ca_service_agreement:I = 0x7f131c9f

.field public static final cs_rnsign_confirm_cs_sign_agreement:I = 0x7f131ca0

.field public static final cs_rnsign_doubleconfirm_02:I = 0x7f131ca1

.field public static final cs_rnsign_doubleconfirm_03:I = 0x7f131ca2

.field public static final cs_rnsign_doubleconfirm_04:I = 0x7f131ca3

.field public static final cs_rnsign_doubleconfirm_05:I = 0x7f131ca4

.field public static final cs_rnsign_doubleconfirm_06:I = 0x7f131ca5

.field public static final cs_rnsign_evidence_01:I = 0x7f131ca6

.field public static final cs_rnsign_evidence_010:I = 0x7f131ca7

.field public static final cs_rnsign_evidence_02:I = 0x7f131ca8

.field public static final cs_rnsign_evidence_03:I = 0x7f131ca9

.field public static final cs_rnsign_evidence_04:I = 0x7f131caa

.field public static final cs_rnsign_evidence_05:I = 0x7f131cab

.field public static final cs_rnsign_evidence_06:I = 0x7f131cac

.field public static final cs_rnsign_evidence_07:I = 0x7f131cad

.field public static final cs_rnsign_evidence_08:I = 0x7f131cae

.field public static final cs_rnsign_evidence_09:I = 0x7f131caf

.field public static final cs_rnsign_evidence_btn_go_now:I = 0x7f131cb0

.field public static final cs_rnsign_evidence_notarial_title:I = 0x7f131cb1

.field public static final cs_rnsign_faceid_01:I = 0x7f131cb2

.field public static final cs_rnsign_faceid_02:I = 0x7f131cb3

.field public static final cs_rnsign_faceid_03:I = 0x7f131cb4

.field public static final cs_rnsign_faceid_04:I = 0x7f131cb5

.field public static final cs_rnsign_faceid_05:I = 0x7f131cb6

.field public static final cs_rnsign_faceid_06:I = 0x7f131cb7

.field public static final cs_rnsign_faceid_07:I = 0x7f131cb8

.field public static final cs_rnsign_faceid_08:I = 0x7f131cb9

.field public static final cs_rnsign_faceid_09:I = 0x7f131cba

.field public static final cs_rnsign_home_01:I = 0x7f131cbb

.field public static final cs_rnsign_home_02:I = 0x7f131cbc

.field public static final cs_rnsign_info_01:I = 0x7f131cbd

.field public static final cs_rnsign_info_02:I = 0x7f131cbe

.field public static final cs_rnsign_info_03:I = 0x7f131cbf

.field public static final cs_rnsign_info_04:I = 0x7f131cc0

.field public static final cs_rnsign_info_05:I = 0x7f131cc1

.field public static final cs_rnsign_info_status_verified:I = 0x7f131cc2

.field public static final cs_rnsign_intro_01:I = 0x7f131cc3

.field public static final cs_rnsign_intro_02:I = 0x7f131cc4

.field public static final cs_rnsign_intro_03:I = 0x7f131cc5

.field public static final cs_rnsign_introduction_01:I = 0x7f131cc6

.field public static final cs_rnsign_introduction_02:I = 0x7f131cc7

.field public static final cs_rnsign_introduction_03:I = 0x7f131cc8

.field public static final cs_rnsign_introduction_04:I = 0x7f131cc9

.field public static final cs_rnsign_introduction_05:I = 0x7f131cca

.field public static final cs_rnsign_introduction_06:I = 0x7f131ccb

.field public static final cs_rnsign_introduction_07:I = 0x7f131ccc

.field public static final cs_rnsign_introduction_08:I = 0x7f131ccd

.field public static final cs_rnsign_introduction_09:I = 0x7f131cce

.field public static final cs_rnsign_introduction_10:I = 0x7f131ccf

.field public static final cs_rnsign_introduction_11:I = 0x7f131cd0

.field public static final cs_rnsign_invite_01:I = 0x7f131cd1

.field public static final cs_rnsign_invite_02:I = 0x7f131cd2

.field public static final cs_rnsign_logo:I = 0x7f131cd3

.field public static final cs_rnsign_sign_01:I = 0x7f131cd4

.field public static final cs_rnsign_tips_01:I = 0x7f131cd5

.field public static final cs_rnsign_tips_02:I = 0x7f131cd6

.field public static final cs_rnsign_tips_03:I = 0x7f131cd7

.field public static final cs_rnsign_tips_04:I = 0x7f131cd8

.field public static final cs_rnsign_tips_05:I = 0x7f131cd9

.field public static final cs_rnsign_tips_08:I = 0x7f131cda

.field public static final cs_rnsign_tips_realname_agreement:I = 0x7f131cdb

.field public static final cs_save_share_ok:I = 0x7f131cdc

.field public static final cs_scan_book_mode_demo_title:I = 0x7f131cdd

.field public static final cs_scan_invoice_title:I = 0x7f131cde

.field public static final cs_send_pc_limit_tips:I = 0x7f131cdf

.field public static final cs_smart_erase_cam_descrip_title:I = 0x7f131ce0

.field public static final cs_t21_main_idcard_doc_tips:I = 0x7f131ce1

.field public static final cs_t23_expire:I = 0x7f131ce2

.field public static final cs_t23_main_idcard_doc_tips:I = 0x7f131ce3

.field public static final cs_t23_main_idcard_doc_title:I = 0x7f131ce4

.field public static final cs_t23_premium_expire_pop:I = 0x7f131ce5

.field public static final cs_t23_use_now:I = 0x7f131ce6

.field public static final cs_t24_coupon_180:I = 0x7f131ce7

.field public static final cs_t24_coupon_nobuy_toast:I = 0x7f131ce8

.field public static final cs_t25_Recognition:I = 0x7f131ce9

.field public static final cs_t26_tools_word:I = 0x7f131cea

.field public static final cs_t27_list_operationa:I = 0x7f131ceb

.field public static final cs_who_sign_01:I = 0x7f131cec

.field public static final cs_who_sign_02:I = 0x7f131ced

.field public static final cs_who_sign_03:I = 0x7f131cee

.field public static final cs_who_sign_04:I = 0x7f131cef

.field public static final cspdf_102_screen:I = 0x7f131cf0

.field public static final db_full:I = 0x7f131cf1

.field public static final db_full_import:I = 0x7f131cf2

.field public static final default_title:I = 0x7f131cf3

.field public static final delete_dialog_alert:I = 0x7f131cf5

.field public static final delete_dialog_cancel:I = 0x7f131cf6

.field public static final delete_dialog_message_normal:I = 0x7f131cf7

.field public static final delete_dialog_ok:I = 0x7f131cf8

.field public static final delete_file:I = 0x7f131cf9

.field public static final deleteing_msg:I = 0x7f131cfa

.field public static final details_ok:I = 0x7f131cfb

.field public static final dialog_cancel:I = 0x7f131cfc

.field public static final dialog_collage_style_content:I = 0x7f131cfd

.field public static final dialog_collage_style_title:I = 0x7f131cfe

.field public static final dialog_enhance_mode:I = 0x7f131cff

.field public static final dialog_my_certification_content:I = 0x7f131d00

.field public static final dialog_ok:I = 0x7f131d01

.field public static final dialog_processing_title:I = 0x7f131d02

.field public static final dialog_title_option:I = 0x7f131d03

.field public static final dir_check_error_msg:I = 0x7f131d04

.field public static final dir_check_error_title:I = 0x7f131d05

.field public static final dissagree_privacy_dialog_confirm:I = 0x7f131d06

.field public static final dissagree_privacy_dialog_content:I = 0x7f131d07

.field public static final dissagree_privacy_dialog_content2:I = 0x7f131d08

.field public static final dissagree_privacy_dialog_title:I = 0x7f131d09

.field public static final dlg_title:I = 0x7f131d10

.field public static final doc_does_not_exist:I = 0x7f131d12

.field public static final edit_hint_water_maker_tip:I = 0x7f131d13

.field public static final edit_jigsaw:I = 0x7f131d14

.field public static final email_body_section7:I = 0x7f131d15

.field public static final email_format_wrong:I = 0x7f131d16

.field public static final empty_page:I = 0x7f131d17

.field public static final error_a11y_label:I = 0x7f131d18

.field public static final error_icon_content_description:I = 0x7f131d19

.field public static final error_title:I = 0x7f131d1a

.field public static final esign_fake20:I = 0x7f131d1d

.field public static final esign_fake23:I = 0x7f131d1e

.field public static final eu_dialog_edit:I = 0x7f131d1f

.field public static final eu_dialog_tips:I = 0x7f131d20

.field public static final exposed_dropdown_menu_content_description:I = 0x7f131d21

.field public static final fab_transformation_scrim_behavior:I = 0x7f131d22

.field public static final fab_transformation_sheet_behavior:I = 0x7f131d23

.field public static final failed_to_download:I = 0x7f131d24

.field public static final fcm_fallback_notification_channel_label:I = 0x7f131d28

.field public static final fcm_notification_channel_id:I = 0x7f131d29

.field public static final fcm_notification_channel_name:I = 0x7f131d2a

.field public static final file_read_error:I = 0x7f131d2b

.field public static final find_pwd_btn:I = 0x7f131d2c

.field public static final find_pwd_title:I = 0x7f131d2d

.field public static final flash_auto:I = 0x7f131d2f

.field public static final flash_off:I = 0x7f131d30

.field public static final flash_on:I = 0x7f131d31

.field public static final folder_name_certification:I = 0x7f131d32

.field public static final go_open:I = 0x7f131d34

.field public static final go_set_page_open_permission:I = 0x7f131d35

.field public static final go_share:I = 0x7f131d36

.field public static final greet_card_guide_btn_use_now:I = 0x7f131d3b

.field public static final greet_card_picture_is_small:I = 0x7f131d3c

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f131d3d

.field public static final icon_content_description:I = 0x7f131d44

.field public static final import_finish:I = 0x7f131d45

.field public static final import_ing:I = 0x7f131d46

.field public static final import_not_pdf:I = 0x7f131d47

.field public static final import_now:I = 0x7f131d48

.field public static final import_remainder_time:I = 0x7f131d49

.field public static final installed_wx_before_experience:I = 0x7f131d4a

.field public static final item_view_role_description:I = 0x7f131d4b

.field public static final key_app_id:I = 0x7f131d55

.field public static final label_sync_action:I = 0x7f131d81

.field public static final label_sync_title:I = 0x7f131d82

.field public static final least_two_document_selected:I = 0x7f131d83

.field public static final login_account_title:I = 0x7f131d84

.field public static final login_btn:I = 0x7f131d85

.field public static final login_email_btn:I = 0x7f131d86

.field public static final login_fail:I = 0x7f131d87

.field public static final login_in:I = 0x7f131d88

.field public static final login_success:I = 0x7f131d89

.field public static final m3_ref_typeface_brand_medium:I = 0x7f131d8a

.field public static final m3_ref_typeface_brand_regular:I = 0x7f131d8b

.field public static final m3_ref_typeface_plain_medium:I = 0x7f131d8c

.field public static final m3_ref_typeface_plain_regular:I = 0x7f131d8d

.field public static final m3_sys_motion_easing_emphasized:I = 0x7f131d8e

.field public static final m3_sys_motion_easing_emphasized_accelerate:I = 0x7f131d8f

.field public static final m3_sys_motion_easing_emphasized_decelerate:I = 0x7f131d90

.field public static final m3_sys_motion_easing_emphasized_path_data:I = 0x7f131d91

.field public static final m3_sys_motion_easing_legacy:I = 0x7f131d92

.field public static final m3_sys_motion_easing_legacy_accelerate:I = 0x7f131d93

.field public static final m3_sys_motion_easing_legacy_decelerate:I = 0x7f131d94

.field public static final m3_sys_motion_easing_linear:I = 0x7f131d95

.field public static final m3_sys_motion_easing_standard:I = 0x7f131d96

.field public static final m3_sys_motion_easing_standard_accelerate:I = 0x7f131d97

.field public static final m3_sys_motion_easing_standard_decelerate:I = 0x7f131d98

.field public static final material_clock_display_divider:I = 0x7f131d99

.field public static final material_clock_toggle_content_description:I = 0x7f131d9a

.field public static final material_hour_24h_suffix:I = 0x7f131d9b

.field public static final material_hour_selection:I = 0x7f131d9c

.field public static final material_hour_suffix:I = 0x7f131d9d

.field public static final material_minute_selection:I = 0x7f131d9e

.field public static final material_minute_suffix:I = 0x7f131d9f

.field public static final material_motion_easing_accelerated:I = 0x7f131da0

.field public static final material_motion_easing_decelerated:I = 0x7f131da1

.field public static final material_motion_easing_emphasized:I = 0x7f131da2

.field public static final material_motion_easing_linear:I = 0x7f131da3

.field public static final material_motion_easing_standard:I = 0x7f131da4

.field public static final material_slider_range_end:I = 0x7f131da5

.field public static final material_slider_range_start:I = 0x7f131da6

.field public static final material_slider_value:I = 0x7f131da7

.field public static final material_timepicker_am:I = 0x7f131da8

.field public static final material_timepicker_clock_mode_description:I = 0x7f131da9

.field public static final material_timepicker_hour:I = 0x7f131daa

.field public static final material_timepicker_minute:I = 0x7f131dab

.field public static final material_timepicker_pm:I = 0x7f131dac

.field public static final material_timepicker_select_time:I = 0x7f131dad

.field public static final material_timepicker_text_input_mode_description:I = 0x7f131dae

.field public static final max_select_pics_prompt:I = 0x7f131daf

.field public static final menu_remove:I = 0x7f131db0

.field public static final menu_remove_all:I = 0x7f131db1

.field public static final menu_retry:I = 0x7f131db2

.field public static final menu_setting:I = 0x7f131db3

.field public static final menu_team_version:I = 0x7f131db4

.field public static final menu_title_copy:I = 0x7f131db5

.field public static final menu_title_cut:I = 0x7f131db6

.field public static final menu_title_delete:I = 0x7f131db7

.field public static final menu_title_open:I = 0x7f131db8

.field public static final menu_title_reedit:I = 0x7f131db9

.field public static final menu_title_rename:I = 0x7f131dba

.field public static final menu_title_share:I = 0x7f131dbb

.field public static final menu_title_shortcut:I = 0x7f131dbc

.field public static final merging_msg:I = 0x7f131dbd

.field public static final msg_account_pwd_not_match:I = 0x7f131dbe

.field public static final msg_connect_erro:I = 0x7f131dbf

.field public static final msg_googleplay_unavailable:I = 0x7f131dc0

.field public static final msg_team_buy_after_login:I = 0x7f131dc1

.field public static final msg_team_buy_fail:I = 0x7f131dc2

.field public static final msg_team_buy_success:I = 0x7f131dc3

.field public static final mtrl_badge_numberless_content_description:I = 0x7f131dc4

.field public static final mtrl_checkbox_button_icon_path_checked:I = 0x7f131dc5

.field public static final mtrl_checkbox_button_icon_path_group_name:I = 0x7f131dc6

.field public static final mtrl_checkbox_button_icon_path_indeterminate:I = 0x7f131dc7

.field public static final mtrl_checkbox_button_icon_path_name:I = 0x7f131dc8

.field public static final mtrl_checkbox_button_path_checked:I = 0x7f131dc9

.field public static final mtrl_checkbox_button_path_group_name:I = 0x7f131dca

.field public static final mtrl_checkbox_button_path_name:I = 0x7f131dcb

.field public static final mtrl_checkbox_button_path_unchecked:I = 0x7f131dcc

.field public static final mtrl_checkbox_state_description_checked:I = 0x7f131dcd

.field public static final mtrl_checkbox_state_description_indeterminate:I = 0x7f131dce

.field public static final mtrl_checkbox_state_description_unchecked:I = 0x7f131dcf

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f131dd0

.field public static final mtrl_exceed_max_badge_number_content_description:I = 0x7f131dd1

.field public static final mtrl_exceed_max_badge_number_suffix:I = 0x7f131dd2

.field public static final mtrl_picker_a11y_next_month:I = 0x7f131dd3

.field public static final mtrl_picker_a11y_prev_month:I = 0x7f131dd4

.field public static final mtrl_picker_announce_current_range_selection:I = 0x7f131dd5

.field public static final mtrl_picker_announce_current_selection:I = 0x7f131dd6

.field public static final mtrl_picker_announce_current_selection_none:I = 0x7f131dd7

.field public static final mtrl_picker_cancel:I = 0x7f131dd8

.field public static final mtrl_picker_confirm:I = 0x7f131dd9

.field public static final mtrl_picker_date_header_selected:I = 0x7f131dda

.field public static final mtrl_picker_date_header_title:I = 0x7f131ddb

.field public static final mtrl_picker_date_header_unselected:I = 0x7f131ddc

.field public static final mtrl_picker_day_of_week_column_header:I = 0x7f131ddd

.field public static final mtrl_picker_end_date_description:I = 0x7f131dde

.field public static final mtrl_picker_invalid_format:I = 0x7f131ddf

.field public static final mtrl_picker_invalid_format_example:I = 0x7f131de0

.field public static final mtrl_picker_invalid_format_use:I = 0x7f131de1

.field public static final mtrl_picker_invalid_range:I = 0x7f131de2

.field public static final mtrl_picker_navigate_to_current_year_description:I = 0x7f131de3

.field public static final mtrl_picker_navigate_to_year_description:I = 0x7f131de4

.field public static final mtrl_picker_out_of_range:I = 0x7f131de5

.field public static final mtrl_picker_range_header_only_end_selected:I = 0x7f131de6

.field public static final mtrl_picker_range_header_only_start_selected:I = 0x7f131de7

.field public static final mtrl_picker_range_header_selected:I = 0x7f131de8

.field public static final mtrl_picker_range_header_title:I = 0x7f131de9

.field public static final mtrl_picker_range_header_unselected:I = 0x7f131dea

.field public static final mtrl_picker_save:I = 0x7f131deb

.field public static final mtrl_picker_start_date_description:I = 0x7f131dec

.field public static final mtrl_picker_text_input_date_hint:I = 0x7f131ded

.field public static final mtrl_picker_text_input_date_range_end_hint:I = 0x7f131dee

.field public static final mtrl_picker_text_input_date_range_start_hint:I = 0x7f131def

.field public static final mtrl_picker_text_input_day_abbr:I = 0x7f131df0

.field public static final mtrl_picker_text_input_month_abbr:I = 0x7f131df1

.field public static final mtrl_picker_text_input_year_abbr:I = 0x7f131df2

.field public static final mtrl_picker_today_description:I = 0x7f131df3

.field public static final mtrl_picker_toggle_to_calendar_input_mode:I = 0x7f131df4

.field public static final mtrl_picker_toggle_to_day_selection:I = 0x7f131df5

.field public static final mtrl_picker_toggle_to_text_input_mode:I = 0x7f131df6

.field public static final mtrl_picker_toggle_to_year_selection:I = 0x7f131df7

.field public static final mtrl_switch_thumb_group_name:I = 0x7f131df8

.field public static final mtrl_switch_thumb_path_checked:I = 0x7f131df9

.field public static final mtrl_switch_thumb_path_morphing:I = 0x7f131dfa

.field public static final mtrl_switch_thumb_path_name:I = 0x7f131dfb

.field public static final mtrl_switch_thumb_path_pressed:I = 0x7f131dfc

.field public static final mtrl_switch_thumb_path_unchecked:I = 0x7f131dfd

.field public static final mtrl_switch_track_decoration_path:I = 0x7f131dfe

.field public static final mtrl_switch_track_path:I = 0x7f131dff

.field public static final mtrl_timepicker_cancel:I = 0x7f131e00

.field public static final mtrl_timepicker_confirm:I = 0x7f131e01

.field public static final multi_new_document:I = 0x7f131e02

.field public static final multi_new_document_title:I = 0x7f131e03

.field public static final multi_new_pages:I = 0x7f131e04

.field public static final multi_new_pages_title:I = 0x7f131e05

.field public static final no_549_file_too_big_content:I = 0x7f131e09

.field public static final no_backup:I = 0x7f131e0a

.field public static final no_backup_in_file:I = 0x7f131e0b

.field public static final no_check_email_hint:I = 0x7f131e0c

.field public static final no_cs_515_clear_history:I = 0x7f131e0d

.field public static final no_cs_515_message_log_in_gift:I = 0x7f131e0e

.field public static final no_cs_515_search_history:I = 0x7f131e0f

.field public static final no_cs_515_title_tasks:I = 0x7f131e10

.field public static final no_cs_518c_filter:I = 0x7f131e11

.field public static final no_cs_518c_image:I = 0x7f131e12

.field public static final no_cs_5205_grid_line:I = 0x7f131e13

.field public static final no_cs_5205_signature:I = 0x7f131e14

.field public static final no_cs_5205_signature_delete:I = 0x7f131e15

.field public static final no_cs_521_guide_lite_02:I = 0x7f131e16

.field public static final no_cs_521_guide_lite_03:I = 0x7f131e17

.field public static final no_cs_521_guide_lite_07:I = 0x7f131e18

.field public static final no_cs_521_guide_lite_08:I = 0x7f131e19

.field public static final no_cs_521_guide_lite_09:I = 0x7f131e1a

.field public static final no_cs_523_poppicture_01:I = 0x7f131e1b

.field public static final no_cs_523_poppicture_02:I = 0x7f131e1c

.field public static final no_cs_523_poppicture_03:I = 0x7f131e1d

.field public static final no_cs_523_poppicture_04:I = 0x7f131e1e

.field public static final no_cs_523_poppicture_05:I = 0x7f131e1f

.field public static final no_cs_523_poppicture_06:I = 0x7f131e20

.field public static final no_cs_523_word:I = 0x7f131e21

.field public static final no_cs_529_other_login_method:I = 0x7f131e22

.field public static final no_cs_530_book_button_come_tomorrow:I = 0x7f131e23

.field public static final no_cs_530_book_hint_vertical:I = 0x7f131e24

.field public static final no_cs_545_torch_off:I = 0x7f131e25

.field public static final no_cs_545_torch_on:I = 0x7f131e26

.field public static final no_document_selected:I = 0x7f131e27

.field public static final no_storage:I = 0x7f131e28

.field public static final no_update:I = 0x7f131e29

.field public static final not_enough_space:I = 0x7f131e2a

.field public static final not_open_install_permission:I = 0x7f131e2b

.field public static final ok:I = 0x7f131e36

.field public static final one_login_agree_service:I = 0x7f131e37

.field public static final one_login_fast_login:I = 0x7f131e38

.field public static final one_login_mail:I = 0x7f131e39

.field public static final one_login_more_login_way:I = 0x7f131e3a

.field public static final one_login_one_key_login:I = 0x7f131e3b

.field public static final one_login_other_phone_login:I = 0x7f131e3c

.field public static final one_login_privacy_01:I = 0x7f131e3d

.field public static final one_login_privacy_02:I = 0x7f131e3e

.field public static final one_login_wx:I = 0x7f131e3f

.field public static final open_pdf:I = 0x7f131e40

.field public static final page_delete_dialog_title:I = 0x7f131e42

.field public static final page_view_next:I = 0x7f131e43

.field public static final password_toggle_content_description:I = 0x7f131e44

.field public static final path_password_eye:I = 0x7f131e45

.field public static final path_password_eye_mask_strike_through:I = 0x7f131e46

.field public static final path_password_eye_mask_visible:I = 0x7f131e47

.field public static final path_password_strike_through:I = 0x7f131e48

.field public static final payment_method_alipay_item:I = 0x7f131e49

.field public static final payment_method_china_mobile_item:I = 0x7f131e4a

.field public static final payment_method_market:I = 0x7f131e4b

.field public static final payment_method_unioncard_item:I = 0x7f131e4c

.field public static final payment_method_weixin_item:I = 0x7f131e4d

.field public static final pdf_create_error_msg:I = 0x7f131e4e

.field public static final preparing_sd:I = 0x7f131e4f

.field public static final pull_to_refresh_pull_label:I = 0x7f131e51

.field public static final pwd_format_wrong:I = 0x7f131e52

.field public static final recommend_msg:I = 0x7f131e53

.field public static final register_btn:I = 0x7f131e54

.field public static final register_fail:I = 0x7f131e55

.field public static final register_in:I = 0x7f131e56

.field public static final remind_title:I = 0x7f131e57

.field public static final rename_dialog_cancel:I = 0x7f131e58

.field public static final rename_dialog_ok:I = 0x7f131e59

.field public static final rename_dialog_text:I = 0x7f131e5a

.field public static final rename_error_msg:I = 0x7f131e5b

.field public static final rename_merge_dialog:I = 0x7f131e5c

.field public static final resend_activate_email_btn:I = 0x7f131e5f

.field public static final review_done:I = 0x7f131e60

.field public static final review_retake:I = 0x7f131e61

.field public static final s_635_account_tip13:I = 0x7f131e69

.field public static final save_result:I = 0x7f131e6a

.field public static final scan_activity_title:I = 0x7f131e6b

.field public static final sdcard_failed:I = 0x7f131e6c

.field public static final search_menu_title:I = 0x7f131e6d

.field public static final searchbar_scrolling_view_behavior:I = 0x7f131e6e

.field public static final searchview_clear_text_content_description:I = 0x7f131e6f

.field public static final searchview_navigation_content_description:I = 0x7f131e70

.field public static final send_email_for_register:I = 0x7f131e71

.field public static final sending_email:I = 0x7f131e72

.field public static final service_notification_message:I = 0x7f131e73

.field public static final service_notification_title:I = 0x7f131e74

.field public static final setting_about:I = 0x7f131e78

.field public static final setting_advice:I = 0x7f131e79

.field public static final setting_batch_mode_summary:I = 0x7f131e7c

.field public static final setting_enhance_mode:I = 0x7f131e7d

.field public static final setting_manage_account:I = 0x7f131e7e

.field public static final setting_others:I = 0x7f131e7f

.field public static final setting_report:I = 0x7f131e80

.field public static final setting_title:I = 0x7f131e81

.field public static final setting_update:I = 0x7f131e82

.field public static final setting_use_sys_camera:I = 0x7f131e83

.field public static final side_sheet_accessibility_pane_title:I = 0x7f131e84

.field public static final side_sheet_behavior:I = 0x7f131e85

.field public static final sign_in_for_reward:I = 0x7f131e86

.field public static final sign_in_tip:I = 0x7f131e87

.field public static final signature_guid_des:I = 0x7f131e88

.field public static final signature_guid_title:I = 0x7f131e89

.field public static final signature_only_local_des:I = 0x7f131e8a

.field public static final signature_pick_photo:I = 0x7f131e8b

.field public static final signature_take_photo:I = 0x7f131e8c

.field public static final signature_take_photo_decs:I = 0x7f131e8d

.field public static final state_completed:I = 0x7f131ec0

.field public static final state_downloading:I = 0x7f131ec1

.field public static final state_empty:I = 0x7f131ec2

.field public static final state_failed:I = 0x7f131ec3

.field public static final state_failed_file_not_found:I = 0x7f131ec4

.field public static final state_failed_size_limit:I = 0x7f131ec5

.field public static final state_processing:I = 0x7f131ec6

.field public static final state_queued:I = 0x7f131ec7

.field public static final state_retry:I = 0x7f131ec8

.field public static final state_uploading:I = 0x7f131ec9

.field public static final status_bar_notification_info_overflow:I = 0x7f131eca

.field public static final step_enhance:I = 0x7f131ecb

.field public static final step_trim:I = 0x7f131ecc

.field public static final summary_about:I = 0x7f131ecd

.field public static final summary_card_recovery_guide:I = 0x7f131ece

.field public static final summary_enhance_mode:I = 0x7f131ecf

.field public static final summary_manage_accounts:I = 0x7f131ed0

.field public static final summary_update:I = 0x7f131ed1

.field public static final summary_use_sys_camera:I = 0x7f131ed2

.field public static final tag_errmessage_titleexists:I = 0x7f131ed3

.field public static final tag_errmessage_titlenull:I = 0x7f131ed4

.field public static final tat_set_dialog:I = 0x7f131ed5

.field public static final team_account_hint:I = 0x7f131ed6

.field public static final team_reviewed:I = 0x7f131ed7

.field public static final team_unreviewed:I = 0x7f131ed8

.field public static final template:I = 0x7f131ed9

.field public static final text_ellipsis:I = 0x7f131eda

.field public static final time_out:I = 0x7f131edb

.field public static final tip_upload_pdf:I = 0x7f131edc

.field public static final tip_upload_pdf_max_length:I = 0x7f131edd

.field public static final title_buy_free:I = 0x7f131ede

.field public static final title_purchase_method:I = 0x7f131ee0

.field public static final title_update:I = 0x7f131ee1

.field public static final title_upload_pdf_pwd:I = 0x7f131ee2

.field public static final toast_buy_3000_points_success:I = 0x7f131ee3

.field public static final topic_generate:I = 0x7f131ee4

.field public static final topic_save_image_only:I = 0x7f131ee5

.field public static final trim_checkbox_tip:I = 0x7f131ee8

.field public static final try_later:I = 0x7f131ee9

.field public static final tv_change_1G_clound:I = 0x7f131f7f

.field public static final tv_points_left:I = 0x7f131f80

.field public static final tv_points_no_enough_tips:I = 0x7f131f81

.field public static final tv_points_price:I = 0x7f131f82

.field public static final tv_vip_only:I = 0x7f131f83

.field public static final tv_vip_only_tips:I = 0x7f131f84

.field public static final tv_vouchers_tips:I = 0x7f131f85

.field public static final upload_begin_msg:I = 0x7f131f86

.field public static final upload_title:I = 0x7f131f87

.field public static final username_hint:I = 0x7f131f8d

.field public static final util_a_label_send_to_wechat_timeline:I = 0x7f131f8e

.field public static final util_a_msg_no_third_share_app:I = 0x7f131f8f

.field public static final util_a_title_dlg_share_to:I = 0x7f131f90

.field public static final valid_period:I = 0x7f131f91

.field public static final verify_failure:I = 0x7f131f92

.field public static final verify_success_msg:I = 0x7f131f93

.field public static final warning_dialog_msg:I = 0x7f131f97

.field public static final warning_dialog_title:I = 0x7f131f98

.field public static final web_636_tips_connect_us_01:I = 0x7f131f9a

.field public static final web_636_tips_connect_us_02:I = 0x7f131f9b

.field public static final web_a_label_menu_copy_link:I = 0x7f131f9c

.field public static final will_open_wx_small_routine:I = 0x7f131fa6


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

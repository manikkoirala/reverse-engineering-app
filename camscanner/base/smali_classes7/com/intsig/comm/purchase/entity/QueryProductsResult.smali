.class public Lcom/intsig/comm/purchase/entity/QueryProductsResult;
.super Ljava/lang/Object;
.source "QueryProductsResult.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecallCN;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfo;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCn;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$AnniversaryPromotion;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$AiPromotion;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremiumBanner;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremium;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$RecurringUser;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldwideShareDonePriceInfo;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldwideShareDone;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldwideMonthlyScanDonePriceInfo;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldwideMonthlyScanDone;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$FreePdfShareTimes;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$AbroadCancelPayStay;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$AreaFreeActivityPop;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$FreeRemoveWatermarkTimes;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$LicencePopShow;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$EduClaimPop;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$EduMeTab;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$EduCfgBanner;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$EduCfg;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdNewUsersProcess;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceStyle;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPop;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceShow;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipBundle;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription656;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$OfficeMemberVipInfo;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipLevel;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$WebPopWindow;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$GuidePopDelay;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRenew;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlShowTimes;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdReward;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$WatermarkPlusPop;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$WatermarkPlusList;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResultObj;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResult;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$GuideInfo;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$Guide;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$MePrice;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$CountDownItem;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$GuideTexts;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$GuideStyleNew;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$CountDownPopup;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$CloudOverrun;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$IconItem;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$OSPriceRecall;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceRecall;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$ExpirePrice;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPopup;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipButtonColor;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$RegionTextColor;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceStr;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPrice;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductId;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$NewGuideItemInfo;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$NewGuideItem;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$NewGuideInfo;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdvertiseStyle;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$WebOutlineProducts;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$WebProducts;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$UserAttendanceWeek;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$NoSvipPrice;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$UnionMember;,
        Lcom/intsig/comm/purchase/entity/QueryProductsResult$ForceInfo;
    }
.end annotation


# instance fields
.field public abroad_cancel_pay_stay:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AbroadCancelPayStay;

.field public ad_new_users_process:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdNewUsersProcess;

.field public ad_premium_pop:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPop;

.field public advertise_style:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdvertiseStyle;

.field public after_scan_premiumpage:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;

.field public ai_promotion:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AiPromotion;

.field public anniversary_promotion:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AnniversaryPromotion;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public area_free_activity_pop:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AreaFreeActivityPop;

.field public capacity_overrun_popup:Lcom/intsig/comm/purchase/entity/QueryProductsResult$CloudOverrun;

.field public content_style:I

.field public countdown_popup:Lcom/intsig/comm/purchase/entity/QueryProductsResult$CountDownPopup;

.field public edu_cfg:Lcom/intsig/comm/purchase/entity/QueryProductsResult$EduCfg;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public expire_price:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ExpirePrice;

.field public extra_guide:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public extra_guide_cn:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public force_login_new:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ForceInfo;

.field public free_pdf_share_times:Lcom/intsig/comm/purchase/entity/QueryProductsResult$FreePdfShareTimes;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public free_remove_watermark_times:Lcom/intsig/comm/purchase/entity/QueryProductsResult$FreeRemoveWatermarkTimes;

.field public guide:Lcom/intsig/comm/purchase/entity/QueryProductsResult$Guide;

.field public guide_cancel:I

.field public guide_pop_delay:Lcom/intsig/comm/purchase/entity/QueryProductsResult$GuidePopDelay;

.field public guide_price_style:I

.field public guide_style:Lcom/intsig/comm/purchase/entity/QueryProductsResult$GuideStyleNew;

.field public guide_vip_gift:I

.field public license_pop_show:Lcom/intsig/comm/purchase/entity/QueryProductsResult$LicencePopShow;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public lifetime:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public main_file_activity_pop:I

.field public me_price_recall:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceRecall;

.field public me_price_recall_os:Lcom/intsig/comm/purchase/entity/QueryProductsResult$OSPriceRecall;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public month:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public monthly_promotion_pop_cn:Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCn;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public ms:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public new_advertise_cn_pop:Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;

.field public new_copywriting:I

.field public new_guide:Lcom/intsig/comm/purchase/entity/QueryProductsResult$NewGuideInfo;

.field public nosvip_price:Lcom/intsig/comm/purchase/entity/QueryProductsResult$NoSvipPrice;

.field public office_member_vip:Lcom/intsig/comm/purchase/entity/QueryProductsResult$OfficeMemberVipInfo;

.field public point:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public price_copywriting:I

.field public price_full_screen:I

.field public product_description:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription;

.field public product_list:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResult;

.field public recall_price:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ExpirePrice;

.field public recurring_user:Lcom/intsig/comm/purchase/entity/QueryProductsResult$RecurringUser;

.field public recurring_user_pop_538_ab:I

.field public renew_recall:Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;

.field public renew_recall_cn:Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecallCN;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public renew_trial_pop:Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRenew;

.field public reward_ads:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdReward;

.field public share_activity_id:I

.field public show_guide:I

.field public trial:I

.field public union_member:Lcom/intsig/comm/purchase/entity/QueryProductsResult$UnionMember;

.field public user_attendance_week:Lcom/intsig/comm/purchase/entity/QueryProductsResult$UserAttendanceWeek;

.field public version:Ljava/lang/String;

.field public video_guide_premium:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremium;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public vip_bundle:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipBundle;

.field public vip_level:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipLevel;

.field public vip_popup:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPopup;

.field public vip_popup_guide:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPopup;

.field public vip_price_recall:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceRecall;

.field public watermark_plus_list:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WatermarkPlusList;

.field public watermark_plus_pop:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WatermarkPlusPop;

.field public web_pop_window:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WebPopWindow;

.field public web_premium_outline_style_3:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WebOutlineProducts;

.field public web_products:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WebProducts;

.field public week:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public worldwide_monthly_membership_activity:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;

.field public worldwide_monthly_scandone:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldwideMonthlyScanDone;

.field public worldwide_sharedone:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldwideShareDone;

.field public ws:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public ws_discount:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public year:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public year_24h:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public year_48h:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public year_48hdiscount:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public year_guide:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public year_guide_cn:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public year_recall:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

.field public ys:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public isGpPurchaseGuideCancel()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->guide_cancel:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isShowGuideGp()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->show_guide:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isTrialYear()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->trial:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

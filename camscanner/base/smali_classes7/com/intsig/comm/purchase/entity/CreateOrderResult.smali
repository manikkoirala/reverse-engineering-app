.class public Lcom/intsig/comm/purchase/entity/CreateOrderResult;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "CreateOrderResult.java"


# instance fields
.field public attach_data:Ljava/lang/String;

.field public client_id:Ljava/lang/String;

.field public coupon_is_locked:I

.field public currency:Ljava/lang/String;

.field public discount:D

.field private mPayWay:I

.field public notify_url:Ljava/lang/String;

.field public price:D

.field public sign:Ljava/lang/String;

.field public total_amount:D

.field public uniq_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/comm/purchase/entity/CreateOrderResult;->mPayWay:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 3
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    const/4 p1, 0x0

    .line 4
    iput p1, p0, Lcom/intsig/comm/purchase/entity/CreateOrderResult;->mPayWay:I

    return-void
.end method


# virtual methods
.method public getPayWay()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/comm/purchase/entity/CreateOrderResult;->mPayWay:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setPayWay(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/comm/purchase/entity/CreateOrderResult;->mPayWay:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

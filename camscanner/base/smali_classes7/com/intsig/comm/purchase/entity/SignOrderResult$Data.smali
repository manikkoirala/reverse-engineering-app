.class public Lcom/intsig/comm/purchase/entity/SignOrderResult$Data;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "SignOrderResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/comm/purchase/entity/SignOrderResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Data"
.end annotation


# instance fields
.field public noncestr:Ljava/lang/String;

.field public notify_token:Ljava/lang/String;

.field public out_trade_no:Ljava/lang/String;

.field public packageValue:Ljava/lang/String;

.field public partnerid:Ljava/lang/String;

.field public pay_param:Ljava/lang/String;

.field public prepay_id:Ljava/lang/String;

.field public sign:Ljava/lang/String;

.field public timestamp:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

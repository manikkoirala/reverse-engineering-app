.class public Lcom/intsig/office/common/bulletnumber/ListData;
.super Ljava/lang/Object;
.source "ListData.java"


# instance fields
.field private isNumber:Z

.field private levels:[Lcom/intsig/office/common/bulletnumber/ListLevel;

.field private linkStyle:[S

.field private linkStyleID:S

.field private listID:I

.field private normalPreParaLevel:B

.field private preParaLevel:B

.field private simpleList:B


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, -0x1

    .line 5
    iput-short v0, p0, Lcom/intsig/office/common/bulletnumber/ListData;->linkStyleID:S

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public dispose()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/bulletnumber/ListData;->levels:[Lcom/intsig/office/common/bulletnumber/ListLevel;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    array-length v1, v0

    .line 6
    const/4 v2, 0x0

    .line 7
    :goto_0
    if-ge v2, v1, :cond_0

    .line 8
    .line 9
    aget-object v3, v0, v2

    .line 10
    .line 11
    invoke-virtual {v3}, Lcom/intsig/office/common/bulletnumber/ListLevel;->dispose()V

    .line 12
    .line 13
    .line 14
    add-int/lit8 v2, v2, 0x1

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    iput-object v0, p0, Lcom/intsig/office/common/bulletnumber/ListData;->levels:[Lcom/intsig/office/common/bulletnumber/ListLevel;

    .line 19
    .line 20
    :cond_1
    return-void
    .line 21
.end method

.method public getLevel(I)Lcom/intsig/office/common/bulletnumber/ListLevel;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/bulletnumber/ListData;->levels:[Lcom/intsig/office/common/bulletnumber/ListLevel;

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    if-ge p1, v1, :cond_0

    .line 5
    .line 6
    aget-object p1, v0, p1

    .line 7
    .line 8
    return-object p1

    .line 9
    :cond_0
    const/4 p1, 0x0

    .line 10
    return-object p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getLevels()[Lcom/intsig/office/common/bulletnumber/ListLevel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/bulletnumber/ListData;->levels:[Lcom/intsig/office/common/bulletnumber/ListLevel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLinkStyle()[S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/bulletnumber/ListData;->linkStyle:[S

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLinkStyleID()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/common/bulletnumber/ListData;->linkStyleID:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getListID()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/bulletnumber/ListData;->listID:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNormalPreParaLevel()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/bulletnumber/ListData;->normalPreParaLevel:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPreParaLevel()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/bulletnumber/ListData;->preParaLevel:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSimpleList()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/bulletnumber/ListData;->simpleList:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isNumber()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/bulletnumber/ListData;->isNumber:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public resetForNormalView()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/bulletnumber/ListData;->levels:[Lcom/intsig/office/common/bulletnumber/ListLevel;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    array-length v1, v0

    .line 6
    const/4 v2, 0x0

    .line 7
    const/4 v3, 0x0

    .line 8
    :goto_0
    if-ge v3, v1, :cond_0

    .line 9
    .line 10
    aget-object v4, v0, v3

    .line 11
    .line 12
    invoke-virtual {v4, v2}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setNormalParaCount(I)V

    .line 13
    .line 14
    .line 15
    add-int/lit8 v3, v3, 0x1

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public setLevels([Lcom/intsig/office/common/bulletnumber/ListLevel;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/bulletnumber/ListData;->levels:[Lcom/intsig/office/common/bulletnumber/ListLevel;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLinkStyle([S)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/bulletnumber/ListData;->linkStyle:[S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLinkStyleID(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/common/bulletnumber/ListData;->linkStyleID:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setListID(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/bulletnumber/ListData;->listID:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setNormalPreParaLevel(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/common/bulletnumber/ListData;->normalPreParaLevel:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setNumber(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/bulletnumber/ListData;->isNumber:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPreParaLevel(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/common/bulletnumber/ListData;->preParaLevel:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSimpleList(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/common/bulletnumber/ListData;->simpleList:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;
.super Ljava/lang/Object;
.source "PictureEffectInfo.java"


# instance fields
.field private alpha:Ljava/lang/Integer;

.field private bright:Ljava/lang/Float;

.field private contrast:Ljava/lang/Float;

.field private croppedRect:Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;

.field private grayscl:Ljava/lang/Boolean;

.field private sat:Ljava/lang/Float;

.field private stretch:Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;

.field private threshold:Ljava/lang/Float;

.field private transparentColor:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->croppedRect:Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->grayscl:Ljava/lang/Boolean;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->threshold:Ljava/lang/Float;

    .line 7
    .line 8
    iput-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->sat:Ljava/lang/Float;

    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->bright:Ljava/lang/Float;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->contrast:Ljava/lang/Float;

    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->alpha:Ljava/lang/Integer;

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAlpha()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->alpha:Ljava/lang/Integer;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBlackWhiteThreshold()Ljava/lang/Float;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->threshold:Ljava/lang/Float;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBrightness()Ljava/lang/Float;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->bright:Ljava/lang/Float;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getContrast()Ljava/lang/Float;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->contrast:Ljava/lang/Float;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPictureCroppedInfor()Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->croppedRect:Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPictureStretchInfo()Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->stretch:Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSaturation()Ljava/lang/Float;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->sat:Ljava/lang/Float;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTransparentColor()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->transparentColor:Ljava/lang/Integer;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isGrayScale()Ljava/lang/Boolean;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->grayscl:Ljava/lang/Boolean;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setAlpha(Ljava/lang/Integer;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->alpha:Ljava/lang/Integer;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBlackWhiteThreshold(F)V
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->threshold:Ljava/lang/Float;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBrightness(F)V
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->bright:Ljava/lang/Float;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setContrast(F)V
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->contrast:Ljava/lang/Float;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setGrayScale(Z)V
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->grayscl:Ljava/lang/Boolean;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPictureCroppedInfor(Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->croppedRect:Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPictureStretchInfo(Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->stretch:Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSaturation(F)V
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->sat:Ljava/lang/Float;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTransparentColor(I)V
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->transparentColor:Ljava/lang/Integer;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;
.super Ljava/lang/Object;
.source "PictureEffectInfoFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static getEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;
    .locals 2

    .line 1
    if-eqz p0, :cond_1

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->getEscherProperties()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherProperty;->getPropertyNumber()S

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-ne v1, p1, :cond_0

    .line 28
    .line 29
    return-object v0

    .line 30
    :cond_1
    const/4 p0, 0x0

    .line 31
    return-object p0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static getPictureEffectInfor(Lcom/intsig/office/fc/ddf/EscherOptRecord;)Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;
    .locals 10

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 45
    :cond_0
    new-instance v1, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    invoke-direct {v1}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;-><init>()V

    const/16 v2, 0x102

    .line 46
    invoke-static {p0, v2}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    move-result-object v2

    check-cast v2, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    const/high16 v3, 0x47800000    # 65536.0f

    const/4 v4, 0x0

    if-nez v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    .line 47
    :cond_1
    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    :goto_0
    const/16 v5, 0x100

    .line 48
    invoke-static {p0, v5}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    move-result-object v5

    check-cast v5, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    if-nez v5, :cond_2

    const/4 v5, 0x0

    goto :goto_1

    .line 49
    :cond_2
    invoke-virtual {v5}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v3

    :goto_1
    const/16 v6, 0x103

    .line 50
    invoke-static {p0, v6}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    move-result-object v6

    check-cast v6, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    if-nez v6, :cond_3

    const/4 v6, 0x0

    goto :goto_2

    .line 51
    :cond_3
    invoke-virtual {v6}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v3

    :goto_2
    const/16 v7, 0x101

    .line 52
    invoke-static {p0, v7}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    move-result-object v7

    check-cast v7, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    if-nez v7, :cond_4

    const/4 v7, 0x0

    goto :goto_3

    .line 53
    :cond_4
    invoke-virtual {v7}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v3

    :goto_3
    const/4 v8, 0x1

    cmpl-float v9, v2, v4

    if-nez v9, :cond_6

    cmpl-float v9, v5, v4

    if-nez v9, :cond_6

    cmpl-float v9, v6, v4

    if-nez v9, :cond_6

    cmpl-float v4, v7, v4

    if-eqz v4, :cond_5

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    goto :goto_5

    .line 54
    :cond_6
    :goto_4
    new-instance v4, Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;

    invoke-direct {v4, v2, v5, v6, v7}, Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;-><init>(FFFF)V

    .line 55
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setPictureCroppedInfor(Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;)V

    const/4 v2, 0x1

    :goto_5
    const/16 v4, 0x13f

    .line 56
    invoke-static {p0, v4}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    move-result-object v4

    check-cast v4, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    if-eqz v4, :cond_8

    .line 57
    invoke-virtual {v4}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v4

    and-int/lit8 v4, v4, 0xf

    const/4 v5, 0x4

    if-ne v4, v5, :cond_7

    .line 58
    invoke-virtual {v1, v8}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setGrayScale(Z)V

    :goto_6
    const/4 v2, 0x1

    goto :goto_7

    :cond_7
    const/4 v5, 0x6

    if-ne v4, v5, :cond_8

    const/high16 v2, 0x43000000    # 128.0f

    .line 59
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setBlackWhiteThreshold(F)V

    goto :goto_6

    :cond_8
    :goto_7
    const/16 v4, 0x109

    .line 60
    invoke-static {p0, v4}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    move-result-object v4

    check-cast v4, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    if-eqz v4, :cond_9

    .line 61
    invoke-virtual {v4}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    int-to-float v2, v2

    const/high16 v4, 0x47000000    # 32768.0f

    div-float/2addr v2, v4

    const/high16 v4, 0x437f0000    # 255.0f

    mul-float v2, v2, v4

    invoke-virtual {v1, v2}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setBrightness(F)V

    const/4 v2, 0x1

    :cond_9
    const/16 v4, 0x108

    .line 62
    invoke-static {p0, v4}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    move-result-object v4

    check-cast v4, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    if-eqz v4, :cond_a

    .line 63
    invoke-virtual {v4}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setContrast(F)V

    const/4 v2, 0x1

    :cond_a
    const/16 v3, 0x107

    .line 64
    invoke-static {p0, v3}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    move-result-object p0

    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    if-eqz p0, :cond_b

    .line 65
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result p0

    and-int/lit16 v2, p0, 0xff

    const v3, 0xff00

    and-int/2addr v3, p0

    shr-int/lit8 v3, v3, 0x8

    const/high16 v4, 0xff0000

    and-int/2addr p0, v4

    shr-int/lit8 p0, p0, 0x10

    .line 66
    invoke-static {v2, v3, p0}, Landroid/graphics/Color;->rgb(III)I

    move-result p0

    invoke-virtual {v1, p0}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setTransparentColor(I)V

    goto :goto_8

    :cond_b
    move v8, v2

    :goto_8
    if-eqz v8, :cond_c

    return-object v1

    :cond_c
    return-object v0
.end method

.method public static getPictureEffectInfor(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;
    .locals 10

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 1
    :cond_0
    new-instance v1, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    invoke-direct {v1}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;-><init>()V

    const-string v2, "srcRect"

    .line 2
    invoke-interface {p0, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v2

    const v3, 0x47c35000    # 100000.0f

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v2, :cond_6

    const-string v6, "l"

    .line 3
    invoke-interface {v2, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 4
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v3

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    :goto_0
    const-string v7, "t"

    .line 5
    invoke-interface {v2, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 6
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v3

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    :goto_1
    const-string v8, "r"

    .line 7
    invoke-interface {v2, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 8
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v8, v3

    goto :goto_2

    :cond_3
    const/4 v8, 0x0

    :goto_2
    const-string v9, "b"

    .line 9
    invoke-interface {v2, v9}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 10
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    cmpl-float v9, v6, v5

    if-nez v9, :cond_5

    cmpl-float v9, v7, v5

    if-nez v9, :cond_5

    cmpl-float v9, v8, v5

    if-nez v9, :cond_5

    cmpl-float v9, v2, v5

    if-eqz v9, :cond_6

    .line 11
    :cond_5
    new-instance v9, Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;

    invoke-direct {v9, v6, v7, v8, v2}, Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;-><init>(FFFF)V

    .line 12
    invoke-virtual {v1, v9}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setPictureCroppedInfor(Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;)V

    const/4 v2, 0x1

    goto :goto_4

    :cond_6
    const/4 v2, 0x0

    :goto_4
    const-string v6, "blip"

    .line 13
    invoke-interface {p0, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p0

    const-string v6, "grayscl"

    .line 14
    invoke-interface {p0, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 15
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setGrayScale(Z)V

    const/4 v2, 0x1

    :cond_7
    const-string v6, "biLevel"

    .line 16
    invoke-interface {p0, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v6

    const/high16 v7, 0x437f0000    # 255.0f

    if-eqz v6, :cond_8

    const-string v8, "thresh"

    .line 17
    invoke-interface {v6, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_8

    .line 18
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    mul-float v2, v2, v7

    invoke-virtual {v1, v2}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setBlackWhiteThreshold(F)V

    const/4 v2, 0x1

    :cond_8
    const-string v6, "lum"

    .line 19
    invoke-interface {p0, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v6

    if-eqz v6, :cond_b

    const-string v8, "bright"

    .line 20
    invoke-interface {v6, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_9

    .line 21
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    mul-float v2, v2, v7

    .line 22
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setBrightness(F)V

    const/4 v2, 0x1

    :cond_9
    const-string v7, "contrast"

    .line 23
    invoke-interface {v6, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 24
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v5, v2, v5

    if-lez v5, :cond_a

    const/high16 v5, 0x41100000    # 9.0f

    mul-float v2, v2, v5

    add-float/2addr v2, v3

    .line 25
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setContrast(F)V

    goto :goto_5

    :cond_a
    add-float/2addr v2, v3

    .line 26
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setContrast(F)V

    :goto_5
    const/4 v2, 0x1

    :cond_b
    const-string v3, "clrChange"

    .line 27
    invoke-interface {p0, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p0

    if-eqz p0, :cond_c

    const-string v3, "clrFrom"

    .line 28
    invoke-interface {p0, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p0

    if-eqz p0, :cond_c

    .line 29
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v2

    invoke-virtual {v2, v0, p0}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getColor(Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)I

    move-result p0

    invoke-virtual {v1, p0}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setTransparentColor(I)V

    goto :goto_6

    :cond_c
    move v4, v2

    :goto_6
    if-eqz v4, :cond_d

    return-object v1

    :cond_d
    return-object v0
.end method

.method public static getPictureEffectInfor(Lcom/intsig/office/fc/hwpf/usermodel/Picture;)Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;
    .locals 9

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 30
    :cond_0
    new-instance v1, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    invoke-direct {v1}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;-><init>()V

    .line 31
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/Picture;->getDxaCropLeft()F

    move-result v2

    .line 32
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/Picture;->getDyaCropTop()F

    move-result v3

    .line 33
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/Picture;->getDxaCropRight()F

    move-result v4

    .line 34
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/Picture;->getDyaCropBottom()F

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    cmpl-float v8, v2, v6

    if-nez v8, :cond_2

    cmpl-float v8, v3, v6

    if-nez v8, :cond_2

    cmpl-float v8, v4, v6

    if-nez v8, :cond_2

    cmpl-float v6, v5, v6

    if-eqz v6, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 35
    :cond_2
    :goto_0
    new-instance v6, Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;

    invoke-direct {v6, v2, v3, v4, v5}, Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;-><init>(FFFF)V

    .line 36
    invoke-virtual {v1, v6}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setPictureCroppedInfor(Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;)V

    const/4 v2, 0x1

    .line 37
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->isSetBright()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 38
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->getBright()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setBrightness(F)V

    const/4 v2, 0x1

    .line 39
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->isSetContrast()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 40
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->getContrast()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setContrast(F)V

    const/4 v2, 0x1

    .line 41
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->isSetGrayScl()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 42
    invoke-virtual {v1, v7}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setGrayScale(Z)V

    const/4 v2, 0x1

    .line 43
    :cond_5
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->isSetThreshold()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 44
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->getThreshold()F

    move-result p0

    invoke-virtual {v1, p0}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setBlackWhiteThreshold(F)V

    goto :goto_2

    :cond_6
    move v7, v2

    :goto_2
    if-eqz v7, :cond_7

    return-object v1

    :cond_7
    return-object v0
.end method

.method public static getPictureEffectInfor_ImageData(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;
    .locals 9

    .line 1
    if-eqz p0, :cond_f

    .line 2
    .line 3
    new-instance v0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 4
    .line 5
    invoke-direct {v0}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v1, "cropleft"

    .line 9
    .line 10
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    const v2, 0x477fff00    # 65535.0f

    .line 15
    .line 16
    .line 17
    const/4 v3, 0x0

    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    div-float/2addr v1, v2

    .line 25
    goto :goto_0

    .line 26
    :cond_0
    const/4 v1, 0x0

    .line 27
    :goto_0
    const-string v4, "croptop"

    .line 28
    .line 29
    invoke-interface {p0, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    if-eqz v4, :cond_1

    .line 34
    .line 35
    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 36
    .line 37
    .line 38
    move-result v4

    .line 39
    div-float/2addr v4, v2

    .line 40
    goto :goto_1

    .line 41
    :cond_1
    const/4 v4, 0x0

    .line 42
    :goto_1
    const-string v5, "cropright"

    .line 43
    .line 44
    invoke-interface {p0, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v5

    .line 48
    if-eqz v5, :cond_2

    .line 49
    .line 50
    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 51
    .line 52
    .line 53
    move-result v5

    .line 54
    div-float/2addr v5, v2

    .line 55
    goto :goto_2

    .line 56
    :cond_2
    const/4 v5, 0x0

    .line 57
    :goto_2
    const-string v6, "cropbottom"

    .line 58
    .line 59
    invoke-interface {p0, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v6

    .line 63
    if-eqz v6, :cond_3

    .line 64
    .line 65
    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 66
    .line 67
    .line 68
    move-result v6

    .line 69
    div-float/2addr v6, v2

    .line 70
    goto :goto_3

    .line 71
    :cond_3
    const/4 v6, 0x0

    .line 72
    :goto_3
    const/4 v7, 0x1

    .line 73
    cmpl-float v8, v1, v3

    .line 74
    .line 75
    if-nez v8, :cond_5

    .line 76
    .line 77
    cmpl-float v8, v4, v3

    .line 78
    .line 79
    if-nez v8, :cond_5

    .line 80
    .line 81
    cmpl-float v8, v5, v3

    .line 82
    .line 83
    if-nez v8, :cond_5

    .line 84
    .line 85
    cmpl-float v3, v6, v3

    .line 86
    .line 87
    if-eqz v3, :cond_4

    .line 88
    .line 89
    goto :goto_4

    .line 90
    :cond_4
    const/4 v1, 0x0

    .line 91
    goto :goto_5

    .line 92
    :cond_5
    :goto_4
    new-instance v3, Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;

    .line 93
    .line 94
    invoke-direct {v3, v1, v4, v5, v6}, Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;-><init>(FFFF)V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setPictureCroppedInfor(Lcom/intsig/office/common/pictureefftect/PictureCroppedInfo;)V

    .line 98
    .line 99
    .line 100
    const/4 v1, 0x1

    .line 101
    :goto_5
    const-string v3, "blacklevel"

    .line 102
    .line 103
    invoke-interface {p0, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v3

    .line 107
    const-string v4, "f"

    .line 108
    .line 109
    if-eqz v3, :cond_7

    .line 110
    .line 111
    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 112
    .line 113
    .line 114
    move-result v1

    .line 115
    const/high16 v5, 0x40000000    # 2.0f

    .line 116
    .line 117
    if-eqz v1, :cond_6

    .line 118
    .line 119
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 120
    .line 121
    .line 122
    move-result v1

    .line 123
    div-float/2addr v1, v2

    .line 124
    goto :goto_6

    .line 125
    :cond_6
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 126
    .line 127
    .line 128
    move-result v1

    .line 129
    :goto_6
    mul-float v1, v1, v5

    .line 130
    .line 131
    const/high16 v3, 0x437f0000    # 255.0f

    .line 132
    .line 133
    mul-float v1, v1, v3

    .line 134
    .line 135
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setBrightness(F)V

    .line 136
    .line 137
    .line 138
    const/4 v1, 0x1

    .line 139
    :cond_7
    const-string v3, "gain"

    .line 140
    .line 141
    invoke-interface {p0, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object v3

    .line 145
    if-eqz v3, :cond_9

    .line 146
    .line 147
    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 148
    .line 149
    .line 150
    move-result v1

    .line 151
    if-eqz v1, :cond_8

    .line 152
    .line 153
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 154
    .line 155
    .line 156
    move-result v1

    .line 157
    div-float/2addr v1, v2

    .line 158
    goto :goto_7

    .line 159
    :cond_8
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 160
    .line 161
    .line 162
    move-result v1

    .line 163
    :goto_7
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setContrast(F)V

    .line 164
    .line 165
    .line 166
    const/4 v1, 0x1

    .line 167
    :cond_9
    const-string v2, "grayscale"

    .line 168
    .line 169
    invoke-interface {p0, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 170
    .line 171
    .line 172
    move-result-object v2

    .line 173
    if-eqz v2, :cond_d

    .line 174
    .line 175
    const-string v3, "t"

    .line 176
    .line 177
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 178
    .line 179
    .line 180
    move-result v4

    .line 181
    const-string v5, "true"

    .line 182
    .line 183
    if-nez v4, :cond_a

    .line 184
    .line 185
    invoke-virtual {v2, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 186
    .line 187
    .line 188
    move-result v2

    .line 189
    if-eqz v2, :cond_d

    .line 190
    .line 191
    :cond_a
    const-string v1, "bilevel"

    .line 192
    .line 193
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 194
    .line 195
    .line 196
    move-result-object v1

    .line 197
    if-eqz v1, :cond_c

    .line 198
    .line 199
    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 200
    .line 201
    .line 202
    move-result v2

    .line 203
    if-nez v2, :cond_b

    .line 204
    .line 205
    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 206
    .line 207
    .line 208
    move-result v1

    .line 209
    if-eqz v1, :cond_c

    .line 210
    .line 211
    :cond_b
    const/high16 v1, 0x43000000    # 128.0f

    .line 212
    .line 213
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setBlackWhiteThreshold(F)V

    .line 214
    .line 215
    .line 216
    goto :goto_8

    .line 217
    :cond_c
    invoke-virtual {v0, v7}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setGrayScale(Z)V

    .line 218
    .line 219
    .line 220
    :goto_8
    const/4 v1, 0x1

    .line 221
    :cond_d
    const-string v2, "chromakey"

    .line 222
    .line 223
    invoke-interface {p0, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 224
    .line 225
    .line 226
    move-result-object p0

    .line 227
    if-eqz p0, :cond_e

    .line 228
    .line 229
    invoke-static {p0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 230
    .line 231
    .line 232
    move-result p0

    .line 233
    invoke-virtual {v0, p0}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setTransparentColor(I)V

    .line 234
    .line 235
    .line 236
    goto :goto_9

    .line 237
    :cond_e
    move v7, v1

    .line 238
    :goto_9
    if-eqz v7, :cond_f

    .line 239
    .line 240
    return-object v0

    .line 241
    :cond_f
    const/4 p0, 0x0

    .line 242
    return-object p0
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.class public Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;
.super Ljava/lang/Object;
.source "PictureStretchInfo.java"


# instance fields
.field private bottomOffset:F

.field private leftOffset:F

.field private rightOffset:F

.field private topOffset:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public getBottomOffset()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->bottomOffset:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLeftOffset()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->leftOffset:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRightOffset()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->rightOffset:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTopOffset()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->topOffset:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setBottomOffset(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->bottomOffset:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLeftOffset(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->leftOffset:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRightOffset(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->rightOffset:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTopOffset(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->topOffset:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public Lcom/intsig/office/common/borders/Borders;
.super Ljava/lang/Object;
.source "Borders.java"


# instance fields
.field private bottom:Lcom/intsig/office/common/borders/Border;

.field private left:Lcom/intsig/office/common/borders/Border;

.field private onType:B

.field private right:Lcom/intsig/office/common/borders/Border;

.field private top:Lcom/intsig/office/common/borders/Border;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public getBottomBorder()Lcom/intsig/office/common/borders/Border;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/borders/Borders;->bottom:Lcom/intsig/office/common/borders/Border;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLeftBorder()Lcom/intsig/office/common/borders/Border;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/borders/Borders;->left:Lcom/intsig/office/common/borders/Border;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOnType()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/borders/Borders;->onType:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRightBorder()Lcom/intsig/office/common/borders/Border;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/borders/Borders;->right:Lcom/intsig/office/common/borders/Border;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTopBorder()Lcom/intsig/office/common/borders/Border;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/borders/Borders;->top:Lcom/intsig/office/common/borders/Border;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setBottomBorder(Lcom/intsig/office/common/borders/Border;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/borders/Borders;->bottom:Lcom/intsig/office/common/borders/Border;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLeftBorder(Lcom/intsig/office/common/borders/Border;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/borders/Borders;->left:Lcom/intsig/office/common/borders/Border;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOnType(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/common/borders/Borders;->onType:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRightBorder(Lcom/intsig/office/common/borders/Border;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/borders/Borders;->right:Lcom/intsig/office/common/borders/Border;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTopBorder(Lcom/intsig/office/common/borders/Border;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/borders/Borders;->top:Lcom/intsig/office/common/borders/Border;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

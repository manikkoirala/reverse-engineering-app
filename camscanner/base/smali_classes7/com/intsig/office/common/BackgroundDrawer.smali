.class public Lcom/intsig/office/common/BackgroundDrawer;
.super Ljava/lang/Object;
.source "BackgroundDrawer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static drawBackground(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/Rect;Lcom/intsig/office/pg/animate/IAnimation;F)Z
    .locals 9

    .line 1
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    move-result-object v8

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-static/range {v1 .. v8}, Lcom/intsig/office/common/BackgroundDrawer;->drawBackground(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/Rect;Lcom/intsig/office/pg/animate/IAnimation;FLandroid/graphics/Paint;)Z

    move-result p0

    return p0
.end method

.method public static drawBackground(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/Rect;Lcom/intsig/office/pg/animate/IAnimation;FLandroid/graphics/Paint;)Z
    .locals 14

    move-object v12, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v8, p7

    const/4 v0, 0x0

    if-eqz v3, :cond_3

    .line 2
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    .line 3
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->isSlideBackgroundFill()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lcom/intsig/office/pg/control/Presentation;

    if-eqz v1, :cond_0

    .line 4
    invoke-virtual {p0, v4}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    const/4 v1, 0x0

    .line 5
    invoke-virtual {p0, v1}, Landroid/graphics/Canvas;->rotate(F)V

    .line 6
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/pg/control/Presentation;

    invoke-virtual {v1}, Lcom/intsig/office/pg/control/Presentation;->getPGModel()Lcom/intsig/office/pg/model/PGModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/office/pg/model/PGModel;->getPageSize()Lcom/intsig/office/java/awt/Dimension;

    move-result-object v1

    .line 7
    iget v5, v1, Lcom/intsig/office/java/awt/Dimension;->width:I

    int-to-float v5, v5

    mul-float v5, v5, p6

    float-to-int v5, v5

    iget v1, v1, Lcom/intsig/office/java/awt/Dimension;->height:I

    int-to-float v1, v1

    mul-float v1, v1, p6

    float-to-int v1, v1

    invoke-virtual {v4, v0, v0, v5, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 8
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    move-result v1

    const/4 v13, 0x1

    packed-switch v1, :pswitch_data_0

    .line 9
    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0

    .line 10
    :pswitch_0
    iget v0, v4, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    .line 11
    iget v1, v4, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    .line 12
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    .line 13
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    .line 14
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getStretch()Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 15
    invoke-virtual {v6}, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->getLeftOffset()F

    move-result v7

    mul-float v7, v7, v5

    add-float/2addr v0, v7

    .line 16
    invoke-virtual {v6}, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->getTopOffset()F

    move-result v7

    mul-float v7, v7, v4

    add-float/2addr v1, v7

    .line 17
    invoke-virtual {v6}, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->getLeftOffset()F

    move-result v7

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float v7, v8, v7

    invoke-virtual {v6}, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->getRightOffset()F

    move-result v9

    sub-float/2addr v7, v9

    mul-float v5, v5, v7

    .line 18
    invoke-virtual {v6}, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->getTopOffset()F

    move-result v7

    sub-float/2addr v8, v7

    invoke-virtual {v6}, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->getBottomOffset()F

    move-result v6

    sub-float/2addr v8, v6

    mul-float v4, v4, v8

    :cond_1
    move v6, v1

    move v9, v4

    move v8, v5

    move v5, v0

    .line 19
    invoke-static {}, Lcom/intsig/office/common/picture/PictureKit;->instance()Lcom/intsig/office/common/picture/PictureKit;

    move-result-object v0

    invoke-virtual {v3, p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getPicture(Lcom/intsig/office/system/IControl;)Lcom/intsig/office/common/picture/Picture;

    move-result-object v4

    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move/from16 v3, p2

    move/from16 v7, p6

    move-object/from16 v11, p5

    invoke-virtual/range {v0 .. v11}, Lcom/intsig/office/common/picture/PictureKit;->drawPicture(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/picture/Picture;FFFFFLcom/intsig/office/common/pictureefftect/PictureEffectInfo;Lcom/intsig/office/pg/animate/IAnimation;)V

    .line 20
    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    return v13

    :pswitch_1
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v8, p7

    .line 21
    invoke-static/range {v0 .. v8}, Lcom/intsig/office/common/BackgroundDrawer;->drawGradientAndTile(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/Rect;Lcom/intsig/office/pg/animate/IAnimation;FLandroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 22
    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    return v13

    .line 23
    :pswitch_2
    invoke-virtual/range {p7 .. p7}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    .line 24
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    move-result v1

    invoke-virtual {v8, v1}, Landroid/graphics/Paint;->setColor(I)V

    if-eqz p5, :cond_2

    .line 25
    invoke-interface/range {p5 .. p5}, Lcom/intsig/office/pg/animate/IAnimation;->getCurrentAnimationInfor()Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->getAlpha()I

    move-result v1

    invoke-virtual {v8, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 26
    :cond_2
    invoke-virtual {p0, v4, v8}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 27
    invoke-virtual {v8, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 28
    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    return v13

    :cond_3
    :goto_0
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private static drawGradientAndTile(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/Rect;Lcom/intsig/office/pg/animate/IAnimation;FLandroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 20

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p4

    .line 4
    .line 5
    move/from16 v2, p6

    .line 6
    .line 7
    move-object/from16 v3, p7

    .line 8
    .line 9
    move-object/from16 v4, p8

    .line 10
    .line 11
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getShader()Lcom/intsig/office/common/bg/AShader;

    .line 12
    .line 13
    .line 14
    move-result-object v5

    .line 15
    if-eqz v5, :cond_11

    .line 16
    .line 17
    instance-of v6, v5, Lcom/intsig/office/common/bg/LinearGradientShader;

    .line 18
    .line 19
    if-eqz v6, :cond_1

    .line 20
    .line 21
    invoke-virtual/range {p8 .. p8}, Landroid/graphics/Paint;->getStrokeWidth()F

    .line 22
    .line 23
    .line 24
    move-result v7

    .line 25
    iget v8, v1, Landroid/graphics/Rect;->left:I

    .line 26
    .line 27
    iget v9, v1, Landroid/graphics/Rect;->right:I

    .line 28
    .line 29
    sub-int/2addr v8, v9

    .line 30
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    .line 31
    .line 32
    .line 33
    move-result v8

    .line 34
    int-to-float v8, v8

    .line 35
    const/high16 v9, 0x40000000    # 2.0f

    .line 36
    .line 37
    cmpg-float v8, v8, v7

    .line 38
    .line 39
    if-gtz v8, :cond_0

    .line 40
    .line 41
    iget v8, v1, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v8, v8

    .line 44
    div-float/2addr v7, v9

    .line 45
    sub-float/2addr v8, v7

    .line 46
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    .line 47
    .line 48
    .line 49
    move-result v8

    .line 50
    iget v9, v1, Landroid/graphics/Rect;->top:I

    .line 51
    .line 52
    int-to-float v9, v9

    .line 53
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    .line 54
    .line 55
    .line 56
    move-result v9

    .line 57
    iget v10, v1, Landroid/graphics/Rect;->right:I

    .line 58
    .line 59
    int-to-float v10, v10

    .line 60
    add-float/2addr v10, v7

    .line 61
    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    .line 62
    .line 63
    .line 64
    move-result v7

    .line 65
    iget v10, v1, Landroid/graphics/Rect;->bottom:I

    .line 66
    .line 67
    int-to-float v10, v10

    .line 68
    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    .line 69
    .line 70
    .line 71
    move-result v10

    .line 72
    invoke-virtual {v1, v8, v9, v7, v10}, Landroid/graphics/Rect;->set(IIII)V

    .line 73
    .line 74
    .line 75
    goto :goto_0

    .line 76
    :cond_0
    iget v8, v1, Landroid/graphics/Rect;->top:I

    .line 77
    .line 78
    iget v10, v1, Landroid/graphics/Rect;->bottom:I

    .line 79
    .line 80
    sub-int/2addr v8, v10

    .line 81
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    .line 82
    .line 83
    .line 84
    move-result v8

    .line 85
    int-to-float v8, v8

    .line 86
    cmpg-float v8, v8, v7

    .line 87
    .line 88
    if-gtz v8, :cond_1

    .line 89
    .line 90
    iget v8, v1, Landroid/graphics/Rect;->left:I

    .line 91
    .line 92
    int-to-float v8, v8

    .line 93
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    .line 94
    .line 95
    .line 96
    move-result v8

    .line 97
    iget v10, v1, Landroid/graphics/Rect;->top:I

    .line 98
    .line 99
    int-to-float v10, v10

    .line 100
    div-float/2addr v7, v9

    .line 101
    sub-float/2addr v10, v7

    .line 102
    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    .line 103
    .line 104
    .line 105
    move-result v9

    .line 106
    iget v10, v1, Landroid/graphics/Rect;->right:I

    .line 107
    .line 108
    int-to-float v10, v10

    .line 109
    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    .line 110
    .line 111
    .line 112
    move-result v10

    .line 113
    iget v11, v1, Landroid/graphics/Rect;->bottom:I

    .line 114
    .line 115
    int-to-float v11, v11

    .line 116
    add-float/2addr v11, v7

    .line 117
    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    .line 118
    .line 119
    .line 120
    move-result v7

    .line 121
    invoke-virtual {v1, v8, v9, v10, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 122
    .line 123
    .line 124
    :cond_1
    :goto_0
    invoke-virtual {v5}, Lcom/intsig/office/common/bg/AShader;->getShader()Landroid/graphics/Shader;

    .line 125
    .line 126
    .line 127
    move-result-object v7

    .line 128
    const/high16 v8, 0x3f800000    # 1.0f

    .line 129
    .line 130
    if-nez v7, :cond_2

    .line 131
    .line 132
    div-float v7, v8, v2

    .line 133
    .line 134
    new-instance v9, Landroid/graphics/Rect;

    .line 135
    .line 136
    iget v10, v1, Landroid/graphics/Rect;->left:I

    .line 137
    .line 138
    int-to-float v10, v10

    .line 139
    mul-float v10, v10, v7

    .line 140
    .line 141
    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    .line 142
    .line 143
    .line 144
    move-result v10

    .line 145
    iget v11, v1, Landroid/graphics/Rect;->top:I

    .line 146
    .line 147
    int-to-float v11, v11

    .line 148
    mul-float v11, v11, v7

    .line 149
    .line 150
    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    .line 151
    .line 152
    .line 153
    move-result v11

    .line 154
    iget v12, v1, Landroid/graphics/Rect;->right:I

    .line 155
    .line 156
    int-to-float v12, v12

    .line 157
    mul-float v12, v12, v7

    .line 158
    .line 159
    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    .line 160
    .line 161
    .line 162
    move-result v12

    .line 163
    iget v13, v1, Landroid/graphics/Rect;->bottom:I

    .line 164
    .line 165
    int-to-float v13, v13

    .line 166
    mul-float v13, v13, v7

    .line 167
    .line 168
    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    .line 169
    .line 170
    .line 171
    move-result v7

    .line 172
    invoke-direct {v9, v10, v11, v12, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 173
    .line 174
    .line 175
    move-object/from16 v7, p1

    .line 176
    .line 177
    move/from16 v10, p2

    .line 178
    .line 179
    invoke-virtual {v5, v7, v10, v9}, Lcom/intsig/office/common/bg/AShader;->createShader(Lcom/intsig/office/system/IControl;ILandroid/graphics/Rect;)Landroid/graphics/Shader;

    .line 180
    .line 181
    .line 182
    move-result-object v7

    .line 183
    if-nez v7, :cond_2

    .line 184
    .line 185
    return-void

    .line 186
    :cond_2
    new-instance v9, Landroid/graphics/Matrix;

    .line 187
    .line 188
    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 189
    .line 190
    .line 191
    iget v10, v1, Landroid/graphics/Rect;->left:I

    .line 192
    .line 193
    int-to-float v10, v10

    .line 194
    iget v11, v1, Landroid/graphics/Rect;->top:I

    .line 195
    .line 196
    int-to-float v11, v11

    .line 197
    instance-of v12, v5, Lcom/intsig/office/common/bg/TileShader;

    .line 198
    .line 199
    if-eqz v12, :cond_3

    .line 200
    .line 201
    move-object v6, v5

    .line 202
    check-cast v6, Lcom/intsig/office/common/bg/TileShader;

    .line 203
    .line 204
    invoke-virtual {v6}, Lcom/intsig/office/common/bg/TileShader;->getOffsetX()I

    .line 205
    .line 206
    .line 207
    move-result v8

    .line 208
    int-to-float v8, v8

    .line 209
    mul-float v8, v8, v2

    .line 210
    .line 211
    add-float/2addr v10, v8

    .line 212
    invoke-virtual {v6}, Lcom/intsig/office/common/bg/TileShader;->getOffsetY()I

    .line 213
    .line 214
    .line 215
    move-result v6

    .line 216
    int-to-float v6, v6

    .line 217
    mul-float v6, v6, v2

    .line 218
    .line 219
    add-float/2addr v11, v6

    .line 220
    invoke-virtual {v9, v2, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 221
    .line 222
    .line 223
    goto/16 :goto_3

    .line 224
    .line 225
    :cond_3
    instance-of v2, v5, Lcom/intsig/office/common/bg/PatternShader;

    .line 226
    .line 227
    if-eqz v2, :cond_4

    .line 228
    .line 229
    goto/16 :goto_3

    .line 230
    .line 231
    :cond_4
    if-eqz v6, :cond_e

    .line 232
    .line 233
    move-object v2, v5

    .line 234
    check-cast v2, Lcom/intsig/office/common/bg/LinearGradientShader;

    .line 235
    .line 236
    invoke-virtual {v2}, Lcom/intsig/office/common/bg/LinearGradientShader;->getAngle()I

    .line 237
    .line 238
    .line 239
    move-result v6

    .line 240
    const/16 v12, 0x5a

    .line 241
    .line 242
    const/16 v13, 0x64

    .line 243
    .line 244
    const/16 v14, 0x32

    .line 245
    .line 246
    const/16 v15, -0x32

    .line 247
    .line 248
    const/16 v16, 0x0

    .line 249
    .line 250
    const/high16 v17, 0x3f000000    # 0.5f

    .line 251
    .line 252
    const/high16 v18, -0x41000000    # -0.5f

    .line 253
    .line 254
    if-ne v6, v12, :cond_9

    .line 255
    .line 256
    invoke-virtual {v2}, Lcom/intsig/office/common/bg/Gradient;->getFocus()I

    .line 257
    .line 258
    .line 259
    move-result v2

    .line 260
    if-eq v2, v15, :cond_8

    .line 261
    .line 262
    if-eqz v2, :cond_5

    .line 263
    .line 264
    if-eq v2, v14, :cond_7

    .line 265
    .line 266
    if-eq v2, v13, :cond_6

    .line 267
    .line 268
    :cond_5
    const/high16 v16, 0x3f800000    # 1.0f

    .line 269
    .line 270
    goto :goto_1

    .line 271
    :cond_6
    const/4 v8, 0x0

    .line 272
    goto :goto_1

    .line 273
    :cond_7
    const/high16 v8, -0x41000000    # -0.5f

    .line 274
    .line 275
    const/high16 v16, -0x41000000    # -0.5f

    .line 276
    .line 277
    goto :goto_1

    .line 278
    :cond_8
    const/high16 v8, 0x3f000000    # 0.5f

    .line 279
    .line 280
    const/high16 v16, 0x3f000000    # 0.5f

    .line 281
    .line 282
    :goto_1
    move/from16 v19, v16

    .line 283
    .line 284
    move/from16 v16, v8

    .line 285
    .line 286
    move/from16 v8, v19

    .line 287
    .line 288
    goto :goto_2

    .line 289
    :cond_9
    invoke-virtual {v2}, Lcom/intsig/office/common/bg/Gradient;->getFocus()I

    .line 290
    .line 291
    .line 292
    move-result v2

    .line 293
    if-eq v2, v15, :cond_d

    .line 294
    .line 295
    if-eqz v2, :cond_a

    .line 296
    .line 297
    if-eq v2, v14, :cond_c

    .line 298
    .line 299
    if-eq v2, v13, :cond_b

    .line 300
    .line 301
    :cond_a
    const/high16 v16, 0x3f800000    # 1.0f

    .line 302
    .line 303
    goto :goto_2

    .line 304
    :cond_b
    const/4 v8, 0x0

    .line 305
    goto :goto_2

    .line 306
    :cond_c
    const/high16 v8, 0x3f000000    # 0.5f

    .line 307
    .line 308
    const/high16 v16, 0x3f000000    # 0.5f

    .line 309
    .line 310
    goto :goto_2

    .line 311
    :cond_d
    const/high16 v8, -0x41000000    # -0.5f

    .line 312
    .line 313
    const/high16 v16, -0x41000000    # -0.5f

    .line 314
    .line 315
    :goto_2
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Rect;->width()I

    .line 316
    .line 317
    .line 318
    move-result v2

    .line 319
    int-to-float v2, v2

    .line 320
    mul-float v8, v8, v2

    .line 321
    .line 322
    add-float/2addr v10, v8

    .line 323
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Rect;->height()I

    .line 324
    .line 325
    .line 326
    move-result v2

    .line 327
    int-to-float v2, v2

    .line 328
    mul-float v16, v16, v2

    .line 329
    .line 330
    add-float v11, v11, v16

    .line 331
    .line 332
    :cond_e
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Rect;->width()I

    .line 333
    .line 334
    .line 335
    move-result v2

    .line 336
    int-to-float v2, v2

    .line 337
    const/high16 v6, 0x42c80000    # 100.0f

    .line 338
    .line 339
    div-float/2addr v2, v6

    .line 340
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Rect;->height()I

    .line 341
    .line 342
    .line 343
    move-result v8

    .line 344
    int-to-float v8, v8

    .line 345
    div-float/2addr v8, v6

    .line 346
    invoke-virtual {v9, v2, v8}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 347
    .line 348
    .line 349
    :goto_3
    invoke-virtual {v9, v10, v11}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 350
    .line 351
    .line 352
    invoke-virtual {v7, v9}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 353
    .line 354
    .line 355
    invoke-virtual {v4, v7}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 356
    .line 357
    .line 358
    invoke-virtual {v5}, Lcom/intsig/office/common/bg/AShader;->getAlpha()I

    .line 359
    .line 360
    .line 361
    move-result v2

    .line 362
    if-eqz p5, :cond_f

    .line 363
    .line 364
    invoke-interface/range {p5 .. p5}, Lcom/intsig/office/pg/animate/IAnimation;->getCurrentAnimationInfor()Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 365
    .line 366
    .line 367
    move-result-object v5

    .line 368
    invoke-virtual {v5}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->getAlpha()I

    .line 369
    .line 370
    .line 371
    move-result v5

    .line 372
    int-to-float v5, v5

    .line 373
    const/high16 v6, 0x437f0000    # 255.0f

    .line 374
    .line 375
    div-float/2addr v5, v6

    .line 376
    int-to-float v2, v2

    .line 377
    mul-float v5, v5, v2

    .line 378
    .line 379
    float-to-int v2, v5

    .line 380
    :cond_f
    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 381
    .line 382
    .line 383
    if-eqz v3, :cond_10

    .line 384
    .line 385
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 386
    .line 387
    .line 388
    goto :goto_4

    .line 389
    :cond_10
    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 390
    .line 391
    .line 392
    :goto_4
    const/4 v0, 0x0

    .line 393
    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 394
    .line 395
    .line 396
    :cond_11
    return-void
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
.end method

.method public static drawLineAndFill(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/shape/AbstractShape;Landroid/graphics/Rect;F)V
    .locals 9

    .line 1
    invoke-virtual {p3}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    .line 12
    .line 13
    .line 14
    move-result-object v8

    .line 15
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 16
    .line 17
    invoke-virtual {v8, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p3}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    int-to-float v0, v0

    .line 29
    mul-float v0, v0, p5

    .line 30
    .line 31
    invoke-virtual {v8, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p3}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 39
    .line 40
    .line 41
    move-result-object v4

    .line 42
    const/4 v6, 0x0

    .line 43
    move-object v1, p0

    .line 44
    move-object v2, p1

    .line 45
    move v3, p2

    .line 46
    move-object v5, p4

    .line 47
    move v7, p5

    .line 48
    invoke-static/range {v1 .. v8}, Lcom/intsig/office/common/BackgroundDrawer;->drawBackground(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/Rect;Lcom/intsig/office/pg/animate/IAnimation;FLandroid/graphics/Paint;)Z

    .line 49
    .line 50
    .line 51
    :cond_0
    invoke-virtual {p3}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    if-eqz v0, :cond_1

    .line 56
    .line 57
    invoke-virtual {p3}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 58
    .line 59
    .line 60
    move-result-object v4

    .line 61
    const/4 v6, 0x0

    .line 62
    move-object v1, p0

    .line 63
    move-object v2, p1

    .line 64
    move v3, p2

    .line 65
    move-object v5, p4

    .line 66
    move v7, p5

    .line 67
    invoke-static/range {v1 .. v7}, Lcom/intsig/office/common/BackgroundDrawer;->drawBackground(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/Rect;Lcom/intsig/office/pg/animate/IAnimation;F)Z

    .line 68
    .line 69
    .line 70
    :cond_1
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
.end method

.method public static drawPathBackground(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/Rect;Lcom/intsig/office/pg/animate/IAnimation;FLandroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 13

    .line 1
    move-object v12, p0

    .line 2
    move-object v2, p1

    .line 3
    move-object/from16 v0, p3

    .line 4
    .line 5
    move-object/from16 v1, p4

    .line 6
    .line 7
    move-object/from16 v3, p7

    .line 8
    .line 9
    move-object/from16 v4, p8

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    .line 15
    .line 16
    .line 17
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->isSlideBackgroundFill()Z

    .line 18
    .line 19
    .line 20
    move-result v5

    .line 21
    if-eqz v5, :cond_1

    .line 22
    .line 23
    if-eqz v2, :cond_1

    .line 24
    .line 25
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    .line 26
    .line 27
    .line 28
    move-result-object v5

    .line 29
    instance-of v5, v5, Lcom/intsig/office/pg/control/Presentation;

    .line 30
    .line 31
    if-eqz v5, :cond_1

    .line 32
    .line 33
    invoke-virtual {p0, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 34
    .line 35
    .line 36
    const/4 v5, 0x0

    .line 37
    invoke-virtual {p0, v5}, Landroid/graphics/Canvas;->rotate(F)V

    .line 38
    .line 39
    .line 40
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    .line 41
    .line 42
    .line 43
    move-result-object v5

    .line 44
    check-cast v5, Lcom/intsig/office/pg/control/Presentation;

    .line 45
    .line 46
    invoke-virtual {v5}, Lcom/intsig/office/pg/control/Presentation;->getPGModel()Lcom/intsig/office/pg/model/PGModel;

    .line 47
    .line 48
    .line 49
    move-result-object v5

    .line 50
    invoke-virtual {v5}, Lcom/intsig/office/pg/model/PGModel;->getPageSize()Lcom/intsig/office/java/awt/Dimension;

    .line 51
    .line 52
    .line 53
    move-result-object v5

    .line 54
    iget v6, v5, Lcom/intsig/office/java/awt/Dimension;->width:I

    .line 55
    .line 56
    int-to-float v6, v6

    .line 57
    mul-float v6, v6, p6

    .line 58
    .line 59
    float-to-int v6, v6

    .line 60
    iget v5, v5, Lcom/intsig/office/java/awt/Dimension;->height:I

    .line 61
    .line 62
    int-to-float v5, v5

    .line 63
    mul-float v5, v5, p6

    .line 64
    .line 65
    float-to-int v5, v5

    .line 66
    const/4 v7, 0x0

    .line 67
    invoke-virtual {v1, v7, v7, v6, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 68
    .line 69
    .line 70
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 71
    .line 72
    .line 73
    move-result v5

    .line 74
    packed-switch v5, :pswitch_data_0

    .line 75
    .line 76
    .line 77
    goto/16 :goto_0

    .line 78
    .line 79
    :pswitch_0
    invoke-virtual {p0, v3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 80
    .line 81
    .line 82
    iget v3, v1, Landroid/graphics/Rect;->left:I

    .line 83
    .line 84
    int-to-float v3, v3

    .line 85
    iget v4, v1, Landroid/graphics/Rect;->top:I

    .line 86
    .line 87
    int-to-float v4, v4

    .line 88
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Rect;->width()I

    .line 89
    .line 90
    .line 91
    move-result v5

    .line 92
    int-to-float v5, v5

    .line 93
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Rect;->height()I

    .line 94
    .line 95
    .line 96
    move-result v1

    .line 97
    int-to-float v1, v1

    .line 98
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getStretch()Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;

    .line 99
    .line 100
    .line 101
    move-result-object v6

    .line 102
    if-eqz v6, :cond_2

    .line 103
    .line 104
    invoke-virtual {v6}, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->getLeftOffset()F

    .line 105
    .line 106
    .line 107
    move-result v7

    .line 108
    mul-float v7, v7, v5

    .line 109
    .line 110
    add-float/2addr v3, v7

    .line 111
    invoke-virtual {v6}, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->getTopOffset()F

    .line 112
    .line 113
    .line 114
    move-result v7

    .line 115
    mul-float v7, v7, v1

    .line 116
    .line 117
    add-float/2addr v4, v7

    .line 118
    invoke-virtual {v6}, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->getLeftOffset()F

    .line 119
    .line 120
    .line 121
    move-result v7

    .line 122
    const/high16 v8, 0x3f800000    # 1.0f

    .line 123
    .line 124
    sub-float v7, v8, v7

    .line 125
    .line 126
    invoke-virtual {v6}, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->getRightOffset()F

    .line 127
    .line 128
    .line 129
    move-result v9

    .line 130
    sub-float/2addr v7, v9

    .line 131
    mul-float v5, v5, v7

    .line 132
    .line 133
    invoke-virtual {v6}, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->getTopOffset()F

    .line 134
    .line 135
    .line 136
    move-result v7

    .line 137
    sub-float/2addr v8, v7

    .line 138
    invoke-virtual {v6}, Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;->getBottomOffset()F

    .line 139
    .line 140
    .line 141
    move-result v6

    .line 142
    sub-float/2addr v8, v6

    .line 143
    mul-float v1, v1, v8

    .line 144
    .line 145
    :cond_2
    move v9, v1

    .line 146
    move v6, v4

    .line 147
    move v8, v5

    .line 148
    move v5, v3

    .line 149
    invoke-static {}, Lcom/intsig/office/common/picture/PictureKit;->instance()Lcom/intsig/office/common/picture/PictureKit;

    .line 150
    .line 151
    .line 152
    move-result-object v1

    .line 153
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getPicture(Lcom/intsig/office/system/IControl;)Lcom/intsig/office/common/picture/Picture;

    .line 154
    .line 155
    .line 156
    move-result-object v4

    .line 157
    const/4 v10, 0x0

    .line 158
    move-object v0, v1

    .line 159
    move-object v1, p0

    .line 160
    move-object v2, p1

    .line 161
    move v3, p2

    .line 162
    move/from16 v7, p6

    .line 163
    .line 164
    move-object/from16 v11, p5

    .line 165
    .line 166
    invoke-virtual/range {v0 .. v11}, Lcom/intsig/office/common/picture/PictureKit;->drawPicture(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/picture/Picture;FFFFFLcom/intsig/office/common/pictureefftect/PictureEffectInfo;Lcom/intsig/office/pg/animate/IAnimation;)V

    .line 167
    .line 168
    .line 169
    goto :goto_0

    .line 170
    :pswitch_1
    invoke-static/range {p0 .. p8}, Lcom/intsig/office/common/BackgroundDrawer;->drawGradientAndTile(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/Rect;Lcom/intsig/office/pg/animate/IAnimation;FLandroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 171
    .line 172
    .line 173
    goto :goto_0

    .line 174
    :pswitch_2
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 175
    .line 176
    .line 177
    move-result v1

    .line 178
    invoke-virtual {v4, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 179
    .line 180
    .line 181
    if-eqz p5, :cond_3

    .line 182
    .line 183
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 184
    .line 185
    .line 186
    move-result v0

    .line 187
    shr-int/lit8 v0, v0, 0x18

    .line 188
    .line 189
    and-int/lit16 v0, v0, 0xff

    .line 190
    .line 191
    invoke-interface/range {p5 .. p5}, Lcom/intsig/office/pg/animate/IAnimation;->getCurrentAnimationInfor()Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 192
    .line 193
    .line 194
    move-result-object v1

    .line 195
    invoke-virtual {v1}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->getAlpha()I

    .line 196
    .line 197
    .line 198
    move-result v1

    .line 199
    int-to-float v1, v1

    .line 200
    const/high16 v2, 0x437f0000    # 255.0f

    .line 201
    .line 202
    div-float/2addr v1, v2

    .line 203
    int-to-float v0, v0

    .line 204
    mul-float v1, v1, v0

    .line 205
    .line 206
    float-to-int v0, v1

    .line 207
    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 208
    .line 209
    .line 210
    :cond_3
    invoke-virtual {p0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 211
    .line 212
    .line 213
    :goto_0
    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    .line 214
    .line 215
    .line 216
    return-void

    .line 217
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
.end method

.class public abstract Lcom/intsig/office/common/bg/Gradient;
.super Lcom/intsig/office/common/bg/AShader;
.source "Gradient.java"


# static fields
.field public static final COORDINATE_LENGTH:I = 0x64


# instance fields
.field protected colors:[I

.field private focus:I

.field protected positions:[F

.field private type:I


# direct methods
.method public constructor <init>([I[F)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/common/bg/AShader;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/common/bg/Gradient;->colors:[I

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/office/common/bg/Gradient;->positions:[F

    .line 8
    .line 9
    const/16 v0, 0x64

    .line 10
    .line 11
    iput v0, p0, Lcom/intsig/office/common/bg/Gradient;->focus:I

    .line 12
    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    array-length v0, p1

    .line 16
    const/4 v1, 0x2

    .line 17
    if-lt v0, v1, :cond_0

    .line 18
    .line 19
    iput-object p1, p0, Lcom/intsig/office/common/bg/Gradient;->colors:[I

    .line 20
    .line 21
    :cond_0
    iput-object p2, p0, Lcom/intsig/office/common/bg/Gradient;->positions:[F

    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public getFocus()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/bg/Gradient;->focus:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getGradientType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/bg/Gradient;->type:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setFocus(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/bg/Gradient;->focus:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setGradientType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/bg/Gradient;->type:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

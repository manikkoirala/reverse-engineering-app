.class public Lcom/intsig/office/common/bg/BackgroundAndFill;
.super Ljava/lang/Object;
.source "BackgroundAndFill.java"


# static fields
.field public static final FILL_BACKGROUND:B = 0x9t

.field public static final FILL_NO:B = -0x1t

.field public static final FILL_PATTERN:B = 0x1t

.field public static final FILL_PICTURE:B = 0x3t

.field public static final FILL_SHADE_LINEAR:B = 0x7t

.field public static final FILL_SHADE_RADIAL:B = 0x4t

.field public static final FILL_SHADE_RECT:B = 0x5t

.field public static final FILL_SHADE_SHAPE:B = 0x6t

.field public static final FILL_SHADE_TILE:B = 0x2t

.field public static final FILL_SOLID:B = 0x0t

.field public static final FILL_TEXTURE:B = 0x8t


# instance fields
.field private bgColor:I

.field private fgColor:I

.field private fgColorIndex:I

.field private fillType:B

.field private isSlideBackgroundFill:Z

.field private pictureIndex:I

.field private shader:Lcom/intsig/office/common/bg/AShader;

.field private stretch:Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->stretch:Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->shader:Lcom/intsig/office/common/bg/AShader;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/common/bg/AShader;->dispose()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->shader:Lcom/intsig/office/common/bg/AShader;

    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBackgoundColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->bgColor:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFgColorIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->fgColorIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFillType()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->fillType:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getForegroundColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->fgColor:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPicture(Lcom/intsig/office/system/IControl;)Lcom/intsig/office/common/picture/Picture;
    .locals 1

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    iget v0, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->pictureIndex:I

    .line 10
    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/picture/PictureManage;->getPicture(I)Lcom/intsig/office/common/picture/Picture;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getPictureIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->pictureIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShader()Lcom/intsig/office/common/bg/AShader;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->shader:Lcom/intsig/office/common/bg/AShader;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStretch()Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->stretch:Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getType()S
    .locals 1

    .line 1
    const/4 v0, 0x3

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isSlideBackgroundFill()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->isSlideBackgroundFill:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setBackgoundColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->bgColor:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFgColorIndex(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->fgColorIndex:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFillType(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->fillType:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setForegroundColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->fgColor:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPictureIndex(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->pictureIndex:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setShader(Lcom/intsig/office/common/bg/AShader;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->shader:Lcom/intsig/office/common/bg/AShader;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSlideBackgroundFill(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->isSlideBackgroundFill:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setStretch(Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/bg/BackgroundAndFill;->stretch:Lcom/intsig/office/common/pictureefftect/PictureStretchInfo;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

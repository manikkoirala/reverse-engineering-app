.class public Lcom/intsig/office/common/bg/LinearGradientShader;
.super Lcom/intsig/office/common/bg/Gradient;
.source "LinearGradientShader.java"


# instance fields
.field private angle:F


# direct methods
.method public constructor <init>(F[I[F)V
    .locals 0

    .line 1
    invoke-direct {p0, p2, p3}, Lcom/intsig/office/common/bg/Gradient;-><init>([I[F)V

    .line 2
    .line 3
    .line 4
    iput p1, p0, Lcom/intsig/office/common/bg/LinearGradientShader;->angle:F

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private getLinearGradientCoordinate()[I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/common/bg/LinearGradientShader;->angle:F

    .line 2
    .line 3
    const/high16 v1, 0x41b00000    # 22.0f

    .line 4
    .line 5
    add-float/2addr v0, v1

    .line 6
    const/high16 v1, 0x43b40000    # 360.0f

    .line 7
    .line 8
    rem-float/2addr v0, v1

    .line 9
    const/high16 v1, 0x42340000    # 45.0f

    .line 10
    .line 11
    div-float/2addr v0, v1

    .line 12
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/4 v1, 0x4

    .line 17
    packed-switch v0, :pswitch_data_0

    .line 18
    .line 19
    .line 20
    new-array v0, v1, [I

    .line 21
    .line 22
    fill-array-data v0, :array_0

    .line 23
    .line 24
    .line 25
    return-object v0

    .line 26
    :pswitch_0
    new-array v0, v1, [I

    .line 27
    .line 28
    fill-array-data v0, :array_1

    .line 29
    .line 30
    .line 31
    return-object v0

    .line 32
    :pswitch_1
    new-array v0, v1, [I

    .line 33
    .line 34
    fill-array-data v0, :array_2

    .line 35
    .line 36
    .line 37
    return-object v0

    .line 38
    :pswitch_2
    new-array v0, v1, [I

    .line 39
    .line 40
    fill-array-data v0, :array_3

    .line 41
    .line 42
    .line 43
    return-object v0

    .line 44
    :pswitch_3
    new-array v0, v1, [I

    .line 45
    .line 46
    fill-array-data v0, :array_4

    .line 47
    .line 48
    .line 49
    return-object v0

    .line 50
    :pswitch_4
    new-array v0, v1, [I

    .line 51
    .line 52
    fill-array-data v0, :array_5

    .line 53
    .line 54
    .line 55
    return-object v0

    .line 56
    :pswitch_5
    new-array v0, v1, [I

    .line 57
    .line 58
    fill-array-data v0, :array_6

    .line 59
    .line 60
    .line 61
    return-object v0

    .line 62
    :pswitch_6
    new-array v0, v1, [I

    .line 63
    .line 64
    fill-array-data v0, :array_7

    .line 65
    .line 66
    .line 67
    return-object v0

    .line 68
    nop

    .line 69
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    :array_0
    .array-data 4
        0x0
        0x64
        0x64
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x64
        0x0
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x64
        0x64
        0x0
        0x0
    .end array-data

    :array_3
    .array-data 4
        0x64
        0x0
        0x0
        0x0
    .end array-data

    :array_4
    .array-data 4
        0x64
        0x0
        0x0
        0x64
    .end array-data

    :array_5
    .array-data 4
        0x0
        0x0
        0x0
        0x64
    .end array-data

    :array_6
    .array-data 4
        0x0
        0x0
        0x64
        0x64
    .end array-data

    :array_7
    .array-data 4
        0x0
        0x0
        0x64
        0x0
    .end array-data
.end method


# virtual methods
.method public createShader(Lcom/intsig/office/system/IControl;ILandroid/graphics/Rect;)Landroid/graphics/Shader;
    .locals 8

    .line 1
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/office/common/bg/LinearGradientShader;->getLinearGradientCoordinate()[I

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance p2, Landroid/graphics/LinearGradient;

    .line 6
    .line 7
    const/4 p3, 0x0

    .line 8
    aget p3, p1, p3

    .line 9
    .line 10
    int-to-float v1, p3

    .line 11
    const/4 p3, 0x1

    .line 12
    aget p3, p1, p3

    .line 13
    .line 14
    int-to-float v2, p3

    .line 15
    const/4 p3, 0x2

    .line 16
    aget p3, p1, p3

    .line 17
    .line 18
    int-to-float v3, p3

    .line 19
    const/4 p3, 0x3

    .line 20
    aget p1, p1, p3

    .line 21
    .line 22
    int-to-float v4, p1

    .line 23
    iget-object v5, p0, Lcom/intsig/office/common/bg/Gradient;->colors:[I

    .line 24
    .line 25
    iget-object v6, p0, Lcom/intsig/office/common/bg/Gradient;->positions:[F

    .line 26
    .line 27
    sget-object v7, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    .line 28
    .line 29
    move-object v0, p2

    .line 30
    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 31
    .line 32
    .line 33
    iput-object p2, p0, Lcom/intsig/office/common/bg/AShader;->shader:Landroid/graphics/Shader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    .line 35
    return-object p2

    .line 36
    :catch_0
    const/4 p1, 0x0

    .line 37
    return-object p1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public getAngle()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/bg/LinearGradientShader;->angle:F

    .line 2
    .line 3
    float-to-int v0, v0

    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getGradientType()I
    .locals 1

    .line 1
    const/4 v0, 0x7

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

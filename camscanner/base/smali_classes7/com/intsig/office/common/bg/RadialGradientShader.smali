.class public Lcom/intsig/office/common/bg/RadialGradientShader;
.super Lcom/intsig/office/common/bg/Gradient;
.source "RadialGradientShader.java"


# static fields
.field public static final Center_BL:I = 0x2

.field public static final Center_BR:I = 0x3

.field public static final Center_Center:I = 0x4

.field public static final Center_TL:I = 0x0

.field public static final Center_TR:I = 0x1


# instance fields
.field private positionType:I


# direct methods
.method public constructor <init>(I[I[F)V
    .locals 0

    .line 1
    invoke-direct {p0, p2, p3}, Lcom/intsig/office/common/bg/Gradient;-><init>([I[F)V

    .line 2
    .line 3
    .line 4
    iput p1, p0, Lcom/intsig/office/common/bg/RadialGradientShader;->positionType:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private getCircleCoordinate()[I
    .locals 7

    .line 1
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    .line 2
    .line 3
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 4
    .line 5
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    mul-double v0, v0, v2

    .line 10
    .line 11
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    .line 16
    .line 17
    .line 18
    move-result-wide v0

    .line 19
    double-to-int v0, v0

    .line 20
    iget v1, p0, Lcom/intsig/office/common/bg/RadialGradientShader;->positionType:I

    .line 21
    .line 22
    const/16 v2, 0x64

    .line 23
    .line 24
    const/4 v3, 0x3

    .line 25
    const/4 v4, 0x1

    .line 26
    const/4 v5, 0x2

    .line 27
    const/4 v6, 0x0

    .line 28
    if-eq v1, v4, :cond_3

    .line 29
    .line 30
    if-eq v1, v5, :cond_2

    .line 31
    .line 32
    if-eq v1, v3, :cond_1

    .line 33
    .line 34
    const/4 v2, 0x4

    .line 35
    if-eq v1, v2, :cond_0

    .line 36
    .line 37
    new-array v1, v3, [I

    .line 38
    .line 39
    aput v6, v1, v6

    .line 40
    .line 41
    aput v6, v1, v4

    .line 42
    .line 43
    aput v0, v1, v5

    .line 44
    .line 45
    return-object v1

    .line 46
    :cond_0
    new-array v1, v3, [I

    .line 47
    .line 48
    const/16 v2, 0x32

    .line 49
    .line 50
    aput v2, v1, v6

    .line 51
    .line 52
    aput v2, v1, v4

    .line 53
    .line 54
    div-int/2addr v0, v5

    .line 55
    aput v0, v1, v5

    .line 56
    .line 57
    return-object v1

    .line 58
    :cond_1
    new-array v1, v3, [I

    .line 59
    .line 60
    aput v2, v1, v6

    .line 61
    .line 62
    aput v2, v1, v4

    .line 63
    .line 64
    aput v0, v1, v5

    .line 65
    .line 66
    return-object v1

    .line 67
    :cond_2
    new-array v1, v3, [I

    .line 68
    .line 69
    aput v6, v1, v6

    .line 70
    .line 71
    aput v2, v1, v4

    .line 72
    .line 73
    aput v0, v1, v5

    .line 74
    .line 75
    return-object v1

    .line 76
    :cond_3
    new-array v1, v3, [I

    .line 77
    .line 78
    aput v2, v1, v6

    .line 79
    .line 80
    aput v6, v1, v4

    .line 81
    .line 82
    aput v0, v1, v5

    .line 83
    .line 84
    return-object v1
    .line 85
    .line 86
.end method


# virtual methods
.method public createShader(Lcom/intsig/office/system/IControl;ILandroid/graphics/Rect;)Landroid/graphics/Shader;
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/common/bg/RadialGradientShader;->getCircleCoordinate()[I

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iget p2, p0, Lcom/intsig/office/common/bg/RadialGradientShader;->positionType:I

    .line 6
    .line 7
    const/4 p3, 0x4

    .line 8
    const/4 v0, 0x0

    .line 9
    if-ne p2, p3, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/Gradient;->getFocus()I

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    if-nez p2, :cond_0

    .line 16
    .line 17
    iget-object p2, p0, Lcom/intsig/office/common/bg/Gradient;->colors:[I

    .line 18
    .line 19
    array-length p2, p2

    .line 20
    const/4 p3, 0x0

    .line 21
    :goto_0
    div-int/lit8 v1, p2, 0x2

    .line 22
    .line 23
    if-ge p3, v1, :cond_0

    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/office/common/bg/Gradient;->colors:[I

    .line 26
    .line 27
    aget v2, v1, p3

    .line 28
    .line 29
    add-int/lit8 v3, p2, -0x1

    .line 30
    .line 31
    sub-int/2addr v3, p3

    .line 32
    aget v4, v1, v3

    .line 33
    .line 34
    aput v4, v1, p3

    .line 35
    .line 36
    aput v2, v1, v3

    .line 37
    .line 38
    add-int/lit8 p3, p3, 0x1

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_0
    new-instance p2, Landroid/graphics/RadialGradient;

    .line 42
    .line 43
    aget p3, p1, v0

    .line 44
    .line 45
    int-to-float v2, p3

    .line 46
    const/4 p3, 0x1

    .line 47
    aget p3, p1, p3

    .line 48
    .line 49
    int-to-float v3, p3

    .line 50
    const/4 p3, 0x2

    .line 51
    aget p1, p1, p3

    .line 52
    .line 53
    int-to-float v4, p1

    .line 54
    iget-object v5, p0, Lcom/intsig/office/common/bg/Gradient;->colors:[I

    .line 55
    .line 56
    iget-object v6, p0, Lcom/intsig/office/common/bg/Gradient;->positions:[F

    .line 57
    .line 58
    sget-object v7, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    .line 59
    .line 60
    move-object v1, p2

    .line 61
    invoke-direct/range {v1 .. v7}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 62
    .line 63
    .line 64
    iput-object p2, p0, Lcom/intsig/office/common/bg/AShader;->shader:Landroid/graphics/Shader;

    .line 65
    .line 66
    return-object p2
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public getGradientType()I
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public Lcom/intsig/office/common/bg/PatternShader;
.super Lcom/intsig/office/common/bg/AShader;
.source "PatternShader.java"


# instance fields
.field private backgroundColor:I

.field private foregroundColor:I

.field private picture:Lcom/intsig/office/common/picture/Picture;


# direct methods
.method public constructor <init>(Lcom/intsig/office/common/picture/Picture;II)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/common/bg/AShader;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/common/bg/PatternShader;->picture:Lcom/intsig/office/common/picture/Picture;

    .line 5
    .line 6
    iput p2, p0, Lcom/intsig/office/common/bg/PatternShader;->backgroundColor:I

    .line 7
    .line 8
    iput p3, p0, Lcom/intsig/office/common/bg/PatternShader;->foregroundColor:I

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public createShader(Lcom/intsig/office/system/IControl;ILandroid/graphics/Rect;)Landroid/graphics/Shader;
    .locals 10

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/intsig/office/common/bg/PatternShader;->picture:Lcom/intsig/office/common/picture/Picture;

    .line 3
    .line 4
    invoke-static {p1, p2, v1, p3, v0}, Lcom/intsig/office/common/bg/TileShader;->getBitmap(Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/picture/Picture;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 5
    .line 6
    .line 7
    move-result-object v2

    .line 8
    if-eqz v2, :cond_2

    .line 9
    .line 10
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    .line 15
    .line 16
    .line 17
    move-result p2

    .line 18
    mul-int p3, p1, p2

    .line 19
    .line 20
    new-array v1, p3, [I

    .line 21
    .line 22
    const/4 v4, 0x0

    .line 23
    const/4 v6, 0x0

    .line 24
    const/4 v7, 0x0

    .line 25
    move-object v3, v1

    .line 26
    move v5, p1

    .line 27
    move v8, p1

    .line 28
    move v9, p2

    .line 29
    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 30
    .line 31
    .line 32
    const/4 v2, 0x0

    .line 33
    :goto_0
    if-ge v2, p3, :cond_1

    .line 34
    .line 35
    aget v3, v1, v2

    .line 36
    .line 37
    const v4, 0xffffff

    .line 38
    .line 39
    .line 40
    and-int/2addr v3, v4

    .line 41
    if-nez v3, :cond_0

    .line 42
    .line 43
    iget v3, p0, Lcom/intsig/office/common/bg/PatternShader;->backgroundColor:I

    .line 44
    .line 45
    aput v3, v1, v2

    .line 46
    .line 47
    goto :goto_1

    .line 48
    :cond_0
    iget v3, p0, Lcom/intsig/office/common/bg/PatternShader;->foregroundColor:I

    .line 49
    .line 50
    aput v3, v1, v2

    .line 51
    .line 52
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_1
    sget-object p3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 56
    .line 57
    invoke-static {v1, p1, p2, p3}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    sget-object p2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    .line 62
    .line 63
    new-instance p3, Landroid/graphics/BitmapShader;

    .line 64
    .line 65
    invoke-direct {p3, p1, p2, p2}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 66
    .line 67
    .line 68
    iput-object p3, p0, Lcom/intsig/office/common/bg/AShader;->shader:Landroid/graphics/Shader;

    .line 69
    .line 70
    :cond_2
    iget-object p1, p0, Lcom/intsig/office/common/bg/AShader;->shader:Landroid/graphics/Shader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .line 72
    return-object p1

    .line 73
    :catch_0
    return-object v0
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

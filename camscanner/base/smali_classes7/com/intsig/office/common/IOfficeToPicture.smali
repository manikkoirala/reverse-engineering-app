.class public interface abstract Lcom/intsig/office/common/IOfficeToPicture;
.super Ljava/lang/Object;
.source "IOfficeToPicture.java"


# static fields
.field public static final INSTANCE_CLASS_PATH:Ljava/lang/String; = "com.intsig.office.officereader.OfficeToPicture"

.field public static final VIEW_CHANGE_END:B = 0x1t

.field public static final VIEW_CHANGING:B


# virtual methods
.method public abstract callBack(Landroid/graphics/Bitmap;)V
.end method

.method public abstract dispose()V
.end method

.method public abstract getBitmap(II)Landroid/graphics/Bitmap;
.end method

.method public abstract getModeType()B
.end method

.method public abstract isZoom()Z
.end method

.method public abstract setModeType(B)V
.end method

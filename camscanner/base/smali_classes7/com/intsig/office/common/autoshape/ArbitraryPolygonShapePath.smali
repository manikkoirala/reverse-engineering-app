.class public Lcom/intsig/office/common/autoshape/ArbitraryPolygonShapePath;
.super Ljava/lang/Object;
.source "ArbitraryPolygonShapePath.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static getArrowPath(Lcom/intsig/office/common/shape/ArbitraryPolygonShape;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;Lcom/intsig/office/common/bg/BackgroundAndFill;ZLandroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/Path;
    .locals 24

    .line 1
    move-object/from16 v0, p2

    .line 2
    .line 3
    new-instance v8, Landroid/graphics/Path;

    .line 4
    .line 5
    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    .line 6
    .line 7
    .line 8
    invoke-interface/range {p1 .. p1}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 9
    .line 10
    .line 11
    move-result-object v9

    .line 12
    invoke-interface {v9}, Ljava/util/List;->size()I

    .line 13
    .line 14
    .line 15
    move-result v10

    .line 16
    add-int/lit8 v11, v10, -0x1

    .line 17
    .line 18
    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 23
    .line 24
    move-object/from16 v13, p5

    .line 25
    .line 26
    move-object/from16 v14, p6

    .line 27
    .line 28
    const/4 v15, 0x0

    .line 29
    :goto_0
    if-ge v15, v10, :cond_10

    .line 30
    .line 31
    invoke-interface {v9, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 36
    .line 37
    const-string v2, "moveTo"

    .line 38
    .line 39
    const-string v3, "pt"

    .line 40
    .line 41
    if-eqz v13, :cond_0

    .line 42
    .line 43
    if-nez v15, :cond_0

    .line 44
    .line 45
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v4

    .line 49
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 50
    .line 51
    .line 52
    move-result v4

    .line 53
    if-eqz v4, :cond_0

    .line 54
    .line 55
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrowType()B

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    invoke-static {v1, v13, v2}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(Lcom/intsig/office/fc/dom4j/Element;Landroid/graphics/PointF;B)Landroid/graphics/PointF;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 68
    .line 69
    iget v3, v1, Landroid/graphics/PointF;->y:F

    .line 70
    .line 71
    invoke-virtual {v8, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 72
    .line 73
    .line 74
    move-object v13, v1

    .line 75
    move-object/from16 p4, v9

    .line 76
    .line 77
    move/from16 v16, v10

    .line 78
    .line 79
    :goto_1
    move/from16 v18, v11

    .line 80
    .line 81
    move/from16 v22, v15

    .line 82
    .line 83
    const/4 v9, 0x0

    .line 84
    goto/16 :goto_4

    .line 85
    .line 86
    :cond_0
    const-string v4, "swAng"

    .line 87
    .line 88
    const-string v5, "stAng"

    .line 89
    .line 90
    const-string v6, "hR"

    .line 91
    .line 92
    const-string v7, "wR"

    .line 93
    .line 94
    const-string v12, "arcTo"

    .line 95
    .line 96
    move-object/from16 p4, v9

    .line 97
    .line 98
    const-string v9, "cubicBezTo"

    .line 99
    .line 100
    move/from16 v16, v10

    .line 101
    .line 102
    const-string v10, "quadBezTo"

    .line 103
    .line 104
    move-object/from16 p5, v13

    .line 105
    .line 106
    const-string v13, "lnTo"

    .line 107
    .line 108
    const v17, 0x476a6000    # 60000.0f

    .line 109
    .line 110
    .line 111
    move-object/from16 p6, v2

    .line 112
    .line 113
    const-string v2, "y"

    .line 114
    .line 115
    move-object/from16 v18, v4

    .line 116
    .line 117
    const-string v4, "x"

    .line 118
    .line 119
    const v19, 0x495f3e00    # 914400.0f

    .line 120
    .line 121
    .line 122
    const/high16 v20, 0x42c00000    # 96.0f

    .line 123
    .line 124
    move-object/from16 v21, v5

    .line 125
    .line 126
    if-eqz v14, :cond_7

    .line 127
    .line 128
    if-ne v15, v11, :cond_7

    .line 129
    .line 130
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 131
    .line 132
    .line 133
    move-result-object v5

    .line 134
    invoke-virtual {v5, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 135
    .line 136
    .line 137
    move-result v5

    .line 138
    if-eqz v5, :cond_1

    .line 139
    .line 140
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 141
    .line 142
    .line 143
    move-result-object v1

    .line 144
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowType()B

    .line 145
    .line 146
    .line 147
    move-result v2

    .line 148
    invoke-static {v1, v14, v2}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(Lcom/intsig/office/fc/dom4j/Element;Landroid/graphics/PointF;B)Landroid/graphics/PointF;

    .line 149
    .line 150
    .line 151
    move-result-object v1

    .line 152
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 153
    .line 154
    iget v3, v1, Landroid/graphics/PointF;->y:F

    .line 155
    .line 156
    invoke-virtual {v8, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 157
    .line 158
    .line 159
    move-object/from16 v13, p5

    .line 160
    .line 161
    move-object v14, v1

    .line 162
    goto :goto_1

    .line 163
    :cond_1
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 164
    .line 165
    .line 166
    move-result-object v3

    .line 167
    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 168
    .line 169
    .line 170
    move-result v3

    .line 171
    if-eqz v3, :cond_3

    .line 172
    .line 173
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 174
    .line 175
    .line 176
    move-result-object v1

    .line 177
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 178
    .line 179
    .line 180
    move-result v3

    .line 181
    const/4 v5, 0x2

    .line 182
    if-eq v3, v5, :cond_2

    .line 183
    .line 184
    goto/16 :goto_5

    .line 185
    .line 186
    :cond_2
    const/4 v3, 0x1

    .line 187
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 188
    .line 189
    .line 190
    move-result-object v3

    .line 191
    check-cast v3, Lcom/intsig/office/fc/dom4j/Element;

    .line 192
    .line 193
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowType()B

    .line 194
    .line 195
    .line 196
    move-result v5

    .line 197
    invoke-static {v3, v14, v5}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(Lcom/intsig/office/fc/dom4j/Element;Landroid/graphics/PointF;B)Landroid/graphics/PointF;

    .line 198
    .line 199
    .line 200
    move-result-object v3

    .line 201
    const/4 v5, 0x0

    .line 202
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 203
    .line 204
    .line 205
    move-result-object v6

    .line 206
    check-cast v6, Lcom/intsig/office/fc/dom4j/Element;

    .line 207
    .line 208
    invoke-interface {v6, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 209
    .line 210
    .line 211
    move-result-object v4

    .line 212
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 213
    .line 214
    .line 215
    move-result v4

    .line 216
    int-to-float v4, v4

    .line 217
    mul-float v4, v4, v20

    .line 218
    .line 219
    div-float v4, v4, v19

    .line 220
    .line 221
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 222
    .line 223
    .line 224
    move-result-object v1

    .line 225
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 226
    .line 227
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 228
    .line 229
    .line 230
    move-result-object v1

    .line 231
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 232
    .line 233
    .line 234
    move-result v1

    .line 235
    int-to-float v1, v1

    .line 236
    mul-float v1, v1, v20

    .line 237
    .line 238
    div-float v1, v1, v19

    .line 239
    .line 240
    iget v2, v3, Landroid/graphics/PointF;->x:F

    .line 241
    .line 242
    iget v5, v3, Landroid/graphics/PointF;->y:F

    .line 243
    .line 244
    invoke-virtual {v8, v4, v1, v2, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 245
    .line 246
    .line 247
    move-object/from16 v13, p5

    .line 248
    .line 249
    move-object v14, v3

    .line 250
    goto/16 :goto_1

    .line 251
    .line 252
    :cond_3
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 253
    .line 254
    .line 255
    move-result-object v3

    .line 256
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 257
    .line 258
    .line 259
    move-result v3

    .line 260
    if-eqz v3, :cond_5

    .line 261
    .line 262
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 263
    .line 264
    .line 265
    move-result-object v1

    .line 266
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 267
    .line 268
    .line 269
    move-result v3

    .line 270
    const/4 v5, 0x3

    .line 271
    if-eq v3, v5, :cond_4

    .line 272
    .line 273
    goto/16 :goto_5

    .line 274
    .line 275
    :cond_4
    const/4 v3, 0x2

    .line 276
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 277
    .line 278
    .line 279
    move-result-object v3

    .line 280
    check-cast v3, Lcom/intsig/office/fc/dom4j/Element;

    .line 281
    .line 282
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowType()B

    .line 283
    .line 284
    .line 285
    move-result v5

    .line 286
    invoke-static {v3, v14, v5}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(Lcom/intsig/office/fc/dom4j/Element;Landroid/graphics/PointF;B)Landroid/graphics/PointF;

    .line 287
    .line 288
    .line 289
    move-result-object v9

    .line 290
    const/4 v3, 0x0

    .line 291
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 292
    .line 293
    .line 294
    move-result-object v5

    .line 295
    check-cast v5, Lcom/intsig/office/fc/dom4j/Element;

    .line 296
    .line 297
    invoke-interface {v5, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 298
    .line 299
    .line 300
    move-result-object v5

    .line 301
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 302
    .line 303
    .line 304
    move-result v5

    .line 305
    int-to-float v5, v5

    .line 306
    mul-float v5, v5, v20

    .line 307
    .line 308
    div-float v5, v5, v19

    .line 309
    .line 310
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 311
    .line 312
    .line 313
    move-result-object v6

    .line 314
    check-cast v6, Lcom/intsig/office/fc/dom4j/Element;

    .line 315
    .line 316
    invoke-interface {v6, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 317
    .line 318
    .line 319
    move-result-object v3

    .line 320
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 321
    .line 322
    .line 323
    move-result v3

    .line 324
    int-to-float v3, v3

    .line 325
    mul-float v3, v3, v20

    .line 326
    .line 327
    div-float v3, v3, v19

    .line 328
    .line 329
    const/4 v6, 0x1

    .line 330
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 331
    .line 332
    .line 333
    move-result-object v7

    .line 334
    check-cast v7, Lcom/intsig/office/fc/dom4j/Element;

    .line 335
    .line 336
    invoke-interface {v7, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 337
    .line 338
    .line 339
    move-result-object v4

    .line 340
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 341
    .line 342
    .line 343
    move-result v4

    .line 344
    int-to-float v4, v4

    .line 345
    mul-float v4, v4, v20

    .line 346
    .line 347
    div-float v4, v4, v19

    .line 348
    .line 349
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 350
    .line 351
    .line 352
    move-result-object v1

    .line 353
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 354
    .line 355
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 356
    .line 357
    .line 358
    move-result-object v1

    .line 359
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 360
    .line 361
    .line 362
    move-result v1

    .line 363
    int-to-float v1, v1

    .line 364
    mul-float v1, v1, v20

    .line 365
    .line 366
    div-float v6, v1, v19

    .line 367
    .line 368
    iget v7, v9, Landroid/graphics/PointF;->x:F

    .line 369
    .line 370
    iget v10, v9, Landroid/graphics/PointF;->y:F

    .line 371
    .line 372
    move-object v1, v8

    .line 373
    move v2, v5

    .line 374
    move v5, v6

    .line 375
    move v6, v7

    .line 376
    move v7, v10

    .line 377
    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 378
    .line 379
    .line 380
    move-object/from16 v13, p5

    .line 381
    .line 382
    move-object v14, v9

    .line 383
    goto/16 :goto_1

    .line 384
    .line 385
    :cond_5
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 386
    .line 387
    .line 388
    move-result-object v2

    .line 389
    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 390
    .line 391
    .line 392
    move-result v2

    .line 393
    if-eqz v2, :cond_6

    .line 394
    .line 395
    invoke-interface {v1, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 396
    .line 397
    .line 398
    move-result-object v2

    .line 399
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 400
    .line 401
    .line 402
    move-result v2

    .line 403
    int-to-float v2, v2

    .line 404
    mul-float v2, v2, v20

    .line 405
    .line 406
    div-float v2, v2, v19

    .line 407
    .line 408
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 409
    .line 410
    .line 411
    move-result-object v3

    .line 412
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 413
    .line 414
    .line 415
    move-result v3

    .line 416
    int-to-float v3, v3

    .line 417
    mul-float v3, v3, v20

    .line 418
    .line 419
    div-float v3, v3, v19

    .line 420
    .line 421
    new-instance v4, Landroid/graphics/RectF;

    .line 422
    .line 423
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getCenterX()D

    .line 424
    .line 425
    .line 426
    move-result-wide v5

    .line 427
    double-to-float v5, v5

    .line 428
    sub-float/2addr v5, v2

    .line 429
    iget v6, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 430
    .line 431
    int-to-float v6, v6

    .line 432
    sub-float/2addr v5, v6

    .line 433
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getCenterY()D

    .line 434
    .line 435
    .line 436
    move-result-wide v6

    .line 437
    double-to-float v6, v6

    .line 438
    sub-float/2addr v6, v3

    .line 439
    iget v7, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 440
    .line 441
    int-to-float v7, v7

    .line 442
    sub-float/2addr v6, v7

    .line 443
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getCenterX()D

    .line 444
    .line 445
    .line 446
    move-result-wide v9

    .line 447
    double-to-float v7, v9

    .line 448
    add-float/2addr v7, v2

    .line 449
    iget v2, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 450
    .line 451
    int-to-float v2, v2

    .line 452
    sub-float/2addr v7, v2

    .line 453
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getCenterY()D

    .line 454
    .line 455
    .line 456
    move-result-wide v9

    .line 457
    double-to-float v2, v9

    .line 458
    add-float/2addr v2, v3

    .line 459
    iget v3, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 460
    .line 461
    int-to-float v3, v3

    .line 462
    sub-float/2addr v2, v3

    .line 463
    invoke-direct {v4, v5, v6, v7, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 464
    .line 465
    .line 466
    move-object/from16 v5, v21

    .line 467
    .line 468
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 469
    .line 470
    .line 471
    move-result-object v2

    .line 472
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 473
    .line 474
    .line 475
    move-result v2

    .line 476
    int-to-float v2, v2

    .line 477
    div-float v2, v2, v17

    .line 478
    .line 479
    move-object/from16 v3, v18

    .line 480
    .line 481
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 482
    .line 483
    .line 484
    move-result-object v1

    .line 485
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 486
    .line 487
    .line 488
    move-result v1

    .line 489
    int-to-float v1, v1

    .line 490
    div-float v1, v1, v17

    .line 491
    .line 492
    invoke-virtual {v8, v4, v2, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 493
    .line 494
    .line 495
    :cond_6
    move/from16 v18, v11

    .line 496
    .line 497
    move-object/from16 v21, v14

    .line 498
    .line 499
    move/from16 v22, v15

    .line 500
    .line 501
    goto :goto_2

    .line 502
    :cond_7
    move-object/from16 v5, v21

    .line 503
    .line 504
    move-object/from16 v23, v18

    .line 505
    .line 506
    move/from16 v18, v11

    .line 507
    .line 508
    move-object/from16 v11, v23

    .line 509
    .line 510
    move-object/from16 v21, v14

    .line 511
    .line 512
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 513
    .line 514
    .line 515
    move-result-object v14

    .line 516
    move/from16 v22, v15

    .line 517
    .line 518
    move-object/from16 v15, p6

    .line 519
    .line 520
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 521
    .line 522
    .line 523
    move-result v14

    .line 524
    if-eqz v14, :cond_8

    .line 525
    .line 526
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 527
    .line 528
    .line 529
    move-result-object v1

    .line 530
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 531
    .line 532
    .line 533
    move-result-object v3

    .line 534
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 535
    .line 536
    .line 537
    move-result v3

    .line 538
    int-to-float v3, v3

    .line 539
    mul-float v3, v3, v20

    .line 540
    .line 541
    div-float v3, v3, v19

    .line 542
    .line 543
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 544
    .line 545
    .line 546
    move-result-object v1

    .line 547
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 548
    .line 549
    .line 550
    move-result v1

    .line 551
    int-to-float v1, v1

    .line 552
    mul-float v1, v1, v20

    .line 553
    .line 554
    div-float v1, v1, v19

    .line 555
    .line 556
    invoke-virtual {v8, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 557
    .line 558
    .line 559
    :goto_2
    const/4 v9, 0x0

    .line 560
    goto/16 :goto_3

    .line 561
    .line 562
    :cond_8
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 563
    .line 564
    .line 565
    move-result-object v14

    .line 566
    invoke-virtual {v14, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 567
    .line 568
    .line 569
    move-result v13

    .line 570
    if-eqz v13, :cond_9

    .line 571
    .line 572
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 573
    .line 574
    .line 575
    move-result-object v1

    .line 576
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 577
    .line 578
    .line 579
    move-result-object v3

    .line 580
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 581
    .line 582
    .line 583
    move-result v3

    .line 584
    int-to-float v3, v3

    .line 585
    mul-float v3, v3, v20

    .line 586
    .line 587
    div-float v3, v3, v19

    .line 588
    .line 589
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 590
    .line 591
    .line 592
    move-result-object v1

    .line 593
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 594
    .line 595
    .line 596
    move-result v1

    .line 597
    int-to-float v1, v1

    .line 598
    mul-float v1, v1, v20

    .line 599
    .line 600
    div-float v1, v1, v19

    .line 601
    .line 602
    invoke-virtual {v8, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 603
    .line 604
    .line 605
    goto :goto_2

    .line 606
    :cond_9
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 607
    .line 608
    .line 609
    move-result-object v3

    .line 610
    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 611
    .line 612
    .line 613
    move-result v3

    .line 614
    if-eqz v3, :cond_b

    .line 615
    .line 616
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 617
    .line 618
    .line 619
    move-result-object v1

    .line 620
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 621
    .line 622
    .line 623
    move-result v3

    .line 624
    const/4 v5, 0x2

    .line 625
    if-eq v3, v5, :cond_a

    .line 626
    .line 627
    goto/16 :goto_5

    .line 628
    .line 629
    :cond_a
    const/4 v3, 0x0

    .line 630
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 631
    .line 632
    .line 633
    move-result-object v5

    .line 634
    check-cast v5, Lcom/intsig/office/fc/dom4j/Element;

    .line 635
    .line 636
    invoke-interface {v5, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 637
    .line 638
    .line 639
    move-result-object v5

    .line 640
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 641
    .line 642
    .line 643
    move-result v5

    .line 644
    int-to-float v5, v5

    .line 645
    mul-float v5, v5, v20

    .line 646
    .line 647
    div-float v5, v5, v19

    .line 648
    .line 649
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 650
    .line 651
    .line 652
    move-result-object v6

    .line 653
    check-cast v6, Lcom/intsig/office/fc/dom4j/Element;

    .line 654
    .line 655
    invoke-interface {v6, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 656
    .line 657
    .line 658
    move-result-object v3

    .line 659
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 660
    .line 661
    .line 662
    move-result v3

    .line 663
    int-to-float v3, v3

    .line 664
    mul-float v3, v3, v20

    .line 665
    .line 666
    div-float v3, v3, v19

    .line 667
    .line 668
    const/4 v6, 0x1

    .line 669
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 670
    .line 671
    .line 672
    move-result-object v7

    .line 673
    check-cast v7, Lcom/intsig/office/fc/dom4j/Element;

    .line 674
    .line 675
    invoke-interface {v7, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 676
    .line 677
    .line 678
    move-result-object v4

    .line 679
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 680
    .line 681
    .line 682
    move-result v4

    .line 683
    int-to-float v4, v4

    .line 684
    mul-float v4, v4, v20

    .line 685
    .line 686
    div-float v4, v4, v19

    .line 687
    .line 688
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 689
    .line 690
    .line 691
    move-result-object v1

    .line 692
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 693
    .line 694
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 695
    .line 696
    .line 697
    move-result-object v1

    .line 698
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 699
    .line 700
    .line 701
    move-result v1

    .line 702
    int-to-float v1, v1

    .line 703
    mul-float v1, v1, v20

    .line 704
    .line 705
    div-float v1, v1, v19

    .line 706
    .line 707
    invoke-virtual {v8, v5, v3, v4, v1}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 708
    .line 709
    .line 710
    goto/16 :goto_2

    .line 711
    .line 712
    :cond_b
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 713
    .line 714
    .line 715
    move-result-object v3

    .line 716
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 717
    .line 718
    .line 719
    move-result v3

    .line 720
    if-eqz v3, :cond_d

    .line 721
    .line 722
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 723
    .line 724
    .line 725
    move-result-object v1

    .line 726
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 727
    .line 728
    .line 729
    move-result v3

    .line 730
    const/4 v5, 0x3

    .line 731
    if-eq v3, v5, :cond_c

    .line 732
    .line 733
    goto/16 :goto_5

    .line 734
    .line 735
    :cond_c
    const/4 v9, 0x0

    .line 736
    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 737
    .line 738
    .line 739
    move-result-object v3

    .line 740
    check-cast v3, Lcom/intsig/office/fc/dom4j/Element;

    .line 741
    .line 742
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 743
    .line 744
    .line 745
    move-result-object v3

    .line 746
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 747
    .line 748
    .line 749
    move-result v3

    .line 750
    int-to-float v3, v3

    .line 751
    mul-float v3, v3, v20

    .line 752
    .line 753
    div-float v3, v3, v19

    .line 754
    .line 755
    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 756
    .line 757
    .line 758
    move-result-object v5

    .line 759
    check-cast v5, Lcom/intsig/office/fc/dom4j/Element;

    .line 760
    .line 761
    invoke-interface {v5, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 762
    .line 763
    .line 764
    move-result-object v5

    .line 765
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 766
    .line 767
    .line 768
    move-result v5

    .line 769
    int-to-float v5, v5

    .line 770
    mul-float v5, v5, v20

    .line 771
    .line 772
    div-float v5, v5, v19

    .line 773
    .line 774
    const/4 v6, 0x1

    .line 775
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 776
    .line 777
    .line 778
    move-result-object v7

    .line 779
    check-cast v7, Lcom/intsig/office/fc/dom4j/Element;

    .line 780
    .line 781
    invoke-interface {v7, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 782
    .line 783
    .line 784
    move-result-object v7

    .line 785
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 786
    .line 787
    .line 788
    move-result v7

    .line 789
    int-to-float v7, v7

    .line 790
    mul-float v7, v7, v20

    .line 791
    .line 792
    div-float v7, v7, v19

    .line 793
    .line 794
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 795
    .line 796
    .line 797
    move-result-object v6

    .line 798
    check-cast v6, Lcom/intsig/office/fc/dom4j/Element;

    .line 799
    .line 800
    invoke-interface {v6, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 801
    .line 802
    .line 803
    move-result-object v6

    .line 804
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 805
    .line 806
    .line 807
    move-result v6

    .line 808
    int-to-float v6, v6

    .line 809
    mul-float v6, v6, v20

    .line 810
    .line 811
    div-float v6, v6, v19

    .line 812
    .line 813
    const/4 v10, 0x2

    .line 814
    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 815
    .line 816
    .line 817
    move-result-object v11

    .line 818
    check-cast v11, Lcom/intsig/office/fc/dom4j/Element;

    .line 819
    .line 820
    invoke-interface {v11, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 821
    .line 822
    .line 823
    move-result-object v4

    .line 824
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 825
    .line 826
    .line 827
    move-result v4

    .line 828
    int-to-float v4, v4

    .line 829
    mul-float v4, v4, v20

    .line 830
    .line 831
    div-float v11, v4, v19

    .line 832
    .line 833
    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 834
    .line 835
    .line 836
    move-result-object v1

    .line 837
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 838
    .line 839
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 840
    .line 841
    .line 842
    move-result-object v1

    .line 843
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 844
    .line 845
    .line 846
    move-result v1

    .line 847
    int-to-float v1, v1

    .line 848
    mul-float v1, v1, v20

    .line 849
    .line 850
    div-float v10, v1, v19

    .line 851
    .line 852
    move-object v1, v8

    .line 853
    move v2, v3

    .line 854
    move v3, v5

    .line 855
    move v4, v7

    .line 856
    move v5, v6

    .line 857
    move v6, v11

    .line 858
    move v7, v10

    .line 859
    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 860
    .line 861
    .line 862
    goto/16 :goto_3

    .line 863
    .line 864
    :cond_d
    const/4 v9, 0x0

    .line 865
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 866
    .line 867
    .line 868
    move-result-object v2

    .line 869
    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 870
    .line 871
    .line 872
    move-result v2

    .line 873
    if-eqz v2, :cond_e

    .line 874
    .line 875
    invoke-interface {v1, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 876
    .line 877
    .line 878
    move-result-object v2

    .line 879
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 880
    .line 881
    .line 882
    move-result v2

    .line 883
    int-to-float v2, v2

    .line 884
    mul-float v2, v2, v20

    .line 885
    .line 886
    div-float v2, v2, v19

    .line 887
    .line 888
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 889
    .line 890
    .line 891
    move-result-object v3

    .line 892
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 893
    .line 894
    .line 895
    move-result v3

    .line 896
    int-to-float v3, v3

    .line 897
    mul-float v3, v3, v20

    .line 898
    .line 899
    div-float v3, v3, v19

    .line 900
    .line 901
    new-instance v4, Landroid/graphics/RectF;

    .line 902
    .line 903
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getCenterX()D

    .line 904
    .line 905
    .line 906
    move-result-wide v6

    .line 907
    double-to-float v6, v6

    .line 908
    sub-float/2addr v6, v2

    .line 909
    iget v7, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 910
    .line 911
    int-to-float v7, v7

    .line 912
    sub-float/2addr v6, v7

    .line 913
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getCenterY()D

    .line 914
    .line 915
    .line 916
    move-result-wide v12

    .line 917
    double-to-float v7, v12

    .line 918
    sub-float/2addr v7, v3

    .line 919
    iget v10, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 920
    .line 921
    int-to-float v10, v10

    .line 922
    sub-float/2addr v7, v10

    .line 923
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getCenterX()D

    .line 924
    .line 925
    .line 926
    move-result-wide v12

    .line 927
    double-to-float v10, v12

    .line 928
    add-float/2addr v10, v2

    .line 929
    iget v2, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 930
    .line 931
    int-to-float v2, v2

    .line 932
    sub-float/2addr v10, v2

    .line 933
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getCenterY()D

    .line 934
    .line 935
    .line 936
    move-result-wide v12

    .line 937
    double-to-float v2, v12

    .line 938
    add-float/2addr v2, v3

    .line 939
    iget v3, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 940
    .line 941
    int-to-float v3, v3

    .line 942
    sub-float/2addr v2, v3

    .line 943
    invoke-direct {v4, v6, v7, v10, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 944
    .line 945
    .line 946
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 947
    .line 948
    .line 949
    move-result-object v2

    .line 950
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 951
    .line 952
    .line 953
    move-result v2

    .line 954
    int-to-float v2, v2

    .line 955
    div-float v2, v2, v17

    .line 956
    .line 957
    invoke-interface {v1, v11}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 958
    .line 959
    .line 960
    move-result-object v1

    .line 961
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 962
    .line 963
    .line 964
    move-result v1

    .line 965
    int-to-float v1, v1

    .line 966
    div-float v1, v1, v17

    .line 967
    .line 968
    invoke-virtual {v8, v4, v2, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 969
    .line 970
    .line 971
    goto :goto_3

    .line 972
    :cond_e
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 973
    .line 974
    .line 975
    move-result-object v1

    .line 976
    const-string v2, "close"

    .line 977
    .line 978
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 979
    .line 980
    .line 981
    move-result v1

    .line 982
    if-eqz v1, :cond_f

    .line 983
    .line 984
    invoke-virtual {v8}, Landroid/graphics/Path;->close()V

    .line 985
    .line 986
    .line 987
    :cond_f
    :goto_3
    move-object/from16 v13, p5

    .line 988
    .line 989
    move-object/from16 v14, v21

    .line 990
    .line 991
    :goto_4
    add-int/lit8 v15, v22, 0x1

    .line 992
    .line 993
    move-object/from16 v9, p4

    .line 994
    .line 995
    move/from16 v10, v16

    .line 996
    .line 997
    move/from16 v11, v18

    .line 998
    .line 999
    goto/16 :goto_0

    .line 1000
    .line 1001
    :cond_10
    :goto_5
    return-object v8
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
.end method

.method public static getArrowSize(Ljava/lang/String;)I
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eqz p0, :cond_2

    .line 3
    .line 4
    const-string v1, "med"

    .line 5
    .line 6
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const-string v1, "sm"

    .line 14
    .line 15
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    const/4 p0, 0x0

    .line 22
    return p0

    .line 23
    :cond_1
    const-string v1, "lg"

    .line 24
    .line 25
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    move-result p0

    .line 29
    if-eqz p0, :cond_2

    .line 30
    .line 31
    const/4 p0, 0x2

    .line 32
    return p0

    .line 33
    :cond_2
    :goto_0
    return v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getPathHeadArrowPath(Lcom/intsig/office/common/shape/Arrow;ILcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;
    .locals 20

    .line 1
    invoke-interface/range {p2 .. p2}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_3

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    const/4 v3, 0x2

    .line 13
    if-ge v2, v3, :cond_0

    .line 14
    .line 15
    goto/16 :goto_0

    .line 16
    .line 17
    :cond_0
    const/4 v2, 0x0

    .line 18
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v4

    .line 22
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 23
    .line 24
    const-string v5, "pt"

    .line 25
    .line 26
    invoke-interface {v4, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 27
    .line 28
    .line 29
    move-result-object v4

    .line 30
    const-string v6, "x"

    .line 31
    .line 32
    invoke-interface {v4, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v7

    .line 36
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 37
    .line 38
    .line 39
    move-result v7

    .line 40
    int-to-float v7, v7

    .line 41
    const/high16 v8, 0x42c00000    # 96.0f

    .line 42
    .line 43
    mul-float v7, v7, v8

    .line 44
    .line 45
    const v9, 0x495f3e00    # 914400.0f

    .line 46
    .line 47
    .line 48
    div-float v16, v7, v9

    .line 49
    .line 50
    const-string v7, "y"

    .line 51
    .line 52
    invoke-interface {v4, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v4

    .line 56
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 57
    .line 58
    .line 59
    move-result v4

    .line 60
    int-to-float v4, v4

    .line 61
    mul-float v4, v4, v8

    .line 62
    .line 63
    div-float v17, v4, v9

    .line 64
    .line 65
    const/4 v4, 0x1

    .line 66
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 71
    .line 72
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v10

    .line 76
    const-string v11, "lnTo"

    .line 77
    .line 78
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 79
    .line 80
    .line 81
    move-result v10

    .line 82
    if-eqz v10, :cond_1

    .line 83
    .line 84
    invoke-interface {v0, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    invoke-interface {v0, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v1

    .line 92
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    int-to-float v1, v1

    .line 97
    mul-float v1, v1, v8

    .line 98
    .line 99
    div-float v10, v1, v9

    .line 100
    .line 101
    invoke-interface {v0, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 106
    .line 107
    .line 108
    move-result v0

    .line 109
    int-to-float v0, v0

    .line 110
    mul-float v0, v0, v8

    .line 111
    .line 112
    div-float v11, v0, v9

    .line 113
    .line 114
    move/from16 v12, v16

    .line 115
    .line 116
    move/from16 v13, v17

    .line 117
    .line 118
    move-object/from16 v14, p0

    .line 119
    .line 120
    move/from16 v15, p1

    .line 121
    .line 122
    invoke-static/range {v10 .. v15}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 123
    .line 124
    .line 125
    move-result-object v1

    .line 126
    goto/16 :goto_0

    .line 127
    .line 128
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v5

    .line 132
    const-string v10, "quadBezTo"

    .line 133
    .line 134
    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 135
    .line 136
    .line 137
    move-result v5

    .line 138
    if-eqz v5, :cond_2

    .line 139
    .line 140
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 145
    .line 146
    .line 147
    move-result v5

    .line 148
    if-ne v5, v3, :cond_3

    .line 149
    .line 150
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 151
    .line 152
    .line 153
    move-result-object v1

    .line 154
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 155
    .line 156
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object v1

    .line 160
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 161
    .line 162
    .line 163
    move-result v1

    .line 164
    int-to-float v1, v1

    .line 165
    mul-float v1, v1, v8

    .line 166
    .line 167
    div-float v12, v1, v9

    .line 168
    .line 169
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 170
    .line 171
    .line 172
    move-result-object v1

    .line 173
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 174
    .line 175
    invoke-interface {v1, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 176
    .line 177
    .line 178
    move-result-object v1

    .line 179
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 180
    .line 181
    .line 182
    move-result v1

    .line 183
    int-to-float v1, v1

    .line 184
    mul-float v1, v1, v8

    .line 185
    .line 186
    div-float v13, v1, v9

    .line 187
    .line 188
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 189
    .line 190
    .line 191
    move-result-object v1

    .line 192
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 193
    .line 194
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 195
    .line 196
    .line 197
    move-result-object v1

    .line 198
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 199
    .line 200
    .line 201
    move-result v1

    .line 202
    int-to-float v1, v1

    .line 203
    mul-float v1, v1, v8

    .line 204
    .line 205
    div-float v10, v1, v9

    .line 206
    .line 207
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 208
    .line 209
    .line 210
    move-result-object v0

    .line 211
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 212
    .line 213
    invoke-interface {v0, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 214
    .line 215
    .line 216
    move-result-object v0

    .line 217
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 218
    .line 219
    .line 220
    move-result v0

    .line 221
    int-to-float v0, v0

    .line 222
    mul-float v0, v0, v8

    .line 223
    .line 224
    div-float v11, v0, v9

    .line 225
    .line 226
    move/from16 v14, v16

    .line 227
    .line 228
    move/from16 v15, v17

    .line 229
    .line 230
    move-object/from16 v16, p0

    .line 231
    .line 232
    move/from16 v17, p1

    .line 233
    .line 234
    invoke-static/range {v10 .. v17}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getQuadBezArrowPath(FFFFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 235
    .line 236
    .line 237
    move-result-object v1

    .line 238
    goto/16 :goto_0

    .line 239
    .line 240
    :cond_2
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 241
    .line 242
    .line 243
    move-result-object v5

    .line 244
    const-string v10, "cubicBezTo"

    .line 245
    .line 246
    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 247
    .line 248
    .line 249
    move-result v5

    .line 250
    if-eqz v5, :cond_3

    .line 251
    .line 252
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 253
    .line 254
    .line 255
    move-result-object v0

    .line 256
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 257
    .line 258
    .line 259
    move-result v5

    .line 260
    const/4 v10, 0x3

    .line 261
    if-ne v5, v10, :cond_3

    .line 262
    .line 263
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 264
    .line 265
    .line 266
    move-result-object v1

    .line 267
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 268
    .line 269
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 270
    .line 271
    .line 272
    move-result-object v1

    .line 273
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 274
    .line 275
    .line 276
    move-result v1

    .line 277
    int-to-float v1, v1

    .line 278
    mul-float v1, v1, v8

    .line 279
    .line 280
    div-float v14, v1, v9

    .line 281
    .line 282
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 283
    .line 284
    .line 285
    move-result-object v1

    .line 286
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 287
    .line 288
    invoke-interface {v1, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 289
    .line 290
    .line 291
    move-result-object v1

    .line 292
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 293
    .line 294
    .line 295
    move-result v1

    .line 296
    int-to-float v1, v1

    .line 297
    mul-float v1, v1, v8

    .line 298
    .line 299
    div-float v15, v1, v9

    .line 300
    .line 301
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 302
    .line 303
    .line 304
    move-result-object v1

    .line 305
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 306
    .line 307
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 308
    .line 309
    .line 310
    move-result-object v1

    .line 311
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 312
    .line 313
    .line 314
    move-result v1

    .line 315
    int-to-float v1, v1

    .line 316
    mul-float v1, v1, v8

    .line 317
    .line 318
    div-float v12, v1, v9

    .line 319
    .line 320
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 321
    .line 322
    .line 323
    move-result-object v1

    .line 324
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 325
    .line 326
    invoke-interface {v1, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 327
    .line 328
    .line 329
    move-result-object v1

    .line 330
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 331
    .line 332
    .line 333
    move-result v1

    .line 334
    int-to-float v1, v1

    .line 335
    mul-float v1, v1, v8

    .line 336
    .line 337
    div-float v13, v1, v9

    .line 338
    .line 339
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 340
    .line 341
    .line 342
    move-result-object v1

    .line 343
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 344
    .line 345
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 346
    .line 347
    .line 348
    move-result-object v1

    .line 349
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 350
    .line 351
    .line 352
    move-result v1

    .line 353
    int-to-float v1, v1

    .line 354
    mul-float v1, v1, v8

    .line 355
    .line 356
    div-float v10, v1, v9

    .line 357
    .line 358
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 359
    .line 360
    .line 361
    move-result-object v0

    .line 362
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 363
    .line 364
    invoke-interface {v0, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 365
    .line 366
    .line 367
    move-result-object v0

    .line 368
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 369
    .line 370
    .line 371
    move-result v0

    .line 372
    int-to-float v0, v0

    .line 373
    mul-float v0, v0, v8

    .line 374
    .line 375
    div-float v11, v0, v9

    .line 376
    .line 377
    move-object/from16 v18, p0

    .line 378
    .line 379
    move/from16 v19, p1

    .line 380
    .line 381
    invoke-static/range {v10 .. v19}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getCubicBezArrowPath(FFFFFFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 382
    .line 383
    .line 384
    move-result-object v1

    .line 385
    :cond_3
    :goto_0
    return-object v1
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method public static getPathTailArrowPath(Lcom/intsig/office/common/shape/Arrow;ILcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;
    .locals 26

    .line 1
    invoke-interface/range {p2 .. p2}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_7

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    const/4 v3, 0x2

    .line 13
    if-lt v2, v3, :cond_7

    .line 14
    .line 15
    add-int/lit8 v4, v2, -0x1

    .line 16
    .line 17
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v5

    .line 21
    check-cast v5, Lcom/intsig/office/fc/dom4j/Element;

    .line 22
    .line 23
    invoke-interface {v5}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v5

    .line 27
    const-string v6, "close"

    .line 28
    .line 29
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    move-result v5

    .line 33
    if-eqz v5, :cond_0

    .line 34
    .line 35
    goto/16 :goto_3

    .line 36
    .line 37
    :cond_0
    sub-int/2addr v2, v3

    .line 38
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    move-result-object v2

    .line 42
    check-cast v2, Lcom/intsig/office/fc/dom4j/Element;

    .line 43
    .line 44
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v5

    .line 48
    const-string v6, "lnTo"

    .line 49
    .line 50
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    move-result v5

    .line 54
    const/4 v7, 0x3

    .line 55
    const-string v8, "cubicBezTo"

    .line 56
    .line 57
    const-string v9, "pt"

    .line 58
    .line 59
    const-string v10, "quadBezTo"

    .line 60
    .line 61
    const/4 v11, 0x1

    .line 62
    const-string v12, "y"

    .line 63
    .line 64
    const-string v13, "x"

    .line 65
    .line 66
    const v14, 0x495f3e00    # 914400.0f

    .line 67
    .line 68
    .line 69
    const/high16 v15, 0x42c00000    # 96.0f

    .line 70
    .line 71
    if-eqz v5, :cond_1

    .line 72
    .line 73
    invoke-interface {v2, v9}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    invoke-interface {v2, v13}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v5

    .line 81
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 82
    .line 83
    .line 84
    move-result v5

    .line 85
    int-to-float v5, v5

    .line 86
    mul-float v5, v5, v15

    .line 87
    .line 88
    div-float/2addr v5, v14

    .line 89
    invoke-interface {v2, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v2

    .line 93
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 94
    .line 95
    .line 96
    move-result v2

    .line 97
    :goto_0
    int-to-float v2, v2

    .line 98
    mul-float v2, v2, v15

    .line 99
    .line 100
    div-float/2addr v2, v14

    .line 101
    :goto_1
    move/from16 v17, v2

    .line 102
    .line 103
    move/from16 v16, v5

    .line 104
    .line 105
    goto :goto_2

    .line 106
    :cond_1
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v5

    .line 110
    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 111
    .line 112
    .line 113
    move-result v5

    .line 114
    const/16 v16, 0x0

    .line 115
    .line 116
    if-eqz v5, :cond_3

    .line 117
    .line 118
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 119
    .line 120
    .line 121
    move-result-object v2

    .line 122
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 123
    .line 124
    .line 125
    move-result v5

    .line 126
    if-ne v5, v3, :cond_2

    .line 127
    .line 128
    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 129
    .line 130
    .line 131
    move-result-object v5

    .line 132
    check-cast v5, Lcom/intsig/office/fc/dom4j/Element;

    .line 133
    .line 134
    invoke-interface {v5, v13}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 135
    .line 136
    .line 137
    move-result-object v5

    .line 138
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 139
    .line 140
    .line 141
    move-result v5

    .line 142
    int-to-float v5, v5

    .line 143
    mul-float v5, v5, v15

    .line 144
    .line 145
    div-float/2addr v5, v14

    .line 146
    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 147
    .line 148
    .line 149
    move-result-object v2

    .line 150
    check-cast v2, Lcom/intsig/office/fc/dom4j/Element;

    .line 151
    .line 152
    invoke-interface {v2, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 153
    .line 154
    .line 155
    move-result-object v2

    .line 156
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 157
    .line 158
    .line 159
    move-result v2

    .line 160
    goto :goto_0

    .line 161
    :cond_2
    const/4 v2, 0x0

    .line 162
    const/4 v5, 0x0

    .line 163
    goto :goto_1

    .line 164
    :cond_3
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 165
    .line 166
    .line 167
    move-result-object v5

    .line 168
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 169
    .line 170
    .line 171
    move-result v5

    .line 172
    if-eqz v5, :cond_4

    .line 173
    .line 174
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 175
    .line 176
    .line 177
    move-result-object v2

    .line 178
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 179
    .line 180
    .line 181
    move-result v5

    .line 182
    if-ne v5, v7, :cond_4

    .line 183
    .line 184
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 185
    .line 186
    .line 187
    move-result-object v5

    .line 188
    check-cast v5, Lcom/intsig/office/fc/dom4j/Element;

    .line 189
    .line 190
    invoke-interface {v5, v13}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 191
    .line 192
    .line 193
    move-result-object v5

    .line 194
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 195
    .line 196
    .line 197
    move-result v5

    .line 198
    int-to-float v5, v5

    .line 199
    mul-float v5, v5, v15

    .line 200
    .line 201
    div-float/2addr v5, v14

    .line 202
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 203
    .line 204
    .line 205
    move-result-object v2

    .line 206
    check-cast v2, Lcom/intsig/office/fc/dom4j/Element;

    .line 207
    .line 208
    invoke-interface {v2, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 209
    .line 210
    .line 211
    move-result-object v2

    .line 212
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 213
    .line 214
    .line 215
    move-result v2

    .line 216
    goto :goto_0

    .line 217
    :cond_4
    const/16 v17, 0x0

    .line 218
    .line 219
    :goto_2
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 220
    .line 221
    .line 222
    move-result-object v0

    .line 223
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 224
    .line 225
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 226
    .line 227
    .line 228
    move-result-object v2

    .line 229
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 230
    .line 231
    .line 232
    move-result v2

    .line 233
    if-eqz v2, :cond_5

    .line 234
    .line 235
    invoke-interface {v0, v9}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 236
    .line 237
    .line 238
    move-result-object v0

    .line 239
    invoke-interface {v0, v13}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 240
    .line 241
    .line 242
    move-result-object v1

    .line 243
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 244
    .line 245
    .line 246
    move-result v1

    .line 247
    int-to-float v1, v1

    .line 248
    mul-float v1, v1, v15

    .line 249
    .line 250
    div-float v18, v1, v14

    .line 251
    .line 252
    invoke-interface {v0, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 253
    .line 254
    .line 255
    move-result-object v0

    .line 256
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 257
    .line 258
    .line 259
    move-result v0

    .line 260
    int-to-float v0, v0

    .line 261
    mul-float v0, v0, v15

    .line 262
    .line 263
    div-float v19, v0, v14

    .line 264
    .line 265
    move-object/from16 v20, p0

    .line 266
    .line 267
    move/from16 v21, p1

    .line 268
    .line 269
    invoke-static/range {v16 .. v21}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 270
    .line 271
    .line 272
    move-result-object v1

    .line 273
    goto/16 :goto_3

    .line 274
    .line 275
    :cond_5
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 276
    .line 277
    .line 278
    move-result-object v2

    .line 279
    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 280
    .line 281
    .line 282
    move-result v2

    .line 283
    const/4 v4, 0x0

    .line 284
    if-eqz v2, :cond_6

    .line 285
    .line 286
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 287
    .line 288
    .line 289
    move-result-object v0

    .line 290
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 291
    .line 292
    .line 293
    move-result v2

    .line 294
    if-ne v2, v3, :cond_7

    .line 295
    .line 296
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 297
    .line 298
    .line 299
    move-result-object v1

    .line 300
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 301
    .line 302
    invoke-interface {v1, v13}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 303
    .line 304
    .line 305
    move-result-object v1

    .line 306
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 307
    .line 308
    .line 309
    move-result v1

    .line 310
    int-to-float v1, v1

    .line 311
    mul-float v1, v1, v15

    .line 312
    .line 313
    div-float v18, v1, v14

    .line 314
    .line 315
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 316
    .line 317
    .line 318
    move-result-object v1

    .line 319
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 320
    .line 321
    invoke-interface {v1, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 322
    .line 323
    .line 324
    move-result-object v1

    .line 325
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 326
    .line 327
    .line 328
    move-result v1

    .line 329
    int-to-float v1, v1

    .line 330
    mul-float v1, v1, v15

    .line 331
    .line 332
    div-float v19, v1, v14

    .line 333
    .line 334
    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 335
    .line 336
    .line 337
    move-result-object v1

    .line 338
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 339
    .line 340
    invoke-interface {v1, v13}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 341
    .line 342
    .line 343
    move-result-object v1

    .line 344
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 345
    .line 346
    .line 347
    move-result v1

    .line 348
    int-to-float v1, v1

    .line 349
    mul-float v1, v1, v15

    .line 350
    .line 351
    div-float v20, v1, v14

    .line 352
    .line 353
    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 354
    .line 355
    .line 356
    move-result-object v0

    .line 357
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 358
    .line 359
    invoke-interface {v0, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 360
    .line 361
    .line 362
    move-result-object v0

    .line 363
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 364
    .line 365
    .line 366
    move-result v0

    .line 367
    int-to-float v0, v0

    .line 368
    mul-float v0, v0, v15

    .line 369
    .line 370
    div-float v21, v0, v14

    .line 371
    .line 372
    move-object/from16 v22, p0

    .line 373
    .line 374
    move/from16 v23, p1

    .line 375
    .line 376
    invoke-static/range {v16 .. v23}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getQuadBezArrowPath(FFFFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 377
    .line 378
    .line 379
    move-result-object v1

    .line 380
    goto/16 :goto_3

    .line 381
    .line 382
    :cond_6
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 383
    .line 384
    .line 385
    move-result-object v2

    .line 386
    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 387
    .line 388
    .line 389
    move-result v2

    .line 390
    if-eqz v2, :cond_7

    .line 391
    .line 392
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 393
    .line 394
    .line 395
    move-result-object v0

    .line 396
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 397
    .line 398
    .line 399
    move-result v2

    .line 400
    if-ne v2, v7, :cond_7

    .line 401
    .line 402
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 403
    .line 404
    .line 405
    move-result-object v1

    .line 406
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 407
    .line 408
    invoke-interface {v1, v13}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 409
    .line 410
    .line 411
    move-result-object v1

    .line 412
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 413
    .line 414
    .line 415
    move-result v1

    .line 416
    int-to-float v1, v1

    .line 417
    mul-float v1, v1, v15

    .line 418
    .line 419
    div-float v18, v1, v14

    .line 420
    .line 421
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 422
    .line 423
    .line 424
    move-result-object v1

    .line 425
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 426
    .line 427
    invoke-interface {v1, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 428
    .line 429
    .line 430
    move-result-object v1

    .line 431
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 432
    .line 433
    .line 434
    move-result v1

    .line 435
    int-to-float v1, v1

    .line 436
    mul-float v1, v1, v15

    .line 437
    .line 438
    div-float v19, v1, v14

    .line 439
    .line 440
    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 441
    .line 442
    .line 443
    move-result-object v1

    .line 444
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 445
    .line 446
    invoke-interface {v1, v13}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 447
    .line 448
    .line 449
    move-result-object v1

    .line 450
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 451
    .line 452
    .line 453
    move-result v1

    .line 454
    int-to-float v1, v1

    .line 455
    mul-float v1, v1, v15

    .line 456
    .line 457
    div-float v20, v1, v14

    .line 458
    .line 459
    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 460
    .line 461
    .line 462
    move-result-object v1

    .line 463
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 464
    .line 465
    invoke-interface {v1, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 466
    .line 467
    .line 468
    move-result-object v1

    .line 469
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 470
    .line 471
    .line 472
    move-result v1

    .line 473
    int-to-float v1, v1

    .line 474
    mul-float v1, v1, v15

    .line 475
    .line 476
    div-float v21, v1, v14

    .line 477
    .line 478
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 479
    .line 480
    .line 481
    move-result-object v1

    .line 482
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 483
    .line 484
    invoke-interface {v1, v13}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 485
    .line 486
    .line 487
    move-result-object v1

    .line 488
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 489
    .line 490
    .line 491
    move-result v1

    .line 492
    int-to-float v1, v1

    .line 493
    mul-float v1, v1, v15

    .line 494
    .line 495
    div-float v22, v1, v14

    .line 496
    .line 497
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 498
    .line 499
    .line 500
    move-result-object v0

    .line 501
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 502
    .line 503
    invoke-interface {v0, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 504
    .line 505
    .line 506
    move-result-object v0

    .line 507
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 508
    .line 509
    .line 510
    move-result v0

    .line 511
    int-to-float v0, v0

    .line 512
    mul-float v0, v0, v15

    .line 513
    .line 514
    div-float v23, v0, v14

    .line 515
    .line 516
    move-object/from16 v24, p0

    .line 517
    .line 518
    move/from16 v25, p1

    .line 519
    .line 520
    invoke-static/range {v16 .. v25}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getCubicBezArrowPath(FFFFFFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 521
    .line 522
    .line 523
    move-result-object v1

    .line 524
    :cond_7
    :goto_3
    return-object v1
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method public static processArbitraryPolygonShape(Lcom/intsig/office/common/shape/ArbitraryPolygonShape;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/bg/BackgroundAndFill;ZLcom/intsig/office/common/bg/BackgroundAndFill;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)V
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    move-object/from16 v7, p0

    .line 2
    .line 3
    move-object/from16 v8, p2

    .line 4
    .line 5
    move-object/from16 v0, p4

    .line 6
    .line 7
    move-object/from16 v1, p5

    .line 8
    .line 9
    move-object/from16 v9, p6

    .line 10
    .line 11
    if-nez v7, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    const v10, 0x495f3e00    # 914400.0f

    .line 15
    .line 16
    .line 17
    const/high16 v11, 0x42c00000    # 96.0f

    .line 18
    .line 19
    const/4 v2, 0x1

    .line 20
    const-string v12, "w"

    .line 21
    .line 22
    if-eqz v1, :cond_1

    .line 23
    .line 24
    invoke-interface {v1, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    if-eqz v3, :cond_1

    .line 29
    .line 30
    invoke-interface {v1, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v3

    .line 34
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    int-to-float v3, v3

    .line 39
    mul-float v3, v3, v11

    .line 40
    .line 41
    div-float/2addr v3, v10

    .line 42
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 43
    .line 44
    .line 45
    move-result v3

    .line 46
    goto :goto_0

    .line 47
    :cond_1
    const/4 v3, 0x1

    .line 48
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->createLine()Lcom/intsig/office/common/borders/Line;

    .line 49
    .line 50
    .line 51
    move-result-object v13

    .line 52
    invoke-virtual {v13, v0}, Lcom/intsig/office/common/borders/Line;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v13, v3}, Lcom/intsig/office/common/borders/Border;->setLineWidth(I)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v7, v9}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 59
    .line 60
    .line 61
    const-string v4, "spPr"

    .line 62
    .line 63
    move-object/from16 v5, p1

    .line 64
    .line 65
    invoke-interface {v5, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 66
    .line 67
    .line 68
    move-result-object v4

    .line 69
    const-string v5, "custGeom"

    .line 70
    .line 71
    invoke-interface {v4, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 72
    .line 73
    .line 74
    move-result-object v4

    .line 75
    const-string v5, "pathLst"

    .line 76
    .line 77
    invoke-interface {v4, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 78
    .line 79
    .line 80
    move-result-object v4

    .line 81
    const-string v5, "path"

    .line 82
    .line 83
    invoke-interface {v4, v5}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 84
    .line 85
    .line 86
    move-result-object v14

    .line 87
    invoke-interface {v14}, Ljava/util/List;->size()I

    .line 88
    .line 89
    .line 90
    move-result v4

    .line 91
    const-string v15, "h"

    .line 92
    .line 93
    const-string v6, "none"

    .line 94
    .line 95
    const-string v10, "stroke"

    .line 96
    .line 97
    const/4 v11, 0x0

    .line 98
    if-ne v4, v2, :cond_11

    .line 99
    .line 100
    if-eqz v1, :cond_11

    .line 101
    .line 102
    invoke-interface {v14, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 103
    .line 104
    .line 105
    move-result-object v4

    .line 106
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 107
    .line 108
    invoke-interface {v4, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object v4

    .line 112
    invoke-interface {v14, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 113
    .line 114
    .line 115
    move-result-object v18

    .line 116
    move-object/from16 v5, v18

    .line 117
    .line 118
    check-cast v5, Lcom/intsig/office/fc/dom4j/Element;

    .line 119
    .line 120
    invoke-interface {v5, v15}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v5

    .line 124
    if-eqz v4, :cond_2

    .line 125
    .line 126
    if-eqz v5, :cond_2

    .line 127
    .line 128
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 129
    .line 130
    .line 131
    move-result v4

    .line 132
    int-to-float v4, v4

    .line 133
    const/high16 v17, 0x42c00000    # 96.0f

    .line 134
    .line 135
    mul-float v4, v4, v17

    .line 136
    .line 137
    const v16, 0x495f3e00    # 914400.0f

    .line 138
    .line 139
    .line 140
    div-float v4, v4, v16

    .line 141
    .line 142
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 143
    .line 144
    .line 145
    move-result v5

    .line 146
    int-to-float v5, v5

    .line 147
    mul-float v5, v5, v17

    .line 148
    .line 149
    div-float v5, v5, v16

    .line 150
    .line 151
    int-to-float v3, v3

    .line 152
    iget v2, v9, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 153
    .line 154
    int-to-float v2, v2

    .line 155
    div-float/2addr v4, v2

    .line 156
    iget v2, v9, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 157
    .line 158
    int-to-float v2, v2

    .line 159
    div-float/2addr v5, v2

    .line 160
    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    .line 161
    .line 162
    .line 163
    move-result v2

    .line 164
    mul-float v3, v3, v2

    .line 165
    .line 166
    float-to-int v3, v3

    .line 167
    :cond_2
    const-string v2, "headEnd"

    .line 168
    .line 169
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 170
    .line 171
    .line 172
    move-result-object v2

    .line 173
    const-string v5, "len"

    .line 174
    .line 175
    const-string v4, "type"

    .line 176
    .line 177
    if-eqz v2, :cond_9

    .line 178
    .line 179
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 180
    .line 181
    .line 182
    move-result-object v19

    .line 183
    if-eqz v19, :cond_9

    .line 184
    .line 185
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v11

    .line 189
    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 190
    .line 191
    .line 192
    move-result v11

    .line 193
    if-nez v11, :cond_9

    .line 194
    .line 195
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 196
    .line 197
    .line 198
    move-result-object v11

    .line 199
    invoke-static {v11}, Lcom/intsig/office/common/shape/Arrow;->getArrowType(Ljava/lang/String;)B

    .line 200
    .line 201
    .line 202
    move-result v11

    .line 203
    invoke-interface {v2, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 204
    .line 205
    .line 206
    move-result-object v20

    .line 207
    invoke-static/range {v20 .. v20}, Lcom/intsig/office/common/autoshape/ArbitraryPolygonShapePath;->getArrowSize(Ljava/lang/String;)I

    .line 208
    .line 209
    .line 210
    move-result v9

    .line 211
    invoke-interface {v2, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 212
    .line 213
    .line 214
    move-result-object v2

    .line 215
    invoke-static {v2}, Lcom/intsig/office/common/autoshape/ArbitraryPolygonShapePath;->getArrowSize(Ljava/lang/String;)I

    .line 216
    .line 217
    .line 218
    move-result v2

    .line 219
    invoke-virtual {v7, v11, v9, v2}, Lcom/intsig/office/common/shape/LineShape;->createStartArrow(BII)V

    .line 220
    .line 221
    .line 222
    const/4 v2, 0x0

    .line 223
    invoke-interface {v14, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 224
    .line 225
    .line 226
    move-result-object v9

    .line 227
    check-cast v9, Lcom/intsig/office/fc/dom4j/Element;

    .line 228
    .line 229
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 230
    .line 231
    .line 232
    move-result-object v2

    .line 233
    invoke-static {v2, v3, v9}, Lcom/intsig/office/common/autoshape/ArbitraryPolygonShapePath;->getPathHeadArrowPath(Lcom/intsig/office/common/shape/Arrow;ILcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 234
    .line 235
    .line 236
    move-result-object v2

    .line 237
    if-eqz v2, :cond_9

    .line 238
    .line 239
    move-object/from16 v20, v15

    .line 240
    .line 241
    invoke-virtual {v2}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 242
    .line 243
    .line 244
    move-result-object v15

    .line 245
    invoke-virtual {v2}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 246
    .line 247
    .line 248
    move-result-object v2

    .line 249
    move-object/from16 v21, v2

    .line 250
    .line 251
    if-eqz v15, :cond_8

    .line 252
    .line 253
    new-instance v2, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 254
    .line 255
    invoke-direct {v2}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 256
    .line 257
    .line 258
    move/from16 v22, v3

    .line 259
    .line 260
    const/4 v3, 0x1

    .line 261
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 262
    .line 263
    .line 264
    invoke-virtual {v2, v15}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 265
    .line 266
    .line 267
    if-nez v8, :cond_3

    .line 268
    .line 269
    if-eqz p3, :cond_7

    .line 270
    .line 271
    :cond_3
    if-eqz p3, :cond_6

    .line 272
    .line 273
    invoke-interface {v9, v10}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 274
    .line 275
    .line 276
    move-result-object v3

    .line 277
    if-eqz v3, :cond_4

    .line 278
    .line 279
    invoke-interface {v9, v10}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 280
    .line 281
    .line 282
    move-result-object v3

    .line 283
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 284
    .line 285
    .line 286
    move-result v3

    .line 287
    if-eqz v3, :cond_6

    .line 288
    .line 289
    :cond_4
    const/4 v3, 0x5

    .line 290
    if-eq v11, v3, :cond_5

    .line 291
    .line 292
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 293
    .line 294
    .line 295
    goto :goto_1

    .line 296
    :cond_5
    invoke-virtual {v2, v13}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 297
    .line 298
    .line 299
    goto :goto_1

    .line 300
    :cond_6
    if-eqz v8, :cond_7

    .line 301
    .line 302
    invoke-virtual {v2, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 303
    .line 304
    .line 305
    :cond_7
    :goto_1
    move-object v3, v2

    .line 306
    move-object/from16 v2, v21

    .line 307
    .line 308
    goto :goto_3

    .line 309
    :cond_8
    move/from16 v22, v3

    .line 310
    .line 311
    goto :goto_2

    .line 312
    :cond_9
    move/from16 v22, v3

    .line 313
    .line 314
    move-object/from16 v20, v15

    .line 315
    .line 316
    const/4 v2, 0x0

    .line 317
    :goto_2
    const/4 v3, 0x0

    .line 318
    :goto_3
    const-string v9, "tailEnd"

    .line 319
    .line 320
    invoke-interface {v1, v9}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 321
    .line 322
    .line 323
    move-result-object v1

    .line 324
    if-eqz v1, :cond_10

    .line 325
    .line 326
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 327
    .line 328
    .line 329
    move-result-object v9

    .line 330
    if-eqz v9, :cond_10

    .line 331
    .line 332
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 333
    .line 334
    .line 335
    move-result-object v9

    .line 336
    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 337
    .line 338
    .line 339
    move-result v9

    .line 340
    if-nez v9, :cond_10

    .line 341
    .line 342
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 343
    .line 344
    .line 345
    move-result-object v4

    .line 346
    invoke-static {v4}, Lcom/intsig/office/common/shape/Arrow;->getArrowType(Ljava/lang/String;)B

    .line 347
    .line 348
    .line 349
    move-result v4

    .line 350
    invoke-interface {v1, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 351
    .line 352
    .line 353
    move-result-object v9

    .line 354
    invoke-static {v9}, Lcom/intsig/office/common/autoshape/ArbitraryPolygonShapePath;->getArrowSize(Ljava/lang/String;)I

    .line 355
    .line 356
    .line 357
    move-result v9

    .line 358
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 359
    .line 360
    .line 361
    move-result-object v1

    .line 362
    invoke-static {v1}, Lcom/intsig/office/common/autoshape/ArbitraryPolygonShapePath;->getArrowSize(Ljava/lang/String;)I

    .line 363
    .line 364
    .line 365
    move-result v1

    .line 366
    invoke-virtual {v7, v4, v9, v1}, Lcom/intsig/office/common/shape/LineShape;->createEndArrow(BII)V

    .line 367
    .line 368
    .line 369
    const/4 v1, 0x0

    .line 370
    invoke-interface {v14, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 371
    .line 372
    .line 373
    move-result-object v5

    .line 374
    check-cast v5, Lcom/intsig/office/fc/dom4j/Element;

    .line 375
    .line 376
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 377
    .line 378
    .line 379
    move-result-object v1

    .line 380
    move/from16 v9, v22

    .line 381
    .line 382
    invoke-static {v1, v9, v5}, Lcom/intsig/office/common/autoshape/ArbitraryPolygonShapePath;->getPathTailArrowPath(Lcom/intsig/office/common/shape/Arrow;ILcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 383
    .line 384
    .line 385
    move-result-object v1

    .line 386
    if-eqz v1, :cond_10

    .line 387
    .line 388
    invoke-virtual {v1}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 389
    .line 390
    .line 391
    move-result-object v9

    .line 392
    invoke-virtual {v1}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 393
    .line 394
    .line 395
    move-result-object v1

    .line 396
    if-eqz v9, :cond_f

    .line 397
    .line 398
    new-instance v11, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 399
    .line 400
    invoke-direct {v11}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 401
    .line 402
    .line 403
    const/4 v15, 0x1

    .line 404
    invoke-virtual {v11, v15}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 405
    .line 406
    .line 407
    invoke-virtual {v11, v9}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 408
    .line 409
    .line 410
    if-nez v8, :cond_a

    .line 411
    .line 412
    if-eqz p3, :cond_e

    .line 413
    .line 414
    :cond_a
    if-eqz p3, :cond_d

    .line 415
    .line 416
    invoke-interface {v5, v10}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 417
    .line 418
    .line 419
    move-result-object v9

    .line 420
    if-eqz v9, :cond_b

    .line 421
    .line 422
    invoke-interface {v5, v10}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 423
    .line 424
    .line 425
    move-result-object v5

    .line 426
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 427
    .line 428
    .line 429
    move-result v5

    .line 430
    if-eqz v5, :cond_d

    .line 431
    .line 432
    :cond_b
    const/4 v5, 0x5

    .line 433
    if-eq v4, v5, :cond_c

    .line 434
    .line 435
    invoke-virtual {v11, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 436
    .line 437
    .line 438
    goto :goto_4

    .line 439
    :cond_c
    invoke-virtual {v11, v13}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 440
    .line 441
    .line 442
    goto :goto_4

    .line 443
    :cond_d
    if-eqz v8, :cond_e

    .line 444
    .line 445
    invoke-virtual {v11, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 446
    .line 447
    .line 448
    :cond_e
    :goto_4
    move-object v9, v1

    .line 449
    move-object v15, v3

    .line 450
    move-object v5, v11

    .line 451
    move-object v11, v2

    .line 452
    goto :goto_5

    .line 453
    :cond_f
    move-object v9, v1

    .line 454
    move-object v11, v2

    .line 455
    move-object v15, v3

    .line 456
    const/4 v5, 0x0

    .line 457
    goto :goto_5

    .line 458
    :cond_10
    move-object v11, v2

    .line 459
    move-object v15, v3

    .line 460
    const/4 v5, 0x0

    .line 461
    const/4 v9, 0x0

    .line 462
    goto :goto_5

    .line 463
    :cond_11
    move-object/from16 v20, v15

    .line 464
    .line 465
    const/4 v5, 0x0

    .line 466
    const/4 v9, 0x0

    .line 467
    const/4 v11, 0x0

    .line 468
    const/4 v15, 0x0

    .line 469
    :goto_5
    const/4 v4, 0x0

    .line 470
    :goto_6
    invoke-interface {v14}, Ljava/util/List;->size()I

    .line 471
    .line 472
    .line 473
    move-result v0

    .line 474
    if-ge v4, v0, :cond_1b

    .line 475
    .line 476
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 477
    .line 478
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 479
    .line 480
    .line 481
    invoke-interface {v14, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 482
    .line 483
    .line 484
    move-result-object v0

    .line 485
    move-object v1, v0

    .line 486
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 487
    .line 488
    move-object/from16 v0, p0

    .line 489
    .line 490
    move-object/from16 v2, p6

    .line 491
    .line 492
    move-object/from16 p4, v15

    .line 493
    .line 494
    move-object v15, v3

    .line 495
    move-object/from16 v3, p2

    .line 496
    .line 497
    move v7, v4

    .line 498
    move/from16 v4, p3

    .line 499
    .line 500
    move-object/from16 v23, v5

    .line 501
    .line 502
    move-object/from16 v18, v13

    .line 503
    .line 504
    const/4 v13, 0x0

    .line 505
    move-object v5, v11

    .line 506
    move-object v13, v6

    .line 507
    move-object v6, v9

    .line 508
    invoke-static/range {v0 .. v6}, Lcom/intsig/office/common/autoshape/ArbitraryPolygonShapePath;->getArrowPath(Lcom/intsig/office/common/shape/ArbitraryPolygonShape;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;Lcom/intsig/office/common/bg/BackgroundAndFill;ZLandroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/Path;

    .line 509
    .line 510
    .line 511
    move-result-object v0

    .line 512
    invoke-virtual {v15, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 513
    .line 514
    .line 515
    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 516
    .line 517
    .line 518
    move-result-object v1

    .line 519
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 520
    .line 521
    invoke-interface {v1, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 522
    .line 523
    .line 524
    move-result-object v2

    .line 525
    move-object/from16 v3, v20

    .line 526
    .line 527
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 528
    .line 529
    .line 530
    move-result-object v4

    .line 531
    new-instance v5, Landroid/graphics/Matrix;

    .line 532
    .line 533
    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 534
    .line 535
    .line 536
    if-eqz v2, :cond_12

    .line 537
    .line 538
    if-eqz v4, :cond_12

    .line 539
    .line 540
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 541
    .line 542
    .line 543
    move-result v2

    .line 544
    int-to-float v2, v2

    .line 545
    const/high16 v6, 0x42c00000    # 96.0f

    .line 546
    .line 547
    mul-float v2, v2, v6

    .line 548
    .line 549
    const v16, 0x495f3e00    # 914400.0f

    .line 550
    .line 551
    .line 552
    div-float v2, v2, v16

    .line 553
    .line 554
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 555
    .line 556
    .line 557
    move-result v4

    .line 558
    int-to-float v4, v4

    .line 559
    mul-float v4, v4, v6

    .line 560
    .line 561
    div-float v4, v4, v16

    .line 562
    .line 563
    move-object/from16 v6, p6

    .line 564
    .line 565
    move-object/from16 v20, v3

    .line 566
    .line 567
    iget v3, v6, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 568
    .line 569
    int-to-float v3, v3

    .line 570
    div-float/2addr v3, v2

    .line 571
    iget v2, v6, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 572
    .line 573
    int-to-float v2, v2

    .line 574
    div-float/2addr v2, v4

    .line 575
    invoke-virtual {v5, v3, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 576
    .line 577
    .line 578
    invoke-virtual {v0, v5}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 579
    .line 580
    .line 581
    goto :goto_7

    .line 582
    :cond_12
    move-object/from16 v6, p6

    .line 583
    .line 584
    move-object/from16 v20, v3

    .line 585
    .line 586
    const v16, 0x495f3e00    # 914400.0f

    .line 587
    .line 588
    .line 589
    :goto_7
    if-nez v8, :cond_14

    .line 590
    .line 591
    if-eqz p3, :cond_13

    .line 592
    .line 593
    goto :goto_9

    .line 594
    :cond_13
    const/4 v0, 0x0

    .line 595
    const/4 v1, 0x0

    .line 596
    :goto_8
    move-object/from16 v3, p0

    .line 597
    .line 598
    move/from16 v19, v7

    .line 599
    .line 600
    move-object/from16 v2, v18

    .line 601
    .line 602
    goto :goto_c

    .line 603
    :cond_14
    :goto_9
    if-eqz v8, :cond_16

    .line 604
    .line 605
    const-string v0, "fill"

    .line 606
    .line 607
    invoke-interface {v1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 608
    .line 609
    .line 610
    move-result-object v2

    .line 611
    if-eqz v2, :cond_15

    .line 612
    .line 613
    invoke-interface {v1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 614
    .line 615
    .line 616
    move-result-object v0

    .line 617
    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 618
    .line 619
    .line 620
    move-result v0

    .line 621
    if-eqz v0, :cond_15

    .line 622
    .line 623
    const/4 v0, 0x0

    .line 624
    invoke-virtual {v15, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 625
    .line 626
    .line 627
    goto :goto_a

    .line 628
    :cond_15
    const/4 v0, 0x0

    .line 629
    invoke-virtual {v15, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 630
    .line 631
    .line 632
    goto :goto_a

    .line 633
    :cond_16
    const/4 v0, 0x0

    .line 634
    :goto_a
    if-eqz p3, :cond_18

    .line 635
    .line 636
    invoke-interface {v1, v10}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 637
    .line 638
    .line 639
    move-result-object v2

    .line 640
    if-eqz v2, :cond_17

    .line 641
    .line 642
    invoke-interface {v1, v10}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 643
    .line 644
    .line 645
    move-result-object v1

    .line 646
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 647
    .line 648
    .line 649
    move-result v1

    .line 650
    if-nez v1, :cond_17

    .line 651
    .line 652
    const/4 v1, 0x0

    .line 653
    invoke-virtual {v15, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Z)V

    .line 654
    .line 655
    .line 656
    goto :goto_8

    .line 657
    :cond_17
    const/4 v1, 0x0

    .line 658
    move-object/from16 v2, v18

    .line 659
    .line 660
    invoke-virtual {v15, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 661
    .line 662
    .line 663
    goto :goto_b

    .line 664
    :cond_18
    move-object/from16 v2, v18

    .line 665
    .line 666
    const/4 v1, 0x0

    .line 667
    :goto_b
    move-object/from16 v3, p0

    .line 668
    .line 669
    move/from16 v19, v7

    .line 670
    .line 671
    :goto_c
    invoke-virtual {v3, v15}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 672
    .line 673
    .line 674
    if-eqz p4, :cond_19

    .line 675
    .line 676
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/office/common/autoshape/ExtendPath;->getPath()Landroid/graphics/Path;

    .line 677
    .line 678
    .line 679
    move-result-object v4

    .line 680
    invoke-virtual {v4, v5}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 681
    .line 682
    .line 683
    move-object/from16 v4, p4

    .line 684
    .line 685
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 686
    .line 687
    .line 688
    goto :goto_d

    .line 689
    :cond_19
    move-object/from16 v4, p4

    .line 690
    .line 691
    :goto_d
    move-object/from16 v7, v23

    .line 692
    .line 693
    if-eqz v7, :cond_1a

    .line 694
    .line 695
    invoke-virtual {v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->getPath()Landroid/graphics/Path;

    .line 696
    .line 697
    .line 698
    move-result-object v15

    .line 699
    invoke-virtual {v15, v5}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 700
    .line 701
    .line 702
    invoke-virtual {v3, v7}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 703
    .line 704
    .line 705
    :cond_1a
    add-int/lit8 v5, v19, 0x1

    .line 706
    .line 707
    move-object v15, v4

    .line 708
    move v4, v5

    .line 709
    move-object v5, v7

    .line 710
    move-object v6, v13

    .line 711
    move-object v13, v2

    .line 712
    move-object v7, v3

    .line 713
    goto/16 :goto_6

    .line 714
    .line 715
    :cond_1b
    return-void
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
.end method

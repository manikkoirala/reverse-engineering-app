.class public Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;
.super Ljava/lang/Object;
.source "RectPathBuilder.java"


# static fields
.field private static path:Landroid/graphics/Path;

.field private static rectF:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->rectF:Landroid/graphics/RectF;

    .line 7
    .line 8
    new-instance v0, Landroid/graphics/Path;

    .line 9
    .line 10
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static getRectPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, 0x1

    .line 11
    if-eq v0, v1, :cond_1

    .line 12
    .line 13
    const/4 v1, 0x2

    .line 14
    if-eq v0, v1, :cond_0

    .line 15
    .line 16
    const/16 v1, 0x88

    .line 17
    .line 18
    if-eq v0, v1, :cond_1

    .line 19
    .line 20
    const/16 v1, 0xca

    .line 21
    .line 22
    if-eq v0, v1, :cond_1

    .line 23
    .line 24
    packed-switch v0, :pswitch_data_0

    .line 25
    .line 26
    .line 27
    const/4 p0, 0x0

    .line 28
    return-object p0

    .line 29
    :pswitch_0
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->getSnipRoundPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 30
    .line 31
    .line 32
    move-result-object p0

    .line 33
    return-object p0

    .line 34
    :pswitch_1
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->getSnip2DiagPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 35
    .line 36
    .line 37
    move-result-object p0

    .line 38
    return-object p0

    .line 39
    :pswitch_2
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->getSnip2SameRectPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 40
    .line 41
    .line 42
    move-result-object p0

    .line 43
    return-object p0

    .line 44
    :pswitch_3
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->getSnip1RectPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 45
    .line 46
    .line 47
    move-result-object p0

    .line 48
    return-object p0

    .line 49
    :pswitch_4
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->getRound2DiagRectPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 50
    .line 51
    .line 52
    move-result-object p0

    .line 53
    return-object p0

    .line 54
    :pswitch_5
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->getRound2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 55
    .line 56
    .line 57
    move-result-object p0

    .line 58
    return-object p0

    .line 59
    :pswitch_6
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->getRound1Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 60
    .line 61
    .line 62
    move-result-object p0

    .line 63
    return-object p0

    .line 64
    :cond_0
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->getRoundRectanglePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 65
    .line 66
    .line 67
    move-result-object p0

    .line 68
    return-object p0

    .line 69
    :cond_1
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->getRectanglePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 70
    .line 71
    .line 72
    move-result-object p0

    .line 73
    return-object p0

    .line 74
    nop

    .line 75
    :pswitch_data_0
    .packed-switch 0xd2
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getRectanglePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 2
    .line 3
    iget p0, p1, Landroid/graphics/Rect;->left:I

    .line 4
    .line 5
    int-to-float v1, p0

    .line 6
    iget p0, p1, Landroid/graphics/Rect;->top:I

    .line 7
    .line 8
    int-to-float v2, p0

    .line 9
    iget p0, p1, Landroid/graphics/Rect;->right:I

    .line 10
    .line 11
    int-to-float v3, p0

    .line 12
    iget p0, p1, Landroid/graphics/Rect;->bottom:I

    .line 13
    .line 14
    int-to-float v4, p0

    .line 15
    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 16
    .line 17
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 18
    .line 19
    .line 20
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 21
    .line 22
    return-object p0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static getRound1Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const v1, 0x3e3851ec    # 0.18f

    .line 15
    .line 16
    .line 17
    mul-float v0, v0, v1

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    const/4 v1, 0x1

    .line 24
    const/4 v2, 0x0

    .line 25
    if-eqz p0, :cond_0

    .line 26
    .line 27
    array-length v3, p0

    .line 28
    if-lt v3, v1, :cond_0

    .line 29
    .line 30
    aget-object v3, p0, v2

    .line 31
    .line 32
    if-eqz v3, :cond_0

    .line 33
    .line 34
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    int-to-float v0, v0

    .line 47
    aget-object p0, p0, v2

    .line 48
    .line 49
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 50
    .line 51
    .line 52
    move-result p0

    .line 53
    mul-float v0, v0, p0

    .line 54
    .line 55
    :cond_0
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->rectF:Landroid/graphics/RectF;

    .line 56
    .line 57
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 58
    .line 59
    int-to-float v3, v3

    .line 60
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 61
    .line 62
    int-to-float v4, v4

    .line 63
    iget v5, p1, Landroid/graphics/Rect;->right:I

    .line 64
    .line 65
    int-to-float v5, v5

    .line 66
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 67
    .line 68
    int-to-float p1, p1

    .line 69
    invoke-virtual {p0, v3, v4, v5, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 70
    .line 71
    .line 72
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 73
    .line 74
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->rectF:Landroid/graphics/RectF;

    .line 75
    .line 76
    const/16 v3, 0x8

    .line 77
    .line 78
    new-array v3, v3, [F

    .line 79
    .line 80
    const/4 v4, 0x0

    .line 81
    aput v4, v3, v2

    .line 82
    .line 83
    aput v4, v3, v1

    .line 84
    .line 85
    const/4 v1, 0x2

    .line 86
    aput v0, v3, v1

    .line 87
    .line 88
    const/4 v1, 0x3

    .line 89
    aput v0, v3, v1

    .line 90
    .line 91
    const/4 v0, 0x4

    .line 92
    aput v4, v3, v0

    .line 93
    .line 94
    const/4 v0, 0x5

    .line 95
    aput v4, v3, v0

    .line 96
    .line 97
    const/4 v0, 0x6

    .line 98
    aput v4, v3, v0

    .line 99
    .line 100
    const/4 v0, 0x7

    .line 101
    aput v4, v3, v0

    .line 102
    .line 103
    sget-object v0, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 104
    .line 105
    invoke-virtual {p0, p1, v3, v0}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    .line 106
    .line 107
    .line 108
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 109
    .line 110
    return-object p0
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getRound2DiagRectPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 8

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const v1, 0x3e3851ec    # 0.18f

    .line 15
    .line 16
    .line 17
    mul-float v0, v0, v1

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    const/4 v1, 0x2

    .line 24
    const/4 v2, 0x1

    .line 25
    const/4 v3, 0x0

    .line 26
    const/4 v4, 0x0

    .line 27
    if-eqz p0, :cond_1

    .line 28
    .line 29
    array-length v5, p0

    .line 30
    if-lt v5, v1, :cond_1

    .line 31
    .line 32
    aget-object v5, p0, v3

    .line 33
    .line 34
    if-eqz v5, :cond_0

    .line 35
    .line 36
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 41
    .line 42
    .line 43
    move-result v5

    .line 44
    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    int-to-float v0, v0

    .line 49
    aget-object v5, p0, v3

    .line 50
    .line 51
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 52
    .line 53
    .line 54
    move-result v5

    .line 55
    mul-float v0, v0, v5

    .line 56
    .line 57
    :cond_0
    aget-object v5, p0, v2

    .line 58
    .line 59
    if-eqz v5, :cond_1

    .line 60
    .line 61
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 62
    .line 63
    .line 64
    move-result v4

    .line 65
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 66
    .line 67
    .line 68
    move-result v5

    .line 69
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    .line 70
    .line 71
    .line 72
    move-result v4

    .line 73
    int-to-float v4, v4

    .line 74
    aget-object p0, p0, v2

    .line 75
    .line 76
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 77
    .line 78
    .line 79
    move-result p0

    .line 80
    mul-float v4, v4, p0

    .line 81
    .line 82
    :cond_1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->rectF:Landroid/graphics/RectF;

    .line 83
    .line 84
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 85
    .line 86
    int-to-float v5, v5

    .line 87
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 88
    .line 89
    int-to-float v6, v6

    .line 90
    iget v7, p1, Landroid/graphics/Rect;->right:I

    .line 91
    .line 92
    int-to-float v7, v7

    .line 93
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 94
    .line 95
    int-to-float p1, p1

    .line 96
    invoke-virtual {p0, v5, v6, v7, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 97
    .line 98
    .line 99
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 100
    .line 101
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->rectF:Landroid/graphics/RectF;

    .line 102
    .line 103
    const/16 v5, 0x8

    .line 104
    .line 105
    new-array v5, v5, [F

    .line 106
    .line 107
    aput v0, v5, v3

    .line 108
    .line 109
    aput v0, v5, v2

    .line 110
    .line 111
    aput v4, v5, v1

    .line 112
    .line 113
    const/4 v1, 0x3

    .line 114
    aput v4, v5, v1

    .line 115
    .line 116
    const/4 v1, 0x4

    .line 117
    aput v0, v5, v1

    .line 118
    .line 119
    const/4 v1, 0x5

    .line 120
    aput v0, v5, v1

    .line 121
    .line 122
    const/4 v0, 0x6

    .line 123
    aput v4, v5, v0

    .line 124
    .line 125
    const/4 v0, 0x7

    .line 126
    aput v4, v5, v0

    .line 127
    .line 128
    sget-object v0, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 129
    .line 130
    invoke-virtual {p0, p1, v5, v0}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    .line 131
    .line 132
    .line 133
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 134
    .line 135
    return-object p0
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getRound2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 8

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const v1, 0x3e3851ec    # 0.18f

    .line 15
    .line 16
    .line 17
    mul-float v0, v0, v1

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    const/4 v1, 0x2

    .line 24
    const/4 v2, 0x1

    .line 25
    const/4 v3, 0x0

    .line 26
    const/4 v4, 0x0

    .line 27
    if-eqz p0, :cond_1

    .line 28
    .line 29
    array-length v5, p0

    .line 30
    if-lt v5, v1, :cond_1

    .line 31
    .line 32
    aget-object v5, p0, v3

    .line 33
    .line 34
    if-eqz v5, :cond_0

    .line 35
    .line 36
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 41
    .line 42
    .line 43
    move-result v5

    .line 44
    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    int-to-float v0, v0

    .line 49
    aget-object v5, p0, v3

    .line 50
    .line 51
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 52
    .line 53
    .line 54
    move-result v5

    .line 55
    mul-float v0, v0, v5

    .line 56
    .line 57
    :cond_0
    aget-object v5, p0, v2

    .line 58
    .line 59
    if-eqz v5, :cond_1

    .line 60
    .line 61
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 62
    .line 63
    .line 64
    move-result v4

    .line 65
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 66
    .line 67
    .line 68
    move-result v5

    .line 69
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    .line 70
    .line 71
    .line 72
    move-result v4

    .line 73
    int-to-float v4, v4

    .line 74
    aget-object p0, p0, v2

    .line 75
    .line 76
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 77
    .line 78
    .line 79
    move-result p0

    .line 80
    mul-float v4, v4, p0

    .line 81
    .line 82
    :cond_1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->rectF:Landroid/graphics/RectF;

    .line 83
    .line 84
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 85
    .line 86
    int-to-float v5, v5

    .line 87
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 88
    .line 89
    int-to-float v6, v6

    .line 90
    iget v7, p1, Landroid/graphics/Rect;->right:I

    .line 91
    .line 92
    int-to-float v7, v7

    .line 93
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 94
    .line 95
    int-to-float p1, p1

    .line 96
    invoke-virtual {p0, v5, v6, v7, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 97
    .line 98
    .line 99
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 100
    .line 101
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->rectF:Landroid/graphics/RectF;

    .line 102
    .line 103
    const/16 v5, 0x8

    .line 104
    .line 105
    new-array v5, v5, [F

    .line 106
    .line 107
    aput v0, v5, v3

    .line 108
    .line 109
    aput v0, v5, v2

    .line 110
    .line 111
    aput v0, v5, v1

    .line 112
    .line 113
    const/4 v1, 0x3

    .line 114
    aput v0, v5, v1

    .line 115
    .line 116
    const/4 v0, 0x4

    .line 117
    aput v4, v5, v0

    .line 118
    .line 119
    const/4 v0, 0x5

    .line 120
    aput v4, v5, v0

    .line 121
    .line 122
    const/4 v0, 0x6

    .line 123
    aput v4, v5, v0

    .line 124
    .line 125
    const/4 v0, 0x7

    .line 126
    aput v4, v5, v0

    .line 127
    .line 128
    sget-object v0, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 129
    .line 130
    invoke-virtual {p0, p1, v5, v0}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    .line 131
    .line 132
    .line 133
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 134
    .line 135
    return-object p0
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getRoundRectanglePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const v1, 0x3e3851ec    # 0.18f

    .line 15
    .line 16
    .line 17
    mul-float v0, v0, v1

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    const/4 v1, 0x1

    .line 24
    const/4 v2, 0x0

    .line 25
    if-eqz p0, :cond_0

    .line 26
    .line 27
    array-length v3, p0

    .line 28
    if-lt v3, v1, :cond_0

    .line 29
    .line 30
    aget-object v3, p0, v2

    .line 31
    .line 32
    if-eqz v3, :cond_0

    .line 33
    .line 34
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    int-to-float v0, v0

    .line 47
    aget-object p0, p0, v2

    .line 48
    .line 49
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 50
    .line 51
    .line 52
    move-result p0

    .line 53
    mul-float v0, v0, p0

    .line 54
    .line 55
    :cond_0
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->rectF:Landroid/graphics/RectF;

    .line 56
    .line 57
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 58
    .line 59
    int-to-float v3, v3

    .line 60
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 61
    .line 62
    int-to-float v4, v4

    .line 63
    iget v5, p1, Landroid/graphics/Rect;->right:I

    .line 64
    .line 65
    int-to-float v5, v5

    .line 66
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 67
    .line 68
    int-to-float p1, p1

    .line 69
    invoke-virtual {p0, v3, v4, v5, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 70
    .line 71
    .line 72
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 73
    .line 74
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->rectF:Landroid/graphics/RectF;

    .line 75
    .line 76
    const/16 v3, 0x8

    .line 77
    .line 78
    new-array v3, v3, [F

    .line 79
    .line 80
    aput v0, v3, v2

    .line 81
    .line 82
    aput v0, v3, v1

    .line 83
    .line 84
    const/4 v1, 0x2

    .line 85
    aput v0, v3, v1

    .line 86
    .line 87
    const/4 v1, 0x3

    .line 88
    aput v0, v3, v1

    .line 89
    .line 90
    const/4 v1, 0x4

    .line 91
    aput v0, v3, v1

    .line 92
    .line 93
    const/4 v1, 0x5

    .line 94
    aput v0, v3, v1

    .line 95
    .line 96
    const/4 v1, 0x6

    .line 97
    aput v0, v3, v1

    .line 98
    .line 99
    const/4 v1, 0x7

    .line 100
    aput v0, v3, v1

    .line 101
    .line 102
    sget-object v0, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 103
    .line 104
    invoke-virtual {p0, p1, v3, v0}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    .line 105
    .line 106
    .line 107
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 108
    .line 109
    return-object p0
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getSnip1RectPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const v1, 0x3e3851ec    # 0.18f

    .line 15
    .line 16
    .line 17
    mul-float v0, v0, v1

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    if-eqz p0, :cond_0

    .line 24
    .line 25
    array-length v1, p0

    .line 26
    const/4 v2, 0x1

    .line 27
    if-lt v1, v2, :cond_0

    .line 28
    .line 29
    const/4 v1, 0x0

    .line 30
    aget-object v2, p0, v1

    .line 31
    .line 32
    if-eqz v2, :cond_0

    .line 33
    .line 34
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    int-to-float v0, v0

    .line 47
    aget-object p0, p0, v1

    .line 48
    .line 49
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 50
    .line 51
    .line 52
    move-result p0

    .line 53
    mul-float v0, v0, p0

    .line 54
    .line 55
    :cond_0
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 56
    .line 57
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 58
    .line 59
    int-to-float v1, v1

    .line 60
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 61
    .line 62
    int-to-float v2, v2

    .line 63
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 64
    .line 65
    .line 66
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 67
    .line 68
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 69
    .line 70
    int-to-float v1, v1

    .line 71
    sub-float/2addr v1, v0

    .line 72
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 73
    .line 74
    int-to-float v2, v2

    .line 75
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 76
    .line 77
    .line 78
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 79
    .line 80
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 81
    .line 82
    int-to-float v1, v1

    .line 83
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 84
    .line 85
    int-to-float v2, v2

    .line 86
    add-float/2addr v2, v0

    .line 87
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 88
    .line 89
    .line 90
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 91
    .line 92
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 93
    .line 94
    int-to-float v0, v0

    .line 95
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 96
    .line 97
    int-to-float v1, v1

    .line 98
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 99
    .line 100
    .line 101
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 102
    .line 103
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 104
    .line 105
    int-to-float v0, v0

    .line 106
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 107
    .line 108
    int-to-float p1, p1

    .line 109
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 110
    .line 111
    .line 112
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 113
    .line 114
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 115
    .line 116
    .line 117
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 118
    .line 119
    return-object p0
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getSnip2DiagPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const v1, 0x3e3851ec    # 0.18f

    .line 15
    .line 16
    .line 17
    mul-float v0, v0, v1

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    const/4 v1, 0x0

    .line 24
    if-eqz p0, :cond_1

    .line 25
    .line 26
    array-length v2, p0

    .line 27
    const/4 v3, 0x2

    .line 28
    if-lt v2, v3, :cond_1

    .line 29
    .line 30
    const/4 v2, 0x0

    .line 31
    aget-object v3, p0, v2

    .line 32
    .line 33
    if-eqz v3, :cond_0

    .line 34
    .line 35
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    int-to-float v1, v1

    .line 48
    aget-object v2, p0, v2

    .line 49
    .line 50
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    mul-float v1, v1, v2

    .line 55
    .line 56
    :cond_0
    const/4 v2, 0x1

    .line 57
    aget-object v3, p0, v2

    .line 58
    .line 59
    if-eqz v3, :cond_1

    .line 60
    .line 61
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 66
    .line 67
    .line 68
    move-result v3

    .line 69
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    int-to-float v0, v0

    .line 74
    aget-object p0, p0, v2

    .line 75
    .line 76
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 77
    .line 78
    .line 79
    move-result p0

    .line 80
    mul-float v0, v0, p0

    .line 81
    .line 82
    :cond_1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 83
    .line 84
    invoke-virtual {p0}, Landroid/graphics/Path;->reset()V

    .line 85
    .line 86
    .line 87
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 88
    .line 89
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 90
    .line 91
    int-to-float v2, v2

    .line 92
    add-float/2addr v2, v1

    .line 93
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 94
    .line 95
    int-to-float v3, v3

    .line 96
    invoke-virtual {p0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 97
    .line 98
    .line 99
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 100
    .line 101
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 102
    .line 103
    int-to-float v2, v2

    .line 104
    sub-float/2addr v2, v0

    .line 105
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 106
    .line 107
    int-to-float v3, v3

    .line 108
    invoke-virtual {p0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 109
    .line 110
    .line 111
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 112
    .line 113
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 114
    .line 115
    int-to-float v2, v2

    .line 116
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 117
    .line 118
    int-to-float v3, v3

    .line 119
    add-float/2addr v3, v0

    .line 120
    invoke-virtual {p0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 121
    .line 122
    .line 123
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 124
    .line 125
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 126
    .line 127
    int-to-float v2, v2

    .line 128
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 129
    .line 130
    int-to-float v3, v3

    .line 131
    sub-float/2addr v3, v1

    .line 132
    invoke-virtual {p0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 133
    .line 134
    .line 135
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 136
    .line 137
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 138
    .line 139
    int-to-float v2, v2

    .line 140
    sub-float/2addr v2, v1

    .line 141
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 142
    .line 143
    int-to-float v3, v3

    .line 144
    invoke-virtual {p0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 145
    .line 146
    .line 147
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 148
    .line 149
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 150
    .line 151
    int-to-float v2, v2

    .line 152
    add-float/2addr v2, v0

    .line 153
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 154
    .line 155
    int-to-float v3, v3

    .line 156
    invoke-virtual {p0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 157
    .line 158
    .line 159
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 160
    .line 161
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 162
    .line 163
    int-to-float v2, v2

    .line 164
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 165
    .line 166
    int-to-float v3, v3

    .line 167
    sub-float/2addr v3, v0

    .line 168
    invoke-virtual {p0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    .line 170
    .line 171
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 172
    .line 173
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 174
    .line 175
    int-to-float v0, v0

    .line 176
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 177
    .line 178
    int-to-float p1, p1

    .line 179
    add-float/2addr p1, v1

    .line 180
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 181
    .line 182
    .line 183
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 184
    .line 185
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 186
    .line 187
    .line 188
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 189
    .line 190
    return-object p0
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getSnip2SameRectPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const v1, 0x3e3851ec    # 0.18f

    .line 15
    .line 16
    .line 17
    mul-float v0, v0, v1

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    const/4 v1, 0x0

    .line 24
    if-eqz p0, :cond_1

    .line 25
    .line 26
    array-length v2, p0

    .line 27
    const/4 v3, 0x2

    .line 28
    if-lt v2, v3, :cond_1

    .line 29
    .line 30
    const/4 v2, 0x0

    .line 31
    aget-object v3, p0, v2

    .line 32
    .line 33
    if-eqz v3, :cond_0

    .line 34
    .line 35
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    int-to-float v0, v0

    .line 48
    aget-object v2, p0, v2

    .line 49
    .line 50
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    mul-float v0, v0, v2

    .line 55
    .line 56
    :cond_0
    const/4 v2, 0x1

    .line 57
    aget-object v3, p0, v2

    .line 58
    .line 59
    if-eqz v3, :cond_1

    .line 60
    .line 61
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 66
    .line 67
    .line 68
    move-result v3

    .line 69
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    .line 70
    .line 71
    .line 72
    move-result v1

    .line 73
    int-to-float v1, v1

    .line 74
    aget-object p0, p0, v2

    .line 75
    .line 76
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 77
    .line 78
    .line 79
    move-result p0

    .line 80
    mul-float v1, v1, p0

    .line 81
    .line 82
    :cond_1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 83
    .line 84
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 85
    .line 86
    int-to-float v2, v2

    .line 87
    add-float/2addr v2, v0

    .line 88
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 89
    .line 90
    int-to-float v3, v3

    .line 91
    invoke-virtual {p0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 92
    .line 93
    .line 94
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 95
    .line 96
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 97
    .line 98
    int-to-float v2, v2

    .line 99
    sub-float/2addr v2, v0

    .line 100
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 101
    .line 102
    int-to-float v3, v3

    .line 103
    invoke-virtual {p0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 104
    .line 105
    .line 106
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 107
    .line 108
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 109
    .line 110
    int-to-float v2, v2

    .line 111
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 112
    .line 113
    int-to-float v3, v3

    .line 114
    add-float/2addr v3, v0

    .line 115
    invoke-virtual {p0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 116
    .line 117
    .line 118
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 119
    .line 120
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 121
    .line 122
    int-to-float v2, v2

    .line 123
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 124
    .line 125
    int-to-float v3, v3

    .line 126
    sub-float/2addr v3, v1

    .line 127
    invoke-virtual {p0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 128
    .line 129
    .line 130
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 131
    .line 132
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 133
    .line 134
    int-to-float v2, v2

    .line 135
    sub-float/2addr v2, v1

    .line 136
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 137
    .line 138
    int-to-float v3, v3

    .line 139
    invoke-virtual {p0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 140
    .line 141
    .line 142
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 143
    .line 144
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 145
    .line 146
    int-to-float v2, v2

    .line 147
    add-float/2addr v2, v1

    .line 148
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 149
    .line 150
    int-to-float v3, v3

    .line 151
    invoke-virtual {p0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 152
    .line 153
    .line 154
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 155
    .line 156
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 157
    .line 158
    int-to-float v2, v2

    .line 159
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 160
    .line 161
    int-to-float v3, v3

    .line 162
    sub-float/2addr v3, v1

    .line 163
    invoke-virtual {p0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 164
    .line 165
    .line 166
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 167
    .line 168
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 169
    .line 170
    int-to-float v1, v1

    .line 171
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 172
    .line 173
    int-to-float p1, p1

    .line 174
    add-float/2addr p1, v0

    .line 175
    invoke-virtual {p0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 176
    .line 177
    .line 178
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 179
    .line 180
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 181
    .line 182
    .line 183
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 184
    .line 185
    return-object p0
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getSnipRoundPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const v1, 0x3e3851ec    # 0.18f

    .line 15
    .line 16
    .line 17
    mul-float v0, v0, v1

    .line 18
    .line 19
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 24
    .line 25
    .line 26
    move-result v3

    .line 27
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    int-to-float v2, v2

    .line 32
    mul-float v2, v2, v1

    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 35
    .line 36
    .line 37
    move-result-object p0

    .line 38
    if-eqz p0, :cond_1

    .line 39
    .line 40
    array-length v1, p0

    .line 41
    const/4 v3, 0x2

    .line 42
    if-lt v1, v3, :cond_1

    .line 43
    .line 44
    const/4 v1, 0x0

    .line 45
    aget-object v3, p0, v1

    .line 46
    .line 47
    if-eqz v3, :cond_0

    .line 48
    .line 49
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    int-to-float v0, v0

    .line 62
    aget-object v1, p0, v1

    .line 63
    .line 64
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 65
    .line 66
    .line 67
    move-result v1

    .line 68
    mul-float v0, v0, v1

    .line 69
    .line 70
    :cond_0
    const/4 v1, 0x1

    .line 71
    aget-object v3, p0, v1

    .line 72
    .line 73
    if-eqz v3, :cond_1

    .line 74
    .line 75
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 76
    .line 77
    .line 78
    move-result v2

    .line 79
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 80
    .line 81
    .line 82
    move-result v3

    .line 83
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 84
    .line 85
    .line 86
    move-result v2

    .line 87
    int-to-float v2, v2

    .line 88
    aget-object p0, p0, v1

    .line 89
    .line 90
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 91
    .line 92
    .line 93
    move-result p0

    .line 94
    mul-float v2, v2, p0

    .line 95
    .line 96
    :cond_1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 97
    .line 98
    invoke-virtual {p0}, Landroid/graphics/Path;->reset()V

    .line 99
    .line 100
    .line 101
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 102
    .line 103
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 104
    .line 105
    int-to-float v1, v1

    .line 106
    add-float/2addr v1, v0

    .line 107
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 108
    .line 109
    int-to-float v3, v3

    .line 110
    invoke-virtual {p0, v1, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 111
    .line 112
    .line 113
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 114
    .line 115
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 116
    .line 117
    int-to-float v1, v1

    .line 118
    sub-float/2addr v1, v2

    .line 119
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 120
    .line 121
    int-to-float v3, v3

    .line 122
    invoke-virtual {p0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 123
    .line 124
    .line 125
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 126
    .line 127
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 128
    .line 129
    int-to-float v1, v1

    .line 130
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 131
    .line 132
    int-to-float v3, v3

    .line 133
    add-float/2addr v3, v2

    .line 134
    invoke-virtual {p0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 135
    .line 136
    .line 137
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 138
    .line 139
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 140
    .line 141
    int-to-float v1, v1

    .line 142
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 143
    .line 144
    int-to-float v2, v2

    .line 145
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 146
    .line 147
    .line 148
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 149
    .line 150
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 151
    .line 152
    int-to-float v1, v1

    .line 153
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 154
    .line 155
    int-to-float v2, v2

    .line 156
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 157
    .line 158
    .line 159
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 160
    .line 161
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 162
    .line 163
    int-to-float v1, v1

    .line 164
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 165
    .line 166
    int-to-float v2, v2

    .line 167
    add-float/2addr v2, v0

    .line 168
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    .line 170
    .line 171
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->rectF:Landroid/graphics/RectF;

    .line 172
    .line 173
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 174
    .line 175
    int-to-float v2, v1

    .line 176
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 177
    .line 178
    int-to-float v3, p1

    .line 179
    int-to-float v1, v1

    .line 180
    const/high16 v4, 0x40000000    # 2.0f

    .line 181
    .line 182
    mul-float v0, v0, v4

    .line 183
    .line 184
    add-float/2addr v1, v0

    .line 185
    int-to-float p1, p1

    .line 186
    add-float/2addr p1, v0

    .line 187
    invoke-virtual {p0, v2, v3, v1, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 188
    .line 189
    .line 190
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 191
    .line 192
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->rectF:Landroid/graphics/RectF;

    .line 193
    .line 194
    const/high16 v0, 0x43340000    # 180.0f

    .line 195
    .line 196
    const/high16 v1, 0x42b40000    # 90.0f

    .line 197
    .line 198
    invoke-virtual {p0, p1, v0, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 199
    .line 200
    .line 201
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 202
    .line 203
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 204
    .line 205
    .line 206
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 207
    .line 208
    return-object p0
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getdrawSnipRoundRectPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const v1, 0x3e3851ec    # 0.18f

    .line 15
    .line 16
    .line 17
    mul-float v0, v0, v1

    .line 18
    .line 19
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 24
    .line 25
    .line 26
    move-result v3

    .line 27
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    int-to-float v2, v2

    .line 32
    mul-float v2, v2, v1

    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 35
    .line 36
    .line 37
    move-result-object p0

    .line 38
    if-eqz p0, :cond_1

    .line 39
    .line 40
    array-length v1, p0

    .line 41
    const/4 v3, 0x2

    .line 42
    if-lt v1, v3, :cond_1

    .line 43
    .line 44
    const/4 v1, 0x0

    .line 45
    aget-object v3, p0, v1

    .line 46
    .line 47
    if-eqz v3, :cond_0

    .line 48
    .line 49
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    int-to-float v0, v0

    .line 62
    aget-object v1, p0, v1

    .line 63
    .line 64
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 65
    .line 66
    .line 67
    move-result v1

    .line 68
    mul-float v0, v0, v1

    .line 69
    .line 70
    :cond_0
    const/4 v1, 0x1

    .line 71
    aget-object v3, p0, v1

    .line 72
    .line 73
    if-eqz v3, :cond_1

    .line 74
    .line 75
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 76
    .line 77
    .line 78
    move-result v2

    .line 79
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 80
    .line 81
    .line 82
    move-result v3

    .line 83
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 84
    .line 85
    .line 86
    move-result v2

    .line 87
    int-to-float v2, v2

    .line 88
    aget-object p0, p0, v1

    .line 89
    .line 90
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 91
    .line 92
    .line 93
    move-result p0

    .line 94
    mul-float v2, v2, p0

    .line 95
    .line 96
    :cond_1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 97
    .line 98
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 99
    .line 100
    int-to-float v1, v1

    .line 101
    add-float/2addr v1, v0

    .line 102
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 103
    .line 104
    int-to-float v3, v3

    .line 105
    invoke-virtual {p0, v1, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 106
    .line 107
    .line 108
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 109
    .line 110
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 111
    .line 112
    int-to-float v1, v1

    .line 113
    sub-float/2addr v1, v2

    .line 114
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 115
    .line 116
    int-to-float v3, v3

    .line 117
    invoke-virtual {p0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 118
    .line 119
    .line 120
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 121
    .line 122
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 123
    .line 124
    int-to-float v1, v1

    .line 125
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 126
    .line 127
    int-to-float v3, v3

    .line 128
    add-float/2addr v3, v2

    .line 129
    invoke-virtual {p0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 130
    .line 131
    .line 132
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 133
    .line 134
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 135
    .line 136
    int-to-float v1, v1

    .line 137
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 138
    .line 139
    int-to-float v2, v2

    .line 140
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 141
    .line 142
    .line 143
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 144
    .line 145
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 146
    .line 147
    int-to-float v1, v1

    .line 148
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 149
    .line 150
    int-to-float v2, v2

    .line 151
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 152
    .line 153
    .line 154
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 155
    .line 156
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 157
    .line 158
    int-to-float v1, v1

    .line 159
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 160
    .line 161
    int-to-float v2, v2

    .line 162
    add-float/2addr v2, v0

    .line 163
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 164
    .line 165
    .line 166
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->rectF:Landroid/graphics/RectF;

    .line 167
    .line 168
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 169
    .line 170
    int-to-float v2, v1

    .line 171
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 172
    .line 173
    int-to-float v3, p1

    .line 174
    int-to-float v1, v1

    .line 175
    const/high16 v4, 0x40000000    # 2.0f

    .line 176
    .line 177
    mul-float v0, v0, v4

    .line 178
    .line 179
    add-float/2addr v1, v0

    .line 180
    int-to-float p1, p1

    .line 181
    add-float/2addr p1, v0

    .line 182
    invoke-virtual {p0, v2, v3, v1, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 183
    .line 184
    .line 185
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 186
    .line 187
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->rectF:Landroid/graphics/RectF;

    .line 188
    .line 189
    const/high16 v0, 0x43340000    # 180.0f

    .line 190
    .line 191
    const/high16 v1, 0x42b40000    # 90.0f

    .line 192
    .line 193
    invoke-virtual {p0, p1, v0, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 194
    .line 195
    .line 196
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 197
    .line 198
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 199
    .line 200
    .line 201
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/rect/RectPathBuilder;->path:Landroid/graphics/Path;

    .line 202
    .line 203
    return-object p0
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

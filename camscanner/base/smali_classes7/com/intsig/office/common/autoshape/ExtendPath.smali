.class public Lcom/intsig/office/common/autoshape/ExtendPath;
.super Ljava/lang/Object;
.source "ExtendPath.java"


# instance fields
.field private fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

.field private hasLine:Z

.field private isArrow:Z

.field private line:Lcom/intsig/office/common/borders/Line;

.field private path:Landroid/graphics/Path;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->path:Landroid/graphics/Path;

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/common/autoshape/ExtendPath;)V
    .locals 2

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    new-instance v0, Landroid/graphics/Path;

    invoke-virtual {p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->getPath()Landroid/graphics/Path;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    iput-object v0, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->path:Landroid/graphics/Path;

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->hasLine()Z

    move-result v0

    iput-boolean v0, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->hasLine:Z

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->getLine()Lcom/intsig/office/common/borders/Line;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->line:Lcom/intsig/office/common/borders/Line;

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->isArrowPath()Z

    move-result p1

    iput-boolean p1, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->isArrow:Z

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->path:Landroid/graphics/Path;

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->dispose()V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLine()Lcom/intsig/office/common/borders/Line;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->line:Lcom/intsig/office/common/borders/Line;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPath()Landroid/graphics/Path;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->path:Landroid/graphics/Path;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public hasLine()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->hasLine:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isArrowPath()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->isArrow:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setArrowFlag(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->isArrow:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLine(Lcom/intsig/office/common/borders/Line;)V
    .locals 0

    .line 4
    iput-object p1, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->line:Lcom/intsig/office/common/borders/Line;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 5
    iput-boolean p1, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->hasLine:Z

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 6
    iput-boolean p1, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->hasLine:Z

    :goto_0
    return-void
.end method

.method public setLine(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->hasLine:Z

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->line:Lcom/intsig/office/common/borders/Line;

    if-nez p1, :cond_0

    .line 3
    new-instance p1, Lcom/intsig/office/common/borders/Line;

    invoke-direct {p1}, Lcom/intsig/office/common/borders/Line;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->line:Lcom/intsig/office/common/borders/Line;

    :cond_0
    return-void
.end method

.method public setPath(Landroid/graphics/Path;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/autoshape/ExtendPath;->path:Landroid/graphics/Path;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

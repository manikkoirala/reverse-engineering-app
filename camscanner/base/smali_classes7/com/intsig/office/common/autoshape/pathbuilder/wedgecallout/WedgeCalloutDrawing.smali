.class public Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;
.super Ljava/lang/Object;
.source "WedgeCalloutDrawing.java"


# static fields
.field private static final kit:Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;

.field private static path:Landroid/graphics/Path;

.field private static paths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation
.end field

.field private static rectF:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 7
    .line 8
    new-instance v0, Landroid/graphics/Path;

    .line 9
    .line 10
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 14
    .line 15
    new-instance v0, Ljava/util/ArrayList;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 21
    .line 22
    new-instance v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;

    .line 23
    .line 24
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;-><init>()V

    .line 25
    .line 26
    .line 27
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->kit:Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;

    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static get03AccentBorderCallout2(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    int-to-float v1, v1

    .line 9
    const/high16 v2, 0x3e400000    # 0.1875f

    .line 10
    .line 11
    mul-float v1, v1, v2

    .line 12
    .line 13
    add-float/2addr v0, v1

    .line 14
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 15
    .line 16
    int-to-float v1, v1

    .line 17
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    int-to-float v3, v3

    .line 22
    const v4, -0x42555715    # -0.08333f

    .line 23
    .line 24
    .line 25
    mul-float v3, v3, v4

    .line 26
    .line 27
    add-float/2addr v1, v3

    .line 28
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 29
    .line 30
    int-to-float v3, v3

    .line 31
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 32
    .line 33
    .line 34
    move-result v5

    .line 35
    int-to-float v5, v5

    .line 36
    mul-float v5, v5, v2

    .line 37
    .line 38
    add-float/2addr v3, v5

    .line 39
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 40
    .line 41
    int-to-float v2, v2

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 43
    .line 44
    .line 45
    move-result v5

    .line 46
    int-to-float v5, v5

    .line 47
    mul-float v5, v5, v4

    .line 48
    .line 49
    add-float/2addr v2, v5

    .line 50
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    if-eqz v4, :cond_3

    .line 55
    .line 56
    array-length v5, v4

    .line 57
    const/4 v6, 0x1

    .line 58
    if-lt v5, v6, :cond_3

    .line 59
    .line 60
    const/4 v5, 0x0

    .line 61
    aget-object v7, v4, v5

    .line 62
    .line 63
    if-eqz v7, :cond_0

    .line 64
    .line 65
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 66
    .line 67
    int-to-float v1, v1

    .line 68
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 69
    .line 70
    .line 71
    move-result v7

    .line 72
    int-to-float v7, v7

    .line 73
    aget-object v5, v4, v5

    .line 74
    .line 75
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 76
    .line 77
    .line 78
    move-result v5

    .line 79
    mul-float v7, v7, v5

    .line 80
    .line 81
    add-float/2addr v1, v7

    .line 82
    :cond_0
    array-length v5, v4

    .line 83
    const/4 v7, 0x2

    .line 84
    if-lt v5, v7, :cond_1

    .line 85
    .line 86
    aget-object v5, v4, v6

    .line 87
    .line 88
    if-eqz v5, :cond_1

    .line 89
    .line 90
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 91
    .line 92
    int-to-float v0, v0

    .line 93
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 94
    .line 95
    .line 96
    move-result v5

    .line 97
    int-to-float v5, v5

    .line 98
    aget-object v6, v4, v6

    .line 99
    .line 100
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 101
    .line 102
    .line 103
    move-result v6

    .line 104
    mul-float v5, v5, v6

    .line 105
    .line 106
    add-float/2addr v0, v5

    .line 107
    :cond_1
    array-length v5, v4

    .line 108
    const/4 v6, 0x3

    .line 109
    if-lt v5, v6, :cond_2

    .line 110
    .line 111
    aget-object v5, v4, v7

    .line 112
    .line 113
    if-eqz v5, :cond_2

    .line 114
    .line 115
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 116
    .line 117
    int-to-float v2, v2

    .line 118
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 119
    .line 120
    .line 121
    move-result v5

    .line 122
    int-to-float v5, v5

    .line 123
    aget-object v7, v4, v7

    .line 124
    .line 125
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 126
    .line 127
    .line 128
    move-result v7

    .line 129
    mul-float v5, v5, v7

    .line 130
    .line 131
    add-float/2addr v2, v5

    .line 132
    :cond_2
    array-length v5, v4

    .line 133
    const/4 v7, 0x4

    .line 134
    if-lt v5, v7, :cond_3

    .line 135
    .line 136
    aget-object v5, v4, v6

    .line 137
    .line 138
    if-eqz v5, :cond_3

    .line 139
    .line 140
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 141
    .line 142
    int-to-float v3, v3

    .line 143
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 144
    .line 145
    .line 146
    move-result v5

    .line 147
    int-to-float v5, v5

    .line 148
    aget-object v4, v4, v6

    .line 149
    .line 150
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 151
    .line 152
    .line 153
    move-result v4

    .line 154
    mul-float v5, v5, v4

    .line 155
    .line 156
    add-float/2addr v3, v5

    .line 157
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 158
    .line 159
    .line 160
    move-result-object v4

    .line 161
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 162
    .line 163
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 164
    .line 165
    .line 166
    new-instance v12, Landroid/graphics/Path;

    .line 167
    .line 168
    invoke-direct {v12}, Landroid/graphics/Path;-><init>()V

    .line 169
    .line 170
    .line 171
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 172
    .line 173
    int-to-float v7, v6

    .line 174
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 175
    .line 176
    int-to-float v8, v6

    .line 177
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 178
    .line 179
    int-to-float v9, v6

    .line 180
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 181
    .line 182
    int-to-float v10, v6

    .line 183
    sget-object v11, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 184
    .line 185
    move-object v6, v12

    .line 186
    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 187
    .line 188
    .line 189
    invoke-virtual {v5, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 190
    .line 191
    .line 192
    invoke-virtual {v5, v12}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 193
    .line 194
    .line 195
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 196
    .line 197
    .line 198
    move-result-object v4

    .line 199
    invoke-virtual {v5, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 200
    .line 201
    .line 202
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 203
    .line 204
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    .line 206
    .line 207
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 208
    .line 209
    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 210
    .line 211
    .line 212
    new-instance v5, Landroid/graphics/Path;

    .line 213
    .line 214
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 215
    .line 216
    .line 217
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 218
    .line 219
    int-to-float v6, v6

    .line 220
    invoke-virtual {v5, v2, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 221
    .line 222
    .line 223
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 224
    .line 225
    int-to-float p1, p1

    .line 226
    invoke-virtual {v5, v2, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 227
    .line 228
    .line 229
    invoke-virtual {v5, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 230
    .line 231
    .line 232
    invoke-virtual {v5, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 233
    .line 234
    .line 235
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 236
    .line 237
    .line 238
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 239
    .line 240
    .line 241
    move-result-object p0

    .line 242
    invoke-virtual {v4, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 243
    .line 244
    .line 245
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 246
    .line 247
    invoke-interface {p0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    .line 249
    .line 250
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 251
    .line 252
    return-object p0
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static get03AccentBorderCallout3(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v6

    .line 37
    int-to-float v6, v6

    .line 38
    mul-float v6, v6, v3

    .line 39
    .line 40
    add-float/2addr v4, v6

    .line 41
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v6, v6

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v7

    .line 48
    int-to-float v7, v7

    .line 49
    mul-float v7, v7, v5

    .line 50
    .line 51
    add-float/2addr v6, v7

    .line 52
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 53
    .line 54
    int-to-float v7, v7

    .line 55
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 56
    .line 57
    .line 58
    move-result v8

    .line 59
    int-to-float v8, v8

    .line 60
    mul-float v8, v8, v3

    .line 61
    .line 62
    add-float/2addr v7, v8

    .line 63
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 64
    .line 65
    int-to-float v3, v3

    .line 66
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 67
    .line 68
    .line 69
    move-result v8

    .line 70
    int-to-float v8, v8

    .line 71
    mul-float v8, v8, v5

    .line 72
    .line 73
    add-float/2addr v3, v8

    .line 74
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 75
    .line 76
    .line 77
    move-result-object v5

    .line 78
    if-eqz v5, :cond_5

    .line 79
    .line 80
    array-length v8, v5

    .line 81
    const/4 v9, 0x1

    .line 82
    if-lt v8, v9, :cond_5

    .line 83
    .line 84
    const/4 v8, 0x0

    .line 85
    aget-object v10, v5, v8

    .line 86
    .line 87
    if-eqz v10, :cond_0

    .line 88
    .line 89
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 90
    .line 91
    int-to-float v2, v2

    .line 92
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 93
    .line 94
    .line 95
    move-result v10

    .line 96
    int-to-float v10, v10

    .line 97
    aget-object v8, v5, v8

    .line 98
    .line 99
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 100
    .line 101
    .line 102
    move-result v8

    .line 103
    mul-float v10, v10, v8

    .line 104
    .line 105
    add-float/2addr v2, v10

    .line 106
    :cond_0
    array-length v8, v5

    .line 107
    const/4 v10, 0x2

    .line 108
    if-lt v8, v10, :cond_1

    .line 109
    .line 110
    aget-object v8, v5, v9

    .line 111
    .line 112
    if-eqz v8, :cond_1

    .line 113
    .line 114
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 115
    .line 116
    int-to-float v1, v1

    .line 117
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 118
    .line 119
    .line 120
    move-result v8

    .line 121
    int-to-float v8, v8

    .line 122
    aget-object v9, v5, v9

    .line 123
    .line 124
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 125
    .line 126
    .line 127
    move-result v9

    .line 128
    mul-float v8, v8, v9

    .line 129
    .line 130
    add-float/2addr v1, v8

    .line 131
    :cond_1
    array-length v8, v5

    .line 132
    const/4 v9, 0x3

    .line 133
    if-lt v8, v9, :cond_2

    .line 134
    .line 135
    aget-object v8, v5, v10

    .line 136
    .line 137
    if-eqz v8, :cond_2

    .line 138
    .line 139
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 140
    .line 141
    int-to-float v6, v6

    .line 142
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 143
    .line 144
    .line 145
    move-result v8

    .line 146
    int-to-float v8, v8

    .line 147
    aget-object v10, v5, v10

    .line 148
    .line 149
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 150
    .line 151
    .line 152
    move-result v10

    .line 153
    mul-float v8, v8, v10

    .line 154
    .line 155
    add-float/2addr v6, v8

    .line 156
    :cond_2
    array-length v8, v5

    .line 157
    const/4 v10, 0x4

    .line 158
    if-lt v8, v10, :cond_3

    .line 159
    .line 160
    aget-object v8, v5, v9

    .line 161
    .line 162
    if-eqz v8, :cond_3

    .line 163
    .line 164
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 165
    .line 166
    int-to-float v4, v4

    .line 167
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 168
    .line 169
    .line 170
    move-result v8

    .line 171
    int-to-float v8, v8

    .line 172
    aget-object v9, v5, v9

    .line 173
    .line 174
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 175
    .line 176
    .line 177
    move-result v9

    .line 178
    mul-float v8, v8, v9

    .line 179
    .line 180
    add-float/2addr v4, v8

    .line 181
    :cond_3
    array-length v8, v5

    .line 182
    const/4 v9, 0x5

    .line 183
    if-lt v8, v9, :cond_4

    .line 184
    .line 185
    aget-object v8, v5, v10

    .line 186
    .line 187
    if-eqz v8, :cond_4

    .line 188
    .line 189
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 190
    .line 191
    int-to-float v3, v3

    .line 192
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 193
    .line 194
    .line 195
    move-result v8

    .line 196
    int-to-float v8, v8

    .line 197
    aget-object v10, v5, v10

    .line 198
    .line 199
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 200
    .line 201
    .line 202
    move-result v10

    .line 203
    mul-float v8, v8, v10

    .line 204
    .line 205
    add-float/2addr v3, v8

    .line 206
    :cond_4
    array-length v8, v5

    .line 207
    const/4 v10, 0x6

    .line 208
    if-lt v8, v10, :cond_5

    .line 209
    .line 210
    aget-object v8, v5, v9

    .line 211
    .line 212
    if-eqz v8, :cond_5

    .line 213
    .line 214
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 215
    .line 216
    int-to-float v7, v7

    .line 217
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 218
    .line 219
    .line 220
    move-result v8

    .line 221
    int-to-float v8, v8

    .line 222
    aget-object v5, v5, v9

    .line 223
    .line 224
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 225
    .line 226
    .line 227
    move-result v5

    .line 228
    mul-float v8, v8, v5

    .line 229
    .line 230
    add-float/2addr v7, v8

    .line 231
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 232
    .line 233
    .line 234
    move-result-object v5

    .line 235
    new-instance v8, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 236
    .line 237
    invoke-direct {v8}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 238
    .line 239
    .line 240
    new-instance v15, Landroid/graphics/Path;

    .line 241
    .line 242
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 243
    .line 244
    .line 245
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 246
    .line 247
    int-to-float v10, v9

    .line 248
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 249
    .line 250
    int-to-float v11, v9

    .line 251
    iget v9, v0, Landroid/graphics/Rect;->right:I

    .line 252
    .line 253
    int-to-float v12, v9

    .line 254
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 255
    .line 256
    int-to-float v13, v9

    .line 257
    sget-object v14, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 258
    .line 259
    move-object v9, v15

    .line 260
    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 261
    .line 262
    .line 263
    invoke-virtual {v8, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 264
    .line 265
    .line 266
    invoke-virtual {v8, v15}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 267
    .line 268
    .line 269
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 270
    .line 271
    .line 272
    move-result-object v5

    .line 273
    invoke-virtual {v8, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 274
    .line 275
    .line 276
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 277
    .line 278
    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 279
    .line 280
    .line 281
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 282
    .line 283
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 284
    .line 285
    .line 286
    new-instance v8, Landroid/graphics/Path;

    .line 287
    .line 288
    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    .line 289
    .line 290
    .line 291
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 292
    .line 293
    int-to-float v9, v9

    .line 294
    invoke-virtual {v8, v3, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 295
    .line 296
    .line 297
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 298
    .line 299
    int-to-float v0, v0

    .line 300
    invoke-virtual {v8, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 301
    .line 302
    .line 303
    invoke-virtual {v8, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 304
    .line 305
    .line 306
    invoke-virtual {v8, v6, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 307
    .line 308
    .line 309
    invoke-virtual {v8, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 310
    .line 311
    .line 312
    invoke-virtual {v5, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 313
    .line 314
    .line 315
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 316
    .line 317
    .line 318
    move-result-object v0

    .line 319
    invoke-virtual {v5, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 320
    .line 321
    .line 322
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 323
    .line 324
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 325
    .line 326
    .line 327
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 328
    .line 329
    return-object v0
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static get03AccentBorderCallout4(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v5

    .line 37
    int-to-float v5, v5

    .line 38
    mul-float v5, v5, v3

    .line 39
    .line 40
    add-float/2addr v4, v5

    .line 41
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v5, v5

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v6

    .line 48
    int-to-float v6, v6

    .line 49
    const v7, -0x41d55476    # -0.16667f

    .line 50
    .line 51
    .line 52
    mul-float v6, v6, v7

    .line 53
    .line 54
    add-float/2addr v5, v6

    .line 55
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 56
    .line 57
    int-to-float v6, v6

    .line 58
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 59
    .line 60
    .line 61
    move-result v7

    .line 62
    int-to-float v7, v7

    .line 63
    mul-float v7, v7, v3

    .line 64
    .line 65
    add-float/2addr v6, v7

    .line 66
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 67
    .line 68
    int-to-float v7, v7

    .line 69
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 70
    .line 71
    .line 72
    move-result v8

    .line 73
    int-to-float v8, v8

    .line 74
    const v9, 0x3f8aaa8f

    .line 75
    .line 76
    .line 77
    mul-float v8, v8, v9

    .line 78
    .line 79
    add-float/2addr v7, v8

    .line 80
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 81
    .line 82
    int-to-float v8, v8

    .line 83
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 84
    .line 85
    .line 86
    move-result v10

    .line 87
    int-to-float v10, v10

    .line 88
    mul-float v10, v10, v3

    .line 89
    .line 90
    add-float/2addr v8, v10

    .line 91
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 92
    .line 93
    int-to-float v3, v3

    .line 94
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 95
    .line 96
    .line 97
    move-result v10

    .line 98
    int-to-float v10, v10

    .line 99
    mul-float v10, v10, v9

    .line 100
    .line 101
    add-float/2addr v3, v10

    .line 102
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 103
    .line 104
    .line 105
    move-result-object v9

    .line 106
    if-eqz v9, :cond_7

    .line 107
    .line 108
    array-length v10, v9

    .line 109
    const/4 v11, 0x1

    .line 110
    if-lt v10, v11, :cond_7

    .line 111
    .line 112
    const/4 v10, 0x0

    .line 113
    aget-object v12, v9, v10

    .line 114
    .line 115
    if-eqz v12, :cond_0

    .line 116
    .line 117
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 118
    .line 119
    int-to-float v2, v2

    .line 120
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 121
    .line 122
    .line 123
    move-result v12

    .line 124
    int-to-float v12, v12

    .line 125
    aget-object v10, v9, v10

    .line 126
    .line 127
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 128
    .line 129
    .line 130
    move-result v10

    .line 131
    mul-float v12, v12, v10

    .line 132
    .line 133
    add-float/2addr v2, v12

    .line 134
    :cond_0
    array-length v10, v9

    .line 135
    const/4 v12, 0x2

    .line 136
    if-lt v10, v12, :cond_1

    .line 137
    .line 138
    aget-object v10, v9, v11

    .line 139
    .line 140
    if-eqz v10, :cond_1

    .line 141
    .line 142
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 143
    .line 144
    int-to-float v1, v1

    .line 145
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 146
    .line 147
    .line 148
    move-result v10

    .line 149
    int-to-float v10, v10

    .line 150
    aget-object v11, v9, v11

    .line 151
    .line 152
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    .line 153
    .line 154
    .line 155
    move-result v11

    .line 156
    mul-float v10, v10, v11

    .line 157
    .line 158
    add-float/2addr v1, v10

    .line 159
    :cond_1
    array-length v10, v9

    .line 160
    const/4 v11, 0x3

    .line 161
    if-lt v10, v11, :cond_2

    .line 162
    .line 163
    aget-object v10, v9, v12

    .line 164
    .line 165
    if-eqz v10, :cond_2

    .line 166
    .line 167
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 168
    .line 169
    int-to-float v5, v5

    .line 170
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 171
    .line 172
    .line 173
    move-result v10

    .line 174
    int-to-float v10, v10

    .line 175
    aget-object v12, v9, v12

    .line 176
    .line 177
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    .line 178
    .line 179
    .line 180
    move-result v12

    .line 181
    mul-float v10, v10, v12

    .line 182
    .line 183
    add-float/2addr v5, v10

    .line 184
    :cond_2
    array-length v10, v9

    .line 185
    const/4 v12, 0x4

    .line 186
    if-lt v10, v12, :cond_3

    .line 187
    .line 188
    aget-object v10, v9, v11

    .line 189
    .line 190
    if-eqz v10, :cond_3

    .line 191
    .line 192
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 193
    .line 194
    int-to-float v4, v4

    .line 195
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 196
    .line 197
    .line 198
    move-result v10

    .line 199
    int-to-float v10, v10

    .line 200
    aget-object v11, v9, v11

    .line 201
    .line 202
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    .line 203
    .line 204
    .line 205
    move-result v11

    .line 206
    mul-float v10, v10, v11

    .line 207
    .line 208
    add-float/2addr v4, v10

    .line 209
    :cond_3
    array-length v10, v9

    .line 210
    const/4 v11, 0x5

    .line 211
    if-lt v10, v11, :cond_4

    .line 212
    .line 213
    aget-object v10, v9, v12

    .line 214
    .line 215
    if-eqz v10, :cond_4

    .line 216
    .line 217
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 218
    .line 219
    int-to-float v7, v7

    .line 220
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 221
    .line 222
    .line 223
    move-result v10

    .line 224
    int-to-float v10, v10

    .line 225
    aget-object v12, v9, v12

    .line 226
    .line 227
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    .line 228
    .line 229
    .line 230
    move-result v12

    .line 231
    mul-float v10, v10, v12

    .line 232
    .line 233
    add-float/2addr v7, v10

    .line 234
    :cond_4
    array-length v10, v9

    .line 235
    const/4 v12, 0x6

    .line 236
    if-lt v10, v12, :cond_5

    .line 237
    .line 238
    aget-object v10, v9, v11

    .line 239
    .line 240
    if-eqz v10, :cond_5

    .line 241
    .line 242
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 243
    .line 244
    int-to-float v6, v6

    .line 245
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 246
    .line 247
    .line 248
    move-result v10

    .line 249
    int-to-float v10, v10

    .line 250
    aget-object v11, v9, v11

    .line 251
    .line 252
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    .line 253
    .line 254
    .line 255
    move-result v11

    .line 256
    mul-float v10, v10, v11

    .line 257
    .line 258
    add-float/2addr v6, v10

    .line 259
    :cond_5
    array-length v10, v9

    .line 260
    const/4 v11, 0x7

    .line 261
    if-lt v10, v11, :cond_6

    .line 262
    .line 263
    aget-object v10, v9, v12

    .line 264
    .line 265
    if-eqz v10, :cond_6

    .line 266
    .line 267
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 268
    .line 269
    int-to-float v3, v3

    .line 270
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 271
    .line 272
    .line 273
    move-result v10

    .line 274
    int-to-float v10, v10

    .line 275
    aget-object v12, v9, v12

    .line 276
    .line 277
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    .line 278
    .line 279
    .line 280
    move-result v12

    .line 281
    mul-float v10, v10, v12

    .line 282
    .line 283
    add-float/2addr v3, v10

    .line 284
    :cond_6
    array-length v10, v9

    .line 285
    const/16 v12, 0x8

    .line 286
    .line 287
    if-lt v10, v12, :cond_7

    .line 288
    .line 289
    aget-object v10, v9, v11

    .line 290
    .line 291
    if-eqz v10, :cond_7

    .line 292
    .line 293
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 294
    .line 295
    int-to-float v8, v8

    .line 296
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 297
    .line 298
    .line 299
    move-result v10

    .line 300
    int-to-float v10, v10

    .line 301
    aget-object v9, v9, v11

    .line 302
    .line 303
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 304
    .line 305
    .line 306
    move-result v9

    .line 307
    mul-float v10, v10, v9

    .line 308
    .line 309
    add-float/2addr v8, v10

    .line 310
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 311
    .line 312
    .line 313
    move-result-object v9

    .line 314
    new-instance v10, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 315
    .line 316
    invoke-direct {v10}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 317
    .line 318
    .line 319
    new-instance v15, Landroid/graphics/Path;

    .line 320
    .line 321
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 322
    .line 323
    .line 324
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 325
    .line 326
    int-to-float v12, v11

    .line 327
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 328
    .line 329
    int-to-float v13, v11

    .line 330
    iget v11, v0, Landroid/graphics/Rect;->right:I

    .line 331
    .line 332
    int-to-float v14, v11

    .line 333
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 334
    .line 335
    int-to-float v11, v11

    .line 336
    sget-object v16, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 337
    .line 338
    move/from16 v17, v11

    .line 339
    .line 340
    move-object v11, v15

    .line 341
    move/from16 v18, v8

    .line 342
    .line 343
    move-object v8, v15

    .line 344
    move/from16 v15, v17

    .line 345
    .line 346
    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 347
    .line 348
    .line 349
    invoke-virtual {v10, v9}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 350
    .line 351
    .line 352
    invoke-virtual {v10, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 353
    .line 354
    .line 355
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 356
    .line 357
    .line 358
    move-result-object v8

    .line 359
    invoke-virtual {v10, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 360
    .line 361
    .line 362
    sget-object v8, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 363
    .line 364
    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 365
    .line 366
    .line 367
    new-instance v8, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 368
    .line 369
    invoke-direct {v8}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 370
    .line 371
    .line 372
    new-instance v9, Landroid/graphics/Path;

    .line 373
    .line 374
    invoke-direct {v9}, Landroid/graphics/Path;-><init>()V

    .line 375
    .line 376
    .line 377
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 378
    .line 379
    int-to-float v10, v10

    .line 380
    invoke-virtual {v9, v3, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 381
    .line 382
    .line 383
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 384
    .line 385
    int-to-float v0, v0

    .line 386
    invoke-virtual {v9, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 387
    .line 388
    .line 389
    invoke-virtual {v9, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 390
    .line 391
    .line 392
    invoke-virtual {v9, v5, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 393
    .line 394
    .line 395
    invoke-virtual {v9, v7, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 396
    .line 397
    .line 398
    move/from16 v0, v18

    .line 399
    .line 400
    invoke-virtual {v9, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 401
    .line 402
    .line 403
    invoke-virtual {v8, v9}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 404
    .line 405
    .line 406
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 407
    .line 408
    .line 409
    move-result-object v0

    .line 410
    invoke-virtual {v8, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 411
    .line 412
    .line 413
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 414
    .line 415
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416
    .line 417
    .line 418
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 419
    .line 420
    return-object v0
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static get03AccentCallout1Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    int-to-float v1, v1

    .line 9
    const/high16 v2, 0x3e400000    # 0.1875f

    .line 10
    .line 11
    mul-float v1, v1, v2

    .line 12
    .line 13
    add-float/2addr v0, v1

    .line 14
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 15
    .line 16
    int-to-float v1, v1

    .line 17
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    int-to-float v3, v3

    .line 22
    const v4, -0x42555715    # -0.08333f

    .line 23
    .line 24
    .line 25
    mul-float v3, v3, v4

    .line 26
    .line 27
    add-float/2addr v1, v3

    .line 28
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 29
    .line 30
    int-to-float v3, v3

    .line 31
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 32
    .line 33
    .line 34
    move-result v4

    .line 35
    int-to-float v4, v4

    .line 36
    mul-float v4, v4, v2

    .line 37
    .line 38
    add-float/2addr v3, v4

    .line 39
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 40
    .line 41
    int-to-float v2, v2

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 43
    .line 44
    .line 45
    move-result v4

    .line 46
    int-to-float v4, v4

    .line 47
    const v5, -0x413bbc2c

    .line 48
    .line 49
    .line 50
    mul-float v4, v4, v5

    .line 51
    .line 52
    add-float/2addr v2, v4

    .line 53
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 54
    .line 55
    .line 56
    move-result-object v4

    .line 57
    if-eqz v4, :cond_3

    .line 58
    .line 59
    array-length v5, v4

    .line 60
    const/4 v6, 0x4

    .line 61
    if-lt v5, v6, :cond_3

    .line 62
    .line 63
    const/4 v5, 0x0

    .line 64
    aget-object v6, v4, v5

    .line 65
    .line 66
    if-eqz v6, :cond_0

    .line 67
    .line 68
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 69
    .line 70
    int-to-float v1, v1

    .line 71
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 72
    .line 73
    .line 74
    move-result v6

    .line 75
    int-to-float v6, v6

    .line 76
    aget-object v5, v4, v5

    .line 77
    .line 78
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 79
    .line 80
    .line 81
    move-result v5

    .line 82
    mul-float v6, v6, v5

    .line 83
    .line 84
    add-float/2addr v1, v6

    .line 85
    :cond_0
    const/4 v5, 0x1

    .line 86
    aget-object v6, v4, v5

    .line 87
    .line 88
    if-eqz v6, :cond_1

    .line 89
    .line 90
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 91
    .line 92
    int-to-float v0, v0

    .line 93
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 94
    .line 95
    .line 96
    move-result v6

    .line 97
    int-to-float v6, v6

    .line 98
    aget-object v5, v4, v5

    .line 99
    .line 100
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 101
    .line 102
    .line 103
    move-result v5

    .line 104
    mul-float v6, v6, v5

    .line 105
    .line 106
    add-float/2addr v0, v6

    .line 107
    :cond_1
    const/4 v5, 0x2

    .line 108
    aget-object v6, v4, v5

    .line 109
    .line 110
    if-eqz v6, :cond_2

    .line 111
    .line 112
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 113
    .line 114
    int-to-float v2, v2

    .line 115
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 116
    .line 117
    .line 118
    move-result v6

    .line 119
    int-to-float v6, v6

    .line 120
    aget-object v5, v4, v5

    .line 121
    .line 122
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 123
    .line 124
    .line 125
    move-result v5

    .line 126
    mul-float v6, v6, v5

    .line 127
    .line 128
    add-float/2addr v2, v6

    .line 129
    :cond_2
    const/4 v5, 0x3

    .line 130
    aget-object v6, v4, v5

    .line 131
    .line 132
    if-eqz v6, :cond_3

    .line 133
    .line 134
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 135
    .line 136
    int-to-float v3, v3

    .line 137
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 138
    .line 139
    .line 140
    move-result v6

    .line 141
    int-to-float v6, v6

    .line 142
    aget-object v4, v4, v5

    .line 143
    .line 144
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 145
    .line 146
    .line 147
    move-result v4

    .line 148
    mul-float v6, v6, v4

    .line 149
    .line 150
    add-float/2addr v3, v6

    .line 151
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 152
    .line 153
    .line 154
    move-result-object v4

    .line 155
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 156
    .line 157
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 158
    .line 159
    .line 160
    new-instance v12, Landroid/graphics/Path;

    .line 161
    .line 162
    invoke-direct {v12}, Landroid/graphics/Path;-><init>()V

    .line 163
    .line 164
    .line 165
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 166
    .line 167
    int-to-float v7, v6

    .line 168
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 169
    .line 170
    int-to-float v8, v6

    .line 171
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 172
    .line 173
    int-to-float v9, v6

    .line 174
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 175
    .line 176
    int-to-float v10, p1

    .line 177
    sget-object v11, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 178
    .line 179
    move-object v6, v12

    .line 180
    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 181
    .line 182
    .line 183
    invoke-virtual {v5, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 184
    .line 185
    .line 186
    invoke-virtual {v5, v12}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 187
    .line 188
    .line 189
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 190
    .line 191
    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    .line 193
    .line 194
    new-instance p1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 195
    .line 196
    invoke-direct {p1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 197
    .line 198
    .line 199
    new-instance v4, Landroid/graphics/Path;

    .line 200
    .line 201
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 202
    .line 203
    .line 204
    invoke-virtual {v4, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 205
    .line 206
    .line 207
    invoke-virtual {v4, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 208
    .line 209
    .line 210
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 211
    .line 212
    .line 213
    move-result-object p0

    .line 214
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 215
    .line 216
    .line 217
    invoke-virtual {p1, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 218
    .line 219
    .line 220
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 221
    .line 222
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    .line 224
    .line 225
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 226
    .line 227
    return-object p0
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static get03AccentCallout2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    int-to-float v1, v1

    .line 9
    const/high16 v2, 0x3e400000    # 0.1875f

    .line 10
    .line 11
    mul-float v1, v1, v2

    .line 12
    .line 13
    add-float/2addr v0, v1

    .line 14
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 15
    .line 16
    int-to-float v1, v1

    .line 17
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    int-to-float v3, v3

    .line 22
    const v4, -0x42555715    # -0.08333f

    .line 23
    .line 24
    .line 25
    mul-float v3, v3, v4

    .line 26
    .line 27
    add-float/2addr v1, v3

    .line 28
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 29
    .line 30
    int-to-float v3, v3

    .line 31
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 32
    .line 33
    .line 34
    move-result v5

    .line 35
    int-to-float v5, v5

    .line 36
    mul-float v5, v5, v2

    .line 37
    .line 38
    add-float/2addr v3, v5

    .line 39
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 40
    .line 41
    int-to-float v2, v2

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 43
    .line 44
    .line 45
    move-result v5

    .line 46
    int-to-float v5, v5

    .line 47
    mul-float v5, v5, v4

    .line 48
    .line 49
    add-float/2addr v2, v5

    .line 50
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    if-eqz v4, :cond_3

    .line 55
    .line 56
    array-length v5, v4

    .line 57
    const/4 v6, 0x1

    .line 58
    if-lt v5, v6, :cond_3

    .line 59
    .line 60
    const/4 v5, 0x0

    .line 61
    aget-object v7, v4, v5

    .line 62
    .line 63
    if-eqz v7, :cond_0

    .line 64
    .line 65
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 66
    .line 67
    int-to-float v1, v1

    .line 68
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 69
    .line 70
    .line 71
    move-result v7

    .line 72
    int-to-float v7, v7

    .line 73
    aget-object v5, v4, v5

    .line 74
    .line 75
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 76
    .line 77
    .line 78
    move-result v5

    .line 79
    mul-float v7, v7, v5

    .line 80
    .line 81
    add-float/2addr v1, v7

    .line 82
    :cond_0
    array-length v5, v4

    .line 83
    const/4 v7, 0x2

    .line 84
    if-lt v5, v7, :cond_1

    .line 85
    .line 86
    aget-object v5, v4, v6

    .line 87
    .line 88
    if-eqz v5, :cond_1

    .line 89
    .line 90
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 91
    .line 92
    int-to-float v0, v0

    .line 93
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 94
    .line 95
    .line 96
    move-result v5

    .line 97
    int-to-float v5, v5

    .line 98
    aget-object v6, v4, v6

    .line 99
    .line 100
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 101
    .line 102
    .line 103
    move-result v6

    .line 104
    mul-float v5, v5, v6

    .line 105
    .line 106
    add-float/2addr v0, v5

    .line 107
    :cond_1
    array-length v5, v4

    .line 108
    const/4 v6, 0x3

    .line 109
    if-lt v5, v6, :cond_2

    .line 110
    .line 111
    aget-object v5, v4, v7

    .line 112
    .line 113
    if-eqz v5, :cond_2

    .line 114
    .line 115
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 116
    .line 117
    int-to-float v2, v2

    .line 118
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 119
    .line 120
    .line 121
    move-result v5

    .line 122
    int-to-float v5, v5

    .line 123
    aget-object v7, v4, v7

    .line 124
    .line 125
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 126
    .line 127
    .line 128
    move-result v7

    .line 129
    mul-float v5, v5, v7

    .line 130
    .line 131
    add-float/2addr v2, v5

    .line 132
    :cond_2
    array-length v5, v4

    .line 133
    const/4 v7, 0x4

    .line 134
    if-lt v5, v7, :cond_3

    .line 135
    .line 136
    aget-object v5, v4, v6

    .line 137
    .line 138
    if-eqz v5, :cond_3

    .line 139
    .line 140
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 141
    .line 142
    int-to-float v3, v3

    .line 143
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 144
    .line 145
    .line 146
    move-result v5

    .line 147
    int-to-float v5, v5

    .line 148
    aget-object v4, v4, v6

    .line 149
    .line 150
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 151
    .line 152
    .line 153
    move-result v4

    .line 154
    mul-float v5, v5, v4

    .line 155
    .line 156
    add-float/2addr v3, v5

    .line 157
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 158
    .line 159
    .line 160
    move-result-object v4

    .line 161
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 162
    .line 163
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 164
    .line 165
    .line 166
    new-instance v12, Landroid/graphics/Path;

    .line 167
    .line 168
    invoke-direct {v12}, Landroid/graphics/Path;-><init>()V

    .line 169
    .line 170
    .line 171
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 172
    .line 173
    int-to-float v7, v6

    .line 174
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 175
    .line 176
    int-to-float v8, v6

    .line 177
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 178
    .line 179
    int-to-float v9, v6

    .line 180
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 181
    .line 182
    int-to-float v10, v6

    .line 183
    sget-object v11, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 184
    .line 185
    move-object v6, v12

    .line 186
    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 187
    .line 188
    .line 189
    invoke-virtual {v5, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 190
    .line 191
    .line 192
    invoke-virtual {v5, v12}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 193
    .line 194
    .line 195
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 196
    .line 197
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    .line 199
    .line 200
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 201
    .line 202
    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 203
    .line 204
    .line 205
    new-instance v5, Landroid/graphics/Path;

    .line 206
    .line 207
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 208
    .line 209
    .line 210
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 211
    .line 212
    int-to-float v6, v6

    .line 213
    invoke-virtual {v5, v2, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 214
    .line 215
    .line 216
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 217
    .line 218
    int-to-float p1, p1

    .line 219
    invoke-virtual {v5, v2, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 220
    .line 221
    .line 222
    invoke-virtual {v5, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 223
    .line 224
    .line 225
    invoke-virtual {v5, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 226
    .line 227
    .line 228
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 229
    .line 230
    .line 231
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 232
    .line 233
    .line 234
    move-result-object p0

    .line 235
    invoke-virtual {v4, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 236
    .line 237
    .line 238
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 239
    .line 240
    invoke-interface {p0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    .line 242
    .line 243
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 244
    .line 245
    return-object p0
.end method

.method private static get03AccentCallout3(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v6

    .line 37
    int-to-float v6, v6

    .line 38
    mul-float v6, v6, v3

    .line 39
    .line 40
    add-float/2addr v4, v6

    .line 41
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v6, v6

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v7

    .line 48
    int-to-float v7, v7

    .line 49
    mul-float v7, v7, v5

    .line 50
    .line 51
    add-float/2addr v6, v7

    .line 52
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 53
    .line 54
    int-to-float v7, v7

    .line 55
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 56
    .line 57
    .line 58
    move-result v8

    .line 59
    int-to-float v8, v8

    .line 60
    mul-float v8, v8, v3

    .line 61
    .line 62
    add-float/2addr v7, v8

    .line 63
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 64
    .line 65
    int-to-float v3, v3

    .line 66
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 67
    .line 68
    .line 69
    move-result v8

    .line 70
    int-to-float v8, v8

    .line 71
    mul-float v8, v8, v5

    .line 72
    .line 73
    add-float/2addr v3, v8

    .line 74
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 75
    .line 76
    .line 77
    move-result-object v5

    .line 78
    if-eqz v5, :cond_5

    .line 79
    .line 80
    array-length v8, v5

    .line 81
    const/4 v9, 0x1

    .line 82
    if-lt v8, v9, :cond_5

    .line 83
    .line 84
    const/4 v8, 0x0

    .line 85
    aget-object v10, v5, v8

    .line 86
    .line 87
    if-eqz v10, :cond_0

    .line 88
    .line 89
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 90
    .line 91
    int-to-float v2, v2

    .line 92
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 93
    .line 94
    .line 95
    move-result v10

    .line 96
    int-to-float v10, v10

    .line 97
    aget-object v8, v5, v8

    .line 98
    .line 99
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 100
    .line 101
    .line 102
    move-result v8

    .line 103
    mul-float v10, v10, v8

    .line 104
    .line 105
    add-float/2addr v2, v10

    .line 106
    :cond_0
    array-length v8, v5

    .line 107
    const/4 v10, 0x2

    .line 108
    if-lt v8, v10, :cond_1

    .line 109
    .line 110
    aget-object v8, v5, v9

    .line 111
    .line 112
    if-eqz v8, :cond_1

    .line 113
    .line 114
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 115
    .line 116
    int-to-float v1, v1

    .line 117
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 118
    .line 119
    .line 120
    move-result v8

    .line 121
    int-to-float v8, v8

    .line 122
    aget-object v9, v5, v9

    .line 123
    .line 124
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 125
    .line 126
    .line 127
    move-result v9

    .line 128
    mul-float v8, v8, v9

    .line 129
    .line 130
    add-float/2addr v1, v8

    .line 131
    :cond_1
    array-length v8, v5

    .line 132
    const/4 v9, 0x3

    .line 133
    if-lt v8, v9, :cond_2

    .line 134
    .line 135
    aget-object v8, v5, v10

    .line 136
    .line 137
    if-eqz v8, :cond_2

    .line 138
    .line 139
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 140
    .line 141
    int-to-float v6, v6

    .line 142
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 143
    .line 144
    .line 145
    move-result v8

    .line 146
    int-to-float v8, v8

    .line 147
    aget-object v10, v5, v10

    .line 148
    .line 149
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 150
    .line 151
    .line 152
    move-result v10

    .line 153
    mul-float v8, v8, v10

    .line 154
    .line 155
    add-float/2addr v6, v8

    .line 156
    :cond_2
    array-length v8, v5

    .line 157
    const/4 v10, 0x4

    .line 158
    if-lt v8, v10, :cond_3

    .line 159
    .line 160
    aget-object v8, v5, v9

    .line 161
    .line 162
    if-eqz v8, :cond_3

    .line 163
    .line 164
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 165
    .line 166
    int-to-float v4, v4

    .line 167
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 168
    .line 169
    .line 170
    move-result v8

    .line 171
    int-to-float v8, v8

    .line 172
    aget-object v9, v5, v9

    .line 173
    .line 174
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 175
    .line 176
    .line 177
    move-result v9

    .line 178
    mul-float v8, v8, v9

    .line 179
    .line 180
    add-float/2addr v4, v8

    .line 181
    :cond_3
    array-length v8, v5

    .line 182
    const/4 v9, 0x5

    .line 183
    if-lt v8, v9, :cond_4

    .line 184
    .line 185
    aget-object v8, v5, v10

    .line 186
    .line 187
    if-eqz v8, :cond_4

    .line 188
    .line 189
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 190
    .line 191
    int-to-float v3, v3

    .line 192
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 193
    .line 194
    .line 195
    move-result v8

    .line 196
    int-to-float v8, v8

    .line 197
    aget-object v10, v5, v10

    .line 198
    .line 199
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 200
    .line 201
    .line 202
    move-result v10

    .line 203
    mul-float v8, v8, v10

    .line 204
    .line 205
    add-float/2addr v3, v8

    .line 206
    :cond_4
    array-length v8, v5

    .line 207
    const/4 v10, 0x6

    .line 208
    if-lt v8, v10, :cond_5

    .line 209
    .line 210
    aget-object v8, v5, v9

    .line 211
    .line 212
    if-eqz v8, :cond_5

    .line 213
    .line 214
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 215
    .line 216
    int-to-float v7, v7

    .line 217
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 218
    .line 219
    .line 220
    move-result v8

    .line 221
    int-to-float v8, v8

    .line 222
    aget-object v5, v5, v9

    .line 223
    .line 224
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 225
    .line 226
    .line 227
    move-result v5

    .line 228
    mul-float v8, v8, v5

    .line 229
    .line 230
    add-float/2addr v7, v8

    .line 231
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 232
    .line 233
    .line 234
    move-result-object v5

    .line 235
    new-instance v8, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 236
    .line 237
    invoke-direct {v8}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 238
    .line 239
    .line 240
    new-instance v15, Landroid/graphics/Path;

    .line 241
    .line 242
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 243
    .line 244
    .line 245
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 246
    .line 247
    int-to-float v10, v9

    .line 248
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 249
    .line 250
    int-to-float v11, v9

    .line 251
    iget v9, v0, Landroid/graphics/Rect;->right:I

    .line 252
    .line 253
    int-to-float v12, v9

    .line 254
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 255
    .line 256
    int-to-float v13, v9

    .line 257
    sget-object v14, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 258
    .line 259
    move-object v9, v15

    .line 260
    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 261
    .line 262
    .line 263
    invoke-virtual {v8, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 264
    .line 265
    .line 266
    invoke-virtual {v8, v15}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 267
    .line 268
    .line 269
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 270
    .line 271
    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    .line 273
    .line 274
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 275
    .line 276
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 277
    .line 278
    .line 279
    new-instance v8, Landroid/graphics/Path;

    .line 280
    .line 281
    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    .line 282
    .line 283
    .line 284
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 285
    .line 286
    int-to-float v9, v9

    .line 287
    invoke-virtual {v8, v3, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 288
    .line 289
    .line 290
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 291
    .line 292
    int-to-float v0, v0

    .line 293
    invoke-virtual {v8, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 294
    .line 295
    .line 296
    invoke-virtual {v8, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 297
    .line 298
    .line 299
    invoke-virtual {v8, v6, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 300
    .line 301
    .line 302
    invoke-virtual {v8, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 303
    .line 304
    .line 305
    invoke-virtual {v5, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 306
    .line 307
    .line 308
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 309
    .line 310
    .line 311
    move-result-object v0

    .line 312
    invoke-virtual {v5, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 313
    .line 314
    .line 315
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 316
    .line 317
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318
    .line 319
    .line 320
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 321
    .line 322
    return-object v0
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static get03AccentCallout4(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v5

    .line 37
    int-to-float v5, v5

    .line 38
    mul-float v5, v5, v3

    .line 39
    .line 40
    add-float/2addr v4, v5

    .line 41
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v5, v5

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v6

    .line 48
    int-to-float v6, v6

    .line 49
    const v7, -0x41d55476    # -0.16667f

    .line 50
    .line 51
    .line 52
    mul-float v6, v6, v7

    .line 53
    .line 54
    add-float/2addr v5, v6

    .line 55
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 56
    .line 57
    int-to-float v6, v6

    .line 58
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 59
    .line 60
    .line 61
    move-result v7

    .line 62
    int-to-float v7, v7

    .line 63
    mul-float v7, v7, v3

    .line 64
    .line 65
    add-float/2addr v6, v7

    .line 66
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 67
    .line 68
    int-to-float v7, v7

    .line 69
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 70
    .line 71
    .line 72
    move-result v8

    .line 73
    int-to-float v8, v8

    .line 74
    const v9, 0x3f8aaa8f

    .line 75
    .line 76
    .line 77
    mul-float v8, v8, v9

    .line 78
    .line 79
    add-float/2addr v7, v8

    .line 80
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 81
    .line 82
    int-to-float v8, v8

    .line 83
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 84
    .line 85
    .line 86
    move-result v10

    .line 87
    int-to-float v10, v10

    .line 88
    mul-float v10, v10, v3

    .line 89
    .line 90
    add-float/2addr v8, v10

    .line 91
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 92
    .line 93
    int-to-float v3, v3

    .line 94
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 95
    .line 96
    .line 97
    move-result v10

    .line 98
    int-to-float v10, v10

    .line 99
    mul-float v10, v10, v9

    .line 100
    .line 101
    add-float/2addr v3, v10

    .line 102
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 103
    .line 104
    .line 105
    move-result-object v9

    .line 106
    if-eqz v9, :cond_7

    .line 107
    .line 108
    array-length v10, v9

    .line 109
    const/4 v11, 0x1

    .line 110
    if-lt v10, v11, :cond_7

    .line 111
    .line 112
    const/4 v10, 0x0

    .line 113
    aget-object v12, v9, v10

    .line 114
    .line 115
    if-eqz v12, :cond_0

    .line 116
    .line 117
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 118
    .line 119
    int-to-float v2, v2

    .line 120
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 121
    .line 122
    .line 123
    move-result v12

    .line 124
    int-to-float v12, v12

    .line 125
    aget-object v10, v9, v10

    .line 126
    .line 127
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 128
    .line 129
    .line 130
    move-result v10

    .line 131
    mul-float v12, v12, v10

    .line 132
    .line 133
    add-float/2addr v2, v12

    .line 134
    :cond_0
    array-length v10, v9

    .line 135
    const/4 v12, 0x2

    .line 136
    if-lt v10, v12, :cond_1

    .line 137
    .line 138
    aget-object v10, v9, v11

    .line 139
    .line 140
    if-eqz v10, :cond_1

    .line 141
    .line 142
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 143
    .line 144
    int-to-float v1, v1

    .line 145
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 146
    .line 147
    .line 148
    move-result v10

    .line 149
    int-to-float v10, v10

    .line 150
    aget-object v11, v9, v11

    .line 151
    .line 152
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    .line 153
    .line 154
    .line 155
    move-result v11

    .line 156
    mul-float v10, v10, v11

    .line 157
    .line 158
    add-float/2addr v1, v10

    .line 159
    :cond_1
    array-length v10, v9

    .line 160
    const/4 v11, 0x3

    .line 161
    if-lt v10, v11, :cond_2

    .line 162
    .line 163
    aget-object v10, v9, v12

    .line 164
    .line 165
    if-eqz v10, :cond_2

    .line 166
    .line 167
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 168
    .line 169
    int-to-float v5, v5

    .line 170
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 171
    .line 172
    .line 173
    move-result v10

    .line 174
    int-to-float v10, v10

    .line 175
    aget-object v12, v9, v12

    .line 176
    .line 177
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    .line 178
    .line 179
    .line 180
    move-result v12

    .line 181
    mul-float v10, v10, v12

    .line 182
    .line 183
    add-float/2addr v5, v10

    .line 184
    :cond_2
    array-length v10, v9

    .line 185
    const/4 v12, 0x4

    .line 186
    if-lt v10, v12, :cond_3

    .line 187
    .line 188
    aget-object v10, v9, v11

    .line 189
    .line 190
    if-eqz v10, :cond_3

    .line 191
    .line 192
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 193
    .line 194
    int-to-float v4, v4

    .line 195
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 196
    .line 197
    .line 198
    move-result v10

    .line 199
    int-to-float v10, v10

    .line 200
    aget-object v11, v9, v11

    .line 201
    .line 202
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    .line 203
    .line 204
    .line 205
    move-result v11

    .line 206
    mul-float v10, v10, v11

    .line 207
    .line 208
    add-float/2addr v4, v10

    .line 209
    :cond_3
    array-length v10, v9

    .line 210
    const/4 v11, 0x5

    .line 211
    if-lt v10, v11, :cond_4

    .line 212
    .line 213
    aget-object v10, v9, v12

    .line 214
    .line 215
    if-eqz v10, :cond_4

    .line 216
    .line 217
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 218
    .line 219
    int-to-float v7, v7

    .line 220
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 221
    .line 222
    .line 223
    move-result v10

    .line 224
    int-to-float v10, v10

    .line 225
    aget-object v12, v9, v12

    .line 226
    .line 227
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    .line 228
    .line 229
    .line 230
    move-result v12

    .line 231
    mul-float v10, v10, v12

    .line 232
    .line 233
    add-float/2addr v7, v10

    .line 234
    :cond_4
    array-length v10, v9

    .line 235
    const/4 v12, 0x6

    .line 236
    if-lt v10, v12, :cond_5

    .line 237
    .line 238
    aget-object v10, v9, v11

    .line 239
    .line 240
    if-eqz v10, :cond_5

    .line 241
    .line 242
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 243
    .line 244
    int-to-float v6, v6

    .line 245
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 246
    .line 247
    .line 248
    move-result v10

    .line 249
    int-to-float v10, v10

    .line 250
    aget-object v11, v9, v11

    .line 251
    .line 252
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    .line 253
    .line 254
    .line 255
    move-result v11

    .line 256
    mul-float v10, v10, v11

    .line 257
    .line 258
    add-float/2addr v6, v10

    .line 259
    :cond_5
    array-length v10, v9

    .line 260
    const/4 v11, 0x7

    .line 261
    if-lt v10, v11, :cond_6

    .line 262
    .line 263
    aget-object v10, v9, v12

    .line 264
    .line 265
    if-eqz v10, :cond_6

    .line 266
    .line 267
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 268
    .line 269
    int-to-float v3, v3

    .line 270
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 271
    .line 272
    .line 273
    move-result v10

    .line 274
    int-to-float v10, v10

    .line 275
    aget-object v12, v9, v12

    .line 276
    .line 277
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    .line 278
    .line 279
    .line 280
    move-result v12

    .line 281
    mul-float v10, v10, v12

    .line 282
    .line 283
    add-float/2addr v3, v10

    .line 284
    :cond_6
    array-length v10, v9

    .line 285
    const/16 v12, 0x8

    .line 286
    .line 287
    if-lt v10, v12, :cond_7

    .line 288
    .line 289
    aget-object v10, v9, v11

    .line 290
    .line 291
    if-eqz v10, :cond_7

    .line 292
    .line 293
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 294
    .line 295
    int-to-float v8, v8

    .line 296
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 297
    .line 298
    .line 299
    move-result v10

    .line 300
    int-to-float v10, v10

    .line 301
    aget-object v9, v9, v11

    .line 302
    .line 303
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 304
    .line 305
    .line 306
    move-result v9

    .line 307
    mul-float v10, v10, v9

    .line 308
    .line 309
    add-float/2addr v8, v10

    .line 310
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 311
    .line 312
    .line 313
    move-result-object v9

    .line 314
    new-instance v10, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 315
    .line 316
    invoke-direct {v10}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 317
    .line 318
    .line 319
    new-instance v15, Landroid/graphics/Path;

    .line 320
    .line 321
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 322
    .line 323
    .line 324
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 325
    .line 326
    int-to-float v12, v11

    .line 327
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 328
    .line 329
    int-to-float v13, v11

    .line 330
    iget v11, v0, Landroid/graphics/Rect;->right:I

    .line 331
    .line 332
    int-to-float v14, v11

    .line 333
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 334
    .line 335
    int-to-float v11, v11

    .line 336
    sget-object v16, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 337
    .line 338
    move/from16 v17, v11

    .line 339
    .line 340
    move-object v11, v15

    .line 341
    move/from16 v18, v8

    .line 342
    .line 343
    move-object v8, v15

    .line 344
    move/from16 v15, v17

    .line 345
    .line 346
    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 347
    .line 348
    .line 349
    invoke-virtual {v10, v9}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 350
    .line 351
    .line 352
    invoke-virtual {v10, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 353
    .line 354
    .line 355
    sget-object v8, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 356
    .line 357
    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 358
    .line 359
    .line 360
    new-instance v8, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 361
    .line 362
    invoke-direct {v8}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 363
    .line 364
    .line 365
    new-instance v9, Landroid/graphics/Path;

    .line 366
    .line 367
    invoke-direct {v9}, Landroid/graphics/Path;-><init>()V

    .line 368
    .line 369
    .line 370
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 371
    .line 372
    int-to-float v10, v10

    .line 373
    invoke-virtual {v9, v3, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 374
    .line 375
    .line 376
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 377
    .line 378
    int-to-float v0, v0

    .line 379
    invoke-virtual {v9, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 380
    .line 381
    .line 382
    invoke-virtual {v9, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 383
    .line 384
    .line 385
    invoke-virtual {v9, v5, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 386
    .line 387
    .line 388
    invoke-virtual {v9, v7, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 389
    .line 390
    .line 391
    move/from16 v0, v18

    .line 392
    .line 393
    invoke-virtual {v9, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 394
    .line 395
    .line 396
    invoke-virtual {v8, v9}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 397
    .line 398
    .line 399
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 400
    .line 401
    .line 402
    move-result-object v0

    .line 403
    invoke-virtual {v8, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 404
    .line 405
    .line 406
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 407
    .line 408
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409
    .line 410
    .line 411
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 412
    .line 413
    return-object v0
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static get03BorderCallout2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    int-to-float v1, v1

    .line 9
    const/high16 v2, 0x3e400000    # 0.1875f

    .line 10
    .line 11
    mul-float v1, v1, v2

    .line 12
    .line 13
    add-float/2addr v0, v1

    .line 14
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 15
    .line 16
    int-to-float v1, v1

    .line 17
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    int-to-float v3, v3

    .line 22
    const v4, -0x42555715    # -0.08333f

    .line 23
    .line 24
    .line 25
    mul-float v3, v3, v4

    .line 26
    .line 27
    add-float/2addr v1, v3

    .line 28
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 29
    .line 30
    int-to-float v3, v3

    .line 31
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 32
    .line 33
    .line 34
    move-result v5

    .line 35
    int-to-float v5, v5

    .line 36
    mul-float v5, v5, v2

    .line 37
    .line 38
    add-float/2addr v3, v5

    .line 39
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 40
    .line 41
    int-to-float v2, v2

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 43
    .line 44
    .line 45
    move-result v5

    .line 46
    int-to-float v5, v5

    .line 47
    mul-float v5, v5, v4

    .line 48
    .line 49
    add-float/2addr v2, v5

    .line 50
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    if-eqz v4, :cond_3

    .line 55
    .line 56
    array-length v5, v4

    .line 57
    const/4 v6, 0x1

    .line 58
    if-lt v5, v6, :cond_3

    .line 59
    .line 60
    const/4 v5, 0x0

    .line 61
    aget-object v7, v4, v5

    .line 62
    .line 63
    if-eqz v7, :cond_0

    .line 64
    .line 65
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 66
    .line 67
    int-to-float v1, v1

    .line 68
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 69
    .line 70
    .line 71
    move-result v7

    .line 72
    int-to-float v7, v7

    .line 73
    aget-object v5, v4, v5

    .line 74
    .line 75
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 76
    .line 77
    .line 78
    move-result v5

    .line 79
    mul-float v7, v7, v5

    .line 80
    .line 81
    add-float/2addr v1, v7

    .line 82
    :cond_0
    array-length v5, v4

    .line 83
    const/4 v7, 0x2

    .line 84
    if-lt v5, v7, :cond_1

    .line 85
    .line 86
    aget-object v5, v4, v6

    .line 87
    .line 88
    if-eqz v5, :cond_1

    .line 89
    .line 90
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 91
    .line 92
    int-to-float v0, v0

    .line 93
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 94
    .line 95
    .line 96
    move-result v5

    .line 97
    int-to-float v5, v5

    .line 98
    aget-object v6, v4, v6

    .line 99
    .line 100
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 101
    .line 102
    .line 103
    move-result v6

    .line 104
    mul-float v5, v5, v6

    .line 105
    .line 106
    add-float/2addr v0, v5

    .line 107
    :cond_1
    array-length v5, v4

    .line 108
    const/4 v6, 0x3

    .line 109
    if-lt v5, v6, :cond_2

    .line 110
    .line 111
    aget-object v5, v4, v7

    .line 112
    .line 113
    if-eqz v5, :cond_2

    .line 114
    .line 115
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 116
    .line 117
    int-to-float v2, v2

    .line 118
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 119
    .line 120
    .line 121
    move-result v5

    .line 122
    int-to-float v5, v5

    .line 123
    aget-object v7, v4, v7

    .line 124
    .line 125
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 126
    .line 127
    .line 128
    move-result v7

    .line 129
    mul-float v5, v5, v7

    .line 130
    .line 131
    add-float/2addr v2, v5

    .line 132
    :cond_2
    array-length v5, v4

    .line 133
    const/4 v7, 0x4

    .line 134
    if-lt v5, v7, :cond_3

    .line 135
    .line 136
    aget-object v5, v4, v6

    .line 137
    .line 138
    if-eqz v5, :cond_3

    .line 139
    .line 140
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 141
    .line 142
    int-to-float v3, v3

    .line 143
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 144
    .line 145
    .line 146
    move-result v5

    .line 147
    int-to-float v5, v5

    .line 148
    aget-object v4, v4, v6

    .line 149
    .line 150
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 151
    .line 152
    .line 153
    move-result v4

    .line 154
    mul-float v5, v5, v4

    .line 155
    .line 156
    add-float/2addr v3, v5

    .line 157
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 158
    .line 159
    .line 160
    move-result-object v4

    .line 161
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 162
    .line 163
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 164
    .line 165
    .line 166
    new-instance v12, Landroid/graphics/Path;

    .line 167
    .line 168
    invoke-direct {v12}, Landroid/graphics/Path;-><init>()V

    .line 169
    .line 170
    .line 171
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 172
    .line 173
    int-to-float v7, v6

    .line 174
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 175
    .line 176
    int-to-float v8, v6

    .line 177
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 178
    .line 179
    int-to-float v9, v6

    .line 180
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 181
    .line 182
    int-to-float v10, p1

    .line 183
    sget-object v11, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 184
    .line 185
    move-object v6, v12

    .line 186
    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 187
    .line 188
    .line 189
    invoke-virtual {v5, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 190
    .line 191
    .line 192
    invoke-virtual {v5, v12}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 193
    .line 194
    .line 195
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 196
    .line 197
    .line 198
    move-result-object p1

    .line 199
    invoke-virtual {v5, p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 200
    .line 201
    .line 202
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 203
    .line 204
    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    .line 206
    .line 207
    new-instance p1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 208
    .line 209
    invoke-direct {p1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 210
    .line 211
    .line 212
    new-instance v4, Landroid/graphics/Path;

    .line 213
    .line 214
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 215
    .line 216
    .line 217
    invoke-virtual {v4, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 218
    .line 219
    .line 220
    invoke-virtual {v4, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 221
    .line 222
    .line 223
    invoke-virtual {p1, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 224
    .line 225
    .line 226
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 227
    .line 228
    .line 229
    move-result-object p0

    .line 230
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 231
    .line 232
    .line 233
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 234
    .line 235
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    .line 237
    .line 238
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 239
    .line 240
    return-object p0
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static get03BorderCallout3Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v6

    .line 37
    int-to-float v6, v6

    .line 38
    mul-float v6, v6, v3

    .line 39
    .line 40
    add-float/2addr v4, v6

    .line 41
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v6, v6

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v7

    .line 48
    int-to-float v7, v7

    .line 49
    mul-float v7, v7, v5

    .line 50
    .line 51
    add-float/2addr v6, v7

    .line 52
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 53
    .line 54
    int-to-float v7, v7

    .line 55
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 56
    .line 57
    .line 58
    move-result v8

    .line 59
    int-to-float v8, v8

    .line 60
    mul-float v8, v8, v3

    .line 61
    .line 62
    add-float/2addr v7, v8

    .line 63
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 64
    .line 65
    int-to-float v3, v3

    .line 66
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 67
    .line 68
    .line 69
    move-result v8

    .line 70
    int-to-float v8, v8

    .line 71
    mul-float v8, v8, v5

    .line 72
    .line 73
    add-float/2addr v3, v8

    .line 74
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 75
    .line 76
    .line 77
    move-result-object v5

    .line 78
    if-eqz v5, :cond_5

    .line 79
    .line 80
    array-length v8, v5

    .line 81
    const/4 v9, 0x1

    .line 82
    if-lt v8, v9, :cond_5

    .line 83
    .line 84
    const/4 v8, 0x0

    .line 85
    aget-object v10, v5, v8

    .line 86
    .line 87
    if-eqz v10, :cond_0

    .line 88
    .line 89
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 90
    .line 91
    int-to-float v2, v2

    .line 92
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 93
    .line 94
    .line 95
    move-result v10

    .line 96
    int-to-float v10, v10

    .line 97
    aget-object v8, v5, v8

    .line 98
    .line 99
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 100
    .line 101
    .line 102
    move-result v8

    .line 103
    mul-float v10, v10, v8

    .line 104
    .line 105
    add-float/2addr v2, v10

    .line 106
    :cond_0
    array-length v8, v5

    .line 107
    const/4 v10, 0x2

    .line 108
    if-lt v8, v10, :cond_1

    .line 109
    .line 110
    aget-object v8, v5, v9

    .line 111
    .line 112
    if-eqz v8, :cond_1

    .line 113
    .line 114
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 115
    .line 116
    int-to-float v1, v1

    .line 117
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 118
    .line 119
    .line 120
    move-result v8

    .line 121
    int-to-float v8, v8

    .line 122
    aget-object v9, v5, v9

    .line 123
    .line 124
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 125
    .line 126
    .line 127
    move-result v9

    .line 128
    mul-float v8, v8, v9

    .line 129
    .line 130
    add-float/2addr v1, v8

    .line 131
    :cond_1
    array-length v8, v5

    .line 132
    const/4 v9, 0x3

    .line 133
    if-lt v8, v9, :cond_2

    .line 134
    .line 135
    aget-object v8, v5, v10

    .line 136
    .line 137
    if-eqz v8, :cond_2

    .line 138
    .line 139
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 140
    .line 141
    int-to-float v6, v6

    .line 142
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 143
    .line 144
    .line 145
    move-result v8

    .line 146
    int-to-float v8, v8

    .line 147
    aget-object v10, v5, v10

    .line 148
    .line 149
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 150
    .line 151
    .line 152
    move-result v10

    .line 153
    mul-float v8, v8, v10

    .line 154
    .line 155
    add-float/2addr v6, v8

    .line 156
    :cond_2
    array-length v8, v5

    .line 157
    const/4 v10, 0x4

    .line 158
    if-lt v8, v10, :cond_3

    .line 159
    .line 160
    aget-object v8, v5, v9

    .line 161
    .line 162
    if-eqz v8, :cond_3

    .line 163
    .line 164
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 165
    .line 166
    int-to-float v4, v4

    .line 167
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 168
    .line 169
    .line 170
    move-result v8

    .line 171
    int-to-float v8, v8

    .line 172
    aget-object v9, v5, v9

    .line 173
    .line 174
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 175
    .line 176
    .line 177
    move-result v9

    .line 178
    mul-float v8, v8, v9

    .line 179
    .line 180
    add-float/2addr v4, v8

    .line 181
    :cond_3
    array-length v8, v5

    .line 182
    const/4 v9, 0x5

    .line 183
    if-lt v8, v9, :cond_4

    .line 184
    .line 185
    aget-object v8, v5, v10

    .line 186
    .line 187
    if-eqz v8, :cond_4

    .line 188
    .line 189
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 190
    .line 191
    int-to-float v3, v3

    .line 192
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 193
    .line 194
    .line 195
    move-result v8

    .line 196
    int-to-float v8, v8

    .line 197
    aget-object v10, v5, v10

    .line 198
    .line 199
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 200
    .line 201
    .line 202
    move-result v10

    .line 203
    mul-float v8, v8, v10

    .line 204
    .line 205
    add-float/2addr v3, v8

    .line 206
    :cond_4
    array-length v8, v5

    .line 207
    const/4 v10, 0x6

    .line 208
    if-lt v8, v10, :cond_5

    .line 209
    .line 210
    aget-object v8, v5, v9

    .line 211
    .line 212
    if-eqz v8, :cond_5

    .line 213
    .line 214
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 215
    .line 216
    int-to-float v7, v7

    .line 217
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 218
    .line 219
    .line 220
    move-result v8

    .line 221
    int-to-float v8, v8

    .line 222
    aget-object v5, v5, v9

    .line 223
    .line 224
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 225
    .line 226
    .line 227
    move-result v5

    .line 228
    mul-float v8, v8, v5

    .line 229
    .line 230
    add-float/2addr v7, v8

    .line 231
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 232
    .line 233
    .line 234
    move-result-object v5

    .line 235
    new-instance v8, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 236
    .line 237
    invoke-direct {v8}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 238
    .line 239
    .line 240
    new-instance v15, Landroid/graphics/Path;

    .line 241
    .line 242
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 243
    .line 244
    .line 245
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 246
    .line 247
    int-to-float v10, v9

    .line 248
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 249
    .line 250
    int-to-float v11, v9

    .line 251
    iget v9, v0, Landroid/graphics/Rect;->right:I

    .line 252
    .line 253
    int-to-float v12, v9

    .line 254
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 255
    .line 256
    int-to-float v13, v0

    .line 257
    sget-object v14, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 258
    .line 259
    move-object v9, v15

    .line 260
    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 261
    .line 262
    .line 263
    invoke-virtual {v8, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 264
    .line 265
    .line 266
    invoke-virtual {v8, v15}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 267
    .line 268
    .line 269
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 270
    .line 271
    .line 272
    move-result-object v0

    .line 273
    invoke-virtual {v8, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 274
    .line 275
    .line 276
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 277
    .line 278
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 279
    .line 280
    .line 281
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 282
    .line 283
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 284
    .line 285
    .line 286
    new-instance v5, Landroid/graphics/Path;

    .line 287
    .line 288
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 289
    .line 290
    .line 291
    invoke-virtual {v5, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 292
    .line 293
    .line 294
    invoke-virtual {v5, v6, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 295
    .line 296
    .line 297
    invoke-virtual {v5, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 298
    .line 299
    .line 300
    invoke-virtual {v0, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 301
    .line 302
    .line 303
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 304
    .line 305
    .line 306
    move-result-object v1

    .line 307
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 308
    .line 309
    .line 310
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 311
    .line 312
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313
    .line 314
    .line 315
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 316
    .line 317
    return-object v0
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static get03BorderCallout4Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v5

    .line 37
    int-to-float v5, v5

    .line 38
    mul-float v5, v5, v3

    .line 39
    .line 40
    add-float/2addr v4, v5

    .line 41
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v5, v5

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v6

    .line 48
    int-to-float v6, v6

    .line 49
    const v7, -0x41d55476    # -0.16667f

    .line 50
    .line 51
    .line 52
    mul-float v6, v6, v7

    .line 53
    .line 54
    add-float/2addr v5, v6

    .line 55
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 56
    .line 57
    int-to-float v6, v6

    .line 58
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 59
    .line 60
    .line 61
    move-result v7

    .line 62
    int-to-float v7, v7

    .line 63
    mul-float v7, v7, v3

    .line 64
    .line 65
    add-float/2addr v6, v7

    .line 66
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 67
    .line 68
    int-to-float v7, v7

    .line 69
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 70
    .line 71
    .line 72
    move-result v8

    .line 73
    int-to-float v8, v8

    .line 74
    const v9, 0x3f8aaa8f

    .line 75
    .line 76
    .line 77
    mul-float v8, v8, v9

    .line 78
    .line 79
    add-float/2addr v7, v8

    .line 80
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 81
    .line 82
    int-to-float v8, v8

    .line 83
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 84
    .line 85
    .line 86
    move-result v10

    .line 87
    int-to-float v10, v10

    .line 88
    mul-float v10, v10, v3

    .line 89
    .line 90
    add-float/2addr v8, v10

    .line 91
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 92
    .line 93
    int-to-float v3, v3

    .line 94
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 95
    .line 96
    .line 97
    move-result v10

    .line 98
    int-to-float v10, v10

    .line 99
    mul-float v10, v10, v9

    .line 100
    .line 101
    add-float/2addr v3, v10

    .line 102
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 103
    .line 104
    .line 105
    move-result-object v9

    .line 106
    if-eqz v9, :cond_7

    .line 107
    .line 108
    array-length v10, v9

    .line 109
    const/4 v11, 0x1

    .line 110
    if-lt v10, v11, :cond_7

    .line 111
    .line 112
    const/4 v10, 0x0

    .line 113
    aget-object v12, v9, v10

    .line 114
    .line 115
    if-eqz v12, :cond_0

    .line 116
    .line 117
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 118
    .line 119
    int-to-float v2, v2

    .line 120
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 121
    .line 122
    .line 123
    move-result v12

    .line 124
    int-to-float v12, v12

    .line 125
    aget-object v10, v9, v10

    .line 126
    .line 127
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 128
    .line 129
    .line 130
    move-result v10

    .line 131
    mul-float v12, v12, v10

    .line 132
    .line 133
    add-float/2addr v2, v12

    .line 134
    :cond_0
    array-length v10, v9

    .line 135
    const/4 v12, 0x2

    .line 136
    if-lt v10, v12, :cond_1

    .line 137
    .line 138
    aget-object v10, v9, v11

    .line 139
    .line 140
    if-eqz v10, :cond_1

    .line 141
    .line 142
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 143
    .line 144
    int-to-float v1, v1

    .line 145
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 146
    .line 147
    .line 148
    move-result v10

    .line 149
    int-to-float v10, v10

    .line 150
    aget-object v11, v9, v11

    .line 151
    .line 152
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    .line 153
    .line 154
    .line 155
    move-result v11

    .line 156
    mul-float v10, v10, v11

    .line 157
    .line 158
    add-float/2addr v1, v10

    .line 159
    :cond_1
    array-length v10, v9

    .line 160
    const/4 v11, 0x3

    .line 161
    if-lt v10, v11, :cond_2

    .line 162
    .line 163
    aget-object v10, v9, v12

    .line 164
    .line 165
    if-eqz v10, :cond_2

    .line 166
    .line 167
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 168
    .line 169
    int-to-float v5, v5

    .line 170
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 171
    .line 172
    .line 173
    move-result v10

    .line 174
    int-to-float v10, v10

    .line 175
    aget-object v12, v9, v12

    .line 176
    .line 177
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    .line 178
    .line 179
    .line 180
    move-result v12

    .line 181
    mul-float v10, v10, v12

    .line 182
    .line 183
    add-float/2addr v5, v10

    .line 184
    :cond_2
    array-length v10, v9

    .line 185
    const/4 v12, 0x4

    .line 186
    if-lt v10, v12, :cond_3

    .line 187
    .line 188
    aget-object v10, v9, v11

    .line 189
    .line 190
    if-eqz v10, :cond_3

    .line 191
    .line 192
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 193
    .line 194
    int-to-float v4, v4

    .line 195
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 196
    .line 197
    .line 198
    move-result v10

    .line 199
    int-to-float v10, v10

    .line 200
    aget-object v11, v9, v11

    .line 201
    .line 202
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    .line 203
    .line 204
    .line 205
    move-result v11

    .line 206
    mul-float v10, v10, v11

    .line 207
    .line 208
    add-float/2addr v4, v10

    .line 209
    :cond_3
    array-length v10, v9

    .line 210
    const/4 v11, 0x5

    .line 211
    if-lt v10, v11, :cond_4

    .line 212
    .line 213
    aget-object v10, v9, v12

    .line 214
    .line 215
    if-eqz v10, :cond_4

    .line 216
    .line 217
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 218
    .line 219
    int-to-float v7, v7

    .line 220
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 221
    .line 222
    .line 223
    move-result v10

    .line 224
    int-to-float v10, v10

    .line 225
    aget-object v12, v9, v12

    .line 226
    .line 227
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    .line 228
    .line 229
    .line 230
    move-result v12

    .line 231
    mul-float v10, v10, v12

    .line 232
    .line 233
    add-float/2addr v7, v10

    .line 234
    :cond_4
    array-length v10, v9

    .line 235
    const/4 v12, 0x6

    .line 236
    if-lt v10, v12, :cond_5

    .line 237
    .line 238
    aget-object v10, v9, v11

    .line 239
    .line 240
    if-eqz v10, :cond_5

    .line 241
    .line 242
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 243
    .line 244
    int-to-float v6, v6

    .line 245
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 246
    .line 247
    .line 248
    move-result v10

    .line 249
    int-to-float v10, v10

    .line 250
    aget-object v11, v9, v11

    .line 251
    .line 252
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    .line 253
    .line 254
    .line 255
    move-result v11

    .line 256
    mul-float v10, v10, v11

    .line 257
    .line 258
    add-float/2addr v6, v10

    .line 259
    :cond_5
    array-length v10, v9

    .line 260
    const/4 v11, 0x7

    .line 261
    if-lt v10, v11, :cond_6

    .line 262
    .line 263
    aget-object v10, v9, v12

    .line 264
    .line 265
    if-eqz v10, :cond_6

    .line 266
    .line 267
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 268
    .line 269
    int-to-float v3, v3

    .line 270
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 271
    .line 272
    .line 273
    move-result v10

    .line 274
    int-to-float v10, v10

    .line 275
    aget-object v12, v9, v12

    .line 276
    .line 277
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    .line 278
    .line 279
    .line 280
    move-result v12

    .line 281
    mul-float v10, v10, v12

    .line 282
    .line 283
    add-float/2addr v3, v10

    .line 284
    :cond_6
    array-length v10, v9

    .line 285
    const/16 v12, 0x8

    .line 286
    .line 287
    if-lt v10, v12, :cond_7

    .line 288
    .line 289
    aget-object v10, v9, v11

    .line 290
    .line 291
    if-eqz v10, :cond_7

    .line 292
    .line 293
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 294
    .line 295
    int-to-float v8, v8

    .line 296
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 297
    .line 298
    .line 299
    move-result v10

    .line 300
    int-to-float v10, v10

    .line 301
    aget-object v9, v9, v11

    .line 302
    .line 303
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 304
    .line 305
    .line 306
    move-result v9

    .line 307
    mul-float v10, v10, v9

    .line 308
    .line 309
    add-float/2addr v8, v10

    .line 310
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 311
    .line 312
    .line 313
    move-result-object v9

    .line 314
    new-instance v10, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 315
    .line 316
    invoke-direct {v10}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 317
    .line 318
    .line 319
    new-instance v15, Landroid/graphics/Path;

    .line 320
    .line 321
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 322
    .line 323
    .line 324
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 325
    .line 326
    int-to-float v12, v11

    .line 327
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 328
    .line 329
    int-to-float v13, v11

    .line 330
    iget v11, v0, Landroid/graphics/Rect;->right:I

    .line 331
    .line 332
    int-to-float v14, v11

    .line 333
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 334
    .line 335
    int-to-float v0, v0

    .line 336
    sget-object v16, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 337
    .line 338
    move-object v11, v15

    .line 339
    move/from16 v17, v3

    .line 340
    .line 341
    move-object v3, v15

    .line 342
    move v15, v0

    .line 343
    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 344
    .line 345
    .line 346
    invoke-virtual {v10, v9}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 347
    .line 348
    .line 349
    invoke-virtual {v10, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 350
    .line 351
    .line 352
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 353
    .line 354
    .line 355
    move-result-object v0

    .line 356
    invoke-virtual {v10, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 357
    .line 358
    .line 359
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 360
    .line 361
    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 362
    .line 363
    .line 364
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 365
    .line 366
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 367
    .line 368
    .line 369
    new-instance v3, Landroid/graphics/Path;

    .line 370
    .line 371
    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 372
    .line 373
    .line 374
    invoke-virtual {v3, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 375
    .line 376
    .line 377
    invoke-virtual {v3, v5, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 378
    .line 379
    .line 380
    invoke-virtual {v3, v7, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 381
    .line 382
    .line 383
    move/from16 v1, v17

    .line 384
    .line 385
    invoke-virtual {v3, v1, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 386
    .line 387
    .line 388
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 389
    .line 390
    .line 391
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 392
    .line 393
    .line 394
    move-result-object v1

    .line 395
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 396
    .line 397
    .line 398
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 399
    .line 400
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 401
    .line 402
    .line 403
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 404
    .line 405
    return-object v0
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static get03Callout2(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    int-to-float v1, v1

    .line 9
    const/high16 v2, 0x3e400000    # 0.1875f

    .line 10
    .line 11
    mul-float v1, v1, v2

    .line 12
    .line 13
    add-float/2addr v0, v1

    .line 14
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 15
    .line 16
    int-to-float v1, v1

    .line 17
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    int-to-float v3, v3

    .line 22
    const v4, -0x42555715    # -0.08333f

    .line 23
    .line 24
    .line 25
    mul-float v3, v3, v4

    .line 26
    .line 27
    add-float/2addr v1, v3

    .line 28
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 29
    .line 30
    int-to-float v3, v3

    .line 31
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 32
    .line 33
    .line 34
    move-result v5

    .line 35
    int-to-float v5, v5

    .line 36
    mul-float v5, v5, v2

    .line 37
    .line 38
    add-float/2addr v3, v5

    .line 39
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 40
    .line 41
    int-to-float v2, v2

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 43
    .line 44
    .line 45
    move-result v5

    .line 46
    int-to-float v5, v5

    .line 47
    mul-float v5, v5, v4

    .line 48
    .line 49
    add-float/2addr v2, v5

    .line 50
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    if-eqz v4, :cond_3

    .line 55
    .line 56
    array-length v5, v4

    .line 57
    const/4 v6, 0x1

    .line 58
    if-lt v5, v6, :cond_3

    .line 59
    .line 60
    const/4 v5, 0x0

    .line 61
    aget-object v7, v4, v5

    .line 62
    .line 63
    if-eqz v7, :cond_0

    .line 64
    .line 65
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 66
    .line 67
    int-to-float v1, v1

    .line 68
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 69
    .line 70
    .line 71
    move-result v7

    .line 72
    int-to-float v7, v7

    .line 73
    aget-object v5, v4, v5

    .line 74
    .line 75
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 76
    .line 77
    .line 78
    move-result v5

    .line 79
    mul-float v7, v7, v5

    .line 80
    .line 81
    add-float/2addr v1, v7

    .line 82
    :cond_0
    array-length v5, v4

    .line 83
    const/4 v7, 0x2

    .line 84
    if-lt v5, v7, :cond_1

    .line 85
    .line 86
    aget-object v5, v4, v6

    .line 87
    .line 88
    if-eqz v5, :cond_1

    .line 89
    .line 90
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 91
    .line 92
    int-to-float v0, v0

    .line 93
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 94
    .line 95
    .line 96
    move-result v5

    .line 97
    int-to-float v5, v5

    .line 98
    aget-object v6, v4, v6

    .line 99
    .line 100
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 101
    .line 102
    .line 103
    move-result v6

    .line 104
    mul-float v5, v5, v6

    .line 105
    .line 106
    add-float/2addr v0, v5

    .line 107
    :cond_1
    array-length v5, v4

    .line 108
    const/4 v6, 0x3

    .line 109
    if-lt v5, v6, :cond_2

    .line 110
    .line 111
    aget-object v5, v4, v7

    .line 112
    .line 113
    if-eqz v5, :cond_2

    .line 114
    .line 115
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 116
    .line 117
    int-to-float v2, v2

    .line 118
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 119
    .line 120
    .line 121
    move-result v5

    .line 122
    int-to-float v5, v5

    .line 123
    aget-object v7, v4, v7

    .line 124
    .line 125
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 126
    .line 127
    .line 128
    move-result v7

    .line 129
    mul-float v5, v5, v7

    .line 130
    .line 131
    add-float/2addr v2, v5

    .line 132
    :cond_2
    array-length v5, v4

    .line 133
    const/4 v7, 0x4

    .line 134
    if-lt v5, v7, :cond_3

    .line 135
    .line 136
    aget-object v5, v4, v6

    .line 137
    .line 138
    if-eqz v5, :cond_3

    .line 139
    .line 140
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 141
    .line 142
    int-to-float v3, v3

    .line 143
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 144
    .line 145
    .line 146
    move-result v5

    .line 147
    int-to-float v5, v5

    .line 148
    aget-object v4, v4, v6

    .line 149
    .line 150
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 151
    .line 152
    .line 153
    move-result v4

    .line 154
    mul-float v5, v5, v4

    .line 155
    .line 156
    add-float/2addr v3, v5

    .line 157
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 158
    .line 159
    .line 160
    move-result-object v4

    .line 161
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 162
    .line 163
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 164
    .line 165
    .line 166
    new-instance v12, Landroid/graphics/Path;

    .line 167
    .line 168
    invoke-direct {v12}, Landroid/graphics/Path;-><init>()V

    .line 169
    .line 170
    .line 171
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 172
    .line 173
    int-to-float v7, v6

    .line 174
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 175
    .line 176
    int-to-float v8, v6

    .line 177
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 178
    .line 179
    int-to-float v9, v6

    .line 180
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 181
    .line 182
    int-to-float v10, p1

    .line 183
    sget-object v11, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 184
    .line 185
    move-object v6, v12

    .line 186
    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 187
    .line 188
    .line 189
    invoke-virtual {v5, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 190
    .line 191
    .line 192
    invoke-virtual {v5, v12}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 193
    .line 194
    .line 195
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 196
    .line 197
    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    .line 199
    .line 200
    new-instance p1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 201
    .line 202
    invoke-direct {p1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 203
    .line 204
    .line 205
    new-instance v4, Landroid/graphics/Path;

    .line 206
    .line 207
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 208
    .line 209
    .line 210
    invoke-virtual {v4, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 211
    .line 212
    .line 213
    invoke-virtual {v4, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 214
    .line 215
    .line 216
    invoke-virtual {p1, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 217
    .line 218
    .line 219
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 220
    .line 221
    .line 222
    move-result-object p0

    .line 223
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 224
    .line 225
    .line 226
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 227
    .line 228
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    .line 230
    .line 231
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 232
    .line 233
    return-object p0
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static get03Callout3(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v6

    .line 37
    int-to-float v6, v6

    .line 38
    mul-float v6, v6, v3

    .line 39
    .line 40
    add-float/2addr v4, v6

    .line 41
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v6, v6

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v7

    .line 48
    int-to-float v7, v7

    .line 49
    mul-float v7, v7, v5

    .line 50
    .line 51
    add-float/2addr v6, v7

    .line 52
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 53
    .line 54
    int-to-float v7, v7

    .line 55
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 56
    .line 57
    .line 58
    move-result v8

    .line 59
    int-to-float v8, v8

    .line 60
    mul-float v8, v8, v3

    .line 61
    .line 62
    add-float/2addr v7, v8

    .line 63
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 64
    .line 65
    int-to-float v3, v3

    .line 66
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 67
    .line 68
    .line 69
    move-result v8

    .line 70
    int-to-float v8, v8

    .line 71
    mul-float v8, v8, v5

    .line 72
    .line 73
    add-float/2addr v3, v8

    .line 74
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 75
    .line 76
    .line 77
    move-result-object v5

    .line 78
    if-eqz v5, :cond_5

    .line 79
    .line 80
    array-length v8, v5

    .line 81
    const/4 v9, 0x1

    .line 82
    if-lt v8, v9, :cond_5

    .line 83
    .line 84
    const/4 v8, 0x0

    .line 85
    aget-object v10, v5, v8

    .line 86
    .line 87
    if-eqz v10, :cond_0

    .line 88
    .line 89
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 90
    .line 91
    int-to-float v2, v2

    .line 92
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 93
    .line 94
    .line 95
    move-result v10

    .line 96
    int-to-float v10, v10

    .line 97
    aget-object v8, v5, v8

    .line 98
    .line 99
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 100
    .line 101
    .line 102
    move-result v8

    .line 103
    mul-float v10, v10, v8

    .line 104
    .line 105
    add-float/2addr v2, v10

    .line 106
    :cond_0
    array-length v8, v5

    .line 107
    const/4 v10, 0x2

    .line 108
    if-lt v8, v10, :cond_1

    .line 109
    .line 110
    aget-object v8, v5, v9

    .line 111
    .line 112
    if-eqz v8, :cond_1

    .line 113
    .line 114
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 115
    .line 116
    int-to-float v1, v1

    .line 117
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 118
    .line 119
    .line 120
    move-result v8

    .line 121
    int-to-float v8, v8

    .line 122
    aget-object v9, v5, v9

    .line 123
    .line 124
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 125
    .line 126
    .line 127
    move-result v9

    .line 128
    mul-float v8, v8, v9

    .line 129
    .line 130
    add-float/2addr v1, v8

    .line 131
    :cond_1
    array-length v8, v5

    .line 132
    const/4 v9, 0x3

    .line 133
    if-lt v8, v9, :cond_2

    .line 134
    .line 135
    aget-object v8, v5, v10

    .line 136
    .line 137
    if-eqz v8, :cond_2

    .line 138
    .line 139
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 140
    .line 141
    int-to-float v6, v6

    .line 142
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 143
    .line 144
    .line 145
    move-result v8

    .line 146
    int-to-float v8, v8

    .line 147
    aget-object v10, v5, v10

    .line 148
    .line 149
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 150
    .line 151
    .line 152
    move-result v10

    .line 153
    mul-float v8, v8, v10

    .line 154
    .line 155
    add-float/2addr v6, v8

    .line 156
    :cond_2
    array-length v8, v5

    .line 157
    const/4 v10, 0x4

    .line 158
    if-lt v8, v10, :cond_3

    .line 159
    .line 160
    aget-object v8, v5, v9

    .line 161
    .line 162
    if-eqz v8, :cond_3

    .line 163
    .line 164
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 165
    .line 166
    int-to-float v4, v4

    .line 167
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 168
    .line 169
    .line 170
    move-result v8

    .line 171
    int-to-float v8, v8

    .line 172
    aget-object v9, v5, v9

    .line 173
    .line 174
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 175
    .line 176
    .line 177
    move-result v9

    .line 178
    mul-float v8, v8, v9

    .line 179
    .line 180
    add-float/2addr v4, v8

    .line 181
    :cond_3
    array-length v8, v5

    .line 182
    const/4 v9, 0x5

    .line 183
    if-lt v8, v9, :cond_4

    .line 184
    .line 185
    aget-object v8, v5, v10

    .line 186
    .line 187
    if-eqz v8, :cond_4

    .line 188
    .line 189
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 190
    .line 191
    int-to-float v3, v3

    .line 192
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 193
    .line 194
    .line 195
    move-result v8

    .line 196
    int-to-float v8, v8

    .line 197
    aget-object v10, v5, v10

    .line 198
    .line 199
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 200
    .line 201
    .line 202
    move-result v10

    .line 203
    mul-float v8, v8, v10

    .line 204
    .line 205
    add-float/2addr v3, v8

    .line 206
    :cond_4
    array-length v8, v5

    .line 207
    const/4 v10, 0x6

    .line 208
    if-lt v8, v10, :cond_5

    .line 209
    .line 210
    aget-object v8, v5, v9

    .line 211
    .line 212
    if-eqz v8, :cond_5

    .line 213
    .line 214
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 215
    .line 216
    int-to-float v7, v7

    .line 217
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 218
    .line 219
    .line 220
    move-result v8

    .line 221
    int-to-float v8, v8

    .line 222
    aget-object v5, v5, v9

    .line 223
    .line 224
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 225
    .line 226
    .line 227
    move-result v5

    .line 228
    mul-float v8, v8, v5

    .line 229
    .line 230
    add-float/2addr v7, v8

    .line 231
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 232
    .line 233
    .line 234
    move-result-object v5

    .line 235
    new-instance v8, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 236
    .line 237
    invoke-direct {v8}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 238
    .line 239
    .line 240
    new-instance v15, Landroid/graphics/Path;

    .line 241
    .line 242
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 243
    .line 244
    .line 245
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 246
    .line 247
    int-to-float v10, v9

    .line 248
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 249
    .line 250
    int-to-float v11, v9

    .line 251
    iget v9, v0, Landroid/graphics/Rect;->right:I

    .line 252
    .line 253
    int-to-float v12, v9

    .line 254
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 255
    .line 256
    int-to-float v13, v0

    .line 257
    sget-object v14, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 258
    .line 259
    move-object v9, v15

    .line 260
    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 261
    .line 262
    .line 263
    invoke-virtual {v8, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 264
    .line 265
    .line 266
    invoke-virtual {v8, v15}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 267
    .line 268
    .line 269
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 270
    .line 271
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    .line 273
    .line 274
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 275
    .line 276
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 277
    .line 278
    .line 279
    new-instance v5, Landroid/graphics/Path;

    .line 280
    .line 281
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 282
    .line 283
    .line 284
    invoke-virtual {v5, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 285
    .line 286
    .line 287
    invoke-virtual {v5, v6, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 288
    .line 289
    .line 290
    invoke-virtual {v5, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 291
    .line 292
    .line 293
    invoke-virtual {v0, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 294
    .line 295
    .line 296
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 297
    .line 298
    .line 299
    move-result-object v1

    .line 300
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 301
    .line 302
    .line 303
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 304
    .line 305
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 306
    .line 307
    .line 308
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 309
    .line 310
    return-object v0
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static get03Callout4(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v5

    .line 37
    int-to-float v5, v5

    .line 38
    mul-float v5, v5, v3

    .line 39
    .line 40
    add-float/2addr v4, v5

    .line 41
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v5, v5

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v6

    .line 48
    int-to-float v6, v6

    .line 49
    const v7, -0x41d55476    # -0.16667f

    .line 50
    .line 51
    .line 52
    mul-float v6, v6, v7

    .line 53
    .line 54
    add-float/2addr v5, v6

    .line 55
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 56
    .line 57
    int-to-float v6, v6

    .line 58
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 59
    .line 60
    .line 61
    move-result v7

    .line 62
    int-to-float v7, v7

    .line 63
    mul-float v7, v7, v3

    .line 64
    .line 65
    add-float/2addr v6, v7

    .line 66
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 67
    .line 68
    int-to-float v7, v7

    .line 69
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 70
    .line 71
    .line 72
    move-result v8

    .line 73
    int-to-float v8, v8

    .line 74
    const v9, 0x3f8aaa8f

    .line 75
    .line 76
    .line 77
    mul-float v8, v8, v9

    .line 78
    .line 79
    add-float/2addr v7, v8

    .line 80
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 81
    .line 82
    int-to-float v8, v8

    .line 83
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 84
    .line 85
    .line 86
    move-result v10

    .line 87
    int-to-float v10, v10

    .line 88
    mul-float v10, v10, v3

    .line 89
    .line 90
    add-float/2addr v8, v10

    .line 91
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 92
    .line 93
    int-to-float v3, v3

    .line 94
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 95
    .line 96
    .line 97
    move-result v10

    .line 98
    int-to-float v10, v10

    .line 99
    mul-float v10, v10, v9

    .line 100
    .line 101
    add-float/2addr v3, v10

    .line 102
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 103
    .line 104
    .line 105
    move-result-object v9

    .line 106
    if-eqz v9, :cond_7

    .line 107
    .line 108
    array-length v10, v9

    .line 109
    const/4 v11, 0x1

    .line 110
    if-lt v10, v11, :cond_7

    .line 111
    .line 112
    const/4 v10, 0x0

    .line 113
    aget-object v12, v9, v10

    .line 114
    .line 115
    if-eqz v12, :cond_0

    .line 116
    .line 117
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 118
    .line 119
    int-to-float v2, v2

    .line 120
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 121
    .line 122
    .line 123
    move-result v12

    .line 124
    int-to-float v12, v12

    .line 125
    aget-object v10, v9, v10

    .line 126
    .line 127
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 128
    .line 129
    .line 130
    move-result v10

    .line 131
    mul-float v12, v12, v10

    .line 132
    .line 133
    add-float/2addr v2, v12

    .line 134
    :cond_0
    array-length v10, v9

    .line 135
    const/4 v12, 0x2

    .line 136
    if-lt v10, v12, :cond_1

    .line 137
    .line 138
    aget-object v10, v9, v11

    .line 139
    .line 140
    if-eqz v10, :cond_1

    .line 141
    .line 142
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 143
    .line 144
    int-to-float v1, v1

    .line 145
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 146
    .line 147
    .line 148
    move-result v10

    .line 149
    int-to-float v10, v10

    .line 150
    aget-object v11, v9, v11

    .line 151
    .line 152
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    .line 153
    .line 154
    .line 155
    move-result v11

    .line 156
    mul-float v10, v10, v11

    .line 157
    .line 158
    add-float/2addr v1, v10

    .line 159
    :cond_1
    array-length v10, v9

    .line 160
    const/4 v11, 0x3

    .line 161
    if-lt v10, v11, :cond_2

    .line 162
    .line 163
    aget-object v10, v9, v12

    .line 164
    .line 165
    if-eqz v10, :cond_2

    .line 166
    .line 167
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 168
    .line 169
    int-to-float v5, v5

    .line 170
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 171
    .line 172
    .line 173
    move-result v10

    .line 174
    int-to-float v10, v10

    .line 175
    aget-object v12, v9, v12

    .line 176
    .line 177
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    .line 178
    .line 179
    .line 180
    move-result v12

    .line 181
    mul-float v10, v10, v12

    .line 182
    .line 183
    add-float/2addr v5, v10

    .line 184
    :cond_2
    array-length v10, v9

    .line 185
    const/4 v12, 0x4

    .line 186
    if-lt v10, v12, :cond_3

    .line 187
    .line 188
    aget-object v10, v9, v11

    .line 189
    .line 190
    if-eqz v10, :cond_3

    .line 191
    .line 192
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 193
    .line 194
    int-to-float v4, v4

    .line 195
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 196
    .line 197
    .line 198
    move-result v10

    .line 199
    int-to-float v10, v10

    .line 200
    aget-object v11, v9, v11

    .line 201
    .line 202
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    .line 203
    .line 204
    .line 205
    move-result v11

    .line 206
    mul-float v10, v10, v11

    .line 207
    .line 208
    add-float/2addr v4, v10

    .line 209
    :cond_3
    array-length v10, v9

    .line 210
    const/4 v11, 0x5

    .line 211
    if-lt v10, v11, :cond_4

    .line 212
    .line 213
    aget-object v10, v9, v12

    .line 214
    .line 215
    if-eqz v10, :cond_4

    .line 216
    .line 217
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 218
    .line 219
    int-to-float v7, v7

    .line 220
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 221
    .line 222
    .line 223
    move-result v10

    .line 224
    int-to-float v10, v10

    .line 225
    aget-object v12, v9, v12

    .line 226
    .line 227
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    .line 228
    .line 229
    .line 230
    move-result v12

    .line 231
    mul-float v10, v10, v12

    .line 232
    .line 233
    add-float/2addr v7, v10

    .line 234
    :cond_4
    array-length v10, v9

    .line 235
    const/4 v12, 0x6

    .line 236
    if-lt v10, v12, :cond_5

    .line 237
    .line 238
    aget-object v10, v9, v11

    .line 239
    .line 240
    if-eqz v10, :cond_5

    .line 241
    .line 242
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 243
    .line 244
    int-to-float v6, v6

    .line 245
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 246
    .line 247
    .line 248
    move-result v10

    .line 249
    int-to-float v10, v10

    .line 250
    aget-object v11, v9, v11

    .line 251
    .line 252
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    .line 253
    .line 254
    .line 255
    move-result v11

    .line 256
    mul-float v10, v10, v11

    .line 257
    .line 258
    add-float/2addr v6, v10

    .line 259
    :cond_5
    array-length v10, v9

    .line 260
    const/4 v11, 0x7

    .line 261
    if-lt v10, v11, :cond_6

    .line 262
    .line 263
    aget-object v10, v9, v12

    .line 264
    .line 265
    if-eqz v10, :cond_6

    .line 266
    .line 267
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 268
    .line 269
    int-to-float v3, v3

    .line 270
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 271
    .line 272
    .line 273
    move-result v10

    .line 274
    int-to-float v10, v10

    .line 275
    aget-object v12, v9, v12

    .line 276
    .line 277
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    .line 278
    .line 279
    .line 280
    move-result v12

    .line 281
    mul-float v10, v10, v12

    .line 282
    .line 283
    add-float/2addr v3, v10

    .line 284
    :cond_6
    array-length v10, v9

    .line 285
    const/16 v12, 0x8

    .line 286
    .line 287
    if-lt v10, v12, :cond_7

    .line 288
    .line 289
    aget-object v10, v9, v11

    .line 290
    .line 291
    if-eqz v10, :cond_7

    .line 292
    .line 293
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 294
    .line 295
    int-to-float v8, v8

    .line 296
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 297
    .line 298
    .line 299
    move-result v10

    .line 300
    int-to-float v10, v10

    .line 301
    aget-object v9, v9, v11

    .line 302
    .line 303
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 304
    .line 305
    .line 306
    move-result v9

    .line 307
    mul-float v10, v10, v9

    .line 308
    .line 309
    add-float/2addr v8, v10

    .line 310
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 311
    .line 312
    .line 313
    move-result-object v9

    .line 314
    new-instance v10, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 315
    .line 316
    invoke-direct {v10}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 317
    .line 318
    .line 319
    new-instance v15, Landroid/graphics/Path;

    .line 320
    .line 321
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 322
    .line 323
    .line 324
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 325
    .line 326
    int-to-float v12, v11

    .line 327
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 328
    .line 329
    int-to-float v13, v11

    .line 330
    iget v11, v0, Landroid/graphics/Rect;->right:I

    .line 331
    .line 332
    int-to-float v14, v11

    .line 333
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 334
    .line 335
    int-to-float v0, v0

    .line 336
    sget-object v16, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 337
    .line 338
    move-object v11, v15

    .line 339
    move/from16 v17, v3

    .line 340
    .line 341
    move-object v3, v15

    .line 342
    move v15, v0

    .line 343
    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 344
    .line 345
    .line 346
    invoke-virtual {v10, v9}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 347
    .line 348
    .line 349
    invoke-virtual {v10, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 350
    .line 351
    .line 352
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 353
    .line 354
    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355
    .line 356
    .line 357
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 358
    .line 359
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 360
    .line 361
    .line 362
    new-instance v3, Landroid/graphics/Path;

    .line 363
    .line 364
    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 365
    .line 366
    .line 367
    invoke-virtual {v3, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 368
    .line 369
    .line 370
    invoke-virtual {v3, v5, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 371
    .line 372
    .line 373
    invoke-virtual {v3, v7, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 374
    .line 375
    .line 376
    move/from16 v1, v17

    .line 377
    .line 378
    invoke-virtual {v3, v1, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 379
    .line 380
    .line 381
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 382
    .line 383
    .line 384
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 385
    .line 386
    .line 387
    move-result-object v1

    .line 388
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 389
    .line 390
    .line 391
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 392
    .line 393
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    .line 395
    .line 396
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 397
    .line 398
    return-object v0
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getAccentBorderCallout1(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 10

    .line 1
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    int-to-float v1, v1

    .line 9
    const/high16 v2, 0x3e400000    # 0.1875f

    .line 10
    .line 11
    mul-float v1, v1, v2

    .line 12
    .line 13
    add-float/2addr v0, v1

    .line 14
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 15
    .line 16
    int-to-float v1, v1

    .line 17
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    int-to-float v2, v2

    .line 22
    const v3, -0x42555715    # -0.08333f

    .line 23
    .line 24
    .line 25
    mul-float v2, v2, v3

    .line 26
    .line 27
    add-float/2addr v1, v2

    .line 28
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 29
    .line 30
    int-to-float v2, v2

    .line 31
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    int-to-float v3, v3

    .line 36
    const/high16 v4, 0x3f900000    # 1.125f

    .line 37
    .line 38
    mul-float v3, v3, v4

    .line 39
    .line 40
    add-float/2addr v2, v3

    .line 41
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v3, v3

    .line 44
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    int-to-float v4, v4

    .line 49
    const v5, -0x413bbc2c

    .line 50
    .line 51
    .line 52
    mul-float v4, v4, v5

    .line 53
    .line 54
    add-float/2addr v3, v4

    .line 55
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 56
    .line 57
    .line 58
    move-result-object p0

    .line 59
    if-eqz p0, :cond_3

    .line 60
    .line 61
    array-length v4, p0

    .line 62
    const/4 v5, 0x4

    .line 63
    if-lt v4, v5, :cond_3

    .line 64
    .line 65
    const/4 v4, 0x0

    .line 66
    aget-object v5, p0, v4

    .line 67
    .line 68
    if-eqz v5, :cond_0

    .line 69
    .line 70
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 71
    .line 72
    int-to-float v0, v0

    .line 73
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 74
    .line 75
    .line 76
    move-result v5

    .line 77
    int-to-float v5, v5

    .line 78
    aget-object v4, p0, v4

    .line 79
    .line 80
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 81
    .line 82
    .line 83
    move-result v4

    .line 84
    mul-float v5, v5, v4

    .line 85
    .line 86
    add-float/2addr v0, v5

    .line 87
    :cond_0
    const/4 v4, 0x1

    .line 88
    aget-object v5, p0, v4

    .line 89
    .line 90
    if-eqz v5, :cond_1

    .line 91
    .line 92
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 93
    .line 94
    int-to-float v1, v1

    .line 95
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 96
    .line 97
    .line 98
    move-result v5

    .line 99
    int-to-float v5, v5

    .line 100
    aget-object v4, p0, v4

    .line 101
    .line 102
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 103
    .line 104
    .line 105
    move-result v4

    .line 106
    mul-float v5, v5, v4

    .line 107
    .line 108
    add-float/2addr v1, v5

    .line 109
    :cond_1
    const/4 v4, 0x2

    .line 110
    aget-object v5, p0, v4

    .line 111
    .line 112
    if-eqz v5, :cond_2

    .line 113
    .line 114
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 115
    .line 116
    int-to-float v2, v2

    .line 117
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 118
    .line 119
    .line 120
    move-result v5

    .line 121
    int-to-float v5, v5

    .line 122
    aget-object v4, p0, v4

    .line 123
    .line 124
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 125
    .line 126
    .line 127
    move-result v4

    .line 128
    mul-float v5, v5, v4

    .line 129
    .line 130
    add-float/2addr v2, v5

    .line 131
    :cond_2
    const/4 v4, 0x3

    .line 132
    aget-object v5, p0, v4

    .line 133
    .line 134
    if-eqz v5, :cond_3

    .line 135
    .line 136
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 137
    .line 138
    int-to-float v3, v3

    .line 139
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 140
    .line 141
    .line 142
    move-result v5

    .line 143
    int-to-float v5, v5

    .line 144
    aget-object p0, p0, v4

    .line 145
    .line 146
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 147
    .line 148
    .line 149
    move-result p0

    .line 150
    mul-float v5, v5, p0

    .line 151
    .line 152
    add-float/2addr v3, v5

    .line 153
    :cond_3
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 154
    .line 155
    iget p0, p1, Landroid/graphics/Rect;->left:I

    .line 156
    .line 157
    int-to-float v5, p0

    .line 158
    iget p0, p1, Landroid/graphics/Rect;->top:I

    .line 159
    .line 160
    int-to-float v6, p0

    .line 161
    iget p0, p1, Landroid/graphics/Rect;->right:I

    .line 162
    .line 163
    int-to-float v7, p0

    .line 164
    iget p0, p1, Landroid/graphics/Rect;->bottom:I

    .line 165
    .line 166
    int-to-float v8, p0

    .line 167
    sget-object v9, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 168
    .line 169
    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 170
    .line 171
    .line 172
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 173
    .line 174
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 175
    .line 176
    int-to-float v4, v4

    .line 177
    invoke-virtual {p0, v1, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 178
    .line 179
    .line 180
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 181
    .line 182
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 183
    .line 184
    int-to-float p1, p1

    .line 185
    invoke-virtual {p0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 186
    .line 187
    .line 188
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 189
    .line 190
    invoke-virtual {p0, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 191
    .line 192
    .line 193
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 194
    .line 195
    invoke-virtual {p0, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 196
    .line 197
    .line 198
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 199
    .line 200
    return-object p0
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getAccentBorderCallout2(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v5

    .line 37
    int-to-float v5, v5

    .line 38
    mul-float v5, v5, v3

    .line 39
    .line 40
    add-float/2addr v4, v5

    .line 41
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v3, v3

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v5

    .line 48
    int-to-float v5, v5

    .line 49
    const v6, -0x41d55476    # -0.16667f

    .line 50
    .line 51
    .line 52
    mul-float v5, v5, v6

    .line 53
    .line 54
    add-float/2addr v3, v5

    .line 55
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 56
    .line 57
    int-to-float v5, v5

    .line 58
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 59
    .line 60
    .line 61
    move-result v6

    .line 62
    int-to-float v6, v6

    .line 63
    const/high16 v7, 0x3f900000    # 1.125f

    .line 64
    .line 65
    mul-float v6, v6, v7

    .line 66
    .line 67
    add-float/2addr v5, v6

    .line 68
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 69
    .line 70
    int-to-float v6, v6

    .line 71
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 72
    .line 73
    .line 74
    move-result v7

    .line 75
    int-to-float v7, v7

    .line 76
    const v8, -0x411110a1    # -0.46667f

    .line 77
    .line 78
    .line 79
    mul-float v7, v7, v8

    .line 80
    .line 81
    add-float/2addr v6, v7

    .line 82
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 83
    .line 84
    .line 85
    move-result-object v7

    .line 86
    if-eqz v7, :cond_5

    .line 87
    .line 88
    array-length v8, v7

    .line 89
    const/4 v9, 0x6

    .line 90
    if-lt v8, v9, :cond_5

    .line 91
    .line 92
    const/4 v8, 0x0

    .line 93
    aget-object v9, v7, v8

    .line 94
    .line 95
    if-eqz v9, :cond_0

    .line 96
    .line 97
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 98
    .line 99
    int-to-float v1, v1

    .line 100
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 101
    .line 102
    .line 103
    move-result v9

    .line 104
    int-to-float v9, v9

    .line 105
    aget-object v8, v7, v8

    .line 106
    .line 107
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 108
    .line 109
    .line 110
    move-result v8

    .line 111
    mul-float v9, v9, v8

    .line 112
    .line 113
    add-float/2addr v1, v9

    .line 114
    :cond_0
    const/4 v8, 0x1

    .line 115
    aget-object v9, v7, v8

    .line 116
    .line 117
    if-eqz v9, :cond_1

    .line 118
    .line 119
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 120
    .line 121
    int-to-float v2, v2

    .line 122
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 123
    .line 124
    .line 125
    move-result v9

    .line 126
    int-to-float v9, v9

    .line 127
    aget-object v8, v7, v8

    .line 128
    .line 129
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 130
    .line 131
    .line 132
    move-result v8

    .line 133
    mul-float v9, v9, v8

    .line 134
    .line 135
    add-float/2addr v2, v9

    .line 136
    :cond_1
    const/4 v8, 0x2

    .line 137
    aget-object v9, v7, v8

    .line 138
    .line 139
    if-eqz v9, :cond_2

    .line 140
    .line 141
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 142
    .line 143
    int-to-float v4, v4

    .line 144
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 145
    .line 146
    .line 147
    move-result v9

    .line 148
    int-to-float v9, v9

    .line 149
    aget-object v8, v7, v8

    .line 150
    .line 151
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 152
    .line 153
    .line 154
    move-result v8

    .line 155
    mul-float v9, v9, v8

    .line 156
    .line 157
    add-float/2addr v4, v9

    .line 158
    :cond_2
    const/4 v8, 0x3

    .line 159
    aget-object v9, v7, v8

    .line 160
    .line 161
    if-eqz v9, :cond_3

    .line 162
    .line 163
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 164
    .line 165
    int-to-float v3, v3

    .line 166
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 167
    .line 168
    .line 169
    move-result v9

    .line 170
    int-to-float v9, v9

    .line 171
    aget-object v8, v7, v8

    .line 172
    .line 173
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 174
    .line 175
    .line 176
    move-result v8

    .line 177
    mul-float v9, v9, v8

    .line 178
    .line 179
    add-float/2addr v3, v9

    .line 180
    :cond_3
    const/4 v8, 0x4

    .line 181
    aget-object v9, v7, v8

    .line 182
    .line 183
    if-eqz v9, :cond_4

    .line 184
    .line 185
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 186
    .line 187
    int-to-float v5, v5

    .line 188
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 189
    .line 190
    .line 191
    move-result v9

    .line 192
    int-to-float v9, v9

    .line 193
    aget-object v8, v7, v8

    .line 194
    .line 195
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 196
    .line 197
    .line 198
    move-result v8

    .line 199
    mul-float v9, v9, v8

    .line 200
    .line 201
    add-float/2addr v5, v9

    .line 202
    :cond_4
    const/4 v8, 0x5

    .line 203
    aget-object v9, v7, v8

    .line 204
    .line 205
    if-eqz v9, :cond_5

    .line 206
    .line 207
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 208
    .line 209
    int-to-float v6, v6

    .line 210
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 211
    .line 212
    .line 213
    move-result v9

    .line 214
    int-to-float v9, v9

    .line 215
    aget-object v7, v7, v8

    .line 216
    .line 217
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 218
    .line 219
    .line 220
    move-result v7

    .line 221
    mul-float v9, v9, v7

    .line 222
    .line 223
    add-float/2addr v6, v9

    .line 224
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 225
    .line 226
    .line 227
    move-result-object v7

    .line 228
    new-instance v8, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 229
    .line 230
    invoke-direct {v8}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 231
    .line 232
    .line 233
    new-instance v15, Landroid/graphics/Path;

    .line 234
    .line 235
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 236
    .line 237
    .line 238
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 239
    .line 240
    int-to-float v10, v9

    .line 241
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 242
    .line 243
    int-to-float v11, v9

    .line 244
    iget v9, v0, Landroid/graphics/Rect;->right:I

    .line 245
    .line 246
    int-to-float v12, v9

    .line 247
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 248
    .line 249
    int-to-float v13, v9

    .line 250
    sget-object v14, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 251
    .line 252
    move-object v9, v15

    .line 253
    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 254
    .line 255
    .line 256
    invoke-virtual {v8, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 257
    .line 258
    .line 259
    invoke-virtual {v8, v15}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 260
    .line 261
    .line 262
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 263
    .line 264
    .line 265
    move-result-object v7

    .line 266
    invoke-virtual {v8, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 267
    .line 268
    .line 269
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 270
    .line 271
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    .line 273
    .line 274
    new-instance v7, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 275
    .line 276
    invoke-direct {v7}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 277
    .line 278
    .line 279
    new-instance v8, Landroid/graphics/Path;

    .line 280
    .line 281
    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    .line 282
    .line 283
    .line 284
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 285
    .line 286
    int-to-float v9, v9

    .line 287
    invoke-virtual {v8, v2, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 288
    .line 289
    .line 290
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 291
    .line 292
    int-to-float v0, v0

    .line 293
    invoke-virtual {v8, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 294
    .line 295
    .line 296
    invoke-virtual {v8, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 297
    .line 298
    .line 299
    invoke-virtual {v8, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 300
    .line 301
    .line 302
    invoke-virtual {v8, v6, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 303
    .line 304
    .line 305
    invoke-virtual {v7, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 306
    .line 307
    .line 308
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 309
    .line 310
    .line 311
    move-result-object v0

    .line 312
    invoke-virtual {v7, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 313
    .line 314
    .line 315
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 316
    .line 317
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318
    .line 319
    .line 320
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 321
    .line 322
    return-object v0
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getAccentBorderCallout3(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v6

    .line 37
    int-to-float v6, v6

    .line 38
    mul-float v6, v6, v3

    .line 39
    .line 40
    add-float/2addr v4, v6

    .line 41
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v3, v3

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v6

    .line 48
    int-to-float v6, v6

    .line 49
    const v7, -0x41d55476    # -0.16667f

    .line 50
    .line 51
    .line 52
    mul-float v6, v6, v7

    .line 53
    .line 54
    add-float/2addr v3, v6

    .line 55
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 56
    .line 57
    int-to-float v6, v6

    .line 58
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 59
    .line 60
    .line 61
    move-result v8

    .line 62
    int-to-float v8, v8

    .line 63
    const/high16 v9, 0x3f800000    # 1.0f

    .line 64
    .line 65
    mul-float v8, v8, v9

    .line 66
    .line 67
    add-float/2addr v6, v8

    .line 68
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 69
    .line 70
    int-to-float v8, v8

    .line 71
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 72
    .line 73
    .line 74
    move-result v9

    .line 75
    int-to-float v9, v9

    .line 76
    mul-float v9, v9, v7

    .line 77
    .line 78
    add-float/2addr v8, v9

    .line 79
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 80
    .line 81
    int-to-float v7, v7

    .line 82
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 83
    .line 84
    .line 85
    move-result v9

    .line 86
    int-to-float v9, v9

    .line 87
    const v10, 0x3f9097b7

    .line 88
    .line 89
    .line 90
    mul-float v9, v9, v10

    .line 91
    .line 92
    add-float/2addr v7, v9

    .line 93
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 94
    .line 95
    int-to-float v9, v9

    .line 96
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 97
    .line 98
    .line 99
    move-result v10

    .line 100
    int-to-float v10, v10

    .line 101
    mul-float v10, v10, v5

    .line 102
    .line 103
    add-float/2addr v9, v10

    .line 104
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 105
    .line 106
    .line 107
    move-result-object v5

    .line 108
    if-eqz v5, :cond_7

    .line 109
    .line 110
    array-length v10, v5

    .line 111
    const/16 v11, 0x8

    .line 112
    .line 113
    if-lt v10, v11, :cond_7

    .line 114
    .line 115
    const/4 v10, 0x0

    .line 116
    aget-object v11, v5, v10

    .line 117
    .line 118
    if-eqz v11, :cond_0

    .line 119
    .line 120
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 121
    .line 122
    int-to-float v1, v1

    .line 123
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 124
    .line 125
    .line 126
    move-result v11

    .line 127
    int-to-float v11, v11

    .line 128
    aget-object v10, v5, v10

    .line 129
    .line 130
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 131
    .line 132
    .line 133
    move-result v10

    .line 134
    mul-float v11, v11, v10

    .line 135
    .line 136
    add-float/2addr v1, v11

    .line 137
    :cond_0
    const/4 v10, 0x1

    .line 138
    aget-object v11, v5, v10

    .line 139
    .line 140
    if-eqz v11, :cond_1

    .line 141
    .line 142
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 143
    .line 144
    int-to-float v2, v2

    .line 145
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 146
    .line 147
    .line 148
    move-result v11

    .line 149
    int-to-float v11, v11

    .line 150
    aget-object v10, v5, v10

    .line 151
    .line 152
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 153
    .line 154
    .line 155
    move-result v10

    .line 156
    mul-float v11, v11, v10

    .line 157
    .line 158
    add-float/2addr v2, v11

    .line 159
    :cond_1
    const/4 v10, 0x2

    .line 160
    aget-object v11, v5, v10

    .line 161
    .line 162
    if-eqz v11, :cond_2

    .line 163
    .line 164
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 165
    .line 166
    int-to-float v4, v4

    .line 167
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 168
    .line 169
    .line 170
    move-result v11

    .line 171
    int-to-float v11, v11

    .line 172
    aget-object v10, v5, v10

    .line 173
    .line 174
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 175
    .line 176
    .line 177
    move-result v10

    .line 178
    mul-float v11, v11, v10

    .line 179
    .line 180
    add-float/2addr v4, v11

    .line 181
    :cond_2
    const/4 v10, 0x3

    .line 182
    aget-object v11, v5, v10

    .line 183
    .line 184
    if-eqz v11, :cond_3

    .line 185
    .line 186
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 187
    .line 188
    int-to-float v3, v3

    .line 189
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 190
    .line 191
    .line 192
    move-result v11

    .line 193
    int-to-float v11, v11

    .line 194
    aget-object v10, v5, v10

    .line 195
    .line 196
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 197
    .line 198
    .line 199
    move-result v10

    .line 200
    mul-float v11, v11, v10

    .line 201
    .line 202
    add-float/2addr v3, v11

    .line 203
    :cond_3
    const/4 v10, 0x4

    .line 204
    aget-object v11, v5, v10

    .line 205
    .line 206
    if-eqz v11, :cond_4

    .line 207
    .line 208
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 209
    .line 210
    int-to-float v6, v6

    .line 211
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 212
    .line 213
    .line 214
    move-result v11

    .line 215
    int-to-float v11, v11

    .line 216
    aget-object v10, v5, v10

    .line 217
    .line 218
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 219
    .line 220
    .line 221
    move-result v10

    .line 222
    mul-float v11, v11, v10

    .line 223
    .line 224
    add-float/2addr v6, v11

    .line 225
    :cond_4
    const/4 v10, 0x5

    .line 226
    aget-object v11, v5, v10

    .line 227
    .line 228
    if-eqz v11, :cond_5

    .line 229
    .line 230
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 231
    .line 232
    int-to-float v8, v8

    .line 233
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 234
    .line 235
    .line 236
    move-result v11

    .line 237
    int-to-float v11, v11

    .line 238
    aget-object v10, v5, v10

    .line 239
    .line 240
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 241
    .line 242
    .line 243
    move-result v10

    .line 244
    mul-float v11, v11, v10

    .line 245
    .line 246
    add-float/2addr v8, v11

    .line 247
    :cond_5
    const/4 v10, 0x6

    .line 248
    aget-object v11, v5, v10

    .line 249
    .line 250
    if-eqz v11, :cond_6

    .line 251
    .line 252
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 253
    .line 254
    int-to-float v7, v7

    .line 255
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 256
    .line 257
    .line 258
    move-result v11

    .line 259
    int-to-float v11, v11

    .line 260
    aget-object v10, v5, v10

    .line 261
    .line 262
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 263
    .line 264
    .line 265
    move-result v10

    .line 266
    mul-float v11, v11, v10

    .line 267
    .line 268
    add-float/2addr v7, v11

    .line 269
    :cond_6
    const/4 v10, 0x7

    .line 270
    aget-object v11, v5, v10

    .line 271
    .line 272
    if-eqz v11, :cond_7

    .line 273
    .line 274
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 275
    .line 276
    int-to-float v9, v9

    .line 277
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 278
    .line 279
    .line 280
    move-result v11

    .line 281
    int-to-float v11, v11

    .line 282
    aget-object v5, v5, v10

    .line 283
    .line 284
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 285
    .line 286
    .line 287
    move-result v5

    .line 288
    mul-float v11, v11, v5

    .line 289
    .line 290
    add-float/2addr v9, v11

    .line 291
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 292
    .line 293
    .line 294
    move-result-object v5

    .line 295
    new-instance v10, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 296
    .line 297
    invoke-direct {v10}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 298
    .line 299
    .line 300
    new-instance v15, Landroid/graphics/Path;

    .line 301
    .line 302
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 303
    .line 304
    .line 305
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 306
    .line 307
    int-to-float v12, v11

    .line 308
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 309
    .line 310
    int-to-float v13, v11

    .line 311
    iget v11, v0, Landroid/graphics/Rect;->right:I

    .line 312
    .line 313
    int-to-float v14, v11

    .line 314
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 315
    .line 316
    int-to-float v11, v11

    .line 317
    sget-object v16, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 318
    .line 319
    move/from16 v17, v11

    .line 320
    .line 321
    move-object v11, v15

    .line 322
    move/from16 v18, v7

    .line 323
    .line 324
    move-object v7, v15

    .line 325
    move/from16 v15, v17

    .line 326
    .line 327
    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 328
    .line 329
    .line 330
    invoke-virtual {v10, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 331
    .line 332
    .line 333
    invoke-virtual {v10, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 334
    .line 335
    .line 336
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 337
    .line 338
    .line 339
    move-result-object v5

    .line 340
    invoke-virtual {v10, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 341
    .line 342
    .line 343
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 344
    .line 345
    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346
    .line 347
    .line 348
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 349
    .line 350
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 351
    .line 352
    .line 353
    new-instance v7, Landroid/graphics/Path;

    .line 354
    .line 355
    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 356
    .line 357
    .line 358
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 359
    .line 360
    int-to-float v10, v10

    .line 361
    invoke-virtual {v7, v2, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 362
    .line 363
    .line 364
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 365
    .line 366
    int-to-float v0, v0

    .line 367
    invoke-virtual {v7, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 368
    .line 369
    .line 370
    invoke-virtual {v7, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 371
    .line 372
    .line 373
    invoke-virtual {v7, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 374
    .line 375
    .line 376
    invoke-virtual {v7, v8, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 377
    .line 378
    .line 379
    move/from16 v0, v18

    .line 380
    .line 381
    invoke-virtual {v7, v9, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 382
    .line 383
    .line 384
    invoke-virtual {v5, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 385
    .line 386
    .line 387
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 388
    .line 389
    .line 390
    move-result-object v0

    .line 391
    invoke-virtual {v5, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 392
    .line 393
    .line 394
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 395
    .line 396
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 397
    .line 398
    .line 399
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 400
    .line 401
    return-object v0
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getAccentCallout1Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    int-to-float v1, v1

    .line 9
    const/high16 v2, 0x3e400000    # 0.1875f

    .line 10
    .line 11
    mul-float v1, v1, v2

    .line 12
    .line 13
    add-float/2addr v0, v1

    .line 14
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 15
    .line 16
    int-to-float v1, v1

    .line 17
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    int-to-float v2, v2

    .line 22
    const v3, -0x42555715    # -0.08333f

    .line 23
    .line 24
    .line 25
    mul-float v2, v2, v3

    .line 26
    .line 27
    add-float/2addr v1, v2

    .line 28
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 29
    .line 30
    int-to-float v2, v2

    .line 31
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    int-to-float v3, v3

    .line 36
    const/high16 v4, 0x3f900000    # 1.125f

    .line 37
    .line 38
    mul-float v3, v3, v4

    .line 39
    .line 40
    add-float/2addr v2, v3

    .line 41
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v3, v3

    .line 44
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    int-to-float v4, v4

    .line 49
    const v5, -0x413bbc2c

    .line 50
    .line 51
    .line 52
    mul-float v4, v4, v5

    .line 53
    .line 54
    add-float/2addr v3, v4

    .line 55
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 56
    .line 57
    .line 58
    move-result-object v4

    .line 59
    if-eqz v4, :cond_3

    .line 60
    .line 61
    array-length v5, v4

    .line 62
    const/4 v6, 0x4

    .line 63
    if-lt v5, v6, :cond_3

    .line 64
    .line 65
    const/4 v5, 0x0

    .line 66
    aget-object v6, v4, v5

    .line 67
    .line 68
    if-eqz v6, :cond_0

    .line 69
    .line 70
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 71
    .line 72
    int-to-float v0, v0

    .line 73
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 74
    .line 75
    .line 76
    move-result v6

    .line 77
    int-to-float v6, v6

    .line 78
    aget-object v5, v4, v5

    .line 79
    .line 80
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 81
    .line 82
    .line 83
    move-result v5

    .line 84
    mul-float v6, v6, v5

    .line 85
    .line 86
    add-float/2addr v0, v6

    .line 87
    :cond_0
    const/4 v5, 0x1

    .line 88
    aget-object v6, v4, v5

    .line 89
    .line 90
    if-eqz v6, :cond_1

    .line 91
    .line 92
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 93
    .line 94
    int-to-float v1, v1

    .line 95
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 96
    .line 97
    .line 98
    move-result v6

    .line 99
    int-to-float v6, v6

    .line 100
    aget-object v5, v4, v5

    .line 101
    .line 102
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 103
    .line 104
    .line 105
    move-result v5

    .line 106
    mul-float v6, v6, v5

    .line 107
    .line 108
    add-float/2addr v1, v6

    .line 109
    :cond_1
    const/4 v5, 0x2

    .line 110
    aget-object v6, v4, v5

    .line 111
    .line 112
    if-eqz v6, :cond_2

    .line 113
    .line 114
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 115
    .line 116
    int-to-float v2, v2

    .line 117
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 118
    .line 119
    .line 120
    move-result v6

    .line 121
    int-to-float v6, v6

    .line 122
    aget-object v5, v4, v5

    .line 123
    .line 124
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 125
    .line 126
    .line 127
    move-result v5

    .line 128
    mul-float v6, v6, v5

    .line 129
    .line 130
    add-float/2addr v2, v6

    .line 131
    :cond_2
    const/4 v5, 0x3

    .line 132
    aget-object v6, v4, v5

    .line 133
    .line 134
    if-eqz v6, :cond_3

    .line 135
    .line 136
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 137
    .line 138
    int-to-float v3, v3

    .line 139
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 140
    .line 141
    .line 142
    move-result v6

    .line 143
    int-to-float v6, v6

    .line 144
    aget-object v4, v4, v5

    .line 145
    .line 146
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 147
    .line 148
    .line 149
    move-result v4

    .line 150
    mul-float v6, v6, v4

    .line 151
    .line 152
    add-float/2addr v3, v6

    .line 153
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 154
    .line 155
    .line 156
    move-result-object v4

    .line 157
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 158
    .line 159
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 160
    .line 161
    .line 162
    new-instance v12, Landroid/graphics/Path;

    .line 163
    .line 164
    invoke-direct {v12}, Landroid/graphics/Path;-><init>()V

    .line 165
    .line 166
    .line 167
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 168
    .line 169
    int-to-float v7, v6

    .line 170
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 171
    .line 172
    int-to-float v8, v6

    .line 173
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 174
    .line 175
    int-to-float v9, v6

    .line 176
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 177
    .line 178
    int-to-float v10, v6

    .line 179
    sget-object v11, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 180
    .line 181
    move-object v6, v12

    .line 182
    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 183
    .line 184
    .line 185
    invoke-virtual {v5, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 186
    .line 187
    .line 188
    invoke-virtual {v5, v12}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 189
    .line 190
    .line 191
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 192
    .line 193
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    .line 195
    .line 196
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 197
    .line 198
    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 199
    .line 200
    .line 201
    new-instance v5, Landroid/graphics/Path;

    .line 202
    .line 203
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 204
    .line 205
    .line 206
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 207
    .line 208
    int-to-float v6, v6

    .line 209
    invoke-virtual {v5, v1, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 210
    .line 211
    .line 212
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 213
    .line 214
    int-to-float p1, p1

    .line 215
    invoke-virtual {v5, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 216
    .line 217
    .line 218
    invoke-virtual {v5, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 219
    .line 220
    .line 221
    invoke-virtual {v5, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 222
    .line 223
    .line 224
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 225
    .line 226
    .line 227
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 228
    .line 229
    .line 230
    move-result-object p0

    .line 231
    invoke-virtual {v4, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 232
    .line 233
    .line 234
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 235
    .line 236
    invoke-interface {p0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    .line 238
    .line 239
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 240
    .line 241
    return-object p0
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getAccentCallout2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v5

    .line 37
    int-to-float v5, v5

    .line 38
    mul-float v5, v5, v3

    .line 39
    .line 40
    add-float/2addr v4, v5

    .line 41
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v3, v3

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v5

    .line 48
    int-to-float v5, v5

    .line 49
    const v6, -0x41d55476    # -0.16667f

    .line 50
    .line 51
    .line 52
    mul-float v5, v5, v6

    .line 53
    .line 54
    add-float/2addr v3, v5

    .line 55
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 56
    .line 57
    int-to-float v5, v5

    .line 58
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 59
    .line 60
    .line 61
    move-result v6

    .line 62
    int-to-float v6, v6

    .line 63
    const/high16 v7, 0x3f900000    # 1.125f

    .line 64
    .line 65
    mul-float v6, v6, v7

    .line 66
    .line 67
    add-float/2addr v5, v6

    .line 68
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 69
    .line 70
    int-to-float v6, v6

    .line 71
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 72
    .line 73
    .line 74
    move-result v7

    .line 75
    int-to-float v7, v7

    .line 76
    const v8, -0x411110a1    # -0.46667f

    .line 77
    .line 78
    .line 79
    mul-float v7, v7, v8

    .line 80
    .line 81
    add-float/2addr v6, v7

    .line 82
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 83
    .line 84
    .line 85
    move-result-object v7

    .line 86
    if-eqz v7, :cond_5

    .line 87
    .line 88
    array-length v8, v7

    .line 89
    const/4 v9, 0x6

    .line 90
    if-lt v8, v9, :cond_5

    .line 91
    .line 92
    const/4 v8, 0x0

    .line 93
    aget-object v9, v7, v8

    .line 94
    .line 95
    if-eqz v9, :cond_0

    .line 96
    .line 97
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 98
    .line 99
    int-to-float v1, v1

    .line 100
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 101
    .line 102
    .line 103
    move-result v9

    .line 104
    int-to-float v9, v9

    .line 105
    aget-object v8, v7, v8

    .line 106
    .line 107
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 108
    .line 109
    .line 110
    move-result v8

    .line 111
    mul-float v9, v9, v8

    .line 112
    .line 113
    add-float/2addr v1, v9

    .line 114
    :cond_0
    const/4 v8, 0x1

    .line 115
    aget-object v9, v7, v8

    .line 116
    .line 117
    if-eqz v9, :cond_1

    .line 118
    .line 119
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 120
    .line 121
    int-to-float v2, v2

    .line 122
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 123
    .line 124
    .line 125
    move-result v9

    .line 126
    int-to-float v9, v9

    .line 127
    aget-object v8, v7, v8

    .line 128
    .line 129
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 130
    .line 131
    .line 132
    move-result v8

    .line 133
    mul-float v9, v9, v8

    .line 134
    .line 135
    add-float/2addr v2, v9

    .line 136
    :cond_1
    const/4 v8, 0x2

    .line 137
    aget-object v9, v7, v8

    .line 138
    .line 139
    if-eqz v9, :cond_2

    .line 140
    .line 141
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 142
    .line 143
    int-to-float v4, v4

    .line 144
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 145
    .line 146
    .line 147
    move-result v9

    .line 148
    int-to-float v9, v9

    .line 149
    aget-object v8, v7, v8

    .line 150
    .line 151
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 152
    .line 153
    .line 154
    move-result v8

    .line 155
    mul-float v9, v9, v8

    .line 156
    .line 157
    add-float/2addr v4, v9

    .line 158
    :cond_2
    const/4 v8, 0x3

    .line 159
    aget-object v9, v7, v8

    .line 160
    .line 161
    if-eqz v9, :cond_3

    .line 162
    .line 163
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 164
    .line 165
    int-to-float v3, v3

    .line 166
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 167
    .line 168
    .line 169
    move-result v9

    .line 170
    int-to-float v9, v9

    .line 171
    aget-object v8, v7, v8

    .line 172
    .line 173
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 174
    .line 175
    .line 176
    move-result v8

    .line 177
    mul-float v9, v9, v8

    .line 178
    .line 179
    add-float/2addr v3, v9

    .line 180
    :cond_3
    const/4 v8, 0x4

    .line 181
    aget-object v9, v7, v8

    .line 182
    .line 183
    if-eqz v9, :cond_4

    .line 184
    .line 185
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 186
    .line 187
    int-to-float v5, v5

    .line 188
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 189
    .line 190
    .line 191
    move-result v9

    .line 192
    int-to-float v9, v9

    .line 193
    aget-object v8, v7, v8

    .line 194
    .line 195
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 196
    .line 197
    .line 198
    move-result v8

    .line 199
    mul-float v9, v9, v8

    .line 200
    .line 201
    add-float/2addr v5, v9

    .line 202
    :cond_4
    const/4 v8, 0x5

    .line 203
    aget-object v9, v7, v8

    .line 204
    .line 205
    if-eqz v9, :cond_5

    .line 206
    .line 207
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 208
    .line 209
    int-to-float v6, v6

    .line 210
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 211
    .line 212
    .line 213
    move-result v9

    .line 214
    int-to-float v9, v9

    .line 215
    aget-object v7, v7, v8

    .line 216
    .line 217
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 218
    .line 219
    .line 220
    move-result v7

    .line 221
    mul-float v9, v9, v7

    .line 222
    .line 223
    add-float/2addr v6, v9

    .line 224
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 225
    .line 226
    .line 227
    move-result-object v7

    .line 228
    new-instance v8, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 229
    .line 230
    invoke-direct {v8}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 231
    .line 232
    .line 233
    new-instance v15, Landroid/graphics/Path;

    .line 234
    .line 235
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 236
    .line 237
    .line 238
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 239
    .line 240
    int-to-float v10, v9

    .line 241
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 242
    .line 243
    int-to-float v11, v9

    .line 244
    iget v9, v0, Landroid/graphics/Rect;->right:I

    .line 245
    .line 246
    int-to-float v12, v9

    .line 247
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 248
    .line 249
    int-to-float v13, v9

    .line 250
    sget-object v14, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 251
    .line 252
    move-object v9, v15

    .line 253
    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 254
    .line 255
    .line 256
    invoke-virtual {v8, v15}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 257
    .line 258
    .line 259
    invoke-virtual {v8, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 260
    .line 261
    .line 262
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 263
    .line 264
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    .line 266
    .line 267
    new-instance v7, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 268
    .line 269
    invoke-direct {v7}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 270
    .line 271
    .line 272
    new-instance v8, Landroid/graphics/Path;

    .line 273
    .line 274
    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    .line 275
    .line 276
    .line 277
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 278
    .line 279
    int-to-float v9, v9

    .line 280
    invoke-virtual {v8, v2, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 281
    .line 282
    .line 283
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 284
    .line 285
    int-to-float v0, v0

    .line 286
    invoke-virtual {v8, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 287
    .line 288
    .line 289
    invoke-virtual {v8, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 290
    .line 291
    .line 292
    invoke-virtual {v8, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 293
    .line 294
    .line 295
    invoke-virtual {v8, v6, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 296
    .line 297
    .line 298
    invoke-virtual {v7, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 299
    .line 300
    .line 301
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 302
    .line 303
    .line 304
    move-result-object v0

    .line 305
    invoke-virtual {v7, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 306
    .line 307
    .line 308
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 309
    .line 310
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 311
    .line 312
    .line 313
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 314
    .line 315
    return-object v0
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getAccentCallout3Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v6

    .line 37
    int-to-float v6, v6

    .line 38
    mul-float v6, v6, v3

    .line 39
    .line 40
    add-float/2addr v4, v6

    .line 41
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v3, v3

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v6

    .line 48
    int-to-float v6, v6

    .line 49
    const v7, -0x41d55476    # -0.16667f

    .line 50
    .line 51
    .line 52
    mul-float v6, v6, v7

    .line 53
    .line 54
    add-float/2addr v3, v6

    .line 55
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 56
    .line 57
    int-to-float v6, v6

    .line 58
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 59
    .line 60
    .line 61
    move-result v8

    .line 62
    int-to-float v8, v8

    .line 63
    const/high16 v9, 0x3f800000    # 1.0f

    .line 64
    .line 65
    mul-float v8, v8, v9

    .line 66
    .line 67
    add-float/2addr v6, v8

    .line 68
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 69
    .line 70
    int-to-float v8, v8

    .line 71
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 72
    .line 73
    .line 74
    move-result v9

    .line 75
    int-to-float v9, v9

    .line 76
    mul-float v9, v9, v7

    .line 77
    .line 78
    add-float/2addr v8, v9

    .line 79
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 80
    .line 81
    int-to-float v7, v7

    .line 82
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 83
    .line 84
    .line 85
    move-result v9

    .line 86
    int-to-float v9, v9

    .line 87
    const v10, 0x3f9097b7

    .line 88
    .line 89
    .line 90
    mul-float v9, v9, v10

    .line 91
    .line 92
    add-float/2addr v7, v9

    .line 93
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 94
    .line 95
    int-to-float v9, v9

    .line 96
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 97
    .line 98
    .line 99
    move-result v10

    .line 100
    int-to-float v10, v10

    .line 101
    mul-float v10, v10, v5

    .line 102
    .line 103
    add-float/2addr v9, v10

    .line 104
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 105
    .line 106
    .line 107
    move-result-object v5

    .line 108
    if-eqz v5, :cond_7

    .line 109
    .line 110
    array-length v10, v5

    .line 111
    const/16 v11, 0x8

    .line 112
    .line 113
    if-lt v10, v11, :cond_7

    .line 114
    .line 115
    const/4 v10, 0x0

    .line 116
    aget-object v11, v5, v10

    .line 117
    .line 118
    if-eqz v11, :cond_0

    .line 119
    .line 120
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 121
    .line 122
    int-to-float v1, v1

    .line 123
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 124
    .line 125
    .line 126
    move-result v11

    .line 127
    int-to-float v11, v11

    .line 128
    aget-object v10, v5, v10

    .line 129
    .line 130
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 131
    .line 132
    .line 133
    move-result v10

    .line 134
    mul-float v11, v11, v10

    .line 135
    .line 136
    add-float/2addr v1, v11

    .line 137
    :cond_0
    const/4 v10, 0x1

    .line 138
    aget-object v11, v5, v10

    .line 139
    .line 140
    if-eqz v11, :cond_1

    .line 141
    .line 142
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 143
    .line 144
    int-to-float v2, v2

    .line 145
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 146
    .line 147
    .line 148
    move-result v11

    .line 149
    int-to-float v11, v11

    .line 150
    aget-object v10, v5, v10

    .line 151
    .line 152
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 153
    .line 154
    .line 155
    move-result v10

    .line 156
    mul-float v11, v11, v10

    .line 157
    .line 158
    add-float/2addr v2, v11

    .line 159
    :cond_1
    const/4 v10, 0x2

    .line 160
    aget-object v11, v5, v10

    .line 161
    .line 162
    if-eqz v11, :cond_2

    .line 163
    .line 164
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 165
    .line 166
    int-to-float v4, v4

    .line 167
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 168
    .line 169
    .line 170
    move-result v11

    .line 171
    int-to-float v11, v11

    .line 172
    aget-object v10, v5, v10

    .line 173
    .line 174
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 175
    .line 176
    .line 177
    move-result v10

    .line 178
    mul-float v11, v11, v10

    .line 179
    .line 180
    add-float/2addr v4, v11

    .line 181
    :cond_2
    const/4 v10, 0x3

    .line 182
    aget-object v11, v5, v10

    .line 183
    .line 184
    if-eqz v11, :cond_3

    .line 185
    .line 186
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 187
    .line 188
    int-to-float v3, v3

    .line 189
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 190
    .line 191
    .line 192
    move-result v11

    .line 193
    int-to-float v11, v11

    .line 194
    aget-object v10, v5, v10

    .line 195
    .line 196
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 197
    .line 198
    .line 199
    move-result v10

    .line 200
    mul-float v11, v11, v10

    .line 201
    .line 202
    add-float/2addr v3, v11

    .line 203
    :cond_3
    const/4 v10, 0x4

    .line 204
    aget-object v11, v5, v10

    .line 205
    .line 206
    if-eqz v11, :cond_4

    .line 207
    .line 208
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 209
    .line 210
    int-to-float v6, v6

    .line 211
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 212
    .line 213
    .line 214
    move-result v11

    .line 215
    int-to-float v11, v11

    .line 216
    aget-object v10, v5, v10

    .line 217
    .line 218
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 219
    .line 220
    .line 221
    move-result v10

    .line 222
    mul-float v11, v11, v10

    .line 223
    .line 224
    add-float/2addr v6, v11

    .line 225
    :cond_4
    const/4 v10, 0x5

    .line 226
    aget-object v11, v5, v10

    .line 227
    .line 228
    if-eqz v11, :cond_5

    .line 229
    .line 230
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 231
    .line 232
    int-to-float v8, v8

    .line 233
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 234
    .line 235
    .line 236
    move-result v11

    .line 237
    int-to-float v11, v11

    .line 238
    aget-object v10, v5, v10

    .line 239
    .line 240
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 241
    .line 242
    .line 243
    move-result v10

    .line 244
    mul-float v11, v11, v10

    .line 245
    .line 246
    add-float/2addr v8, v11

    .line 247
    :cond_5
    const/4 v10, 0x6

    .line 248
    aget-object v11, v5, v10

    .line 249
    .line 250
    if-eqz v11, :cond_6

    .line 251
    .line 252
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 253
    .line 254
    int-to-float v7, v7

    .line 255
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 256
    .line 257
    .line 258
    move-result v11

    .line 259
    int-to-float v11, v11

    .line 260
    aget-object v10, v5, v10

    .line 261
    .line 262
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 263
    .line 264
    .line 265
    move-result v10

    .line 266
    mul-float v11, v11, v10

    .line 267
    .line 268
    add-float/2addr v7, v11

    .line 269
    :cond_6
    const/4 v10, 0x7

    .line 270
    aget-object v11, v5, v10

    .line 271
    .line 272
    if-eqz v11, :cond_7

    .line 273
    .line 274
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 275
    .line 276
    int-to-float v9, v9

    .line 277
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 278
    .line 279
    .line 280
    move-result v11

    .line 281
    int-to-float v11, v11

    .line 282
    aget-object v5, v5, v10

    .line 283
    .line 284
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 285
    .line 286
    .line 287
    move-result v5

    .line 288
    mul-float v11, v11, v5

    .line 289
    .line 290
    add-float/2addr v9, v11

    .line 291
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 292
    .line 293
    .line 294
    move-result-object v5

    .line 295
    new-instance v10, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 296
    .line 297
    invoke-direct {v10}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 298
    .line 299
    .line 300
    new-instance v15, Landroid/graphics/Path;

    .line 301
    .line 302
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 303
    .line 304
    .line 305
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 306
    .line 307
    int-to-float v12, v11

    .line 308
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 309
    .line 310
    int-to-float v13, v11

    .line 311
    iget v11, v0, Landroid/graphics/Rect;->right:I

    .line 312
    .line 313
    int-to-float v14, v11

    .line 314
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 315
    .line 316
    int-to-float v11, v11

    .line 317
    sget-object v16, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 318
    .line 319
    move/from16 v17, v11

    .line 320
    .line 321
    move-object v11, v15

    .line 322
    move/from16 v18, v7

    .line 323
    .line 324
    move-object v7, v15

    .line 325
    move/from16 v15, v17

    .line 326
    .line 327
    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 328
    .line 329
    .line 330
    invoke-virtual {v10, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 331
    .line 332
    .line 333
    invoke-virtual {v10, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 334
    .line 335
    .line 336
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 337
    .line 338
    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 339
    .line 340
    .line 341
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 342
    .line 343
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 344
    .line 345
    .line 346
    new-instance v7, Landroid/graphics/Path;

    .line 347
    .line 348
    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 349
    .line 350
    .line 351
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 352
    .line 353
    int-to-float v10, v10

    .line 354
    invoke-virtual {v7, v2, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 355
    .line 356
    .line 357
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 358
    .line 359
    int-to-float v0, v0

    .line 360
    invoke-virtual {v7, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 361
    .line 362
    .line 363
    invoke-virtual {v7, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 364
    .line 365
    .line 366
    invoke-virtual {v7, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 367
    .line 368
    .line 369
    invoke-virtual {v7, v8, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 370
    .line 371
    .line 372
    move/from16 v0, v18

    .line 373
    .line 374
    invoke-virtual {v7, v9, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 375
    .line 376
    .line 377
    invoke-virtual {v5, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 378
    .line 379
    .line 380
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 381
    .line 382
    .line 383
    move-result-object v0

    .line 384
    invoke-virtual {v5, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 385
    .line 386
    .line 387
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 388
    .line 389
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    .line 391
    .line 392
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 393
    .line 394
    return-object v0
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getAngle(DD)D
    .locals 4

    .line 1
    mul-double v0, p0, p0

    .line 2
    .line 3
    mul-double v2, p2, p2

    .line 4
    .line 5
    add-double/2addr v0, v2

    .line 6
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    .line 7
    .line 8
    .line 9
    move-result-wide v0

    .line 10
    div-double/2addr p0, v0

    .line 11
    invoke-static {p0, p1}, Ljava/lang/Math;->acos(D)D

    .line 12
    .line 13
    .line 14
    move-result-wide p0

    .line 15
    const-wide v0, 0x4066800000000000L    # 180.0

    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    mul-double p0, p0, v0

    .line 21
    .line 22
    const-wide v0, 0x400921fb54442d18L    # Math.PI

    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    div-double/2addr p0, v0

    .line 28
    const-wide/16 v0, 0x0

    .line 29
    .line 30
    cmpg-double v2, p2, v0

    .line 31
    .line 32
    if-gez v2, :cond_0

    .line 33
    .line 34
    const-wide p2, 0x4076800000000000L    # 360.0

    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    sub-double p0, p2, p0

    .line 40
    .line 41
    :cond_0
    return-wide p0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static getBorderCallout1Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 10

    .line 1
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    int-to-float v1, v1

    .line 9
    const/high16 v2, 0x3e400000    # 0.1875f

    .line 10
    .line 11
    mul-float v1, v1, v2

    .line 12
    .line 13
    add-float/2addr v0, v1

    .line 14
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 15
    .line 16
    int-to-float v1, v1

    .line 17
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    int-to-float v2, v2

    .line 22
    const v3, -0x42555715    # -0.08333f

    .line 23
    .line 24
    .line 25
    mul-float v2, v2, v3

    .line 26
    .line 27
    add-float/2addr v1, v2

    .line 28
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 29
    .line 30
    int-to-float v2, v2

    .line 31
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    int-to-float v3, v3

    .line 36
    const/high16 v4, 0x3f900000    # 1.125f

    .line 37
    .line 38
    mul-float v3, v3, v4

    .line 39
    .line 40
    add-float/2addr v2, v3

    .line 41
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v3, v3

    .line 44
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    int-to-float v4, v4

    .line 49
    const v5, -0x413bbc2c

    .line 50
    .line 51
    .line 52
    mul-float v4, v4, v5

    .line 53
    .line 54
    add-float/2addr v3, v4

    .line 55
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 56
    .line 57
    .line 58
    move-result-object p0

    .line 59
    if-eqz p0, :cond_3

    .line 60
    .line 61
    array-length v4, p0

    .line 62
    const/4 v5, 0x4

    .line 63
    if-lt v4, v5, :cond_3

    .line 64
    .line 65
    const/4 v4, 0x0

    .line 66
    aget-object v5, p0, v4

    .line 67
    .line 68
    if-eqz v5, :cond_0

    .line 69
    .line 70
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 71
    .line 72
    int-to-float v0, v0

    .line 73
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 74
    .line 75
    .line 76
    move-result v5

    .line 77
    int-to-float v5, v5

    .line 78
    aget-object v4, p0, v4

    .line 79
    .line 80
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 81
    .line 82
    .line 83
    move-result v4

    .line 84
    mul-float v5, v5, v4

    .line 85
    .line 86
    add-float/2addr v0, v5

    .line 87
    :cond_0
    const/4 v4, 0x1

    .line 88
    aget-object v5, p0, v4

    .line 89
    .line 90
    if-eqz v5, :cond_1

    .line 91
    .line 92
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 93
    .line 94
    int-to-float v1, v1

    .line 95
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 96
    .line 97
    .line 98
    move-result v5

    .line 99
    int-to-float v5, v5

    .line 100
    aget-object v4, p0, v4

    .line 101
    .line 102
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 103
    .line 104
    .line 105
    move-result v4

    .line 106
    mul-float v5, v5, v4

    .line 107
    .line 108
    add-float/2addr v1, v5

    .line 109
    :cond_1
    const/4 v4, 0x2

    .line 110
    aget-object v5, p0, v4

    .line 111
    .line 112
    if-eqz v5, :cond_2

    .line 113
    .line 114
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 115
    .line 116
    int-to-float v2, v2

    .line 117
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 118
    .line 119
    .line 120
    move-result v5

    .line 121
    int-to-float v5, v5

    .line 122
    aget-object v4, p0, v4

    .line 123
    .line 124
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 125
    .line 126
    .line 127
    move-result v4

    .line 128
    mul-float v5, v5, v4

    .line 129
    .line 130
    add-float/2addr v2, v5

    .line 131
    :cond_2
    const/4 v4, 0x3

    .line 132
    aget-object v5, p0, v4

    .line 133
    .line 134
    if-eqz v5, :cond_3

    .line 135
    .line 136
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 137
    .line 138
    int-to-float v3, v3

    .line 139
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 140
    .line 141
    .line 142
    move-result v5

    .line 143
    int-to-float v5, v5

    .line 144
    aget-object p0, p0, v4

    .line 145
    .line 146
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 147
    .line 148
    .line 149
    move-result p0

    .line 150
    mul-float v5, v5, p0

    .line 151
    .line 152
    add-float/2addr v3, v5

    .line 153
    :cond_3
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 154
    .line 155
    iget p0, p1, Landroid/graphics/Rect;->left:I

    .line 156
    .line 157
    int-to-float v5, p0

    .line 158
    iget p0, p1, Landroid/graphics/Rect;->top:I

    .line 159
    .line 160
    int-to-float v6, p0

    .line 161
    iget p0, p1, Landroid/graphics/Rect;->right:I

    .line 162
    .line 163
    int-to-float v7, p0

    .line 164
    iget p0, p1, Landroid/graphics/Rect;->bottom:I

    .line 165
    .line 166
    int-to-float v8, p0

    .line 167
    sget-object v9, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 168
    .line 169
    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 170
    .line 171
    .line 172
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 173
    .line 174
    invoke-virtual {p0, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 175
    .line 176
    .line 177
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 178
    .line 179
    invoke-virtual {p0, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 180
    .line 181
    .line 182
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 183
    .line 184
    return-object p0
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getBorderCallout2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v5

    .line 37
    int-to-float v5, v5

    .line 38
    mul-float v5, v5, v3

    .line 39
    .line 40
    add-float/2addr v4, v5

    .line 41
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v3, v3

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v5

    .line 48
    int-to-float v5, v5

    .line 49
    const v6, -0x41d55476    # -0.16667f

    .line 50
    .line 51
    .line 52
    mul-float v5, v5, v6

    .line 53
    .line 54
    add-float/2addr v3, v5

    .line 55
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 56
    .line 57
    int-to-float v5, v5

    .line 58
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 59
    .line 60
    .line 61
    move-result v6

    .line 62
    int-to-float v6, v6

    .line 63
    const/high16 v7, 0x3f900000    # 1.125f

    .line 64
    .line 65
    mul-float v6, v6, v7

    .line 66
    .line 67
    add-float/2addr v5, v6

    .line 68
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 69
    .line 70
    int-to-float v6, v6

    .line 71
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 72
    .line 73
    .line 74
    move-result v7

    .line 75
    int-to-float v7, v7

    .line 76
    const v8, -0x411110a1    # -0.46667f

    .line 77
    .line 78
    .line 79
    mul-float v7, v7, v8

    .line 80
    .line 81
    add-float/2addr v6, v7

    .line 82
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 83
    .line 84
    .line 85
    move-result-object v7

    .line 86
    if-eqz v7, :cond_5

    .line 87
    .line 88
    array-length v8, v7

    .line 89
    const/4 v9, 0x6

    .line 90
    if-lt v8, v9, :cond_5

    .line 91
    .line 92
    const/4 v8, 0x0

    .line 93
    aget-object v9, v7, v8

    .line 94
    .line 95
    if-eqz v9, :cond_0

    .line 96
    .line 97
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 98
    .line 99
    int-to-float v1, v1

    .line 100
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 101
    .line 102
    .line 103
    move-result v9

    .line 104
    int-to-float v9, v9

    .line 105
    aget-object v8, v7, v8

    .line 106
    .line 107
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 108
    .line 109
    .line 110
    move-result v8

    .line 111
    mul-float v9, v9, v8

    .line 112
    .line 113
    add-float/2addr v1, v9

    .line 114
    :cond_0
    const/4 v8, 0x1

    .line 115
    aget-object v9, v7, v8

    .line 116
    .line 117
    if-eqz v9, :cond_1

    .line 118
    .line 119
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 120
    .line 121
    int-to-float v2, v2

    .line 122
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 123
    .line 124
    .line 125
    move-result v9

    .line 126
    int-to-float v9, v9

    .line 127
    aget-object v8, v7, v8

    .line 128
    .line 129
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 130
    .line 131
    .line 132
    move-result v8

    .line 133
    mul-float v9, v9, v8

    .line 134
    .line 135
    add-float/2addr v2, v9

    .line 136
    :cond_1
    const/4 v8, 0x2

    .line 137
    aget-object v9, v7, v8

    .line 138
    .line 139
    if-eqz v9, :cond_2

    .line 140
    .line 141
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 142
    .line 143
    int-to-float v4, v4

    .line 144
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 145
    .line 146
    .line 147
    move-result v9

    .line 148
    int-to-float v9, v9

    .line 149
    aget-object v8, v7, v8

    .line 150
    .line 151
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 152
    .line 153
    .line 154
    move-result v8

    .line 155
    mul-float v9, v9, v8

    .line 156
    .line 157
    add-float/2addr v4, v9

    .line 158
    :cond_2
    const/4 v8, 0x3

    .line 159
    aget-object v9, v7, v8

    .line 160
    .line 161
    if-eqz v9, :cond_3

    .line 162
    .line 163
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 164
    .line 165
    int-to-float v3, v3

    .line 166
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 167
    .line 168
    .line 169
    move-result v9

    .line 170
    int-to-float v9, v9

    .line 171
    aget-object v8, v7, v8

    .line 172
    .line 173
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 174
    .line 175
    .line 176
    move-result v8

    .line 177
    mul-float v9, v9, v8

    .line 178
    .line 179
    add-float/2addr v3, v9

    .line 180
    :cond_3
    const/4 v8, 0x4

    .line 181
    aget-object v9, v7, v8

    .line 182
    .line 183
    if-eqz v9, :cond_4

    .line 184
    .line 185
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 186
    .line 187
    int-to-float v5, v5

    .line 188
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 189
    .line 190
    .line 191
    move-result v9

    .line 192
    int-to-float v9, v9

    .line 193
    aget-object v8, v7, v8

    .line 194
    .line 195
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 196
    .line 197
    .line 198
    move-result v8

    .line 199
    mul-float v9, v9, v8

    .line 200
    .line 201
    add-float/2addr v5, v9

    .line 202
    :cond_4
    const/4 v8, 0x5

    .line 203
    aget-object v9, v7, v8

    .line 204
    .line 205
    if-eqz v9, :cond_5

    .line 206
    .line 207
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 208
    .line 209
    int-to-float v6, v6

    .line 210
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 211
    .line 212
    .line 213
    move-result v9

    .line 214
    int-to-float v9, v9

    .line 215
    aget-object v7, v7, v8

    .line 216
    .line 217
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 218
    .line 219
    .line 220
    move-result v7

    .line 221
    mul-float v9, v9, v7

    .line 222
    .line 223
    add-float/2addr v6, v9

    .line 224
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 225
    .line 226
    .line 227
    move-result-object v7

    .line 228
    new-instance v8, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 229
    .line 230
    invoke-direct {v8}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 231
    .line 232
    .line 233
    new-instance v15, Landroid/graphics/Path;

    .line 234
    .line 235
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 236
    .line 237
    .line 238
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 239
    .line 240
    int-to-float v10, v9

    .line 241
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 242
    .line 243
    int-to-float v11, v9

    .line 244
    iget v9, v0, Landroid/graphics/Rect;->right:I

    .line 245
    .line 246
    int-to-float v12, v9

    .line 247
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 248
    .line 249
    int-to-float v13, v0

    .line 250
    sget-object v14, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 251
    .line 252
    move-object v9, v15

    .line 253
    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 254
    .line 255
    .line 256
    invoke-virtual {v8, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 257
    .line 258
    .line 259
    invoke-virtual {v8, v15}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 260
    .line 261
    .line 262
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 263
    .line 264
    .line 265
    move-result-object v0

    .line 266
    invoke-virtual {v8, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 267
    .line 268
    .line 269
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 270
    .line 271
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    .line 273
    .line 274
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 275
    .line 276
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 277
    .line 278
    .line 279
    new-instance v7, Landroid/graphics/Path;

    .line 280
    .line 281
    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 282
    .line 283
    .line 284
    invoke-virtual {v7, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 285
    .line 286
    .line 287
    invoke-virtual {v7, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 288
    .line 289
    .line 290
    invoke-virtual {v7, v6, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 291
    .line 292
    .line 293
    invoke-virtual {v0, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 294
    .line 295
    .line 296
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 297
    .line 298
    .line 299
    move-result-object v1

    .line 300
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 301
    .line 302
    .line 303
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 304
    .line 305
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 306
    .line 307
    .line 308
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 309
    .line 310
    return-object v0
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getBorderCallout3Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v6

    .line 37
    int-to-float v6, v6

    .line 38
    mul-float v6, v6, v3

    .line 39
    .line 40
    add-float/2addr v4, v6

    .line 41
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v3, v3

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v6

    .line 48
    int-to-float v6, v6

    .line 49
    const v7, -0x41d55476    # -0.16667f

    .line 50
    .line 51
    .line 52
    mul-float v6, v6, v7

    .line 53
    .line 54
    add-float/2addr v3, v6

    .line 55
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 56
    .line 57
    int-to-float v6, v6

    .line 58
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 59
    .line 60
    .line 61
    move-result v8

    .line 62
    int-to-float v8, v8

    .line 63
    const/high16 v9, 0x3f800000    # 1.0f

    .line 64
    .line 65
    mul-float v8, v8, v9

    .line 66
    .line 67
    add-float/2addr v6, v8

    .line 68
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 69
    .line 70
    int-to-float v8, v8

    .line 71
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 72
    .line 73
    .line 74
    move-result v9

    .line 75
    int-to-float v9, v9

    .line 76
    mul-float v9, v9, v7

    .line 77
    .line 78
    add-float/2addr v8, v9

    .line 79
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 80
    .line 81
    int-to-float v7, v7

    .line 82
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 83
    .line 84
    .line 85
    move-result v9

    .line 86
    int-to-float v9, v9

    .line 87
    const v10, 0x3f9097b7

    .line 88
    .line 89
    .line 90
    mul-float v9, v9, v10

    .line 91
    .line 92
    add-float/2addr v7, v9

    .line 93
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 94
    .line 95
    int-to-float v9, v9

    .line 96
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 97
    .line 98
    .line 99
    move-result v10

    .line 100
    int-to-float v10, v10

    .line 101
    mul-float v10, v10, v5

    .line 102
    .line 103
    add-float/2addr v9, v10

    .line 104
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 105
    .line 106
    .line 107
    move-result-object v5

    .line 108
    if-eqz v5, :cond_7

    .line 109
    .line 110
    array-length v10, v5

    .line 111
    const/16 v11, 0x8

    .line 112
    .line 113
    if-lt v10, v11, :cond_7

    .line 114
    .line 115
    const/4 v10, 0x0

    .line 116
    aget-object v11, v5, v10

    .line 117
    .line 118
    if-eqz v11, :cond_0

    .line 119
    .line 120
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 121
    .line 122
    int-to-float v1, v1

    .line 123
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 124
    .line 125
    .line 126
    move-result v11

    .line 127
    int-to-float v11, v11

    .line 128
    aget-object v10, v5, v10

    .line 129
    .line 130
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 131
    .line 132
    .line 133
    move-result v10

    .line 134
    mul-float v11, v11, v10

    .line 135
    .line 136
    add-float/2addr v1, v11

    .line 137
    :cond_0
    const/4 v10, 0x1

    .line 138
    aget-object v11, v5, v10

    .line 139
    .line 140
    if-eqz v11, :cond_1

    .line 141
    .line 142
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 143
    .line 144
    int-to-float v2, v2

    .line 145
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 146
    .line 147
    .line 148
    move-result v11

    .line 149
    int-to-float v11, v11

    .line 150
    aget-object v10, v5, v10

    .line 151
    .line 152
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 153
    .line 154
    .line 155
    move-result v10

    .line 156
    mul-float v11, v11, v10

    .line 157
    .line 158
    add-float/2addr v2, v11

    .line 159
    :cond_1
    const/4 v10, 0x2

    .line 160
    aget-object v11, v5, v10

    .line 161
    .line 162
    if-eqz v11, :cond_2

    .line 163
    .line 164
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 165
    .line 166
    int-to-float v4, v4

    .line 167
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 168
    .line 169
    .line 170
    move-result v11

    .line 171
    int-to-float v11, v11

    .line 172
    aget-object v10, v5, v10

    .line 173
    .line 174
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 175
    .line 176
    .line 177
    move-result v10

    .line 178
    mul-float v11, v11, v10

    .line 179
    .line 180
    add-float/2addr v4, v11

    .line 181
    :cond_2
    const/4 v10, 0x3

    .line 182
    aget-object v11, v5, v10

    .line 183
    .line 184
    if-eqz v11, :cond_3

    .line 185
    .line 186
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 187
    .line 188
    int-to-float v3, v3

    .line 189
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 190
    .line 191
    .line 192
    move-result v11

    .line 193
    int-to-float v11, v11

    .line 194
    aget-object v10, v5, v10

    .line 195
    .line 196
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 197
    .line 198
    .line 199
    move-result v10

    .line 200
    mul-float v11, v11, v10

    .line 201
    .line 202
    add-float/2addr v3, v11

    .line 203
    :cond_3
    const/4 v10, 0x4

    .line 204
    aget-object v11, v5, v10

    .line 205
    .line 206
    if-eqz v11, :cond_4

    .line 207
    .line 208
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 209
    .line 210
    int-to-float v6, v6

    .line 211
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 212
    .line 213
    .line 214
    move-result v11

    .line 215
    int-to-float v11, v11

    .line 216
    aget-object v10, v5, v10

    .line 217
    .line 218
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 219
    .line 220
    .line 221
    move-result v10

    .line 222
    mul-float v11, v11, v10

    .line 223
    .line 224
    add-float/2addr v6, v11

    .line 225
    :cond_4
    const/4 v10, 0x5

    .line 226
    aget-object v11, v5, v10

    .line 227
    .line 228
    if-eqz v11, :cond_5

    .line 229
    .line 230
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 231
    .line 232
    int-to-float v8, v8

    .line 233
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 234
    .line 235
    .line 236
    move-result v11

    .line 237
    int-to-float v11, v11

    .line 238
    aget-object v10, v5, v10

    .line 239
    .line 240
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 241
    .line 242
    .line 243
    move-result v10

    .line 244
    mul-float v11, v11, v10

    .line 245
    .line 246
    add-float/2addr v8, v11

    .line 247
    :cond_5
    const/4 v10, 0x6

    .line 248
    aget-object v11, v5, v10

    .line 249
    .line 250
    if-eqz v11, :cond_6

    .line 251
    .line 252
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 253
    .line 254
    int-to-float v7, v7

    .line 255
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 256
    .line 257
    .line 258
    move-result v11

    .line 259
    int-to-float v11, v11

    .line 260
    aget-object v10, v5, v10

    .line 261
    .line 262
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 263
    .line 264
    .line 265
    move-result v10

    .line 266
    mul-float v11, v11, v10

    .line 267
    .line 268
    add-float/2addr v7, v11

    .line 269
    :cond_6
    const/4 v10, 0x7

    .line 270
    aget-object v11, v5, v10

    .line 271
    .line 272
    if-eqz v11, :cond_7

    .line 273
    .line 274
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 275
    .line 276
    int-to-float v9, v9

    .line 277
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 278
    .line 279
    .line 280
    move-result v11

    .line 281
    int-to-float v11, v11

    .line 282
    aget-object v5, v5, v10

    .line 283
    .line 284
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 285
    .line 286
    .line 287
    move-result v5

    .line 288
    mul-float v11, v11, v5

    .line 289
    .line 290
    add-float/2addr v9, v11

    .line 291
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 292
    .line 293
    .line 294
    move-result-object v5

    .line 295
    new-instance v10, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 296
    .line 297
    invoke-direct {v10}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 298
    .line 299
    .line 300
    new-instance v15, Landroid/graphics/Path;

    .line 301
    .line 302
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 303
    .line 304
    .line 305
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 306
    .line 307
    int-to-float v12, v11

    .line 308
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 309
    .line 310
    int-to-float v13, v11

    .line 311
    iget v11, v0, Landroid/graphics/Rect;->right:I

    .line 312
    .line 313
    int-to-float v14, v11

    .line 314
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 315
    .line 316
    int-to-float v0, v0

    .line 317
    sget-object v16, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 318
    .line 319
    move-object v11, v15

    .line 320
    move/from16 v17, v7

    .line 321
    .line 322
    move-object v7, v15

    .line 323
    move v15, v0

    .line 324
    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 325
    .line 326
    .line 327
    invoke-virtual {v10, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 328
    .line 329
    .line 330
    invoke-virtual {v10, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 331
    .line 332
    .line 333
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 334
    .line 335
    .line 336
    move-result-object v0

    .line 337
    invoke-virtual {v10, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 338
    .line 339
    .line 340
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 341
    .line 342
    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343
    .line 344
    .line 345
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 346
    .line 347
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 348
    .line 349
    .line 350
    new-instance v5, Landroid/graphics/Path;

    .line 351
    .line 352
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 353
    .line 354
    .line 355
    invoke-virtual {v5, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 356
    .line 357
    .line 358
    invoke-virtual {v5, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 359
    .line 360
    .line 361
    invoke-virtual {v5, v8, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 362
    .line 363
    .line 364
    move/from16 v7, v17

    .line 365
    .line 366
    invoke-virtual {v5, v9, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 367
    .line 368
    .line 369
    invoke-virtual {v0, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 370
    .line 371
    .line 372
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 373
    .line 374
    .line 375
    move-result-object v1

    .line 376
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 377
    .line 378
    .line 379
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 380
    .line 381
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382
    .line 383
    .line 384
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 385
    .line 386
    return-object v0
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getCallout1(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    int-to-float v1, v1

    .line 9
    const/high16 v2, 0x3e400000    # 0.1875f

    .line 10
    .line 11
    mul-float v1, v1, v2

    .line 12
    .line 13
    add-float/2addr v0, v1

    .line 14
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 15
    .line 16
    int-to-float v1, v1

    .line 17
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    int-to-float v2, v2

    .line 22
    const v3, -0x42555715    # -0.08333f

    .line 23
    .line 24
    .line 25
    mul-float v2, v2, v3

    .line 26
    .line 27
    add-float/2addr v1, v2

    .line 28
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 29
    .line 30
    int-to-float v2, v2

    .line 31
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    int-to-float v3, v3

    .line 36
    const/high16 v4, 0x3f900000    # 1.125f

    .line 37
    .line 38
    mul-float v3, v3, v4

    .line 39
    .line 40
    add-float/2addr v2, v3

    .line 41
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v3, v3

    .line 44
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    int-to-float v4, v4

    .line 49
    const v5, -0x413bbc2c

    .line 50
    .line 51
    .line 52
    mul-float v4, v4, v5

    .line 53
    .line 54
    add-float/2addr v3, v4

    .line 55
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 56
    .line 57
    .line 58
    move-result-object v4

    .line 59
    if-eqz v4, :cond_3

    .line 60
    .line 61
    array-length v5, v4

    .line 62
    const/4 v6, 0x4

    .line 63
    if-lt v5, v6, :cond_3

    .line 64
    .line 65
    const/4 v5, 0x0

    .line 66
    aget-object v6, v4, v5

    .line 67
    .line 68
    if-eqz v6, :cond_0

    .line 69
    .line 70
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 71
    .line 72
    int-to-float v0, v0

    .line 73
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 74
    .line 75
    .line 76
    move-result v6

    .line 77
    int-to-float v6, v6

    .line 78
    aget-object v5, v4, v5

    .line 79
    .line 80
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 81
    .line 82
    .line 83
    move-result v5

    .line 84
    mul-float v6, v6, v5

    .line 85
    .line 86
    add-float/2addr v0, v6

    .line 87
    :cond_0
    const/4 v5, 0x1

    .line 88
    aget-object v6, v4, v5

    .line 89
    .line 90
    if-eqz v6, :cond_1

    .line 91
    .line 92
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 93
    .line 94
    int-to-float v1, v1

    .line 95
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 96
    .line 97
    .line 98
    move-result v6

    .line 99
    int-to-float v6, v6

    .line 100
    aget-object v5, v4, v5

    .line 101
    .line 102
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 103
    .line 104
    .line 105
    move-result v5

    .line 106
    mul-float v6, v6, v5

    .line 107
    .line 108
    add-float/2addr v1, v6

    .line 109
    :cond_1
    const/4 v5, 0x2

    .line 110
    aget-object v6, v4, v5

    .line 111
    .line 112
    if-eqz v6, :cond_2

    .line 113
    .line 114
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 115
    .line 116
    int-to-float v2, v2

    .line 117
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 118
    .line 119
    .line 120
    move-result v6

    .line 121
    int-to-float v6, v6

    .line 122
    aget-object v5, v4, v5

    .line 123
    .line 124
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 125
    .line 126
    .line 127
    move-result v5

    .line 128
    mul-float v6, v6, v5

    .line 129
    .line 130
    add-float/2addr v2, v6

    .line 131
    :cond_2
    const/4 v5, 0x3

    .line 132
    aget-object v6, v4, v5

    .line 133
    .line 134
    if-eqz v6, :cond_3

    .line 135
    .line 136
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 137
    .line 138
    int-to-float v3, v3

    .line 139
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 140
    .line 141
    .line 142
    move-result v6

    .line 143
    int-to-float v6, v6

    .line 144
    aget-object v4, v4, v5

    .line 145
    .line 146
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 147
    .line 148
    .line 149
    move-result v4

    .line 150
    mul-float v6, v6, v4

    .line 151
    .line 152
    add-float/2addr v3, v6

    .line 153
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 154
    .line 155
    .line 156
    move-result-object v4

    .line 157
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 158
    .line 159
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 160
    .line 161
    .line 162
    new-instance v12, Landroid/graphics/Path;

    .line 163
    .line 164
    invoke-direct {v12}, Landroid/graphics/Path;-><init>()V

    .line 165
    .line 166
    .line 167
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 168
    .line 169
    int-to-float v7, v6

    .line 170
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 171
    .line 172
    int-to-float v8, v6

    .line 173
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 174
    .line 175
    int-to-float v9, v6

    .line 176
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 177
    .line 178
    int-to-float v10, p1

    .line 179
    sget-object v11, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 180
    .line 181
    move-object v6, v12

    .line 182
    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 183
    .line 184
    .line 185
    invoke-virtual {v5, v12}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 186
    .line 187
    .line 188
    invoke-virtual {v5, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 189
    .line 190
    .line 191
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 192
    .line 193
    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    .line 195
    .line 196
    new-instance p1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 197
    .line 198
    invoke-direct {p1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 199
    .line 200
    .line 201
    new-instance v4, Landroid/graphics/Path;

    .line 202
    .line 203
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 204
    .line 205
    .line 206
    invoke-virtual {v4, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 207
    .line 208
    .line 209
    invoke-virtual {v4, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 210
    .line 211
    .line 212
    invoke-virtual {p1, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 213
    .line 214
    .line 215
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 216
    .line 217
    .line 218
    move-result-object p0

    .line 219
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 220
    .line 221
    .line 222
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 223
    .line 224
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    .line 226
    .line 227
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 228
    .line 229
    return-object p0
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getCallout2(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v5

    .line 37
    int-to-float v5, v5

    .line 38
    mul-float v5, v5, v3

    .line 39
    .line 40
    add-float/2addr v4, v5

    .line 41
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v3, v3

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v5

    .line 48
    int-to-float v5, v5

    .line 49
    const v6, -0x41d55476    # -0.16667f

    .line 50
    .line 51
    .line 52
    mul-float v5, v5, v6

    .line 53
    .line 54
    add-float/2addr v3, v5

    .line 55
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 56
    .line 57
    int-to-float v5, v5

    .line 58
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 59
    .line 60
    .line 61
    move-result v6

    .line 62
    int-to-float v6, v6

    .line 63
    const/high16 v7, 0x3f900000    # 1.125f

    .line 64
    .line 65
    mul-float v6, v6, v7

    .line 66
    .line 67
    add-float/2addr v5, v6

    .line 68
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 69
    .line 70
    int-to-float v6, v6

    .line 71
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 72
    .line 73
    .line 74
    move-result v7

    .line 75
    int-to-float v7, v7

    .line 76
    const v8, -0x411110a1    # -0.46667f

    .line 77
    .line 78
    .line 79
    mul-float v7, v7, v8

    .line 80
    .line 81
    add-float/2addr v6, v7

    .line 82
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 83
    .line 84
    .line 85
    move-result-object v7

    .line 86
    if-eqz v7, :cond_5

    .line 87
    .line 88
    array-length v8, v7

    .line 89
    const/4 v9, 0x6

    .line 90
    if-lt v8, v9, :cond_5

    .line 91
    .line 92
    const/4 v8, 0x0

    .line 93
    aget-object v9, v7, v8

    .line 94
    .line 95
    if-eqz v9, :cond_0

    .line 96
    .line 97
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 98
    .line 99
    int-to-float v1, v1

    .line 100
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 101
    .line 102
    .line 103
    move-result v9

    .line 104
    int-to-float v9, v9

    .line 105
    aget-object v8, v7, v8

    .line 106
    .line 107
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 108
    .line 109
    .line 110
    move-result v8

    .line 111
    mul-float v9, v9, v8

    .line 112
    .line 113
    add-float/2addr v1, v9

    .line 114
    :cond_0
    const/4 v8, 0x1

    .line 115
    aget-object v9, v7, v8

    .line 116
    .line 117
    if-eqz v9, :cond_1

    .line 118
    .line 119
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 120
    .line 121
    int-to-float v2, v2

    .line 122
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 123
    .line 124
    .line 125
    move-result v9

    .line 126
    int-to-float v9, v9

    .line 127
    aget-object v8, v7, v8

    .line 128
    .line 129
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 130
    .line 131
    .line 132
    move-result v8

    .line 133
    mul-float v9, v9, v8

    .line 134
    .line 135
    add-float/2addr v2, v9

    .line 136
    :cond_1
    const/4 v8, 0x2

    .line 137
    aget-object v9, v7, v8

    .line 138
    .line 139
    if-eqz v9, :cond_2

    .line 140
    .line 141
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 142
    .line 143
    int-to-float v4, v4

    .line 144
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 145
    .line 146
    .line 147
    move-result v9

    .line 148
    int-to-float v9, v9

    .line 149
    aget-object v8, v7, v8

    .line 150
    .line 151
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 152
    .line 153
    .line 154
    move-result v8

    .line 155
    mul-float v9, v9, v8

    .line 156
    .line 157
    add-float/2addr v4, v9

    .line 158
    :cond_2
    const/4 v8, 0x3

    .line 159
    aget-object v9, v7, v8

    .line 160
    .line 161
    if-eqz v9, :cond_3

    .line 162
    .line 163
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 164
    .line 165
    int-to-float v3, v3

    .line 166
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 167
    .line 168
    .line 169
    move-result v9

    .line 170
    int-to-float v9, v9

    .line 171
    aget-object v8, v7, v8

    .line 172
    .line 173
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 174
    .line 175
    .line 176
    move-result v8

    .line 177
    mul-float v9, v9, v8

    .line 178
    .line 179
    add-float/2addr v3, v9

    .line 180
    :cond_3
    const/4 v8, 0x4

    .line 181
    aget-object v9, v7, v8

    .line 182
    .line 183
    if-eqz v9, :cond_4

    .line 184
    .line 185
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 186
    .line 187
    int-to-float v5, v5

    .line 188
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 189
    .line 190
    .line 191
    move-result v9

    .line 192
    int-to-float v9, v9

    .line 193
    aget-object v8, v7, v8

    .line 194
    .line 195
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 196
    .line 197
    .line 198
    move-result v8

    .line 199
    mul-float v9, v9, v8

    .line 200
    .line 201
    add-float/2addr v5, v9

    .line 202
    :cond_4
    const/4 v8, 0x5

    .line 203
    aget-object v9, v7, v8

    .line 204
    .line 205
    if-eqz v9, :cond_5

    .line 206
    .line 207
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 208
    .line 209
    int-to-float v6, v6

    .line 210
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 211
    .line 212
    .line 213
    move-result v9

    .line 214
    int-to-float v9, v9

    .line 215
    aget-object v7, v7, v8

    .line 216
    .line 217
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 218
    .line 219
    .line 220
    move-result v7

    .line 221
    mul-float v9, v9, v7

    .line 222
    .line 223
    add-float/2addr v6, v9

    .line 224
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 225
    .line 226
    .line 227
    move-result-object v7

    .line 228
    new-instance v8, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 229
    .line 230
    invoke-direct {v8}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 231
    .line 232
    .line 233
    new-instance v15, Landroid/graphics/Path;

    .line 234
    .line 235
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 236
    .line 237
    .line 238
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 239
    .line 240
    int-to-float v10, v9

    .line 241
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 242
    .line 243
    int-to-float v11, v9

    .line 244
    iget v9, v0, Landroid/graphics/Rect;->right:I

    .line 245
    .line 246
    int-to-float v12, v9

    .line 247
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 248
    .line 249
    int-to-float v13, v0

    .line 250
    sget-object v14, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 251
    .line 252
    move-object v9, v15

    .line 253
    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 254
    .line 255
    .line 256
    invoke-virtual {v8, v15}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 257
    .line 258
    .line 259
    invoke-virtual {v8, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 260
    .line 261
    .line 262
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 263
    .line 264
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    .line 266
    .line 267
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 268
    .line 269
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 270
    .line 271
    .line 272
    new-instance v7, Landroid/graphics/Path;

    .line 273
    .line 274
    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 275
    .line 276
    .line 277
    invoke-virtual {v7, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 278
    .line 279
    .line 280
    invoke-virtual {v7, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 281
    .line 282
    .line 283
    invoke-virtual {v7, v6, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 284
    .line 285
    .line 286
    invoke-virtual {v0, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 287
    .line 288
    .line 289
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 290
    .line 291
    .line 292
    move-result-object v1

    .line 293
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 294
    .line 295
    .line 296
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 297
    .line 298
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 299
    .line 300
    .line 301
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 302
    .line 303
    return-object v0
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getCallout3(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    int-to-float v2, v2

    .line 11
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 12
    .line 13
    mul-float v2, v2, v3

    .line 14
    .line 15
    add-float/2addr v1, v2

    .line 16
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v2, v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    int-to-float v4, v4

    .line 24
    const v5, -0x42555715    # -0.08333f

    .line 25
    .line 26
    .line 27
    mul-float v4, v4, v5

    .line 28
    .line 29
    add-float/2addr v2, v4

    .line 30
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v6

    .line 37
    int-to-float v6, v6

    .line 38
    mul-float v6, v6, v3

    .line 39
    .line 40
    add-float/2addr v4, v6

    .line 41
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 42
    .line 43
    int-to-float v3, v3

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v6

    .line 48
    int-to-float v6, v6

    .line 49
    const v7, -0x41d55476    # -0.16667f

    .line 50
    .line 51
    .line 52
    mul-float v6, v6, v7

    .line 53
    .line 54
    add-float/2addr v3, v6

    .line 55
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 56
    .line 57
    int-to-float v6, v6

    .line 58
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 59
    .line 60
    .line 61
    move-result v8

    .line 62
    int-to-float v8, v8

    .line 63
    const/high16 v9, 0x3f800000    # 1.0f

    .line 64
    .line 65
    mul-float v8, v8, v9

    .line 66
    .line 67
    add-float/2addr v6, v8

    .line 68
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 69
    .line 70
    int-to-float v8, v8

    .line 71
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 72
    .line 73
    .line 74
    move-result v9

    .line 75
    int-to-float v9, v9

    .line 76
    mul-float v9, v9, v7

    .line 77
    .line 78
    add-float/2addr v8, v9

    .line 79
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 80
    .line 81
    int-to-float v7, v7

    .line 82
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 83
    .line 84
    .line 85
    move-result v9

    .line 86
    int-to-float v9, v9

    .line 87
    const v10, 0x3f9097b7

    .line 88
    .line 89
    .line 90
    mul-float v9, v9, v10

    .line 91
    .line 92
    add-float/2addr v7, v9

    .line 93
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 94
    .line 95
    int-to-float v9, v9

    .line 96
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 97
    .line 98
    .line 99
    move-result v10

    .line 100
    int-to-float v10, v10

    .line 101
    mul-float v10, v10, v5

    .line 102
    .line 103
    add-float/2addr v9, v10

    .line 104
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 105
    .line 106
    .line 107
    move-result-object v5

    .line 108
    if-eqz v5, :cond_7

    .line 109
    .line 110
    array-length v10, v5

    .line 111
    const/16 v11, 0x8

    .line 112
    .line 113
    if-lt v10, v11, :cond_7

    .line 114
    .line 115
    const/4 v10, 0x0

    .line 116
    aget-object v11, v5, v10

    .line 117
    .line 118
    if-eqz v11, :cond_0

    .line 119
    .line 120
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 121
    .line 122
    int-to-float v1, v1

    .line 123
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 124
    .line 125
    .line 126
    move-result v11

    .line 127
    int-to-float v11, v11

    .line 128
    aget-object v10, v5, v10

    .line 129
    .line 130
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 131
    .line 132
    .line 133
    move-result v10

    .line 134
    mul-float v11, v11, v10

    .line 135
    .line 136
    add-float/2addr v1, v11

    .line 137
    :cond_0
    const/4 v10, 0x1

    .line 138
    aget-object v11, v5, v10

    .line 139
    .line 140
    if-eqz v11, :cond_1

    .line 141
    .line 142
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 143
    .line 144
    int-to-float v2, v2

    .line 145
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 146
    .line 147
    .line 148
    move-result v11

    .line 149
    int-to-float v11, v11

    .line 150
    aget-object v10, v5, v10

    .line 151
    .line 152
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 153
    .line 154
    .line 155
    move-result v10

    .line 156
    mul-float v11, v11, v10

    .line 157
    .line 158
    add-float/2addr v2, v11

    .line 159
    :cond_1
    const/4 v10, 0x2

    .line 160
    aget-object v11, v5, v10

    .line 161
    .line 162
    if-eqz v11, :cond_2

    .line 163
    .line 164
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 165
    .line 166
    int-to-float v4, v4

    .line 167
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 168
    .line 169
    .line 170
    move-result v11

    .line 171
    int-to-float v11, v11

    .line 172
    aget-object v10, v5, v10

    .line 173
    .line 174
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 175
    .line 176
    .line 177
    move-result v10

    .line 178
    mul-float v11, v11, v10

    .line 179
    .line 180
    add-float/2addr v4, v11

    .line 181
    :cond_2
    const/4 v10, 0x3

    .line 182
    aget-object v11, v5, v10

    .line 183
    .line 184
    if-eqz v11, :cond_3

    .line 185
    .line 186
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 187
    .line 188
    int-to-float v3, v3

    .line 189
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 190
    .line 191
    .line 192
    move-result v11

    .line 193
    int-to-float v11, v11

    .line 194
    aget-object v10, v5, v10

    .line 195
    .line 196
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 197
    .line 198
    .line 199
    move-result v10

    .line 200
    mul-float v11, v11, v10

    .line 201
    .line 202
    add-float/2addr v3, v11

    .line 203
    :cond_3
    const/4 v10, 0x4

    .line 204
    aget-object v11, v5, v10

    .line 205
    .line 206
    if-eqz v11, :cond_4

    .line 207
    .line 208
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 209
    .line 210
    int-to-float v6, v6

    .line 211
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 212
    .line 213
    .line 214
    move-result v11

    .line 215
    int-to-float v11, v11

    .line 216
    aget-object v10, v5, v10

    .line 217
    .line 218
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 219
    .line 220
    .line 221
    move-result v10

    .line 222
    mul-float v11, v11, v10

    .line 223
    .line 224
    add-float/2addr v6, v11

    .line 225
    :cond_4
    const/4 v10, 0x5

    .line 226
    aget-object v11, v5, v10

    .line 227
    .line 228
    if-eqz v11, :cond_5

    .line 229
    .line 230
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 231
    .line 232
    int-to-float v8, v8

    .line 233
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 234
    .line 235
    .line 236
    move-result v11

    .line 237
    int-to-float v11, v11

    .line 238
    aget-object v10, v5, v10

    .line 239
    .line 240
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 241
    .line 242
    .line 243
    move-result v10

    .line 244
    mul-float v11, v11, v10

    .line 245
    .line 246
    add-float/2addr v8, v11

    .line 247
    :cond_5
    const/4 v10, 0x6

    .line 248
    aget-object v11, v5, v10

    .line 249
    .line 250
    if-eqz v11, :cond_6

    .line 251
    .line 252
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 253
    .line 254
    int-to-float v7, v7

    .line 255
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 256
    .line 257
    .line 258
    move-result v11

    .line 259
    int-to-float v11, v11

    .line 260
    aget-object v10, v5, v10

    .line 261
    .line 262
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    .line 263
    .line 264
    .line 265
    move-result v10

    .line 266
    mul-float v11, v11, v10

    .line 267
    .line 268
    add-float/2addr v7, v11

    .line 269
    :cond_6
    const/4 v10, 0x7

    .line 270
    aget-object v11, v5, v10

    .line 271
    .line 272
    if-eqz v11, :cond_7

    .line 273
    .line 274
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 275
    .line 276
    int-to-float v9, v9

    .line 277
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 278
    .line 279
    .line 280
    move-result v11

    .line 281
    int-to-float v11, v11

    .line 282
    aget-object v5, v5, v10

    .line 283
    .line 284
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 285
    .line 286
    .line 287
    move-result v5

    .line 288
    mul-float v11, v11, v5

    .line 289
    .line 290
    add-float/2addr v9, v11

    .line 291
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 292
    .line 293
    .line 294
    move-result-object v5

    .line 295
    new-instance v10, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 296
    .line 297
    invoke-direct {v10}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 298
    .line 299
    .line 300
    new-instance v15, Landroid/graphics/Path;

    .line 301
    .line 302
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 303
    .line 304
    .line 305
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 306
    .line 307
    int-to-float v12, v11

    .line 308
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 309
    .line 310
    int-to-float v13, v11

    .line 311
    iget v11, v0, Landroid/graphics/Rect;->right:I

    .line 312
    .line 313
    int-to-float v14, v11

    .line 314
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 315
    .line 316
    int-to-float v0, v0

    .line 317
    sget-object v16, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 318
    .line 319
    move-object v11, v15

    .line 320
    move/from16 v17, v7

    .line 321
    .line 322
    move-object v7, v15

    .line 323
    move v15, v0

    .line 324
    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 325
    .line 326
    .line 327
    invoke-virtual {v10, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 328
    .line 329
    .line 330
    invoke-virtual {v10, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 331
    .line 332
    .line 333
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 334
    .line 335
    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336
    .line 337
    .line 338
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 339
    .line 340
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 341
    .line 342
    .line 343
    new-instance v5, Landroid/graphics/Path;

    .line 344
    .line 345
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 346
    .line 347
    .line 348
    invoke-virtual {v5, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 349
    .line 350
    .line 351
    invoke-virtual {v5, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 352
    .line 353
    .line 354
    invoke-virtual {v5, v8, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 355
    .line 356
    .line 357
    move/from16 v7, v17

    .line 358
    .line 359
    invoke-virtual {v5, v9, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 360
    .line 361
    .line 362
    invoke-virtual {v0, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 363
    .line 364
    .line 365
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 366
    .line 367
    .line 368
    move-result-object v1

    .line 369
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 370
    .line 371
    .line 372
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 373
    .line 374
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 375
    .line 376
    .line 377
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 378
    .line 379
    return-object v0
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getCloudCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 19

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 4
    .line 5
    const v2, 0x438e8000    # 285.0f

    .line 6
    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/high16 v4, 0x43200000    # 160.0f

    .line 10
    .line 11
    const/high16 v5, 0x42b40000    # 90.0f

    .line 12
    .line 13
    invoke-virtual {v1, v3, v4, v5, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 14
    .line 15
    .line 16
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 17
    .line 18
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 19
    .line 20
    const/high16 v4, 0x42f00000    # 120.0f

    .line 21
    .line 22
    const/high16 v6, 0x43140000    # 148.0f

    .line 23
    .line 24
    invoke-virtual {v1, v2, v4, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 25
    .line 26
    .line 27
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 28
    .line 29
    const/high16 v2, 0x433c0000    # 188.0f

    .line 30
    .line 31
    const/high16 v4, 0x437a0000    # 250.0f

    .line 32
    .line 33
    const/high16 v6, 0x42240000    # 41.0f

    .line 34
    .line 35
    const/high16 v7, 0x42300000    # 44.0f

    .line 36
    .line 37
    invoke-virtual {v1, v6, v7, v2, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 38
    .line 39
    .line 40
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 41
    .line 42
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 43
    .line 44
    const v4, 0x432c8000    # 172.5f

    .line 45
    .line 46
    .line 47
    const/high16 v6, 0x42ff0000    # 127.5f

    .line 48
    .line 49
    invoke-virtual {v1, v2, v4, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 50
    .line 51
    .line 52
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 53
    .line 54
    const/high16 v2, 0x43840000    # 264.0f

    .line 55
    .line 56
    const/high16 v4, 0x435c0000    # 220.0f

    .line 57
    .line 58
    const/high16 v6, 0x430c0000    # 140.0f

    .line 59
    .line 60
    const/high16 v7, 0x41600000    # 14.0f

    .line 61
    .line 62
    invoke-virtual {v1, v6, v7, v2, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 63
    .line 64
    .line 65
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 66
    .line 67
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 68
    .line 69
    const/high16 v4, 0x435a0000    # 218.0f

    .line 70
    .line 71
    invoke-virtual {v1, v2, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 72
    .line 73
    .line 74
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 75
    .line 76
    const/high16 v2, 0x43aa0000    # 340.0f

    .line 77
    .line 78
    const/high16 v4, 0x43520000    # 210.0f

    .line 79
    .line 80
    const/high16 v5, 0x43660000    # 230.0f

    .line 81
    .line 82
    invoke-virtual {v1, v5, v3, v2, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 83
    .line 84
    .line 85
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 86
    .line 87
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 88
    .line 89
    const/high16 v4, 0x435b0000    # 219.0f

    .line 90
    .line 91
    const/high16 v5, 0x42b80000    # 92.0f

    .line 92
    .line 93
    invoke-virtual {v1, v2, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 94
    .line 95
    .line 96
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 97
    .line 98
    const/high16 v2, 0x43d60000    # 428.0f

    .line 99
    .line 100
    const/high16 v4, 0x43760000    # 246.0f

    .line 101
    .line 102
    const/high16 v5, 0x43940000    # 296.0f

    .line 103
    .line 104
    invoke-virtual {v1, v5, v3, v2, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 105
    .line 106
    .line 107
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 108
    .line 109
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 110
    .line 111
    const/high16 v3, 0x43680000    # 232.0f

    .line 112
    .line 113
    const/high16 v4, 0x42ca0000    # 101.0f

    .line 114
    .line 115
    invoke-virtual {v1, v2, v3, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 116
    .line 117
    .line 118
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 119
    .line 120
    const/high16 v2, 0x43e30000    # 454.0f

    .line 121
    .line 122
    const/high16 v3, 0x43560000    # 214.0f

    .line 123
    .line 124
    const/high16 v4, 0x43ab0000    # 342.0f

    .line 125
    .line 126
    const/high16 v5, 0x42700000    # 60.0f

    .line 127
    .line 128
    invoke-virtual {v1, v4, v5, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 129
    .line 130
    .line 131
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 132
    .line 133
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 134
    .line 135
    const v3, 0x43928000    # 293.0f

    .line 136
    .line 137
    .line 138
    const/high16 v4, 0x42b20000    # 89.0f

    .line 139
    .line 140
    invoke-virtual {v1, v2, v3, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 141
    .line 142
    .line 143
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 144
    .line 145
    const v2, 0x43a38000    # 327.0f

    .line 146
    .line 147
    .line 148
    const/high16 v3, 0x43a20000    # 324.0f

    .line 149
    .line 150
    const/high16 v4, 0x43020000    # 130.0f

    .line 151
    .line 152
    const/high16 v5, 0x43ea0000    # 468.0f

    .line 153
    .line 154
    invoke-virtual {v1, v3, v4, v5, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 155
    .line 156
    .line 157
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 158
    .line 159
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 160
    .line 161
    const v3, 0x439f8000    # 319.0f

    .line 162
    .line 163
    .line 164
    const/high16 v7, 0x42ee0000    # 119.0f

    .line 165
    .line 166
    invoke-virtual {v1, v2, v3, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 167
    .line 168
    .line 169
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 170
    .line 171
    const v2, 0x43ca8000    # 405.0f

    .line 172
    .line 173
    .line 174
    const/high16 v3, 0x43ce0000    # 412.0f

    .line 175
    .line 176
    const/high16 v7, 0x438c0000    # 280.0f

    .line 177
    .line 178
    const/high16 v8, 0x43700000    # 240.0f

    .line 179
    .line 180
    invoke-virtual {v1, v7, v8, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 181
    .line 182
    .line 183
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 184
    .line 185
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 186
    .line 187
    const/high16 v3, 0x3f800000    # 1.0f

    .line 188
    .line 189
    const/high16 v7, 0x42f40000    # 122.0f

    .line 190
    .line 191
    invoke-virtual {v1, v2, v3, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 192
    .line 193
    .line 194
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 195
    .line 196
    const/high16 v2, 0x43890000    # 274.0f

    .line 197
    .line 198
    const/high16 v3, 0x439c0000    # 312.0f

    .line 199
    .line 200
    const/high16 v7, 0x43280000    # 168.0f

    .line 201
    .line 202
    invoke-virtual {v1, v7, v2, v3, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 203
    .line 204
    .line 205
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 206
    .line 207
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 208
    .line 209
    const/high16 v3, 0x41800000    # 16.0f

    .line 210
    .line 211
    invoke-virtual {v1, v2, v3, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 212
    .line 213
    .line 214
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 215
    .line 216
    const/high16 v2, 0x43550000    # 213.0f

    .line 217
    .line 218
    const v4, 0x43dc8000    # 441.0f

    .line 219
    .line 220
    .line 221
    const/high16 v7, 0x42640000    # 57.0f

    .line 222
    .line 223
    const/high16 v8, 0x43790000    # 249.0f

    .line 224
    .line 225
    invoke-virtual {v1, v7, v8, v2, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 226
    .line 227
    .line 228
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 229
    .line 230
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 231
    .line 232
    const/high16 v4, 0x42600000    # 56.0f

    .line 233
    .line 234
    const/high16 v7, 0x42940000    # 74.0f

    .line 235
    .line 236
    invoke-virtual {v1, v2, v4, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 237
    .line 238
    .line 239
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 240
    .line 241
    const/high16 v2, 0x42c60000    # 99.0f

    .line 242
    .line 243
    const/high16 v4, 0x43c10000    # 386.0f

    .line 244
    .line 245
    const/high16 v7, 0x41300000    # 11.0f

    .line 246
    .line 247
    const v8, 0x43818000    # 259.0f

    .line 248
    .line 249
    .line 250
    invoke-virtual {v1, v7, v8, v2, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 251
    .line 252
    .line 253
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 254
    .line 255
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 256
    .line 257
    const/high16 v4, 0x42a80000    # 84.0f

    .line 258
    .line 259
    invoke-virtual {v1, v2, v4, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 260
    .line 261
    .line 262
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 263
    .line 264
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 265
    .line 266
    .line 267
    new-instance v1, Landroid/graphics/Matrix;

    .line 268
    .line 269
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 270
    .line 271
    .line 272
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 273
    .line 274
    .line 275
    move-result v2

    .line 276
    int-to-float v2, v2

    .line 277
    div-float/2addr v2, v5

    .line 278
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 279
    .line 280
    .line 281
    move-result v4

    .line 282
    int-to-float v4, v4

    .line 283
    div-float/2addr v4, v5

    .line 284
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 285
    .line 286
    .line 287
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 288
    .line 289
    invoke-virtual {v2, v1}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 290
    .line 291
    .line 292
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 293
    .line 294
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 295
    .line 296
    int-to-float v2, v2

    .line 297
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 298
    .line 299
    int-to-float v4, v4

    .line 300
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->offset(FF)V

    .line 301
    .line 302
    .line 303
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 304
    .line 305
    .line 306
    move-result-object v1

    .line 307
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 308
    .line 309
    .line 310
    move-result v2

    .line 311
    const v4, 0x3f333333    # 0.7f

    .line 312
    .line 313
    .line 314
    const/4 v6, 0x1

    .line 315
    const/4 v7, 0x2

    .line 316
    const/4 v8, 0x0

    .line 317
    if-eqz v2, :cond_2

    .line 318
    .line 319
    if-eqz v1, :cond_1

    .line 320
    .line 321
    array-length v2, v1

    .line 322
    if-lt v2, v7, :cond_1

    .line 323
    .line 324
    aget-object v2, v1, v8

    .line 325
    .line 326
    if-eqz v2, :cond_0

    .line 327
    .line 328
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 329
    .line 330
    .line 331
    move-result v2

    .line 332
    int-to-float v2, v2

    .line 333
    aget-object v9, v1, v8

    .line 334
    .line 335
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 336
    .line 337
    .line 338
    move-result v9

    .line 339
    mul-float v2, v2, v9

    .line 340
    .line 341
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 342
    .line 343
    .line 344
    move-result v2

    .line 345
    goto :goto_0

    .line 346
    :cond_0
    const/4 v2, 0x0

    .line 347
    :goto_0
    aget-object v9, v1, v6

    .line 348
    .line 349
    if-eqz v9, :cond_5

    .line 350
    .line 351
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 352
    .line 353
    .line 354
    move-result v8

    .line 355
    int-to-float v8, v8

    .line 356
    aget-object v1, v1, v6

    .line 357
    .line 358
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 359
    .line 360
    .line 361
    move-result v1

    .line 362
    mul-float v8, v8, v1

    .line 363
    .line 364
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    .line 365
    .line 366
    .line 367
    move-result v8

    .line 368
    goto/16 :goto_2

    .line 369
    .line 370
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 371
    .line 372
    .line 373
    move-result v1

    .line 374
    int-to-float v1, v1

    .line 375
    const v2, -0x41b33333    # -0.2f

    .line 376
    .line 377
    .line 378
    mul-float v1, v1, v2

    .line 379
    .line 380
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 381
    .line 382
    .line 383
    move-result v2

    .line 384
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 385
    .line 386
    .line 387
    move-result v1

    .line 388
    int-to-float v1, v1

    .line 389
    const v6, 0x3f19999a    # 0.6f

    .line 390
    .line 391
    .line 392
    mul-float v1, v1, v6

    .line 393
    .line 394
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 395
    .line 396
    .line 397
    move-result v8

    .line 398
    goto :goto_2

    .line 399
    :cond_2
    if-eqz v1, :cond_4

    .line 400
    .line 401
    array-length v2, v1

    .line 402
    if-lt v2, v7, :cond_4

    .line 403
    .line 404
    aget-object v2, v1, v8

    .line 405
    .line 406
    if-eqz v2, :cond_3

    .line 407
    .line 408
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 409
    .line 410
    .line 411
    move-result v2

    .line 412
    int-to-float v2, v2

    .line 413
    aget-object v9, v1, v8

    .line 414
    .line 415
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 416
    .line 417
    .line 418
    move-result v9

    .line 419
    mul-float v2, v2, v9

    .line 420
    .line 421
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 422
    .line 423
    .line 424
    move-result v9

    .line 425
    div-int/2addr v9, v7

    .line 426
    int-to-float v9, v9

    .line 427
    sub-float/2addr v2, v9

    .line 428
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 429
    .line 430
    .line 431
    move-result v2

    .line 432
    goto :goto_1

    .line 433
    :cond_3
    const/4 v2, 0x0

    .line 434
    :goto_1
    aget-object v9, v1, v6

    .line 435
    .line 436
    if-eqz v9, :cond_5

    .line 437
    .line 438
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 439
    .line 440
    .line 441
    move-result v8

    .line 442
    int-to-float v8, v8

    .line 443
    aget-object v1, v1, v6

    .line 444
    .line 445
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 446
    .line 447
    .line 448
    move-result v1

    .line 449
    mul-float v8, v8, v1

    .line 450
    .line 451
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 452
    .line 453
    .line 454
    move-result v1

    .line 455
    div-int/2addr v1, v7

    .line 456
    int-to-float v1, v1

    .line 457
    sub-float/2addr v8, v1

    .line 458
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    .line 459
    .line 460
    .line 461
    move-result v8

    .line 462
    goto :goto_2

    .line 463
    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 464
    .line 465
    .line 466
    move-result v1

    .line 467
    int-to-float v1, v1

    .line 468
    const v2, -0x41224dd3    # -0.433f

    .line 469
    .line 470
    .line 471
    mul-float v1, v1, v2

    .line 472
    .line 473
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 474
    .line 475
    .line 476
    move-result v2

    .line 477
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 478
    .line 479
    .line 480
    move-result v1

    .line 481
    int-to-float v1, v1

    .line 482
    mul-float v1, v1, v4

    .line 483
    .line 484
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 485
    .line 486
    .line 487
    move-result v8

    .line 488
    :cond_5
    :goto_2
    int-to-double v9, v2

    .line 489
    int-to-double v11, v8

    .line 490
    invoke-static {v9, v10, v11, v12}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getAngle(DD)D

    .line 491
    .line 492
    .line 493
    move-result-wide v9

    .line 494
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 495
    .line 496
    .line 497
    move-result v1

    .line 498
    div-int/2addr v1, v7

    .line 499
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 500
    .line 501
    .line 502
    move-result v6

    .line 503
    div-int/2addr v6, v7

    .line 504
    mul-int v7, v1, v6

    .line 505
    .line 506
    int-to-double v11, v7

    .line 507
    int-to-double v6, v6

    .line 508
    const-wide/high16 v13, 0x4000000000000000L    # 2.0

    .line 509
    .line 510
    invoke-static {v6, v7, v13, v14}, Ljava/lang/Math;->pow(DD)D

    .line 511
    .line 512
    .line 513
    move-result-wide v6

    .line 514
    int-to-double v3, v1

    .line 515
    const-wide v15, 0x400921fb54442d18L    # Math.PI

    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    mul-double v15, v15, v9

    .line 521
    .line 522
    const-wide v17, 0x4066800000000000L    # 180.0

    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    div-double v15, v15, v17

    .line 528
    .line 529
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->tan(D)D

    .line 530
    .line 531
    .line 532
    move-result-wide v17

    .line 533
    mul-double v3, v3, v17

    .line 534
    .line 535
    invoke-static {v3, v4, v13, v14}, Ljava/lang/Math;->pow(DD)D

    .line 536
    .line 537
    .line 538
    move-result-wide v3

    .line 539
    add-double/2addr v6, v3

    .line 540
    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    .line 541
    .line 542
    .line 543
    move-result-wide v3

    .line 544
    div-double/2addr v11, v3

    .line 545
    double-to-float v1, v11

    .line 546
    const-wide v3, 0x4056800000000000L    # 90.0

    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    cmpl-double v6, v9, v3

    .line 552
    .line 553
    if-lez v6, :cond_6

    .line 554
    .line 555
    const-wide v3, 0x4070e00000000000L    # 270.0

    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    cmpg-double v6, v9, v3

    .line 561
    .line 562
    if-gez v6, :cond_6

    .line 563
    .line 564
    neg-float v1, v1

    .line 565
    :cond_6
    float-to-double v3, v1

    .line 566
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->tan(D)D

    .line 567
    .line 568
    .line 569
    move-result-wide v6

    .line 570
    mul-double v3, v3, v6

    .line 571
    .line 572
    double-to-float v3, v3

    .line 573
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 574
    .line 575
    .line 576
    move-result v4

    .line 577
    add-int/2addr v4, v2

    .line 578
    int-to-float v2, v4

    .line 579
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 580
    .line 581
    .line 582
    move-result v4

    .line 583
    add-int/2addr v4, v8

    .line 584
    int-to-float v4, v4

    .line 585
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 586
    .line 587
    .line 588
    move-result v6

    .line 589
    int-to-float v6, v6

    .line 590
    add-float/2addr v6, v1

    .line 591
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 592
    .line 593
    .line 594
    move-result v1

    .line 595
    int-to-float v1, v1

    .line 596
    add-float/2addr v1, v3

    .line 597
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 598
    .line 599
    .line 600
    move-result v3

    .line 601
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 602
    .line 603
    .line 604
    move-result v0

    .line 605
    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    .line 606
    .line 607
    .line 608
    move-result v0

    .line 609
    int-to-float v0, v0

    .line 610
    div-float/2addr v0, v5

    .line 611
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 612
    .line 613
    const/high16 v5, 0x41800000    # 16.0f

    .line 614
    .line 615
    mul-float v5, v5, v0

    .line 616
    .line 617
    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 618
    .line 619
    invoke-virtual {v3, v2, v4, v5, v7}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 620
    .line 621
    .line 622
    sub-float/2addr v2, v6

    .line 623
    const v3, 0x3f333333    # 0.7f

    .line 624
    .line 625
    .line 626
    mul-float v5, v2, v3

    .line 627
    .line 628
    add-float/2addr v5, v6

    .line 629
    sub-float/2addr v4, v1

    .line 630
    mul-float v3, v3, v4

    .line 631
    .line 632
    add-float/2addr v3, v1

    .line 633
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 634
    .line 635
    const/high16 v8, 0x41c00000    # 24.0f

    .line 636
    .line 637
    mul-float v8, v8, v0

    .line 638
    .line 639
    sget-object v9, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 640
    .line 641
    invoke-virtual {v7, v5, v3, v8, v9}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 642
    .line 643
    .line 644
    const v3, 0x3e99999a    # 0.3f

    .line 645
    .line 646
    .line 647
    mul-float v2, v2, v3

    .line 648
    .line 649
    add-float/2addr v6, v2

    .line 650
    mul-float v4, v4, v3

    .line 651
    .line 652
    add-float/2addr v1, v4

    .line 653
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 654
    .line 655
    const/high16 v3, 0x42200000    # 40.0f

    .line 656
    .line 657
    mul-float v0, v0, v3

    .line 658
    .line 659
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 660
    .line 661
    invoke-virtual {v2, v6, v1, v0, v3}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 662
    .line 663
    .line 664
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 665
    .line 666
    return-object v0
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getWedgeEllipseCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 8

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    neg-int v0, v0

    .line 6
    int-to-float v0, v0

    .line 7
    const v1, 0x3e4ccccd    # 0.2f

    .line 8
    .line 9
    .line 10
    mul-float v0, v0, v1

    .line 11
    .line 12
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    int-to-float v1, v1

    .line 17
    const v2, 0x3f19999a    # 0.6f

    .line 18
    .line 19
    .line 20
    mul-float v1, v1, v2

    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 27
    .line 28
    .line 29
    move-result p0

    .line 30
    const/4 v3, 0x1

    .line 31
    const/4 v4, 0x0

    .line 32
    const/4 v5, 0x2

    .line 33
    if-eqz p0, :cond_1

    .line 34
    .line 35
    if-eqz v2, :cond_3

    .line 36
    .line 37
    array-length p0, v2

    .line 38
    if-lt p0, v5, :cond_3

    .line 39
    .line 40
    aget-object p0, v2, v4

    .line 41
    .line 42
    if-eqz p0, :cond_0

    .line 43
    .line 44
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result p0

    .line 48
    int-to-float p0, p0

    .line 49
    aget-object v0, v2, v4

    .line 50
    .line 51
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    mul-float v0, v0, p0

    .line 56
    .line 57
    :cond_0
    aget-object p0, v2, v3

    .line 58
    .line 59
    if-eqz p0, :cond_3

    .line 60
    .line 61
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 62
    .line 63
    .line 64
    move-result p0

    .line 65
    int-to-float p0, p0

    .line 66
    aget-object v1, v2, v3

    .line 67
    .line 68
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    mul-float v1, v1, p0

    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 76
    .line 77
    .line 78
    move-result p0

    .line 79
    neg-int p0, p0

    .line 80
    int-to-float p0, p0

    .line 81
    const v0, 0x3eddb22d    # 0.433f

    .line 82
    .line 83
    .line 84
    mul-float v0, v0, p0

    .line 85
    .line 86
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 87
    .line 88
    .line 89
    move-result p0

    .line 90
    int-to-float p0, p0

    .line 91
    const v1, 0x3f333333    # 0.7f

    .line 92
    .line 93
    .line 94
    mul-float v1, v1, p0

    .line 95
    .line 96
    if-eqz v2, :cond_3

    .line 97
    .line 98
    array-length p0, v2

    .line 99
    if-lt p0, v5, :cond_3

    .line 100
    .line 101
    aget-object p0, v2, v4

    .line 102
    .line 103
    if-eqz p0, :cond_2

    .line 104
    .line 105
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 106
    .line 107
    .line 108
    move-result p0

    .line 109
    int-to-float p0, p0

    .line 110
    aget-object v0, v2, v4

    .line 111
    .line 112
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 113
    .line 114
    .line 115
    move-result v0

    .line 116
    mul-float p0, p0, v0

    .line 117
    .line 118
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    div-int/2addr v0, v5

    .line 123
    int-to-float v0, v0

    .line 124
    sub-float/2addr p0, v0

    .line 125
    move v0, p0

    .line 126
    :cond_2
    aget-object p0, v2, v3

    .line 127
    .line 128
    if-eqz p0, :cond_3

    .line 129
    .line 130
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 131
    .line 132
    .line 133
    move-result p0

    .line 134
    int-to-float p0, p0

    .line 135
    aget-object v1, v2, v3

    .line 136
    .line 137
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 138
    .line 139
    .line 140
    move-result v1

    .line 141
    mul-float p0, p0, v1

    .line 142
    .line 143
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 144
    .line 145
    .line 146
    move-result v1

    .line 147
    div-int/2addr v1, v5

    .line 148
    int-to-float v1, v1

    .line 149
    sub-float v1, p0, v1

    .line 150
    .line 151
    :cond_3
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 152
    .line 153
    .line 154
    move-result p0

    .line 155
    int-to-double v2, p0

    .line 156
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 157
    .line 158
    .line 159
    move-result p0

    .line 160
    int-to-double v4, p0

    .line 161
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    .line 162
    .line 163
    .line 164
    move-result-wide v2

    .line 165
    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    .line 166
    .line 167
    .line 168
    move-result-wide v2

    .line 169
    double-to-float p0, v2

    .line 170
    const/high16 v2, 0x40000000    # 2.0f

    .line 171
    .line 172
    div-float/2addr p0, v2

    .line 173
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 174
    .line 175
    .line 176
    move-result v3

    .line 177
    float-to-double v3, v3

    .line 178
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    .line 179
    .line 180
    .line 181
    move-result v5

    .line 182
    float-to-double v5, v5

    .line 183
    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->atan2(DD)D

    .line 184
    .line 185
    .line 186
    move-result-wide v3

    .line 187
    invoke-static {v3, v4}, Ljava/lang/Math;->toDegrees(D)D

    .line 188
    .line 189
    .line 190
    move-result-wide v3

    .line 191
    double-to-float v3, v3

    .line 192
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 193
    .line 194
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 195
    .line 196
    .line 197
    move-result v5

    .line 198
    add-float/2addr v5, v0

    .line 199
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 200
    .line 201
    .line 202
    move-result v6

    .line 203
    add-float/2addr v6, v1

    .line 204
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 205
    .line 206
    .line 207
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 208
    .line 209
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 210
    .line 211
    int-to-float v5, v5

    .line 212
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 213
    .line 214
    int-to-float v6, v6

    .line 215
    iget v7, p1, Landroid/graphics/Rect;->right:I

    .line 216
    .line 217
    int-to-float v7, v7

    .line 218
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 219
    .line 220
    int-to-float p1, p1

    .line 221
    invoke-virtual {v4, v5, v6, v7, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 222
    .line 223
    .line 224
    const/high16 p1, 0x43b40000    # 360.0f

    .line 225
    .line 226
    const/high16 v4, 0x43340000    # 180.0f

    .line 227
    .line 228
    const/4 v5, 0x0

    .line 229
    cmpl-float v1, v1, v5

    .line 230
    .line 231
    if-ltz v1, :cond_5

    .line 232
    .line 233
    cmpl-float v0, v0, v5

    .line 234
    .line 235
    if-ltz v0, :cond_4

    .line 236
    .line 237
    div-float v0, p0, v2

    .line 238
    .line 239
    add-float/2addr v3, v0

    .line 240
    goto :goto_1

    .line 241
    :cond_4
    sub-float/2addr v4, v3

    .line 242
    div-float v0, p0, v2

    .line 243
    .line 244
    add-float v3, v4, v0

    .line 245
    .line 246
    :goto_1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 247
    .line 248
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 249
    .line 250
    sub-float/2addr p1, p0

    .line 251
    invoke-virtual {v0, v1, v3, p1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 252
    .line 253
    .line 254
    goto :goto_3

    .line 255
    :cond_5
    cmpl-float v0, v0, v5

    .line 256
    .line 257
    if-ltz v0, :cond_6

    .line 258
    .line 259
    sub-float/2addr p1, v3

    .line 260
    div-float v0, p0, v2

    .line 261
    .line 262
    sub-float/2addr p1, v0

    .line 263
    goto :goto_2

    .line 264
    :cond_6
    add-float/2addr v3, v4

    .line 265
    div-float p1, p0, v2

    .line 266
    .line 267
    sub-float p1, v3, p1

    .line 268
    .line 269
    :goto_2
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 270
    .line 271
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 272
    .line 273
    const/high16 v2, -0x3c4c0000    # -360.0f

    .line 274
    .line 275
    add-float/2addr p0, v2

    .line 276
    invoke-virtual {v0, v1, p1, p0}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 277
    .line 278
    .line 279
    :goto_3
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 280
    .line 281
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 282
    .line 283
    .line 284
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 285
    .line 286
    return-object p0
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getWedgeRectCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 8

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    neg-int v0, v0

    .line 6
    int-to-float v0, v0

    .line 7
    const v1, 0x3e4ccccd    # 0.2f

    .line 8
    .line 9
    .line 10
    mul-float v0, v0, v1

    .line 11
    .line 12
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    int-to-float v1, v1

    .line 17
    const v2, 0x3f19999a    # 0.6f

    .line 18
    .line 19
    .line 20
    mul-float v1, v1, v2

    .line 21
    .line 22
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    int-to-float v2, v2

    .line 27
    const/high16 v3, 0x41400000    # 12.0f

    .line 28
    .line 29
    div-float/2addr v2, v3

    .line 30
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 31
    .line 32
    .line 33
    move-result-object v4

    .line 34
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 35
    .line 36
    .line 37
    move-result p0

    .line 38
    const/4 v5, 0x1

    .line 39
    const/4 v6, 0x0

    .line 40
    const/4 v7, 0x2

    .line 41
    if-eqz p0, :cond_1

    .line 42
    .line 43
    if-eqz v4, :cond_3

    .line 44
    .line 45
    array-length p0, v4

    .line 46
    if-lt p0, v7, :cond_3

    .line 47
    .line 48
    aget-object p0, v4, v6

    .line 49
    .line 50
    if-eqz p0, :cond_0

    .line 51
    .line 52
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 53
    .line 54
    .line 55
    move-result p0

    .line 56
    int-to-float p0, p0

    .line 57
    aget-object v0, v4, v6

    .line 58
    .line 59
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    mul-float v0, v0, p0

    .line 64
    .line 65
    :cond_0
    aget-object p0, v4, v5

    .line 66
    .line 67
    if-eqz p0, :cond_3

    .line 68
    .line 69
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 70
    .line 71
    .line 72
    move-result p0

    .line 73
    int-to-float p0, p0

    .line 74
    aget-object v1, v4, v5

    .line 75
    .line 76
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 77
    .line 78
    .line 79
    move-result v1

    .line 80
    mul-float v1, v1, p0

    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 84
    .line 85
    .line 86
    move-result p0

    .line 87
    neg-int p0, p0

    .line 88
    int-to-float p0, p0

    .line 89
    const v0, 0x3eddb22d    # 0.433f

    .line 90
    .line 91
    .line 92
    mul-float v0, v0, p0

    .line 93
    .line 94
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 95
    .line 96
    .line 97
    move-result p0

    .line 98
    int-to-float p0, p0

    .line 99
    const v1, 0x3f333333    # 0.7f

    .line 100
    .line 101
    .line 102
    mul-float v1, v1, p0

    .line 103
    .line 104
    if-eqz v4, :cond_3

    .line 105
    .line 106
    array-length p0, v4

    .line 107
    if-lt p0, v7, :cond_3

    .line 108
    .line 109
    aget-object p0, v4, v6

    .line 110
    .line 111
    if-eqz p0, :cond_2

    .line 112
    .line 113
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 114
    .line 115
    .line 116
    move-result p0

    .line 117
    int-to-float p0, p0

    .line 118
    aget-object v0, v4, v6

    .line 119
    .line 120
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 121
    .line 122
    .line 123
    move-result v0

    .line 124
    mul-float p0, p0, v0

    .line 125
    .line 126
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 127
    .line 128
    .line 129
    move-result v0

    .line 130
    div-int/2addr v0, v7

    .line 131
    int-to-float v0, v0

    .line 132
    sub-float/2addr p0, v0

    .line 133
    move v0, p0

    .line 134
    :cond_2
    aget-object p0, v4, v5

    .line 135
    .line 136
    if-eqz p0, :cond_3

    .line 137
    .line 138
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 139
    .line 140
    .line 141
    move-result p0

    .line 142
    int-to-float p0, p0

    .line 143
    aget-object v1, v4, v5

    .line 144
    .line 145
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 146
    .line 147
    .line 148
    move-result v1

    .line 149
    mul-float p0, p0, v1

    .line 150
    .line 151
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 152
    .line 153
    .line 154
    move-result v1

    .line 155
    div-int/2addr v1, v7

    .line 156
    int-to-float v1, v1

    .line 157
    sub-float v1, p0, v1

    .line 158
    .line 159
    :cond_3
    :goto_0
    div-float p0, v1, v0

    .line 160
    .line 161
    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    .line 162
    .line 163
    .line 164
    move-result p0

    .line 165
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 166
    .line 167
    .line 168
    move-result v4

    .line 169
    int-to-float v4, v4

    .line 170
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 171
    .line 172
    .line 173
    move-result v5

    .line 174
    int-to-float v5, v5

    .line 175
    div-float/2addr v4, v5

    .line 176
    const/4 v5, 0x0

    .line 177
    const/high16 v6, 0x40000000    # 2.0f

    .line 178
    .line 179
    cmpg-float p0, p0, v4

    .line 180
    .line 181
    if-gez p0, :cond_7

    .line 182
    .line 183
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 184
    .line 185
    .line 186
    move-result p0

    .line 187
    int-to-float p0, p0

    .line 188
    div-float/2addr p0, v3

    .line 189
    cmpl-float v2, v0, v5

    .line 190
    .line 191
    if-ltz v2, :cond_5

    .line 192
    .line 193
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 194
    .line 195
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 196
    .line 197
    int-to-float v3, v3

    .line 198
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 199
    .line 200
    int-to-float v4, v4

    .line 201
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 202
    .line 203
    .line 204
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 205
    .line 206
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 207
    .line 208
    int-to-float v3, v3

    .line 209
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 210
    .line 211
    int-to-float v4, v4

    .line 212
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 213
    .line 214
    .line 215
    cmpl-float v2, v1, v5

    .line 216
    .line 217
    if-ltz v2, :cond_4

    .line 218
    .line 219
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 220
    .line 221
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 222
    .line 223
    int-to-float v3, v3

    .line 224
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 225
    .line 226
    .line 227
    move-result v4

    .line 228
    add-float/2addr v4, p0

    .line 229
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 230
    .line 231
    .line 232
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 233
    .line 234
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 235
    .line 236
    .line 237
    move-result v3

    .line 238
    add-float/2addr v3, v0

    .line 239
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 240
    .line 241
    .line 242
    move-result v0

    .line 243
    add-float/2addr v0, v1

    .line 244
    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 245
    .line 246
    .line 247
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 248
    .line 249
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 250
    .line 251
    int-to-float v1, v1

    .line 252
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 253
    .line 254
    int-to-float v2, v2

    .line 255
    mul-float p0, p0, v6

    .line 256
    .line 257
    sub-float/2addr v2, p0

    .line 258
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 259
    .line 260
    .line 261
    goto :goto_1

    .line 262
    :cond_4
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 263
    .line 264
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 265
    .line 266
    int-to-float v3, v3

    .line 267
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 268
    .line 269
    int-to-float v4, v4

    .line 270
    mul-float v6, v6, p0

    .line 271
    .line 272
    add-float/2addr v4, v6

    .line 273
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 274
    .line 275
    .line 276
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 277
    .line 278
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 279
    .line 280
    .line 281
    move-result v3

    .line 282
    add-float/2addr v3, v0

    .line 283
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 284
    .line 285
    .line 286
    move-result v0

    .line 287
    add-float/2addr v0, v1

    .line 288
    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 289
    .line 290
    .line 291
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 292
    .line 293
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 294
    .line 295
    int-to-float v1, v1

    .line 296
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 297
    .line 298
    .line 299
    move-result v2

    .line 300
    sub-float/2addr v2, p0

    .line 301
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 302
    .line 303
    .line 304
    :goto_1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 305
    .line 306
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 307
    .line 308
    int-to-float v0, v0

    .line 309
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 310
    .line 311
    int-to-float v1, v1

    .line 312
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 313
    .line 314
    .line 315
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 316
    .line 317
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 318
    .line 319
    int-to-float v0, v0

    .line 320
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 321
    .line 322
    int-to-float p1, p1

    .line 323
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 324
    .line 325
    .line 326
    goto/16 :goto_4

    .line 327
    .line 328
    :cond_5
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 329
    .line 330
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 331
    .line 332
    int-to-float v3, v3

    .line 333
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 334
    .line 335
    int-to-float v4, v4

    .line 336
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 337
    .line 338
    .line 339
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 340
    .line 341
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 342
    .line 343
    int-to-float v3, v3

    .line 344
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 345
    .line 346
    int-to-float v4, v4

    .line 347
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 348
    .line 349
    .line 350
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 351
    .line 352
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 353
    .line 354
    int-to-float v3, v3

    .line 355
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 356
    .line 357
    int-to-float v4, v4

    .line 358
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 359
    .line 360
    .line 361
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 362
    .line 363
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 364
    .line 365
    int-to-float v3, v3

    .line 366
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 367
    .line 368
    int-to-float v4, v4

    .line 369
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 370
    .line 371
    .line 372
    cmpl-float v2, v1, v5

    .line 373
    .line 374
    if-ltz v2, :cond_6

    .line 375
    .line 376
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 377
    .line 378
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 379
    .line 380
    int-to-float v3, v3

    .line 381
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 382
    .line 383
    int-to-float v4, v4

    .line 384
    mul-float v6, v6, p0

    .line 385
    .line 386
    sub-float/2addr v4, v6

    .line 387
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 388
    .line 389
    .line 390
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 391
    .line 392
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 393
    .line 394
    .line 395
    move-result v3

    .line 396
    add-float/2addr v3, v0

    .line 397
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 398
    .line 399
    .line 400
    move-result v0

    .line 401
    add-float/2addr v0, v1

    .line 402
    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 403
    .line 404
    .line 405
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 406
    .line 407
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 408
    .line 409
    int-to-float v1, v1

    .line 410
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 411
    .line 412
    .line 413
    move-result p1

    .line 414
    add-float/2addr p1, p0

    .line 415
    invoke-virtual {v0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 416
    .line 417
    .line 418
    goto/16 :goto_4

    .line 419
    .line 420
    :cond_6
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 421
    .line 422
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 423
    .line 424
    int-to-float v3, v3

    .line 425
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 426
    .line 427
    .line 428
    move-result v4

    .line 429
    sub-float/2addr v4, p0

    .line 430
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 431
    .line 432
    .line 433
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 434
    .line 435
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 436
    .line 437
    .line 438
    move-result v3

    .line 439
    add-float/2addr v3, v0

    .line 440
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 441
    .line 442
    .line 443
    move-result v0

    .line 444
    add-float/2addr v0, v1

    .line 445
    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 446
    .line 447
    .line 448
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 449
    .line 450
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 451
    .line 452
    int-to-float v1, v1

    .line 453
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 454
    .line 455
    int-to-float p1, p1

    .line 456
    mul-float p0, p0, v6

    .line 457
    .line 458
    add-float/2addr p1, p0

    .line 459
    invoke-virtual {v0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 460
    .line 461
    .line 462
    goto/16 :goto_4

    .line 463
    .line 464
    :cond_7
    cmpl-float p0, v1, v5

    .line 465
    .line 466
    if-ltz p0, :cond_9

    .line 467
    .line 468
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 469
    .line 470
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 471
    .line 472
    int-to-float v3, v3

    .line 473
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 474
    .line 475
    int-to-float v4, v4

    .line 476
    invoke-virtual {p0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 477
    .line 478
    .line 479
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 480
    .line 481
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 482
    .line 483
    int-to-float v3, v3

    .line 484
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 485
    .line 486
    int-to-float v4, v4

    .line 487
    invoke-virtual {p0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 488
    .line 489
    .line 490
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 491
    .line 492
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 493
    .line 494
    int-to-float v3, v3

    .line 495
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 496
    .line 497
    int-to-float v4, v4

    .line 498
    invoke-virtual {p0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 499
    .line 500
    .line 501
    cmpl-float p0, v0, v5

    .line 502
    .line 503
    if-ltz p0, :cond_8

    .line 504
    .line 505
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 506
    .line 507
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 508
    .line 509
    int-to-float v3, v3

    .line 510
    mul-float v6, v6, v2

    .line 511
    .line 512
    sub-float/2addr v3, v6

    .line 513
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 514
    .line 515
    int-to-float v4, v4

    .line 516
    invoke-virtual {p0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 517
    .line 518
    .line 519
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 520
    .line 521
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 522
    .line 523
    .line 524
    move-result v3

    .line 525
    add-float/2addr v3, v0

    .line 526
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 527
    .line 528
    .line 529
    move-result v0

    .line 530
    add-float/2addr v0, v1

    .line 531
    invoke-virtual {p0, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 532
    .line 533
    .line 534
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 535
    .line 536
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 537
    .line 538
    .line 539
    move-result v0

    .line 540
    add-float/2addr v0, v2

    .line 541
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 542
    .line 543
    int-to-float v1, v1

    .line 544
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 545
    .line 546
    .line 547
    goto :goto_2

    .line 548
    :cond_8
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 549
    .line 550
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 551
    .line 552
    .line 553
    move-result v3

    .line 554
    sub-float/2addr v3, v2

    .line 555
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 556
    .line 557
    int-to-float v4, v4

    .line 558
    invoke-virtual {p0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 559
    .line 560
    .line 561
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 562
    .line 563
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 564
    .line 565
    .line 566
    move-result v3

    .line 567
    add-float/2addr v3, v0

    .line 568
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 569
    .line 570
    .line 571
    move-result v0

    .line 572
    add-float/2addr v0, v1

    .line 573
    invoke-virtual {p0, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 574
    .line 575
    .line 576
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 577
    .line 578
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 579
    .line 580
    int-to-float v0, v0

    .line 581
    mul-float v2, v2, v6

    .line 582
    .line 583
    add-float/2addr v0, v2

    .line 584
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 585
    .line 586
    int-to-float v1, v1

    .line 587
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 588
    .line 589
    .line 590
    :goto_2
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 591
    .line 592
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 593
    .line 594
    int-to-float v0, v0

    .line 595
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 596
    .line 597
    int-to-float p1, p1

    .line 598
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 599
    .line 600
    .line 601
    goto/16 :goto_4

    .line 602
    .line 603
    :cond_9
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 604
    .line 605
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 606
    .line 607
    int-to-float v3, v3

    .line 608
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 609
    .line 610
    int-to-float v4, v4

    .line 611
    invoke-virtual {p0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 612
    .line 613
    .line 614
    cmpl-float p0, v0, v5

    .line 615
    .line 616
    if-ltz p0, :cond_a

    .line 617
    .line 618
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 619
    .line 620
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 621
    .line 622
    .line 623
    move-result v3

    .line 624
    add-float/2addr v3, v2

    .line 625
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 626
    .line 627
    int-to-float v4, v4

    .line 628
    invoke-virtual {p0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 629
    .line 630
    .line 631
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 632
    .line 633
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 634
    .line 635
    .line 636
    move-result v3

    .line 637
    add-float/2addr v3, v0

    .line 638
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 639
    .line 640
    .line 641
    move-result v0

    .line 642
    add-float/2addr v0, v1

    .line 643
    invoke-virtual {p0, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 644
    .line 645
    .line 646
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 647
    .line 648
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 649
    .line 650
    int-to-float v0, v0

    .line 651
    mul-float v2, v2, v6

    .line 652
    .line 653
    sub-float/2addr v0, v2

    .line 654
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 655
    .line 656
    int-to-float v1, v1

    .line 657
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 658
    .line 659
    .line 660
    goto :goto_3

    .line 661
    :cond_a
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 662
    .line 663
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 664
    .line 665
    int-to-float v3, v3

    .line 666
    mul-float v6, v6, v2

    .line 667
    .line 668
    add-float/2addr v3, v6

    .line 669
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 670
    .line 671
    int-to-float v4, v4

    .line 672
    invoke-virtual {p0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 673
    .line 674
    .line 675
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 676
    .line 677
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 678
    .line 679
    .line 680
    move-result v3

    .line 681
    add-float/2addr v3, v0

    .line 682
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 683
    .line 684
    .line 685
    move-result v0

    .line 686
    add-float/2addr v0, v1

    .line 687
    invoke-virtual {p0, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 688
    .line 689
    .line 690
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 691
    .line 692
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 693
    .line 694
    .line 695
    move-result v0

    .line 696
    sub-float/2addr v0, v2

    .line 697
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 698
    .line 699
    int-to-float v1, v1

    .line 700
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 701
    .line 702
    .line 703
    :goto_3
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 704
    .line 705
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 706
    .line 707
    int-to-float v0, v0

    .line 708
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 709
    .line 710
    int-to-float v1, v1

    .line 711
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 712
    .line 713
    .line 714
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 715
    .line 716
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 717
    .line 718
    int-to-float v0, v0

    .line 719
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 720
    .line 721
    int-to-float v1, v1

    .line 722
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 723
    .line 724
    .line 725
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 726
    .line 727
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 728
    .line 729
    int-to-float v0, v0

    .line 730
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 731
    .line 732
    int-to-float p1, p1

    .line 733
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 734
    .line 735
    .line 736
    :goto_4
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 737
    .line 738
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 739
    .line 740
    .line 741
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 742
    .line 743
    return-object p0
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getWedgeRoundRectCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 13

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    neg-int v0, v0

    .line 6
    int-to-float v0, v0

    .line 7
    const v1, 0x3e4ccccd    # 0.2f

    .line 8
    .line 9
    .line 10
    mul-float v0, v0, v1

    .line 11
    .line 12
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    int-to-float v1, v1

    .line 17
    const v2, 0x3f19999a    # 0.6f

    .line 18
    .line 19
    .line 20
    mul-float v1, v1, v2

    .line 21
    .line 22
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    int-to-float v2, v2

    .line 27
    const/high16 v3, 0x41400000    # 12.0f

    .line 28
    .line 29
    div-float/2addr v2, v3

    .line 30
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 31
    .line 32
    .line 33
    move-result v4

    .line 34
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 35
    .line 36
    .line 37
    move-result v5

    .line 38
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    .line 39
    .line 40
    .line 41
    move-result v4

    .line 42
    int-to-float v4, v4

    .line 43
    const v5, 0x3e2aab8a    # 0.16667f

    .line 44
    .line 45
    .line 46
    mul-float v4, v4, v5

    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 49
    .line 50
    .line 51
    move-result-object v5

    .line 52
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 53
    .line 54
    .line 55
    move-result p0

    .line 56
    const/4 v6, 0x1

    .line 57
    const/4 v7, 0x0

    .line 58
    const/4 v8, 0x2

    .line 59
    if-eqz p0, :cond_2

    .line 60
    .line 61
    if-eqz v5, :cond_4

    .line 62
    .line 63
    array-length p0, v5

    .line 64
    const/4 v9, 0x3

    .line 65
    if-lt p0, v9, :cond_4

    .line 66
    .line 67
    aget-object p0, v5, v7

    .line 68
    .line 69
    if-eqz p0, :cond_0

    .line 70
    .line 71
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 72
    .line 73
    .line 74
    move-result p0

    .line 75
    int-to-float p0, p0

    .line 76
    aget-object v0, v5, v7

    .line 77
    .line 78
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 79
    .line 80
    .line 81
    move-result v0

    .line 82
    mul-float v0, v0, p0

    .line 83
    .line 84
    :cond_0
    aget-object p0, v5, v6

    .line 85
    .line 86
    if-eqz p0, :cond_1

    .line 87
    .line 88
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 89
    .line 90
    .line 91
    move-result p0

    .line 92
    int-to-float p0, p0

    .line 93
    aget-object v1, v5, v6

    .line 94
    .line 95
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 96
    .line 97
    .line 98
    move-result v1

    .line 99
    mul-float v1, v1, p0

    .line 100
    .line 101
    :cond_1
    aget-object p0, v5, v8

    .line 102
    .line 103
    if-eqz p0, :cond_4

    .line 104
    .line 105
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 106
    .line 107
    .line 108
    move-result p0

    .line 109
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 110
    .line 111
    .line 112
    move-result v4

    .line 113
    invoke-static {p0, v4}, Ljava/lang/Math;->min(II)I

    .line 114
    .line 115
    .line 116
    move-result p0

    .line 117
    int-to-float p0, p0

    .line 118
    aget-object v4, v5, v8

    .line 119
    .line 120
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 121
    .line 122
    .line 123
    move-result v4

    .line 124
    mul-float v4, v4, p0

    .line 125
    .line 126
    goto :goto_0

    .line 127
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 128
    .line 129
    .line 130
    move-result p0

    .line 131
    neg-int p0, p0

    .line 132
    int-to-float p0, p0

    .line 133
    const v0, 0x3eddb22d    # 0.433f

    .line 134
    .line 135
    .line 136
    mul-float v0, v0, p0

    .line 137
    .line 138
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 139
    .line 140
    .line 141
    move-result p0

    .line 142
    int-to-float p0, p0

    .line 143
    const v1, 0x3f333333    # 0.7f

    .line 144
    .line 145
    .line 146
    mul-float v1, v1, p0

    .line 147
    .line 148
    if-eqz v5, :cond_4

    .line 149
    .line 150
    array-length p0, v5

    .line 151
    if-lt p0, v8, :cond_4

    .line 152
    .line 153
    aget-object p0, v5, v7

    .line 154
    .line 155
    if-eqz p0, :cond_3

    .line 156
    .line 157
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 158
    .line 159
    .line 160
    move-result p0

    .line 161
    int-to-float p0, p0

    .line 162
    aget-object v0, v5, v7

    .line 163
    .line 164
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 165
    .line 166
    .line 167
    move-result v0

    .line 168
    mul-float p0, p0, v0

    .line 169
    .line 170
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 171
    .line 172
    .line 173
    move-result v0

    .line 174
    div-int/2addr v0, v8

    .line 175
    int-to-float v0, v0

    .line 176
    sub-float/2addr p0, v0

    .line 177
    move v0, p0

    .line 178
    :cond_3
    aget-object p0, v5, v6

    .line 179
    .line 180
    if-eqz p0, :cond_4

    .line 181
    .line 182
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 183
    .line 184
    .line 185
    move-result p0

    .line 186
    int-to-float p0, p0

    .line 187
    aget-object v1, v5, v6

    .line 188
    .line 189
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 190
    .line 191
    .line 192
    move-result v1

    .line 193
    mul-float p0, p0, v1

    .line 194
    .line 195
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 196
    .line 197
    .line 198
    move-result v1

    .line 199
    div-int/2addr v1, v8

    .line 200
    int-to-float v1, v1

    .line 201
    sub-float v1, p0, v1

    .line 202
    .line 203
    :cond_4
    :goto_0
    div-float p0, v1, v0

    .line 204
    .line 205
    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    .line 206
    .line 207
    .line 208
    move-result p0

    .line 209
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 210
    .line 211
    .line 212
    move-result v5

    .line 213
    int-to-float v5, v5

    .line 214
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 215
    .line 216
    .line 217
    move-result v6

    .line 218
    int-to-float v6, v6

    .line 219
    div-float/2addr v5, v6

    .line 220
    const/high16 v6, 0x43870000    # 270.0f

    .line 221
    .line 222
    const/high16 v7, 0x43340000    # 180.0f

    .line 223
    .line 224
    const/4 v8, 0x0

    .line 225
    const/high16 v9, 0x40000000    # 2.0f

    .line 226
    .line 227
    const/high16 v10, 0x42b40000    # 90.0f

    .line 228
    .line 229
    cmpg-float p0, p0, v5

    .line 230
    .line 231
    if-gez p0, :cond_8

    .line 232
    .line 233
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 234
    .line 235
    .line 236
    move-result p0

    .line 237
    int-to-float p0, p0

    .line 238
    div-float/2addr p0, v3

    .line 239
    cmpl-float v2, v0, v8

    .line 240
    .line 241
    if-ltz v2, :cond_6

    .line 242
    .line 243
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 244
    .line 245
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 246
    .line 247
    int-to-float v5, v3

    .line 248
    iget v11, p1, Landroid/graphics/Rect;->top:I

    .line 249
    .line 250
    int-to-float v12, v11

    .line 251
    int-to-float v3, v3

    .line 252
    mul-float v4, v4, v9

    .line 253
    .line 254
    add-float/2addr v3, v4

    .line 255
    int-to-float v11, v11

    .line 256
    add-float/2addr v11, v4

    .line 257
    invoke-virtual {v2, v5, v12, v3, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 258
    .line 259
    .line 260
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 261
    .line 262
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 263
    .line 264
    invoke-virtual {v2, v3, v7, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 265
    .line 266
    .line 267
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 268
    .line 269
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 270
    .line 271
    int-to-float v5, v3

    .line 272
    sub-float/2addr v5, v4

    .line 273
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 274
    .line 275
    int-to-float v11, v7

    .line 276
    int-to-float v3, v3

    .line 277
    int-to-float v7, v7

    .line 278
    add-float/2addr v7, v4

    .line 279
    invoke-virtual {v2, v5, v11, v3, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 280
    .line 281
    .line 282
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 283
    .line 284
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 285
    .line 286
    invoke-virtual {v2, v3, v6, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 287
    .line 288
    .line 289
    cmpl-float v2, v1, v8

    .line 290
    .line 291
    if-ltz v2, :cond_5

    .line 292
    .line 293
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 294
    .line 295
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 296
    .line 297
    int-to-float v3, v3

    .line 298
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 299
    .line 300
    .line 301
    move-result v5

    .line 302
    add-float/2addr v5, p0

    .line 303
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 304
    .line 305
    .line 306
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 307
    .line 308
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 309
    .line 310
    .line 311
    move-result v3

    .line 312
    add-float/2addr v3, v0

    .line 313
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 314
    .line 315
    .line 316
    move-result v0

    .line 317
    add-float/2addr v0, v1

    .line 318
    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 319
    .line 320
    .line 321
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 322
    .line 323
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 324
    .line 325
    int-to-float v1, v1

    .line 326
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 327
    .line 328
    int-to-float v2, v2

    .line 329
    mul-float p0, p0, v9

    .line 330
    .line 331
    sub-float/2addr v2, p0

    .line 332
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 333
    .line 334
    .line 335
    goto :goto_1

    .line 336
    :cond_5
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 337
    .line 338
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 339
    .line 340
    int-to-float v3, v3

    .line 341
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 342
    .line 343
    int-to-float v5, v5

    .line 344
    mul-float v9, v9, p0

    .line 345
    .line 346
    add-float/2addr v5, v9

    .line 347
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 348
    .line 349
    .line 350
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 351
    .line 352
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 353
    .line 354
    .line 355
    move-result v3

    .line 356
    add-float/2addr v3, v0

    .line 357
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 358
    .line 359
    .line 360
    move-result v0

    .line 361
    add-float/2addr v0, v1

    .line 362
    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 363
    .line 364
    .line 365
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 366
    .line 367
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 368
    .line 369
    int-to-float v1, v1

    .line 370
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 371
    .line 372
    .line 373
    move-result v2

    .line 374
    sub-float/2addr v2, p0

    .line 375
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 376
    .line 377
    .line 378
    :goto_1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 379
    .line 380
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 381
    .line 382
    int-to-float v1, v0

    .line 383
    sub-float/2addr v1, v4

    .line 384
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 385
    .line 386
    int-to-float v3, v2

    .line 387
    sub-float/2addr v3, v4

    .line 388
    int-to-float v0, v0

    .line 389
    int-to-float v2, v2

    .line 390
    invoke-virtual {p0, v1, v3, v0, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 391
    .line 392
    .line 393
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 394
    .line 395
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 396
    .line 397
    invoke-virtual {p0, v0, v8, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 398
    .line 399
    .line 400
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 401
    .line 402
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 403
    .line 404
    int-to-float v1, v0

    .line 405
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 406
    .line 407
    int-to-float v2, p1

    .line 408
    sub-float/2addr v2, v4

    .line 409
    int-to-float v0, v0

    .line 410
    add-float/2addr v0, v4

    .line 411
    int-to-float p1, p1

    .line 412
    invoke-virtual {p0, v1, v2, v0, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 413
    .line 414
    .line 415
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 416
    .line 417
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 418
    .line 419
    invoke-virtual {p0, p1, v10, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 420
    .line 421
    .line 422
    goto/16 :goto_4

    .line 423
    .line 424
    :cond_6
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 425
    .line 426
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 427
    .line 428
    int-to-float v5, v3

    .line 429
    iget v11, p1, Landroid/graphics/Rect;->top:I

    .line 430
    .line 431
    int-to-float v12, v11

    .line 432
    int-to-float v3, v3

    .line 433
    mul-float v4, v4, v9

    .line 434
    .line 435
    add-float/2addr v3, v4

    .line 436
    int-to-float v11, v11

    .line 437
    add-float/2addr v11, v4

    .line 438
    invoke-virtual {v2, v5, v12, v3, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 439
    .line 440
    .line 441
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 442
    .line 443
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 444
    .line 445
    invoke-virtual {v2, v3, v7, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 446
    .line 447
    .line 448
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 449
    .line 450
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 451
    .line 452
    int-to-float v5, v3

    .line 453
    sub-float/2addr v5, v4

    .line 454
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 455
    .line 456
    int-to-float v11, v7

    .line 457
    int-to-float v3, v3

    .line 458
    int-to-float v7, v7

    .line 459
    add-float/2addr v7, v4

    .line 460
    invoke-virtual {v2, v5, v11, v3, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 461
    .line 462
    .line 463
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 464
    .line 465
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 466
    .line 467
    invoke-virtual {v2, v3, v6, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 468
    .line 469
    .line 470
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 471
    .line 472
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 473
    .line 474
    int-to-float v5, v3

    .line 475
    sub-float/2addr v5, v4

    .line 476
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 477
    .line 478
    int-to-float v7, v6

    .line 479
    sub-float/2addr v7, v4

    .line 480
    int-to-float v3, v3

    .line 481
    int-to-float v6, v6

    .line 482
    invoke-virtual {v2, v5, v7, v3, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 483
    .line 484
    .line 485
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 486
    .line 487
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 488
    .line 489
    invoke-virtual {v2, v3, v8, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 490
    .line 491
    .line 492
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 493
    .line 494
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 495
    .line 496
    int-to-float v5, v3

    .line 497
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 498
    .line 499
    int-to-float v7, v6

    .line 500
    sub-float/2addr v7, v4

    .line 501
    int-to-float v3, v3

    .line 502
    add-float/2addr v3, v4

    .line 503
    int-to-float v4, v6

    .line 504
    invoke-virtual {v2, v5, v7, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 505
    .line 506
    .line 507
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 508
    .line 509
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 510
    .line 511
    invoke-virtual {v2, v3, v10, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 512
    .line 513
    .line 514
    cmpl-float v2, v1, v8

    .line 515
    .line 516
    if-ltz v2, :cond_7

    .line 517
    .line 518
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 519
    .line 520
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 521
    .line 522
    int-to-float v3, v3

    .line 523
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 524
    .line 525
    int-to-float v4, v4

    .line 526
    mul-float v9, v9, p0

    .line 527
    .line 528
    sub-float/2addr v4, v9

    .line 529
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 530
    .line 531
    .line 532
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 533
    .line 534
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 535
    .line 536
    .line 537
    move-result v3

    .line 538
    add-float/2addr v3, v0

    .line 539
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 540
    .line 541
    .line 542
    move-result v0

    .line 543
    add-float/2addr v0, v1

    .line 544
    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 545
    .line 546
    .line 547
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 548
    .line 549
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 550
    .line 551
    int-to-float v1, v1

    .line 552
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 553
    .line 554
    .line 555
    move-result p1

    .line 556
    add-float/2addr p1, p0

    .line 557
    invoke-virtual {v0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 558
    .line 559
    .line 560
    goto/16 :goto_4

    .line 561
    .line 562
    :cond_7
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 563
    .line 564
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 565
    .line 566
    int-to-float v3, v3

    .line 567
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 568
    .line 569
    .line 570
    move-result v4

    .line 571
    sub-float/2addr v4, p0

    .line 572
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 573
    .line 574
    .line 575
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 576
    .line 577
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 578
    .line 579
    .line 580
    move-result v3

    .line 581
    add-float/2addr v3, v0

    .line 582
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 583
    .line 584
    .line 585
    move-result v0

    .line 586
    add-float/2addr v0, v1

    .line 587
    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 588
    .line 589
    .line 590
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 591
    .line 592
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 593
    .line 594
    int-to-float v1, v1

    .line 595
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 596
    .line 597
    int-to-float p1, p1

    .line 598
    mul-float p0, p0, v9

    .line 599
    .line 600
    add-float/2addr p1, p0

    .line 601
    invoke-virtual {v0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 602
    .line 603
    .line 604
    goto/16 :goto_4

    .line 605
    .line 606
    :cond_8
    cmpl-float p0, v1, v8

    .line 607
    .line 608
    if-ltz p0, :cond_a

    .line 609
    .line 610
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 611
    .line 612
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 613
    .line 614
    int-to-float v5, v3

    .line 615
    iget v11, p1, Landroid/graphics/Rect;->top:I

    .line 616
    .line 617
    int-to-float v12, v11

    .line 618
    int-to-float v3, v3

    .line 619
    mul-float v4, v4, v9

    .line 620
    .line 621
    add-float/2addr v3, v4

    .line 622
    int-to-float v11, v11

    .line 623
    add-float/2addr v11, v4

    .line 624
    invoke-virtual {p0, v5, v12, v3, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 625
    .line 626
    .line 627
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 628
    .line 629
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 630
    .line 631
    invoke-virtual {p0, v3, v7, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 632
    .line 633
    .line 634
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 635
    .line 636
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 637
    .line 638
    int-to-float v5, v3

    .line 639
    sub-float/2addr v5, v4

    .line 640
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 641
    .line 642
    int-to-float v11, v7

    .line 643
    int-to-float v3, v3

    .line 644
    int-to-float v7, v7

    .line 645
    add-float/2addr v7, v4

    .line 646
    invoke-virtual {p0, v5, v11, v3, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 647
    .line 648
    .line 649
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 650
    .line 651
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 652
    .line 653
    invoke-virtual {p0, v3, v6, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 654
    .line 655
    .line 656
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 657
    .line 658
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 659
    .line 660
    int-to-float v5, v3

    .line 661
    sub-float/2addr v5, v4

    .line 662
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 663
    .line 664
    int-to-float v7, v6

    .line 665
    sub-float/2addr v7, v4

    .line 666
    int-to-float v3, v3

    .line 667
    int-to-float v6, v6

    .line 668
    invoke-virtual {p0, v5, v7, v3, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 669
    .line 670
    .line 671
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 672
    .line 673
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 674
    .line 675
    invoke-virtual {p0, v3, v8, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 676
    .line 677
    .line 678
    cmpl-float p0, v0, v8

    .line 679
    .line 680
    if-ltz p0, :cond_9

    .line 681
    .line 682
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 683
    .line 684
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 685
    .line 686
    int-to-float v3, v3

    .line 687
    mul-float v9, v9, v2

    .line 688
    .line 689
    sub-float/2addr v3, v9

    .line 690
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 691
    .line 692
    int-to-float v5, v5

    .line 693
    invoke-virtual {p0, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 694
    .line 695
    .line 696
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 697
    .line 698
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 699
    .line 700
    .line 701
    move-result v3

    .line 702
    add-float/2addr v3, v0

    .line 703
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 704
    .line 705
    .line 706
    move-result v0

    .line 707
    add-float/2addr v0, v1

    .line 708
    invoke-virtual {p0, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 709
    .line 710
    .line 711
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 712
    .line 713
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 714
    .line 715
    .line 716
    move-result v0

    .line 717
    add-float/2addr v0, v2

    .line 718
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 719
    .line 720
    int-to-float v1, v1

    .line 721
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 722
    .line 723
    .line 724
    goto :goto_2

    .line 725
    :cond_9
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 726
    .line 727
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 728
    .line 729
    .line 730
    move-result v3

    .line 731
    sub-float/2addr v3, v2

    .line 732
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 733
    .line 734
    int-to-float v5, v5

    .line 735
    invoke-virtual {p0, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 736
    .line 737
    .line 738
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 739
    .line 740
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 741
    .line 742
    .line 743
    move-result v3

    .line 744
    add-float/2addr v3, v0

    .line 745
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 746
    .line 747
    .line 748
    move-result v0

    .line 749
    add-float/2addr v0, v1

    .line 750
    invoke-virtual {p0, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 751
    .line 752
    .line 753
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 754
    .line 755
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 756
    .line 757
    int-to-float v0, v0

    .line 758
    mul-float v2, v2, v9

    .line 759
    .line 760
    add-float/2addr v0, v2

    .line 761
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 762
    .line 763
    int-to-float v1, v1

    .line 764
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 765
    .line 766
    .line 767
    :goto_2
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 768
    .line 769
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 770
    .line 771
    int-to-float v1, v0

    .line 772
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 773
    .line 774
    int-to-float v2, p1

    .line 775
    sub-float/2addr v2, v4

    .line 776
    int-to-float v0, v0

    .line 777
    add-float/2addr v0, v4

    .line 778
    int-to-float p1, p1

    .line 779
    invoke-virtual {p0, v1, v2, v0, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 780
    .line 781
    .line 782
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 783
    .line 784
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 785
    .line 786
    invoke-virtual {p0, p1, v10, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 787
    .line 788
    .line 789
    goto/16 :goto_4

    .line 790
    .line 791
    :cond_a
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 792
    .line 793
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 794
    .line 795
    int-to-float v5, v3

    .line 796
    iget v11, p1, Landroid/graphics/Rect;->top:I

    .line 797
    .line 798
    int-to-float v12, v11

    .line 799
    int-to-float v3, v3

    .line 800
    mul-float v4, v4, v9

    .line 801
    .line 802
    add-float/2addr v3, v4

    .line 803
    int-to-float v11, v11

    .line 804
    add-float/2addr v11, v4

    .line 805
    invoke-virtual {p0, v5, v12, v3, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 806
    .line 807
    .line 808
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 809
    .line 810
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 811
    .line 812
    invoke-virtual {p0, v3, v7, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 813
    .line 814
    .line 815
    cmpl-float p0, v0, v8

    .line 816
    .line 817
    if-ltz p0, :cond_b

    .line 818
    .line 819
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 820
    .line 821
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 822
    .line 823
    .line 824
    move-result v3

    .line 825
    add-float/2addr v3, v2

    .line 826
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 827
    .line 828
    int-to-float v5, v5

    .line 829
    invoke-virtual {p0, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 830
    .line 831
    .line 832
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 833
    .line 834
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 835
    .line 836
    .line 837
    move-result v3

    .line 838
    add-float/2addr v3, v0

    .line 839
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 840
    .line 841
    .line 842
    move-result v0

    .line 843
    add-float/2addr v0, v1

    .line 844
    invoke-virtual {p0, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 845
    .line 846
    .line 847
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 848
    .line 849
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 850
    .line 851
    int-to-float v0, v0

    .line 852
    mul-float v2, v2, v9

    .line 853
    .line 854
    sub-float/2addr v0, v2

    .line 855
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 856
    .line 857
    int-to-float v1, v1

    .line 858
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 859
    .line 860
    .line 861
    goto :goto_3

    .line 862
    :cond_b
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 863
    .line 864
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 865
    .line 866
    int-to-float v3, v3

    .line 867
    mul-float v9, v9, v2

    .line 868
    .line 869
    add-float/2addr v3, v9

    .line 870
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 871
    .line 872
    int-to-float v5, v5

    .line 873
    invoke-virtual {p0, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 874
    .line 875
    .line 876
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 877
    .line 878
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 879
    .line 880
    .line 881
    move-result v3

    .line 882
    add-float/2addr v3, v0

    .line 883
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 884
    .line 885
    .line 886
    move-result v0

    .line 887
    add-float/2addr v0, v1

    .line 888
    invoke-virtual {p0, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 889
    .line 890
    .line 891
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 892
    .line 893
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 894
    .line 895
    .line 896
    move-result v0

    .line 897
    sub-float/2addr v0, v2

    .line 898
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 899
    .line 900
    int-to-float v1, v1

    .line 901
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 902
    .line 903
    .line 904
    :goto_3
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 905
    .line 906
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 907
    .line 908
    int-to-float v1, v0

    .line 909
    sub-float/2addr v1, v4

    .line 910
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 911
    .line 912
    int-to-float v3, v2

    .line 913
    int-to-float v0, v0

    .line 914
    int-to-float v2, v2

    .line 915
    add-float/2addr v2, v4

    .line 916
    invoke-virtual {p0, v1, v3, v0, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 917
    .line 918
    .line 919
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 920
    .line 921
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 922
    .line 923
    invoke-virtual {p0, v0, v6, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 924
    .line 925
    .line 926
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 927
    .line 928
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 929
    .line 930
    int-to-float v1, v0

    .line 931
    sub-float/2addr v1, v4

    .line 932
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 933
    .line 934
    int-to-float v3, v2

    .line 935
    sub-float/2addr v3, v4

    .line 936
    int-to-float v0, v0

    .line 937
    int-to-float v2, v2

    .line 938
    invoke-virtual {p0, v1, v3, v0, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 939
    .line 940
    .line 941
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 942
    .line 943
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 944
    .line 945
    invoke-virtual {p0, v0, v8, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 946
    .line 947
    .line 948
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 949
    .line 950
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 951
    .line 952
    int-to-float v1, v0

    .line 953
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 954
    .line 955
    int-to-float v2, p1

    .line 956
    sub-float/2addr v2, v4

    .line 957
    int-to-float v0, v0

    .line 958
    add-float/2addr v0, v4

    .line 959
    int-to-float p1, p1

    .line 960
    invoke-virtual {p0, v1, v2, v0, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 961
    .line 962
    .line 963
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 964
    .line 965
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->rectF:Landroid/graphics/RectF;

    .line 966
    .line 967
    invoke-virtual {p0, p1, v10, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 968
    .line 969
    .line 970
    :goto_4
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 971
    .line 972
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 973
    .line 974
    .line 975
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 976
    .line 977
    return-object p0
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method public static instance()Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->kit:Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public getWedgeCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/lang/Object;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->paths:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->path:Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    const/16 v1, 0x6a

    .line 16
    .line 17
    if-eq v0, v1, :cond_c

    .line 18
    .line 19
    packed-switch v0, :pswitch_data_0

    .line 20
    .line 21
    .line 22
    packed-switch v0, :pswitch_data_1

    .line 23
    .line 24
    .line 25
    packed-switch v0, :pswitch_data_2

    .line 26
    .line 27
    .line 28
    const/4 p1, 0x0

    .line 29
    return-object p1

    .line 30
    :pswitch_0
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    if-eqz v0, :cond_0

    .line 35
    .line 36
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getAccentBorderCallout1(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    return-object p1

    .line 41
    :cond_0
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03BorderCallout2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    return-object p1

    .line 46
    :pswitch_1
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    if-eqz v0, :cond_1

    .line 51
    .line 52
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getBorderCallout1Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    return-object p1

    .line 57
    :cond_1
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03BorderCallout2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    return-object p1

    .line 62
    :pswitch_2
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    if-eqz v0, :cond_2

    .line 67
    .line 68
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getAccentCallout1Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    return-object p1

    .line 73
    :cond_2
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03AccentCallout1Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    return-object p1

    .line 78
    :pswitch_3
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 79
    .line 80
    .line 81
    move-result v0

    .line 82
    if-eqz v0, :cond_3

    .line 83
    .line 84
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getCallout1(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    return-object p1

    .line 89
    :cond_3
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03AccentCallout1Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    return-object p1

    .line 94
    :pswitch_4
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getWedgeEllipseCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    return-object p1

    .line 99
    :pswitch_5
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getWedgeRoundRectCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    return-object p1

    .line 104
    :pswitch_6
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getWedgeRectCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    return-object p1

    .line 109
    :pswitch_7
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03AccentBorderCallout4(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    return-object p1

    .line 114
    :pswitch_8
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 115
    .line 116
    .line 117
    move-result v0

    .line 118
    if-eqz v0, :cond_4

    .line 119
    .line 120
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getAccentBorderCallout3(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 121
    .line 122
    .line 123
    move-result-object p1

    .line 124
    return-object p1

    .line 125
    :cond_4
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03AccentBorderCallout3(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 126
    .line 127
    .line 128
    move-result-object p1

    .line 129
    return-object p1

    .line 130
    :pswitch_9
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 131
    .line 132
    .line 133
    move-result v0

    .line 134
    if-eqz v0, :cond_5

    .line 135
    .line 136
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getAccentBorderCallout2(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 137
    .line 138
    .line 139
    move-result-object p1

    .line 140
    return-object p1

    .line 141
    :cond_5
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03AccentBorderCallout2(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 142
    .line 143
    .line 144
    move-result-object p1

    .line 145
    return-object p1

    .line 146
    :pswitch_a
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03BorderCallout4Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 147
    .line 148
    .line 149
    move-result-object p1

    .line 150
    return-object p1

    .line 151
    :pswitch_b
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 152
    .line 153
    .line 154
    move-result v0

    .line 155
    if-eqz v0, :cond_6

    .line 156
    .line 157
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getBorderCallout3Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 158
    .line 159
    .line 160
    move-result-object p1

    .line 161
    return-object p1

    .line 162
    :cond_6
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03BorderCallout3Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 163
    .line 164
    .line 165
    move-result-object p1

    .line 166
    return-object p1

    .line 167
    :pswitch_c
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 168
    .line 169
    .line 170
    move-result v0

    .line 171
    if-eqz v0, :cond_7

    .line 172
    .line 173
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getBorderCallout2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 174
    .line 175
    .line 176
    move-result-object p1

    .line 177
    return-object p1

    .line 178
    :cond_7
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03BorderCallout2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 179
    .line 180
    .line 181
    move-result-object p1

    .line 182
    return-object p1

    .line 183
    :pswitch_d
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03AccentCallout4(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 184
    .line 185
    .line 186
    move-result-object p1

    .line 187
    return-object p1

    .line 188
    :pswitch_e
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 189
    .line 190
    .line 191
    move-result v0

    .line 192
    if-eqz v0, :cond_8

    .line 193
    .line 194
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getAccentCallout3Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 195
    .line 196
    .line 197
    move-result-object p1

    .line 198
    return-object p1

    .line 199
    :cond_8
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03AccentCallout3(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 200
    .line 201
    .line 202
    move-result-object p1

    .line 203
    return-object p1

    .line 204
    :pswitch_f
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 205
    .line 206
    .line 207
    move-result v0

    .line 208
    if-eqz v0, :cond_9

    .line 209
    .line 210
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getAccentCallout2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 211
    .line 212
    .line 213
    move-result-object p1

    .line 214
    return-object p1

    .line 215
    :cond_9
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03AccentCallout2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 216
    .line 217
    .line 218
    move-result-object p1

    .line 219
    return-object p1

    .line 220
    :pswitch_10
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03Callout4(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 221
    .line 222
    .line 223
    move-result-object p1

    .line 224
    return-object p1

    .line 225
    :pswitch_11
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 226
    .line 227
    .line 228
    move-result v0

    .line 229
    if-eqz v0, :cond_a

    .line 230
    .line 231
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getCallout3(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 232
    .line 233
    .line 234
    move-result-object p1

    .line 235
    return-object p1

    .line 236
    :cond_a
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03Callout3(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 237
    .line 238
    .line 239
    move-result-object p1

    .line 240
    return-object p1

    .line 241
    :pswitch_12
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 242
    .line 243
    .line 244
    move-result v0

    .line 245
    if-eqz v0, :cond_b

    .line 246
    .line 247
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getCallout2(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 248
    .line 249
    .line 250
    move-result-object p1

    .line 251
    return-object p1

    .line 252
    :cond_b
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->get03Callout2(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 253
    .line 254
    .line 255
    move-result-object p1

    .line 256
    return-object p1

    .line 257
    :cond_c
    invoke-static {p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/wedgecallout/WedgeCalloutDrawing;->getCloudCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 258
    .line 259
    .line 260
    move-result-object p1

    .line 261
    return-object p1

    .line 262
    nop

    .line 263
    :pswitch_data_0
    .packed-switch 0x29
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
    .end packed-switch

    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    :pswitch_data_1
    .packed-switch 0x3d
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    :pswitch_data_2
    .packed-switch 0xb2
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.class public Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;
.super Lcom/intsig/office/common/autoshape/pathbuilder/arrow/ArrowPathBuilder;
.source "EarlyArrowPathBuilder.java"


# static fields
.field private static final TODEGREE:F = 0.3295496f

.field private static path:Landroid/graphics/Path;

.field private static s_rect:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 7
    .line 8
    new-instance v0, Landroid/graphics/Path;

    .line 9
    .line 10
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/ArrowPathBuilder;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static getArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/lang/Object;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/16 v1, 0xd

    .line 11
    .line 12
    if-eq v0, v1, :cond_6

    .line 13
    .line 14
    const/16 v1, 0xf

    .line 15
    .line 16
    if-eq v0, v1, :cond_5

    .line 17
    .line 18
    const/16 v1, 0x37

    .line 19
    .line 20
    if-eq v0, v1, :cond_4

    .line 21
    .line 22
    const/16 v1, 0x63

    .line 23
    .line 24
    if-eq v0, v1, :cond_3

    .line 25
    .line 26
    const/16 v1, 0xb6

    .line 27
    .line 28
    if-eq v0, v1, :cond_2

    .line 29
    .line 30
    const/16 v1, 0x5d

    .line 31
    .line 32
    if-eq v0, v1, :cond_1

    .line 33
    .line 34
    const/16 v1, 0x5e

    .line 35
    .line 36
    if-eq v0, v1, :cond_0

    .line 37
    .line 38
    packed-switch v0, :pswitch_data_0

    .line 39
    .line 40
    .line 41
    packed-switch v0, :pswitch_data_1

    .line 42
    .line 43
    .line 44
    packed-switch v0, :pswitch_data_2

    .line 45
    .line 46
    .line 47
    packed-switch v0, :pswitch_data_3

    .line 48
    .line 49
    .line 50
    new-instance p0, Landroid/graphics/Path;

    .line 51
    .line 52
    invoke-direct {p0}, Landroid/graphics/Path;-><init>()V

    .line 53
    .line 54
    .line 55
    return-object p0

    .line 56
    :pswitch_0
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getCurvedDownArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 57
    .line 58
    .line 59
    move-result-object p0

    .line 60
    return-object p0

    .line 61
    :pswitch_1
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getCurvedUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 62
    .line 63
    .line 64
    move-result-object p0

    .line 65
    return-object p0

    .line 66
    :pswitch_2
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getCurvedLeftArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 67
    .line 68
    .line 69
    move-result-object p0

    .line 70
    return-object p0

    .line 71
    :pswitch_3
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getCurvedRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 72
    .line 73
    .line 74
    move-result-object p0

    .line 75
    return-object p0

    .line 76
    :pswitch_4
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getUturnArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 77
    .line 78
    .line 79
    move-result-object p0

    .line 80
    return-object p0

    .line 81
    :pswitch_5
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getBentArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 82
    .line 83
    .line 84
    move-result-object p0

    .line 85
    return-object p0

    .line 86
    :pswitch_6
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getBentUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 87
    .line 88
    .line 89
    move-result-object p0

    .line 90
    return-object p0

    .line 91
    :pswitch_7
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getLeftUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 92
    .line 93
    .line 94
    move-result-object p0

    .line 95
    return-object p0

    .line 96
    :pswitch_8
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getQuadArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 97
    .line 98
    .line 99
    move-result-object p0

    .line 100
    return-object p0

    .line 101
    :pswitch_9
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getUpDownArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 102
    .line 103
    .line 104
    move-result-object p0

    .line 105
    return-object p0

    .line 106
    :pswitch_a
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getLeftRightArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 107
    .line 108
    .line 109
    move-result-object p0

    .line 110
    return-object p0

    .line 111
    :pswitch_b
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getDownArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 112
    .line 113
    .line 114
    move-result-object p0

    .line 115
    return-object p0

    .line 116
    :pswitch_c
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getUpArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 117
    .line 118
    .line 119
    move-result-object p0

    .line 120
    return-object p0

    .line 121
    :pswitch_d
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getRightArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 122
    .line 123
    .line 124
    move-result-object p0

    .line 125
    return-object p0

    .line 126
    :pswitch_e
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getLeftArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 127
    .line 128
    .line 129
    move-result-object p0

    .line 130
    return-object p0

    .line 131
    :pswitch_f
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getQuadArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 132
    .line 133
    .line 134
    move-result-object p0

    .line 135
    return-object p0

    .line 136
    :pswitch_10
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getUpDownArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 137
    .line 138
    .line 139
    move-result-object p0

    .line 140
    return-object p0

    .line 141
    :pswitch_11
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getLeftRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 142
    .line 143
    .line 144
    move-result-object p0

    .line 145
    return-object p0

    .line 146
    :pswitch_12
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 147
    .line 148
    .line 149
    move-result-object p0

    .line 150
    return-object p0

    .line 151
    :pswitch_13
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getDownArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 152
    .line 153
    .line 154
    move-result-object p0

    .line 155
    return-object p0

    .line 156
    :pswitch_14
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getLeftArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 157
    .line 158
    .line 159
    move-result-object p0

    .line 160
    return-object p0

    .line 161
    :cond_0
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getNotchedRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 162
    .line 163
    .line 164
    move-result-object p0

    .line 165
    return-object p0

    .line 166
    :cond_1
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getStripedRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 167
    .line 168
    .line 169
    move-result-object p0

    .line 170
    return-object p0

    .line 171
    :cond_2
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getLeftRightUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 172
    .line 173
    .line 174
    move-result-object p0

    .line 175
    return-object p0

    .line 176
    :cond_3
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getCircularArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 177
    .line 178
    .line 179
    move-result-object p0

    .line 180
    return-object p0

    .line 181
    :cond_4
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getChevronPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 182
    .line 183
    .line 184
    move-result-object p0

    .line 185
    return-object p0

    .line 186
    :cond_5
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getHomePlatePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 187
    .line 188
    .line 189
    move-result-object p0

    .line 190
    return-object p0

    .line 191
    :cond_6
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->getRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 192
    .line 193
    .line 194
    move-result-object p0

    .line 195
    return-object p0

    .line 196
    nop

    .line 197
    :pswitch_data_0
    .packed-switch 0x42
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
    .end packed-switch

    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    :pswitch_data_1
    .packed-switch 0x4c
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
    .end packed-switch

    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    :pswitch_data_2
    .packed-switch 0x59
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch

    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    :pswitch_data_3
    .packed-switch 0x65
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getBentArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 9

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/4 v0, 0x2

    .line 6
    const/high16 v1, 0x3e000000    # 0.125f

    .line 7
    .line 8
    const v2, 0x3f333333    # 0.7f

    .line 9
    .line 10
    .line 11
    if-eqz p0, :cond_2

    .line 12
    .line 13
    array-length v3, p0

    .line 14
    const/4 v4, 0x1

    .line 15
    if-lt v3, v4, :cond_2

    .line 16
    .line 17
    const/4 v3, 0x0

    .line 18
    aget-object v5, p0, v3

    .line 19
    .line 20
    if-eqz v5, :cond_0

    .line 21
    .line 22
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    int-to-float v2, v2

    .line 27
    aget-object v3, p0, v3

    .line 28
    .line 29
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 30
    .line 31
    .line 32
    move-result v3

    .line 33
    mul-float v2, v2, v3

    .line 34
    .line 35
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    goto :goto_0

    .line 40
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    int-to-float v3, v3

    .line 45
    mul-float v3, v3, v2

    .line 46
    .line 47
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 48
    .line 49
    .line 50
    move-result v2

    .line 51
    :goto_0
    array-length v3, p0

    .line 52
    if-lt v3, v0, :cond_1

    .line 53
    .line 54
    aget-object v3, p0, v4

    .line 55
    .line 56
    if-eqz v3, :cond_1

    .line 57
    .line 58
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 59
    .line 60
    .line 61
    move-result v1

    .line 62
    int-to-float v1, v1

    .line 63
    aget-object p0, p0, v4

    .line 64
    .line 65
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 66
    .line 67
    .line 68
    move-result p0

    .line 69
    mul-float v1, v1, p0

    .line 70
    .line 71
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 72
    .line 73
    .line 74
    move-result p0

    .line 75
    goto :goto_1

    .line 76
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 77
    .line 78
    .line 79
    move-result p0

    .line 80
    int-to-float p0, p0

    .line 81
    mul-float p0, p0, v1

    .line 82
    .line 83
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 84
    .line 85
    .line 86
    move-result p0

    .line 87
    goto :goto_1

    .line 88
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 89
    .line 90
    .line 91
    move-result p0

    .line 92
    int-to-float p0, p0

    .line 93
    mul-float p0, p0, v2

    .line 94
    .line 95
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 96
    .line 97
    .line 98
    move-result v2

    .line 99
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 100
    .line 101
    .line 102
    move-result p0

    .line 103
    int-to-float p0, p0

    .line 104
    mul-float p0, p0, v1

    .line 105
    .line 106
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 107
    .line 108
    .line 109
    move-result p0

    .line 110
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 111
    .line 112
    .line 113
    move-result v1

    .line 114
    int-to-float v1, v1

    .line 115
    const v3, 0x3f11eb85    # 0.57f

    .line 116
    .line 117
    .line 118
    mul-float v1, v1, v3

    .line 119
    .line 120
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 121
    .line 122
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 123
    .line 124
    int-to-float v4, v4

    .line 125
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 126
    .line 127
    int-to-float v5, v5

    .line 128
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 129
    .line 130
    .line 131
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 132
    .line 133
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 134
    .line 135
    int-to-float v4, v4

    .line 136
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 137
    .line 138
    int-to-float v5, v5

    .line 139
    add-float/2addr v5, v1

    .line 140
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 141
    .line 142
    .line 143
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 144
    .line 145
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 146
    .line 147
    int-to-float v5, v4

    .line 148
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 149
    .line 150
    add-int/2addr v6, p0

    .line 151
    int-to-float v6, v6

    .line 152
    int-to-float v4, v4

    .line 153
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 154
    .line 155
    .line 156
    move-result v7

    .line 157
    int-to-float v7, v7

    .line 158
    const v8, 0x3f851eb8    # 1.04f

    .line 159
    .line 160
    .line 161
    mul-float v7, v7, v8

    .line 162
    .line 163
    add-float/2addr v4, v7

    .line 164
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 165
    .line 166
    int-to-float v7, v7

    .line 167
    add-float/2addr v7, v1

    .line 168
    int-to-float v8, p0

    .line 169
    sub-float v8, v1, v8

    .line 170
    .line 171
    add-float/2addr v7, v8

    .line 172
    invoke-virtual {v3, v5, v6, v4, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 173
    .line 174
    .line 175
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 176
    .line 177
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 178
    .line 179
    const/high16 v5, 0x43340000    # 180.0f

    .line 180
    .line 181
    const/high16 v6, 0x42b40000    # 90.0f

    .line 182
    .line 183
    invoke-virtual {v3, v4, v5, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 184
    .line 185
    .line 186
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 187
    .line 188
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 189
    .line 190
    add-int/2addr v4, v2

    .line 191
    int-to-float v4, v4

    .line 192
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 193
    .line 194
    add-int/2addr v5, p0

    .line 195
    int-to-float v5, v5

    .line 196
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 197
    .line 198
    .line 199
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 200
    .line 201
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 202
    .line 203
    add-int/2addr v4, v2

    .line 204
    int-to-float v4, v4

    .line 205
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 206
    .line 207
    int-to-float v5, v5

    .line 208
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 209
    .line 210
    .line 211
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 212
    .line 213
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 214
    .line 215
    int-to-float v4, v4

    .line 216
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 217
    .line 218
    int-to-float v5, v5

    .line 219
    const/high16 v6, 0x40000000    # 2.0f

    .line 220
    .line 221
    div-float v7, v1, v6

    .line 222
    .line 223
    add-float/2addr v5, v7

    .line 224
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 225
    .line 226
    .line 227
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 228
    .line 229
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 230
    .line 231
    add-int/2addr v4, v2

    .line 232
    int-to-float v4, v4

    .line 233
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 234
    .line 235
    int-to-float v5, v5

    .line 236
    add-float/2addr v5, v1

    .line 237
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 238
    .line 239
    .line 240
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 241
    .line 242
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 243
    .line 244
    add-int/2addr v4, v2

    .line 245
    int-to-float v2, v4

    .line 246
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 247
    .line 248
    int-to-float v4, v4

    .line 249
    add-float/2addr v4, v7

    .line 250
    mul-int/lit8 p0, p0, 0x2

    .line 251
    .line 252
    int-to-float p0, p0

    .line 253
    sub-float p0, v1, p0

    .line 254
    .line 255
    div-float v0, p0, v6

    .line 256
    .line 257
    add-float/2addr v4, v0

    .line 258
    invoke-virtual {v3, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 259
    .line 260
    .line 261
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 262
    .line 263
    .line 264
    move-result v2

    .line 265
    int-to-float v2, v2

    .line 266
    div-float/2addr p0, v2

    .line 267
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 268
    .line 269
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 270
    .line 271
    int-to-float v3, v3

    .line 272
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 273
    .line 274
    .line 275
    move-result v4

    .line 276
    int-to-float v4, v4

    .line 277
    mul-float v4, v4, p0

    .line 278
    .line 279
    add-float/2addr v3, v4

    .line 280
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 281
    .line 282
    int-to-float v4, v4

    .line 283
    add-float/2addr v4, v7

    .line 284
    add-float/2addr v4, v0

    .line 285
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 286
    .line 287
    int-to-float v5, v5

    .line 288
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 289
    .line 290
    .line 291
    move-result v6

    .line 292
    int-to-float v6, v6

    .line 293
    const v8, 0x3f91eb85    # 1.14f

    .line 294
    .line 295
    .line 296
    sub-float/2addr v8, p0

    .line 297
    mul-float v6, v6, v8

    .line 298
    .line 299
    add-float/2addr v5, v6

    .line 300
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 301
    .line 302
    int-to-float v6, v6

    .line 303
    add-float/2addr v6, v1

    .line 304
    add-float/2addr v6, v1

    .line 305
    add-float/2addr v7, v0

    .line 306
    sub-float/2addr v6, v7

    .line 307
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 308
    .line 309
    .line 310
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 311
    .line 312
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 313
    .line 314
    const/high16 v2, 0x43870000    # 270.0f

    .line 315
    .line 316
    const/high16 v3, -0x3d4c0000    # -90.0f

    .line 317
    .line 318
    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 319
    .line 320
    .line 321
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 322
    .line 323
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 324
    .line 325
    int-to-float v1, v1

    .line 326
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 327
    .line 328
    .line 329
    move-result v2

    .line 330
    int-to-float v2, v2

    .line 331
    mul-float v2, v2, p0

    .line 332
    .line 333
    add-float/2addr v1, v2

    .line 334
    iget p0, p1, Landroid/graphics/Rect;->bottom:I

    .line 335
    .line 336
    int-to-float p0, p0

    .line 337
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 338
    .line 339
    .line 340
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 341
    .line 342
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 343
    .line 344
    .line 345
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 346
    .line 347
    return-object p0
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getBentUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const v0, 0x3e8f5c29    # 0.28f

    .line 6
    .line 7
    .line 8
    const v1, 0x3f5c28f6    # 0.86f

    .line 9
    .line 10
    .line 11
    const v2, 0x3edc28f6    # 0.43f

    .line 12
    .line 13
    .line 14
    if-eqz p0, :cond_3

    .line 15
    .line 16
    array-length v3, p0

    .line 17
    const/4 v4, 0x1

    .line 18
    if-lt v3, v4, :cond_3

    .line 19
    .line 20
    const/4 v3, 0x0

    .line 21
    aget-object v5, p0, v3

    .line 22
    .line 23
    if-eqz v5, :cond_0

    .line 24
    .line 25
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    int-to-float v2, v2

    .line 30
    aget-object v3, p0, v3

    .line 31
    .line 32
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    mul-float v2, v2, v3

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    int-to-float v3, v3

    .line 44
    mul-float v3, v3, v2

    .line 45
    .line 46
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    int-to-float v2, v2

    .line 51
    :goto_0
    array-length v3, p0

    .line 52
    const/4 v5, 0x2

    .line 53
    if-lt v3, v5, :cond_1

    .line 54
    .line 55
    aget-object v3, p0, v4

    .line 56
    .line 57
    if-eqz v3, :cond_1

    .line 58
    .line 59
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 60
    .line 61
    .line 62
    move-result v1

    .line 63
    int-to-float v1, v1

    .line 64
    aget-object v3, p0, v4

    .line 65
    .line 66
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 67
    .line 68
    .line 69
    move-result v3

    .line 70
    mul-float v1, v1, v3

    .line 71
    .line 72
    goto :goto_1

    .line 73
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 74
    .line 75
    .line 76
    move-result v3

    .line 77
    int-to-float v3, v3

    .line 78
    mul-float v3, v3, v1

    .line 79
    .line 80
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 81
    .line 82
    .line 83
    move-result v1

    .line 84
    int-to-float v1, v1

    .line 85
    :goto_1
    array-length v3, p0

    .line 86
    const/4 v4, 0x3

    .line 87
    if-lt v3, v4, :cond_2

    .line 88
    .line 89
    aget-object v3, p0, v5

    .line 90
    .line 91
    if-eqz v3, :cond_2

    .line 92
    .line 93
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    int-to-float v0, v0

    .line 98
    aget-object p0, p0, v5

    .line 99
    .line 100
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 101
    .line 102
    .line 103
    move-result p0

    .line 104
    mul-float v0, v0, p0

    .line 105
    .line 106
    goto :goto_3

    .line 107
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 108
    .line 109
    .line 110
    move-result p0

    .line 111
    int-to-float p0, p0

    .line 112
    mul-float p0, p0, v0

    .line 113
    .line 114
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 115
    .line 116
    .line 117
    move-result p0

    .line 118
    goto :goto_2

    .line 119
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 120
    .line 121
    .line 122
    move-result p0

    .line 123
    int-to-float p0, p0

    .line 124
    mul-float p0, p0, v2

    .line 125
    .line 126
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 127
    .line 128
    .line 129
    move-result p0

    .line 130
    int-to-float v2, p0

    .line 131
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 132
    .line 133
    .line 134
    move-result p0

    .line 135
    int-to-float p0, p0

    .line 136
    mul-float p0, p0, v1

    .line 137
    .line 138
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 139
    .line 140
    .line 141
    move-result p0

    .line 142
    int-to-float v1, p0

    .line 143
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 144
    .line 145
    .line 146
    move-result p0

    .line 147
    int-to-float p0, p0

    .line 148
    mul-float p0, p0, v0

    .line 149
    .line 150
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 151
    .line 152
    .line 153
    move-result p0

    .line 154
    :goto_2
    int-to-float v0, p0

    .line 155
    :goto_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 156
    .line 157
    .line 158
    move-result p0

    .line 159
    int-to-float p0, p0

    .line 160
    sub-float/2addr p0, v2

    .line 161
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 162
    .line 163
    .line 164
    move-result v3

    .line 165
    int-to-float v3, v3

    .line 166
    sub-float/2addr v3, v1

    .line 167
    const/high16 v4, 0x40000000    # 2.0f

    .line 168
    .line 169
    mul-float v3, v3, v4

    .line 170
    .line 171
    sub-float v3, p0, v3

    .line 172
    .line 173
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 174
    .line 175
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 176
    .line 177
    int-to-float v6, v6

    .line 178
    add-float/2addr v6, v1

    .line 179
    sub-float/2addr v6, v3

    .line 180
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 181
    .line 182
    int-to-float v7, v7

    .line 183
    add-float/2addr v7, v0

    .line 184
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 185
    .line 186
    .line 187
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 188
    .line 189
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 190
    .line 191
    int-to-float v6, v6

    .line 192
    add-float/2addr v6, v2

    .line 193
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 194
    .line 195
    int-to-float v2, v2

    .line 196
    add-float/2addr v2, v0

    .line 197
    invoke-virtual {v5, v6, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 198
    .line 199
    .line 200
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 201
    .line 202
    iget v5, p1, Landroid/graphics/Rect;->right:I

    .line 203
    .line 204
    int-to-float v5, v5

    .line 205
    div-float v6, p0, v4

    .line 206
    .line 207
    sub-float/2addr v5, v6

    .line 208
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 209
    .line 210
    int-to-float v6, v6

    .line 211
    invoke-virtual {v2, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 212
    .line 213
    .line 214
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 215
    .line 216
    iget v5, p1, Landroid/graphics/Rect;->right:I

    .line 217
    .line 218
    int-to-float v5, v5

    .line 219
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 220
    .line 221
    int-to-float v6, v6

    .line 222
    add-float/2addr v6, v0

    .line 223
    invoke-virtual {v2, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 224
    .line 225
    .line 226
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 227
    .line 228
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 229
    .line 230
    int-to-float v5, v5

    .line 231
    add-float/2addr v5, v1

    .line 232
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 233
    .line 234
    int-to-float v6, v6

    .line 235
    add-float/2addr v6, v0

    .line 236
    invoke-virtual {v2, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 237
    .line 238
    .line 239
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 240
    .line 241
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 242
    .line 243
    int-to-float v2, v2

    .line 244
    add-float/2addr v2, v1

    .line 245
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 246
    .line 247
    int-to-float v5, v5

    .line 248
    invoke-virtual {v0, v2, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 249
    .line 250
    .line 251
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 252
    .line 253
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 254
    .line 255
    int-to-float v2, v2

    .line 256
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 257
    .line 258
    int-to-float v5, v5

    .line 259
    invoke-virtual {v0, v2, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 260
    .line 261
    .line 262
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 263
    .line 264
    .line 265
    move-result v0

    .line 266
    int-to-float v0, v0

    .line 267
    mul-float v0, v0, v3

    .line 268
    .line 269
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 270
    .line 271
    .line 272
    move-result v2

    .line 273
    int-to-float v2, v2

    .line 274
    div-float/2addr v0, v2

    .line 275
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 276
    .line 277
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 278
    .line 279
    int-to-float v3, v3

    .line 280
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 281
    .line 282
    int-to-float v5, v5

    .line 283
    sub-float/2addr v5, v0

    .line 284
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 285
    .line 286
    .line 287
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 288
    .line 289
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 290
    .line 291
    int-to-float v3, v3

    .line 292
    add-float/2addr v3, v1

    .line 293
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 294
    .line 295
    .line 296
    move-result v5

    .line 297
    int-to-float v5, v5

    .line 298
    sub-float/2addr v5, v1

    .line 299
    mul-float v5, v5, v4

    .line 300
    .line 301
    sub-float/2addr p0, v5

    .line 302
    sub-float/2addr v3, p0

    .line 303
    iget p0, p1, Landroid/graphics/Rect;->bottom:I

    .line 304
    .line 305
    int-to-float p0, p0

    .line 306
    sub-float/2addr p0, v0

    .line 307
    invoke-virtual {v2, v3, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 308
    .line 309
    .line 310
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 311
    .line 312
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 313
    .line 314
    .line 315
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 316
    .line 317
    return-object p0
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getChevronPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    if-eqz p0, :cond_0

    .line 6
    .line 7
    array-length v0, p0

    .line 8
    const/4 v1, 0x1

    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    aget-object v1, p0, v0

    .line 13
    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    int-to-float v1, v1

    .line 21
    aget-object p0, p0, v0

    .line 22
    .line 23
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 24
    .line 25
    .line 26
    move-result p0

    .line 27
    mul-float v1, v1, p0

    .line 28
    .line 29
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 30
    .line 31
    .line 32
    move-result p0

    .line 33
    goto :goto_0

    .line 34
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 35
    .line 36
    .line 37
    move-result p0

    .line 38
    int-to-float p0, p0

    .line 39
    const/high16 v0, 0x3f400000    # 0.75f

    .line 40
    .line 41
    mul-float p0, p0, v0

    .line 42
    .line 43
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 44
    .line 45
    .line 46
    move-result p0

    .line 47
    :goto_0
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 48
    .line 49
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 50
    .line 51
    int-to-float v1, v1

    .line 52
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 53
    .line 54
    int-to-float v2, v2

    .line 55
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 56
    .line 57
    .line 58
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 59
    .line 60
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 61
    .line 62
    add-int/2addr v1, p0

    .line 63
    int-to-float v1, v1

    .line 64
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 65
    .line 66
    int-to-float v2, v2

    .line 67
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 68
    .line 69
    .line 70
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 71
    .line 72
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 73
    .line 74
    int-to-float v1, v1

    .line 75
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 76
    .line 77
    .line 78
    move-result v2

    .line 79
    int-to-float v2, v2

    .line 80
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 81
    .line 82
    .line 83
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 84
    .line 85
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 86
    .line 87
    add-int/2addr v1, p0

    .line 88
    int-to-float v1, v1

    .line 89
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 90
    .line 91
    int-to-float v2, v2

    .line 92
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 93
    .line 94
    .line 95
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 96
    .line 97
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 98
    .line 99
    int-to-float v1, v1

    .line 100
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 101
    .line 102
    int-to-float v2, v2

    .line 103
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 104
    .line 105
    .line 106
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 107
    .line 108
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 109
    .line 110
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 111
    .line 112
    .line 113
    move-result v2

    .line 114
    add-int/2addr v1, v2

    .line 115
    sub-int/2addr v1, p0

    .line 116
    int-to-float p0, v1

    .line 117
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 118
    .line 119
    .line 120
    move-result p1

    .line 121
    int-to-float p1, p1

    .line 122
    invoke-virtual {v0, p0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 123
    .line 124
    .line 125
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 126
    .line 127
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 128
    .line 129
    .line 130
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 131
    .line 132
    return-object p0
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getCircularArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 17

    .line 1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/high16 v1, 0x3e800000    # 0.25f

    .line 6
    .line 7
    const/high16 v2, 0x43340000    # 180.0f

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    const/high16 v4, 0x43b40000    # 360.0f

    .line 11
    .line 12
    const/16 v5, 0x64

    .line 13
    .line 14
    if-eqz v0, :cond_3

    .line 15
    .line 16
    array-length v6, v0

    .line 17
    const/4 v7, 0x1

    .line 18
    if-lt v6, v7, :cond_3

    .line 19
    .line 20
    const/4 v6, 0x0

    .line 21
    aget-object v6, v0, v6

    .line 22
    .line 23
    const v8, 0x3ea8baba

    .line 24
    .line 25
    .line 26
    if-eqz v6, :cond_0

    .line 27
    .line 28
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    mul-float v2, v2, v8

    .line 33
    .line 34
    cmpg-float v6, v2, v3

    .line 35
    .line 36
    if-gez v6, :cond_0

    .line 37
    .line 38
    add-float/2addr v2, v4

    .line 39
    :cond_0
    array-length v6, v0

    .line 40
    const/4 v9, 0x2

    .line 41
    if-lt v6, v9, :cond_2

    .line 42
    .line 43
    aget-object v6, v0, v7

    .line 44
    .line 45
    if-eqz v6, :cond_2

    .line 46
    .line 47
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 48
    .line 49
    .line 50
    move-result v6

    .line 51
    mul-float v6, v6, v8

    .line 52
    .line 53
    cmpg-float v3, v6, v3

    .line 54
    .line 55
    if-gez v3, :cond_1

    .line 56
    .line 57
    add-float/2addr v6, v4

    .line 58
    :cond_1
    move v3, v6

    .line 59
    :cond_2
    array-length v6, v0

    .line 60
    const/4 v7, 0x3

    .line 61
    if-lt v6, v7, :cond_3

    .line 62
    .line 63
    aget-object v0, v0, v9

    .line 64
    .line 65
    if-eqz v0, :cond_3

    .line 66
    .line 67
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 68
    .line 69
    .line 70
    move-result v0

    .line 71
    int-to-float v1, v5

    .line 72
    goto :goto_0

    .line 73
    :cond_3
    int-to-float v0, v5

    .line 74
    :goto_0
    mul-float v0, v0, v1

    .line 75
    .line 76
    const/16 v1, 0x32

    .line 77
    .line 78
    int-to-float v1, v1

    .line 79
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 80
    .line 81
    float-to-double v7, v1

    .line 82
    float-to-double v9, v2

    .line 83
    const-wide v11, 0x400921fb54442d18L    # Math.PI

    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    mul-double v9, v9, v11

    .line 89
    .line 90
    const-wide v13, 0x4066800000000000L    # 180.0

    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    div-double/2addr v9, v13

    .line 96
    invoke-static {v9, v10}, Ljava/lang/Math;->cos(D)D

    .line 97
    .line 98
    .line 99
    move-result-wide v15

    .line 100
    mul-double v13, v7, v15

    .line 101
    .line 102
    double-to-float v13, v13

    .line 103
    invoke-static {v9, v10}, Ljava/lang/Math;->sin(D)D

    .line 104
    .line 105
    .line 106
    move-result-wide v9

    .line 107
    mul-double v7, v7, v9

    .line 108
    .line 109
    double-to-float v7, v7

    .line 110
    invoke-virtual {v6, v13, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 111
    .line 112
    .line 113
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 114
    .line 115
    neg-float v7, v1

    .line 116
    invoke-virtual {v6, v7, v7, v1, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 117
    .line 118
    .line 119
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 120
    .line 121
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 122
    .line 123
    sub-float v8, v3, v2

    .line 124
    .line 125
    add-float/2addr v8, v4

    .line 126
    rem-float v9, v8, v4

    .line 127
    .line 128
    invoke-virtual {v6, v7, v2, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 129
    .line 130
    .line 131
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 132
    .line 133
    int-to-float v5, v5

    .line 134
    const/high16 v6, 0x3e000000    # 0.125f

    .line 135
    .line 136
    mul-float v5, v5, v6

    .line 137
    .line 138
    add-float v6, v1, v5

    .line 139
    .line 140
    float-to-double v6, v6

    .line 141
    float-to-double v9, v3

    .line 142
    mul-double v9, v9, v11

    .line 143
    .line 144
    const-wide v13, 0x4066800000000000L    # 180.0

    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    div-double/2addr v9, v13

    .line 150
    invoke-static {v9, v10}, Ljava/lang/Math;->cos(D)D

    .line 151
    .line 152
    .line 153
    move-result-wide v13

    .line 154
    mul-double v13, v13, v6

    .line 155
    .line 156
    double-to-float v13, v13

    .line 157
    invoke-static {v9, v10}, Ljava/lang/Math;->sin(D)D

    .line 158
    .line 159
    .line 160
    move-result-wide v14

    .line 161
    mul-double v6, v6, v14

    .line 162
    .line 163
    double-to-float v6, v6

    .line 164
    invoke-virtual {v2, v13, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 165
    .line 166
    .line 167
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 168
    .line 169
    add-float/2addr v1, v0

    .line 170
    const/high16 v6, 0x3f000000    # 0.5f

    .line 171
    .line 172
    mul-float v1, v1, v6

    .line 173
    .line 174
    float-to-double v6, v1

    .line 175
    const/high16 v1, 0x41f00000    # 30.0f

    .line 176
    .line 177
    add-float/2addr v1, v3

    .line 178
    float-to-double v13, v1

    .line 179
    mul-double v13, v13, v11

    .line 180
    .line 181
    const-wide v11, 0x4066800000000000L    # 180.0

    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    div-double/2addr v13, v11

    .line 187
    invoke-static {v13, v14}, Ljava/lang/Math;->cos(D)D

    .line 188
    .line 189
    .line 190
    move-result-wide v11

    .line 191
    mul-double v11, v11, v6

    .line 192
    .line 193
    double-to-float v1, v11

    .line 194
    invoke-static {v13, v14}, Ljava/lang/Math;->sin(D)D

    .line 195
    .line 196
    .line 197
    move-result-wide v11

    .line 198
    mul-double v6, v6, v11

    .line 199
    .line 200
    double-to-float v6, v6

    .line 201
    invoke-virtual {v2, v1, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 202
    .line 203
    .line 204
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 205
    .line 206
    sub-float v2, v0, v5

    .line 207
    .line 208
    float-to-double v5, v2

    .line 209
    invoke-static {v9, v10}, Ljava/lang/Math;->cos(D)D

    .line 210
    .line 211
    .line 212
    move-result-wide v11

    .line 213
    mul-double v11, v11, v5

    .line 214
    .line 215
    double-to-float v2, v11

    .line 216
    invoke-static {v9, v10}, Ljava/lang/Math;->sin(D)D

    .line 217
    .line 218
    .line 219
    move-result-wide v9

    .line 220
    mul-double v5, v5, v9

    .line 221
    .line 222
    double-to-float v5, v5

    .line 223
    invoke-virtual {v1, v2, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 224
    .line 225
    .line 226
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 227
    .line 228
    neg-float v2, v0

    .line 229
    invoke-virtual {v1, v2, v2, v0, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 230
    .line 231
    .line 232
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 233
    .line 234
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 235
    .line 236
    neg-float v2, v8

    .line 237
    rem-float/2addr v2, v4

    .line 238
    invoke-virtual {v0, v1, v3, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 239
    .line 240
    .line 241
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 242
    .line 243
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 244
    .line 245
    .line 246
    new-instance v0, Landroid/graphics/Matrix;

    .line 247
    .line 248
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 249
    .line 250
    .line 251
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 252
    .line 253
    .line 254
    move-result v1

    .line 255
    int-to-float v1, v1

    .line 256
    const/high16 v2, 0x42c80000    # 100.0f

    .line 257
    .line 258
    div-float/2addr v1, v2

    .line 259
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 260
    .line 261
    .line 262
    move-result v3

    .line 263
    int-to-float v3, v3

    .line 264
    div-float/2addr v3, v2

    .line 265
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 266
    .line 267
    .line 268
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 269
    .line 270
    invoke-virtual {v1, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 271
    .line 272
    .line 273
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 274
    .line 275
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 276
    .line 277
    .line 278
    move-result v1

    .line 279
    int-to-float v1, v1

    .line 280
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 281
    .line 282
    .line 283
    move-result v2

    .line 284
    int-to-float v2, v2

    .line 285
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->offset(FF)V

    .line 286
    .line 287
    .line 288
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 289
    .line 290
    return-object v0
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getCurvedDownArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    new-instance v1, Ljava/util/ArrayList;

    .line 4
    .line 5
    const/4 v2, 0x2

    .line 6
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 7
    .line 8
    .line 9
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    new-instance v4, Landroid/graphics/Path;

    .line 14
    .line 15
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 16
    .line 17
    .line 18
    const v5, 0x3f666666    # 0.9f

    .line 19
    .line 20
    .line 21
    const v6, 0x3f19999a    # 0.6f

    .line 22
    .line 23
    .line 24
    if-eqz v3, :cond_3

    .line 25
    .line 26
    array-length v7, v3

    .line 27
    const/4 v8, 0x1

    .line 28
    if-lt v7, v8, :cond_3

    .line 29
    .line 30
    const/4 v7, 0x0

    .line 31
    aget-object v9, v3, v7

    .line 32
    .line 33
    if-eqz v9, :cond_0

    .line 34
    .line 35
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 36
    .line 37
    .line 38
    move-result v6

    .line 39
    int-to-float v6, v6

    .line 40
    aget-object v7, v3, v7

    .line 41
    .line 42
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 43
    .line 44
    .line 45
    move-result v7

    .line 46
    mul-float v6, v6, v7

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 50
    .line 51
    .line 52
    move-result v7

    .line 53
    int-to-float v7, v7

    .line 54
    mul-float v7, v7, v6

    .line 55
    .line 56
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 57
    .line 58
    .line 59
    move-result v6

    .line 60
    int-to-float v6, v6

    .line 61
    :goto_0
    array-length v7, v3

    .line 62
    if-lt v7, v2, :cond_1

    .line 63
    .line 64
    aget-object v7, v3, v8

    .line 65
    .line 66
    if-eqz v7, :cond_1

    .line 67
    .line 68
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 69
    .line 70
    .line 71
    move-result v5

    .line 72
    int-to-float v5, v5

    .line 73
    aget-object v7, v3, v8

    .line 74
    .line 75
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 76
    .line 77
    .line 78
    move-result v7

    .line 79
    mul-float v5, v5, v7

    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 83
    .line 84
    .line 85
    move-result v7

    .line 86
    int-to-float v7, v7

    .line 87
    mul-float v7, v7, v5

    .line 88
    .line 89
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 90
    .line 91
    .line 92
    move-result v5

    .line 93
    int-to-float v5, v5

    .line 94
    :goto_1
    array-length v7, v3

    .line 95
    const/4 v8, 0x3

    .line 96
    if-lt v7, v8, :cond_2

    .line 97
    .line 98
    aget-object v7, v3, v2

    .line 99
    .line 100
    if-eqz v7, :cond_2

    .line 101
    .line 102
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 103
    .line 104
    .line 105
    move-result v7

    .line 106
    int-to-float v7, v7

    .line 107
    aget-object v2, v3, v2

    .line 108
    .line 109
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 110
    .line 111
    .line 112
    move-result v2

    .line 113
    mul-float v7, v7, v2

    .line 114
    .line 115
    goto :goto_3

    .line 116
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 117
    .line 118
    .line 119
    move-result v2

    .line 120
    int-to-float v2, v2

    .line 121
    const v3, 0x3f2aaae3

    .line 122
    .line 123
    .line 124
    mul-float v2, v2, v3

    .line 125
    .line 126
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 127
    .line 128
    .line 129
    move-result v2

    .line 130
    goto :goto_2

    .line 131
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 132
    .line 133
    .line 134
    move-result v2

    .line 135
    int-to-float v2, v2

    .line 136
    mul-float v2, v2, v6

    .line 137
    .line 138
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 139
    .line 140
    .line 141
    move-result v2

    .line 142
    int-to-float v6, v2

    .line 143
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 144
    .line 145
    .line 146
    move-result v2

    .line 147
    int-to-float v2, v2

    .line 148
    mul-float v2, v2, v5

    .line 149
    .line 150
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 151
    .line 152
    .line 153
    move-result v2

    .line 154
    int-to-float v5, v2

    .line 155
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 156
    .line 157
    .line 158
    move-result v2

    .line 159
    int-to-float v2, v2

    .line 160
    const v3, 0x3f2aaab0

    .line 161
    .line 162
    .line 163
    mul-float v2, v2, v3

    .line 164
    .line 165
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 166
    .line 167
    .line 168
    move-result v2

    .line 169
    :goto_2
    int-to-float v7, v2

    .line 170
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 171
    .line 172
    .line 173
    move-result v2

    .line 174
    int-to-float v2, v2

    .line 175
    const v3, 0x3ecccccd    # 0.4f

    .line 176
    .line 177
    .line 178
    mul-float v2, v2, v3

    .line 179
    .line 180
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 181
    .line 182
    .line 183
    move-result v3

    .line 184
    int-to-float v3, v3

    .line 185
    sub-float/2addr v3, v6

    .line 186
    sub-float/2addr v5, v6

    .line 187
    sub-float v5, v2, v5

    .line 188
    .line 189
    const/high16 v6, 0x40000000    # 2.0f

    .line 190
    .line 191
    mul-float v5, v5, v6

    .line 192
    .line 193
    sub-float/2addr v2, v5

    .line 194
    const/4 v5, 0x0

    .line 195
    cmpg-float v8, v2, v5

    .line 196
    .line 197
    if-gez v8, :cond_4

    .line 198
    .line 199
    const/4 v2, 0x0

    .line 200
    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 201
    .line 202
    .line 203
    move-result v5

    .line 204
    int-to-float v5, v5

    .line 205
    sub-float/2addr v5, v7

    .line 206
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 207
    .line 208
    .line 209
    move-result v7

    .line 210
    int-to-float v7, v7

    .line 211
    div-float/2addr v3, v6

    .line 212
    sub-float/2addr v7, v3

    .line 213
    div-float v8, v2, v6

    .line 214
    .line 215
    sub-float/2addr v7, v8

    .line 216
    div-float/2addr v7, v6

    .line 217
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 218
    .line 219
    .line 220
    move-result v9

    .line 221
    int-to-float v9, v9

    .line 222
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 223
    .line 224
    int-to-float v10, v10

    .line 225
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 226
    .line 227
    int-to-float v11, v11

    .line 228
    invoke-virtual {v4, v10, v11}, Landroid/graphics/Path;->moveTo(FF)V

    .line 229
    .line 230
    .line 231
    sget-object v10, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 232
    .line 233
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 234
    .line 235
    int-to-float v12, v11

    .line 236
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 237
    .line 238
    int-to-float v14, v13

    .line 239
    int-to-float v11, v11

    .line 240
    mul-float v15, v7, v6

    .line 241
    .line 242
    add-float/2addr v11, v15

    .line 243
    int-to-float v13, v13

    .line 244
    mul-float v6, v6, v9

    .line 245
    .line 246
    add-float/2addr v13, v6

    .line 247
    invoke-virtual {v10, v12, v14, v11, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 248
    .line 249
    .line 250
    sget-object v10, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 251
    .line 252
    const/high16 v11, 0x43340000    # 180.0f

    .line 253
    .line 254
    const/high16 v12, 0x42b40000    # 90.0f

    .line 255
    .line 256
    invoke-virtual {v4, v10, v11, v12}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 257
    .line 258
    .line 259
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 260
    .line 261
    int-to-float v10, v10

    .line 262
    add-float/2addr v10, v7

    .line 263
    add-float/2addr v10, v2

    .line 264
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 265
    .line 266
    int-to-float v11, v11

    .line 267
    invoke-virtual {v4, v10, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 268
    .line 269
    .line 270
    sget-object v10, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 271
    .line 272
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 273
    .line 274
    int-to-float v12, v11

    .line 275
    add-float/2addr v12, v2

    .line 276
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 277
    .line 278
    int-to-float v14, v13

    .line 279
    int-to-float v11, v11

    .line 280
    add-float/2addr v11, v15

    .line 281
    add-float/2addr v11, v2

    .line 282
    int-to-float v13, v13

    .line 283
    add-float/2addr v13, v6

    .line 284
    invoke-virtual {v10, v12, v14, v11, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 285
    .line 286
    .line 287
    sget-object v10, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 288
    .line 289
    const/high16 v11, -0x3d4c0000    # -90.0f

    .line 290
    .line 291
    const/high16 v12, 0x43870000    # 270.0f

    .line 292
    .line 293
    invoke-virtual {v4, v10, v12, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 294
    .line 295
    .line 296
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 297
    .line 298
    .line 299
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 300
    .line 301
    .line 302
    new-instance v4, Landroid/graphics/Path;

    .line 303
    .line 304
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 305
    .line 306
    .line 307
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 308
    .line 309
    int-to-float v10, v10

    .line 310
    add-float/2addr v10, v7

    .line 311
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 312
    .line 313
    int-to-float v11, v11

    .line 314
    invoke-virtual {v4, v10, v11}, Landroid/graphics/Path;->moveTo(FF)V

    .line 315
    .line 316
    .line 317
    float-to-double v10, v7

    .line 318
    const-wide/high16 v13, 0x4000000000000000L    # 2.0

    .line 319
    .line 320
    invoke-static {v10, v11, v13, v14}, Ljava/lang/Math;->pow(DD)D

    .line 321
    .line 322
    .line 323
    move-result-wide v10

    .line 324
    move-object/from16 v16, v1

    .line 325
    .line 326
    move/from16 p0, v2

    .line 327
    .line 328
    float-to-double v1, v9

    .line 329
    invoke-static {v1, v2, v13, v14}, Ljava/lang/Math;->pow(DD)D

    .line 330
    .line 331
    .line 332
    move-result-wide v17

    .line 333
    move/from16 v19, v8

    .line 334
    .line 335
    float-to-double v8, v5

    .line 336
    invoke-static {v8, v9, v13, v14}, Ljava/lang/Math;->pow(DD)D

    .line 337
    .line 338
    .line 339
    move-result-wide v20

    .line 340
    sub-double v17, v17, v20

    .line 341
    .line 342
    mul-double v10, v10, v17

    .line 343
    .line 344
    invoke-static {v1, v2, v13, v14}, Ljava/lang/Math;->pow(DD)D

    .line 345
    .line 346
    .line 347
    move-result-wide v1

    .line 348
    div-double/2addr v10, v1

    .line 349
    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    .line 350
    .line 351
    .line 352
    move-result-wide v1

    .line 353
    div-double v8, v1, v8

    .line 354
    .line 355
    invoke-static {v8, v9}, Ljava/lang/Math;->atan(D)D

    .line 356
    .line 357
    .line 358
    move-result-wide v8

    .line 359
    const-wide v10, 0x4066800000000000L    # 180.0

    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    mul-double v8, v8, v10

    .line 365
    .line 366
    const-wide v10, 0x400921fb54442d18L    # Math.PI

    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    div-double/2addr v8, v10

    .line 372
    double-to-int v8, v8

    .line 373
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 374
    .line 375
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 376
    .line 377
    int-to-float v11, v10

    .line 378
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 379
    .line 380
    int-to-float v14, v13

    .line 381
    int-to-float v10, v10

    .line 382
    add-float/2addr v10, v15

    .line 383
    int-to-float v13, v13

    .line 384
    add-float/2addr v13, v6

    .line 385
    invoke-virtual {v9, v11, v14, v10, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 386
    .line 387
    .line 388
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 389
    .line 390
    int-to-float v10, v8

    .line 391
    invoke-virtual {v4, v9, v12, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 392
    .line 393
    .line 394
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 395
    .line 396
    int-to-float v9, v9

    .line 397
    add-float/2addr v9, v7

    .line 398
    double-to-float v1, v1

    .line 399
    add-float/2addr v9, v1

    .line 400
    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 401
    .line 402
    int-to-float v2, v2

    .line 403
    sub-float/2addr v2, v5

    .line 404
    invoke-virtual {v4, v9, v2}, Landroid/graphics/Path;->setLastPoint(FF)V

    .line 405
    .line 406
    .line 407
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 408
    .line 409
    int-to-float v2, v2

    .line 410
    add-float/2addr v2, v7

    .line 411
    add-float/2addr v2, v1

    .line 412
    add-float v2, v2, v19

    .line 413
    .line 414
    sub-float/2addr v2, v3

    .line 415
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 416
    .line 417
    int-to-float v9, v9

    .line 418
    sub-float/2addr v9, v5

    .line 419
    invoke-virtual {v4, v2, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 420
    .line 421
    .line 422
    iget v2, v0, Landroid/graphics/Rect;->right:I

    .line 423
    .line 424
    int-to-float v2, v2

    .line 425
    sub-float/2addr v2, v3

    .line 426
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 427
    .line 428
    int-to-float v9, v9

    .line 429
    invoke-virtual {v4, v2, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 430
    .line 431
    .line 432
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 433
    .line 434
    int-to-float v2, v2

    .line 435
    add-float/2addr v2, v7

    .line 436
    add-float/2addr v2, v1

    .line 437
    add-float v2, v2, v19

    .line 438
    .line 439
    add-float/2addr v2, v3

    .line 440
    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 441
    .line 442
    int-to-float v3, v3

    .line 443
    sub-float/2addr v3, v5

    .line 444
    invoke-virtual {v4, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 445
    .line 446
    .line 447
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 448
    .line 449
    int-to-float v2, v2

    .line 450
    add-float/2addr v2, v7

    .line 451
    add-float/2addr v2, v1

    .line 452
    add-float v2, v2, p0

    .line 453
    .line 454
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 455
    .line 456
    int-to-float v1, v1

    .line 457
    sub-float/2addr v1, v5

    .line 458
    invoke-virtual {v4, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 459
    .line 460
    .line 461
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 462
    .line 463
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 464
    .line 465
    int-to-float v3, v2

    .line 466
    add-float v3, v3, p0

    .line 467
    .line 468
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 469
    .line 470
    int-to-float v5, v0

    .line 471
    int-to-float v2, v2

    .line 472
    add-float/2addr v2, v15

    .line 473
    add-float v2, v2, p0

    .line 474
    .line 475
    int-to-float v0, v0

    .line 476
    add-float/2addr v0, v6

    .line 477
    invoke-virtual {v1, v3, v5, v2, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 478
    .line 479
    .line 480
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 481
    .line 482
    add-int/lit16 v1, v8, 0x10e

    .line 483
    .line 484
    int-to-float v1, v1

    .line 485
    neg-int v2, v8

    .line 486
    int-to-float v2, v2

    .line 487
    invoke-virtual {v4, v0, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 488
    .line 489
    .line 490
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 491
    .line 492
    .line 493
    move-object/from16 v0, v16

    .line 494
    .line 495
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 496
    .line 497
    .line 498
    return-object v0
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getCurvedLeftArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    new-instance v1, Ljava/util/ArrayList;

    .line 4
    .line 5
    const/4 v2, 0x2

    .line 6
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 7
    .line 8
    .line 9
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    new-instance v4, Landroid/graphics/Path;

    .line 14
    .line 15
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 16
    .line 17
    .line 18
    const v5, 0x3f2aaae3

    .line 19
    .line 20
    .line 21
    const v6, 0x3f666666    # 0.9f

    .line 22
    .line 23
    .line 24
    const v7, 0x3f19999a    # 0.6f

    .line 25
    .line 26
    .line 27
    if-eqz v3, :cond_3

    .line 28
    .line 29
    array-length v8, v3

    .line 30
    const/4 v9, 0x1

    .line 31
    if-lt v8, v9, :cond_3

    .line 32
    .line 33
    const/4 v8, 0x0

    .line 34
    aget-object v10, v3, v8

    .line 35
    .line 36
    if-eqz v10, :cond_0

    .line 37
    .line 38
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 39
    .line 40
    .line 41
    move-result v7

    .line 42
    int-to-float v7, v7

    .line 43
    aget-object v8, v3, v8

    .line 44
    .line 45
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 46
    .line 47
    .line 48
    move-result v8

    .line 49
    mul-float v7, v7, v8

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 53
    .line 54
    .line 55
    move-result v8

    .line 56
    int-to-float v8, v8

    .line 57
    mul-float v8, v8, v7

    .line 58
    .line 59
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    .line 60
    .line 61
    .line 62
    move-result v7

    .line 63
    int-to-float v7, v7

    .line 64
    :goto_0
    array-length v8, v3

    .line 65
    if-lt v8, v2, :cond_1

    .line 66
    .line 67
    aget-object v8, v3, v9

    .line 68
    .line 69
    if-eqz v8, :cond_1

    .line 70
    .line 71
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 72
    .line 73
    .line 74
    move-result v6

    .line 75
    int-to-float v6, v6

    .line 76
    aget-object v8, v3, v9

    .line 77
    .line 78
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 79
    .line 80
    .line 81
    move-result v8

    .line 82
    mul-float v6, v6, v8

    .line 83
    .line 84
    goto :goto_1

    .line 85
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 86
    .line 87
    .line 88
    move-result v8

    .line 89
    int-to-float v8, v8

    .line 90
    mul-float v8, v8, v6

    .line 91
    .line 92
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    .line 93
    .line 94
    .line 95
    move-result v6

    .line 96
    int-to-float v6, v6

    .line 97
    :goto_1
    array-length v8, v3

    .line 98
    const/4 v9, 0x3

    .line 99
    if-lt v8, v9, :cond_2

    .line 100
    .line 101
    aget-object v8, v3, v2

    .line 102
    .line 103
    if-eqz v8, :cond_2

    .line 104
    .line 105
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 106
    .line 107
    .line 108
    move-result v5

    .line 109
    int-to-float v5, v5

    .line 110
    aget-object v3, v3, v2

    .line 111
    .line 112
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 113
    .line 114
    .line 115
    move-result v3

    .line 116
    mul-float v5, v5, v3

    .line 117
    .line 118
    goto :goto_3

    .line 119
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 120
    .line 121
    .line 122
    move-result v3

    .line 123
    int-to-float v3, v3

    .line 124
    mul-float v3, v3, v5

    .line 125
    .line 126
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 127
    .line 128
    .line 129
    move-result v3

    .line 130
    goto :goto_2

    .line 131
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 132
    .line 133
    .line 134
    move-result v3

    .line 135
    int-to-float v3, v3

    .line 136
    mul-float v3, v3, v7

    .line 137
    .line 138
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 139
    .line 140
    .line 141
    move-result v3

    .line 142
    int-to-float v7, v3

    .line 143
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 144
    .line 145
    .line 146
    move-result v3

    .line 147
    int-to-float v3, v3

    .line 148
    mul-float v3, v3, v6

    .line 149
    .line 150
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 151
    .line 152
    .line 153
    move-result v3

    .line 154
    int-to-float v6, v3

    .line 155
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 156
    .line 157
    .line 158
    move-result v3

    .line 159
    int-to-float v3, v3

    .line 160
    mul-float v3, v3, v5

    .line 161
    .line 162
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 163
    .line 164
    .line 165
    move-result v3

    .line 166
    :goto_2
    int-to-float v5, v3

    .line 167
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 168
    .line 169
    .line 170
    move-result v3

    .line 171
    int-to-float v3, v3

    .line 172
    const v8, 0x3ecccccd    # 0.4f

    .line 173
    .line 174
    .line 175
    mul-float v3, v3, v8

    .line 176
    .line 177
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 178
    .line 179
    .line 180
    move-result v8

    .line 181
    int-to-float v8, v8

    .line 182
    sub-float/2addr v8, v7

    .line 183
    sub-float/2addr v6, v7

    .line 184
    sub-float v6, v3, v6

    .line 185
    .line 186
    const/high16 v7, 0x40000000    # 2.0f

    .line 187
    .line 188
    mul-float v6, v6, v7

    .line 189
    .line 190
    sub-float/2addr v3, v6

    .line 191
    const/4 v6, 0x0

    .line 192
    cmpg-float v9, v3, v6

    .line 193
    .line 194
    if-gez v9, :cond_4

    .line 195
    .line 196
    const/4 v3, 0x0

    .line 197
    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 198
    .line 199
    .line 200
    move-result v9

    .line 201
    mul-int/lit8 v9, v9, 0x2

    .line 202
    .line 203
    int-to-float v2, v9

    .line 204
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 205
    .line 206
    int-to-float v9, v9

    .line 207
    div-float/2addr v8, v7

    .line 208
    sub-float/2addr v9, v8

    .line 209
    div-float v10, v3, v7

    .line 210
    .line 211
    sub-float/2addr v9, v10

    .line 212
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 213
    .line 214
    int-to-float v12, v11

    .line 215
    sub-float/2addr v9, v12

    .line 216
    iget v12, v0, Landroid/graphics/Rect;->right:I

    .line 217
    .line 218
    int-to-float v12, v12

    .line 219
    int-to-float v11, v11

    .line 220
    div-float v13, v9, v7

    .line 221
    .line 222
    add-float/2addr v11, v13

    .line 223
    invoke-virtual {v4, v12, v11}, Landroid/graphics/Path;->moveTo(FF)V

    .line 224
    .line 225
    .line 226
    float-to-double v11, v13

    .line 227
    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    .line 228
    .line 229
    invoke-static {v11, v12, v14, v15}, Ljava/lang/Math;->pow(DD)D

    .line 230
    .line 231
    .line 232
    move-result-wide v11

    .line 233
    div-float v7, v2, v7

    .line 234
    .line 235
    float-to-double v6, v7

    .line 236
    invoke-static {v6, v7, v14, v15}, Ljava/lang/Math;->pow(DD)D

    .line 237
    .line 238
    .line 239
    move-result-wide v16

    .line 240
    move/from16 v19, v3

    .line 241
    .line 242
    move-object/from16 v18, v4

    .line 243
    .line 244
    float-to-double v3, v5

    .line 245
    invoke-static {v3, v4, v14, v15}, Ljava/lang/Math;->pow(DD)D

    .line 246
    .line 247
    .line 248
    move-result-wide v20

    .line 249
    sub-double v16, v16, v20

    .line 250
    .line 251
    mul-double v11, v11, v16

    .line 252
    .line 253
    invoke-static {v6, v7, v14, v15}, Ljava/lang/Math;->pow(DD)D

    .line 254
    .line 255
    .line 256
    move-result-wide v6

    .line 257
    div-double/2addr v11, v6

    .line 258
    invoke-static {v11, v12}, Ljava/lang/Math;->sqrt(D)D

    .line 259
    .line 260
    .line 261
    move-result-wide v6

    .line 262
    div-double v3, v6, v3

    .line 263
    .line 264
    invoke-static {v3, v4}, Ljava/lang/Math;->atan(D)D

    .line 265
    .line 266
    .line 267
    move-result-wide v3

    .line 268
    const-wide v11, 0x4066800000000000L    # 180.0

    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    mul-double v3, v3, v11

    .line 274
    .line 275
    const-wide v11, 0x400921fb54442d18L    # Math.PI

    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    div-double/2addr v3, v11

    .line 281
    double-to-int v3, v3

    .line 282
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 283
    .line 284
    iget v11, v0, Landroid/graphics/Rect;->right:I

    .line 285
    .line 286
    int-to-float v12, v11

    .line 287
    sub-float/2addr v12, v2

    .line 288
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 289
    .line 290
    int-to-float v15, v14

    .line 291
    int-to-float v11, v11

    .line 292
    int-to-float v14, v14

    .line 293
    add-float/2addr v14, v9

    .line 294
    invoke-virtual {v4, v12, v15, v11, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 295
    .line 296
    .line 297
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 298
    .line 299
    int-to-float v11, v3

    .line 300
    move-object/from16 v12, v18

    .line 301
    .line 302
    const/4 v14, 0x0

    .line 303
    invoke-virtual {v12, v4, v14, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 304
    .line 305
    .line 306
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 307
    .line 308
    int-to-float v4, v4

    .line 309
    add-float/2addr v4, v5

    .line 310
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 311
    .line 312
    int-to-float v14, v14

    .line 313
    add-float/2addr v14, v13

    .line 314
    double-to-int v6, v6

    .line 315
    int-to-float v6, v6

    .line 316
    add-float/2addr v14, v6

    .line 317
    invoke-virtual {v12, v4, v14}, Landroid/graphics/Path;->setLastPoint(FF)V

    .line 318
    .line 319
    .line 320
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 321
    .line 322
    int-to-float v4, v4

    .line 323
    add-float/2addr v4, v5

    .line 324
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 325
    .line 326
    int-to-float v7, v7

    .line 327
    add-float/2addr v7, v13

    .line 328
    add-float/2addr v7, v6

    .line 329
    add-float/2addr v7, v10

    .line 330
    sub-float/2addr v7, v8

    .line 331
    invoke-virtual {v12, v4, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 332
    .line 333
    .line 334
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 335
    .line 336
    int-to-float v4, v4

    .line 337
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    .line 338
    .line 339
    int-to-float v7, v7

    .line 340
    sub-float/2addr v7, v8

    .line 341
    invoke-virtual {v12, v4, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 342
    .line 343
    .line 344
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 345
    .line 346
    int-to-float v4, v4

    .line 347
    add-float/2addr v4, v5

    .line 348
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 349
    .line 350
    int-to-float v7, v7

    .line 351
    add-float/2addr v7, v13

    .line 352
    add-float/2addr v7, v6

    .line 353
    add-float/2addr v7, v10

    .line 354
    add-float/2addr v7, v8

    .line 355
    invoke-virtual {v12, v4, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 356
    .line 357
    .line 358
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 359
    .line 360
    int-to-float v4, v4

    .line 361
    add-float/2addr v4, v5

    .line 362
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 363
    .line 364
    int-to-float v5, v5

    .line 365
    add-float/2addr v5, v13

    .line 366
    add-float/2addr v5, v6

    .line 367
    add-float v5, v5, v19

    .line 368
    .line 369
    invoke-virtual {v12, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 370
    .line 371
    .line 372
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 373
    .line 374
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 375
    .line 376
    int-to-float v6, v5

    .line 377
    sub-float/2addr v6, v2

    .line 378
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 379
    .line 380
    int-to-float v8, v7

    .line 381
    add-float v8, v8, v19

    .line 382
    .line 383
    int-to-float v5, v5

    .line 384
    int-to-float v7, v7

    .line 385
    add-float/2addr v7, v9

    .line 386
    add-float v7, v7, v19

    .line 387
    .line 388
    invoke-virtual {v4, v6, v8, v5, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 389
    .line 390
    .line 391
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 392
    .line 393
    neg-int v3, v3

    .line 394
    int-to-float v3, v3

    .line 395
    invoke-virtual {v12, v4, v11, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 396
    .line 397
    .line 398
    invoke-virtual {v12}, Landroid/graphics/Path;->close()V

    .line 399
    .line 400
    .line 401
    invoke-interface {v1, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 402
    .line 403
    .line 404
    new-instance v3, Landroid/graphics/Path;

    .line 405
    .line 406
    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 407
    .line 408
    .line 409
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 410
    .line 411
    int-to-float v4, v4

    .line 412
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 413
    .line 414
    int-to-float v5, v5

    .line 415
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 416
    .line 417
    .line 418
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 419
    .line 420
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 421
    .line 422
    int-to-float v6, v5

    .line 423
    sub-float/2addr v6, v2

    .line 424
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 425
    .line 426
    int-to-float v8, v7

    .line 427
    int-to-float v5, v5

    .line 428
    int-to-float v7, v7

    .line 429
    add-float/2addr v7, v9

    .line 430
    invoke-virtual {v4, v6, v8, v5, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 431
    .line 432
    .line 433
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 434
    .line 435
    const/high16 v5, 0x43870000    # 270.0f

    .line 436
    .line 437
    const/high16 v6, 0x42b40000    # 90.0f

    .line 438
    .line 439
    invoke-virtual {v3, v4, v5, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 440
    .line 441
    .line 442
    iget v4, v0, Landroid/graphics/Rect;->right:I

    .line 443
    .line 444
    int-to-float v4, v4

    .line 445
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 446
    .line 447
    int-to-float v5, v5

    .line 448
    add-float/2addr v5, v13

    .line 449
    add-float v5, v5, v19

    .line 450
    .line 451
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 452
    .line 453
    .line 454
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 455
    .line 456
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 457
    .line 458
    int-to-float v6, v5

    .line 459
    sub-float/2addr v6, v2

    .line 460
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 461
    .line 462
    int-to-float v2, v0

    .line 463
    add-float v2, v2, v19

    .line 464
    .line 465
    int-to-float v5, v5

    .line 466
    int-to-float v0, v0

    .line 467
    add-float/2addr v0, v9

    .line 468
    add-float v0, v0, v19

    .line 469
    .line 470
    invoke-virtual {v4, v6, v2, v5, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 471
    .line 472
    .line 473
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 474
    .line 475
    const/high16 v2, -0x3d4c0000    # -90.0f

    .line 476
    .line 477
    const/4 v4, 0x0

    .line 478
    invoke-virtual {v3, v0, v4, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 479
    .line 480
    .line 481
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    .line 482
    .line 483
    .line 484
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 485
    .line 486
    .line 487
    return-object v1
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getCurvedRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    new-instance v1, Ljava/util/ArrayList;

    .line 4
    .line 5
    const/4 v2, 0x2

    .line 6
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 7
    .line 8
    .line 9
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    new-instance v4, Landroid/graphics/Path;

    .line 14
    .line 15
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 16
    .line 17
    .line 18
    const v5, 0x3f2aaae3

    .line 19
    .line 20
    .line 21
    const v6, 0x3f666666    # 0.9f

    .line 22
    .line 23
    .line 24
    const v7, 0x3f19999a    # 0.6f

    .line 25
    .line 26
    .line 27
    if-eqz v3, :cond_3

    .line 28
    .line 29
    array-length v8, v3

    .line 30
    const/4 v9, 0x1

    .line 31
    if-lt v8, v9, :cond_3

    .line 32
    .line 33
    const/4 v8, 0x0

    .line 34
    aget-object v10, v3, v8

    .line 35
    .line 36
    if-eqz v10, :cond_0

    .line 37
    .line 38
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 39
    .line 40
    .line 41
    move-result v7

    .line 42
    int-to-float v7, v7

    .line 43
    aget-object v8, v3, v8

    .line 44
    .line 45
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 46
    .line 47
    .line 48
    move-result v8

    .line 49
    mul-float v7, v7, v8

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 53
    .line 54
    .line 55
    move-result v8

    .line 56
    int-to-float v8, v8

    .line 57
    mul-float v8, v8, v7

    .line 58
    .line 59
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    .line 60
    .line 61
    .line 62
    move-result v7

    .line 63
    int-to-float v7, v7

    .line 64
    :goto_0
    array-length v8, v3

    .line 65
    if-lt v8, v2, :cond_1

    .line 66
    .line 67
    aget-object v8, v3, v9

    .line 68
    .line 69
    if-eqz v8, :cond_1

    .line 70
    .line 71
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 72
    .line 73
    .line 74
    move-result v6

    .line 75
    int-to-float v6, v6

    .line 76
    aget-object v8, v3, v9

    .line 77
    .line 78
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 79
    .line 80
    .line 81
    move-result v8

    .line 82
    mul-float v6, v6, v8

    .line 83
    .line 84
    goto :goto_1

    .line 85
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 86
    .line 87
    .line 88
    move-result v8

    .line 89
    int-to-float v8, v8

    .line 90
    mul-float v8, v8, v6

    .line 91
    .line 92
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    .line 93
    .line 94
    .line 95
    move-result v6

    .line 96
    int-to-float v6, v6

    .line 97
    :goto_1
    array-length v8, v3

    .line 98
    const/4 v9, 0x3

    .line 99
    if-lt v8, v9, :cond_2

    .line 100
    .line 101
    aget-object v8, v3, v2

    .line 102
    .line 103
    if-eqz v8, :cond_2

    .line 104
    .line 105
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 106
    .line 107
    .line 108
    move-result v5

    .line 109
    int-to-float v5, v5

    .line 110
    aget-object v3, v3, v2

    .line 111
    .line 112
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 113
    .line 114
    .line 115
    move-result v3

    .line 116
    mul-float v5, v5, v3

    .line 117
    .line 118
    goto :goto_3

    .line 119
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 120
    .line 121
    .line 122
    move-result v3

    .line 123
    int-to-float v3, v3

    .line 124
    mul-float v3, v3, v5

    .line 125
    .line 126
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 127
    .line 128
    .line 129
    move-result v3

    .line 130
    goto :goto_2

    .line 131
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 132
    .line 133
    .line 134
    move-result v3

    .line 135
    int-to-float v3, v3

    .line 136
    mul-float v3, v3, v7

    .line 137
    .line 138
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 139
    .line 140
    .line 141
    move-result v3

    .line 142
    int-to-float v7, v3

    .line 143
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 144
    .line 145
    .line 146
    move-result v3

    .line 147
    int-to-float v3, v3

    .line 148
    mul-float v3, v3, v6

    .line 149
    .line 150
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 151
    .line 152
    .line 153
    move-result v3

    .line 154
    int-to-float v6, v3

    .line 155
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 156
    .line 157
    .line 158
    move-result v3

    .line 159
    int-to-float v3, v3

    .line 160
    mul-float v3, v3, v5

    .line 161
    .line 162
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 163
    .line 164
    .line 165
    move-result v3

    .line 166
    :goto_2
    int-to-float v5, v3

    .line 167
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 168
    .line 169
    .line 170
    move-result v3

    .line 171
    int-to-float v3, v3

    .line 172
    const v8, 0x3ecccccd    # 0.4f

    .line 173
    .line 174
    .line 175
    mul-float v3, v3, v8

    .line 176
    .line 177
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 178
    .line 179
    .line 180
    move-result v8

    .line 181
    int-to-float v8, v8

    .line 182
    sub-float/2addr v8, v7

    .line 183
    sub-float/2addr v6, v7

    .line 184
    sub-float v6, v3, v6

    .line 185
    .line 186
    const/high16 v7, 0x40000000    # 2.0f

    .line 187
    .line 188
    mul-float v6, v6, v7

    .line 189
    .line 190
    sub-float/2addr v3, v6

    .line 191
    const/4 v6, 0x0

    .line 192
    cmpg-float v9, v3, v6

    .line 193
    .line 194
    if-gez v9, :cond_4

    .line 195
    .line 196
    const/4 v3, 0x0

    .line 197
    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 198
    .line 199
    .line 200
    move-result v6

    .line 201
    int-to-float v6, v6

    .line 202
    sub-float/2addr v6, v5

    .line 203
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 204
    .line 205
    .line 206
    move-result v5

    .line 207
    mul-int/lit8 v5, v5, 0x2

    .line 208
    .line 209
    int-to-float v2, v5

    .line 210
    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 211
    .line 212
    int-to-float v5, v5

    .line 213
    div-float/2addr v8, v7

    .line 214
    sub-float/2addr v5, v8

    .line 215
    div-float v9, v3, v7

    .line 216
    .line 217
    sub-float/2addr v5, v9

    .line 218
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 219
    .line 220
    int-to-float v11, v10

    .line 221
    sub-float/2addr v5, v11

    .line 222
    iget v11, v0, Landroid/graphics/Rect;->right:I

    .line 223
    .line 224
    int-to-float v11, v11

    .line 225
    int-to-float v10, v10

    .line 226
    invoke-virtual {v4, v11, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 227
    .line 228
    .line 229
    sget-object v10, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 230
    .line 231
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 232
    .line 233
    int-to-float v12, v11

    .line 234
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 235
    .line 236
    int-to-float v14, v13

    .line 237
    int-to-float v11, v11

    .line 238
    add-float/2addr v11, v2

    .line 239
    int-to-float v13, v13

    .line 240
    add-float/2addr v13, v5

    .line 241
    invoke-virtual {v10, v12, v14, v11, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 242
    .line 243
    .line 244
    sget-object v10, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 245
    .line 246
    const/high16 v11, 0x43870000    # 270.0f

    .line 247
    .line 248
    const/high16 v12, -0x3d4c0000    # -90.0f

    .line 249
    .line 250
    invoke-virtual {v4, v10, v11, v12}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 251
    .line 252
    .line 253
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 254
    .line 255
    int-to-float v10, v10

    .line 256
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 257
    .line 258
    int-to-float v11, v11

    .line 259
    div-float v12, v5, v7

    .line 260
    .line 261
    add-float/2addr v11, v12

    .line 262
    add-float/2addr v11, v3

    .line 263
    invoke-virtual {v4, v10, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 264
    .line 265
    .line 266
    sget-object v10, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 267
    .line 268
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 269
    .line 270
    int-to-float v13, v11

    .line 271
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 272
    .line 273
    int-to-float v15, v14

    .line 274
    add-float/2addr v15, v3

    .line 275
    int-to-float v11, v11

    .line 276
    add-float/2addr v11, v2

    .line 277
    int-to-float v14, v14

    .line 278
    add-float/2addr v14, v5

    .line 279
    add-float/2addr v14, v3

    .line 280
    invoke-virtual {v10, v13, v15, v11, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 281
    .line 282
    .line 283
    sget-object v10, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 284
    .line 285
    const/high16 v11, 0x42b40000    # 90.0f

    .line 286
    .line 287
    const/high16 v13, 0x43340000    # 180.0f

    .line 288
    .line 289
    invoke-virtual {v4, v10, v13, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 290
    .line 291
    .line 292
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 293
    .line 294
    .line 295
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    .line 297
    .line 298
    new-instance v4, Landroid/graphics/Path;

    .line 299
    .line 300
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 301
    .line 302
    .line 303
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 304
    .line 305
    int-to-float v10, v10

    .line 306
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 307
    .line 308
    int-to-float v11, v11

    .line 309
    add-float/2addr v11, v12

    .line 310
    invoke-virtual {v4, v10, v11}, Landroid/graphics/Path;->moveTo(FF)V

    .line 311
    .line 312
    .line 313
    float-to-double v10, v12

    .line 314
    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    .line 315
    .line 316
    invoke-static {v10, v11, v14, v15}, Ljava/lang/Math;->pow(DD)D

    .line 317
    .line 318
    .line 319
    move-result-wide v10

    .line 320
    div-float v7, v2, v7

    .line 321
    .line 322
    move/from16 p0, v8

    .line 323
    .line 324
    float-to-double v7, v7

    .line 325
    invoke-static {v7, v8, v14, v15}, Ljava/lang/Math;->pow(DD)D

    .line 326
    .line 327
    .line 328
    move-result-wide v16

    .line 329
    move/from16 v18, v3

    .line 330
    .line 331
    move-object/from16 v19, v4

    .line 332
    .line 333
    float-to-double v3, v6

    .line 334
    invoke-static {v3, v4, v14, v15}, Ljava/lang/Math;->pow(DD)D

    .line 335
    .line 336
    .line 337
    move-result-wide v20

    .line 338
    sub-double v16, v16, v20

    .line 339
    .line 340
    mul-double v10, v10, v16

    .line 341
    .line 342
    invoke-static {v7, v8, v14, v15}, Ljava/lang/Math;->pow(DD)D

    .line 343
    .line 344
    .line 345
    move-result-wide v7

    .line 346
    div-double/2addr v10, v7

    .line 347
    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    .line 348
    .line 349
    .line 350
    move-result-wide v7

    .line 351
    div-double v3, v7, v3

    .line 352
    .line 353
    invoke-static {v3, v4}, Ljava/lang/Math;->atan(D)D

    .line 354
    .line 355
    .line 356
    move-result-wide v3

    .line 357
    const-wide v10, 0x4066800000000000L    # 180.0

    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    mul-double v3, v3, v10

    .line 363
    .line 364
    const-wide v10, 0x400921fb54442d18L    # Math.PI

    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    div-double/2addr v3, v10

    .line 370
    double-to-int v3, v3

    .line 371
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 372
    .line 373
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 374
    .line 375
    int-to-float v11, v10

    .line 376
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 377
    .line 378
    int-to-float v15, v14

    .line 379
    int-to-float v10, v10

    .line 380
    add-float/2addr v10, v2

    .line 381
    int-to-float v14, v14

    .line 382
    add-float/2addr v14, v5

    .line 383
    invoke-virtual {v4, v11, v15, v10, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 384
    .line 385
    .line 386
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 387
    .line 388
    neg-int v10, v3

    .line 389
    int-to-float v10, v10

    .line 390
    move-object/from16 v11, v19

    .line 391
    .line 392
    invoke-virtual {v11, v4, v13, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 393
    .line 394
    .line 395
    iget v4, v0, Landroid/graphics/Rect;->right:I

    .line 396
    .line 397
    int-to-float v4, v4

    .line 398
    sub-float/2addr v4, v6

    .line 399
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 400
    .line 401
    int-to-float v10, v10

    .line 402
    add-float/2addr v10, v12

    .line 403
    double-to-int v7, v7

    .line 404
    int-to-float v7, v7

    .line 405
    add-float/2addr v10, v7

    .line 406
    invoke-virtual {v11, v4, v10}, Landroid/graphics/Path;->setLastPoint(FF)V

    .line 407
    .line 408
    .line 409
    iget v4, v0, Landroid/graphics/Rect;->right:I

    .line 410
    .line 411
    int-to-float v4, v4

    .line 412
    sub-float/2addr v4, v6

    .line 413
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 414
    .line 415
    int-to-float v8, v8

    .line 416
    add-float/2addr v8, v12

    .line 417
    add-float/2addr v8, v7

    .line 418
    add-float/2addr v8, v9

    .line 419
    sub-float v8, v8, p0

    .line 420
    .line 421
    invoke-virtual {v11, v4, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 422
    .line 423
    .line 424
    iget v4, v0, Landroid/graphics/Rect;->right:I

    .line 425
    .line 426
    int-to-float v4, v4

    .line 427
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    .line 428
    .line 429
    int-to-float v8, v8

    .line 430
    sub-float v8, v8, p0

    .line 431
    .line 432
    invoke-virtual {v11, v4, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 433
    .line 434
    .line 435
    iget v4, v0, Landroid/graphics/Rect;->right:I

    .line 436
    .line 437
    int-to-float v4, v4

    .line 438
    sub-float/2addr v4, v6

    .line 439
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 440
    .line 441
    int-to-float v8, v8

    .line 442
    add-float/2addr v8, v12

    .line 443
    add-float/2addr v8, v7

    .line 444
    add-float/2addr v8, v9

    .line 445
    add-float v8, v8, p0

    .line 446
    .line 447
    invoke-virtual {v11, v4, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 448
    .line 449
    .line 450
    iget v4, v0, Landroid/graphics/Rect;->right:I

    .line 451
    .line 452
    int-to-float v4, v4

    .line 453
    sub-float/2addr v4, v6

    .line 454
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 455
    .line 456
    int-to-float v6, v6

    .line 457
    add-float/2addr v6, v12

    .line 458
    add-float/2addr v6, v7

    .line 459
    add-float v6, v6, v18

    .line 460
    .line 461
    invoke-virtual {v11, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 462
    .line 463
    .line 464
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 465
    .line 466
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 467
    .line 468
    int-to-float v7, v6

    .line 469
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 470
    .line 471
    int-to-float v8, v0

    .line 472
    add-float v8, v8, v18

    .line 473
    .line 474
    int-to-float v6, v6

    .line 475
    add-float/2addr v6, v2

    .line 476
    int-to-float v0, v0

    .line 477
    add-float/2addr v0, v5

    .line 478
    add-float v0, v0, v18

    .line 479
    .line 480
    invoke-virtual {v4, v7, v8, v6, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 481
    .line 482
    .line 483
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 484
    .line 485
    rsub-int v2, v3, 0xb4

    .line 486
    .line 487
    int-to-float v2, v2

    .line 488
    int-to-float v3, v3

    .line 489
    invoke-virtual {v11, v0, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 490
    .line 491
    .line 492
    invoke-virtual {v11}, Landroid/graphics/Path;->close()V

    .line 493
    .line 494
    .line 495
    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 496
    .line 497
    .line 498
    return-object v1
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getCurvedUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    new-instance v1, Ljava/util/ArrayList;

    .line 4
    .line 5
    const/4 v2, 0x2

    .line 6
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 7
    .line 8
    .line 9
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    new-instance v4, Landroid/graphics/Path;

    .line 14
    .line 15
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 16
    .line 17
    .line 18
    const v5, 0x3f666666    # 0.9f

    .line 19
    .line 20
    .line 21
    const v6, 0x3f19999a    # 0.6f

    .line 22
    .line 23
    .line 24
    if-eqz v3, :cond_3

    .line 25
    .line 26
    array-length v7, v3

    .line 27
    const/4 v8, 0x1

    .line 28
    if-lt v7, v8, :cond_3

    .line 29
    .line 30
    const/4 v7, 0x0

    .line 31
    aget-object v9, v3, v7

    .line 32
    .line 33
    if-eqz v9, :cond_0

    .line 34
    .line 35
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 36
    .line 37
    .line 38
    move-result v6

    .line 39
    int-to-float v6, v6

    .line 40
    aget-object v7, v3, v7

    .line 41
    .line 42
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 43
    .line 44
    .line 45
    move-result v7

    .line 46
    mul-float v6, v6, v7

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 50
    .line 51
    .line 52
    move-result v7

    .line 53
    int-to-float v7, v7

    .line 54
    mul-float v7, v7, v6

    .line 55
    .line 56
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 57
    .line 58
    .line 59
    move-result v6

    .line 60
    int-to-float v6, v6

    .line 61
    :goto_0
    array-length v7, v3

    .line 62
    if-lt v7, v2, :cond_1

    .line 63
    .line 64
    aget-object v7, v3, v8

    .line 65
    .line 66
    if-eqz v7, :cond_1

    .line 67
    .line 68
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 69
    .line 70
    .line 71
    move-result v5

    .line 72
    int-to-float v5, v5

    .line 73
    aget-object v7, v3, v8

    .line 74
    .line 75
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 76
    .line 77
    .line 78
    move-result v7

    .line 79
    mul-float v5, v5, v7

    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 83
    .line 84
    .line 85
    move-result v7

    .line 86
    int-to-float v7, v7

    .line 87
    mul-float v7, v7, v5

    .line 88
    .line 89
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 90
    .line 91
    .line 92
    move-result v5

    .line 93
    int-to-float v5, v5

    .line 94
    :goto_1
    array-length v7, v3

    .line 95
    const/4 v8, 0x3

    .line 96
    if-lt v7, v8, :cond_2

    .line 97
    .line 98
    aget-object v7, v3, v2

    .line 99
    .line 100
    if-eqz v7, :cond_2

    .line 101
    .line 102
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 103
    .line 104
    .line 105
    move-result v7

    .line 106
    int-to-float v7, v7

    .line 107
    aget-object v2, v3, v2

    .line 108
    .line 109
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 110
    .line 111
    .line 112
    move-result v2

    .line 113
    mul-float v7, v7, v2

    .line 114
    .line 115
    goto :goto_3

    .line 116
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 117
    .line 118
    .line 119
    move-result v2

    .line 120
    int-to-float v2, v2

    .line 121
    const v3, 0x3f2aaae3

    .line 122
    .line 123
    .line 124
    mul-float v2, v2, v3

    .line 125
    .line 126
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 127
    .line 128
    .line 129
    move-result v2

    .line 130
    goto :goto_2

    .line 131
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 132
    .line 133
    .line 134
    move-result v2

    .line 135
    int-to-float v2, v2

    .line 136
    mul-float v2, v2, v6

    .line 137
    .line 138
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 139
    .line 140
    .line 141
    move-result v2

    .line 142
    int-to-float v6, v2

    .line 143
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 144
    .line 145
    .line 146
    move-result v2

    .line 147
    int-to-float v2, v2

    .line 148
    mul-float v2, v2, v5

    .line 149
    .line 150
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 151
    .line 152
    .line 153
    move-result v2

    .line 154
    int-to-float v5, v2

    .line 155
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 156
    .line 157
    .line 158
    move-result v2

    .line 159
    int-to-float v2, v2

    .line 160
    const v3, 0x3eaaaa3b    # 0.33333f

    .line 161
    .line 162
    .line 163
    mul-float v2, v2, v3

    .line 164
    .line 165
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 166
    .line 167
    .line 168
    move-result v2

    .line 169
    :goto_2
    int-to-float v7, v2

    .line 170
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 171
    .line 172
    .line 173
    move-result v2

    .line 174
    int-to-float v2, v2

    .line 175
    const v3, 0x3ecccccd    # 0.4f

    .line 176
    .line 177
    .line 178
    mul-float v2, v2, v3

    .line 179
    .line 180
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 181
    .line 182
    .line 183
    move-result v3

    .line 184
    int-to-float v3, v3

    .line 185
    sub-float/2addr v3, v6

    .line 186
    sub-float/2addr v5, v6

    .line 187
    sub-float v5, v2, v5

    .line 188
    .line 189
    const/high16 v6, 0x40000000    # 2.0f

    .line 190
    .line 191
    mul-float v5, v5, v6

    .line 192
    .line 193
    sub-float/2addr v2, v5

    .line 194
    const/4 v5, 0x0

    .line 195
    cmpg-float v8, v2, v5

    .line 196
    .line 197
    if-gez v8, :cond_4

    .line 198
    .line 199
    const/4 v2, 0x0

    .line 200
    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 201
    .line 202
    .line 203
    move-result v5

    .line 204
    int-to-float v5, v5

    .line 205
    div-float/2addr v3, v6

    .line 206
    sub-float/2addr v5, v3

    .line 207
    div-float v8, v2, v6

    .line 208
    .line 209
    sub-float/2addr v5, v8

    .line 210
    div-float/2addr v5, v6

    .line 211
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 212
    .line 213
    .line 214
    move-result v9

    .line 215
    int-to-float v9, v9

    .line 216
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 217
    .line 218
    int-to-float v10, v10

    .line 219
    add-float/2addr v10, v5

    .line 220
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 221
    .line 222
    int-to-float v11, v11

    .line 223
    invoke-virtual {v4, v10, v11}, Landroid/graphics/Path;->moveTo(FF)V

    .line 224
    .line 225
    .line 226
    float-to-double v10, v5

    .line 227
    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    .line 228
    .line 229
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    .line 230
    .line 231
    .line 232
    move-result-wide v10

    .line 233
    float-to-double v14, v9

    .line 234
    invoke-static {v14, v15, v12, v13}, Ljava/lang/Math;->pow(DD)D

    .line 235
    .line 236
    .line 237
    move-result-wide v16

    .line 238
    move-object/from16 v18, v1

    .line 239
    .line 240
    move/from16 p0, v2

    .line 241
    .line 242
    float-to-double v1, v7

    .line 243
    invoke-static {v1, v2, v12, v13}, Ljava/lang/Math;->pow(DD)D

    .line 244
    .line 245
    .line 246
    move-result-wide v19

    .line 247
    sub-double v16, v16, v19

    .line 248
    .line 249
    mul-double v10, v10, v16

    .line 250
    .line 251
    invoke-static {v14, v15, v12, v13}, Ljava/lang/Math;->pow(DD)D

    .line 252
    .line 253
    .line 254
    move-result-wide v12

    .line 255
    div-double/2addr v10, v12

    .line 256
    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    .line 257
    .line 258
    .line 259
    move-result-wide v10

    .line 260
    div-double v1, v10, v1

    .line 261
    .line 262
    invoke-static {v1, v2}, Ljava/lang/Math;->atan(D)D

    .line 263
    .line 264
    .line 265
    move-result-wide v1

    .line 266
    const-wide v12, 0x4066800000000000L    # 180.0

    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    mul-double v1, v1, v12

    .line 272
    .line 273
    const-wide v12, 0x400921fb54442d18L    # Math.PI

    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    div-double/2addr v1, v12

    .line 279
    double-to-int v1, v1

    .line 280
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 281
    .line 282
    iget v12, v0, Landroid/graphics/Rect;->left:I

    .line 283
    .line 284
    int-to-float v13, v12

    .line 285
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 286
    .line 287
    int-to-float v15, v14

    .line 288
    sub-float/2addr v15, v9

    .line 289
    int-to-float v12, v12

    .line 290
    mul-float v6, v6, v5

    .line 291
    .line 292
    add-float/2addr v12, v6

    .line 293
    int-to-float v14, v14

    .line 294
    add-float/2addr v14, v9

    .line 295
    invoke-virtual {v2, v13, v15, v12, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 296
    .line 297
    .line 298
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 299
    .line 300
    neg-int v12, v1

    .line 301
    int-to-float v12, v12

    .line 302
    const/high16 v13, 0x42b40000    # 90.0f

    .line 303
    .line 304
    invoke-virtual {v4, v2, v13, v12}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 305
    .line 306
    .line 307
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 308
    .line 309
    int-to-float v2, v2

    .line 310
    add-float/2addr v2, v5

    .line 311
    double-to-float v10, v10

    .line 312
    add-float/2addr v2, v10

    .line 313
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 314
    .line 315
    int-to-float v11, v11

    .line 316
    add-float/2addr v11, v7

    .line 317
    invoke-virtual {v4, v2, v11}, Landroid/graphics/Path;->setLastPoint(FF)V

    .line 318
    .line 319
    .line 320
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 321
    .line 322
    int-to-float v2, v2

    .line 323
    add-float/2addr v2, v5

    .line 324
    add-float/2addr v2, v10

    .line 325
    add-float/2addr v2, v8

    .line 326
    sub-float/2addr v2, v3

    .line 327
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 328
    .line 329
    int-to-float v11, v11

    .line 330
    add-float/2addr v11, v7

    .line 331
    invoke-virtual {v4, v2, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 332
    .line 333
    .line 334
    iget v2, v0, Landroid/graphics/Rect;->right:I

    .line 335
    .line 336
    int-to-float v2, v2

    .line 337
    sub-float/2addr v2, v3

    .line 338
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 339
    .line 340
    int-to-float v11, v11

    .line 341
    invoke-virtual {v4, v2, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 342
    .line 343
    .line 344
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 345
    .line 346
    int-to-float v2, v2

    .line 347
    add-float/2addr v2, v5

    .line 348
    add-float/2addr v2, v10

    .line 349
    add-float/2addr v2, v8

    .line 350
    add-float/2addr v2, v3

    .line 351
    iget v3, v0, Landroid/graphics/Rect;->top:I

    .line 352
    .line 353
    int-to-float v3, v3

    .line 354
    add-float/2addr v3, v7

    .line 355
    invoke-virtual {v4, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 356
    .line 357
    .line 358
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 359
    .line 360
    int-to-float v2, v2

    .line 361
    add-float/2addr v2, v5

    .line 362
    add-float/2addr v2, v10

    .line 363
    add-float v2, v2, p0

    .line 364
    .line 365
    iget v3, v0, Landroid/graphics/Rect;->top:I

    .line 366
    .line 367
    int-to-float v3, v3

    .line 368
    add-float/2addr v3, v7

    .line 369
    invoke-virtual {v4, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 370
    .line 371
    .line 372
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 373
    .line 374
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 375
    .line 376
    int-to-float v7, v3

    .line 377
    add-float v7, v7, p0

    .line 378
    .line 379
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 380
    .line 381
    int-to-float v10, v8

    .line 382
    sub-float/2addr v10, v9

    .line 383
    int-to-float v3, v3

    .line 384
    add-float/2addr v3, v6

    .line 385
    add-float v3, v3, p0

    .line 386
    .line 387
    int-to-float v8, v8

    .line 388
    add-float/2addr v8, v9

    .line 389
    invoke-virtual {v2, v7, v10, v3, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 390
    .line 391
    .line 392
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 393
    .line 394
    rsub-int/lit8 v3, v1, 0x5a

    .line 395
    .line 396
    int-to-float v3, v3

    .line 397
    int-to-float v1, v1

    .line 398
    invoke-virtual {v4, v2, v3, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 399
    .line 400
    .line 401
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 402
    .line 403
    .line 404
    move-object/from16 v1, v18

    .line 405
    .line 406
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 407
    .line 408
    .line 409
    new-instance v2, Landroid/graphics/Path;

    .line 410
    .line 411
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 412
    .line 413
    .line 414
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 415
    .line 416
    int-to-float v3, v3

    .line 417
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 418
    .line 419
    int-to-float v4, v4

    .line 420
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 421
    .line 422
    .line 423
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 424
    .line 425
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 426
    .line 427
    int-to-float v7, v4

    .line 428
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 429
    .line 430
    int-to-float v10, v8

    .line 431
    sub-float/2addr v10, v9

    .line 432
    int-to-float v4, v4

    .line 433
    add-float/2addr v4, v6

    .line 434
    int-to-float v8, v8

    .line 435
    add-float/2addr v8, v9

    .line 436
    invoke-virtual {v3, v7, v10, v4, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 437
    .line 438
    .line 439
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 440
    .line 441
    const/high16 v4, 0x43340000    # 180.0f

    .line 442
    .line 443
    const/high16 v7, -0x3d4c0000    # -90.0f

    .line 444
    .line 445
    invoke-virtual {v2, v3, v4, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 446
    .line 447
    .line 448
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 449
    .line 450
    int-to-float v3, v3

    .line 451
    add-float/2addr v3, v5

    .line 452
    add-float v3, v3, p0

    .line 453
    .line 454
    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    .line 455
    .line 456
    int-to-float v4, v4

    .line 457
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 458
    .line 459
    .line 460
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 461
    .line 462
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 463
    .line 464
    int-to-float v5, v4

    .line 465
    add-float v5, v5, p0

    .line 466
    .line 467
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 468
    .line 469
    int-to-float v7, v0

    .line 470
    sub-float/2addr v7, v9

    .line 471
    int-to-float v4, v4

    .line 472
    add-float/2addr v4, v6

    .line 473
    add-float v4, v4, p0

    .line 474
    .line 475
    int-to-float v0, v0

    .line 476
    add-float/2addr v0, v9

    .line 477
    invoke-virtual {v3, v5, v7, v4, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 478
    .line 479
    .line 480
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 481
    .line 482
    invoke-virtual {v2, v0, v13, v13}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 483
    .line 484
    .line 485
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 486
    .line 487
    .line 488
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 489
    .line 490
    .line 491
    return-object v1
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getDownArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/high16 v0, 0x3ec00000    # 0.375f

    .line 6
    .line 7
    const v1, 0x3f547ae1    # 0.83f

    .line 8
    .line 9
    .line 10
    const/high16 v2, 0x3e800000    # 0.25f

    .line 11
    .line 12
    const v3, 0x3f2b851f    # 0.67f

    .line 13
    .line 14
    .line 15
    if-eqz p0, :cond_4

    .line 16
    .line 17
    array-length v4, p0

    .line 18
    const/4 v5, 0x1

    .line 19
    if-lt v4, v5, :cond_4

    .line 20
    .line 21
    const/4 v4, 0x0

    .line 22
    aget-object v6, p0, v4

    .line 23
    .line 24
    if-eqz v6, :cond_0

    .line 25
    .line 26
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    int-to-float v3, v3

    .line 31
    aget-object v4, p0, v4

    .line 32
    .line 33
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    mul-float v3, v3, v4

    .line 38
    .line 39
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    goto :goto_0

    .line 44
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    int-to-float v4, v4

    .line 49
    mul-float v4, v4, v3

    .line 50
    .line 51
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 52
    .line 53
    .line 54
    move-result v3

    .line 55
    :goto_0
    array-length v4, p0

    .line 56
    const/4 v6, 0x2

    .line 57
    if-lt v4, v6, :cond_1

    .line 58
    .line 59
    aget-object v4, p0, v5

    .line 60
    .line 61
    if-eqz v4, :cond_1

    .line 62
    .line 63
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    int-to-float v2, v2

    .line 68
    aget-object v4, p0, v5

    .line 69
    .line 70
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 71
    .line 72
    .line 73
    move-result v4

    .line 74
    mul-float v2, v2, v4

    .line 75
    .line 76
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 77
    .line 78
    .line 79
    move-result v2

    .line 80
    goto :goto_1

    .line 81
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 82
    .line 83
    .line 84
    move-result v4

    .line 85
    int-to-float v4, v4

    .line 86
    mul-float v4, v4, v2

    .line 87
    .line 88
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 89
    .line 90
    .line 91
    move-result v2

    .line 92
    :goto_1
    array-length v4, p0

    .line 93
    const/4 v5, 0x3

    .line 94
    if-lt v4, v5, :cond_2

    .line 95
    .line 96
    aget-object v4, p0, v6

    .line 97
    .line 98
    if-eqz v4, :cond_2

    .line 99
    .line 100
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 101
    .line 102
    .line 103
    move-result v1

    .line 104
    int-to-float v1, v1

    .line 105
    aget-object v4, p0, v6

    .line 106
    .line 107
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 108
    .line 109
    .line 110
    move-result v4

    .line 111
    mul-float v1, v1, v4

    .line 112
    .line 113
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 114
    .line 115
    .line 116
    move-result v1

    .line 117
    goto :goto_2

    .line 118
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 119
    .line 120
    .line 121
    move-result v4

    .line 122
    int-to-float v4, v4

    .line 123
    mul-float v4, v4, v1

    .line 124
    .line 125
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 126
    .line 127
    .line 128
    move-result v1

    .line 129
    :goto_2
    array-length v4, p0

    .line 130
    const/4 v6, 0x4

    .line 131
    if-lt v4, v6, :cond_3

    .line 132
    .line 133
    aget-object v4, p0, v5

    .line 134
    .line 135
    if-eqz v4, :cond_3

    .line 136
    .line 137
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 138
    .line 139
    .line 140
    move-result v0

    .line 141
    int-to-float v0, v0

    .line 142
    aget-object p0, p0, v5

    .line 143
    .line 144
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 145
    .line 146
    .line 147
    move-result p0

    .line 148
    mul-float v0, v0, p0

    .line 149
    .line 150
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 151
    .line 152
    .line 153
    move-result p0

    .line 154
    goto :goto_3

    .line 155
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 156
    .line 157
    .line 158
    move-result p0

    .line 159
    int-to-float p0, p0

    .line 160
    mul-float p0, p0, v0

    .line 161
    .line 162
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 163
    .line 164
    .line 165
    move-result p0

    .line 166
    goto :goto_3

    .line 167
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 168
    .line 169
    .line 170
    move-result p0

    .line 171
    int-to-float p0, p0

    .line 172
    mul-float p0, p0, v3

    .line 173
    .line 174
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 175
    .line 176
    .line 177
    move-result v3

    .line 178
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 179
    .line 180
    .line 181
    move-result p0

    .line 182
    int-to-float p0, p0

    .line 183
    mul-float p0, p0, v2

    .line 184
    .line 185
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 186
    .line 187
    .line 188
    move-result v2

    .line 189
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 190
    .line 191
    .line 192
    move-result p0

    .line 193
    int-to-float p0, p0

    .line 194
    mul-float p0, p0, v1

    .line 195
    .line 196
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 197
    .line 198
    .line 199
    move-result v1

    .line 200
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 201
    .line 202
    .line 203
    move-result p0

    .line 204
    int-to-float p0, p0

    .line 205
    mul-float p0, p0, v0

    .line 206
    .line 207
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 208
    .line 209
    .line 210
    move-result p0

    .line 211
    :goto_3
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 212
    .line 213
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 214
    .line 215
    int-to-float v4, v4

    .line 216
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 217
    .line 218
    add-int/2addr v5, v3

    .line 219
    int-to-float v5, v5

    .line 220
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 221
    .line 222
    .line 223
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 224
    .line 225
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 226
    .line 227
    int-to-float v4, v4

    .line 228
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 229
    .line 230
    int-to-float v5, v5

    .line 231
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 232
    .line 233
    .line 234
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 235
    .line 236
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 237
    .line 238
    int-to-float v4, v4

    .line 239
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 240
    .line 241
    int-to-float v5, v5

    .line 242
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 243
    .line 244
    .line 245
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 246
    .line 247
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 248
    .line 249
    int-to-float v4, v4

    .line 250
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 251
    .line 252
    add-int/2addr v5, v3

    .line 253
    int-to-float v5, v5

    .line 254
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 255
    .line 256
    .line 257
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 258
    .line 259
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 260
    .line 261
    sub-int/2addr v4, p0

    .line 262
    int-to-float v4, v4

    .line 263
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 264
    .line 265
    add-int/2addr v5, v3

    .line 266
    int-to-float v5, v5

    .line 267
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 268
    .line 269
    .line 270
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 271
    .line 272
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 273
    .line 274
    sub-int/2addr v4, p0

    .line 275
    int-to-float v4, v4

    .line 276
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 277
    .line 278
    add-int/2addr v5, v1

    .line 279
    int-to-float v5, v5

    .line 280
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 281
    .line 282
    .line 283
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 284
    .line 285
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 286
    .line 287
    sub-int/2addr v4, v2

    .line 288
    int-to-float v4, v4

    .line 289
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 290
    .line 291
    add-int/2addr v5, v1

    .line 292
    int-to-float v5, v5

    .line 293
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 294
    .line 295
    .line 296
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 297
    .line 298
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 299
    .line 300
    .line 301
    move-result v4

    .line 302
    int-to-float v4, v4

    .line 303
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 304
    .line 305
    int-to-float v5, v5

    .line 306
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 307
    .line 308
    .line 309
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 310
    .line 311
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 312
    .line 313
    add-int/2addr v4, v2

    .line 314
    int-to-float v2, v4

    .line 315
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 316
    .line 317
    add-int/2addr v4, v1

    .line 318
    int-to-float v4, v4

    .line 319
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 320
    .line 321
    .line 322
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 323
    .line 324
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 325
    .line 326
    add-int/2addr v2, p0

    .line 327
    int-to-float v2, v2

    .line 328
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 329
    .line 330
    add-int/2addr v4, v1

    .line 331
    int-to-float v1, v4

    .line 332
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 333
    .line 334
    .line 335
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 336
    .line 337
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 338
    .line 339
    add-int/2addr v1, p0

    .line 340
    int-to-float p0, v1

    .line 341
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 342
    .line 343
    add-int/2addr p1, v3

    .line 344
    int-to-float p1, p1

    .line 345
    invoke-virtual {v0, p0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 346
    .line 347
    .line 348
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 349
    .line 350
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 351
    .line 352
    .line 353
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 354
    .line 355
    return-object p0
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getDownArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/high16 v0, 0x3e800000    # 0.25f

    .line 6
    .line 7
    const/high16 v1, 0x3f400000    # 0.75f

    .line 8
    .line 9
    if-eqz p0, :cond_2

    .line 10
    .line 11
    array-length v2, p0

    .line 12
    const/4 v3, 0x1

    .line 13
    if-lt v2, v3, :cond_2

    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    aget-object v4, p0, v2

    .line 17
    .line 18
    if-eqz v4, :cond_0

    .line 19
    .line 20
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    int-to-float v1, v1

    .line 25
    aget-object v2, p0, v2

    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    mul-float v1, v1, v2

    .line 32
    .line 33
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    goto :goto_0

    .line 38
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    int-to-float v2, v2

    .line 43
    mul-float v2, v2, v1

    .line 44
    .line 45
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    :goto_0
    array-length v2, p0

    .line 50
    const/4 v4, 0x2

    .line 51
    if-lt v2, v4, :cond_1

    .line 52
    .line 53
    aget-object v2, p0, v3

    .line 54
    .line 55
    if-eqz v2, :cond_1

    .line 56
    .line 57
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    int-to-float v0, v0

    .line 62
    aget-object p0, p0, v3

    .line 63
    .line 64
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 65
    .line 66
    .line 67
    move-result p0

    .line 68
    mul-float v0, v0, p0

    .line 69
    .line 70
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 71
    .line 72
    .line 73
    move-result p0

    .line 74
    goto :goto_1

    .line 75
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 76
    .line 77
    .line 78
    move-result p0

    .line 79
    int-to-float p0, p0

    .line 80
    mul-float p0, p0, v0

    .line 81
    .line 82
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 83
    .line 84
    .line 85
    move-result p0

    .line 86
    goto :goto_1

    .line 87
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 88
    .line 89
    .line 90
    move-result p0

    .line 91
    int-to-float p0, p0

    .line 92
    mul-float p0, p0, v1

    .line 93
    .line 94
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 95
    .line 96
    .line 97
    move-result v1

    .line 98
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 99
    .line 100
    .line 101
    move-result p0

    .line 102
    int-to-float p0, p0

    .line 103
    mul-float p0, p0, v0

    .line 104
    .line 105
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 106
    .line 107
    .line 108
    move-result p0

    .line 109
    :goto_1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 110
    .line 111
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 112
    .line 113
    add-int/2addr v2, p0

    .line 114
    int-to-float v2, v2

    .line 115
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 116
    .line 117
    int-to-float v3, v3

    .line 118
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 119
    .line 120
    .line 121
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 122
    .line 123
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 124
    .line 125
    sub-int/2addr v2, p0

    .line 126
    int-to-float v2, v2

    .line 127
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 128
    .line 129
    int-to-float v3, v3

    .line 130
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 131
    .line 132
    .line 133
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 134
    .line 135
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 136
    .line 137
    sub-int/2addr v2, p0

    .line 138
    int-to-float v2, v2

    .line 139
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 140
    .line 141
    add-int/2addr v3, v1

    .line 142
    int-to-float v3, v3

    .line 143
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 144
    .line 145
    .line 146
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 147
    .line 148
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 149
    .line 150
    int-to-float v2, v2

    .line 151
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 152
    .line 153
    add-int/2addr v3, v1

    .line 154
    int-to-float v3, v3

    .line 155
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 156
    .line 157
    .line 158
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 159
    .line 160
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 161
    .line 162
    .line 163
    move-result v2

    .line 164
    int-to-float v2, v2

    .line 165
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 166
    .line 167
    int-to-float v3, v3

    .line 168
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    .line 170
    .line 171
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 172
    .line 173
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 174
    .line 175
    int-to-float v2, v2

    .line 176
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 177
    .line 178
    add-int/2addr v3, v1

    .line 179
    int-to-float v3, v3

    .line 180
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 181
    .line 182
    .line 183
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 184
    .line 185
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 186
    .line 187
    add-int/2addr v2, p0

    .line 188
    int-to-float p0, v2

    .line 189
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 190
    .line 191
    add-int/2addr p1, v1

    .line 192
    int-to-float p1, p1

    .line 193
    invoke-virtual {v0, p0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 194
    .line 195
    .line 196
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 197
    .line 198
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 199
    .line 200
    .line 201
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 202
    .line 203
    return-object p0
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getHomePlatePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    if-eqz p0, :cond_0

    .line 6
    .line 7
    array-length v0, p0

    .line 8
    const/4 v1, 0x1

    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    aget-object v1, p0, v0

    .line 13
    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    int-to-float v1, v1

    .line 21
    aget-object p0, p0, v0

    .line 22
    .line 23
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 24
    .line 25
    .line 26
    move-result p0

    .line 27
    mul-float v1, v1, p0

    .line 28
    .line 29
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 30
    .line 31
    .line 32
    move-result p0

    .line 33
    goto :goto_0

    .line 34
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 35
    .line 36
    .line 37
    move-result p0

    .line 38
    int-to-float p0, p0

    .line 39
    const/high16 v0, 0x3f400000    # 0.75f

    .line 40
    .line 41
    mul-float p0, p0, v0

    .line 42
    .line 43
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 44
    .line 45
    .line 46
    move-result p0

    .line 47
    :goto_0
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 48
    .line 49
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 50
    .line 51
    int-to-float v1, v1

    .line 52
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 53
    .line 54
    int-to-float v2, v2

    .line 55
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 56
    .line 57
    .line 58
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 59
    .line 60
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 61
    .line 62
    add-int/2addr v1, p0

    .line 63
    int-to-float v1, v1

    .line 64
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 65
    .line 66
    int-to-float v2, v2

    .line 67
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 68
    .line 69
    .line 70
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 71
    .line 72
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 73
    .line 74
    int-to-float v1, v1

    .line 75
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 76
    .line 77
    .line 78
    move-result v2

    .line 79
    int-to-float v2, v2

    .line 80
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 81
    .line 82
    .line 83
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 84
    .line 85
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 86
    .line 87
    add-int/2addr v1, p0

    .line 88
    int-to-float p0, v1

    .line 89
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 90
    .line 91
    int-to-float v1, v1

    .line 92
    invoke-virtual {v0, p0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 93
    .line 94
    .line 95
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 96
    .line 97
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 98
    .line 99
    int-to-float v0, v0

    .line 100
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 101
    .line 102
    int-to-float p1, p1

    .line 103
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 104
    .line 105
    .line 106
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 107
    .line 108
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 109
    .line 110
    .line 111
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 112
    .line 113
    return-object p0
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getLeftArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/high16 v0, 0x3ec00000    # 0.375f

    .line 6
    .line 7
    const v1, 0x3e2e147b    # 0.17f

    .line 8
    .line 9
    .line 10
    const/high16 v2, 0x3e800000    # 0.25f

    .line 11
    .line 12
    const v3, 0x3ea8f5c3    # 0.33f

    .line 13
    .line 14
    .line 15
    if-eqz p0, :cond_4

    .line 16
    .line 17
    array-length v4, p0

    .line 18
    const/4 v5, 0x1

    .line 19
    if-lt v4, v5, :cond_4

    .line 20
    .line 21
    const/4 v4, 0x0

    .line 22
    aget-object v6, p0, v4

    .line 23
    .line 24
    if-eqz v6, :cond_0

    .line 25
    .line 26
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    int-to-float v3, v3

    .line 31
    aget-object v4, p0, v4

    .line 32
    .line 33
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    mul-float v3, v3, v4

    .line 38
    .line 39
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    goto :goto_0

    .line 44
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    int-to-float v4, v4

    .line 49
    mul-float v4, v4, v3

    .line 50
    .line 51
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 52
    .line 53
    .line 54
    move-result v3

    .line 55
    :goto_0
    array-length v4, p0

    .line 56
    const/4 v6, 0x2

    .line 57
    if-lt v4, v6, :cond_1

    .line 58
    .line 59
    aget-object v4, p0, v5

    .line 60
    .line 61
    if-eqz v4, :cond_1

    .line 62
    .line 63
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    int-to-float v2, v2

    .line 68
    aget-object v4, p0, v5

    .line 69
    .line 70
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 71
    .line 72
    .line 73
    move-result v4

    .line 74
    mul-float v2, v2, v4

    .line 75
    .line 76
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 77
    .line 78
    .line 79
    move-result v2

    .line 80
    goto :goto_1

    .line 81
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 82
    .line 83
    .line 84
    move-result v4

    .line 85
    int-to-float v4, v4

    .line 86
    mul-float v4, v4, v2

    .line 87
    .line 88
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 89
    .line 90
    .line 91
    move-result v2

    .line 92
    :goto_1
    array-length v4, p0

    .line 93
    const/4 v5, 0x3

    .line 94
    if-lt v4, v5, :cond_2

    .line 95
    .line 96
    aget-object v4, p0, v6

    .line 97
    .line 98
    if-eqz v4, :cond_2

    .line 99
    .line 100
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 101
    .line 102
    .line 103
    move-result v1

    .line 104
    int-to-float v1, v1

    .line 105
    aget-object v4, p0, v6

    .line 106
    .line 107
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 108
    .line 109
    .line 110
    move-result v4

    .line 111
    mul-float v1, v1, v4

    .line 112
    .line 113
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 114
    .line 115
    .line 116
    move-result v1

    .line 117
    goto :goto_2

    .line 118
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 119
    .line 120
    .line 121
    move-result v4

    .line 122
    int-to-float v4, v4

    .line 123
    mul-float v4, v4, v1

    .line 124
    .line 125
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 126
    .line 127
    .line 128
    move-result v1

    .line 129
    :goto_2
    array-length v4, p0

    .line 130
    const/4 v6, 0x4

    .line 131
    if-lt v4, v6, :cond_3

    .line 132
    .line 133
    aget-object v4, p0, v5

    .line 134
    .line 135
    if-eqz v4, :cond_3

    .line 136
    .line 137
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 138
    .line 139
    .line 140
    move-result v0

    .line 141
    int-to-float v0, v0

    .line 142
    aget-object p0, p0, v5

    .line 143
    .line 144
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 145
    .line 146
    .line 147
    move-result p0

    .line 148
    mul-float v0, v0, p0

    .line 149
    .line 150
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 151
    .line 152
    .line 153
    move-result p0

    .line 154
    goto :goto_3

    .line 155
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 156
    .line 157
    .line 158
    move-result p0

    .line 159
    int-to-float p0, p0

    .line 160
    mul-float p0, p0, v0

    .line 161
    .line 162
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 163
    .line 164
    .line 165
    move-result p0

    .line 166
    goto :goto_3

    .line 167
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 168
    .line 169
    .line 170
    move-result p0

    .line 171
    int-to-float p0, p0

    .line 172
    mul-float p0, p0, v3

    .line 173
    .line 174
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 175
    .line 176
    .line 177
    move-result v3

    .line 178
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 179
    .line 180
    .line 181
    move-result p0

    .line 182
    int-to-float p0, p0

    .line 183
    mul-float p0, p0, v2

    .line 184
    .line 185
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 186
    .line 187
    .line 188
    move-result v2

    .line 189
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 190
    .line 191
    .line 192
    move-result p0

    .line 193
    int-to-float p0, p0

    .line 194
    mul-float p0, p0, v1

    .line 195
    .line 196
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 197
    .line 198
    .line 199
    move-result v1

    .line 200
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 201
    .line 202
    .line 203
    move-result p0

    .line 204
    int-to-float p0, p0

    .line 205
    mul-float p0, p0, v0

    .line 206
    .line 207
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 208
    .line 209
    .line 210
    move-result p0

    .line 211
    :goto_3
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 212
    .line 213
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 214
    .line 215
    int-to-float v4, v4

    .line 216
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 217
    .line 218
    .line 219
    move-result v5

    .line 220
    int-to-float v5, v5

    .line 221
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 222
    .line 223
    .line 224
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 225
    .line 226
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 227
    .line 228
    add-int/2addr v4, v1

    .line 229
    int-to-float v4, v4

    .line 230
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 231
    .line 232
    add-int/2addr v5, v2

    .line 233
    int-to-float v5, v5

    .line 234
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 235
    .line 236
    .line 237
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 238
    .line 239
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 240
    .line 241
    add-int/2addr v4, v1

    .line 242
    int-to-float v4, v4

    .line 243
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 244
    .line 245
    add-int/2addr v5, p0

    .line 246
    int-to-float v5, v5

    .line 247
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 248
    .line 249
    .line 250
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 251
    .line 252
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 253
    .line 254
    add-int/2addr v4, v3

    .line 255
    int-to-float v4, v4

    .line 256
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 257
    .line 258
    add-int/2addr v5, p0

    .line 259
    int-to-float v5, v5

    .line 260
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 261
    .line 262
    .line 263
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 264
    .line 265
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 266
    .line 267
    add-int/2addr v4, v3

    .line 268
    int-to-float v4, v4

    .line 269
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 270
    .line 271
    int-to-float v5, v5

    .line 272
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 273
    .line 274
    .line 275
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 276
    .line 277
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 278
    .line 279
    int-to-float v4, v4

    .line 280
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 281
    .line 282
    int-to-float v5, v5

    .line 283
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 284
    .line 285
    .line 286
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 287
    .line 288
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 289
    .line 290
    int-to-float v4, v4

    .line 291
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 292
    .line 293
    int-to-float v5, v5

    .line 294
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 295
    .line 296
    .line 297
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 298
    .line 299
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 300
    .line 301
    add-int/2addr v4, v3

    .line 302
    int-to-float v4, v4

    .line 303
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 304
    .line 305
    int-to-float v5, v5

    .line 306
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 307
    .line 308
    .line 309
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 310
    .line 311
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 312
    .line 313
    add-int/2addr v4, v3

    .line 314
    int-to-float v3, v4

    .line 315
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 316
    .line 317
    sub-int/2addr v4, p0

    .line 318
    int-to-float v4, v4

    .line 319
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 320
    .line 321
    .line 322
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 323
    .line 324
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 325
    .line 326
    add-int/2addr v3, v1

    .line 327
    int-to-float v3, v3

    .line 328
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 329
    .line 330
    sub-int/2addr v4, p0

    .line 331
    int-to-float p0, v4

    .line 332
    invoke-virtual {v0, v3, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 333
    .line 334
    .line 335
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 336
    .line 337
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 338
    .line 339
    add-int/2addr v0, v1

    .line 340
    int-to-float v0, v0

    .line 341
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 342
    .line 343
    sub-int/2addr p1, v2

    .line 344
    int-to-float p1, p1

    .line 345
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 346
    .line 347
    .line 348
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 349
    .line 350
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 351
    .line 352
    .line 353
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 354
    .line 355
    return-object p0
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getLeftArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/high16 v0, 0x3e800000    # 0.25f

    .line 6
    .line 7
    if-eqz p0, :cond_2

    .line 8
    .line 9
    array-length v1, p0

    .line 10
    const/4 v2, 0x1

    .line 11
    if-lt v1, v2, :cond_2

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    aget-object v3, p0, v1

    .line 15
    .line 16
    if-eqz v3, :cond_0

    .line 17
    .line 18
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    int-to-float v3, v3

    .line 23
    aget-object v1, p0, v1

    .line 24
    .line 25
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    mul-float v3, v3, v1

    .line 30
    .line 31
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    int-to-float v1, v1

    .line 41
    mul-float v1, v1, v0

    .line 42
    .line 43
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    :goto_0
    array-length v3, p0

    .line 48
    const/4 v4, 0x2

    .line 49
    if-lt v3, v4, :cond_1

    .line 50
    .line 51
    aget-object v3, p0, v2

    .line 52
    .line 53
    if-eqz v3, :cond_1

    .line 54
    .line 55
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    int-to-float v0, v0

    .line 60
    aget-object p0, p0, v2

    .line 61
    .line 62
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 63
    .line 64
    .line 65
    move-result p0

    .line 66
    mul-float v0, v0, p0

    .line 67
    .line 68
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 69
    .line 70
    .line 71
    move-result p0

    .line 72
    goto :goto_1

    .line 73
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 74
    .line 75
    .line 76
    move-result p0

    .line 77
    int-to-float p0, p0

    .line 78
    mul-float p0, p0, v0

    .line 79
    .line 80
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 81
    .line 82
    .line 83
    move-result p0

    .line 84
    goto :goto_1

    .line 85
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 86
    .line 87
    .line 88
    move-result p0

    .line 89
    int-to-float p0, p0

    .line 90
    mul-float p0, p0, v0

    .line 91
    .line 92
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 97
    .line 98
    .line 99
    move-result p0

    .line 100
    int-to-float p0, p0

    .line 101
    mul-float p0, p0, v0

    .line 102
    .line 103
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 104
    .line 105
    .line 106
    move-result p0

    .line 107
    :goto_1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 108
    .line 109
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 110
    .line 111
    int-to-float v2, v2

    .line 112
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 113
    .line 114
    .line 115
    move-result v3

    .line 116
    int-to-float v3, v3

    .line 117
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 118
    .line 119
    .line 120
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 121
    .line 122
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 123
    .line 124
    add-int/2addr v2, v1

    .line 125
    int-to-float v2, v2

    .line 126
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 127
    .line 128
    int-to-float v3, v3

    .line 129
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 130
    .line 131
    .line 132
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 133
    .line 134
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 135
    .line 136
    add-int/2addr v2, v1

    .line 137
    int-to-float v2, v2

    .line 138
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 139
    .line 140
    add-int/2addr v3, p0

    .line 141
    int-to-float v3, v3

    .line 142
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 143
    .line 144
    .line 145
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 146
    .line 147
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 148
    .line 149
    int-to-float v2, v2

    .line 150
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 151
    .line 152
    add-int/2addr v3, p0

    .line 153
    int-to-float v3, v3

    .line 154
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 155
    .line 156
    .line 157
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 158
    .line 159
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 160
    .line 161
    int-to-float v2, v2

    .line 162
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 163
    .line 164
    sub-int/2addr v3, p0

    .line 165
    int-to-float v3, v3

    .line 166
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 167
    .line 168
    .line 169
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 170
    .line 171
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 172
    .line 173
    add-int/2addr v2, v1

    .line 174
    int-to-float v2, v2

    .line 175
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 176
    .line 177
    sub-int/2addr v3, p0

    .line 178
    int-to-float p0, v3

    .line 179
    invoke-virtual {v0, v2, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 180
    .line 181
    .line 182
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 183
    .line 184
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 185
    .line 186
    add-int/2addr v0, v1

    .line 187
    int-to-float v0, v0

    .line 188
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 189
    .line 190
    int-to-float p1, p1

    .line 191
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 192
    .line 193
    .line 194
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 195
    .line 196
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 197
    .line 198
    .line 199
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 200
    .line 201
    return-object p0
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getLeftRightArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/high16 v0, 0x3ec00000    # 0.375f

    .line 6
    .line 7
    const v1, 0x3e051eb8    # 0.13f

    .line 8
    .line 9
    .line 10
    const/high16 v2, 0x3e800000    # 0.25f

    .line 11
    .line 12
    const v3, 0x3eb33333    # 0.35f

    .line 13
    .line 14
    .line 15
    if-eqz p0, :cond_4

    .line 16
    .line 17
    array-length v4, p0

    .line 18
    const/4 v5, 0x1

    .line 19
    if-lt v4, v5, :cond_4

    .line 20
    .line 21
    const/4 v4, 0x0

    .line 22
    aget-object v6, p0, v4

    .line 23
    .line 24
    if-eqz v6, :cond_0

    .line 25
    .line 26
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    int-to-float v3, v3

    .line 31
    aget-object v4, p0, v4

    .line 32
    .line 33
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    mul-float v3, v3, v4

    .line 38
    .line 39
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    goto :goto_0

    .line 44
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    int-to-float v4, v4

    .line 49
    mul-float v4, v4, v3

    .line 50
    .line 51
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 52
    .line 53
    .line 54
    move-result v3

    .line 55
    :goto_0
    array-length v4, p0

    .line 56
    const/4 v6, 0x2

    .line 57
    if-lt v4, v6, :cond_1

    .line 58
    .line 59
    aget-object v4, p0, v5

    .line 60
    .line 61
    if-eqz v4, :cond_1

    .line 62
    .line 63
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    int-to-float v2, v2

    .line 68
    aget-object v4, p0, v5

    .line 69
    .line 70
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 71
    .line 72
    .line 73
    move-result v4

    .line 74
    mul-float v2, v2, v4

    .line 75
    .line 76
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 77
    .line 78
    .line 79
    move-result v2

    .line 80
    goto :goto_1

    .line 81
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 82
    .line 83
    .line 84
    move-result v4

    .line 85
    int-to-float v4, v4

    .line 86
    mul-float v4, v4, v2

    .line 87
    .line 88
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 89
    .line 90
    .line 91
    move-result v2

    .line 92
    :goto_1
    array-length v4, p0

    .line 93
    const/4 v5, 0x3

    .line 94
    if-lt v4, v5, :cond_2

    .line 95
    .line 96
    aget-object v4, p0, v6

    .line 97
    .line 98
    if-eqz v4, :cond_2

    .line 99
    .line 100
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 101
    .line 102
    .line 103
    move-result v1

    .line 104
    int-to-float v1, v1

    .line 105
    aget-object v4, p0, v6

    .line 106
    .line 107
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 108
    .line 109
    .line 110
    move-result v4

    .line 111
    mul-float v1, v1, v4

    .line 112
    .line 113
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 114
    .line 115
    .line 116
    move-result v1

    .line 117
    goto :goto_2

    .line 118
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 119
    .line 120
    .line 121
    move-result v4

    .line 122
    int-to-float v4, v4

    .line 123
    mul-float v4, v4, v1

    .line 124
    .line 125
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 126
    .line 127
    .line 128
    move-result v1

    .line 129
    :goto_2
    array-length v4, p0

    .line 130
    const/4 v6, 0x4

    .line 131
    if-lt v4, v6, :cond_3

    .line 132
    .line 133
    aget-object v4, p0, v5

    .line 134
    .line 135
    if-eqz v4, :cond_3

    .line 136
    .line 137
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 138
    .line 139
    .line 140
    move-result v0

    .line 141
    int-to-float v0, v0

    .line 142
    aget-object p0, p0, v5

    .line 143
    .line 144
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 145
    .line 146
    .line 147
    move-result p0

    .line 148
    mul-float v0, v0, p0

    .line 149
    .line 150
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 151
    .line 152
    .line 153
    move-result p0

    .line 154
    goto :goto_3

    .line 155
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 156
    .line 157
    .line 158
    move-result p0

    .line 159
    int-to-float p0, p0

    .line 160
    mul-float p0, p0, v0

    .line 161
    .line 162
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 163
    .line 164
    .line 165
    move-result p0

    .line 166
    goto :goto_3

    .line 167
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 168
    .line 169
    .line 170
    move-result p0

    .line 171
    int-to-float p0, p0

    .line 172
    mul-float p0, p0, v3

    .line 173
    .line 174
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 175
    .line 176
    .line 177
    move-result v3

    .line 178
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 179
    .line 180
    .line 181
    move-result p0

    .line 182
    int-to-float p0, p0

    .line 183
    mul-float p0, p0, v2

    .line 184
    .line 185
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 186
    .line 187
    .line 188
    move-result v2

    .line 189
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 190
    .line 191
    .line 192
    move-result p0

    .line 193
    int-to-float p0, p0

    .line 194
    mul-float p0, p0, v1

    .line 195
    .line 196
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 197
    .line 198
    .line 199
    move-result v1

    .line 200
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 201
    .line 202
    .line 203
    move-result p0

    .line 204
    int-to-float p0, p0

    .line 205
    mul-float p0, p0, v0

    .line 206
    .line 207
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 208
    .line 209
    .line 210
    move-result p0

    .line 211
    :goto_3
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 212
    .line 213
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 214
    .line 215
    add-int/2addr v4, v3

    .line 216
    int-to-float v4, v4

    .line 217
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 218
    .line 219
    sub-int/2addr v5, p0

    .line 220
    int-to-float v5, v5

    .line 221
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 222
    .line 223
    .line 224
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 225
    .line 226
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 227
    .line 228
    add-int/2addr v4, v1

    .line 229
    int-to-float v4, v4

    .line 230
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 231
    .line 232
    sub-int/2addr v5, p0

    .line 233
    int-to-float v5, v5

    .line 234
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 235
    .line 236
    .line 237
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 238
    .line 239
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 240
    .line 241
    add-int/2addr v4, v1

    .line 242
    int-to-float v4, v4

    .line 243
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 244
    .line 245
    sub-int/2addr v5, v2

    .line 246
    int-to-float v5, v5

    .line 247
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 248
    .line 249
    .line 250
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 251
    .line 252
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 253
    .line 254
    int-to-float v4, v4

    .line 255
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 256
    .line 257
    .line 258
    move-result v5

    .line 259
    int-to-float v5, v5

    .line 260
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 261
    .line 262
    .line 263
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 264
    .line 265
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 266
    .line 267
    add-int/2addr v4, v1

    .line 268
    int-to-float v4, v4

    .line 269
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 270
    .line 271
    add-int/2addr v5, v2

    .line 272
    int-to-float v5, v5

    .line 273
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 274
    .line 275
    .line 276
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 277
    .line 278
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 279
    .line 280
    add-int/2addr v4, v1

    .line 281
    int-to-float v4, v4

    .line 282
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 283
    .line 284
    add-int/2addr v5, p0

    .line 285
    int-to-float v5, v5

    .line 286
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 287
    .line 288
    .line 289
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 290
    .line 291
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 292
    .line 293
    add-int/2addr v4, v3

    .line 294
    int-to-float v4, v4

    .line 295
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 296
    .line 297
    add-int/2addr v5, p0

    .line 298
    int-to-float v5, v5

    .line 299
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 300
    .line 301
    .line 302
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 303
    .line 304
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 305
    .line 306
    add-int/2addr v4, v3

    .line 307
    int-to-float v4, v4

    .line 308
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 309
    .line 310
    int-to-float v5, v5

    .line 311
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 312
    .line 313
    .line 314
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 315
    .line 316
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 317
    .line 318
    sub-int/2addr v4, v3

    .line 319
    int-to-float v4, v4

    .line 320
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 321
    .line 322
    int-to-float v5, v5

    .line 323
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 324
    .line 325
    .line 326
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 327
    .line 328
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 329
    .line 330
    sub-int/2addr v4, v3

    .line 331
    int-to-float v4, v4

    .line 332
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 333
    .line 334
    add-int/2addr v5, p0

    .line 335
    int-to-float v5, v5

    .line 336
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 337
    .line 338
    .line 339
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 340
    .line 341
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 342
    .line 343
    sub-int/2addr v4, v1

    .line 344
    int-to-float v4, v4

    .line 345
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 346
    .line 347
    add-int/2addr v5, p0

    .line 348
    int-to-float v5, v5

    .line 349
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 350
    .line 351
    .line 352
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 353
    .line 354
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 355
    .line 356
    sub-int/2addr v4, v1

    .line 357
    int-to-float v4, v4

    .line 358
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 359
    .line 360
    add-int/2addr v5, v2

    .line 361
    int-to-float v5, v5

    .line 362
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 363
    .line 364
    .line 365
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 366
    .line 367
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 368
    .line 369
    int-to-float v4, v4

    .line 370
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 371
    .line 372
    .line 373
    move-result v5

    .line 374
    int-to-float v5, v5

    .line 375
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 376
    .line 377
    .line 378
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 379
    .line 380
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 381
    .line 382
    sub-int/2addr v4, v1

    .line 383
    int-to-float v4, v4

    .line 384
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 385
    .line 386
    sub-int/2addr v5, v2

    .line 387
    int-to-float v2, v5

    .line 388
    invoke-virtual {v0, v4, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 389
    .line 390
    .line 391
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 392
    .line 393
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 394
    .line 395
    sub-int/2addr v2, v1

    .line 396
    int-to-float v1, v2

    .line 397
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 398
    .line 399
    sub-int/2addr v2, p0

    .line 400
    int-to-float v2, v2

    .line 401
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 402
    .line 403
    .line 404
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 405
    .line 406
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 407
    .line 408
    sub-int/2addr v1, v3

    .line 409
    int-to-float v1, v1

    .line 410
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 411
    .line 412
    sub-int/2addr v2, p0

    .line 413
    int-to-float p0, v2

    .line 414
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 415
    .line 416
    .line 417
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 418
    .line 419
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 420
    .line 421
    sub-int/2addr v0, v3

    .line 422
    int-to-float v0, v0

    .line 423
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 424
    .line 425
    int-to-float v1, v1

    .line 426
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 427
    .line 428
    .line 429
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 430
    .line 431
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 432
    .line 433
    add-int/2addr v0, v3

    .line 434
    int-to-float v0, v0

    .line 435
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 436
    .line 437
    int-to-float p1, p1

    .line 438
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 439
    .line 440
    .line 441
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 442
    .line 443
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 444
    .line 445
    .line 446
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 447
    .line 448
    return-object p0
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getLeftRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/high16 v0, 0x3e800000    # 0.25f

    .line 6
    .line 7
    const v1, 0x3e4ccccd    # 0.2f

    .line 8
    .line 9
    .line 10
    if-eqz p0, :cond_2

    .line 11
    .line 12
    array-length v2, p0

    .line 13
    const/4 v3, 0x1

    .line 14
    if-lt v2, v3, :cond_2

    .line 15
    .line 16
    const/4 v2, 0x0

    .line 17
    aget-object v4, p0, v2

    .line 18
    .line 19
    if-eqz v4, :cond_0

    .line 20
    .line 21
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    int-to-float v1, v1

    .line 26
    aget-object v2, p0, v2

    .line 27
    .line 28
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    mul-float v1, v1, v2

    .line 33
    .line 34
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    goto :goto_0

    .line 39
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    int-to-float v2, v2

    .line 44
    mul-float v2, v2, v1

    .line 45
    .line 46
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    :goto_0
    array-length v2, p0

    .line 51
    const/4 v4, 0x2

    .line 52
    if-lt v2, v4, :cond_1

    .line 53
    .line 54
    aget-object v2, p0, v3

    .line 55
    .line 56
    if-eqz v2, :cond_1

    .line 57
    .line 58
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 59
    .line 60
    .line 61
    move-result v0

    .line 62
    int-to-float v0, v0

    .line 63
    aget-object p0, p0, v3

    .line 64
    .line 65
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 66
    .line 67
    .line 68
    move-result p0

    .line 69
    mul-float v0, v0, p0

    .line 70
    .line 71
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 72
    .line 73
    .line 74
    move-result p0

    .line 75
    goto :goto_1

    .line 76
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 77
    .line 78
    .line 79
    move-result p0

    .line 80
    int-to-float p0, p0

    .line 81
    mul-float p0, p0, v0

    .line 82
    .line 83
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 84
    .line 85
    .line 86
    move-result p0

    .line 87
    goto :goto_1

    .line 88
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 89
    .line 90
    .line 91
    move-result p0

    .line 92
    int-to-float p0, p0

    .line 93
    mul-float p0, p0, v1

    .line 94
    .line 95
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 96
    .line 97
    .line 98
    move-result v1

    .line 99
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 100
    .line 101
    .line 102
    move-result p0

    .line 103
    int-to-float p0, p0

    .line 104
    mul-float p0, p0, v0

    .line 105
    .line 106
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 107
    .line 108
    .line 109
    move-result p0

    .line 110
    :goto_1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 111
    .line 112
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 113
    .line 114
    int-to-float v2, v2

    .line 115
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 116
    .line 117
    .line 118
    move-result v3

    .line 119
    int-to-float v3, v3

    .line 120
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 121
    .line 122
    .line 123
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 124
    .line 125
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 126
    .line 127
    add-int/2addr v2, v1

    .line 128
    int-to-float v2, v2

    .line 129
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 130
    .line 131
    int-to-float v3, v3

    .line 132
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 133
    .line 134
    .line 135
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 136
    .line 137
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 138
    .line 139
    add-int/2addr v2, v1

    .line 140
    int-to-float v2, v2

    .line 141
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 142
    .line 143
    add-int/2addr v3, p0

    .line 144
    int-to-float v3, v3

    .line 145
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 146
    .line 147
    .line 148
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 149
    .line 150
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 151
    .line 152
    sub-int/2addr v2, v1

    .line 153
    int-to-float v2, v2

    .line 154
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 155
    .line 156
    add-int/2addr v3, p0

    .line 157
    int-to-float v3, v3

    .line 158
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 159
    .line 160
    .line 161
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 162
    .line 163
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 164
    .line 165
    sub-int/2addr v2, v1

    .line 166
    int-to-float v2, v2

    .line 167
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 168
    .line 169
    int-to-float v3, v3

    .line 170
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 171
    .line 172
    .line 173
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 174
    .line 175
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 176
    .line 177
    int-to-float v2, v2

    .line 178
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 179
    .line 180
    .line 181
    move-result v3

    .line 182
    int-to-float v3, v3

    .line 183
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 184
    .line 185
    .line 186
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 187
    .line 188
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 189
    .line 190
    sub-int/2addr v2, v1

    .line 191
    int-to-float v2, v2

    .line 192
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 193
    .line 194
    int-to-float v3, v3

    .line 195
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 196
    .line 197
    .line 198
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 199
    .line 200
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 201
    .line 202
    sub-int/2addr v2, v1

    .line 203
    int-to-float v2, v2

    .line 204
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 205
    .line 206
    sub-int/2addr v3, p0

    .line 207
    int-to-float v3, v3

    .line 208
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 209
    .line 210
    .line 211
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 212
    .line 213
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 214
    .line 215
    add-int/2addr v2, v1

    .line 216
    int-to-float v2, v2

    .line 217
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 218
    .line 219
    sub-int/2addr v3, p0

    .line 220
    int-to-float p0, v3

    .line 221
    invoke-virtual {v0, v2, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 222
    .line 223
    .line 224
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 225
    .line 226
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 227
    .line 228
    add-int/2addr v0, v1

    .line 229
    int-to-float v0, v0

    .line 230
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 231
    .line 232
    int-to-float p1, p1

    .line 233
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 234
    .line 235
    .line 236
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 237
    .line 238
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 239
    .line 240
    .line 241
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 242
    .line 243
    return-object p0
    .line 244
    .line 245
.end method

.method private static getLeftRightUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 14

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const v0, 0x3ecccccd    # 0.4f

    .line 6
    .line 7
    .line 8
    const v1, 0x3dcccccd    # 0.1f

    .line 9
    .line 10
    .line 11
    const v2, 0x3f333333    # 0.7f

    .line 12
    .line 13
    .line 14
    const v3, 0x3e99999a    # 0.3f

    .line 15
    .line 16
    .line 17
    const v4, 0x3e4ccccd    # 0.2f

    .line 18
    .line 19
    .line 20
    const/high16 v5, 0x40e00000    # 7.0f

    .line 21
    .line 22
    const/high16 v6, 0x41200000    # 10.0f

    .line 23
    .line 24
    if-eqz p0, :cond_3

    .line 25
    .line 26
    array-length v7, p0

    .line 27
    const/4 v8, 0x1

    .line 28
    if-lt v7, v8, :cond_3

    .line 29
    .line 30
    const/4 v7, 0x0

    .line 31
    aget-object v9, p0, v7

    .line 32
    .line 33
    const/high16 v10, 0x3f000000    # 0.5f

    .line 34
    .line 35
    if-eqz v9, :cond_0

    .line 36
    .line 37
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    int-to-float v3, v3

    .line 42
    aget-object v9, p0, v7

    .line 43
    .line 44
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 45
    .line 46
    .line 47
    move-result v9

    .line 48
    sub-float v9, v10, v9

    .line 49
    .line 50
    mul-float v3, v3, v9

    .line 51
    .line 52
    mul-float v3, v3, v6

    .line 53
    .line 54
    div-float/2addr v3, v5

    .line 55
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 56
    .line 57
    .line 58
    move-result v3

    .line 59
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 60
    .line 61
    .line 62
    move-result v9

    .line 63
    int-to-float v9, v9

    .line 64
    aget-object v7, p0, v7

    .line 65
    .line 66
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 67
    .line 68
    .line 69
    move-result v7

    .line 70
    mul-float v9, v9, v7

    .line 71
    .line 72
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    .line 73
    .line 74
    .line 75
    move-result v7

    .line 76
    goto :goto_0

    .line 77
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 78
    .line 79
    .line 80
    move-result v7

    .line 81
    int-to-float v7, v7

    .line 82
    mul-float v7, v7, v4

    .line 83
    .line 84
    mul-float v7, v7, v6

    .line 85
    .line 86
    div-float/2addr v7, v5

    .line 87
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 88
    .line 89
    .line 90
    move-result v7

    .line 91
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 92
    .line 93
    .line 94
    move-result v9

    .line 95
    int-to-float v9, v9

    .line 96
    mul-float v9, v9, v3

    .line 97
    .line 98
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    .line 99
    .line 100
    .line 101
    move-result v3

    .line 102
    move v12, v7

    .line 103
    move v7, v3

    .line 104
    move v3, v12

    .line 105
    :goto_0
    array-length v9, p0

    .line 106
    const/4 v11, 0x2

    .line 107
    if-lt v9, v11, :cond_1

    .line 108
    .line 109
    aget-object v9, p0, v8

    .line 110
    .line 111
    if-eqz v9, :cond_1

    .line 112
    .line 113
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 114
    .line 115
    .line 116
    move-result v0

    .line 117
    int-to-float v0, v0

    .line 118
    aget-object v1, p0, v8

    .line 119
    .line 120
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 121
    .line 122
    .line 123
    move-result v1

    .line 124
    sub-float/2addr v10, v1

    .line 125
    mul-float v0, v0, v10

    .line 126
    .line 127
    mul-float v0, v0, v6

    .line 128
    .line 129
    div-float/2addr v0, v5

    .line 130
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 131
    .line 132
    .line 133
    move-result v0

    .line 134
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 135
    .line 136
    .line 137
    move-result v1

    .line 138
    int-to-float v1, v1

    .line 139
    aget-object v5, p0, v8

    .line 140
    .line 141
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 142
    .line 143
    .line 144
    move-result v5

    .line 145
    mul-float v1, v1, v5

    .line 146
    .line 147
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 148
    .line 149
    .line 150
    move-result v1

    .line 151
    goto :goto_1

    .line 152
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 153
    .line 154
    .line 155
    move-result v8

    .line 156
    int-to-float v8, v8

    .line 157
    mul-float v8, v8, v1

    .line 158
    .line 159
    mul-float v8, v8, v6

    .line 160
    .line 161
    div-float/2addr v8, v5

    .line 162
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    .line 163
    .line 164
    .line 165
    move-result v1

    .line 166
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 167
    .line 168
    .line 169
    move-result v5

    .line 170
    int-to-float v5, v5

    .line 171
    mul-float v5, v5, v0

    .line 172
    .line 173
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 174
    .line 175
    .line 176
    move-result v0

    .line 177
    move v12, v1

    .line 178
    move v1, v0

    .line 179
    move v0, v12

    .line 180
    :goto_1
    array-length v5, p0

    .line 181
    const/4 v6, 0x3

    .line 182
    if-lt v5, v6, :cond_2

    .line 183
    .line 184
    aget-object v5, p0, v11

    .line 185
    .line 186
    if-eqz v5, :cond_2

    .line 187
    .line 188
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 189
    .line 190
    .line 191
    move-result v4

    .line 192
    int-to-float v4, v4

    .line 193
    aget-object v5, p0, v11

    .line 194
    .line 195
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 196
    .line 197
    .line 198
    move-result v5

    .line 199
    mul-float v4, v4, v5

    .line 200
    .line 201
    mul-float v4, v4, v2

    .line 202
    .line 203
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 204
    .line 205
    .line 206
    move-result v2

    .line 207
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 208
    .line 209
    .line 210
    move-result v4

    .line 211
    int-to-float v4, v4

    .line 212
    aget-object p0, p0, v11

    .line 213
    .line 214
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 215
    .line 216
    .line 217
    move-result p0

    .line 218
    mul-float v4, v4, p0

    .line 219
    .line 220
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 221
    .line 222
    .line 223
    move-result p0

    .line 224
    goto :goto_2

    .line 225
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 226
    .line 227
    .line 228
    move-result p0

    .line 229
    int-to-float p0, p0

    .line 230
    mul-float p0, p0, v4

    .line 231
    .line 232
    mul-float p0, p0, v2

    .line 233
    .line 234
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 235
    .line 236
    .line 237
    move-result v2

    .line 238
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 239
    .line 240
    .line 241
    move-result p0

    .line 242
    int-to-float p0, p0

    .line 243
    mul-float p0, p0, v4

    .line 244
    .line 245
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 246
    .line 247
    .line 248
    move-result p0

    .line 249
    goto :goto_2

    .line 250
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 251
    .line 252
    .line 253
    move-result p0

    .line 254
    int-to-float p0, p0

    .line 255
    mul-float p0, p0, v4

    .line 256
    .line 257
    mul-float p0, p0, v6

    .line 258
    .line 259
    div-float/2addr p0, v5

    .line 260
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 261
    .line 262
    .line 263
    move-result p0

    .line 264
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 265
    .line 266
    .line 267
    move-result v7

    .line 268
    int-to-float v7, v7

    .line 269
    mul-float v7, v7, v1

    .line 270
    .line 271
    mul-float v7, v7, v6

    .line 272
    .line 273
    div-float/2addr v7, v5

    .line 274
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 275
    .line 276
    .line 277
    move-result v1

    .line 278
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 279
    .line 280
    .line 281
    move-result v5

    .line 282
    int-to-float v5, v5

    .line 283
    mul-float v5, v5, v3

    .line 284
    .line 285
    mul-float v5, v5, v2

    .line 286
    .line 287
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 288
    .line 289
    .line 290
    move-result v2

    .line 291
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 292
    .line 293
    .line 294
    move-result v5

    .line 295
    int-to-float v5, v5

    .line 296
    mul-float v5, v5, v3

    .line 297
    .line 298
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 299
    .line 300
    .line 301
    move-result v7

    .line 302
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 303
    .line 304
    .line 305
    move-result v3

    .line 306
    int-to-float v3, v3

    .line 307
    mul-float v3, v3, v0

    .line 308
    .line 309
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 310
    .line 311
    .line 312
    move-result v0

    .line 313
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 314
    .line 315
    .line 316
    move-result v3

    .line 317
    int-to-float v3, v3

    .line 318
    mul-float v3, v3, v4

    .line 319
    .line 320
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 321
    .line 322
    .line 323
    move-result v3

    .line 324
    move v12, v3

    .line 325
    move v3, p0

    .line 326
    move p0, v12

    .line 327
    move v13, v1

    .line 328
    move v1, v0

    .line 329
    move v0, v13

    .line 330
    :goto_2
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 331
    .line 332
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 333
    .line 334
    add-int/2addr v5, v2

    .line 335
    int-to-float v5, v5

    .line 336
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 337
    .line 338
    sub-int/2addr v6, v3

    .line 339
    add-int/2addr v6, v0

    .line 340
    int-to-float v6, v6

    .line 341
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 342
    .line 343
    .line 344
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 345
    .line 346
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 347
    .line 348
    add-int/2addr v5, v2

    .line 349
    int-to-float v5, v5

    .line 350
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 351
    .line 352
    int-to-float v6, v6

    .line 353
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 354
    .line 355
    .line 356
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 357
    .line 358
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 359
    .line 360
    int-to-float v5, v5

    .line 361
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 362
    .line 363
    sub-int/2addr v6, v3

    .line 364
    int-to-float v6, v6

    .line 365
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 366
    .line 367
    .line 368
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 369
    .line 370
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 371
    .line 372
    add-int/2addr v5, v2

    .line 373
    int-to-float v5, v5

    .line 374
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 375
    .line 376
    mul-int/lit8 v8, v3, 0x2

    .line 377
    .line 378
    sub-int/2addr v6, v8

    .line 379
    int-to-float v6, v6

    .line 380
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 381
    .line 382
    .line 383
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 384
    .line 385
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 386
    .line 387
    add-int/2addr v5, v2

    .line 388
    int-to-float v5, v5

    .line 389
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 390
    .line 391
    sub-int/2addr v6, v3

    .line 392
    sub-int/2addr v6, v0

    .line 393
    int-to-float v6, v6

    .line 394
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 395
    .line 396
    .line 397
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 398
    .line 399
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 400
    .line 401
    add-int/2addr v5, v1

    .line 402
    int-to-float v5, v5

    .line 403
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 404
    .line 405
    sub-int/2addr v6, v3

    .line 406
    sub-int/2addr v6, v0

    .line 407
    int-to-float v6, v6

    .line 408
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 409
    .line 410
    .line 411
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 412
    .line 413
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 414
    .line 415
    add-int/2addr v5, v1

    .line 416
    int-to-float v5, v5

    .line 417
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 418
    .line 419
    add-int/2addr v6, p0

    .line 420
    int-to-float v6, v6

    .line 421
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 422
    .line 423
    .line 424
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 425
    .line 426
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 427
    .line 428
    add-int/2addr v5, v7

    .line 429
    int-to-float v5, v5

    .line 430
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 431
    .line 432
    add-int/2addr v6, p0

    .line 433
    int-to-float v6, v6

    .line 434
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 435
    .line 436
    .line 437
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 438
    .line 439
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 440
    .line 441
    .line 442
    move-result v5

    .line 443
    int-to-float v5, v5

    .line 444
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 445
    .line 446
    int-to-float v6, v6

    .line 447
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 448
    .line 449
    .line 450
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 451
    .line 452
    iget v5, p1, Landroid/graphics/Rect;->right:I

    .line 453
    .line 454
    sub-int/2addr v5, v7

    .line 455
    int-to-float v5, v5

    .line 456
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 457
    .line 458
    add-int/2addr v6, p0

    .line 459
    int-to-float v6, v6

    .line 460
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 461
    .line 462
    .line 463
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 464
    .line 465
    iget v5, p1, Landroid/graphics/Rect;->right:I

    .line 466
    .line 467
    sub-int/2addr v5, v1

    .line 468
    int-to-float v5, v5

    .line 469
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 470
    .line 471
    add-int/2addr v6, p0

    .line 472
    int-to-float p0, v6

    .line 473
    invoke-virtual {v4, v5, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 474
    .line 475
    .line 476
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 477
    .line 478
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 479
    .line 480
    sub-int/2addr v4, v1

    .line 481
    int-to-float v1, v4

    .line 482
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 483
    .line 484
    sub-int/2addr v4, v3

    .line 485
    sub-int/2addr v4, v0

    .line 486
    int-to-float v4, v4

    .line 487
    invoke-virtual {p0, v1, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 488
    .line 489
    .line 490
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 491
    .line 492
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 493
    .line 494
    sub-int/2addr v1, v2

    .line 495
    int-to-float v1, v1

    .line 496
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 497
    .line 498
    sub-int/2addr v4, v3

    .line 499
    sub-int/2addr v4, v0

    .line 500
    int-to-float v4, v4

    .line 501
    invoke-virtual {p0, v1, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 502
    .line 503
    .line 504
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 505
    .line 506
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 507
    .line 508
    sub-int/2addr v1, v2

    .line 509
    int-to-float v1, v1

    .line 510
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 511
    .line 512
    sub-int/2addr v4, v8

    .line 513
    int-to-float v4, v4

    .line 514
    invoke-virtual {p0, v1, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 515
    .line 516
    .line 517
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 518
    .line 519
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 520
    .line 521
    int-to-float v1, v1

    .line 522
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 523
    .line 524
    sub-int/2addr v4, v3

    .line 525
    int-to-float v4, v4

    .line 526
    invoke-virtual {p0, v1, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 527
    .line 528
    .line 529
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 530
    .line 531
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 532
    .line 533
    sub-int/2addr v1, v2

    .line 534
    int-to-float v1, v1

    .line 535
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 536
    .line 537
    int-to-float v4, v4

    .line 538
    invoke-virtual {p0, v1, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 539
    .line 540
    .line 541
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 542
    .line 543
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 544
    .line 545
    sub-int/2addr v1, v2

    .line 546
    int-to-float v1, v1

    .line 547
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 548
    .line 549
    sub-int/2addr p1, v3

    .line 550
    add-int/2addr p1, v0

    .line 551
    int-to-float p1, p1

    .line 552
    invoke-virtual {p0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 553
    .line 554
    .line 555
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 556
    .line 557
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 558
    .line 559
    .line 560
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 561
    .line 562
    return-object p0
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getLeftUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 13

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const v0, 0x3e8f5c29    # 0.28f

    .line 6
    .line 7
    .line 8
    const v1, 0x3f5c28f6    # 0.86f

    .line 9
    .line 10
    .line 11
    const v2, 0x3edc28f6    # 0.43f

    .line 12
    .line 13
    .line 14
    if-eqz p0, :cond_3

    .line 15
    .line 16
    array-length v3, p0

    .line 17
    const/4 v4, 0x1

    .line 18
    if-lt v3, v4, :cond_3

    .line 19
    .line 20
    const/4 v3, 0x0

    .line 21
    aget-object v5, p0, v3

    .line 22
    .line 23
    if-eqz v5, :cond_0

    .line 24
    .line 25
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    int-to-float v2, v2

    .line 30
    aget-object v5, p0, v3

    .line 31
    .line 32
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 33
    .line 34
    .line 35
    move-result v5

    .line 36
    mul-float v2, v2, v5

    .line 37
    .line 38
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 39
    .line 40
    .line 41
    move-result v5

    .line 42
    int-to-float v5, v5

    .line 43
    aget-object v3, p0, v3

    .line 44
    .line 45
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 46
    .line 47
    .line 48
    move-result v3

    .line 49
    mul-float v5, v5, v3

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    int-to-float v3, v3

    .line 57
    mul-float v3, v3, v2

    .line 58
    .line 59
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 60
    .line 61
    .line 62
    move-result v3

    .line 63
    int-to-float v3, v3

    .line 64
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 65
    .line 66
    .line 67
    move-result v5

    .line 68
    int-to-float v5, v5

    .line 69
    mul-float v5, v5, v2

    .line 70
    .line 71
    move v2, v3

    .line 72
    :goto_0
    array-length v3, p0

    .line 73
    const/4 v6, 0x2

    .line 74
    if-lt v3, v6, :cond_1

    .line 75
    .line 76
    aget-object v3, p0, v4

    .line 77
    .line 78
    if-eqz v3, :cond_1

    .line 79
    .line 80
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 81
    .line 82
    .line 83
    move-result v1

    .line 84
    int-to-float v1, v1

    .line 85
    aget-object v3, p0, v4

    .line 86
    .line 87
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 88
    .line 89
    .line 90
    move-result v3

    .line 91
    mul-float v1, v1, v3

    .line 92
    .line 93
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 94
    .line 95
    .line 96
    move-result v3

    .line 97
    int-to-float v3, v3

    .line 98
    aget-object v4, p0, v4

    .line 99
    .line 100
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 101
    .line 102
    .line 103
    move-result v4

    .line 104
    mul-float v3, v3, v4

    .line 105
    .line 106
    goto :goto_1

    .line 107
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 108
    .line 109
    .line 110
    move-result v3

    .line 111
    int-to-float v3, v3

    .line 112
    mul-float v3, v3, v1

    .line 113
    .line 114
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 115
    .line 116
    .line 117
    move-result v3

    .line 118
    int-to-float v3, v3

    .line 119
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 120
    .line 121
    .line 122
    move-result v4

    .line 123
    int-to-float v4, v4

    .line 124
    mul-float v1, v1, v4

    .line 125
    .line 126
    move v11, v3

    .line 127
    move v3, v1

    .line 128
    move v1, v11

    .line 129
    :goto_1
    array-length v4, p0

    .line 130
    const/4 v7, 0x3

    .line 131
    if-lt v4, v7, :cond_2

    .line 132
    .line 133
    aget-object v4, p0, v6

    .line 134
    .line 135
    if-eqz v4, :cond_2

    .line 136
    .line 137
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 138
    .line 139
    .line 140
    move-result v0

    .line 141
    int-to-float v0, v0

    .line 142
    aget-object v4, p0, v6

    .line 143
    .line 144
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 145
    .line 146
    .line 147
    move-result v4

    .line 148
    mul-float v0, v0, v4

    .line 149
    .line 150
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 151
    .line 152
    .line 153
    move-result v4

    .line 154
    int-to-float v4, v4

    .line 155
    aget-object p0, p0, v6

    .line 156
    .line 157
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 158
    .line 159
    .line 160
    move-result p0

    .line 161
    mul-float v4, v4, p0

    .line 162
    .line 163
    goto :goto_2

    .line 164
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 165
    .line 166
    .line 167
    move-result p0

    .line 168
    int-to-float p0, p0

    .line 169
    mul-float p0, p0, v0

    .line 170
    .line 171
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 172
    .line 173
    .line 174
    move-result p0

    .line 175
    int-to-float p0, p0

    .line 176
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 177
    .line 178
    .line 179
    move-result v4

    .line 180
    int-to-float v4, v4

    .line 181
    mul-float v4, v4, v0

    .line 182
    .line 183
    move v0, p0

    .line 184
    goto :goto_2

    .line 185
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 186
    .line 187
    .line 188
    move-result p0

    .line 189
    int-to-float p0, p0

    .line 190
    mul-float p0, p0, v2

    .line 191
    .line 192
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 193
    .line 194
    .line 195
    move-result p0

    .line 196
    int-to-float p0, p0

    .line 197
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 198
    .line 199
    .line 200
    move-result v3

    .line 201
    int-to-float v3, v3

    .line 202
    mul-float v3, v3, v1

    .line 203
    .line 204
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 205
    .line 206
    .line 207
    move-result v3

    .line 208
    int-to-float v3, v3

    .line 209
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 210
    .line 211
    .line 212
    move-result v4

    .line 213
    int-to-float v4, v4

    .line 214
    mul-float v4, v4, v0

    .line 215
    .line 216
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 217
    .line 218
    .line 219
    move-result v4

    .line 220
    int-to-float v4, v4

    .line 221
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 222
    .line 223
    .line 224
    move-result v5

    .line 225
    int-to-float v5, v5

    .line 226
    mul-float v5, v5, v2

    .line 227
    .line 228
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 229
    .line 230
    .line 231
    move-result v2

    .line 232
    int-to-float v2, v2

    .line 233
    mul-float v1, v1, v2

    .line 234
    .line 235
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 236
    .line 237
    .line 238
    move-result v2

    .line 239
    int-to-float v2, v2

    .line 240
    mul-float v0, v0, v2

    .line 241
    .line 242
    move v2, p0

    .line 243
    move v11, v4

    .line 244
    move v4, v0

    .line 245
    move v0, v11

    .line 246
    move v12, v3

    .line 247
    move v3, v1

    .line 248
    move v1, v12

    .line 249
    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 250
    .line 251
    .line 252
    move-result p0

    .line 253
    int-to-float p0, p0

    .line 254
    sub-float/2addr p0, v5

    .line 255
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 256
    .line 257
    .line 258
    move-result v6

    .line 259
    int-to-float v6, v6

    .line 260
    sub-float/2addr v6, v3

    .line 261
    const/high16 v7, 0x40000000    # 2.0f

    .line 262
    .line 263
    mul-float v6, v6, v7

    .line 264
    .line 265
    sub-float v6, p0, v6

    .line 266
    .line 267
    sget-object v8, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 268
    .line 269
    iget v9, p1, Landroid/graphics/Rect;->left:I

    .line 270
    .line 271
    int-to-float v9, v9

    .line 272
    add-float/2addr v9, v4

    .line 273
    iget v10, p1, Landroid/graphics/Rect;->top:I

    .line 274
    .line 275
    int-to-float v10, v10

    .line 276
    add-float/2addr v10, v3

    .line 277
    invoke-virtual {v8, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 278
    .line 279
    .line 280
    sget-object v8, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 281
    .line 282
    iget v9, p1, Landroid/graphics/Rect;->left:I

    .line 283
    .line 284
    int-to-float v9, v9

    .line 285
    add-float/2addr v9, v4

    .line 286
    iget v10, p1, Landroid/graphics/Rect;->bottom:I

    .line 287
    .line 288
    int-to-float v10, v10

    .line 289
    invoke-virtual {v8, v9, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 290
    .line 291
    .line 292
    sget-object v8, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 293
    .line 294
    iget v9, p1, Landroid/graphics/Rect;->left:I

    .line 295
    .line 296
    int-to-float v9, v9

    .line 297
    iget v10, p1, Landroid/graphics/Rect;->bottom:I

    .line 298
    .line 299
    int-to-float v10, v10

    .line 300
    div-float/2addr p0, v7

    .line 301
    sub-float/2addr v10, p0

    .line 302
    invoke-virtual {v8, v9, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 303
    .line 304
    .line 305
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 306
    .line 307
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 308
    .line 309
    int-to-float v8, v8

    .line 310
    add-float/2addr v8, v4

    .line 311
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 312
    .line 313
    int-to-float v9, v9

    .line 314
    add-float/2addr v9, v5

    .line 315
    invoke-virtual {p0, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 316
    .line 317
    .line 318
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 319
    .line 320
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 321
    .line 322
    int-to-float v5, v5

    .line 323
    add-float/2addr v5, v4

    .line 324
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 325
    .line 326
    int-to-float v4, v4

    .line 327
    add-float/2addr v4, v3

    .line 328
    sub-float/2addr v4, v6

    .line 329
    invoke-virtual {p0, v5, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 330
    .line 331
    .line 332
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 333
    .line 334
    .line 335
    move-result p0

    .line 336
    int-to-float p0, p0

    .line 337
    sub-float/2addr p0, v2

    .line 338
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 339
    .line 340
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 341
    .line 342
    int-to-float v5, v5

    .line 343
    add-float/2addr v5, v1

    .line 344
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 345
    .line 346
    .line 347
    move-result v8

    .line 348
    int-to-float v8, v8

    .line 349
    sub-float/2addr v8, v1

    .line 350
    mul-float v8, v8, v7

    .line 351
    .line 352
    sub-float v8, p0, v8

    .line 353
    .line 354
    sub-float/2addr v5, v8

    .line 355
    iget v8, p1, Landroid/graphics/Rect;->top:I

    .line 356
    .line 357
    int-to-float v8, v8

    .line 358
    add-float/2addr v8, v3

    .line 359
    sub-float/2addr v8, v6

    .line 360
    invoke-virtual {v4, v5, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 361
    .line 362
    .line 363
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 364
    .line 365
    .line 366
    move-result v4

    .line 367
    int-to-float v4, v4

    .line 368
    sub-float/2addr v4, v1

    .line 369
    mul-float v4, v4, v7

    .line 370
    .line 371
    sub-float v4, p0, v4

    .line 372
    .line 373
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 374
    .line 375
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 376
    .line 377
    int-to-float v6, v6

    .line 378
    add-float/2addr v6, v1

    .line 379
    sub-float/2addr v6, v4

    .line 380
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 381
    .line 382
    int-to-float v4, v4

    .line 383
    add-float/2addr v4, v0

    .line 384
    invoke-virtual {v5, v6, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 385
    .line 386
    .line 387
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 388
    .line 389
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 390
    .line 391
    int-to-float v5, v5

    .line 392
    add-float/2addr v5, v2

    .line 393
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 394
    .line 395
    int-to-float v2, v2

    .line 396
    add-float/2addr v2, v0

    .line 397
    invoke-virtual {v4, v5, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 398
    .line 399
    .line 400
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 401
    .line 402
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 403
    .line 404
    int-to-float v4, v4

    .line 405
    div-float/2addr p0, v7

    .line 406
    sub-float/2addr v4, p0

    .line 407
    iget p0, p1, Landroid/graphics/Rect;->top:I

    .line 408
    .line 409
    int-to-float p0, p0

    .line 410
    invoke-virtual {v2, v4, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 411
    .line 412
    .line 413
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 414
    .line 415
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 416
    .line 417
    int-to-float v2, v2

    .line 418
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 419
    .line 420
    int-to-float v4, v4

    .line 421
    add-float/2addr v4, v0

    .line 422
    invoke-virtual {p0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 423
    .line 424
    .line 425
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 426
    .line 427
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 428
    .line 429
    int-to-float v2, v2

    .line 430
    add-float/2addr v2, v1

    .line 431
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 432
    .line 433
    int-to-float v4, v4

    .line 434
    add-float/2addr v4, v0

    .line 435
    invoke-virtual {p0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 436
    .line 437
    .line 438
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 439
    .line 440
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 441
    .line 442
    int-to-float v0, v0

    .line 443
    add-float/2addr v0, v1

    .line 444
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 445
    .line 446
    int-to-float p1, p1

    .line 447
    add-float/2addr p1, v3

    .line 448
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 449
    .line 450
    .line 451
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 452
    .line 453
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 454
    .line 455
    .line 456
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 457
    .line 458
    return-object p0
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getNotchedRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/high16 v0, 0x3e800000    # 0.25f

    .line 6
    .line 7
    const/high16 v1, 0x3f400000    # 0.75f

    .line 8
    .line 9
    if-eqz p0, :cond_2

    .line 10
    .line 11
    array-length v2, p0

    .line 12
    const/4 v3, 0x1

    .line 13
    if-lt v2, v3, :cond_2

    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    aget-object v4, p0, v2

    .line 17
    .line 18
    if-eqz v4, :cond_0

    .line 19
    .line 20
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    int-to-float v1, v1

    .line 25
    aget-object v2, p0, v2

    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    mul-float v1, v1, v2

    .line 32
    .line 33
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    goto :goto_0

    .line 38
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    int-to-float v2, v2

    .line 43
    mul-float v2, v2, v1

    .line 44
    .line 45
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    :goto_0
    array-length v2, p0

    .line 50
    const/4 v4, 0x2

    .line 51
    if-lt v2, v4, :cond_1

    .line 52
    .line 53
    aget-object v2, p0, v3

    .line 54
    .line 55
    if-eqz v2, :cond_1

    .line 56
    .line 57
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    int-to-float v0, v0

    .line 62
    aget-object p0, p0, v3

    .line 63
    .line 64
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 65
    .line 66
    .line 67
    move-result p0

    .line 68
    mul-float v0, v0, p0

    .line 69
    .line 70
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 71
    .line 72
    .line 73
    move-result p0

    .line 74
    goto :goto_1

    .line 75
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 76
    .line 77
    .line 78
    move-result p0

    .line 79
    int-to-float p0, p0

    .line 80
    mul-float p0, p0, v0

    .line 81
    .line 82
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 83
    .line 84
    .line 85
    move-result p0

    .line 86
    goto :goto_1

    .line 87
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 88
    .line 89
    .line 90
    move-result p0

    .line 91
    int-to-float p0, p0

    .line 92
    mul-float p0, p0, v1

    .line 93
    .line 94
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 95
    .line 96
    .line 97
    move-result v1

    .line 98
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 99
    .line 100
    .line 101
    move-result p0

    .line 102
    int-to-float p0, p0

    .line 103
    mul-float p0, p0, v0

    .line 104
    .line 105
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 106
    .line 107
    .line 108
    move-result p0

    .line 109
    :goto_1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 110
    .line 111
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 112
    .line 113
    int-to-float v2, v2

    .line 114
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 115
    .line 116
    sub-int/2addr v3, p0

    .line 117
    int-to-float v3, v3

    .line 118
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 119
    .line 120
    .line 121
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 122
    .line 123
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 124
    .line 125
    int-to-float v2, v2

    .line 126
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 127
    .line 128
    .line 129
    move-result v3

    .line 130
    mul-int/lit8 v4, p0, 0x2

    .line 131
    .line 132
    sub-int/2addr v3, v4

    .line 133
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 134
    .line 135
    .line 136
    move-result v4

    .line 137
    sub-int/2addr v4, v1

    .line 138
    mul-int v3, v3, v4

    .line 139
    .line 140
    int-to-float v3, v3

    .line 141
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 142
    .line 143
    .line 144
    move-result v4

    .line 145
    int-to-float v4, v4

    .line 146
    div-float/2addr v3, v4

    .line 147
    add-float/2addr v2, v3

    .line 148
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 149
    .line 150
    .line 151
    move-result v3

    .line 152
    int-to-float v3, v3

    .line 153
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 154
    .line 155
    .line 156
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 157
    .line 158
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 159
    .line 160
    int-to-float v2, v2

    .line 161
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 162
    .line 163
    add-int/2addr v3, p0

    .line 164
    int-to-float v3, v3

    .line 165
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 166
    .line 167
    .line 168
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 169
    .line 170
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 171
    .line 172
    add-int/2addr v2, v1

    .line 173
    int-to-float v2, v2

    .line 174
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 175
    .line 176
    add-int/2addr v3, p0

    .line 177
    int-to-float v3, v3

    .line 178
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 179
    .line 180
    .line 181
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 182
    .line 183
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 184
    .line 185
    add-int/2addr v2, v1

    .line 186
    int-to-float v2, v2

    .line 187
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 188
    .line 189
    int-to-float v3, v3

    .line 190
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 191
    .line 192
    .line 193
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 194
    .line 195
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 196
    .line 197
    int-to-float v2, v2

    .line 198
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 199
    .line 200
    .line 201
    move-result v3

    .line 202
    int-to-float v3, v3

    .line 203
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 204
    .line 205
    .line 206
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 207
    .line 208
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 209
    .line 210
    add-int/2addr v2, v1

    .line 211
    int-to-float v2, v2

    .line 212
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 213
    .line 214
    int-to-float v3, v3

    .line 215
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 216
    .line 217
    .line 218
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 219
    .line 220
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 221
    .line 222
    add-int/2addr v2, v1

    .line 223
    int-to-float v1, v2

    .line 224
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 225
    .line 226
    sub-int/2addr p1, p0

    .line 227
    int-to-float p0, p1

    .line 228
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 229
    .line 230
    .line 231
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 232
    .line 233
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 234
    .line 235
    .line 236
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 237
    .line 238
    return-object p0
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getQuadArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 11

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const v0, 0x3ee66666    # 0.45f

    .line 6
    .line 7
    .line 8
    const/high16 v1, 0x3e000000    # 0.125f

    .line 9
    .line 10
    const/high16 v2, 0x3ec00000    # 0.375f

    .line 11
    .line 12
    const/high16 v3, 0x3e800000    # 0.25f

    .line 13
    .line 14
    if-eqz p0, :cond_4

    .line 15
    .line 16
    array-length v4, p0

    .line 17
    const/4 v5, 0x1

    .line 18
    if-lt v4, v5, :cond_4

    .line 19
    .line 20
    const/4 v4, 0x0

    .line 21
    aget-object v6, p0, v4

    .line 22
    .line 23
    if-eqz v6, :cond_0

    .line 24
    .line 25
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    int-to-float v3, v3

    .line 30
    aget-object v6, p0, v4

    .line 31
    .line 32
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 33
    .line 34
    .line 35
    move-result v6

    .line 36
    mul-float v3, v3, v6

    .line 37
    .line 38
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 43
    .line 44
    .line 45
    move-result v6

    .line 46
    int-to-float v6, v6

    .line 47
    aget-object v4, p0, v4

    .line 48
    .line 49
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 50
    .line 51
    .line 52
    move-result v4

    .line 53
    mul-float v6, v6, v4

    .line 54
    .line 55
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 56
    .line 57
    .line 58
    move-result v4

    .line 59
    goto :goto_0

    .line 60
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 61
    .line 62
    .line 63
    move-result v4

    .line 64
    int-to-float v4, v4

    .line 65
    mul-float v4, v4, v3

    .line 66
    .line 67
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 68
    .line 69
    .line 70
    move-result v4

    .line 71
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 72
    .line 73
    .line 74
    move-result v6

    .line 75
    int-to-float v6, v6

    .line 76
    mul-float v6, v6, v3

    .line 77
    .line 78
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 79
    .line 80
    .line 81
    move-result v3

    .line 82
    move v10, v4

    .line 83
    move v4, v3

    .line 84
    move v3, v10

    .line 85
    :goto_0
    array-length v6, p0

    .line 86
    const/4 v7, 0x2

    .line 87
    if-lt v6, v7, :cond_1

    .line 88
    .line 89
    aget-object v6, p0, v5

    .line 90
    .line 91
    if-eqz v6, :cond_1

    .line 92
    .line 93
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 94
    .line 95
    .line 96
    move-result v2

    .line 97
    int-to-float v2, v2

    .line 98
    aget-object v6, p0, v5

    .line 99
    .line 100
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 101
    .line 102
    .line 103
    move-result v6

    .line 104
    mul-float v2, v2, v6

    .line 105
    .line 106
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 107
    .line 108
    .line 109
    move-result v2

    .line 110
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 111
    .line 112
    .line 113
    move-result v6

    .line 114
    int-to-float v6, v6

    .line 115
    aget-object v5, p0, v5

    .line 116
    .line 117
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 118
    .line 119
    .line 120
    move-result v5

    .line 121
    mul-float v6, v6, v5

    .line 122
    .line 123
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 124
    .line 125
    .line 126
    move-result v5

    .line 127
    goto :goto_1

    .line 128
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 129
    .line 130
    .line 131
    move-result v5

    .line 132
    int-to-float v5, v5

    .line 133
    mul-float v5, v5, v2

    .line 134
    .line 135
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 136
    .line 137
    .line 138
    move-result v5

    .line 139
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 140
    .line 141
    .line 142
    move-result v6

    .line 143
    int-to-float v6, v6

    .line 144
    mul-float v6, v6, v2

    .line 145
    .line 146
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 147
    .line 148
    .line 149
    move-result v2

    .line 150
    move v10, v5

    .line 151
    move v5, v2

    .line 152
    move v2, v10

    .line 153
    :goto_1
    array-length v6, p0

    .line 154
    const/4 v8, 0x3

    .line 155
    if-lt v6, v8, :cond_2

    .line 156
    .line 157
    aget-object v6, p0, v7

    .line 158
    .line 159
    if-eqz v6, :cond_2

    .line 160
    .line 161
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 162
    .line 163
    .line 164
    move-result v1

    .line 165
    int-to-float v1, v1

    .line 166
    aget-object v6, p0, v7

    .line 167
    .line 168
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 169
    .line 170
    .line 171
    move-result v6

    .line 172
    mul-float v1, v1, v6

    .line 173
    .line 174
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 175
    .line 176
    .line 177
    move-result v1

    .line 178
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 179
    .line 180
    .line 181
    move-result v6

    .line 182
    int-to-float v6, v6

    .line 183
    aget-object v7, p0, v7

    .line 184
    .line 185
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 186
    .line 187
    .line 188
    move-result v7

    .line 189
    mul-float v6, v6, v7

    .line 190
    .line 191
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 192
    .line 193
    .line 194
    move-result v6

    .line 195
    goto :goto_2

    .line 196
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 197
    .line 198
    .line 199
    move-result v6

    .line 200
    int-to-float v6, v6

    .line 201
    mul-float v6, v6, v1

    .line 202
    .line 203
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 204
    .line 205
    .line 206
    move-result v6

    .line 207
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 208
    .line 209
    .line 210
    move-result v7

    .line 211
    int-to-float v7, v7

    .line 212
    mul-float v7, v7, v1

    .line 213
    .line 214
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 215
    .line 216
    .line 217
    move-result v1

    .line 218
    move v10, v6

    .line 219
    move v6, v1

    .line 220
    move v1, v10

    .line 221
    :goto_2
    array-length v7, p0

    .line 222
    const/4 v9, 0x4

    .line 223
    if-lt v7, v9, :cond_3

    .line 224
    .line 225
    aget-object v7, p0, v8

    .line 226
    .line 227
    if-eqz v7, :cond_3

    .line 228
    .line 229
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 230
    .line 231
    .line 232
    move-result v0

    .line 233
    int-to-float v0, v0

    .line 234
    aget-object v7, p0, v8

    .line 235
    .line 236
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 237
    .line 238
    .line 239
    move-result v7

    .line 240
    mul-float v0, v0, v7

    .line 241
    .line 242
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 243
    .line 244
    .line 245
    move-result v0

    .line 246
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 247
    .line 248
    .line 249
    move-result v7

    .line 250
    int-to-float v7, v7

    .line 251
    aget-object p0, p0, v8

    .line 252
    .line 253
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 254
    .line 255
    .line 256
    move-result p0

    .line 257
    mul-float v7, v7, p0

    .line 258
    .line 259
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 260
    .line 261
    .line 262
    move-result p0

    .line 263
    goto/16 :goto_3

    .line 264
    .line 265
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 266
    .line 267
    .line 268
    move-result p0

    .line 269
    int-to-float p0, p0

    .line 270
    mul-float p0, p0, v0

    .line 271
    .line 272
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 273
    .line 274
    .line 275
    move-result p0

    .line 276
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 277
    .line 278
    .line 279
    move-result v7

    .line 280
    int-to-float v7, v7

    .line 281
    mul-float v7, v7, v0

    .line 282
    .line 283
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 284
    .line 285
    .line 286
    move-result v0

    .line 287
    move v10, v0

    .line 288
    move v0, p0

    .line 289
    move p0, v10

    .line 290
    goto :goto_3

    .line 291
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 292
    .line 293
    .line 294
    move-result p0

    .line 295
    int-to-float p0, p0

    .line 296
    mul-float p0, p0, v3

    .line 297
    .line 298
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 299
    .line 300
    .line 301
    move-result p0

    .line 302
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 303
    .line 304
    .line 305
    move-result v4

    .line 306
    int-to-float v4, v4

    .line 307
    mul-float v4, v4, v2

    .line 308
    .line 309
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 310
    .line 311
    .line 312
    move-result v4

    .line 313
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 314
    .line 315
    .line 316
    move-result v5

    .line 317
    int-to-float v5, v5

    .line 318
    mul-float v5, v5, v1

    .line 319
    .line 320
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 321
    .line 322
    .line 323
    move-result v5

    .line 324
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 325
    .line 326
    .line 327
    move-result v6

    .line 328
    int-to-float v6, v6

    .line 329
    mul-float v6, v6, v0

    .line 330
    .line 331
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 332
    .line 333
    .line 334
    move-result v6

    .line 335
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 336
    .line 337
    .line 338
    move-result v7

    .line 339
    int-to-float v7, v7

    .line 340
    mul-float v7, v7, v3

    .line 341
    .line 342
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 343
    .line 344
    .line 345
    move-result v3

    .line 346
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 347
    .line 348
    .line 349
    move-result v7

    .line 350
    int-to-float v7, v7

    .line 351
    mul-float v7, v7, v2

    .line 352
    .line 353
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 354
    .line 355
    .line 356
    move-result v2

    .line 357
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 358
    .line 359
    .line 360
    move-result v7

    .line 361
    int-to-float v7, v7

    .line 362
    mul-float v7, v7, v1

    .line 363
    .line 364
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 365
    .line 366
    .line 367
    move-result v1

    .line 368
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 369
    .line 370
    .line 371
    move-result v7

    .line 372
    int-to-float v7, v7

    .line 373
    mul-float v7, v7, v0

    .line 374
    .line 375
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 376
    .line 377
    .line 378
    move-result v0

    .line 379
    move v10, v3

    .line 380
    move v3, p0

    .line 381
    move p0, v0

    .line 382
    move v0, v6

    .line 383
    move v6, v1

    .line 384
    move v1, v5

    .line 385
    move v5, v2

    .line 386
    move v2, v4

    .line 387
    move v4, v10

    .line 388
    :goto_3
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 389
    .line 390
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 391
    .line 392
    add-int/2addr v8, v3

    .line 393
    int-to-float v8, v8

    .line 394
    iget v9, p1, Landroid/graphics/Rect;->bottom:I

    .line 395
    .line 396
    sub-int/2addr v9, v0

    .line 397
    int-to-float v9, v9

    .line 398
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 399
    .line 400
    .line 401
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 402
    .line 403
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 404
    .line 405
    add-int/2addr v8, v1

    .line 406
    int-to-float v8, v8

    .line 407
    iget v9, p1, Landroid/graphics/Rect;->bottom:I

    .line 408
    .line 409
    sub-int/2addr v9, v0

    .line 410
    int-to-float v9, v9

    .line 411
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 412
    .line 413
    .line 414
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 415
    .line 416
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 417
    .line 418
    add-int/2addr v8, v1

    .line 419
    int-to-float v8, v8

    .line 420
    iget v9, p1, Landroid/graphics/Rect;->bottom:I

    .line 421
    .line 422
    sub-int/2addr v9, v2

    .line 423
    int-to-float v9, v9

    .line 424
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 425
    .line 426
    .line 427
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 428
    .line 429
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 430
    .line 431
    int-to-float v8, v8

    .line 432
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 433
    .line 434
    .line 435
    move-result v9

    .line 436
    int-to-float v9, v9

    .line 437
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 438
    .line 439
    .line 440
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 441
    .line 442
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 443
    .line 444
    add-int/2addr v8, v1

    .line 445
    int-to-float v8, v8

    .line 446
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 447
    .line 448
    add-int/2addr v9, v2

    .line 449
    int-to-float v9, v9

    .line 450
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 451
    .line 452
    .line 453
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 454
    .line 455
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 456
    .line 457
    add-int/2addr v8, v1

    .line 458
    int-to-float v8, v8

    .line 459
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 460
    .line 461
    add-int/2addr v9, v0

    .line 462
    int-to-float v9, v9

    .line 463
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 464
    .line 465
    .line 466
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 467
    .line 468
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 469
    .line 470
    add-int/2addr v8, v3

    .line 471
    int-to-float v8, v8

    .line 472
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 473
    .line 474
    add-int/2addr v9, v0

    .line 475
    int-to-float v9, v9

    .line 476
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 477
    .line 478
    .line 479
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 480
    .line 481
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 482
    .line 483
    add-int/2addr v8, v3

    .line 484
    int-to-float v8, v8

    .line 485
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 486
    .line 487
    add-int/2addr v9, v4

    .line 488
    int-to-float v9, v9

    .line 489
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 490
    .line 491
    .line 492
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 493
    .line 494
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 495
    .line 496
    add-int/2addr v8, p0

    .line 497
    int-to-float v8, v8

    .line 498
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 499
    .line 500
    add-int/2addr v9, v4

    .line 501
    int-to-float v9, v9

    .line 502
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 503
    .line 504
    .line 505
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 506
    .line 507
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 508
    .line 509
    add-int/2addr v8, p0

    .line 510
    int-to-float v8, v8

    .line 511
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 512
    .line 513
    add-int/2addr v9, v6

    .line 514
    int-to-float v9, v9

    .line 515
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 516
    .line 517
    .line 518
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 519
    .line 520
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 521
    .line 522
    add-int/2addr v8, v5

    .line 523
    int-to-float v8, v8

    .line 524
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 525
    .line 526
    add-int/2addr v9, v6

    .line 527
    int-to-float v9, v9

    .line 528
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 529
    .line 530
    .line 531
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 532
    .line 533
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 534
    .line 535
    .line 536
    move-result v8

    .line 537
    int-to-float v8, v8

    .line 538
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 539
    .line 540
    int-to-float v9, v9

    .line 541
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 542
    .line 543
    .line 544
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 545
    .line 546
    iget v8, p1, Landroid/graphics/Rect;->right:I

    .line 547
    .line 548
    sub-int/2addr v8, v5

    .line 549
    int-to-float v8, v8

    .line 550
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 551
    .line 552
    add-int/2addr v9, v6

    .line 553
    int-to-float v9, v9

    .line 554
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 555
    .line 556
    .line 557
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 558
    .line 559
    iget v8, p1, Landroid/graphics/Rect;->right:I

    .line 560
    .line 561
    sub-int/2addr v8, p0

    .line 562
    int-to-float v8, v8

    .line 563
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 564
    .line 565
    add-int/2addr v9, v6

    .line 566
    int-to-float v9, v9

    .line 567
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 568
    .line 569
    .line 570
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 571
    .line 572
    iget v8, p1, Landroid/graphics/Rect;->right:I

    .line 573
    .line 574
    sub-int/2addr v8, p0

    .line 575
    int-to-float v8, v8

    .line 576
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 577
    .line 578
    add-int/2addr v9, v4

    .line 579
    int-to-float v9, v9

    .line 580
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 581
    .line 582
    .line 583
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 584
    .line 585
    iget v8, p1, Landroid/graphics/Rect;->right:I

    .line 586
    .line 587
    sub-int/2addr v8, v3

    .line 588
    int-to-float v8, v8

    .line 589
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 590
    .line 591
    add-int/2addr v9, v4

    .line 592
    int-to-float v9, v9

    .line 593
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 594
    .line 595
    .line 596
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 597
    .line 598
    iget v8, p1, Landroid/graphics/Rect;->right:I

    .line 599
    .line 600
    sub-int/2addr v8, v3

    .line 601
    int-to-float v8, v8

    .line 602
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 603
    .line 604
    add-int/2addr v9, v0

    .line 605
    int-to-float v9, v9

    .line 606
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 607
    .line 608
    .line 609
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 610
    .line 611
    iget v8, p1, Landroid/graphics/Rect;->right:I

    .line 612
    .line 613
    sub-int/2addr v8, v1

    .line 614
    int-to-float v8, v8

    .line 615
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 616
    .line 617
    add-int/2addr v9, v0

    .line 618
    int-to-float v9, v9

    .line 619
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 620
    .line 621
    .line 622
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 623
    .line 624
    iget v8, p1, Landroid/graphics/Rect;->right:I

    .line 625
    .line 626
    sub-int/2addr v8, v1

    .line 627
    int-to-float v8, v8

    .line 628
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 629
    .line 630
    add-int/2addr v9, v2

    .line 631
    int-to-float v9, v9

    .line 632
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 633
    .line 634
    .line 635
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 636
    .line 637
    iget v8, p1, Landroid/graphics/Rect;->right:I

    .line 638
    .line 639
    int-to-float v8, v8

    .line 640
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 641
    .line 642
    .line 643
    move-result v9

    .line 644
    int-to-float v9, v9

    .line 645
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 646
    .line 647
    .line 648
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 649
    .line 650
    iget v8, p1, Landroid/graphics/Rect;->right:I

    .line 651
    .line 652
    sub-int/2addr v8, v1

    .line 653
    int-to-float v8, v8

    .line 654
    iget v9, p1, Landroid/graphics/Rect;->bottom:I

    .line 655
    .line 656
    sub-int/2addr v9, v2

    .line 657
    int-to-float v2, v9

    .line 658
    invoke-virtual {v7, v8, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 659
    .line 660
    .line 661
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 662
    .line 663
    iget v7, p1, Landroid/graphics/Rect;->right:I

    .line 664
    .line 665
    sub-int/2addr v7, v1

    .line 666
    int-to-float v1, v7

    .line 667
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 668
    .line 669
    sub-int/2addr v7, v0

    .line 670
    int-to-float v7, v7

    .line 671
    invoke-virtual {v2, v1, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 672
    .line 673
    .line 674
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 675
    .line 676
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 677
    .line 678
    sub-int/2addr v2, v3

    .line 679
    int-to-float v2, v2

    .line 680
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 681
    .line 682
    sub-int/2addr v7, v0

    .line 683
    int-to-float v0, v7

    .line 684
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 685
    .line 686
    .line 687
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 688
    .line 689
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 690
    .line 691
    sub-int/2addr v1, v3

    .line 692
    int-to-float v1, v1

    .line 693
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 694
    .line 695
    sub-int/2addr v2, v4

    .line 696
    int-to-float v2, v2

    .line 697
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 698
    .line 699
    .line 700
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 701
    .line 702
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 703
    .line 704
    sub-int/2addr v1, p0

    .line 705
    int-to-float v1, v1

    .line 706
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 707
    .line 708
    sub-int/2addr v2, v4

    .line 709
    int-to-float v2, v2

    .line 710
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 711
    .line 712
    .line 713
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 714
    .line 715
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 716
    .line 717
    sub-int/2addr v1, p0

    .line 718
    int-to-float v1, v1

    .line 719
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 720
    .line 721
    sub-int/2addr v2, v6

    .line 722
    int-to-float v2, v2

    .line 723
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 724
    .line 725
    .line 726
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 727
    .line 728
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 729
    .line 730
    sub-int/2addr v1, v5

    .line 731
    int-to-float v1, v1

    .line 732
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 733
    .line 734
    sub-int/2addr v2, v6

    .line 735
    int-to-float v2, v2

    .line 736
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 737
    .line 738
    .line 739
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 740
    .line 741
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 742
    .line 743
    .line 744
    move-result v1

    .line 745
    int-to-float v1, v1

    .line 746
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 747
    .line 748
    int-to-float v2, v2

    .line 749
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 750
    .line 751
    .line 752
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 753
    .line 754
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 755
    .line 756
    add-int/2addr v1, v5

    .line 757
    int-to-float v1, v1

    .line 758
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 759
    .line 760
    sub-int/2addr v2, v6

    .line 761
    int-to-float v2, v2

    .line 762
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 763
    .line 764
    .line 765
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 766
    .line 767
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 768
    .line 769
    add-int/2addr v1, p0

    .line 770
    int-to-float v1, v1

    .line 771
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 772
    .line 773
    sub-int/2addr v2, v6

    .line 774
    int-to-float v2, v2

    .line 775
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 776
    .line 777
    .line 778
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 779
    .line 780
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 781
    .line 782
    add-int/2addr v1, p0

    .line 783
    int-to-float p0, v1

    .line 784
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 785
    .line 786
    sub-int/2addr v1, v4

    .line 787
    int-to-float v1, v1

    .line 788
    invoke-virtual {v0, p0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 789
    .line 790
    .line 791
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 792
    .line 793
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 794
    .line 795
    add-int/2addr v0, v3

    .line 796
    int-to-float v0, v0

    .line 797
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 798
    .line 799
    sub-int/2addr p1, v4

    .line 800
    int-to-float p1, p1

    .line 801
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 802
    .line 803
    .line 804
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 805
    .line 806
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 807
    .line 808
    .line 809
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 810
    .line 811
    return-object p0
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getQuadArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 9

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const v0, 0x3e4ccccd    # 0.2f

    .line 6
    .line 7
    .line 8
    const v1, 0x3ecccccd    # 0.4f

    .line 9
    .line 10
    .line 11
    const v2, 0x3e99999a    # 0.3f

    .line 12
    .line 13
    .line 14
    if-eqz p0, :cond_3

    .line 15
    .line 16
    array-length v3, p0

    .line 17
    const/4 v4, 0x1

    .line 18
    if-lt v3, v4, :cond_3

    .line 19
    .line 20
    const/4 v3, 0x0

    .line 21
    aget-object v5, p0, v3

    .line 22
    .line 23
    if-eqz v5, :cond_0

    .line 24
    .line 25
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    int-to-float v2, v2

    .line 30
    aget-object v5, p0, v3

    .line 31
    .line 32
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 33
    .line 34
    .line 35
    move-result v5

    .line 36
    mul-float v2, v2, v5

    .line 37
    .line 38
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 43
    .line 44
    .line 45
    move-result v5

    .line 46
    int-to-float v5, v5

    .line 47
    aget-object v3, p0, v3

    .line 48
    .line 49
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 50
    .line 51
    .line 52
    move-result v3

    .line 53
    mul-float v5, v5, v3

    .line 54
    .line 55
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 56
    .line 57
    .line 58
    move-result v3

    .line 59
    goto :goto_0

    .line 60
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    int-to-float v3, v3

    .line 65
    mul-float v3, v3, v2

    .line 66
    .line 67
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 68
    .line 69
    .line 70
    move-result v3

    .line 71
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 72
    .line 73
    .line 74
    move-result v5

    .line 75
    int-to-float v5, v5

    .line 76
    mul-float v5, v5, v2

    .line 77
    .line 78
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 79
    .line 80
    .line 81
    move-result v2

    .line 82
    move v8, v3

    .line 83
    move v3, v2

    .line 84
    move v2, v8

    .line 85
    :goto_0
    array-length v5, p0

    .line 86
    const/4 v6, 0x2

    .line 87
    if-lt v5, v6, :cond_1

    .line 88
    .line 89
    aget-object v5, p0, v4

    .line 90
    .line 91
    if-eqz v5, :cond_1

    .line 92
    .line 93
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 94
    .line 95
    .line 96
    move-result v1

    .line 97
    int-to-float v1, v1

    .line 98
    aget-object v5, p0, v4

    .line 99
    .line 100
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 101
    .line 102
    .line 103
    move-result v5

    .line 104
    mul-float v1, v1, v5

    .line 105
    .line 106
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 107
    .line 108
    .line 109
    move-result v1

    .line 110
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 111
    .line 112
    .line 113
    move-result v5

    .line 114
    int-to-float v5, v5

    .line 115
    aget-object v4, p0, v4

    .line 116
    .line 117
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 118
    .line 119
    .line 120
    move-result v4

    .line 121
    mul-float v5, v5, v4

    .line 122
    .line 123
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 124
    .line 125
    .line 126
    move-result v4

    .line 127
    goto :goto_1

    .line 128
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 129
    .line 130
    .line 131
    move-result v4

    .line 132
    int-to-float v4, v4

    .line 133
    mul-float v4, v4, v1

    .line 134
    .line 135
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 136
    .line 137
    .line 138
    move-result v4

    .line 139
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 140
    .line 141
    .line 142
    move-result v5

    .line 143
    int-to-float v5, v5

    .line 144
    mul-float v5, v5, v1

    .line 145
    .line 146
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 147
    .line 148
    .line 149
    move-result v1

    .line 150
    move v8, v4

    .line 151
    move v4, v1

    .line 152
    move v1, v8

    .line 153
    :goto_1
    array-length v5, p0

    .line 154
    const/4 v7, 0x3

    .line 155
    if-lt v5, v7, :cond_2

    .line 156
    .line 157
    aget-object v5, p0, v6

    .line 158
    .line 159
    if-eqz v5, :cond_2

    .line 160
    .line 161
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 162
    .line 163
    .line 164
    move-result v0

    .line 165
    int-to-float v0, v0

    .line 166
    aget-object v5, p0, v6

    .line 167
    .line 168
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 169
    .line 170
    .line 171
    move-result v5

    .line 172
    mul-float v0, v0, v5

    .line 173
    .line 174
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 175
    .line 176
    .line 177
    move-result v0

    .line 178
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 179
    .line 180
    .line 181
    move-result v5

    .line 182
    int-to-float v5, v5

    .line 183
    aget-object p0, p0, v6

    .line 184
    .line 185
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 186
    .line 187
    .line 188
    move-result p0

    .line 189
    mul-float v5, v5, p0

    .line 190
    .line 191
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 192
    .line 193
    .line 194
    move-result p0

    .line 195
    goto :goto_2

    .line 196
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 197
    .line 198
    .line 199
    move-result p0

    .line 200
    int-to-float p0, p0

    .line 201
    mul-float p0, p0, v0

    .line 202
    .line 203
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 204
    .line 205
    .line 206
    move-result p0

    .line 207
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 208
    .line 209
    .line 210
    move-result v5

    .line 211
    int-to-float v5, v5

    .line 212
    mul-float v5, v5, v0

    .line 213
    .line 214
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 215
    .line 216
    .line 217
    move-result v0

    .line 218
    move v8, v0

    .line 219
    move v0, p0

    .line 220
    move p0, v8

    .line 221
    goto :goto_2

    .line 222
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 223
    .line 224
    .line 225
    move-result p0

    .line 226
    int-to-float p0, p0

    .line 227
    mul-float p0, p0, v2

    .line 228
    .line 229
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 230
    .line 231
    .line 232
    move-result p0

    .line 233
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 234
    .line 235
    .line 236
    move-result v3

    .line 237
    int-to-float v3, v3

    .line 238
    mul-float v3, v3, v1

    .line 239
    .line 240
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 241
    .line 242
    .line 243
    move-result v3

    .line 244
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 245
    .line 246
    .line 247
    move-result v4

    .line 248
    int-to-float v4, v4

    .line 249
    mul-float v4, v4, v0

    .line 250
    .line 251
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 252
    .line 253
    .line 254
    move-result v4

    .line 255
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 256
    .line 257
    .line 258
    move-result v5

    .line 259
    int-to-float v5, v5

    .line 260
    mul-float v5, v5, v2

    .line 261
    .line 262
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 263
    .line 264
    .line 265
    move-result v2

    .line 266
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 267
    .line 268
    .line 269
    move-result v5

    .line 270
    int-to-float v5, v5

    .line 271
    mul-float v5, v5, v1

    .line 272
    .line 273
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 274
    .line 275
    .line 276
    move-result v1

    .line 277
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 278
    .line 279
    .line 280
    move-result v5

    .line 281
    int-to-float v5, v5

    .line 282
    mul-float v5, v5, v0

    .line 283
    .line 284
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 285
    .line 286
    .line 287
    move-result v0

    .line 288
    move v8, v2

    .line 289
    move v2, p0

    .line 290
    move p0, v0

    .line 291
    move v0, v4

    .line 292
    move v4, v1

    .line 293
    move v1, v3

    .line 294
    move v3, v8

    .line 295
    :goto_2
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 296
    .line 297
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 298
    .line 299
    add-int/2addr v6, v4

    .line 300
    int-to-float v6, v6

    .line 301
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 302
    .line 303
    sub-int/2addr v7, v1

    .line 304
    int-to-float v7, v7

    .line 305
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 306
    .line 307
    .line 308
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 309
    .line 310
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 311
    .line 312
    add-int/2addr v6, v0

    .line 313
    int-to-float v6, v6

    .line 314
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 315
    .line 316
    sub-int/2addr v7, v1

    .line 317
    int-to-float v7, v7

    .line 318
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 319
    .line 320
    .line 321
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 322
    .line 323
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 324
    .line 325
    add-int/2addr v6, v0

    .line 326
    int-to-float v6, v6

    .line 327
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 328
    .line 329
    sub-int/2addr v7, v2

    .line 330
    int-to-float v7, v7

    .line 331
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 332
    .line 333
    .line 334
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 335
    .line 336
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 337
    .line 338
    int-to-float v6, v6

    .line 339
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 340
    .line 341
    .line 342
    move-result v7

    .line 343
    int-to-float v7, v7

    .line 344
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 345
    .line 346
    .line 347
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 348
    .line 349
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 350
    .line 351
    add-int/2addr v6, v0

    .line 352
    int-to-float v6, v6

    .line 353
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 354
    .line 355
    add-int/2addr v7, v2

    .line 356
    int-to-float v7, v7

    .line 357
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 358
    .line 359
    .line 360
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 361
    .line 362
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 363
    .line 364
    add-int/2addr v6, v0

    .line 365
    int-to-float v6, v6

    .line 366
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 367
    .line 368
    add-int/2addr v7, v1

    .line 369
    int-to-float v7, v7

    .line 370
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 371
    .line 372
    .line 373
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 374
    .line 375
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 376
    .line 377
    add-int/2addr v6, v4

    .line 378
    int-to-float v6, v6

    .line 379
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 380
    .line 381
    add-int/2addr v7, v1

    .line 382
    int-to-float v7, v7

    .line 383
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 384
    .line 385
    .line 386
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 387
    .line 388
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 389
    .line 390
    add-int/2addr v6, v4

    .line 391
    int-to-float v6, v6

    .line 392
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 393
    .line 394
    add-int/2addr v7, p0

    .line 395
    int-to-float v7, v7

    .line 396
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 397
    .line 398
    .line 399
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 400
    .line 401
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 402
    .line 403
    add-int/2addr v6, v3

    .line 404
    int-to-float v6, v6

    .line 405
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 406
    .line 407
    add-int/2addr v7, p0

    .line 408
    int-to-float v7, v7

    .line 409
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 410
    .line 411
    .line 412
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 413
    .line 414
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 415
    .line 416
    .line 417
    move-result v6

    .line 418
    int-to-float v6, v6

    .line 419
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 420
    .line 421
    int-to-float v7, v7

    .line 422
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 423
    .line 424
    .line 425
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 426
    .line 427
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 428
    .line 429
    sub-int/2addr v6, v3

    .line 430
    int-to-float v6, v6

    .line 431
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 432
    .line 433
    add-int/2addr v7, p0

    .line 434
    int-to-float v7, v7

    .line 435
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 436
    .line 437
    .line 438
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 439
    .line 440
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 441
    .line 442
    sub-int/2addr v6, v4

    .line 443
    int-to-float v6, v6

    .line 444
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 445
    .line 446
    add-int/2addr v7, p0

    .line 447
    int-to-float v7, v7

    .line 448
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 449
    .line 450
    .line 451
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 452
    .line 453
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 454
    .line 455
    sub-int/2addr v6, v4

    .line 456
    int-to-float v6, v6

    .line 457
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 458
    .line 459
    add-int/2addr v7, v1

    .line 460
    int-to-float v7, v7

    .line 461
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 462
    .line 463
    .line 464
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 465
    .line 466
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 467
    .line 468
    sub-int/2addr v6, v0

    .line 469
    int-to-float v6, v6

    .line 470
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 471
    .line 472
    add-int/2addr v7, v1

    .line 473
    int-to-float v7, v7

    .line 474
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 475
    .line 476
    .line 477
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 478
    .line 479
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 480
    .line 481
    sub-int/2addr v6, v0

    .line 482
    int-to-float v6, v6

    .line 483
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 484
    .line 485
    add-int/2addr v7, v2

    .line 486
    int-to-float v7, v7

    .line 487
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 488
    .line 489
    .line 490
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 491
    .line 492
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 493
    .line 494
    int-to-float v6, v6

    .line 495
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 496
    .line 497
    .line 498
    move-result v7

    .line 499
    int-to-float v7, v7

    .line 500
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 501
    .line 502
    .line 503
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 504
    .line 505
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 506
    .line 507
    sub-int/2addr v6, v0

    .line 508
    int-to-float v6, v6

    .line 509
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 510
    .line 511
    sub-int/2addr v7, v2

    .line 512
    int-to-float v2, v7

    .line 513
    invoke-virtual {v5, v6, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 514
    .line 515
    .line 516
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 517
    .line 518
    iget v5, p1, Landroid/graphics/Rect;->right:I

    .line 519
    .line 520
    sub-int/2addr v5, v0

    .line 521
    int-to-float v0, v5

    .line 522
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 523
    .line 524
    sub-int/2addr v5, v1

    .line 525
    int-to-float v5, v5

    .line 526
    invoke-virtual {v2, v0, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 527
    .line 528
    .line 529
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 530
    .line 531
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 532
    .line 533
    sub-int/2addr v2, v4

    .line 534
    int-to-float v2, v2

    .line 535
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 536
    .line 537
    sub-int/2addr v5, v1

    .line 538
    int-to-float v1, v5

    .line 539
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 540
    .line 541
    .line 542
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 543
    .line 544
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 545
    .line 546
    sub-int/2addr v1, v4

    .line 547
    int-to-float v1, v1

    .line 548
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 549
    .line 550
    sub-int/2addr v2, p0

    .line 551
    int-to-float v2, v2

    .line 552
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 553
    .line 554
    .line 555
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 556
    .line 557
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 558
    .line 559
    sub-int/2addr v1, v3

    .line 560
    int-to-float v1, v1

    .line 561
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 562
    .line 563
    sub-int/2addr v2, p0

    .line 564
    int-to-float v2, v2

    .line 565
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 566
    .line 567
    .line 568
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 569
    .line 570
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 571
    .line 572
    .line 573
    move-result v1

    .line 574
    int-to-float v1, v1

    .line 575
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 576
    .line 577
    int-to-float v2, v2

    .line 578
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 579
    .line 580
    .line 581
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 582
    .line 583
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 584
    .line 585
    add-int/2addr v1, v3

    .line 586
    int-to-float v1, v1

    .line 587
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 588
    .line 589
    sub-int/2addr v2, p0

    .line 590
    int-to-float v2, v2

    .line 591
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 592
    .line 593
    .line 594
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 595
    .line 596
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 597
    .line 598
    add-int/2addr v1, v4

    .line 599
    int-to-float v1, v1

    .line 600
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 601
    .line 602
    sub-int/2addr p1, p0

    .line 603
    int-to-float p0, p1

    .line 604
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 605
    .line 606
    .line 607
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 608
    .line 609
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 610
    .line 611
    .line 612
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 613
    .line 614
    return-object p0
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getRightArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/high16 v0, 0x3ec00000    # 0.375f

    .line 6
    .line 7
    const v1, 0x3f547ae1    # 0.83f

    .line 8
    .line 9
    .line 10
    const/high16 v2, 0x3e800000    # 0.25f

    .line 11
    .line 12
    const v3, 0x3f2b851f    # 0.67f

    .line 13
    .line 14
    .line 15
    if-eqz p0, :cond_4

    .line 16
    .line 17
    array-length v4, p0

    .line 18
    const/4 v5, 0x1

    .line 19
    if-lt v4, v5, :cond_4

    .line 20
    .line 21
    const/4 v4, 0x0

    .line 22
    aget-object v6, p0, v4

    .line 23
    .line 24
    if-eqz v6, :cond_0

    .line 25
    .line 26
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    int-to-float v3, v3

    .line 31
    aget-object v4, p0, v4

    .line 32
    .line 33
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    mul-float v3, v3, v4

    .line 38
    .line 39
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    goto :goto_0

    .line 44
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    int-to-float v4, v4

    .line 49
    mul-float v4, v4, v3

    .line 50
    .line 51
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 52
    .line 53
    .line 54
    move-result v3

    .line 55
    :goto_0
    array-length v4, p0

    .line 56
    const/4 v6, 0x2

    .line 57
    if-lt v4, v6, :cond_1

    .line 58
    .line 59
    aget-object v4, p0, v5

    .line 60
    .line 61
    if-eqz v4, :cond_1

    .line 62
    .line 63
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    int-to-float v2, v2

    .line 68
    aget-object v4, p0, v5

    .line 69
    .line 70
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 71
    .line 72
    .line 73
    move-result v4

    .line 74
    mul-float v2, v2, v4

    .line 75
    .line 76
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 77
    .line 78
    .line 79
    move-result v2

    .line 80
    goto :goto_1

    .line 81
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 82
    .line 83
    .line 84
    move-result v4

    .line 85
    int-to-float v4, v4

    .line 86
    mul-float v4, v4, v2

    .line 87
    .line 88
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 89
    .line 90
    .line 91
    move-result v2

    .line 92
    :goto_1
    array-length v4, p0

    .line 93
    const/4 v5, 0x3

    .line 94
    if-lt v4, v5, :cond_2

    .line 95
    .line 96
    aget-object v4, p0, v6

    .line 97
    .line 98
    if-eqz v4, :cond_2

    .line 99
    .line 100
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 101
    .line 102
    .line 103
    move-result v1

    .line 104
    int-to-float v1, v1

    .line 105
    aget-object v4, p0, v6

    .line 106
    .line 107
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 108
    .line 109
    .line 110
    move-result v4

    .line 111
    mul-float v1, v1, v4

    .line 112
    .line 113
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 114
    .line 115
    .line 116
    move-result v1

    .line 117
    goto :goto_2

    .line 118
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 119
    .line 120
    .line 121
    move-result v4

    .line 122
    int-to-float v4, v4

    .line 123
    mul-float v4, v4, v1

    .line 124
    .line 125
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 126
    .line 127
    .line 128
    move-result v1

    .line 129
    :goto_2
    array-length v4, p0

    .line 130
    const/4 v6, 0x4

    .line 131
    if-lt v4, v6, :cond_3

    .line 132
    .line 133
    aget-object v4, p0, v5

    .line 134
    .line 135
    if-eqz v4, :cond_3

    .line 136
    .line 137
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 138
    .line 139
    .line 140
    move-result v0

    .line 141
    int-to-float v0, v0

    .line 142
    aget-object p0, p0, v5

    .line 143
    .line 144
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 145
    .line 146
    .line 147
    move-result p0

    .line 148
    mul-float v0, v0, p0

    .line 149
    .line 150
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 151
    .line 152
    .line 153
    move-result p0

    .line 154
    goto :goto_3

    .line 155
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 156
    .line 157
    .line 158
    move-result p0

    .line 159
    int-to-float p0, p0

    .line 160
    mul-float p0, p0, v0

    .line 161
    .line 162
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 163
    .line 164
    .line 165
    move-result p0

    .line 166
    goto :goto_3

    .line 167
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 168
    .line 169
    .line 170
    move-result p0

    .line 171
    int-to-float p0, p0

    .line 172
    mul-float p0, p0, v3

    .line 173
    .line 174
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 175
    .line 176
    .line 177
    move-result v3

    .line 178
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 179
    .line 180
    .line 181
    move-result p0

    .line 182
    int-to-float p0, p0

    .line 183
    mul-float p0, p0, v2

    .line 184
    .line 185
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 186
    .line 187
    .line 188
    move-result v2

    .line 189
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 190
    .line 191
    .line 192
    move-result p0

    .line 193
    int-to-float p0, p0

    .line 194
    mul-float p0, p0, v1

    .line 195
    .line 196
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 197
    .line 198
    .line 199
    move-result v1

    .line 200
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 201
    .line 202
    .line 203
    move-result p0

    .line 204
    int-to-float p0, p0

    .line 205
    mul-float p0, p0, v0

    .line 206
    .line 207
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 208
    .line 209
    .line 210
    move-result p0

    .line 211
    :goto_3
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 212
    .line 213
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 214
    .line 215
    int-to-float v4, v4

    .line 216
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 217
    .line 218
    int-to-float v5, v5

    .line 219
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 220
    .line 221
    .line 222
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 223
    .line 224
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 225
    .line 226
    add-int/2addr v4, v3

    .line 227
    int-to-float v4, v4

    .line 228
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 229
    .line 230
    int-to-float v5, v5

    .line 231
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 232
    .line 233
    .line 234
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 235
    .line 236
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 237
    .line 238
    add-int/2addr v4, v3

    .line 239
    int-to-float v4, v4

    .line 240
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 241
    .line 242
    add-int/2addr v5, p0

    .line 243
    int-to-float v5, v5

    .line 244
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 245
    .line 246
    .line 247
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 248
    .line 249
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 250
    .line 251
    add-int/2addr v4, v1

    .line 252
    int-to-float v4, v4

    .line 253
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 254
    .line 255
    add-int/2addr v5, p0

    .line 256
    int-to-float v5, v5

    .line 257
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 258
    .line 259
    .line 260
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 261
    .line 262
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 263
    .line 264
    add-int/2addr v4, v1

    .line 265
    int-to-float v4, v4

    .line 266
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 267
    .line 268
    add-int/2addr v5, v2

    .line 269
    int-to-float v5, v5

    .line 270
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 271
    .line 272
    .line 273
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 274
    .line 275
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 276
    .line 277
    int-to-float v4, v4

    .line 278
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 279
    .line 280
    .line 281
    move-result v5

    .line 282
    int-to-float v5, v5

    .line 283
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 284
    .line 285
    .line 286
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 287
    .line 288
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 289
    .line 290
    add-int/2addr v4, v1

    .line 291
    int-to-float v4, v4

    .line 292
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 293
    .line 294
    sub-int/2addr v5, v2

    .line 295
    int-to-float v2, v5

    .line 296
    invoke-virtual {v0, v4, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 297
    .line 298
    .line 299
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 300
    .line 301
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 302
    .line 303
    add-int/2addr v2, v1

    .line 304
    int-to-float v1, v2

    .line 305
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 306
    .line 307
    sub-int/2addr v2, p0

    .line 308
    int-to-float v2, v2

    .line 309
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 310
    .line 311
    .line 312
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 313
    .line 314
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 315
    .line 316
    add-int/2addr v1, v3

    .line 317
    int-to-float v1, v1

    .line 318
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 319
    .line 320
    sub-int/2addr v2, p0

    .line 321
    int-to-float p0, v2

    .line 322
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 323
    .line 324
    .line 325
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 326
    .line 327
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 328
    .line 329
    add-int/2addr v0, v3

    .line 330
    int-to-float v0, v0

    .line 331
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 332
    .line 333
    int-to-float v1, v1

    .line 334
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 335
    .line 336
    .line 337
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 338
    .line 339
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 340
    .line 341
    int-to-float v0, v0

    .line 342
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 343
    .line 344
    int-to-float p1, p1

    .line 345
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 346
    .line 347
    .line 348
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 349
    .line 350
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 351
    .line 352
    .line 353
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 354
    .line 355
    return-object p0
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/high16 v0, 0x3e800000    # 0.25f

    .line 6
    .line 7
    const/high16 v1, 0x3f400000    # 0.75f

    .line 8
    .line 9
    if-eqz p0, :cond_2

    .line 10
    .line 11
    array-length v2, p0

    .line 12
    const/4 v3, 0x1

    .line 13
    if-lt v2, v3, :cond_2

    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    aget-object v4, p0, v2

    .line 17
    .line 18
    if-eqz v4, :cond_0

    .line 19
    .line 20
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    int-to-float v1, v1

    .line 25
    aget-object v2, p0, v2

    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    mul-float v1, v1, v2

    .line 32
    .line 33
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    goto :goto_0

    .line 38
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    int-to-float v2, v2

    .line 43
    mul-float v2, v2, v1

    .line 44
    .line 45
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    :goto_0
    array-length v2, p0

    .line 50
    const/4 v4, 0x2

    .line 51
    if-lt v2, v4, :cond_1

    .line 52
    .line 53
    aget-object v2, p0, v3

    .line 54
    .line 55
    if-eqz v2, :cond_1

    .line 56
    .line 57
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    int-to-float v0, v0

    .line 62
    aget-object p0, p0, v3

    .line 63
    .line 64
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 65
    .line 66
    .line 67
    move-result p0

    .line 68
    mul-float v0, v0, p0

    .line 69
    .line 70
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 71
    .line 72
    .line 73
    move-result p0

    .line 74
    goto :goto_1

    .line 75
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 76
    .line 77
    .line 78
    move-result p0

    .line 79
    int-to-float p0, p0

    .line 80
    mul-float p0, p0, v0

    .line 81
    .line 82
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 83
    .line 84
    .line 85
    move-result p0

    .line 86
    goto :goto_1

    .line 87
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 88
    .line 89
    .line 90
    move-result p0

    .line 91
    int-to-float p0, p0

    .line 92
    mul-float p0, p0, v1

    .line 93
    .line 94
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 95
    .line 96
    .line 97
    move-result v1

    .line 98
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 99
    .line 100
    .line 101
    move-result p0

    .line 102
    int-to-float p0, p0

    .line 103
    mul-float p0, p0, v0

    .line 104
    .line 105
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 106
    .line 107
    .line 108
    move-result p0

    .line 109
    :goto_1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 110
    .line 111
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 112
    .line 113
    int-to-float v2, v2

    .line 114
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 115
    .line 116
    add-int/2addr v3, p0

    .line 117
    int-to-float v3, v3

    .line 118
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 119
    .line 120
    .line 121
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 122
    .line 123
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 124
    .line 125
    add-int/2addr v2, v1

    .line 126
    int-to-float v2, v2

    .line 127
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 128
    .line 129
    add-int/2addr v3, p0

    .line 130
    int-to-float v3, v3

    .line 131
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 132
    .line 133
    .line 134
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 135
    .line 136
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 137
    .line 138
    add-int/2addr v2, v1

    .line 139
    int-to-float v2, v2

    .line 140
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 141
    .line 142
    int-to-float v3, v3

    .line 143
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 144
    .line 145
    .line 146
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 147
    .line 148
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 149
    .line 150
    int-to-float v2, v2

    .line 151
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 152
    .line 153
    .line 154
    move-result v3

    .line 155
    int-to-float v3, v3

    .line 156
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 157
    .line 158
    .line 159
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 160
    .line 161
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 162
    .line 163
    add-int/2addr v2, v1

    .line 164
    int-to-float v2, v2

    .line 165
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 166
    .line 167
    int-to-float v3, v3

    .line 168
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    .line 170
    .line 171
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 172
    .line 173
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 174
    .line 175
    add-int/2addr v2, v1

    .line 176
    int-to-float v1, v2

    .line 177
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 178
    .line 179
    sub-int/2addr v2, p0

    .line 180
    int-to-float v2, v2

    .line 181
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 182
    .line 183
    .line 184
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 185
    .line 186
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 187
    .line 188
    int-to-float v1, v1

    .line 189
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 190
    .line 191
    sub-int/2addr p1, p0

    .line 192
    int-to-float p0, p1

    .line 193
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 194
    .line 195
    .line 196
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 197
    .line 198
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 199
    .line 200
    .line 201
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 202
    .line 203
    return-object p0
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getStripedRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 17

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const/high16 v2, 0x3e800000    # 0.25f

    .line 8
    .line 9
    const/high16 v3, 0x3f400000    # 0.75f

    .line 10
    .line 11
    if-eqz v1, :cond_2

    .line 12
    .line 13
    array-length v4, v1

    .line 14
    const/4 v5, 0x1

    .line 15
    if-lt v4, v5, :cond_2

    .line 16
    .line 17
    const/4 v4, 0x0

    .line 18
    aget-object v6, v1, v4

    .line 19
    .line 20
    if-eqz v6, :cond_0

    .line 21
    .line 22
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 23
    .line 24
    .line 25
    move-result v3

    .line 26
    int-to-float v3, v3

    .line 27
    aget-object v4, v1, v4

    .line 28
    .line 29
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 30
    .line 31
    .line 32
    move-result v4

    .line 33
    mul-float v3, v3, v4

    .line 34
    .line 35
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    goto :goto_0

    .line 40
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 41
    .line 42
    .line 43
    move-result v4

    .line 44
    int-to-float v4, v4

    .line 45
    mul-float v4, v4, v3

    .line 46
    .line 47
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 48
    .line 49
    .line 50
    move-result v3

    .line 51
    :goto_0
    array-length v4, v1

    .line 52
    const/4 v6, 0x2

    .line 53
    if-lt v4, v6, :cond_1

    .line 54
    .line 55
    aget-object v4, v1, v5

    .line 56
    .line 57
    if-eqz v4, :cond_1

    .line 58
    .line 59
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    int-to-float v2, v2

    .line 64
    aget-object v1, v1, v5

    .line 65
    .line 66
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 67
    .line 68
    .line 69
    move-result v1

    .line 70
    mul-float v2, v2, v1

    .line 71
    .line 72
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 73
    .line 74
    .line 75
    move-result v1

    .line 76
    goto :goto_1

    .line 77
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    int-to-float v1, v1

    .line 82
    mul-float v1, v1, v2

    .line 83
    .line 84
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 85
    .line 86
    .line 87
    move-result v1

    .line 88
    goto :goto_1

    .line 89
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 90
    .line 91
    .line 92
    move-result v1

    .line 93
    int-to-float v1, v1

    .line 94
    mul-float v1, v1, v3

    .line 95
    .line 96
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 97
    .line 98
    .line 99
    move-result v3

    .line 100
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 101
    .line 102
    .line 103
    move-result v1

    .line 104
    int-to-float v1, v1

    .line 105
    mul-float v1, v1, v2

    .line 106
    .line 107
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 108
    .line 109
    .line 110
    move-result v1

    .line 111
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 112
    .line 113
    .line 114
    move-result v2

    .line 115
    int-to-float v2, v2

    .line 116
    const v4, 0x3cf5c28f    # 0.03f

    .line 117
    .line 118
    .line 119
    mul-float v2, v2, v4

    .line 120
    .line 121
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 122
    .line 123
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 124
    .line 125
    int-to-float v6, v5

    .line 126
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 127
    .line 128
    add-int/2addr v7, v1

    .line 129
    int-to-float v7, v7

    .line 130
    int-to-float v5, v5

    .line 131
    add-float v8, v5, v2

    .line 132
    .line 133
    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 134
    .line 135
    sub-int/2addr v5, v1

    .line 136
    int-to-float v9, v5

    .line 137
    sget-object v10, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 138
    .line 139
    move v5, v6

    .line 140
    move v6, v7

    .line 141
    move v7, v8

    .line 142
    move v8, v9

    .line 143
    move-object v9, v10

    .line 144
    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 145
    .line 146
    .line 147
    sget-object v11, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 148
    .line 149
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 150
    .line 151
    int-to-float v5, v4

    .line 152
    const/high16 v6, 0x40000000    # 2.0f

    .line 153
    .line 154
    mul-float v6, v6, v2

    .line 155
    .line 156
    add-float v12, v5, v6

    .line 157
    .line 158
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 159
    .line 160
    add-int/2addr v5, v1

    .line 161
    int-to-float v13, v5

    .line 162
    int-to-float v4, v4

    .line 163
    const/high16 v5, 0x40800000    # 4.0f

    .line 164
    .line 165
    mul-float v5, v5, v2

    .line 166
    .line 167
    add-float v14, v4, v5

    .line 168
    .line 169
    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    .line 170
    .line 171
    sub-int/2addr v4, v1

    .line 172
    int-to-float v15, v4

    .line 173
    sget-object v16, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 174
    .line 175
    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 176
    .line 177
    .line 178
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 179
    .line 180
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 181
    .line 182
    int-to-float v5, v5

    .line 183
    const/high16 v6, 0x40a00000    # 5.0f

    .line 184
    .line 185
    mul-float v2, v2, v6

    .line 186
    .line 187
    add-float/2addr v5, v2

    .line 188
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 189
    .line 190
    add-int/2addr v6, v1

    .line 191
    int-to-float v6, v6

    .line 192
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 193
    .line 194
    .line 195
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 196
    .line 197
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 198
    .line 199
    add-int/2addr v5, v3

    .line 200
    int-to-float v5, v5

    .line 201
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 202
    .line 203
    add-int/2addr v6, v1

    .line 204
    int-to-float v6, v6

    .line 205
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 206
    .line 207
    .line 208
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 209
    .line 210
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 211
    .line 212
    add-int/2addr v5, v3

    .line 213
    int-to-float v5, v5

    .line 214
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 215
    .line 216
    int-to-float v6, v6

    .line 217
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 218
    .line 219
    .line 220
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 221
    .line 222
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 223
    .line 224
    int-to-float v5, v5

    .line 225
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 226
    .line 227
    .line 228
    move-result v6

    .line 229
    int-to-float v6, v6

    .line 230
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 231
    .line 232
    .line 233
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 234
    .line 235
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 236
    .line 237
    add-int/2addr v5, v3

    .line 238
    int-to-float v5, v5

    .line 239
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    .line 240
    .line 241
    int-to-float v6, v6

    .line 242
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 243
    .line 244
    .line 245
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 246
    .line 247
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 248
    .line 249
    add-int/2addr v5, v3

    .line 250
    int-to-float v3, v5

    .line 251
    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 252
    .line 253
    sub-int/2addr v5, v1

    .line 254
    int-to-float v5, v5

    .line 255
    invoke-virtual {v4, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 256
    .line 257
    .line 258
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 259
    .line 260
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 261
    .line 262
    int-to-float v4, v4

    .line 263
    add-float/2addr v4, v2

    .line 264
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 265
    .line 266
    sub-int/2addr v0, v1

    .line 267
    int-to-float v0, v0

    .line 268
    invoke-virtual {v3, v4, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 269
    .line 270
    .line 271
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 272
    .line 273
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 274
    .line 275
    .line 276
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 277
    .line 278
    return-object v0
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getUpArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/high16 v0, 0x3ec00000    # 0.375f

    .line 6
    .line 7
    const v1, 0x3e2e147b    # 0.17f

    .line 8
    .line 9
    .line 10
    const/high16 v2, 0x3e800000    # 0.25f

    .line 11
    .line 12
    const v3, 0x3ea8f5c3    # 0.33f

    .line 13
    .line 14
    .line 15
    if-eqz p0, :cond_4

    .line 16
    .line 17
    array-length v4, p0

    .line 18
    const/4 v5, 0x1

    .line 19
    if-lt v4, v5, :cond_4

    .line 20
    .line 21
    const/4 v4, 0x0

    .line 22
    aget-object v6, p0, v4

    .line 23
    .line 24
    if-eqz v6, :cond_0

    .line 25
    .line 26
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    int-to-float v3, v3

    .line 31
    aget-object v4, p0, v4

    .line 32
    .line 33
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    mul-float v3, v3, v4

    .line 38
    .line 39
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    goto :goto_0

    .line 44
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    int-to-float v4, v4

    .line 49
    mul-float v4, v4, v3

    .line 50
    .line 51
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 52
    .line 53
    .line 54
    move-result v3

    .line 55
    :goto_0
    array-length v4, p0

    .line 56
    const/4 v6, 0x2

    .line 57
    if-lt v4, v6, :cond_1

    .line 58
    .line 59
    aget-object v4, p0, v5

    .line 60
    .line 61
    if-eqz v4, :cond_1

    .line 62
    .line 63
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    int-to-float v2, v2

    .line 68
    aget-object v4, p0, v5

    .line 69
    .line 70
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 71
    .line 72
    .line 73
    move-result v4

    .line 74
    mul-float v2, v2, v4

    .line 75
    .line 76
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 77
    .line 78
    .line 79
    move-result v2

    .line 80
    goto :goto_1

    .line 81
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 82
    .line 83
    .line 84
    move-result v4

    .line 85
    int-to-float v4, v4

    .line 86
    mul-float v4, v4, v2

    .line 87
    .line 88
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 89
    .line 90
    .line 91
    move-result v2

    .line 92
    :goto_1
    array-length v4, p0

    .line 93
    const/4 v5, 0x3

    .line 94
    if-lt v4, v5, :cond_2

    .line 95
    .line 96
    aget-object v4, p0, v6

    .line 97
    .line 98
    if-eqz v4, :cond_2

    .line 99
    .line 100
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 101
    .line 102
    .line 103
    move-result v1

    .line 104
    int-to-float v1, v1

    .line 105
    aget-object v4, p0, v6

    .line 106
    .line 107
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 108
    .line 109
    .line 110
    move-result v4

    .line 111
    mul-float v1, v1, v4

    .line 112
    .line 113
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 114
    .line 115
    .line 116
    move-result v1

    .line 117
    goto :goto_2

    .line 118
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 119
    .line 120
    .line 121
    move-result v4

    .line 122
    int-to-float v4, v4

    .line 123
    mul-float v4, v4, v1

    .line 124
    .line 125
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 126
    .line 127
    .line 128
    move-result v1

    .line 129
    :goto_2
    array-length v4, p0

    .line 130
    const/4 v6, 0x4

    .line 131
    if-lt v4, v6, :cond_3

    .line 132
    .line 133
    aget-object v4, p0, v5

    .line 134
    .line 135
    if-eqz v4, :cond_3

    .line 136
    .line 137
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 138
    .line 139
    .line 140
    move-result v0

    .line 141
    int-to-float v0, v0

    .line 142
    aget-object p0, p0, v5

    .line 143
    .line 144
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 145
    .line 146
    .line 147
    move-result p0

    .line 148
    mul-float v0, v0, p0

    .line 149
    .line 150
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 151
    .line 152
    .line 153
    move-result p0

    .line 154
    goto :goto_3

    .line 155
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 156
    .line 157
    .line 158
    move-result p0

    .line 159
    int-to-float p0, p0

    .line 160
    mul-float p0, p0, v0

    .line 161
    .line 162
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 163
    .line 164
    .line 165
    move-result p0

    .line 166
    goto :goto_3

    .line 167
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 168
    .line 169
    .line 170
    move-result p0

    .line 171
    int-to-float p0, p0

    .line 172
    mul-float p0, p0, v3

    .line 173
    .line 174
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 175
    .line 176
    .line 177
    move-result v3

    .line 178
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 179
    .line 180
    .line 181
    move-result p0

    .line 182
    int-to-float p0, p0

    .line 183
    mul-float p0, p0, v2

    .line 184
    .line 185
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 186
    .line 187
    .line 188
    move-result v2

    .line 189
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 190
    .line 191
    .line 192
    move-result p0

    .line 193
    int-to-float p0, p0

    .line 194
    mul-float p0, p0, v1

    .line 195
    .line 196
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 197
    .line 198
    .line 199
    move-result v1

    .line 200
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 201
    .line 202
    .line 203
    move-result p0

    .line 204
    int-to-float p0, p0

    .line 205
    mul-float p0, p0, v0

    .line 206
    .line 207
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 208
    .line 209
    .line 210
    move-result p0

    .line 211
    :goto_3
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 212
    .line 213
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 214
    .line 215
    int-to-float v4, v4

    .line 216
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 217
    .line 218
    int-to-float v5, v5

    .line 219
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 220
    .line 221
    .line 222
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 223
    .line 224
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 225
    .line 226
    int-to-float v4, v4

    .line 227
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 228
    .line 229
    add-int/2addr v5, v3

    .line 230
    int-to-float v5, v5

    .line 231
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 232
    .line 233
    .line 234
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 235
    .line 236
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 237
    .line 238
    add-int/2addr v4, p0

    .line 239
    int-to-float v4, v4

    .line 240
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 241
    .line 242
    add-int/2addr v5, v3

    .line 243
    int-to-float v5, v5

    .line 244
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 245
    .line 246
    .line 247
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 248
    .line 249
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 250
    .line 251
    add-int/2addr v4, p0

    .line 252
    int-to-float v4, v4

    .line 253
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 254
    .line 255
    add-int/2addr v5, v1

    .line 256
    int-to-float v5, v5

    .line 257
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 258
    .line 259
    .line 260
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 261
    .line 262
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 263
    .line 264
    add-int/2addr v4, v2

    .line 265
    int-to-float v4, v4

    .line 266
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 267
    .line 268
    add-int/2addr v5, v1

    .line 269
    int-to-float v5, v5

    .line 270
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 271
    .line 272
    .line 273
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 274
    .line 275
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 276
    .line 277
    .line 278
    move-result v4

    .line 279
    int-to-float v4, v4

    .line 280
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 281
    .line 282
    int-to-float v5, v5

    .line 283
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 284
    .line 285
    .line 286
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 287
    .line 288
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 289
    .line 290
    sub-int/2addr v4, v2

    .line 291
    int-to-float v2, v4

    .line 292
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 293
    .line 294
    add-int/2addr v4, v1

    .line 295
    int-to-float v4, v4

    .line 296
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 297
    .line 298
    .line 299
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 300
    .line 301
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 302
    .line 303
    sub-int/2addr v2, p0

    .line 304
    int-to-float v2, v2

    .line 305
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 306
    .line 307
    add-int/2addr v4, v1

    .line 308
    int-to-float v1, v4

    .line 309
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 310
    .line 311
    .line 312
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 313
    .line 314
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 315
    .line 316
    sub-int/2addr v1, p0

    .line 317
    int-to-float p0, v1

    .line 318
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 319
    .line 320
    add-int/2addr v1, v3

    .line 321
    int-to-float v1, v1

    .line 322
    invoke-virtual {v0, p0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 323
    .line 324
    .line 325
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 326
    .line 327
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 328
    .line 329
    int-to-float v0, v0

    .line 330
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 331
    .line 332
    add-int/2addr v1, v3

    .line 333
    int-to-float v1, v1

    .line 334
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 335
    .line 336
    .line 337
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 338
    .line 339
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 340
    .line 341
    int-to-float v0, v0

    .line 342
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 343
    .line 344
    int-to-float p1, p1

    .line 345
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 346
    .line 347
    .line 348
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 349
    .line 350
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 351
    .line 352
    .line 353
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 354
    .line 355
    return-object p0
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/high16 v0, 0x3e800000    # 0.25f

    .line 6
    .line 7
    if-eqz p0, :cond_2

    .line 8
    .line 9
    array-length v1, p0

    .line 10
    const/4 v2, 0x1

    .line 11
    if-lt v1, v2, :cond_2

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    aget-object v3, p0, v1

    .line 15
    .line 16
    if-eqz v3, :cond_0

    .line 17
    .line 18
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    int-to-float v3, v3

    .line 23
    aget-object v1, p0, v1

    .line 24
    .line 25
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    mul-float v3, v3, v1

    .line 30
    .line 31
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    int-to-float v1, v1

    .line 41
    mul-float v1, v1, v0

    .line 42
    .line 43
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    :goto_0
    array-length v3, p0

    .line 48
    const/4 v4, 0x2

    .line 49
    if-lt v3, v4, :cond_1

    .line 50
    .line 51
    aget-object v3, p0, v2

    .line 52
    .line 53
    if-eqz v3, :cond_1

    .line 54
    .line 55
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    int-to-float v0, v0

    .line 60
    aget-object p0, p0, v2

    .line 61
    .line 62
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 63
    .line 64
    .line 65
    move-result p0

    .line 66
    mul-float v0, v0, p0

    .line 67
    .line 68
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 69
    .line 70
    .line 71
    move-result p0

    .line 72
    goto :goto_1

    .line 73
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 74
    .line 75
    .line 76
    move-result p0

    .line 77
    int-to-float p0, p0

    .line 78
    mul-float p0, p0, v0

    .line 79
    .line 80
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 81
    .line 82
    .line 83
    move-result p0

    .line 84
    goto :goto_1

    .line 85
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 86
    .line 87
    .line 88
    move-result p0

    .line 89
    int-to-float p0, p0

    .line 90
    mul-float p0, p0, v0

    .line 91
    .line 92
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 97
    .line 98
    .line 99
    move-result p0

    .line 100
    int-to-float p0, p0

    .line 101
    mul-float p0, p0, v0

    .line 102
    .line 103
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 104
    .line 105
    .line 106
    move-result p0

    .line 107
    :goto_1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 108
    .line 109
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 110
    .line 111
    .line 112
    move-result v2

    .line 113
    int-to-float v2, v2

    .line 114
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 115
    .line 116
    int-to-float v3, v3

    .line 117
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 118
    .line 119
    .line 120
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 121
    .line 122
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 123
    .line 124
    int-to-float v2, v2

    .line 125
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 126
    .line 127
    add-int/2addr v3, v1

    .line 128
    int-to-float v3, v3

    .line 129
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 130
    .line 131
    .line 132
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 133
    .line 134
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 135
    .line 136
    sub-int/2addr v2, p0

    .line 137
    int-to-float v2, v2

    .line 138
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 139
    .line 140
    add-int/2addr v3, v1

    .line 141
    int-to-float v3, v3

    .line 142
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 143
    .line 144
    .line 145
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 146
    .line 147
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 148
    .line 149
    sub-int/2addr v2, p0

    .line 150
    int-to-float v2, v2

    .line 151
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 152
    .line 153
    int-to-float v3, v3

    .line 154
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 155
    .line 156
    .line 157
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 158
    .line 159
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 160
    .line 161
    add-int/2addr v2, p0

    .line 162
    int-to-float v2, v2

    .line 163
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 164
    .line 165
    int-to-float v3, v3

    .line 166
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 167
    .line 168
    .line 169
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 170
    .line 171
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 172
    .line 173
    add-int/2addr v2, p0

    .line 174
    int-to-float p0, v2

    .line 175
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 176
    .line 177
    add-int/2addr v2, v1

    .line 178
    int-to-float v2, v2

    .line 179
    invoke-virtual {v0, p0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 180
    .line 181
    .line 182
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 183
    .line 184
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 185
    .line 186
    int-to-float v0, v0

    .line 187
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 188
    .line 189
    add-int/2addr p1, v1

    .line 190
    int-to-float p1, p1

    .line 191
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 192
    .line 193
    .line 194
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 195
    .line 196
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 197
    .line 198
    .line 199
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 200
    .line 201
    return-object p0
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getUpDownArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/high16 v0, 0x3ec00000    # 0.375f

    .line 6
    .line 7
    const/high16 v1, 0x3e000000    # 0.125f

    .line 8
    .line 9
    const/high16 v2, 0x3e800000    # 0.25f

    .line 10
    .line 11
    if-eqz p0, :cond_4

    .line 12
    .line 13
    array-length v3, p0

    .line 14
    const/4 v4, 0x1

    .line 15
    if-lt v3, v4, :cond_4

    .line 16
    .line 17
    const/4 v3, 0x0

    .line 18
    aget-object v5, p0, v3

    .line 19
    .line 20
    if-eqz v5, :cond_0

    .line 21
    .line 22
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 23
    .line 24
    .line 25
    move-result v5

    .line 26
    int-to-float v5, v5

    .line 27
    aget-object v3, p0, v3

    .line 28
    .line 29
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 30
    .line 31
    .line 32
    move-result v3

    .line 33
    mul-float v5, v5, v3

    .line 34
    .line 35
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    goto :goto_0

    .line 40
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    int-to-float v3, v3

    .line 45
    mul-float v3, v3, v2

    .line 46
    .line 47
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 48
    .line 49
    .line 50
    move-result v3

    .line 51
    :goto_0
    array-length v5, p0

    .line 52
    const/4 v6, 0x2

    .line 53
    if-lt v5, v6, :cond_1

    .line 54
    .line 55
    aget-object v5, p0, v4

    .line 56
    .line 57
    if-eqz v5, :cond_1

    .line 58
    .line 59
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    int-to-float v2, v2

    .line 64
    aget-object v4, p0, v4

    .line 65
    .line 66
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 67
    .line 68
    .line 69
    move-result v4

    .line 70
    mul-float v2, v2, v4

    .line 71
    .line 72
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 73
    .line 74
    .line 75
    move-result v2

    .line 76
    goto :goto_1

    .line 77
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 78
    .line 79
    .line 80
    move-result v4

    .line 81
    int-to-float v4, v4

    .line 82
    mul-float v4, v4, v2

    .line 83
    .line 84
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 85
    .line 86
    .line 87
    move-result v2

    .line 88
    :goto_1
    array-length v4, p0

    .line 89
    const/4 v5, 0x3

    .line 90
    if-lt v4, v5, :cond_2

    .line 91
    .line 92
    aget-object v4, p0, v6

    .line 93
    .line 94
    if-eqz v4, :cond_2

    .line 95
    .line 96
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 97
    .line 98
    .line 99
    move-result v1

    .line 100
    int-to-float v1, v1

    .line 101
    aget-object v4, p0, v6

    .line 102
    .line 103
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 104
    .line 105
    .line 106
    move-result v4

    .line 107
    mul-float v1, v1, v4

    .line 108
    .line 109
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 110
    .line 111
    .line 112
    move-result v1

    .line 113
    goto :goto_2

    .line 114
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 115
    .line 116
    .line 117
    move-result v4

    .line 118
    int-to-float v4, v4

    .line 119
    mul-float v4, v4, v1

    .line 120
    .line 121
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 122
    .line 123
    .line 124
    move-result v1

    .line 125
    :goto_2
    array-length v4, p0

    .line 126
    const/4 v6, 0x4

    .line 127
    if-lt v4, v6, :cond_3

    .line 128
    .line 129
    aget-object v4, p0, v5

    .line 130
    .line 131
    if-eqz v4, :cond_3

    .line 132
    .line 133
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 134
    .line 135
    .line 136
    move-result v0

    .line 137
    int-to-float v0, v0

    .line 138
    aget-object p0, p0, v5

    .line 139
    .line 140
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 141
    .line 142
    .line 143
    move-result p0

    .line 144
    mul-float v0, v0, p0

    .line 145
    .line 146
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 147
    .line 148
    .line 149
    move-result p0

    .line 150
    goto :goto_3

    .line 151
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 152
    .line 153
    .line 154
    move-result p0

    .line 155
    int-to-float p0, p0

    .line 156
    mul-float p0, p0, v0

    .line 157
    .line 158
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 159
    .line 160
    .line 161
    move-result p0

    .line 162
    goto :goto_3

    .line 163
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 164
    .line 165
    .line 166
    move-result p0

    .line 167
    int-to-float p0, p0

    .line 168
    mul-float p0, p0, v2

    .line 169
    .line 170
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 171
    .line 172
    .line 173
    move-result v3

    .line 174
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 175
    .line 176
    .line 177
    move-result p0

    .line 178
    int-to-float p0, p0

    .line 179
    mul-float p0, p0, v2

    .line 180
    .line 181
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 182
    .line 183
    .line 184
    move-result v2

    .line 185
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 186
    .line 187
    .line 188
    move-result p0

    .line 189
    int-to-float p0, p0

    .line 190
    mul-float p0, p0, v1

    .line 191
    .line 192
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 193
    .line 194
    .line 195
    move-result v1

    .line 196
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 197
    .line 198
    .line 199
    move-result p0

    .line 200
    int-to-float p0, p0

    .line 201
    mul-float p0, p0, v0

    .line 202
    .line 203
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 204
    .line 205
    .line 206
    move-result p0

    .line 207
    :goto_3
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 208
    .line 209
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 210
    .line 211
    int-to-float v4, v4

    .line 212
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 213
    .line 214
    add-int/2addr v5, v3

    .line 215
    int-to-float v5, v5

    .line 216
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 217
    .line 218
    .line 219
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 220
    .line 221
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 222
    .line 223
    add-int/2addr v4, p0

    .line 224
    int-to-float v4, v4

    .line 225
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 226
    .line 227
    add-int/2addr v5, v3

    .line 228
    int-to-float v5, v5

    .line 229
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 230
    .line 231
    .line 232
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 233
    .line 234
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 235
    .line 236
    add-int/2addr v4, p0

    .line 237
    int-to-float v4, v4

    .line 238
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 239
    .line 240
    add-int/2addr v5, v1

    .line 241
    int-to-float v5, v5

    .line 242
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 243
    .line 244
    .line 245
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 246
    .line 247
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 248
    .line 249
    add-int/2addr v4, v2

    .line 250
    int-to-float v4, v4

    .line 251
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 252
    .line 253
    add-int/2addr v5, v1

    .line 254
    int-to-float v5, v5

    .line 255
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 256
    .line 257
    .line 258
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 259
    .line 260
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 261
    .line 262
    .line 263
    move-result v4

    .line 264
    int-to-float v4, v4

    .line 265
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 266
    .line 267
    int-to-float v5, v5

    .line 268
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 269
    .line 270
    .line 271
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 272
    .line 273
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 274
    .line 275
    sub-int/2addr v4, v2

    .line 276
    int-to-float v4, v4

    .line 277
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 278
    .line 279
    add-int/2addr v5, v1

    .line 280
    int-to-float v5, v5

    .line 281
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 282
    .line 283
    .line 284
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 285
    .line 286
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 287
    .line 288
    sub-int/2addr v4, p0

    .line 289
    int-to-float v4, v4

    .line 290
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 291
    .line 292
    add-int/2addr v5, v1

    .line 293
    int-to-float v5, v5

    .line 294
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 295
    .line 296
    .line 297
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 298
    .line 299
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 300
    .line 301
    sub-int/2addr v4, p0

    .line 302
    int-to-float v4, v4

    .line 303
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 304
    .line 305
    add-int/2addr v5, v3

    .line 306
    int-to-float v5, v5

    .line 307
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 308
    .line 309
    .line 310
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 311
    .line 312
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 313
    .line 314
    int-to-float v4, v4

    .line 315
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 316
    .line 317
    add-int/2addr v5, v3

    .line 318
    int-to-float v5, v5

    .line 319
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 320
    .line 321
    .line 322
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 323
    .line 324
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 325
    .line 326
    int-to-float v4, v4

    .line 327
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 328
    .line 329
    sub-int/2addr v5, v3

    .line 330
    int-to-float v5, v5

    .line 331
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 332
    .line 333
    .line 334
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 335
    .line 336
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 337
    .line 338
    sub-int/2addr v4, p0

    .line 339
    int-to-float v4, v4

    .line 340
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 341
    .line 342
    sub-int/2addr v5, v3

    .line 343
    int-to-float v5, v5

    .line 344
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 345
    .line 346
    .line 347
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 348
    .line 349
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 350
    .line 351
    sub-int/2addr v4, p0

    .line 352
    int-to-float v4, v4

    .line 353
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 354
    .line 355
    sub-int/2addr v5, v1

    .line 356
    int-to-float v5, v5

    .line 357
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 358
    .line 359
    .line 360
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 361
    .line 362
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 363
    .line 364
    sub-int/2addr v4, v2

    .line 365
    int-to-float v4, v4

    .line 366
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 367
    .line 368
    sub-int/2addr v5, v1

    .line 369
    int-to-float v5, v5

    .line 370
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 371
    .line 372
    .line 373
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 374
    .line 375
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 376
    .line 377
    .line 378
    move-result v4

    .line 379
    int-to-float v4, v4

    .line 380
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 381
    .line 382
    int-to-float v5, v5

    .line 383
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 384
    .line 385
    .line 386
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 387
    .line 388
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 389
    .line 390
    add-int/2addr v4, v2

    .line 391
    int-to-float v2, v4

    .line 392
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 393
    .line 394
    sub-int/2addr v4, v1

    .line 395
    int-to-float v4, v4

    .line 396
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 397
    .line 398
    .line 399
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 400
    .line 401
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 402
    .line 403
    add-int/2addr v2, p0

    .line 404
    int-to-float v2, v2

    .line 405
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 406
    .line 407
    sub-int/2addr v4, v1

    .line 408
    int-to-float v1, v4

    .line 409
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 410
    .line 411
    .line 412
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 413
    .line 414
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 415
    .line 416
    add-int/2addr v1, p0

    .line 417
    int-to-float p0, v1

    .line 418
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 419
    .line 420
    sub-int/2addr v1, v3

    .line 421
    int-to-float v1, v1

    .line 422
    invoke-virtual {v0, p0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 423
    .line 424
    .line 425
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 426
    .line 427
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 428
    .line 429
    int-to-float v0, v0

    .line 430
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 431
    .line 432
    sub-int/2addr p1, v3

    .line 433
    int-to-float p1, p1

    .line 434
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 435
    .line 436
    .line 437
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 438
    .line 439
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 440
    .line 441
    .line 442
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 443
    .line 444
    return-object p0
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getUpDownArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/high16 v0, 0x3e800000    # 0.25f

    .line 6
    .line 7
    if-eqz p0, :cond_2

    .line 8
    .line 9
    array-length v1, p0

    .line 10
    const/4 v2, 0x1

    .line 11
    if-lt v1, v2, :cond_2

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    aget-object v3, p0, v1

    .line 15
    .line 16
    if-eqz v3, :cond_0

    .line 17
    .line 18
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    int-to-float v3, v3

    .line 23
    aget-object v1, p0, v1

    .line 24
    .line 25
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    mul-float v3, v3, v1

    .line 30
    .line 31
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    int-to-float v1, v1

    .line 41
    mul-float v1, v1, v0

    .line 42
    .line 43
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    :goto_0
    array-length v3, p0

    .line 48
    const/4 v4, 0x2

    .line 49
    if-lt v3, v4, :cond_1

    .line 50
    .line 51
    aget-object v3, p0, v2

    .line 52
    .line 53
    if-eqz v3, :cond_1

    .line 54
    .line 55
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    int-to-float v0, v0

    .line 60
    aget-object p0, p0, v2

    .line 61
    .line 62
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 63
    .line 64
    .line 65
    move-result p0

    .line 66
    mul-float v0, v0, p0

    .line 67
    .line 68
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 69
    .line 70
    .line 71
    move-result p0

    .line 72
    goto :goto_1

    .line 73
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 74
    .line 75
    .line 76
    move-result p0

    .line 77
    int-to-float p0, p0

    .line 78
    mul-float p0, p0, v0

    .line 79
    .line 80
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 81
    .line 82
    .line 83
    move-result p0

    .line 84
    goto :goto_1

    .line 85
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 86
    .line 87
    .line 88
    move-result p0

    .line 89
    int-to-float p0, p0

    .line 90
    mul-float p0, p0, v0

    .line 91
    .line 92
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 97
    .line 98
    .line 99
    move-result p0

    .line 100
    int-to-float p0, p0

    .line 101
    mul-float p0, p0, v0

    .line 102
    .line 103
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 104
    .line 105
    .line 106
    move-result p0

    .line 107
    :goto_1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 108
    .line 109
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 110
    .line 111
    .line 112
    move-result v2

    .line 113
    int-to-float v2, v2

    .line 114
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 115
    .line 116
    int-to-float v3, v3

    .line 117
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 118
    .line 119
    .line 120
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 121
    .line 122
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 123
    .line 124
    int-to-float v2, v2

    .line 125
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 126
    .line 127
    add-int/2addr v3, p0

    .line 128
    int-to-float v3, v3

    .line 129
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 130
    .line 131
    .line 132
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 133
    .line 134
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 135
    .line 136
    sub-int/2addr v2, v1

    .line 137
    int-to-float v2, v2

    .line 138
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 139
    .line 140
    add-int/2addr v3, p0

    .line 141
    int-to-float v3, v3

    .line 142
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 143
    .line 144
    .line 145
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 146
    .line 147
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 148
    .line 149
    sub-int/2addr v2, v1

    .line 150
    int-to-float v2, v2

    .line 151
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 152
    .line 153
    sub-int/2addr v3, p0

    .line 154
    int-to-float v3, v3

    .line 155
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 156
    .line 157
    .line 158
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 159
    .line 160
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 161
    .line 162
    int-to-float v2, v2

    .line 163
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 164
    .line 165
    sub-int/2addr v3, p0

    .line 166
    int-to-float v3, v3

    .line 167
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 168
    .line 169
    .line 170
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 171
    .line 172
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 173
    .line 174
    .line 175
    move-result v2

    .line 176
    int-to-float v2, v2

    .line 177
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 178
    .line 179
    int-to-float v3, v3

    .line 180
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 181
    .line 182
    .line 183
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 184
    .line 185
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 186
    .line 187
    int-to-float v2, v2

    .line 188
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 189
    .line 190
    sub-int/2addr v3, p0

    .line 191
    int-to-float v3, v3

    .line 192
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 193
    .line 194
    .line 195
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 196
    .line 197
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 198
    .line 199
    add-int/2addr v2, v1

    .line 200
    int-to-float v2, v2

    .line 201
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 202
    .line 203
    sub-int/2addr v3, p0

    .line 204
    int-to-float v3, v3

    .line 205
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 206
    .line 207
    .line 208
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 209
    .line 210
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 211
    .line 212
    add-int/2addr v2, v1

    .line 213
    int-to-float v1, v2

    .line 214
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 215
    .line 216
    add-int/2addr v2, p0

    .line 217
    int-to-float v2, v2

    .line 218
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 219
    .line 220
    .line 221
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 222
    .line 223
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 224
    .line 225
    int-to-float v1, v1

    .line 226
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 227
    .line 228
    add-int/2addr p1, p0

    .line 229
    int-to-float p0, p1

    .line 230
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 231
    .line 232
    .line 233
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 234
    .line 235
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 236
    .line 237
    .line 238
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 239
    .line 240
    return-object p0
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getUturnArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 8

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 10
    .line 11
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 12
    .line 13
    int-to-float v2, v2

    .line 14
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 15
    .line 16
    int-to-float v3, v3

    .line 17
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 18
    .line 19
    .line 20
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 21
    .line 22
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 23
    .line 24
    int-to-float v2, v2

    .line 25
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 26
    .line 27
    int-to-float v3, v3

    .line 28
    int-to-float v0, v0

    .line 29
    const v4, 0x3ec28f5c    # 0.38f

    .line 30
    .line 31
    .line 32
    mul-float v4, v4, v0

    .line 33
    .line 34
    add-float/2addr v3, v4

    .line 35
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 36
    .line 37
    .line 38
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 39
    .line 40
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 41
    .line 42
    int-to-float v2, v2

    .line 43
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 44
    .line 45
    int-to-float v5, v3

    .line 46
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 47
    .line 48
    int-to-float v6, v6

    .line 49
    int-to-float p0, p0

    .line 50
    const v7, 0x3e0f5c29    # 0.14f

    .line 51
    .line 52
    .line 53
    mul-float v7, v7, p0

    .line 54
    .line 55
    sub-float/2addr v6, v7

    .line 56
    int-to-float v3, v3

    .line 57
    const v7, 0x3f428f5c    # 0.76f

    .line 58
    .line 59
    .line 60
    mul-float v7, v7, v0

    .line 61
    .line 62
    add-float/2addr v3, v7

    .line 63
    invoke-virtual {v1, v2, v5, v6, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 64
    .line 65
    .line 66
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 67
    .line 68
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 69
    .line 70
    const/high16 v3, 0x43340000    # 180.0f

    .line 71
    .line 72
    invoke-virtual {v1, v2, v3, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 73
    .line 74
    .line 75
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 76
    .line 77
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 78
    .line 79
    int-to-float v2, v2

    .line 80
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 81
    .line 82
    int-to-float v3, v3

    .line 83
    add-float/2addr v3, v4

    .line 84
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 85
    .line 86
    .line 87
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 88
    .line 89
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 90
    .line 91
    int-to-float v2, v2

    .line 92
    const v3, 0x3e8f5c29    # 0.28f

    .line 93
    .line 94
    .line 95
    mul-float v5, p0, v3

    .line 96
    .line 97
    sub-float/2addr v2, v5

    .line 98
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 99
    .line 100
    int-to-float v6, v6

    .line 101
    const v7, 0x3f28f5c3    # 0.66f

    .line 102
    .line 103
    .line 104
    mul-float v7, v7, v0

    .line 105
    .line 106
    add-float/2addr v6, v7

    .line 107
    invoke-virtual {v1, v2, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 108
    .line 109
    .line 110
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 111
    .line 112
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 113
    .line 114
    int-to-float v2, v2

    .line 115
    const v6, 0x3f0f5c29    # 0.56f

    .line 116
    .line 117
    .line 118
    mul-float v6, v6, p0

    .line 119
    .line 120
    sub-float/2addr v2, v6

    .line 121
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 122
    .line 123
    int-to-float v6, v6

    .line 124
    add-float/2addr v6, v4

    .line 125
    invoke-virtual {v1, v2, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 126
    .line 127
    .line 128
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 129
    .line 130
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 131
    .line 132
    int-to-float v2, v2

    .line 133
    const v6, 0x3ed70a3e    # 0.42000002f

    .line 134
    .line 135
    .line 136
    mul-float p0, p0, v6

    .line 137
    .line 138
    sub-float/2addr v2, p0

    .line 139
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 140
    .line 141
    int-to-float v6, v6

    .line 142
    add-float/2addr v6, v4

    .line 143
    invoke-virtual {v1, v2, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 144
    .line 145
    .line 146
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 147
    .line 148
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 149
    .line 150
    int-to-float v2, v2

    .line 151
    add-float/2addr v2, v5

    .line 152
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 153
    .line 154
    int-to-float v6, v4

    .line 155
    mul-float v3, v3, v0

    .line 156
    .line 157
    add-float/2addr v6, v3

    .line 158
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 159
    .line 160
    int-to-float v3, v3

    .line 161
    sub-float/2addr v3, p0

    .line 162
    int-to-float p0, v4

    .line 163
    const v4, 0x3ef5c28f    # 0.48f

    .line 164
    .line 165
    .line 166
    mul-float v0, v0, v4

    .line 167
    .line 168
    add-float/2addr p0, v0

    .line 169
    invoke-virtual {v1, v2, v6, v3, p0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 170
    .line 171
    .line 172
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 173
    .line 174
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 175
    .line 176
    const/4 v1, 0x0

    .line 177
    const/high16 v2, -0x3ccc0000    # -180.0f

    .line 178
    .line 179
    invoke-virtual {p0, v0, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 180
    .line 181
    .line 182
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 183
    .line 184
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 185
    .line 186
    int-to-float v0, v0

    .line 187
    add-float/2addr v0, v5

    .line 188
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 189
    .line 190
    int-to-float p1, p1

    .line 191
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 192
    .line 193
    .line 194
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 195
    .line 196
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 197
    .line 198
    .line 199
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/EarlyArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 200
    .line 201
    return-object p0
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

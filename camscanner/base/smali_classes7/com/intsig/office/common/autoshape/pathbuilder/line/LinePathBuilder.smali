.class public Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;
.super Ljava/lang/Object;
.source "LinePathBuilder.java"


# static fields
.field private static paths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static getBentConnectorPath2(Lcom/intsig/office/common/shape/LineShape;Landroid/graphics/Rect;F)Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/LineShape;",
            "Landroid/graphics/Rect;",
            "F)",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    const/4 v8, 0x1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    array-length v2, v1

    .line 14
    if-lt v2, v8, :cond_0

    .line 15
    .line 16
    const/4 v2, 0x0

    .line 17
    aget-object v3, v1, v2

    .line 18
    .line 19
    if-eqz v3, :cond_0

    .line 20
    .line 21
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 22
    .line 23
    .line 24
    aget-object v1, v1, v2

    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 27
    .line 28
    .line 29
    :cond_0
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 30
    .line 31
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 32
    .line 33
    .line 34
    new-instance v2, Landroid/graphics/Path;

    .line 35
    .line 36
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 37
    .line 38
    .line 39
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 40
    .line 41
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 42
    .line 43
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 44
    .line 45
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    .line 46
    .line 47
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrowhead()Z

    .line 48
    .line 49
    .line 50
    move-result v7

    .line 51
    const/4 v9, 0x2

    .line 52
    const/high16 v10, 0x3f400000    # 0.75f

    .line 53
    .line 54
    if-eqz v7, :cond_2

    .line 55
    .line 56
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 57
    .line 58
    .line 59
    move-result-object v7

    .line 60
    invoke-virtual {v7}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 61
    .line 62
    .line 63
    move-result v7

    .line 64
    if-eq v7, v8, :cond_1

    .line 65
    .line 66
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 67
    .line 68
    .line 69
    move-result-object v7

    .line 70
    invoke-virtual {v7}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 71
    .line 72
    .line 73
    move-result v7

    .line 74
    if-ne v7, v9, :cond_2

    .line 75
    .line 76
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 77
    .line 78
    .line 79
    move-result-object v7

    .line 80
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 81
    .line 82
    .line 83
    move-result-object v11

    .line 84
    invoke-virtual {v11}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 85
    .line 86
    .line 87
    move-result v11

    .line 88
    invoke-static {v7, v11}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getArrowLength(Lcom/intsig/office/common/shape/Arrow;I)I

    .line 89
    .line 90
    .line 91
    move-result v7

    .line 92
    int-to-double v11, v3

    .line 93
    int-to-float v7, v7

    .line 94
    mul-float v7, v7, p2

    .line 95
    .line 96
    sub-int v3, v5, v3

    .line 97
    .line 98
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    .line 99
    .line 100
    .line 101
    move-result v13

    .line 102
    int-to-float v13, v13

    .line 103
    div-float/2addr v7, v13

    .line 104
    int-to-float v3, v3

    .line 105
    mul-float v7, v7, v3

    .line 106
    .line 107
    mul-float v7, v7, v10

    .line 108
    .line 109
    float-to-double v13, v7

    .line 110
    invoke-static {v13, v14}, Ljava/lang/Math;->ceil(D)D

    .line 111
    .line 112
    .line 113
    move-result-wide v13

    .line 114
    add-double/2addr v11, v13

    .line 115
    double-to-int v3, v11

    .line 116
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowhead()Z

    .line 117
    .line 118
    .line 119
    move-result v7

    .line 120
    if-eqz v7, :cond_4

    .line 121
    .line 122
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 123
    .line 124
    .line 125
    move-result-object v7

    .line 126
    invoke-virtual {v7}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 127
    .line 128
    .line 129
    move-result v7

    .line 130
    if-eq v7, v8, :cond_3

    .line 131
    .line 132
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 133
    .line 134
    .line 135
    move-result-object v7

    .line 136
    invoke-virtual {v7}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 137
    .line 138
    .line 139
    move-result v7

    .line 140
    if-ne v7, v9, :cond_4

    .line 141
    .line 142
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 143
    .line 144
    .line 145
    move-result-object v7

    .line 146
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 147
    .line 148
    .line 149
    move-result-object v9

    .line 150
    invoke-virtual {v9}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 151
    .line 152
    .line 153
    move-result v9

    .line 154
    invoke-static {v7, v9}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getArrowLength(Lcom/intsig/office/common/shape/Arrow;I)I

    .line 155
    .line 156
    .line 157
    move-result v7

    .line 158
    int-to-double v11, v6

    .line 159
    int-to-float v7, v7

    .line 160
    mul-float v7, v7, p2

    .line 161
    .line 162
    sub-int v9, v6, v4

    .line 163
    .line 164
    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    .line 165
    .line 166
    .line 167
    move-result v9

    .line 168
    int-to-float v9, v9

    .line 169
    div-float/2addr v7, v9

    .line 170
    sub-int v6, v4, v6

    .line 171
    .line 172
    int-to-float v6, v6

    .line 173
    mul-float v7, v7, v6

    .line 174
    .line 175
    mul-float v7, v7, v10

    .line 176
    .line 177
    float-to-double v6, v7

    .line 178
    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    .line 179
    .line 180
    .line 181
    move-result-wide v6

    .line 182
    add-double/2addr v11, v6

    .line 183
    double-to-int v6, v11

    .line 184
    :cond_4
    int-to-float v9, v3

    .line 185
    int-to-float v3, v4

    .line 186
    invoke-virtual {v2, v9, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 187
    .line 188
    .line 189
    iget v3, v0, Landroid/graphics/Rect;->right:I

    .line 190
    .line 191
    int-to-float v3, v3

    .line 192
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 193
    .line 194
    int-to-float v4, v4

    .line 195
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 196
    .line 197
    .line 198
    int-to-float v3, v5

    .line 199
    int-to-float v4, v6

    .line 200
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 201
    .line 202
    .line 203
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 204
    .line 205
    .line 206
    move-result-object v3

    .line 207
    if-nez v3, :cond_5

    .line 208
    .line 209
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 210
    .line 211
    .line 212
    move-result-object v3

    .line 213
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 214
    .line 215
    .line 216
    move-result-object v3

    .line 217
    :cond_5
    move-object v10, v3

    .line 218
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 219
    .line 220
    .line 221
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 222
    .line 223
    .line 224
    move-result-object v2

    .line 225
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 226
    .line 227
    .line 228
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 229
    .line 230
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    .line 232
    .line 233
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowhead()Z

    .line 234
    .line 235
    .line 236
    move-result v1

    .line 237
    const/4 v11, 0x5

    .line 238
    if-eqz v1, :cond_7

    .line 239
    .line 240
    new-instance v12, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 241
    .line 242
    invoke-direct {v12}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 243
    .line 244
    .line 245
    invoke-virtual {v12, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 246
    .line 247
    .line 248
    iget v1, v0, Landroid/graphics/Rect;->right:I

    .line 249
    .line 250
    int-to-float v2, v1

    .line 251
    int-to-float v3, v1

    .line 252
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 253
    .line 254
    int-to-float v5, v1

    .line 255
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 256
    .line 257
    .line 258
    move-result-object v6

    .line 259
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 260
    .line 261
    .line 262
    move-result-object v1

    .line 263
    invoke-virtual {v1}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 264
    .line 265
    .line 266
    move-result v7

    .line 267
    move v1, v2

    .line 268
    move v2, v4

    .line 269
    move v4, v5

    .line 270
    move-object v5, v6

    .line 271
    move v6, v7

    .line 272
    move/from16 v7, p2

    .line 273
    .line 274
    invoke-static/range {v1 .. v7}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;IF)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 275
    .line 276
    .line 277
    move-result-object v1

    .line 278
    invoke-virtual {v1}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 279
    .line 280
    .line 281
    move-result-object v1

    .line 282
    invoke-virtual {v12, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 283
    .line 284
    .line 285
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 286
    .line 287
    .line 288
    move-result-object v1

    .line 289
    invoke-virtual {v1}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 290
    .line 291
    .line 292
    move-result v1

    .line 293
    if-eq v1, v11, :cond_6

    .line 294
    .line 295
    invoke-virtual {v12, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 296
    .line 297
    .line 298
    goto :goto_0

    .line 299
    :cond_6
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 300
    .line 301
    .line 302
    move-result-object v1

    .line 303
    invoke-virtual {v12, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 304
    .line 305
    .line 306
    :goto_0
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 307
    .line 308
    invoke-interface {v1, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 309
    .line 310
    .line 311
    :cond_7
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrowhead()Z

    .line 312
    .line 313
    .line 314
    move-result v1

    .line 315
    if-eqz v1, :cond_9

    .line 316
    .line 317
    new-instance v7, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 318
    .line 319
    invoke-direct {v7}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 320
    .line 321
    .line 322
    invoke-virtual {v7, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 323
    .line 324
    .line 325
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 326
    .line 327
    int-to-float v2, v1

    .line 328
    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 329
    .line 330
    int-to-float v3, v0

    .line 331
    int-to-float v4, v1

    .line 332
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 333
    .line 334
    .line 335
    move-result-object v5

    .line 336
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 337
    .line 338
    .line 339
    move-result-object v0

    .line 340
    invoke-virtual {v0}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 341
    .line 342
    .line 343
    move-result v6

    .line 344
    move v0, v9

    .line 345
    move v1, v2

    .line 346
    move v2, v3

    .line 347
    move v3, v4

    .line 348
    move-object v4, v5

    .line 349
    move v5, v6

    .line 350
    move/from16 v6, p2

    .line 351
    .line 352
    invoke-static/range {v0 .. v6}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;IF)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 353
    .line 354
    .line 355
    move-result-object v0

    .line 356
    invoke-virtual {v0}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 357
    .line 358
    .line 359
    move-result-object v0

    .line 360
    invoke-virtual {v7, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 361
    .line 362
    .line 363
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 364
    .line 365
    .line 366
    move-result-object v0

    .line 367
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 368
    .line 369
    .line 370
    move-result v0

    .line 371
    if-eq v0, v11, :cond_8

    .line 372
    .line 373
    invoke-virtual {v7, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 374
    .line 375
    .line 376
    goto :goto_1

    .line 377
    :cond_8
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 378
    .line 379
    .line 380
    move-result-object v0

    .line 381
    invoke-virtual {v7, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 382
    .line 383
    .line 384
    :goto_1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 385
    .line 386
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 387
    .line 388
    .line 389
    :cond_9
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 390
    .line 391
    return-object v0
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private static getBentConnectorPath3(Lcom/intsig/office/common/shape/LineShape;Landroid/graphics/Rect;F)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/LineShape;",
            "Landroid/graphics/Rect;",
            "F)",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    int-to-float v1, v1

    .line 8
    const/high16 v2, 0x3f000000    # 0.5f

    .line 9
    .line 10
    mul-float v1, v1, v2

    .line 11
    .line 12
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    const/4 v8, 0x1

    .line 17
    if-eqz v2, :cond_0

    .line 18
    .line 19
    array-length v3, v2

    .line 20
    if-lt v3, v8, :cond_0

    .line 21
    .line 22
    const/4 v3, 0x0

    .line 23
    aget-object v4, v2, v3

    .line 24
    .line 25
    if-eqz v4, :cond_0

    .line 26
    .line 27
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    int-to-float v1, v1

    .line 32
    aget-object v2, v2, v3

    .line 33
    .line 34
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    mul-float v1, v1, v2

    .line 39
    .line 40
    :cond_0
    move v9, v1

    .line 41
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 42
    .line 43
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 44
    .line 45
    .line 46
    new-instance v2, Landroid/graphics/Path;

    .line 47
    .line 48
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 49
    .line 50
    .line 51
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 52
    .line 53
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 54
    .line 55
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 56
    .line 57
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    .line 58
    .line 59
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrowhead()Z

    .line 60
    .line 61
    .line 62
    move-result v7

    .line 63
    const/4 v10, 0x2

    .line 64
    const/high16 v11, 0x3f400000    # 0.75f

    .line 65
    .line 66
    if-eqz v7, :cond_2

    .line 67
    .line 68
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 69
    .line 70
    .line 71
    move-result-object v7

    .line 72
    invoke-virtual {v7}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 73
    .line 74
    .line 75
    move-result v7

    .line 76
    if-eq v7, v8, :cond_1

    .line 77
    .line 78
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 79
    .line 80
    .line 81
    move-result-object v7

    .line 82
    invoke-virtual {v7}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 83
    .line 84
    .line 85
    move-result v7

    .line 86
    if-ne v7, v10, :cond_2

    .line 87
    .line 88
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 89
    .line 90
    .line 91
    move-result-object v7

    .line 92
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 93
    .line 94
    .line 95
    move-result-object v12

    .line 96
    invoke-virtual {v12}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 97
    .line 98
    .line 99
    move-result v12

    .line 100
    invoke-static {v7, v12}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getArrowLength(Lcom/intsig/office/common/shape/Arrow;I)I

    .line 101
    .line 102
    .line 103
    move-result v7

    .line 104
    int-to-double v12, v3

    .line 105
    int-to-float v7, v7

    .line 106
    mul-float v7, v7, p2

    .line 107
    .line 108
    sub-int v3, v5, v3

    .line 109
    .line 110
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    .line 111
    .line 112
    .line 113
    move-result v14

    .line 114
    int-to-float v14, v14

    .line 115
    div-float/2addr v7, v14

    .line 116
    int-to-float v3, v3

    .line 117
    mul-float v7, v7, v3

    .line 118
    .line 119
    mul-float v7, v7, v11

    .line 120
    .line 121
    float-to-double v14, v7

    .line 122
    invoke-static {v14, v15}, Ljava/lang/Math;->ceil(D)D

    .line 123
    .line 124
    .line 125
    move-result-wide v14

    .line 126
    add-double/2addr v12, v14

    .line 127
    double-to-int v3, v12

    .line 128
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowhead()Z

    .line 129
    .line 130
    .line 131
    move-result v7

    .line 132
    if-eqz v7, :cond_4

    .line 133
    .line 134
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 135
    .line 136
    .line 137
    move-result-object v7

    .line 138
    invoke-virtual {v7}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 139
    .line 140
    .line 141
    move-result v7

    .line 142
    if-eq v7, v8, :cond_3

    .line 143
    .line 144
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 145
    .line 146
    .line 147
    move-result-object v7

    .line 148
    invoke-virtual {v7}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 149
    .line 150
    .line 151
    move-result v7

    .line 152
    if-ne v7, v10, :cond_4

    .line 153
    .line 154
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 155
    .line 156
    .line 157
    move-result-object v7

    .line 158
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 159
    .line 160
    .line 161
    move-result-object v10

    .line 162
    invoke-virtual {v10}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 163
    .line 164
    .line 165
    move-result v10

    .line 166
    invoke-static {v7, v10}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getArrowLength(Lcom/intsig/office/common/shape/Arrow;I)I

    .line 167
    .line 168
    .line 169
    move-result v7

    .line 170
    int-to-double v12, v5

    .line 171
    int-to-float v7, v7

    .line 172
    mul-float v7, v7, p2

    .line 173
    .line 174
    sub-int v10, v5, v3

    .line 175
    .line 176
    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    .line 177
    .line 178
    .line 179
    move-result v10

    .line 180
    int-to-float v10, v10

    .line 181
    div-float/2addr v7, v10

    .line 182
    sub-int v5, v3, v5

    .line 183
    .line 184
    int-to-float v5, v5

    .line 185
    mul-float v7, v7, v5

    .line 186
    .line 187
    mul-float v7, v7, v11

    .line 188
    .line 189
    float-to-double v10, v7

    .line 190
    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    .line 191
    .line 192
    .line 193
    move-result-wide v10

    .line 194
    add-double/2addr v12, v10

    .line 195
    double-to-int v5, v12

    .line 196
    :cond_4
    int-to-float v3, v3

    .line 197
    int-to-float v4, v4

    .line 198
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 199
    .line 200
    .line 201
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 202
    .line 203
    int-to-float v3, v3

    .line 204
    add-float/2addr v3, v9

    .line 205
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 206
    .line 207
    int-to-float v4, v4

    .line 208
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 209
    .line 210
    .line 211
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 212
    .line 213
    int-to-float v3, v3

    .line 214
    add-float/2addr v3, v9

    .line 215
    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    .line 216
    .line 217
    int-to-float v4, v4

    .line 218
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 219
    .line 220
    .line 221
    int-to-float v3, v5

    .line 222
    int-to-float v4, v6

    .line 223
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 224
    .line 225
    .line 226
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 227
    .line 228
    .line 229
    move-result-object v3

    .line 230
    if-nez v3, :cond_5

    .line 231
    .line 232
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 233
    .line 234
    .line 235
    move-result-object v3

    .line 236
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 237
    .line 238
    .line 239
    move-result-object v3

    .line 240
    :cond_5
    move-object v10, v3

    .line 241
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 242
    .line 243
    .line 244
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 245
    .line 246
    .line 247
    move-result-object v2

    .line 248
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 249
    .line 250
    .line 251
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 252
    .line 253
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    .line 255
    .line 256
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowhead()Z

    .line 257
    .line 258
    .line 259
    move-result v1

    .line 260
    const/4 v11, 0x5

    .line 261
    if-eqz v1, :cond_7

    .line 262
    .line 263
    new-instance v12, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 264
    .line 265
    invoke-direct {v12}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 266
    .line 267
    .line 268
    invoke-virtual {v12, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 269
    .line 270
    .line 271
    iget v1, v0, Landroid/graphics/Rect;->left:I

    .line 272
    .line 273
    int-to-float v1, v1

    .line 274
    add-float/2addr v1, v9

    .line 275
    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 276
    .line 277
    int-to-float v3, v2

    .line 278
    iget v4, v0, Landroid/graphics/Rect;->right:I

    .line 279
    .line 280
    int-to-float v4, v4

    .line 281
    int-to-float v5, v2

    .line 282
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 283
    .line 284
    .line 285
    move-result-object v6

    .line 286
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 287
    .line 288
    .line 289
    move-result-object v2

    .line 290
    invoke-virtual {v2}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 291
    .line 292
    .line 293
    move-result v7

    .line 294
    move v2, v3

    .line 295
    move v3, v4

    .line 296
    move v4, v5

    .line 297
    move-object v5, v6

    .line 298
    move v6, v7

    .line 299
    move/from16 v7, p2

    .line 300
    .line 301
    invoke-static/range {v1 .. v7}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;IF)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 302
    .line 303
    .line 304
    move-result-object v1

    .line 305
    invoke-virtual {v1}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 306
    .line 307
    .line 308
    move-result-object v1

    .line 309
    invoke-virtual {v12, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 310
    .line 311
    .line 312
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 313
    .line 314
    .line 315
    move-result-object v1

    .line 316
    invoke-virtual {v1}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 317
    .line 318
    .line 319
    move-result v1

    .line 320
    if-eq v1, v11, :cond_6

    .line 321
    .line 322
    invoke-virtual {v12, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 323
    .line 324
    .line 325
    goto :goto_0

    .line 326
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 327
    .line 328
    .line 329
    move-result-object v1

    .line 330
    invoke-virtual {v12, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 331
    .line 332
    .line 333
    :goto_0
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 334
    .line 335
    invoke-interface {v1, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336
    .line 337
    .line 338
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrowhead()Z

    .line 339
    .line 340
    .line 341
    move-result v1

    .line 342
    if-eqz v1, :cond_9

    .line 343
    .line 344
    new-instance v7, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 345
    .line 346
    invoke-direct {v7}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 347
    .line 348
    .line 349
    invoke-virtual {v7, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 350
    .line 351
    .line 352
    iget v1, v0, Landroid/graphics/Rect;->left:I

    .line 353
    .line 354
    int-to-float v2, v1

    .line 355
    add-float/2addr v2, v9

    .line 356
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 357
    .line 358
    int-to-float v3, v0

    .line 359
    int-to-float v4, v1

    .line 360
    int-to-float v5, v0

    .line 361
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 362
    .line 363
    .line 364
    move-result-object v6

    .line 365
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 366
    .line 367
    .line 368
    move-result-object v0

    .line 369
    invoke-virtual {v0}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 370
    .line 371
    .line 372
    move-result v8

    .line 373
    move v0, v2

    .line 374
    move v1, v3

    .line 375
    move v2, v4

    .line 376
    move v3, v5

    .line 377
    move-object v4, v6

    .line 378
    move v5, v8

    .line 379
    move/from16 v6, p2

    .line 380
    .line 381
    invoke-static/range {v0 .. v6}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;IF)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 382
    .line 383
    .line 384
    move-result-object v0

    .line 385
    invoke-virtual {v0}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 386
    .line 387
    .line 388
    move-result-object v0

    .line 389
    invoke-virtual {v7, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 390
    .line 391
    .line 392
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 393
    .line 394
    .line 395
    move-result-object v0

    .line 396
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 397
    .line 398
    .line 399
    move-result v0

    .line 400
    if-eq v0, v11, :cond_8

    .line 401
    .line 402
    invoke-virtual {v7, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 403
    .line 404
    .line 405
    goto :goto_1

    .line 406
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 407
    .line 408
    .line 409
    move-result-object v0

    .line 410
    invoke-virtual {v7, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 411
    .line 412
    .line 413
    :goto_1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 414
    .line 415
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416
    .line 417
    .line 418
    :cond_9
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 419
    .line 420
    return-object v0
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private static getCurvedConnector2Path(Lcom/intsig/office/common/shape/LineShape;Landroid/graphics/Rect;F)Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/LineShape;",
            "Landroid/graphics/Rect;",
            "F)",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 4
    .line 5
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 6
    .line 7
    .line 8
    new-instance v2, Landroid/graphics/Path;

    .line 9
    .line 10
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 14
    .line 15
    .line 16
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 17
    .line 18
    int-to-float v3, v3

    .line 19
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 20
    .line 21
    int-to-float v4, v4

    .line 22
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 23
    .line 24
    .line 25
    iget v3, v0, Landroid/graphics/Rect;->right:I

    .line 26
    .line 27
    int-to-float v4, v3

    .line 28
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 29
    .line 30
    int-to-float v5, v5

    .line 31
    int-to-float v3, v3

    .line 32
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    .line 33
    .line 34
    int-to-float v6, v6

    .line 35
    invoke-virtual {v2, v4, v5, v3, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 39
    .line 40
    .line 41
    move-result-object v3

    .line 42
    if-nez v3, :cond_0

    .line 43
    .line 44
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 49
    .line 50
    .line 51
    move-result-object v3

    .line 52
    :cond_0
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 60
    .line 61
    .line 62
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 63
    .line 64
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    .line 66
    .line 67
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowhead()Z

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    const/4 v2, 0x5

    .line 72
    const/4 v4, 0x1

    .line 73
    if-eqz v1, :cond_2

    .line 74
    .line 75
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 76
    .line 77
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 78
    .line 79
    .line 80
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 81
    .line 82
    .line 83
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 84
    .line 85
    int-to-float v6, v5

    .line 86
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 87
    .line 88
    int-to-float v7, v5

    .line 89
    iget v8, v0, Landroid/graphics/Rect;->right:I

    .line 90
    .line 91
    int-to-float v9, v8

    .line 92
    int-to-float v5, v5

    .line 93
    int-to-float v10, v8

    .line 94
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    .line 95
    .line 96
    int-to-float v11, v8

    .line 97
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 98
    .line 99
    .line 100
    move-result-object v12

    .line 101
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 102
    .line 103
    .line 104
    move-result-object v8

    .line 105
    invoke-virtual {v8}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 106
    .line 107
    .line 108
    move-result v13

    .line 109
    move v8, v9

    .line 110
    move v9, v5

    .line 111
    move/from16 v14, p2

    .line 112
    .line 113
    invoke-static/range {v6 .. v14}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getQuadBezArrowPath(FFFFFFLcom/intsig/office/common/shape/Arrow;IF)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 114
    .line 115
    .line 116
    move-result-object v5

    .line 117
    invoke-virtual {v5}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 118
    .line 119
    .line 120
    move-result-object v5

    .line 121
    invoke-virtual {v1, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 122
    .line 123
    .line 124
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 125
    .line 126
    .line 127
    move-result-object v5

    .line 128
    invoke-virtual {v5}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 129
    .line 130
    .line 131
    move-result v5

    .line 132
    if-eq v5, v2, :cond_1

    .line 133
    .line 134
    invoke-virtual {v1, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 135
    .line 136
    .line 137
    goto :goto_0

    .line 138
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 139
    .line 140
    .line 141
    move-result-object v5

    .line 142
    invoke-virtual {v1, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 143
    .line 144
    .line 145
    :goto_0
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 146
    .line 147
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    .line 149
    .line 150
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrowhead()Z

    .line 151
    .line 152
    .line 153
    move-result v1

    .line 154
    if-eqz v1, :cond_4

    .line 155
    .line 156
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 157
    .line 158
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 159
    .line 160
    .line 161
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 162
    .line 163
    .line 164
    iget v4, v0, Landroid/graphics/Rect;->right:I

    .line 165
    .line 166
    int-to-float v5, v4

    .line 167
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    .line 168
    .line 169
    int-to-float v6, v6

    .line 170
    int-to-float v7, v4

    .line 171
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 172
    .line 173
    int-to-float v8, v4

    .line 174
    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 175
    .line 176
    int-to-float v9, v0

    .line 177
    int-to-float v10, v4

    .line 178
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 179
    .line 180
    .line 181
    move-result-object v11

    .line 182
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 183
    .line 184
    .line 185
    move-result-object v0

    .line 186
    invoke-virtual {v0}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 187
    .line 188
    .line 189
    move-result v12

    .line 190
    move/from16 v13, p2

    .line 191
    .line 192
    invoke-static/range {v5 .. v13}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getQuadBezArrowPath(FFFFFFLcom/intsig/office/common/shape/Arrow;IF)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 193
    .line 194
    .line 195
    move-result-object v0

    .line 196
    invoke-virtual {v0}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 197
    .line 198
    .line 199
    move-result-object v0

    .line 200
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 201
    .line 202
    .line 203
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 204
    .line 205
    .line 206
    move-result-object v0

    .line 207
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 208
    .line 209
    .line 210
    move-result v0

    .line 211
    if-eq v0, v2, :cond_3

    .line 212
    .line 213
    invoke-virtual {v1, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 214
    .line 215
    .line 216
    goto :goto_1

    .line 217
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 218
    .line 219
    .line 220
    move-result-object v0

    .line 221
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 222
    .line 223
    .line 224
    :goto_1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 225
    .line 226
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    .line 228
    .line 229
    :cond_4
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 230
    .line 231
    return-object v0
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private static getCurvedConnector3Path(Lcom/intsig/office/common/shape/LineShape;Landroid/graphics/Rect;F)Ljava/util/List;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/LineShape;",
            "Landroid/graphics/Rect;",
            "F)",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    int-to-float v1, v1

    .line 8
    const/high16 v2, 0x3f000000    # 0.5f

    .line 9
    .line 10
    mul-float v1, v1, v2

    .line 11
    .line 12
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    const/4 v3, 0x1

    .line 17
    if-eqz v2, :cond_0

    .line 18
    .line 19
    array-length v4, v2

    .line 20
    if-lt v4, v3, :cond_0

    .line 21
    .line 22
    const/4 v4, 0x0

    .line 23
    aget-object v5, v2, v4

    .line 24
    .line 25
    if-eqz v5, :cond_0

    .line 26
    .line 27
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    int-to-float v1, v1

    .line 32
    aget-object v2, v2, v4

    .line 33
    .line 34
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    mul-float v1, v1, v2

    .line 39
    .line 40
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 41
    .line 42
    .line 43
    move-result-object v2

    .line 44
    if-nez v2, :cond_1

    .line 45
    .line 46
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    invoke-virtual {v2}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 51
    .line 52
    .line 53
    move-result-object v2

    .line 54
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowhead()Z

    .line 55
    .line 56
    .line 57
    move-result v4

    .line 58
    const/4 v5, 0x5

    .line 59
    const/4 v6, 0x2

    .line 60
    const/4 v7, 0x0

    .line 61
    if-eqz v4, :cond_5

    .line 62
    .line 63
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 64
    .line 65
    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 66
    .line 67
    .line 68
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 69
    .line 70
    .line 71
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 72
    .line 73
    int-to-float v8, v8

    .line 74
    add-float v9, v8, v1

    .line 75
    .line 76
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 77
    .line 78
    .line 79
    move-result v10

    .line 80
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 81
    .line 82
    int-to-float v8, v8

    .line 83
    add-float v11, v8, v1

    .line 84
    .line 85
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    .line 86
    .line 87
    int-to-float v12, v8

    .line 88
    iget v13, v0, Landroid/graphics/Rect;->right:I

    .line 89
    .line 90
    int-to-float v13, v13

    .line 91
    int-to-float v14, v8

    .line 92
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 93
    .line 94
    .line 95
    move-result-object v15

    .line 96
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 97
    .line 98
    .line 99
    move-result-object v8

    .line 100
    invoke-virtual {v8}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 101
    .line 102
    .line 103
    move-result v16

    .line 104
    move/from16 v17, p2

    .line 105
    .line 106
    invoke-static/range {v9 .. v17}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getQuadBezArrowPath(FFFFFFLcom/intsig/office/common/shape/Arrow;IF)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 107
    .line 108
    .line 109
    move-result-object v8

    .line 110
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 111
    .line 112
    .line 113
    move-result-object v9

    .line 114
    invoke-virtual {v9}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 115
    .line 116
    .line 117
    move-result v9

    .line 118
    if-eq v9, v3, :cond_3

    .line 119
    .line 120
    if-ne v9, v6, :cond_2

    .line 121
    .line 122
    goto :goto_0

    .line 123
    :cond_2
    move-object v10, v7

    .line 124
    goto :goto_1

    .line 125
    :cond_3
    :goto_0
    invoke-virtual {v8}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 126
    .line 127
    .line 128
    move-result-object v10

    .line 129
    :goto_1
    invoke-virtual {v8}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 130
    .line 131
    .line 132
    move-result-object v8

    .line 133
    invoke-virtual {v4, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 134
    .line 135
    .line 136
    if-eq v9, v5, :cond_4

    .line 137
    .line 138
    invoke-virtual {v4, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 139
    .line 140
    .line 141
    goto :goto_2

    .line 142
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 143
    .line 144
    .line 145
    move-result-object v8

    .line 146
    invoke-virtual {v4, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 147
    .line 148
    .line 149
    :goto_2
    sget-object v8, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 150
    .line 151
    invoke-interface {v8, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    .line 153
    .line 154
    goto :goto_3

    .line 155
    :cond_5
    move-object v10, v7

    .line 156
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrowhead()Z

    .line 157
    .line 158
    .line 159
    move-result v4

    .line 160
    if-eqz v4, :cond_9

    .line 161
    .line 162
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 163
    .line 164
    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 165
    .line 166
    .line 167
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 168
    .line 169
    .line 170
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 171
    .line 172
    int-to-float v8, v8

    .line 173
    add-float v11, v8, v1

    .line 174
    .line 175
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 176
    .line 177
    .line 178
    move-result v12

    .line 179
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 180
    .line 181
    int-to-float v9, v8

    .line 182
    add-float v13, v9, v1

    .line 183
    .line 184
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 185
    .line 186
    int-to-float v14, v9

    .line 187
    int-to-float v15, v8

    .line 188
    int-to-float v8, v9

    .line 189
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 190
    .line 191
    .line 192
    move-result-object v17

    .line 193
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 194
    .line 195
    .line 196
    move-result-object v9

    .line 197
    invoke-virtual {v9}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 198
    .line 199
    .line 200
    move-result v18

    .line 201
    move/from16 v16, v8

    .line 202
    .line 203
    move/from16 v19, p2

    .line 204
    .line 205
    invoke-static/range {v11 .. v19}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getQuadBezArrowPath(FFFFFFLcom/intsig/office/common/shape/Arrow;IF)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 206
    .line 207
    .line 208
    move-result-object v8

    .line 209
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 210
    .line 211
    .line 212
    move-result-object v9

    .line 213
    invoke-virtual {v9}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 214
    .line 215
    .line 216
    move-result v9

    .line 217
    if-eq v9, v3, :cond_6

    .line 218
    .line 219
    if-ne v9, v6, :cond_7

    .line 220
    .line 221
    :cond_6
    invoke-virtual {v8}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 222
    .line 223
    .line 224
    move-result-object v7

    .line 225
    :cond_7
    invoke-virtual {v8}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 226
    .line 227
    .line 228
    move-result-object v3

    .line 229
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 230
    .line 231
    .line 232
    if-eq v9, v5, :cond_8

    .line 233
    .line 234
    invoke-virtual {v4, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 235
    .line 236
    .line 237
    goto :goto_4

    .line 238
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 239
    .line 240
    .line 241
    move-result-object v2

    .line 242
    invoke-virtual {v4, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 243
    .line 244
    .line 245
    :goto_4
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 246
    .line 247
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    .line 249
    .line 250
    :cond_9
    new-instance v2, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 251
    .line 252
    invoke-direct {v2}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 253
    .line 254
    .line 255
    new-instance v3, Landroid/graphics/Path;

    .line 256
    .line 257
    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 258
    .line 259
    .line 260
    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    .line 261
    .line 262
    .line 263
    if-eqz v7, :cond_a

    .line 264
    .line 265
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 266
    .line 267
    int-to-float v4, v4

    .line 268
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 269
    .line 270
    int-to-float v5, v5

    .line 271
    iget v6, v7, Landroid/graphics/PointF;->x:F

    .line 272
    .line 273
    iget v7, v7, Landroid/graphics/PointF;->y:F

    .line 274
    .line 275
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 276
    .line 277
    .line 278
    move-result-object v8

    .line 279
    invoke-virtual {v8}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 280
    .line 281
    .line 282
    move-result v8

    .line 283
    invoke-static {v4, v5, v6, v7, v8}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    .line 284
    .line 285
    .line 286
    move-result-object v4

    .line 287
    iget v5, v4, Landroid/graphics/PointF;->x:F

    .line 288
    .line 289
    iget v4, v4, Landroid/graphics/PointF;->y:F

    .line 290
    .line 291
    invoke-virtual {v3, v5, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 292
    .line 293
    .line 294
    goto :goto_5

    .line 295
    :cond_a
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 296
    .line 297
    int-to-float v4, v4

    .line 298
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 299
    .line 300
    int-to-float v5, v5

    .line 301
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 302
    .line 303
    .line 304
    :goto_5
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 305
    .line 306
    int-to-float v5, v4

    .line 307
    add-float/2addr v5, v1

    .line 308
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 309
    .line 310
    int-to-float v6, v6

    .line 311
    int-to-float v4, v4

    .line 312
    add-float/2addr v4, v1

    .line 313
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 314
    .line 315
    .line 316
    move-result v7

    .line 317
    invoke-virtual {v3, v5, v6, v4, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 318
    .line 319
    .line 320
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 321
    .line 322
    int-to-float v4, v4

    .line 323
    add-float/2addr v4, v1

    .line 324
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 325
    .line 326
    .line 327
    move-result v5

    .line 328
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 329
    .line 330
    .line 331
    if-eqz v10, :cond_b

    .line 332
    .line 333
    iget v4, v0, Landroid/graphics/Rect;->right:I

    .line 334
    .line 335
    int-to-float v4, v4

    .line 336
    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 337
    .line 338
    int-to-float v5, v5

    .line 339
    iget v6, v10, Landroid/graphics/PointF;->x:F

    .line 340
    .line 341
    iget v7, v10, Landroid/graphics/PointF;->y:F

    .line 342
    .line 343
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 344
    .line 345
    .line 346
    move-result-object v8

    .line 347
    invoke-virtual {v8}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 348
    .line 349
    .line 350
    move-result v8

    .line 351
    invoke-static {v4, v5, v6, v7, v8}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    .line 352
    .line 353
    .line 354
    move-result-object v4

    .line 355
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 356
    .line 357
    int-to-float v5, v5

    .line 358
    add-float/2addr v5, v1

    .line 359
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 360
    .line 361
    int-to-float v0, v0

    .line 362
    iget v1, v4, Landroid/graphics/PointF;->x:F

    .line 363
    .line 364
    iget v4, v4, Landroid/graphics/PointF;->y:F

    .line 365
    .line 366
    invoke-virtual {v3, v5, v0, v1, v4}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 367
    .line 368
    .line 369
    goto :goto_6

    .line 370
    :cond_b
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 371
    .line 372
    int-to-float v4, v4

    .line 373
    add-float/2addr v4, v1

    .line 374
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 375
    .line 376
    int-to-float v5, v1

    .line 377
    iget v0, v0, Landroid/graphics/Rect;->right:I

    .line 378
    .line 379
    int-to-float v0, v0

    .line 380
    int-to-float v1, v1

    .line 381
    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 382
    .line 383
    .line 384
    :goto_6
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 385
    .line 386
    .line 387
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 388
    .line 389
    .line 390
    move-result-object v0

    .line 391
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 392
    .line 393
    .line 394
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 395
    .line 396
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 397
    .line 398
    .line 399
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 400
    .line 401
    return-object v0
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private static getCurvedConnector4Path(Lcom/intsig/office/common/shape/LineShape;Landroid/graphics/Rect;F)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/LineShape;",
            "Landroid/graphics/Rect;",
            "F)",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    int-to-float v1, v1

    .line 8
    const/high16 v2, 0x3f000000    # 0.5f

    .line 9
    .line 10
    mul-float v1, v1, v2

    .line 11
    .line 12
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 13
    .line 14
    .line 15
    move-result v3

    .line 16
    int-to-float v3, v3

    .line 17
    mul-float v3, v3, v2

    .line 18
    .line 19
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    const/4 v4, 0x1

    .line 24
    if-eqz v2, :cond_1

    .line 25
    .line 26
    array-length v5, v2

    .line 27
    if-lt v5, v4, :cond_1

    .line 28
    .line 29
    const/4 v5, 0x0

    .line 30
    aget-object v6, v2, v5

    .line 31
    .line 32
    if-eqz v6, :cond_0

    .line 33
    .line 34
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    int-to-float v1, v1

    .line 39
    aget-object v5, v2, v5

    .line 40
    .line 41
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 42
    .line 43
    .line 44
    move-result v5

    .line 45
    mul-float v1, v1, v5

    .line 46
    .line 47
    :cond_0
    aget-object v5, v2, v4

    .line 48
    .line 49
    if-eqz v5, :cond_1

    .line 50
    .line 51
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 52
    .line 53
    .line 54
    move-result v3

    .line 55
    int-to-float v3, v3

    .line 56
    aget-object v2, v2, v4

    .line 57
    .line 58
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 59
    .line 60
    .line 61
    move-result v2

    .line 62
    mul-float v3, v3, v2

    .line 63
    .line 64
    :cond_1
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 65
    .line 66
    int-to-float v2, v2

    .line 67
    add-float v7, v2, v1

    .line 68
    .line 69
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 70
    .line 71
    int-to-float v2, v1

    .line 72
    const/high16 v5, 0x40000000    # 2.0f

    .line 73
    .line 74
    div-float v6, v3, v5

    .line 75
    .line 76
    add-float/2addr v6, v2

    .line 77
    iget v2, v0, Landroid/graphics/Rect;->right:I

    .line 78
    .line 79
    int-to-float v2, v2

    .line 80
    add-float/2addr v2, v7

    .line 81
    div-float v8, v2, v5

    .line 82
    .line 83
    int-to-float v1, v1

    .line 84
    add-float v11, v1, v3

    .line 85
    .line 86
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 87
    .line 88
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 89
    .line 90
    .line 91
    new-instance v2, Landroid/graphics/Path;

    .line 92
    .line 93
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 94
    .line 95
    .line 96
    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 97
    .line 98
    .line 99
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 100
    .line 101
    int-to-float v3, v3

    .line 102
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 103
    .line 104
    int-to-float v5, v5

    .line 105
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 106
    .line 107
    .line 108
    iget v3, v0, Landroid/graphics/Rect;->top:I

    .line 109
    .line 110
    int-to-float v3, v3

    .line 111
    invoke-virtual {v2, v7, v3, v7, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 112
    .line 113
    .line 114
    invoke-virtual {v2, v7, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {v2, v7, v11, v8, v11}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 118
    .line 119
    .line 120
    invoke-virtual {v2, v8, v11}, Landroid/graphics/Path;->moveTo(FF)V

    .line 121
    .line 122
    .line 123
    iget v3, v0, Landroid/graphics/Rect;->right:I

    .line 124
    .line 125
    int-to-float v5, v3

    .line 126
    int-to-float v3, v3

    .line 127
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 128
    .line 129
    int-to-float v9, v9

    .line 130
    invoke-virtual {v2, v5, v11, v3, v9}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 131
    .line 132
    .line 133
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 134
    .line 135
    .line 136
    move-result-object v3

    .line 137
    if-nez v3, :cond_2

    .line 138
    .line 139
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 140
    .line 141
    .line 142
    move-result-object v3

    .line 143
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 144
    .line 145
    .line 146
    move-result-object v3

    .line 147
    :cond_2
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 148
    .line 149
    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 151
    .line 152
    .line 153
    move-result-object v2

    .line 154
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 155
    .line 156
    .line 157
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 158
    .line 159
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    .line 161
    .line 162
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowhead()Z

    .line 163
    .line 164
    .line 165
    move-result v1

    .line 166
    const/4 v2, 0x5

    .line 167
    if-eqz v1, :cond_4

    .line 168
    .line 169
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 170
    .line 171
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 172
    .line 173
    .line 174
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 175
    .line 176
    .line 177
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 178
    .line 179
    int-to-float v10, v5

    .line 180
    int-to-float v12, v5

    .line 181
    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 182
    .line 183
    int-to-float v13, v5

    .line 184
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 185
    .line 186
    .line 187
    move-result-object v14

    .line 188
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 189
    .line 190
    .line 191
    move-result-object v5

    .line 192
    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 193
    .line 194
    .line 195
    move-result v15

    .line 196
    move v9, v11

    .line 197
    move/from16 v16, p2

    .line 198
    .line 199
    invoke-static/range {v8 .. v16}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getQuadBezArrowPath(FFFFFFLcom/intsig/office/common/shape/Arrow;IF)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 200
    .line 201
    .line 202
    move-result-object v5

    .line 203
    invoke-virtual {v5}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 204
    .line 205
    .line 206
    move-result-object v5

    .line 207
    invoke-virtual {v1, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 208
    .line 209
    .line 210
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 211
    .line 212
    .line 213
    move-result-object v5

    .line 214
    invoke-virtual {v5}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 215
    .line 216
    .line 217
    move-result v5

    .line 218
    if-eq v5, v2, :cond_3

    .line 219
    .line 220
    invoke-virtual {v1, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 221
    .line 222
    .line 223
    goto :goto_0

    .line 224
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 225
    .line 226
    .line 227
    move-result-object v5

    .line 228
    invoke-virtual {v1, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 229
    .line 230
    .line 231
    :goto_0
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 232
    .line 233
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 234
    .line 235
    .line 236
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrowhead()Z

    .line 237
    .line 238
    .line 239
    move-result v1

    .line 240
    if-eqz v1, :cond_6

    .line 241
    .line 242
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 243
    .line 244
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 245
    .line 246
    .line 247
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 248
    .line 249
    .line 250
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 251
    .line 252
    int-to-float v8, v4

    .line 253
    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 254
    .line 255
    int-to-float v9, v0

    .line 256
    int-to-float v10, v4

    .line 257
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 258
    .line 259
    .line 260
    move-result-object v11

    .line 261
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 262
    .line 263
    .line 264
    move-result-object v0

    .line 265
    invoke-virtual {v0}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 266
    .line 267
    .line 268
    move-result v12

    .line 269
    move v5, v7

    .line 270
    move/from16 v13, p2

    .line 271
    .line 272
    invoke-static/range {v5 .. v13}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getQuadBezArrowPath(FFFFFFLcom/intsig/office/common/shape/Arrow;IF)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 273
    .line 274
    .line 275
    move-result-object v0

    .line 276
    invoke-virtual {v0}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 277
    .line 278
    .line 279
    move-result-object v0

    .line 280
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 281
    .line 282
    .line 283
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 284
    .line 285
    .line 286
    move-result-object v0

    .line 287
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 288
    .line 289
    .line 290
    move-result v0

    .line 291
    if-eq v0, v2, :cond_5

    .line 292
    .line 293
    invoke-virtual {v1, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 294
    .line 295
    .line 296
    goto :goto_1

    .line 297
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 298
    .line 299
    .line 300
    move-result-object v0

    .line 301
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 302
    .line 303
    .line 304
    :goto_1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 305
    .line 306
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    .line 308
    .line 309
    :cond_6
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 310
    .line 311
    return-object v0
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method public static getLinePath(Lcom/intsig/office/common/shape/LineShape;Landroid/graphics/Rect;F)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/LineShape;",
            "Landroid/graphics/Rect;",
            "F)",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/16 v1, 0x14

    .line 11
    .line 12
    if-eq v0, v1, :cond_0

    .line 13
    .line 14
    packed-switch v0, :pswitch_data_0

    .line 15
    .line 16
    .line 17
    packed-switch v0, :pswitch_data_1

    .line 18
    .line 19
    .line 20
    const/4 p0, 0x0

    .line 21
    return-object p0

    .line 22
    :pswitch_0
    invoke-static {p0, p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->getCurvedConnector4Path(Lcom/intsig/office/common/shape/LineShape;Landroid/graphics/Rect;F)Ljava/util/List;

    .line 23
    .line 24
    .line 25
    move-result-object p0

    .line 26
    return-object p0

    .line 27
    :pswitch_1
    invoke-static {p0, p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->getCurvedConnector4Path(Lcom/intsig/office/common/shape/LineShape;Landroid/graphics/Rect;F)Ljava/util/List;

    .line 28
    .line 29
    .line 30
    move-result-object p0

    .line 31
    return-object p0

    .line 32
    :pswitch_2
    invoke-static {p0, p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->getCurvedConnector3Path(Lcom/intsig/office/common/shape/LineShape;Landroid/graphics/Rect;F)Ljava/util/List;

    .line 33
    .line 34
    .line 35
    move-result-object p0

    .line 36
    return-object p0

    .line 37
    :pswitch_3
    invoke-static {p0, p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->getCurvedConnector2Path(Lcom/intsig/office/common/shape/LineShape;Landroid/graphics/Rect;F)Ljava/util/List;

    .line 38
    .line 39
    .line 40
    move-result-object p0

    .line 41
    return-object p0

    .line 42
    :pswitch_4
    invoke-static {p0, p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->getBentConnectorPath3(Lcom/intsig/office/common/shape/LineShape;Landroid/graphics/Rect;F)Ljava/util/List;

    .line 43
    .line 44
    .line 45
    move-result-object p0

    .line 46
    return-object p0

    .line 47
    :pswitch_5
    invoke-static {p0, p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->getBentConnectorPath2(Lcom/intsig/office/common/shape/LineShape;Landroid/graphics/Rect;F)Ljava/util/List;

    .line 48
    .line 49
    .line 50
    move-result-object p0

    .line 51
    return-object p0

    .line 52
    :cond_0
    :pswitch_6
    invoke-static {p0, p1, p2}, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->getStraightConnectorPath(Lcom/intsig/office/common/shape/LineShape;Landroid/graphics/Rect;F)Ljava/util/List;

    .line 53
    .line 54
    .line 55
    move-result-object p0

    .line 56
    return-object p0

    .line 57
    :pswitch_data_0
    .packed-switch 0x20
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    :pswitch_data_1
    .packed-switch 0x25
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private static getStraightConnectorPath(Lcom/intsig/office/common/shape/LineShape;Landroid/graphics/Rect;F)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/LineShape;",
            "Landroid/graphics/Rect;",
            "F)",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 4
    .line 5
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 6
    .line 7
    .line 8
    new-instance v2, Landroid/graphics/Path;

    .line 9
    .line 10
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 11
    .line 12
    .line 13
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 14
    .line 15
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 16
    .line 17
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 18
    .line 19
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    .line 20
    .line 21
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 22
    .line 23
    .line 24
    move-result v7

    .line 25
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 26
    .line 27
    .line 28
    move-result v8

    .line 29
    mul-int v7, v7, v8

    .line 30
    .line 31
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 32
    .line 33
    .line 34
    move-result v8

    .line 35
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 36
    .line 37
    .line 38
    move-result v9

    .line 39
    mul-int v8, v8, v9

    .line 40
    .line 41
    add-int/2addr v7, v8

    .line 42
    int-to-double v7, v7

    .line 43
    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    .line 44
    .line 45
    .line 46
    move-result-wide v7

    .line 47
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrowhead()Z

    .line 48
    .line 49
    .line 50
    move-result v9

    .line 51
    const/4 v10, 0x2

    .line 52
    const/4 v13, 0x1

    .line 53
    if-eqz v9, :cond_2

    .line 54
    .line 55
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 56
    .line 57
    .line 58
    move-result-object v9

    .line 59
    invoke-virtual {v9}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 60
    .line 61
    .line 62
    move-result v9

    .line 63
    if-eq v9, v13, :cond_0

    .line 64
    .line 65
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 66
    .line 67
    .line 68
    move-result-object v9

    .line 69
    invoke-virtual {v9}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 70
    .line 71
    .line 72
    move-result v9

    .line 73
    if-ne v9, v10, :cond_2

    .line 74
    .line 75
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 76
    .line 77
    .line 78
    move-result-object v9

    .line 79
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 80
    .line 81
    .line 82
    move-result-object v14

    .line 83
    invoke-virtual {v14}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 84
    .line 85
    .line 86
    move-result v14

    .line 87
    invoke-static {v9, v14}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getArrowLength(Lcom/intsig/office/common/shape/Arrow;I)I

    .line 88
    .line 89
    .line 90
    move-result v9

    .line 91
    sub-int v14, v5, v3

    .line 92
    .line 93
    invoke-static {v14}, Ljava/lang/Math;->abs(I)I

    .line 94
    .line 95
    .line 96
    move-result v15

    .line 97
    if-lt v15, v13, :cond_1

    .line 98
    .line 99
    int-to-double v10, v3

    .line 100
    int-to-float v3, v9

    .line 101
    mul-float v3, v3, p2

    .line 102
    .line 103
    move-object v12, v1

    .line 104
    float-to-double v0, v3

    .line 105
    div-double/2addr v0, v7

    .line 106
    int-to-double v13, v14

    .line 107
    mul-double v0, v0, v13

    .line 108
    .line 109
    const-wide/high16 v13, 0x3fe8000000000000L    # 0.75

    .line 110
    .line 111
    mul-double v0, v0, v13

    .line 112
    .line 113
    add-double/2addr v10, v0

    .line 114
    double-to-int v3, v10

    .line 115
    goto :goto_0

    .line 116
    :cond_1
    move-object v12, v1

    .line 117
    :goto_0
    sub-int v0, v6, v4

    .line 118
    .line 119
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    .line 120
    .line 121
    .line 122
    move-result v1

    .line 123
    const/4 v10, 0x1

    .line 124
    if-lt v1, v10, :cond_3

    .line 125
    .line 126
    int-to-double v10, v4

    .line 127
    int-to-float v1, v9

    .line 128
    mul-float v1, v1, p2

    .line 129
    .line 130
    float-to-double v13, v1

    .line 131
    div-double/2addr v13, v7

    .line 132
    int-to-double v0, v0

    .line 133
    mul-double v13, v13, v0

    .line 134
    .line 135
    const-wide/high16 v0, 0x3fe8000000000000L    # 0.75

    .line 136
    .line 137
    mul-double v13, v13, v0

    .line 138
    .line 139
    add-double/2addr v10, v13

    .line 140
    double-to-int v4, v10

    .line 141
    goto :goto_1

    .line 142
    :cond_2
    move-object v12, v1

    .line 143
    :cond_3
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowhead()Z

    .line 144
    .line 145
    .line 146
    move-result v0

    .line 147
    if-eqz v0, :cond_6

    .line 148
    .line 149
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 150
    .line 151
    .line 152
    move-result-object v0

    .line 153
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 154
    .line 155
    .line 156
    move-result v0

    .line 157
    const/4 v1, 0x1

    .line 158
    if-eq v0, v1, :cond_4

    .line 159
    .line 160
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 161
    .line 162
    .line 163
    move-result-object v0

    .line 164
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 165
    .line 166
    .line 167
    move-result v0

    .line 168
    const/4 v1, 0x2

    .line 169
    if-ne v0, v1, :cond_6

    .line 170
    .line 171
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 172
    .line 173
    .line 174
    move-result-object v0

    .line 175
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 176
    .line 177
    .line 178
    move-result-object v1

    .line 179
    invoke-virtual {v1}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 180
    .line 181
    .line 182
    move-result v1

    .line 183
    invoke-static {v0, v1}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getArrowLength(Lcom/intsig/office/common/shape/Arrow;I)I

    .line 184
    .line 185
    .line 186
    move-result v0

    .line 187
    sub-int v1, v5, v3

    .line 188
    .line 189
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    .line 190
    .line 191
    .line 192
    move-result v1

    .line 193
    const/4 v9, 0x1

    .line 194
    if-lt v1, v9, :cond_5

    .line 195
    .line 196
    int-to-double v9, v5

    .line 197
    int-to-float v1, v0

    .line 198
    mul-float v1, v1, p2

    .line 199
    .line 200
    float-to-double v13, v1

    .line 201
    div-double/2addr v13, v7

    .line 202
    sub-int v1, v3, v5

    .line 203
    .line 204
    move-object v15, v12

    .line 205
    int-to-double v11, v1

    .line 206
    mul-double v13, v13, v11

    .line 207
    .line 208
    const-wide/high16 v11, 0x3fe8000000000000L    # 0.75

    .line 209
    .line 210
    mul-double v13, v13, v11

    .line 211
    .line 212
    add-double/2addr v9, v13

    .line 213
    double-to-int v5, v9

    .line 214
    goto :goto_2

    .line 215
    :cond_5
    move-object v15, v12

    .line 216
    :goto_2
    sub-int v1, v6, v4

    .line 217
    .line 218
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    .line 219
    .line 220
    .line 221
    move-result v1

    .line 222
    const/4 v9, 0x1

    .line 223
    if-lt v1, v9, :cond_7

    .line 224
    .line 225
    int-to-double v9, v6

    .line 226
    int-to-float v0, v0

    .line 227
    mul-float v0, v0, p2

    .line 228
    .line 229
    float-to-double v0, v0

    .line 230
    div-double/2addr v0, v7

    .line 231
    sub-int v6, v4, v6

    .line 232
    .line 233
    int-to-double v6, v6

    .line 234
    mul-double v0, v0, v6

    .line 235
    .line 236
    const-wide/high16 v6, 0x3fe8000000000000L    # 0.75

    .line 237
    .line 238
    mul-double v0, v0, v6

    .line 239
    .line 240
    add-double/2addr v9, v0

    .line 241
    double-to-int v6, v9

    .line 242
    goto :goto_3

    .line 243
    :cond_6
    move-object v15, v12

    .line 244
    :cond_7
    :goto_3
    int-to-float v0, v3

    .line 245
    int-to-float v1, v4

    .line 246
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 247
    .line 248
    .line 249
    int-to-float v0, v5

    .line 250
    int-to-float v1, v6

    .line 251
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 252
    .line 253
    .line 254
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 255
    .line 256
    .line 257
    move-result-object v0

    .line 258
    if-nez v0, :cond_8

    .line 259
    .line 260
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 261
    .line 262
    .line 263
    move-result-object v0

    .line 264
    invoke-virtual {v0}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 265
    .line 266
    .line 267
    move-result-object v0

    .line 268
    :cond_8
    move-object v8, v0

    .line 269
    move-object v0, v15

    .line 270
    invoke-virtual {v0, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 271
    .line 272
    .line 273
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 274
    .line 275
    .line 276
    move-result-object v1

    .line 277
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 278
    .line 279
    .line 280
    invoke-virtual {v0, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 281
    .line 282
    .line 283
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 284
    .line 285
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 286
    .line 287
    .line 288
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowhead()Z

    .line 289
    .line 290
    .line 291
    move-result v0

    .line 292
    const/4 v9, 0x5

    .line 293
    if-eqz v0, :cond_a

    .line 294
    .line 295
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 296
    .line 297
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 298
    .line 299
    .line 300
    const/4 v1, 0x1

    .line 301
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 302
    .line 303
    .line 304
    move-object/from16 v10, p1

    .line 305
    .line 306
    iget v1, v10, Landroid/graphics/Rect;->left:I

    .line 307
    .line 308
    int-to-float v1, v1

    .line 309
    iget v2, v10, Landroid/graphics/Rect;->top:I

    .line 310
    .line 311
    int-to-float v2, v2

    .line 312
    iget v3, v10, Landroid/graphics/Rect;->right:I

    .line 313
    .line 314
    int-to-float v3, v3

    .line 315
    iget v4, v10, Landroid/graphics/Rect;->bottom:I

    .line 316
    .line 317
    int-to-float v4, v4

    .line 318
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 319
    .line 320
    .line 321
    move-result-object v5

    .line 322
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 323
    .line 324
    .line 325
    move-result-object v6

    .line 326
    invoke-virtual {v6}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 327
    .line 328
    .line 329
    move-result v6

    .line 330
    move/from16 v7, p2

    .line 331
    .line 332
    invoke-static/range {v1 .. v7}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;IF)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 333
    .line 334
    .line 335
    move-result-object v1

    .line 336
    invoke-virtual {v1}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 337
    .line 338
    .line 339
    move-result-object v1

    .line 340
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 341
    .line 342
    .line 343
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 344
    .line 345
    .line 346
    move-result-object v1

    .line 347
    invoke-virtual {v1}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 348
    .line 349
    .line 350
    move-result v1

    .line 351
    if-eq v1, v9, :cond_9

    .line 352
    .line 353
    invoke-virtual {v0, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 354
    .line 355
    .line 356
    goto :goto_4

    .line 357
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 358
    .line 359
    .line 360
    move-result-object v1

    .line 361
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 362
    .line 363
    .line 364
    :goto_4
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 365
    .line 366
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    .line 368
    .line 369
    goto :goto_5

    .line 370
    :cond_a
    move-object/from16 v10, p1

    .line 371
    .line 372
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrowhead()Z

    .line 373
    .line 374
    .line 375
    move-result v0

    .line 376
    if-eqz v0, :cond_c

    .line 377
    .line 378
    new-instance v7, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 379
    .line 380
    invoke-direct {v7}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 381
    .line 382
    .line 383
    const/4 v0, 0x1

    .line 384
    invoke-virtual {v7, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 385
    .line 386
    .line 387
    iget v0, v10, Landroid/graphics/Rect;->right:I

    .line 388
    .line 389
    int-to-float v0, v0

    .line 390
    iget v1, v10, Landroid/graphics/Rect;->bottom:I

    .line 391
    .line 392
    int-to-float v1, v1

    .line 393
    iget v2, v10, Landroid/graphics/Rect;->left:I

    .line 394
    .line 395
    int-to-float v2, v2

    .line 396
    iget v3, v10, Landroid/graphics/Rect;->top:I

    .line 397
    .line 398
    int-to-float v3, v3

    .line 399
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 400
    .line 401
    .line 402
    move-result-object v4

    .line 403
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 404
    .line 405
    .line 406
    move-result-object v5

    .line 407
    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 408
    .line 409
    .line 410
    move-result v5

    .line 411
    move/from16 v6, p2

    .line 412
    .line 413
    invoke-static/range {v0 .. v6}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;IF)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 414
    .line 415
    .line 416
    move-result-object v0

    .line 417
    invoke-virtual {v0}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 418
    .line 419
    .line 420
    move-result-object v0

    .line 421
    invoke-virtual {v7, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 422
    .line 423
    .line 424
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 425
    .line 426
    .line 427
    move-result-object v0

    .line 428
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 429
    .line 430
    .line 431
    move-result v0

    .line 432
    if-eq v0, v9, :cond_b

    .line 433
    .line 434
    invoke-virtual {v7, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 435
    .line 436
    .line 437
    goto :goto_6

    .line 438
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 439
    .line 440
    .line 441
    move-result-object v0

    .line 442
    invoke-virtual {v7, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 443
    .line 444
    .line 445
    :goto_6
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 446
    .line 447
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 448
    .line 449
    .line 450
    :cond_c
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/line/LinePathBuilder;->paths:Ljava/util/List;

    .line 451
    .line 452
    return-object v0
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

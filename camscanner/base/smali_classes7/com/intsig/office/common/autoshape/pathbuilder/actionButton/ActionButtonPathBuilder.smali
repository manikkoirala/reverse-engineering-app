.class public Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;
.super Ljava/lang/Object;
.source "ActionButtonPathBuilder.java"


# static fields
.field private static final PICTURECOLOR:I = -0x70bbbbbc

.field private static final PICTURECOLOR_DARK:I = -0x70323233

.field private static final PICTURECOLOR_LIGHT:I = -0x70aaaaab

.field private static final TINT_DARK:F = -0.5f

.field private static final TINT_LIGHT1:F = -0.3f

.field private static final TINT_LIGHT2:F = 0.6f

.field private static pathExList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation
.end field

.field private static tempRect:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 7
    .line 8
    new-instance v0, Ljava/util/ArrayList;

    .line 9
    .line 10
    const/4 v1, 0x2

    .line 11
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 12
    .line 13
    .line 14
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static getActionButtonExtendPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    packed-switch v0, :pswitch_data_0

    .line 11
    .line 12
    .line 13
    const/4 p0, 0x0

    .line 14
    return-object p0

    .line 15
    :pswitch_0
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->getMoviePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 16
    .line 17
    .line 18
    move-result-object p0

    .line 19
    return-object p0

    .line 20
    :pswitch_1
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->getSoundPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 21
    .line 22
    .line 23
    move-result-object p0

    .line 24
    return-object p0

    .line 25
    :pswitch_2
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->getDocumentPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 26
    .line 27
    .line 28
    move-result-object p0

    .line 29
    return-object p0

    .line 30
    :pswitch_3
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->getReturnPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 31
    .line 32
    .line 33
    move-result-object p0

    .line 34
    return-object p0

    .line 35
    :pswitch_4
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->getBeginningPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 36
    .line 37
    .line 38
    move-result-object p0

    .line 39
    return-object p0

    .line 40
    :pswitch_5
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->getEndPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 41
    .line 42
    .line 43
    move-result-object p0

    .line 44
    return-object p0

    .line 45
    :pswitch_6
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->getBackPreviousPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 46
    .line 47
    .line 48
    move-result-object p0

    .line 49
    return-object p0

    .line 50
    :pswitch_7
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->getForwardNextPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 51
    .line 52
    .line 53
    move-result-object p0

    .line 54
    return-object p0

    .line 55
    :pswitch_8
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->getInformationPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 56
    .line 57
    .line 58
    move-result-object p0

    .line 59
    return-object p0

    .line 60
    :pswitch_9
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->getHelpPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 61
    .line 62
    .line 63
    move-result-object p0

    .line 64
    return-object p0

    .line 65
    :pswitch_a
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->getHomePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 66
    .line 67
    .line 68
    move-result-object p0

    .line 69
    return-object p0

    .line 70
    :pswitch_b
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->getBlankPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 71
    .line 72
    .line 73
    move-result-object p0

    .line 74
    return-object p0

    .line 75
    :pswitch_data_0
    .packed-switch 0xbd
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getBackPreviousPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 9
    .line 10
    .line 11
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 12
    .line 13
    invoke-virtual {v2, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 17
    .line 18
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 19
    .line 20
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 31
    .line 32
    .line 33
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 34
    .line 35
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 39
    .line 40
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 41
    .line 42
    .line 43
    new-instance v1, Landroid/graphics/Path;

    .line 44
    .line 45
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    int-to-float v2, v2

    .line 61
    const/high16 v3, 0x3f400000    # 0.75f

    .line 62
    .line 63
    mul-float v2, v2, v3

    .line 64
    .line 65
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 66
    .line 67
    .line 68
    move-result v2

    .line 69
    const-wide/high16 v3, 0x4008000000000000L    # 3.0

    .line 70
    .line 71
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    .line 72
    .line 73
    .line 74
    move-result-wide v3

    .line 75
    const-wide/high16 v5, 0x4010000000000000L    # 4.0

    .line 76
    .line 77
    div-double/2addr v3, v5

    .line 78
    int-to-double v5, v2

    .line 79
    mul-double v3, v3, v5

    .line 80
    .line 81
    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    .line 82
    .line 83
    .line 84
    move-result-wide v3

    .line 85
    long-to-float v3, v3

    .line 86
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 87
    .line 88
    .line 89
    move-result v4

    .line 90
    int-to-float v4, v4

    .line 91
    sub-float/2addr v4, v3

    .line 92
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 93
    .line 94
    .line 95
    move-result v5

    .line 96
    int-to-float v5, v5

    .line 97
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 101
    .line 102
    .line 103
    move-result v4

    .line 104
    int-to-float v4, v4

    .line 105
    add-float/2addr v4, v3

    .line 106
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 107
    .line 108
    .line 109
    move-result v5

    .line 110
    div-int/lit8 v2, v2, 0x2

    .line 111
    .line 112
    sub-int/2addr v5, v2

    .line 113
    int-to-float v5, v5

    .line 114
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 118
    .line 119
    .line 120
    move-result v4

    .line 121
    int-to-float v4, v4

    .line 122
    add-float/2addr v4, v3

    .line 123
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 124
    .line 125
    .line 126
    move-result p1

    .line 127
    add-int/2addr p1, v2

    .line 128
    int-to-float p1, p1

    .line 129
    invoke-virtual {v1, v4, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 130
    .line 131
    .line 132
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 133
    .line 134
    .line 135
    new-instance p1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 136
    .line 137
    invoke-direct {p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 138
    .line 139
    .line 140
    const/4 v2, 0x0

    .line 141
    invoke-virtual {p1, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 142
    .line 143
    .line 144
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 145
    .line 146
    .line 147
    move-result-object p0

    .line 148
    if-eqz p0, :cond_0

    .line 149
    .line 150
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 151
    .line 152
    .line 153
    move-result v2

    .line 154
    if-nez v2, :cond_0

    .line 155
    .line 156
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 157
    .line 158
    .line 159
    move-result-object v2

    .line 160
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 161
    .line 162
    .line 163
    move-result p0

    .line 164
    const-wide/high16 v3, -0x4020000000000000L    # -0.5

    .line 165
    .line 166
    invoke-virtual {v2, p0, v3, v4}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 167
    .line 168
    .line 169
    move-result p0

    .line 170
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 171
    .line 172
    .line 173
    goto :goto_0

    .line 174
    :cond_0
    const p0, -0x70bbbbbc

    .line 175
    .line 176
    .line 177
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 178
    .line 179
    .line 180
    :goto_0
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 181
    .line 182
    .line 183
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 184
    .line 185
    .line 186
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 187
    .line 188
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    .line 190
    .line 191
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 192
    .line 193
    return-object p0
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getBeginningPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 9
    .line 10
    .line 11
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 12
    .line 13
    invoke-virtual {v2, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 17
    .line 18
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 19
    .line 20
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 31
    .line 32
    .line 33
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 34
    .line 35
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 39
    .line 40
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 41
    .line 42
    .line 43
    new-instance v7, Landroid/graphics/Path;

    .line 44
    .line 45
    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    .line 57
    .line 58
    .line 59
    move-result v8

    .line 60
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    int-to-float v1, v1

    .line 65
    int-to-float v2, v8

    .line 66
    const/high16 v3, 0x3ec00000    # 0.375f

    .line 67
    .line 68
    mul-float v9, v2, v3

    .line 69
    .line 70
    sub-float v3, v1, v9

    .line 71
    .line 72
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 73
    .line 74
    .line 75
    move-result v1

    .line 76
    int-to-float v1, v1

    .line 77
    sub-float v4, v1, v9

    .line 78
    .line 79
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 80
    .line 81
    .line 82
    move-result v1

    .line 83
    int-to-float v1, v1

    .line 84
    const v5, 0x3e8ccccd    # 0.275f

    .line 85
    .line 86
    .line 87
    mul-float v2, v2, v5

    .line 88
    .line 89
    sub-float v5, v1, v2

    .line 90
    .line 91
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 92
    .line 93
    .line 94
    move-result v1

    .line 95
    int-to-float v1, v1

    .line 96
    add-float v6, v1, v9

    .line 97
    .line 98
    sget-object v10, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 99
    .line 100
    move-object v1, v7

    .line 101
    move v2, v3

    .line 102
    move v3, v4

    .line 103
    move v4, v5

    .line 104
    move v5, v6

    .line 105
    move-object v6, v10

    .line 106
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 110
    .line 111
    .line 112
    move-result v1

    .line 113
    int-to-float v1, v1

    .line 114
    mul-int/lit8 v8, v8, 0x3

    .line 115
    .line 116
    int-to-float v2, v8

    .line 117
    const/high16 v3, 0x41800000    # 16.0f

    .line 118
    .line 119
    div-float/2addr v2, v3

    .line 120
    sub-float/2addr v1, v2

    .line 121
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 122
    .line 123
    .line 124
    move-result v2

    .line 125
    int-to-float v2, v2

    .line 126
    invoke-virtual {v7, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 127
    .line 128
    .line 129
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 130
    .line 131
    .line 132
    move-result v1

    .line 133
    int-to-float v1, v1

    .line 134
    add-float/2addr v1, v9

    .line 135
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 136
    .line 137
    .line 138
    move-result v2

    .line 139
    int-to-float v2, v2

    .line 140
    sub-float/2addr v2, v9

    .line 141
    invoke-virtual {v7, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 142
    .line 143
    .line 144
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 145
    .line 146
    .line 147
    move-result v1

    .line 148
    int-to-float v1, v1

    .line 149
    add-float/2addr v1, v9

    .line 150
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 151
    .line 152
    .line 153
    move-result p1

    .line 154
    int-to-float p1, p1

    .line 155
    add-float/2addr p1, v9

    .line 156
    invoke-virtual {v7, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 157
    .line 158
    .line 159
    invoke-virtual {v7}, Landroid/graphics/Path;->close()V

    .line 160
    .line 161
    .line 162
    invoke-virtual {v0, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 163
    .line 164
    .line 165
    new-instance p1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 166
    .line 167
    invoke-direct {p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 168
    .line 169
    .line 170
    const/4 v1, 0x0

    .line 171
    invoke-virtual {p1, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 172
    .line 173
    .line 174
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 175
    .line 176
    .line 177
    move-result-object p0

    .line 178
    if-eqz p0, :cond_0

    .line 179
    .line 180
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 181
    .line 182
    .line 183
    move-result v1

    .line 184
    if-nez v1, :cond_0

    .line 185
    .line 186
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 187
    .line 188
    .line 189
    move-result-object v1

    .line 190
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 191
    .line 192
    .line 193
    move-result p0

    .line 194
    const-wide/high16 v2, -0x4020000000000000L    # -0.5

    .line 195
    .line 196
    invoke-virtual {v1, p0, v2, v3}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 197
    .line 198
    .line 199
    move-result p0

    .line 200
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 201
    .line 202
    .line 203
    goto :goto_0

    .line 204
    :cond_0
    const p0, -0x70bbbbbc

    .line 205
    .line 206
    .line 207
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 208
    .line 209
    .line 210
    :goto_0
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 211
    .line 212
    .line 213
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 214
    .line 215
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    .line 217
    .line 218
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 219
    .line 220
    return-object p0
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getBlankPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 9
    .line 10
    .line 11
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 12
    .line 13
    invoke-virtual {v2, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 17
    .line 18
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 19
    .line 20
    invoke-virtual {v1, p1, v2}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 27
    .line 28
    .line 29
    move-result-object p0

    .line 30
    invoke-virtual {v0, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 31
    .line 32
    .line 33
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 34
    .line 35
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 39
    .line 40
    return-object p0
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static getDocumentPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 9
    .line 10
    .line 11
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 12
    .line 13
    invoke-virtual {v2, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 17
    .line 18
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 19
    .line 20
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 31
    .line 32
    .line 33
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 34
    .line 35
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 51
    .line 52
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 53
    .line 54
    .line 55
    new-instance v2, Landroid/graphics/Path;

    .line 56
    .line 57
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    int-to-float v3, v3

    .line 65
    int-to-float v0, v0

    .line 66
    const v4, 0x3e8f5c29    # 0.28f

    .line 67
    .line 68
    .line 69
    mul-float v4, v4, v0

    .line 70
    .line 71
    sub-float/2addr v3, v4

    .line 72
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 73
    .line 74
    .line 75
    move-result v5

    .line 76
    int-to-float v5, v5

    .line 77
    const v6, 0x3ec28f5c    # 0.38f

    .line 78
    .line 79
    .line 80
    mul-float v6, v6, v0

    .line 81
    .line 82
    sub-float/2addr v5, v6

    .line 83
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 87
    .line 88
    .line 89
    move-result v3

    .line 90
    int-to-float v3, v3

    .line 91
    const v5, 0x3dcccccd    # 0.1f

    .line 92
    .line 93
    .line 94
    mul-float v5, v5, v0

    .line 95
    .line 96
    add-float/2addr v3, v5

    .line 97
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 98
    .line 99
    .line 100
    move-result v7

    .line 101
    int-to-float v7, v7

    .line 102
    sub-float/2addr v7, v6

    .line 103
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 107
    .line 108
    .line 109
    move-result v3

    .line 110
    int-to-float v3, v3

    .line 111
    add-float/2addr v3, v5

    .line 112
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 113
    .line 114
    .line 115
    move-result v7

    .line 116
    int-to-float v7, v7

    .line 117
    const v8, 0x3e3851ec    # 0.18f

    .line 118
    .line 119
    .line 120
    mul-float v0, v0, v8

    .line 121
    .line 122
    sub-float/2addr v7, v0

    .line 123
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 124
    .line 125
    .line 126
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 127
    .line 128
    .line 129
    move-result v3

    .line 130
    int-to-float v3, v3

    .line 131
    add-float/2addr v3, v4

    .line 132
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 133
    .line 134
    .line 135
    move-result v7

    .line 136
    int-to-float v7, v7

    .line 137
    sub-float/2addr v7, v0

    .line 138
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 139
    .line 140
    .line 141
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 142
    .line 143
    .line 144
    move-result v3

    .line 145
    int-to-float v3, v3

    .line 146
    add-float/2addr v3, v4

    .line 147
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 148
    .line 149
    .line 150
    move-result v7

    .line 151
    int-to-float v7, v7

    .line 152
    add-float/2addr v7, v6

    .line 153
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 154
    .line 155
    .line 156
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 157
    .line 158
    .line 159
    move-result v3

    .line 160
    int-to-float v3, v3

    .line 161
    sub-float/2addr v3, v4

    .line 162
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 163
    .line 164
    .line 165
    move-result v7

    .line 166
    int-to-float v7, v7

    .line 167
    add-float/2addr v7, v6

    .line 168
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    .line 170
    .line 171
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 172
    .line 173
    .line 174
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 175
    .line 176
    .line 177
    new-instance v2, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 178
    .line 179
    invoke-direct {v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 180
    .line 181
    .line 182
    const/4 v3, 0x0

    .line 183
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 184
    .line 185
    .line 186
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 187
    .line 188
    .line 189
    move-result-object p0

    .line 190
    if-eqz p0, :cond_0

    .line 191
    .line 192
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 193
    .line 194
    .line 195
    move-result v7

    .line 196
    if-nez v7, :cond_0

    .line 197
    .line 198
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 199
    .line 200
    .line 201
    move-result-object v7

    .line 202
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 203
    .line 204
    .line 205
    move-result v8

    .line 206
    const-wide v9, -0x402cccccc0000000L    # -0.30000001192092896

    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    invoke-virtual {v7, v8, v9, v10}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 212
    .line 213
    .line 214
    move-result v7

    .line 215
    invoke-virtual {v2, v7}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 216
    .line 217
    .line 218
    goto :goto_0

    .line 219
    :cond_0
    const v7, -0x70aaaaab

    .line 220
    .line 221
    .line 222
    invoke-virtual {v2, v7}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 223
    .line 224
    .line 225
    :goto_0
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 226
    .line 227
    .line 228
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 229
    .line 230
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    .line 232
    .line 233
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 234
    .line 235
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 236
    .line 237
    .line 238
    new-instance v2, Landroid/graphics/Path;

    .line 239
    .line 240
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 241
    .line 242
    .line 243
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 244
    .line 245
    .line 246
    move-result v7

    .line 247
    int-to-float v7, v7

    .line 248
    add-float/2addr v7, v5

    .line 249
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 250
    .line 251
    .line 252
    move-result v8

    .line 253
    int-to-float v8, v8

    .line 254
    sub-float/2addr v8, v6

    .line 255
    invoke-virtual {v2, v7, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 256
    .line 257
    .line 258
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 259
    .line 260
    .line 261
    move-result v6

    .line 262
    int-to-float v6, v6

    .line 263
    add-float/2addr v6, v5

    .line 264
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 265
    .line 266
    .line 267
    move-result v5

    .line 268
    int-to-float v5, v5

    .line 269
    sub-float/2addr v5, v0

    .line 270
    invoke-virtual {v2, v6, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 271
    .line 272
    .line 273
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 274
    .line 275
    .line 276
    move-result v5

    .line 277
    int-to-float v5, v5

    .line 278
    add-float/2addr v5, v4

    .line 279
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 280
    .line 281
    .line 282
    move-result p1

    .line 283
    int-to-float p1, p1

    .line 284
    sub-float/2addr p1, v0

    .line 285
    invoke-virtual {v2, v5, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 286
    .line 287
    .line 288
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 289
    .line 290
    .line 291
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 292
    .line 293
    .line 294
    new-instance p1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 295
    .line 296
    invoke-direct {p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 297
    .line 298
    .line 299
    invoke-virtual {p1, v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 300
    .line 301
    .line 302
    if-eqz p0, :cond_1

    .line 303
    .line 304
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 305
    .line 306
    .line 307
    move-result v0

    .line 308
    if-nez v0, :cond_1

    .line 309
    .line 310
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 311
    .line 312
    .line 313
    move-result-object v0

    .line 314
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 315
    .line 316
    .line 317
    move-result p0

    .line 318
    const-wide/high16 v2, -0x4020000000000000L    # -0.5

    .line 319
    .line 320
    invoke-virtual {v0, p0, v2, v3}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 321
    .line 322
    .line 323
    move-result p0

    .line 324
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 325
    .line 326
    .line 327
    goto :goto_1

    .line 328
    :cond_1
    const p0, -0x70bbbbbc

    .line 329
    .line 330
    .line 331
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 332
    .line 333
    .line 334
    :goto_1
    invoke-virtual {v1, p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 335
    .line 336
    .line 337
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 338
    .line 339
    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 340
    .line 341
    .line 342
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 343
    .line 344
    return-object p0
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getEndPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 9
    .line 10
    .line 11
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 12
    .line 13
    invoke-virtual {v2, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 17
    .line 18
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 19
    .line 20
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 31
    .line 32
    .line 33
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 34
    .line 35
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 39
    .line 40
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 41
    .line 42
    .line 43
    new-instance v7, Landroid/graphics/Path;

    .line 44
    .line 45
    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    .line 57
    .line 58
    .line 59
    move-result v8

    .line 60
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    int-to-float v1, v1

    .line 65
    int-to-float v2, v8

    .line 66
    const v3, 0x3e8ccccd    # 0.275f

    .line 67
    .line 68
    .line 69
    mul-float v3, v3, v2

    .line 70
    .line 71
    add-float/2addr v3, v1

    .line 72
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 73
    .line 74
    .line 75
    move-result v1

    .line 76
    int-to-float v1, v1

    .line 77
    const/high16 v4, 0x3ec00000    # 0.375f

    .line 78
    .line 79
    mul-float v9, v2, v4

    .line 80
    .line 81
    sub-float v4, v1, v9

    .line 82
    .line 83
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 84
    .line 85
    .line 86
    move-result v1

    .line 87
    int-to-float v1, v1

    .line 88
    add-float v5, v1, v9

    .line 89
    .line 90
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 91
    .line 92
    .line 93
    move-result v1

    .line 94
    int-to-float v1, v1

    .line 95
    add-float v6, v1, v9

    .line 96
    .line 97
    sget-object v10, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 98
    .line 99
    move-object v1, v7

    .line 100
    move v2, v3

    .line 101
    move v3, v4

    .line 102
    move v4, v5

    .line 103
    move v5, v6

    .line 104
    move-object v6, v10

    .line 105
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 106
    .line 107
    .line 108
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 109
    .line 110
    .line 111
    move-result v1

    .line 112
    int-to-float v1, v1

    .line 113
    mul-int/lit8 v8, v8, 0x3

    .line 114
    .line 115
    int-to-float v2, v8

    .line 116
    const/high16 v3, 0x41800000    # 16.0f

    .line 117
    .line 118
    div-float/2addr v2, v3

    .line 119
    add-float/2addr v1, v2

    .line 120
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 121
    .line 122
    .line 123
    move-result v2

    .line 124
    int-to-float v2, v2

    .line 125
    invoke-virtual {v7, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 126
    .line 127
    .line 128
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 129
    .line 130
    .line 131
    move-result v1

    .line 132
    int-to-float v1, v1

    .line 133
    sub-float/2addr v1, v9

    .line 134
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 135
    .line 136
    .line 137
    move-result v2

    .line 138
    int-to-float v2, v2

    .line 139
    add-float/2addr v2, v9

    .line 140
    invoke-virtual {v7, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 141
    .line 142
    .line 143
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 144
    .line 145
    .line 146
    move-result v1

    .line 147
    int-to-float v1, v1

    .line 148
    sub-float/2addr v1, v9

    .line 149
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 150
    .line 151
    .line 152
    move-result p1

    .line 153
    int-to-float p1, p1

    .line 154
    sub-float/2addr p1, v9

    .line 155
    invoke-virtual {v7, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 156
    .line 157
    .line 158
    invoke-virtual {v7}, Landroid/graphics/Path;->close()V

    .line 159
    .line 160
    .line 161
    invoke-virtual {v0, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 162
    .line 163
    .line 164
    new-instance p1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 165
    .line 166
    invoke-direct {p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 167
    .line 168
    .line 169
    const/4 v1, 0x0

    .line 170
    invoke-virtual {p1, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 171
    .line 172
    .line 173
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 174
    .line 175
    .line 176
    move-result-object p0

    .line 177
    if-eqz p0, :cond_0

    .line 178
    .line 179
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 180
    .line 181
    .line 182
    move-result v1

    .line 183
    if-nez v1, :cond_0

    .line 184
    .line 185
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 186
    .line 187
    .line 188
    move-result-object v1

    .line 189
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 190
    .line 191
    .line 192
    move-result p0

    .line 193
    const-wide/high16 v2, -0x4020000000000000L    # -0.5

    .line 194
    .line 195
    invoke-virtual {v1, p0, v2, v3}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 196
    .line 197
    .line 198
    move-result p0

    .line 199
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 200
    .line 201
    .line 202
    goto :goto_0

    .line 203
    :cond_0
    const p0, -0x70bbbbbc

    .line 204
    .line 205
    .line 206
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 207
    .line 208
    .line 209
    :goto_0
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 210
    .line 211
    .line 212
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 213
    .line 214
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    .line 216
    .line 217
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 218
    .line 219
    return-object p0
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getForwardNextPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 9
    .line 10
    .line 11
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 12
    .line 13
    invoke-virtual {v2, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 17
    .line 18
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 19
    .line 20
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 31
    .line 32
    .line 33
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 34
    .line 35
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 39
    .line 40
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 41
    .line 42
    .line 43
    new-instance v1, Landroid/graphics/Path;

    .line 44
    .line 45
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    int-to-float v2, v2

    .line 61
    const/high16 v3, 0x3f400000    # 0.75f

    .line 62
    .line 63
    mul-float v2, v2, v3

    .line 64
    .line 65
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 66
    .line 67
    .line 68
    move-result v2

    .line 69
    const-wide/high16 v3, 0x4008000000000000L    # 3.0

    .line 70
    .line 71
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    .line 72
    .line 73
    .line 74
    move-result-wide v3

    .line 75
    const-wide/high16 v5, 0x4010000000000000L    # 4.0

    .line 76
    .line 77
    div-double/2addr v3, v5

    .line 78
    int-to-double v5, v2

    .line 79
    mul-double v3, v3, v5

    .line 80
    .line 81
    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    .line 82
    .line 83
    .line 84
    move-result-wide v3

    .line 85
    long-to-float v3, v3

    .line 86
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 87
    .line 88
    .line 89
    move-result v4

    .line 90
    int-to-float v4, v4

    .line 91
    add-float/2addr v4, v3

    .line 92
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 93
    .line 94
    .line 95
    move-result v5

    .line 96
    int-to-float v5, v5

    .line 97
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 101
    .line 102
    .line 103
    move-result v4

    .line 104
    int-to-float v4, v4

    .line 105
    sub-float/2addr v4, v3

    .line 106
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 107
    .line 108
    .line 109
    move-result v5

    .line 110
    div-int/lit8 v2, v2, 0x2

    .line 111
    .line 112
    add-int/2addr v5, v2

    .line 113
    int-to-float v5, v5

    .line 114
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 118
    .line 119
    .line 120
    move-result v4

    .line 121
    int-to-float v4, v4

    .line 122
    sub-float/2addr v4, v3

    .line 123
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 124
    .line 125
    .line 126
    move-result p1

    .line 127
    sub-int/2addr p1, v2

    .line 128
    int-to-float p1, p1

    .line 129
    invoke-virtual {v1, v4, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 130
    .line 131
    .line 132
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 133
    .line 134
    .line 135
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 136
    .line 137
    .line 138
    new-instance p1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 139
    .line 140
    invoke-direct {p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 141
    .line 142
    .line 143
    const/4 v1, 0x0

    .line 144
    invoke-virtual {p1, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 145
    .line 146
    .line 147
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 148
    .line 149
    .line 150
    move-result-object p0

    .line 151
    if-eqz p0, :cond_0

    .line 152
    .line 153
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 154
    .line 155
    .line 156
    move-result v1

    .line 157
    if-nez v1, :cond_0

    .line 158
    .line 159
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 160
    .line 161
    .line 162
    move-result-object v1

    .line 163
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 164
    .line 165
    .line 166
    move-result p0

    .line 167
    const-wide/high16 v2, -0x4020000000000000L    # -0.5

    .line 168
    .line 169
    invoke-virtual {v1, p0, v2, v3}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 170
    .line 171
    .line 172
    move-result p0

    .line 173
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 174
    .line 175
    .line 176
    goto :goto_0

    .line 177
    :cond_0
    const p0, -0x70bbbbbc

    .line 178
    .line 179
    .line 180
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 181
    .line 182
    .line 183
    :goto_0
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 184
    .line 185
    .line 186
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 187
    .line 188
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    .line 190
    .line 191
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 192
    .line 193
    return-object p0
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getHelpPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 9
    .line 10
    .line 11
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 12
    .line 13
    invoke-virtual {v2, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 17
    .line 18
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 19
    .line 20
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 31
    .line 32
    .line 33
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 34
    .line 35
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 51
    .line 52
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 53
    .line 54
    .line 55
    new-instance v2, Landroid/graphics/Path;

    .line 56
    .line 57
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    int-to-float v3, v3

    .line 65
    int-to-float v0, v0

    .line 66
    const v4, 0x3e4ccccd    # 0.2f

    .line 67
    .line 68
    .line 69
    mul-float v4, v4, v0

    .line 70
    .line 71
    sub-float/2addr v3, v4

    .line 72
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 73
    .line 74
    .line 75
    move-result v5

    .line 76
    int-to-float v5, v5

    .line 77
    const v6, 0x3e23d70a    # 0.16f

    .line 78
    .line 79
    .line 80
    mul-float v6, v6, v0

    .line 81
    .line 82
    sub-float/2addr v5, v6

    .line 83
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 84
    .line 85
    .line 86
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 87
    .line 88
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 89
    .line 90
    .line 91
    move-result v5

    .line 92
    int-to-float v5, v5

    .line 93
    sub-float/2addr v5, v4

    .line 94
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 95
    .line 96
    .line 97
    move-result v6

    .line 98
    int-to-float v6, v6

    .line 99
    const v7, 0x3eb851ec    # 0.36f

    .line 100
    .line 101
    .line 102
    mul-float v7, v7, v0

    .line 103
    .line 104
    sub-float/2addr v6, v7

    .line 105
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 106
    .line 107
    .line 108
    move-result v7

    .line 109
    int-to-float v7, v7

    .line 110
    add-float/2addr v7, v4

    .line 111
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 112
    .line 113
    .line 114
    move-result v4

    .line 115
    int-to-float v4, v4

    .line 116
    const v8, 0x3d23d70a    # 0.04f

    .line 117
    .line 118
    .line 119
    mul-float v8, v8, v0

    .line 120
    .line 121
    add-float/2addr v4, v8

    .line 122
    invoke-virtual {v3, v5, v6, v7, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 123
    .line 124
    .line 125
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 126
    .line 127
    const/high16 v4, 0x43700000    # 240.0f

    .line 128
    .line 129
    const/high16 v5, 0x43340000    # 180.0f

    .line 130
    .line 131
    invoke-virtual {v2, v3, v5, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 132
    .line 133
    .line 134
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 135
    .line 136
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 137
    .line 138
    .line 139
    move-result v4

    .line 140
    int-to-float v4, v4

    .line 141
    const v6, 0x3d4ccccd    # 0.05f

    .line 142
    .line 143
    .line 144
    mul-float v6, v6, v0

    .line 145
    .line 146
    add-float/2addr v4, v6

    .line 147
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 148
    .line 149
    .line 150
    move-result v7

    .line 151
    int-to-float v7, v7

    .line 152
    const v8, 0x3c449ba6    # 0.012f

    .line 153
    .line 154
    .line 155
    mul-float v8, v8, v0

    .line 156
    .line 157
    add-float/2addr v7, v8

    .line 158
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 159
    .line 160
    .line 161
    move-result v8

    .line 162
    int-to-float v8, v8

    .line 163
    const v9, 0x3e19999a    # 0.15f

    .line 164
    .line 165
    .line 166
    mul-float v9, v9, v0

    .line 167
    .line 168
    add-float/2addr v8, v9

    .line 169
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 170
    .line 171
    .line 172
    move-result v10

    .line 173
    int-to-float v10, v10

    .line 174
    const v11, 0x3de56042    # 0.112f

    .line 175
    .line 176
    .line 177
    mul-float v11, v11, v0

    .line 178
    .line 179
    add-float/2addr v10, v11

    .line 180
    invoke-virtual {v3, v4, v7, v8, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 181
    .line 182
    .line 183
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 184
    .line 185
    const/high16 v4, 0x43870000    # 270.0f

    .line 186
    .line 187
    const/high16 v7, -0x3d4c0000    # -90.0f

    .line 188
    .line 189
    invoke-virtual {v2, v3, v4, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 190
    .line 191
    .line 192
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 193
    .line 194
    .line 195
    move-result v3

    .line 196
    int-to-float v3, v3

    .line 197
    add-float/2addr v3, v6

    .line 198
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 199
    .line 200
    .line 201
    move-result v4

    .line 202
    int-to-float v4, v4

    .line 203
    const v7, 0x3e3851ec    # 0.18f

    .line 204
    .line 205
    .line 206
    mul-float v7, v7, v0

    .line 207
    .line 208
    add-float/2addr v4, v7

    .line 209
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 210
    .line 211
    .line 212
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 213
    .line 214
    .line 215
    move-result v3

    .line 216
    int-to-float v3, v3

    .line 217
    sub-float/2addr v3, v6

    .line 218
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 219
    .line 220
    .line 221
    move-result v4

    .line 222
    int-to-float v4, v4

    .line 223
    add-float/2addr v4, v7

    .line 224
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 225
    .line 226
    .line 227
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 228
    .line 229
    .line 230
    move-result v3

    .line 231
    int-to-float v3, v3

    .line 232
    sub-float/2addr v3, v6

    .line 233
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 234
    .line 235
    .line 236
    move-result v4

    .line 237
    int-to-float v4, v4

    .line 238
    const v7, 0x3dcccccd    # 0.1f

    .line 239
    .line 240
    .line 241
    mul-float v7, v7, v0

    .line 242
    .line 243
    add-float/2addr v4, v7

    .line 244
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 245
    .line 246
    .line 247
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 248
    .line 249
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 250
    .line 251
    .line 252
    move-result v4

    .line 253
    int-to-float v4, v4

    .line 254
    sub-float/2addr v4, v6

    .line 255
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 256
    .line 257
    .line 258
    move-result v6

    .line 259
    int-to-float v6, v6

    .line 260
    const v8, 0x3d958106    # 0.073f

    .line 261
    .line 262
    .line 263
    mul-float v8, v8, v0

    .line 264
    .line 265
    sub-float/2addr v6, v8

    .line 266
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 267
    .line 268
    .line 269
    move-result v8

    .line 270
    int-to-float v8, v8

    .line 271
    add-float/2addr v8, v9

    .line 272
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 273
    .line 274
    .line 275
    move-result v9

    .line 276
    int-to-float v9, v9

    .line 277
    const v10, 0x3e8bc6a8    # 0.273f

    .line 278
    .line 279
    .line 280
    mul-float v10, v10, v0

    .line 281
    .line 282
    add-float/2addr v9, v10

    .line 283
    invoke-virtual {v3, v4, v6, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 284
    .line 285
    .line 286
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 287
    .line 288
    const/high16 v4, 0x42b40000    # 90.0f

    .line 289
    .line 290
    invoke-virtual {v2, v3, v5, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 291
    .line 292
    .line 293
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 294
    .line 295
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 296
    .line 297
    .line 298
    move-result v4

    .line 299
    int-to-float v4, v4

    .line 300
    sub-float/2addr v4, v7

    .line 301
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 302
    .line 303
    .line 304
    move-result v5

    .line 305
    int-to-float v5, v5

    .line 306
    const v6, 0x3e851eb8    # 0.26f

    .line 307
    .line 308
    .line 309
    mul-float v6, v6, v0

    .line 310
    .line 311
    sub-float/2addr v5, v6

    .line 312
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 313
    .line 314
    .line 315
    move-result v6

    .line 316
    int-to-float v6, v6

    .line 317
    add-float/2addr v6, v7

    .line 318
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 319
    .line 320
    .line 321
    move-result v7

    .line 322
    int-to-float v7, v7

    .line 323
    const v8, 0x3d75c28f    # 0.06f

    .line 324
    .line 325
    .line 326
    mul-float v8, v8, v0

    .line 327
    .line 328
    sub-float/2addr v7, v8

    .line 329
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 330
    .line 331
    .line 332
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 333
    .line 334
    const/high16 v4, 0x42700000    # 60.0f

    .line 335
    .line 336
    const/high16 v5, -0x3c900000    # -240.0f

    .line 337
    .line 338
    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 339
    .line 340
    .line 341
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 342
    .line 343
    .line 344
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 345
    .line 346
    .line 347
    move-result v3

    .line 348
    int-to-float v3, v3

    .line 349
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 350
    .line 351
    .line 352
    move-result p1

    .line 353
    int-to-float p1, p1

    .line 354
    const v4, 0x3e8f5c29    # 0.28f

    .line 355
    .line 356
    .line 357
    mul-float v4, v4, v0

    .line 358
    .line 359
    add-float/2addr p1, v4

    .line 360
    const v4, 0x3da3d70a    # 0.08f

    .line 361
    .line 362
    .line 363
    mul-float v0, v0, v4

    .line 364
    .line 365
    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 366
    .line 367
    invoke-virtual {v2, v3, p1, v0, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 368
    .line 369
    .line 370
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 371
    .line 372
    .line 373
    new-instance p1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 374
    .line 375
    invoke-direct {p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 376
    .line 377
    .line 378
    const/4 v0, 0x0

    .line 379
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 380
    .line 381
    .line 382
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 383
    .line 384
    .line 385
    move-result-object p0

    .line 386
    if-eqz p0, :cond_0

    .line 387
    .line 388
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 389
    .line 390
    .line 391
    move-result v0

    .line 392
    if-nez v0, :cond_0

    .line 393
    .line 394
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 395
    .line 396
    .line 397
    move-result-object v0

    .line 398
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 399
    .line 400
    .line 401
    move-result p0

    .line 402
    const-wide/high16 v2, -0x4020000000000000L    # -0.5

    .line 403
    .line 404
    invoke-virtual {v0, p0, v2, v3}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 405
    .line 406
    .line 407
    move-result p0

    .line 408
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 409
    .line 410
    .line 411
    goto :goto_0

    .line 412
    :cond_0
    const p0, -0x70aaaaab

    .line 413
    .line 414
    .line 415
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 416
    .line 417
    .line 418
    :goto_0
    invoke-virtual {v1, p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 419
    .line 420
    .line 421
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 422
    .line 423
    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 424
    .line 425
    .line 426
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 427
    .line 428
    return-object p0
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getHomePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 9
    .line 10
    .line 11
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 12
    .line 13
    move-object/from16 v3, p1

    .line 14
    .line 15
    invoke-virtual {v2, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 16
    .line 17
    .line 18
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 19
    .line 20
    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 21
    .line 22
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 33
    .line 34
    .line 35
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 36
    .line 37
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 41
    .line 42
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 43
    .line 44
    .line 45
    new-instance v1, Landroid/graphics/Path;

    .line 46
    .line 47
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 48
    .line 49
    .line 50
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 55
    .line 56
    .line 57
    move-result v4

    .line 58
    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    .line 59
    .line 60
    .line 61
    move-result v2

    .line 62
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 63
    .line 64
    .line 65
    move-result v4

    .line 66
    int-to-float v4, v4

    .line 67
    int-to-float v2, v2

    .line 68
    const v5, 0x3e8f5c29    # 0.28f

    .line 69
    .line 70
    .line 71
    mul-float v5, v5, v2

    .line 72
    .line 73
    sub-float v6, v4, v5

    .line 74
    .line 75
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 76
    .line 77
    .line 78
    move-result v4

    .line 79
    int-to-float v7, v4

    .line 80
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 81
    .line 82
    .line 83
    move-result v4

    .line 84
    int-to-float v4, v4

    .line 85
    add-float v8, v4, v5

    .line 86
    .line 87
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 88
    .line 89
    .line 90
    move-result v4

    .line 91
    int-to-float v4, v4

    .line 92
    const/high16 v5, 0x3ec00000    # 0.375f

    .line 93
    .line 94
    mul-float v10, v2, v5

    .line 95
    .line 96
    add-float v9, v4, v10

    .line 97
    .line 98
    sget-object v11, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 99
    .line 100
    move-object v4, v1

    .line 101
    move v5, v6

    .line 102
    move v6, v7

    .line 103
    move v7, v8

    .line 104
    move v8, v9

    .line 105
    move-object v9, v11

    .line 106
    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 107
    .line 108
    .line 109
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 110
    .line 111
    .line 112
    move-result v4

    .line 113
    int-to-float v4, v4

    .line 114
    const v5, 0x3e0f5c29    # 0.14f

    .line 115
    .line 116
    .line 117
    mul-float v5, v5, v2

    .line 118
    .line 119
    add-float/2addr v4, v5

    .line 120
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 121
    .line 122
    .line 123
    move-result v6

    .line 124
    int-to-float v6, v6

    .line 125
    const v7, 0x3ea8f5c3    # 0.33f

    .line 126
    .line 127
    .line 128
    mul-float v7, v7, v2

    .line 129
    .line 130
    sub-float/2addr v6, v7

    .line 131
    invoke-virtual {v1, v4, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 132
    .line 133
    .line 134
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 135
    .line 136
    .line 137
    move-result v4

    .line 138
    int-to-float v4, v4

    .line 139
    const v6, 0x3e75c28f    # 0.24f

    .line 140
    .line 141
    .line 142
    mul-float v6, v6, v2

    .line 143
    .line 144
    add-float/2addr v4, v6

    .line 145
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 146
    .line 147
    .line 148
    move-result v8

    .line 149
    int-to-float v8, v8

    .line 150
    sub-float/2addr v8, v7

    .line 151
    invoke-virtual {v1, v4, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 152
    .line 153
    .line 154
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 155
    .line 156
    .line 157
    move-result v4

    .line 158
    int-to-float v4, v4

    .line 159
    add-float/2addr v4, v6

    .line 160
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 161
    .line 162
    .line 163
    move-result v6

    .line 164
    int-to-float v6, v6

    .line 165
    const v7, 0x3e0a3d71    # 0.135f

    .line 166
    .line 167
    .line 168
    mul-float v7, v7, v2

    .line 169
    .line 170
    sub-float/2addr v6, v7

    .line 171
    invoke-virtual {v1, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 172
    .line 173
    .line 174
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 175
    .line 176
    .line 177
    move-result v4

    .line 178
    int-to-float v4, v4

    .line 179
    add-float/2addr v4, v5

    .line 180
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 181
    .line 182
    .line 183
    move-result v5

    .line 184
    int-to-float v5, v5

    .line 185
    const v6, 0x3e70a3d7    # 0.235f

    .line 186
    .line 187
    .line 188
    mul-float v6, v6, v2

    .line 189
    .line 190
    sub-float/2addr v5, v6

    .line 191
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 192
    .line 193
    .line 194
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 195
    .line 196
    .line 197
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 198
    .line 199
    .line 200
    new-instance v1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 201
    .line 202
    invoke-direct {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 203
    .line 204
    .line 205
    const/4 v4, 0x0

    .line 206
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 207
    .line 208
    .line 209
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 210
    .line 211
    .line 212
    move-result-object v5

    .line 213
    if-eqz v5, :cond_0

    .line 214
    .line 215
    invoke-virtual {v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 216
    .line 217
    .line 218
    move-result v6

    .line 219
    if-nez v6, :cond_0

    .line 220
    .line 221
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 222
    .line 223
    .line 224
    move-result-object v6

    .line 225
    invoke-virtual {v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 226
    .line 227
    .line 228
    move-result v7

    .line 229
    const-wide v8, -0x402cccccc0000000L    # -0.30000001192092896

    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    invoke-virtual {v6, v7, v8, v9}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 235
    .line 236
    .line 237
    move-result v6

    .line 238
    invoke-virtual {v1, v6}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 239
    .line 240
    .line 241
    goto :goto_0

    .line 242
    :cond_0
    const v6, -0x70aaaaab

    .line 243
    .line 244
    .line 245
    invoke-virtual {v1, v6}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 246
    .line 247
    .line 248
    :goto_0
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 249
    .line 250
    .line 251
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 252
    .line 253
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    .line 255
    .line 256
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 257
    .line 258
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 259
    .line 260
    .line 261
    new-instance v1, Landroid/graphics/Path;

    .line 262
    .line 263
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 264
    .line 265
    .line 266
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 267
    .line 268
    .line 269
    move-result v6

    .line 270
    int-to-float v6, v6

    .line 271
    sub-float/2addr v6, v10

    .line 272
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 273
    .line 274
    .line 275
    move-result v7

    .line 276
    int-to-float v7, v7

    .line 277
    invoke-virtual {v1, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 278
    .line 279
    .line 280
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 281
    .line 282
    .line 283
    move-result v6

    .line 284
    int-to-float v6, v6

    .line 285
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 286
    .line 287
    .line 288
    move-result v7

    .line 289
    int-to-float v7, v7

    .line 290
    sub-float/2addr v7, v10

    .line 291
    invoke-virtual {v1, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 292
    .line 293
    .line 294
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 295
    .line 296
    .line 297
    move-result v6

    .line 298
    int-to-float v6, v6

    .line 299
    add-float/2addr v6, v10

    .line 300
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 301
    .line 302
    .line 303
    move-result v7

    .line 304
    int-to-float v7, v7

    .line 305
    invoke-virtual {v1, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 306
    .line 307
    .line 308
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 309
    .line 310
    .line 311
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 312
    .line 313
    .line 314
    move-result v6

    .line 315
    int-to-float v6, v6

    .line 316
    const v7, 0x3d4ccccd    # 0.05f

    .line 317
    .line 318
    .line 319
    mul-float v7, v7, v2

    .line 320
    .line 321
    sub-float v12, v6, v7

    .line 322
    .line 323
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 324
    .line 325
    .line 326
    move-result v6

    .line 327
    int-to-float v6, v6

    .line 328
    const v8, 0x3e3851ec    # 0.18f

    .line 329
    .line 330
    .line 331
    mul-float v2, v2, v8

    .line 332
    .line 333
    add-float v13, v6, v2

    .line 334
    .line 335
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 336
    .line 337
    .line 338
    move-result v2

    .line 339
    int-to-float v2, v2

    .line 340
    add-float v14, v2, v7

    .line 341
    .line 342
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 343
    .line 344
    .line 345
    move-result v2

    .line 346
    int-to-float v2, v2

    .line 347
    add-float v15, v2, v10

    .line 348
    .line 349
    sget-object v16, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 350
    .line 351
    move-object v11, v1

    .line 352
    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 353
    .line 354
    .line 355
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 356
    .line 357
    .line 358
    new-instance v1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 359
    .line 360
    invoke-direct {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 361
    .line 362
    .line 363
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 364
    .line 365
    .line 366
    if-eqz v5, :cond_1

    .line 367
    .line 368
    invoke-virtual {v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 369
    .line 370
    .line 371
    move-result v2

    .line 372
    if-nez v2, :cond_1

    .line 373
    .line 374
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 375
    .line 376
    .line 377
    move-result-object v2

    .line 378
    invoke-virtual {v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 379
    .line 380
    .line 381
    move-result v3

    .line 382
    const-wide/high16 v4, -0x4020000000000000L    # -0.5

    .line 383
    .line 384
    invoke-virtual {v2, v3, v4, v5}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 385
    .line 386
    .line 387
    move-result v2

    .line 388
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 389
    .line 390
    .line 391
    goto :goto_1

    .line 392
    :cond_1
    const v2, -0x70bbbbbc

    .line 393
    .line 394
    .line 395
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 396
    .line 397
    .line 398
    :goto_1
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 399
    .line 400
    .line 401
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 402
    .line 403
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 404
    .line 405
    .line 406
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 407
    .line 408
    return-object v0
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getInformationPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 9
    .line 10
    .line 11
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 12
    .line 13
    invoke-virtual {v2, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 17
    .line 18
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 19
    .line 20
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 31
    .line 32
    .line 33
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 34
    .line 35
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 39
    .line 40
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 41
    .line 42
    .line 43
    new-instance v1, Landroid/graphics/Path;

    .line 44
    .line 45
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    int-to-float v3, v3

    .line 65
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 66
    .line 67
    .line 68
    move-result v4

    .line 69
    int-to-float v4, v4

    .line 70
    int-to-float v2, v2

    .line 71
    const/high16 v5, 0x3ec00000    # 0.375f

    .line 72
    .line 73
    mul-float v5, v5, v2

    .line 74
    .line 75
    sget-object v6, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 76
    .line 77
    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 81
    .line 82
    .line 83
    new-instance v1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 84
    .line 85
    invoke-direct {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 86
    .line 87
    .line 88
    const/4 v3, 0x0

    .line 89
    invoke-virtual {v1, v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 93
    .line 94
    .line 95
    move-result-object p0

    .line 96
    if-eqz p0, :cond_0

    .line 97
    .line 98
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 99
    .line 100
    .line 101
    move-result v4

    .line 102
    if-nez v4, :cond_0

    .line 103
    .line 104
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 105
    .line 106
    .line 107
    move-result-object v4

    .line 108
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 109
    .line 110
    .line 111
    move-result v5

    .line 112
    const-wide/high16 v6, -0x4020000000000000L    # -0.5

    .line 113
    .line 114
    invoke-virtual {v4, v5, v6, v7}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 115
    .line 116
    .line 117
    move-result v4

    .line 118
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 119
    .line 120
    .line 121
    goto :goto_0

    .line 122
    :cond_0
    const v4, -0x70bbbbbc

    .line 123
    .line 124
    .line 125
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 126
    .line 127
    .line 128
    :goto_0
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 129
    .line 130
    .line 131
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 132
    .line 133
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    .line 135
    .line 136
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 137
    .line 138
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 139
    .line 140
    .line 141
    new-instance v1, Landroid/graphics/Path;

    .line 142
    .line 143
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 144
    .line 145
    .line 146
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 147
    .line 148
    .line 149
    move-result v4

    .line 150
    int-to-float v4, v4

    .line 151
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 152
    .line 153
    .line 154
    move-result v5

    .line 155
    int-to-float v5, v5

    .line 156
    const v6, 0x3e6147ae    # 0.22f

    .line 157
    .line 158
    .line 159
    mul-float v6, v6, v2

    .line 160
    .line 161
    sub-float/2addr v5, v6

    .line 162
    const v6, 0x3d75c28f    # 0.06f

    .line 163
    .line 164
    .line 165
    mul-float v6, v6, v2

    .line 166
    .line 167
    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 168
    .line 169
    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 170
    .line 171
    .line 172
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 173
    .line 174
    .line 175
    move-result v4

    .line 176
    int-to-float v4, v4

    .line 177
    const v5, 0x3df5c28f    # 0.12f

    .line 178
    .line 179
    .line 180
    mul-float v5, v5, v2

    .line 181
    .line 182
    sub-float/2addr v4, v5

    .line 183
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 184
    .line 185
    .line 186
    move-result v7

    .line 187
    int-to-float v7, v7

    .line 188
    const v8, 0x3de147ae    # 0.11f

    .line 189
    .line 190
    .line 191
    mul-float v8, v8, v2

    .line 192
    .line 193
    sub-float/2addr v7, v8

    .line 194
    invoke-virtual {v1, v4, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 195
    .line 196
    .line 197
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 198
    .line 199
    .line 200
    move-result v4

    .line 201
    int-to-float v4, v4

    .line 202
    add-float/2addr v4, v6

    .line 203
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 204
    .line 205
    .line 206
    move-result v7

    .line 207
    int-to-float v7, v7

    .line 208
    sub-float/2addr v7, v8

    .line 209
    invoke-virtual {v1, v4, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 210
    .line 211
    .line 212
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 213
    .line 214
    .line 215
    move-result v4

    .line 216
    int-to-float v4, v4

    .line 217
    add-float/2addr v4, v6

    .line 218
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 219
    .line 220
    .line 221
    move-result v7

    .line 222
    int-to-float v7, v7

    .line 223
    const v8, 0x3e23d70a    # 0.16f

    .line 224
    .line 225
    .line 226
    mul-float v8, v8, v2

    .line 227
    .line 228
    add-float/2addr v7, v8

    .line 229
    invoke-virtual {v1, v4, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 230
    .line 231
    .line 232
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 233
    .line 234
    .line 235
    move-result v4

    .line 236
    int-to-float v4, v4

    .line 237
    add-float/2addr v4, v5

    .line 238
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 239
    .line 240
    .line 241
    move-result v7

    .line 242
    int-to-float v7, v7

    .line 243
    add-float/2addr v7, v8

    .line 244
    invoke-virtual {v1, v4, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 245
    .line 246
    .line 247
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 248
    .line 249
    .line 250
    move-result v4

    .line 251
    int-to-float v4, v4

    .line 252
    add-float/2addr v4, v5

    .line 253
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 254
    .line 255
    .line 256
    move-result v7

    .line 257
    int-to-float v7, v7

    .line 258
    const v9, 0x3e4ccccd    # 0.2f

    .line 259
    .line 260
    .line 261
    mul-float v9, v9, v2

    .line 262
    .line 263
    add-float/2addr v7, v9

    .line 264
    invoke-virtual {v1, v4, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 265
    .line 266
    .line 267
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 268
    .line 269
    .line 270
    move-result v4

    .line 271
    int-to-float v4, v4

    .line 272
    sub-float/2addr v4, v5

    .line 273
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 274
    .line 275
    .line 276
    move-result v7

    .line 277
    int-to-float v7, v7

    .line 278
    add-float/2addr v7, v9

    .line 279
    invoke-virtual {v1, v4, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 280
    .line 281
    .line 282
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 283
    .line 284
    .line 285
    move-result v4

    .line 286
    int-to-float v4, v4

    .line 287
    sub-float/2addr v4, v5

    .line 288
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 289
    .line 290
    .line 291
    move-result v7

    .line 292
    int-to-float v7, v7

    .line 293
    add-float/2addr v7, v8

    .line 294
    invoke-virtual {v1, v4, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 295
    .line 296
    .line 297
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 298
    .line 299
    .line 300
    move-result v4

    .line 301
    int-to-float v4, v4

    .line 302
    sub-float/2addr v4, v6

    .line 303
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 304
    .line 305
    .line 306
    move-result v7

    .line 307
    int-to-float v7, v7

    .line 308
    add-float/2addr v7, v8

    .line 309
    invoke-virtual {v1, v4, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 310
    .line 311
    .line 312
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 313
    .line 314
    .line 315
    move-result v4

    .line 316
    int-to-float v4, v4

    .line 317
    sub-float/2addr v4, v6

    .line 318
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 319
    .line 320
    .line 321
    move-result v6

    .line 322
    int-to-float v6, v6

    .line 323
    const v7, 0x3da3d70a    # 0.08f

    .line 324
    .line 325
    .line 326
    mul-float v2, v2, v7

    .line 327
    .line 328
    sub-float/2addr v6, v2

    .line 329
    invoke-virtual {v1, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 330
    .line 331
    .line 332
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 333
    .line 334
    .line 335
    move-result v4

    .line 336
    int-to-float v4, v4

    .line 337
    sub-float/2addr v4, v5

    .line 338
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 339
    .line 340
    .line 341
    move-result p1

    .line 342
    int-to-float p1, p1

    .line 343
    sub-float/2addr p1, v2

    .line 344
    invoke-virtual {v1, v4, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 345
    .line 346
    .line 347
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 348
    .line 349
    .line 350
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 351
    .line 352
    .line 353
    new-instance p1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 354
    .line 355
    invoke-direct {p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 356
    .line 357
    .line 358
    invoke-virtual {p1, v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 359
    .line 360
    .line 361
    if-eqz p0, :cond_1

    .line 362
    .line 363
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 364
    .line 365
    .line 366
    move-result v1

    .line 367
    if-nez v1, :cond_1

    .line 368
    .line 369
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 370
    .line 371
    .line 372
    move-result-object v1

    .line 373
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 374
    .line 375
    .line 376
    move-result p0

    .line 377
    const-wide v2, 0x3fe3333340000000L    # 0.6000000238418579

    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    invoke-virtual {v1, p0, v2, v3}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 383
    .line 384
    .line 385
    move-result p0

    .line 386
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 387
    .line 388
    .line 389
    goto :goto_1

    .line 390
    :cond_1
    const p0, -0x70323233

    .line 391
    .line 392
    .line 393
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 394
    .line 395
    .line 396
    :goto_1
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 397
    .line 398
    .line 399
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 400
    .line 401
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 402
    .line 403
    .line 404
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 405
    .line 406
    return-object p0
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getMoviePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 9
    .line 10
    .line 11
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 12
    .line 13
    invoke-virtual {v2, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 17
    .line 18
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 19
    .line 20
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 31
    .line 32
    .line 33
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 34
    .line 35
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 51
    .line 52
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 53
    .line 54
    .line 55
    new-instance v2, Landroid/graphics/Path;

    .line 56
    .line 57
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    int-to-float v3, v3

    .line 65
    int-to-float v0, v0

    .line 66
    const v4, 0x3ec28f5c    # 0.38f

    .line 67
    .line 68
    .line 69
    mul-float v4, v4, v0

    .line 70
    .line 71
    sub-float/2addr v3, v4

    .line 72
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 73
    .line 74
    .line 75
    move-result v5

    .line 76
    int-to-float v5, v5

    .line 77
    const v6, 0x3e4ccccd    # 0.2f

    .line 78
    .line 79
    .line 80
    mul-float v6, v6, v0

    .line 81
    .line 82
    sub-float/2addr v5, v6

    .line 83
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 87
    .line 88
    .line 89
    move-result v3

    .line 90
    int-to-float v3, v3

    .line 91
    const v5, 0x3e9eb852    # 0.31f

    .line 92
    .line 93
    .line 94
    mul-float v5, v5, v0

    .line 95
    .line 96
    sub-float/2addr v3, v5

    .line 97
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 98
    .line 99
    .line 100
    move-result v7

    .line 101
    int-to-float v7, v7

    .line 102
    sub-float/2addr v7, v6

    .line 103
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 107
    .line 108
    .line 109
    move-result v3

    .line 110
    int-to-float v3, v3

    .line 111
    const v6, 0x3e99999a    # 0.3f

    .line 112
    .line 113
    .line 114
    mul-float v6, v6, v0

    .line 115
    .line 116
    sub-float/2addr v3, v6

    .line 117
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 118
    .line 119
    .line 120
    move-result v6

    .line 121
    int-to-float v6, v6

    .line 122
    const v7, 0x3e3851ec    # 0.18f

    .line 123
    .line 124
    .line 125
    mul-float v7, v7, v0

    .line 126
    .line 127
    sub-float/2addr v6, v7

    .line 128
    invoke-virtual {v2, v3, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 129
    .line 130
    .line 131
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 132
    .line 133
    .line 134
    move-result v3

    .line 135
    int-to-float v3, v3

    .line 136
    add-float/2addr v3, v7

    .line 137
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 138
    .line 139
    .line 140
    move-result v6

    .line 141
    int-to-float v6, v6

    .line 142
    sub-float/2addr v6, v7

    .line 143
    invoke-virtual {v2, v3, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 144
    .line 145
    .line 146
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 147
    .line 148
    .line 149
    move-result v3

    .line 150
    int-to-float v3, v3

    .line 151
    const v6, 0x3e6147ae    # 0.22f

    .line 152
    .line 153
    .line 154
    mul-float v6, v6, v0

    .line 155
    .line 156
    add-float/2addr v3, v6

    .line 157
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 158
    .line 159
    .line 160
    move-result v7

    .line 161
    int-to-float v7, v7

    .line 162
    const v8, 0x3e19999a    # 0.15f

    .line 163
    .line 164
    .line 165
    mul-float v8, v8, v0

    .line 166
    .line 167
    sub-float/2addr v7, v8

    .line 168
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    .line 170
    .line 171
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 172
    .line 173
    .line 174
    move-result v3

    .line 175
    int-to-float v3, v3

    .line 176
    add-float/2addr v3, v6

    .line 177
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 178
    .line 179
    .line 180
    move-result v7

    .line 181
    int-to-float v7, v7

    .line 182
    const v9, 0x3df5c28f    # 0.12f

    .line 183
    .line 184
    .line 185
    mul-float v9, v9, v0

    .line 186
    .line 187
    sub-float/2addr v7, v9

    .line 188
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 189
    .line 190
    .line 191
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 192
    .line 193
    .line 194
    move-result v3

    .line 195
    int-to-float v3, v3

    .line 196
    add-float/2addr v3, v5

    .line 197
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 198
    .line 199
    .line 200
    move-result v7

    .line 201
    int-to-float v7, v7

    .line 202
    sub-float/2addr v7, v9

    .line 203
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 204
    .line 205
    .line 206
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 207
    .line 208
    .line 209
    move-result v3

    .line 210
    int-to-float v3, v3

    .line 211
    const v7, 0x3eae147b    # 0.34f

    .line 212
    .line 213
    .line 214
    mul-float v7, v7, v0

    .line 215
    .line 216
    add-float/2addr v3, v7

    .line 217
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 218
    .line 219
    .line 220
    move-result v10

    .line 221
    int-to-float v10, v10

    .line 222
    sub-float/2addr v10, v8

    .line 223
    invoke-virtual {v2, v3, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 224
    .line 225
    .line 226
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 227
    .line 228
    .line 229
    move-result v3

    .line 230
    int-to-float v3, v3

    .line 231
    const v10, 0x3ebd70a4    # 0.37f

    .line 232
    .line 233
    .line 234
    mul-float v10, v10, v0

    .line 235
    .line 236
    add-float/2addr v3, v10

    .line 237
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 238
    .line 239
    .line 240
    move-result v11

    .line 241
    int-to-float v11, v11

    .line 242
    sub-float/2addr v11, v8

    .line 243
    invoke-virtual {v2, v3, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 244
    .line 245
    .line 246
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 247
    .line 248
    .line 249
    move-result v3

    .line 250
    int-to-float v3, v3

    .line 251
    add-float/2addr v3, v10

    .line 252
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 253
    .line 254
    .line 255
    move-result v10

    .line 256
    int-to-float v10, v10

    .line 257
    add-float/2addr v10, v8

    .line 258
    invoke-virtual {v2, v3, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 259
    .line 260
    .line 261
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 262
    .line 263
    .line 264
    move-result v3

    .line 265
    int-to-float v3, v3

    .line 266
    add-float/2addr v3, v7

    .line 267
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 268
    .line 269
    .line 270
    move-result v7

    .line 271
    int-to-float v7, v7

    .line 272
    add-float/2addr v7, v8

    .line 273
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 274
    .line 275
    .line 276
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 277
    .line 278
    .line 279
    move-result v3

    .line 280
    int-to-float v3, v3

    .line 281
    const v7, 0x3e947ae1    # 0.29f

    .line 282
    .line 283
    .line 284
    mul-float v7, v7, v0

    .line 285
    .line 286
    add-float/2addr v3, v7

    .line 287
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 288
    .line 289
    .line 290
    move-result v8

    .line 291
    int-to-float v8, v8

    .line 292
    add-float/2addr v8, v9

    .line 293
    invoke-virtual {v2, v3, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 294
    .line 295
    .line 296
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 297
    .line 298
    .line 299
    move-result v3

    .line 300
    int-to-float v3, v3

    .line 301
    add-float/2addr v3, v6

    .line 302
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 303
    .line 304
    .line 305
    move-result v8

    .line 306
    int-to-float v8, v8

    .line 307
    add-float/2addr v8, v9

    .line 308
    invoke-virtual {v2, v3, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 309
    .line 310
    .line 311
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 312
    .line 313
    .line 314
    move-result v3

    .line 315
    int-to-float v3, v3

    .line 316
    add-float/2addr v3, v6

    .line 317
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 318
    .line 319
    .line 320
    move-result v6

    .line 321
    int-to-float v6, v6

    .line 322
    const v8, 0x3e23d70a    # 0.16f

    .line 323
    .line 324
    .line 325
    mul-float v8, v8, v0

    .line 326
    .line 327
    add-float/2addr v6, v8

    .line 328
    invoke-virtual {v2, v3, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 329
    .line 330
    .line 331
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 332
    .line 333
    .line 334
    move-result v3

    .line 335
    int-to-float v3, v3

    .line 336
    sub-float/2addr v3, v7

    .line 337
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 338
    .line 339
    .line 340
    move-result v6

    .line 341
    int-to-float v6, v6

    .line 342
    add-float/2addr v6, v8

    .line 343
    invoke-virtual {v2, v3, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 344
    .line 345
    .line 346
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 347
    .line 348
    .line 349
    move-result v3

    .line 350
    int-to-float v3, v3

    .line 351
    sub-float/2addr v3, v7

    .line 352
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 353
    .line 354
    .line 355
    move-result v6

    .line 356
    int-to-float v6, v6

    .line 357
    const v7, 0x3d75c28f    # 0.06f

    .line 358
    .line 359
    .line 360
    mul-float v7, v7, v0

    .line 361
    .line 362
    sub-float/2addr v6, v7

    .line 363
    invoke-virtual {v2, v3, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 364
    .line 365
    .line 366
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 367
    .line 368
    .line 369
    move-result v3

    .line 370
    int-to-float v3, v3

    .line 371
    sub-float/2addr v3, v5

    .line 372
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 373
    .line 374
    .line 375
    move-result v5

    .line 376
    int-to-float v5, v5

    .line 377
    sub-float/2addr v5, v7

    .line 378
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 379
    .line 380
    .line 381
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 382
    .line 383
    .line 384
    move-result v3

    .line 385
    int-to-float v3, v3

    .line 386
    const v5, 0x3ea3d70a    # 0.32f

    .line 387
    .line 388
    .line 389
    mul-float v5, v5, v0

    .line 390
    .line 391
    sub-float/2addr v3, v5

    .line 392
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 393
    .line 394
    .line 395
    move-result v5

    .line 396
    int-to-float v5, v5

    .line 397
    const v6, 0x3d23d70a    # 0.04f

    .line 398
    .line 399
    .line 400
    mul-float v0, v0, v6

    .line 401
    .line 402
    sub-float/2addr v5, v0

    .line 403
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 404
    .line 405
    .line 406
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 407
    .line 408
    .line 409
    move-result v3

    .line 410
    int-to-float v3, v3

    .line 411
    sub-float/2addr v3, v4

    .line 412
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 413
    .line 414
    .line 415
    move-result p1

    .line 416
    int-to-float p1, p1

    .line 417
    sub-float/2addr p1, v0

    .line 418
    invoke-virtual {v2, v3, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 419
    .line 420
    .line 421
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 422
    .line 423
    .line 424
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 425
    .line 426
    .line 427
    new-instance p1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 428
    .line 429
    invoke-direct {p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 430
    .line 431
    .line 432
    const/4 v0, 0x0

    .line 433
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 434
    .line 435
    .line 436
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 437
    .line 438
    .line 439
    move-result-object p0

    .line 440
    if-eqz p0, :cond_0

    .line 441
    .line 442
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 443
    .line 444
    .line 445
    move-result v0

    .line 446
    if-nez v0, :cond_0

    .line 447
    .line 448
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 449
    .line 450
    .line 451
    move-result-object v0

    .line 452
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 453
    .line 454
    .line 455
    move-result p0

    .line 456
    const-wide/high16 v2, -0x4020000000000000L    # -0.5

    .line 457
    .line 458
    invoke-virtual {v0, p0, v2, v3}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 459
    .line 460
    .line 461
    move-result p0

    .line 462
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 463
    .line 464
    .line 465
    goto :goto_0

    .line 466
    :cond_0
    const p0, -0x70bbbbbc

    .line 467
    .line 468
    .line 469
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 470
    .line 471
    .line 472
    :goto_0
    invoke-virtual {v1, p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 473
    .line 474
    .line 475
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 476
    .line 477
    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 478
    .line 479
    .line 480
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 481
    .line 482
    return-object p0
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getReturnPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 9
    .line 10
    .line 11
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 12
    .line 13
    invoke-virtual {v2, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 17
    .line 18
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 19
    .line 20
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 31
    .line 32
    .line 33
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 34
    .line 35
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 51
    .line 52
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 53
    .line 54
    .line 55
    new-instance v2, Landroid/graphics/Path;

    .line 56
    .line 57
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    int-to-float v3, v3

    .line 65
    int-to-float v0, v0

    .line 66
    const v4, 0x3ecccccd    # 0.4f

    .line 67
    .line 68
    .line 69
    mul-float v4, v4, v0

    .line 70
    .line 71
    sub-float/2addr v3, v4

    .line 72
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 73
    .line 74
    .line 75
    move-result v5

    .line 76
    int-to-float v5, v5

    .line 77
    const v6, 0x3e4ccccd    # 0.2f

    .line 78
    .line 79
    .line 80
    mul-float v6, v6, v0

    .line 81
    .line 82
    sub-float/2addr v5, v6

    .line 83
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 87
    .line 88
    .line 89
    move-result v3

    .line 90
    int-to-float v3, v3

    .line 91
    sub-float/2addr v3, v6

    .line 92
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 93
    .line 94
    .line 95
    move-result v5

    .line 96
    int-to-float v5, v5

    .line 97
    sub-float/2addr v5, v6

    .line 98
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 99
    .line 100
    .line 101
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 102
    .line 103
    .line 104
    move-result v3

    .line 105
    int-to-float v3, v3

    .line 106
    sub-float/2addr v3, v6

    .line 107
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 108
    .line 109
    .line 110
    move-result v5

    .line 111
    int-to-float v5, v5

    .line 112
    const v7, 0x3dcccccd    # 0.1f

    .line 113
    .line 114
    .line 115
    mul-float v7, v7, v0

    .line 116
    .line 117
    add-float/2addr v5, v7

    .line 118
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 119
    .line 120
    .line 121
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 122
    .line 123
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 124
    .line 125
    .line 126
    move-result v5

    .line 127
    int-to-float v5, v5

    .line 128
    sub-float/2addr v5, v6

    .line 129
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 130
    .line 131
    .line 132
    move-result v8

    .line 133
    int-to-float v8, v8

    .line 134
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 135
    .line 136
    .line 137
    move-result v9

    .line 138
    int-to-float v9, v9

    .line 139
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 140
    .line 141
    .line 142
    move-result v10

    .line 143
    int-to-float v10, v10

    .line 144
    add-float/2addr v10, v6

    .line 145
    invoke-virtual {v3, v5, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 146
    .line 147
    .line 148
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 149
    .line 150
    const/high16 v5, 0x43340000    # 180.0f

    .line 151
    .line 152
    const/high16 v8, -0x3d4c0000    # -90.0f

    .line 153
    .line 154
    invoke-virtual {v2, v3, v5, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 155
    .line 156
    .line 157
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 158
    .line 159
    .line 160
    move-result v3

    .line 161
    int-to-float v3, v3

    .line 162
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 163
    .line 164
    .line 165
    move-result v5

    .line 166
    int-to-float v5, v5

    .line 167
    add-float/2addr v5, v6

    .line 168
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    .line 170
    .line 171
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 172
    .line 173
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 174
    .line 175
    .line 176
    move-result v5

    .line 177
    int-to-float v5, v5

    .line 178
    sub-float/2addr v5, v7

    .line 179
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 180
    .line 181
    .line 182
    move-result v9

    .line 183
    int-to-float v9, v9

    .line 184
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 185
    .line 186
    .line 187
    move-result v10

    .line 188
    int-to-float v10, v10

    .line 189
    add-float/2addr v10, v7

    .line 190
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 191
    .line 192
    .line 193
    move-result v11

    .line 194
    int-to-float v11, v11

    .line 195
    add-float/2addr v11, v6

    .line 196
    invoke-virtual {v3, v5, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 197
    .line 198
    .line 199
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 200
    .line 201
    const/high16 v5, 0x42b40000    # 90.0f

    .line 202
    .line 203
    invoke-virtual {v2, v3, v5, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 204
    .line 205
    .line 206
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 207
    .line 208
    .line 209
    move-result v3

    .line 210
    int-to-float v3, v3

    .line 211
    add-float/2addr v3, v7

    .line 212
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 213
    .line 214
    .line 215
    move-result v7

    .line 216
    int-to-float v7, v7

    .line 217
    sub-float/2addr v7, v6

    .line 218
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 219
    .line 220
    .line 221
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 222
    .line 223
    .line 224
    move-result v3

    .line 225
    int-to-float v3, v3

    .line 226
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 227
    .line 228
    .line 229
    move-result v7

    .line 230
    int-to-float v7, v7

    .line 231
    sub-float/2addr v7, v6

    .line 232
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 233
    .line 234
    .line 235
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 236
    .line 237
    .line 238
    move-result v3

    .line 239
    int-to-float v3, v3

    .line 240
    add-float/2addr v3, v6

    .line 241
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 242
    .line 243
    .line 244
    move-result v7

    .line 245
    int-to-float v7, v7

    .line 246
    sub-float/2addr v7, v4

    .line 247
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 248
    .line 249
    .line 250
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 251
    .line 252
    .line 253
    move-result v3

    .line 254
    int-to-float v3, v3

    .line 255
    add-float/2addr v3, v4

    .line 256
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 257
    .line 258
    .line 259
    move-result v7

    .line 260
    int-to-float v7, v7

    .line 261
    sub-float/2addr v7, v6

    .line 262
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 263
    .line 264
    .line 265
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 266
    .line 267
    .line 268
    move-result v3

    .line 269
    int-to-float v3, v3

    .line 270
    const v7, 0x3e99999a    # 0.3f

    .line 271
    .line 272
    .line 273
    mul-float v0, v0, v7

    .line 274
    .line 275
    add-float/2addr v3, v0

    .line 276
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 277
    .line 278
    .line 279
    move-result v7

    .line 280
    int-to-float v7, v7

    .line 281
    sub-float/2addr v7, v6

    .line 282
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 283
    .line 284
    .line 285
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 286
    .line 287
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 288
    .line 289
    .line 290
    move-result v7

    .line 291
    int-to-float v7, v7

    .line 292
    sub-float/2addr v7, v0

    .line 293
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 294
    .line 295
    .line 296
    move-result v8

    .line 297
    int-to-float v8, v8

    .line 298
    sub-float/2addr v8, v6

    .line 299
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 300
    .line 301
    .line 302
    move-result v9

    .line 303
    int-to-float v9, v9

    .line 304
    add-float/2addr v9, v0

    .line 305
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 306
    .line 307
    .line 308
    move-result v0

    .line 309
    int-to-float v0, v0

    .line 310
    add-float/2addr v0, v4

    .line 311
    invoke-virtual {v3, v7, v8, v9, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 312
    .line 313
    .line 314
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 315
    .line 316
    const/4 v3, 0x0

    .line 317
    invoke-virtual {v2, v0, v3, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 318
    .line 319
    .line 320
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 321
    .line 322
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 323
    .line 324
    .line 325
    move-result v3

    .line 326
    int-to-float v3, v3

    .line 327
    sub-float/2addr v3, v4

    .line 328
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 329
    .line 330
    .line 331
    move-result v7

    .line 332
    int-to-float v7, v7

    .line 333
    sub-float/2addr v7, v6

    .line 334
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 335
    .line 336
    .line 337
    move-result v8

    .line 338
    int-to-float v8, v8

    .line 339
    add-float/2addr v8, v6

    .line 340
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 341
    .line 342
    .line 343
    move-result p1

    .line 344
    int-to-float p1, p1

    .line 345
    add-float/2addr p1, v4

    .line 346
    invoke-virtual {v0, v3, v7, v8, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 347
    .line 348
    .line 349
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 350
    .line 351
    invoke-virtual {v2, p1, v5, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 352
    .line 353
    .line 354
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 355
    .line 356
    .line 357
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 358
    .line 359
    .line 360
    new-instance p1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 361
    .line 362
    invoke-direct {p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 363
    .line 364
    .line 365
    const/4 v0, 0x0

    .line 366
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 367
    .line 368
    .line 369
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 370
    .line 371
    .line 372
    move-result-object p0

    .line 373
    if-eqz p0, :cond_0

    .line 374
    .line 375
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 376
    .line 377
    .line 378
    move-result v0

    .line 379
    if-nez v0, :cond_0

    .line 380
    .line 381
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 382
    .line 383
    .line 384
    move-result-object v0

    .line 385
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 386
    .line 387
    .line 388
    move-result p0

    .line 389
    const-wide/high16 v2, -0x4020000000000000L    # -0.5

    .line 390
    .line 391
    invoke-virtual {v0, p0, v2, v3}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 392
    .line 393
    .line 394
    move-result p0

    .line 395
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 396
    .line 397
    .line 398
    goto :goto_0

    .line 399
    :cond_0
    const p0, -0x70bbbbbc

    .line 400
    .line 401
    .line 402
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 403
    .line 404
    .line 405
    :goto_0
    invoke-virtual {v1, p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 406
    .line 407
    .line 408
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 409
    .line 410
    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 411
    .line 412
    .line 413
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 414
    .line 415
    return-object p0
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getSoundPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 9
    .line 10
    .line 11
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 12
    .line 13
    invoke-virtual {v2, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 17
    .line 18
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 19
    .line 20
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 31
    .line 32
    .line 33
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 34
    .line 35
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 51
    .line 52
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 53
    .line 54
    .line 55
    new-instance v2, Landroid/graphics/Path;

    .line 56
    .line 57
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    int-to-float v3, v3

    .line 65
    int-to-float v0, v0

    .line 66
    const v4, 0x3ec28f5c    # 0.38f

    .line 67
    .line 68
    .line 69
    mul-float v4, v4, v0

    .line 70
    .line 71
    sub-float/2addr v3, v4

    .line 72
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 73
    .line 74
    .line 75
    move-result v5

    .line 76
    int-to-float v5, v5

    .line 77
    const v6, 0x3e0f5c29    # 0.14f

    .line 78
    .line 79
    .line 80
    mul-float v6, v6, v0

    .line 81
    .line 82
    sub-float/2addr v5, v6

    .line 83
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 87
    .line 88
    .line 89
    move-result v3

    .line 90
    int-to-float v3, v3

    .line 91
    sub-float/2addr v3, v6

    .line 92
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 93
    .line 94
    .line 95
    move-result v5

    .line 96
    int-to-float v5, v5

    .line 97
    sub-float/2addr v5, v6

    .line 98
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 99
    .line 100
    .line 101
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 102
    .line 103
    .line 104
    move-result v3

    .line 105
    int-to-float v3, v3

    .line 106
    const v5, 0x3dcccccd    # 0.1f

    .line 107
    .line 108
    .line 109
    mul-float v5, v5, v0

    .line 110
    .line 111
    add-float/2addr v3, v5

    .line 112
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 113
    .line 114
    .line 115
    move-result v7

    .line 116
    int-to-float v7, v7

    .line 117
    sub-float/2addr v7, v4

    .line 118
    invoke-virtual {v2, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 119
    .line 120
    .line 121
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 122
    .line 123
    .line 124
    move-result v3

    .line 125
    int-to-float v3, v3

    .line 126
    add-float/2addr v3, v5

    .line 127
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 128
    .line 129
    .line 130
    move-result v5

    .line 131
    int-to-float v5, v5

    .line 132
    add-float/2addr v5, v4

    .line 133
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 134
    .line 135
    .line 136
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 137
    .line 138
    .line 139
    move-result v3

    .line 140
    int-to-float v3, v3

    .line 141
    sub-float/2addr v3, v6

    .line 142
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 143
    .line 144
    .line 145
    move-result v5

    .line 146
    int-to-float v5, v5

    .line 147
    add-float/2addr v5, v6

    .line 148
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 149
    .line 150
    .line 151
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 152
    .line 153
    .line 154
    move-result v3

    .line 155
    int-to-float v3, v3

    .line 156
    sub-float/2addr v3, v4

    .line 157
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 158
    .line 159
    .line 160
    move-result v5

    .line 161
    int-to-float v5, v5

    .line 162
    add-float/2addr v5, v6

    .line 163
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 167
    .line 168
    .line 169
    const v3, 0x3c23d70a    # 0.01f

    .line 170
    .line 171
    .line 172
    mul-float v3, v3, v0

    .line 173
    .line 174
    const/high16 v5, 0x40a00000    # 5.0f

    .line 175
    .line 176
    invoke-static {v5, v3}, Ljava/lang/Math;->min(FF)F

    .line 177
    .line 178
    .line 179
    move-result v3

    .line 180
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 181
    .line 182
    .line 183
    move-result v5

    .line 184
    int-to-float v5, v5

    .line 185
    const v7, 0x3e3851ec    # 0.18f

    .line 186
    .line 187
    .line 188
    mul-float v7, v7, v0

    .line 189
    .line 190
    add-float/2addr v5, v7

    .line 191
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 192
    .line 193
    .line 194
    move-result v8

    .line 195
    int-to-float v8, v8

    .line 196
    sub-float/2addr v8, v6

    .line 197
    invoke-virtual {v2, v5, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 198
    .line 199
    .line 200
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 201
    .line 202
    .line 203
    move-result v5

    .line 204
    int-to-float v5, v5

    .line 205
    add-float/2addr v5, v4

    .line 206
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 207
    .line 208
    .line 209
    move-result v8

    .line 210
    int-to-float v8, v8

    .line 211
    const v9, 0x3e8f5c29    # 0.28f

    .line 212
    .line 213
    .line 214
    mul-float v0, v0, v9

    .line 215
    .line 216
    sub-float/2addr v8, v0

    .line 217
    invoke-virtual {v2, v5, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 218
    .line 219
    .line 220
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 221
    .line 222
    .line 223
    move-result v5

    .line 224
    int-to-float v5, v5

    .line 225
    add-float/2addr v5, v4

    .line 226
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 227
    .line 228
    .line 229
    move-result v8

    .line 230
    int-to-float v8, v8

    .line 231
    sub-float/2addr v8, v0

    .line 232
    add-float/2addr v8, v3

    .line 233
    invoke-virtual {v2, v5, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 234
    .line 235
    .line 236
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 237
    .line 238
    .line 239
    move-result v5

    .line 240
    int-to-float v5, v5

    .line 241
    add-float/2addr v5, v7

    .line 242
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 243
    .line 244
    .line 245
    move-result v8

    .line 246
    int-to-float v8, v8

    .line 247
    sub-float/2addr v8, v6

    .line 248
    add-float/2addr v8, v3

    .line 249
    invoke-virtual {v2, v5, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 250
    .line 251
    .line 252
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 253
    .line 254
    .line 255
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 256
    .line 257
    .line 258
    move-result v5

    .line 259
    int-to-float v5, v5

    .line 260
    add-float/2addr v5, v7

    .line 261
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 262
    .line 263
    .line 264
    move-result v8

    .line 265
    int-to-float v8, v8

    .line 266
    invoke-virtual {v2, v5, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 267
    .line 268
    .line 269
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 270
    .line 271
    .line 272
    move-result v5

    .line 273
    int-to-float v5, v5

    .line 274
    add-float/2addr v5, v4

    .line 275
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 276
    .line 277
    .line 278
    move-result v8

    .line 279
    int-to-float v8, v8

    .line 280
    invoke-virtual {v2, v5, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 281
    .line 282
    .line 283
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 284
    .line 285
    .line 286
    move-result v5

    .line 287
    int-to-float v5, v5

    .line 288
    add-float/2addr v5, v4

    .line 289
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 290
    .line 291
    .line 292
    move-result v8

    .line 293
    int-to-float v8, v8

    .line 294
    add-float/2addr v8, v3

    .line 295
    invoke-virtual {v2, v5, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 296
    .line 297
    .line 298
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 299
    .line 300
    .line 301
    move-result v5

    .line 302
    int-to-float v5, v5

    .line 303
    add-float/2addr v5, v7

    .line 304
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 305
    .line 306
    .line 307
    move-result v8

    .line 308
    int-to-float v8, v8

    .line 309
    add-float/2addr v8, v3

    .line 310
    invoke-virtual {v2, v5, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 311
    .line 312
    .line 313
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 314
    .line 315
    .line 316
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 317
    .line 318
    .line 319
    move-result v5

    .line 320
    int-to-float v5, v5

    .line 321
    add-float/2addr v5, v7

    .line 322
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 323
    .line 324
    .line 325
    move-result v8

    .line 326
    int-to-float v8, v8

    .line 327
    add-float/2addr v8, v6

    .line 328
    invoke-virtual {v2, v5, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 329
    .line 330
    .line 331
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 332
    .line 333
    .line 334
    move-result v5

    .line 335
    int-to-float v5, v5

    .line 336
    add-float/2addr v5, v4

    .line 337
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 338
    .line 339
    .line 340
    move-result v8

    .line 341
    int-to-float v8, v8

    .line 342
    add-float/2addr v8, v0

    .line 343
    invoke-virtual {v2, v5, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 344
    .line 345
    .line 346
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 347
    .line 348
    .line 349
    move-result v5

    .line 350
    int-to-float v5, v5

    .line 351
    add-float/2addr v5, v4

    .line 352
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 353
    .line 354
    .line 355
    move-result v4

    .line 356
    int-to-float v4, v4

    .line 357
    add-float/2addr v4, v0

    .line 358
    add-float/2addr v4, v3

    .line 359
    invoke-virtual {v2, v5, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 360
    .line 361
    .line 362
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 363
    .line 364
    .line 365
    move-result v0

    .line 366
    int-to-float v0, v0

    .line 367
    add-float/2addr v0, v7

    .line 368
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 369
    .line 370
    .line 371
    move-result p1

    .line 372
    int-to-float p1, p1

    .line 373
    add-float/2addr p1, v6

    .line 374
    add-float/2addr p1, v3

    .line 375
    invoke-virtual {v2, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 376
    .line 377
    .line 378
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 379
    .line 380
    .line 381
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 382
    .line 383
    .line 384
    new-instance p1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 385
    .line 386
    invoke-direct {p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 387
    .line 388
    .line 389
    const/4 v0, 0x0

    .line 390
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 391
    .line 392
    .line 393
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 394
    .line 395
    .line 396
    move-result-object p0

    .line 397
    if-eqz p0, :cond_0

    .line 398
    .line 399
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 400
    .line 401
    .line 402
    move-result v0

    .line 403
    if-nez v0, :cond_0

    .line 404
    .line 405
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 406
    .line 407
    .line 408
    move-result-object v0

    .line 409
    invoke-virtual {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 410
    .line 411
    .line 412
    move-result p0

    .line 413
    const-wide/high16 v2, -0x4020000000000000L    # -0.5

    .line 414
    .line 415
    invoke-virtual {v0, p0, v2, v3}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 416
    .line 417
    .line 418
    move-result p0

    .line 419
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 420
    .line 421
    .line 422
    goto :goto_0

    .line 423
    :cond_0
    const p0, -0x70aaaaab

    .line 424
    .line 425
    .line 426
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 427
    .line 428
    .line 429
    :goto_0
    invoke-virtual {v1, p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 430
    .line 431
    .line 432
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 433
    .line 434
    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 435
    .line 436
    .line 437
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/actionButton/ActionButtonPathBuilder;->pathExList:Ljava/util/List;

    .line 438
    .line 439
    return-object p0
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method
